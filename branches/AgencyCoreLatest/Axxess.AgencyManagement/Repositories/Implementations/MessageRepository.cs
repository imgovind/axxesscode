﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.AgencyManagement;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class MessageRepository : IMessageRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MessageRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IMessageRepository Members

        public IList<Message> GetSentMessages(Guid userId, Guid agencyId, int pageNumber, int pageSize)
        {
            var messages = new List<Message>();
            if (!userId.IsEmpty() && !agencyId.IsEmpty())
            {
                var sql = new StringBuilder("SELECT Id, Created, FromName, Subject, RecipientNames FROM messagedetails ")
                    .AppendFormat("WHERE FromId = '{0}' ", userId)
                    .AppendFormat("AND AgencyId = '{0}' ", agencyId)
                    .AppendFormat("ORDER BY Created DESC LIMIT {0}, {1}", (pageNumber - 1) * pageSize, pageSize)
                    .ToString();

                using (var cmd = new FluentCommand<Message>(sql))
                {
                    messages = cmd.SetConnection("AgencyManagementConnectionString")
                       .SetMap(reader => new Message
                       {
                           Id = reader.GetGuid("Id"),
                           Created = reader.GetDateTime("Created"),
                           FromName = reader.GetString("FromName"),
                           Subject = reader.GetStringNullable("Subject"),
                           RecipientNames = reader.GetStringNullable("RecipientNames"),
                           Type = MessageType.Sent
                       }).AsList();
                }
            }

            return messages;
        }


        public IList<Message> GetMessages(Guid userId, Guid agencyId, Guid folderId, int pageNumber, bool isDeprecated, int pageSize)
        {
            var messages = new List<Message>();
            if (!userId.IsEmpty() && !agencyId.IsEmpty())
            {
                var sql = new StringBuilder("SELECT * FROM (")
                    .Append("SELECT messagedetails.FromName, usermessages.MessageId, ")
                    .Append("usermessages.MessageType, usermessages.IsRead, ")
                    .Append("usermessages.IsDeprecated, messagedetails.FromId, ")
                    .Append("messagedetails.Subject, messagedetails.Created ")
                    .Append("FROM usermessages JOIN messagedetails ON usermessages.MessageId = messagedetails.Id ")
                    .AppendFormat("WHERE usermessages.UserId = '{0}' ", userId)
                    .AppendFormat("AND usermessages.AgencyId = '{0}' ", agencyId)
                    //.AppendFormat("AND usermessages.FolderId = '{0}' ", folderId)
                    .AppendFormat("AND usermessages.IsDeprecated = {0} ", isDeprecated ? 1 : 0)
                    .Append("AND usermessages.MessageType = 0 ")
                    .Append("UNION ")
                    .Append("SELECT 'Axxess', usermessages.MessageId, usermessages.MessageType, ")
                    .Append("usermessages.IsRead, usermessages.IsDeprecated, '00000000-0000-0000-0000-000000000000', ")
                    .Append("systemmessages.Subject, systemmessages.Created ")
                    .Append("FROM usermessages JOIN systemmessages ON usermessages.MessageId = systemmessages.Id     ")
                    .AppendFormat("WHERE usermessages.UserId = '{0}' ", userId)
                    .AppendFormat("AND usermessages.AgencyId = '{0}' ", agencyId)
                    //.AppendFormat("AND usermessages.FolderId = '{0}' ", folderId)
                    .AppendFormat("AND usermessages.IsDeprecated = {0} ", isDeprecated ? 1 : 0)
                    .Append("AND usermessages.MessageType = 1 ")
                    .AppendFormat(") allmessages ORDER BY Created DESC LIMIT {0}, {1}", (pageNumber - 1) * pageSize, pageSize)
                    .ToString();

                using (var cmd = new FluentCommand<Message>(sql))
                {
                    messages = cmd.SetConnection("AgencyManagementConnectionString")
                       .SetMap(reader => new Message
                       {
                           Id = reader.GetGuid("MessageId"),
                           Subject = reader.GetStringNullable("Subject"),
                           MarkAsRead = reader.GetBoolean("IsRead"),
                           FromName = reader.GetString("FromName"),
                           FromId = reader.GetGuidIncludeEmpty("FromId"),
                           Created = reader.GetDateTime("Created"),
                           IsDeprecated = reader.GetBoolean("IsDeprecated"),
                           Type = reader.GetInt("MessageType").ToEnum<MessageType>(MessageType.User)
                       }).AsList();
                }
            }

            return messages;
        }


        public bool Delete(Guid messageId, Guid userId, Guid agencyId)
        {
            var result = false;
            var message = database.Single<UserMessage>(m => m.MessageId == messageId && m.UserId == userId && m.AgencyId == agencyId);
            if (message != null)
            {
                message.IsDeprecated = true;
                database.Update<UserMessage>(message);
                result = true;
            }

            return result;
        }

        public bool DeleteMany(List<Guid> messageList, Guid userId, Guid agencyId)
        {
            var result = false;
            var messageIds = string.Empty;
            messageList.ForEach(messageId =>
            {
                messageIds += string.Format("'{0}',", messageId);
            });

            var sql = new StringBuilder("UPDATE usermessages SET IsDeprecated = 1 ")
            .AppendFormat("WHERE messageid in ({0}) ", messageIds.RemoveAt(messageIds.Length - 1))
            .AppendFormat("AND AgencyId = '{0}' ", agencyId)
            .AppendFormat("AND UserId = '{0}' ", userId)
            .ToString();

            using (var cmd = new FluentCommand<Message>(sql))
            {
                if (cmd.SetConnection("AgencyManagementConnectionString").AsNonQuery() > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        public bool Read(Guid messageId, Guid userId, Guid agencyId)
        {
            var result = false;
            var message = database.Single<UserMessage>(m => m.MessageId == messageId && m.UserId == userId && m.AgencyId == agencyId);
            if (message != null)
            {
                message.IsRead = true;
                database.Update<UserMessage>(message);
                result = true;
            }

            return result;
        }

        public Message GetUserMessage(Guid messageId, Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(userId, "UserId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(messageId, "MessageId");

            var message = new Message();

            var sql = new StringBuilder("SELECT messagedetails.FromName, usermessages.MessageId, usermessages.UserId, ")
                    .Append("usermessages.AgencyId, usermessages.MessageType, usermessages.FolderId, usermessages.IsRead, ")
                    .Append("usermessages.IsDeprecated, messagedetails.FromId, messagedetails.AttachmentId, messagedetails.RecipientNames, ")
                    .Append("messagedetails.CarbonCopyNames, messagedetails.PatientId, messagedetails.Subject, messagedetails.Body, ")
                    .Append("messagedetails.Created ")
                    .Append("FROM messagedetails INNER JOIN usermessages ON messagedetails.Id = usermessages.MessageId ")
                    .AppendFormat("WHERE messagedetails.Id = '{0}' ", messageId)
                    .AppendFormat("AND messagedetails.AgencyId = '{0}' ", agencyId)
                    .AppendFormat("AND usermessages.UserId = '{0}' ", userId)
                    .Append("AND usermessages.MessageType = 0 LIMIT 0, 1;")
                    .ToString();

            using (var cmd = new FluentCommand<Message>(sql))
            {
                message = cmd.SetConnection("AgencyManagementConnectionString")
                   .SetMap(reader => new Message
                   {
                       Id = reader.GetGuid("MessageId"),
                       FromName = reader.GetString("FromName"),
                       Body = reader.GetStringNullable("Body"),
                       Created = reader.GetDateTime("Created"),
                       MarkAsRead = reader.GetBoolean("IsRead"),
                       FromId = reader.GetGuidIncludeEmpty("FromId"),
                       Subject = reader.GetStringNullable("Subject"),
                       FolderId = reader.GetGuidIncludeEmpty("FolderId"),
                       RecipientId = reader.GetGuidIncludeEmpty("UserId"),
                       PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                       AttachmentId = reader.GetGuidIncludeEmpty("AttachmentId"),
                       RecipientNames = reader.GetStringNullable("RecipientNames"),
                       CarbonCopyNames = reader.GetStringNullable("CarbonCopyNames"),
                       IsDeprecated = reader.GetBoolean("IsDeprecated"),
                       Type = reader.GetInt("MessageType").ToEnum<MessageType>(MessageType.User)
                   }).AsSingle();
            }

            return message;
        }

        public Message GetSentMessage(Guid messageId, Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(messageId, "messageId");

            var message = database.Single<MessageDetail>(m => m.Id == messageId && m.FromId == userId && m.AgencyId == agencyId);
            if (message != null)
            {
                return new Message
                {
                    Id = message.Id,
                    Body = message.Body,
                    FromId = message.FromId,
                    Subject = message.Subject,
                    Created = message.Created,
                    FromName = message.FromName,
                    PatientId = message.PatientId,
                    AttachmentId = message.AttachmentId,
                    RecipientNames = message.RecipientNames,
                    CarbonCopyNames = message.CarbonCopyNames,
                    Type = MessageType.Sent
                };
            }
            return new Message();
        }

        public Message GetSystemMessage(Guid messageId, Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(userId, "UserId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(messageId, "MessageId");

            var message = new Message();

            var sql = new StringBuilder("SELECT systemmessages.Id as MessageId, ")
            .Append("usermessages.UserId, usermessages.MessageType, '00000000-0000-0000-0000-000000000000' as FromId, ")
            .Append("usermessages.FolderId, usermessages.IsRead, usermessages.IsDeprecated, systemmessages.Subject, ")
            .Append("systemmessages.Body, systemmessages.Created ")
            .Append("FROM systemmessages INNER JOIN usermessages ON systemmessages.Id = usermessages.MessageId ")
            .AppendFormat("WHERE systemmessages.Id = '{0}' ", messageId)
            .AppendFormat("AND usermessages.AgencyId = '{0}' ", agencyId)
            .AppendFormat("AND usermessages.UserId = '{0}' ", userId)
            .Append("AND usermessages.MessageType = 1 LIMIT 0, 1;")
            .ToString();

            using (var cmd = new FluentCommand<Message>(sql))
            {
                message = cmd.SetConnection("AgencyManagementConnectionString")
                   .SetMap(reader => new Message
                   {
                       FromName = "Axxess",
                       Id = reader.GetGuid("MessageId"),
                       Body = reader.GetStringNullable("Body"),
                       Created = reader.GetDateTime("Created"),
                       MarkAsRead = reader.GetBoolean("IsRead"),
                       FromId = reader.GetGuidIncludeEmpty("FromId"),
                       Subject = reader.GetStringNullable("Subject"),
                       RecipientId = reader.GetGuidIncludeEmpty("UserId"),
                       FolderId = reader.GetGuidIncludeEmpty("FolderId"),
                       IsDeprecated = reader.GetBoolean("IsDeprecated"),
                       Type = reader.GetInt("MessageType").ToEnum<MessageType>(MessageType.System)
                   }).AsSingle();
            }

            return message;
        }

        public bool AddMessageDetail(MessageDetail messageDetail)
        {
            bool result = false;

            if (messageDetail != null)
            {
                database.Add<MessageDetail>(messageDetail);
                result = true;
            }

            return result;
        }

        public bool AddUserMessage(UserMessage userMessage)
        {
            bool result = false;

            if (userMessage != null)
            {
                database.Add<UserMessage>(userMessage);
                result = true;
            }

            return result;
        }

        public bool AddUserMessage(List<UserMessage> userMessages)
        {
            bool result = false;

            if (userMessages.Count > 0)
            {
                database.AddMany<UserMessage>(userMessages);
                result = true;
            }

            return result;
        }

        public List<SystemMessage> GetSystemMessages()
        {
            return database.All<SystemMessage>().ToList();
        }

        public bool AddSystemMessage(SystemMessage message)
        {
            bool result = false;

            if (message != null)
            {
                database.Add<SystemMessage>(message);
                result = true;
            }

            return result;
        }

        public int GetUserMessageCount(Guid userId, Guid agencyId, bool isDeprecated)
        {
            int count = 0;
            var sql = new StringBuilder("SELECT COUNT(*) FROM usermessages ")
                .AppendFormat("WHERE UserId = '{0}' ", userId)
                .AppendFormat("AND AgencyId = '{0}' ", agencyId)
                .AppendFormat("AND IsDeprecated = {0};", isDeprecated ? 1 : 0)
                .ToString();
            using (var cmd = new FluentCommand<int>(sql))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString").AsScalar();
            }
            return count;
        }

        public int GetSentMessageCount(Guid userId, Guid agencyId)
        {
            int count = 0;
            var sql = new StringBuilder("SELECT COUNT(*) FROM messagedetails ")
                .AppendFormat("WHERE FromId = '{0}' ", userId)
                .AppendFormat("AND AgencyId = '{0}';", agencyId)
                .ToString();
            using (var cmd = new FluentCommand<int>(sql))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString").AsScalar();
            }
            return count;
        }

        public DashboardMessage GetCurrentDashboardMessage()
        {
            DashboardMessage message = null;
            var folderScript =
               @"select Id,Title, Text,Created,CreatedBy from dashboardmessages  order by Created desc limit 1;";


            using (var cmd = new FluentCommand<DashboardMessage>(folderScript))
            {
                message = cmd.SetConnection("AgencyManagementConnectionString")
                   .SetMap(reader => new DashboardMessage
                   {
                       Id = reader.GetGuid("Id"),
                       Title = reader.GetStringNullable("Title"),
                       Text = reader.GetStringNullable("Text"),
                       CreatedBy = reader.GetStringNullable("CreatedBy"),
                       Created = reader.GetDateTime("Created")
                   }).AsSingle();
            }

            return message;
           // return database.All<DashboardMessage>().OrderByDescending(m => m.Created).FirstOrDefault();
        }

        public bool AddDashboardMessage(DashboardMessage message)
        {
            bool result = false;

            if (message.IsNull() == true) return result;

            message.Id = Guid.NewGuid();
            message.Created = DateTime.Now;
            database.Add<DashboardMessage>(message);
            result = true;

            return result;
        }

        #endregion

        #region Folder functionality

        public bool AddMessageFolder(Guid ownerId, Guid agencyId, string name)
        {
            bool status = false;

            Guid folderId = Guid.NewGuid();

            if ((name != null) && (name.Length > 0))
            {
                if (!folderId.IsEmpty() && !folderId.IsNull())
                {
                    MessageFolder folder = new MessageFolder();
                    folder.Id = folderId;
                    folder.ParentId = new Guid("00000000-0000-0000-0000-000000000000");
                    folder.OwnerId = ownerId;
                    folder.AgencyId = agencyId;
                    folder.Name = name;
                    folder.Created = DateTime.Now;
                    database.Add<MessageFolder>(folder);
                    status = true;
                }
            }
            return status;
        }

        public IList<MessageFolder> FolderList(Guid userId, Guid agencyId)
        {
            var folderScript =
                @"select Id, ParentId, OwnerId, AgencyId, Name, Created from messagefolders where OwnerId = @userId and AgencyId = @agencyId order by Name desc;";


            return new FluentCommand<MessageFolder>(folderScript)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("userId", userId)
                .AddGuid("agencyId", agencyId)
                .SetMap(reader => new MessageFolder
                {
                    Id = reader.GetGuid("Id"),
                    ParentId = reader.GetGuid("ParentId"),
                    OwnerId = reader.GetGuid("OwnerId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Name = reader.GetString("Name"),
                    Created = (System.DateTime)reader.GetDateTimeNullable("Created")
                }).AsList();
        }

        public bool DeleteFolder(Guid folderId)
        {
            bool status = false;
            if (folderId.IsEmpty() || folderId.IsNull()) return status;
            status = true;

            // todo: set all folder guids to guid.empty for both system messages and user messages in this folder.

            status = ReturnUserMessagesToInbox(folderId);

            if (status == true)
                status = ReturnSystemMessagesToInbox(folderId);

            return status;
        }

        private bool ReturnSystemMessagesToInbox(Guid folderId)
        {
            bool status = true;
            return status;
        }

        private bool ReturnUserMessagesToInbox(Guid folderId)
        {
            bool status = true;
            return status;
        }

        public bool MoveMessageToFolder(Guid userId, Guid messageId, Guid folderId, string messageType)
        {
            bool status = false;
            if (messageId.IsEmpty() || messageId.IsNull()) return status;
            if (folderId.IsNull()) return status;

            MessageType t = (MessageType)Enum.Parse(typeof(MessageType), messageType);

            switch (t)
            {
                case MessageType.System:
                    {
                        status = MoveSystemMessageToFolder(userId, messageId, folderId, messageType);

                    }
                    break;
                case MessageType.Sent:
                case MessageType.User:
                    {
                        UserMessage message = database.Single<UserMessage>(m => m.MessageId == messageId);

                        if (message != null)
                        {
                            message.FolderId = folderId;
                            database.Update<UserMessage>(message);
                            status = true;
                        }
                    }

                    break;
                default:
                    status = false;
                    break;
            }
            return status;
        }

        public bool MoveSystemMessageToFolder(Guid userId, Guid messageId, Guid folderId, string messageType)
        {
            bool status = false;

            UserMessage message = database.Single<UserMessage>(msg => (msg.MessageId == messageId) && (msg.UserId == userId));
            if (message != null)
            {
                message.FolderId = folderId;
                database.Update<UserMessage>(message);
                status = true;
            }

            return status;
        }

        #endregion

    }
}