﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;
    using Extensions;

    using SubSonic.Repository;
    using System.Text;

    public class AccountingRepository : IAccountingRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AccountingRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }


        #endregion

        #region IAccountingRepository Members

        public Payment GetAccountingPayment(string accountId)
        {
            var payment = new Payment();

            var sql = new StringBuilder("SELECT payment.Id, payment.Account, payment.Sequence, ")
            .Append("payment.Amount, payment.Start, payment.End, payment.Package ")
            .Append("from payment ")
            .Append("where payment.Account = @accountId ")
            .Append("Order by payment.Sequence Desc Limit 1;")
            .ToString();

            using (var cmd = new FluentCommand<Payment>(sql))
            {
                payment = cmd.SetConnection("AccountingConnectionString")
                    .AddString("accountId", accountId)

                    .SetMap(reader => new Payment
                    {
                        Id = reader.GetInt("Id"),
                        Account = reader.GetString("Account"),
                        Sequence = reader.GetInt("Sequence"),
                        Amount = reader.GetDouble("Amount"),
                        Start = reader.GetDateTime("Start"),
                        End = reader.GetDateTime("End"),
                        Package = reader.GetInt("Package")

                    })
                    .AsSingle();
            }

            return payment;
        }

        public double GetAmount(bool IsUserPlan, int RequestedPackageId)
        {
            if (IsUserPlan)
            {
                switch (RequestedPackageId)
                {
                    case 5:
                        return 499;
                    case 10:
                        return 699;
                    case 20:
                        return 899;
                    case 30:
                        return 1099;
                    case 40:
                        return 1299;
                    case 100:
                        return 1499;
                    case 200:
                        return 1699;
                    default:
                        return 0;
                }
            }
            else
            {
                switch (RequestedPackageId)
                {
                    case 25:
                        return 499;
                    case 50:
                        return 699;
                    case 75:
                        return 899;
                    case 100:
                        return 1099;
                    case 150:
                        return 1299;
                    case 200:
                        return 1499;
                    case 300:
                        return 1699;
                    default:
                        return 0;
                }
            }
        }

        public bool AddUpgrade(AgencyUpgrade agencyUpgrade)
        {
            if (agencyUpgrade != null)
            {
                agencyUpgrade.Modified = DateTime.Now;
                database.Add<AgencyUpgrade>(agencyUpgrade);
                return true;
            }
            return false;
        }

        public bool RemoveAgencyUpgrade(Guid Id, Guid agencyId)
        {
            try
            {
                var sql = @"DELETE
                             from  agencyupgrades 
                                         WHERE
                                              agencyId = @agencyId AND Id = @Id";

                using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
                {
                    return cmd.SetConnection("AccountingConnectionString")
                         .AddGuid("Id", Id)
                         .AddGuid("agencyId", agencyId)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddChange(AgencyChange agencyChange)
        {
            if (agencyChange != null)
            {
                agencyChange.Id = Guid.NewGuid();
                agencyChange.Created = DateTime.Now;
                database.Add<AgencyChange>(agencyChange);
                return true;
            }
            return false;
        }

        public bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            var result = false;
            if (plan != null)
            {
                database.Update<AgencySubscriptionPlan>(plan);
                result = true;
            }
            return result;
        }

        public bool AddSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            if (plan != null)
            {
                plan.Id = Guid.NewGuid();
                database.Add<AgencySubscriptionPlan>(plan);
                return true;
            }
            return false;
        }

        public List<AgencySubscriptionPlan> GetSubscriptionPlans(Guid agencyId)
        {
            return database.Find<AgencySubscriptionPlan>(p => p.AgencyId == agencyId).ToList();
        }

        public AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId)
        {
            return database.Single<AgencySubscriptionPlan>(p => p.AgencyId == agencyId && p.AgencyLocationId == locationId);
        }

        #endregion


    }
}
