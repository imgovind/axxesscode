﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    public static class UserAgencyExtensions
    {
        public static bool HasTrialPeriodExpired(this UserAgencyInfo info)
        {
            var expirationDate = info.Created.Add(TimeSpan.FromDays(info.TrialPeriod));
            if (!info.IsAgreementSigned && DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }

        public static bool IsReadOnlyPeriodExpired(this UserAgencyInfo info)
        {
            var expirationDate = info.FrozenDate.AddYears(2);
            if (DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }

        public static bool HasTrialAccountExpired(this UserAgencyInfo info)
        {
            var result = false;

            if (info != null && info.AccountExpireDate != DateTime.MinValue)
            {
                if (info.AccountExpireDate < DateTime.Today)
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool AllowWeekendAccess(this UserAgencyInfo info)
        {
            var result = false;

            if (info != null)
            {
                if (DateTime.Today.IsWeekend())
                {
                    if (info.AllowWeekendAccess)
                    {
                        if (info.EarliestLoginTime.IsNotNullOrEmpty())
                        {
                            var earliestTime = new Time(info.EarliestLoginTime);
                            if (earliestTime != null && earliestTime.TimeOfDay <= DateTime.Now.TimeOfDay)
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsReadOnly(this UserAgencyInfo info)
        {
            if (info.IsFrozen && info.FrozenDate.IsInPast())
            {
                return true;
            }
            return false;
        }
    }
}
