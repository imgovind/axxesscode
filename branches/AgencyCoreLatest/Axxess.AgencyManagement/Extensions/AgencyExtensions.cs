﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class AgencyExtensions
    {
        public static int NextPatientSubscriptionPlan(this int planLimit)
        {
            if (planLimit > 0)
            {
                if (planLimit == 25)
                {
                    return 50;
                }
                else if (planLimit == 50)
                {
                    return 75;
                }
                else if (planLimit == 75)
                {
                    return 100;
                }
                else if (planLimit == 100)
                {
                    return 150;
                }
                else if (planLimit == 150)
                {
                    return 200;
                }
                else if (planLimit == 200)
                {
                    return 300;
                }
                else if (planLimit == 300)
                {
                    return 100000;
                }
            }
            return 0;
        }

        public static string PatientSubscriptionPlanName(this int planLimit)
        {
            if (planLimit > 0)
            {
                if (planLimit >= 100000)
                {
                    return "over 300 patients";
                }
                return string.Format("{0} patients", planLimit.ToString());
            }
            return string.Empty;
        }

        public static string UserSubscriptionPlanName(this int planLimit)
        {
            if (planLimit > 0)
            {
                if (planLimit == 100000)
                {
                    return "over 200 users";
                }
                else if (planLimit == 100001)
                {
                    return "over 200 users (Premier Plan)";
                }
                else if (planLimit == 100002)
                {
                    return "over 200 users (Enterprise Plan)";
                }
                return string.Format("{0} users", planLimit.ToString());
            }
            return string.Empty;
        }

        public static int NextUserSubscriptionPlan(this int planLimit)
        {
            if (planLimit > 0)
            {
                if (planLimit == 5)
                {
                    return 10;
                }
                else if (planLimit == 10)
                {
                    return 20;
                }
                else if (planLimit == 20)
                {
                    return 30;
                }
                else if (planLimit == 30)
                {
                    return 40;
                }
                else if (planLimit == 40)
                {
                    return 100;
                }
                else if (planLimit == 100)
                {
                    return 200;
                }
                else if (planLimit == 200)
                {
                    return 100000;
                }
                else if (planLimit == 100000)
                {
                    return 100001;
                }
                else if (planLimit == 100001)
                {
                    return 100002;
                }
            }
            return 0;
        }

        public static bool HasTrialPeriodExpired(this Agency agency)
        {
            var expirationDate = agency.Created.Add(TimeSpan.FromDays(agency.TrialPeriod));
            if (!agency.IsAgreementSigned && DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }

        public static AgencyLocation GetMainOffice(this Agency agency)
        {
            if (agency.Branches != null && agency.Branches.Count > 0)
            {
                return agency.Branches.Where(b => b.IsMainOffice == true).SingleOrDefault();
            }
            return null;
        }

        public static AgencyLocation GetBranch(this Agency agency, Guid locationId)
        {
            if (agency != null && agency.Branches != null && agency.Branches.Count > 0)
            {
                var location = agency.Branches.SingleOrDefault(b => b.Id == locationId);
                if (location != null && !location.IsLocationStandAlone)
                {
                    location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    location.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                    location.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                }
                return location;
            }
            return null;
        }

        public static bool IsReadOnly(this Agency agency)
        {
            if (agency.IsFrozen && agency.FrozenDate.IsInPast())
            {
                return true;
            }
            return false;
        }

        public static bool IsReadOnlyPeriodExpired(this Agency agency)
        {
            var expirationDate = agency.FrozenDate.AddYears(2);
            if (DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }
    }
}
