﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class UserAgencyInfo
    {
        #region Members

        public int Status { get; set; }
        public Guid UserId { get; set; }
        public string Roles { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public bool IsFrozen { get; set; }
        public string Suffix { get; set; }
        public int ClusterId { get; set; }
        public bool IsPrimary { get; set; }
        public string LastName { get; set; }
        public bool IsSuspended { get; set; }
        public string FirstName { get; set; }
        public string TitleType { get; set; }
        public string MiddleName { get; set; }
        public string AgencyName { get; set; }
        public bool IsDeprecated { get; set; }
        public string Credentials { get; set; }
        public string Permissions { get; set; }
        public string ProfileData { get; set; }
        public string AgencyPayor { get; set; }
        public DateTime FrozenDate { get; set; }
        public string TitleTypeOther { get; set; }
        public bool AllowWeekendAccess { get; set; }
        public string CredentialsOther { get; set; }
        public string EarliestLoginTime { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public DateTime AccountExpireDate { get; set; }
        public string OasisAuditVendorApiKey { get; set; }

        public DateTime Created { get; set; }
        public int TrialPeriod { get; set; }
        public bool IsAgreementSigned { get; set; }

        public string DisplayName
        {
            get
            {
                var nameBuilder = new StringBuilder();

                if (Credentials.IsEqual("None"))
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Suffix);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");

                    return nameBuilder.ToString().TrimEnd();
                }

                if (CredentialsOther.IsNotNullOrEmpty()&&Credentials=="Other")
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                    if (this.CredentialsOther.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.CredentialsOther);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");
                    return nameBuilder.ToString().TrimEnd();
                }

                if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                if (this.Credentials.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Credentials);
                if (IsDeprecated) nameBuilder.Append(" [deleted]");
                return nameBuilder.ToString().TrimEnd();
            }
        }
        public string AddressFull
        {
            get
            {
                var userProfile = this.ProfileData.IsNotNullOrEmpty() ? this.ProfileData.ToObject<UserProfile>() : null;
                if (userProfile != null)
                {
                    return userProfile.AddressFull;
                }
                return string.Empty;
            }
        }
        public List<string> PermissionsArray
        {
            get
            {
                if (this.Permissions.IsNotNullOrEmpty())
                {
                    return this.Permissions.ToObject<List<string>>();
                }
                else
                {
                    return new List<string>();
                }
            }
        }
        public override string ToString()
        {
            return this.DisplayName;
        }

        #endregion
    }
}
