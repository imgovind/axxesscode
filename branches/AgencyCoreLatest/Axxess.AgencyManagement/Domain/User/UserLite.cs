﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;
    public class UserLite
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContactPersonDisplayName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhoneFormatted { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public string ActionText { get; set; }
        public string Location
        {
            get
            {
                var location = string.Empty;
                if (City.IsNotNullOrEmpty() && State.IsNotNullOrEmpty())
                {
                    location = string.Format("{0}, {1}", City.Trim(), State.Trim());
                }
                return location;
            }
        }
        public string DateSortable
        {
            get
            {
                return Date.IsNotNullOrEmpty() ? "<div class='float-left'><span class='float-right'>" + Date.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + Date.Split('/')[0] + "/" + Date.Split('/')[1] + "</span></div>" : "";
            }
        }
        public string AttentionLevel
        {
            get
            {
                var attentionLevel = string.Empty;
                var createdDate = DateTime.Parse(this.Date);
                if (createdDate >= DateTime.Now.AddDays(-90))
                {
                    attentionLevel = "red";
                }
                else if (createdDate >= DateTime.Now.AddDays(-180) && createdDate <= DateTime.Now.AddDays(-90))
                {
                    attentionLevel = "blue";
                }
                return attentionLevel;
            }
        }
    }
}
