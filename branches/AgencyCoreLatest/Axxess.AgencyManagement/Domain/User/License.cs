﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;
    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;

    public class License
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AssetId { get; set; }
        public DateTime Created { get; set; }
        public string LicenseType { get; set; }
        [XmlIgnore]
        private string licenseNumber;
        public string LicenseNumber
        {
            get { return licenseNumber.IsNotNullOrEmpty() ? licenseNumber : ""; }
            set { licenseNumber = value; }
        }
        public DateTime InitiationDate { get; set; }
        public bool IsDeprecated { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        public string OtherLicenseType { get; set; }

        [XmlIgnore]
        public string AssetUrl { get; set; }

        [XmlIgnore]
        public Guid UserId { get; set; }

        [XmlIgnore]
        public string InitiationDateFormatted { get { return this.InitiationDate>DateTime.MinValue? this.InitiationDate.ToString("MM/dd/yyyy"):string.Empty; } }

        [XmlIgnore]
        public string ExpirationDateFormatted { get { return this.ExpirationDate>DateTime.MinValue? this.ExpirationDate.ToString("MM/dd/yyyy"):string.Empty; } }

        [XmlIgnore]
        public string UserDisplayName { get; set; }

        [XmlIgnore]
        public string CustomId { get; set; }

        [XmlIgnore]
        public string Employee { get { 
            return UserDisplayName + (CustomId.IsNotNullOrEmpty() ? " - " + CustomId : ""); } }

        #endregion
    }
}
