﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public class AgencyTeam : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string Users { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public List<Guid> UserArray { get; set; }

        [SubSonicIgnore]
        public string UserToolTip { get; set; }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Name is required.<br/>"));
        }
    }
}
