﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class DeletedItem
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Schedule { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
