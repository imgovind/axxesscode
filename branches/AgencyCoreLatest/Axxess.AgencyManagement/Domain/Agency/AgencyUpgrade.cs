﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;

    public class AgencyUpgrade
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public double Amount { get; set; }
        public string Comments { get; set; }
        public int AnnualPlanId { get; set; }
        public DateTime Created { get; set; }
        public string AccountId { get; set; }
        public Guid RequestedById { get; set; }
        public Guid AgencyLocationId { get; set; }
        public int PreviousPackageId { get; set; }
        public double PreviousAmount { get; set; }
        public int RequestedPackageId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool IsUserPlan { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string RequestedPackageDescription
        {
            get
            {
                if (this.PreviousPackageId != this.RequestedPackageId)
                {
                    if (this.IsUserPlan)
                    {

                        return RequestedPackageId.UserSubscriptionPlanName();
                    }
                    else
                    {
                        return RequestedPackageId.PatientSubscriptionPlanName();
                    }
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string AnnualPlanDescription
        {
            get
            {
                if (this.AnnualPlanId == 1)
                {
                    return "5% Discount for 6 month payment";
                }
                else if (this.AnnualPlanId == 2)
                {
                    return "10% Discount for full year payment";
                }
                else if (this.AnnualPlanId == 3)
                {
                    return "15% Discount for 2 years payment";
                }
                return string.Empty;
            }
        }

        #endregion

    }
}
