﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;

    public class PatientVisitLog
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime Created { get; set; }
        public string PatientAddress { get; set; }
        public string VerifiedAddress { get; set; }
        public decimal VerifiedLatitude { get; set; }
        public decimal VerifiedLongitude { get; set; }
        public DateTime VerifiedDateTime { get; set; }
        public String Signature { get; set; }
        public Guid PatientSignatureAssetId { get; set; }
        public string PatientUnableToSignReason { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }

        [SubSonicIgnore]
        public string DateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }

        [SubSonicIgnore]
        public string PatientMRN { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public string TaskName { get; set; }

        [SubSonicIgnore]
        public DateTime DOB { get; set; }

        [SubSonicIgnore]
        public DateTime StartofCareDate { get; set; }

        [SubSonicIgnore]
        public string AddressLine1 { get; set; }

        [SubSonicIgnore]
        public string AddressLine2 { get; set; }

        [SubSonicIgnore]
        public string AddressCity { get; set; }

        [SubSonicIgnore]
        public string AddressStateCode { get; set; }

        [SubSonicIgnore]
        public string AddressZipCode { get; set; }

        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1} {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
    }
}
