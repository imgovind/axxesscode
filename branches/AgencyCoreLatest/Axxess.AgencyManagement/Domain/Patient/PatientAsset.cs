﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public class PatientAsset : EntityBase
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid AssetId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ClassificationId { get; set; }
        public bool IsDeprecated { get; set; }
        public string Filename { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string ModifiedDateFormatted { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string ClassificationName { get; set; }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => ClassificationId != Guid.Empty, "Classification is required.<br/>"));
        }
    }
}
