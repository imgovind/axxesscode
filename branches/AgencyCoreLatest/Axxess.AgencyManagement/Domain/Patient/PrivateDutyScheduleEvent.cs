﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    public class PrivateDutyScheduleEvent : ScheduleEvent {
        public bool AllDay;
        public DateTime StartTime;
        public DateTime EndTime;
    }
}
