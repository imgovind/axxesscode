﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    using Enums;
    using Axxess.Core.Infrastructure;

    public class NonAdmit
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string DateOfBirth { get; set; }
        public string NonAdmitDate { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string NonAdmissionReason { get; set; }
        public string MedicareNumber { get; set; }
        public string InsuranceNumber { get; set; }
        public string Comments { get; set; }
        public string CommentsCleaned
        {
            get
            {
                return this.Comments.IsNotNullOrEmpty() ? this.Comments.Clean() : string.Empty;
            }
        }

        public NonAdmitTypes Type { get; set; }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName, " ", this.MiddleInitial.IsNotNullOrEmpty() ? this.MiddleInitial + "." : string.Empty).ToTitleCase();
            }
        }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1} {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), StringFormatter.FormatZipCode(this.AddressZipCode.TrimWithNullable()));
            }
        }
        public string AddressFullFormatted
        {
            get
            {
                if (this.AddressFull.IsNotNullOrEmpty())
                {
                    return string.Format("{0}\n{1}, {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), StringFormatter.FormatZipCode(this.AddressZipCode.TrimWithNullable()));
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string InsuranceName { get; set; }
        public string PolicyNumber
        {
            get
            {
                string policyNumber = "";
                if (this.MedicareNumber.IsNotNullOrEmpty())
                {
                    policyNumber = this.MedicareNumber;
                }
                else
                {
                    policyNumber = this.InsuranceNumber;
                }
                return policyNumber;
            }
        }

    }
}
