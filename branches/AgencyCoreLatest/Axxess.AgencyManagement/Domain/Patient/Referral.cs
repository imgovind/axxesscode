﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class Referral : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ReferrerPhysician { get; set; }
        public int AdmissionSource { get; set; }
        public DateTime ReferralDate { get; set; }
        public Guid InternalReferral { get; set; }
        public string OtherReferralSource { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }
        public string MaritalStatus { get; set; }
        public float Height { get; set; }
        public int HeightMetric { get; set; }
        public float Weight { get; set; }
        public int WeightMetric { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string PhoneHome { get; set; }
        public string EmailAddress { get; set; }
        public string Physicians { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string ServicesRequired { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string DME { get; set; }
        public string OtherDME { get; set; }
        public Guid UserId { get; set; }
        public Guid CreatedById { get; set; }
        public int Status { get; set; }
        public string VerificationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }
        public bool IsDNR { get; set; }
        public string PrimaryHealthPlanId { get; set; }
        public string SecondaryHealthPlanId { get; set; }
        public string TertiaryHealthPlanId { get; set; }

        public string PrimaryGroupName { get; set; }
        public string SecondaryGroupName { get; set; }
        public string TertiaryGroupName { get; set; }

        public string PrimaryGroupId { get; set; }
        public string SecondaryGroupId { get; set; }
        public string TertiaryGroupId { get; set; }

        public string PrimaryRelationship { get; set; }
        public string SecondaryRelationship { get; set; }
        public string TertiaryRelationship { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ReferralStatus)Enum.ToObject(typeof(ReferralStatus), this.Status));
            }
        }

        [SubSonicIgnore]
        public string ReferralPhysicianName { get; set; }
        [SubSonicIgnore]
        public string InternalReferralName { get; set; }
        [SubSonicIgnore]
        public string ClinicianName { get; set; }
        [SubSonicIgnore]
        public string ReferralSourceName { get; set; }
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [SubSonicIgnore]
        public string SecondaryInsuranceName { get; set; }
        [SubSonicIgnore]
        public string TertiaryInsuranceName { get; set; }
        [SubSonicIgnore]
        public string PrimaryRelationshipDescription { get; set; }
        [SubSonicIgnore]
        public string SecondaryRelationshipDescription { get; set; }
        [SubSonicIgnore]
        public string TertiaryRelationshipDescription { get; set; }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), StringFormatter.FormatZipCode(this.AddressZipCode.TrimWithNullable()));
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2}, {3} {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, StringFormatter.FormatZipCode(this.AddressZipCode.TrimWithNullable()));
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0}, {1}, {2} {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, StringFormatter.FormatZipCode(this.AddressZipCode.TrimWithNullable()));
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public List<string> PhoneHomeArray { get; set; }       
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }       
        [SubSonicIgnore]
        public List<string> DMECollection { get; set; }  
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        [SubSonicIgnore]
        public string DOBFormatted { get { return this.DOB.ToShortDateString(); } }
        [SubSonicIgnore]
        public List<Guid> AgencyPhysicians { get; set; }
        [SubSonicIgnore]
        public List<Physician> PhysicianContacts { get; set; }
        [SubSonicIgnore]
        public string CreatedBy { get; set; }
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        [SubSonicIgnore]
        public string NonAdmissionDateFormatted { get { return this.NonAdmissionDate.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public ReferralEmergencyContact EmergencyContact { get; set; }
        [SubSonicIgnore]
        public AgencyPhysician PrimaryPhysician { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Referral first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Referral last name is required."));
            AddValidationRule(new Validation(() => this.DOB == DateTime.MinValue, "Referral date of birth is required."));
            AddValidationRule(new Validation(() => !this.DOB.ToString().IsValidDate(), "Date of birth is not a valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Referral gender has to be selected."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Referral address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Referral city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode) || (this.AddressStateCode.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressStateCode.Trim())), "Referral state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Referral zip is required."));
            AddValidationRule(new Validation(() => this.ReferralDate == DateTime.MinValue, "Referral Date is required."));

            if (this.PhoneHome.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneHomeArray.Count == 0, "Referral phone is required."));
            }
        }

        #endregion
    }
}
