﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    public class PatientListData : Patient
    {
        public string ClinicianAssignedFirstName { get; set; }
        public string ClinicianAssignedLastName { get; set; }
        public string ClinicianAssignedMiddleName { get; set; }
        public string ClinicianAssignedName { get; set; }
        public string CaseManagerFirstName { get; set; }
        public string CaseManagerLastName { get; set; }
        public string CaseManagerMiddleName { get; set; }
        public string CaseManagerName { get; set; }
        public int DNR { get; set; }
        public string PrimaryInsuranceName { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string PhysicianMiddleName { get; set; }
        public string PhysicianNPI { get; set; }
        public string Payer { get; set; }
        public int PrimaryInsuranceNumber { get; set; }
        public int Ethnicity { get; set; }
    }
}
