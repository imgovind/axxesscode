﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PatientTeam
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid TeamId { get; set; }     
    }
}
