﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class ReturnComment
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid EventId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
