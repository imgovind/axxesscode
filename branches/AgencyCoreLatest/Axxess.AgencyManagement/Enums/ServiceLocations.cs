﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum ServiceLocations
    {
        [Description("Patient's Home/Residence")]
        Q5001,
        [Description("Assisted Living Facility")]
        Q5002,
        [Description("Place not otherwise specified")]
        Q5009
    }
}
