﻿namespace Axxess.AgencyManagement.Enums
{

    using System.ComponentModel;

    public enum BillType
    {
        [Description("321--Home Health - Admit thru Discharge")]
        HHPPSAdmitThruDischarge = 321,
        [Description("322-Home Health - PPS RAP")]
        HHPPSRAP = 322,
        [Description("323-Home Health - Interim-Continuing Claims")]
        HHICC = 323,
        [Description("327-Home Health - Adjustment For Insurance")]
        HHAdjustmentForInsurance = 327,
        [Description("329-Home Health - PPS Final")]
        HHPPSFinal = 329,
        [Description("331-Home Health - Admit thru Discharge (Not for PPS bills)")]
        HHAdmitThruDischarge = 331,
        [Description("332-Home Health - 1st Claim (Not for PPS bills)")]
        HHFirstClaim = 332,
        [Description("333-Home Health - Continuing Claim (Not for PPS bills)")]
        HHContinuingClaim = 333,
        [Description("334-Home Health - Last Claim (Not for PPS bills)")]
        HHLastClaim = 334,
        [Description("341-OPT - Admit thru Discharge")]
        OPTAdmitThruDischarge = 341,
        [Description("342 OPT - 1st Claim")]
        OPTFirstClaim = 342,
        [Description("343-OPT - Continuing Claim")]
        OPTContinuingClaim = 343,
        [Description("344-OPT - Last Claim")]
        OPTLastClaim = 344,
        [Description("811-Hospice - Admit thru Discharge")]
        HospiceAdmitThruDischarge = 811,
        [Description("812-Hospice - 1st Claim")]
        HospiceFirstClaim = 812,
        [Description("813-Hospice - Continuing Claim")]
        HospiceContinuingClaim = 813,
        [Description("814-Hospice - Last Claim")]
        HospiceLastClaim = 814

    }
}
