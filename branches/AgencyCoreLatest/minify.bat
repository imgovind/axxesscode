@ECHO OFF

GOTO %1

:all
ECHO Compressing All JavaScript Files...

:acore
ECHO Compressing AgencyCore Files...
set FOLDER_LIST=(Modules Message Visits Plugins\Custom System)
FOR %%i in %FOLDER_LIST% DO (
ECHO Folder: %%i
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\%%i\*.js') DO (
ECHO File: %%f
java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\%%i\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\%%i\min\%%f)
)
IF "%1"=="acore" GOTO end

:support
ECHO Compressing Axxess Support Files...
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.SupportSite\Scripts\Modules\*.js') DO (
echo %%f 
java -jar compiler.jar --js Axxess.AgencyManagement.SupportSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.SupportSite\Scripts\Modules\min\%%f
)
REM FOR /f %%f IN ('dir /b .\AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\min\%%f
IF "%1"=="support" GOTO end

:md
ECHO Compressing Axxess MD Files...
FOR /f %%f IN ('dir /b .\Axxess.Physician.WebSite\Scripts\Modules\*.js') DO (
echo %%f 
java -jar compiler.jar --js Axxess.Physician.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.Physician.WebSite\Scripts\Modules\min\%%f
)
FOR /f %%f IN ('dir /b .\Axxess.Physician.WebSite\Scripts\Plugins\Custom\*.js') DO (
echo %%f 
java -jar compiler.jar --js Axxess.Physician.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.Physician.WebSite\Scripts\Plugins\Custom\min\%%f
)

:end
del ~compile.tmp
ECHO Complete
pause