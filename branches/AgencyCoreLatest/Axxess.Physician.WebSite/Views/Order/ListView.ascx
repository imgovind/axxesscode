﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Pending Orders</span>
<%= Html.Telerik().Grid<Order>().Name("orderGrid").HtmlAttributes(new { style = "top:110px;" }).Columns(columns =>
{
            columns.Bound(o => o.AgencyName);
            columns.Bound(o => o.PatientName);
            columns.Bound(o => o.TypeDescription).ClientTemplate("<#=PrintUrl#>").Title("Order Type");
            columns.Bound(o => o.OrderDate).Width(100);
            columns.Bound(o => o.StatusName).Title("Status");
        })
        .Groupable(settings => settings.Groups(groups => {
            var data = ViewData["GroupName"].ToString();
            if (data == "PatientName")
            {
                groups.Add(o => o.PatientName);
            }
            else if (data == "TypeDescription")
            {
                groups.Add(o => o.TypeDescription);
            }
            else if (data == "OrderDate")
            {
                groups.Add(o => o.OrderDate);
            }
            else
            {
                groups.Add(o => o.OrderDate);
            }
        }))
            .DataBinding(dataBinding => dataBinding.Ajax().Select("ListGrid", "Order", new { agencyId = Current.DefaultAgencyId, startDate = DateTime.Now.AddDays(-60).ToShortDateString(), endDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
%>
