﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Care Plan Oversight list</span>
<%= Html.Telerik().Grid<CarePlanOversight>().Name("List_CPO").HtmlAttributes(new { style = "top:110px;" })
    .ClientEvents(evnt => evnt.OnRowDataBound("CarePlanOversight.AddCheckListOnRowDataBound()"))
    .Columns(columns => {
        columns.Bound(c => c.Id).Template(t=>t.Checkbox).ClientTemplate("<#=Checkbox#>").Title("").Width(50).Sortable(false);
        columns.Bound(c => c.AgencyName).Title("Agency").Sortable(false).Width(200);
        columns.Bound(c => c.PatientName).Title("Patient").Sortable(false).Width(130);
        columns.Bound(c => c.CptCode).Title("CPT Code").Sortable(false).Width(70);
        columns.Bound(c => c.CpoActivity).Title("Activity").Sortable(false);
        columns.Bound(c => c.Duration).Title("Duration").Sortable(false).Width(75);
        columns.Bound(c => c.LogDateFormatted).Title("Entry Date").Sortable(false).Width(100);
        columns.Bound(c => c.StatusName).Title("Status").Sortable(false).Width(100);
        columns.Bound(c => c.Id).Template(t => "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditCPO('" + t.Id + "'); \">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"CarePlanOversight.Delete('" + t.Id + "');\">Delete</a>")
            .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditCPO('<#=Id#>'); \">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"CarePlanOversight.Delete('<#=Id#>');\">Delete</a>")
            .Title("Action").Sortable(false).Width(110);
    })
    .Groupable(settings => settings.Groups(groups =>
    {
         var data = ViewData["GroupName"].ToString();
         if (data == "AgencyName")
         {
             groups.Add(o => o.AgencyName);
         }
         else if (data == "PatientName")
         {
             groups.Add(o => o.PatientName);
         }
         else if (data == "CptCode")
         {
             groups.Add(o => o.CptCode);
         }
         else if (data == "StatusName")
         {
             groups.Add(o => o.StatusName);
         }
         else
         {
             groups.Add(o => o.LogDateFormatted);
         }
    }))
            .DataBinding(dataBinding => dataBinding.Ajax().Select("CPOGrid", "Order", new { agencyId = Current.DefaultAgencyId, startDate = DateTime.Now.AddDays(-60).ToShortDateString(), endDate = DateTime.Now.ToShortDateString() }))
    .Scrollable().Sortable()
    %>
    <%= Html.Hidden("BillList", "", new { @id = "CPO_BillList" })%>
<script type="text/javascript">
    $("#List_CPO div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "/Order/CPOContent?SortParams=" + U.ParameterByName(link, 'List_CPO-orderBy'));
    });
</script>    