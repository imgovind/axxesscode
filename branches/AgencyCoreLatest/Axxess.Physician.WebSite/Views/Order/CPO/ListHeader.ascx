﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<fieldset class="ac orders-filter">
    <div class="buttons float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="CarePlanOversight.GetCPOList();">Generate</a></li>
        </ul>
    </div>
    <div>
        <label class="float-left">Agency:</label>
        <%=Html.LookupSelectList("agencies", "CPO_AgencyId", Current.DefaultAgencyId.ToString(), new { @id = "CPO_AgencyId", @class = "" })%>
    </div>
    <div class="clear" />
    <label class="float-left">Date Range:</label>
    <input type="text" name="StartDate" class="date-picker shortdate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="CPO_StartDate" />
    <input type="text" name="EndDate" class="date-picker shortdate" value="<%= DateTime.Now.ToShortDateString() %>" id="CPO_EndDate" />
    <div class="buttons">
        <ul>
           <li><a class="groupLink" onclick="CarePlanOversight.Load('PatientName');">Group By Patient</a></li>
           <li><a class="groupLink" onclick="CarePlanOversight.RebindList();">Refresh</a></li>
           <li><a class="groupLink" onclick="CarePlanOversight.SubmitCpoBill($('#CPO_BillList').val());">Generate Claim</a></li>
        </ul>
    </div>
</fieldset>

<div id="cpoListContent"><% Html.RenderPartial("~/Views/Order/CPO/ListView.ascx"); %></div>   