﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CarePlanOversight>" %>
<span class="wintitle">Edit Care Plan Oversight</span>

<% using (Html.BeginForm("UpdateCPO", "Order", FormMethod.Post, new { @id = "newCPO" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "CPO_Id" })%>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="CPO_Agency" class="float-left width200">Agency:</label>
                <div class="float-right"><%=Html.LookupSelectList("agency", "AgencyId", "", new { @id = "CPO_AgencyId", @class = "requireddropdown" })%></div>
            </div>
            <div class="clear" />
            <div class="row">
                <label for="CPO_Patient" class="float-left width200">Patient:</label>
                <div class="float-right"><%=Html.AgencyPhysicianPatient("PatientId", Model.PatientId.ToString(), Model.AgencyId, new { @id = "CPO_PatientId", @class = "requireddropdown" })%></div>
            </div>
            <div class="clear" />
            <div class="row">
                <label for="CPO_LogDate" class="float-left">Date:</label>
                <div class="float-right"><input type="date" class="data-picker" name="LogDate" value="<%=Model.LogDate.ToShortDateString() %>" id="New_CPO_LogDate" /></div>
            </div>
            <div class="clear" />
        </div>
        <div class="column">
            <div class="row">
                <label for="CPO_Activity" class="float-left width200">CPT Code:</label>
                <div class="float-right"><%=Html.LookupSelectList("CPTCode", "CptCode", "", new { @id = "CPO_CptCode", @class = "requireddropdown" })%></div>
            </div>
            <div class="clear" />
            <div class="row">
                <label for="CPO_Activity" class="float-left width200">CPO Activity:</label>
                <div class="float-right"><%=Html.LookupSelectList("CPOActivity", "Activity", Model.PatientId.ToString(), new { @id = "CPO_Activity", @class = "requireddropdown" })%></div>
            </div>
            <div class="clear" />
            <div class="row">
                <label for="CPO_Duration" class="float-left width200">Duration <em>(in minutes)</em>:</label>
                <div class="float-right"><%=Html.TextBox("Duration", Model.Duration, new { @id = "CPO_Duration", @class = "numeric required", @style = "width: 100px;", @maxlength = "4" })%></div>
            </div>
            <div class="clear" />
        </div>
        <div class="wide_column">
            <div class="row">
                <label for="CPO_Comments" class="float-left">Comment:</label>
            </div>
            <div class="row">
                <%=Html.TextArea("Comments", Model.Comments, 8, 30, new { @id = "CPO_Comments", @class = "fill", @maxcharacters = "5000", @style="height: 160px" })%>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="button" value="" id="CarePlanOversight_Button" />
    <%= Html.Hidden("Status", "", new { @id = "CPO_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#CPO_Status').val('415');Remove();CarePlanOversight.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#CPO_Status').val('425');Add();CarePlanOversight.Submit($(this));">Complete</a></li>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $(".numeric").numeric();
    $('#CPO_AgencyId').change(function() { CarePlanOversight.LoadPatientsDropDown('CPO_PatientId', $(this)); });
    function Remove() {
        $("#New_CPO_Activity").removeClass('requireddropdown');
    }
    function Add() {
        $("#New_CPO_Activity").removeClass('requireddropdown').addClass('requireddropdown');
    }
</script>