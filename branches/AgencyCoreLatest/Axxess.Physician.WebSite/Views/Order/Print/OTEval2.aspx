﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<%  string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
<%  string[] genericSensoryAcuity = data.AnswerArray("GenericSensoryAcuity"); %>
<%  string[] genericSensoryTracking = data.AnswerArray("GenericSensoryTracking"); %>
<%  string[] genericSensoryVisual = data.AnswerArray("GenericSensoryVisual"); %>
<%  string[] genericMotorComponents = data.AnswerArray("GenericMotorComponents"); %>
<%  var noteDiscipline = Model.Type; %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<%  string[] genericOTGoals = data.AnswerArray("GenericOTGoals"); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.8.3.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E" +
        "<%= Model.TypeName %>" + 
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("EpsPeriod") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn")%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PhysicianDisplayName.Clean() %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EOccupational Therapy <%= Model.Type == "OTDischarge" ? "Discharge" : (Model.Type == "OTReEvaluation" ? "Re-" : "") + "Evaluation" %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
        
        printview.addsection(
        printview.col(7,
            printview.span("SBP",true) +
            printview.span("DBP",true) +
            printview.span("HR",true) +
            printview.span("Resp",true) +
            printview.span("Temp",true) +
            printview.span("Weight",true) +
            printview.span("O2 Sat",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressure").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressurePer").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericPulse").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericResp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTemp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWeight").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericO2Sat").Clean() %>")),
        "Vital Signs");
        
        printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("MedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("TherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
        
        printview.addsection(
        printview.span("Prior Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorFunctionalStatus").Clean() %>",0,1) +
        printview.span("Pertinent Medical History",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalHistory").Clean() %>",0,1),
        "PLOF and Medical History");
        
        printview.addsection(
        printview.col(5,
        printview.span("Dwelling Level:",true) +
        printview.span("&#160;") +
        printview.checkbox("One",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Multiple",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0").ToString().ToLower() %>) +
        printview.span("&#160;") +
        printview.span("Stairs",true) +
        printview.span("&#160;") +
        printview.span("#Steps<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairsNumber").Clean() %>",0,1) +
        printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0").ToString().ToLower() %>) +
        printview.span("Lives with:",true) +
        printview.checkbox("Alone",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2").ToString().ToLower() %>) +
        printview.checkbox("Family",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Friends",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0").ToString().ToLower() %>) +
        printview.checkbox("Significant Other",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("3").ToString().ToLower() %>)) +
        printview.span("Support:",true) +
        printview.col(3,
        printview.checkbox("Willing caregiver available",<%= genericLivingSituationSupport.Contains("1").ToString().ToLower() %>) +
        printview.checkbox("Limited caregiver support",<%= genericLivingSituationSupport.Contains("2").ToString().ToLower() %>) +
        printview.checkbox("No caregiver available",<%= genericLivingSituationSupport.Contains("3").ToString().ToLower() %>)) +
        printview.col(2,
        printview.span("Home Safety Barriers:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericHomeSafetyBarriers").Clean() %>",0,1)),
        "Living Situation");
        
        printview.addsection(
        printview.col(2,
            printview.checkbox("Requires considerable and taxing effort.",<%= genericHomeBoundStatus.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Medical restriction.",<%= genericHomeBoundStatus.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with transfer.",<%= genericHomeBoundStatus.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with ambulation.",<%= genericHomeBoundStatus.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Needs assist leaving the home.",<%= genericHomeBoundStatus.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Unable to be up for long period.",<%= genericHomeBoundStatus.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Severe SOB upon exertion.",<%= genericHomeBoundStatus.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Unsafe to go out of home alone.",<%= genericHomeBoundStatus.Contains("6").ToString().ToLower() %>)),
        "Homebound Reason");
        
        printview.addsection(
        printview.col(3,
            printview.span("I.FUNCTIONAL MOBILITY") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed mobility",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Bed/WC transfers",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/shower transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLFunctionalComment").Clean()%>") +
        printview.col(3,
            printview.span("II.SELFCARE/ADL SKILLS") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Self Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Oral Hygiene",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGroomingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("UB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("LB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("UB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("LB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLSelfcareComment").Clean()%>") +
        printview.col(3,
            printview.span("III.INSTRUMENTAL ADL") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Housekeeping",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Meal prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Laundry",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Telephone use",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Money management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Medication management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLInstrumentalComment").Clean()%>") +
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),            
            "ADLs / Functional Mobility Level / Level of Assist");
            
       printview.addsection(
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentComment").Clean() %>",0,1) +
        printview.col(3,
            printview.span("") +
            printview.span("%3Cspan class=%22align-center fill%22%3EROM%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3EManual Muscle Test%3C/span%3E",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthLeft").Clean() %>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthLeft").Clean() %>",0,1) +
            
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Lat Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Long Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthLeft").Clean() %>",0,1)),
        "Physical Assessment");
        
        printview.addsection(
        printview.col(4,
            printview.span("Pain Location",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentLocation").Clean() %>",0,1) +
            printview.span("Pain Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainLevel").Clean() %>",0,1) +
            printview.span("Increased by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy").Clean() %>",0,1) +
            printview.span("Relieved by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy").Clean() %>",0,1)),
        "Pain Assessment");
        
        printview.addsection(
        printview.col(7,
            printview.span("")+
            printview.span("Sharp/Dull",true)+
            printview.span("")+
            printview.span("Light/Firm Touch",true)+
            printview.span("")+
            printview.span("Proprioception",true)+
            printview.span("")+
            printview.span("Area",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea1").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea2").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea3").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea4").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1)),
            "Sensory/Perceptual Skills");
            
    printview.addsection(
        printview.span("Visual Skills:Acuity",true)+
        printview.col(4,
            printview.checkbox("Intact",<%= genericSensoryAcuity.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Impaired",<%= genericSensoryAcuity.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Double",<%= genericSensoryAcuity.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Blurred",<%= genericSensoryAcuity.Contains("4").ToString().ToLower() %>)) +
        printview.span("Tracking:",true)+
        printview.col(5,
            printview.checkbox("Unilaterally",<%= genericSensoryTracking.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bilagterally",<%= genericSensoryTracking.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Smooth",<%= genericSensoryTracking.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Jumpy",<%= genericSensoryTracking.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Not Tracking",<%= genericSensoryTracking.Contains("5").ToString().ToLower() %>)) +
        printview.span("Visual Field Cut or Neglect Suspected:",true)+
        printview.col(2,
            printview.checkbox("Right",<%= genericSensoryVisual.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Left",<%= genericSensoryVisual.Contains("2").ToString().ToLower() %>)) +
        printview.span("Impacting Function?",true)+
        printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No(Specify):<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunctionSpecify").Clean() %>",<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("0").ToString().ToLower() %>)) +
        printview.span("Referral Needed?",true)+
        printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No(Who contacted?):<%= data.AnswerOrEmptyString("GenericSensoryReferralNeededContact").Clean() %>",<%= data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("0").ToString().ToLower() %>)),        
        "");
        
   printview.addsection(
        printview.col(4,
            printview.span("Memory:Short term",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionShortTerm").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Sequencing",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSequencing").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Memory:Long term",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionLongTerm").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Problem Solving",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionProblemSolving").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Attention/Concentration",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionConcentration").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Coping Skills",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionCopingSkills").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Auditory Comprehension",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionAuditory").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Able to Express Needs",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionExpressNeeds").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Visual Comprehension",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionVisual").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Safety/Judgment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSafety").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Self-Control",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSelfControl").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Initiation of Activity",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionInitiation").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1))+
            printview.span("Comments",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionComment").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1),
            "Cognitive Status/Comprehension");
            
      printview.addsection(
        printview.col(3,
            printview.span("Fine Motor Coordination",true)+
            printview.span("Right <%= data.AnswerOrEmptyString("GenericFineMotorR").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFineMotorSensoryR").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("")+
            printview.span("Left <%= data.AnswerOrEmptyString("GenericFineMotorL").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFineMotorSensoryL").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
            printview.span("Gross Motor Coordination",true)+
            printview.span("Right <%= data.AnswerOrEmptyString("GenericGrossMotorR").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGrossMotorSensoryR").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("")+
            printview.span("Left <%= data.AnswerOrEmptyString("GenericGrossMotorL").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGrossMotorSensoryL").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1)) +
          printview.col(4,
            printview.checkbox("Right handed",<%= genericMotorComponents.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Left handed",<%= genericMotorComponents.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Orthosis",<%= genericMotorComponents.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Used",<%= genericMotorComponents.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Needed(specify)",<%= genericMotorComponents.Contains("5").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMotorComponentsSpecify").Clean() %>",0,1) +
            printview.span("")+
            printview.span(""))+
            printview.span("Comments",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMotorComments").Clean() %>",0,1),
            "Motor Components(Enter Appropriate Response)");
</script>




<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Comments",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericAssessmentMore").Clean() %>",0,2),
        "Assessment");
        
    printview.addsection(
        printview.col(2,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Therapeutic activities (reaching, bending, etc)",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Sensory integrative techniques",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Wheelchair management training",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Teach work simplification",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Community/work integration",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Self care management training",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Cognitive skills development/training",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach task segmentation",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Manual therapy techniques",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.col(2,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19Duration").Clean() %>",0,1))+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.col(3,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Dosage: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20Dosage").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20Duration").Clean() %>",0,1)))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTreatmentPlanOther").Clean() %>",0,1),
            "Treatment Plan");   
            
      printview.addsection(
        <%if(genericOTGoals.Contains("1")){ %>
        printview.checkbox("Patient will be able to perform upper body dressing with <%= data.AnswerOrEmptyString("GenericOTGoals1With").Clean() %> assist  utilizing <%= data.AnswerOrEmptyString("GenericOTGoals1With").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals1Weeks").Clean() %> weeks",<%= genericOTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("2")){ %>
        printview.checkbox("Patient will be able to perform lower body dressing with <%= data.AnswerOrEmptyString("GenericOTGoals2With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals2Utilize").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals2Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform toilet hygiene with <%= data.AnswerOrEmptyString("GenericOTGoals3Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals3Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("4")){ %>
        printview.checkbox("Patient will be able to perform grooming with <%= data.AnswerOrEmptyString("GenericOTGoals4Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals4Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("5")){ %>
        printview.checkbox("Patient will be able to perform <%= data.AnswerOrEmptyString("GenericOTGoals5With").Clean() %> (task)  with <%= data.AnswerOrEmptyString("GenericOTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("GenericOTGoals5Within").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals5Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("6")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("GenericOTGoals6Of").Clean() %> to <%= data.AnswerOrEmptyString("GenericOTGoals6To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("GenericOTGoals6Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals6Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("7")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("GenericOTGoals7Skill").Clean() %> to <%= data.AnswerOrEmptyString("GenericOTGoals7Assist").Clean() %> grade to improve <%= data.AnswerOrEmptyString("GenericOTGoals7Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("GenericOTGoals7Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("8")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericOTGoals8To").Clean() %> to improve postural control and balance necessary to perform <%= data.AnswerOrEmptyString("GenericOTGoals8Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("GenericOTGoals8Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("9")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericOTGoals9Within").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("GenericOTGoals9Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("10")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("GenericOTGoals10Device").Clean() %> joint to <%= data.AnswerOrEmptyString("GenericOTGoals10Feet").Clean() %> degree of <%= data.AnswerOrEmptyString("GenericOTGoals10Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals10Weeks").Clean() %> weeks to improve <%= data.AnswerOrEmptyString("GenericOTGoals10DegreeOf").Clean() %> (task).",<%= genericOTGoals.Contains("10").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("11")){ %>
        printview.checkbox("Patient/caregiver will be able to perform HEP safely and effectively within <%= data.AnswerOrEmptyString("GenericOTGoals11Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("12")){ %>
        printview.checkbox("Patient will learn <%= data.AnswerOrEmptyString("GenericOTGoals12Device").Clean() %> techniques to <%= data.AnswerOrEmptyString("GenericOTGoals12Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals12Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("13")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("GenericOTGoals13Of").Clean() %> standardized test score to <%= data.AnswerOrEmptyString("GenericOTGoals13To").Clean() %> to improve <%= data.AnswerOrEmptyString("GenericOTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals13Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("14")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of brace/splint within <%= data.AnswerOrEmptyString("GenericOTGoals14Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("15")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of wheelchair with <%= data.AnswerOrEmptyString("GenericOTGoals15To").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals15Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("16")){ %>
        printview.checkbox("Patient will be able to perform upper body bathing with <%= data.AnswerOrEmptyString("GenericOTGoals16With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals16To").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals16Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("17")){ %>
        printview.checkbox("Patient will be able to perform lower body bathing with <%= data.AnswerOrEmptyString("GenericOTGoals17Joint").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals17Degree").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals17Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericOTGoalsComments").Clean() %>",0,2)+
        printview.span("Rehab Potential:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericRehabPotential").Clean() %>",0,2),
        "OT Goals"); 
        
       printview.addsection(
        printview.span(""),
        "Standardized test");
    printview.addsubsection(
        printview.col(2,
            printview.span("Katz Index:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericKatzIndex").Clean() %>",0,1)+
            printview.span("9 Hole Peg Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("Generic9HolePeg").Clean() %>",0,1)+
            printview.span("Lawton & Brody IADL Scale:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericLawtonBrody").Clean() %>",0,1)+
            printview.span("Mini-Mental State Exam:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMiniMentalState").Clean() %>",0,1)+
            printview.span("Other:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericOtherTest").Clean() %>:<%= data.AnswerOrEmptyString("GenericStandardizedTestOther").Clean() %>",0,1)),
            "Prior",2);
    printview.addsubsection(
        printview.col(2,
            printview.span("Katz Index:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericKatzIndex1").Clean() %>",0,1)+
            printview.span("9 Hole Peg Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("Generic9HolePeg1").Clean() %>",0,1)+
            printview.span("Lawton & Brody IADL Scale:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericLawtonBrody1").Clean() %>",0,1)+
            printview.span("Mini-Mental State Exam:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMiniMentalState1").Clean() %>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericOtherTest1").Clean() %>:<%= data.AnswerOrEmptyString("GenericStandardizedTestOther1").Clean()%>",0,1)),
            "Current");       
</script>
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericCareCoordination").Clean() %>",0,4),
        "Care Coordination");
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericSkilledTreatmentProvided").Clean() %>",0,4),
        "Skilled Treatment Provided This Visit");    
</script>

<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>
