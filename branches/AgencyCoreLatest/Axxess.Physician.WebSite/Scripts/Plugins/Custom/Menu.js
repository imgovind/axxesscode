(function($) {
    $.extend($.fn, {
        Menu: function(commands) {
            return this.each(function() {
                if (commands == "destroy") {
                    $(this).removeClass("sf-menu");
                    $("li", this).unbind();
                    $("li ul", this).removeAttr("style");
                } else $(this).superfish({
                    onBeforeShow: function() {
                        if ($(this).parent().length && $(this).parent().offset().left + $(this).parent().width() + $(this).width() > $(document).width()) $(this).css({ right: $(this).css("left"), left: "auto" });
                        if ($(this).parent().length && $(this).parent().offset().top + $(this).height() > $(document).height()) $(this).css({ top: $(document).height() - ($(this).parent().offset().top + $(this).height() + 10) });
                    },
                    onHide: function() {
                        $(this).attr("style", "").hide();
                    }
                })
            })
        }
    })
})(jQuery);