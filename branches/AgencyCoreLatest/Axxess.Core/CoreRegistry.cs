﻿namespace Axxess.Core
{
    using System;

    using StructureMap;
    using StructureMap.Pipeline;
    using StructureMap.Configuration.DSL;

    using Axxess.Core.Infrastructure;

    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            For<IWebConfiguration>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<WebConfiguration>();
            For<ICache>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<MembaseCache>().Named("MembaseCache");
            For<ICache>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<HttpRuntimeCache>().Named("HttpRuntimeCache");
            For<INotification>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<EmailNotification>();
            For<ISessionStore>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<HttpContextSession>();
            For<ICryptoProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<RijndaelProvider>();
            For<IDatabaseAdministration>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<MySqlAdministration>();
            For<ICouchbaseClientFactory>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<CouchbaseClientFactory>();
        }
    }
}
