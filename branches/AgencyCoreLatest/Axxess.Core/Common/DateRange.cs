﻿namespace Axxess.Core
{
    using System;

    public class DateRange
    {
        public string Id { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsLinkedToAssessment { get; set; }
        public string StartDateFormatted { get { return this.StartDate.ToString("MM/dd/yyyy"); } }
        public string EndDateFormatted { get { return this.EndDate.ToString("MM/dd/yyyy"); } }
        public int DateRangeDays
        {
            get
            {
                return (this.EndDate - this.StartDate).Days;
            }
        }

        public override string ToString()
        {
            return StartDateFormatted + " - " + EndDateFormatted;
        }
    }
}
