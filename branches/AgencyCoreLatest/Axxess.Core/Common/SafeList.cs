﻿namespace Axxess.Core
{
    using System;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    public class SafeList<T>
    {
        private readonly List<T> baseList = null;
        private readonly ReaderWriterLockSlim readWriteLockSlim = new ReaderWriterLockSlim();

        public SafeList()
        {
            this.baseList = new List<T>();
        }

        public T this[int index]
        {
            get
            {
                using (readWriteLockSlim.AcquireReadLock())
                {
                    return this.baseList[index];
                }
            }
            set
            {
                using (readWriteLockSlim.AcquireWriteLock())
                {
                    this.baseList[index] = value;
                }
            }
        }

        public void AddRange(IEnumerable<T> items)
        {
            this.baseList.AddRange(items);
        }

        public void Add(T item)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseList.Add(item);
            }
        }

        public T Single(Predicate<T> match)
        {
            using (readWriteLockSlim.AcquireReadLock())
            {
                return this.baseList.Find(match);
            }
        }

        public List<T> Find(Predicate<T> match)
        {
            using (readWriteLockSlim.AcquireReadLock())
            {
                return this.baseList.FindAll(match);
            }
        }
        
        public int Count
        {
            get
            {
                using (readWriteLockSlim.AcquireReadLock())
                {
                    return this.baseList.Count;
                }
            }
        }

        public void Remove(T item)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseList.Remove(item);
            }
        }

        public void ForEach(Action<T> action)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseList.ForEach(action);
            }
        }
    }
}
