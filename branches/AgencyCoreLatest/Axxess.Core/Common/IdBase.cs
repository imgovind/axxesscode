﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core
{
    [Serializable]
    public abstract class IdBase : EntityBase
    {
        public Guid Id { get; set; }
    }
}
