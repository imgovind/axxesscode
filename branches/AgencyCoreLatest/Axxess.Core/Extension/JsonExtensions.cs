﻿namespace Axxess.Core.Extension
{
    using System;
    using System.IO;
    using System.Text;
    using System.Diagnostics;
    using System.Runtime.Serialization.Json;

    using Axxess.Core.Infrastructure;

    public static class JsonExtensions
    {
        [DebuggerStepThrough]
        public static T FromJson<T>(this string json)
        {
            Check.Argument.IsNotEmpty(json, "json");

            return JsonSerializer.Deserialize<T>(json);
        }

        [DebuggerStepThrough]
        public static string ToJson<T>(this T instance)
        {
            Check.Argument.IsNotNull(instance, "instance");

            return JsonSerializer.Serialize(instance);
        }
    }
}
