﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Diagnostics;

    using Extension;

    using MySql.Data.MySqlClient;
    using System.Data;
    using System.Configuration;

    public partial class FluentCommand<T> : Disposable where T : new()
    {
        public delegate T ResultMapDelegate(DataReader reader);

        private readonly string commandText;

        protected ResultMapDelegate mapper;
        protected readonly MySqlCommand command;
        protected string dictionaryId;

        public FluentCommand(string commandText)
        {
            command = CreateCommand();
            command.CommandText = commandText;
            this.commandText = commandText;
        }

        public FluentCommand(string commandText, MySqlConnection connection)
        {
            command = CreateCommand();
            command.Connection = connection;
            command.CommandText = commandText;
            this.commandText = commandText;
        }

        protected MySqlCommand CreateCommand()
        {
            return new MySqlCommand();
        }

        protected MySqlConnection CreateConnection(string connectionStringName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public FluentCommand<T> SetConnection(string connectionStringName)
        {
            command.Connection = CreateConnection(connectionStringName);
            return this;
        }

        public FluentCommand<T> UseConnectionString(string connectionString, int commandTimeout)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            command.Connection = connection;
            command.CommandTimeout = commandTimeout;
            return this;
        }

        public FluentCommand<T> SetMap(ResultMapDelegate resultMapDelegate)
        {
            mapper = resultMapDelegate;
            return this;
        }

        public void SetTransaction(MySqlTransaction transaction)
        {
            command.Transaction = transaction;
            return;
        }

        public DataTable AsTable()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            DataTable table = new DataTable();
            using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
            {
                table = new DataTable();
                dataAdapter.Fill(table);
            }
            return table;
        }

        public List<T> AsList()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            var returnList = new List<T>();
            using (command)
            {
                using (MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var myReader = new DataReader(reader);
                    while (reader.Read())
                    {
                        T data = new T();
                        if (mapper != null)
                        {
                            data = mapper.Invoke(myReader);
                        }
                        else
                        {
                            reader.Load(data);
                        }

                        returnList.Add(data);
                    }
                }
                command.Connection.Close();
            }
            return returnList;
        }

        public Dictionary<string, T> AsDictionary()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            var returnList = new Dictionary<string, T>();
            if (dictionaryId.IsNotNullOrEmpty())
            {
                using (command)
                {
                    using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var myReader = new DataReader(reader);
                        while (reader.Read())
                        {
                            T data = new T();
                            if (mapper != null)
                            {
                                data = mapper.Invoke(myReader);
                                var key = myReader.GetStringNullable(dictionaryId);
                                if (key.IsNotNullOrEmpty() && !returnList.ContainsKey(key))
                                {
                                    returnList.Add(key, data);
                                }
                            }
                            else
                            {
                                reader.Load(data);
                            }

                        }
                    }
                    command.Connection.Close();
                }
            }
            return returnList;
        }

        public IEnumerable<T> AsEnumerable()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var myReader = new DataReader(reader);
                    while (reader.Read())
                    {
                        T data = new T();
                        if (mapper != null)
                        {
                            data = mapper.Invoke(myReader);
                        }
                        else
                        {
                            reader.Load(data);
                        }
                        yield return data;
                    }
                }
                command.Connection.Close();
            }
        }

        public IList<T> AsList(ResultMapDelegate map)
        {
            mapper = map;
            return AsList();
        }

        public IEnumerable<T> AsEnumerable(ResultMapDelegate map)
        {
            mapper = map;
            return AsEnumerable();
        }

        public DbDataReader AsDataReader()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    return reader;
                }
            }
        }

        public int AsScalar()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        public T AsSingle()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var myReader = new DataReader(reader);
                    if (reader.Read())
                    {
                        if (mapper != null)
                        {
                            return mapper.Invoke(myReader);
                        }
                        else
                        {
                            T item = default(T);
                            if (!typeof(T).IsValueType)
                            {
                                item = new T();
                            }
                            reader.Load(item);
                            return item;
                        }
                    }
                }
                command.Connection.Close();
            }
            return default(T);
        }

        public int AsNonQuery()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                return command.ExecuteNonQuery();
            }
        }

        public T AsScalar(params object[] list)
        {
            ResetParams(list);
            return (T)command.ExecuteScalar();
        }

        public T AsSingle(params object[] list)
        {
            ResetParams(list);
            Debug.WriteLine("Executing SQL: " + commandText);

            using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
            {
                var myReader = new DataReader(reader);
                if (reader.Read())
                {
                    if (mapper != null)
                    {
                        return mapper.Invoke(myReader);
                    }
                    else
                    {
                        T item = new T();
                        reader.Load(item);
                        return item;
                    }
                }
            }
            return default(T);
        }

        public int AsNonQuery(params object[] list)
        {
            ResetParams(list);
            return command.ExecuteNonQuery();
        }

        private void ResetParams(params object[] list)
        {
            Debug.Assert(list.Length == command.Parameters.Count);
            for (int i = 0; i < list.Length; i++)
            {
                command.Parameters[i].Value = SetParameterValue(list[i]);
            }
        }

        protected object SetParameterValue(object value)
        {
            return value ?? DBNull.Value;
        }

        protected object SetGuidParamValue(object value)
        {
            if (value == null)
                return DBNull.Value;
            return value.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && command != null)
            {
                if (command.Connection != null && command.Connection.State != ConnectionState.Closed)
                {
                    command.Connection.Close();
                }
                command.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
