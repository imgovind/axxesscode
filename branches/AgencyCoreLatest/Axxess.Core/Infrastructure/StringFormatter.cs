﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;

namespace Axxess.Core.Infrastructure
{
    public static class StringFormatter
    {
        public static string FormatZipCode(string zipCode)
        {
            if (zipCode.IsNotNullOrEmpty())
            {
                if (zipCode.Length == 9)
                {
                    return zipCode.Substring(0, 5) + "-" + zipCode.Substring(4, 4);
                }
                return zipCode;
            }
            return string.Empty;
        }
    }
}
