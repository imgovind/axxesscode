﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.DataProviders;
    using SubSonic.Extensions;
    using SubSonic.Repository;
    using SubSonic.SqlGeneration.Schema;

    public class EntityHelper
    {
        public static bool ToggleUserEntityDeprecation<T>(Guid id, Guid userId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND UserId = @userid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated?1:0)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ToggleReferralEntityDeprecation<T>(Guid id, Guid referralId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND ReferralId = @referralid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("referralid", referralId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated ? 1 : 0)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool TogglePatientEntityDeprecation<T>(Guid id, Guid patientId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated ? 1 : 0)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool TogglePatientEntitiesDeprecation<T>(Guid patientId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid ";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddInt("deprecated", isDeprecated ? 1 : 0)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ToggleEntityDeprecation<T,TV>(TV id, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .Add("id", id)
                        .AddInt("deprecated", isDeprecated ? 1 : 0)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ToggleEntityDeprecationWithoutModificationTime<T, TV>(TV id, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated WHERE AgencyId = @agencyid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .Add("id", id)
                        .AddInt("deprecated", isDeprecated ? 1 : 0)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemovePatientEntity<T, TV>(Guid agencyId, Guid patientId, TV id, string connectionString)
        {
            var name = GetTableName<T>();
            var script = "DELETE FROM " + name + " WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
            try
            {
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .Add("id", id).AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemovePatientEntities<T>(Guid agencyId, Guid patientId, string connectionString)
        {
            var name = GetTableName<T>();
            var script = "DELETE FROM " + name + " WHERE AgencyId = @agencyid AND PatientId = @patientid ";
            try
            {
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId).AsNonQuery() > 0;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool RemoveEntity<T, TV>(Guid agencyId, TV id, string connectionString)
        {
            var name = GetTableName<T>();
            var script = "DELETE FROM " + name + " WHERE AgencyId = @agencyid AND Id = @id";
            try
            {
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                    .AddGuid("agencyid", agencyId)
                    .Add("id", id).AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool RemoveMultiplePatientEntities<T>(Guid agencyId, Guid patientId, List<Guid> eventIds, string connectionString)
        {
            var name = GetTableName<T>();
            var script = string.Format("DELETE FROM " + name + " WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id IN ( {0} ) ", eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
            try
            {
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool UpdateEntityStatus<T>(Guid agencyId, Guid id, int status, string connectionString)
        {
            var name = GetTableName<T>();
            string script = @"UPDATE " + name + " SET Status = @status, Modified = @modified WHERE AgencyId = @agencyid AND Id = @id";
            using (var cmd = new FluentCommand<int>(script))
            {
                return cmd.SetConnection(connectionString)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", id)
                    .AddInt("status", status)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery() > 0;
            }
        }

        /// <summary>
        /// Selects data that does not have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="id">Entity's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectEntityJsonByColumns<T>(Guid agencyId, Guid id, string connectionStringName, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>()
                .Where("AgencyId").IsEqualTo(agencyId).And("Id").IsEqualTo(id);
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Selects data that does have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="patientId">Patient's Id</param>
        /// <param name="id">Entity's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectPatientEntityJsonByColumns<T>(Guid agencyId, Guid patientId, Guid id, string connectionStringName, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>()
                .Where("AgencyId").IsEqualTo(agencyId).And("PatientId").IsEqualTo(patientId).And("Id").IsEqualTo(id);
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Selects data that does have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectWhereByColumns<T>(Guid agencyId, string connectionStringName, Dictionary<string, string> where, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>().Where("AgencyId").IsEqualTo(agencyId);
            query = @where.Aggregate(query, (current, w) => current.And(w.Key).IsEqualTo(w.Value));
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.GetString(i));
                    }
                }
            }
            return values;
        }


        public static bool UpdateEntityForReassigningUser( Guid agencyId, Guid userId, string script,string connectionStringName)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public static bool UpdateEntityForReassigningUser(Guid agencyId, Guid patientId, Guid userId, string script, string connectionStringName)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public static bool UpdateDocumentEntityForDelete(Guid agencyId, Guid patientId, Guid episodeId, string script, string connectionStringName)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid", episodeId)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        private static string GetTableName<T>()
        {
            var subSonicTableNameOverrideAttribute = (SubSonicTableNameOverrideAttribute)Attribute.GetCustomAttribute(typeof(T), typeof(SubSonicTableNameOverrideAttribute));
            if (subSonicTableNameOverrideAttribute == null)
            {
                var name = typeof(T).Name.ToLower();
                return name.MakePlural();
            }
            return subSonicTableNameOverrideAttribute.TableName;
        }
    }
}
