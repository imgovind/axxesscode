﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Net;
    using System.Configuration;

    using Couchbase;
    using Couchbase.Configuration;

    using Enyim.Caching;
    using Enyim.Caching.Memcached;

    public static class SessionBucketActivator
    {
        #region Members

        private static IMemcachedClient memcachedClient;

        public static IMemcachedClient Cache
        {
            get
            {
                if (memcachedClient == null)
                {
                    Initialize();
                }
                return memcachedClient;
            }
        }

        private static void Initialize()
        {
            ICouchbaseClientFactory factory = Container.Resolve<ICouchbaseClientFactory>();
            if (factory != null)
            {
                memcachedClient = factory.Create((ICouchbaseClientConfiguration)ConfigurationManager.GetSection("session-bucket"));
            }
        }

        #endregion
    }
}
