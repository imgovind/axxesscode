﻿namespace Axxess.Core.Infrastructure
{
    using System;
    public class JsonViewData
    {
        public string url { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }
        public string warningMessage { get; set; }

        public JsonViewData() : this(false, string.Empty)
        {
            
        }

        public JsonViewData(bool success, string message)
        {
            isSuccessful = success;
            errorMessage = message;
        }
    }
}
