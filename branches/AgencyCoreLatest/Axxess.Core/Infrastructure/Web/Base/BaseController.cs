﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Text;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Infrastructure.Web.Base;

    [Audit]
    public abstract class BaseController : Controller
    {
        public static T Validate<T>(params Validation[] validations) where T : JsonViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();
            
            if (!entityValidator.IsValid)
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }

            else
            {
                return new T { isSuccessful = true };
            }
        }

        protected internal JsonResult CustomJson(object data)
        {
            return new CustomJsonResult()
            {
                Data = data,
                ContentType = (string)null,
                ContentEncoding = (Encoding)null,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
