﻿namespace Axxess.Core.Infrastructure
{
    public static class Crypto
    {
        private static ICryptoProvider InternalCryptoProvider
        {
            get
            {
                return Container.Resolve<ICryptoProvider>();
            }
        }

        public static string Encrypt(string plainText)
        {
            Check.Argument.IsNotEmpty(plainText, "plainText");

            return InternalCryptoProvider.Encrypt(plainText,
                CoreSettings.CryptoPassPhrase,
                CoreSettings.CryptoSaltValue,
                CoreSettings.CryptoHashAlgorithm,
                CoreSettings.CryptoPasswordIterations,
                CoreSettings.CryptoInitVector,
                CoreSettings.CryptoKeySize);
        }

        public static string Decrypt(string cypherText)
        {
            Check.Argument.IsNotEmpty(cypherText, "cypherText");

            return InternalCryptoProvider.Decrypt(cypherText,
                CoreSettings.CryptoPassPhrase,
                CoreSettings.CryptoSaltValue,
                CoreSettings.CryptoHashAlgorithm,
                CoreSettings.CryptoPasswordIterations,
                CoreSettings.CryptoInitVector,
                CoreSettings.CryptoKeySize);
        }
    }
}
