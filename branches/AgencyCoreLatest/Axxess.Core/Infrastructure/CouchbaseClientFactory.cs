﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Specialized;
    using System.Configuration;

    using Enyim.Caching;
    using Enyim.Reflection;

    using Couchbase;
    using Couchbase.Configuration;

    using Extension;

    public interface ICouchbaseClientFactory
    {
        IMemcachedClient Create(ICouchbaseClientConfiguration config);
        IMemcachedClient Create(string name, NameValueCollection config, out bool disposeClient);
    }

    public sealed class CouchbaseClientFactory : ICouchbaseClientFactory
    {
        public IMemcachedClient Create(string name, NameValueCollection config, out bool disposeClient)
        {
            // This client should be disposed of as it is not shared
            disposeClient = true;

            // Get the section name from the configuration file. If not found, create a default Couchbase client which
            // will get the configuration information from the default Couchbase client section in the Web.config file
            var sectionName = ProviderHelper.GetAndRemove(config, "section", false);
            if (sectionName.IsNullOrEmpty())
                return new CouchbaseClient();

            // If a custom section name is passed in, get the section information and use it to construct the Couchbase client
            var section = ConfigurationManager.GetSection(sectionName) as ICouchbaseClientConfiguration;
            if (section == null)
                throw new InvalidOperationException("Invalid config section: " + section);
            return new CouchbaseClient(section);
        }

        public IMemcachedClient Create(ICouchbaseClientConfiguration config)
        {
            return new CouchbaseClient(config);
        }
    }

    internal static class ProviderHelper
    {
        public static string GetAndRemove(NameValueCollection nvc, string name, bool required)
        {
            var tmp = nvc[name];
            if (tmp == null)
            {
                if (required)
                    throw new System.Configuration.ConfigurationErrorsException("Missing parameter: " + name);
            }
            else
            {
                nvc.Remove(name);
            }

            return tmp;
        }

        public static void CheckForUnknownAttributes(NameValueCollection nvc)
        {
            if (nvc.Count > 0)
                throw new System.Configuration.ConfigurationErrorsException("Unknown parameter: " + nvc.Keys[0]);
        }

        public static IMemcachedClient GetClient(string name, NameValueCollection config, Func<ICouchbaseClientFactory> createDefault, out bool disposeClient)
        {
            var factory = GetFactoryInstance(ProviderHelper.GetAndRemove(config, "factory", false), createDefault);
            System.Diagnostics.Debug.Assert(factory != null, "factory == null");

            return factory.Create(name, config, out disposeClient);
        }

        private static ICouchbaseClientFactory GetFactoryInstance(string typeName, Func<ICouchbaseClientFactory> createDefault)
        {
            if (typeName.IsNullOrEmpty())
                return createDefault();

            var type = Type.GetType(typeName, false);
            if (type == null)
                throw new System.Configuration.ConfigurationErrorsException("Could not load type: " + typeName);

            if (!typeof(ICouchbaseClientFactory).IsAssignableFrom(type))
                throw new System.Configuration.ConfigurationErrorsException("Type '" + typeName + "' must implement IMemcachedClientFactory");

            return FastActivator.Create(type) as ICouchbaseClientFactory;
        }

    }
}
