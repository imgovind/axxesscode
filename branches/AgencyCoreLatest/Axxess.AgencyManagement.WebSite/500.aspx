﻿<script type="text/C#" runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Response.StatusCode = (int) System.Net.HttpStatusCode.InternalServerError;
    }
</script>
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Axxess&#8482; AgencyCore&#8482; | Application Error</title>
    <style type="text/css">
        html{height:100%;}
        body{overflow:hidden;background:#d7d7d7;}
        div{position:fixed;top:50%;left:50%;width:32em;height:auto;background-color:#fff;margin:-6em 0 0 -16em;text-align:center;border-radius:1em;-moz-border-radius:1em;-webkit-border-radius:1em;font:1em 'Lucida Grande',Arial,'Liberation Sans',FreeSans,sans-serif;padding:1em;}
        #logo{float:left;margin:1.5em 0;}
        em{position:absolute;bottom:1em;right:1em;font-size:.7em;}
        .axxesslogo{float:left;background:url(/Images/logos.png) no-repeat 0 -119px;display:inline-block;height:141px;width:156px;margin:20px;}
        .erroricon{width:32px;height:32px;display:inline-block;background:url(/Images/sprite.png) no-repeat -128px 0;vertical-align:middle;}
    </style>
</head>
<body>
    <div>
        <span class="axxesslogo"></span>
        <h1><span class="erroricon"></span>Application Error</h1>
        <hr />
        <p>There was a problem with the application, please try again.  If problem persists, contact Axxess Healthcare Consult.</p>
        <em>HTTP 500</em>
    </div>
</body>
</html>
