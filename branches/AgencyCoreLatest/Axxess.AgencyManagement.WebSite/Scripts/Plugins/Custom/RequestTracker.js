﻿var RequestTracker = {
    Requests: {},
    Abort: function(url) {
        if (RequestTracker.Requests[url] != undefined) {
            RequestTracker.Requests[url].abort();
            RequestTracker.Requests[url] = null;
        }
    },
    Track: function(request, url) {
        RequestTracker.Abort(url);
        RequestTracker.Requests[url] = request;
    }
};