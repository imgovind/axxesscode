﻿var Acore = {
    Animated: false,                // Keep this false, this old AgencyCore is not provisioned for animations
    AutocompleteId: 0,              // Autocomplete iterator
    CascadeGap: 25,                 // Pixel gap for window cascading
    Cookies: false,                 // Keep this false, this old AgencyCore is not provisioned for cookies
    CssTop: 0,                      // CSS top iterator, used for window cascading
    CssLeft: 0,                     // CSS left iterator, used for window cascading
    GetRemoteContent: true,         // Enable remote content loading
    IdleTime: 0,                    // System idle timer
    Idle: true,                     // Idle flag used in Ping to extend the session
    MaxWindows: 8,                  // Maximum allowed number of concurrent open windows
    MinWinWidth: 1000,              // Minimum width of any window (may be overloaded per window)
    MinWinHeight: 555,              // Minimum height of any window (may be overloaded per window)
    Mobile: false,                  // Mobile site switch
    OpenWindows: 0,                 // Open window counter
    PrintId: 0,                     // Print lightbox iterator
    ReturnReasonId: "ReturnReason", // Default acore id for return reason
    Timeout: 180,                   // Auto-logout to occur at this cycle number
    TimeoutCount: 20,               // Number of cycles to display auto-logout warning
    TimeoutInterval: (15 * 1000),   // Auto-logout interval (cycle length in milliseconds)
    Windows: {},                    // Registry of available windows
    PingTimer: null,                // Timer for keep-alive pings
    TimeoutTimer: null,             // Timer for idle timeout counting
    Autosave: true,                 // Turns on/off Autosaving functionality.
    DialogId: "ModalWindow",        // Default acore id for dialog window
    AddMenu: function(Options) {
        //  Options:
        //      Name    Display name for the menu
        //      Id      DOM identifier for the menu
        //      Parent  DOM identifier for the parent menu
        //      IconX   Pixel position in sprite for menu icon X axis
        //      IconY   Pixel position in sprite for menu icon Y axis
        if (Options.Name != null && (Options.Parent == null || $("#" + Options.Parent).length)) {
            // Create basic menu option
            var menu =
                $("<li/>").append(
                    $("<a/>", { href: "javascript:void(0)", "class": "menu-trigger", text: Options.Name })).append(
                    $("<ul/>", { id: Options.Id == undefined ? Options.Name.replace(/[^0-9a-zA-Z]/, "") : Options.Id, "class": "menu" })
                );
            // If icon x position in sprite defined, add icon (default y is 86)
            if (Options.IconX != null)
                $("a", menu).prepend(
                    $("<span/>", { "class": "img icon", style: "background-position:-" + (parseInt(Options.IconX) > 0 ? parseInt(Options.IconX) : 0) + "px -" + (parseInt(Options.IconY) > 0 ? parseInt(Options.IconY) : "86") + "px;" })
                );
            // If it is located in a submenu, add the more icon
            if (Options.Parent != null && Options.Parent != "mainmenu")
                $("a", menu).append(
                    $("<span/>", { "class": "img icon more" })
                );
            // Add menu into main or submenu depending upon the Parent option
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(menu);
        }
    },
    AddMenuItem: function(Options) {
        //  Options:
        //      Id      Window identifier
        //      Name    Name for menu item
        //      Href    URL for menu link
        //      Parent  DOM identifier for parent menu
        // If external link
        if (Options.Name != null && Options.Href != null) {
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
                $("<li/>").append(
                    $("<a/>", { href: Options.Href, target: "_blank", text: Options.Name })
                )
            );
        }
        // If internal window load
        if (Options.Id != null && this.Windows[Options.Id] != null) {
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
                $("<li/>").click(function() {
                    Acore.Open(Options.Id, Acore.Windows[Options.Id].Url, Acore.Windows[Options.Id].OnLoad)
                }).append(
                    $("<a/>", { href: "javascript:void(0)", text: this.Windows[Options.Id].MenuName })
                )
            );
        }
    },
    AddWindow: function(Options) {
        //  Options:
        //      Name        Master window name
        //      WindowName  Name as seen on the top of the window
        //      MenuName    Name as seen on the top navigation bar menus
        //      TaskName    Name as seen on the bottom task bar
        //      Status      Default text seen on the status bar of the window
        //      Id          Identifier for window in acore's window registry
        //      Menu        DOM identifier for parent menu (leave null if not on menu, pass array if in multiple menus)
        //      Url         AJAX URL for window's content
        //      OnLoad      Window onLoad/Callback event
        //      WindowFrame Bool switch to display window with or without the standard frame
        //      StatusBar   Bool switch to display window with or without the status bar
        //      Width       Overloaded window default width
        //      Height      Overloaded window default height
        //      Resize      Bool switch to allow or disallow window resizing
        //      Center      Bool switch to display window centered as opposed to cascading
        //      Modal       Bool switch to shade out background
        //      IconX       Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY       Pixel position in sprite for Window/Taskbar icon Y axis
        // Ensure Id and Name are set and that the Id isn't already used
        if (Options.Id != null && Options.Name != null && this.Windows[Options.Id] == null) {
            // If more specific names are not set, set them to the default name
            Options.WindowName = Options.WindowName == null ? Options.Name : Options.WindowsName;
            Options.MenuName = Options.MenuName == null ? Options.Name : Options.MenuName;
            Options.TaskName = Options.TaskName == null ? Options.Name : Options.TaskName;
            // Set default bools
            Options.IgnoreMinSize = Options.IgnoreMinSize ? true : false;
            Options.Centered = Options.IgnoreMinSize ? true : false;
            // Set options
            this.Windows[Options.Id] = Options;
            // Add the items to the menu accordingly
            if (Options.Menu != null && Options.Menu.constructor == Array) for (i = 0; i < Options.Menu.length; i++) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu[i] });
            else if (Options.Menu != null) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu });
        }
    },
    CloseDialog: function() {
        if (Acore.Windows.ModalWindow != undefined) $("#window_ModalWindow").Close();
    },
    IncrementIdle: function() {
        // Add to idle time
        Acore.IdleTime++;
        // If idle for long enough to start the count down
        if (Acore.IdleTime + Acore.TimeoutCount >= Acore.Timeout) {
            // Open AcoreIdle Modal
            var WindowOptions = {
                Name: "Idle Logout",
                Height: 75,
                Width: 450,
                WindowFrame: false
            };
            // If there is already a modal window open, close it first
            if (Acore.Windows.ModalWindow != null) $("#window_ModalWindow").Close();
            Acore.Modal(WindowOptions);
            $("#window_ModalWindow_content").AcoreIdle();
        }
    },
    Modal: function(Options) {
        //  Options:
        //      Name        Master window name
        //      Url         AJAX URL for window's content
        //      Content     Alternate to URL, pass DOM object to append to window content
        //      Input       Variables to post to Url
        //      OnLoad      Window onLoad/Callback event
        //      Width       Overloaded window default width
        //      Height      Overloaded window default height
        //      WindowFrame Bool switch to display window with or without the standard frame
        //      IconX       Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY       Pixel position in sprite for Window/Taskbar icon Y axis
        // Ensure all temporary modal windows have the ModalWindow identifier
        Options.Id = "ModalWindow";
        // If no name is specified, set default name
        if (Options.Name == null) Options.Name = "Axxess Agencycore";
        // Add window
        this.AddWindow(Options);
        // Set modal overrides
        this.Windows.ModalWindow.Modal = true;
        this.Windows.ModalWindow.IgnoreMinSize = true;
        this.Windows.ModalWindow.Center = true;
        this.Windows.ModalWindow.Resize = false;
        this.Windows.ModalWindow.StatusBar = false;
        this.Windows.ModalWindow.TaskName = null;
        // Open modal window
        this.Open(Options.Id, Options.Url, Options.OnLoad, Options.Input);
    },
    OnLoad: function(Element) {
        // Rename Window
        if ($(".wintitle", Element).html() != undefined) {
            Element.closest(".window").Rename($(".wintitle", Element).html());
            $(".wintitle", Element).remove();
        }
        // Telerick Grid placement and manipulations
        if ($(".grid-controls", Element).length == 1 && $(".t-grid", Element).length == 1)
            $(".t-grid", Element).css("top", $(".wrapper", Element).prop("offsetHeight"));
        $(".t-group-indicator", Element).hide();
        $(".t-grouping-header", Element).remove();
        // Initialize special inputs
        $(".date-picker", Element).DatePicker();
        $(".time-picker", Element).TimePicker();
        // $("input[type=file]", Element).StyledUploader();
        $(".physicians", Element).PhysicianInput();
        $(".patient-selector", Element).PatientInput();
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        // $(".row .phone-short:eq(0)", Element).PhoneAutoTab();
        $(".numeric", Element).numeric();
        $(".floatnum", Element).floatnum();
        // $(".templates", Element).TemplateSelect();
        // Set remaining character feedback on textareas
        $("textarea[maxcharacters]", Element).after($("<em/>").addClass("characters-remaining"));
        $("textarea[maxcharacters]", Element).limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings(".characters-remaining").text("You have " + remaining + " characters remaining");
                if (remaining > 0) $(this).css("background-color", "#fff")
            },
            onLimit: function() {
                $(this).css("background-color", "#ecbab3")
            }
        })
        // Set Radios Deselectable
        $("input[type=radio]", Element).DeselectableRadio();
        // Append red asterisk to required fields
        $(".row .required,.row .requireddropdown", Element).closest(".row").prepend("<span class='required-red abs-right'>*</span>");
        if ($("form", Element).length && $(".row .required", Element).length) Element.find("form").prepend(
            $("<div/>", { "class": "abs required-legend", text: " = Required Field" }).prepend(
                $("<span/>", { "class": "required-red", text: "*" })));
        // Add hover functions on mouseover fields        
        $(".reports li,.acore-grid ol li,.cal tbody td:not(.inactive),.hover-toggle", Element).not(".no-hover").mouseover(function() {
            $(this).addClass("hover")
        }).mouseout(function() {
            $(this).removeClass("hover")
        });
        $("[status]").mouseover(function() {
            $(this).closest(".window").Status($(this).attr("status"))
        }).mouseout(function() {
            $(this).closest(".window").Status("")
        });
        // Set checkgoup functionality
        $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", Element).each(function() {
            if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
            else $(this).closest(".option").removeClass("selected");
            $(this).change(function() {
                $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", Element).each(function() {
                    if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                    else $(this).closest(".option").removeClass("selected");
                })
            })
        });
        //set autocomplete
        $("form").each(function() {
            if ($(this).attr("method") == "post") {
                $(this).removeAttr("autocomplete");
                $(this).attr('autocomplete', 'off');
            }
        });

        // Set events for select processing
        var disableSelect = function(e) { $(this).blur() };
        $("select", Element).bind("ProcessingStart", function() {
            $(this).addClass("processing").prepend(
                        $("<option/>", { text: "Loading...", value: "loading" })
                    ).val("loading").bind("focus", disableSelect);
        }).bind("ProcessingComplete", function() {
            $(this).removeClass("processing").unbind("focus", disableSelect).find("option[value=loading]").remove();
        });

        // Set events for button processing
        $(".buttons a,.button a", Element).bind("ProcessingStart", function() {
            $(this).addClass("processing-button").parent().hide().after(
                        $("<li/>", { "class": "processing", text: "Processing..." }).append(
                            $("<span/>").addClass("icon processing")));
        }).bind("ProcessingComplete", function() {
            $(this).parent().show().siblings("li.processing").remove();
        });

        // Set standard buttons
        $(".buttons .close", Element).not(".save").click(function() {
            $(this).closest(".window").Close();
            return false
        });
        if ($(".buttons .save", Element).length) {
            $(".buttons .save", Element).click(function(evnt) {
                var Form = $(this).closest("form"), Button = $(this);
                if ($("input[name=button]", Element).length) $("input[name=button]", Element).val("Save");
                $(".complete-required", Form).removeClass("required error");
                if (Button.hasClass("close")) Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                    $(this).closest(".window").Close();
                });
                else Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                    $(".processing-button", this).trigger("ProcessingComplete");
                });
                Form.unbind("SubmitFailure").bind("SubmitFailure", function() {
                    $(".processing-button", this).trigger("ProcessingComplete");
                });
                if (Form.valid()) {
                    Button.trigger("ProcessingStart");
                    Form.submit();
                }
                return false;
            });
        }
        $(".buttons .complete", Element).click(function() {
            $(".complete-required", Element).addClass("required");
            if ($("input[name=button]", Element).length) $("input[name=button]", Element).val("Complete");
            if ($(this).closest("form").valid()) {
                $(this).closest("form").submit();
                $(this).closest(".window").Close();
            }
            return false
        })
    },
    Open: function(Id, Url, OnLoad, Inputs) {
        // If using defaults, just passing arguments
        if (typeof Url === "object") Inputs = Url, Url = this.Windows[Id].Url, OnLoad = this.Windows[Id].OnLoad;
        // Ensure the top menus are all collapsed
        $("ul.menu").hide();
        // If window exists
        if (this.Windows[Id] != null) {
            // If window is already open, focus and growl
            if (this.Windows[Id].IsOpen) {
                U.Growl("You already have an open window for " + this.Windows[Id].Name + ". Please close down this window before opening another one like it.", "error");
                $("#window_" + Id).WinFocus();
                // If number of windows open is less than maximum number of windows
            } else if (this.OpenWindows < this.MaxWindows) {
                // Set window options
                var Options = {};
                $.extend(Options, Acore.Windows[Id]);
                Options.Id = Id;
                // Set window overrides
                if (Url != undefined) Options.Url = Url;
                if (OnLoad != undefined) Options.OnLoad = OnLoad;
                if (Inputs != undefined) Options.Inputs = Inputs;
                // Add window onto the desktop
                $("#desktop").append(
                    $("<div/>").AcoreWindow(Options)
                );
                if (!Acore.Mobile) {
                    // If window is smaller than minimum sizes, set to minimum size
                    if (Options.IgnoreMinSize == false && $("#window_" + Id).width() < Acore.MinWinWidth) $("#window_" + Id).css("width", Acore.MinWinWidth);
                    if (Options.IgnoreMinSize == false && $("#window_" + Id).height() < Acore.MinWinHeight) $("#window_" + Id).css("height", Acore.MinWinHeight);
                    // If window is larger than desktop, auto-maximize window
                    if (($("#window_" + Id).width() > $("#desktop").width()) || ($("#window_" + Id).height() > $("#desktop").height())) $("#window_" + Id).Maximize();
                    // If window is to be centered, set top and left accordingly
                    if (Options.Center) $("#window_" + Id).css({
                        top: ($("#desktop").height() - $("#window_" + Id).height()) / 2,
                        left: ($("#desktop").width() - $("#window_" + Id).width()) / 2
                    });
                    // If window is to be cascaded, set top and left accordingly
                    else {
                        // Increment left and top counters, and if window can't fit reset to zero
                        if (++Acore.CssLeft * Acore.CascadeGap + $("#window_" + Id).width() > $("#desktop").width()) Acore.CssLeft = 0;
                        if (++Acore.CssTop * Acore.CascadeGap + $("#window_" + Id).height() > $("#desktop").height()) Acore.CssTop = 0;
                        // Set top and left accordingly
                        $("#window_" + Id).css({
                            top: (Acore.CssTop * Acore.CascadeGap),
                            left: (Acore.CssLeft * Acore.CascadeGap)
                        });
                    }
                }
                // Set IsOpen flag to true
                this.Windows[Id].IsOpen = true;
                // Else growl to user that maximum number of windows are already open
            } else U.Growl("You can only have a maximum of " + this.MaxWindows + " concurrent windows. Please close down some of your windows to proceed", "error");
            // Else growl to user that window cannot be found
        } else U.Growl("You do not have access to open this window: " + Id + ".  Please contact Axxess for assistance in troubleshooting.", "error");
    },
    OpenPrintView: function(Options) {
        //  Options:
        //      Url         URL to print preview
        //      PdfUrl      URL to fetch PDF
        //      PdfData     Data for PDF Print POST
        //      ReturnClick Function for return button
        //      Buttons     Buttons array
        //          Text        Button text
        //          Click       Button click function
        // Open AcorePrintView modal
        var WindowOptions = {
            Name: "Print View",
            Height: "95%",
            Width: 950,
            WindowFrame: false
        };
        this.Modal(WindowOptions);
        $("#window_ModalWindow_content").AcorePrintView(Options);
    },
    Ping: function() {
        // Make AJAX POST to ping server
        U.PostUrl("Ping", { extend: !Acore.Idle });
        Acore.Idle = true;
    },
    RemoveWindow: function(Id) {
        // If window is found, remove from Acore window information registry
        if (this.Windows[Id].Menu == undefined) this.Windows[Id] = null;
        // Else growl to user that window cannot be found
        else U.Growl("Cannot remove a window which is referenced in the main menu.", "error");
    },
    ReturnReason: function(EventId, EpisodeId, PatientId, ReturnFunction) {
        if (Acore.Windows[Acore.ReturnReasonId] != undefined) $("#window_" + Acore.ReturnReasonId + "_content").ReturnComments("Refresh");
        else {
            Acore.AddWindow({
                AlwaysOnTop: true,
                Centered: false,
                Content: $("<div/>", { "class": "wrapper main" }).ReturnComments({ EventId: EventId, EpisodeId: EpisodeId, PatientId: PatientId, ReturnFunction: ReturnFunction }),
                Height: 375,
                Id: Acore.ReturnReasonId,
                IgnoreMinSize: true,
                Name: "Return Reason",
                Minimizable: typeof ReturnFunction != "function",
                Modal: false,
                Resize: false,
                ReturnReason: true,
                StatusBar: false,
                Width: 400
            });
            Acore.Open(Acore.ReturnReasonId);
        }
    },
    SessionExpired: function(request) {
        var $reauth = $('#reauthenticate');
        if (!$reauth.length) {
            
            // Pause Idle Timeout   
            clearInterval(Acore.TimeoutTimer);
            if ($('#idlemodal').length) 
                UserInterface.CloseModal();
            
            // Show login modal
            $(document.body)
                .append(
                    $('<div id="shade">')
                        .css({
                            "z-index": Acore.MaxWindows + 1,
                            "overflow": "auto",
                            "padding-top": "120px",
                            opacity: 1,
                            background: "rgba(0,0,0,.3)"
                        })
                        .append($('<div id="reauthenticate"/>').AcoreAuthenticate(request)))
        }
    }
};
(function($) {
    $.extend($.fn, {
        AcoreDesktop: function() {
            return this.each(function() {
                // If loded in frame, reload out of the frame
                if (window.location !== window.top.location) window.top.location = window.location;
                // If mobile device detected, set mobile flag to true and import stylesheet
                var browser = navigator.userAgent;
                if (browser.match(/Android/i) || browser.match(/webOS/i) || browser.match(/iPhone/i) || browser.match(/iPod/i) || browser.match(/iPad/i)) Acore.Mobile = true;
                if (Acore.Mobile) $("head").append(
                    $("<link/>", { rel: "stylesheet", type: "text/css", href: "Content/handheld.css" }));
                // Build DOM for desktop
                $(this).append(
                    $("<div/>", { "class": "abs", id: "desktop" }).bind("contextmenu", function(Event) {
                        $("#mainmenu").clone(true).ContextMenu(Event);
                    })).append(
                    $("<div/>", { "class": "abs", id: "bar-top", style: "z-index:" + parseInt(Acore.MaxWindows + 1) }).append(
                        $("<span/>", { "class": "float-right" }).append(
                            $("<a/>", { text: "Logout" }).click(function() {
                                if (confirm("Are you sure you want to logout?")) $(location).attr("href", "Logout");
                                return false;
                            }).prepend(
                                $("<span/>", { "class": "img icon", style: "background-position:-208px -84px" })))).append(
                        $("<ul/>", { id: "mainmenu" })
                    )).append(
                    $("<div/>", { id: "tab", style: "z-index:" + parseInt(Acore.MaxWindows + 1) })).append(
                    $("<div/>", { "class": "abs", id: "bar-bottom", style: "z-index:" + parseInt(Acore.MaxWindows + 1) }).append(
                        $("<ul/>", { id: "task" })).append(
                        $("<span/>", { "class": "float-right", html: "&#169; 2009 &#8211; " + (new Date).getFullYear() + " Axxess&#8482; Technology Solutions, All Rights Reserved" })));
                // Set keyboard shortcuts
                $(document).keypress(function(e) {
                    // Disable backspace for back in browser
                    if (e.which == 8 && $("input.t-input:focus,input[type=text]:focus,input[type=password]:focus,textarea:focus").length == 0) e.preventDefault();
                    // ALT+R JavaScript run prompt
                    if (e.which == 114 && e.altKey) eval(prompt("Run:\nRun the following JavaScript command:", ""));
                    // ALT+W Open acore window by id
                    if (e.which == 119 && e.altKey) eval("Acore.Open('" + prompt("Load Window:\nOpen the following AgencyCore window:", "") + "')");
                    // ALT+L Logout of software
                    if (e.which == 108 && e.altKey) if (confirm("Are you sure you want to logout?")) $(location).attr(href, "/Logout");
                    // ALT+X Close current acore window
                    if (e.which == 120 && e.altKey) if ($(".window.active").length) $(".window.active").Close();
                });
                // Bind mouse and keyboard events to trigger resetting idle counter
                var events = "mousemove keydown DOMMouseScroll mousewheel mousedown";
                //if (Acore.Mobile) events += ' touchmove touchstart';
                $(document).bind(events, function() {
                    Acore.IdleTime = 0;
                    Acore.Idle = false; // Ping
                });
                // Set thread to keep server connection alive
                Acore.PingTimer = setInterval(Acore.Ping, Acore.TimeoutInterval * 20); // every 5 minutes
                // Set thread to count idle time
                Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                // Set thread to autosave
                if (Acore.Autosave) {
                    Acore.AutosaveTimeout = window.setInterval(function() {
                        if (!Acore.Autosave) return;

                        // Click the first autosave button in each window record that we have autosaved
                        var $saves = $('.window').find('.autosave:first');
                        if ($saves.length) {
                            var $form = $saves.closest('form');
                            // if form has been edited, we can autosave it  
                            $form.each(function(i, el) {
                                var $el = $(el);
                                if ($el.data('changed')) {
                                    // Let's handler on return know whether to hide or display messages.
                                    $el.data('autosaved', true);
                                    $saves.eq(i).click()
                                    // Should be done AFTER saving.
                                        .closest('.window').find('.window-bottom').html(
                                            'Last saved ' + (new Date).toLocaleTimeString() +
                                            ' (<a href="javascript:void(0)" class="autosave_onoff">Autosave ' + (Acore.Autosave ? 'On' : 'Off') + '</a>)'
                                        );
                                }
                            });
                        }
                        // } 
                    }, 10 * 60 * 1000); // Every 10 minutes, let's save (if feature is turned on).
                    $('#desktop')
                    // Sets the form to "changed" so that we know whether or not to autosave it.
                        .delegate('form', 'keyup change', function(e) {
                            var $form = $(this);
                            if ($form.find('.autosave').length) {
                                $form.data('changed', true);
                            }
                        })
                    // Toggle Autosave
                        .delegate('.autosave_onoff', 'click', function(e) {
                            Acore.Autosave = !Acore.Autosave;
                            $('.autosave_onoff').text('Autosave ' + (Acore.Autosave ? 'On' : 'Off'));
                        });
                }
                // RE-AUTHENTICATE
                $(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
                    if (jqXHR.status === 401) {
                        Acore.SessionExpired(ajaxSettings);
                    }
                });
            });
        },
        AcoreAuthenticate: function(request) {
            return this.each(function() {
                var $modal = $(this).html(
                '<h2>Session Expired</h2>' +
                '<div style="padding:1em">' +
                    '<p>Your session has expired. Please provide your password to continue working.</p>' +
                    '<table>' +
                        '<tr>' +
                            '<td><label for="Login_UserName">Email</label></td>' +
                            '<td><input id="Login_UserName" type="text" value="' +
                                Acore.UserName + '" disabled></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td><label for="Login_Password">Password</label></td>' +
                            '<td><input id="Login_Password" type="password"></td>' +
                        '</tr>' +
                    '</table>' +
                    '<div class="btn-wrapper">' +
                        '<a href="javascript:void(0)" class="btn-custom btn-cancel">Logout</a>' +
                        '<a href="javascript:void(0)" class="btn-custom btn-ok">Continue Working</a>' +
                    '</div>' +
                '</div>'
                );

                $modal
                    .delegate('#reauthenticate .btn-ok', 'click', function(e) {

                        var pass = $modal.find('#Login_Password').val();
                        if (pass === '') {
                            U.Growl('Password is required. Please enter your password.','error');
                        } else {
                            $.ajax({
                                url: '/Reauthenticate',
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    EmailAddress: Acore.UserName,
                                    Password: pass,
                                    AgencyId: Acore.AgencyId,
                                    UserId: Acore.UserId
                                },
                                success: function(result, textStatus, jqXHR) {
                                    if (result.isSuccessful) {
                                        $('#shade').remove();
                                        U.Growl('You have successfully re-authenticated.', "success");
                                        $.ajax(request);
                                        // Restart Idle Time
                                        Acore.IdleTime = 0;
                                        Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                                    } else {
                                        U.Growl(result.errorMessage, "error");
                                    }
                                }
                            })
                        }
                    })
                    .delegate('#reauthenticate .btn-cancel', 'click', function(e) {
                        window.location.href = '/Logout';
                    })
            })
        },
        // Initialize idle modal
        AcoreIdle: function() {
            return this.each(function() {
                // Stop thread to count idle time
                clearInterval(Acore.TimeoutTimer);

                // Build idle countdown modal
                $(this).append(
                    $("<div/>", { id: "idlemodal", "class": "wrapper main ac", text: "Due to inactivity, you will automatically be logged off in " }).append(
                        $("<span/>", { id: "timeoutsec" })
                    ).append(
                        $("<div/>", { "class": "buttons" }).append(
                            $("<ul/>").append(
                                $("<li/>").append(
                                    $("<a/>", { id: "idlemodal_stay", text: "Stay Logged In" }).click(function() {
                                        // Stop thread for second by second updates
                                        clearInterval(IdleCountdown);
                                        // Close idle countdown modal
                                        $(this).closest(".window").Close();
                                        // Reset thread to count idle time
                                        Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                                        return false;
                                    })
                                )
                            ).append(
                                $("<li/>").append(
                                    $("<a/>", { id: "idlemodal_logout", text: "Logout" }).click(function() {
                                        // Logout of software
                                        $(location).attr("href", "Logout");
                                        return false;
                                    })
                                )
                            )
                        )
                    )
                );
                // Calculate auto-logout time
                var logout = new Date();
                logout.setTime(logout.getTime() + Acore.TimeoutCount * Acore.TimeoutInterval);
                // Set thread for second by second updates
                IdleCountdown = setInterval(function() {
                    // Calulate time remaining till auto-logout
                    var currentTime = new Date();
                    var remaining = new Date();
                    remaining.setTime(logout.getTime() - currentTime.getTime());
                    // If time is up, logout of software
                    if (parseInt(remaining.getMinutes()) == 59) Acore.SessionExpired();
                    // Else display amount of time remaining
                    else $("#timeoutsec").html(remaining.getMinutes() + ":" + (remaining.getSeconds() < 10 ? "0" + remaining.getSeconds() : remaining.getSeconds()));
                }, 1000);
            });
        },
        AcorePrintView: function(Options) {
            return this.each(function() {
                // Build basic print view
                $(this).css("overflow", "hidden").append(
                    $("<div/>", { id: "print-box" }).append(
                        $("<iframe/>", { name: "printview" + Acore.PrintId, id: "printview", "class": "loading", src: Options.Url }).load(function() {
                            $(this).removeClass("loading");
                        })
                    )).append(
                        $("<div/>", { id: "print-controls", "class": "buttons" }).append(
                            $("<ul/>").append(
                                $("<li/>").append(
                                    $("<a/>", { id: "printbutton", text: "Print" })
                                )).append(
                                $("<li/>").append(
                                    $("<a/>", { href: "javascript:void(0)", text: "Close" }).click(function() {
                                        $(this).closest(".window").Close();
                                    })
                                )
                            )
                        )
                    );
                // If Mobile, fix scrolling issue
                if (Acore.Mobile) {
                    $('iframe#printview').wrap(function() {
                        var $this = $(this);
                        return $('<div>').css({
                            width: $this.width(),
                            height: $this.height(),
                            overflow: 'auto',
                            '-webkit-overflow-scrolling': 'touch'
                        });
                    });
                }
                // If PDF Print, set click function
                if (Options.PdfUrl != null) $("#printbutton").click(function() {
                    U.GetAttachment(Options.PdfUrl, Options.PdfData);
                });
                // If HTML Print, set click function
                else
                    $("#printbutton").attr("href", "javascript:void(0)").click(function() {
                        window.frames["printview" + Acore.PrintId].focus();
                        window.frames["printview" + Acore.PrintId].print();
                        Acore.PrintId++;
                    });
                // If return button is enabled, add return reason field and proper button functionality
                if (Options.ReturnClick != null && typeof (Options.ReturnClick) == "function") $("#print-controls ul").prepend(
                    $("<li/>").append(
                        $("<a/>", { id: "printreturn", text: "Return" }).click(function() {
                            $("ul", "#print-controls").hide();
                            Acore.ReturnReason(Options.PdfData.eventId, Options.PdfData.episodeId, Options.PdfData.patientId, Options.ReturnClick);
                        })
                    )
                )
                // If custom buttons are defined, add them to the start of the button list
                if (Options.Buttons != null) for (i = Options.Buttons.length; i > 0; i--) {
                    $("#print-controls ul").prepend(
                        $("<li/>").append(
                            $("<a/>", { href: "javascript:void(0)", text: Options.Buttons[i - 1].Text }).click(Options.Buttons[i - 1].Click)
                        )
                    )
                }
            });
        },
        AcoreTask: function(Options) {
            return this.each(function() {
                // Build task bar item
                $(this).attr({
                    id: "task_" + Options.Id,
                    "class": "task active",
                    style: "width:" + 100 / Acore.MaxWindows + "%"
                }).append(
                    $("<a/>", { href: "javascript:void(0)", title: Options.TaskName, text: Options.TaskName }).click(function() {
                        if ($(this).closest(".task").GetWindow().hasClass("active")) $(this).closest(".task").GetWindow().Minimize();
                        else $(this).closest(".task").GetWindow().WinFocus();
                    }).prepend(
                        $("<span/>", { "class": "img icon", style: "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                    )
                ).bind("contextmenu", function(Event) {
                    // Build context menu
                    var Menu = $("<ul/>");
                    var AcoreWindow = $(this).GetWindow();
                    // If not active window, add option to bring to front
                    if (!AcoreWindow.hasClass("active")) Menu.append(
                        $("<li/>", { text: "Bring to Front" }).click(function() {
                            AcoreWindow.WinFocus();
                        })
                    );
                    // If not minimized, add option to minimize
                    if (!AcoreWindow.hasClass("minimized")) Menu.append(
                        $("<li/>", { text: "Minimize" }).click(function() {
                            AcoreWindow.Minimize();
                        })
                    );
                    // If not maximized, add option to maximize
                    if (!AcoreWindow.hasClass("maximized")) Menu.append(
                        $("<li/>", { text: "Maximize" }).click(function() {
                            AcoreWindow.Maximize();
                        })
                    );
                    // If maximized, add option to restore
                    else Menu.append(
                        $("<li/>", { text: "Restore" }).click(function() {
                            AcoreWindow.Restore();
                        })
                    );
                    // Add option to close
                    Menu.append(
                        $("<li/>", { text: "Close" }).click(function() {
                            AcoreWindow.Close();
                        })
                    );
                    // Enable context menu
                    Menu.ContextMenu(Event);
                })
            })
        },
        AcoreWindow: function(Overrides) {
            // Set window variable defaults and overrides
            var Options = $.extend({}, {
                AlwaysOnTop: false,
                ContentOverflow: true,
                Width: "85%",
                Height: "85%",
                IgnoreMinSize: false,
                WindowFrame: true,
                StatusBar: true,
                Resize: true,
                Center: false,
                Minimizanle: true,
                Modal: false,
                IconX: 220,
                IconY: 12
            }, Overrides);
            return this.each(function() {
                if (Acore.Mobile) {
                    // Minimize all windows
                    $(".window").Minimize();
                    // CSS overflow for basic window content
                    var contentCSS = { 'overflow': 'hidden' };
                    if (Options.ContentOverflow) {
                        contentCSS['overflow'] = 'auto';
                        if (Acore.Mobile) {
                            contentCSS['-webkit-overflow-scrolling'] = 'touch';
                        }
                    }
                    // Build basic mobile window
                    $(this).attr({ id: "window_" + Options.Id, "class": "window" }).append(
                        $("<div/>", { "class": "window-top" }).append(
                            $("<span/>", { "class": "float-left" }).append(
                                $("<span/>", { "class": "img icon", style: "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                            )).append(
                            $("<span/>", { "class": "abs", text: Options.WindowName })).append(
                            $("<span/>", { "class": "float-right" }).append(
                                $("<a/>", { href: "javascript:void(0)", "class": "window-min" }).click(function() { $(this).closest(".window").Minimize() })).append(
                                $("<a/>", { href: "javascript:void(0)", "class": "window-close" }).click(function() { $(this).closest(".window").Close() })
                            )
                        )).append(
                        $("<div/>", { id: "window_" + Options.Id + "_content", "class": "abs window-content loading" }).css(contentCSS)
                    );
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).Load(Options.Url, Options.Inputs, Options.OnLoad);
                    else $(".window-content", this).removeClass("loading");
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                } else {
                    // Build basic window
                    $(this).attr({ id: "window_" + Options.Id, "class": "abs window" }).css({ position: "absolute", "z-index": ++Acore.OpenWindows }).mousedown(function() {
                        if (!$(this).hasClass("active")) $(this).WinFocus();
                    }).append(
                        $("<div/>", { "class": "abs window-inner" }).append(
                            $("<div/>", { id: "window_" + Options.Id + "_content", "class": "abs window-content loading" }).css("overflow", Options.ContentOverflow ? "auto" : "hidden").scroll(function() {
                                // Collapse date picker on scroll
                                $(".ui-datepicker").hide();
                            })
                        )
                    );
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).Load(Options.Url, Options.Inputs, Options.OnLoad);
                    else $(".window-content", this).removeClass("loading");
                    // If window has frame, add top bar
                    if (Options.WindowFrame)
                        $(".window-inner", this).prepend(
                            $("<div/>", { "class": "window-top" }).dblclick(function() {
                                if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                else $(this).closest(".window").Maximize();
                            }).append(
                                $("<span/>", { "class": "float-left" }).append(
                                    $("<span/>", { "class": "img icon", style: "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" }).dblclick(function() {
                                        if (Options.Id == Acore.ReturnReasonId && $("#print-controls").length) $("ul", "#print-controls").show();
                                        $(this).closest(".window").Close();
                                    })
                                )).append(
                                $("<span/>", { "class": "abs", text: Options.WindowName })).append(
                                $("<span/>", { "class": "float-right" }).append(
                                    $("<a/>", { href: "javascript:void(0)", "class": "window-min" }).click(function() {
                                        $(this).closest(".window").Minimize();
                                    })).append(
                                    $("<a/>", { href: "javascript:void(0)", "class": "window-resize" }).click(function() {
                                        if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                        else $(this).closest(".window").Maximize();
                                    })).append(
                                    $("<a/>", { href: "javascript:void(0)", "class": "window-close" }).click(function() {
                                        if (Options.Id == Acore.ReturnReasonId && $("#print-controls").length) $("ul", "#print-controls").show();
                                        $(this).closest(".window").Close();
                                    })
                                )
                            )
                        );
                    // If unframed window, set content top accordingly
                    else $(".window-content", this).css({ top: 0, bottom: 0 });
                    if (Options.Minimizable == false) $(".window-min", this).remove();
                    // If window is resizable, enable resizing
                    if (Options.Resize)
                        $(this).resizable({
                            containment: "parent",
                            handles: "n, ne, e, se, s, sw, w, nw",
                            minWidth: Acore.MinWinWidth,
                            minHeight: Acore.MinWinHeight
                        });
                    // If window is not resizable and framed, remove resize button
                    else if (Options.WindowFrame) $(".window-resize", this).remove();
                    // If framed window has status bar, add in status bar and set content bottom accordingly
                    if (Options.WindowFrame && Options.StatusBar) {
                        $(".window-inner", this).append($("<div/>", { "class": "abs window-bottom", text: Options.Status }));
                        $(".window-content", this).css("bottom", 21);
                    } else $(".window-content", this).css("bottom", 0);
                    // Set window size
                    if (parseInt(Options.Width) > 0) $(this).css("width", Options.Width);
                    if (parseInt(Options.Height) > 0) $(this).css("height", Options.Height);
                    // If modal window, shade out the background and increase the z-index
                    if (Options.Modal) {
                        $("body").append(
                            $("<div/>", { id: "shade" }).css("z-index", parseInt(Acore.MaxWindows + 1)).bind("contextmenu", function(Event) { Event.preventDefault() })
                        );
                        $(this).css("z-index", parseInt(Acore.MaxWindows + 2));
                        // If framed non-modal window, enable draggable
                    } else if (Options.WindowFrame)
                        $(this).draggable({
                            containment: "parent",
                            handle: ".window-top .abs"
                        });
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // If always on top, fix z-index and set class
                    if (Options.AlwaysOnTop) $(this).css("z-index", parseInt(Acore.MaxWindows + ($("#shade").length ? 2 : 1))).addClass("top-window");
                    // Bring window to focus
                    $(this).WinFocus();
                }
            })
        },
        Center: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // Set window in center of desktop
                if (Window) $(this).css({
                    top: ($("#desktop").height() - $(this).height()) / 2,
                    left: ($("#desktop").width() - $(this).width()) / 2
                })
            })
        },
        Close: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If CKEditor found in window, remove instance
                    if ($(".cke_editor", Window).length) $(".cke_editor", Window).each(function() {
                        var TextArea = $(this).parent().parent().parent().attr("id").replace(/cke_/, "");
                        CKEDITOR.remove(CKEDITOR.instances[TextArea]);
                    });
                    // Set IsOpen flag to false
                    Acore.Windows[Window.GetAcoreId()].IsOpen = false;
                    // Decrement number of open windows
                    Acore.OpenWindows--;
                    // If temporary modal window, remove window from registry
                    if (Window.GetAcoreId() == "ModalWindow") Acore.RemoveWindow("ModalWindow");
                    // Remove task bar from DOM
                    Window.GetTask().remove();
                    // Remove window from DOM
                    Window.remove();
                    // Remove Print Return Reason
                    if (Window.GetAcoreId() == Acore.ReturnReasonId) Acore.RemoveWindow(Acore.ReturnReasonId);
                    // If background is shaded, remove shade
                    if ($("#shade").length) {
                        if (Acore.Windows.ModalWindow == undefined) $("#shade").remove();
                        else if (Acore.Windows.ModalWindow != undefined && !Acore.Windows.ModalWindow.Modal) $("#shade").remove();
                        else $("#window_ModalWindow").show().addClass("active");
                    }
                }
            })
        },
        ContextMenu: function(Event) {
            return this.each(function() {
                // If no other context menus are open and the DOM focus is not upon an input of type text nor textarea
                if ($(".context-menu").length == 0 && $("input.t-input:focus,input[type=text]:focus,textarea:focus").length == 0) {
                    // If no text is highlighted
                    if ((document.getSelection && document.getSelection() == "") || (document.selection && document.selection.createRange().text == "")) {
                        // Prevent the default context menu from popping up
                        Event.preventDefault();
                        // Build context menu into the DOM
                        $("body").append(
                            $("<div/>").css({
                                position: "fixed",
                                top: 0,
                                left: 0,
                                width: "100%",
                                height: "100%",
                                "z-index": 999
                            }).click(function() {
                                $(this).remove();
                            }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                                // If menu is too tall to display below the cursor, then display above
                                if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css({ bottom: $("body").height() - Event.pageY, top: "auto" });
                                else $(".context-menu").css({ top: Event.pageY, bottom: "auto" });
                                // If menu is too wide to display to the right of the cursor, then display to the left
                                if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css({ right: $("body").width() - Event.pageX, left: "auto" });
                                else $(".context-menu").css({ left: Event.pageX, right: "auto" });
                            }).append(
                                $(this).addClass("context-menu").css("z-index", "1000")
                            )
                        );
                        // If menu is too tall to display below the cursor, then display above
                        if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css("bottom", $("body").height() - Event.pageY);
                        else $(".context-menu").css("top", Event.pageY);
                        // If menu is too wide to display to the right of the cursor, then display to the left
                        if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css("right", $("body").width() - Event.pageX);
                        else $(".context-menu").css("left", Event.pageX);
                        // If context menu has submenus, enable superfish plugin
                        if ($(".context-menu ul").length) $(".context-menu").Menu();
                        // Apply hover class on mouse over and remove on mouse out
                        $(".context-menu li").each(function() { $(this).mouseover(function() { $(this).addClass("hover") }) });
                        $(".context-menu li").each(function() { $(this).mouseout(function() { $(this).removeClass("hover") }) });
                    }
                }
            })
        },
        GetAcoreId: function() {
            // If object is window or task, extrapolate the string literal for the Acore Id and return it
            if (($(this).hasClass("window") || $(this).hasClass("task") != null) && $(this).attr("id") != null) return $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
        },
        GetTask: function() {
            // If object is a window, find and return its corresponding task bar item
            if ($(this).hasClass("window") && $(this).attr("id") != null) {
                var task = "#task_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(task).length) return $(task);
            }
            return false;
        },
        GetWindow: function() {
            // If object is a task bar item, find and return its corresponding window
            if (($(this).hasClass("context") || $(this).hasClass("task")) && $(this).attr("id") != null) {
                var window = "#window_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(window).length) return $(window);
            }
            return false;
        },
        LoadTracked: function(Url, Data, Callback, ErrorCallback) {
            var element = $(this).empty().addClass("loading");
            U.LoadTrackedUrl(Url, Data, function(responseText, textStatus, XMLHttpRequest) {
                element.removeClass("loading");
                if (textStatus != 'abort') {
                    if (textStatus == 'error') {
                        element.html(U.MessageErrorAjax());
                        if (typeof ErrorCallback == "function" && responseText.status !== 401) ErrorCallback();
                    } else {
                        element.html(responseText);
                        if (typeof Callback == "function") Callback();
                    }
                }
            });
        },
        Load: function(Url, Data, Callback) {
            // If load is on window, set element to content div, detect versitile height
            var Element = $(this).hasClass("window") ? $(".window-content", this) : $(this),
                VersitileHeight = (Element.attr("id") == "window_DialogWindow_content" && Acore.Windows.DialogWindow.Height == undefined);
            // Adjust variables is optional variable Data is skipped, but optional variable Callback is specified
            if (typeof Data === "function") Callback = Data, Data = undefined;
            // If object scales to fit, set too small default height/width
            if (VersitileHeight) Element.closest(".window").css({ width: 200, height: 50 });
            // Empty element, set to loading and make AJAX request for content
            Element.empty().addClass("loading").load(Url, Data, function(responseText, textStatus, XMLHttpRequest) {
                // If session is expired, send to login page
                // if (/AgencyCore Login/.test(XMLHttpRequest.responseText)) $(location).attr("href", "SessionExpired");
                // If there was an error loading page, display error message

                if (textStatus == "error") {
                    Element.html(U.MessageErrorAjax()).removeClass("loading");
                }
                else if (responseText.charAt(0) == "{") {
                    var json = JSON.parse(responseText);
                    U.Growl(json.errorMessage, "error");
                    Element.html(U.MessageInfo("Important Information", json.errorMessage)).removeClass("loading");
                }
                else {
                    // Run global init script
                    Acore.OnLoad(Element);
                    // Remove loading image
                    Element.removeClass("loading");
                    // Adjust height on model/dialog windows and center window
                    if (VersitileHeight) {
                        Element.closest(".window").css({ width: Acore.Windows.DialogWindow.Width, height: 50 });
                        var Height = Element.prop("scrollHeight") + (Acore.Windows.DialogWindow.WindowFrame ? 31 : 2);
                        if (Height < $("#desktop").height()) Element.closest(".window").css("height", Height).Center();
                    }

                    // Run callback function, if specified
                    if (typeof Callback === "function") {
                        try {
                            Callback(responseText, textStatus, XMLHttpRequest, Element)
                        } catch (e) {
                            Element.html(U.MessageErrorJS());
                            console.log("Error in callback function on load for #" + Element.attr("id") + ": " + e.toString());
                            $.error(e)
                        }
                    }
                }
            })
        },
        Maximize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // Maximize window
                    Window.attr({
                        "restore-top": $(this).css("top"),
                        "restore-left": $(this).css("left"),
                        "restore-right": $(this).css("right"),
                        "restore-bottom": $(this).css("bottom"),
                        "restore-width": $(this).css("width"),
                        "restore-height": $(this).css("height")
                    }).addClass("maximized").removeClass("minimized").css({
                        top: $(this).attr("id") == "window_ModalWindow" ? 20 : 0,
                        left: $(this).attr("id") == "window_ModalWindow" ? 20 : 0,
                        right: $(this).attr("id") == "window_ModalWindow" ? 20 : 0,
                        bottom: $(this).attr("id") == "window_ModalWindow" ? 20 : 0,
                        width: "auto",
                        height: "auto"
                    });
                    Window.trigger("resizestop");
                    $(window).resize();
                    // If not the active window, then bring to front
                    if (!Window.hasClass("active")) Window.WinFocus();
                }
            })
        },
        Minimize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // Minimize window
                if (Window) Window.removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active");
            })
        },
        Rename: function(NewName, TaskRename) {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If window has frame, rename at the top of the window
                    if ($(".window-top .abs", Window).length) $(".window-top .abs", Window).html(NewName);
                    // Rename task bar item
                    if (TaskRename) Window.GetTask().find("a").html(
                        Window.GetTask().find(".img")
                    ).append(NewName).attr("title", Window.GetTask().text());
                }
            })
        },
        Restore: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // If window is maximized, restore css to old numbers
                if (Window && Window.hasClass("maximized")) {
                    $(this).removeClass("maximized").css({
                        top: $(this).attr("restore-top"),
                        left: $(this).attr("restore-left"),
                        right: $(this).attr("restore-right"),
                        bottom: $(this).attr("restore-bottom"),
                        width: $(this).attr("restore-width"),
                        height: $(this).attr("restore-height")
                    })
                    Window.trigger("resizestop");
                    $(window).resize();
                }
            })
        },
        Status: function(Message) {
            return this.each(function() {
                // Find window and status bar
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window"),
                    StatusBar = Window.find(".window-bottom");
                // Set status bar message
                if (StatusBar) StatusBar.text(Message);
            })
        },
        WinFocus: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If mobile, minimize all windows
                    if (Acore.Mobile) $(".window").Minimize();
                    // Clear current active window
                    $(".window.active", "#desktop").removeClass("active");
                    $(".active", "#task").removeClass("active");
                    // Hide menus
                    $(".ui-menu").hide();
                    // Make current window active
                    if (Window.css("z-index") < Acore.MaxWindows) Window.css("z-index", Acore.OpenWindows);
                    Window.addClass("active").removeClass("minimized");
                    Window.GetTask().addClass("active");
                    // Recalculate other windows' z-index
                    $(".window:not(.active)").each(function() {
                        if ($(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    });
                    // Recalculate other windows' z-index
                    $(".window:not(.active):not(.top-window)").each(function() {
                        if (!Acore.Windows[$(this).GetAcoreId()].Modal && $(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    })
                }
            })
        },
        Layout: function(Options) {
            if (!Options) Options = {
                west: {
                    paneSelector: ".ui-layout-west",
                    size: 200,
                    minSize: 160,
                    maxSize: 400,
                    livePaneResizing: true,
                    spacing_open: 3
                }
            };
            if ($(this).length) $(this).layout(Options);
        }
    })
})(jQuery);