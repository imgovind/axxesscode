// Define Plugin
(function($) {
    var multiGridMethods = {
        buildGrid: function(left, title) {
            return $("<div/>").addClass((left ? "left" : "right") + "-grid acore-grid").append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<h3>").addClass("ac strong").text(title))).append(
                    $("<li/>").append(
                        $("<input/>").addClass("grid-search")).append(
                        $("<span/>").addClass("select-all").append(
                            $("<input/>", { type: "checkbox" })).append(
                            $("<label/>").text("Select All"))))).append(
                $("<ol/>").sortable({
                    connectWith: ".multi-grid .acore-grid ol",
                    start: function(event, ui) {
                        ui.item.siblings(".selected.match").not(".ui-sortable-placeholder").appendTo(ui.item);
                    },
                    stop: function(event, ui) {
                        var directionLeft = ui.item.closest(".acore-grid").hasClass("left-grid"),
                            movedItems = [];
                        $("[type=checkbox]", ui.item).each(function() {
                            movedItems.push({ name: "movedItems", value: $(this).val() })
                        });
                        ui.item.after(ui.item.find("li"));
                        ui.item.closest("ol").find("li").each(function() {
                            $(this).removeClass("selected hover").find("[type=checkbox]").prop("checked", false)
                        });
                        ui.item.addClass("selected").find("[type=checkbox]").prop("checked", true).closest(".multi-grid").MultiGrid("sort");
                        ui.item.closest(".multi-grid").MultiGrid("move", directionLeft, movedItems)
                    }
                })
            )
        },
        fetchData: function() {
            this.each(function() {
                var e = $(this),
                    gridData = e.data("MultiGrid");
                U.PostUrl(gridData.DataUri, { id: e.attr("guid") }, function(result) { e.MultiGrid("rebuild", result) });
                return e;
            })
        },
        init: function(options) {
            this.each(function() {
                var e = $(this);
                e.data("MultiGrid", options).addClass("multi-grid").append(
                    multiGridMethods.buildGrid(true, options.LeftGridTitle)).append(
                    multiGridMethods.buildGrid(false, options.LeftGridTitle)).append(
                    $("<div/>").addClass("controls").append(
                        $("<span/>").addClass("img icon left-arrow")).append(
                        $("<span/>").addClass("img icon right-arrow")));
                e.MultiGrid("fetchData");
                $(".left-arrow,.right-arrow", e).click(function() {
                    var directionLeft = $(this).hasClass("left-arrow"),
                        selectedItems = $("." + (directionLeft ? "right" : "left") + "-grid .selected.match", e),
                        movedItems = [];
                    if (selectedItems.length) {
                        $("[type=checkbox]", selectedItems).each(function() {
                            movedItems.push({ name: "movedItems", value: $(this).val() })
                        });
                        selectedItems.appendTo($("." + (directionLeft ? "left" : "right") + "-grid ol", e)).removeClass("selected").find("[type=checkbox]").prop("checked", false);
                        $(".select-all [type=checkbox]", e).prop("checked", false).siblings("label").text("Select All");
                        e.MultiGrid("move", directionLeft, movedItems);
                    } else U.Growl("Please select at least one item that you wish to move.", "error");
                });
                $(".select-all input", e).change(function() {
                    var checked = $(this).prop("checked");
                    $(this).siblings("label").text((checked ? "Des" : "S") + "elect All").closest(".acore-grid").find("ol li").each(function() {
                        if (checked) $(this).addClass("selected");
                        else $(this).removeClass("selected");
                        $(this).find("[type=chackbox]").prop("checked", checked)
                    })
                });
                $(".grid-search", e).AcoreGridSearch();
                return e;
            })
        },
        move: function(left, data) {
            this.each(function() {
                var e = $(this),
                    gridData = e.data("MultiGrid");
                data.push({ name: "left", value: left });
                data.push({ name: "id", value: e.attr("guid") })
                U.PostUrl(gridData.MoveUri, data, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) {
                        e.MultiGrid("sort");
                        if (typeof gridData.MoveCallback == "function") gridData.MoveCallback.apply(this);
                    } else e.MultiGrid("rebuild", result);
                }, function(result) {
                    e.MultiGrid("rebuild", result)
                })
            })
        },
        rebuild: function(data) {
            this.each(function() {
                var e = $(this);
                $(".left-grid ol,.right-grid ol", e).html();
                $.each(data.Left, function() {
                    $(".left-grid ol", e).append(
                        $("<li/>").addClass("match").text($(this).attr("DisplayName")).prepend(
                            $("<input/>", { type: "checkbox", value: $(this).attr("Id") })))
                });
                $.each(data.Right, function() {
                    $(".right-grid ol", e).append(
                        $("<li/>").addClass("match").text($(this).attr("DisplayName")).prepend(
                            $("<input/>", { type: "checkbox", value: $(this).attr("Id") })))
                });
                $(".left-grid ol,.right-grid ol", e).Zebra();
                $("ol li", e).mouseover(function() { $(this).addClass("hover") }).mouseout(function() { $(this).removeClass("hover") }).click(function() {
                    $(this).toggleClass("selected").find("[type=checkbox]").prop("checked", $(this).hasClass("selected"))
                });
                return e;
            })
        },
        sort: function() {
            return this.each(function() {
                var e = $(this);
                $(".left-grid ol,.right-grid ol", e).addClass("loading").each(function() {
                    var list = $(this),
                        items = list.children("li");
                    items.sort(function(a, b) { return $(a).text().toUpperCase() > $(b).text().toUpperCase() });
                    $.each(items, function(index, item) { list.append(item) });
                    list.Zebra("li.match").removeClass("loading");
                })
            })
        }
    };
    $.extend($.fn, {
        MultiGrid: function(method) {
            if (multiGridMethods[method]) return multiGridMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            else if (typeof method === "object" || !method) return multiGridMethods.init.apply(this, arguments);
            else $.error("Method " + method + " does not exist for the MultiGrid plugin.");
        }
    })
})(jQuery);