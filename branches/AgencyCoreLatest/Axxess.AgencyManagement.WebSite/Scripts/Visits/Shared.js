﻿var Visit = {
    Shared: {
        Init: function(Type, InitFunction, ContentUrl) {
            var Prefix = "#" + Type + "_";
            if (ContentUrl == undefined) ContentUrl = "Schedule/" + Type + "Content";
            Template.OnChangeInit();
            Visit.Shared.PreviousNotesClickInit(Visit.Shared.PreviousNotesInit(Type, ContentUrl, InitFunction), Type);
        },
        PreviousNotesInit: function(Type, ContentUrl, InitFunction) {
            var Prefix = "#" + Type + "_";
            return $(Prefix + "PreviousNotes").change(function() {
                var prevNote = $(this).val();
                $(Prefix + "Content").Load(ContentUrl, {
                    patientId: $(Prefix + "PatientId").val(),
                    noteId: prevNote,
                    type: Type
                }, InitFunction);
            });
        },
        PreviousNotesClickInit: function(Element, Type) {
            var Prefix = "#" + Type + "_";
            return Element.click(function() {
                Element.trigger("ProcessingStart");
                var id = $(Prefix + "Id").length ? $(Prefix + "Id").val() : $(Prefix + "EventId").val();
                U.PostUrl("/Schedule/Previous", { EpisodeId: $(Prefix + "EpisodeId").val(), PatientId: $(Prefix + "PatientId").val(), EventId: id }, function(data) {
                    Element.empty().trigger("ProcessingComplete").unbind("click");
                    $.each(data, function(index, itemData) { Element[0].add(new Option(itemData.Text, itemData.Value, false, false)); });
                }, function() {
                    Element.trigger("ProcessingComplete");
                });
            });
        },
        Submit: function(Button, Completing, Type) {
            var $form = Button.closest("form");
            if (Completing) $(".complete-required", "#window_" + Type).addClass("required");
            else $(".complete-required", "#window_" + Type).removeClass("required");
            $("#" + Type + "_Button").val(Button.text());
            $form.validate();
            if ($form.valid()) {
                Button.trigger("ProcessingStart");
                $form.ajaxSubmit({
                    dataType: "json",
                    data: { AuditSilence: $form.data('autosaved') },
                    beforeSubmit: Visit.Shared.BeforeSubmit,
                    success: function(Result) { Visit.Shared.AfterSubmit(Result, Button, Type, $form) },
                    error: function(Result) { Visit.Shared.AfterSubmit(Result, Button, Type, $form) }
                })
            } else U.ValidationError(Button);
        },
        BeforeSubmit: function() { },
        AfterSubmit: function(Result, Button, Type, $form) {
            var Command = Button.text();
            Button.trigger("ProcessingComplete");
            // If the form was autosaved, we don't want to do Growl messages or refresh the UI.
            if ($form.data('autosaved')) {
                $form.data({
                    autosaved: false,
                    changed: false
                });
                return;
            }
            var patientId = $("#" + Type + "_PatientId").val();
            if (Result.responseText && typeof Result.responseText == "string" && Result.responseText[0] == "{") {
                Result = JSON.parse(Result.responseText);
            }
            if (Result.isSuccessful) switch (Command) {
                case "Save":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.Refresh(patientId);
                    break;
                case "Complete":
                case "Approve":
                case "Return":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.CloseAndRefresh(Type, patientId);
                    break;
            } else {
                if (Result.errorMessage) {
                    U.Growl(Result.errorMessage, "error");
                }
                else {
                    U.Growl("A problem occured while attempting to save the note.", "error");
                }

            };
        },
        Print: function(EpisodeId, PatientId, EventId, QA, Type, Load) {
            if (Load == undefined) Load = function() {
                eval("Visit." + Type + ".Load('" + EpisodeId + "','" + PatientId + "','" + EventId + "')");
                UserInterface.CloseModal();
            };
            var Arguments = {
                Url: Type + "/View/" + EpisodeId + "/" + PatientId + "/" + EventId,
                PdfUrl: "Schedule/" + Type + "Pdf",
                PdfData: {
                    episodeId: EpisodeId,
                    patientId: PatientId,
                    eventId: EventId
                }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Schedule.ProcessNote("Return", EpisodeId, PatientId, EventId) },
                Buttons: [
                    {
                        Text: "Edit",
                        Click: function() {
                            if (typeof Load === "function") {
                                try {
                                    Load();
                                } catch (e) {
                                    U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                    console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                    $.error(e)
                                }
                            }
                        }
                    }, {
                        Text: "Approve",
                        Click: function() {
                            Schedule.ProcessNote("Approve", EpisodeId, PatientId, EventId)
                        }
                    }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
};