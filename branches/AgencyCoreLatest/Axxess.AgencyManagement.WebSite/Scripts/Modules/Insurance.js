﻿var Insurance = {
    Delete: function(Id) { U.DeleteTemplate("Insurance", Id); },
    InitEdit: function() {
        U.InitEditTemplate("Insurance");
        U.PhoneAutoTab("Edit_Insurance_SubmitterPhoneArray");
        U.PhoneAutoTab("Edit_Insurance_PhoneNumberArray");
        U.PhoneAutoTab("Edit_Insurance_FaxNumberArray");
    },
    InitNew: function() {
        U.InitNewTemplate("Insurance");
        U.PhoneAutoTab("New_Insurance_SubmitterPhone");
        U.PhoneAutoTab("New_Insurance_PhoneNumberArray");
        U.PhoneAutoTab("New_Insurance_FaxNumberArray");
    },
    RebindList: function() { U.RebindTGrid($('#List_AgencyInsurance')); },
    loadInsuarnceDropDown: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/InsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() }, function(data) {
            var s = $("select#" + reportName + "_InsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[0] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false) })
            if (action != null && action != undefined && typeof (action) == "function") { action(); }
        })
    },
    loadMedicareInsuarnceDropDown: function(reportName, title, isZeroIndexPrefilled) {
        U.PostUrl("Agency/MedicareInsuranceSelectList", {
            branchId: $("#" + reportName + "_BranchCode").val()
        }, function(data) {
            var s = $("select#" + reportName + "_InsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[0] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false) });
        })
    },
    loadMedicareWithHMOInsuarnceDropDown: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/MedicareWithHMOInsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() },
        function(data) {
            var s = $("select#" + reportName + "_InsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled)
            { s.get(0).options[0] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData)
            { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, itemData.Selected) });
            if (action != null && action != undefined && typeof (action) == "function") {
                action();
            }
        })
    },
    VisitInfoReplace: function(Id, replacedId) {
        if (replacedId <= 0) {
            alert('Select the insurance.');
            return;
        }
        else {
            if (confirm("Are you sure you want to replace/overwrite visit information?")) {
                U.PostUrl("/Agency/ReplaceInsuranceVisit", { Id: Id, replacedId: replacedId }, function(result) {
                    if (result.isSuccessful) {
                        var insuranceVisitInfoGrid = $('#Edit_Insurance_BillDatas').data('tGrid');
                        if (insuranceVisitInfoGrid != null) {
                            U.Growl(result.errorMessage, "success");
                            insuranceVisitInfoGrid.rebind({ InsuranceId: Id });
                        }
                    }
                    else {
                        U.Growl(result.errorMessage, "error");
                    }
                });
            }
        }
    }

}