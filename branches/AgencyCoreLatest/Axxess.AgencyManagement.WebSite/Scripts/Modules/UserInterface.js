var UserInterface = {
    ShowNoteModal: function(Text, Color) {
        if (Color != "red" && Color != "blue") Color = "yellow";
        $("body").append(unescape("%3Cdiv id=%22shade%22%3E%3C/div%3E%3Cdiv class=%22note_modal bottom " + Color + "%22%3E%3C/div%3E%3Cdiv class=%" +
            "22note_modal middle " + Color + "%22%3E%3C/div%3E%3Cdiv class=%22note_modal top " + Color + "%22%3E%3Cdiv id=%22note_close%22%3EX%3C/" +
            "div%3E%3Cdiv class=%22note_modal_content%22%3E" + Text + "%3C/div%3E%3C/div%3E"));
        $("#note_close").click(function() { $("#shade").remove(); $(".note_modal").remove() });
    },
    ShowPatientChart: function(Id, patientStatus, patientBranch) {
        if (Acore.Windows.patientcenter.IsOpen) {
            $("#window_" + Patient.Charts.AcoreId).WinFocus();
            Patient.Charts.PatientSelector.Select(Id, patientStatus, patientBranch);
        } else Acore.Open(Patient.Charts.AcoreId, "Patient/Center", function(r, t, x, e) {
            Patient.Charts.Init(r, t, x, e);
            Patient.Charts.PatientToSelect = Id;
        }, { status: patientStatus, branch: patientBranch });
    },
    ShowScheduleCenter: function(Id, patientStatus, patientBranch) {
        if (Acore.Windows.ScheduleCenter.IsOpen) {
            $("#window_" + Schedule.Center.AcoreId).WinFocus();
            Schedule.Center.PatientSelector.Select(Id, patientStatus, patientBranch);
        } else Acore.Open(Schedule.Center.AcoreId, "Schedule/Center",
            function(r, t, x , e) {
                Schedule.Center.Init(r, t, x, e);
                Schedule.Center.PatientToSelect = Id;
            }, { status: patientStatus });
    },
    ShowUserTasks: function() {
        Acore.Open("listuserschedule", 'User/Schedule', function() { });
    },
    ShowPastDueRecerts: function() {
        Acore.Open("listpastduerecerts", 'Agency/RecertsPastDueGrid', function() { });
    },
    ShowUpcomingRecerts: function() {
        Acore.Open("listupcomingrecerts", 'Agency/RecertsUpcomingGrid', function() { });
    },
    ShowPatientCenter: function() {
        Acore.Open("patientcenter", 'Patient/Center', Patient.Charts.Init);
    },
    ShowNewPatient: function() {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); });
    },
    ShowEditPatient: function(Id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() { Patient.InitEdit(); }, { patientId: Id });
    },
    ShowNewReferral: function() {
        Acore.Open("newreferral", 'Referral/New', function() { Referral.InitNew(); });
    },
    ShowEditReferral: function(Id) {
        Acore.Open("editreferral", 'Referral/Edit', function() { Referral.InitEdit(); }, { Id: Id });
    },
    ShowNewContact: function() {
        Acore.Open("newcontact", 'Contact/New', function() { Contact.InitNew(); });
    },
    ShowEditContact: function(Id) {
        Acore.Open("editcontact", "Contact/Edit", function() { Contact.InitEdit(); }, { Id: Id });
    },
    ShowNewLocation: function() {
        Acore.Open("newlocation", 'Location/New', function() { Location.InitNew(); });
    },
    ShowEditLocation: function(Id) {
        Acore.Open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { Id: Id });
    },
    ShowNewPhysician: function() {
        Acore.Open("newphysician", 'Physician/New', function() { Physician.InitNew(); });
    },
    ShowEditPhysician: function(Id) {
        Acore.Open("editphysician", 'Physician/Edit', function() { Physician.InitEdit(); }, { Id: Id });
    },
    ShowNewHospital: function() {
        Acore.Open("newhospital", "Hospital/New", function() { Hospital.InitNew(); });
    },
    ShowEditHospital: function(Id) {
        Acore.Open("edithospital", "Hospital/Edit", function() { Hospital.InitEdit(); }, { Id: Id });
    },
    ShowNewPharmacy: function() {
        Acore.Open("newpharmacy", "Pharmacy/New", function() { Pharmacy.InitNew(); });
    },
    ShowEditPharmacy: function(Id) {
        Acore.Open("editpharmacy", "Pharmacy/Edit", function() { Pharmacy.InitEdit(); }, { Id: Id });
    },
    ShowNewInsurance: function() {
        Acore.Open("newinsurance", "Insurance/New", function() { Insurance.InitNew(); });
    },
    ShowEditInsurance: function(Id) {
        Acore.Open("editinsurance", "Insurance/Edit", function() { Insurance.InitEdit(); }, { Id: Id });
    },
    ShowNewTemplate: function() {
        Acore.Open("newtemplate", 'Template/New', function() { Template.InitNew(); });
    },
    ShowEditTemplate: function(id) {
        Acore.Open("edittemplate", "Template/Edit", function() { Template.InitEdit(); }, { Id: id });
    },
    ShowNewNonVisitTask: function() {
        Acore.Open("newnonvisittask", 'NonVisitTask/New', function() { NonVisitTask.InitNew(); });
    },
    ShowEditNonVisitTask: function(id) {
        Acore.Open("editnonvisittask", "NonVisitTask/Edit", function() { NonVisitTask.InitEdit(); }, { Id: id });
    },
    ShowNewNonVisitTaskManager: function() {
        Acore.Open("newnonvisittaskmanager", 'NonVisitTaskManager/New', function() { NonVisitTaskManager.InitNew(); });
    },
    ShowEditNonVisitTaskManager: function(id) {
        Acore.Open("editnonvisittaskmanager", "NonVisitTaskManager/Edit", function() { NonVisitTaskManager.InitEdit(); }, { Id: id });
    },
    ShowNewSupply: function() {
        Acore.Open("newsupply", 'Supply/New', function() { Supply.InitNew(); });
    },
    ShowEditSupply: function(id) {
        Acore.Open("editsupply", "Supply/Edit", function() { Supply.InitEdit(); }, { Id: id });
    },
    ShowRap: function(episodeId, patientId) {
        Acore.Open("rap", "Billing/Rap", function() { Billing.InitRap(); }, { episodeId: episodeId, patientId: patientId });
    },
    ShowFinal: function(episodeId, patientId) {
        Acore.Open("final", "Billing/Final", function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowEditPlanofCare: function(episodeId, patientId, eventId) {
        Acore.Open("editplanofcare", 'Oasis/Edit485', function() { PlanOfCare.InitEdit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditOTPlanOfCare: function(episodeId, patientId, eventId) {
        Acore.Open("OTPlanOfCare", 'Schedule/OTPlanOfCare', function() { $("#OTPlanOfCare_PhysicianId").PhysicianInput(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditPTPlanOfCare: function(episodeId, patientId, eventId) {
        Acore.Open("PTPlanOfCare", 'Schedule/PTPlanOfCare', function() { $("#PTPlanOfCare_PhysicianId").PhysicianInput(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditSTPlanOfCare: function(episodeId, patientId, eventId) {
        Acore.Open("STPlanOfCare", 'Schedule/STPlanOfCare', function() { $("#STPlanOfCare_PhysicianId").PhysicianInput(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditPlanofCareStandAlone: function(episodeId, patientId, eventId) {
        Acore.Open("newplanofcare", 'Oasis/New485', function() { PlanOfCare.InitNew(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowNewUser: function() {
        Acore.Open("newuser", "User/New", function() { User.InitNew(); });
    },
    ShowEditOrder: function(id, patientId) {
        Acore.Open("editorder", "Order/Edit", function() { Patient.InitEditOrder(); }, { id: id, patientId: patientId });
    },
    ShowReportCenter: function(DefaultReport) {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); Report.Show(DefaultReport); });
    },
    ShowBillingCenterRAP: function() {
        Acore.Open("billingcenterrap", "Billing/RAPCenter", function() { });
    },
    ShowBillingCenterFinal: function() {
        Acore.Open("billingcenterfinal", "Billing/FinalCenter", function() { });
    },
    ShowBillingCenterAllClaim: function() {
        Acore.Open("allbillingclaims", "Billing/AllClaims", function() { });
    },
    ShowPatientPrompt: function() {
        Acore.Modal({
            Name: "Next Step",
            Url: "Patient/NextStep",
            Height: "200px",
            Width: "350px",
            WindowFrame: false
        })
    },
    ShowNewIncidentReport: function() {
        Acore.Open("newincidentreport", 'Incident/New', IncidentReport.InitNew);
    },
    ShowEditIncident: function(Id) {
        Acore.Open("editincidentreport", 'Incident/Edit', IncidentReport.InitEdit, { Id: Id });
    },
    ShowNewInfectionReport: function() {
        Acore.Open("newinfectionreport", 'Infection/New', InfectionReport.InitNew);
    },
    ShowEditInfection: function(Id) {
        Acore.Open("editinfectionreport", 'Infection/Edit', InfectionReport.InitEdit, { Id: Id });
    },
    ShowPatientBirthdays: function() {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); Report.Show(1, '#patient_reports', '/Report/Patient/Birthdays'); return false; });
    },
    ShowPatientAuthorizations: function(patientId) {
        Acore.Open("listauthorizations", 'Authorization/Grid', function() { }, { patientId: patientId });
    },
    ShowDeletedTaskHistory: function(patientId) {
        Acore.Open("patientdeletedtaskhistory", 'Patient/DeletedTaskHistory', function() { }, { patientId: patientId });
    },
    ShowPatientOrdersHistory: function(patientId) {
        Acore.Open("patientordershistory", 'Patient/OrdersHistory', function() { }, { patientId: patientId });
    },
    ShowPatientSixtyDaySummary: function(patientId) {
        Acore.Open("patientsixtydaysummary", 'Patient/SixtyDaySummary', function() { }, { patientId: patientId });
    },
    ShowPatientVitalSigns: function(patientId) {
        Acore.Open("patientvitalsigns", 'Patient/VitalSigns', function() { }, { PatientId: patientId });
    },
    ShowMedicareEligibilityReports: function(patientId) {
        Acore.Open("medicareeligibilitylist", 'Patient/MedicareEligibilityList', function() { }, { patientId: patientId });
    },
    ShowPatientAllergies: function(patientId) {
        Acore.Open("allergyprofile", 'Patient/AllergyProfile', function() { }, { patientId: patientId });
    },
    ShowPatientHospitalizationLogs: function(Id) {
        Acore.Open("patienthospitalizationlogs", 'Patient/HospitalizationLogs', function() { }, { patientId: Id });
    },
    RefreshPlanofCare: function(episodeId, patientId, eventId) {
        $("#planofCareContentId").empty().addClass("loading").load('Oasis/PlanofCareContent', { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('This page could not be refreshed. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#planofCareContentId").removeClass("loading");
        });
    },
    RefreshTherapyPlanOfCare: function(type, eventId, version) {
        $("#" + type + "_Content").empty().addClass("loading").load('Schedule/PlanofCareContent', { type: type, eventId: eventId, version: version }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('This page could not be refreshed. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#" + type + "_Content").removeClass("loading");
        });
    },
    ShowAgencySelectionModal: function(loginId) {
        $("#Agency_Selection_Container").load("/User/Agencies", { loginId: loginId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") U.ShowDialog("#Agency_Selection_Container", function() { User.InitChangeSignature() })
        })
    },
    ShowEpisodeOrders: function(episodeId, patientId) {
        Acore.Open("patientepisodeorders", 'Patient/EpisodeOrdersView', function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowPatientEligibility: function(medicareNumber, lastName, firstName, dob, gender) {
        if (medicareNumber == "" || lastName == "" || firstName == "" || dob == "" || gender == undefined) {
            var error = "Unable to process Medicare eligibility request:";
            if (medicareNumber == "") error += "<br /> &#8226; Medicare Number is Required";
            if (lastName == "") error += "<br /> &#8226; Last Name is Required";
            if (firstName == "") error += "<br /> &#8226; First Name is Required";
            if (dob == "") error += "<br /> &#8226; Date of Birth is Required";
            if (gender == undefined) error += "<br /> &#8226; Gender is Required";
            U.Growl(error, "error");
        } else {
            Acore.Modal({
                Name: "Medicare Eligibility",
                Url: "Patient/Verify",
                Input: {
                    medicareNumber: medicareNumber,
                    lastName: lastName,
                    firstName: firstName,
                    dob: dob,
                    gender: gender
                },
                Width: "800px",
                Height: "400px",
                WindowFrame: false
            })
        }
    },
    ShowMedicareEligibility: function(medEligibilityId, patientId) {
        Acore.Modal({
            Name: "Medicare Eligibility",
            Url: "Patient/MedicareEligibility",
            Input: {
                medicareEligibilityId: medEligibilityId,
                patientId: patientId
            },
            Width: "800px",
            Height: "400px",
            WindowFrame: false
        })
    },
    ShowMissedVisitModal: function(episodeId, patientId, eventId) {
        Acore.Modal({
            Name: "New Physician",
            Url: "Visit/Miss",
            Input: { episodeId: episodeId, patientId: patientId, eventId: eventId },
            OnLoad: User.InitMissedVisit,
            Width: "800px",
            Height: "475px",
            WindowFrame: false
        })
    },
    ShowMissedVisitEdit: function(eventId) {
        Acore.Open("editmissedvisit", "Schedule/EditMissedVisitInfo", function() { User.InitEditMissedVisit(); }, { Id: eventId });
        //        Acore.Modal({
        //            Name: "Edit Missed Visit Form",
        //            Url: "Schedule/EditMissedVisitInfo",
        //            Input: { Id: eventId },
        //            Width: "800px",
        //            Height: "475px",
        //            WindowFrame: false
        //        })
    },
    ShowNewPhysicianModal: function() {
        Acore.Modal({
            Name: "New Physician",
            Url: "Physician/New",
            OnLoad: Physician.InitNewModal,
            Width: "900px",
            Height: "540px",
            WindowFrame: false
        })
    },
    ShowNewPhotoModal: function(patientId) {
        Acore.Modal({
            Name: "New Photo",
            Url: "Patient/NewPhoto",
            Input: { patientId: patientId },
            OnLoad: Patient.InitNewPhoto,
            Width: "315px",
            Height: "343px",
            WindowFrame: false
        })
    },
    ShowNewDocumentModal: function(patientId) {
        Acore.Modal({
            Name: "New Document",
            Url: "Patient/NewDocument",
            Input: { patientId: patientId },
            OnLoad: Patient.InitNewDocument,
            Width: "315px",
            Height: "343px",
            WindowFrame: false
        })
    },
    ShowEditDocumentModal: function(patientId, documentId) {
        Acore.Modal({
            Name: "Edit Document",
            Url: "Patient/EditDocument",
            Input: { patientId: patientId, documentId: documentId },
            OnLoad: Patient.InitEditDocument,
            Width: "315px",
            Height: "250px",
            WindowFrame: false
        })
    },
    ShowPatientDocuments: function(patientId) {
        Acore.Open("patientdocuments", "Patient/Documents", function() { }, { patientId: patientId });
    },
    ShowMultipleDayScheduleModal: function(episodeId, patientId) {
        Acore.Modal({
            Name: "Quick Employee Scheduler",
            Url: "Schedule/MultiDay",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: Schedule.InitMultiDayScheduler,
            Width: "900px",
            Height: "450px",
            WindowFrame: false
        })
    },
    ShowNewEpisodeModal: function(Id) {
        Acore.Modal({
            Name: "New Episode",
            Url: "Schedule/NewEpisode",
            Input: { patientId: Id },
            OnLoad: Schedule.InitNewEpisode,
            Width: "800px",
            Height: "515px",
            WindowFrame: false
        })
    },
    ShowEditEpisodeModal: function(episodeId, patientId, action) {
        Acore.Modal({
            Name: "Edit Episode",
            Url: "Schedule/EditEpisode",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: function() { Schedule.InitEpisode(action); },
            Width: "800px",
            Height: "585px",
            WindowFrame: false
        })
    },
    ShowAdmitPatientModal: function(id, type) {
        Acore.Modal({
            Name: "Admit Patient",
            Url: "Patient/NewAdmit",
            Input: { id: id, type: type },
            OnLoad: Patient.InitAdmit,
            Width: "850px",
            Height: "345px",
            WindowFrame: false
        })
    },
    ShowNonAdmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Patient",
            Url: "Patient/NewNonAdmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitNonAdmit,
            Width: "800px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowNonAdmitReferralModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Referral",
            Url: "Referral/NewNonAdmit",
            Input: { referralId: Id },
            OnLoad: Referral.InitNonAdmit,
            Width: "800px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowMedicationModal: function(patientId, isnew) {
        Acore.Modal({
            Name: "Medications",
            Url: "485/Medication",
            Input: { patientId: patientId },
            OnLoad: function() {
                $("#PlanofCareMedicationIsNew").val(isnew);
                $("window_ModalWindow_content").css("overflow", "hidden");
                $('#MedicationGrid485 .t-grid-content').css("top", "60px")
            },
            Width: "800px",
            Height: "286px",
            WindowFrame: false
        })
    },
    ShowOasisValidationModal: function(Id, patientId, episodeId, assessmentType, showPpsPlus) {
        OasisValidation.Validate(Id, patientId, episodeId, assessmentType, showPpsPlus);
    },
    ShowModalEditClaim: function(patientId, Id, type) {
        Acore.Modal({
            Name: "Edit Claim",
            Url: "Billing/Update",
            Input: { patientId: patientId, id: Id, type: type },
            OnLoad: Billing.InitClaim,
            Width: "800px",
            Height: "270px",
            WindowFrame: false
        })
    },
    ShowNewClaimModal: function(type) {
        Acore.Modal({
            Name: "New Claim",
            Url: "Billing/NewClaim",
            Input: { patientId: Billing._patientId, type: type },
            OnLoad: Billing.InitNewClaim,
            Width: "500px",
            Height: "140px",
            WindowFrame: false
        })
    },
    ShowChangeStatusModal: function(Id) {
        Acore.Modal({
            Name: "Change Status",
            Url: "Patient/Status",
            Input: { patientId: Id },
            OnLoad: Patient.InitChangePatientStatus,
            Width: "800px",
            Height: "520px",
            WindowFrame: false
        })
    },
    ShowReadmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Patient Re-admit",
            Url: "Patient/Readmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitPatientReadmitted,
            Width: "800px",
            Height: "345px",
            WindowFrame: false
        })
    },
    ShowNewManagedClaimModal: function() {
        Acore.Modal({
            Name: "New Managed Claim",
            Url: "Billing/NewManagedClaim",
            Input: { patientId: ManagedBilling._patientId },
            OnLoad: ManagedBilling.InitNewClaim,
            Width: "500px",
            Height: "175px",
            WindowFrame: false
        })
    },
    ShowModalUpdateStatusManagedClaim: function(patientId, Id) {
        Acore.Modal({
            Name: "Update Status of Managed Claim",
            Url: "Billing/UpdateManagedClaim",
            Input: { patientId: patientId, id: Id },
            OnLoad: ManagedBilling.InitClaim,
            Width: "800px",
            Height: "250px",
            WindowFrame: false
        })
    },
    ShowNewManagedClaimPaymentModal: function() {
        Acore.Modal({
            Name: "Add Managed Claim Payment",
            Url: "Billing/NewManagedClaimPayment",
            Input: { id: ManagedBilling._ClaimId },
            OnLoad: ManagedBilling.InitNewPayment,
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowModalUpdatePaymentManagedClaim: function(Id) {
        Acore.Modal({
            Name: "Update Managed Claim Payment",
            Url: "Billing/UpdateManagedClaimPayment",
            Input: { id: Id },
            OnLoad: ManagedBilling.InitEditPayment,
            Width: "400px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowManagedClaimPayments: function(Id, patientId) {
        Acore.Open("managedclaimpayments", "Billing/ManagedClaimPayments", function() { }, { ClaimId: Id, patientId: patientId });
    },
    ShowNewManagedClaimAdjustmentModal: function() {
        Acore.Modal({
            Name: "Add Managed Claim Adjustment",
            Url: "Billing/NewManagedClaimAdjustment",
            Input: { id: ManagedBilling._ClaimId },
            OnLoad: ManagedBilling.InitNewAdjustment,
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowModalUpdateAdjustmentManagedClaim: function(Id) {
        Acore.Modal({
            Name: "Update Managed Claim Adjustment",
            Url: "Billing/UpdateManagedClaimAdjustment",
            Input: { id: Id },
            OnLoad: ManagedBilling.InitEditAdjustment,
            Width: "400px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowManagedClaimAdjustments: function(Id, patientId) {
        Acore.Open("managedclaimadjustments", "Billing/ManagedClaimAdjustments", function() { }, { ClaimId: Id, patientId: patientId });
    },
    ShowManagedClaim: function(Id, patientId) {
        Acore.Open("managedclaimedit", "Billing/ManagedClaim", function() { }, { Id: Id, patientId: patientId });
    },
    ShowMultipleReassignModal: function(episodeId, patientId, type) {
        Acore.Modal({
            Name: "Reasign Multiple Tasks",
            Url: "Schedule/ReAssignSchedulesContent",
            Input: { episodeId: episodeId, patientId: patientId, type: type },
            OnLoad: function() { Schedule.reassignScheduleInit(type) },
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowCorrectionNumberModal: function(Id, patientId, episodeId, type, correctionNumber) {
        Acore.Modal({
            Name: "Correction Number",
            Url: "Oasis/Correction",
            Input: { Id: Id, PatientId: patientId, EpisodeId: episodeId, Type: type, CorrectionNumber: correctionNumber },
            OnLoad: Oasis.InitEditCorrectionNumber,
            Width: "675px",
            Height: "245px",
            WindowFrame: false
        })
    },
    ShowRemittanceDetail: function(Id) {
        Acore.Open("remittancedetail", 'Billing/RemittanceDetail', function() { }, { Id: Id });
    },
    ShowOrdersHistoryModal: function(id, patientId, episodeId, type) {
        Acore.Modal({
            Name: "Orders History",
            Url: "Agency/OrderHistoryEdit",
            Input: { id: id, patientId: patientId, episodeId: episodeId, type: type },
            OnLoad: Agency.OrderHistoryEditInit,
            Width: "500px",
            Height: "200px",
            WindowFrame: false
        })
    },
    Refresh: function(patientId) {
        Patient.Charts.Activities.Refresh(patientId);
        Schedule.Center.RefreshSchedule(patientId);
    },
    CloseModal: function() {
        if (Acore.Windows.ModalWindow != null && Acore.Windows.ModalWindow.IsOpen) $("#window_ModalWindow").Close();
    },
    CloseAndRefresh: function(window, patientId) {
        UserInterface.Refresh(patientId);
        Agency.RebindCaseManagement();
        User.RebindScheduleList();
        $("#window_" + window).Close();
    },
    CloseWindow: function(window) {
        if (Acore.Windows[window] && Acore.Windows[window].IsOpen) $("#window_" + window).Close();
    },
    ShowNewBillData: function(insuranceId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/NewBillData",
            Input: { InsuranceId: insuranceId },
            OnLoad: Agency.InitBillDate,
            Width: "540px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowEditBillData: function(insuranceId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/EditBillData",
            Input: { InsuranceId: insuranceId, Id: Id },
            OnLoad: Agency.InitEditBillDate,
            Width: "540px",
            Height: "450px",
            WindowFrame: false
        })
    },
    ShowNewBillDataInBilling: function(claimId, typeOfClaim) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Billing/" + typeOfClaim + "ClaimNewBillData",
            Input: { ClaimId: claimId },
            OnLoad: typeOfClaim == "Managed" ? ManagedBilling.InitBillData : SecondaryBilling.InitBillData,
            Width: "540px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowEditBillDataInBilling: function(claimId, Id, typeOfClaim) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Billing/" + typeOfClaim + "ClaimEditBillData",
            Input: { ClaimId: claimId, Id: Id },
            OnLoad: typeOfClaim == "Managed" ? ManagedBilling.InitEditBillData : SecondaryBilling.InitEditBillData,
            Width: "500px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowNewLocationBillData: function(locationId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/NewLocationBillData",
            Input: { LocationId: locationId },
            OnLoad: Agency.InitLocationBillDate,
            Width: "500px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowEditLocationBillData: function(locationId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/EditLocationBillData",
            Input: { LocationId: locationId, Id: Id },
            OnLoad: Agency.InitLocationEditBillDate,
            Width: "500px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowClaimResponse: function(Id) {
        Acore.Open("billingclaimresponse", 'Billing/ClaimResponse', function() { }, { Id: Id });
    },
    ShowEditPatientAdmission: function(patientId, Id, type) {
        Acore.Open(type.toLowerCase() + "patientadmission", 'Patient/AdmissionPatientInfo', function() { Patient.InitAdmissionPatient(patientId, type); }, { patientId: patientId, Id: Id, Type: type });
    },
    ShowClaimRemittance: function(Id, type) {
        Acore.Open("claimremittances", 'Billing/ClaimRemittance', function() { }, { Id: Id, Type: type });
    },
    ShowFrequencies: function(Id, patientId) {
        Acore.Modal({
            Name: "Visit Frequencies and Count",
            Url: "/Schedule/Frequencies",
            Input: { episodeId: Id, patientId: patientId },
            Width: 600,
            Height: 290,
            WindowFrame: false
        });
    },
    ShowNewAdjustmentCode: function() {
        Acore.Open("newadjustmentcode", 'AdjustmentCode/New', function() { AdjustmentCode.InitNew(); });
    },
    ShowEditAdjustmentCode: function(id) {
        Acore.Open("editadjustmentcode", "AdjustmentCode/Edit", function() { AdjustmentCode.InitEdit(); }, { Id: id });
    },
    ShowNewUploadType: function() {
        Acore.Open("newuploadtype", "UploadType/New", function() { UploadType.InitNew(); });
    },
    ShowEditUploadType: function(id) {
        Acore.Open("edituploadtype", "UploadType/Edit", function() { UploadType.InitEdit(); }, { Id: id });
    },
    ShowMultipleDelete: function(episodeId, patientId) {
        Acore.Open("scheduledelete", 'Schedule/DeleteSchedules', function() { }, { episodeId: episodeId, patientId: patientId });
    },
    VisitLogEdit: function(episodeId, patientId) {
        Acore.Open("visitlog", { episodeId: episodeId, patientId: patientId });
    },
    ShowNewAgencyTeam: function() {
        Acore.Open("newagencyteam", "AgencyTeam/New", function() { AgencyTeam.InitNew(); });
    },
    ShowEditAgencyTeam: function(id) {
        Acore.Open("editagencyteam", "AgencyTeam/Edit", function() { AgencyTeam.InitEdit(); }, { Id: id });
    },
    ShowManagePatientsToTeam: function(id) {
        Acore.Open("managepatientstoteam", "AgencyTeam/ManagePatients", function() { AgencyTeam.LoadManagePatients(); }, { teamId: id });
    },
    ShowAddUsersToTeam: function(Id) {
        Acore.Modal({
            Name: "Add Clinicians",
            Url: "AgencyTeam/AddUsers",
            Input: { teamId: Id },
            OnLoad: AgencyTeam.LoadAddUsersToTeam,
            Width: "800px",
            Height: "610px",
            WindowFrame: true
        })
    },
    ShowAddPatientsToTeam: function(id) {
        Acore.Modal({
            Name: "Assign Patients",
            Url: "AgencyTeam/AddPatients",
            Input: { agencyTeamId: id },
            OnLoad: AgencyTeam.LoadAddPatientsToTeam,
            Width: "800px",
            Height: "610px",
            WindowFrame: true
        })
    },
    ShowAddUserAccess: function(id) {
        Acore.Modal({
            Name: "Add User Access",
            Url: "Patient/ShowAddUserAccess",
            Input: { patientId: id },
            OnLoad: Patient.LoadAddUserAccess,
            Width: "800px",
            Height: "610px",
            WindowFrame: true
        })
    },
    ShowAddPatientAccess: function(id) {
        Acore.Modal({
            Name: "Add Patient Access",
            Url: "User/ShowAddPatientAccess",
            Input: { userId: id },
            OnLoad: User.Sections.LoadAddPatientAccess,
            Width: "800px",
            Height: "610px",
            WindowFrame: true
        })
    },
    EditReturnComments: function(id, existing, element) {
        Acore.Modal({
            Name: "Edit Return Comment",
            Content: $("<div/>").addClass("main wrapper").append(
                $("<fieldset/>").append(
                    $("<legend/>").text("Edit Return Comment")).append(
                    $("<div/>").addClass("wide-column").append(
                        $("<div/>").addClass("row").append(
                            $("<textarea>").addClass("fill tall").val(existing))))).append(
                $("<div/>").Buttons([
                    {
                        Text: "Update",
                        Click: function() {
                            U.PostUrl("Schedule/EditReturnReason", { id: id, comment: $(this).closest(".main").find("textarea").val() }, function(Result) {
                                U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                if (Result.isSuccessful) {
                                    $(".return-comments-container").each(function() { $(this).ReturnComments("Refresh") });
                                    $("#window_ModalWindow").Close()
                                }
                            })
                        }
                    }, {
                        Text: "Cancel",
                        Click: function() { $("#window_ModalWindow").Close() }
                    }
                ])
            ),
            Width: 400,
            Height: 240,
            WindowFrame: false
        })
    },
    ShowModalBillingNewSupply: function(Id, patientId, Type) {
        Acore.Modal({
            Name: "New Supply",
            Url: "Billing/NewSupplyBillable",
            Input: { Id: Id, patientId: patientId, Type: Type },
            OnLoad: Supply.InitNewBillingSupply,
            Width: "750px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowModalBillingEditSupply: function(ClaimId, patientId, Id, Type, isBilling) {
        Acore.Modal({
            Name: "Edit Supply",
            Url: "Billing/EditSupplyBillable",
            Input: { ClaimId: ClaimId, patientId: patientId, Id: Id, Type: Type },
            OnLoad: isBilling ? Supply.InitEditBillingSupply : Supply.InitEditUnBillingSupply,
            Width: "750px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowNewSecondaryClaimModal: function(episodeId, patientId, primaryClaimId) {
        Acore.Modal({
            Name: "Create Secondary Claim",
            Url: "Billing/NewSecondaryClaim",
            Input: { episodeId: episodeId, patientId: patientId, primaryClaimId: primaryClaimId },
            OnLoad: Billing.InitNewSecondaryClaim,
            Width: "500px",
            Height: "175px",
            WindowFrame: false
        })
    },
    ShowSecondaryClaims: function(Id, patientId) {
        SecondaryBilling._patientId = patientId;
        Acore.Open("secondaryclaims", "Billing/SecondaryClaimCenter", function() { }, { PatientId: patientId, PrimaryClaimId: Id });
    },
    ShowSecondaryClaim: function(Id) {
        Acore.Open("secondaryclaimedit", "Billing/SecondaryClaim", function() { }, { Id: Id });
    },
    ShowModalEditClaimSnapShot: function(Id, batchId, type) {
        Acore.Modal({
            Name: "Edit Batch Claim",
            Url: "Billing/EditClaimSnapShot",
            Input: { id: Id, batchId: batchId, type: type },
            OnLoad: Billing.InitBatchClaim,
            Width: "400px",
            Height: "200px",
            WindowFrame: false
        })
    },
    ShowModalUpdateStatusSecondaryClaim: function(Id) {
        Acore.Modal({
            Name: "Update Status of Secondary Claim",
            Url: "Billing/UpdateSecondaryClaim",
            Input: { id: Id },
            OnLoad: SecondaryBilling.InitClaim,
            Width: "800px",
            Height: "260px",
            WindowFrame: false
        })
    },
    ShowVitalSignsCharts: function(patientId, startDate, endDate) {
        Acore.Open("patientvitalsignscharts", "Patient/VitalSignsCharts", function() { }, { PatientId: patientId, StartDate: startDate, EndDate: endDate });
    }
}
