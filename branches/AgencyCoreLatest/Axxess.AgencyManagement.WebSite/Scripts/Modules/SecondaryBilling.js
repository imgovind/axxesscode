﻿var SecondaryBilling = {
    _patientId: "",
    _ClaimId: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    SecondaryBilling.UnBlockClickAndEnable();
                    var tabstrip = $("#SecondaryClaimTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[index];
                    tabstrip.select(item);
                    var $link = $(item).find('.t-link');
                    var contentUrl = $link.data('ContentUrl');
                    if (contentUrl != null) {
                        SecondaryBilling.LoadContent(contentUrl, $("#SecondaryClaimTabStrip").find("div.t-content.t-state-active"));
                    }
                    SecondaryBilling.RebindActivity(SecondaryBilling._patientId);
                } else U.Growl(result.errorMessage, 'error');
            },
            error: function(jqXHR, textStatus, error){
                U.Growl("Unable to complete request, please try again", "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    secondaryClaimTabStripOnSelect: function(e) {
        var element = $(e.item);
        if (!element.hasClass("t-state-disabled")) {
            SecondaryBilling.LoadContent(element.find('.t-link').data('ContentUrl'), e.contentElement);
        }
        return false;
    },
    InitCenter: function() {
        $("#txtSearch_secondaryBilling_Selection").keyup(function() { $('#secondaryBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#SecondaryBillingActivityGrid').find('.t-grid-content tbody').empty(); SecondaryBilling._patientId = ""; SecondaryBilling.RebindPatientList(); });
        $("select.secondaryBillingBranchCode").change(function() { Insurance.loadInsuarnceDropDown('SecondaryBillingHistory', 'All', true, function() { $('#secondaryBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#SecondaryBillingActivityGrid').find('.t-grid-content tbody').empty(); SecondaryBilling._patientId = ""; SecondaryBilling.loadInsuranceForActivity('SecondaryBillingHistory', 'All', true, function() { SecondaryBilling.RebindPatientList(); }); }); });
        $("select.secondaryBillingStatusDropDown").change(function() { $('#secondaryBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#SecondaryBillingActivityGrid').find('.t-grid-content tbody').empty(); SecondaryBilling._patientId = ""; SecondaryBilling.RebindPatientList(); });
        $("select.secondaryBillingInsuranceDropDown").change(function() { $('#secondaryBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#SecondaryBillingActivityGrid').find('.t-grid-content tbody').empty(); SecondaryBilling._patientId = ""; SecondaryBilling.RebindPatientList(); });
    },
    ReLoadUnProcessedSecondaryClaim: function(branchId, insuranceId) {
        $("#Billing_SecondaryClaimCenterContent ol").addClass('loading');
        $("#Billing_SecondaryClaimCenterContent").load('Billing/SecondaryGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#Billing_SecondaryClaimCenterContent ol").removeClass("loading");
            if (textStatus == 'error') $('#Billing_SecondaryClaimCenterContent').html(U.AjaxError);
        });
    },
    SubmitClaimDirectly: function(control) {
        U.PostUrl('Billing/SubmitSecondaryClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('secondaryclaimsummary');
                SecondaryBilling.ReLoadUnProcessedSecondaryClaim($('#Billing_SecondaryClaimCenterBranchCode').val(), $('#Billing_SecondaryClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    LoadGeneratedSecondaryClaim: function(control) {
        U.PostUrl("/Billing/CreateSecondaryANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
            else U.Growl(result.errorMessage, "error");
        }, function(result) { });
    },
    LoadGeneratedSingleSecondaryClaim: function(id) {
        U.PostUrl("/Billing/CreateSingleSecondaryANSI", { Id: id }, function(result) {
            if (result != null) {
                if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                else U.Growl(result.errorMessage, "error");
            }
        }, function(result) { });
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/SubmitSecondaryClaims', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('secondaryclaimsummary');
                SecondaryBilling.ReLoadUnProcessedSecondaryClaim($('#Billing_SecondaryClaimCenterBranchCode').val(), $('#Billing_SecondaryClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    RebindPatientList: function() {
        var patientGrid = $('#SecondaryBillingSelectionGrid').data('tGrid');
        if (patientGrid != null) patientGrid.rebind({
            branchId: $("select.secondaryBillingBranchCode").val(),
            statusId: $("select.secondaryBillingStatusDropDown").val(),
            insurnace: $("select.secondaryBillingInsuranceDropDown").val(),
            name: $("#txtSearch_secondaryBilling_Selection").val()
        });
    },
    HideOrShowNewClaimMenu: function() {
        var gridTr = $('#SecondaryBillingSelectionGrid').find('.t-grid-content tbody tr');
        if (gridTr != undefined && gridTr != null) {
            if (gridTr.length > 0) $("#secondaryBillingTopMenu").show();
            else $("#secondaryBillingTopMenu").hide();
        }
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl)
            $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function() {
                $(contentElement).removeClass("loading");

            });
    },
    Navigate: function(index, id, patientId) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            SecondaryBilling.UnBlockClickAndEnable();
                            var tabstrip = $("#SecondaryClaimTabStrip").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                            var $link = $(item).find('.t-link');
                            var contentUrl = $link.data('ContentUrl');
                            if (contentUrl != null) {
                                SecondaryBilling.LoadContent(contentUrl, $("#SecondaryClaimTabStrip").find("div.t-content.t-state-active"));
                            }
                            SecondaryBilling.RebindActivity(patientId);
                            SecondaryBilling.ReLoadUnProcessedSecondaryClaim($("#Billing_SecondaryClaimCenterBranchCode").val(), $("#Billing_SecondaryClaimCenterPrimaryInsurance").val());
                        } else U.Growl(result.errorMessage, 'error');
                    },
                    error: function(jqXHR, textStatus, error){
                        U.Growl("Unable to complete request, please try again", "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    NavigateBack: function(index) {
        var tabstrip = $("#SecondaryClaimTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    loadGenerate: function(control) {
        Acore.Open("secondaryclaimsummary", 'Billing/SecondaryClaimSummary', function() { }, $(":input", $(control)).serializeArray());
    },
    GenerateAllCompleted: function(control) {
        $("input[name=SecondaryClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true);
        });
        SecondaryBilling.loadGenerate(control);
    },
    NoPatientBind: function(id) { SecondaryBilling.RebindActivity(id); },
    PatientListDataBound: function() {
        if ($("#SecondaryBillingSelectionGrid .t-grid-content tr").length) {
            if (SecondaryBilling._patientId == "") $('#SecondaryBillingSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + SecondaryBilling._patientId + ')', $('#SecondaryBillingSelectionGrid')).closest('tr').click();
            $("#secondaryBillingTopMenu").show();
        } else {
            $("#secondaryBillingClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
            $("#secondaryBillingTopMenu").hide();
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            SecondaryBilling._patientId = patientId;
            SecondaryBilling._ClaimId = "";
            SecondaryBilling.RebindActivity(patientId);
        }
        $("#secondaryBillingClaimData").empty();
    },
    RebindActivity: function(id) {
        var activityGrid = $('#SecondaryClaimsGrid').data('tGrid');
        if (activityGrid != null) {
            activityGrid.rebind();
            if ($('#SecondaryClaimsGrid').find('.t-grid-content tbody').is(':empty')) $("#SecondaryClaimsGrid").empty();
        }
    },

    SecondaryClaimComplete: function(id, patientId) {
        U.PostUrl('Billing/SecondaryClaimComplete', { id: id, patientId: patientId, total: $("#Claim_Total").val() }, function(result) {
            if (result.isSuccessful) {
                SecondaryBilling.RebindActivity();
                UserInterface.CloseWindow('secondaryclaimedit');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    InitClaim: function() {
        $("#updateSecondaryClaimForm #Status").change(function(e) {
            var selectValue = $(this).val();
            if (selectValue == "305" || selectValue == 3005) {
                var now = new Date();
                $("#updateSecondaryClaimForm #ClaimDate").val(U.FormatDate(now));
            }
        });
        U.InitTemplate($("#updateSecondaryClaimForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.RebindActivity();
        }, "Claim was updated successfully.");
    },
    InitNewPayment: function() {
        U.InitTemplate($("#newSecondaryClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.Rebind();
        }, "Payment was created successfully.");
    },
    InitEditPayment: function() {
        U.InitTemplate($("#updateSecondaryClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.Rebind();
        }, "Payment was updated successfully.");
    },
    InitNewAdjustment: function() {
        U.InitTemplate($("#newSecondaryClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.Rebind();
        }, "Payment was created successfully.");
    },
    InitEditAdjustment: function() {
        U.InitTemplate($("#updateSecondaryClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.Rebind();
        }, "Payment was updated successfully.");
    },
    InitNewClaim: function() {
        U.InitTemplate($("#createSecondaryClaimForm"), function() {
            UserInterface.CloseModal();
            SecondaryBilling.RebindActivity(SecondaryBilling._patientId);
            SecondaryBilling.Rebind();
        }, "Claim is created successfully.");
    },
    DeleteClaim: function(patientId, id) {
        if (confirm("Are you sure you want to delete this claim?")) U.PostUrl("/Billing/DeleteSecondaryClaim", { patientId: patientId, id: id }, function(result) {
            if (result.isSuccessful) {
                SecondaryBilling.RebindActivity(patientId);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    OnClaimRowSelected: function(e) {
        if (e.row.cells[0] != undefined && SecondaryBilling._patientId != undefined && SecondaryBilling._patientId != "") {
            var claimId = e.row.cells[0].innerHTML;
            SecondaryBilling._ClaimId = claimId;
            SecondaryBilling.loadClaimInfo(SecondaryBilling._patientId, claimId);
        }
    },
    OnCliamDataBound: function() {
        if ($("#SecondaryClaimsGrid .t-grid-content tr").length) $('#SecondaryClaimsGrid .t-grid-content tbody tr').eq(0).click();
        else $("#secondaryBillingClaimData").empty();
    },
    loadClaimInfo: function(patientId, claimId) {
        $("#secondaryBillingClaimData").empty().addClass("loading").load('Billing/SecondarySnapShotClaimInfo', { patientId: patientId, claimId: claimId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#secondaryBillingClaimData").removeClass("loading");
            if (textStatus == 'error') $('#secondaryBillingClaimData').html(U.AjaxError);
        });
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
                U.PostUrl("Billing/ChangeSupplyBillStatus", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#SecondaryBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#SecondaryUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    }
                });
            }
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else {
            if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
                if (inputs != null) {
                    inputs[inputs.length] = { name: "Id", value: Id };
                    inputs[inputs.length] = { name: "PatientId", value: PatientId };
                    U.PostUrl("Billing/SecondaryCareSuppliesDelete", inputs, function(result) {
                        if (result != null && result.isSuccessful) {
                            U.RebindTGrid($("#SecondaryBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                            U.RebindTGrid($("#SecondaryUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        }
                    });
                }
            }
        }
    },
    loadInsuranceForActivity: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/InsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() }, function(data) {
            var s = $("select#" + reportName + "_PrimaryInsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[s.get(0).options.length] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false); });
            s.get(0).options[s.get(0).options.length] = new Option("No Insurance assigned", "-1", false, false);
            if (action != null && action != undefined && typeof (action) == "function") action();
        });
    },
    InitBillData: function() { U.InitTemplate($("#newInsuranceBillData"), function() { SecondaryBilling.RebindSecondaryBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully saved"); },
    InitEditBillData: function() { U.InitTemplate($("#editInsuranceBillData"), function() { SecondaryBilling.RebindSecondaryBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully updated"); },
    RebindSecondaryBillDataList: function() { var grid = $("#SecondaryInsurance_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ ClaimId: $("#SecondaryInsurance_ClaimId").val() }); } },
    DeleteBillData: function(claimId, Id) {
        if (confirm("Are you sure you want to delete this visit rate?")) {
            U.PostUrl("/Billing/SecondaryClaimDeleteBillData", { ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    SecondaryBilling.RebindSecondaryBillDataList();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    ReloadInsuranceData: function(claimId) {
        if (confirm("Any edits you have made to the visit rates or locators will be lost. Are you sure you want to reload the insurance?")) {
            U.PostUrl("/Billing/SecondaryClaimReloadInsurance", { Id: claimId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    SecondaryBilling.LoadContent($("#SecondaryClaimTabStrip ul li.t-state-active .t-link").data('ContentUrl'), $("#SecondaryClaimTabStrip div.t-content.t-state-active"));
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    UnBlockClickAndEnable: function() {
        var element = $("#SecondaryClaimTabStrip ul li.t-state-active").next();
        element.removeClass("t-state-disabled");
        element.find(".t-link").unbind('click', U.BlockClick);
    },
    DeletePayment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this payment?")) {
            U.PostUrl("/Billing/DeleteSecondaryClaimPayment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    SecondaryBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    Rebind: function() {
        U.RebindTGrid($('#SecondaryBillingPaymentsGrid'));
        U.RebindTGrid($('#SecondaryBillingAdjustmentsGrid'));
        SecondaryBilling.RebindActivity(SecondaryBilling._patientId);
    },
    ToolTip: function(Id) {
        $("a.tooltip", $(Id)).each(function() {
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ""); });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    DeleteAdjustment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this adjustment?")) {
            U.PostUrl("/Billing/DeleteSecondaryClaimAdjustment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    SecondaryBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    OnRowWithCommentsDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")); });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    OnAdjustmentsDataBound: function(e) {
        U.ToolTip($("#SecondaryBillingAdjustmentsGrid .t-grid-content tr"), "");
    },
    InitVisits: function() {
        $("#secondaryBillingVisitForm .day-per-line").each(function(index, Element) {
            var checkbox = $(Element);

            checkbox.closest(".main-line-item").find("ol input").each(function(index2, actualVisitCheckbox) {
                var jActualVisitCheckbox = $(actualVisitCheckbox);
                jActualVisitCheckbox.change(function() {
                    if (jActualVisitCheckbox.prop("checked") == false) {
                        checkbox.prop('checked', false);
                    }
                    var secondarySection = jActualVisitCheckbox.closest(".main-line-item ol");
                    var totalCount = secondarySection.find("input").length;
                    var checkedCount = secondarySection.find("input:checked").length;
                    if (totalCount == checkedCount) {
                        checkbox.prop('checked', true);
                    }
                });
            });

            checkbox.change(function() {
                if (checkbox.prop("checked") == true) {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', true);
                    });
                } else {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', false);
                    });
                }
            });

        });
    },
    DeleteAdjustment: function(row) {
        var listItem = $(row);
        if (listItem.hasClass("last")) {
            var previous = listItem.prev("li");
            if (!previous.length) {
                listItem.parent().addClass("hide-list");
            }
            else {
                previous.addClass("last");
            }
            listItem.remove();
        }
        else {
            listItem.remove();
        }
    },
    InitRemittance: function() {
        $("li .required", $("#secondaryBillingRemittanceForm")).closest("input").after("<span class='required-red'>*</span>");
        $("#secondaryBillingRemittanceForm .adjustment-amount").live("input", function() {
            var sum = 0;
            $('#secondaryBillingRemittanceForm .adjustment-amount').each(function() {
                sum += Number($(this).val());
            });
            $("#Secondary_TotalAdjustmentAmount").text(U.FormatMoney(sum));
        });
        $("#secondaryBillingRemittanceForm .adjustment-item input").live("input", function() {
            var element = $(this);
            if (element.hasClass("required error") && element.val() != "") {
                element.removeClass("required error");
            }
        });
        $(".floatnum", "#secondaryBillingRemittanceForm").floatnum();
        $(".adjustment-add-button").click(function(e) {
            var index = $(this).attr("value");
            var list = $(this).parent().parent().find("ol");
            //            if (!list.length) {
            //                
            //                list = $(this).parent().parent().find("ol");
            //            }
            var numberOfRows = $(list).find("li").length;
            if (numberOfRows == 0) {
                list.removeClass("hide-list");
            }
            var lastAdjustment = $(list).find("li:last");
            lastAdjustment.removeClass("last");
            var hover = "onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\"";
            list.append('<li ' + hover + '  class="adjustment-item last ' + (numberOfRows % 2 != 0 ? "odd notready" : "even notready") + '"><div class="align-right">' +
                '<div class="rem-fourth-width" style="margin-left:3px;"><label style="margin-right:3px;" for="[' + index + '].Value[' + numberOfRows + '].AdjGroup" class="strong">Adjustment Group Code:</label><input id="[' + index + '].Value[' + numberOfRows + '].AdjGroup" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjGroup" class="rem-half-width required"/><span class="required-red">*</span></div>' +
                '<div class="rem-fourth-width" style="margin-left:3px;"><label style="margin-right:3px;" for="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" class="strong">Adjustment Reason Code:</label><input id="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" class="rem-half-width required"/><span class="required-red">*</span></div>' +
                '<div class="rem-fourth-width" style="margin-left:3px;"><label style="margin-right:3px;" for="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" class="strong">Adjustment Amount:</label>$<input id="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" class="rem-half-width floatnum adjustment-amount required"/><span class="required-red">*</span></div>' +
                '<div onclick="SecondaryBilling.DeleteAdjustment($(this).parent().parent());" class="remit-delete-icon t-icon t-delete"/></div></li>');
            $(".floatnum", $(this).parent().parent().find("ol li.last")).floatnum();
        });
    },
    SubmitVerifyRemittance: function() {
        var form = $("#secondaryBillingRemittanceForm");

        var adjustments = form.find(".adjustment-item");
        var success = true;
        adjustments.each(function(index) {
            var filledInputs = new Array();
            var notFilledInputs = new Array();
            $(this).find("input[type='text']").each(function(index) {
                if ($(this).val() != "") {
                    filledInputs.push(this);
                } else {
                    notFilledInputs.push(this);
                }
            });
            if (notFilledInputs.length > 0 && filledInputs.length > 0) {
                notFilledInputs.forEach(function() {
                    $(this).addClass("required error");
                });
                success = false;
            }
        });
        if (success) {
            form.submit();
        }
    }
};