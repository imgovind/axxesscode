﻿$.extend(Schedule, {
    PrivateDuty: {
        Center: {
            AcoreId: "PrivateDutyScheduleCenter",   // Acore Window ID for Private Duty Schedule Center
            PatientId: "",                          // Current Active Patient's GUID
            PatientName: "",                        // Current Active Patient's Name
            // Initialize Private Duty Schedule Center
            Init: function(r, t, x, e) {
                // Set layout for patient selector
                $(".layout", e).layout({
                    west: {
                        paneSelector: ".ui-layout-west",
                        size: 200,
                        minSize: 160,
                        maxSize: 400,
                        livePaneResizing: true,
                        spacing_open: 3
                    }
                });
                // Set new patient button
                $(".new-patient", e).click(function() {
                    Patient.New();
                    return false;
                });
                // Set patient selector filters
                $(".ui-layout-west .top input", e).keyup(function() { Schedule.PrivateDuty.Center.PatientSelector.Filter(e) });
                $(".ui-layout-west .top select", e).change(function() { Schedule.PrivateDuty.Center.PatientSelector.Rebind(e) });
            },
            // External Loader
            Load: function(patientId, currentDate) {
                if (Acore.Windows[Schedule.PD.Center.AcoreId].IsOpen) {
                    Schedule.PrivateDuty.Center.LoadContent(patientId, episodeId);
                    $("#window_" + Schedule.PrivateDuty.Center.AcoreId).WinFocus();
                } else {
                    Schedule.PrivateDuty.Center.PatientId = patientId, Schedule.PrivateDuty.Center.CurrentDate = currentDate;
                    Acore.Open(Schedule.PrivateDuty.Center.AcoreId);
                }
            },
            // Patient Selection Loader
            LoadContent: function(patientId) {
                var e = $("#window_" + Schedule.PrivateDuty.Center.AcoreId + "_content");
                if (U.IsGuid(patientId)) {
                    var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
                    row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, 500);
                    Schedule.PrivateDuty.Center.PatientName = row.find("td:eq(2)").text();
                    Schedule.PrivateDuty.Center.PatientId = patientId;
                    Schedule.PrivateDuty.Center.Refresh();
                }
            },
            PatientSelector: {
                // Initialize Patient Selector
                Init: function() {
                    var e = $("#window_" + Schedule.PrivateDuty.Center.AcoreId + "_content");
                    $(".ui-layout-west .t-grid-content", e).css("height", "auto");
                    Schedule.PrivateDuty.Center.PatientSelector.Filter(e);
                    if (U.IsGuid(Schedule.PrivateDuty.Center.PatientId) && $(".ui-layout-west .t-last:contains(" + Schedule.PrivateDuty.Center.PatientId + ")", e).length) Schedule.PrivateDuty.Center.LoadContent(Schedule.PrivateDuty.Center.PatientId);
                    else if ($(".ui-layout-west .t-last", e).length) Schedule.PrivateDuty.Center.LoadContent($(".ui-layout-west .t-last:first", e).text());
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
                },
                // Text Filter Functionality
                Filter: function(e) {
                    var text = $(".ui-layout-west .top input", e).val();
                    if (text && text.length) {
                        search = text.split(" ");
                        $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                        for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                            if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                        });
                        $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                        if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                        else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                    } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
                },
                // Selecting Patient from Patient Selector
                Select: function(e) {
                    Schedule.PrivateDuty.Center.LoadContent($(".t-last", e.row).text());
                }
            },
            // Refresh Calendar Data
            Refresh: function() {
                var e = $("#window_" + Schedule.PrivateDuty.Center.AcoreId + "_content");
                if ($(".fc", e).length) $(".fc", e).PrivateDutyScheduler("refresh");
                else $(".pd-calendar", e).PrivateDutyScheduler();
            }
        },
        Event: {
            // Delete Private Duty Event
            Delete: function(id) {
                if (confirm("Are you sure you want to delete this event?")) U.PostUrl("Schedule/PrivateDuty/Event/Delete", { id: id, patientId: Schedule.PrivateDuty.Center.PatientId }, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) Schedule.PrivateDuty.Center.Refresh();
                })
            },
            // Open Edit Private Event Window
            Edit: function(id) {
                Acore.Modal({
                    Name: "Task Editor",
                    Url: "Schedule/PrivateDuty/Event/Edit",
                    Input: { id: id, patientId: Schedule.PrivateDuty.Center.PatientId },
                    OnLoad: Schedule.PrivateDuty.Event.InitEdit,
                    Width: 575,
                    WindowFrame: false,
                    Height: 375
                })
            },
            // Initialize Edit Private Event Window
            InitEdit: function(r, t, x, e) {
                U.HideIfChecked($("#EditPrivateDutyEvent_AllDay"), $("#EditPrivateDutyEvent_End"));
            },
            // Initialize New Private Event Window
            InitNew: function(r, t, x, e) {
                U.HideIfChecked($("#NewPrivateDutyEvent_AllDay"), $("#NewPrivateDutyEvent_End"));
            },
            // Open New Private Event Window
            New: function() {
                Acore.Modal({
                    Name: "Employee Scheduler",
                    Url: "Schedule/PrivateDuty/Event/New",
                    Input: { patientId: Schedule.PrivateDuty.Center.PatientId },
                    OnLoad: Schedule.PrivateDuty.Event.InitNew,
                    Width: 575,
                    WindowFrame: false,
                    Height: 375
                })
            }
        }
    }
});