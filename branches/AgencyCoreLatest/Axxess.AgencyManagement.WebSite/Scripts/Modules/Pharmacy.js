﻿var Pharmacy = {
    Delete: function(Id) { U.DeleteTemplate("Pharmacy", Id); },
    InitEdit: function() {
        U.InitEditTemplate("Pharmacy");
        U.PhoneAutoTab("Edit_Pharmacy_PhoneArray");
        U.PhoneAutoTab("Edit_Pharmacy_FaxNumberArray");
    },
    InitNew: function() {
        U.InitNewTemplate("Pharmacy");
        U.PhoneAutoTab("New_Pharmacy_PhoneArray");
        U.PhoneAutoTab("New_Pharmacy_FaxNumberArray");
    },
    RebindList: function() { U.RebindTGrid($('#List_AgencyPharmacy')); }
}