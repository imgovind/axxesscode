﻿var ManagedBilling = {
    _patientId: "",
    _ClaimId: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    ManagedBilling.UnBlockClickAndEnable();
                    var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[index];
                    tabstrip.select(item);
                    var $link = $(item).find('.t-link');
                    var contentUrl = $link.data('ContentUrl');
                    if (contentUrl != null) {
                        ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active"));
                    }
                    ManagedBilling.RebindActivity(ManagedBilling._patientId);
                } else U.Growl(result.errorMessage, 'error');
            },
            error: function(jqXHR, textStatus, error) {
                U.Growl("Unable to complete request, please try again", "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    managedClaimTabStripOnSelect: function(e) {
        var element = $(e.item);
        if (!element.hasClass("t-state-disabled")) {
            ManagedBilling.LoadContent(element.find('.t-link').data('ContentUrl'), e.contentElement);
        }
        return false;
    },
    InitCenter: function() {
        $("#txtSearch_managedBilling_Selection").keyup(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
        $("select.managedBillingBranchCode").change(function() { Insurance.loadInsuarnceDropDown('ManagedBillingHistory', 'All', true, function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.loadInsuranceForActivity('ManagedBillingHistory', 'All', true, function() { ManagedBilling.RebindPatientList(); }); }); });
        $("select.managedBillingStatusDropDown").change(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
        $("select.managedBillingInsuranceDropDown").change(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
    },
    ReLoadUnProcessedManagedClaim: function() {
        var content = $("#Billing_ManagedClaimCenterContent");
        if (content.length) {
            $("ol", content).addClass('loading');
            content.load('Billing/ManagedGrid', { branchId: $('#Billing_ManagedClaimCenterBranchCode').val(), insuranceId: $('#Billing_ManagedClaimCenterPrimaryInsurance').val(), startDate: $('#Billing_ManagedClaimCenterStartDate').val(), endDate: $('#Billing_ManagedClaimCenterEndDate').val() },
            function(responseText, textStatus, XMLHttpRequest) {
                $("ol", content).removeClass("loading");
                if (textStatus == 'error') content.html(U.AjaxError);
            });
        }
    },
    SubmitClaimDirectly: function(control) {
        U.PostUrl('Billing/SubmitManagedClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim();
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    LoadGeneratedManagedClaim: function(control) {
        U.PostUrl("/Billing/CreateManagedANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
            else U.Growl(result.errorMessage, "error");
        }, function(result) { });
    },
    LoadGeneratedSingleManagedClaim: function(id) {
        U.PostUrl("/Billing/CreateSingleManagedANSI", { Id: id }, function(result) {
            if (result != null) {
                if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                else U.Growl(result.errorMessage, "error");
            }
        }, function(result) { });
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/SubmitManagedClaims', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim();
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    OnPatientListLoad: function(e) {
        var grid = $(this).data("tGrid");
        grid.ajax.selectUrl = grid.ajax.selectUrl.replace("branchId=" + U.GuidEmpty, "branchId=" + $("#ManagedBillingHistory_BranchCode").val());
    },
    RebindPatientList: function() {
        var patientGrid = $('#ManagedBillingSelectionGrid').data('tGrid');
        if (patientGrid != null) patientGrid.rebind({
            branchId: $("select.managedBillingBranchCode").val(),
            statusId: $("select.managedBillingStatusDropDown").val(),
            insurnace: $("select.managedBillingInsuranceDropDown").val(),
            name: $("#txtSearch_managedBilling_Selection").val()
        });
    },
    HideOrShowNewClaimMenu: function() {
        var gridTr = $('#ManagedBillingSelectionGrid').find('.t-grid-content tbody tr');
        if (gridTr != undefined && gridTr != null) {
            if (gridTr.length > 0) $("#managedBillingTopMenu").show();
            else $("#managedBillingTopMenu").hide();
        }
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl)
            $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function() {
                $(contentElement).removeClass("loading");

            });
    },
    Navigate: function(index, id, patientId) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            ManagedBilling.UnBlockClickAndEnable();
                            var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                            var $link = $(item).find('.t-link');
                            var contentUrl = $link.data('ContentUrl');
                            if (contentUrl != null) {
                                ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active"));
                            }
                            ManagedBilling.RebindActivity(patientId);
                            ManagedBilling.ReLoadUnProcessedManagedClaim();
                        } else U.Growl(result.errorMessage, 'error');
                    },
                    error: function(jqXHR, textStatus, error) {
                        U.Growl("Unable to complete request, please try again", "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    NavigateBack: function(index) {
        var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    loadGenerate: function(control) {
        var a = $(":input", $(control)).serializeArray();
        Acore.Open("managedclaimsummary", 'Billing/ManagedClaimSummary', function() { }, $(":input", $(control)).serializeArray());

    },
    GenerateAllCompleted: function(control) {
        $("input[name=ManagedClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true);
        });
        ManagedBilling.loadGenerate(control);
    },
    Download: function(control) {
        if ($("input:checkbox:checked", $(control)).length) {
            U.PostUrl("Request/ClaimFormDownload", $("input:checkbox:checked,input[name=BranchId],input[name=PrimaryInsurance]", $(control)).serializeArray(), function(result) {
                U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            });
        } else { U.Growl("Select at least one managed claims to generate.", "error"); }
    },

    NoPatientBind: function(id) { ManagedBilling.RebindActivity(id); },
    PatientListDataBound: function() {
        if ($("#ManagedBillingSelectionGrid .t-grid-content tr").length) {
            if (ManagedBilling._patientId == "") $('#ManagedBillingSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + ManagedBilling._patientId + ')', $('#ManagedBillingSelectionGrid')).closest('tr').click();
            $("#managedBillingTopMenu").show();
        } else {
            $("#managedBillingClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
            $("#managedBillingTopMenu").hide();
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            ManagedBilling._patientId = patientId;
            ManagedBilling._ClaimId = "";
            ManagedBilling.RebindActivity(patientId);
        }
        $("#managedBillingClaimData").empty();
    },
    RebindActivity: function(id) {
        var activityGrid = $('#ManagedBillingActivityGrid').data('tGrid');
        if (activityGrid != null) {
            activityGrid.rebind({
                patientId: id,
                insuranceId: $("#ManagedBillingHistory_PrimaryInsuranceId").val()
            });
            if ($('#ManagedBillingActivityGrid').find('.t-grid-content tbody').is(':empty')) $("#managedBillingClaimData").empty();
        }
    },
    ManagedComplete: function(id, patientId) {
        U.PostUrl('Billing/ManagedComplete', { id: id, patientId: patientId, total: $("#Claim_Total").val() }, function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimedit');
                U.Growl(result.errorMessage, "success");
                ManagedBilling.Rebind();
            } else U.Growl(result.errorMessage, "error");
        }, null);
    },
    InitClaim: function() {
        U.InitTemplate($("#updateManagedClaimForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.RebindActivity(ManagedBilling._patientId);
        }, "Claim update is successfully.");
    },
    InitNewPayment: function() {
        U.InitTemplate($("#newManagedClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was created successfully.");
    },
    InitEditPayment: function() {
        U.InitTemplate($("#updateManagedClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was updated successfully.");
    },
    InitNewAdjustment: function() {
        U.InitTemplate($("#newManagedClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was created successfully.");
    },
    InitEditAdjustment: function() {
        U.InitTemplate($("#updateManagedClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was updated successfully.");
    },
    InitNewClaim: function() {
        U.InitTemplate($("#createManagedClaimForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.RebindActivity(ManagedBilling._patientId);
            ManagedBilling.Rebind();
        }, "Claim is created successfully.");
    },
    DeleteClaim: function(patientId, id) {
        if (confirm("Are you sure you want to delete this claim?")) U.PostUrl("/Billing/DeleteManagedClaim", { patientId: patientId, id: id }, function(result) {
            if (result.isSuccessful) {
                ManagedBilling.RebindActivity(patientId);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    OnClaimRowSelected: function(e) {
        if (e.row.cells[12] != undefined && ManagedBilling._patientId != undefined && ManagedBilling._patientId != "") {
            var claimId = e.row.cells[12].innerHTML;
            ManagedBilling._ClaimId = claimId;
            ManagedBilling.loadClaimInfo(ManagedBilling._patientId, claimId);
        }
    },
    OnCliamDataBound: function() {
        if ($("#ManagedBillingActivityGrid .t-grid-content tr").length) $('#ManagedBillingActivityGrid .t-grid-content tbody tr').eq(0).click();
        else $("#managedBillingClaimData").empty();
    },
    loadClaimInfo: function(patientId, claimId) {
        $("#managedBillingClaimData").empty().addClass("loading").load('Billing/ManagedSnapShotClaimInfo', { patientId: patientId, claimId: claimId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#managedBillingClaimData").removeClass("loading");
            if (textStatus == 'error') $('#managedBillingClaimData').html(U.AjaxError);
        });
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
                U.PostUrl("Billing/ChangeSupplyBillStatus", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    }
                });
            }
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else {
            if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
                if (inputs != null) {
                    inputs[inputs.length] = { name: "Id", value: Id };
                    inputs[inputs.length] = { name: "PatientId", value: PatientId };
                    U.PostUrl("Billing/ManagedCareSuppliesDelete", inputs, function(result) {
                        if (result != null && result.isSuccessful) {
                            U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                            U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        }
                    });
                }
            }
        }
    },
    loadInsuranceForActivity: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/InsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() }, function(data) {
            var s = $("select#" + reportName + "_PrimaryInsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[s.get(0).options.length] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false); });
            s.get(0).options[s.get(0).options.length] = new Option("No Insurance assigned", "-1", false, false);
            if (action != null && action != undefined && typeof (action) == "function") action();
        });
    },
    InitBillData: function() { U.InitTemplate($("#newInsuranceBillData"), function() { ManagedBilling.RebindManagedBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully saved"); },
    InitEditBillData: function() { U.InitTemplate($("#editInsuranceBillData"), function() { ManagedBilling.RebindManagedBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully updated"); },
    RebindManagedBillDataList: function() { var grid = $("#ManagedInsurance_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ ClaimId: $("#ManagedInsurance_ClaimId").val() }); } },
    DeleteBillData: function(claimId, Id) {
        if (confirm("Are you sure you want to delete this visit rate?")) {
            U.PostUrl("/Billing/ManagedClaimDeleteBillData", { ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.RebindManagedBillDataList();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    ReloadInsuranceData: function(claimId) {
        if (confirm("Any edits you have made to the visit rates or locators will be lost. Are you sure you want to reload the insurance?")) {
            U.PostUrl("/Billing/ManagedClaimReloadInsurance", { Id: claimId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.LoadContent($("#ManagedClaimTabStrip ul li.t-state-active .t-link").data('ContentUrl'), $("#ManagedClaimTabStrip div.t-content.t-state-active"));
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    UnBlockClickAndEnable: function() {
        var element = $("#ManagedClaimTabStrip ul li.t-state-active").next();
        element.removeClass("t-state-disabled");
        element.find(".t-link").unbind('click', U.BlockClick);
    },
    DeletePayment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this payment?")) {
            U.PostUrl("/Billing/DeleteManagedClaimPayment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    Rebind: function() {
        U.RebindTGrid($('#ManagedBillingPaymentsGrid'));
        U.RebindTGrid($('#ManagedBillingAdjustmentsGrid'));
        ManagedBilling.RebindActivity(ManagedBilling._patientId);
    },
    ToolTip: function(Id) {
        $("a.tooltip", $(Id)).each(function() {
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ""); });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    DeleteAdjustment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this adjustment?")) {
            U.PostUrl("/Billing/DeleteManagedClaimAdjustment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    OnRowWithCommentsDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")); });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    OnAdjustmentsDataBound: function(e) {
        U.ToolTip($("#ManagedBillingAdjustmentsGrid .t-grid-content tr"), "");
    },
    InitVisits: function() {
        $("#managedBillingVisitForm .day-per-line").each(function(index, Element) {
            var checkbox = $(Element);

            checkbox.closest(".main-line-item").find("ol input").each(function(index2, actualVisitCheckbox) {
                var jActualVisitCheckbox = $(actualVisitCheckbox);
                jActualVisitCheckbox.change(function() {
                    if (jActualVisitCheckbox.prop("checked") == false) {
                        checkbox.prop('checked', false);
                    }
                    var secondarySection = jActualVisitCheckbox.closest(".main-line-item ol");
                    var totalCount = secondarySection.find("input").length;
                    var checkedCount = secondarySection.find("input:checked").length;
                    if (totalCount == checkedCount) {
                        checkbox.prop('checked', true);
                    }
                });
            });

            checkbox.change(function() {
                if (checkbox.prop("checked") == true) {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', true);
                    });
                } else {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', false);
                    });
                }
            });

        });
    }
};