﻿if (typeof CareTeam == "undefined") var CareTeam = new Object();
$.extend(CareTeam, {
    Delete: function(id) {
        if (confirm("Are you sure you want to delete this team?")) {
            U.PostUrl("CareTeam/Delete", { Id: id }, function(result) {
                U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                if (result.isSuccessful) CareTeam.Refresh();
            })
        }
    },
    EditPatients: function(id) {
        Acore.Open("EditCareTeamPatients", { id: id })
    },
    EditUsers: function(id) {
        Acore.Open("EditCareTeamUsers", { id: id })
    },
    InitEditPatients: function(r, t, x, e) {
        $(".team-access-patients", e).MultiGrid({
            DataUri: "CareTeam/PatientData",
            MoveUri: "CareTeam/PatientMove",
            MoveCallback: CareTeam.Refresh,
            LeftGridTitle: "Team Has Access to These Patients",
            RightGridTitle: "Team Does Not Have Access to These Patients"
        })
    },
    InitEditUsers: function(r, t, x, e) {
        $(".team-access-users", e).MultiGrid({
            DataUri: "CareTeam/UserData",
            MoveUri: "CareTeam/UserMove",
            MoveCallback: CareTeam.Refresh,
            LeftGridTitle: "Members of this Team",
            RightGridTitle: "Not Members of This Team"
        })
    },
    InitList: function(r, t, x, e) {
        $(".teams-tooltip", e).each(function() {
            var html = $(this).html();
            $(this).parent().find(".grid-threequarters").tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: "calday",
                bodyHandler: function() { return "<strong>Users:</strong><br/>" + html }
            });
            $(this).remove();
        })
        $(".grid-search", e).AcoreGridSearch();
        $(".acore-grid ol", e).Zebra();
        $(".new-team", e).click(function() { CareTeam.New() });
        $(".grid-refresh", e).click(function() { CareTeam.Refresh() });
        $(".teams-manage-patients", e).click(function() { CareTeam.EditPatients($(this).attr("guid")) });
        $(".teams-manage-users", e).click(function() { CareTeam.EditUsers($(this).attr("guid")) });
        $(".teams-delete", e).click(function() { CareTeam.Delete($(this).attr("guid")) });
    },
    InitNew: function(r, t, x, e) {
        $(".acore-grid ol", e).Zebra().find("li").click(function() {
            $(this).toggleClass("selected").find("[type=checkbox]").prop("checked", $(this).hasClass("selected"))
        });
        $("form", e).Validate({ Success: function() { CareTeam.Refresh() } });
    },
    List: function() {
        Acore.Open("ListCareTeams")
    },
    New: function() {
        Acore.Modal({
            Name: "New Care Team",
            Width: 450,
            Height: 600,
            Url: "CareTeam/New",
            OnLoad: CareTeam.InitNew,
            WindowFrame: false
        })
    },
    Refresh: function() {
        $("#window_ListCareTeams_content").Load("CareTeam/List", CareTeam.InitList)
    }
});