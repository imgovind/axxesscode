﻿var GridProcessor = {
    ProcessMissedVisits: function(rows) {
        rows.each(function() {
            $(this.cells[3]).find("a").addClass("link");
            var actionCell = $(this.cells[11]),
                canDelete = actionCell.html() == 2,
                patientId = $(".pid", this).remove().html(),
                episodeId = $(".eid", this).remove().html(),
                eventId = $(".id", this).remove().html();
            actionCell.empty();
            if (canDelete) {
                actionCell.append($("<a>Delete</a>").attr("href", "javascript:void(0)").addClass("link")
                    .click(function() { Schedule.Delete(patientId, episodeId, eventId); }));
            }
            actionCell.append($("<a>Restore</a>").attr("href", "javascript:void(0)").addClass("link")
                .click(function() { Schedule.RestoreMissedVisit(episodeId, patientId, eventId); }));
            $("a.tooltip", this).each(function() {
                var c = "";
                if ($(this).hasClass("blue-note")) c = "blue-note";
                if ($(this).hasClass("red-note")) c = "red-note";
                if ($(this).html().length) {
                    $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                    $(this).tooltip({
                        track: true,
                        showURL: false,
                        top: 5,
                        left: -15,
                        extraClass: c,
                        bodyHandler: function() { return $(this).html() }
                    });
                } else $(this).hide();
            });
        });
    },
    ProcessActivityRow: function(row, activity) {
        $(row.cells[0]).find("a").addClass("link");
        $(row).data('discipline', activity.Discipline);
        if (activity.IsVisitVerified) $(row.cells[4]).html($("<a/>").attr("href", "javascript:void(0)")
                .click(function() { Acore.Open('verifiedvisit', { eventId: activity.EventId, patientId: activity.PatientId, episodeId: activity.EpisodeId }); }).append($("<span/>").addClass("verified")));
        else row.cells[4].innerHTML = "";
        if (activity.HasAttachments)
            $(row.cells[10]).html($("<a/>").attr("href", "javascript:void(0)")
                .click(function() { Schedule.GetTaskAttachments(activity.EpisodeId, activity.PatientId, activity.EventId); }).append($("<span/>").addClass("img icon paperclip")));
        else row.cells[10].innerHTML = "";
        var actionCell = $(row.cells[11]).empty();
        if (activity.AP) {
            if (activity.AP & 1) {
                actionCell.append($("<a>Details</a>").addClass("link")
                    .click(function() { Schedule.GetTaskDetails(activity.EpisodeId, activity.PatientId, activity.EventId); }));
            }
            if (activity.AP & 2) {
                actionCell.append($("<a>Delete</a>").addClass("link")
                    .click(function() { Schedule.Delete(activity.PatientId, activity.EpisodeId, activity.EventId); }));
            }
            if (activity.AP & 4) {
                actionCell.append($("<a>Reassign</a>").addClass("reassign link")
                    .click(function() { Schedule.ReAssign(activity.EpisodeId, activity.PatientId, activity.EventId); }));
            }
            if (activity.AP & 8) {
                actionCell.append($("<a>Reopen</a>").addClass("link")
                    .click(function() {
                        if (confirm("Are you certain you would like to reopen this task?")) {
                            Schedule.ReOpen(activity.EpisodeId, activity.PatientId, activity.EventId);
                            eval(activity.OnClick);
                        }
                }));
            }
        } else if (activity.IsMissed) {
            actionCell.html($("<a>Restore</a>").attr("href", "javascript:void(0)").addClass("link")
                .click(function() { Schedule.RestoreMissedVisit(activity.EpisodeId, activity.PatientId, activity.EventId); }));
        }
        $("a.tooltip", row).each(function() {
            var c = "";
            if ($(this).hasClass("blue-note")) c = "blue-note";
            if ($(this).hasClass("red-note")) c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() { return $(this).html() }
                });
            } else $(this).hide();
        });
        if (activity.IsComplete) {
            $(row).addClass("darkgreen");
            if (activity.StatusName == "Missed Visit(complete)") {
                $(row).removeClass("darkgreen");
                $(row).addClass('darkred');
            }
        }
        if (activity.IsOrphaned) {
            $(row).addClass("black").tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly." }
            });
            $(row.cells[1]).addClass("darkred");
        }
        $(row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (activity.AP & 8) Menu.append($("<li/>").text("Reopen Task").click(function() {
                $(row).find("a:contains('Reopen Task')").click();
            }));
            else if (!activity.IsOrphaned && !actionCell.IsComplete) Menu.append($("<li/>").text("Edit Note").click(function() {
                $(row).find("a:first").click();
            }));
            if (activity.AP & 1) Menu.append($("<li/>").text("Details").click(function() {
                $(row).find("a:contains('Details')").click();
            }));
            if (activity.AP & 2) Menu.append($("<li/>").text("Delete").click(function() {
                $(row).find("a:contains('Delete')").click();
            }));
            if (activity.PrintUrl) Menu.append($("<li/>").text("Print").click(function() {
                $(row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        })
    }
}