﻿var PlanOfCare = {
    InitNew: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#New_485_PhysicianId").PhysicianInput();
    },
    Submit: function(control, page) {
        var form = control.closest("form");
        if (form.valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) {

                },
                success: function(result) {
                    var patientId = $(form).find("[name=PatientId]").val();
                    if (result.isSuccessful) {
                        if (control.html() == "Save") U.Growl("The Plan of Care (485) has been saved successfully.", "success");
                        else if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page, patientId);
                            U.Growl("The Plan of Care (485) has been saved successfully.", "success");
                        } else if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page, patientId);
                            U.Growl("The Plan of Care (485) has been saved and completed successfully.", "success");
                        }
                    } else U.Growl(result.errorMessage, "error");
                },
                error: function(result) {
                    U.Growl(result.errorMessage, "error");
                }
            };
            $(form).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitEdit: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#Edit_485_PhysicianId").PhysicianInput();
        U.InitEditTemplate("485");
    },
    SaveMedications: function(patientId) {
        U.PostUrl("/Patient/LastestMedications", { patientId: patientId }, function(data) {
            if ($("#PlanofCareMedicationIsNew").val() == "true") $("#New_485_Medications").val(data);
            else $("#Edit_485_Medications").val(data);
            UserInterface.CloseModal();
        });
    },
    GetAlleries: function(patientId) {
        U.PostUrl("/Patient/LastestAllergies", { patientId: patientId }, function(data) {
            $("#485AllergiesDescription").val(data);
        });
    }
}