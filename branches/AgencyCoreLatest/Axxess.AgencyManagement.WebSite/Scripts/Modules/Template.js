﻿var Template = {
    Delete: function(id) { U.DeleteTemplate("Template", id); },
    InitEdit: function() {
        U.InitEditTemplate("Template");
    },
    InitNew: function() {
        U.InitNewTemplate("Template");
    },
    RebindList: function() { U.RebindTGrid($('#List_Template')); },
    OnChangeInit: function() {
        $("select.Templates").click(function() {
            var s = $(this);
            Template.LoadFormTemplates(s);
        }).change(function() {
            var selectList = this;
            var textarea = $(this).attr("template");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/Template/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    },
    LoadFormTemplates: function(listElement) {
        var contaningForm = $(listElement).closest("form");
        $("select.Templates", contaningForm).trigger("ProcessingStart");
        var callBack = function(currentSelect, form, options) {
            currentSelect.empty().append(options.join()).trigger("ProcessingComplete").unbind("click").val("-- Select Template --");
            var clonedOptions = $("option", currentSelect).clone();
            $("select.Templates", form).not(currentSelect).each(function() {
                $(this).empty().append(clonedOptions).trigger("ProcessingComplete").unbind("click");
                clonedOptions = $("option", this).clone();
            });
        };
        Template.DropDownList(listElement, contaningForm, callBack);
    },
    DropDownList: function(currentItem, form, callback) {
        U.PostUrl("/Agency/TemplateSelect", null, function(data) {
            var options = [];
            $.each(data, function(index, itemData) {
                var isBlankOption = itemData.Value == "" ? "selected" : "";
                options[index] = '<option value="' + itemData.Value + '"' + isBlankOption + '>' + itemData.Text + '</option>';
            });
            if (typeof callback == "function") callback(currentItem, form, options);
        }, function() {
            U.Error("Unable to load templates, please try again later");
            $("select.Templates").trigger("ProcessingComplete");
        });
    }
}