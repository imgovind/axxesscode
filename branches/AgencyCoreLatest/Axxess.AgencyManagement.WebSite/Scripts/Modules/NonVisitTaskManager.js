﻿var NonVisitTaskManager = {
    Delete: function(id) {
    U.DeleteTemplate("NonVisitTaskManager", id); 
    },
    InitEdit: function() {
    U.InitEditTemplate("NonVisitTaskManager");
    },
    InitNew: function() {
    U.InitNewTemplate("NonVisitTaskManager");
    },
    RebindList: function() {
    U.RebindTGrid($('#List_NonVisitTaskManager'));
    },
    Modal: {
    InitEdit: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { NonVisitTaskManager.RebindList()  } });
        },
        Edit: function(id, userId, insurance) {
            Acore.Modal({
                Name: "Edit User-Non-VisitTask",
                Url: "NonVisitTaskManager/Edit",
                Input: { Id: id },
                OnLoad: NonVisitTaskManager.Modal.InitEdit,
                Width: 420,
                Height: 380,
                WindowFrame: false
            })
        },
        Refresh: function() {
        $("#List_NonVisitTaskManager").Load("User/Rate/List", { UserId: $("#List_NonVisitTaskManager").attr("guid") })
        }
    },
    OnChangeInit: function() {
    $("select.NonVisitTaskManagers").change(function() {
            var selectList = this;
            var textarea = $(this).attr("nonvisittaskmanager");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/NonVisitTaskManager/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    }
}