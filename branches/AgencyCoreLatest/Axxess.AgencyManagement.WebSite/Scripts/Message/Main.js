﻿if (typeof Message == "undefined") var Message = new Object();
$.extend(Message, {
    Delete: function(id, messageType, inboxType) {
        if (confirm("Are you sure you want to delete this message?")) {
            U.TGridAjax("Message/Delete", { id: id, messageType: messageType, inboxType: inboxType }, $("#list-messages"));
            Message.LoadWidgetMessages();
        }
    },
    Move: function(id, messageType, folderId, folderName) {
        // Ensure the menus are all hidden
        $("ul.menu").hide();
        U.PostUrl("Message/Move", {
            id: id,
            messageType: messageType,
            folderId: folderId,
            folderName: folderName
        }, function(result) {
            U.Growl(result.errorMessage, "success");
            Message.RefreshInbox();
        }, function(result) {
            U.Growl(result.errorMessage, "error")
        })
    },
    New: function() {
        Acore.Open("NewMessage");
    },
    InitWidget: function() {
        Message.LoadWidgetMessages();
        $(".mail-controls select").change(function() {
            if (!$(this).find("option:first").prop("selected)")) {
                if ($("#messages-widget-content input:checked").length) {
                    if ($(this).find(":selected").html() == "Archive") { U.Growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Delete") {
                        if (confirm("Are you sure you want to delete all selected messages?")) {
                            $("#MessageWidget_Form").submit();
                        }
                    }
                    if ($(this).find(":selected").html() == "Mark as Read") { U.Growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Mark as Unread") { U.Growl("This feature coming soon."); }
                } else {
                    U.Growl("Error: No messages selected to " + $(this).find(":selected").html(), "error");
                    $(this).find("option:first").attr("selected", true);
                }
            }
        });
        $("#MessageWidget_Form").Validate({
            BeforeSubmit: function() {
            },
            Success: function() {
                $("#messages-widget-content input:checked").each(function() {
                    $(this).parent().remove();
                });
                $(".mail-controls select").val(0);
                Message.LoadWidgetMessages();
            }
        });
    },
    LoadWidgetMessages: function() {
        U.PostUrl("Message/GridWidget", {}, function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                $("#messages-widget-content").html("");
                for (var i = 0; i < data.Data.length; i++) {
                    var current = data.Data[i];
                    $("#messages-widget-content").append(
                        $("<div/>", { "class": current.MarkAsRead ? "" : "strong" }).append(
                            $("<input/>", { type: "checkbox", "class": "radio float-left", messagetype: current.Type, value: current.Id, name: "messageList" })).append(
                            $("<a/>", { href: "javascript:void(0)", "class": "message", html: current.FromName + " &#8211; " + current.Subject }).click(function() {
                                $(this).parent().removeClass("strong");
                                if (Acore.Windows.Inbox.IsOpen) {
                                    $("#window_Inbox").WinFocus();
                                    $("#" + $(this).prev().val()).closest("tr").click();
                                } else Message.Inbox($(this).prev().val());
                            }).mouseover(function() {
                                $(this).addClass("t-state-hover")
                            }).mouseout(function() {
                                $(this).removeClass("t-state-hover")
                            })
                        )
                    )
                }
            } else $("#messages-widget-content").append($("<h1/>", { "class": "align-center", text: "No Messages Found" })).parent().find(".widget-more").remove();
        });
    },
    OnDataBound: function(e) {
        var isAgencyFrozen = $("#Messages_IsAgencyFrozen").val();
        if (Message._MessageId != "" && $("#" + Message._MessageId).length) {
            $("#" + Message._MessageId).closest("tr").click();
        } else {
            $("#List_Messages .t-grid-content tr").eq(0).click();
            if ($("#List_Messages .t-grid-content tr").length == 0 && !isAgencyFrozen) {
                $("#messageInfoResult").html(
                    $("<div/>", { "class": "ajaxerror" }).append(
                        $("<h1/>", { "text": "No Messages found." })).append(
                        $("<div/>", { "class": "heading" }).Buttons([{
                            Text: "Add New Message",
                            Click: function() {
                                Message.New()
                            }
}])
                    )
                )
            }
        }
        Message._MessageId = "";
    }
});