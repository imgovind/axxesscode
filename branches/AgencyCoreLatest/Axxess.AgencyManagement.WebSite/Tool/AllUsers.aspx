﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<script runat="server">
    public void RefreshBtn_Click(Object sender, EventArgs e)
    {
        if (txtAgencyId.Text.IsNotNullOrEmpty() && txtAgencyId.Text.IsGuid())
        {
            UserEngine.Refresh(txtAgencyId.Text.ToGuid());
        }
    }
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess All Users Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
    <asp:TextBox ID="txtAgencyId" runat="server"></asp:TextBox>
        <% foreach(Axxess.Api.Contracts.UserData user in UserEngine.Users) { %>
            User Id: <%= user.Id %><br />
            User Name: <%= user.DisplayName %><br />
            User Agency: <%= AgencyEngine.Get(user.AgencyId).Name %><br />
            <br />
        <% } %>
        <asp:Button ID="refreshButton" runat="server" OnClick="RefreshBtn_Click" Text="Refresh" />
    <% } %>
</asp:Content>