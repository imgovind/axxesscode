﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<script runat="server" type="text/C#">
	public void GetCurrentSessionButton_Click(Object sender, EventArgs e)
	{
		txtCacheKey.Text = Context.User.Identity.Name;
	}

	public void GetSessionButton_Click(Object sender, EventArgs e)
	{
		if (txtCacheKey.Text.IsNotNullOrEmpty())
		{
			var principal = Cacher.Get<AxxessPrincipal>(txtCacheKey.Text);
			if (principal != null)
			{
				AxxessIdentity identity = (AxxessIdentity)principal.Identity;
				if (identity != null && identity.Session != null)
				{
					sessionData.Text = Newtonsoft.Json.JsonConvert.SerializeObject(identity.Session, Newtonsoft.Json.Formatting.Indented);
					sessionData.ForeColor = System.Drawing.Color.Green;
					return;
				}
				sessionData.Text = "Principle found, but identity failed.";
			}
			sessionData.Text = "Unable to find that user's session.";
			sessionData.ForeColor = System.Drawing.Color.Red;
		}
		else
		{
			sessionData.Text = "Please enter an user id";
			sessionData.ForeColor = System.Drawing.Color.Red;
		}
	}
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">
	Axxess Session</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
	<% if (!Current.IsIpAddressRestricted)
	{ %>
	<asp:TextBox ID="txtCacheKey" runat="server" Width="600"></asp:TextBox>
	<asp:Button ID="btnUse" runat="server" Text="Get Current User Id" OnClick="GetCurrentSessionButton_Click" />
	<asp:Button ID="btnRemove" runat="server" Text="Get User Session" OnClick="GetSessionButton_Click" />
	<br />
	<br />
	<asp:TextBox TextMode="multiline" Columns="75" Rows="25" ID="sessionData" runat="server"></asp:TextBox>
	<% } %>
</asp:Content>
