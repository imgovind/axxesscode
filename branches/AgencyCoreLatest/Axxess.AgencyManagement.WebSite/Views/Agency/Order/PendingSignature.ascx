﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders Pending Signature | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg" >
    <div class="float-right buttons">
        <ul>
            <li><%= Html.ActionLink("Export to Excel", "OrdersPendingSignature", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersPendingSignature_ExportLink", @class = "excel" })%></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
        
        <div class="float-right buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Agency.RebindPendingOrders();">Generate</a></li>
            </ul>
        </div>
        
        <label class="strong" for="OrdersPendingSignature_BranchId">Branch:
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersPendingSignature_BranchId" })%>
        </label>
        
        <div style="display:inline-block">
            <label class="strong">Date Range:
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersPendingSignature_StartDate" />
            </label>
            <label class="strong">To
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersPendingSignature_EndDate" />
            </label>
        </div>
            
        
        
        <div id="OrdersPendingSignature_Search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersPendingSignature").HtmlAttributes(new { @style = "top:70px;" }).DataKeys(keys => {
            keys.Add(o => o.Id).RouteKey("id");
            keys.Add(o => o.PatientId).RouteKey("patientId");
            keys.Add(o => o.EpisodeId).RouteKey("episodeId");
            keys.Add(o => o.Type).RouteKey("type");
            keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
            keys.Add(o => o.StartDate).RouteKey("startDate");
            keys.Add(o => o.EndDate).RouteKey("endDate");
            keys.Add(o => o.BranchId).RouteKey("branchId");
            keys.Add(o => o.PhysicianSignatureDate).RouteKey("physicianSignatureDate");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(200).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();          
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true).ReadOnly();
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.SentDate).Title("Sent Date").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.ReceivedDate).ClientTemplate("<#=''#>").Title("Received Date").Width(100).Sortable(true).HtmlAttributes(new { @class = "test" });
            columns.Bound(o => o.PhysicianSignatureDate).ClientTemplate("<#='' #>").Title("MD Sign Date").Width(100).Sortable(true);           
            columns.Command(commands => { commands.Edit(); }).Width(160).Title("Action").Visible(!Current.IsAgencyFrozen);
            columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false).ReadOnly();
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersPendingSignature", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }).Update("MarkOrderAsReturned", "Agency")).Editable(editing => editing.Mode(GridEditMode.InLine)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).ClientEvents(events => events.OnRowDataBound("Agency.OrderCenterOnload").OnEdit("Agency.PendingSignatureOrdersOnEdit")) %>

<script type="text/javascript">
    if (Acore.Mobile) $('#List_OrdersPendingSignature').css('top', 130);
    $("#OrdersPendingSignature_Search").append($("<div/>").GridSearchById("#List_OrdersPendingSignature"));
    $("#List_OrdersPendingSignature .t-grid-content").css({ 'height': 'auto', 'bottom': '23px' });
    $("#window_orderspendingsignature_content").css({
        "background-color": "#d6e5f3"
    });
    $('.grid-search').css({ 'position': 'relative', 'left': '0' });
</script>