﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserNonVisitTask>" %>
<span class="wintitle">New User Task | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Add", "NonVisitTaskManager", FormMethod.Post, new { @id = "newNonVisitTaskManagerForm" })) { %>
<div class="wrapper main">
    <fieldset>
    <legend> Add New User Tasks</legend>
    <table class="form">
        <colgroup>
            <col width="20%"/>
            <col width="15%"/>
            <col width="20%"/>
            <col width="12%"/>
            <col width="12%"/>
            <col width="20%"/>
        </colgroup>
        <tbody>
            <tr>
                <td><label class=""><strong>User:</strong></label></td>
                <td><label><strong>Task Date:</strong></label></td>
                <td><label class=""><strong>Task:</strong></label></td>
                <td class="width100"><label class="fill"><strong>Time In:</strong></label></td>
                <td class="width100"><label class="fill"><strong>Time Out:</strong></label></td>
                <td><label class=""><strong>Comments:</strong></label></td>
            </tr>
            <tr>
                <td><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "[0].UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = "Task_UserId_1", @class = "valid fill" })%></td>
                <td><input type="text" class="date-picker shortdate fill" name="[0].TaskDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Task_Date_1" /></td>
                <td><%= Html.AllNonVisitTaskList("[0].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "Task_Id_1", @class = "valid fill" })%></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeIn_1" name="[0].TimeIn" class="time-picker short" value="" /></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeOut_1" name="[0].TimeOut" class="time-picker short" value="" /></td>
                <td><input type="text" size="20" id="Task_Comments_1" name="[0].Comments" class="" /></td>
            </tr>
            <tr>
                <td><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "[1].UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = "Task_UserId_2", @class = "valid fill" })%></td>
                <td><input type="text" class="date-picker shortdate fill" name="[1].TaskDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Task_Date_2" /></td>
                <td><%= Html.AllNonVisitTaskList("[1].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "Task_Id_2", @class = "valid fill" })%></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeIn_2" name="[1].TimeIn" class="time-picker short" value="" /></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeOut_2" name="[1].TimeOut" class="time-picker short" value="" /></td>
                <td><input type="text" size="20" id="Task_Comments_2" name="[1].Comments" class="" /></td>
            </tr>
            <tr>
                <td><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "[2].UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = "Task_UserId_3", @class = "valid fill" })%></td>
                <td><input type="text" class="date-picker shortdate fill" name="[2].TaskDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Task_Date_3" /></td>
                <td><%= Html.AllNonVisitTaskList("[2].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "Task_Id_3", @class = "valid fill" })%></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeIn_3" name="[2].TimeIn" class="time-picker short" value="" /></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeOut_3" name="[2].TimeOut" class="time-picker short" value="" /></td>
                <td><input type="text" size="20" id="Task_Comments_3" name="[2].Comments" class="" /></td>
            </tr>
            <tr>
                <td><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "[3].UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = "Task_UserId_4", @class = "valid fill" })%></td>
                <td><input type="text" class="date-picker shortdate fill" name="[3].TaskDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Task_Date_4" /></td>
                <td><%= Html.AllNonVisitTaskList("[3].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "Task_Id_4", @class = "valid fill" })%></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeIn_4" name="[3].TimeIn" class="time-picker short" value="" /></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeOut_4" name="[3].TimeOut" class="time-picker short" value="" /></td>
                <td><input type="text" size="20" id="Task_Comments_4" name="[3].Comments" class="" /></td>
            </tr>
            <tr>
                <td><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "[4].UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = "Task_UserId_5", @class = "valid fill" })%></td>
                <td><input type="text" class="date-picker shortdate fill" name="[4].TaskDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Task_Date_5" /></td>
                <td><%= Html.AllNonVisitTaskList("[4].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "Task_Id_5", @class = "valid fill" })%></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeIn_5" name="[4].TimeIn" class="time-picker short" value="" /></td>
                <td class="width100"><input type="text" size="3" id="Task_TimeOut_5" name="[4].TimeOut" class="time-picker short" value="" /></td>
                <td><input type="text" size="20" id="Task_Comments_5" name="[4].Comments" class="" /></td>
            </tr>
        </tbody>
    </table>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a Class="save close">Save</a></li>
            <li><a Class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>