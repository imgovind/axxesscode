﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Pharmacies | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Pharmacies", "Export", FormMethod.Post))
   { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyPharmacy>().Name("List_AgencyPharmacy").ToolBar(commnds => commnds.Custom()).Columns(columns =>
{
    columns.Bound(c => c.Name).Title("Pharmacy Name").Width(150).Sortable(true);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(true);
    columns.Bound(c => c.AddressFull).Title("Address").Sortable(false);
    columns.Bound(c => c.PhoneFormatted).Title("Phone").Width(120).Sortable(false);
    columns.Bound(c => c.FaxFormatted).Title("Fax Number").Width(120).Sortable(false);
    columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
    columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPharmacy('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Pharmacy.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Pharmacy")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_AgencyPharmacy .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageHospital) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Pharmacy", Click: UserInterface.ShowNewPharmacy } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css({ height: "auto", top: 60 });
</script>
