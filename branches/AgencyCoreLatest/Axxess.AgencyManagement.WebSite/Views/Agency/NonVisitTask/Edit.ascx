﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyNonVisit>" %>
<span class="wintitle">Edit Task | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Update", "NonVisitTask", FormMethod.Post, new { @id = "editNonVisitTaskForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_NonVisitTask_Id" }) %>
<div class="wrapper main">
    
    <fieldset>
       <div class="wide-column">
         <div><label for="Edit_NonVisitTask_Title" class="strong">Name</label><br /><%= Html.TextBox("Title", Model.Title, new { @id = "Edit_NonVisitTask_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%><span class='required-red'>*</span></div><br />
         <div><label for="Edit_NonVisitTask_Text" class="strong">Text</label><br /><textarea id="Edit_NonVisitTask_Text" name="Text" cols="5" rows="6" style="height:200px" class="fill" maxcharacters="5000"><%= Model.Text %></textarea></div>
       </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadNonVisitTaskLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a Class = "save close">Save</a></li>
        <li><a Class = "close">Exit</a></li>
    </ul></div>
</div>
<% } %>
