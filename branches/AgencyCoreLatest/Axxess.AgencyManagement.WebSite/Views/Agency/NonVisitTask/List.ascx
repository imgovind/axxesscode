﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Tasks | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("NonVisitTasks", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencyNonVisit>()
        .Name("List_NonVisitTask")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.Title).Title("Name").Sortable(true);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditNonVisitTask('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"NonVisitTask.Delete('<#=Id#>');\" class=\"deleteTemplate\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "NonVisitTask"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_NonVisitTask .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Task", Click: UserInterface.ShowNewNonVisitTask } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css({ height: "auto", top: 60 });
</script>
<% } %>