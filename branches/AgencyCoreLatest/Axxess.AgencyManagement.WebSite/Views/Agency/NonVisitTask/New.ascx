﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Task | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Add", "NonVisitTask", FormMethod.Post, new { @id = "newNonVisitTaskForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div>
                <label for="New_NonVisitTask_Title" class="strong">Name</label><br />
                <%= Html.TextBox("Title", "", new { @id = "New_NonVisitTask_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%>
                <span class='required-red'>*</span>
            </div>
            <br />
            <div>
                <label for="New_NonVisitTask_Text" class="strong">Text</label><br />
                <textarea id="New_NonVisitTask_Text" name="Text" cols="5" rows="6" style="height:200px" class="fill" maxcharacters="5000"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a Class="save close">Save</a></li>
            <li><a Class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>
