﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<span class="wintitle">Quality Assurance (QA) Center | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "caseManagementForm", @style = "width: 100%; " })) { %>
<div class="buttons">      
    <ul class="float-right">
        
        <li><%= Html.ActionLink("Export to Excel", "QAScheduleList", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-7), EndDate = DateTime.Now }, new { id = "CaseManagement_ExportLink" })%></li>
    </ul>
<fieldset class="orders-filter">
    <%  var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]); %>
    
                <div class="button float-right">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="if($('#CaseManagement_StartDate').val() && $('#CaseManagement_EndDate').val()){Agency.LoadCaseManagement('<%=ViewData["GroupName"]%>','<%=sortParams %>')}else{U.Growl('StartDate and EndDate is required','error')};">Generate</a></li>
                    </ul>
                </div>
                <label class="float-left">Branch:</label>
                <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "CaseManagement_BranchCode" })%></div>
           
                <label class="float-left">Status:</label>
                <div class="float-left">
                <select id="CaseManagement_Status" name="StatusId">
                    <option value="0">All</option>
                    <option value="1" selected>Active</option>
                    <option value="2">Discharged</option>
                    <option value="4">Non-Admitted</option>
                </select></div> 
           <div class="clear" />
               <label class="float-left">Date Range:</label>
               <div class="float-left">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-7).ToShortDateString() %>" id="CaseManagement_StartDate" /> 
                </div>
                <label class="float-left">To</label> 
                <div class="float-left">
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="CaseManagement_EndDate" />
                </div>
            
           
                
    <div class="button">      
    
    <ul>
        <li><a href="javascript:void(0);" onclick="if($('#CaseManagement_StartDate').val() && $('#CaseManagement_EndDate').val()){Agency.LoadCaseManagement('PatientName','<%=sortParams %>')}else{U.Growl('StartDate and EndDate is required','error')};">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="if($('#CaseManagement_StartDate').val() && $('#CaseManagement_EndDate').val()){Agency.LoadCaseManagement('EventDate','<%=sortParams %>')}else{U.Growl('StartDate and EndDate is required','error')};">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="if($('#CaseManagement_StartDate').val() && $('#CaseManagement_EndDate').val()){Agency.LoadCaseManagement('DisciplineTaskName','<%=sortParams %>')}else{U.Growl('StartDate and EndDate is required','error')};">Group By Task</a></li>
        <li><a href="javascript:void(0);" onclick="if($('#CaseManagement_StartDate').val() && $('#CaseManagement_EndDate').val()){Agency.LoadCaseManagement('UserName','<%=sortParams %>')}else{U.Growl('StartDate and EndDate is required','error')};">Group By Clinician</a></li>
    </ul>
    </div>  
</fieldset>
</div>
<div id="caseManagementContentId"><% Html.RenderPartial("~/Views/Agency/QA/CaseManagementContent.ascx", Model); %></div>

<div class="buttons abs_bottom">
    <% if(!Current.IsAgencyFrozen) { %>
    <%= Html.Hidden("CommandType", "", new {@id="BulkUpdate_Type" })%>
    <ul>
        <li><a href="javascript:void(0);" onclick="BulkUpdate('Approve');">Approve Selected</a></li>
        <li><a href="javascript:void(0);" onclick="BulkUpdate('Return');">Return Selected</a></li>
    </ul>
    <% } %>
</div>
<%  } %>
<script type="text/javascript">
    $('#caseManagementContentId').css({ "top": (Acore.Mobile ? "120px" : "107px") });
    function BulkUpdate(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            $("#BulkUpdate_Type").val(type);
            Schedule.BulkUpdate('#caseManagementForm');
        } else U.Growl("Select at least one item to approve or return.", "error");
    }
</script>