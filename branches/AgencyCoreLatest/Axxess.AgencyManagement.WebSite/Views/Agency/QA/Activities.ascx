﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<% 
    Html.Telerik().Grid(Model).Name("QACenterActivityGrid").Columns(columns => {
        columns.Bound(s => s.EventDate).Title("Scheduled Date").Width(100);
        columns.Bound(s => s.EpisodeRange).Title("Episode").Width(170);
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").Title("Task");
        columns.Bound(s => s.Status).Width(200);
        columns.Bound(s => s.RedNote).Title(" ").Width(35).Template(s => s.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", s.RedNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.YellowNote).Title(" ").Width(35).Template(s => s.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.YellowNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.BlueNote).Title(" ").Width(35).Template(s => s.BlueNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.BlueNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
    }).Sortable(s => s.Enabled(false)).Scrollable().Footer(false).Render(); %>
   <%--Html.Telerik().Grid<ScheduleEvent>().Name("QACenterActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDateSortable).Title("Scheduled Date").ClientTemplate("<#=EventDateSortable#>").Width(105);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.OasisProfileUrl).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(30);
       columns.Bound(s => s.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
       columns.Bound(s => s.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
       columns.Bound(s => s.AttachmentUrl).Title(" ").ClientTemplate("<#=AttachmentUrl#>").Width(30);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200);
       columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Agency.ActivityRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("CaseManagementActivity", "Agency", new { patientId = val, discipline = "All", dateRangeId = "ThisEpisode", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now })).Sortable().Scrollable().Footer(false).Render();--%>
%>
