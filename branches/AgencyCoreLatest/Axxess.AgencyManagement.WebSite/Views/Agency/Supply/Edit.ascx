﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySupply>" %>
<span class="wintitle">Edit Supply | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Update", "Supply", FormMethod.Post, new { @id = "editSupplyForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Supply_Id" }) %>
<div class="wrapper main">
    
    <fieldset>
       <div class="wide-column">
         <div class="row"><label for="Edit_Supply_Description" class="strong">Description:</label><div class="float-right"><%= Html.TextBox("Description", Model.Description, new { @id = "Edit_Supply_Description", @class = "required", @maxlength = "350", @style = "width: 320px;" })%></div></div>
         <div class="row"><label for="Edit_Supply_HcpcsCode" class="strong">HCPCS:</label><div class="float-right"><%= Html.TextBox("Code", Model.Code, new { @id = "Edit_Supply_HcpcsCode", @class = "", @maxlength = "6", @style = "width: 70px;" })%></div></div>
         <div class="row"><label for="Edit_Supply_RevCode" class="strong">Revenue Code:</label><div class="float-right"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "Edit_Supply_RevCode", @class = "", @maxlength = "6", @style = "width: 70px;" })%></div></div>
         <div class="row"><label for="Edit_Supply_UnitCost" class="strong">Unit Cost:</label><div class="float-right"><%= Html.TextBox("UnitCost", Model.UnitCost, new { @id = "Edit_Supply_UnitCost", @class = "floatnum", @maxlength = "6", @style = "width: 70px;" })%></div></div>
       </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadSupplyLog('{0}');\">Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editsupply');">Exit</a></li>
    </ul></div>
</div>
<% } %>
