﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<div class="wrapper main">
<%  if (!Current.IsAgencyFrozen) { %>
    <div class="buttons">
        <ul class="fl">
            <li>
                <a class="new-license">Add Non-User License</a>
            </li>
        </ul>
        <ul class="fr">
            <li>
                <a class="grid-refresh">Refresh</a>
            </li>
        </ul>
    </div>
<%  } %>
    <div class="clear"></div>
    <div class="acore-grid"><% Html.RenderPartial("License/List", Model); %></div>
</div>