<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LicenseItem>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("License/Update", "Agency", FormMethod.Post, new { @id = "EditLicenseItem_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditLicenseItem_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditLicenseItem_UserId" })%>
    <fieldset>
        <legend>Edit License</legend>
        <div class="wide-column">
    <%  if (Model.IsUser == "No") { %>
            <div class="row">
                <label for="EditLicenseItem_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditLicenseItem_FirstName", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditLicenseItem_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditLicenseItem_LastName", @class = "required" })%></div>
            </div>
    <%  } else { %>
            <div class="row">
                <label class="fl strong">License User</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="EditLicenseItem_LicenseType" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", Model.LicenseType, new { @id = "EditLicenseItem_Type" })%></div>
                <div class="clr"></div>
                <div class="fr license-type-other">
                    <label for="EditLicenseItem_TypeOther"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", Model.OtherLicenseType, new { @id = "EditLicenseItem_TypeOther" }) %>
                </div>
            </div>
            <div class="row">
                <label for="EditLicenseItem_IssueDate" class="fl strong">Issue Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="IssueDate" value="<%= Model.IssueDate.IsValid() ? Model.IssueDateFormatted : string.Empty %>" id="EditLicenseItem_IssueDate" /></div>
            </div>
            <div class="row">
                <label for="EditLicenseItem_ExpireDate" class="fl strong">Expire Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpireDate" value="<%= Model.ExpireDate.IsValid() ? Model.ExpireDateFormatted : string.Empty %>" id="EditLicenseItem_ExpireDate" /></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>