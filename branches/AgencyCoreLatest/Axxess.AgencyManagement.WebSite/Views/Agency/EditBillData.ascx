﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateLocationBillData", "Agency", FormMethod.Post, new { @id = "editLocationBillData" }))
    { %>
    <%= Html.Hidden("LocationId", Model.LocationId, new { @id = "Edit_LocationBillData_LocationId" })%>
    <fieldset class="newmed">
        <legend>Edit Visit Information</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Edit_LocationBillData_Task" class="float-left">Task:</label>
                <div class="float-right"><%=Html.LocationCostDisciplineTask("Id", Model.Id, Model.LocationId, true, new { @id = "Edit_LocationBillData_Task", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_Description" class="float-left">Preferred Description:</label>
                <div class="float-right"><%= Html.TextBox("PreferredDescription", Model.PreferredDescription, new { @id = "Edit_LocationBillData_Description", @class = "text input_wrapper required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_RevenueCode" class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "Edit_LocationBillData_RevenueCode", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_HCPCS" class="float-left">HCPCS Code:</label>
                <div class="float-right"><%= Html.TextBox("Code", Model.Code, new { @id = "Edit_LocationBillData_HCPCS", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_ChargeRate" class="float-left">Rate:</label>
                <div class="float-right"><%= Html.TextBox("Charge", Model.Charge, new { @id = "Edit_LocationBillData_ChargeRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_Modifier" class="float-left">Modifier:</label>
                <div class="float-right">
                    <%= Html.TextBox("Modifier", Model.Modifier, new { @id = "Edit_LocationBillData_Modifier", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", Model.Modifier2, new { @id = "Edit_LocationBillData_Modifier2", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", Model.Modifier3, new { @id = "Edit_LocationBillData_Modifier3", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", Model.Modifier4, new { @id = "Edit_LocationBillData_Modifier4", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false'>Save &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
