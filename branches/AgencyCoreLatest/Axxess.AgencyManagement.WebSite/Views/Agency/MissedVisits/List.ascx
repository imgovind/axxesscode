﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Missed Visits | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg">
    <div class="buttons float-right">
        <ul>
            <li><%= Html.ActionLink("Export to Excel", "MissedVisits", "Export", new { branchId = Guid.Empty, startDate = DateTime.Now.AddDays(-89), endDate = DateTime.Now }, new { id = "MissedVisits_ExportLink" })%></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
        <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'PatientName','<%=sortParams %>');">Generate</a></li>
            </ul>
        </div>
        <label class="strong">
            Branch:
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = "MissedVisits_BranchCode", @class = "AddressBranchCode report_input valid" })%>
        </label>
        <div style="display:inline-block">
            <label class="strong">
                Date Range:
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-89).ToShortDateString() %>"
                    id="MissedVisits_StartDate" />
            </label>
            <label class="strong">
                To
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>"
                    id="MissedVisits_EndDate" />
            </label>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'PatientName','<%=sortParams %>');">
                    Group By Patient</a></li>
                <li><a href="javascript:void(0);" onclick="Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'EventDate','<%=sortParams %>');">
                    Group By Scheduled Date</a></li>
                <li><a href="javascript:void(0);" onclick="Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'UserName','<%=sortParams %>');">
                    Group By Employee</a></li>
                <li><a href="javascript:void(0);" onclick="Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'DisciplineTaskName','<%=sortParams %>');">
                    Group By Task</a></li>
            </ul>
        </div>
    </fieldset>
</div>
<div class="clear">
</div>
<div id="MissedVisitsContentId" class="report-grid" style="top: 105px;">
    <% Html.RenderPartial("~/Views/Agency/MissedVisits/Content.ascx"); %></div>

<script type="text/javascript">
    if (Acore.Mobile) $('#MissedVisitsContentId').css('top', 120); 
    $("#window_listmissedvisits_content").css({
        "background-color": "#d6e5f3"
    });
</script>

