﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTeam>" %>
<span class="wintitle">Manage Care Team&#8217;s Users | <%= Model.Name %></span>
<div class="wrapper main">
    <div class="team-access-users" guid="<%= Model.Id %>"></div>
</div>