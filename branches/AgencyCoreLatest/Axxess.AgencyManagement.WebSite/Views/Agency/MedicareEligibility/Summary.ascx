﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibilitySummary>>" %>
<% string pagename = "MedicareEligibility"; %>
<div class="wrapper">
    <fieldset>
        <div class="column">
            <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To:<input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons row"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Agency.RebindMedicareEligibilitySummaryGridContent('{0}','MedicareEligibilityContent',{{ StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }},'{1}');\">Generate Summaries</a>", pagename, sortParams)%></li></ul></div>
           <%-- <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "", new { StartDate= DateTime.Now, EndDate=DateTime.Now.AddDays(7)}, new { id = pagename + "_ExportLink" })%></li></ul></div>--%>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid" style="top:125px;">
        <% Html.RenderPartial("MedicareEligibility/Content", Model); %>
    </div>
</div>
<script type="text/javascript">
   
</script>


