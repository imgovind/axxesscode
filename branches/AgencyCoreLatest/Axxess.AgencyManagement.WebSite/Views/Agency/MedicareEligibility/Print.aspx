﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<MedicareEligibilitySummary>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.AgencyName.IsNotNullOrEmpty() ? Model.AgencyName + " | " : string.Empty %>Medicare Eligibility Summary</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Agency/medeligibility.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
    <% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.7.1.min.js")
     .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
     .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))
     .OnDocumentReady(() => {  %>
             PdfPrint.Fields = {
                "agency": "<h1><%= Model != null && Model.AgencyName.IsNotNullOrEmpty() ? Model.AgencyName : string.Empty %></h1>",
                "report": "<h1>Medicare Eligibility Report</h1>",
            };
             PdfPrint.BuildSections([ 
                   <% if (Model.Data.Errors != null && Model.Data.Errors.Count > 0)
                      {
                        bool first = true;
                        foreach (var error in Model.Data.Errors) {
                            if (first) { %>
                              
                                    { Content: [ [ "<h3>Errors</h3>" ] ] },
                                    {
                                        Content: [
                                            [
                                                "<strong>MRN</strong>",
                                                "<strong>Name</strong>",
                                                "<strong>Medicare Number</strong>",
                                                "<strong>Error</strong>",
                                            ]
                                        ]
                                    } 
                               <% first = false; %>  
                          <% } %>    , {
                                        Content: [
                                            [
                                                "<%= error.PatientIdNumber.Clean() %>",
                                                "<%= error.DisplayNameWithMi.Clean()%>",
                                                "<%= error.MedicareNumber.Clean()%>",
                                                "<%= error.Error.Clean()%>",
                                            ]
                                        ]
                                    }
                            <% } %>
                        <% } else { %>
                        { Content: [ [ "<h3>Errors</h3>" ] ] },
                        {
                            Content: [
                                [
                                    "<p>No errors found</p>"
                                ]
                            ]
                        } 
                        <% } %>
                    <% if (Model.Data.Payors != null && Model.Data.Payors.Count > 0)
                       {
                        bool first = true;
                        foreach (var payor in Model.Data.Payors) {
                            if (first) { 
                    %>
                        <%= Model.Data.Payors.Count > 0 ? "," : "No Payors found" %>     
                                    { Content: [ [ "<h3>Payors</h3>" ] ] },
                                    {
                                        Content: [
                                            [
                                                "<strong>MRN</strong>",
                                                "<strong>Name</strong>",
                                                "<strong>Medicare Number</strong>",
                                                "<strong>Payor</strong>",
                                                "<strong>Plan Number</strong>",
                                                "<strong>Start Date</strong>",
                                                "<strong>End Date</strong>",
                                            ]
                                        ]
                                    } 
                              <%  first = false; %>
                         <% } %>    , {
                                        Content: [
                                            [
                                                "<%= payor.PatientIdNumber.Clean() %>",
                                                "<%= payor.DisplayNameWithMi.Clean()%>",
                                                "<%= payor.MedicareNumber.Clean()%>",
                                                "<%= payor.PayorWithHtmlAddress.Clean()%>",
                                                "<%= payor.PlanNumber.Clean()%>",
                                                "<%= payor.StartDate.Clean()%>",
                                                "<%= payor.EndDate.Clean()%>",
                                            ]
                                        ]
                                    }
                            <% } %>
                        <% } else { %>
                        ,{ Content: [ [ "<h3>Payors</h3>" ] ] },
                        {
                            Content: [
                                [
                                    "<p>No payors found</p>"
                                ]
                            ]
                        } 
                        <% } %>
                        
                 ]) 
     <% }).Render(); %>
</body>
</html>
