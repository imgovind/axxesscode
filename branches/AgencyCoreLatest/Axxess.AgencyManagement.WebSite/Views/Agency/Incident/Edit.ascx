﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Incident>" %>
<span class="wintitle">Edit Incident/Accident Log | <%= Current.AgencyName%></span>
<% using (Html.BeginForm("Update", "Incident", FormMethod.Post, new { @id = "editIncidentReportForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Incident_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Incident_PatientId" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Incident_UserId" })%>
<% if (Model != null) Model.SignatureDate = DateTime.Today; %>
<div class="wrapper main">
    <%  if (Model.IsCommentExist) { %>
    <fieldset class="return-alert">
        <div>
            <span class="img icon error float-left"></span>
            <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
            <div class="buttons">
                <ul>
                    <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">Return Comments</a></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_PatientId" class="float-left">Patient Name:</label>
                <div class="float-right"><span class="bigtext"><%= Model.PatientName %></span></div>
            </div>
             <% if (Model.EpisodeId.IsEmpty()) { %> <div class="row"><label class="float-left">Episode Associated </label><div class="float-right"> <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_Incident_EpisodeId", @class = "requireddropdown" })%></div> </div><%}else{ %> <div class="row"><label  class="float-left">Episode Associated:</label><div class="float-right"><span class="bigtext"><%=string.Format("{0}-{1}",Model.EpisodeStartDate,Model.EpisodeEndDate) %></span></div></div><%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Incident_EpisodeId" })%><%} %>
            <div class="row">
                <label for="Edit_Incident_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Edit_Incident_PhysicianId", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
         </div><div class="column">
            <div class="row">
                <label for="Edit_Incident_IncidentDate" class="float-left">Date of Incident:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="IncidentDate" id="Edit_Incident_IncidentDate" value="<%= Model.IncidentDate.IsValid() ? Model.IncidentDate.ToShortDateString() : string.Empty %>" /></div>
            </div><div class="row">
                <label for="Edit_Incident_IncidentType" class="float-left">Type of Incident:</label>
                <div class="float-right"><%= Html.TextBox("IncidentType", Model.IncidentType, new { @id = "Edit_Incident_IncidentType", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div><div class="row">
                <label for="Edit_Incident_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right"><%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes" ? true : false, new { @id = "Edit_Incident_MDNotifiedYes", @class = "radio" })%><label for="Edit_Incident_MDNotifiedYes" class="inline-radio">Yes</label><%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No" ? true : false, new { @id = "Edit_Incident_MDNotifiedNo", @class = "radio" })%><label for="Edit_Incident_MDNotifiedNo" class="inline-radio">No</label><%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA" ? true : false, new { @id = "Edit_Incident_MDNotifiedNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_MDNotifiedNA" class="inline-radio">N/A</label></div>
            </div><div class="row">
                <label for="Edit_Incident_FamilyNotifiedYes" class="float-left">Family/CG Notified ?</label>
                <div class="float-right"><%= Html.RadioButton("FamilyNotified", "Yes", Model.FamilyNotified == "Yes" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedYes", @class = "radio" })%><label for="Edit_Incident_FamilyNotifiedYes" class="inline-radio">Yes</label><%= Html.RadioButton("FamilyNotified", "No", Model.FamilyNotified == "No" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedNo", @class = "radio" })%><label for="Edit_Incident_FamilyNotifiedNo" class="inline-radio">No</label><%= Html.RadioButton("FamilyNotified", "NA", Model.FamilyNotified == "NA" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_FamilyNotifiedNA" class="inline-radio">N/A</label></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right"><%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes" ? true : false, new { @id = "Edit_Incident_NewOrdersYes", @class = "radio" })%><label for="Edit_Incident_NewOrdersYes" class="inline-radio">Yes</label><%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No" ? true : false, new { @id = "Edit_Incident_NewOrdersNo", @class = "radio" })%><label for="Edit_Incident_NewOrdersNo" class="inline-radio">No</label><%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA" ? true : false, new { @id = "Edit_Incident_NewOrdersNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_NewOrdersNA" class="inline-radio">N/A</label></div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) involved:</legend>
         <table class="form"><tbody>
            <tr><%string[] individualsInvolved = Model.IndividualInvolved != null && Model.IndividualInvolved != "" ? Model.IndividualInvolved.Split(';') : null;  %>
                <td>
                    <%= string.Format("<input id='Edit_Incident_IndividualInvolved1' type='checkbox' value='Patient' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Patient") ? "checked='checked'" : "")%>
                    <label for="Edit_Incident_IndividualInvolved1" class="radio">Patient</label>
                </td><td>
                    <%= string.Format("<input id='Edit_Incident_IndividualInvolved2' type='checkbox' value='Caregiver' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Caregiver") ? "checked='checked'" : "")%>
                    <label for="Edit_Incident_IndividualInvolved2" class="radio">Caregiver</label>
                </td><td>
                    <%= string.Format("<input id='Edit_Incident_IndividualInvolved3' type='checkbox' value='Employee/Contractor' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Employee/Contractor") ? "checked='checked'" : "")%>
                    <label for="Edit_Incident_IndividualInvolved3" class="radio">Employee/Contractor</label>
                </td><td>
                    <%= string.Format("<input id='Edit_Incident_IndividualInvolved4' type='checkbox' value='Other' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Other") ? "checked='checked'" : "")%>
                    <label for="Edit_Incident_IndividualInvolved4" class="radio">Other (specify) &#160;</label>
                </td><td>
                    <%= Html.TextBox("IndividualInvolvedOther", Model.IndividualInvolvedOther, new { @id = "Edit_Incident_IndividualInvolvedOther", @class = "text input_wrapper", @maxlength = "100" })%>
                </td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <table class="form"><tbody>           
             <tr class="line-seperated vert">
                <td><label for="Comment">Describe Incident/Accident:</label>
                <%= Html.Templates("DescriptionTemplates", new { @class = "Templates", @template = "#New_Incident_Description" })%>
                <div><%= Html.TextArea("Description", Model.Description, new {@id = "New_Incident_Description", @style = "height: 180px;" })%></div></td>
            </tr>
        </tbody></table>
        <table class="form"><tbody>           
             <tr class="line-seperated vert">
                <td><label for="Comment">Action Taken/Interventions Performed:</label>
                <%= Html.Templates("ActionTakenTemplates", new { @class = "Templates", @template = "#New_Incident_ActionTaken" })%>
                <div><%= Html.TextArea("ActionTaken", Model.ActionTaken, new { @id = "New_Incident_ActionTaken", @style = "height: 180px;" })%></div></td>
            </tr>
        </tbody></table>  
        <table class="form"><tbody>           
             <tr class="line-seperated vert">
                <td><label for="Comment">Orders:</label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "Templates", @template = "#New_Incident_Narrative" })%>
                <div><%= Html.TextArea("Orders", Model.Orders, new {@id = "New_Incident_Narrative", @style = "height: 180px;" })%></div></td>
            </tr>
        </tbody></table>  
         <table class="form"><tbody>           
             <tr class="line-seperated vert">
                <td><label for="FollowUp">Follow Up:</label>
                <%= Html.Templates("FollowUpTemplates", new { @class = "Templates", @template = "#New_Incident_FollowUp" })%> 
                <div><%= Html.TextArea("FollowUp", Model.FollowUp, new {@id = "New_Incident_FollowUp", @style = "height: 180px;" })%></div></td>
            </tr>
        </tbody></table>  
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_ClinicianSignature" class="bigtext float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Incident_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_SignatureDate" class="bigtext float-left">Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="SignatureDate" id="Edit_Incident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "Edit_Incident_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$('#Edit_Incident_Status').val('515');$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="$('#Edit_Incident_Status').val('520');$(this).closest('form').submit();">Complete</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editincidentreport');">Exit</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    Template.OnChangeInit();
</script>
<%} %>



