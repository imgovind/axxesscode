﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Incidents", "Export", FormMethod.Post)) { %>
<span class="wintitle">List Incidents | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Incidents", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Incident>().Name("List_IncidentReport").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
    columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
    columns.Bound(i => i.IncidentType).Title("Type of Incident").Sortable(true);
    columns.Bound(i => i.IncidentDateFormatted).Title("Incident Date").Sortable(true);
    columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
    columns.Bound(i => i.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditIncident('<#=Id#>');\">Edit</a>  | <a href=\"javascript:void(0);\" onclick=\"IncidentReport.Delete('<#=PatientId#>', '<#=EpisodeId#>', '<#=Id#>');\">Delete</a>").Sortable(false).Title("Action").Width(200).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Incident")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_IncidentReport .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Incident Log", Click: UserInterface.ShowNewIncidentReport } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>