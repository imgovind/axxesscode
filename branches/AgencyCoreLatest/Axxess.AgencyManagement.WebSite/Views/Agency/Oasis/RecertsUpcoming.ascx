﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Upcoming Recerts | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg">
<div class="buttons float-right" ><ul> <li><%if (Current.HasRight(Permissions.ExportListToExcel)){ %><li><%= Html.ActionLink("Export to Excel", "RecertsUpcoming", "Export", new { BranchId = Guid.Empty, InsuranceId = Model }, new { id = "AgencyUpcomingRecet_ExportLink" })%></li><%} %></ul></div>
    
    <fieldset class="orders-filter">
        <div class="buttons float-right">
            <ul><li><a href="javascript:void(0);" onclick="Agency.RebindAgencyUpcomingRecet();">Generate</a></li></ul>
        </div>
        <label class="strong">Branch:
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyUpcomingRecet_BranchCode" })%>
        </label>
        <label class="strong">Insurance:
            <%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyUpcomingRecet_InsuranceId", @class = "Insurances" })%>
        </label> 
        <div id="AgencyUpcomingRecet_Search"></div>
    </fieldset>
    
    <%= Html.Telerik()
        .Grid<RecertEvent>().Name("List_UpcomingRecerts").HtmlAttributes(new { @style = "top:85px;" })
        .Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.Status).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDateFormatted).Title("Due Date").Width(120).Sortable(true);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsUpcoming", "Agency", new { BranchId = Guid.Empty, InsuranceId = Model })).Footer(false).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
 </div>
<script type="text/javascript">
    if (Acore.Mobile) $('#List_UpcomingRecerts').css('top', 130);
    $("#AgencyUpcomingRecet_Search").append($("<div/>").GridSearchById("#List_UpcomingRecerts"));
    $("#window_listupcomingrecerts #List_UpcomingRecerts .t-grid-content").css({ "height": "auto", "top": "26px" });
    $("#window_listupcomingrecerts_content").css({
        "background-color": "#d6e5f3"
    });
    $('.grid-search').css({ 'position': 'relative', left: 0, 'margin-left': 0 });
</script>
