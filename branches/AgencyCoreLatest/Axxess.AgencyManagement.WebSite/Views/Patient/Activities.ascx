﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<script type="text/javascript">
	Patient.Charts._dateFilter = Patient.Charts._dateFilter != "" ? Patient.Charts._dateFilter : <% if (Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) || Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) || Current.IfOnlyRole(AgencyRoles.Auditor)) { %> "All" <% } else { %> "ThisEpisode" <% } %>;
	Patient.Charts._showFilter = Patient.Charts._showFilter != "" ? Patient.Charts._showFilter : "All";
</script>
<div class="abs above">
    <div class="float-left">
        <label>Show</label>
        <select id="patient-activity-drop-down" name="discipline" class="patient-activity-drop-down">
            <option value="All" selected="selected">All</option>
            <option value="Orders">Orders</option>
            <option value="Nursing">Nursing</option>
            <option value="PT">PT</option>
            <option value="OT">OT</option>
            <option value="ST">ST</option>
            <option value="HHA">HHA</option>
            <option value="MSW">MSW</option>
            <option value="Dietician">Dietician</option>
        </select>
        <label>Date</label>
        <select id="patient-activity-date-drop-down" name="dateRangeId" class="patient-activity-date-drop-down">
            <option value="All">All</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="DateRange">Date Range</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="ThisEpisode">This Episode</option>
            <option value="LastEpisode">Last Episode</option>
            <option value="NextEpisode">Next Episode</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="Today">Today</option>
            <option value="ThisWeek">This Week</option>
            <option value="ThisWeekToDate">This Week-to-date</option>
            <option value="LastMonth">Last Month</option>
            <option value="ThisMonth">This Month</option>
            <option value="ThisMonthToDate">This Month-to-date</option>
            <option value="ThisFiscalQuarter">This Fiscal Quarter</option>
            <option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option>
            <option value="ThisFiscalYear">This Fiscal Year</option>
            <option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option>
            <option value="Yesterday">Yesterday</option>
            <option value="LastWeek">Last Week</option>
            <option value="LastWeekToDate">Last Week-to-date</option>
            <option value="LastMonthToDate">Last Month-to-date</option>
            <option value="LastFiscalQuarter">Last Fiscal Quarter</option>
            <option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option>
            <option value="LastFiscalYear">Last Fiscal Year</option>
            <option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option>
            <option value="NextWeek">Next Week</option>
            <option value="Next4Weeks">Next 4 Weeks</option>
            <option value="NextMonth">Next Month</option>
            <option value="NextFiscalQuarter">Next Fiscal Quarter</option>
            <option value="NextFiscalYear">Next Fiscal Year</option>
        </select>
    </div>
    <div id="patient-center-activity-filter" class="float-left">
        <div id="date-range-text" class="float-right"></div>
        <div class="custom-date-range" class="hidden">
            <div class="buttons float-right" style="margin-top:-3px">
                <ul>
                    <li><a onclick="Patient.Charts.Activities.Rebind();return false">Search</a></li>
                </ul>
            </div>
            <label>From</label>
            <input type="text" class="date-picker" name="RangeStartDate" id="patient-activity-from-date" />
            <label>To</label>
            <input type="text" class="date-picker" name="RangeEndDate" id="patient-activity-to-date" />
        </div>
    </div>
</div>
<% var val = Model != null && !Model.IsEmpty() ? Model : Guid.Empty;
    Html.Telerik().Grid<ScheduleEventJson>().Name("PatientActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false).Width(0);
       columns.Bound(s => s.Task).ClientTemplate("<# if(IsComplete || !OnClick) { #><#= Task #><# } else { #><a class=\"link\" onclick=\"<#= OnClick #>\"><#= Task #></a><# } #>").Title("Task").Width(180);
       columns.Bound(s => s.Date).ClientTemplate("<#= U.FormatDate(Date) #>").Title("Scheduled Date").Width(120);
       columns.Bound(s => s.User).Title("Assigned To").Width(130);
       columns.Bound(s => s.Status).Title("Status").Width(100);
       columns.Bound(s => s.IsVisitVerified).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" });
       columns.Bound(s => s.OasisProfileUrl).Sortable(false).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" });
       columns.Bound(s => s.StatusComment).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"red-note tooltip\" href=\"javascript:void(0);\"><#=StatusComment#></a>");
       columns.Bound(s => s.Comments).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=Comments#></a>");
       columns.Bound(s => s.EpisodeNotes).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\"><#=EpisodeNotes#></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").Sortable(false).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=PrintUrl#>").Width(22);
       columns.Bound(s => s.HasAttachments).Title(" ").Sortable(false).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Width(22);
       columns.Bound(s => s.AP).ClientTemplate("<#=AP#>").Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
   }).ClientEvents(c => c.OnRowDataBound("Patient.Charts.Activities.OnRowDataBound").OnDataBinding("Patient.Charts.Activities.OnDataBinding").OnDataBound("Patient.Charts.Activities.OnDataBound").OnLoad("Patient.Charts.Activities.OnLoad"))
   .DataBinding(dataBinding => dataBinding.Ajax())
   .Sortable(s => s.OrderBy(o => o.Add("Date").Descending())).Scrollable().Footer(false).Render();
%>