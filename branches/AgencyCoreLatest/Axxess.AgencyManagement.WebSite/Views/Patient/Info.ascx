﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="window-menu">
    <ul class="">
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen && !Model.IsNotAdmitted) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%=Model.Id %>');">New Order</a></li><% } %>
        <% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewCommunicationNote('<%=Model.Id %>');">New Communication Note</a></li><% } %>
        <% if (Current.HasRight(Permissions.ManageInsurance) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewAuthorization('<%=Model.Id %>');">New Authorization</a></li><% } %>
        <% if (Current.HasRight(Permissions.ScheduleVisits) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen && !Model.IsPending)
           { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleReassignModal('<%= Guid.Empty %>', '<%= Model.Id %>','Patient');" title="Reassign Schedules">Reassign Schedules</a></li><%}%>
        <li>
            <a href="javascript:void(0);" class="menu-trigger">Upload Document</a>
            <ul class="menu">
                <li>
                    <a href="javascript:void(0);" onclick="UserInterface.ShowNewDocumentModal('<%= Model.Id %>');">New Document</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="UserInterface.ShowPatientDocuments('<%= Model.Id %>');">View Documents</a>
                </li>
            </ul>
        </li>
        <%--<% if(Current.HasRight(Permissions.ManagePatientAccess)) {%>
        <li>
            <a href="javascript:void(0);" onclick="Patient.LoadUserAccess('<%=Model.Id %>');">Clinician Access</a>
        </li>
        <%} %>--%>
    </ul>
</div>
<div class="two-thirds">
    <div class="float-left"><% if (!Model.PhotoId.IsEmpty()) { %><img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" /><% } else { %><img src="/Images/blank_user.jpeg" alt="User Photo" /><% } %><div class="clear"></div><% if (Current.HasRight(Permissions.ManagePatients)&& !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><div class="align-center">[ <a href="javascript:void(0);" onclick="UserInterface.ShowNewPhotoModal('<%= Model.Id%>');">Change Photo</a> ] </div><% } %></div>
    <div class="column"><span class="bigtext"><%= Model.DisplayNameWithMi  %></span><div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="refreshInfo()">Refresh</a></li><% if (Current.HasRight(Permissions.ManagePatients) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowChangeStatusModal('<%= Model.Id%>');">Change Status</a></li><% } %></ul></div></div>
    <div class="shortrow"><label class="float-left">MR #:</label><span><%= Model.PatientIdNumber%></span></div><% if (Current.HasRight(Permissions.ManagePatients)  && !Current.IfOnlyRole(AgencyRoles.Auditor) && (Model.IsDischarged||Model.IsNotAdmitted)) { %><div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowReadmitPatientModal('<%= Model.Id%>');">Re-Admit</a></li></ul></div><% } %>
    <% if (Current.HasRight(Permissions.ManagePatients)  && !Current.IfOnlyRole(AgencyRoles.Auditor) && (Model.IsPending)){ %><div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowAdmitPatientModal('<%= Model.Id%>','<%=1%>');">Admit</a></li></ul></div><% } %>
    <div class="shortrow"><label class="float-left">Birthday:</label><span><%= Model.DOB != DateTime.MinValue ? Model.DOB.ToString("MMMM dd, yyyy") : "" %></span></div>
    <div class="shortrow"><label class="float-left">Start of Care Date:</label><span><%= Model.StartOfCareDateFormatted %></span></div>
    <% if (Model.PhoneHome.IsNotNullOrEmpty()) { %><div class="shortrow"><label class="float-left">Primary Phone:</label><span><%= Model.PhoneHome.ToPhone() %></span></div><% } %>
    <% var physician = Model.Physician; %>
    <% if (physician != null) { %><div class="shortrow"><label class="float-left">Physician Name:</label><span><%= physician != null ? physician.DisplayName.Trim() : string.Empty %></span></div><% } %>
    <div class="shortrow"><label class="float-left">Insurance Name / ID:</label><span><%=Model.PrimaryInsuranceName %>&nbsp;/&nbsp;<%=Model.PrimaryHealthPlanId.IsNotNullOrEmpty()?Model.PrimaryHealthPlanId:Model.MedicareNumber %></span></div>
    <div class="shortrow">
        <% if (Current.HasRight(Permissions.ManagePatients) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %>[ <a href="javascript:void(0);" onclick="UserInterface.ShowEditPatient('<%= Model.Id%>'); ">Edit</a> ]&#160;<% } %>
        [ <a href="javascript:void(0);" onclick="Patient.Charts.MoreInfo($(this));">More</a> ]&#160;
        [ <a href="https://<%= Request.ServerVariables["HTTP_HOST"] %>/Map/Google/<%= Model.Id %>/<%= Model.AgencyId %>" target="_blank">Directions</a> ]&#160;
        <% if (Current.HasRight(Permissions.ManagePatients) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %>[ <a href="javascript:void(0);" onclick="Patient.LoadPatientAdmissionPeriods('<%= Model.Id %>');">Admission Periods</a> ]&#160;<% } %>
    </div>
</div>
<div class="one-third encapsulation">
    <h4>Quick Reports<span class="mobile_hidden"> For This Patient</span></h4>
    <ul>
        <li><a href="javascript:void(0);" onclick="Acore.OpenPrintView({ Url: '/Patient/Profile/<%=Model.Id %>', PdfUrl: '/Patient/PatientProfilePdf', PdfData: { 'id': '<%=Model.Id %>' } })">Patient Profile</a></li>
        <% if (Current.HasRight(Permissions.ViewMedicationProfile)&& !Model.IsPending ) { %><li><a href="javascript:void(0);" onclick="Patient.LoadMedicationProfile('<%= Model.Id%>');">Medication Profile(s)</a></li><% } %>
        <% if (!Model.IsPending ) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowPatientAllergies('<%= Model.Id%>');">Allergy Profile</a></li><% } %>
        <% if (Current.HasRight(Permissions.ManageInsurance) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowPatientAuthorizations('<%= Model.Id%>');">Authorizations Listing</a></li><% } %>
        <li><a href="javascript:void(0);" onclick="Patient.LoadPatientCommunicationNotes('<%= Model.Id%>');">Communication Notes</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.ShowPatientOrdersHistory('<%= Model.Id%>');">Orders And Care Plans</a></li>
        <% if (!Model.IsPending ) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowPatientSixtyDaySummary('<%= Model.Id%>');">60 Day Summaries</a></li><% } %>
        <% if (!Model.IsPending ) { %><li><a href="javascript:void(0);" onclick="Patient.VitalSigns.Load('<%= Model.Id%>');">Vital Signs Charts</a></li><% } %>
        <li><a href="javascript:void(0);" onclick="Acore.OpenPrintView({ Url: '/Patient/TriageClassification/<%=Model.Id %>' });">Triage Classification</a></li>
        <% if (Current.HasRight(Permissions.DeleteTasks) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowDeletedTaskHistory('<%= Model.Id%>');">Deleted Tasks/Doc<span class="mobile_hidden">ument</span>s</a></li><% } %>
    </ul>
</div>
<div class="float-left">
    <% if (Current.HasRight(Permissions.ScheduleVisits) && !Current.IfOnlyRole(AgencyRoles.Auditor))
       { %><div class="buttons float-left"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowScheduleCenter('<%= Model.Id %>', '<%= Model.Status %>', $('.PatientBranchCode', '#window_patientcenter').val());">Schedule Activity</a></li></ul></div><% } %>
    <fieldset class="note-legend float-left"><ul><li><span class="img icon note"></span>Visit Comments</li><li><span class="img icon note-blue"></span>Episode Comments</li><li><span class="img icon note-red"></span>Missed/Returned</li></ul></fieldset>
</div>
<div id="discharge_patient_Container" class="dischargepatientmodal hidden"></div>
<div id="activate_patient_Container" class="dischargepatientmodal hidden"></div>
<div id="reassign_Patient_Container" class="reassignpatientmodal hidden"></div>
<div id="readmit_Patient_Container" class="readmitpatientmodal hidden"></div>
<script type="text/javascript">
    $("#PatientMainResult .window-menu ul").Menu();
    function refreshInfo() {
        Patient._rebind = true; 
        Patient.Charts.LoadInfoAndActivity('<%= Model.Id %>');
    }
</script>