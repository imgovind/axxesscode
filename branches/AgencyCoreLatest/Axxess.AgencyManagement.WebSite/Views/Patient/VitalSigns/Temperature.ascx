﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayTemp = new List<double>(); %>
<%  var arrayTempDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.TempGraph > 0) { %>
        <%  arrayTemp.Add(v.TempGraph); %>
        <%  arrayTempDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var tempJson = arrayTemp.ToJavascrptArray(); %>
<%  var dateTempJson = arrayTempDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Temperature
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Temperature" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Temperature</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-single-value"><%= item.Temp %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Temperature").Graph({
        Type: "line",
        Title: "Temperature",
        YAxisTitle: "Temperature",
        YAxisSuffix: "°",
        XAxisData: <%= dateTempJson %>,
        YAxisData: [{ name: "Temperature", data: <%= tempJson %> }]
    });
</script>