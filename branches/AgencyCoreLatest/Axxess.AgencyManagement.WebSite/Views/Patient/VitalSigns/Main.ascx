﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var pagename = "Patient_VitalSignsCharts"; %>
<%  var patientId = ViewData.ContainsKey("PatientId") ? ViewData["PatientId"] : Guid.Empty; %>
<div class="wrapper main">
    <div class="fr buttons">
        <ul>
            <li><a class="refresh">Refresh</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export vitals-report-show">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="ac grid-controls">
        <div class="filter">
            <label class="strong">Patient</label>
            <%= Html.TextBox("PatientId", ViewData["PatientId"].ToSafeString(), new { @id = pagename + "_PatientId", @class = "patient-selector" })%>
        </div>
        <div class="buttons continuous fr">
            <ul>
                <li class="selected"><a class="vitals-graphs">Vital Signs Charts</a></li>
                <li><a class="vitals-report">Vital Signs Log</a></li>
            </ul>
        </div>
        <div class="filter vitals-report-show">
            <label for="<%= pagename %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
            <label for="<%= pagename %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
        </div>
    </fieldset>
    <div id="<%= pagename %>_Content" class="vitals-content"><% Html.RenderPartial("VitalSigns/Graphs", Model); %></div>
</div>

