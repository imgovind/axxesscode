﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayWeight = new List<double>(); %>
<%  var arrayWeightDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.WeightGraph > 0) { %>
        <%  arrayWeight.Add(v.WeightGraph); %>
        <%  arrayWeightDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var weightJson = arrayWeight.ToJavascrptArray(); %>
<%  var dateWeightJson = arrayWeightDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Weight
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Weight" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Weight</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-single-value"><%= item.Weight %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Weight").Graph({
        Type: "line",
        Title: "Weight",
        YAxisTitle: "Weight",
        YAxisSuffix: "lbs",
        XAxisData: <%= dateWeightJson %>,
        YAxisData: [{ name: "Weight", data: <%= weightJson %> }]
    });
</script>