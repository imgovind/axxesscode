﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<MedicationProfileSnapshotViewData>" %><%
var medications = Model != null&& Model.MedicationProfile !=null && Model.MedicationProfile.Medication.IsNotNullOrEmpty() ? Model.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + " | " : "" %>Medication Profile<%= Model != null && Model.Patient != null ? (" | " + (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.Clean().ToTitleCase() : "") + ", " + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.Clean().ToTitleCase() : "") + " " + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.Clean().ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/medprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "mr": "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>",
            "physician": "<%= Model.PhysicianName.Clean().ToTitleCase() %>",
            "episode": "<%= Model != null && Model.StartDate.IsValid() && Model.EndDate.IsValid() ? string.Format(" {0}&#8211;{1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() : string.Empty %>",
            "pharmacy": "<%= Model != null ? Model.PharmacyName.Clean() : string.Empty %>",
            "pharmphone": "<%= Model != null ? Model.PharmacyPhone.ToPhone() : string.Empty %>",
            "pridiagnosis": "<%= Model != null && Model.PrimaryDiagnosis.IsNotNullOrEmpty() ? Model.PrimaryDiagnosis : "" %>",
            "secdiagnosis": "<%= Model != null && Model.SecondaryDiagnosis.IsNotNullOrEmpty() ? Model.SecondaryDiagnosis : string.Empty %>",
            "allergies": "<%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies.Clean() : string.Empty %>",
            "sign": "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",
            "signdate": "<%= Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString().Clean() : string.Empty %>"
        };
        PdfPrint.BuildSections([ <%
if (medications.Count > 0) {
    bool first = true;
    foreach (var medication in medications) if (medication.MedicationCategory == "Active") {
        if (first) { %>
                { Content: [ [ "<h3>Active Medications</h3>" ] ] },
                {
                    Content: [
                        [
                            "<strong>LS</strong>",
                            "<strong>Start Date</strong>",
                            "<strong>Medication &#38; Dosage</strong>",
                            "<strong>Frequency</strong>",
                            "<strong>Route</strong>",
                            "",
                            "<strong>Classification</strong>"
                        ]
                    ]
                } <%
            first = false;
        } %>    , {
                    Content: [
                        [
                            PdfPrint.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
                            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
                            "<%= medication.MedicationDosage.Clean() %>",
                            "<%= medication.Frequency.Clean() %>",
                            "<%= medication.Route.Clean() %>",
                            "<%= medication.MedicationType.Value.Clean() %>",
                            "<%= medication.Classification.Clean() %>"
                        ]
                    ]
                } <%
    }
    first = true;
    foreach (var medication in medications) if (medication.MedicationCategory == "DC") {
        if (first) { %>
                ,{ Content: [ [ "<h3>Discontinued Medications</h3>" ] ] },
                {
                    Content: [
                        [
                            "<strong>LS</strong>",
                            "<strong>Start Date</strong>",
                            "<strong>Medication &#38; Dosage</strong>",
                            "<strong>Frequency</strong>",
                            "<strong>Route</strong>",
                            "",
                            "<strong>Classification</strong>",
                            "<strong>D/C Date</strong>"
                        ]
                    ]
                } <%
            first = false;
        } %>    , {
                    Content: [
                        [
                            PdfPrint.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
                            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
                            "<%= medication.MedicationDosage.Clean() %>",
                            "<%= medication.Frequency.Clean() %>",
                            "<%= medication.Route.Clean() %>",
                            "<%= medication.MedicationType.Value.Clean() %>",
                            "<%= medication.Classification.Clean() %>",
                            "<%= medication.DCDate.IsValid() ? medication.DCDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty%>"
                        ]
                    ]
                } <%
    }
} %>
        ]);
    <% }).Render(); %>
</body>
</html>