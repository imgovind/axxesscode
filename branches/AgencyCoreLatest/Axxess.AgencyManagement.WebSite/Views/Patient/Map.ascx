﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="navigation">
            <div class="start address-block">
                <div>
                    <label class="strong">Starting Address:</label><input name="start" type="hidden" value="" />
                    <%= Html.LookupSelectList(SelectListTypes.MapAddress,"startaddress_select",Current.UserAddress, new {  @id = "Map_startaddress_select", @class = "float-right required" }) %>
                    <div class="clear"></div>
                </div>
                <div id="Map_startaddress">
                    <input type="text" id="Map_startaddr" value="" class="mapaddr" /><br />
                    <input type="text" id="Map_startcity" value="" class="mapcity" />
                    <%= Html.LookupSelectList(SelectListTypes.States, "startstate", Model.AddressStateCode, new { @id = "Map_startstate", @class = "mapcity" })%>
                    <%= Html.TextBox("startzip", "", new { @id = "Map_startzip", @class = "mapzip", @maxlength = "9" })%>
                </div>
            </div><div class="end address-block">
                <div><label class="strong">Patient&#8217;s Address</label><input name="end" type="hidden" value="<%= Model.AddressFull %>" /></div> 
                <div>
                    <input type="text" id="Map_endaddr" value="<%= Model.AddressLine1 %> <%= Model.AddressLine2 %>" class="mapaddr" /><br />
                    <input type="text" id="Map_endcity" value="<%= Model.AddressCity %>" class="end mapcity" />
                    <%= Html.LookupSelectList(SelectListTypes.States, "endstate", Model.AddressStateCode, new { @id = "Map_endstate", @class = "mapcity" })%>
                    <%= Html.TextBox("endzip", Model.AddressZipCode, new { @id = "Map_endzip", @class = "mapzip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="buttons">
                <ul>
                    <li><a href="javascript:void(0);" id="Map_Recalculate">Recalculate Directions</a></li>
                </ul>
            </div>
        </div>
        <div id="directions"></div>
    </div><div class="ui-layout-center">
        <div id="map"></div>
    </div>
</div>
<script type="text/javascript">
    GoogleMap.init($("#Map_startaddress_select option:selected").val(), $("input[name=end]").val());
</script>