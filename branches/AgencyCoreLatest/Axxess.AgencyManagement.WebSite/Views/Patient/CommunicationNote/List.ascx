﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Communication Notes | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg" >
<div class="buttons"><ul class="float-right"><li><%= Html.ActionLink("Export to Excel", "CommunicationNotes", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "AgencyCommunicationNote_ExportLink", @class = "excel" })%></li></ul></div>
<fieldset class="orders-filter">
    <div class="buttons float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="Agency.RebindAgencyCommunicationNotes();">Generate</a></li>
        </ul>
    </div>
        <label class="float-left">Branch:</label><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "AgencyCommunicationNote_BranchCode" })%><label class="strong">Status:</label><select id="AgencyCommunicationNote_Status" name="Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select>
        <br />
        <label class="float-left">Date Range:</label>
        <div class="float-left"><%= Html.Telerik().DatePicker().Name("StartDate").Value(DateTime.Now.AddDays(-59)).HtmlAttributes(new { @id = "AgencyCommunicationNote_StartDate", @class = "shortdate date" })%><label class="strong">To</label><%= Html.Telerik().DatePicker().Name("EndDate").Value(DateTime.Now).HtmlAttributes(new { @id = "AgencyCommunicationNote_EndDate", @class = "shortdate date" })%></div>
    <div id="AgencyCommunicationNote_Search" ></div>
</fieldset>
        
    
    
    <%= Html.Telerik()
            .Grid<CommunicationNote>().Name("List_CommunicationNote").HtmlAttributes(new { @style = "top:70px;" })
            .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("id"); })
            .Columns(columns =>
            {
                columns.Bound(c => c.DisplayName).Title("Patient Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(true).ReadOnly();
                columns.Bound(c => c.StatusName).Title("Status").Sortable(true).ReadOnly();
                columns.Bound(c => c.Id).Title(" ").ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ Url: '/CommunicationNote/View/<#=PatientId#>/<#=Id#>', PdfUrl: 'Patient/CommunicationNotePdf', PdfData: {  'patientId': '<#=PatientId#>', 'eventId': '<#=Id#>', 'episodeId': '<#=EpisodeId#>' }})\"><span class=\"img icon print\"></span></a>").Width(35).Sortable(false);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("CommunicationNotes", "Patient", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))%>
    </div>
    <script type="text/javascript">
        $("#AgencyCommunicationNote_Search").append($("<div/>").GridSearchById("#List_CommunicationNote"));
        $("#List_CommunicationNote .t-grid-content").css({ 'height': 'auto', 'bottom': '23px' });
        $("#window_communicationnoteslist_content").css({
            "background-color": "#d6e5f3"
        });
        $('.grid-search').css({ 'position': 'relative', 'left': '0','margin-left':'5px' });
    </script>
