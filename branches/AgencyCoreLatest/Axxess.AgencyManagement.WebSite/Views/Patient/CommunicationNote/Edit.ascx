﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>
<span class="wintitle">Edit Communication Note | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "CommunicationNote", FormMethod.Post, new { @id = "editCommunicationNoteForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_CommunicationNote_Id" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_CommunicationNote_UserId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_CommunicationNote_PatientId" })%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    Communication Note
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
    <%  } %>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="Edit_CommunicationNote_PatientName" class="float-left width200">Patient Name:</label>
                        <div class="float-left">
                            <span id = "Edit_CommunicationNote_PatientName"><%= Model != null && Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName : string.Empty %></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="Edit_CommunicationNote_EpisodeList" class="float-left width200">Episode Associated:</label>
                        <div class="float-left">
    <%  if (Model.EpisodeId.IsEmpty()) { %>
                            <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_CommunicationNote_EpisodeList", @class = "requireddropdown" })%>
    <%  } else { %>
                            <span id="Edit_CommunicationNote_EpisodeList"><%= string.Format("{0} - {1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></span>
                            <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_CommunicationNote_EpisodeId" })%>
    <%  } %>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="Edit_CommunicationNote_Date" class="float-left width200">Date:</label>
                        <div class="float-left">
                            <span id="Edit_CommunicationNote_Date"></span>
                            <input type="text" class="date-picker required" name="Created" value="<%= Model.Created.ToShortDateString() %>" id="Edit_CommunicationNote_Date" />
                        </div>
                    </div>
                    <div class="clear"></div>
                </td>
                <td colspan="2">
                    <div>
                        <label for="Edit_CommunicationNote_PhysicianDropDown" class="float-left width200">Physician:</label>
                        <div class="float-left"><%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : "", new { @id = "Edit_CommunicationNote_PhysicianDropDown", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left width200">&nbsp;</label>
                        <div class="float-left ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="align-left">
                    <div>
                        <label class="width200 float-left">Communication Text</label>
                        <div class="float-left"><%= Html.Templates("Edit_CommunicationNote_Templates", new { @class = "Templates mobile_fr", @template = "#Edit_CommunicationNote_Text" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div class="align-center"><%= Html.TextArea("Text", Model.Text, 8, 20, new { @id = "Edit_CommunicationNote_Text", @class = "fill", @maxcharacters = "5000" })%></div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div>
                        <label for="Edit_CommunicationNote_SendAsMessage" class="float-left width200">Send note as Message:</label>
                        <div class="float-left"><%= Html.CheckBox("SendAsMessage", false, new { @id = "Edit_CommunicationNote_SendAsMessage", @class = "bigradio" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div id="Edit_CommunicationNote_Recipients"><%= Html.Recipients("Edit_CommunicationNote", Model.RecipientArray) %></div>
                </td>
            </tr>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="Edit_CommunicationNote_ClinicianSignature" class="float-left">Staff Signature:</label>
                        <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_CommunicationNote_ClinicianSignature" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="Edit_CommunicationNote_ClinicianSignatureDate" class="float-left">Signature Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="SignatureDate" id="Edit_CommunicationNote_ClinicianSignatureDate" /></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <%= Html.Hidden("Status", "", new { @id = "Edit_CommunicationNote_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="EditCommNoteRemove();$('#Edit_CommunicationNote_Status').val('415');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="EditCommNoteAdd();$('#Edit_CommunicationNote_Status').val('420');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editcommunicationnote');">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Template.OnChangeInit();
    $("#Edit_CommunicationNote_SendAsMessage").click(function() {
        $("#Edit_CommunicationNote_Recipients").toggle()
    });
    $("#Edit_CommunicationNote_PhysicianDropDown").PhysicianInput();
    function EditCommNoteAdd() {
        $("#Edit_CommunicationNote_ClinicianSignature").removeClass('required').addClass('required');
        $("#Edit_CommunicationNote_ClinicianSignatureDate").removeClass('required').addClass('required');
    }
    function EditCommNoteRemove() {
        $("#Edit_CommunicationNote_ClinicianSignature").removeClass('required');
        $("#Edit_CommunicationNote_ClinicianSignatureDate").removeClass('required');
    }
</script>