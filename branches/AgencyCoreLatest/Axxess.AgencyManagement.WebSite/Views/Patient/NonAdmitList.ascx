﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Non-Admitted Patients | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("NonAdmitExport", "Patient", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<NonAdmit>()
        .Name("List_Patient_NonAdmit_Grid")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(2);
            columns.Bound(p => p.DisplayName).Title("Patient").Width(4);
            columns.Bound(p => p.InsuranceName).Title("Insurance").Width(4).Sortable(false);
            columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(4).Sortable(false);
            columns.Bound(p => p.AddressFull).Title("Address").Width(6).Sortable(false);
            columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(3).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(3).Sortable(false);
            columns.Bound(p => p.Gender).Width(2).Sortable(true);
            columns.Bound(p => p.NonAdmissionReason).Width(5).Title("Non-Admit Reason").Sortable(false);
            columns.Bound(p => p.NonAdmitDate).Title("Non-Admit Date").Width(3).Sortable(true);
            columns.Bound(p => p.Comments).Width(1).Sortable(false).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=CommentsCleaned#></a>"); ;
            columns.Bound(p => p.Id).Width(2).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowAdmitPatientModal('<#=Id#>', '<#=Type#>');\" class=\"\">Admit</a>").Title("Action").Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("NonAdmitList", "Patient"))
        .ClientEvents(evnts => evnts.OnRowDataBound("Patient.PatientRowDataBound"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } %>
<script type="text/javascript">
    $("#List_Patient_NonAdmit_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients) && !Current.IsAgencyFrozen) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
    )<% } %>;
    $("#List_Patient_NonAdmit_Grid .t-grid-content").css("height", "auto");
</script>