﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationViewData>" %>
<span class="wintitle">Hospitalization Logs | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Hospitalization Logs</th></tr>
            <tr><td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td></tr>
        </tbody>
    </table>
    <div class="buttons">
        <ul class="float-left">
            <li><a href="javascript:void(0);" onclick="HospitalizationLog.Add('<%= Model.Patient.Id %>');">Add Hospitalization Log</a></li>
        </ul>
        <ul class="float-right">
            <li><a href="javascript:void(0);" onclick="U.GetAttachment('Export/PatientHospitalizationLog', {'patientId':'<%= Model.Patient.Id %>'});">Export to Excel</a></li>
        </ul>
    </div><div class="clear"></div>
    <div id="Hospitalization_list" class="standard-chart"><% Html.RenderPartial("~/Views/Patient/Hospitalization/Logs.ascx", Model.Logs); %></div>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>


