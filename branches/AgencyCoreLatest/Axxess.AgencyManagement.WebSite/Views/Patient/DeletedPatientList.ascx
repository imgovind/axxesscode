﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Deleted Patients | <%= Current.AgencyName %></span>
<div class="wrapper">
    <div class="grid-bg" style="height: 50px;">
        <div class="buttons">
            <ul class="float-right">
                <li>
                    <%= Html.ActionLink("Excel Export", "DeletedPatients", "Export", new { BranchId = Guid.Empty }, new { @id = "List_Patient_Deleted_ExportLink", @class = "excel" })%>
                </li>
            </ul>
        </div>
        <fieldset class="orders-filter">
            <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]); %>
            <div class="buttons float-right">
                <ul>
                    <li>
                        <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"U.RebindDataGridContent('{0}','Patient/DeletedPatientContent',{{  BranchId: $('#{0}_BranchId').val() }},'{1}');\">Generate</a>", "List_Patient_Deleted", sortParams)%>
                    </li>
                </ul>
            </div>
            <label class="strong">
                Branch:
                <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "List_Patient_Deleted_BranchId" })%>
            </label>
            <div style="display:inline-block" id="List_Patient_Deleted_Grid_Search"></div>
        </fieldset>
    </div>
    <div id="List_Patient_DeletedGridContainer" class="">
        <% Html.RenderPartial("DeletedPatientListContent", Model); %>
    </div>
</div>

<script type="text/javascript">

    $("#window_listdeletedpatients_content").css({
        "background-color": "#d6e5f3"
    });
    $("#List_Patient_Deleted_Grid_Search").append($("<div/>").GridSearchById("#List_Patient_Deleted_Grid"));
    $('#window_listdeletedpatients_content .grid-search').css({ 'position': 'relative', 'left': '0', 'margin-left': '0px' });
</script>

