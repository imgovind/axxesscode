﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Existing Referral List | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Referrals", "Export", FormMethod.Post)) { %>
<%  var action = Current.HasRight(Permissions.ManageReferrals) ? "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditReferral('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Delete('<#=Id#>');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Admit('<#=Id#>');\">Admit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowNonAdmitReferralModal('<#=Id#>');\" class=\"\">Non-Admit</a>" : string.Empty;%>
<%  var visible = Current.HasRight(Permissions.ManageReferrals) && !Current.IsAgencyFrozen; %>
<%= Html.Telerik().Grid<ReferralData>().Name("List_Referral").ToolBar(commnds => commnds.Custom()).Columns(columns => {
        columns.Bound(r => r.ReferralDate).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width(95);
        columns.Bound(r => r.DisplayName).Title("Name");
        columns.Bound(r => r.AdmissionSource).Title("Referral Source");
        columns.Bound(r => r.DateOfBirth).Title("Date of Birth").Width(95);
        columns.Bound(r => r.Gender).Title("Gender").Width(75);
        columns.Bound(r => r.Status).Title("Status").Width(75).Sortable(true);
        columns.Bound(r => r.CreatedBy).Title("Created By").Width(175).Sortable(true);
        columns.Bound(r => r.Id).ClientTemplate("<a href='javascript:void(0)' onclick=\"U.GetAttachment('Referral/ReferralPdf',{id:'<#=Id#>'})\"><span class='img icon print'></span></a>").Title("").Width(30);
        columns.Bound(r => r.Id).ClientTemplate(action).Title("Action").Width(200).Sortable(false).Visible(visible);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Referral")).ClientEvents(events => events.OnDataBound("Referral.BindGridButton")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Referral .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManageReferrals) && !Current.IsAgencyFrozen) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Referral", Click: UserInterface.ShowNewReferral } ])
    )<% } 
        if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>