﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<SurveyCensus>>" %>
<% string pagename = "PatientSurveyCensusByDateRange"; %>
<div class="wrapper">
  <fieldset>
    <legend>Survey Census (By Date Range)</legend>
    <div class="column">
        <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
        <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected >Active</option><option value="2">Discharged</option></select></div></div>
        <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%=Html.Insurances("InsuranceId", "0", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div>
        <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
    </div>
    <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
    <div class="buttons"><ul><li> <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientSurveyCensusByDateRangeContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), InsuranceId: $('#{0}_InsuranceId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="GetSurveyCensus()">Export to Excel</a></li></ul></div>
  </fieldset>
  <div id="<%= pagename %>GridContainer" class="report-grid">
    <% Html.RenderPartial("Patient/Content/SurveyCensusByDateRangeContent", Model); %>
  </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>', 'All', true); });
    function GetSurveyCensus() {
        insuranceNumber = $('#PatientSurveyCensusByDateRange_InsuranceId').val();
        statusId = $('#PatientSurveyCensusByDateRange_StatusId').val();
        branchCode = $('#PatientSurveyCensusByDateRange_BranchCode').val();
        startDate = $('#PatientSurveyCensusByDateRange_StartDate').val();
        endDate = $('#PatientSurveyCensusByDateRange_EndDate').val();
        U.GetAttachment("Report/ExportPatientSurveyCensusByDateRange", { BranchCode: branchCode, StatusId: statusId, InsuranceId: insuranceNumber, StartDate: startDate, EndDate: endDate });
    }
</script>