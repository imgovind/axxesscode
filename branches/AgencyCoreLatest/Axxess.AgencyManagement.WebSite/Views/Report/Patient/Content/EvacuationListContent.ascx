﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEvacuation>>" %>
<% string pagename = "PatientEmergencyList"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
   {
       columns.Bound(p => p.PatientName).Title("Patient").Width("9%");
       columns.Bound(p => p.Triage).Title("Triage").Width("3%");
       columns.Bound(p => p.ContactName).Title("Contact").Width("9%");
       columns.Bound(p => p.ContactRelation).Title("Relationship").Width("9%");
       columns.Bound(p => p.ContactPhoneHome).Title("Contact Phone").Width("9%");
       columns.Bound(p => p.ContactEmailAddress).Title("Contact E-mail").Width("13%");
       columns.Bound(p => p.Address).Title("Evacuation Address").Width("23%");
       columns.Bound(p => p.EvacuationZone).Title("Evacuation Zone").Width("7%");
       columns.Bound(p => p.EvacuationPhoneHomeFormatted).Title("Evacuation Home Phone").Width("9%");
       columns.Bound(p => p.EvacuationPhoneMobileFormatted).Title("Evacuation Mobile").Width("9%");
   })
    //.DataBinding(dataBinding => dataBinding.Ajax().Select("PatientEmergencyList", "Report", new { StatusId = 1, BranchCode = Guid.Empty }))
           .Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "PatientName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientName).Descending();
                                      }
                                  }
                                  else if (sortName == "ContactName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ContactName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ContactName).Descending();
                                      }
                                  }
                              })
                      )
     .Scrollable()
     .Footer(false)
%>
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PatientEmergencyPreparednessListContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>