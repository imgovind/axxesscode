﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ReferralInfo>>" %>
<% string pagename = "ReferralLog"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
   {
       columns.Bound(p => p.ReferralName).Title("Name").Width("10%");
       columns.Bound(p => p.ReferralDate).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width("10%");
       columns.Bound(p => p.PhysicianName).Title("Referral Physician").Width("10%");
       columns.Bound(p => p.Gender).Title("Gender").Width("10%");
       columns.Bound(p => p.DOB).Format("{0:MM/dd/yyyy}").Title("Date Of Birth").Width("10%");
       columns.Bound(p => p.Address).Title("Address").Width("10%");
       columns.Bound(p => p.StatusName).Title("Status").Width("10%");
       columns.Bound(p => p.ServiceRequiredName).Title("Service Required").Width("10%");
       columns.Bound(p => p.OtherReferralSource).Title("Other Referral Source").Width("10%");
   })
           .Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "ReferralName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ReferralName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ReferralName).Descending();
                                      }
                                  }
                                  else if (sortName == "ReferralDate")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ReferralDate).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ReferralDate).Descending();
                                      }
                                  }
                              })
                      )
     .Scrollable()
     .Footer(false)
%>
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ReferralLogContent',{  Status : \"" + $('#<%= pagename %>_Status').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>