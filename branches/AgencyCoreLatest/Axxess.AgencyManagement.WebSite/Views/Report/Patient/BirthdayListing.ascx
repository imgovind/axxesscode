﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Birthday Listing</legend>
        <div class="column"><div class="row"><label for="Report_Patient_BL_BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = "Report_Patient_BL_BranchCode", @class = "AddressBranchCode report_input" })%></div></div></div>
        <div class="column"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientBirthdayListing();">Generate Report</a></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<Birthday>().Name("Report_Patient_BL_Grid").Columns(columns =>
           {
               columns.Bound(p => p.Name).Width(150);
               columns.Bound(p => p.Age).Width(50);
               columns.Bound(p => p.BirthDay).Width(130);
               columns.Bound(p => p.AddressFirstRow);
               columns.Bound(p => p.AddressSecondRow);
               columns.Bound(p => p.PhoneHomeFormatted).Title("Home Phone").Width(110);
           })
           .DataBinding(dataBinding => dataBinding.Ajax().Select("PatientBirthdayListing", "Report", new { AddressBranchCode = Guid.Empty }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $("#Report_Patient_BL_Grid .t-grid-content").css({ 'height': 'auto' });</script>