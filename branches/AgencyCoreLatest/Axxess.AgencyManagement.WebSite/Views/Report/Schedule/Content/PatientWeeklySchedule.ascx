﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "SchedulePatientWeeklySchedule"; %>
        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
             .Columns(columns =>
                 {
                 columns.Bound(s => s.DisciplineTaskName).Sortable(false).Title("Task");
                 columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
                 columns.Bound(p => p.EventDate).Sortable(false).Title("Schedule Date").Width(100);
                 columns.Bound(p => p.VisitDate).Sortable(false).Title("Visit Date").Width(80);
                 columns.Bound(s => s.UserName).Title("Employee");
               })
              // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { patientId = Guid.Empty}))
                            .Sortable(sorting =>
                                  sorting.SortMode(GridSortMode.SingleColumn)
                                      .OrderBy(order =>
                                      {
                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                          if (sortName == "UserName")
                                          {
                                              if (sortDirection == "ASC")
                                              {
                                                  order.Add(o => o.UserName).Ascending();
                                              }
                                              else if (sortDirection == "DESC")
                                              {
                                                  order.Add(o => o.UserName).Descending();
                                              }

                                          }
                                         
                                      })
                              )
                       .Scrollable()
                               .Footer(false)%>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PatientWeeklyScheduleContent',{  PatientId : \"" + $('#<%= pagename %>_PatientId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
