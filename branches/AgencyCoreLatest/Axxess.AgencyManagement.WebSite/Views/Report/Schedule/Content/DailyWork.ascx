﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleDailyWork"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(70);
         columns.Bound(m => m.PatientName).Title("Patient");
         columns.Bound(s => s.DisciplineTaskName).Title("Task").Sortable(false);
         columns.Bound(p => p.StatusName).Title("Status").Sortable(false);
         columns.Bound(p => p.EventDate).Title("Schedule Date").Sortable(false).Width(100);
         columns.Bound(s => s.UserName).Title("Employee");
       })
              // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, Date = DateTime.Now }))
               .Sortable(sorting =>
                              sorting.SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                      if (sortName == "PatientIdNumber")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Descending();
                                          }

                                      }
                                      else if (sortName == "PatientName")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientName).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientName).Descending();
                                          }

                                      }
                                      else if (sortName == "UserName")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.UserName).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.UserName).Descending();
                                          }

                                      }

                                  })
                                  )
                               .Scrollable()
                                       .Footer(false)%>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','DailyWorkContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", Date : \"" + $('#<%= pagename %>_Date').val()  + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
  </script>
 
