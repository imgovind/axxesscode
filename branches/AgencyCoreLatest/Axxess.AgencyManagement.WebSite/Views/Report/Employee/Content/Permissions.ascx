﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserPermission>>" %>
<% string pagename = "EmployeePermissions"; %>

<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
{
   // columns.Bound(s => s.UserDisplayName).Sortable(true).Visible(false);
    columns.Bound(p => p.Permission).Title("Permission");
})
        .Groupable(settings => settings.Groups(groups =>
        {
            groups.Add(s => s.Employee);
        }))
               .Sortable(sorting =>
                sorting.SortMode(GridSortMode.SingleColumn)
                    .OrderBy(order =>
                    {
                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                    })
            )
        .Scrollable()
        .Footer(false)%>
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeePermissionsContent',{ BranchId : \"" + $('#<%= pagename %>_BranchCode').val() + "\"  , StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>Grid .t-grouping-header").remove();
 </script>