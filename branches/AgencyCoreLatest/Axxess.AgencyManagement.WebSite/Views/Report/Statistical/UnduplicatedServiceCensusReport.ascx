﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalUnduplicatedServiceCensusReport"; %>
<div class="wrapper">
    <fieldset>
        <legend>Unduplicated Census Report By Date Range</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
           <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
         <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','StatisticalUnduplicatedServiceCensusReportContent',{{ BranchCode: $('#{0}_BranchCode').val(),StatusId: $('#{0}_StatusId').val(),  StartDate: $('#{0}_StartDate').val(),  EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalUnduplicatedServiceCensusReport", new { BranchCode = Guid.Empty, StatusId = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now.Date }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Statistical/Content/UnduplicatedServiceCensusReport", Model); %>
    </div>
    
</div>
