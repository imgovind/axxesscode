﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VisitsByPayor>>" %>
<% string pagename = "VisitsByPayor"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                     columns.Bound(m => m.InsuranceName).Title("Insurance").Width(150).Sortable(false);
                     columns.Bound(m => m.RN).Title("RN").Sortable(false);
                     columns.Bound(m => m.LPNLVN).Title("LPN/LVN").Sortable(false);
                     columns.Bound(m => m.PT).Title("PT").Sortable(false);
                     columns.Bound(m => m.PTA).Title("PTA").Sortable(false);
                     columns.Bound(m => m.OT).Title("OT").Sortable(false);
                     columns.Bound(m => m.COTA).Title("COTA").Sortable(false);
                     columns.Bound(m => m.ST).Title("ST").Sortable(false);
                     columns.Bound(m => m.MSW).Title("MSW").Sortable(false);
                     columns.Bound(m => m.Dietician).Title("Dietician").Sortable(false);
                     columns.Bound(m => m.HHA).Title("HHA").Sortable(false);
                     columns.Bound(m => m.PCW).Title("PCW").Sortable(false);
                     columns.Bound(m => m.HMK).Title("HMK").Sortable(false);
                     columns.Bound(m => m.Total).Title("Total").Sortable(false);
               })
               .Scrollable().Footer(false)  %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ScheduleVisitsByTypeContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", PatientId : \"" + $('#<%= pagename %>_Patient').val() + "\", ClinicianId : \"" + $('#<%= pagename %>_Clinician').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\", Type : \"" + $('#<%= pagename %>_Type').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>