﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Medicare Cost Report</legend>
        <div class="column">
            <div class="row">
                <label for="CostReport_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "CostReport_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="CostReport_DateRange" class="float-left">Date Range:</label>
                <div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddMonths(-DateTime.Now.Month+1).AddDays(-DateTime.Now.Day+1).ToShortDateString() %>" id="CostReport_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddYears(1).AddMonths(-DateTime.Now.Month+1).AddDays(-DateTime.Now.Day).ToShortDateString() %>" id="CostReport_EndDate" /></div>
                </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReportWithRange('/Request/CostReport', '#CostReport_BranchId', '#CostReport_StartDate','#CostReport_EndDate');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
