﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patients And Visits By Age</legend>
        <div class="column">
            <div class="row">
                <label for="PatientsAndVisitsByAge_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "PatientsAndVisitsByAge_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="PatientsAndVisitsByAge_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "PatientsAndVisitsByAge_Year" })%></div>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/PatientsAndVisitsByAgeReport', '#PatientsAndVisitsByAge_BranchId', '#PatientsAndVisitsByAge_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
