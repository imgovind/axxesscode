﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ClaimsByStatus"; %>
<div class="wrapper">
    <fieldset>
        <legend> Claims History by Status </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "All", Value = "All" }, new SelectListItem { Text = "RAP", Value = "RAP" }, new SelectListItem { Text = "Final", Value = "Final" }}, "Value", "Text", "All");%><%= Html.DropDownList("Type", billType, new { @id = pagename + "_Type", @class = "oe" })%></div> </div>
            <div class="row"><label  class="float-left">Bill Status:</label><div class="float-right"><%= Html.BillStatus("Status", "300", false, new { @id = pagename + "_Status" })%></div></div> 
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ClaimsByStatusContent',{{ BranchCode: $('#{0}_BranchCode').val(),Status: $('#{0}_Status').val(),Type: $('#{0}_Type').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClaimsByStatus", new { BranchCode = Guid.Empty, Type = "All", Status = 300, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now}, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/ClaimByStatus", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
</script>
