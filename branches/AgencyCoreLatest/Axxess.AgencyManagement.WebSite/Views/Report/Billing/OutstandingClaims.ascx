﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TypeOfBill>>" %>
<% string pagename = "OutstandingClaims"; %>
<div class="wrapper">
    <fieldset>
        <legend>Outstanding Claims</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "-- All Types--", Value = "All" }, new SelectListItem { Text = "RAP", Value = "RAP" }, new SelectListItem { Text = "Final", Value = "Final" }}, "Value", "Text", "All");%><%= Html.DropDownList("Type", billType, new { @id = pagename + "_Type", @class = "oe" })%></div> </div>
            <div class="row"><label class="float-left">Payor:</label><div class="float-right"><%= Html.AllInsurances("PayorId", "0", true, "-- All Payors --", new { @id = pagename+"_PayorId" })%></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','OutstandingClaimsContent',{{ BranchCode: $('#{0}_BranchCode').val(),Type: $('#{0}_Type').val(), InsuranceId: $('#{0}_PayorId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="U.GetAttachment('Report/ExportOutstandingClaims',{'BranchCode': $('#<%=pagename %>_BranchCode').val(), 'Type': $('#<%=pagename %>_Type').val(), 'InsuranceId':$('#<%=pagename %>_PayorId').val()});">Export to Excel</a></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/OutstandingClaims", Model); %>
    </div>
</div>
