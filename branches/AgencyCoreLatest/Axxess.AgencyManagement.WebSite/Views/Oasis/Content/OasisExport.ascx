﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AssessmentExport>>" %>
<% string pagename = "OasisExport"; %>
<%  Html.Telerik()
                .Grid(Model)
                .Name(pagename + "_Grid")
                .HtmlAttributes(new { @style = "top:90px;bottom:40px;" }).Columns(columns =>
            {
                columns.Bound(o => o.AssessmentId).Template(t =>
                { %><%= (!t.IsValidated || t.IsSubmissionEmpty) ? "":string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", t.Identifier) %><% }).Title("").Width(30).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
                columns.Bound(o => o.Index).Title("#").Width(30);
                columns.Bound(o => o.PatientName).Title("Patient Name");
                columns.Bound(o => o.AssessmentName).Title("Assessment Type").Sortable(true);
                columns.Bound(o => o.AssessmentDateFormatted).Title("Assessment Date").Width(125).Sortable(true);
                columns.Bound(o => o.EpisodeRange).Width(150).Title("Episode").Sortable(true);
                columns.Bound(o => o.Insurance).Sortable(true);
                columns.Bound(o => o.AssessmentId).Template(t => (!t.IsValidated || t.IsSubmissionEmpty) ? string.Format("<a href=\"javaScript:void(0);\" onclick=\"OasisValidation.ExportRegenerate('{0}','{1}','{2}','{3}');\">Generate</a>", t.AssessmentId, t.PatientId, t.EpisodeId, t.AssessmentType) : string.Format("{4:00} ( <a href=\"javaScript:void(0);\" onclick=\"UserInterface.ShowCorrectionNumberModal('{0}','{1}','{2}','{3}','{4}');\"> Edit </a>  )", t.AssessmentId, t.PatientId, t.EpisodeId, t.AssessmentType, t.CorrectionNumber)).Title("Correction #").Width(100).Visible(!Current.IsAgencyFrozen).Sortable(false);
            }).Footer(false).Scrollable().Sortable(sorting =>
                 sorting.SortMode(GridSortMode.SingleColumn)
                     .OrderBy(order =>
                     {
                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                         if (sortName == "PatientName")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.PatientName).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.PatientName).Descending();
                             }
                         }
                         else if (sortName == "AssessmentName")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.AssessmentName).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.AssessmentName).Descending();
                             }
                         }
                         else if (sortName == "Index")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.Index).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.Index).Descending();
                             }
                         }
                         else if (sortName == "Insurance")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.Insurance).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.Insurance).Descending();
                             }
                         }
                         else if (sortName == "EpisodeRange")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.EpisodeRange).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.EpisodeRange).Descending();
                             }
                         }
                         else if (sortName == "AssessmentDateFormatted")
                         {
                             if (sortDirection == "ASC")
                             {
                                 order.Add(o => o.AssessmentDateFormatted).Ascending();
                             }
                             else if (sortDirection == "DESC")
                             {
                                 order.Add(o => o.AssessmentDateFormatted).Descending();
                             }
                         }

                     })
                     ).Render(); %>
<script type="text/javascript">
    
    // Set Height of Grid if Mobile
    if (Acore.Mobile) $('#OasisExport_Grid').css('top', 105);

    var branchId = $("#OasisExport_BranchId").val();
    var paymentSources = U.PullMultiComboBoxValues($("#OasisExport_PaymentSources"));
    var startDate = $("#OasisExport_StartDate").val();
    var endDate = $("#OasisExport_EndDate").val();
    $("#<%=pagename %>_GridContainer #<%=pagename %>_Grid .t-grid-content").css({ "height": "auto", "position": "absolute", "top": "25px" });
    $("#<%= pagename %>_Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "U.RebindDataGridContent('<%= pagename %>_','Oasis/Export',{  BranchId : \"" + branchId + "\", paymentSources : \"" + paymentSources + "\", startDate : \"" + startDate + "\", endDate :\"" + endDate + "\" },'" + U.ParameterByName(link, '<%= pagename %>_Grid-orderBy') + "');");
    });
</script>