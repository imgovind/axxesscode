﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>"  %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5||(Model.Version==4 && Model.AssessmentTypeNum.ToInteger() % 10==9)) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "OrdersDisciplineTreatmentForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.OrdersDisciplineTreatment.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <div id="<%= Model.TypeName %>_BlankMasterCalendar"></div>
    
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() == 1) { %>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInitialSummaryComments" class="strong">Initial Summary of Patient History:</label>
                <%= Html.Templates(Model.TypeName + "_485SkilledInitialSummaryTemplate", new { @class = "Templates", @template = "#" + Model.TypeName + "_485SkilledInitialSummaryComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485SkilledInitialSummaryComments", data.AnswerOrEmptyString("485SkilledInitialSummaryComments"), 5, 70, new { @id = Model.TypeName + "_485SkilledInitialSummaryComments", @style = "height:150px", @title = "(Optional) Narrative" })%>
            </div>
            <% } %>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionComments" class="strong">Skilled Intervention/Teaching:</label>
                <%= Html.Templates(Model.TypeName + "_485SkilledInterventionTemplate", new { @class = "Templates", @template = "#" + Model.TypeName + "_485SkilledInterventionComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485SkilledInterventionComments", data.AnswerOrEmptyString("485SkilledInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485SkilledInterventionComments", @style = "height:150px", @title = "(Optional) Narrative" })%>
                <%  string[] iResponse = data.AnswerArray("485SIResponse"); %>
                <%= Html.Hidden(Model.TypeName + "_485SIResponse", "", new { @id = Model.TypeName + "_485SIResponseHidden" })%>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Verbalizes Understanding' id='{0}_485SIResponse1' name='{0}_485SIResponse' value='1' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("1").ToChecked()) %>
                        <span class="radio">
                            <label for="<%= Model.TypeName %>_485SIResponse1">Verbalizes</label>
                            <%= Html.TextBox(Model.TypeName + "_485SIVerbalizedUnderstandingPercent", data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPercent"), new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPercent" })%>
                            <label for="<%= Model.TypeName %>_485SIResponse1">understanding of teaching.</label>
                            <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingPT", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Verbalizes Understanding' id='{0}_485SIVerbalizedUnderstandingPT' class='no_float' name='{0}_485SIVerbalizedUnderstandingPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIVerbalizedUnderstandingPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingCG", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingCGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Verbalizes Understanding' id='{0}_485SIVerbalizedUnderstandingCG' class='no_float' name='{0}_485SIVerbalizedUnderstandingCG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485SIVerbalizedUnderstandingCG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIVerbalizedUnderstandingCG">CG</label>
                        </span>
                    </div>
                    
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Able to Return Demonstration' id='{0}_485SIResponse3' name='{0}_485SIResponse' value='3' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("3").ToChecked()) %>
                        <span class="radio">
                            <label for="<%= Model.TypeName %>_485SIResponse3">Able to return correct demonstration of procedure.</label>
                            <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Able to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDUREPT' class='no_float' name='{0}_485DEMONSTRATIONOFPROCEDUREPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDUREPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485DEMONSTRATIONOFPROCEDUREPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Able to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDURECG' class='no_float' name='{0}_485DEMONSTRATIONOFPROCEDURECG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDURECG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485DEMONSTRATIONOFPROCEDURECG">CG</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Unable to Return Demonstration' id='{0}_485SIResponse4' name='{0}_485SIResponse' value='4' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("4").ToChecked()) %>
                        <span class="radio">
                            <label for="<%= Model.TypeName %>_485SIResponse4">Unable to return correct demonstration of procedure.</label>
                            <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Unable to Return Demonstration' id='{0}_485UNDEMONSTRATIONOFPROCEDUREPT' class='no_float' name='{0}_485UNDEMONSTRATIONOFPROCEDUREPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDUREPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485UNDEMONSTRATIONOFPROCEDUREPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Unable to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDURECG' class='no_float' name='{0}_485UNDEMONSTRATIONOFPROCEDURECG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDURECG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485UNDEMONSTRATIONOFPROCEDURECG">CG</label>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Coordination of Care</legend>
        <div class="wide-column">
             <div class="row">
                <label class="strong">Conferenced With:</label>
                <%  string[] conferencedWithName = data.AnswerArray("485ConferencedWith"); %>
                <%= Html.Hidden(Model.TypeName + "_485ConferencedWith", "", new { @id = Model.TypeName + "_485ConferencedWithHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Medical Doctor' id='{0}_485ConferencedWithMD' name='{0}_485ConferencedWith' value='MD' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("MD").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithMD">MD</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Skilled Nurse' id='{0}_485ConferencedWithSN' name='{0}_485ConferencedWith' value='SN' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("SN").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithSN">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Physical Therapist' id='{0}_485ConferencedWithPT' name='{0}_485ConferencedWith' value='PT' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("PT").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithPT">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Occupational Therapist' id='{0}_485ConferencedWithOT' name='{0}_485ConferencedWith' value='OT' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("OT").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithOT">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Speech Therapist' id='{0}_485ConferencedWithST' name='{0}_485ConferencedWith' value='ST' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("ST").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithST">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Medical Social Worker' id='{0}_485ConferencedWithMSW' name='{0}_485ConferencedWith' value='MSW' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("MSW").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithMSW">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Home Health Aide' id='{0}_485ConferencedWithHHA' name='{0}_485ConferencedWith' value='HHA' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("HHA").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithHHA">HHA</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485ConferencedWithName" class="float-left">Name:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_485ConferencedWithName", data.AnswerOrEmptyString("485ConferencedWithName"), new { @id = Model.TypeName + "_485ConferencedWithName", @maxlength = "30", @title = "(Optional) Coordination of Care Name" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionRegarding" class="strong">Regarding:</label>
                <%= Html.TextArea(Model.TypeName + "_485SkilledInterventionRegarding", data.AnswerOrEmptyString("485SkilledInterventionRegarding"), 5, 70, new { @id = Model.TypeName + "_485SkilledInterventionRegarding", @title = "(Optional) Coordination of Care Regarding" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Discharge Reason</legend>
        <div>
            <%= Html.DischargeReasons(Model.TypeName + "_GenericDischargeReasonId", Convert.ToInt32(data.AnswerOrEmptyString("GenericDischargeReasonId").IsNullOrEmpty() ? "0" : data.AnswerOrEmptyString("GenericDischargeReasonId")), new { @id = Model.TypeName + "_GenericDischargeReasonId", @class = "" })%>
        </div>
        <div><label class="strong">Comments:</label></div>
        <div><%= Html.TextArea(Model.TypeName + "_GenericDischargeReasonComments", data.AnswerOrEmptyString("GenericDischargeReasonComments"), new { @id=Model.TypeName+"_GenericDischargeReasonComments", @class="fill" })%></div>
    </fieldset>
     <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    $("#<%= Model.TypeName %>_485SIVerbalizedUnderstandingPercent").Autocomplete({
        source: [ "GOOD", "NO", "FAIR", "POOR", "0%", "100%", "50%", "FULL", "PARTIAL" ]
    })
</script>