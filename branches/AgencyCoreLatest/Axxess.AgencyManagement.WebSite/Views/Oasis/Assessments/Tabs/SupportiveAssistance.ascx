<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main"> 
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SupportiveAssistanceForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.SupportiveAssistance.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
    <fieldset class="oasis nomobile">
        <legend>Patient Living Situation</legend>
        <%= Html.Hidden(Model.TypeName + "_M1100LivingSituation", "", new { @id = Model.TypeName + "_M1100LivingSituationHidden" })%>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1100">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1100" class="green" onclick="Oasis.ToolTip('M1100')">(M1100)</a>
                    Patient Living Situation: Which of the following best describes the patient&#8217;s residential circumstance and availability of assistance?
                    <em>(Check one box only)</em>
                </label>
                <table class="form align-center">
                    <thead>
                        <tr>
                            <th colspan="3">Living Arrangement</th>
                            <th colspan="5">Availability of Assistance</th>
                        </tr>
                        <tr>
                            <th colspan="3"></th>
                            <th>Around the Clock</th>
                            <th>Regular Daytime</th>
                            <th>Regular Nighttime</th>
                            <th>Occasional/ Short-term Assistance</th>
                            <th>No Assistance Available</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup padfix">
                        <tr>
                            <td colspan="3" class="strong align-left">a. Patient lives alone</td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "01", data.AnswerOrEmptyString("M1100LivingSituation").Equals("01"), new { @id = Model.TypeName + "_M1100LivingSituation01", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives Alone with Assistance Around the Clock" })%>
                                    <label for="M1100LivingSituation01">01</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "02", data.AnswerOrEmptyString("M1100LivingSituation").Equals("02"), new { @id = Model.TypeName + "_M1100LivingSituation02", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives Alone with Assistance Regular Daytime" })%>
                                    <label for="M1100LivingSituation02">02</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "03", data.AnswerOrEmptyString("M1100LivingSituation").Equals("03"), new { @id = Model.TypeName + "_M1100LivingSituation03", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives Alone with Assistance Regular Nighttime" })%>
                                    <label for="M1100LivingSituation03">03</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "04", data.AnswerOrEmptyString("M1100LivingSituation").Equals("04"), new { @id = Model.TypeName + "_M1100LivingSituation04", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives Alone with Short-term Assistance" })%>
                                    <label for="M1100LivingSituation04">04</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "05", data.AnswerOrEmptyString("M1100LivingSituation").Equals("05"), new { @id = Model.TypeName + "_M1100LivingSituation05", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives Alone with No Assistance Available" })%>
                                    <label for="M1100LivingSituation05">05</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong align-left">b. Patient lives with other person(s) in the home</td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "06", data.AnswerOrEmptyString("M1100LivingSituation").Equals("06"), new { @id = Model.TypeName + "_M1100LivingSituation06", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives with Other(s) at Home with Assistance Around the Clock" })%>
                                    <label for="M1100LivingSituation06">06</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "07", data.AnswerOrEmptyString("M1100LivingSituation").Equals("07"), new { @id = Model.TypeName + "_M1100LivingSituation07", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives with Other(s) at Home with Assistance Regular Daytime" })%>
                                    <label for="M1100LivingSituation07">07</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "08", data.AnswerOrEmptyString("M1100LivingSituation").Equals("08"), new { @id = Model.TypeName + "_M1100LivingSituation08", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives with Other(s) at Home with Assistance Regular Nighttime" })%>
                                    <label for="M1100LivingSituation08">08</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "09", data.AnswerOrEmptyString("M1100LivingSituation").Equals("09"), new { @id = Model.TypeName + "_M1100LivingSituation09", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives with Other(s) at Home with Short-term Assistance" })%>
                                    <label for="M1100LivingSituation09">09</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "10", data.AnswerOrEmptyString("M1100LivingSituation").Equals("10"), new { @id = Model.TypeName + "_M1100LivingSituation10", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Lives with Other(s) at Home with No Assistance Available" })%>
                                    <label for="M1100LivingSituation10">10</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong align-left">c. Patient lives in congregate situation (e.g., assisted living)</td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "11", data.AnswerOrEmptyString("M1100LivingSituation").Equals("11"), new { @id = Model.TypeName + "_M1100LivingSituation11", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Congregate Situation with Assistance Around the Clock" })%>
                                    <label for="M1100LivingSituation11">11</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "12", data.AnswerOrEmptyString("M1100LivingSituation").Equals("12"), new { @id = Model.TypeName + "_M1100LivingSituation12", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Congregate Situation with Assistance Regular Daytime" })%>
                                    <label for="M1100LivingSituation12">12</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "13", data.AnswerOrEmptyString("M1100LivingSituation").Equals("13"), new { @id = Model.TypeName + "_M1100LivingSituation13", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Congregate Situation with Assistance Regular Nighttime" })%>
                                    <label for="M1100LivingSituation13">13</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "14", data.AnswerOrEmptyString("M1100LivingSituation").Equals("14"), new { @id = Model.TypeName + "_M1100LivingSituation14", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Congregate Situation with Short-term Assistance" })%>
                                    <label for="M1100LivingSituation14">14</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1100LivingSituation", "15", data.AnswerOrEmptyString("M1100LivingSituation").Equals("15"), new { @id = Model.TypeName + "_M1100LivingSituation15", @class = "radio", @title = "(OASIS M1100) Patient Living Situation, Congregate Situation with No Assistance Available" })%>
                                    <label for="M1100LivingSituation15">15</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1100')" title="More Information about M1100">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Social Service Screening</legend>
        <div class="column">
            <div class="row">
                <label class="strong">Community resource info needed to manage care</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericCommunityResourceInfoNeeded", "", new { @id = Model.TypeName + "_GenericCommunityResourceInfoNeededHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCommunityResourceInfoNeeded", "1", data.AnswerOrEmptyString("GenericCommunityResourceInfoNeeded").Equals("1"), new { @id = Model.TypeName + "_GenericCommunityResourceInfoNeededYes", @class = "radio", @title = "(Optional) Community Resource Information Needed, Yes" }) %>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GenericCommunityResourceInfoNeededYes">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCommunityResourceInfoNeeded", "0", data.AnswerOrEmptyString("GenericCommunityResourceInfoNeeded").Equals("0"), new { @id = Model.TypeName + "_GenericCommunityResourceInfoNeededNo", @class = "radio", @title = "(Optional) Community Resource Information Needed, No" })%>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GenericCommunityResourceInfoNeededNo">No</label>
                </div>
            </div>
            <div class="row">
                <label class="strong">Altered affect, e.g., express sadness or anxiety, grief </label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericAlteredAffect", "", new { @id = Model.TypeName + "_GenericAlteredAffectHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericAlteredAffect", "1", data.AnswerOrEmptyString("GenericAlteredAffect").Equals("1"), new { @id = Model.TypeName + "_GenericAlteredAffectYes", @class = "radio", @title = "(Optional) Community Resource Information Needed, Yes" })%>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GenericAlteredAffectYes">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericAlteredAffect", "0", data.AnswerOrEmptyString("GenericAlteredAffect").Equals("0"), new { @id = Model.TypeName + "_GenericAlteredAffectNo", @class = "radio", @title = "(Optional) Community Resource Information Needed, No" })%>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GGenericAlteredAffectNo">No</label>
                </div>
            </div>
            <div class="row">
                <label class="strong">Suicidal ideation</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericSuicidalIdeation", "", new { @id = Model.TypeName + "_GenericSuicidalIdeationHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericSuicidalIdeation", "1", data.AnswerOrEmptyString("GenericSuicidalIdeation").Equals("1"), new { @id = Model.TypeName + "_GenericSuicidalIdeationYes", @class = "radio", @title = "(Optional) Community Resource Information Needed, Yes" })%>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GenericSuicidalIdeationYes">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericSuicidalIdeation", "0", data.AnswerOrEmptyString("GenericSuicidalIdeation").Equals("0"), new { @id = Model.TypeName + "_GenericSuicidalIdeationNo", @class = "radio", @title = "(Optional) Community Resource Information Needed, No" })%>
                    <label class="inline-radio" for="<%= Model.TypeName %>_GenericSuicidalIdeationNo">No</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float-left">
                    <label for="<%= Model.TypeName %>_GenericMSWIndicatedForWhat" class="strong">MSW referral indicated for</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMSWIndicatedForWhat", data.AnswerOrEmptyString("GenericMSWIndicatedForWhat"), new { @id = Model.TypeName + "_GenericMSWIndicatedForWhat", @maxlength="150", @title = "(Optional) MSW Referral Reason" }) %>
                </div>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericMSWIndicatedFor", "", new { @id = Model.TypeName + "_GenericMSWIndicatedForHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericMSWIndicatedFor", "Yes", data.AnswerOrEmptyString("GenericMSWIndicatedFor").Equals("Yes"), new { @id = Model.TypeName + "_GenericMSWIndicatedForYes", @class = "radio", @title = "(Optional) MSW Referral, Yes" })%>
                    <label for="GenericMSWIndicatedForYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericMSWIndicatedFor", "No", data.AnswerOrEmptyString("GenericMSWIndicatedFor").Equals("No"), new { @id = Model.TypeName + "_GenericMSWIndicatedForNo", @class = "radio", @title = "(Optional) MSW Referral, No" })%>
                    <label for="GenericMSWIndicatedForNo" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Coordinator notified</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericCoordinatorNotified", "", new { @id = Model.TypeName + "_GenericCoordinatorNotifiedHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCoordinatorNotified", "Yes", data.AnswerOrEmptyString("GenericCoordinatorNotified").Equals("Yes"), new { @id = Model.TypeName + "_GenericCoordinatorNotifiedYes", @class = "radio", @title = "(Optional) Coordinator Notified, Yes" })%>
                    <label for="GenericCoordinatorNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCoordinatorNotified", "No", data.AnswerOrEmptyString("GenericCoordinatorNotified").Equals("No"), new { @id = Model.TypeName + "_GenericCoordinatorNotifiedNo", @class = "radio", @title = "(Optional) Coordinator Notified, No" })%>
                    <label for="GenericCoordinatorNotifiedNo" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Ability of Patient to Handle Finances</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericAbilityHandleFinance", "", new { @id = Model.TypeName + "_GenericAbilityHandleFinanceHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericAbilityHandleFinance", "Independent", data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Independent"), new { @id = Model.TypeName + "_GenericAbilityHandleFinanceIndependent", @class = "radio", @title = "(Optional) Ability to Handle Finances, Independent" })%>
                    <label for="GenericAbilityHandleFinanceIndependent" class="inline-radio">Independent</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericAbilityHandleFinance", "Dependent", data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Dependent"), new { @id = Model.TypeName + "_GenericAbilityHandleFinanceDependent", @class = "radio", @title = "(Optional) Ability to Handle Finances, Dependent" })%>
                    <label for="GenericAbilityHandleFinanceDependent" class="inline-radio">Dependent</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericAbilityHandleFinance", "Needs assistance", data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Needs assistance"), new { @id = Model.TypeName + "_GenericAbilityHandleFinanceAssistance", @class = "radio", @title = "(Optional) Ability to Handle Finances, Needs Assistance" })%>
                    <label for="GenericAbilityHandleFinanceAssistance" class="inline-radio">Needs assistance</label>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label class="strong">Suspected Abuse/Neglect</label>
                <%  string[] genericSuspected = data.AnswerArray("GenericSuspected"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSuspected", "", new { @id = Model.TypeName + "_GenericSuspectedHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Unexplained Bruises' id='{0}_GenericSuspected1' type='checkbox' name='{0}_GenericSuspected' value='1' {1} />", Model.TypeName, genericSuspected.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected1" class="radio">Unexplained bruises</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Inadequate Food' id='{0}_GenericSuspected2' type='checkbox' name='{0}_GenericSuspected' value='2' {1} />", Model.TypeName, genericSuspected.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected2" class="radio">Inadequate food</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Fearful of Family' id='{0}_GenericSuspected3' type='checkbox' name='{0}_GenericSuspected' value='3' {1} />", Model.TypeName, genericSuspected.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected3" class="radio">Fearful of family member</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Exploitation of Funds' id='{0}_GenericSuspected4' type='checkbox' name='{0}_GenericSuspected' value='4' {1} />", Model.TypeName, genericSuspected.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected4" class="radio">Exploitation of funds</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Sexual Abuse' id='{0}_GenericSuspected5' type='checkbox' name='{0}_GenericSuspected' value='5' {1} />", Model.TypeName, genericSuspected.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected5" class="radio">Sexual abuse</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Neglect' id='{0}_GenericSuspected6' type='checkbox' name='{0}_GenericSuspected' value='6' {1} />", Model.TypeName, genericSuspected.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected6" class="radio">Neglect</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Suspencted Abuse/Neglect, Unattended' id='{0}_GenericSuspected7' type='checkbox' name='{0}_GenericSuspected' value='7' {1} />", Model.TypeName, genericSuspected.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericSuspected7" class="radio">Left unattended if constant supervision is needed</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericAlteredAffectComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericAlteredAffectComments", data.AnswerOrEmptyString("GenericAlteredAffectComments"), 5, 70, new { @id = Model.TypeName + "_GenericAlteredAffectComments", @title = "(Optional) Social Service Comments" })%>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOrgProvidingAssistanceNames" class="strong">Supportive Assistance: Names of organizations providing assistance</label>
                <%= Html.TextArea(Model.TypeName + "_GenericOrgProvidingAssistanceNames", data.AnswerOrEmptyString("GenericOrgProvidingAssistanceNames"), 5, 70, new { @id = Model.TypeName + "_GenericOrgProvidingAssistanceNames", @title = "(Optional) Supportive Assistance Organizations" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Safety/Sanitation Hazards</legend>
        <div class="wide-column">
            <div class="row">
                <label class="strong">
                    Safety/Sanitation Hazards affecting patient:
                    <em>(Select all that apply)</em>
                </label>
                <% string[] genericHazardsIdentified = data.AnswerArray("GenericHazardsIdentified"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericNoHazardsIdentified", "", new { @id = Model.TypeName + "_GenericNoHazardsIdentifiedHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, None' type='checkbox' id='{0}_GenericHazardsIdentified0' name='{0}_GenericHazardsIdentified' value='0' {1} />", Model.TypeName, genericHazardsIdentified.Contains("0").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified0" class="radio">No hazards identified</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Stairs' type='checkbox' id='{0}_GenericHazardsIdentified1' name='{0}_GenericHazardsIdentified' value='1' {1} />", Model.TypeName, genericHazardsIdentified.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified1" class="radio">Stairs</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Narrow/Obstructed Walkway' type='checkbox' id='{0}_GenericHazardsIdentified2' name='{0}_GenericHazardsIdentified' value='2' {1} />", Model.TypeName, genericHazardsIdentified.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified2" class="radio">Narrow or obstructed walkway</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, No Appliance' type='checkbox' id='{0}_GenericHazardsIdentified3' name='{0}_GenericHazardsIdentified' value='3' {1} />", Model.TypeName, genericHazardsIdentified.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified3" class="radio">No gas/ electric appliance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, No Plumbing' type='checkbox' id='{0}_GenericHazardsIdentified4' name='{0}_GenericHazardsIdentified' value='4' {1} />", Model.TypeName, genericHazardsIdentified.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified4" class="radio">No running water, plumbing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Infestation' type='checkbox' id='{0}_GenericHazardsIdentified5' name='{0}_GenericHazardsIdentified' value='5' {1} />", Model.TypeName, genericHazardsIdentified.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified5" class="radio">Insect/ rodent infestation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Cluttered/Soiled' type='checkbox' id='{0}_GenericHazardsIdentified6' name='{0}_GenericHazardsIdentified' value='6' {1} />", Model.TypeName, genericHazardsIdentified.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified6" class="radio">Cluttered/ soiled living area</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Lack of Fire Safety Devices' type='checkbox' id='{0}_GenericHazardsIdentified8' name='{0}_GenericHazardsIdentified' value='8' {1} />", Model.TypeName, genericHazardsIdentified.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified8" class="radio">Lack of fire safety devices</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Inadequate Lighting/Heating/Cooling' type='checkbox' id='{0}_GenericHazardsIdentified7' name='{0}_GenericHazardsIdentified' value='7' {1} />", Model.TypeName, genericHazardsIdentified.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified7" class="radio">Inadequate lighting, heating and cooling</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Safety/Sanitation Hazards, Other' type='checkbox' id='{0}_GenericHazardsIdentified9' name='{0}_GenericHazardsIdentified' value='9' {1} />", Model.TypeName, genericHazardsIdentified.Contains("9").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHazardsIdentified9" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_GenericHazardsIdentified9More">
                            <label for="<%= Model.TypeName %>_GenericOtherHazards"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericOtherHazards", data.AnswerOrEmptyString("GenericOtherHazards"), new { @class = "oe", @id = Model.TypeName + "_GenericOtherHazards", @maxlength = "30", @title = "(Optional) Safety/Sanitation Hazards, Specify Other" }) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHazardsComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericHazardsComments", data.AnswerOrEmptyString("GenericHazardsComments"), 5, 70, new { @id = Model.TypeName + "_GenericHazardsComments", @title = "(Optional) Safety/Sanitation Hazards, Comments" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Cultural</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPrimaryLanguage" class="float-left">Primary language?</label>
                <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericPrimaryLanguage", data.AnswerOrEmptyString("GenericPrimaryLanguage"), new { @id = Model.TypeName + "_GenericPrimaryLanguage", @title = "(Optional) Primary Language" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Does patient have cultural practices that influence health care?</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericCulturalPractices", "", new { @id = Model.TypeName + "_GenericCulturalPracticesHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCulturalPractices", "1", data.AnswerOrEmptyString("GenericCulturalPractices").Equals("1"), new { @id = Model.TypeName + "_GenericCulturalPracticesYes", @class = "radio", @title = "(Optional) Cultural Influences on Health Care, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericCulturalPracticesYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericCulturalPractices", "0", data.AnswerOrEmptyString("GenericCulturalPractices").Equals("0"), new { @id = Model.TypeName + "_GenericCulturalPracticesNo", @class = "radio", @title = "(Optional) Cultural Influences on Health Care, No" })%>
                    <label for="<%= Model.TypeName %>_GenericCulturalPracticesNo" class="inline-radio">No</label>
                </div>
                <div class="clear"></div>
                <div id="<%= Model.TypeName %>_GenericCulturalPracticesYesMore">
                    <label for="<%= Model.TypeName %>_GenericCulturalPracticesDetails">Please explain:</label>
                    <%= Html.TextArea(Model.TypeName + "_GenericCulturalPracticesDetails", data.AnswerOrEmptyString("GenericCulturalPracticesDetails"), 5, 70, new { @id = Model.TypeName + "_GenericCulturalPracticesDetails", @title = "(Optional) Cultural Influences on Health Care" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <%  string[] useOfInterpreter = data.AnswerArray("GenericUseOfInterpreter"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericUseOfInterpreter", "", new { @id = Model.TypeName + "_GenericUseOfInterpreterHidden" })%>
                <label class="strong">
                    Use of interpreter
                    <em>(Select Patient Preferences)</em>
                </label>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Interpreter, Family' class='radio' type='checkbox' id='{0}_GenericUseOfInterpreter1' name='{0}_GenericUseOfInterpreter' value='1' {1} />", Model.TypeName, useOfInterpreter.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericUseOfInterpreter1" class="fixed inline-radio">Family</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Interpreter, Friend' class='radio' type='checkbox' id='{0}_GenericUseOfInterpreter2' name='{0}_GenericUseOfInterpreter' value='2' {1} />", Model.TypeName, useOfInterpreter.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericUseOfInterpreter2" class="fixed inline-radio">Friend</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Interpreter, Professional' class='radio' type='checkbox' id='{0}_GenericUseOfInterpreter3' name='{0}_GenericUseOfInterpreter' value='3' {1} />", Model.TypeName, useOfInterpreter.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericUseOfInterpreter3" class="fixed inline-radio">Professional</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Interpreter, Other' class='radio' type='checkbox' id='{0}_GenericUseOfInterpreter4' name='{0}_GenericUseOfInterpreter' value='4' {1} />", Model.TypeName, useOfInterpreter.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericUseOfInterpreter4" class="fixed inline-radio">Other</label>
                        <div id="<%= Model.TypeName %>_GenericUseOfInterpreter4More">
                            <label for="<%= Model.TypeName %>_GenericUseOfInterpreterOtherDetails"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericUseOfInterpreterOtherDetails", data.AnswerOrEmptyString("GenericUseOfInterpreterOtherDetails"), new { @class = "oe", @id = Model.TypeName + "_GenericUseOfInterpreterOtherDetails", @maxlength = "20", @title = "(Optional) Interpreter, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Homebound</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Is The Patient Homebound?</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericIsHomeBound", "", new { @id = Model.TypeName + "_GenericIsHomeBoundHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericIsHomeBound", "Yes", data.AnswerOrEmptyString("GenericIsHomeBound").Equals("Yes"), new { @id = Model.TypeName + "_GenericIsHomeBoundYes", @class = "radio", @title = "(Optional) Homebound, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericIsHomeBoundYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericIsHomeBound", "No", data.AnswerOrEmptyString("GenericIsHomeBound").Equals("No"), new { @id = Model.TypeName + "_GenericIsHomeBoundNo", @class = "radio", @title = "(Optional) Homebound, No" })%>
                    <label for="<%= Model.TypeName %>_GenericIsHomeBoundNo" class="inline-radio">No</label>
                </div>
            </div>
        </div>
        <div id="<%= Model.TypeName %>_Homebound" class="wide-column">
            <%  string[] homeBoundReason = data.AnswerArray("GenericHomeBoundReason"); %>
            <%= Html.Hidden(Model.TypeName + "_GenericHomeBoundReason", "", new { @id = Model.TypeName + "_GenericHomeBoundReasonHidden" })%>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Effort to Leave' type='checkbox' id='{0}_GenericHomeBoundReason1' name='{0}_GenericHomeBoundReason' value='1' {1} />", Model.TypeName, homeBoundReason.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason1" class="radio">Exhibits considerable &#38; taxing effort to leave home</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Requires Assistance' type='checkbox' id='{0}_GenericHomeBoundReason2' name='{0}_GenericHomeBoundReason' value='2' {1} />", Model.TypeName, homeBoundReason.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason2" class="radio">Requires the assistance of another to get up and move safely</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Dyspnea' type='checkbox' id='{0}_GenericHomeBoundReason3' name='{0}_GenericHomeBoundReason' value='3' {1} />", Model.TypeName, homeBoundReason.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason3" class="radio">Severe Dyspnea</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Needs Assistance to Safely Leave' type='checkbox' id='{0}_GenericHomeBoundReason4' name='{0}_GenericHomeBoundReason' value='4' {1} />", Model.TypeName, homeBoundReason.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason4" class="radio">Unable to safely leave home unassisted</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Cognitive/Psychiatric Impairments' type='checkbox' id='{0}_GenericHomeBoundReason5' name='{0}_GenericHomeBoundReason' value='5' {1} />", Model.TypeName, homeBoundReason.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason5" class="radio">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Medical Restrictions' type='checkbox' id='{0}_GenericHomeBoundReason6' name='{0}_GenericHomeBoundReason' value='6' {1} />", Model.TypeName, homeBoundReason.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason6" class="radio">Unable to leave home due to medical restriction(s)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Homebound, Other' type='checkbox' id='{0}_GenericHomeBoundReason7' name='{0}_GenericHomeBoundReason' value='7' {1} />", Model.TypeName, homeBoundReason.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericHomeBoundReason7" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_GenericHomeBoundReason7More">
                            <label for="<%= Model.TypeName %>_GenericOtherHomeBoundDetails"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericOtherHomeBoundDetails", data.AnswerOrEmptyString("GenericOtherHomeBoundDetails"), new { @id = Model.TypeName + "_GenericOtherHomeBoundDetails", @maxlength = "60", @title = "(Optional) Homebound, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHomeBoundComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericHomeBoundComments", data.AnswerOrEmptyString("GenericHomeBoundComments"), 5, 70, new { @id = Model.TypeName + "_GenericHomeBoundComments", @title = "(Optional) Homebound, Comments" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Safety Measures (Locator #15)</legend>
        <%  string[] safetyMeasure = data.AnswerArray("485SafetyMeasures"); %>
        <%= Html.Hidden(Model.TypeName + "_485SafetyMeasures", "", new { @id = Model.TypeName + "_485SafetyMeasuresHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Anticoagulant Precautions' type='checkbox' id='{0}_485SafetyMeasures1' name='{0}_485SafetyMeasures' value='1' {1} />", Model.TypeName, safetyMeasure.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures1" class="radio">Anticoagulant Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Emergency Plan Developed' type='checkbox' id='{0}_485SafetyMeasures2' name='{0}_485SafetyMeasures' value='2' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("2") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures2" class="radio">Emergency Plan Developed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Fall Precautions' type='checkbox' id='{0}_485SafetyMeasures3' name='{0}_485SafetyMeasures' value='3' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("3") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures3" class="radio">Fall Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, >Keep Pathway Clear' type='checkbox' id='{0}_485SafetyMeasures4' name='{0}_485SafetyMeasures' value='4' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("4") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures4" class="radio">Keep Pathway Clear</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Keep Side Rails Up' type='checkbox' id='{0}_485SafetyMeasures5' name='{0}_485SafetyMeasures' value='5' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("5") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures5" class="radio">Keep Side Rails Up</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Neutropenic Precautions' type='checkbox' id='{0}_485SafetyMeasures6' name='{0}_485SafetyMeasures' value='6' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("6") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures6" class="radio">Neutropenic Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Oxygen Precautions' type='checkbox' id='{0}_485SafetyMeasures7' name='{0}_485SafetyMeasures' value='7' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("7") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures7" class="radio">O2 Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Proper Position During Meals' type='checkbox' id='{0}_485SafetyMeasures8' name='{0}_485SafetyMeasures' value='8' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("8") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures8" class="radio">Proper Position During Meals</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Safety in ADLs' type='checkbox' id='{0}_485SafetyMeasures9' name='{0}_485SafetyMeasures' value='9' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("9") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures9" class="radio">Safety in ADLs</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Seizure Precautions' type='checkbox' id='{0}_485SafetyMeasures10' name='{0}_485SafetyMeasures' value='10' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("10") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures10" class="radio">Seizure Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Sharps Safety' type='checkbox' id='{0}_485SafetyMeasures11' name='{0}_485SafetyMeasures' value='11' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("11") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures11" class="radio">Sharps Safety</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Slow Position Change' type='checkbox' id='{0}_485SafetyMeasures12' name='{0}_485SafetyMeasures' value='12' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("12") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures12" class="radio">Slow Position Change</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Infection Control' type='checkbox' id='{0}_485SafetyMeasures13' name='{0}_485SafetyMeasures' value='13' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("13") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Transfer/Ambulation Support' type='checkbox' id='{0}_485SafetyMeasures14' name='{0}_485SafetyMeasures' value='14' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("14") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Assistive Device' type='checkbox' id='{0}_485SafetyMeasures15' name='{0}_485SafetyMeasures' value='15' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("15") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures15" class="radio">Use of Assistive Devices</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Safe Utilities Management' type='checkbox' id='{0}_485SafetyMeasures16' name='{0}_485SafetyMeasures' value='16' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("16") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures16" class="radio">Instruct on safe utilities management</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Mmobility Safety' type='checkbox' id='{0}_485SafetyMeasures17' name='{0}_485SafetyMeasures' value='17' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("17") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures17" class="radio">Instruct on mobility safety</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, DME &#38; Electrical Safety' type='checkbox' id='{0}_485SafetyMeasures18' name='{0}_485SafetyMeasures' value='18' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("18") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures18" class="radio">Instruct on DME &#38; electrical safety</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Sharps Container' type='checkbox' id='{0}_485SafetyMeasures19' name='{0}_485SafetyMeasures' value='19' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("19") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures19" class="radio">Instruct on sharps container</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Medical Gas' type='checkbox' id='{0}_485SafetyMeasures20' name='{0}_485SafetyMeasures' value='20' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("20") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures20" class="radio">Instruct on medical gas</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Emergency Plan' type='checkbox' id='{0}_485SafetyMeasures21' name='{0}_485SafetyMeasures' value='21' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("21") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures21" class="radio">Instruct on disaster/ emergency plan</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Safety Measures' type='checkbox' id='{0}_485SafetyMeasures22' name='{0}_485SafetyMeasures' value='22' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("22") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures22" class="radio">Instruct on safety measures</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 15) Safety Measures, Handling of Biohazard Waste' type='checkbox' id='{0}_485SafetyMeasures23' name='{0}_485SafetyMeasures' value='23' {1} />", Model.TypeName, safetyMeasure != null && safetyMeasure.Contains("23") ? "checked='checked'" : "")%>
                        <label for="<%= Model.TypeName %>_485SafetyMeasures23" class="radio">Instruct on proper handling of biohazard waste</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485TriageEmergencyCode" class="float-left">Emergency Triage Code</label>
                <div class="float-right">
                    <%  var triageEmergencyCode = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("485TriageEmergencyCode", "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485TriageEmergencyCode", triageEmergencyCode, new { @title = "(485 Locator 15) Safety Measures, Emergency Triage Code" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485OtherSafetyMeasures" class="strong">Other</label>
                <%= Html.TextArea(Model.TypeName + "_485OtherSafetyMeasures", data.AnswerOrEmptyString("485OtherSafetyMeasures"), 5, 70, new { @id = Model.TypeName + "_485OtherSafetyMeasures", @title = "(485 Locator 15) Safety Measures, Other" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/SupportiveAssistance.ascx", Model); %>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericHazardsIdentified9"),
        $("#<%= Model.TypeName %>_GenericHazardsIdentified9More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericUseOfInterpreter4"),
        $("#<%= Model.TypeName %>_GenericUseOfInterpreter4More"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericCulturalPractices", "1",
        $("#<%= Model.TypeName %>_GenericCulturalPracticesYesMore"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericIsHomeBound", "Yes",
        $("#window_startofcare .HomeBound"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericHomeBoundReason7"),
        $("#<%= Model.TypeName %>_GenericHomeBoundReason7More"));
    Lookup.Language("#StartOfCare_GenericPrimaryLanguage");
</script>