﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var ActivitiesPermitted = data.AnswerArray("485ActivitiesPermitted"); %>
<%  var Musculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
<% var GenericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 6 || Model.AssessmentTypeNum % 10 > 8) { %>
    
    
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= Musculoskeletal.Contains("1").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("Impaired Motor Skill:",<%= Musculoskeletal.Contains("3").ToString().ToLower() %>,true) +
                printview.span("<%= Musculoskeletal.Contains("3") ? (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("2") ? "Fine" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("3") ? "Gross" : string.Empty) : string.Empty %>",false,1))) +
        printview.col(5,
            printview.checkbox("Grip Strength:",<%= Musculoskeletal.Contains("2").ToString().ToLower() %>,true) +
            printview.span("<%= Musculoskeletal.Contains("2") ? (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("1") ? "Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("2") ? "Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("3") ? "Other" : string.Empty) : string.Empty %>",false,1) +
            printview.checkbox("Bilateral",<%= (Musculoskeletal.Contains("2") && data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (Musculoskeletal.Contains("2") && data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (Musculoskeletal.Contains("2") && data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("2")).ToString().ToLower() %>)) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Limited ROM:",<%= Musculoskeletal.Contains("4").ToString().ToLower() %>,true) +
                printview.span("Location: <%= Musculoskeletal.Contains("4") ? data.AnswerOrEmptyString("GenericLimitedROMLocation").Clean() : string.Empty %>")) +
            printview.col(2,
                printview.checkbox("Mobility:",<%= Musculoskeletal.Contains("5").ToString().ToLower() %>,true) +
                printview.span("<%= Musculoskeletal.Contains("5") ? (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("1") ? "WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("2") ? "Ambulatory" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("3") ? "Ambulatory w/assistance" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("4") ? "Chair fast" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("5") ? "Bedfast" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("6") ? "Non-ambulatory" : string.Empty) : string.Empty %>",false,1))) +
        printview.col(6,
            printview.checkbox("Assistive Device:",<%= Musculoskeletal.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("Cane",<%= (Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("Crutches",<%= (Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("2")).ToString().ToLower() %>) +
            printview.checkbox("Walker",<%= (Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Wheelchair",<%= (Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("4")).ToString().ToLower() %>) +
            printview.checkbox("Other <%= Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("5") ? data.AnswerOrEmptyString("GenericAssistiveDeviceOther").Clean() : string.Empty %>",<%= (Musculoskeletal.Contains("6") && GenericAssistiveDevice.Contains("5")).ToString().ToLower() %>)) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Contracture:",<%= Musculoskeletal.Contains("7").ToString().ToLower() %>,true) +
                printview.span("Location: <%= Musculoskeletal.Contains("7") ? data.AnswerOrEmptyString("GenericContractureLocation").Clean() : string.Empty %>")) +
            printview.checkbox("Weakness",<%= Musculoskeletal.Contains("8").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("Joint Pain:",<%= Musculoskeletal.Contains("9").ToString().ToLower() %>,true) +
                printview.span("Location: <%= Musculoskeletal.Contains("9") ? data.AnswerOrEmptyString("GenericJointPainLocation").Clean() : string.Empty %>")) +
            printview.checkbox("Poor Balance",<%= Musculoskeletal.Contains("10").ToString().ToLower() %>,true) +
            printview.checkbox("Joint Stiffness",<%= Musculoskeletal.Contains("11").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("Amputation:",<%= Musculoskeletal.Contains("12").ToString().ToLower() %>,true) +
                printview.span("Location: <%= Musculoskeletal.Contains("12") ? data.AnswerOrEmptyString("GenericAmputationLocation").Clean() : string.Empty %>"))) +
        printview.col(4,
            printview.checkbox("Weight Bearing Restrictions:",<%= Musculoskeletal.Contains("13").ToString().ToLower() %>,true) +
            printview.span("Location: <%= Musculoskeletal.Contains("13") ? data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation").Clean() : string.Empty %>") +
            printview.checkbox("Full",<%= (Musculoskeletal.Contains("13") && data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Partial",<%= (Musculoskeletal.Contains("13") && data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("1")).ToString().ToLower() %>)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMusculoskeletalComments").Clean() %>",false,2),
        "Musculoskeletal");
    
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1800) " : string.Empty %>Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).",true) +
        printview.checkbox("0 &#8211; Able to groom self unaided, with or without the use of assistive devices or adapted methods.",<%= data.AnswerOrEmptyString("M1800Grooming").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Grooming utensils must be placed within reach before able to complete grooming activities.",<%= data.AnswerOrEmptyString("M1800Grooming").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Someone must assist the patient to groom self.",<%= data.AnswerOrEmptyString("M1800Grooming").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Patient depends entirely upon someone else for grooming needs.",<%= data.AnswerOrEmptyString("M1800Grooming").Equals("03").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum != 11 && Model.AssessmentTypeNum != 14) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1810) " : string.Empty %>Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:",true) +
        printview.checkbox("0 &#8211; Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.",<%= data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to dress upper body without assistance if clothing is laid out or handed to the patient.",<%= data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Someone must help the patient put on upper body clothing.",<%= data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Patient depends entirely upon another person to dress the upper body.",<%= data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("03").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1820) " : string.Empty %>Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:",true) +
        printview.checkbox("0 &#8211; Able to obtain, put on, and remove clothing and shoes without assistance.",<%= data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.",<%= data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.",<%= data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Patient depends entirely upon another person to dress lower body.",<%= data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("03").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1830) " : string.Empty %>Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, hands, and hair).",true) +
        printview.checkbox("0 &#8211; Able to bathe self in shower or tub independently, including getting in and out of tub/shower.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Able to bathe in shower or tub with the intermittent assistance of another person:<ul style='margin-left:8px;'><li>(a) for intermittent supervision or encouragement or reminders, OR</li><li>(b) to get in and out of the shower or tub, OR</li><li>(c) for washing difficult to reach areas.</li></ul>",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Unable to use shower/tub, but able to participate in bathing in bed, sink, bedside chair, or commode, w/ assistance/supervision of another.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("05").ToString().ToLower() %>) +
        printview.checkbox("6 &#8211; Unable to participate effectively in bathing and is bathed totally by another person.",<%= data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("06").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1840) " : string.Empty %>Toilet Transferring: Current ability to get to/from the toilet or bedside commode safely and transfer on/off toilet/commode.",true) +
        printview.checkbox("0 &#8211; Able to get to and from the toilet and transfer independently with or without a device.",<%= data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.",<%= data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).",<%= data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.",<%= data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Is totally dependent in toileting.",<%= data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("04").ToString().ToLower() %>));
        <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1845) " : string.Empty %>Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.",true) +
        printview.checkbox("0 &#8211; Able to manage toileting hygiene and clothing management without assistance.",<%= data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.",<%= data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Someone must help the patient to maintain toileting hygiene and/or adjust clothing.",<%= data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Patient depends entirely upon another person to maintain toileting hygiene.",<%= data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("03").ToString().ToLower() %>));
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1850) " : string.Empty %>Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.",true) +
        printview.checkbox("0 &#8211; Able to independently transfer.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to transfer with minimal human assistance or with use of an assistive device.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Able to bear weight and pivot during the transfer process but unable to transfer self.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Unable to transfer self and is unable to bear weight or pivot when transferred by another person.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Bedfast, unable to transfer but is able to turn and position self in bed.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Bedfast, unable to transfer and is unable to turn and position self.",<%= data.AnswerOrEmptyString("M1850Transferring").Equals("05").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1860) " : string.Empty %>Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.",true) +
        printview.checkbox("0 &#8211; Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; With the use of a one-handed device, able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Requires a two-handed device to walk alone on a level surface and/or requires human supervision/assistance to negotiate uneven surfaces.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Able to walk only with the supervision or assistance of another person at all times.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Chairfast, unable to ambulate but is able to wheel self independently.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Chairfast, unable to ambulate and is unable to wheel self.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("05").ToString().ToLower() %>) +
        printview.checkbox("6 &#8211; Bedfast, unable to ambulate or be up in a chair.",<%= data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("06").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1870) " : string.Empty %>Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.",true) +
        printview.checkbox("0 &#8211; Able to independently feed self.",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to feed self independently but requires:<ul style='margin-left:8px;'><li>(a) meal set-up; OR</li><li>(b) intermittent assistance or supervision from another person; OR</li><li>(c) a liquid, pureed or ground meat diet.</li></ul>",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Unable to feed self and must be assisted or supervised throughout the meal/snack.",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Unable to take in nutrients orally or by tube feeding.",<%= data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("05").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1880) " : string.Empty %>Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:",true) +
        printview.checkbox("0 &#8211; <ul style='margin:-17px 0 0 8px;'><li>(a) Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</li><li>(b) Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</li></ul>",<%= data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.",<%= data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Unable to prepare any light meals or reheat any delivered meals.",<%= data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("02").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1890) " : string.Empty %>Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.",true) +
        printview.checkbox("0 &#8211; Able to dial numbers and answer calls appropriately and as desired.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Able to answer the telephone only some of the time or is able to carry on only a limited conversation.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Unable to answer the telephone at all but can listen if assisted with equipment.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Totally unable to use the telephone.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("05").ToString().ToLower() %>) +
        printview.checkbox("NA &#8211; Patient does not have a telephone.",<%= data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Adl.ascx", Model); %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1900) " : string.Empty %>Prior Functioning ADL/IADL: Indicate the patient&#8217;s usual ability with everyday activities prior to this current illness, exacerbation, or injury. Check only one box in each row.",true) +
        printview.col(2,
            printview.span("a. Self-Care (e.g., grooming, dressing, and bathing)") +
            printview.col(3,
                printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("00").ToString().ToLower() %>) +
                printview.checkbox("1 &#8211; Need Some Help",<%= data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("01").ToString().ToLower() %>) +
                printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("02").ToString().ToLower() %>)) +
            printview.span("b. Ambulation") +
            printview.col(3,
                printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M1900Ambulation").Equals("00").ToString().ToLower() %>) +
                printview.checkbox("1 &#8211; Need Some Help",<%= data.AnswerOrEmptyString("M1900Ambulation").Equals("01").ToString().ToLower() %>) +
                printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M1900Ambulation").Equals("02").ToString().ToLower() %>)) +
            printview.span("c. Transfer") +
            printview.col(3,
                printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M1900Transfer").Equals("00").ToString().ToLower() %>) +
                printview.checkbox("1 &#8211; Need Some Help",<%= data.AnswerOrEmptyString("M1900Transfer").Equals("01").ToString().ToLower() %>) +
                printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M1900Transfer").Equals("02").ToString().ToLower() %>)) +
            printview.span("d. Household tasks (e.g., light meal preparation, laundry, shopping)") +
            printview.col(3,
                printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("00").ToString().ToLower() %>) +
                printview.checkbox("1 &#8211; Need Some Help",<%= data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("01").ToString().ToLower() %>) +
                printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("02").ToString().ToLower() %>))));
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
        printview.col(1,
            printview.checkbox("Age 65+",<%= data.AnswerOrEmptyString("GenericAge65Plus").Equals("1").ToString().ToLower() %>,true) +
            printview.checkbox("Diagnosis (3+ co-existing",<%= data.AnswerOrEmptyString("GenericHypotensionDiagnosis").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em> &nbsp;&nbsp;&nbsp; Assess for hypotension.</em>") +
            printview.checkbox("Prior history of falls within 3mo",<%= data.AnswerOrEmptyString("GenericPriorFalls").Equals("1").ToString().ToLower() %>,true) +
            printview.span(" <em>&nbsp;&nbsp;&nbsp;Fall definition: &#8220;An unintentional change in position resulting in coming to rest on the ground or at a lower level.&#8221;</em>") +
            printview.checkbox("Incontinence",<%= data.AnswerOrEmptyString("GenericFallIncontinence").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;Inability to make it to the bathroom or commode in timely manner. Includes frequency, urgency, and/or nocturia.</em>") +
            printview.checkbox("Visual impairment",<%= data.AnswerOrEmptyString("GenericVisualImpairment").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;Includes macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription.</em>") +
            printview.checkbox("Impaired functional mobility",<%= data.AnswerOrEmptyString("GenericImpairedFunctionalMobility").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;May include patients who need help with IADL’s or ADL’s or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices.</em>") +
            printview.checkbox("Environmental hazards",<%= data.AnswerOrEmptyString("GenericEnvHazards").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.</em>") +
            printview.checkbox("Poly Pharmacy (4+ Rx Drugs)",<%= data.AnswerOrEmptyString("GenericPolyPharmacy").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;Drugs highly associated with fall risk include but are not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</em>") +
            printview.checkbox("Pain affecting level of function",<%= data.AnswerOrEmptyString("GenericPainAffectingFunction").Equals("1").ToString().ToLower() %>,true) +
            printview.span("<em>&nbsp;&nbsp;&nbsp;Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or compliance with safety recommendations.</em>") +
            printview.checkbox("Cognitive impairment",<%= data.AnswerOrEmptyString("GenericCognitiveImpairment").Equals("1").ToString().ToLower() %>,true)+
            printview.span("<em>&nbsp;&nbsp;&nbsp;Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patient’s ability to adhere to the plan of care.</em>") +
            printview.span("Total    : <%= data.AnswerOrEmptyString("485FallAssessmentScore").Clean() %>   ")),
        "Fall Assessment");
    <%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
    printview.addsection(
        printview.col(2,
            printview.span("1. Sitting balance",true) +
            printview.checkbox("Leans or slides in chair",<%= data.AnswerOrEmptyString("GenericTinettiSitting").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady, safe",<%= data.AnswerOrEmptyString("GenericTinettiSitting").Equals("1").ToString().ToLower() %>) +
            printview.span("2. Arises",true) +
            printview.checkbox("Unable without Help",<%= data.AnswerOrEmptyString("GenericTinettiArises").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Able, uses arms to help",<%= data.AnswerOrEmptyString("GenericTinettiArises").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Able without using arms",<%= data.AnswerOrEmptyString("GenericTinettiArises").Equals("2").ToString().ToLower() %>) +
            printview.span("3. Attempts to arise",true) +
            printview.checkbox("Unable without Help",<%= data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Able, requires greater than 1 attempt",<%= data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Able to rise, 1 attempt",<%= data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("2").ToString().ToLower() %>) +
            printview.span("4. Immediate standing balance (first 5 seconds)",true) +
            printview.checkbox("Unsteady (swaggers, moves feet, trunk sway)",<%= data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady but uses walker or other support",<%= data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady without walker or other support",<%= data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("2").ToString().ToLower() %>) +
            printview.span("5. Standing balance",true) +
            printview.checkbox("Unsteady",<%= data.AnswerOrEmptyString("GenericTinettiStanding").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady but wide stance (medial heels greater than 4 inches apart) and uses cane or other support",<%= data.AnswerOrEmptyString("GenericTinettiStanding").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Narrow stance without support",<%= data.AnswerOrEmptyString("GenericTinettiStanding").Equals("2").ToString().ToLower() %>) +
            printview.span("6. Nudged (subject at max position with feet as close together as possible, examiner pushes lightly on subject&#8217;s sternum with palm of hand 3 times)",true) +
            printview.checkbox("Begins to fall",<%= data.AnswerOrEmptyString("GenericTinettiNudged").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Staggers, grabs, catches self",<%= data.AnswerOrEmptyString("GenericTinettiNudged").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady",<%= data.AnswerOrEmptyString("GenericTinettiNudged").Equals("2").ToString().ToLower() %>) +
            printview.span("7. Eyes closed (at maximum position #6)",true) +
            printview.checkbox("Unsteady",<%= data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady",<%= data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("1").ToString().ToLower() %>) +
            printview.span("8. Turning 360 degrees",true) +
            printview.checkbox("Discontinuous steps",<%= data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Continuous steps",<%= data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("Unsteady (grabs, swaggers)",<%= data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steady",<%= data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("1").ToString().ToLower() %>) +
            printview.span("9. Sitting down",true) +
            printview.checkbox("Unsafe (misjudged distance, falls into chair)",<%= data.AnswerOrEmptyString("GenericTinettiSitting").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Uses arms or not a smooth motion",<%= data.AnswerOrEmptyString("GenericTinettiSitting").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Safe, smooth motion",<%= data.AnswerOrEmptyString("GenericTinettiSitting").Equals("2").ToString().ToLower() %>) +
            printview.span("Balance Total",true) +
            printview.span("<%= data.AnswerOrEmptyString("TinettiBalanceTotal") %>") +
            printview.span("10. Initiation of gait (immediately after told to &#8220;go&#8221;)",true) +
            printview.checkbox("Any hesitancy or multiple attempts to start",<%= data.AnswerOrEmptyString("GenericTinettiGait").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("No hesitancy",<%= data.AnswerOrEmptyString("GenericTinettiGait").Equals("1").ToString().ToLower() %>) +
            printview.span("11. Step length and height",true) +
            printview.checkbox("a. Right swing foot does not pass left stance foot with step",<%= data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("b. Right foot passes left stance foot",<%= data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("c. Right foot does not clear floor completely with step",<%= data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("d. Right foot completely clears floor",<%= data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("e. Left swing foot does not pass right stance foot with step",<%= data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("f. Left foot passes right stance foot",<%= data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("g. Left foot does not clear floor completely with step",<%= data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("h. Left foot completely clears floor",<%= data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("1").ToString().ToLower() %>) +
            printview.span("12. Step symmetry",true) +
            printview.checkbox("Right and left step length not equal (estimate)",<%= data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Right and left step appear equal",<%= data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("1").ToString().ToLower() %>) +
            printview.span("13. Step continually",true) +
            printview.checkbox("Stopping or discontinuity between steps",<%= data.AnswerOrEmptyString("GenericTinettiContinually").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Steps appear continuous",<%= data.AnswerOrEmptyString("GenericTinettiContinually").Equals("1").ToString().ToLower() %>) +
            printview.span("14. Path (estimated in relation to floor tiles, 12-inch diameter; observe excursion of 1 foot over about 10 feet of the course)",true) +
            printview.checkbox("Marked deviation",<%= data.AnswerOrEmptyString("GenericTinettiPath").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Mild/moderate deviation or uses walking aid",<%= data.AnswerOrEmptyString("GenericTinettiPath").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Straight without walking aid",<%= data.AnswerOrEmptyString("GenericTinettiPath").Equals("2").ToString().ToLower() %>) +
            printview.span("15. Trunk",true) +
            printview.checkbox("Marked sway or uses walking aid",<%= data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("No sway but flexion of knees or back, or spreads arms out while walking",<%= data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("1").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("No sway, no flexion, no use of arms, and no use of walking aid",<%= data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("2").ToString().ToLower() %>) +
            printview.span("16. Walking stance",true) +
            printview.checkbox("Heels apart",<%= data.AnswerOrEmptyString("GenericTinettiWalking").Equals("0").ToString().ToLower() %>) + printview.span("") +
            printview.checkbox("Heels almost touching while walking",<%= data.AnswerOrEmptyString("GenericTinettiWalking").Equals("1").ToString().ToLower() %>) +
            printview.span("Gait Total",true) +
            printview.span("<%= data.AnswerOrEmptyString("TinettiGaitTotal") %>") +
            printview.span("Total (Balance and Gait)",true) +
            printview.span("<%= data.AnswerOrEmptyString("TinettiTotal") %>")
        ), "Tinetti Assessment");
    <%  } %>
    printview.addsection(
        printview.col(1,
            printview.span("Timed Up and Go Findings    : <%= data.AnswerOrEmptyString("GenericTimedUpAndFindings").Clean() %>") +
            printview.span("<em>&nbsp;&nbsp; Note:  &#60; 10 seconds= Normal, &#60; 14 seconds= not a falls risk, &#62; 14 seconds = increased risk for falls</em>")
        ), "Timed Up and Go Findings");
        
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1910) " : string.Empty %>Has this patient had a multi-factor Fall Risk Assessment (such as falls history, use of multiple medications, mental impairment, toileting frequency, general mobility/transferring impairment, environmental hazards)?",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; No falls risk assessment conducted.",<%= data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes &#38; it does not indicate a risk for falls.",<%= data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Yes &#38; it indicates a risk for falls.",<%= data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("02").ToString().ToLower() %>)));
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Iadl.ascx", Model); %>
<%  } %>
     printview.addsection(
        printview.col(4,
            printview.span("Name:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEProviderName").Clean() %>",false,1) +
            printview.span("Phone Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEProviderPhone").Clean() %>",false,1)) +
        printview.col(2,
        printview.span("Address:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericDMEProviderAddress").Clean() %>",false,1))+
        printview.span("DME/Supplies Provided:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericDMESuppliesProvided").Clean() %>",false,2),
        "DME Provider");
</script>