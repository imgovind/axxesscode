<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var Eyes = data.AnswerArray("GenericEyes"); %>
<%  var Ears = data.AnswerArray("GenericEars"); %>
<%  var Nose = data.AnswerArray("GenericNose"); %>
<%  var Mouth = data.AnswerArray("GenericMouth"); %>
<%  var Throat = data.AnswerArray("GenericThroat"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 6 || Model.AssessmentTypeNum % 10 > 8) { %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
        "","Sensory");
    printview.addsection(
        printview.col(5,
            printview.checkbox("WNL",<%= Eyes.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Glasses",<%= Eyes.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Contacts Left",<%= Eyes.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Contacts Right",<%= Eyes.Contains("4").ToString().ToLower() %>,true) +
            printview.checkbox("Blurred Vision",<%= Eyes.Contains("5").ToString().ToLower() %>,true) +
            printview.checkbox("Glaucoma",<%= Eyes.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("Cataracts",<%= Eyes.Contains("7").ToString().ToLower() %>,true) +
            printview.checkbox("Macular Degeneration",<%= Eyes.Contains("8").ToString().ToLower() %>,true) +
            printview.checkbox("Other",<%= Eyes.Contains("13").ToString().ToLower() %>,true) +
            printview.span("<%= Eyes.Contains("13") ? data.AnswerOrEmptyString("GenericEyesOtherDetails").Clean() : string.Empty %>",0,1) +
            printview.span("",true))+
        printview.col(2,
            printview.span("Last Eye Exam Date",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericEyesLastEyeExamDate").Clean() %>",false,1)),
        "Eyes");
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= Ears.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Deaf",<%= Ears.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Drainage",<%= Ears.Contains("4").ToString().ToLower() %>,true) +
            printview.checkbox("Pain",<%= Ears.Contains("5").ToString().ToLower() %>,true) +
            <%if(Ears.Contains("2")){ %>
            printview.col(4,
            printview.checkbox("Hearing Impaired",true)+
            printview.checkbox("Bilateral",<%= (Ears.Contains("2") && data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (Ears.Contains("2") && data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (Ears.Contains("2") && data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("2")).ToString().ToLower() %>)) +
            <%}else{ %>
            printview.checkbox("Hearing Impaired",false)+
            <%} %>
            <%if(Ears.Contains("6")){ %>
            printview.col(4,
            printview.checkbox("Hearing Aids",true) +
            printview.checkbox("Bilateral",<%= (Ears.Contains("6") && data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (Ears.Contains("6") && data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (Ears.Contains("6") && data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("2")).ToString().ToLower() %>))) +
            <%}else{ %>
            printview.checkbox("Hearing Aids",false)) +
            <%} %>
        printview.checkbox("<strong>Other</strong> <%= Ears.Contains("7") ? data.AnswerOrEmptyString("GenericEarsOtherDetails").Clean() : string.Empty %>",<%= Ears.Contains("7").ToString().ToLower() %>),
        "Ears");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= Nose.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Congestion",<%= Nose.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Loss of Smell",<%= Nose.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Nose Bleeds",<%= Nose.Contains("4").ToString().ToLower() %>,true) +
            printview.span("How often? <%= Nose.Contains("4") ? data.AnswerOrEmptyString("GenericNoseBleedsFrequency").Clean() : string.Empty %>") +
            printview.checkbox("<strong>Other</strong> <%= Nose.Contains("5") ? data.AnswerOrEmptyString("GenericNoseOtherDetails").Clean() : string.Empty %>", <%= Nose.Contains("5").ToString().ToLower() %>)),
        "Nose");
    printview.addsection(
        printview.col(5,
            printview.checkbox("WNL",<%= Mouth.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Dentures",<%= Mouth.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Difficulty chewing",<%= Mouth.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Dysphagia",<%= Mouth.Contains("4").ToString().ToLower() %>,true) +
            printview.checkbox("Other<%= Mouth.Contains("5")?" "+data.AnswerOrEmptyString("GenericMouthOther").Clean():string.Empty%>",<%= Mouth.Contains("5").ToString().ToLower() %>,true)),
            
        "Mouth");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= Throat.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Sore throat",<%= Throat.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Hoarseness",<%= Throat.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("<strong>Other</strong> <%= Throat.Contains("4") ? data.AnswerOrEmptyString("GenericThroatOther").Clean() : string.Empty %>",<%= Throat.Contains("4").ToString().ToLower() %>)),
        "Throat");
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 6) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1200) " : string.Empty %>Vision (with corrective lenses if the patient usually wears them)",true) +
        printview.checkbox("0 &#8211; Normal vision: sees adequately in most situations; can see medication labels, newsprint.",<%= data.AnswerOrEmptyString("M1200Vision").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Partially impaired: cannot see medication labels/newsprint, but can see obstacles in path/surrounding layout; can count fingers at arm&#8217;s length.",<%= data.AnswerOrEmptyString("M1200Vision").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.",<%= data.AnswerOrEmptyString("M1200Vision").Equals("02").ToString().ToLower() %>),
        "OASIS M1200");
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1210) " : string.Empty %>Ability to hear (with hearing aid or hearing appliance if normally used)",true) +
        printview.checkbox("0 &#8211; Adequate: hears normal conversation without difficulty.",<%= data.AnswerOrEmptyString("M1210Hearing").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Mildly to Moderately Impaired: difficulty hearing in some environments or speaker may need to increase volume or speak distinctly.",<%= data.AnswerOrEmptyString("M1210Hearing").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Severely Impaired: absence of useful hearing.",<%= data.AnswerOrEmptyString("M1210Hearing").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("UK &#8211; Unable to assess hearing.",<%= data.AnswerOrEmptyString("M1210Hearing").Equals("UK").ToString().ToLower() %>),
        "OASIS M1210/M1220");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1220) " : string.Empty %>Understanding of Verbal Content in patient&#8217;s own language (with hearing aid or device if used)",true) +
        printview.checkbox("0 &#8211; Understands: clear comprehension without cues or repetitions.",<%= data.AnswerOrEmptyString("M1220VerbalContent").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Usually Understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand.",<%= data.AnswerOrEmptyString("M1220VerbalContent").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Sometimes Understands: understands only basic conversations or simple, direct phrases. Frequently requires cues to understand.",<%= data.AnswerOrEmptyString("M1220VerbalContent").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Rarely/Never Understands.",<%= data.AnswerOrEmptyString("M1220VerbalContent").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("UK &#8211; Unable to assess understanding.",<%= data.AnswerOrEmptyString("M1220VerbalContent").Equals("UK").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1230) " : string.Empty %>Speech and Oral (Verbal) Expression of Language (in patient&#8217;s own language)",true) +
        printview.checkbox("0 &#8211; Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Patient nonresponsive or unable to speak.",<%= data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("05").ToString().ToLower() %>),
        "OASIS M1230");
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Sensory.ascx", Model); %>
<%  } %>
</script>