<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var EliminationInterventions = data.AnswerArray("485EliminationInterventions"); %>
<%  var EliminationGoals = data.AnswerArray("485EliminationGoals"); %>
<%  if (EliminationInterventions.Length > 0 || (data.ContainsKey("485EliminationInterventionComments") && data["485EliminationInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (EliminationInterventions.Contains("1")) { %>
    printview.checkbox("SN to instruct on establishing bowel regimen.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("9")) { %>
    printview.checkbox("SN to instruct on establishing bladder regimen.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("2")) { %>
    printview.checkbox("SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct on care of stoma, surrounding skin and use of skin barrier.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("4")) { %>
    printview.checkbox("SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("5")) { %>
    printview.checkbox("SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("6")) { %>
    printview.checkbox("SN to change catheter every month and PRN using a <%= data.AnswerOrDefault("485EliminationFoleyCatheterType","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> F catheter.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("7")) { %>
    printview.checkbox("SN to instruct on intermittent catheterizations.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("8")) { %>
    printview.checkbox("SN to perform intermittent catheterization every <%= data.AnswerOrDefault("485EliminationCatheterizationNumber","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> &#38; prn using sterile technique.",true) +
    <%  } %>
    <%  if (EliminationInterventions.Contains("10")) { %>
    printview.checkbox("No blood pressure in <%= data.AnswerOrDefault("485EliminationBloodPressureArm", "%3Cspan class=%22blank%22%3E%3C/span%3E").Clean()%> arm.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485EliminationInterventionComments") && data["485EliminationInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485EliminationInterventionComments").Clean()%>",false,2) +
    <%  } %>
    "","Elimination Interventions");
<%  } %>
<%  if (EliminationGoals.Length > 0 || (data.ContainsKey("485EliminationGoalsComments") && data["485EliminationGoalsComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (EliminationGoals.Contains("1")) { %>
    printview.checkbox("Foley will remain patent during this episode and patient will be free of signs and symptoms of UTI.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("2")) { %>
    printview.checkbox("Suprapubic tube will remain patent during this episode and patient will be free of signs and symptoms of UTI.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("3")) { %>
    printview.checkbox("Patient will be without signs/symptoms of UTI (pain, foul odor, cloudy or blood-tinged urine and fever) during this episode.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("4")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485EliminationOstomyManagementIndependent","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> will be independent in ostomy management by: <%= data.AnswerOrDefault("485EliminationOstomyManagementIndependentDate","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("5")) { %>
    printview.checkbox("Patient will be free from signs and symptoms of constipation during the episode.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("6")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485EliminationAcidFoodVerbalizedGivenTo","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> will be independent in ostomy management by:  <%= data.AnswerOrDefault("485EliminationAcidFoodVerbalizedGivenToDate","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("7")) { %>
    printview.checkbox("Patient will verbalize understanding not to eat 4 hours before bedtime to reduce acid reflux/indigestion by:  <%= data.AnswerOrDefault("485EliminationEatingTimeVerbalizedGivenDate","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>.",true) +
    <%  } %>
    <%  if (EliminationGoals.Contains("8")) { %>
    printview.checkbox("Patient will not develop any signs and symptoms of dehydration during the episode.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485EliminationGoalsComments") && data["485EliminationGoalsComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485EliminationGoalsComments").Clean()%>",false,2) +
    <%  } %>
    "","Elimination Goals");
<%  } %>