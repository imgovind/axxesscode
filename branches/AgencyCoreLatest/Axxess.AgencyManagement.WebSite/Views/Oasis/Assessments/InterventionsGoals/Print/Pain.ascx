<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var PainInterventions = data.AnswerArray("485PainInterventions"); %>
<%  var PainGoals = data.AnswerArray("485PainGoals"); %>
<%  if (PainInterventions.Length > 0 || (data.ContainsKey("485PainInterventionComments") && data["485PainInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (PainInterventions.Contains("1")) { %>
    printview.checkbox("SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("2")) { %>
    printview.checkbox("SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("4")) { %>
    printview.checkbox("SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than <%= data.AnswerOrDefault("485PainTooGreatLevel", "<span class='short blank'></span>") %>, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("5")) { %>
    printview.checkbox("Therapist to assess pain level and effectiveness of pain medications and current pain management therapy every visit.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("6")) { %>
    printview.checkbox("Therapist to instruct patient to take pain medication before pain becomes severe to achieve better pain control.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("7")) { %>
    printview.checkbox("Therapist to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("8")) { %>
    printview.checkbox("Therapist to assess patient&#8217;s willingness to take pain medications and/or barriers to compliance, e.g., patient is unable to tolerate side effects such as drowsiness, dizziness, constipation.",true) +
    <%  } %>
    <%  if (PainInterventions.Contains("9")) { %>
    printview.checkbox("Therapist to report to physician if patient experiences pain level not acceptable to patient, pain level greater than <%= data.AnswerOrDefault("485PainInterventionsPainLevelGreaterThan", "<span class='short blank'></span>")%>, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.") +
    <%  } %>
    <%  if (data.ContainsKey("485PainInterventionComments") && data["485PainInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485PainInterventionComments").Clean()%>") +
    <%  } %>
    "","Pain Interventions");
<%  } %>
<%  if (PainGoals.Length > 0 || (data.ContainsKey("485PainGoalComments") && data["485PainGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (PainGoals.Contains("1")) { %>
    printview.checkbox("Patient will verbalize understanding of proper use of pain medication by the end of the episode.",true) +
    <%  } %>
    <%  if (PainGoals.Contains("2")) { %>
    printview.checkbox("PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.",true) +
    <%  } %>
    <%  if (PainGoals.Contains("3")) { %>
    printview.checkbox("Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.",true) +
    <%  } %>
    <%  if (PainGoals.Contains("4")) { %>
    printview.checkbox("Patient will verbalize understanding of proper use of pain medication by <%= data.AnswerOrDefault("485PainGoalsMedication", "<span class='short blank'></span>")%>.",true) +
    <%  } %>
    <%  if (PainGoals.Contains("5")) { %>
    printview.checkbox("Patient will achieve pain level less than <%= data.AnswerOrDefault("485PainGoalsPainLevelLessThan", "<span class='short blank'></span>")%> within <%= data.AnswerOrDefault("485PainGoalsWithinWeeks", "<span class='short blank'></span>")%> weeks.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485PainGoalComments") && data["485PainGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485PainGoalComments").Clean()%>") +
    <%  } %>
    "","Pain Goals");
<%  } %>