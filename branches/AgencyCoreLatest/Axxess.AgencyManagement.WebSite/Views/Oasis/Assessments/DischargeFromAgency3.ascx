﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Discharge from Agency (Version 2) | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M0010 &#8211; M0150'>Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' >Patient History &#38; Diagnoses</a>", Model.TypeName, AssessmentCategory.PatientHistory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1040 &#8211; M1050'>Risk Assessment</a>", Model.TypeName, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' >Supportive Assistance</a>", Model.TypeName, AssessmentCategory.SupportiveAssistance.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1230'>Sensory Status</a>", Model.TypeName, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1242'>Pain</a>", Model.TypeName, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1306 &#8211; M1350'>Integumentary Status</a>", Model.TypeName, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1400 &#8211; M1410'>Respiratory Status</a>", Model.TypeName, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' > Endocrine</a>", Model.TypeName, AssessmentCategory.Endocrine.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1500 &#8211; M1510'> Cardiac</a>", Model.TypeName, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1600 &#8211; M1620'>Elimination Status</a>", Model.TypeName, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' >Nutrition</a>", Model.TypeName, AssessmentCategory.Nutrition.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1700 &#8211; M1750'>Neuro/Behavioral</a>", Model.TypeName, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M1800 &#8211; M1890'>ADL/IADLs</a>", Model.TypeName, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}_2' tooltip='M2004 &#8211; M2030'>Medications</a>", Model.TypeName, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M2100 &#8211; M2110'>Care Management</a>", Model.TypeName, AssessmentCategory.CareManagement.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M2300 &#8211; M2310'>Emergent Care</a>", Model.TypeName, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M0903 &#8211; M0906<br />M2400 &#8211; M2420'>Discharge</a>", Model.TypeName, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}'>Discharge Plans</a>", Model.TypeName, AssessmentCategory.OrdersDisciplineTreatment.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.PatientHistory.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.RiskAssessment.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.SupportiveAssistance.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Sensory.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Pain.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Integumentary.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Respiratory.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Endocrine.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Cardiac.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Elimination.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Nutrition.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.NeuroBehavioral.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.AdlIadl.ToString())%>
    <%= string.Format("<div id='{0}_{1}_2' class='general loading'></div>", Model.TypeName, AssessmentCategory.Medications.ToString())%>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.CareManagement.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.EmergentCare.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.TransferDischargeDeath.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.OrdersDisciplineTreatment.ToString()) %>
</div>