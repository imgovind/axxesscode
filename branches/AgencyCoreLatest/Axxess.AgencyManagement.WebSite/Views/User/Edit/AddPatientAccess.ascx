﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<% using (Html.BeginForm("AddPatientAccess", "User", FormMethod.Post, new { @id = "AddPatientAccess_Form" })) { %>
<%= Html.Hidden("UserId",Model.Id) %>
<fieldset class="medication">
<div class="wrapper main">
    <%= Html.Telerik().Grid<SelectedPatient>().Name("AddUserPatientAccess_List").HtmlAttributes(new { @style = "height:auto;position:relative" }).ToolBar(commnds => commnds.Custom()).Columns(columns =>
{
    columns.Bound(p => p.PatientId).Hidden(true);
    columns.Bound(p => p.Selected).Title("").ClientTemplate("<input name=\"selectedPatient\" type=\"checkbox\" value=\"<#=PatientId#>\"/>").Width(40); ;
    columns.Bound(p => p.DisplayName).Title("Name");
}).DataBinding(databinding => databinding.Ajax().Select("GetAddPatientAccess", "User", new { userId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)%>
</div>
</fieldset>
<div class="buttons" style="position:relative">
    <ul>
        <li>
            <a href="javascript:void(0);" onclick="$('#AddPatientAccess_Form').submit();">Add Selected</a>
        </li>
        <li>
            <a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a>
        </li>
    </ul>
</div>
<%} %>
<script type="text/javascript">
    $("#AddUserPatientAccess_List .t-grid-toolbar").html("")
    .append($("<div/>").GridSearch());
    $("#AddUserPatientAccess_List .t-grid-content").css("top", 60);
</script>