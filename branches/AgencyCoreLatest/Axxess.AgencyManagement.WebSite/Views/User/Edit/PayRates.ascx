﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-rate" guid="<%= Model.Id %>">New Pay Rate</a></li>
        </ul>
        <ul class="fr">
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
    </div>
    <div class="clear"></div><div class="clear"></div>
    <div id="EditUser_PayRateGrid" class="acore-grid" guid="<%= Model.Id %>"><% Html.RenderPartial("Rate/List", Model.RatesArray); %></div>
    <div class="clear"><br /></div>
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-nonvisit-rate" guid="<%= Model.Id %>">New Task Pay Rate</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div id="EditUser_NonVisitPayRateGrid" class="acore-grid" guid="<%= Model.Id %>"><% Html.RenderPartial("NonVisitRate/List", Model.NonVisitRatesArray); %></div>
    <hr />
    <fieldset>
        <legend>Import Pay Rates</legend>
        <div class="column">
            <div class="row">
                <label for="EditUserRate_UserId" class="fl strong">Duplicate Pay Rates from</label>
                <div class="fr"><%= Html.RatedUser("Rated_UserId", Model.LocationList.FirstOrDefault().ToString(), true, "-- Select User --", new { @id = "EditUserRate_UserId" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row buttons">
                <ul>
                    <li><a href="javascript:void(0)" onclick="User.LoadRate($('#EditUserRate_UserId').val(),'<%=Model.Id %>');">Apply</a></li>
                </ul>
            </div>
        </div>
    </fieldset>
</div>