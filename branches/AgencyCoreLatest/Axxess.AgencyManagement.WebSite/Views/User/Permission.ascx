﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="main">
<%  using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "editUserPermissionsForm" }))
    { %>
    <%= Html.Hidden("UserId", Model.Id, new { @id = "Edit_UserPermission_UserId" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update Permissions</a></li>
        </ul>
    </div>
    <fieldset>
        <legend></legend>
        <div class="wide-column">
            <input id="Edit_User_AllPermissions" type="checkbox" class="radio" value="" />
            <label for="Edit_User_AllPermissions">Select all permissions</label>
        </div>
        <div class="column">
            <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
            <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
            <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
            <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
        </div>
        <div class="column">
            <%= Html.PermissionList("QA", Model.PermissionsArray)%>
            <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
            <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
            <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
            <%= Html.PermissionList("State Survey/Audit", Model.PermissionsArray)%>
         </div>
     </fieldset>
     <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update Permissions</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $(".permission").each(function() {
        if ($(this).attr("tooltip") != undefined) $(this).tooltip({
            track: true,
            showURL: false,
            top: 10,
            left: 10,
            extraClass: "calday",
            bodyHandler: function() { return $(this).attr("tooltip") }
        })
    })
</script>