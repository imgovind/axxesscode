﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<ul>
    <li><h3 class="ac strong">Current Licenses</h3></li>
    <li>
        <span class="grid-quarter">License Type</span>
        <span class="grid-fifth">License Number</span>
        <span class="grid-tenth">Issue Date</span>
        <span class="grid-tenth">Expiration Date</span>
        <span class="grid-fifth">Attachment</span>
        <span>Action</span>
    </li>
</ul>
<%  if (Model != null && Model.Count() > 0) { %>
<ol>
    <%  foreach(var license in Model) { %>
    <li>
        <span class="grid-quarter"><%= license.LicenseType == "Other" && license.OtherLicenseType.IsNotNullOrEmpty() ? license.OtherLicenseType : license.LicenseType %></span>
        <span class="grid-fifth"><%= license.LicenseNumber %></span>
        <span class="grid-tenth"><%= license.InitiationDateFormatted %></span>
        <span class="grid-tenth"><%= license.ExpirationDateFormatted %></span>
        <span class="grid-fifth"><%= license.AssetUrl %></span>
        <span>
            <a class="link edit-license" guid="<%= license.Id %>">Edit</a>
            |
            <a class="link delete-license" guid="<%= license.Id %>">Delete</a>
        </span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h1 class="blue">No Licenses Found</h1>
<%  } %>