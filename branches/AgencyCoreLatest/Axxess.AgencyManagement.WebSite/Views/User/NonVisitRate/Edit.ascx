﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserNonVisitTaskRate>" %>
<div class="wrapper main">
<% using (Html.BeginForm("NonVisitRate/Update", "User", FormMethod.Post, new { @id = "EditUserNonVisitPayRate_Form" }))
   { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUserNonVisitPayRate_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserNonVisitPayRate_UserId" })%>
    <fieldset>
        <legend>Edit Task Pay Rate</legend>
        <div class="wide_column">
            <div class="row">
                <label class="fl strong">Task</label>
                <div class="fr"><strong><%= Model.TaskTitle %></strong></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_RateType" class="fl strong">Rate Type</label>
                <div class="fr"><%=Html.UserRateTypes("RateType", "0" , new { @id = "EditUserNonVisitPayRate_RateType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_VisitRate" class="fl strong">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", Model.Rate, new { @id = "EditUserNonVisitPayRate_Rate", @class = "required floatnum", @maxlength = "8" })%></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>