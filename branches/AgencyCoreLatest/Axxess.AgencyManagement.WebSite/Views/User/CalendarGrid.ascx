﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<%= Html.Telerik().Grid(Model.UserEvents).HtmlAttributes(new { @style = "height: auto; position:relative" }).Name("List_UserCalander_Schedule").Columns(columns =>
   {
       columns.Bound(v => v.PatientName);
       columns.Bound(v => v.TaskName).Template(v => v.IsComplete ? v.TaskName : v.Url).ClientTemplate("<#= IsComplete ? TaskName : Url#>").Title("Task").Width(250);
       columns.Bound(v => v.VisitDate).Title("Date").Width(100);
       columns.Bound(v => v.StatusName).Width(200).Title("Status");
       columns.Bound(v => v.StatusComment).Title(" ").Width(30).Template(v => v.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\">{0}</a>", v.StatusComment) : string.Empty);
       columns.Bound(v => v.VisitNotes).Title(" ").Width(30).Template(v => v.VisitNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\">{0}</a>", v.VisitNotes) : string.Empty);
       columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).Template(v => v.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\">{0}</a>", v.EpisodeNotes) : string.Empty);
       columns.Bound(v => v.Id).Template(v => v.IsMissedVisitReady ? string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('{0}','{1}','{2}');\">Missed Visit Form</a>", v.EpisodeId, v.PatientId, v.Id) : string.Empty).Title(" ").Width(150);
   })
    .Footer(false).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
            if (sortName == "PatientName") {
                if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
            } else if (sortName == "VisitDate") {
                if (sortDirection == "ASC") order.Add(o => o.VisitDate).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.VisitDate).Descending();
            } else if (sortName == "TaskName") {
                if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
            }
        })
    ) %>
<script type="text/javascript">
    $("#List_UserCalander_Schedule .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px', 'bottom': '0px' });
    $("#List_UserCalander_Schedule  table thead.t-grid-header tr th.t-header a.t-link").each(function() {
        if ($(this).attr("href")) {
            var link = $(this).attr("href");
            $(this).attr("href", "javascript:void(0)").click(function() {
                User.LoadUserCalendar("<%=Model.FromDate.Month %>", "<%=Model.FromDate.Year %>", U.ParameterByName(link, "List_UserCalander_Schedule-orderBy"));
            })
        }
  });
	$("#List_UserCalander_Schedule tbody a").addClass("link");
  Agency.ToolTip("#List_UserCalander_Schedule");
</script>