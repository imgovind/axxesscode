﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Pending Claims | <%= Current.AgencyName %></span>
<div class="trical">
 <span class="strong">Branch:</span><%= Html.DropDownList("BranchId", Model.Branches, new { @id = "PendingClaim_BranchCode", @class = "valid" })%> <span class="strong">Insurance:&nbsp;</span><span><%= Html.DropDownList("PrimaryInsurance", Model.Insurances, new { @id = "PendingClaim_InsuranceId", @class = "Insurances" })%></span>
 <div class="buttons editeps"><ul><li><a href="javascript:void(0);" onclick="Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $('#PendingClaim_InsuranceId').val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $('#PendingClaim_InsuranceId').val());">Refresh</a></li>
    <li><a href="javascript:void(0);" onclick="U.GetAttachment('Report/ExportPendingClaims', {'branchId': $('#PendingClaim_BranchCode').val(), 'primaryInsurance':$('#PendingClaim_InsuranceId').val(), 'insuranceName':$('#PendingClaim_InsuranceId').text()});">Export to Excel</a></li>
 </ul></div>
</div>
<div id="pendingClaim_rapContent"><%Html.RenderPartial("/Views/Billing/PendingClaimRap.ascx",Model); %></div>
<div id="pendingClaim_finalContent"><% Html.RenderPartial("/Views/Billing/PendingClaimFinal.ascx", Model); %></div>
<script type="text/javascript">
    $('#PendingClaim_BranchCode').change(function() { Insurance.loadMedicareWithHMOInsuarnceDropDown("PendingClaim", "", false, function() { Billing.ReLoadPendingClaimRap($(this).val(), $('#PendingClaim_InsuranceId').val()); Billing.ReLoadPendingClaimFinal($(this).val(), $('#PendingClaim_InsuranceId').val()); }); });
    $('#PendingClaim_InsuranceId').change(function() { Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $(this).val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $(this).val()); }); 
</script>