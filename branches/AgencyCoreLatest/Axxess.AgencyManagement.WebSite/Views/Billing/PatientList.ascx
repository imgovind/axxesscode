﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
 <% Html.Telerik().Grid<PatientSelection>().Name("BillingSelectionGrid").Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name/MI");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AllMedicare", "Patient", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = Model, name = string.Empty }))
    .Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events.OnLoad("Billing.OnPatientListLoad").OnDataBound("Billing.PatientListDataBound").OnRowSelected("Billing.OnPatientRowSelected")).Render(); %>
