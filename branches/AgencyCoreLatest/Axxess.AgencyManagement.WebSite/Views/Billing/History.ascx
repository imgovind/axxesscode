﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Billing History | <%= Current.AgencyName %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top" style="margin-top: 10px;">
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @class = "billingBranchCode", @id = "BillingHistory_BranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="billingStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option><option value="3">Pending Patients</option><option value="4">Non-Admitted Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><%= Html.DropDownList("list", Model.Insurances?? new List<SelectListItem>(), new { @class = "billingPaymentDropDown", @id = "BillingHistory_InsuranceId" })%></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_billing_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom" style="top: 110px;"><%Html.RenderPartial("PatientList", Model.SelecetdInsurance.IsNotNullOrEmpty()? Model.SelecetdInsurance:string.Empty); %></div>
    </div>
    <div id="billingMainResult" class="ui-layout-center">
        <div class="top">
        <% if(!Current.IsAgencyFrozen) { %>
            <div class="window-menu"><ul id="billingHistoryTopMenu"><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('RAP');">New RAP</a></li><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('Final');">New Final</a></li></ul></div>
            <% } %>
            <div id="billingHistoryClaimData"></div>
        </div>
        <div class="bottom" style="top: 234px;"><% Html.RenderPartial("HistoryActivity", new PatientInsuranceViewData() { PatientId = Guid.Empty, InsuranceId = 0 }); %></div>   
    </div>
</div>
<script type="text/javascript">
	$('#window_billingHistory .layout').Layout();
    $('#window_billingHistory .t-grid-content').css({ height: 'auto', top: "26px" });
    $("#BillingHistory_BranchCode").change(function() { Insurance.loadInsuarnceDropDown('BillingHistory', '', false); });
</script>
<div id="Claim_Update_Container" class="claimupdatemodal hidden"></div>
<div id="Claim_New_Container" class="claimnewmodal hidden"></div>

