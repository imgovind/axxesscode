﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimData>" %>
<% var response = Model != null ? Model.Response.Replace("\r\n", "<br />") : string.Empty; %>
<span class="wintitle">Claim Response | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <li><a onclick="$(this).closest('.window').Close();return false">Close</a></li>
            <li><a onclick="U.GetAttachment('Billing/ClaimResponsePdf', {id:<%=Model.Id %>})" href="javascript:void(0)">Print</a></li>
        </ul>
    </div>
    <fieldset>
        <pre><%= response.Replace('\u0009'.ToString(), "    ") %></pre>
    </fieldset>
    
</div>