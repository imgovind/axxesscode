﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimNewSupplyViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("SupplyBillableAdd", "Billing", FormMethod.Post, new { @id = "new" + Model.Type + "ClaimSupplyForm" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <%= Html.Hidden("UniqueIdentifier", Guid.Empty)%>
    <fieldset>
        <legend>New Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Description:</label>
                <div class="float-right"><%= Html.TextBox("Description", "", new { @class = "text input_wrapper required", @style="width:320px;" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", "", new {@class = "text input_wrapper required", @maxlength = "" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">HCPCS:</label>
                <div class="float-right"><%= Html.TextBox("Code", "", new { @class = "text input_wrapper required" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Date:</label>
                <div class="float-right"><%= Html.TextBox("Date", "", new { @class = "date-picker input_wrapper required" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Unit:</label>
                <div class="float-right"><%= Html.TextBox("Quantity", "", new { @class = "text input_wrapper required numeric" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Unit Cost:</label>
                <div class="float-right">$<%= Html.TextBox("UnitCost", "", new { @class = "text input_wrapper required floatnum" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Total Cost:</label>
                <div class="float-right">
                    $<%= Html.TextBox("NewSupply_TotalCost", "", new { @class = "text input_wrapper", @id = "NewSupply_TotalCost", disabled = "disabled" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add Supply</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script>

</script>