﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
<% var contentId = string.Format("Billing_CenterContent{2}ManagedCare_{0}{1}", Model.BranchId, Model.Insurance, ViewData.ContainsKey("BillUIIdentifier") ? ViewData["BillUIIdentifier"].ToString() : string.Empty); %>

<div id="<%=contentId %>">
<div class="billing">
      <input type="hidden" name="BranchId" value="<%=Model.BranchId %>" />
      <input type="hidden" name="InvoiceType" value="<%=Model.InvoiceType %>" />
     <input type="hidden" name="PrimaryInsurance" value="<%=Model.Insurance %>" />
     <input type="hidden" name="Type" value="ManagedCare" />
     <input type="hidden" name="ManagedCareSort" value="managedcare-name" />
    <input type="hidden" name="IsSortInvert" value="true" />
    <ul>
         <li class="align-center">
            <%=string.Format("{0} | {1}", Model.BranchName, Model.InsuranceName)%>
            <% if (isDataExist)
               { %> [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsXlsByDate', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>','startDate':$('#Billing_ManagedClaimCenterStartDate').val(),'endDate':$('#Billing_ManagedClaimCenterEndDate').val(),'parentSortType': 'branch', 'columnSortType': $('input[name=ManagedCareSort]').val(), 'claimType': 'ManagedCare', });" >Export to Excel</a> ]
            [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdfByDate', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>', 'startDate':$('#Billing_ManagedClaimCenterStartDate').val(),'endDate':$('#Billing_ManagedClaimCenterEndDate').val(),'parentSortType': 'branch', 'columnSortType': $('input[name=ManagedCareSort]').val(), 'claimType': 'ManagedCare', });" >Print</a> ]<%} %>
        </li>
        <li>
            <% if(!Current.IsAgencyFrozen) { %>
            <span class="managedcare-checkbox"></span>
            <% } %>
            <span class="managedcare-name pointer" onclick="Billing.Sort('<%=contentId %>','ManagedCare', 'managedcare-name');">Patient Name</span>
            <span class="managedcare-id pointer" onclick="Billing.Sort('<%=contentId %>','ManagedCare', 'managedcare-id');">Patient ID</span>
            <span class="managedcare-episode pointer" onclick="Billing.Sort('<%=contentId %>','ManagedCare', 'managedcare-episode');">Episode Date</span>
            <span class="managedcareicon">Detail</span>
            <span class="managedcareicon">Visit</span>
            <span class="managedcareicon">Supply</span>
            <span class="managedcareicon">Verified</span>
        </li>
    </ul>
   
    <%  if (isDataExist) { %>
    
    <ol>
        <%  var claims = Model.Claims.Where(c =>c.EpisodeEndDate.Date <= DateTime.Now.Date); %>
        <%  int i = 1; %>
        <%  foreach (var managed in claims)
            {
              var claim = (ManagedBill)managed; %>
              <li class="<%= i % 2 != 0 ? "odd" : "even" %>" onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')">
                    <% if(!Current.IsAgencyFrozen) { %>
                    <span class="managedcare-checkbox"><%= i %>. <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? "<input name='ManagedClaimSelected' class='radio' type='checkbox' value='" + claim.Id + "' id='ManagedClaimSelected" + claim.Id + "' />" : string.Empty %></span>
                    <% } %> 
                    
                     <% if(Model.InvoiceType==1) { %>
                    <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='float-right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/ManagedUB04Pdf', {{ 'Id': '{0}' ,'patientId': '{1}' }});\"><span class='img icon print'></span></a>", claim.Id, claim.PatientId) : string.Empty%>
                    <% } %>
                     <% if(Model.InvoiceType==2) { %>
                    <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='float-right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/ManagedHCFA1500Pdf', {{ 'Id': '{0}' ,'patientId': '{1}' }});\"><span class='img icon print'></span></a>", claim.Id, claim.PatientId) : string.Empty%>
                    <% } %>
                    <% if(Model.InvoiceType==3) { %>
                    <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='float-right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/InvoicePdf', {{ 'Id': '{0}' ,'patientId': '{1}','isForPatient':'false' }});\"><span class='img icon print'></span></a>", claim.Id, claim.PatientId) : string.Empty%>
                    <% } %>
                    
                    <% if(!Current.IsAgencyFrozen) { %>
                    <a href='javascript:void(0);' onclick="<%= (true) ? "UserInterface.ShowManagedClaim('" + claim.Id + "','" + claim.PatientId + "');\" title=\"Final Ready" : "U.Growl('Error: RAP Not Generated', 'error');\" title=\"Not Complete" %>">
                    <% } %>  
                        <span class="managedcare-name"><%= claim.LastName%>, <%= claim.FirstName%></span>
                        <span class="managedcare-id"><%= claim.PatientIdNumber%></span>
                        <span class="managedcare-episode"><span class="very-hidden"><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= claim.EpisodeEndDate != null ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %></span>
                        <span class="managedcareicon"><span class='img icon <%= claim.IsInfoVerified? "success-small" : "error-small" %>'></span></span>
                        <span class="managedcareicon"><span class='img icon <%= claim.IsVisitVerified? "success-small" : "error-small" %>'></span></span>
                        <span class="managedcareicon"><span class='img icon <%= claim.IsSupplyVerified ? "success-small" : "error-small" %>'></span></span>
                        <span class="managedcareicon"><span class='img icon <%= claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified ? "success-small" : "error-small" %>'></span></span>
                    </a>
            </li>
            <%  i++;  } %>
        </ol>
       
    <%  } %>
    </div>
     <%  if (isDataExist && Model.Insurance > 0 && Current.HasRight(Permissions.GenerateClaimFiles) && !Current.IsAgencyFrozen) { %>
            <div class="buttons">
                <ul>
                    <li><a href="javascript:void(0);" onclick="ManagedBilling.loadGenerate('#<%=contentId %>');">Generate Selected</a></li>
                    <li><a href="javascript:void(0);" onclick="ManagedBilling.GenerateAllCompleted('#<%=contentId %>');">Generate All Completed</a></li>
                    <li><a href="javascript:void(0);" onclick="ManagedBilling.Download('#<%=contentId %>');">Print Selected</a></li>
                    
                </ul>
            </div>
        <%  } %>

     <script type="text/javascript">
        $("#<%=contentId %> ol li:first").addClass("first");
        $("#<%=contentId %> ol li:last").addClass("last");
  
    </script>
</div>
