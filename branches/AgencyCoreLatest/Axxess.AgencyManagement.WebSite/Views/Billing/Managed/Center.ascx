﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle">Create Managed Care/Other Insurances Claims | <%= Current.AgencyName %></span>
<% var titleCaseType = Model.ClaimType.ToString().ToTitleCase(); %>
<% var insuranceId = ViewData.ContainsKey("Insurance") && ViewData["Insurance"] != null && ViewData["Insurance"].ToString().IsInteger() ? ViewData["Insurance"].ToString().ToInteger() : 0; %>
<% var branchId = ViewData.ContainsKey("Branch") && ViewData["Branch"] != null && ViewData["Branch"].ToString().IsGuid() ? ViewData["Branch"].ToString().ToGuid() : Guid.Empty; %>
<div id="Billing_ManagedClaimCenterContentMain" class="main wrapper">
    <div class="trical">
         <span class="strong">Branch:</span>
         <%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", branchId.ToString(), new { @id = "Billing_ManagedClaimCenterBranchCode", @class = "report_input valid" })%> 
         <%= Html.InsurancesNoneMedicare("PrimaryInsurance", insuranceId.ToString(), false,"", new { @id = "Billing_ManagedClaimCenterPrimaryInsurance", @class = "Insurances requireddropdown" })%>
         <div>
            <label class="strong" for="Billing_ManagedClaimCenterStartDate">Date Range:
                <input type="text" class="date-picker shortdate" name="ManagedClaimStartDate" value="<%= DateTime.Now.AddDays(-64).ToShortDateString() %>" id="Billing_ManagedClaimCenterStartDate" />
            </label>
            <label class="strong" for="Billing_ManagedClaimCenterEndDate">To
                <input type="text" class="date-picker shortdate" name="ManagedClaimEndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="Billing_ManagedClaimCenterEndDate" />
            </label>
        </div>
         <div class="buttons editeps">
            <ul>
                <li><a href="javascript:void(0);" onclick="ManagedBilling.ReLoadUnProcessedManagedClaim();">Refresh</a></li>
                <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdfByDate', { 'branchId': $('#Billing_ManagedClaimCenterBranchCode').val(), 'insuranceid':$('#Billing_ManagedClaimCenterPrimaryInsurance').val(), 'startDate':$('#Billing_ManagedClaimCenterStartDate').val(),'endDate':$('#Billing_ManagedClaimCenterEndDate').val(),'parentSortType': 'branch','columnSortType':'', 'claimType': '<%=titleCaseType%>' });">Print</a></li>
            </ul>
        </div>
    </div>
    <% var bill = Model;
       bill.Insurance = insuranceId; 
       bill.BranchId = branchId; %>
   <div id="Billing_ManagedClaimCenterContent" style="min-height:200px;"><% Html.RenderPartial("Managed/ManagedGrid", Model); %></div>
</div>