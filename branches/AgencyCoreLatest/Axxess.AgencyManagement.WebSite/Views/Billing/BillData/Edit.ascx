﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EditBillDataViewData>" %>
<div class="wrapper main">
<% var data = Model.ChargeRate; %>
<%  using (Html.BeginForm(Model.TypeOfClaim + "ClaimUpdateBillData", "Billing", FormMethod.Post, new { @id = "editInsuranceBillData" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id, new { @id = "Edit_BillData_ClaimId" })%>
    <%= Html.Hidden("Id", data.Id, new { @id = "Edit_BillData_Task" })%>
      <fieldset class="newmed">
        <legend>Edit Visit Information</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Edit_BillData_Task" class="float-left">Task:</label>
                <div class="float-right" style="width:205px;"><%= data.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_Description" class="float-left">Preferred Description:</label>
                <div class="float-right"><%= Html.TextBox("PreferredDescription", data.PreferredDescription, new { @id = "Edit_BillData_Description", @class = "text input_wrapper required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_RevenueCode" class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", data.RevenueCode, new { @id = "Edit_BillData_RevenueCode", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_HCPCS" class="float-left">HCPCS Code:</label>
                <div class="float-right"><%= Html.TextBox("Code", data.Code, new { @id = "Edit_BillData_HCPCS", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_ChargeRate" class="float-left">Rate:</label>
                <div class="float-right"><%= Html.TextBox("Charge", data.Charge, new { @id = "Edit_BillData_ChargeRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_Modifier" class="float-left">Modifier:</label>
                <div class="float-right">
                    <%= Html.TextBox("Modifier", data.Modifier, new { @id = "Edit_BillData_Modifier", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", data.Modifier2, new { @id = "Edit_BillData_Modifier2", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", data.Modifier3, new { @id = "Edit_BillData_Modifier3", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", data.Modifier4, new { @id = "Edit_BillData_Modifier4", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_BillData_ChargeType" class="float-left">Service Unit Type:</label>
                <div class="float-right"><%=Html.UnitType("ChargeType", data.ChargeType, new { @id = "Edit_BillData_ChargeType", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class='row <%= data.ChargeType=="1" ? "" : "hidden" %>' id="Edit_BillData_PerVisitUnitContent">
                <label for="Edit_BillData_PerVisitUnit" class="float-left">Service Units per Visit:</label>
                <div class="float-right"><%= Html.TextBox("Unit", data.Unit, new { @id = "Edit_BillData_PerVisitUnit", @class = string.Format("text input_wrapper sn {0}", (data.ChargeType == "1" ? "required" : "")), @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit.</em>
            </div>
            <% if (data.IsMedicareHMO)
               { %>
            <% var isRateHidden = (data.ChargeType == "2" || data.ChargeType == "3") ? false : true;%>
            <div class='row <%= isRateHidden ? "hidden" : "" %>' id="Edit_BillData_MedicareHMORateContent">
                <label for="Edit_BillData_ChargeRate" class="float-left">Medicare HMO Rate:</label>
                <div class="float-right"><%= Html.TextBox("MedicareHMORate", data.MedicareHMORate, new { @id = "Edit_BillData_MedicareHMORate", @class = string.Format("text input_wrapper {0}", !isRateHidden ? "required" : ""), @maxlength = "100" })%></div>
                <div class="clear"></div>
                <em>This rate applies for the Medicare final claim per visit if the service unit type is not per a visit.</em>
            </div>
            <%} %>
            <div class="row">
                <%= Html.CheckBox("IsUnitsPerDayOnSingleLineItem", data.IsUnitsPerDayOnSingleLineItem, new { @id = "Edit_BillData_IsUnitsPerDayOnSingleLineItem", @class = "radio float-left" })%>
                <label for="Edit_BillData_IsUnitsPerDayOnSingleLineItem">Check if the units of this visit type are totaled per day on a single line.</label>
            </div>
            <div id="Edit_BillData_IsTimeLimitContent" class='<%= (data.ChargeType == "3" || data.ChargeType == "2") ? "" : "hidden" %>'>
                <div class="row">
                    <%= Html.CheckBox("IsTimeLimit", data.IsTimeLimit, new { @id = "Edit_BillData_IsTimeLimit", @class = "radio float-left" })%>
                    <label for="Edit_BillData_IsTimeLimit">Check if a time limit applies per unit.</label>
                </div>
                <div class='margin <%= data.IsTimeLimit ? "" : "hidden" %>' id="Edit_BillData_TimeLimitContent">
                    <div class="row">
                        <label for="Edit_BillData_TimeLimit" class="float-left">Time Limit:</label>
                        <div class="float-right">
                            Hour:<%= Html.TextBox("TimeLimitHour", data.TimeLimitHour, new { @id = "Edit_BillData_TimeLimitHour", @class = "text  sn", @maxlength = "2" })%>
                            MIN:<%= Html.TextBox("TimeLimitMin", data.TimeLimitMin, new { @id = "Edit_BillData_TimeLimitMin", @class = "text input_wrapper sn", @maxlength = "2" })%>
                        </div>
                        <div class="clear"></div>
                        <em>This applies for visit times which exceed the insurance provider limit</em>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondDescription" class="float-left">Description:</label>
                        <div class="float-right"><%= Html.TextBox("SecondDescription", data.SecondDescription, new { @id = "Edit_BillData_SecondDescription", @class = "text", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondRevenueCode" class="float-left">Revenue Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondRevenueCode", data.SecondRevenueCode, new { @id = "Edit_BillData_SecondRevenueCode", @class = "text sn ", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondHCPCS" class="float-left">HCPCS Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondCode", data.SecondCode, new { @id = "Edit_BillData_SecondHCPCS", @class = "text sn", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsSecondChargeDifferent", data.IsSecondChargeDifferent, new { @id = "Edit_BillData_IsSecondChargeDifferent", @class = "radio float-left" })%>
                         <label for="Edit_BillData_IsSecondChargeDifferent">Check if a rate is different.</label>
                    </div>
                    <div class='row <%= data.IsSecondChargeDifferent ? "" : "hidden" %>'  id="Edit_BillData_SecondChargeContent">
                         <label for="Edit_BillData_SecondCharge" class="float-left">Rate:</label>
                         <div class="float-right"><%= Html.TextBox("SecondCharge", data.SecondCharge, new { @id = "Edit_BillData_SecondCharge", @class = "text input_wrapper", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondModifier" class="float-left">Modifier:</label>
                        <div class="float-right">
                            <%= Html.TextBox("SecondModifier", data.SecondModifier, new { @id = "Edit_BillData_SecondModifier", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", data.SecondModifier2, new { @id = "Edit_BillData_SecondModifier2", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", data.SecondModifier3, new { @id = "Edit_BillData_SecondModifier3", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", data.SecondModifier4, new { @id = "Edit_BillData_SecondModifier4", @class = "text insurance-modifier", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondChargeType" class="float-left">Unit Type:</label>
                        <div class="float-right"><%=Html.UnitType("SecondChargeType", data.SecondChargeType, new { @id = "Edit_BillData_SecondChargeType", @class = "text input_wrapper" })%></div>
                   </div>
                    <div class="row <%= data.SecondChargeType=="1" ? "" : "hidden" %>" id="Edit_BillData_SecondPerVisitUnitContent">
                        <label for="Edit_BillData_SecondPerVisitUnit" class="float-left">Service Units per Visit:</label>
                        <div class="float-right"><%= Html.TextBox("SecondUnit", data.SecondUnit, new { @id = "Edit_BillData_SecondPerVisitUnit", @class = string.Format("text input_wrapper sn {0}", (data.SecondChargeType == "1" ? "required" : "")), @maxlength = "1" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsUnitPerALineItem", data.IsUnitPerALineItem, new { @id = "Edit_BillData_IsUnitPerALineItem", @class = "radio float-left" })%>
                         <label for="Edit_BillData_IsUnitPerALineItem">Check if a unit apply per a line item.</label>
                    </div>
                </div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false'>Save &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#Edit_BillData_ChargeType").change(function() {
        var chargeTypevalue=$(this).val();
        if (chargeTypevalue == 3 || chargeTypevalue == 2 ) 
        {$("#Edit_BillData_IsTimeLimitContent").show();}
        else 
        {$("#Edit_BillData_IsTimeLimitContent").hide();}
        
        if (chargeTypevalue == 1) {
            $("#Edit_BillData_PerVisitUnitContent").show();
            $("#Edit_BillData_PerVisitUnit").addClass("required");
             <% if(data.IsMedicareHMO){ %>$("#Edit_BillData_MedicareHMORateContent").hide().find("input[name=MedicareHMORate]").removeClass("required");<%} %>
        } else {
            $("#Edit_BillData_PerVisitUnitContent").hide();
            $("#Edit_BillData_PerVisitUnit").removeClass("required");
            if(chargeTypevalue!=0){<% if(data.IsMedicareHMO){ %>$("#Edit_BillData_MedicareHMORateContent").show().find("input[name=MedicareHMORate]").addClass("required");<%} %>}
        }
    });
    
    if ($("#Edit_BillData_IsTimeLimit").is(":checked")) 
    {$("#window_ModalWindow").css({ height: 540, "margin-top": -80 });}
    
    $("#Edit_BillData_IsTimeLimit").click(function() {
        if ($(this).is(":checked"))
         {$("#window_ModalWindow").animate({ height: 540, "margin-top": -62 }, 500, function() {$("#Edit_BillData_TimeLimitContent").show();})} 
        else
         {$("#Edit_BillData_TimeLimitContent").hide();$("#window_ModalWindow").animate({ height: 385, "margin-top": 0 }, 500)}
    });

    if ($("#Edit_BillData_IsSecondChargeDifferent").is(":checked"))
     {$("#window_ModalWindow").css({ height: 570, "margin-top": -80 });}
     
    $("#Edit_BillData_IsSecondChargeDifferent").click(function() {
        if ($(this).is(":checked")) 
        {$("#window_ModalWindow").animate({ height: 570, "margin-top": -62 }, 500, function() {$("#Edit_BillData_SecondChargeContent").show();})}
        else 
        {$("#Edit_BillData_SecondChargeContent").hide();$("#window_ModalWindow").animate({ height: 540, "margin-top": -62 }, 500)}
    });
    
    $("#Edit_BillData_SecondChargeType").change(function() {
         var secondChargeTypevalue=$(this).val();
        if (secondChargeTypevalue == 1) 
        {$("#Edit_BillData_SecondPerVisitUnitContent").show();$("#Edit_BillData_SecondPerVisitUnit").addClass("required");} 
        else 
        {$("#Edit_BillData_SecondPerVisitUnitContent").hide();$("#Edit_BillData_SecondPerVisitUnit").removeClass("required");}
    });
    $("#Edit_BillData_Task").change(function (e) { 
       var value = $("#Edit_BillData_Task option:selected").text();
       $("#Edit_BillData_Description").val(value);
    });
</script>