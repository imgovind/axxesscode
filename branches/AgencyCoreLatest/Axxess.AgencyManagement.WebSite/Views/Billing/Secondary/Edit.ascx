﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<span class="wintitle">Secondary Claim | <%= Model.DisplayName %></span>
<%  Html.Telerik().TabStrip().HtmlAttributes(new { @style = "height:auto" }).Name("SecondaryClaimTabStrip")
        .Items(parent => {
            parent.Add().Text("Step 1 of 6:^Demographics").LoadContentFrom("SecondaryClaimInfo", "Billing", new { Id = Model.Id }).Selected(true);
            parent.Add().Text("Step 2 of 6:^Verify Insurance").LoadContentFrom("SecondaryClaimInsurance", "Billing", new { Id = Model.Id }).Enabled(Model.IsInfoVerified);
            parent.Add().Text("Step 3 of 6:^Verify Visits").LoadContentFrom("SecondaryClaimVisit", "Billing", new { Id = Model.Id }).Enabled(Model.IsInsuranceVerified || Model.IsVisitVerified);
            parent.Add().Text("Step 4 of 6:^Verify Remittance").LoadContentFrom("SecondaryClaimRemittance", "Billing", new { Id = Model.Id }).Enabled(Model.IsVisitVerified || Model.IsRemittanceVerified);
            parent.Add().Text("Step 5 of 6:^Verify Supplies").LoadContentFrom("SecondaryClaimSupply", "Billing", new { Id = Model.Id }).Enabled(Model.IsVisitVerified || Model.IsSupplyVerified);
            parent.Add().Text("Step 6 of 6:^Summary").LoadContentFrom("SecondaryClaimSummary", "Billing", new { Id = Model.Id, PatientId = Model.PatientId }).Enabled(Model.IsSupplyVerified);
        }).ClientEvents(events => events.OnSelect("ManagedBilling.managedClaimTabStripOnSelect")).Render(); %>
<script type="text/javascript">    $("#SecondaryClaimTabStrip li.t-item a").each(function() { if ($(this).closest("li").hasClass("t-state-disabled")) { $(this).click(U.BlockClick) } $(this).html("<span><strong>" + ($(this).html().substring(0, $(this).html().indexOf(":^") + 1) + "</strong><br/>" + $(this).html().substring($(this).html().indexOf(":^") + 2)) + "</span>") });</script>
