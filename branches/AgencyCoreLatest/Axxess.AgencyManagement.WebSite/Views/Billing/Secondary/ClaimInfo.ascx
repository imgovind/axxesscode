﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimSnapShotViewData>" %>

<div class="one-half float-right" style="height:268px">
    <table width="100%" height="100%">
        <thead>
            <tr>
                <th colspan="2" class="bigtext"><strong>Secondary Claim</strong></th>
            </tr>
        </thead>
        <tbody>
            <tr style="vertical-align:top; ">
                                <td class="align-right">Patient Name:</td>
                                <td><label class="strong"><%= Model.PatientName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient MRN:</td>
                                <td><label class="strong"><%= Model.PatientIdNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Medicare Number:</td>
                                <td><label class="strong"><%= Model.IsuranceIdNumber %></label></td>
                            </tr>
                        <%  if (Model.Visible) { %>
                            <tr>
                                <td class="align-right">Insurance/Payer:</td>
                                <td><label class="strong"><%= Model.PayorName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Health Plan Id:</td>
                                <td class="align-left"><label class="strong"><%= Model.HealthPlainId %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Authorization Number:</td>
                                <td class="align-left"><label class="strong"><%= Model.AuthorizationNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Billed Date:</td>
                                <td><label class="strong"><%= Model.ClaimDateFormatted %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Payment Date:</td>
                                <td><label class="strong"><%= Model.PaymentDateFormatted %></label></td>
                            </tr>
                             <tr>
                                <td class="align-right">Claim:</td>
                                <td><label class="strong">$<%= Model.ClaimAmount %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Payment:</td>
                                <td><label class="strong">$<%= Model.PaymentAmount %></label></td>
                            </tr>

                        <%  } %>
            
        </tbody>
    </table>
</div>
