﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimSupplyViewData>" %>
<div id="supplyTab" class="wrapper main">
<%  using (Html.BeginForm("SecondaryClaimSupplyVerify", "Billing", FormMethod.Post, new { @id = "secondaryBillingSupplyForm" })){ %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "SecondarySupply_PatientId" })%>
    <div class="billing">
        <h3 class="align_center">Episode: <%= Model.EpisodeStartDate.ToShortDateString()%> &#8211; <%= Model.EpisodeEndDate.ToShortDateString()%></h3>
        <div id="SecondaryBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>(Model.BilledSupplies).Name("SecondaryBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.BillingId).RouteKey("BillingId"); }).ToolBar(commands => {commands.Custom();}).DataBinding(dataBinding => { dataBinding.Ajax()
                    .Select("SupplyBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId, Type = "Secondary" });
                }).HtmlAttributes(new { style = "position:relative;" }).Columns(columns => {
                    columns.Bound(s => s.BillingId).ClientTemplate("<input name='BillingId' type='checkbox'  value='<#= BillingId #>' />").Template(s => s.IdCheckbox).Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Bound(s => s.BillingId).ClientTemplate("<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '<#= BillingId #>', 'Secondary', true);\">Edit</a>").
                        Template(s => "<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '" + s.BillingId + "', 'Secondary', true);\">Edit</a>").Width(140).Title("Action");
                }).ClientEvents(events => events
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
        <div id="SecondaryUnBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Non-Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>(Model.UnbilledSupplies).Name("SecondaryUnBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.BillingId).RouteKey("BillingId"); }).HtmlAttributes(new { style = "position:relative;" }).ToolBar(commands => { commands.Custom(); }).Columns(columns => {
                    columns.Bound(s => s.BillingId).ClientTemplate("<input name='BillingId' type='checkbox'  value='<#= BillingId #>' />").Template(s => s.IdCheckbox).ReadOnly().Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Bound(s => s.BillingId).ClientTemplate("<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '<#= BillingId #>', 'Secondary', false);\">Edit</a>").
                        Template(s => "<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '" + s.BillingId + "', 'Secondary', false);\">Edit</a>").Width(140).Title("Action");
                }).DataBinding(dataBinding =>{ dataBinding.Ajax()
                    .Select("SupplyUnBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId, Type = "Secondary" });
                }).ClientEvents(events => events
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="SecondaryBilling.NavigateBack(3);">Back</a></li>
            <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    SecondaryBilling.Navigate(5, '#secondaryBillingSupplyForm', $("#SecondarySupply_PatientId").val());
    $("#SecondaryBillingSupplyGrid .t-grid-toolbar").empty();
    $("#SecondaryBillingSupplyGrid .t-grid-toolbar").append("<a class=\"t-button float-left\" href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingNewSupply('<%=Model.Id%>','<%=Model.PatientId%>', 'Secondary');\">Add New Supply</a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#SecondaryBillingSupplyGrid'),false, 'Secondary');\"> Mark As Non-Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#SecondaryBillingSupplyGrid'), 'Secondary');\"> Delete </a> <lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#SecondaryUnBillingSupplyGrid .t-grid-toolbar").empty().append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#SecondaryUnBillingSupplyGrid'),true, 'Secondary');\"> Mark As Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#SecondaryUnBillingSupplyGrid'), 'Secondary');\"> Delete </a><lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#secondaryBillingSupplyForm #SecondaryBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
    $("#secondaryBillingSupplyForm #SecondaryUnBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
</script>