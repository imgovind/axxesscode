﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimRemittanceViewData>" %>
<%  using (Html.BeginForm("SecondaryClaimRemittanceVerify", "Billing", FormMethod.Post, new { @id = "secondaryBillingRemittanceForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
<div class="wrapper main">
    <fieldset>
        <legend>Remittance Information</legend>
        <div class="column-wide">
            <div class="row">
                <label>Remittance Date:</label>
                <div class="float-right">
                    <%= Html.DatePicker("RemitDate", Model.RemitDate, true, new { id = "SecondaryClaim_RemitDate" })%>
                </div>
            </div>
            <div class="row">
                <label>Remittance Id:</label>
                <div class="float-right">
                    <%= Html.TextBox("RemitId", Model.RemitId, new { @class = "required" })%>
                </div>
            </div>
            <div class="row">
                <label>Total Adjustment Amount:</label>
                <div class="float-right">
                    <span id="Secondary_TotalAdjustmentAmount"><%= string.Format("{0:C}", Model.TotalAdjustmentAmount) %></span>
                </div>
            </div>
        </div>
    </fieldset>
    
    <div class="billing">
   <% if(Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) 
   {
        var billCategoryVisits = Model.BillVisitDatas[BillVisitCategory.Billable];
        if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
        <ul>
            <li>
                <span class="rap-checkbox"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Scheduled Date</span>
                <span class="visitdate">Visit Date</span>
                <span class="visithcpcs"></span>
                <span class="visitstatus"></span>
                <span class="visitunits"></span>
                <span class="visitcharge"></span>
            </li>
        </ul>
        <% int visitIndex = 0;
            foreach (var disciplineVisitKey in billCategoryVisits.Keys) {
              var visits = billCategoryVisits[disciplineVisitKey];
              if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="discpiline-title"><span><%=  disciplineVisitKey.GetDescription() %></span></li><% 
                        int i = 1;
                        foreach (var visit in visits) { %> 
            <li class="<%= i % 2 != 0 ? "odd notready" : "even notready" %> main-line-item" >
                <label for="<%= "Visit" + visit.EventId.ToString() %>">
                    <span class="rap-checkbox"><%= i%>.</span>
                    <span class="visittype"><%= visit.DisciplineTaskName%></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visithcpcs"></span>
                    <span class="visitstatus"></span>
                    <span class="visitunits"></span>
                    <span class="visitcharge"></span>
                    <span class="adjustment-add-button float-right t-icon t-add" value="<%= visitIndex %>"></span>
                </label>
                
                <% if (visit.Adjustments != null && visit.Adjustments.Count > 0){ %>
                    <ol>
                       <%= Html.Hidden("[" + (visitIndex) + "].Key", visit.EventId)%>
                    <% for (int adjustmentCount = 0; adjustmentCount < visit.Adjustments.Count; adjustmentCount++) {%>
                       <li class="adjustment-item <%= adjustmentCount % 2 != 0 ? "odd notready" : "even notready" %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                        
                        <div class="align-right">
                            <div class="rem-fourth-width">
                                <label for="[<%= visitIndex %>].AdjGroup" class="strong">Adjustment Group Code:</label>
                                <%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjGroup",
                                    visit.Adjustments[adjustmentCount].AdjGroup, new { @class = "rem-half-width required" })%>
                            </div>
                            <div class="rem-fourth-width">
                                <label for="[<%= visitIndex %>].AdjData.AdjReason" class="strong">Adjustment Reason Code:</label>
                                <%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjReason",
                                                                        visit.Adjustments[adjustmentCount].AdjData.AdjReason, new { @class = "rem-half-width required" })%>
                            </div>
                            <div class="rem-fourth-width">
                                <label for="[<%= visitIndex %>].AdjData.AdjAmount" class="strong">Adjustment Amount:</label>
                                $<%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjAmount",
                                    visit.Adjustments[adjustmentCount].AdjData.AdjAmount, new { @class = "rem-half-width floatnum adjustment-amount required" })%>
                            </div>
                            <div onclick="SecondaryBilling.DeleteAdjustment($(this).parent().parent());" class="remit-delete-icon t-icon t-delete"/>
                        </div>
                     </li>
                  <% } %>
                       </ol>
                 <% } else { %>
                    <ol class="hide-list">
                       <%= Html.Hidden("[" + (visitIndex) + "].Key", visit.EventId)%>
                    </ol>
                   <% } %>
                
                
            </li><% i++; visitIndex++;
                        } %>
        </ol><%
                }
            }
        }
    } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SecondaryBilling.NavigateBack(2);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    U.BasicTabSetup($("#secondaryBillingRemittanceForm"));
    SecondaryBilling.Navigate(4, "#secondaryBillingRemittanceForm", $("#SecondaryVisit_PatientId").val());
    $("#SecondaryClaimTabStrip-4 ol").each(function() { $("li:last", $(this)).addClass("last") });
    SecondaryBilling.InitRemittance();
</script>