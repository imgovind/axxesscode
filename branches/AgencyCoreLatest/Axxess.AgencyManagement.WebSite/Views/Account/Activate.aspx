﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Activate Account - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="activateaccount-wrapper">
        <div id="activateaccount-window">
            <% if (Model.UserId != Guid.Empty) { %>
                <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Account Activation</span></div>
                <div class="box">
                    <div id="messages"></div>
                    <h1>Welcome to Axxess&#8482; Home Health Management System.</h1>
                    To activate your account, follow the steps below.
                    <div class="row">&#160;</div>
                    <h2><strong>Step 1</strong> - Verify your information below.</h2>
                     <% using (Html.BeginForm("Activate", "Account", FormMethod.Post, new { @id = "activateAccountForm", @class = "activateaccount" })) %>
                    <% { %>
                    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Activate_User_Id" })%>
                    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Activate_User_AgencyId" })%>
                    <%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "Activate_User_EmailAddress" })%>
                    <div class="row">
                        <label class="fl">Name</label>
                        <div class="fr"><%= Model.Name %></div>
                    </div>
                    <div class="row">
                        <label class="fl">Email Address</label>
                        <div class="fr"><%= Model.EmailAddress%></div>
                    </div>
                    <div class="row">
                        <label class="fl">Agency Name</label>
                        <div class="fr"><%= Model.AgencyName%></div>
                    </div>
                    <div class="row">&#160;</div>
                    <h2><strong>Step 2</strong> - Enter a new password.</h2>
                    <div class="row">
                        <label for="Password" class="fl">Password</label>
                        <div class="fr"><%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength="20" })%></div>
                    </div>
                    <div class="row">Note: Your Electronic Signature will be the same as your new password.</div>
                    <div class="bottom-right">
                        <input type="submit" value="Activate Account" class="button" style="clear: both; width: 150px!important;" />
                    </div>
                    <% } %>
                </div>
            <% } else { %>
                <div class="box-header"><span class="img icon axxess"></span><span class="title">The page you requested was not found.</span></div>
                <div class="box">
                    <div class="notification warning">You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.
                    </div>
                </div>
            <% } %>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(false)
         .DefaultGroup(group => group
             .Add("jquery-1.7.1.min.js")
             .Add("Plugins/Other/blockui.min.js")
             .Add("Plugins/Other/form.min.js")
             .Add("Plugins/Other/validate.min.js")
             .Add("Plugins/Other/jgrowl.min.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
             .Compress(true).Combined(true)
             .CacheDurationInDays(1)
             .Version(Current.AssemblyVersion))
        .OnDocumentReady(() =>
        { 
    %>
    Activate.Init();
    <% 
        }).Render(); %>
</body>
</html>