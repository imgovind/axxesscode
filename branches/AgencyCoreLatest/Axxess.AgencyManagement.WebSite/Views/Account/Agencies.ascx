﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyLite>>" %>
<% if (Model != null)
   { %>
<div id="agencySelectionLinks">
    <div><h1>Select Agency Profile</h1></div>
    <ul> 
    <% foreach(var agency in Model) { %>
        <li>
            <a href="javascript:void(0);" onclick="Logon.Select('<%= agency.Id %>', '<%= agency.UserId %>');" title="<%= agency.Name %>">
                <span class="agency-name"><%= agency.Name%></span>
                <span><%= agency.Title %> profile created on <%= agency.Date %></span>
            </a>
        </li> 
    <% } %>
    </ul> 
</div>
<% } %>