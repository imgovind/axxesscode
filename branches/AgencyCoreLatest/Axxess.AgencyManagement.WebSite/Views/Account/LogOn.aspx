﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Login - Axxess Home Health Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <%= Html.Telerik()
            .StyleSheetRegistrar()
            .DefaultGroup(group => group
            .Add("account.css")
            .Combined(true)
            .Compress(true)
            .CacheDurationInDays(1)
            .Version(Current.AssemblyVersion))
        %>
        <link href="/Images/favicon.ico" rel="shortcut icon" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/Images/touch-icon-ipad-retina.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/Images/touch-icon-iphone-retina.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/Images/touch-icon-ipad.png" />
        <link rel="apple-touch-icon-precomposed" href="/Images/touch-icon-iphone.png" />
        <link href="/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    </head>
    <body>
    
        <div id="login">
            <div class="login-left">
                
                <% var browser = HttpContext.Current.Request.Browser; %>
                <% if (AppSettings.BrowserCompatibilityCheck == "false" || browser.IsBrowserAllowed()) { %>
                
                    <img src="/Images/Login/logo.png" class="logo">
                    
                    <noscript>
                        <div class="alert alert-error">
                            <h4>JavaScript Check</h4>
                            <p>Your browser version does not meet our minimum browser requirements.</p>
                            <p>Our software requires JavaScript to be enabled in your browser.</p>
                            <p>Please contact Axxess for assistance on how to enable JavaScript.</p>
                        </div>
                    </noscript> 
                    
                    <div class="login-form-wrapper">
                        <% using (Html.BeginForm(
                            "LogOn", 
                            "Account", 
                            FormMethod.Post, 
                            new { 
                                @id = "login-form", 
                                @class = "login", 
                                @returnUrl = Request.QueryString["returnUrl"] 
                            })) { %>
                            
                            <div style="padding:1em">
                                <table>
                                    <tr>
                                        <td class="label"><label for="Login_UserName">Email</label></td>
                                        <td><input id="Login_UserName" name="UserName" class="required" type="text"></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><label for="Login_Password">Password</label></td>
                                        <td><input id="Login_Password" name="Password" class="required" type="password"></td>
                                    </tr>
                                </table>
                            </div>
                            <p id="Login_RememberMe_Forgot">
                                <input id="Login_RememberMe" name="RememberMe" checked="checked" value="true" type="checkbox">
                                <label for="Login_RememberMe">Remember Me</label>
                                |
                                <a href="Forgot" id="Login_Forgot">Forgot your Password?</a>
                            </p>
                            <input id="Login_Submit" type="submit" value="Login" >
                        <% } %>
                    </div>

                    <div class="login-loading">
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-circle"></i>
                    </div>
                    
                    <div class="login-message">
                        <div class="message"></div>
                        <div class="btn-wrapper">
                            <a href="#" class="btn-custom btn-cancel">Cancel</a>
                            <a href="#" class="btn-custom btn-ok">OK</a>
                        </div>
                    </div>
                    
                    <div class="login-agency-selection"></div>

                    <div class="didyouknow">
                        <h2>DID YOU <b>KNOW?</b></h2>
                        <div>
                        <%
                            var didyouknows = new string[] {
                                "Users from <strong>36</strong> states login to Axxess daily!",
                                "Over <strong>160,000</strong> patients are being served using Axxess Agencycore!",
                                "Over <strong>35,000</strong> Home Health Professionals use Axxess daily!",
                                "Over <strong>20,000,000</strong> documents were created in Agencycore in the past year!",
                                "More agencies choose <strong>AXXESS</strong> than any other Home Health Software!",
                                "<a href=\"http://axxessweb.com/referrals\" target=\"_blank\">You can get rewarded for referring your friends to <strong>AXXESS</strong> !</a>"
                            };
                            var rand = new Random();
                            // didyouknows[rand.Next(0, didyouknows.Count())]
                            foreach (string s in didyouknows) { %>
                                <p><%= s %></p>
                            <% } %>
                        </div>
                    </div>
                
                <% } else { %>
                
                    <div class="login-message">
                        <h4>Browser Compatibility Check</h4>
                        <p>Your browser version does not meet our minimum browser requirements.</p>
                        <p>Our software supports Internet Explorer 8, FireFox 3+, and Chrome.</p>
                        <p>Please download Internet Explorer 8 or Firefox by clicking on the links below.</p>
                        <ul>
                            <li><a href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer Download">Download Internet Explorer</a></li>
                            <li><a href="http://www.mozilla.com/en-US/firefox" title="Firefox Download">Download Firefox</a></li>
                            <li><a href="http://www.google.com/intl/en/chrome/browser/" title="Chrome Download">Download Chrome</a></li>
                        </ul>
                    </div>
                    
                <% } %>
                
               <%-- <div class="ad">
                    
                </div>--%>
                
                <footer>
                    <img src="/Images/Login/axxess_nahc_login.jpg" class="Axxess goes to NAHC">
                    <%--<a href="http://axxessweb.com/referrals" class="referral" target="_blank">
                        <h3><b>REFERRAL</b><br>PROGRAM</h3>
                        Tell your friends about <b>AXXESS</b><br>and take advantage of our referral program!</a>
                    </a>--%>
                    <p class="rights">Axxess | Copyright © 2008 - 2013 | All Rights Reserved | Dallas, Texas</p>
                </footer>
            </div>
            <div class="login-right"><div class="upcoming-events"></div></div>
        </div>
        
        <% Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %> 
            Logon.Init(); 
        <% }).Render(); %>
        
        <% if (Model.IsLocked) { %>
        
            <script type="text/javascript">
                $("#Login_UserName").attr("disabled", "disabled");
                $("#Login_Password").attr("disabled", "disabled");
                $("#Login_RememberMe").hide();
                $("#Login_Forgot").hide();
                $("#Login_Button").hide();
                $("#messages").empty().removeClass().addClass("notification error").append('<span>You have made too many failed login attempts. Please contact your Agency/Companys Administrator.</span>');
            </script>
        
        <% } %>
    </body>
</html>

