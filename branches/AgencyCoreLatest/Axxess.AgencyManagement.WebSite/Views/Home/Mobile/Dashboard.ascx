﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Axxess.Core.Infrastructure" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Dashboard | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <ul id="widget-column-1" class="widgets">
        <li class="widget stationary" id="intro-widget">
            <div class="widget-head">
                <h5>Hello,&#160;<%= Current.DisplayName %>!</h5>
            </div>
            <% Html.RenderPartial("~/Views/Widget/CustomMessage.ascx"); %>
        </li>
        <li class="widget" id="local-widget">
            <div class="widget-head">
                <h5>Local</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Local.ascx"); %>
            </div>
        </li>
        <li class="widget stationary" id="news-widget">
            <div class="widget-head">
                <h5>News/Updates</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/NewsFeed.ascx"); %>    
            </div>
        </li>
        <li class="widget" id="messages-widget">
            <div class="widget-head">
                <h5>Messages</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Messages.ascx"); %>
            </div>
        </li>
        
        <% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        <li class="widget" id="recertsdue-widget">
            <div class="widget-head">
                <h5>Past Due Recerts</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/RecertsPastDue.ascx"); %>
            </div>
        </li>
        <% } %>
        
        <% if (!Current.IsCommunityLiason) { %>
        <li class="widget" id="schedule-widget">
            <div class="widget-head">
                <h5>My Scheduled Tasks</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Schedule.ascx"); %>
            </div>
        </li>
        <% } %>
        <% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        <li class="widget" id="upcomingrecert-widget">
            <div class="widget-head">
                <h5>Upcoming Recerts</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/RecertsUpcoming.ascx"); %>
            </div>
        </li>
        <% } %>
        
        <% if (!Current.IsCommunityLiason) { %>
        <li class="widget" id="birthday-widget">
            <div class="widget-head">
                <h5>Patient Birthdays</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Birthday.ascx"); %>
            </div>
        </li>
        <% } %>
        <% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
        <li class="widget" id="claims-widget">
            <div class="widget-head">
                <h5>Outstanding Claims</h5>
            </div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Claims.ascx"); %>
            </div>
        </li>
        <% } %>
    </ul>
</div>
<script type="text/javascript">
    Home.Init();
</script>