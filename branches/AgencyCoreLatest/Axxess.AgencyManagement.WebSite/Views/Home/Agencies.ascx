﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyLite>>" %>
<% if (Model != null)
   { %>
<div id="agencySelectionLinks">
    <h1>Switch Account</h1>
    <ul> 
    <% foreach(var agency in Model) { %>
        <% if (agency.Id != Current.AgencyId) { %>
        <li>
            <a href="javascript:void(0);" onclick="Home.SwitchAccount('<%= agency.Id %>', '<%= agency.UserId %>');" title="<%= agency.Name %>">
                <span class="agency-name"><%= agency.Name%></span>
                <span><%= agency.Title %> profile created on <%= agency.Date %></span>
            </a>
        </li> 
        <% } %>
    <% } %>
    </ul> 
</div>
<div class="buttons">
    <ul>
        <li>
            <a href="javascript:void(0);" onclick="$(this).closest('.window').Close();">Close</a>
        </li>
    </ul>
</div>
<% } %>