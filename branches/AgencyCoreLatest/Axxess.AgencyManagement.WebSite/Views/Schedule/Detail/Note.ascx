﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<% DateTime startdate=Model.StartDate;%>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task:</label></td>
            <td>
                <label for="Schedule_Detail_Billable" class="bold">Billable:</label>
                <%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "Schedule_Detail_Billable", @class = "radio" })%>
                <label for="Schedule_Detail_Payable" class="bold">Payable:</label>
                <%= Model.IsPayable == true ? string.Format("<input type='checkbox' id='Schedule_Detail_IsPayable' class='radio' value='true' checked='checked' name='IsPayable'>") : string.Format("<input type='checkbox' id='Schedule_Detail_IsPayable' class='radio' value='true' name='IsPayable'>")%>
            </td>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Travel Time In:</label></td>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Travel Time Out:</label></td>
        </tr>
        <tr>
            <td>
            <%if (Model != null && Model.GCode.IsNotNullOrEmpty()){ %>
            <%= Html.DisciplineTypesWithGcode("DisciplineTask", Model.DisciplineTask, Model.PatientId, Model.GCode, new { @id = "Schedule_Detail_DisciplineTask", @class = "requireddropdown" })%>
            <%}else{ %>
            <%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, Model.PatientId, new { @id = "Schedule_Detail_DisciplineTask", @class = "requireddropdown" })%>
            <%} %>
            </td>
            
            <td><% if(Model.IsSkilledNurseNote()||Model.IsOASISSOC()||Model.IsOASISRecert()){ %><label class="float-left">Supply:</label> <%} %>
               <% if (Model.IsSkilledNurseNote())
                  { %> <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" class=\"float-left\" >Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li></ul></div> 
                  <%} else if(Model.IsOASISSOC()||Model.IsOASISRecert()){ %>
                    <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.LoadSupplyWorkSheet('{0}','{1}','{2}','{3}');\" class=\"float-left\" >Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId, Model.DisciplineTask)%></li></ul></div>
                  <%} %>
            </td>
            <td><input type="text" size="10" id="Schedule_Detail_TravelTimeIn" name="TravelTimeIn" class="time-picker" value="<%= Model.TravelTimeIn %>" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TravelTimeOut" name="TravelTimeOut" class="time-picker" value="<%= Model.TravelTimeOut %>" /></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date:</label></td>
            <td><label for="Schedule_Detail_Status" class="bold">Actual Visit Date:</label></td>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Visit Time In:<label></td>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Visit Time Out:</label></td>
        </tr>
        <tr>
            <td><input type="text" class="date-picker required" name="EventDate" value="<%= Model.EventDate %>" id="Schedule_Detail_EventDate" /></td>
            <td><input type="text" class="date-picker required" name="VisitDate" value="<%= Model.VisitDate %>" id="Schedule_Detail_VisitDate" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeIn" name="TimeIn" class="time-picker" value="<%= Model.TimeIn %>" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeOut" name="TimeOut" class="time-picker" value="<%= Model.TimeOut %>" /></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_Status" class="bold">Status:</label></td>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Assigned To:</label></td>
            <td><label for="Schedule_Detail_Surcharge" class="bold">Surcharge:</label></td>
            <td><label for="Schedule_Detail_AssociatedMileage" class="bold">Associated Mileage:</label></td>
        </tr>
        <tr>
            <td><%= Html.Status("Status", Model.Status, Model.DisciplineTask, Model.EventDate.ToDateTime(), new { @id = "Schedule_Detail_Status" }) %></td>
            <td><% if ((!Model.IsComplete || Model.DisciplineTask == (int) DisciplineTasks.FaceToFaceEncounter) && !Model.IsUserDeleted){ %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users requireddropdown" })%><% } else { %><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName %><% } %></td>
            <td><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "Schedule_Detail_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></td>
            <td><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "Schedule_Detail_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></td>
        </tr>
        <% if (Model.Discipline.IsEqual(Disciplines.Orders.ToString())) { %>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td colspan="4"><label for="Schedule_Detail_PhysicianId" class="bold">Physician:</label></td>
        </tr>
        <tr>
            <td colspan="4">
                <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Schedule_Detail_PhysicianId", @class = "Physicians" })%>
                <script type="text/javascript"> $("#Schedule_Detail_PhysicianId").PhysicianInput(); </script>
            </td>
        </tr>
        <% } else { %>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td colspan="4"><label for="Schedule_Detail_ServiceLocation" class="bold">Service Location (Q Codes):</label></td>
        </tr>
        <tr>
            <td><%if (Model.IsServiceLocationRequired)
                  { %>
                <%= Html.ServiceLocations("ServiceLocation", Model.ServiceLocation, new { @id = "Schedule_Detail_ServiceLocation", @class = "requireddropdown valid" })%>
            <%}
                  else
                  { %>
                <%= Html.ServiceLocations("ServiceLocation", Model.ServiceLocation, new { @id = "Schedule_Detail_ServiceLocation" })%>
                <%} %>
            </td>
            <td colspan="3">
            </td>
        </tr>
        <% } %>
    </tbody></table>               
</fieldset>
    
<%--<script type="text/javascript">
    var ActivateDate = new Date("05/01/2013");
    var StartDate = new Date("<%=startdate %>");
    if (StartDate >= ActivateDate) {
        alert("ok");
        $("#Schedule_Detail_ServiceLocation").addClass("requireddropdown valid");
    } else {
    alert("not ok");
    }
</script>--%>