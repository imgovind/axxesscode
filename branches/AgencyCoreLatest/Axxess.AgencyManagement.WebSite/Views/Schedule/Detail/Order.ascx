﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<%= Html.Hidden("VisitDate", Model.EventDate, new { @id = "" })%>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task:</label></td>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date:</label></td>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Clinician:</label></td>
            <td><label for="Schedule_Detail_Status" class="bold">Status:</label></td>
        </tr><tr>
            <td><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, Model.PatientId, new { @id = "Schedule_Detail_DisciplineTask", @class = "requireddropdown" })%></td>
            <td><input type="text" class="date-picker required" name="EventDate" value="<%= Model.EventDate %>" id="Schedule_Detail_EventDate" /></td>
            <td><% if (!Model.IsComplete) { %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users requireddropdown" })%><% } else { %><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName%><% } %></td>
            <td><%= Html.Status("Status", Model.Status, Model.DisciplineTask, Model.EventDate.ToDateTime(), new { @id = "Schedule_Detail_Status" })%></td>
        </tr>
    </tbody></table>               
</fieldset>
