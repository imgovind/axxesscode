﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Private Duty Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west ui-layout-west">
        <div class="top"><%  Html.RenderPartial("PrivateDuty/Center/PatientFilters"); %></div>
        <div class="bottom"><%  Html.RenderPartial("PrivateDuty/Center/PatientSelector"); %></div>
    </div>
    <div id="PDScheduleMainResult" class="ui-layout-center">
        <div class="window-menu">
            <ul>
                <li><a class="schedule-employee">Schedule Employee</a></li>
                <li><a class="reassign-schedule">Reassign Schedules</a></li>
                <li><a class="master-calendar">Master Calendar</a></li>
            </ul>
        </div>
        <div class="main">
            <div class="pd-calendar"></div>
        </div>
    </div>
</div>