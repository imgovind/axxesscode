﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  Html.Telerik().Grid<PatientSelection>().Name("PrivateDutyScheduleSelectionGrid").Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
        columns.Bound(p => p.DisplayNameWithMi).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("PrivateDutyCenterPatientGrid", "Schedule", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = 0, name = string.Empty })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("Schedule.PrivateDuty.Center.PatientSelector.Init")
        .OnRowSelected("Schedule.PrivateDuty.Center.PatientSelector.Select")
    ).Render(); %>