﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MasterCalendarViewData>" %>
<%  var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%  var startDate = Model.StartDate; %>
<%  var endDate = Model.EndDate; %>
<%  var currentDate = Model.StartDate.AddDays(-startWeekDay); %>
<%  if (Model.PreviousEpisodeId.IsNotEmpty()) { %>
<span class="trical">
    <span class="abs-left">
        <a onclick="Schedule.loadMasterCalendarNavigation('<%= Model.PreviousEpisodeId %>','<%= Model.PatientId %>')">
            <span class="largefont">&#171;</span>
            Previous Episode
        </a>
    </span>
</span>
<%  } %>
<%  if (Model.NextEpisodeId.IsNotEmpty()) { %>
<span class="trical">
    <span class="abs-right">
        <a onclick="Schedule.loadMasterCalendarNavigation('<%= Model.NextEpisodeId %>','<%= Model.PatientId %>')">
            Next Episode
            <span class="largefont">&#187;</span>
        </a>
    </span>
</span>
<%  } %>
<div class="buttons">
    <ul>
        <li><a href="javascript:void(0)" onclick="U.GetAttachment('Schedule/MasterCalendarPdf', { 'patientId': '<%= Model.PatientId %>', 'episodeId': '<%= Model.Id %>', 'showMissedVisits': $('#MasterCalendar_ShowMissedVisits').attr('checked') != undefined })">Print</a></li>
    </ul>
</div>
<div style="width: 160px;" class="center">
    <input type="checkbox" id="MasterCalendar_ShowMissedVisits" class="radio">
    <label for="MasterCalendar_ShowMissedVisits">Show Missed Visits</label>
</div>
<label>Patient Name:</label>
<span class="largefont strong"><%= Model.DisplayName %></span>
<%  if (Model.FrequencyList.IsNotNullOrEmpty()) { %>
<br />
<label>Frequencies:</label>
<span class="largefont strong"><%= Model.FrequencyList %></span>
<%  } %>
<table id="masterCalendarTable" class="masterCalendar">
    <thead>
        <tr>
            <th></th>
            <th>Sun</th>
            <th>Mon</th>
            <th>Tue</th>
            <th>Wed</th>
            <th>Thu</th>
            <th>Fri</th>
            <th>Sat</th>
        </tr>
    </thead>
    <tbody>
<%  for (int i = 1; i <= 10; i++) { %>
        <tr class="<%= i == 10 ? "lastTr" : string.Empty %>">
            <th>Week <%= i %></th>
    <%  int addedDate = (i - 1) * 7; %>
    <%  for (int j = 0; j <= 6; j++) { %>
        <%  var specificDate = currentDate.AddDays(j + addedDate); %>
        <%  if (specificDate < startDate || specificDate > endDate) { %>
            <td></td>
        <%  } else { %>
            <%  var currentSchedules = Model.ScheduleEvents.FindAll(e => e.EventDate.ToDateTime() == specificDate); %>
            <%  if (currentSchedules.Count > 0) { %>
            <td onmouseover="$(this).addClass('active')" onmouseout="$(this).removeClass('active')" class="<%= j == 6 ? "lastTd" : string.Empty %>">
                <div>
                    <span><%= string.Format("{0:MM/dd}", specificDate) %><br /></span>
                <%  foreach (var evnt in currentSchedules) { %>
                    <%  if (evnt.Discipline != Disciplines.Orders.ToString() && evnt.Discipline != Disciplines.ReportsAndNotes.ToString()) { %>
                        <%  if (!evnt.IsMissedVisit && evnt.TimeIn.IsNotNullOrEmpty() && evnt.TimeOut.IsNotNullOrEmpty()) { %>
                    <span class="desc"><%= evnt.TimeIn %> &#8211; <%= evnt.TimeOut %><br /></span>
                        <%  } %>
                    <span class='<%= evnt.IsMissedVisit ? "eventTitle red-text hidden missed-visit" : "eventTitle" %>'><%= evnt.DisciplineTask.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetCustomShortDescription().ToUpper() %><br /></span>
                    <span class='<%= evnt.IsMissedVisit ? "desc missed-visit red-text hidden" : "desc" %>'>Employee: <%= evnt.UserName %><br /></span>
                    <span class='<%= evnt.IsMissedVisit ? "desc missed-visit red-text hidden" : "desc" %>'>Status: <%= evnt.StatusName %><br /></span>
                    <%  } %>
                <%  } %>
                </div>
            </td>
            <%  } else { %>
            <td>
                <div>
                    <span><%= string.Format("{0:MM/dd}", specificDate) %></span>
                </div>
            </td>
            <%  } %>
        <%  } %>
    <%  } %>
        </tr>
<%  } %>
    </tbody>
</table>
<script type="text/javascript" language="javascript">
    var checkBox = $('#MasterCalendar_ShowMissedVisits');
    U.ShowIfChecked(checkBox, $('.missed-visit.eventTitle'));
    checkBox.change(function() {
        if (checkBox.prop("checked") == true) {
            $('.missed-visit.desc').removeClass("hidden");
        } else {
            $('.missed-visit.desc').addClass("hidden");
        }
    });
</script>