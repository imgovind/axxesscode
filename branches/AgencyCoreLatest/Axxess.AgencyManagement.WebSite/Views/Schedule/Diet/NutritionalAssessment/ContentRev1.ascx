﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <td>
            
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="halfOfTd">
<label for="<%= Model.Type %>_Height1" class="float-left">Height: </label>

<%= Html.TextBox(Model.Type + "_Height1", data.AnswerOrEmptyString("Height1"), new { @class = "sn", @id = Model.Type + "_Height1", @maxlength = "2" })%>'
<%= Html.TextBox(Model.Type + "_Height2", data.AnswerOrEmptyString("Height2"), new { @class = "sn", @id = Model.Type + "_Height2", @maxlength = "2" })%>"
</div>
<div class="halfOfTd">
<label for="<%= Model.Type %>_Weight" class="float-left">Weight: </label> 
<%= Html.TextBox(Model.Type + "_Weight", data.AnswerOrEmptyString("Weight"), new { @class = "sn", @id = Model.Type + "_Weight", @maxlength = "" })%>lbs
<label for="<%= Model.Type %>_BMI">BMI: </label>
<%= Html.TextBox(Model.Type + "_BMI", data.AnswerOrEmptyString("BMI"), new { @class = "sn", @id = Model.Type + "_BMI", @maxlength = "" })%>
</div>
<br />

<label for="<%= Model.Type %>_WeightStatus" class="float-left">Weight Status: </label>
<div class="float-right">
<%  var weightStatusLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "Underweight", Value = "Underweight" },
                new SelectListItem { Text = "Healthy Weight", Value = "Healthy Weight" },
                new SelectListItem { Text = "Overweight", Value = "Overweight" },
                new SelectListItem { Text = "Obese", Value = "Obese" },
                new SelectListItem { Text = "Morbidly Obese", Value = "Morbidly Obese" }
            }, "Value", "Text", data.AnswerOrDefault("WeightStatus", ""));%>
<%= Html.DropDownList(Model.Type + "_WeightStatus", weightStatusLevel, new { @id = Model.Type + "_WeightStatus", @class = "" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_WeightStatus" class="float-left">Past Medical History: </label>
<div class="float-right">
<%  var medicalHistoryLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "DM", Value = "DM" },
                new SelectListItem { Text = "CKD", Value = "CKD" },
                new SelectListItem { Text = "HTN", Value = "HTN" },
                new SelectListItem { Text = "CVA", Value = "CVA" },
                new SelectListItem { Text = "COPD", Value = "COPD" },
                new SelectListItem { Text = "Cancer", Value = "Cancer" }
            }, "Value", "Text", data.AnswerOrDefault("MedicalHistory", ""));%>
<%= Html.DropDownList(Model.Type + "_MedicalHistory", medicalHistoryLevel, new { @id = Model.Type + "_MedicalHistory", @class = "" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_SurgicalHistory" class="float-left">Surgical History </label> 
<div class="float-right">
<%= Html.TextBox(Model.Type + "_SurgicalHistory", data.AnswerOrEmptyString("SurgicalHistory"), new { @class = "", @id = Model.Type + "_SurgicalHistory" })%>
</div>
<div class="clear" />              
<label for="<%= Model.Type %>_CurrentDiagnosis" class="float-left">Current Diagnosis: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_CurrentDiagnosis", data.AnswerOrEmptyString("CurrentDiagnosis"), new { @class = "", @id = Model.Type + "_CurrentDiagnosis" })%>
</div>  
<div class="clear" />
<label for="<%= Model.Type %>_FoodAllergies" class="float-left">Food Allergies: </label>  
<div class="float-right">           
<%= Html.TextBox(Model.Type + "_FoodAllergies", data.AnswerOrEmptyString("FoodAllergies"), new { @class = "", @id = Model.Type + "_FoodAllergies" })%>
</div>  
<div class="clear" />
<label for="<%= Model.Type %>_FoodIntolerances" class="float-left">Food Intolerances: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_FoodIntolerances", data.AnswerOrEmptyString("FoodIntolerances"), new { @class = "", @id = Model.Type + "_FoodIntolerances" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Dentition" class="float-left">Dentition: </label>
<div class="float-right">
<%  var dentitionLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "Adequate", Value = "Adequate" },
                new SelectListItem { Text = "Missing", Value = "Missing" },
                new SelectListItem { Text = "Partial", Value = "Partial" },
                new SelectListItem { Text = "Dentures", Value = "Dentures" },
                new SelectListItem { Text = "Edentulous", Value = "Edentulous" }
            }, "Value", "Text", data.AnswerOrDefault("Dentition", ""));%>
<%= Html.DropDownList(Model.Type + "_Dentition", dentitionLevel, new { @id = Model.Type + "_Dentition", @class = "" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_ChewingAbility" class="float-left">Chewing Ability: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_ChewingAbility", data.AnswerOrEmptyString("ChewingAbility"), new { @class = "", @id = Model.Type + "_ChewingAbility" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_SwallowingAbility" class="float-left">Swallowing Ability: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_SwallowingAbility", data.AnswerOrEmptyString("SwallowingAbility"), new { @class = "", @id = Model.Type + "_SwallowingAbility" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_CurrentDiet" class="float-left">Current Diet/Texture: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_CurrentDiet", data.AnswerOrEmptyString("CurrentDiet"), new { @class = "", @id = Model.Type + "_CurrentDiet" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_LiquidConsistency" class="float-left">Liquid Consistency: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_LiquidConsistency", data.AnswerOrEmptyString("LiquidConsistency"), new { @class = "", @id = Model.Type + "_LiquidConsistency" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_PreviousDiets" class="float-left">Previous Diets: </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_PreviousDiets", data.AnswerOrEmptyString("PreviousDiets"), new { @class = "", @id = Model.Type + "_PreviousDiets" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_HasClient" class="float-left">Has client been counseled on diet? </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_HasClient", data.AnswerOrEmptyString("HasClient"), new { @class = "", @id = Model.Type + "_HasClient" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_IsClient" class="float-left">Is client taking any vitamins/minerals? </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_IsClient", data.AnswerOrEmptyString("IsClient"), new { @class = "", @id = Model.Type + "_IsClient" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_OralNutritionalSupplements" class="float-left">Oral nutritional supplements? </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_OralNutritionalSupplements", data.AnswerOrEmptyString("OralNutritionalSupplements"), new { @class = "", @id = Model.Type + "_OralNutritionalSupplements" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_TubeFeeding" class="float-left">Is client receiving a tube feeding? </label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_TubeFeeding", data.AnswerOrEmptyString("TubeFeeding"), new { @class = "", @id = Model.Type + "_TubeFeedingkj" })%>
<%  var feedingLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "Formula", Value = "Formula" },
                new SelectListItem { Text = "Rate", Value = "Rate" },
                new SelectListItem { Text = "Flushes", Value = "Flushes" },
                new SelectListItem { Text = "Route", Value = "Route" }
            }, "Value", "Text", data.AnswerOrDefault("FeedingType", ""));%>
<%= Html.DropDownList(Model.Type + "_FeedingType", feedingLevel, new { @id = Model.Type + "_FeedingType", @class = "" })%>
</div>

</td>
<td>
<label class="float-left">Diet history</label>
<div class="clear" />
<label for="<%= Model.Type %>_Breakfast" class="float-left">Breakfast:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Breakfast", data.AnswerOrEmptyString("Breakfast"), new { @class = "", @id = Model.Type + "_Breakfast" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Lunch" class="float-left">Lunch:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Lunch", data.AnswerOrEmptyString("Lunch"), new { @class = "", @id = Model.Type + "_Lunch" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Dinner" class="float-left">Dinner:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Dinner", data.AnswerOrEmptyString("Dinner"), new { @class = "", @id = Model.Type + "_Dinner" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Snacks" class="float-left">Snacks:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Snacks", data.AnswerOrEmptyString("Snacks"), new { @class = "", @id = Model.Type + "_Snacks" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_SkinCondition" class="float-left">Skin condition:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_SkinCondition", data.AnswerOrEmptyString("SkinCondition"), new { @class = "", @id = Model.Type + "_SkinCondition" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_Stasis" class="float-left">Stasis ulcer/location:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Stasis", data.AnswerOrEmptyString("Stasis"), new { @class = "", @id = Model.Type + "_Stasis" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Diabetic" class="float-left">Diabetic ulcer/location:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_Diabetic", data.AnswerOrEmptyString("Diabetic"), new { @class = "", @id = Model.Type + "_Diabetic" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_SkinTurgor" class="float-left">Skin Turgor:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_SkinTurgor", data.AnswerOrEmptyString("SkinTurgor"), new { @class = "", @id = Model.Type + "_SkinTurgor" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_Mucous" class="float-left">Mucous Membranes:</label>
<div class="float-right">
<%  var mucousLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "Moist", Value = "Moist" },
                new SelectListItem { Text = "Dry", Value = "Dry" }
            }, "Value", "Text", data.AnswerOrDefault("MucousMembranes", ""));%>
<%= Html.DropDownList(Model.Type + "_MucousMembranes", mucousLevel, new { @id = Model.Type + "_MucousMembranes", @class = "" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_PatientLabData" class="float-left">Patient Lab Data:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_PatientLabData", data.AnswerOrEmptyString("PatientLabData"), new { @class = "", @id = Model.Type + "_PatientLabData" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_PertinentMedications" class="float-left">Pertinent Medications:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_PertinentMedications", data.AnswerOrEmptyString("PertinentMedications"), new { @class = "", @id = Model.Type + "_PertinentMedications" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_EstimatedCaloricNeeds" class="float-left">Estimated Caloric Needs:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_EstimatedCaloricNeeds", data.AnswerOrEmptyString("EstimatedCaloricNeeds"), new { @class = "", @id = Model.Type + "_EstimatedCaloricNeeds" })%>
</div>
<div class="clear" />
<label for="<%= Model.Type %>_EstimatedProteinNeeds" class="float-left">Estimated Protein Needs:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_EstimatedProteinNeeds", data.AnswerOrEmptyString("EstimatedProteinNeeds"), new { @class = "", @id = Model.Type + "_EstimatedProteinNeeds" })%>
</div>
<div class="clear" />

<label for="<%= Model.Type %>_EstimatedFluidNeeds" class="float-left">Estimated Fluid Needs:</label>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_EstimatedFluidNeeds", data.AnswerOrEmptyString("EstimatedFluidNeeds"), new { @class = "", @id = Model.Type + "_EstimatedFluidNeeds" })%>
</div>
<div class="clear" />

<div class="float-left">Is current diet/TF adequate to meet caloric/protein/fluid needs?</div>
<div class="float-right">
<%= Html.RadioButton(Model.Type + "_IfAdequateToMeet", "1", data.AnswerOrEmptyString("IfAdequateToMeet").Equals("1"), new { @id = Model.Type + "_IfAdequateToMeet1", @class = "radio" })%>
<label for="<%= Model.Type %>_IfAdequateToMeet1" class="inline-radio">Yes</label>
<%= Html.RadioButton(Model.Type + "_IfAdequateToMeet", "0", data.AnswerOrEmptyString("IfAdequateToMeet").Equals("0"), new { @id = Model.Type + "_IfAdequateToMeet0", @class = "radio" })%>
<label for="<%= Model.Type %>_IfAdequateToMeet0" class="inline-radio">No</label>
</div>
<div class="clear" />
<div class="float-left">
<%  var formulaLevel = new SelectList(new[] {
                new SelectListItem { Text = "---", Value = "" },
                new SelectListItem { Text = "Volume", Value = "Volume" },
                new SelectListItem { Text = "Calories", Value = "Calories" },
                new SelectListItem { Text = "Protein", Value = "Protein" },
                new SelectListItem { Text = "Free water", Value = "Free water" },
                new SelectListItem { Text = "RDA", Value = "RDA" }
            }, "Value", "Text", data.AnswerOrDefault("FormulaType", ""));%>
<%= Html.DropDownList(Model.Type + "_FormulaType", formulaLevel, new { @id = Model.Type + "_FormulaType", @class = "" })%>
<label for="<%= Model.Type %>_TubeFeeding">provided by formula:</label>
</div>
<div class="float-right">
<%= Html.TextBox(Model.Type + "_ProvidedByFormula", data.AnswerOrEmptyString("ProvidedByFormula"), new { @class = "", @id = Model.Type + "_ProvidedByFormula" })%>
</div>            
            </td>
        </tr>
        <tr>
            <th>Assessment</th>
            <th>Plan</th>
        </tr>
        <tr>
            <td>
                <%= Html.Templates(Model.Type + "_AssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Assessment" })%>
                <%= Html.TextArea(Model.Type + "_Assessment", data.AnswerOrEmptyString("Assessment"),10,1, new { @class = "fill", @id = Model.Type + "_Assessment" })%>
            </td>
            <td>
                <%= Html.Templates(Model.Type + "_PlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Plan" })%>
                <%= Html.TextArea(Model.Type + "_Plan", data.AnswerOrEmptyString("Plan"),10,1, new { @class = "fill", @id = Model.Type + "_Plan" })%>
            </td>
        </tr>
    </tbody>
</table>

