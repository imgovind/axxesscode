﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] ffbs = data.AnswerArray("FFBS"); %>
<%  string[] bsTolerated = data.AnswerArray("BSTolerated"); %>
<%  string[] insulinTolerated = data.AnswerArray("InsulinTolerated"); %>
<%  string[] paDueTo = data.AnswerArray("PADueTo"); %>
<%  string[] supplies = data.AnswerArray("Supplies"); %>
<%  string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %>
<%  string[] isVitalSigns = data.ContainsKey("IsVitalSigns") && data["IsVitalSigns"].Answer != "" ? data["IsVitalSigns"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <input name="UAPInsulinPrepAdminVisit_IsVitalSignParameter" value=" " type="hidden" />
            <th>Vital Sign Parameters &#8212; <%= string.Format("<input class='radio' id='UAPInsulinPrepAdminVisit_IsVitalSignParameter' name='UAPInsulinPrepAdminVisit_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1") ? "checked='checked'" : "")%><label for="UAPInsulinPrepAdminVisit_IsVitalSignParameter">N/A</label></th>
            <input name="UAPInsulinPrepAdminVisit_IsVitalSigns" value=" " type="hidden" />
            <th>Vital Signs &#8212; <%= string.Format("<input class='radio' id='UAPInsulinPrepAdminVisit_IsVitalSigns' name='UAPInsulinPrepAdminVisit_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1") ? "checked='checked'" : "")%><label for="UAPInsulinPrepAdminVisit_IsVitalSigns">N/A</label></th>
        </tr>
        <tr>
            <td>
                <table id="UAPInsulinPrepAdminVisit_IsVitalSignParameterMore" class="fixed vitalsigns">
                    <tbody>
                        <tr>
                            <th></th>
                            <th>SBP</th>
                            <th>DBP</th>
                            <th>HR</th>
                            <th>Resp</th>
                            <th>Temp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <th>greater than (&#62;)</th>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_SystolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_DiastolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_PulseGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_RespirationGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_TempGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_WeightGreaterThan", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <th>less than (&#60;)</th>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_SystolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_DiastolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_PulseLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_RespirationLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_TempLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_WeightLessThan", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table id="UAPInsulinPrepAdminVisit_IsVitalSignsMore" class="fixed vitalsignparameter">
                    <tbody>
                        <tr>
                            <th>SBP</th>
                            <th>DBP</th>
                            <th>HR</th>
                            <th>Resp</th>
                            <th>Temp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignSBPVal", data.ContainsKey("VitalSignSBPVal") ? data["VitalSignSBPVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignSBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignDBPVal", data.ContainsKey("VitalSignDBPVal") ? data["VitalSignDBPVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignDBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignHRVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignRespVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignTempVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignWeightVal", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="align-left">
                                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FootCheck1' class='float-left radio' name='UAPInsulinPrepAdminVisit_FootCheck' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FootCheck").IsEqual("1") ? "checked=checked" : "") %>
                                    <label for="UAPInsulinPrepAdminVisit_FootCheck1" class="radio">Foot Check</label>
                                </div>
                            </td>
                            <td colspan="3">
                                <label for="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" class="float-left">Last BM Date:</label>
                                <div class="float-right"><input type="text" class="date-picker required" id="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" name="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" /></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Insulin Administration</th>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FFBS' class='float-left radio' name='UAPInsulinPrepAdminVisit_FFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FFBS").Equals("1").ToChecked())%>
                <label for="UAPInsulinPrepAdminVisit_FFBS" class="radio">FFBS</label>
                <table class="align-right" id="UAPInsulinPrepAdminVisit_FFBSMore">
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSDigit", data.AnswerOrEmptyString("FFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_FFBSDigit", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_FFBSDigit">digit</label>
                            </td>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSHandGlucometer", data.AnswerOrEmptyString("FFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_FFBSHandGlucometer", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_FFBSHandGlucometer">hand via glucometer</label>
                            </td>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSMgdl", data.AnswerOrEmptyString("FFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_FFBSMgdl", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_FFBSmgdl">mg/dl</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FFBSPressureApplied1' class='float-left radio' name='UAPInsulinPrepAdminVisit_FFBSPressureApplied' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FFBSPressureApplied").IsEqual("1") ? "checked=checked" : "")%>
                                <label for="UAPInsulinPrepAdminVisit_FFBSPressureApplied1" class="radio">pressure applied</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clear"></div>
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_NFFBS' class='float-left radio' name='UAPInsulinPrepAdminVisit_NFFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("NFFBS").Equals("1").ToChecked())%>
                <label for="UAPInsulinPrepAdminVisit_NFFBS" class="radio">NFFBS</label>
                <table class="align-right" id="UAPInsulinPrepAdminVisit_NFFBSMore">
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSDigit", data.AnswerOrEmptyString("NFFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSDigit", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_NFFBSDigit">digit</label>
                            </td>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", data.AnswerOrEmptyString("NFFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_NFFBSHandGlucometer">hand via glucometer</label>
                            </td>
                            <td>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSMgdl", data.AnswerOrEmptyString("NFFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSMgdl", @class = "vitals", @maxlength = "10" })%>
                                <label for="UAPInsulinPrepAdminVisit_NFFBSmgdl">mg/dl</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_NFFBSPressureApplied1' class='float-left radio' name='UAPInsulinPrepAdminVisit_NFFBSPressureApplied' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("NFFBSPressureApplied").IsEqual("1") ? "checked=checked" : "")%>
                                <label for="UAPInsulinPrepAdminVisit_NFFBSPressureApplied1" class="radio">pressure applied</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clear"></div>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <label class="float-left">Tolerated</label>
                                <div class="float-right">
                                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_BSTolerated1' class='radio' name='UAPInsulinPrepAdminVisit_BSTolerated' value='1' type='checkbox' {0} />", bsTolerated.Contains("1").ToChecked())%>
                                    <label for="UAPInsulinPrepAdminVisit_BSTolerated1" class="inlineradio">Well</label>
                                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_BSTolerated2' class='radio' name='UAPInsulinPrepAdminVisit_BSTolerated' value='2' type='checkbox' {0} />", bsTolerated.Contains("2").ToChecked())%>
                                    <label for="UAPInsulinPrepAdminVisit_BSTolerated2" class="inlineradio">Poor</label>
                                </div>
                            </td>
                            <td>
                                <label for="UAPInsulinPrepAdminVisit_LastMeal" class="float-left">Last Meal:</label>
                                <div class="float-right"><%= Html.TextBox("UAPInsulinPrepAdminVisit_LastMeal", data.AnswerOrEmptyString("LastMeal"), new { @id = "UAPInsulinPrepAdminVisit_LastMeal", @class = "", @maxlength = "" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTech1' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTech' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTech").IsEqual("1") ? "checked=checked" : string.Empty)%>
                <label for="UAPInsulinPrepAdminVisit_AsepticTech1" class="radio">Aseptic Tech.</label>
                <table id="UAPInsulinPrepAdminVisit_AsepticTech1More" class="align-right">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechType">Type</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechType", data.AnswerOrEmptyString("AsepticTechType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechType", @class = "", @maxlength = "30" })%>
                            </td>
                            <td colspan="2">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechAmount">Amount:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechAmount", data.AnswerOrEmptyString("AsepticTechAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechAmount", @class = "vitals", @maxlength = "5" })%>
                                units
                            </td>
                            <td colspan="2">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechRoute' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTechRoute' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechRoute").IsEqual("1") ? "checked=checked" : string.Empty)%>
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechRoute" class="radio">Subcut. Route</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSite">Site:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSite", data.AnswerOrEmptyString("AsepticTechSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSite", @class = "", @maxlength = "" })%>
                            </td>
                            <td colspan="3">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechTime">Time:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechTime", data.AnswerOrEmptyString("AsepticTechTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechTime", @class = "oe", @maxlength = "10" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clear"></div>
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechSlide1' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTechSlide' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechSlide").IsEqual("1") ? "checked=checked" : string.Empty)%>
                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlide1" class="radio">Aseptic Tech.</label>
                <table id="UAPInsulinPrepAdminVisit_AsepticTechSlide1More" class="align-right">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideType">Per Sliding Scale:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideType", data.AnswerOrEmptyString("AsepticTechSlideType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideType", @class = "", @maxlength = "30" })%>
                            </td>
                            <td colspan="2">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideAmount">Amount:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", data.AnswerOrEmptyString("AsepticTechSlideAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", @class = "vitals", @maxlength = "5" })%>
                                units
                            </td>
                            <td colspan="2">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechSlideRoute' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTechSlideRoute' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechSlideRoute").IsEqual("1") ? "checked=checked" : string.Empty)%>
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideRoute" class="radio">Subcut. Route</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideSite">Site:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideSite", data.AnswerOrEmptyString("AsepticTechSlideSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideSite", @class = "", @maxlength = "" })%>
                            </td>
                            <td colspan="3">
                                <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideTime">Time:</label>
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideTime", data.AnswerOrEmptyString("AsepticTechSlideTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideTime", @class = "oe", @maxlength = "10" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clear"></div>
                <div class="align-left strong">Tolerated:</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated1' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='1' type='checkbox' {0} />", insulinTolerated.Contains("1").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_InsulinTolerated1" class="radio">Well</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated2' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='2' type='checkbox' {0} />", insulinTolerated.Contains("2").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_InsulinTolerated2" class="radio">Poor</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated3' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='3' type='checkbox' {0} />", insulinTolerated.Contains("3").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_InsulinTolerated3" class="radio">Standard Precautions Maintained</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-left strong">P/A Due To:</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo1' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='1' type='checkbox' {0} />", paDueTo.Contains("1").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo1" class="radio">Poor Vision</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo2' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='2' type='checkbox' {0} />", paDueTo.Contains("2").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo2" class="radio">Poor Manual Dexterity</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo3' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='3' type='checkbox' {0} />", paDueTo.Contains("3").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo3" class="radio">Fear</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo4' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='4' type='checkbox' {0} />", paDueTo.Contains("4").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo4" class="radio">Cognition</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo5' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='5' type='checkbox' {0} />", paDueTo.Contains("5").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo5" class="radio">No Willing cg</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo6' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='6' type='checkbox' {0} />", paDueTo.Contains("6").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_PADueTo6" class="radio">Other</label>
                                <div class="float-right"><%= Html.TextBox("UAPInsulinPrepAdminVisit_PADueToOther", data.AnswerOrEmptyString("PADueToOther"), new { @id = "UAPInsulinPrepAdminVisit_PADueToOther" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>RN Notification</th>
            <th>MD Notification</th>
        </tr>
        <tr>
            <td>
                <table class="fixed">
                    <tr>
                        <td><%= string.Format("<input id='UAPInsulinPrepAdminVisit_ClinicianNotified' name='UAPInsulinPrepAdminVisit_ClinicianNotified' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("ClinicianNotified") && data["ClinicianNotified"] != null && data["ClinicianNotified"].Answer.IsNotNullOrEmpty() && data["ClinicianNotified"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%><label for="UAPInsulinPrepAdminVisit_ClinicianNotified" class="radio">RN Notified</label></td>
                        <td><div class="float-right"><%= Html.Clinicians("UAPInsulinPrepAdminVisit_ClinicianId", data.ContainsKey("ClinicianId") ? data["ClinicianId"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_ClinicianId", @class = "fill" })%></div></td>
                        <td><div class="float-right"><input type="text" class="date-picker" name="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" value="<%= data.ContainsKey("ClinicianNotifiedDate") && data["ClinicianNotifiedDate"] != null && data["ClinicianNotifiedDate"].Answer.IsNotNullOrEmpty() && data["ClinicianNotifiedDate"].Answer.IsValidDate() ? data["ClinicianNotifiedDate"].Answer : string.Empty %>" id="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" /></div></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                        <label for="UAPInsulinPrepAdminVisit_InstructionsGiven" class="strong">Instructions Given:</label>
                        <div class="align-center"><%= Html.TextArea("UAPInsulinPrepAdminVisit_InstructionsGiven", data.AnswerOrEmptyString("InstructionsGiven"), new { @class = "fill", @id = Model.Type + "UAPInsulinPrepAdminVisit_InstructionsGiven" })%></div>
                    </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="fixed">
                    <tr>
                        <td><%= string.Format("<input id='UAPInsulinPrepAdminVisit_DoctorNotified' name='UAPInsulinPrepAdminVisit_DoctorNotified' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("DoctorNotified") && data["DoctorNotified"] != null && data["DoctorNotified"].Answer.IsNotNullOrEmpty() && data["DoctorNotified"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%><label for="UAPInsulinPrepAdminVisit_DoctorNotified" class="radio">MD Notified</label></td>
                        <td><%= string.Format("<input id='UAPInsulinPrepAdminVisit_NewOrders' name='UAPInsulinPrepAdminVisit_NewOrders' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("NewOrders") && data["NewOrders"] != null && data["NewOrders"].Answer.IsNotNullOrEmpty() && data["NewOrders"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%><label for="UAPInsulinPrepAdminVisit_NewOrders" class="radio">New Order(s)</label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Supplies</th></tr>
        <tr>
            <td colspan="2">
                <table class="fixed">
                    <tr>
                        <td>
                            <div class="align-left">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies1' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='1' type='checkbox' {0} />", supplies.Contains("1").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_Supplies1" class="radio">NS Gloves</label>
                            </div>
                        </td>
                        <td>
                            <div class="align-left">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies2' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='2' type='checkbox' {0} />", supplies.Contains("2").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_Supplies2" class="radio">Alcohol Pads</label>
                            </div>
                        </td>
                        <td>
                            <div class="align-left">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies3' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='3' type='checkbox' {0} />", supplies.Contains("3").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_Supplies3" class="radio">Probe Covers</label>
                            </div>
                        </td>
                        <td>
                            <div class="align-left">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies4' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='4' type='checkbox' {0} />", supplies.Contains("4").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_Supplies4" class="radio">Insulin Syringe</label>
                            </div>
                        </td>
                        <td>
                            <div class="align-left">
                                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies5' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='5' type='checkbox' {0} />", supplies.Contains("5").ToChecked())%>
                                <label for="UAPInsulinPrepAdminVisit_Supplies5" class="radio">Other</label>
                            </div>
                            <div class="float-right">
                                <%= Html.TextBox("UAPInsulinPrepAdminVisit_SuppliesOther", data.AnswerOrEmptyString("SuppliesOther"), new { @id = "UAPInsulinPrepAdminVisit_SuppliesOther" })%>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Comments</th></tr>
        <tr><td colspan="2">
            <%= Html.Templates("UAPInsulinPrepAdminVisit_CommentTemplates", new { @class = "Templates", @template = "#UAPInsulinPrepAdminVisit_Comment" })%><br />
            <%= Html.TextArea("UAPInsulinPrepAdminVisit_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_Comment", @class = "fill" })%>
        </td></tr>
    </tbody>    
</table>