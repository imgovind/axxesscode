﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericMusculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
<%= Html.Hidden(Model.Type + "_GenericMusculoskeletal", string.Empty, new { @id = Model.Type + "_GenericMusculoskeletalHidden" })%>
<ul class="checkgroup bold">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal1' name='{0}_GenericMusculoskeletal' value='1' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal1">WNL (Within Normal Limits)</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal2' name='{0}_GenericMusculoskeletal' value='2' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal2">Grip Strength:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal2More">
            <%  var handGrips = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "Strong", Value = "1" },
                new SelectListItem { Text = "Weak", Value = "2" },
                new SelectListItem { Text = "Other", Value = "3" }
            }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalHandGrips", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalHandGrips", handGrips, new { @id = Model.Type + "_GenericMusculoskeletalHandGrips", @class = "oe" }) %>
            <ul class="checkgroup inline">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericMusculoskeletalHandGripsPosition0' name='{0}_GenericMusculoskeletalHandGripsPosition' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("0").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericMusculoskeletalHandGripsPosition0">Bilateral</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericMusculoskeletalHandGripsPosition1' name='{0}_GenericMusculoskeletalHandGripsPosition' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericMusculoskeletalHandGripsPosition1">Left</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericMusculoskeletalHandGripsPosition2' name='{0}_GenericMusculoskeletalHandGripsPosition' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericMusculoskeletalHandGripsPosition2">Right</label>
                    </div>
                </li>
            </ul>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal3' name='{0}_GenericMusculoskeletal' value='3' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal3">Impaired Motor Skill:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal3More">
            <%  var impairedMotorSkills = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "N/A", Value = "1" },
                new SelectListItem { Text = "Fine", Value = "2" },
                new SelectListItem { Text = "Gross", Value = "3" }
            }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalImpairedMotorSkills", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalImpairedMotorSkills", impairedMotorSkills, new { @id = Model.Type + "_GenericMusculoskeletalImpairedMotorSkills", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal4' name='{0}_GenericMusculoskeletal' value='4' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal4">Limited ROM:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal4More">
            <label for="<%= Model.Type %>_GenericLimitedROMLocation">Location:</label>
            <%= Html.TextBox(Model.Type + "_GenericLimitedROMLocation", data.AnswerOrEmptyString("GenericLimitedROMLocation"), new { @id = Model.Type + "_GenericLimitedROMLocation", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal5' name='{0}_GenericMusculoskeletal' value='5' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal5">Mobility:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal5More">
            <%  var mobility = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "WNL (Within Normal Limits)", Value = "1" },
                new SelectListItem { Text = "Ambulatory", Value = "2" },
                new SelectListItem { Text = "Ambulatory w/assistance", Value = "3" },
                new SelectListItem { Text = "Chair fast", Value = "4" },
                new SelectListItem { Text = "Bedfast", Value = "5" },
                new SelectListItem { Text = "Non-ambulatory", Value = "6" }
            }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalMobility", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalMobility", mobility, new { @id = Model.Type + "_GenericMusculoskeletalMobility", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal6' name='{0}_GenericMusculoskeletal' value='6' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal6">Type Assistive Device:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal6More">
            <%  string[] genericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
            <%= Html.Hidden(Model.Type + "_GenericAssistiveDevice", string.Empty, new { @id = Model.Type + "_GenericAssistiveDeviceHidden" })%>
            <ul class="checkgroup">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericAssistiveDevice1' name='{0}_GenericAssistiveDevice' value='1' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice1">Cane</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericAssistiveDevice2' name='{0}_GenericAssistiveDevice' value='2' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice2">Crutches</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericAssistiveDevice3' name='{0}_GenericAssistiveDevice' value='3' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice3">Walker</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericAssistiveDevice4' name='{0}_GenericAssistiveDevice' value='4' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice4">Wheelchair</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericAssistiveDevice5' name='{0}_GenericAssistiveDevice' value='5' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice5">Other</label>
                    </div>
                    <div class="extra" id="<%= Model.Type %>_GenericAssistiveDevice5More">
                        <%= Html.TextBox(Model.Type + "_GenericAssistiveDeviceOther", data.AnswerOrEmptyString("GenericAssistiveDeviceOther"), new { @id = Model.Type + "_GenericAssistiveDeviceOther", @class = "oe" }) %>
                    </div>
                </li>
            </ul>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal7' name='{0}_GenericMusculoskeletal' value='7' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal7">Contracture:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal7More">
            <label for="<%= Model.Type %>_GenericContractureLocation">Location:</label>
            <%= Html.TextBox(Model.Type + "_GenericContractureLocation", data.AnswerOrEmptyString("GenericContractureLocation"), new { @id = Model.Type + "_GenericContractureLocation", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal8' name='{0}_GenericMusculoskeletal' value='8' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("8").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal8">Weakness</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal9' name='{0}_GenericMusculoskeletal' value='9' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("9").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal9">Joint Pain:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal9More">
            <label for="<%= Model.Type %>_GenericJointPainLocation">Location:</label>
            <%= Html.TextBox(Model.Type + "_GenericJointPainLocation", data.AnswerOrEmptyString("GenericJointPainLocation"), new { @id = Model.Type + "_GenericJointPainLocation", @class = "oe" })%>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal10' name='{0}_GenericMusculoskeletal' value='10' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("10").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal10">Poor Balance</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal11' name='{0}_GenericMusculoskeletal' value='11' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("11").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal11">Joint Stiffness</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal12' name='{0}_GenericMusculoskeletal' value='12' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("12").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal12">Amputation:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal12More">
            <label for="<%= Model.Type %>_GenericAmputationLocation">Location:</label>
            <%= Html.TextBox(Model.Type + "_GenericAmputationLocation", data.AnswerOrEmptyString("GenericAmputationLocation"), new { @id = Model.Type + "_GenericAmputationLocation", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericMusculoskeletal13' name='{0}_GenericMusculoskeletal' value='13' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("13").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericMusculoskeletal13">Weight Bearing Restrictions:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericMusculoskeletal13More">
            <ul class="checkgroup inline">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericWeightBearingRestriction0' name='{0}_GenericWeightBearingRestriction' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("0").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericWeightBearingRestriction0">Full</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericWeightBearingRestriction1' name='{0}_GenericWeightBearingRestriction' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericWeightBearingRestriction1">Partial</label>
                    </div>
                </li>
            </ul>
            <div class="clear"></div>
            <label for="<%= Model.Type %>_GenericWeightBearingRestrictionLocation">Location:</label>
            <%= Html.TextBox(Model.Type + "_GenericWeightBearingRestrictionLocation", data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation"), new { @id = Model.Type + "_GenericWeightBearingRestrictionLocation", @class = "oe" }) %>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericMusculoskeletalComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericMusculoskeletalComment", data.AnswerOrEmptyString("GenericMusculoskeletalComment"), new { @class = "fill", @id = Model.Type + "_GenericMusculoskeletalComment" }) %></div>
<script type="text/javascript">
    U.ChangeToRadio("<%= Model.Type %>_GenericMusculoskeletalHandGripsPosition");
    U.ChangeToRadio("<%= Model.Type %>_GenericWeightBearingRestriction");
</script>
