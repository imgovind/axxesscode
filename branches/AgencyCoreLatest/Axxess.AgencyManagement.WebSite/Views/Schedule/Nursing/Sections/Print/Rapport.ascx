﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Patient with Family:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericRapportPatientFamily").ToString()%>")) +
printview.col(2,
    printview.span("Family with Patient:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericRapportFamilyPatient").ToString()%>")) +
printview.col(2,
    printview.span("Patient with RN:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericRapportPatientRN").ToString()%>")) +
printview.col(2,
    printview.span("Family with RN:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericRapportFamilyRN").ToString()%>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericRapportComment").Clean()%>"),
   "Rapport"