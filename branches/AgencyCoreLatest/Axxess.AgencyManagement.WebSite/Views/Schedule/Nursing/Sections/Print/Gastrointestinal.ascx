﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
<%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
<%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
<%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericDigestive.Contains("1").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Bowel Sounds",<%= genericDigestive.Contains("2").ToString().ToLower() %>) +
    <%if(genericDigestive.Contains("2")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("1") ? "Present/WNL x4 quadrants" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("2") ? "Hyperactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("3") ? "Hypoactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("4") ? "Absent" : string.Empty)%>") +
    <%}else{ %>
    printview.span("")+
    <%} %>
    printview.checkbox("Ab. Palpation",<%= genericDigestive.Contains("3").ToString().ToLower() %>) +
    <%if(genericDigestive.Contains("3")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("1") ? "Soft/WNL (Within Normal Limits)" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("2") ? "Firm" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("3") ? "Tender" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("4") ? "Other" : string.Empty)%>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Bowel Incontinence",<%= genericDigestive.Contains("4").ToString().ToLower() %>) +
printview.col(3,
    printview.checkbox("Nausea",<%= genericDigestive.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Vomiting",<%= genericDigestive.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("GERD",<%= genericDigestive.Contains("7").ToString().ToLower() %>)) +
printview.col(2,
    printview.checkbox("Abd Girth",<%= genericDigestive.Contains("8").ToString().ToLower() %>) +
    printview.span("<%= genericDigestive.Contains("8")?data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength").Clean():string.Empty %>")) +
printview.span("Elimination:",true) +
printview.checkbox("Last BM:",<%= genericDigestive.Contains("11").ToString().ToLower() %>) +
<%if(genericDigestive.Contains("11")){ %>
printview.col(2,
    printview.span("Date",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate").Clean() %>")) +
printview.checkbox("WNL (Within Normal Limits)",<%= genericDigestiveLastBM.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Abnormal Stool",<%= genericDigestiveLastBM.Contains("2").ToString().ToLower() %>) +

printview.col(2,
    printview.checkbox("Gray",<%= genericDigestiveLastBMAbnormalStool.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Tarry",<%= genericDigestiveLastBMAbnormalStool.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Black",<%= genericDigestiveLastBMAbnormalStool.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Fresh Blood",<%= genericDigestiveLastBMAbnormalStool.Contains("3").ToString().ToLower() %>)) +

printview.checkbox("Constipation",<%= genericDigestiveLastBM.Contains("3").ToString().ToLower() %>) +
printview.span("<%=genericDigestiveLastBM.Contains("3")? data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Clean():string.Empty %>")+
printview.checkbox("Diarrhea:",<%= genericDigestiveLastBM.Contains("4").ToString().ToLower() %>) +
printview.span("<%=genericDigestiveLastBM.Contains("4")? data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Clean():string.Empty %>")+
<%} %>
printview.span("Ostomy:",true) +
printview.col(2,
    printview.checkbox("Ostomy Type:",<%= genericDigestiveOstomy.Contains("1").ToString().ToLower() %>) +
    <%if(genericDigestiveOstomy.Contains("1")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("2") ? "Ileostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("3") ? "Colostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("4") ? "Other" : string.Empty) %>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Stoma Appearance<%=genericDigestiveOstomy.Contains("2")?":"+data.AnswerOrEmptyString("GenericDigestiveStomaAppearance").Clean():string.Empty %>",<%= genericDigestiveOstomy.Contains("2").ToString().ToLower() %>) +

printview.checkbox("Surrounding Skin<%=genericDigestiveOstomy.Contains("3")?":"+data.AnswerOrEmptyString("GenericDigestiveSurSkinType").Clean():string.Empty%>",<%= genericDigestiveOstomy.Contains("3").ToString().ToLower() %>) +

printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGastrointestinalComment").Clean() %>"),
"Gastrointestinal"