﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] pateintReceivedDischarge = data.AnswerArray("GenericPateintReceivedDischarge"); %>
printview.span("Discharge Plan Discussed:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("0").ToString().ToLower() %>) +
    printview.span("Discussed with:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDischargePlanningDiscussedWith").Clean() %>") +
    printview.span("Discharge Reason:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDischargeReasonForDischarge").Clean() %>")) +
printview.span("Physician Notified of Discharge:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("0").ToString().ToLower() %>)) +
printview.checkbox("Patient received discharge notice",<%= pateintReceivedDischarge.Contains("1").ToString().ToLower() %>) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericDischargeComment").Clean() %>"),
"Discharge Planning"