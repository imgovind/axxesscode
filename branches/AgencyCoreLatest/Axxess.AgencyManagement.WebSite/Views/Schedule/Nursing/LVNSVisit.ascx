﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">LVN/LPN Supervisory Visit | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "LVNSVisitForm" })) { %>
    <%= Html.Hidden("LVNSVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("LVNSVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("LVNSVisit_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", "LVNSVisit")%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    Licensed Vocational/Practical Nurse Supervisory Visit
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="LVNSVisit_HealthAide" class="float-left">LVN/LPN:</label>
                        <div class="float-right"><%= Html.LVNs("LVN", data.ContainsKey("LVN") ? data["LVN"].Answer : "", new { @id = "LVNSVisit_HealthAide" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div style="height:26px;">
                        <label for="LVNSVisit_LVNPresentY" class="float-left">LVN/LPN Present:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("LVNSVisit_LVNPresent", "1", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "1" ? true : false, new { @id = "LVNSVisit_LVNPresentY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_LVNPresentY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_LVNPresent", "0", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "0" ? true : false, new { @id = "LVNSVisit_LVNPresentN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_LVNPresentN">No</label>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="LVNSVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="LVNSVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="LVNSVisit_VisitDate" /></div>
                    </div>
                </td>
                <td colspan="2">
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes) && Model.DisciplineTask !=(int)DisciplineTasks.LVNSupervisoryVisit)
                        { %>
                        <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="LVNSVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("LVNSVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digits", @maxlength = 6, @id = "LVNSVisit_AssociatedMileage" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Evaluation</th>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">1.</span>
                        Arrives for assigned visits as scheduled:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_ArriveOnTimeY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_ArriveOnTimeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">2.</span>
                        Follows client&#8217;s plan of care:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "LVNSVisit_FollowPOCY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_FollowPOCY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "LVNSVisit_FollowPOCN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_FollowPOCN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">3.</span>
                        Demonstrates positive and helpful attitude towards the client and others:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_HasPositiveAttitudeY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_HasPositiveAttitudeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">4.</span>
                        Informs Nurse Supervisor/Case Manager of client needs and changes in condition:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_InformChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_InformChangesY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_InformChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_InformChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">5.</span>
                        Implements Universal Precautions per agency policy:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_IsUniversalPrecautionsY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_IsUniversalPrecautionsN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">6.</span>
                        Any changes made to client plan of care at this time:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_POCChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_POCChangesY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_POCChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_POCChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">7.</span>
                        Patient/CG satisfied with care and services provided by LVN/LPN:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryY", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_IsServicesSatisfactoryY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryN", @class = "radio" })%>
                        <label class="inline-radio" for="LVNSVisit_IsServicesSatisfactoryN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="float-left">
                        <span class="alphali">8.</span>
                        Additional Comments/Findings:
                    </div>
                    <div class="clear"></div>
                    <div class="float-left" style="width:99%"><%= Html.TextArea("LVNSVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @style = "height:80px;width:100%" }) %></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="LVNSVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password("LVNSVisit_Clinician", "", new { @class = "", @id = "LVNSVisit_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="LVNSVisit_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="LVNSVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="LVNSVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type) %> Return to Clinician for Signature</div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="LVNSVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="LVNSVisitAdd(); lvnSupVisit.Submit($(this));">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); UserInterface.CloseWindow('lvnsVisit');">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    function LVNSVisitAdd() {
        $("#LVNSVisit_Clinician").removeClass('required').addClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function LVNSVisitRemove() {
        $("#LVNSVisit_Clinician").removeClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required');
    }
</script>