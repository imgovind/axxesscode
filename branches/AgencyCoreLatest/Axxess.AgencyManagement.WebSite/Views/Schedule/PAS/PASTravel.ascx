﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle">PAS Travel Note | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "PASVisitForm" })) {%>
    <%= Html.Hidden("PASTravel_PatientId", Model.PatientId)%>
    <%= Html.Hidden("PASTravel_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("PASTravel_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("Type", "PASTravel")%>
    <%= Html.Hidden("DisciplineTask", "131")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                     Personal Assistance Services Travel Note 
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Travel Start Time:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TravelTimeIn",data.AnswerOrEmptyString("TravelTimeIn").IsNullOrEmpty()? data.AnswerOrEmptyString("TimeIn"):data.AnswerOrEmptyString("TravelTimeIn"), new { @id = Model.Type + "_TravelTimeIn", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Travel End Time:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TravelTimeOut",data.AnswerOrEmptyString("TravelTimeOut").IsNullOrEmpty()? data.AnswerOrEmptyString("TimeOut"):data.AnswerOrEmptyString("TravelTimeOut"), new { @id = Model.Type + "_TravelTimeOut", @class = "time-picker complete-required" })%></div>
                    </div>
                </td>
                <td>
                    <div class="clear"></div>
                     <div>
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">Comments</th>
            </tr>
            <tr>
                <td colspan="2"><%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "fill" })%></td>
            </tr>
        </tbody>    
    </table>
    
    <table class="fixed nursing">
        <tbody>       
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this))" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>