<% Html.Telerik().Grid<ScheduleEvent>().Name("ScheduleActivityGrid").Columns(columns =>
   {
       columns.Bound(s => s.EventId).Visible(false).Title("");
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task").Width(180);
       columns.Bound(s => s.EventDateSortable).ClientTemplate("<#=EventDateSortable#>").Title("Scheduled Date").Width(120);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(130);
       columns.Bound(s => s.StatusName).Title("Status").Width(100);
       columns.Bound(s => s.VisitVerifyUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=VisitVerifyUrl#>");
       columns.Bound(s => s.OasisProfileUrl).Title(" ").Width(22).ClientTemplate("<#=OasisProfileUrl#>").HtmlAttributes(new { @class = "centered-unpadded-cell" });
       columns.Bound(s => s.StatusComment).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\"><#=StatusComment#></a>");
       columns.Bound(s => s.Comments).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=CommentsCleaned#></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\"><#=EpisodeNotesCleaned#></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=PrintUrl#>");
       columns.Bound(s => s.AttachmentUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=AttachmentUrl#>");
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Schedule.ActivityRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline })).Sortable().Scrollable().Footer(false).Render();
   
%>
<script type="text/javascript">
    $('#ScheduleActivityGrid .t-grid-content').css({ 'height': 'auto' });
    if ($('#schedulecenter_showall').length) $('#schedulecenter_showall').remove();
    Schedule.SetEpisodeId('<%=Model.EpisodeId%>');
    Schedule.SetId('<%=Model.PatientId%>');
    
    $('#schedule-tab-strip').prepend(unescape("%3Cdiv id=%22schedulecenter_showall%22 class=%22abs buttons%22 style=%22left:650px;line-height:25px;%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22Schedule.showAll();%22%3EShow all%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
</script>