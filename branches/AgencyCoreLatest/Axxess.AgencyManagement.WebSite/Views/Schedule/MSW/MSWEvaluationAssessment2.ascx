﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituation = data.AnswerArray("GenericLivingSituation"); %>
<%  string[] genericReasonForReferral = data.AnswerArray("GenericReasonForReferral"); %>
<%  string[] genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<%  string[] genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<%  string[] genericIdentifiedProblems = data.AnswerArray("GenericIdentifiedProblems"); %>
<%  string[] genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<%  string[] genericGoals = data.AnswerArray("GenericGoals"); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<%  string sendAsOrder = data.ContainsKey("SendAsOrder") && data["SendAsOrder"].Answer.IsNotNullOrEmpty() ? data["SendAsOrder"].Answer : "0";%>

<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <table class="fixed nursing">
        <tbody>
           <tr>
                <th colspan="2">
                    <%= string.Format("{0}", Model.TypeName) %> | Version 2
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span>
                    <span class="float-right"><%= string.Format("<input id='{1}_SendAsOrder' class='radio' name='{1}_SendAsOrder' value='1' type='checkbox' {0} />", sendAsOrder.Contains("1").ToChecked(), Model.Type)%>Send as an order</span>
                </td>
                </tr>
            <tr>
                <td>
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                     <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="Text1" /></div>
                    </div>
                    <div class="clear"/>       
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @id = Model.Type + "_TimeIn", @class = "time-picker" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @id = Model.Type + "_TimeOut", @class = "time-picker" })%></div>
                    </div>
                </td>
                <td>         
                     <div>
                        <label for="<%= Model.Type %>_PhysicianDropDown" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : data.AnswerOrEmptyString("PhysicianId"), new { @id = Model.Type + "_PhysicianDropDown", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"/>
                    <div >
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"/>
                </td>
            </tr>
            <tr>
                <th>Living Situation</th>
                <th>Reason(s) for Referral (check all that apply)</th>
            </tr>
            <tr>
                <td>
                    <div class="padnoterow">
                        <div class="align-left strong">Patient Lives (check all that apply)</div>
                        <input type="hidden" name="<%= Model.Type %>_GenericLivingSituation" value="" />
                        <table class="align-left">
                            <tbody>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation1' class='float-left radio' name='{1}_GenericLivingSituation' value='1' type='checkbox' {0} />", genericLivingSituation.Contains("1").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation1" class="radio">Alone</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation2' class='float-left radio' name='{1}_GenericLivingSituation' value='2' type='checkbox' {0} />", genericLivingSituation.Contains("2").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation2" class="radio">With Friend/Family</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation3' class='float-left radio' name='{1}_GenericLivingSituation' value='3' type='checkbox' {0} />", genericLivingSituation.Contains("3").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation3" class="radio">With Dependent</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation4' class='float-left radio' name='{1}_GenericLivingSituation' value='4' type='checkbox' {0} />", genericLivingSituation.Contains("4").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation4" class="radio">With Spouse/Partner</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation5' class='float-left radio' name='{1}_GenericLivingSituation' value='5' type='checkbox' {0} />", genericLivingSituation.Contains("5").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation5" class="radio">With Religious Community</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation6' class='float-left radio' name='{1}_GenericLivingSituation' value='6' type='checkbox' {0} />", genericLivingSituation.Contains("6").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation6" class="radio">Assisted Living</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation7' class='float-left radio' name='{1}_GenericLivingSituation' value='7' type='checkbox' {0} />", genericLivingSituation.Contains("7").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation7" class="radio">With Partner</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation8' class='float-left radio' name='{1}_GenericLivingSituation' value='8' type='checkbox' {0} />", genericLivingSituation.Contains("8").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation8" class="radio">Has Paid Caregiver (&#62; 10 hours)</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation9' class='float-left radio' name='{1}_GenericLivingSituation' value='9' type='checkbox' {0} />", genericLivingSituation.Contains("9").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation9" class="radio">Has Live-In, Paid Caregiver</label>
                                    </td>
                                    <td>
                                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLivingSituationOther", data.AnswerOrEmptyString("GenericLivingSituationOther"), new { @id = Model.Type + "_GenericLivingSituationOther" })%></div>
                                        <%= string.Format("<input id='{1}_GenericLivingSituation10' class='float-left radio' name='{1}_GenericLivingSituation' value='10' type='checkbox' {0} />", genericLivingSituation.Contains("10").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericLivingSituation10" class="radio">Other:</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="padnoterow">
                        <label for="<%= Model.Type %>_GenericPrimaryCaregiver" class="float-left">Primary Caregiver</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPrimaryCaregiver", data.AnswerOrEmptyString("GenericPrimaryCaregiver"), new { @id = Model.Type + "_GenericPrimaryCaregiver" })%></div>
                    </div>
                    <div class="padnoterow">
                        <label for="<%= Model.Type %>_GenericQualityOfCare" for="<%= Model.Type %>_GenericIntensityOfPain" class="float-left">The quality of care that patient receives at home</label>
                        <div class="float-right">
                            <%  var genericQualityOfCare = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "" },
                                    new SelectListItem { Text = "Good", Value = "Good" },
                                    new SelectListItem { Text = "Adequate", Value = "Adequate" },
                                    new SelectListItem { Text = "Marginal", Value = "Marginal" },
                                    new SelectListItem { Text = "Inadequate", Value = "Inadequate" }
                                }, "Value", "Text", data.AnswerOrEmptyString("GenericQualityOfCare"));%>
                            <%= Html.DropDownList(Model.Type + "_GenericQualityOfCare", genericQualityOfCare, new { @id = Model.Type + "_GenericQualityOfCare", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="padnoterow">
                        <label for="<%= Model.Type %>_GenericEnvironmentalConditions" class="float-left">Environmental Conditions</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericEnvironmentalConditions", data.AnswerOrEmptyString("GenericEnvironmentalConditions"), new { @id = Model.Type + "_GenericEnvironmentalConditions" })%></div>
                    </div>
                    <div class="padnoterow">
                        <label for="<%= Model.Type %>_GenericIdentificationOfResponsibleParty" class="float-left">Identification of the responsible party</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericIdentificationOfResponsibleParty", data.AnswerOrEmptyString("GenericIdentificationOfResponsibleParty"), new { @id = Model.Type + "_GenericIdentificationOfResponsibleParty" })%></div>
                    </div>
                    <div class="padnoterow">
                        <label for="<%= Model.Type %>_GenericEmergencyContact" class="float-left">Emergency Contact:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericEmergencyContact", data.AnswerOrEmptyString("GenericEmergencyContact"), new { @id = Model.Type + "_GenericEmergencyContact" })%></div>
                    </div>
                </td>
                <td>
                    <div class="padnoterow">
                        <input type="hidden" name="<%= Model.Type %>_GenericReasonForReferral" value="" />
                        <table class="align-left">
                            <tbody>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral1' class='float-left radio' name='{1}_GenericReasonForReferral' value='1' type='checkbox' {0} />", genericReasonForReferral.Contains("1").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral1" class="radio">Assessment for Psychosocial Coping</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral2' class='float-left radio' name='{1}_GenericReasonForReferral' value='2' type='checkbox' {0} />", genericReasonForReferral.Contains("2").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral2" class="radio">Lives Alone, No Identified Caregiver</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral3' class='float-left radio' name='{1}_GenericReasonForReferral' value='3' type='checkbox' {0} />", genericReasonForReferral.Contains("3").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral3" class="radio">Counseling re Disease Process or Management</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral4' class='float-left radio' name='{1}_GenericReasonForReferral' value='4' type='checkbox' {0} />", genericReasonForReferral.Contains("4").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral4" class="radio">Solo Caregiver for Minor Children and/or Other Dependents</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral5' class='float-left radio' name='{1}_GenericReasonForReferral' value='5' type='checkbox' {0} />", genericReasonForReferral.Contains("5").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral5" class="radio">Family/Caregiver Coping Support</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral6' class='float-left radio' name='{1}_GenericReasonForReferral' value='6' type='checkbox' {0} />", genericReasonForReferral.Contains("6").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral6" class="radio">Reported Noncompliance to Medical Plan of Care</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral7' class='float-left radio' name='{1}_GenericReasonForReferral' value='7' type='checkbox' {0} />", genericReasonForReferral.Contains("7").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral7" class="radio">Hospice Eligibility</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral8' class='float-left radio' name='{1}_GenericReasonForReferral' value='8' type='checkbox' {0} />", genericReasonForReferral.Contains("8").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral8" class="radio">Suspected Negligence or Abuse</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral9' class='float-left radio' name='{1}_GenericReasonForReferral' value='9' type='checkbox' {0} />", genericReasonForReferral.Contains("9").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral9" class="radio">Financial/Practical Resources</label>
                                    </td>
                                    <td>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral10' class='float-left radio' name='{1}_GenericReasonForReferral' value='10' type='checkbox' {0} />", genericReasonForReferral.Contains("10").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral10" class="radio">Assistance with Advanced Directive / DPOA/DNR</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericReasonForReferralOther", data.AnswerOrEmptyString("GenericReasonForReferralOther"), new { @id = Model.Type + "_GenericReasonForReferralOther" })%></div>
                                        <%= string.Format("<input id='{1}_GenericReasonForReferral11' class='float-left radio' name='{1}_GenericReasonForReferral' value='11' type='checkbox' {0} />", genericReasonForReferral.Contains("11").ToChecked(), Model.Type)%>
                                        <label for="<%= Model.Type %>_GenericReasonForReferral11" class="radio">Other:</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Psychosocial Assessment</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="padnoterow">
                                        <input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
                                        <div class="align-left strong">Mental Status (check all that apply)</div>
                                        <table class="align-left">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus1' class='float-left radio' name='{1}_GenericMentalStatus' value='1' type='checkbox' {0} />", genericMentalStatus.Contains("1").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus1">Alert</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus2' class='float-left radio' name='{1}_GenericMentalStatus' value='2' type='checkbox' {0} />", genericMentalStatus.Contains("2").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus2">Forgetful</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus3' class='float-left radio' name='{1}_GenericMentalStatus' value='3' type='checkbox' {0} />", genericMentalStatus.Contains("3").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus3">Confused</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus4' class='float-left radio' name='{1}_GenericMentalStatus' value='4' type='checkbox' {0} />", genericMentalStatus.Contains("4").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus4">Disoriented</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus5' class='float-left radio' name='{1}_GenericMentalStatus' value='5' type='checkbox' {0} />", genericMentalStatus.Contains("5").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus5">Oriented</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus6' class='float-left radio' name='{1}_GenericMentalStatus' value='6' type='checkbox' {0} />", genericMentalStatus.Contains("6").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus6">Lethargic</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus7' class='float-left radio' name='{1}_GenericMentalStatus' value='7' type='checkbox' {0} />", genericMentalStatus.Contains("7").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus7">Poor Short Term Memory</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus8' class='float-left radio' name='{1}_GenericMentalStatus' value='8' type='checkbox' {0} />", genericMentalStatus.Contains("8").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus8">Unconscious</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus9' class='float-left radio' name='{1}_GenericMentalStatus' value='9' type='checkbox' {0} />", genericMentalStatus.Contains("9").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus9">Cannot Determine</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus10' class='float-left radio' name='{1}_GenericMentalStatus' value='10' type='checkbox' {0} />", genericMentalStatus.Contains("10").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus10">Reasoning</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericMentalStatus11' class='float-left radio' name='{1}_GenericMentalStatus' value='11' type='checkbox' {0} />", genericMentalStatus.Contains("11").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericMentalStatus11">Judgement</label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div class="padnoterow">
                                        <input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
                                        <div class="align-left strong">Emotional Status (check all that apply)</div>
                                        <table class="align-left">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus1' class='float-left radio' name='{1}_GenericEmotionalStatus' value='1' type='checkbox' {0} />", genericEmotionalStatus.Contains("1").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus1" class="radio">Stable</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus2' class='float-left radio' name='{1}_GenericEmotionalStatus' value='2' type='checkbox' {0} />", genericEmotionalStatus.Contains("2").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus2" class="radio">Tearful</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus3' class='float-left radio' name='{1}_GenericEmotionalStatus' value='3' type='checkbox' {0} />", genericEmotionalStatus.Contains("3").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus3" class="radio">Stressed</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus4' class='float-left radio' name='{1}_GenericEmotionalStatus' value='4' type='checkbox' {0} />", genericEmotionalStatus.Contains("4").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus4" class="radio">Angry</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus5' class='float-left radio' name='{1}_GenericEmotionalStatus' value='5' type='checkbox' {0} />", genericEmotionalStatus.Contains("5").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus5" class="radio">Sad</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus6' class='float-left radio' name='{1}_GenericEmotionalStatus' value='6' type='checkbox' {0} />", genericEmotionalStatus.Contains("6").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus6" class="radio">Withdrawn</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus7' class='float-left radio' name='{1}_GenericEmotionalStatus' value='7' type='checkbox' {0} />", genericEmotionalStatus.Contains("7").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus7" class="radio">Fearful</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus8' class='float-left radio' name='{1}_GenericEmotionalStatus' value='8' type='checkbox' {0} />", genericEmotionalStatus.Contains("8").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus8" class="radio">Anxious</label>
                                                    </td>
                                                    <td>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus9' class='float-left radio' name='{1}_GenericEmotionalStatus' value='9' type='checkbox' {0} />", genericEmotionalStatus.Contains("9").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus9" class="radio">Flat Affect</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericEmotionalStatusOther", data.AnswerOrEmptyString("GenericEmotionalStatusOther"), new { @id = Model.Type + "_GenericEmotionalStatusOther" })%></div>
                                                        <%= string.Format("<input id='{1}_GenericEmotionalStatus10' class='float-left radio float-left' name='{1}_GenericEmotionalStatus' value='10' type='checkbox' {0} />", genericEmotionalStatus.Contains("10").ToChecked(), Model.Type)%>
                                                        <label for="<%= Model.Type %>_GenericEmotionalStatus10" class="radio">Other:</label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%= Html.Templates(Model.Type + "_PsychosocialAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPsychosocialAssessment" })%>
                    <%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.AnswerOrEmptyString("GenericPsychosocialAssessment"), 4, 20, new { @id = Model.Type + "_GenericPsychosocialAssessment", @class = "fill" })%>
                    <div class="strong">The patient’s involvement with social and community activities</div>
                    <%= Html.Templates(Model.Type + "_PatientInvolvementTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_PatientInvolvement" })%>
                    <%= Html.TextArea(Model.Type + "_PatientInvolvement", data.AnswerOrEmptyString("PatientInvolvement"), 4, 20, new { @id = Model.Type + "_PatientInvolvement", @class = "fill" })%>
                </td>
            </tr>
            <tr>
                <th colspan="2">Financial Assessment</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <tbody>
                            <tr>
                                <td>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="align-left">Income Sources</th>
                                                <th>NA</th>
                                                <th>No</th>
                                                <th>Yes</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="align-left strong">Employment</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericEmployment", "2", data.AnswerOrEmptyString("GenericEmployment").Equals("2"), new { @id = Model.Type + "_GenericEmployment2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericEmployment", "0", data.AnswerOrEmptyString("GenericEmployment").Equals("0"), new { @id = Model.Type + "_GenericEmployment0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericEmployment", "1", data.AnswerOrEmptyString("GenericEmployment").Equals("1"), new { @id = Model.Type + "_GenericEmployment1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericEmploymentAmount", data.AnswerOrEmptyString("GenericEmploymentAmount"), new { @id = Model.Type + "_GenericEmploymentAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Pt Social Security</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "2", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("2"), new { @id = Model.Type + "_GenericPtSocialSecurity2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "0", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("0"), new { @id = Model.Type + "_GenericPtSocialSecurity0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "1", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("1"), new { @id = Model.Type + "_GenericPtSocialSecurity1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericPtSocialSecurityAmount", data.AnswerOrEmptyString("GenericPtSocialSecurityAmount"), new { @id = Model.Type + "_GenericPtSocialSecurityAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Spouse Social Security</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "2", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("2"), new { @id = Model.Type + "_GenericSpouseSocialSecurity2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "0", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("0"), new { @id = Model.Type + "_GenericSpouseSocialSecurity0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "1", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("1"), new { @id = Model.Type + "_GenericSpouseSocialSecurity1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericSpouseSocialSecurityAmount", data.AnswerOrEmptyString("GenericSpouseSocialSecurityAmount"), new { @id = Model.Type + "_GenericSpouseSocialSecurityAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Pt SSI</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSSI", "2", data.AnswerOrEmptyString("GenericPtSSI").Equals("2"), new { @id = Model.Type + "_GenericPtSSI2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSSI", "0", data.AnswerOrEmptyString("GenericPtSSI").Equals("0"), new { @id = Model.Type + "_GenericPtSSI0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPtSSI", "1", data.AnswerOrEmptyString("GenericPtSSI").Equals("1"), new { @id = Model.Type + "_GenericPtSSI1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericPtSSIAmount", data.AnswerOrEmptyString("GenericPtSSIAmount"), new { @id = Model.Type + "_GenericPtSSIAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Spouse SSI</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "2", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("2"), new { @id = Model.Type + "_GenericSpouseSSI2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "0", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("0"), new { @id = Model.Type + "_GenericSpouseSSI0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "1", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("1"), new { @id = Model.Type + "_GenericSpouseSSI1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericSpouseSSIAmount", data.AnswerOrEmptyString("GenericSpouseSSIAmount"), new { @id = Model.Type + "_GenericSpouseSSIAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Pensions</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPensions", "2", data.AnswerOrEmptyString("GenericPensions").Equals("2"), new { @id = Model.Type + "_GenericPensions2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPensions", "0", data.AnswerOrEmptyString("GenericPensions").Equals("0"), new { @id = Model.Type + "_GenericPensions0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericPensions", "1", data.AnswerOrEmptyString("GenericPensions").Equals("1"), new { @id = Model.Type + "_GenericPensions1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericPensionsAmount", data.AnswerOrEmptyString("GenericPensionsAmount"), new { @id = Model.Type + "_GenericPensionsAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Other Income</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "2", data.AnswerOrEmptyString("GenericOtherIncome").Equals("2"), new { @id = Model.Type + "_GenericOtherIncome2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "0", data.AnswerOrEmptyString("GenericOtherIncome").Equals("0"), new { @id = Model.Type + "_GenericOtherIncome0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "1", data.AnswerOrEmptyString("GenericOtherIncome").Equals("1"), new { @id = Model.Type + "_GenericOtherIncome1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericOtherIncomeAmount", data.AnswerOrEmptyString("GenericOtherIncomeAmount"), new { @id = Model.Type + "_GenericOtherIncomeAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Food Stamps</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "2", data.AnswerOrEmptyString("GenericFoodStamps").Equals("2"), new { @id = Model.Type + "_GenericFoodStamps2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "0", data.AnswerOrEmptyString("GenericFoodStamps").Equals("0"), new { @id = Model.Type + "_GenericFoodStamps0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "1", data.AnswerOrEmptyString("GenericFoodStamps").Equals("1"), new { @id = Model.Type + "_GenericFoodStamps1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericFoodStampsAmount", data.AnswerOrEmptyString("GenericFoodStampsAmount"), new { @id = Model.Type + "_GenericFoodStampsAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="align-right">Total Income</td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericTotalIncomeAmount", data.AnswerOrEmptyString("GenericTotalIncomeAmount"), new { @id = Model.Type + "_GenericTotalIncomeAmount", @class = "sn" })%></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="vertical-align:top;">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="align-left">Assets</th>
                                                <th>NA</th>
                                                <th>No</th>
                                                <th>Yes</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="align-left strong">Savings Account</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "2", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("2"), new { @id = Model.Type + "_GenericSavingsAccount2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "0", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("0"), new { @id = Model.Type + "_GenericSavingsAccount0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "1", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("1"), new { @id = Model.Type + "_GenericSavingsAccount1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericSavingsAccountAmount", data.AnswerOrEmptyString("GenericSavingsAccountAmount"), new { @id = Model.Type + "_GenericSavingsAccountAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Owns Home (value)</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "2", data.AnswerOrEmptyString("GenericOwnsHome").Equals("2"), new { @id = Model.Type + "_GenericOwnsHome2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "0", data.AnswerOrEmptyString("GenericOwnsHome").Equals("0"), new { @id = Model.Type + "_GenericOwnsHome0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "1", data.AnswerOrEmptyString("GenericOwnsHome").Equals("1"), new { @id = Model.Type + "_GenericOwnsHome1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericOwnsHomeAmount", data.AnswerOrEmptyString("GenericOwnsHomeAmount"), new { @id = Model.Type + "_GenericOwnsHomeAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Owns Other Property (value)</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "2", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("2"), new { @id = Model.Type + "_GenericOwnsOtherProperty2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "0", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("0"), new { @id = Model.Type + "_GenericOwnsOtherProperty0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "1", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("1"), new { @id = Model.Type + "_GenericOwnsOtherProperty1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericOwnsOtherPropertyAmount", data.AnswerOrEmptyString("GenericOwnsOtherPropertyAmount"), new { @id = Model.Type + "_GenericOwnsOtherPropertyAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">VA Aid &#38; Assistance</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericVAAid", "2", data.AnswerOrEmptyString("GenericVAAid").Equals("2"), new { @id = Model.Type + "_GenericVAAid2", @class = "radio" }) %></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericVAAid", "0", data.AnswerOrEmptyString("GenericVAAid").Equals("0"), new { @id = Model.Type + "_GenericVAAid0", @class = "radio" }) %></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericVAAid", "1", data.AnswerOrEmptyString("GenericVAAid").Equals("1"), new { @id = Model.Type + "_GenericVAAid1", @class = "radio" }) %></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericVAAidAmount", data.AnswerOrEmptyString("GenericVAAidAmount"), new { @id = Model.Type + "_GenericVAAidAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Spouse SSI</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "2", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("2"), new { @id = Model.Type + "_GenericSpouseAssetSSI2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "0", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("0"), new { @id = Model.Type + "_GenericSpouseAssetSSI0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "1", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("1"), new { @id = Model.Type + "_GenericSpouseAssetSSI1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericSpouseAssetSSIAmount", data.AnswerOrEmptyString("GenericSpouseAssetSSIAmount"), new { @id = Model.Type + "_GenericSpouseAssetSSIAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td class="align-left strong">Other Assets</td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "2", data.AnswerOrEmptyString("GenericOtherAssets").Equals("2"), new { @id = Model.Type + "_GenericOtherAssets2", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "0", data.AnswerOrEmptyString("GenericOtherAssets").Equals("0"), new { @id = Model.Type + "_GenericOtherAssets0", @class = "radio" })%></td>
                                                <td><%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "1", data.AnswerOrEmptyString("GenericOtherAssets").Equals("1"), new { @id = Model.Type + "_GenericOtherAssets1", @class = "radio" })%></td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericOtherAssetsAmount", data.AnswerOrEmptyString("GenericOtherAssetsAmount"), new { @id = Model.Type + "_GenericOtherAssetsAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="align-right">Total Assets</td>
                                                <td><%= Html.TextBox(Model.Type + "_GenericTotalAssetsAmount", data.AnswerOrEmptyString("GenericTotalAssetsAmount"), new { @id = Model.Type + "_GenericTotalAssetsAmount", @class = "sn" })%></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <div class="padnoterow">
                                                        <label class="float-left">Transportation for medical care provided by</label>
                                                        <%= Html.TextBox(Model.Type + "_GenericTransportationProvidedBy", data.AnswerOrEmptyString("GenericTransportationProvidedBy"), new { @id = Model.Type + "_GenericTransportationProvidedBy" })%>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="align-left strong">Identified Problems (check all that apply)</div>
                    <input type="hidden" name="<%= Model.Type %>_GenericIdentifiedProblems" value="" />
                    <table class="align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems1' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='1' type='checkbox' {0} />", genericIdentifiedProblems.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems1" class="radio">Patient needs a meal prepared or delivered daily</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems2' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='2' type='checkbox' {0} />", genericIdentifiedProblems.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems2" class="radio">Patient/family reported noncompliant to medical plan of care</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems3' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='3' type='checkbox' {0} />", genericIdentifiedProblems.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems3" class="radio">Patient needs assistance with housekeeping/shopping</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems4' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='4' type='checkbox' {0} />", genericIdentifiedProblems.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems4" class="radio">Patient needs assistance with advanced directive/DPOA/DNR</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems5' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='5' type='checkbox' {0} />", genericIdentifiedProblems.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems5" class="radio">Patient needs daily contact to check on him/her</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems6' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='6' type='checkbox' {0} />", genericIdentifiedProblems.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems6" class="radio">Patient needs assistance with medical/insurance forms</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems7' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='7' type='checkbox' {0} />", genericIdentifiedProblems.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems7" class="radio">Patient needs assistance with alert device (ERS, PRS)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems8' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='8' type='checkbox' {0} />", genericIdentifiedProblems.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems8" class="radio">Patient needs assistance with entitlement forms</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems9' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='9' type='checkbox' {0} />", genericIdentifiedProblems.Contains("9").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems9" class="radio">Patient needs transportation assistance to medical care</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems10' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='10' type='checkbox' {0} />", genericIdentifiedProblems.Contains("10").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems10" class="radio">Patient needs alternative living arrangements</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems11' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='11' type='checkbox' {0} />", genericIdentifiedProblems.Contains("11").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems11" class="radio">Patient needs alternative living arrangements</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericIdentifiedProblems12' class='float-left radio' name='{1}_GenericIdentifiedProblems' value='12' type='checkbox' {0} />", genericIdentifiedProblems.Contains("12").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericIdentifiedProblems12" class="radio">Psychosocial counseling indicated</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="align-left strong">Provide further information</div>
                    <%= Html.Templates(Model.Type + "_FurtherInformationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericFurtherInformation" })%>
                    <%= Html.TextArea(Model.Type + "_GenericFurtherInformation", data.AnswerOrEmptyString("GenericFurtherInformation"), 4, 20, new { @id = Model.Type + "_GenericFurtherInformation", @class = "fill" })%>
                    <div class="align-left strong">Identified Strengths and Supports</div>
                    <%= Html.Templates(Model.Type + "_IdentifiedStrengthsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericIdentifiedStrengths" })%>
                    <%= Html.TextArea(Model.Type + "_GenericIdentifiedStrengths", data.AnswerOrEmptyString("GenericIdentifiedStrengths"), 4, 20, new { @id = Model.Type + "_GenericIdentifiedStrengths", @class = "fill" })%>
                    <div class="align-left strong">Planned Interventions (check all that apply)</div>
                    <input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
                    <table class="align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions1' class='float-left radio' name='{1}_GenericPlannedInterventions' value='1' type='checkbox' {0} />", genericPlannedInterventions.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions1" class="radio">Psychosocial Assessment</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions2' class='float-left radio' name='{1}_GenericPlannedInterventions' value='2' type='checkbox' {0} />", genericPlannedInterventions.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions2" class="radio">Develop Appropriate Support System</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions3' class='float-left radio' name='{1}_GenericPlannedInterventions' value='3' type='checkbox' {0} />", genericPlannedInterventions.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions3" class="radio">Counseling re Disease Process &#38; Management</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions4' class='float-left radio' name='{1}_GenericPlannedInterventions' value='4' type='checkbox' {0} />", genericPlannedInterventions.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions4" class="radio">Community Resource Planning &#38; Outreach</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions5' class='float-left radio' name='{1}_GenericPlannedInterventions' value='5' type='checkbox' {0} />", genericPlannedInterventions.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions5" class="radio">Counseling re Family Coping</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions6' class='float-left radio' name='{1}_GenericPlannedInterventions' value='6' type='checkbox' {0} />", genericPlannedInterventions.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions6" class="radio">Stabilize Current Placement</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions7' class='float-left radio' name='{1}_GenericPlannedInterventions' value='7' type='checkbox' {0} />", genericPlannedInterventions.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions7" class="radio">Crisis Intervention</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions8' class='float-left radio' name='{1}_GenericPlannedInterventions' value='8' type='checkbox' {0} />", genericPlannedInterventions.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions8" class="radio">Determine/Locate Alternative Placement</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions9' class='float-left radio' name='{1}_GenericPlannedInterventions' value='9' type='checkbox' {0} />", genericPlannedInterventions.Contains("9").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions9" class="radio">Long-range Planning &#38; Decision Making</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions10' class='float-left radio' name='{1}_GenericPlannedInterventions' value='10' type='checkbox' {0} />", genericPlannedInterventions.Contains("10").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions10" class="radio">Financial Counseling and/or Referrals</label>
                                </td>
                                <td colspan="2">
                                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPlannedInterventionsOther", data.AnswerOrEmptyString("GenericPlannedInterventionsOther"), new { @id = Model.Type + "_GenericPlannedInterventionsOther" })%></div>
                                    <%= string.Format("<input id='{1}_GenericPlannedInterventions11' class='float-left radio' name='{1}_GenericPlannedInterventions' value='11' type='checkbox' {0} />", genericPlannedInterventions.Contains("11").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlannedInterventions11" class="radio">Other (enter details below)</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="align-left strong">Intervention Details</div>
                    <%= Html.Templates(Model.Type + "_InterventionDetailsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericInterventionDetails" })%>
                    <%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.AnswerOrEmptyString("GenericInterventionDetails"), 4, 20, new { @id = Model.Type + "_GenericInterventionDetails", @class = "fill" })%>
                    <div class="align-left strong">Plan of Care</div>
                    <%= Html.Templates(Model.Type + "_PlanOfCareTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPlanOfCare" })%>
                    <%= Html.TextArea(Model.Type + "_GenericPlanOfCare", data.AnswerOrEmptyString("GenericPlanOfCare"), 4, 20, new { @id = Model.Type + "_GenericPlanOfCare", @class = "fill" })%>
                </td>
            </tr>
            <tr>
                <th colspan="2">Goals</th>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="<%= Model.Type %>_GenericGoals" value="" />
                    <table class="align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals1' class='float-left radio' name='{1}_GenericGoals' value='1' type='checkbox' {0} />", genericGoals.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals1" class="radio">Adequate Support System</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals2' class='float-left radio' name='{1}_GenericGoals' value='2' type='checkbox' {0} />", genericGoals.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals2" class="radio">Improved Client/Family Coping</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals3' class='float-left radio' name='{1}_GenericGoals' value='3' type='checkbox' {0} />", genericGoals.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals3" class="radio">Normal Grieving Process</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals4' class='float-left radio' name='{1}_GenericGoals' value='4' type='checkbox' {0} />", genericGoals.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals4" class="radio">Appropriate Goals for Care Set by Client/Family</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals5' class='float-left radio' name='{1}_GenericGoals' value='5' type='checkbox' {0} />", genericGoals.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals5" class="radio">Appropriate Community Resource Referrals</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals6' class='float-left radio' name='{1}_GenericGoals' value='6' type='checkbox' {0} />", genericGoals.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals6" class="radio">Stable Placement Setting</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericGoals7' class='float-left radio' name='{1}_GenericGoals' value='7' type='checkbox' {0} />", genericGoals.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals7" class="radio">Mobilization of Financial Resources</label>
                                </td>
                                <td colspan="2">
                                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericGoalsOther", data.AnswerOrEmptyString("GenericGoalsOther"), new { @id = Model.Type + "_GenericGoalsOther" })%></div>
                                    <%= string.Format("<input id='{1}_GenericGoals8' class='float-left radio' name='{1}_GenericGoals' value='8' type='checkbox' {0} />", genericGoals.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericGoals8" class="radio">Other (enter details below)</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Frequency:<%= Html.TextBox(Model.Type + "_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @class = "", @id = Model.Type + "_GenericFrequency", @maxlength = "50" })%>
            Duration:<%= Html.TextBox(Model.Type + "_GenericDuration", data.AnswerOrEmptyString("GenericDuration"), new { @class = "", @id = Model.Type + "_GenericDuration", @maxlength = "50" })%> </td>
        
                            </tr>
                        </tbody>
                    </table>
                    <div class="align-left strong">Visit Goal Details</div>
                    <%= Html.Templates(Model.Type + "_VisitGoalDetailsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericVisitGoalDetails" })%>
                    <%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.AnswerOrEmptyString("GenericVisitGoalDetails"), 4, 20, new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "fill" })%>
                </td>
            </tr>
            <tr>
                <th colspan="2">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="2">
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.MSWEvaluationAssessment.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>

<script type="text/javascript">
    $(".Physicians").PhysicianInput();
</script>