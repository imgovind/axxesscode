﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>%3Cbr /%3E<%= location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "Phone: " +location.PhoneWorkFormatted.Clean().ToTitleCase() : ""%><%= location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " +location.FaxNumberFormatted.Clean().ToTitleCase() : ""%>" +
        
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Discharge%3C/th%3E%3C/tr%3E" +
        "%3C/tr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%><%= Model.Patient != null ? " (" + Model.Patient.PatientIdNumber +")" : "" %>" +      
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("PTDischargeDOB") ? data["PTDischargeDOB"].Answer.Clean() : string.Empty %>" +  
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +   
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>%3Cbr /%3E<%= location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "Phone: " +location.PhoneWorkFormatted.Clean().ToTitleCase() : ""%><%= location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " +location.FaxNumberFormatted.Clean().ToTitleCase() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Discharge%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("Reached Max Potential",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("No Longer Homebound",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Per Patient/Family Request",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Prolonged On-Hold Status",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Prolonged On-Hold Status",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Hospitalized",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Expired",<%= data.AnswerOrEmptyString("GenericReasonForDischarge").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E <%= data.AnswerOrEmptyString("GenericReasonForDischargeOther").Clean() %>",0,1)),
        "Reason for Discharge");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Narrative/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>
