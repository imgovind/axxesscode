﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">Bed Mobility</th>
            <th colspan="6">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev1.ascx", Model); %></td>
            <td colspan="6" rowspan="8"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Transfer</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Gait</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">W/C Mobility</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="5">Reason for Discharge</th>
            <th colspan="5">Treatment Plan</th>
        </tr>
        <tr>
            <td colspan="5">
                <%  string[] genericReasonForDischarge = data.AnswerArray("GenericReasonForDischarge"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericReasonForDischarge" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", genericReasonForDischarge.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", genericReasonForDischarge.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge2">No Longer Homebound</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", genericReasonForDischarge.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per Patient/Family Request</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", genericReasonForDischarge.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge4">Prolonged On-Hold Status</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", genericReasonForDischarge.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge5">Prolonged On-Hold Status</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", genericReasonForDischarge.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge6">Hospitalized</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", genericReasonForDischarge.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Expired</label>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericReasonForDischargeOther" class="float-left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.AnswerOrEmptyString("GenericReasonForDischargeOther"), new { @class = "", @id = Model.Type+"_GenericReasonForDischargeOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <td colspan="10">
                <div class="third">
                    <label for="<%= Model.Type %>_GenericFrequency" class="float-left">Frequency:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @id = Model.Type+"_GenericFrequency" })%></div>
                </div>
                <div class="third"></div>
                <div class="third">
                    <label for="<%= Model.Type %>_GenericDuration" class="float-left">Duration:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericDuration", data.AnswerOrEmptyString("GenericDuration"), new { @id = Model.Type+"_GenericDuration" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Narrative</th>
        </tr>
        <tr>
            <td colspan="10"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Narrative/FormRev1.ascx", Model); %></td>
         </tr>
    </tbody>
</table>