﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">
                Vital Signs
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </td>
        </tr>
        
        
        <tr>
            <th colspan="4">
                Functional Mobility Key
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                ADLs/Functional Mobility Level/Level of Assist
            </th>
            <th colspan="2">
                Physical Assessment
            </th>
        </tr>
        <tr>
            <td colspan="2"  rowspan="3">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev4.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                W/C Mobility
                
            </th>
            
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %>
                
            </td>
            
        </tr>
        <tr>
            <th colspan="4">Sensory/Perceptual Skills</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Cognitive Status/Comprehension</th>
            <th colspan="2">Motor Components (Enter Appropriate Response)</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Standardized test</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTStandardizedTest/FormRev1.ascx", Model); %>
                </td>
        </tr>
        <tr>
            <th colspan="2">
                Pain Assessment
            </th>
            <th colspan="2">
                Skilled Care Provided This Visit
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericSkilledCareTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSkilledCareProvided" })%>
                <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.ContainsKey("GenericSkilledCareProvided") ? data["GenericSkilledCareProvided"].Answer : string.Empty, 15, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="2">Reason for discharge</th>
            <th colspan="2">Condition of patient at time of discharge</th>
        </tr>
        <tr>
            <td colspan="2">
                <%  string[] genericReasonForDischarge = data.AnswerArray("GenericReasonForDischarge"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericReasonForDischarge" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", genericReasonForDischarge.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", genericReasonForDischarge.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge2">No Longer Homebound</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", genericReasonForDischarge.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per Patient/Family Request</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", genericReasonForDischarge.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge4">Prolonged On-Hold Status</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", genericReasonForDischarge.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge5">Goals Met</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", genericReasonForDischarge.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge6">Hospitalized</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", genericReasonForDischarge.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Expired</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge8' class='radio' name='{1}_GenericReasonForDischarge' value='8' type='checkbox' {0} />", genericReasonForDischarge.Contains("8").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Moved Out Of Service Area</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericReasonForDischargeOther" class="float-left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.AnswerOrEmptyString("GenericReasonForDischargeOther"), new { @class = "", @id = Model.Type+"_GenericReasonForDischargeOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DischargeCondition/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Summary of care provided</th>
            <th colspan="2">Summary of progress made</th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericSummaryOfCareTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSummaryOfCare" })%>
                <%= Html.TextArea(Model.Type + "_GenericSummaryOfCare", data.AnswerOrEmptyString("GenericSummaryOfCare"), 8, 20, new { @id = Model.Type + "_GenericSummaryOfCare", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericSummaryOfProgressTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSummaryOfProgress" })%>
                <%= Html.TextArea(Model.Type + "_GenericSummaryOfProgress", data.AnswerOrEmptyString("GenericSummaryOfProgress"), 8, 20, new { @id = Model.Type + "_GenericSummaryOfProgress", @class = "fill" })%>
            </td>
         </tr>
    </tbody>
</table>
