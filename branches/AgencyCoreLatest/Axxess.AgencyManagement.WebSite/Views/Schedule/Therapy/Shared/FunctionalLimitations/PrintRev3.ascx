﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Upper body Rom/strength deficit.",<%= genericFunctionalLimitations.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Pain affecting function.",<%= genericFunctionalLimitations.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Impaired safety.",<%= genericFunctionalLimitations.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Difficulty with dressing/grooming/bathing/hygiene/toileting.",<%= genericFunctionalLimitations.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Difficulty with homemaking skills/money management/meal prep/laundry.",<%= genericFunctionalLimitations.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Impaired problem solving skills/attention/concentration/sequencing.",<%= genericFunctionalLimitations.Contains("6").ToString().ToLower() %>)+
            printview.checkbox("Impaired coordination.",<%= genericFunctionalLimitations.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Visual deficit/disturbance/limitation.",<%= genericFunctionalLimitations.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Cognition (memory, safety awareness, judgement).",<%= genericFunctionalLimitations.Contains("9").ToString().ToLower() %>)),
        "Functional Limitations/Problem Areas");
</script>  