﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<% if(data.ContainsKey("IsLimitationsApply") && data.AnswerOrEmptyString("IsLimitationsApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Functional Limitations");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("ROM/Strength.",<%= genericFunctionalLimitations.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Pain.",<%= genericFunctionalLimitations.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Safety Techniques.",<%= genericFunctionalLimitations.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("W/C Mobility.",<%= genericFunctionalLimitations.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Balance/Gait.",<%= genericFunctionalLimitations.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility.",<%= genericFunctionalLimitations.Contains("6").ToString().ToLower() %>)+
            printview.checkbox("Transfer.",<%= genericFunctionalLimitations.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Increased fall risk.",<%= genericFunctionalLimitations.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Coordination.",<%= genericFunctionalLimitations.Contains("9").ToString().ToLower() %>)),
        "Functional Limitations");
</script>  

<%} %>