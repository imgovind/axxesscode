﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var type=Model!=null && Model.Type!=null ? Model.Type: string.Empty; %>
<%  string[] genericPTGoals = data.AnswerArray("GenericPTGoals"); %>
<% if(data.ContainsKey("IsGoalsApply") && data.AnswerOrEmptyString("IsGoalsApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "PT Goals");
</script>    
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
    <%if(genericPTGoals.Contains("1")){ %>
        printview.checkbox("Patient will demonstrate ability to perform home exercise program within <%= data.AnswerOrEmptyString("GenericPTGoals1Weeks").Clean() %> weeks",<%= genericPTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("2")){ %>
        printview.checkbox("Demonstrate effective pain management utilizing <%= data.AnswerOrEmptyString("GenericPTGoals2Utilize").Clean() %> within <%= data.AnswerOrEmptyString("GenericPTGoals2Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform sit to supine/supine to sit with <%= data.AnswerOrEmptyString("GenericPTGoals3Assist").Clean() %> assist with safe and effective technique within <%= data.AnswerOrEmptyString("GenericPTGoals3Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("4")){ %>
        printview.checkbox("Improve bed mobility to independent within <%= data.AnswerOrEmptyString("GenericPTGoals4Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("5")){ %>
        printview.checkbox("Improve transfers to <%= data.AnswerOrEmptyString("GenericPTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("GenericPTGoals5Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericPTGoals5Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("6")){ %>
        printview.checkbox("Independent with safe transfer skills within <%= data.AnswerOrEmptyString("GenericPTGoals6Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("7")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("GenericPTGoals7Skill").Clean() %> transfer skill to <%= data.AnswerOrEmptyString("GenericPTGoals7Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("GenericPTGoals7Within").Clean() %> device within <%= data.AnswerOrEmptyString("GenericPTGoals7Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("8")){ %>
        printview.checkbox("Patient to be independent with safety issues in <%= data.AnswerOrEmptyString("GenericPTGoals8Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("9")){ %>
        printview.checkbox("Patient will be able to negotiate stairs with <%= data.AnswerOrEmptyString("GenericPTGoals9Assist").Clean() %> device with <%= data.AnswerOrEmptyString("GenericPTGoals9Within").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericPTGoals9Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("10")){ %>
        printview.checkbox("Patient will be able to ambulate using <%= data.AnswerOrEmptyString("GenericPTGoals10Device").Clean() %> device at least <%= data.AnswerOrEmptyString("GenericPTGoals10Feet").Clean() %> feet with <%= data.AnswerOrEmptyString("GenericPTGoals10Within").Clean() %> assist with safe and effective gait pattern",<%= genericPTGoals.Contains("10").ToString().ToLower() %>)+ 
        printview.span("  on even/uneven surface within <%= data.AnswerOrEmptyString("GenericPTGoals10Weeks").Clean() %> weeks.",0,1)+ 
            
        <%} %>
        <%if(genericPTGoals.Contains("11")){ %>
        printview.checkbox("Independent with ambulation without device indoor/outdoor at least <%= data.AnswerOrEmptyString("GenericPTGoals11Feet").Clean() %> feet to allow community access within <%= data.AnswerOrEmptyString("GenericPTGoals11Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("12")){ %>
        printview.checkbox("Patient will be able to ambulate using <%= data.AnswerOrEmptyString("GenericPTGoals12Device").Clean() %> device at least <%= data.AnswerOrEmptyString("GenericPTGoals12Feet").Clean() %> feet on <%= data.AnswerOrEmptyString("GenericPTGoals12Within").Clean() %> surface to be able to perform ADL within <%= data.AnswerOrEmptyString("GenericPTGoals12Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("13")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("GenericPTGoals13Of").Clean() %> to <%= data.AnswerOrEmptyString("GenericPTGoals13To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("GenericPTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericPTGoals13Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("14")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("GenericPTGoals14Of").Clean() %> to <%= data.AnswerOrEmptyString("GenericPTGoals14To").Clean() %> grade to improve gait pattern/stability and decrease fall risk within <%= data.AnswerOrEmptyString("GenericPTGoals14Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("15")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericPTGoals15To").Clean() %> to improve postural control and balance within <%= data.AnswerOrEmptyString("GenericPTGoals15Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("16")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericPTGoals16To").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("GenericPTGoals16Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("17")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("GenericPTGoals17Joint").Clean() %> joint to <%= data.AnswerOrEmptyString("GenericPTGoals17Degree").Clean() %> degree of <%= data.AnswerOrEmptyString("GenericPTGoals17In").Clean() %> in <%= data.AnswerOrEmptyString("GenericPTGoals17Weeks").Clean() %> weeks to <%= data.AnswerOrEmptyString("GenericPTGoals17IncreaseROMTo").Clean() %>.",<%= genericPTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("18")){ %>
        printview.checkbox("Demonstrate safe and effective use of prosthesis/brace/splint within <%= data.AnswerOrEmptyString("GenericPTGoals18Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("18").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("19")){ %>
        printview.checkbox("Demonstrate safe and effective use of <%= data.AnswerOrEmptyString("GenericPTGoals19Within").Clean() %> DME within <%= data.AnswerOrEmptyString("GenericPTGoals19Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("19").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("20")){ %>
        printview.checkbox("Patient will have increase in Tinetti Performance Oriented Mobility Assessment score to <%= data.AnswerOrEmptyString("GenericPTGoals20Within").Clean() %> over 28 to reduce",<%= genericPTGoals.Contains("20").ToString().ToLower() %>)+ 
        printview.span("  fall risk within <%= data.AnswerOrEmptyString("GenericPTGoals20Weeks").Clean() %> weeks.",0,1)+
        <%} %>
        <%if(genericPTGoals.Contains("21")){ %>
        printview.checkbox("Patient will have improved <%= data.AnswerOrEmptyString("GenericPTGoals21Improve").Clean() %> standardized test score to improve <%= data.AnswerOrEmptyString("GenericPTGoals21Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericPTGoals21Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("21").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("22")){ %>
        printview.checkbox("Patient will have increase in Timed Up and Go score to <%= data.AnswerOrEmptyString("GenericPTGoals22Seconds").Clean() %> seconds to reduce fall risk and improve mobility within <%= data.AnswerOrEmptyString("GenericPTGoals22Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("22").ToString().ToLower() %>)+
        <%} %>
        <%if(type.Contains("PT")){ %>
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPTGoalsComments").Clean() %>",0,2)+
        printview.span("PT Short Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPTShortTermGoalsComments").Clean() %>",0,2)+
        printview.span("PT Long Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPTLongTermGoalsComments").Clean() %>",0,2)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("PTGoalsDesired").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver desired outcomes:<%=data.AnswerOrEmptyString("PTGoalsDesiredOutcomes").Clean() %>",<%= data.AnswerOrEmptyString("PTGoalsDesired").Contains("1").ToString().ToLower() %>)),
        "PT Goals");
        <%}else if(type.Contains("OT")){ %>
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPTGoalsComments").Clean() %>",0,2)+
        printview.span("Rehab Potential:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericRehabPotential").Clean() %>",0,2),
        "OT Goals");
        <%} %>
</script>
<%} %>