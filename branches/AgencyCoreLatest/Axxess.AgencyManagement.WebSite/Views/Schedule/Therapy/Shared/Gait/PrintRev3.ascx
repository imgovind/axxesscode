﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
<% if(data.ContainsKey("IsGaitApply") && data.AnswerOrEmptyString("IsGaitApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Gait Analysis");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet").Clean() %> steps")+
            printview.span("")+
            printview.col(3,
            printview.checkbox("No Rail",<%= genericGaitStepStairRail.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("1 Rail",<%= genericGaitStepStairRail.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("2 Rails",<%= genericGaitStepStairRail.Contains("3").ToString().ToLower() %>)) +
            printview.span("Assistive Device",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Gait Quality/Deviation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment").Clean() %>",0,1)+
            printview.col(2,
                printview.span("WB Status",true)+
                printview.span("<%=data.AnswerOrEmptyString("WBStatus").Clean() %>",true))+
            printview.span("Comment",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericGaitFinalComment").Clean() %>",0,1),
        "Gait Analysis");
</script>
<%} %>