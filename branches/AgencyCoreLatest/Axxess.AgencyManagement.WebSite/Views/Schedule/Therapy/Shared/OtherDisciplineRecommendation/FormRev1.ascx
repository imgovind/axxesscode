﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>RecommendationContainer">
    <%  string[] genericDisciplineRecommendation = data.AnswerArray("POCGenericDisciplineRecommendation"); %>
    <input type="hidden" name="<%= Model.Type %>_POCGenericDisciplineRecommendation"
        value="" />
    <table class="fixed">
        <tbody>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation1" class="float-left">
                            Disciplines</label></div>
                    <div class="margin">
                        <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation1' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation1">
                            PT</label>
                        <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation2' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation2">
                            MSW</label>
                        <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation3' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation3">
                            ST</label>
                        <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation4' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation4">
                            Podiatrist</label>
                        <span>Other</span>
                        <%= Html.TextBox(Model.Type + "_POCGenericDisciplineRecommendationOther", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther"), new { @id = Model.Type + "_POCGenericDisciplineRecommendationOther" })%>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="<%= Model.Type %>_POCGenericDisciplineRecommendationReason" class="strong">
                            Reason</label>
                        <%= Html.Templates(Model.Type + "_POCDisciplineRecommendationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                        <%= Html.TextArea(Model.Type + "_POCGenericDisciplineRecommendationReason", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason"), new { @class = "fill", @id = Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
