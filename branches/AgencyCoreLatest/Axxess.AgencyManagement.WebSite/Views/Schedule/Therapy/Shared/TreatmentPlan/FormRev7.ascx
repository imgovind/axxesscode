﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("POCGenericTreatmentPlan"); %>
<input type="hidden" name="<%= Model.Type %>_POCGenericTreatmentPlan" value="" />
<table class="fixed align-left">
    <tbody>
        <tr>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_POCGenericFrequencyAndDuration" class="strong">OT Frequency & Duration</label>
                    
                    <%= Html.TextBox(Model.Type + "_POCGenericFrequencyAndDuration", data.AnswerOrEmptyString("POCGenericFrequencyAndDuration"), new { @id = Model.Type + "_POCGenericFrequencyAndDuration", @class = "" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan1' class='radio' name='{1}_POCGenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan1">Therapeutic exercise</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan2' class='radio' name='{1}_POCGenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan2">Therapeutic activities (reaching, bending, etc)</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan3' class='radio' name='{1}_POCGenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan3">Neuromuscular re-education</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan4' class='radio' name='{1}_POCGenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan4">Teach safe and effective use of adaptive/assist device</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan5' class='radio' name='{1}_POCGenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan.Contains("5").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan5">Teach fall prevention/safety</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan6' class='radio' name='{1}_POCGenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan.Contains("6").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan6">Establish/upgrade home exercise program</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan7' class='radio' name='{1}_POCGenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan.Contains("7").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan7">Pt/caregiver education/training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan8' class='radio' name='{1}_POCGenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan.Contains("8").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan8">Sensory integrative techniques</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan9' class='radio' name='{1}_POCGenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan.Contains("9").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan9">Postural control training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan10' class='radio' name='{1}_POCGenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan.Contains("10").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan10">Teach energy conservation techniques</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan11' class='radio' name='{1}_POCGenericTreatmentPlan' value='11' type='checkbox' {0} />", genericTreatmentPlan.Contains("11").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan11">Wheelchair management training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan12' class='radio' name='{1}_POCGenericTreatmentPlan' value='12' type='checkbox' {0} />", genericTreatmentPlan.Contains("12").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan12">Teach safe and effective breathing technique</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan13' class='radio' name='{1}_POCGenericTreatmentPlan' value='13' type='checkbox' {0} />", genericTreatmentPlan.Contains("13").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan13">Teach work simplification</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan14' class='radio' name='{1}_POCGenericTreatmentPlan' value='14' type='checkbox' {0} />", genericTreatmentPlan.Contains("14").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan14">Community/work integration</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan15' class='radio' name='{1}_POCGenericTreatmentPlan' value='15' type='checkbox' {0} />", genericTreatmentPlan.Contains("15").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan15">Self care management training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan16' class='radio' name='{1}_POCGenericTreatmentPlan' value='16' type='checkbox' {0} />", genericTreatmentPlan.Contains("16").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan16">Cognitive skills development/training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan17' class='radio' name='{1}_POCGenericTreatmentPlan' value='17' type='checkbox' {0} />", genericTreatmentPlan.Contains("17").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan17">Teach task segmentation</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_POCGenericTreatmentPlan18' class='radio' name='{1}_POCGenericTreatmentPlan' value='18' type='checkbox' {0} />", genericTreatmentPlan.Contains("18").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_POCGenericTreatmentPlan18">Manual therapy techniques</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_POCGenericTreatmentPlan19' class='radio' name='{1}_POCGenericTreatmentPlan' value='19' type='checkbox' {0} />", genericTreatmentPlan.Contains("19").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericTreatmentPlan19">Electrical stimulation</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_POCGenericTreatmentPlan19BodyParts", data.AnswerOrEmptyString("POCGenericTreatmentPlan19BodyParts"), new { @class = "", @id = Model.Type + "_POCGenericTreatmentPlan19BodyParts" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_POCGenericTreatmentPlan19Duration", data.AnswerOrEmptyString("POCGenericTreatmentPlan19Duration"), new { @class = "", @id = Model.Type + "_POCGenericTreatmentPlan19Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_POCGenericTreatmentPlan20' class='radio' name='{1}_POCGenericTreatmentPlan' value='20' type='checkbox' {0} />", genericTreatmentPlan.Contains("20").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericTreatmentPlan20">Ultrasound</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_POCGenericTreatmentPlan20BodyParts", data.AnswerOrEmptyString("POCGenericTreatmentPlan20BodyParts"), new { @class = "", @id = Model.Type + "_POCGenericTreatmentPlan20BodyParts" })%>
                    <span>Dosage </span>
                    <%= Html.TextBox(Model.Type + "_POCGenericTreatmentPlan20Dosage", data.AnswerOrEmptyString("POCGenericTreatmentPlan20Dosage"), new { @class = "", @id = Model.Type + "_POCGenericTreatmentPlan20Dosage" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_POCGenericTreatmentPlan20Duration", data.AnswerOrEmptyString("POCGenericTreatmentPlan20Duration"), new { @class = "", @id = Model.Type + "_POCGenericTreatmentPlan20Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="align-center">
                <label for="<%= Model.Type %>_POCGenericTreatmentPlanOther">Other</label>
                <%= Html.Templates(Model.Type + "_POCTreatmentPlanOtherTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericTreatmentPlanOther" })%>
                <%= Html.TextArea(Model.Type + "_POCGenericTreatmentPlanOther", data.AnswerOrEmptyString("POCGenericTreatmentPlanOther"), new { @id = Model.Type + "_POCGenericTreatmentPlanOther", @class = "fill" })%>
            </td>
        </tr>
    </tbody>
</table>