﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Patient ",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("1").ToString().ToLower() %>)) +
        printview.span("understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)",true)+
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("0").ToString().ToLower() %>))+
        printview.checkbox("Physician notified and agrees with POC, frequency and duration. Comments (if any):<%=data.AnswerOrEmptyString("POCNotificationPhysicianComment").Clean() %>",<%= data.AnswerOrEmptyString("POCNotificationPhysician").Contains("0").ToString().ToLower() %>),
    "Notification");
</script>

