﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
printview.addsection(
        printview.col(2,
            printview.checkbox("Patient agreed to poc.",<%= data.AnswerOrEmptyString("Notification").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Verbal order received from physician.",<%= data.AnswerOrEmptyString("Notification").Contains("1").ToString().ToLower() %>)),
            "Notification");
</script>