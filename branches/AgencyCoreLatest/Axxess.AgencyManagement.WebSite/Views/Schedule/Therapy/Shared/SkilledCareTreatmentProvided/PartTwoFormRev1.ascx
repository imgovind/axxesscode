﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>SkilledCareContainer2">
    <span class="float-left">Treatment Performed:</span>
    <%= Html.Templates(Model.Type + "_POCSkilledCareTreatmentPerformedTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
    <%= Html.TextArea(Model.Type + "_POCSkilledCareTreatmentPerformed", data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
    <div class="clear" />
    <span class="float-left">Patient Response:</span>
    <%= Html.Templates(Model.Type + "_POCSkilledCarePatientResponseTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCarePatientResponse" })%>
    <%= Html.TextArea(Model.Type + "_POCSkilledCarePatientResponse", data.AnswerOrEmptyString("POCSkilledCarePatientResponse"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCarePatientResponse" })%>
</div>
