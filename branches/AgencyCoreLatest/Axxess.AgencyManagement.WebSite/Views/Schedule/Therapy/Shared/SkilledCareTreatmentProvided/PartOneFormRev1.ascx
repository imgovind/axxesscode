﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>SkilledCareContainer1">
    <span class="float-left">Training Topics:</span>
    <%= Html.Templates(Model.Type + "_POCSkilledCareTrainingTopicsTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTrainingTopics" })%>
    <%= Html.TextArea(Model.Type + "_POCSkilledCareTrainingTopics", data.AnswerOrEmptyString("POCSkilledCareTrainingTopics"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTrainingTopics" })%>
    <div class="clear" />
    <span class="float-left">Trained:</span>
    <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained0' name='{0}_POCSkilledCareTrained' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToChecked())%>Patient
    <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained1' name='{0}_POCSkilledCareTrained' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToChecked())%>Caregiver
</div>
