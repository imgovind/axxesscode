﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(7,
            printview.span("SBP",true) +
            printview.span("DBP",true) +
            printview.span("HR",true) +
            printview.span("Resp",true) +
            printview.span("Temp",true) +
            printview.span("Weight",true) +
            printview.span("BS",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressure").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressurePer").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericPulse").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericResp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTemp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWeight").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugar").Clean() %>")),
        "Vital Signs");
</script>