﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PTA Supervisory Visit | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "PTSupervisoryVisitForm" })) { %>
    <%= Html.Hidden("PTSupervisoryVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("PTSupervisoryVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("PTSupervisoryVisit_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("DisciplineTask", "121")%>
    <%= Html.Hidden("Type", "PTSupervisoryVisit")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    PTA Supervisory Visit
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="PTSupervisoryVisit_Therapist" class="float-left">PTA:</label>
                        <div class="float-right"><%= Html.PTTherapists("Therapist", data.ContainsKey("Therapist") ? data["Therapist"].Answer : "", new { @id = "PTSupervisoryVisit_Therapist" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left">PTA Present:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("PTSupervisoryVisit_TherapistPresent", "1", data.ContainsKey("TherapistPresent") && data["TherapistPresent"].Answer == "1" ? true : false, new { @id="PTSupervisoryVisit_TherapistPresentY", @class="radio" })%>
                            <label class="inline-radio" for="PTSupervisoryVisit_TherapistPresentY">Yes</label>
                            <%= Html.RadioButton("PTSupervisoryVisit_TherapistPresent", "0", data.ContainsKey("TherapistPresent") && data["TherapistPresent"].Answer == "0" ? true : false, new { @id="PTSupervisoryVisit_TherapistPresentN", @class="radio" })%>
                            <label class="inline-radio" for="PTSupervisoryVisit_TherapistPresentN">No</label>
                        </div>
                    </div>
                </td>
                <td colspan="2">
                    <div>
                        <label for="PTSupervisoryVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="PTSupervisoryVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="PTSupervisoryVisit_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="PTSupervisoryVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("PTSupervisoryVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digitd", @maxlength=6, @id = "PTSupervisoryVisit_AssociatedMileage" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Evaluation</th>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">1.</span>
                        Arrives for assigned visits as scheduled:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_ArriveOnTimeY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_ArriveOnTimeY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_ArriveOnTimeN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_ArriveOnTimeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">2.</span>
                        Follows client&#8217;s plan of care:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_FollowPOCY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_FollowPOCY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_FollowPOCN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_FollowPOCN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">3.</span>
                        Demonstrates positive and helpful attitude towards the client and others:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_HasPositiveAttitudeY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_HasPositiveAttitudeY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_HasPositiveAttitudeN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_HasPositiveAttitudeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">4.</span>
                        Informs PT/Nurse Supervisor of client needs and changes in condition as appropriate:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_InformChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_InformChangesY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_InformChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_InformChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">5.</span>
                        PTA Implements Universal Precautions per agency policy:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_IsUniversalPrecautionsY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_IsUniversalPrecautionsY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_IsUniversalPrecautionsN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_IsUniversalPrecautionsN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">6.</span>
                        Any changes made to client plan of care at this time:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_POCChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_POCChangesY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_POCChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_POCChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">7.</span>
                        Patient/CG satisfied with care and services provided by PTA:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("PTSupervisoryVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "PTSupervisoryVisit_IsServicesSatisfactoryY", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_IsServicesSatisfactoryY">Yes</label>
                        <%= Html.RadioButton("PTSupervisoryVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "PTSupervisoryVisit_IsServicesSatisfactoryN", @class = "radio" })%>
                        <label class="inline-radio" for="PTSupervisoryVisit_IsServicesSatisfactoryN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="float-left">
                        <span class="alphali">8.</span>
                        Additional Comments/Findings:
                    </div>
                    <div class="clear"></div>
                    <div class="float-left" style="width:99%"><%= Html.TextArea("PTSupervisoryVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @style="height:80px;width:100%" })%></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="PTSupervisoryVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password("PTSupervisoryVisit_Clinician", "", new { @class = "", @id = "PTSupervisoryVisit_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="PTSupervisoryVisit_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="PTSupervisoryVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="PTSupervisoryVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="PTSupervisoryVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.PTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.PTSupervisoryVisit.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Visit.PTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.PTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#window_PTSupervisoryVisit form label.error").css({ 'position': 'relative', 'float': 'left' });
    function PTSupervisoryVisitAdd() {
        $("#PTSupervisoryVisit_Clinician").removeClass('required').addClass('required');
        $("#PTSupervisoryVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function PTSupervisoryVisitRemove() {
        $("#PTSupervisoryVisit_Clinician").removeClass('required');
        $("#PTSupervisoryVisit_SignatureDate").removeClass('required');
    }
</script>