﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Diagnosis</th>
            <th colspan="2">Status and History</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Pain Assessment</th>
            <th colspan="2">Homebound Status</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">ADLs/Functional Mobility Level/Level of Assist</th>
            <th colspan="2">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev1.ascx", Model); %></td>
            <td colspan="2" rowspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Treatment Codes</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentCodes/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Neuromuscular Reeducation</th>
        </tr>
        <tr>
            <td colspan="2">
                <%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="float-left">Sitting Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="float-left">Standing Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div>
                <div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
                    <%= string.Format("<input id='{1}_GenericUEWeightBearing1' class='radio' name='{1}_GenericUEWeightBearing' value='1' type='checkbox' {0} />", genericUEWeightBearing.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericUEWeightBearing1">UE Weight-Bearing Activities</label>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Assessment</th>
        </tr>
        <tr>
            <td colspan="4">
                <%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment1' class='radio' name='{1}_GenericAssessment' value='1' type='checkbox' {0} />", genericAssessment.Contains("1").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment1">Decreased Strength</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment2' class='radio' name='{1}_GenericAssessment' value='2' type='checkbox' {0} />", genericAssessment.Contains("2").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment2">Decreased ROM</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment3' class='radio' name='{1}_GenericAssessment' value='3' type='checkbox' {0} />", genericAssessment.Contains("3").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment3">Decreased ADL</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment4' class='radio' name='{1}_GenericAssessment' value='4' type='checkbox' {0} />", genericAssessment.Contains("4").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment4">Ind</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment5' class='radio' name='{1}_GenericAssessment' value='5' type='checkbox' {0} />", genericAssessment.Contains("5").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment5">Decreased Mobility</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment6' class='radio' name='{1}_GenericAssessment' value='6' type='checkbox' {0} />", genericAssessment.Contains("6").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment6">Pain</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment7' class='radio' name='{1}_GenericAssessment' value='7' type='checkbox' {0} />", genericAssessment.Contains("7").ToChecked(), Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment7">Decreased Endurance</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="third">
                    <label for="<%= Model.Type %>_GenericRehabPotential" class="float-left">Rehab Potential</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @class = "", @id = Model.Type + "_GenericRehabPotential" }) %></div>
                </div>
                <div class="third">
                    <label for="<%= Model.Type %>_GenericPrognosis" class="float-left">Prognosis</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPrognosis", data.AnswerOrEmptyString("GenericPrognosis"), new { @class = "", @id = Model.Type + "_GenericPrognosis" })%></div>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericAssessmentMore" class="float-left">Comments:</label>
                <div class="clear"></div>
                <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"), new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="2">Short Term Goals</th>
            <th colspan="2">Long Term Goals</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ShortTermGoals/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LongTermGoals/FormRev1.ascx", Model); %></td>
        </tr>
    </tbody>
</table>