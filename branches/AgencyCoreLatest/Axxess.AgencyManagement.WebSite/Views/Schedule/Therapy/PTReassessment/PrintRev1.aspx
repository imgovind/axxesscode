﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
<%  string[] genericGaitStepStairRail1 = data.AnswerArray("GenericGaitStepStairRail1"); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Reassessment" +
        "<%= Model.Type == "PTEvaluation" ? "Evaluation" : ""%><%= Model.Type == "PTReEvaluation" ? "Re-Evaluation" : ""%><%= Model.Type == "PTMaintenance" ? "Maintenance Visit" : ""%>" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : ""%>Evaluation%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev2.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Orientation:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentOrientation").Clean() %>",0,1) +
            printview.span("Level of Consciousness::",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentLOC").Clean() %>",0,1))+
            printview.span("Comment:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentComment").Clean() %>",0,1),
            "Mental Assessment");
   
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model); %>
<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>
 <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/PrintRev1.ascx", Model); %>           
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("Speech:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech").Clean() %>",0,1) +
            printview.span("Vision:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentVision").Clean() %>",0,1) +
            printview.span("Hearing:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing").Clean() %>",0,1) +
            printview.span("Skin:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin").Clean() %>",0,1) +
            printview.span("Edema:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema").Clean() %>",0,1) +
            printview.span("Muscle Tone:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone").Clean() %>",0,1) +
            printview.span("Coordination:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination").Clean() %>",0,1) +
            printview.span("Sensation:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation").Clean() %>",0,1) +
            printview.span("Endurance:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance").Clean() %>",0,1))+
            printview.col(3,
            printview.span("Safety Awareness:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness").Clean() %>",0,1)+
            printview.span("&#160;")),
        "Physical Assessment");
        
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LevelOfFunction/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorTherapyReceived").Clean()%>",0,3),
        "Prior Therapy Received");

    printview.addsection(
        printview.span(""),
        "Compare functional status");
    
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Rolling to Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Rolling to Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityComment").Clean() %>",0,1),
        "Bed Mobility-Prior",2);
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Rolling to Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Rolling to Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityComment1").Clean() %>",0,1),
        "Bed Mobility-Current");
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),
        "Transfer-Prior",2);
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic1").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist1").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceStatic1").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic1").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),
        "Transfer-Current");
        
        printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet").Clean() %> feet")+
            printview.span("")+
            printview.col(3,
            printview.checkbox("No Rail",<%= genericGaitStepStairRail.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("1 Rail",<%= genericGaitStepStairRail.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("2 Rails",<%= genericGaitStepStairRail.Contains("3").ToString().ToLower() %>)) +
            printview.span("Assistive Device",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Gait Quality/Deviation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment").Clean() %>",0,1),
        "Gait Analysis-Prior",2);
        
        printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet1").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet1").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet1").Clean() %> feet")+
            printview.span("")+
            printview.col(3,
            printview.checkbox("No Rail",<%= genericGaitStepStairRail1.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("1 Rail",<%= genericGaitStepStairRail1.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("2 Rails",<%= genericGaitStepStairRail1.Contains("3").ToString().ToLower() %>)) +
            printview.span("Assistive Device",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Gait Quality/Deviation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment1").Clean() %>",0,1),
        "Gait Analysis-Current");

    printview.addsubsection(
        printview.col(2,
            printview.span("<b>Status:</b><%= data.AnswerOrEmptyString("GenericWBStatus").StringIntToEnumDescriptionFactory("WeightBearingStatus").Clean() %>",0,1) +
            printview.span("<b>Other:</b><%= data.AnswerOrEmptyString("GenericWBStatusOther").Clean()%>",0,1)) +
            printview.span("Comment", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSComment").Clean()%>",0,1),
        "WB-Prior",2);
        
    printview.addsubsection(
        printview.col(2,
            printview.span("<b>Status:</b><%= data.AnswerOrEmptyString("GenericWBStatus1").StringIntToEnumDescriptionFactory("WeightBearingStatus").Clean() %>",0,1) +
            printview.span("<b>Other:</b><%= data.AnswerOrEmptyString("GenericWBStatusOther1").Clean()%>",0,1)) +
            printview.span("Comment", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSComment1").Clean()%>",0,1),
        "WB-Current");
        
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA").Clean()%>",0,1)+
            printview.span("Timed Get Up & Go Test",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp").Clean()%>",0,1)+
            printview.span("Functional Reach",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach").Clean()%>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther").Clean()%>",0,2)),
            "Standardized test-Prior",2);
    
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA1").Clean()%>",0,1)+
            printview.span("Timed Get Up & Go Test",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp1").Clean()%>",0,1)+
            printview.span("Functional Reach",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach1").Clean()%>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther1").Clean()%>",0,2)),
            "Standardized test-Prior");

    printview.addsection(
        printview.span("Indicate all factors influencing the patient's progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericFactors").Clean() %>",0,2)+
        printview.span("Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician's professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericExpectations").Clean() %>",0,2)+
        printview.span("Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericRecommendations").Clean() %>",0,2),
        "");
        
   printview.addsection(
    printview.col(2,
        printview.checkbox("POC UNCHANGED (PHYSICIAN SIGNATURE NOT REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("1").ToString().ToLower() %>) +
        printview.checkbox("POC CHANGED (PHYSICIAN SIGNATURE REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("0").ToString().ToLower() %>) ),
        "");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericCareCoordination").Clean() %>",0,3),"Care Coordination");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericSkilledCareProvided").Clean() %>",0,3),"Skilled Care Provided This Visit");
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));

</script>
</body>
</html>
