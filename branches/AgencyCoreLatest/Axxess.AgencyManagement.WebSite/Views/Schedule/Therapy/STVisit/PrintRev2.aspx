﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EST Visit " +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EST Reassessment%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
        
    printview.addsubsection(
        printview.span("<%=data.AnswerOrEmptyString("GenericTreatmentDiagnosis").Clean() %>"),
        "Treatment Diagnosis/Problem",2);
    printview.addsubsection(
        printview.span("<%=data.AnswerOrEmptyString("GenericTreatmentOutcome").Clean() %>"),
        "Subjective");
 </script>
 
    <%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
       printview.col(3,
          printview.checkbox("Evaluation",<%= data.AnswerArray("STInstruction").Contains("1").ToString().ToLower()%>) +
          printview.checkbox("Dysphagia treatments",<%= data.AnswerArray("STInstruction").Contains("2").ToString().ToLower()%>) +
          printview.checkbox("Safe swallowing evaluation",<%= data.AnswerArray("STInstruction").Contains("3").ToString().ToLower()%>) +
          printview.checkbox("Patient/Family education",<%= data.AnswerArray("STInstruction").Contains("4").ToString().ToLower()%>) +
          printview.checkbox("Language disorders",<%= data.AnswerArray("STInstruction").Contains("5").ToString().ToLower()%>) +
          printview.checkbox("Speech dysphagia instruction program",<%= data.AnswerArray("STInstruction").Contains("6").ToString().ToLower()%>) +
          printview.checkbox("Voice disorders",<%= data.AnswerArray("STInstruction").Contains("7").ToString().ToLower()%>) +
          printview.checkbox("Aural rehabilitation",<%= data.AnswerArray("STInstruction").Contains("8").ToString().ToLower()%>) +
          printview.checkbox("Teach/Develop communication system",<%= data.AnswerArray("STInstruction").Contains("9").ToString().ToLower()%>) +
          printview.checkbox("Speech articulation disorders",<%= data.AnswerArray("STInstruction").Contains("10").ToString().ToLower()%>) +
          printview.checkbox("Non-oral communication",<%= data.AnswerArray("STInstruction").Contains("11").ToString().ToLower()%>) +
          printview.checkbox("Pain Management",<%= data.AnswerArray("STInstruction").Contains("12").ToString().ToLower()%>) +
          printview.checkbox("Alaryngeal speech skills",<%= data.AnswerArray("STInstruction").Contains("13").ToString().ToLower()%>) +
          printview.checkbox("Language processing",<%= data.AnswerArray("STInstruction").Contains("14").ToString().ToLower()%>) +
          printview.checkbox("Food texture recommendations",<%= data.AnswerArray("STInstruction").Contains("15").ToString().ToLower()%>)) +
      printview.checkbox("Other<%=data.AnswerArray("STInstruction").Contains("17")?":"+data.AnswerOrEmptyString("STInstructionOther").Clean():"" %>",<%= data.AnswerArray("STInstruction").Contains("17").ToString().ToLower()%>) +
      printview.checkbox("Establish home maintenance program:",<%= data.AnswerArray("STInstruction").Contains("16").ToString().ToLower()%>) +  
      printview.col(2,
          printview.checkbox("Copy given to patient",<%= data.AnswerArray("STInstruction").Contains("16a").ToString().ToLower()%>) +
          printview.checkbox("Copy attached to chart",<%= data.AnswerArray("STInstruction").Contains("16b").ToString().ToLower()%>)),
      "Skilled treatment provided this visit");
      printview.addsection(
        printview.span("<%=data.AnswerOrEmptyString("ObservationOutcomes").Clean() %>",2),
        "Assessment(Observations, instructions and measurable outcomes)");
      printview.addsection(
        printview.span("<%=data.AnswerOrEmptyString("ProgressMade").Clean() %>",2),
        "Progress made towards goals");
      printview.addsection(
        printview.span("<%=data.AnswerOrEmptyString("GenericResponse").Clean() %>",2),
        "Patient/caregiver response to treatment");
     printview.addsection(
        printview.span("<%=data.AnswerOrEmptyString("Teaching").Clean() %>",2),
        "Teaching");
     printview.addsection(
        printview.span("<%=data.AnswerOrEmptyString("GenericPlan").Clean() %>",2),
        "Plan");
        
            
            
 printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>


</body>
</html>