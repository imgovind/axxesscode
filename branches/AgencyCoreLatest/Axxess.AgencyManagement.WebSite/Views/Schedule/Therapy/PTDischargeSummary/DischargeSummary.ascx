﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PT Discharge Summary | <%= Model.Patient.DisplayName %></span>
<% var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<% using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "PTDischargeSummaryForm" })) { %>
    <%= Html.Hidden("PTDischargeSummary_PatientId", Model.PatientId)%>
    <%= Html.Hidden("PTDischargeSummary_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("PTDischargeSummary_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("DisciplineTask", "116")%>
    <%= Html.Hidden("Type", "PTDischargeSummary")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">   
                    PT Discharge Summary
    <%  if (Model.IsCommentExist) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.IsCommentExist) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
                <td>
                    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
                    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="PTDischargeSummary_EpsPeriod" class="float-left">Episode/Period:</label>
                        <div class="float-right"><%= Html.TextBox("PTDischargeSummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "PTDischargeSummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="PTDischargeSummary_DateCompleted" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="PTDischargeSummary_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="PTDischargeSummary_VisitDate" /></div>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="PTDischargeSummary_DischargeDate" class="float-left">Discharge Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="PTDischargeSummary_DischargeDate" value="<%= data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() && data["DischargeDate"].Answer.IsValidDate() ? data["DischargeDate"].Answer : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="PTDischargeSummary_DischargeDate" /></div>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
                <td colspan="2"> 
                    
                    <div>
                        <label for="PTDischargeSummary_Physician" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox("PTDischargeSummary_PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : (data.ContainsKey("Physician") && data["Physician"].Answer.IsNotNullOrEmpty() && data["Physician"].Answer.IsGuid() ?data["Physician"].Answer:Guid.Empty.ToString() ), new { @id = "PTDischargeSummary_PhysicianId", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left">Notification of Discharge given to patient:</label>
                        <div class="float-right">
                            <%= Html.Hidden("PTDischargeSummary_IsNotificationDC", " ", new { @id = "" })%>
                            <%= Html.RadioButton("PTDischargeSummary_IsNotificationDC", "1", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? true : false, new { @id = "PTDischargeSummary_IsNotificationDCY", @class = "radio" })%>
                            <label for="PTDischargeSummary_IsNotificationDCY" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("PTDischargeSummary_IsNotificationDC", "0", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "0" ? true : false, new { @id = "PTDischargeSummary_IsNotificationDCN", @class = "radio" })%>
                            <label for="PTDischargeSummary_IsNotificationDCN" class="inline-radio">No</label>
                        </div>
                        <div class="clear" />
                        <div class="float-right">
                            <label for="PTDischargeSummary_NotificationDate">If Yes:</label>
                            <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 day", Value = "1" },
                                    new SelectListItem { Text = "2 day", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                            <%= Html.DropDownList("PTDischargeSummary_NotificationDate", patientReceived, new { @id = "PTDischargeSummary_NotificationDate" }) %>
                            <%= Html.TextBox("PTDischargeSummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "PTDischargeSummary_NotificationDateOther", @style = "display:none;"}) %>
                        </div>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="PTDischargeSummary_ReasonForDC" class="float-left">Reason for Discharge:</label>
                        <%  var reasonForDC = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Goals Met", Value = "1" },
                                new SelectListItem { Text = "To Nursing Home", Value = "2" },
                                new SelectListItem { Text = "Deceased", Value = "3" },
                                new SelectListItem { Text = "Noncompliant", Value = "4" },
                                new SelectListItem { Text = "To Hospital", Value = "5" },
                                new SelectListItem { Text = "Moved from Service Area", Value = "6" },
                                new SelectListItem { Text = "Refused Care", Value = "7" },
                                new SelectListItem { Text = "No Longer Homebound", Value = "8" },
                                new SelectListItem { Text = "Other", Value = "9" }
                            }, "Value", "Text", data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : "0"); %>
                        <%= Html.DropDownList("PTDischargeSummary_ReasonForDC", reasonForDC, new { @id = "PTDischargeSummary_ReasonForDC", @class = "float-right" })%>
                        <%= Html.TextBox("PTDischargeSummary_ReasonForDCOther",data.ContainsKey("ReasonForDCOther") ? data["ReasonForDCOther"].Answer : "", new { @id = "PTDischargeSummary_ReasonForDCOther", @style = "display:none;", @class = "float-right" })%>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Patient Condition and Outcomes</th>
                <th colspan="2">Service(s) Provided</th>
            </tr>
            <tr>
                <td colspan="2">
                    <%  string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
                    <input name="PTDischargeSummary_PatientCondition" value=" " type="hidden" />
                    <table class="fixed align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionStable' name='PTDischargeSummary_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionStable" class="radio">Stable</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionImproved' name='PTDischargeSummary_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionImproved" class="radio">Improved</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionUnchanged' name='PTDischargeSummary_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionUnchanged" class="radio">Unchanged</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionUnstable' name='PTDischargeSummary_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionUnstable" class="radio">Unstable</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionDeclined' name='PTDischargeSummary_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionDeclined" class="radio">Declined</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionGoalsMet' name='PTDischargeSummary_PatientCondition' value='6' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("6") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionGoalsMet" class="radio">Goals Met</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionGoalsPartiallyMet' name='PTDischargeSummary_PatientCondition' value='7' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("7") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionGoalsPartiallyMet" class="radio">GoalsNot Met</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_PatientConditionGoalsNotMet' name='PTDischargeSummary_PatientCondition' value='8' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("8") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_PatientConditionGoalsNotMet" class="radio">Goals Partially Met</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="2">
                    <%  string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
                    <input name="PTDischargeSummary_ServiceProvided" value=" " type="hidden" />
                    <table class="fixed align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedSN' name='PTDischargeSummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedSN" class="radio">SN</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedPT' name='PTDischargeSummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedPT" class="radio">PT</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedOT' name='PTDischargeSummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedOT" class="radio">OT</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedST' name='PTDischargeSummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedST" class="radio">ST</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedMSW' name='PTDischargeSummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedMSW" class="radio">MSW</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedHHA' name='PTDischargeSummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedHHA" class="radio">HHA</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%= string.Format("<input id='PTDischargeSummary_ServiceProvidedOther' name='PTDischargeSummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "") %>
                                    <label for="PTDischargeSummary_ServiceProvidedOther" class="radio">Other</label>
                                    <%= Html.TextBox("PTDischargeSummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "", new { @id = "PTDischargeSummary_ServiceProvidedOtherValue", @class = "oe" })%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="2">Care Summary</th>
                <th colspan="2">Condition of Discharge</th>
            </tr>
            <tr>
                <td colspan="2">
                    <em>(Care Given, Progress, Regress including Therapies)</em>
                    <div class="float-right">
                        <label for="PTDischargeSummary_CareSummaryTemplates">Templates:</label>
                        <%= Html.Templates("PTDischargeSummary_CareSummaryTemplates", new { @class = "Templates", @template = "#PTDischargeSummary_CareSummary" })%>
                    </div>
                    <%= Html.TextArea("PTDischargeSummary_CareSummary",data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : "", new { @class = "fill", @id = "PTDischargeSummary_CareSummary", @style="height:150px" })%>
                </td>
                <td colspan="2">
                    <em>(Include VS, BS, Functional and Overall Status)</em>
                    <div class="float-right"><label for="PTDischargeSummary_ConditionOfDischargeTemplates">Templates:</label><%= Html.Templates("PTDischargeSummary_ConditionOfDischargeTemplates", new { @class = "Templates", @template = "#PTDischargeSummary_ConditionOfDischarge" })%></div>
                    <%= Html.TextArea("PTDischargeSummary_ConditionOfDischarge", data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer : "", new { @class = "fill", @id = "PTDischargeSummary_ConditionOfDischarge", @style = "height:150px" })%>
                </td>
            </tr>
            <tr>
                <th colspan="4">Discharge Details</th>
            </tr>
            <tr class="align-left">
                <td colspan="2">
                    <label class="strong">Discharge Disposition: Where is the Patient after Discharge from your Agency?</label>
                    <%= Html.Hidden("PTDischargeSummary_DischargeDisposition", " ", new { @id = "" })%>
                    <div>
                        <%= Html.RadioButton("PTDischargeSummary_DischargeDisposition", "01", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? true : false, new { @id = "PTDischargeSummary_DischargeDisposition1", @class = "radio float-left" })%>
                        <label for="PTDischargeSummary_DischargeDisposition1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (without formal assistive services)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("PTDischargeSummary_DischargeDisposition", "02", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? true : false, new { @id = "PTDischargeSummary_DischargeDisposition2", @class = "radio float-left" })%>
                        <label for="PTDischargeSummary_DischargeDisposition2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (with formal assistive services)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("PTDischargeSummary_DischargeDisposition", "03", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? true : false, new { @id = "PTDischargeSummary_DischargeDisposition3", @class = "radio float-left" })%>
                        <label for="PTDischargeSummary_DischargeDisposition3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient transferred to a non-institutional hospice)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("PTDischargeSummary_DischargeDisposition", "04", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? true : false, new { @id = "PTDischargeSummary_DischargeDisposition4", @class = "radio float-left" })%>
                        <label for="PTDischargeSummary_DischargeDisposition4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("PTDischargeSummary_DischargeDisposition", "UK", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? true : false, new { @id = "PTDischargeSummary_DischargeDispositionUK", @class = "radio float-left" })%>
                        <label for="PTDischargeSummary_DischargeDispositionUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Other unknown</span>
                        </label>
                    </div>
                </td>
                <td colspan="2">
                    <%  string[] dischargeInstructionsGivenTo = data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null; %>
                    <input name="PTDischargeSummary_DischargeInstructionsGivenTo" value=" " type="hidden" />
                    <label class="float-left">Discharge Instructions Given To:</label>
                    <div class="float-left">
                        <div class="float-left">
                            <%= string.Format("<input id='PTDischargeSummary_DischargeInstructionsGivenTo1' name='PTDischargeSummary_DischargeInstructionsGivenTo' value='1' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "checked='checked'" : "")%>
                            <label for="PTDischargeSummary_DischargeInstructionsGivenTo1" class="fixed radio">Patient</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='PTDischargeSummary_DischargeInstructionsGivenTo2' name='PTDischargeSummary_DischargeInstructionsGivenTo' value='2' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "checked='checked'" : "")%>
                            <label for="PTDischargeSummary_DischargeInstructionsGivenTo2" class="fixed radio">Caregiver</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='PTDischargeSummary_DischargeInstructionsGivenTo3' name='PTDischargeSummary_DischargeInstructionsGivenTo' value='3' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "checked='checked'" : "")%>
                            <label for="PTDischargeSummary_DischargeInstructionsGivenTo3" class="fixed radio">N/A</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='PTDischargeSummary_DischargeInstructionsGivenTo4' name='PTDischargeSummary_DischargeInstructionsGivenTo' value='4' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "checked='checked'" : "")%>
                            <label for="PTDischargeSummary_DischargeInstructionsGivenTo4" class="radio">Other:</label>
                            <%= Html.TextBox("PTDischargeSummary_DischargeInstructionsGivenToOther", data.ContainsKey("DischargeInstructionsGivenToOther") ? data["DischargeInstructionsGivenToOther"].Answer : "", new { @id = "PTDischargeSummary_DischargeInstructionsGivenToOther", @class="oe" })%>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <label class="strong" for="PTDischargeSummary_DischargeInstructions">Discharge Instructions:</label>
                    <%= Html.TextArea("PTDischargeSummary_DischargeInstructions", data.AnswerOrEmptyString("DischargeInstructions"), new { @id = "PTDischargeSummary_DischargeInstructions", @class="fill" }) %>
                    <div class="clear"></div>
                    <label class="float-left">Verbalized understanding:</label>
                    <div class="float-left">
                        <%= Html.Hidden("PTDischargeSummary_IsVerbalizedUnderstanding", " ", new { @id = "" })%>
                        <%= Html.RadioButton("PTDischargeSummary_IsVerbalizedUnderstanding", "1", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? true : false, new { @id = "PTDischargeSummary_IsVerbalizedUnderstandingY", @class = "radio" })%>
                        <label for="PTDischargeSummary_IsVerbalizedUnderstandingY" class="inline-radio">Yes</label>
                        <%= Html.RadioButton("PTDischargeSummary_IsVerbalizedUnderstanding", "0", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? true : false, new { @id = "PTDischargeSummary_IsVerbalizedUnderstandingN", @class = "radio" })%>
                        <label for="PTDischargeSummary_IsVerbalizedUnderstandingN" class="inline-radio">No</label>
                    </div>
                    <div class="clear"></div>
                    <%  string[] differentTasks = data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null; %>
                    <input name="PTDischargeSummary_DifferentTasks" value=" " type="hidden" />
                    <div>
                        <%= string.Format("<input id='PTDischargeSummary_DifferentTasks1' name='PTDischargeSummary_DifferentTasks' value='1' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("1") ? "checked='checked'" : "")%>
                        <label for="PTDischargeSummary_DifferentTasks1" style="margin-left:20px">All services notified and discontinued</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='PTDischargeSummary_DifferentTasks2' name='PTDischargeSummary_DifferentTasks' value='2' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("2") ? "checked='checked'" : "")%>
                        <label for="PTDischargeSummary_DifferentTasks2" style="margin-left:20px">Order and summary completed</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='PTDischargeSummary_DifferentTasks3' name='PTDischargeSummary_DifferentTasks' value='3' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("3") ? "checked='checked'" : "")%>
                        <label for="PTDischargeSummary_DifferentTasks3" style="margin-left:20px">Information provided to patient for continuing needs</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='PTDischargeSummary_DifferentTasks4' name='PTDischargeSummary_DifferentTasks' value='4' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("4") ? "checked='checked'" : "")%>
                        <label for="PTDischargeSummary_DifferentTasks4" style="margin-left:20px">Physician notified</label>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="PTDischargeSummary_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("PTDischargeSummary_Clinician", "", new { @id = "PTDischargeSummary_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="PTDischargeSummary_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="PTDischargeSummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : "" %>" id="PTDischargeSummary_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div>
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        Return to Clinician for Signature
                    </div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="PTDischargeSummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="PTDischargeSummaryRemove(); PTDischargeSummary.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="PTDischargeSummaryAdd(); PTDischargeSummary.Submit($(this));">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="PTDischargeSummaryRemove(); PTDischargeSummary.Submit($(this));">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="PTDischargeSummaryRemove(); PTDischargeSummary.Submit($(this));">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="PTDischargeSummaryRemove(); UserInterface.CloseWindow('PTDischargeSummary');">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#PTDischargeSummary_MR").attr('readonly', true);
    $("#PTDischargeSummary_EpsPeriod").attr('readonly', true);
    function PTDischargeSummaryAdd() {
        $("#PTDischargeSummary_Clinician").removeClass('required').addClass('required');
        $("#PTDischargeSummary_SignatureDate").removeClass('required').addClass('required');
    }
    function PTDischargeSummaryRemove() {
        $("#PTDischargeSummary_Clinician").removeClass('required');
        $("#PTDischargeSummary_SignatureDate").removeClass('required');
    }
</script>