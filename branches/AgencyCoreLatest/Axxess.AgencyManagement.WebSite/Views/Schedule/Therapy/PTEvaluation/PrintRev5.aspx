﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null&&Model.Agency!=null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT " +
        "<%= Model.Type == "PTEvaluation" ? "Evaluation" : ""%><%= Model.Type == "PTReEvaluation" ? "Re-Evaluation" : ""%><%= Model.Type == "PTMaintenance" ? "Maintenance Visit" : ""%>" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : ""%>Evaluation%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev2.ascx", Model); %>
<%if(data.AnswerOrEmptyString("IsMentalApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Mental Assessment");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Orientation:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentOrientation").Clean() %>",0,1) +
            printview.span("Level of Consciousness::",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentLOC").Clean() %>",0,1))+
            printview.span("Comment:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentComment").Clean() %>",0,1),
            "Mental Assessment");
   
</script>
<%} %>

<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model); %>

 <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/PrintRev1.ascx", Model); %>  
<% if (data.AnswerOrEmptyString("IsPhysicalApply").Equals("1"))
   { %>    
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Physical Assessment");
</script>
<%}
   else
   { %>     
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("Speech:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech").Clean() %>",0,1) +
            printview.span("Vision:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentVision").Clean() %>",0,1) +
            printview.span("Hearing:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing").Clean() %>",0,1) +
            printview.span("Skin:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin").Clean() %>",0,1) +
            printview.span("Edema:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema").Clean() %>",0,1) +
            printview.span("Muscle Tone:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone").Clean() %>",0,1) +
            printview.span("Coordination:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination").Clean() %>",0,1) +
            printview.span("Sensation:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation").Clean() %>",0,1) +
            printview.span("Endurance:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance").Clean() %>",0,1))+
            printview.col(3,
            printview.span("Safety Awareness:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness").Clean() %>",0,1)+
            printview.span("&#160;")),
        "Physical Assessment");
</script>
<%} %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LevelOfFunction/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model); %>
<% if (data.AnswerOrEmptyString("IsPTRApply").Equals("1"))
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Prior Therapy Received");
</script>
<%}
   else
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorTherapyReceived").Clean()%>",0,3),
        "Prior Therapy Received");
</script>
<%} %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/PrintRev2.ascx", Model); %>
<% if (data.AnswerOrEmptyString("IsWBApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "WB");
</script>
<%}
   else
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("<b>Status:</b><%= data.AnswerOrEmptyString("GenericWBStatus").StringIntToEnumDescriptionFactory("WeightBearingStatus").Clean() %>",0,1) +
            printview.span("<b>Other:</b><%= data.AnswerOrEmptyString("GenericWBStatusOther").Clean()%>",0,1)) +
            printview.span("Comment", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSComment").Clean()%>",0,1),
        "WB");
</script>
<%} %>
<% if (data.AnswerOrEmptyString("GenericIsWCMobilityApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsWCMobilityApplied").ToString() == "1")
   { %>
<script type="text/javascript">
printview.addsection(printview.checkbox("N/A",true),"W/C Mobility")
</script>
<%}
   else
   { %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/PrintRev1.ascx", Model); %>
<%} %>
<% if (data.AnswerOrEmptyString("IsAssessmentApply").Equals("1"))
   {%>
    <script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "Assessment");
    </script>
<%}
   else
   { %>
<script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericAssessmentComment").Clean() %>",0,3),"Assessment");
</script>
<%} %>

<% if (data.AnswerOrEmptyString("IsTestApply").Equals("1"))
    {%>
 <script type="text/javascript">
     printview.addsection(
        printview.checkbox("N/A", true),
        "Standardized test");
 </script> 
 <%}
    else
    { %>
 <script type="text/javascript">
    printview.addsection(
        printview.span(""),
        "Standardized test");
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA").Clean() %>",0,1)+
            printview.span("Timed Get Up & Go Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp").Clean() %>",0,1)+
            printview.span("Functional Reach:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach").Clean() %>",0,1))+
            printview.span("Other:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther").Clean() %>",0,3),
            "Prior",2);
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA1").Clean() %>",0,1)+
            printview.span("Timed Get Up & Go Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp1").Clean() %>",0,1)+
            printview.span("Functional Reach:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach1").Clean() %>",0,1))+
            printview.span("Other:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther1").Clean() %>",0,3),
            "Current");
 </script>
 <%} %>
<script type="text/javascript">
    printview.addsection(
        printview.span(""),"MD Orders");
    
</script>
<% if (data.AnswerOrEmptyString("IsDMEApply").Equals("1"))
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "DME");
</script>   
<%}
   else
   { %>
<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>
<%} %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/PrintRev3.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/PrintRev5.ascx", Model); %>
 <% if (data.AnswerOrEmptyString("IsModalitiesApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Modalities");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericModalitiesComment").Clean() %>",0,3),"Modalities");
  </script>
  <%} %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PTGoals/PrintRev3.ascx", Model); %>
<% if (data.AnswerOrEmptyString("IsRecommendationApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Other Discipline Recommendation");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("OT", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("MSW", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("ST", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Podiatrist", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther").Clean() %>",0,1)) +
            printview.span("Reason", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason").Clean() %>",0,1) ,
        "Other Discipline Recommendation");
 </script>       
 <%} %>
 <% if (data.AnswerOrEmptyString("IsRehabApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Rehab");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Rehab Diagnosis:<%=data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis").Clean() %>",true)+
        printview.span("Rehab Potential:",true)+
        printview.col(3,
            printview.checkbox("Good",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("0").ToString().ToLower() %>)+
            printview.checkbox("Fair",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Poor",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("2").ToString().ToLower() %>))+
        printview.span("<%=data.AnswerOrEmptyString("POCOtherRehabPotential").Clean() %>",0,1),
        "Rehab");
            
</script>
 <%} %>
 <% if (data.AnswerOrEmptyString("IsDCPlanApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Discharge Plan");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(
        printview.span("Patient to be discharged to the care of:",true)+
        printview.col(3,
            printview.checkbox("Physician",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToString().ToLower() %>)+
            printview.checkbox("Selfcare",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToString().ToLower() %>))+
        printview.span("Discharge Plans:",true)+
        printview.col(2,
            printview.checkbox("Discharge when caregiver willing and able to manage all aspects of patient’s care",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Discharge when goals met.",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToString().ToLower() %>))+
        printview.span("<%= data.AnswerOrEmptyString("POCDCPlanAdditional").Clean() %>",0,3),"Discharge Plan");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("IsSkilledCareApply").Equals("1"))
    { %>
 <script type="text/javascript">
     printview.addsection(
        printview.checkbox("N/A", true),
        "Skilled Care Provided");
  </script>
  <%}
    else
    { %>
    <script type="text/javascript">
    printview.addsection(
        printview.span("Training Topics:<%= data.AnswerOrEmptyString("POCSkilledCareTrainingTopics").Clean() %>",0,3)+
        printview.span("Trained:",true)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToString().ToLower() %>)) +
        printview.span("Treatment Performed: <%= data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed").Clean() %>",0,3)+
        printview.span("Patient Response: <%= data.AnswerOrEmptyString("POCSkilledCarePatientResponse").Clean() %>",0,3),
    "Skilled Care Provided");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("IsCareApply").Equals("1"))
    {%>
  <script type="text/javascript">
    printview.addsubsection(
        printview.checkbox("N/A",true),
        "Care Coordination",2);
  </script>
 <%}
    else
    { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericCareCoordination").Clean() %>",0,3),"Care Coordination");
  </script>
  <%} %>
  <% if (data.AnswerOrEmptyString("IsSafetyIssueApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsubsection(
        printview.checkbox("N/A", true),
        "Safety Issues/Instruction/Education");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericSafetyIssue").Clean() %>",0,3),"Safety Issues/Instruction/Education");
  </script>
  <%} %>
  <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/PrintRev3.ascx", Model); %>
  <script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>
