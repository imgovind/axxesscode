﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="10">Vital Signs
            <%= string.Format("<input class='radio' id='{0}IsVitalSignsApply' name='{0}_IsVitalSignsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsVitalSignsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10">
            <div id="<%= Model.Type %>VitalSignsContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Mental Assessment
            <%= string.Format("<input class='radio' id='{0}IsMentalApply' name='{0}_IsMentalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMentalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="6">Medical Diagnosis
            <%= string.Format("<input class='radio' id='{0}IsMedicalApply' name='{0}_IsMedicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMedicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%= Model.Type %>MentalContainer">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Orientation:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left strong">Level of Consciousness:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentLOC" })%></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericMentalAssessmentComment" class="strong">Comment</label>
                                    <%= Html.Templates(Model.Type + "_MentalAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericMentalAssessmentComment" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericMentalAssessmentComment", data.ContainsKey("GenericMentalAssessmentComment") ? data["GenericMentalAssessmentComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericMentalAssessmentComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table></div>
            </td>
            <td colspan="6"><div id="<%=Model.Type %>MedicalContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Pain Assessment
            <%= string.Format("<input class='radio' id='{0}IsPainApply' name='{0}_IsPainApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPainApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="6">Physical Assessment
            <%= string.Format("<input class='radio' id='{0}IsPhysicalAssessmentApply' name='{0}_IsPhysicalAssessmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPhysicalAssessmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>PainContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></div></td>
            <td colspan="6" rowspan="9"><div id="<%=Model.Type %>PhysicalAssessmentContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="4">DME
            <%= string.Format("<input class='radio' id='{0}IsDMEApply' name='{0}_IsDMEApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDMEApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>DMEContainer">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">Available:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @class = "", @id = Model.Type + "_GenericDMEAvailable" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">Needs:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @class = "", @id = Model.Type + "_GenericDMENeeds" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">Suggestion:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @class = "", @id = Model.Type + "_GenericDMESuggestion" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Living Situation
            <%= string.Format("<input class='radio' id='{0}IsLivingApply' name='{0}_IsLivingApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsLivingApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>LivingContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Physical Assessment
            <%= string.Format("<input class='radio' id='{0}IsPhysicalApply' name='{0}_IsPhysicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPhysicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>PhysicalContainer">
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSpeech" class="float-left">Speech:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSpeech", data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSpeech" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentVision" class="float-left">Vision:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentVision", data.AnswerOrEmptyString("GenericPhysicalAssessmentVision"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentVision" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentHearing" class="float-left">Hearing:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentHearing", data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentHearing" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSkin" class="float-left">Skin:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSkin", data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSkin" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEdema" class="float-left">Edema:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEdema", data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEdema" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentMuscleTone" class="float-left">Muscle Tone:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentMuscleTone", data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentMuscleTone" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentCoordination" class="float-left">Coordination:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentCoordination", data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentCoordination" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSensation" class="float-left">Sensation:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSensation", data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSensation" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEndurance" class="float-left">Endurance:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEndurance", data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEndurance" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSafetyAwareness" class="float-left">Safety Awareness:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSafetyAwareness", data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSafetyAwareness" })%></div>
                <div class="clear"></div>   
                </div>         
            </td>
        </tr>
        <tr>
            <th colspan="4">Prior And Current Level Of Function
            <%= string.Format("<input class='radio' id='{0}IsLOFApply' name='{0}_IsLOFApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsLOFApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>LOFContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LevelOfFunction/FormRev1.ascx", Model); %>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Homebound Reason
            <%= string.Format("<input class='radio' id='{0}IsHomeboundApply' name='{0}_IsHomeboundApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsHomeboundApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="6">Prior Therapy Received
            <%= string.Format("<input class='radio' id='{0}IsPTRApply' name='{0}_IsPTRApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPTRApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>HomeboundContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></div>
            </td>
            <td colspan="6"><div id="<%=Model.Type %>PTRContainer">
                <%= Html.Templates(Model.Type + "_GenericPriorTherapyTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPriorTherapyReceived" })%>
                <%= Html.TextArea(Model.Type + "_GenericPriorTherapyReceived", data.AnswerOrEmptyString("GenericPriorTherapyReceived"),6,20, new { @id = Model.Type + "_GenericPriorTherapyReceived", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Functional Mobility Key</th>
        </tr>
        <tr>
            <td colspan="10">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>I = Independent</td>
                            <td>S = Supervision</td>
                            <td>VC = Verbal Cue</td>
                            <td>CGA = Contact Guard Assist</td>
                        </tr>
                        <tr>
                            <td>Min A = 25% Assist</td>
                            <td>Mod A = 50% Assist</td>
                            <td>Max A = 75% Assist</td>
                            <td>Total = 100% Assist</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="5">Bed Mobility
            <%= string.Format("<input class='radio' id='{0}IsBedApply' name='{0}_IsBedApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsBedApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Gait Analysis
            <%= string.Format("<input class='radio' id='{0}IsGaitApply' name='{0}_IsGaitApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGaitApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>BedContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev2.ascx", Model); %></div></td>
            <td colspan="5"><div id="<%=Model.Type %>GaitContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="5">Transfer
            <%= string.Format("<input class='radio' id='{0}IsTransferApply' name='{0}_IsTransferApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTransferApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">WB
            <%= string.Format("<input class='radio' id='{0}IsWBApply' name='{0}_IsWBApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsWBApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5" rowspan="7"><div id="<%=Model.Type %>TransferContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev2.ascx", Model); %></div></td>
            <td colspan="5"><div id="<%=Model.Type %>WBContainer">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Status:</td>
                            <td><%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus", data.AnswerOrEmptyString("GenericWBStatus"), new { @id = Model.Type + "_GenericWBStatus", @class = "" })%></td>
                            <td><span>Other: </span><%= Html.TextBox(Model.Type + "_GenericWBStatusOther", data.AnswerOrEmptyString("GenericWBStatusOther"), new { @class = "", @id = Model.Type + "_GenericWBStatusOther" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div>
                                    <label for="<%= Model.Type %>_GenericWBSComment" class="strong">Comment</label>
                                    <%= Html.Templates(Model.Type + "_WBSTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericWBSComment" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericWBSComment", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">
                W/C Mobility &#8212;
                <%  string[] isWCMobilityApplied = data.AnswerArray("GenericIsWCMobilityApplied"); %>
                <%= Html.Hidden(Model.Type + "isWCMobilityApplied", string.Empty, new { @id = Model.Type + "_GenericIsWCMobilityAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsWCMobilityApplied1' name='{0}_GenericIsWCMobilityApplied' value='1' type='checkbox' {1} />", Model.Type, isWCMobilityApplied.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericIsWCMobilityApplied1">N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%= Model.Type %>_WCMobilityContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="5">Assessment
            <%= string.Format("<input class='radio' id='{0}IsAssessmentApply' name='{0}_IsAssessmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsAssessmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>AssessmentContainer">
            <%= Html.Templates(Model.Type + "_GenericAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericAssessmentComment" })%>
            <%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.ContainsKey("GenericAssessmentComment") ? data["GenericAssessmentComment"].Answer : string.Empty, 3,20,new { @id = Model.Type + "_GenericAssessmentComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Discharge Plan<%= string.Format("<input class='radio' id='{0}IsDCPlanApply' name='{0}_IsDCPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDCPlanApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>DCPlanContainer">
            <%= Html.Templates(Model.Type + "_GenericDCPlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericDCPlanComment" })%>
            <%= Html.TextArea(Model.Type + "_GenericDCPlanComment", data.ContainsKey("GenericDCPlanComment") ? data["GenericDCPlanComment"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericDCPlanComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Treatment Plan
            <%= string.Format("<input class='radio' id='{0}IsTreatmentApply' name='{0}_IsTreatmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTreatmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>TreatmentContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">PT Goals
            <%= string.Format("<input class='radio' id='{0}IsGoalsApply' name='{0}_IsGoalsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGoalsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>GoalsContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PTGoals/FormRev1.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">Other Discipline Recommendation
            <%= string.Format("<input class='radio' id='{0}IsRecommendationApply' name='{0}_IsRecommendationApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRecommendationApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>RecommendationContainer">
                <%  string[] genericDisciplineRecommendation = data.AnswerArray("GenericDisciplineRecommendation"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <div><label for="<%= Model.Type %>_GenericDisciplineRecommendation1" class="float-left">Disciplines</label></div>
                                <div class="margin">
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation1' class='radio' name='{1}_GenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation1">OT</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation2' class='radio' name='{1}_GenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation2">MSW</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation3' class='radio' name='{1}_GenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation3">ST</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation4' class='radio' name='{1}_GenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation4">Podiatrist</label>
                                    <span>Other</span>
                                    <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.AnswerOrEmptyString("GenericDisciplineRecommendationOther"), new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="strong">Reason</label>
                                    <%= Html.Templates(Model.Type + "_DisciplineRecommendationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericDisciplineRecommendationReason" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericDisciplineRecommendationReason", data.AnswerOrEmptyString("GenericDisciplineRecommendationReason"), new { @class = "fill", @id = Model.Type + "_GenericDisciplineRecommendationReason" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericRehabPotential" class="strong">Rehab Potential</label>
                                    <%= Html.Templates(Model.Type + "_RehabTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericRehabPotential" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @class = "fill", @id = Model.Type + "_GenericRehabPotential" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="strong">Frequency & Duration</label>
                                    <%= Html.Templates(Model.Type + "_FrequencyAndDurationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericFrequencyAndDuration" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericFrequencyAndDuration", data.AnswerOrEmptyString("GenericFrequencyAndDuration"), new { @id = Model.Type + "_GenericFrequencyAndDuration", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Standardized test
            <%= string.Format("<input class='radio' id='{0}IsTestApply' name='{0}_IsTestApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTestApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>TestContainer1">
                <div class="float-middle strong">Prior</div>
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA", data.AnswerOrEmptyString("GenericTinettiPOMA"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp", data.AnswerOrEmptyString("GenericTimedGetUp"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach", data.AnswerOrEmptyString("GenericFunctionalReach"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span>
                <%= Html.Templates(Model.Type + "_StandardizedTestTemplates1", new { @class = "Templates", @template = "#" + Model.Type + "_GenericStandardizedTestOther" })%>
                <%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther" })%></div>
            </div>
            </td>
            <td colspan="5"><div id="<%=Model.Type %>TestContainer2">
                <div class="float-middle strong">Current</div>
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA1", data.AnswerOrEmptyString("GenericTinettiPOMA1"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp1", data.AnswerOrEmptyString("GenericTimedGetUp1"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach1", data.AnswerOrEmptyString("GenericFunctionalReach1"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span>
                <%= Html.Templates(Model.Type + "_StandardizedTestTemplates2", new { @class = "Templates", @template = "#" + Model.Type + "_GenericStandardizedTestOther1" })%>
                <%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther1" })%></div>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="5">Care Coordination
            <%= string.Format("<input class='radio' id='{0}IsCareApply' name='{0}_IsCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Skilled Care Provided This Visit
            <%= string.Format("<input class='radio' id='{0}IsSkilledCareApply' name='{0}_IsSkilledCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSkilledCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>CareContainer">
            <%= Html.Templates(Model.Type + "_GenericCareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCareCoordination" })%>
            <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%></div></td>
            <td colspan="5"><div id="<%=Model.Type %>SkilledCareContainer">
            <%= Html.Templates(Model.Type + "_GenericSkilledCareTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSkilledCareProvided" })%>
            <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.ContainsKey("GenericSkilledCareProvided") ? data["GenericSkilledCareProvided"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%></div></td>
        </tr>  
        <tr>
            <th colspan="10">Notification</th>
        </tr>
        <tr>
            <td colspan="10">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev1.ascx", Model); %>
            </td>
        </tr>     
    </tbody>
</table>

<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>IsVitalSignsApply"), $("#<%= Model.Type %>VitalSignsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMentalApply"), $("#<%= Model.Type %>MentalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMedicalApply"), $("#<%= Model.Type %>MedicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPainApply"), $("#<%= Model.Type %>PainContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsDMEApply"), $("#<%= Model.Type %>DMEContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLivingApply"), $("#<%= Model.Type %>LivingContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalApply"), $("#<%= Model.Type %>PhysicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLOFApply"), $("#<%= Model.Type %>LOFContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalAssessmentApply"), $("#<%= Model.Type %>PhysicalAssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsHomeboundApply"), $("#<%= Model.Type %>HomeboundContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPTRApply"), $("#<%= Model.Type %>PTRContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsBedApply"), $("#<%= Model.Type %>BedContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGaitApply"), $("#<%= Model.Type %>GaitContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTransferApply"), $("#<%= Model.Type %>TransferContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsWBApply"), $("#<%= Model.Type %>WBContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsAssessmentApply"), $("#<%= Model.Type %>AssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTreatmentApply"), $("#<%= Model.Type %>TreatmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGoalsApply"), $("#<%= Model.Type %>GoalsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRecommendationApply"), $("#<%= Model.Type %>RecommendationContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsCareApply"), $("#<%= Model.Type %>CareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsDCPlanApply"), $("#<%= Model.Type %>DCPlanContainer"));
</script>