﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %><input name="HHACarePlan_IsVitalSignParameter" value=" " type="hidden" />
<table class="fixed nursing"> 
    <tr>
        <th colspan="4">Vital Sign Parameters &#8212; <%= string.Format("<input class='radio' id='HHACarePlan_IsVitalSignParameter' name='HHACarePlan_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1") ? "checked='checked'" : "")%><label for="HHACarePlan_IsVitalSignParameter">N/A</label></th></tr>
    <tr class="vitalsigns"><td colspan="4">
        <table class="fixed"><tbody>
            <tr><th></th><th>SBP</th><th>DBP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th></tr>
            <tr>
                <th>greater than (&#62;)</th>
                <td><%= Html.TextBox("HHACarePlan_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_SystolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_DiastolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_PulseGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_RespirationGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_TempGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = "HHACarePlan_WeightGreaterThan", @class = "fill" })%></td>
            </tr><tr>
                <th>less than (&#60;)</th>
                <td><%= Html.TextBox("HHACarePlan_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_SystolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_DiastolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_PulseLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_RespirationLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_TempLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox("HHACarePlan_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = "HHACarePlan_WeightLessThan", @class = "fill" })%></td>
            </tr>
        </tbody></table>
    </td></tr>
    <tr><th colspan="4">Safety Precautions</th></tr>
    <tr><td colspan="4"><%string[] safetyMeasure = data.ContainsKey("SafetyMeasures") && data["SafetyMeasures"].Answer != "" ? data["SafetyMeasures"].Answer.Split(',') : null; %><input type="hidden" name="HHACarePlan_SafetyMeasures" value="" />
        <table class="fixed align-left"><tbody>
            <tr>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures1' name='HHACarePlan_SafetyMeasures' value='1' {0} />", safetyMeasure != null && safetyMeasure.Contains("1") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures1" class="radio">Anticoagulant Precautions</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures5' name='HHACarePlan_SafetyMeasures' value='5' {0} />", safetyMeasure != null && safetyMeasure.Contains("5") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures5" class="radio">Keep Side Rails Up</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures9' name='HHACarePlan_SafetyMeasures' value='9' {0} />", safetyMeasure != null && safetyMeasure.Contains("9") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures9" class="radio">Safety in ADLs</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures13' name='HHACarePlan_SafetyMeasures' value='13' {0} />", safetyMeasure != null && safetyMeasure.Contains("13") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label></td>
            </tr><tr>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures2' name='HHACarePlan_SafetyMeasures' value='2' {0} />", safetyMeasure != null && safetyMeasure.Contains("2") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures2" class="radio">Emergency Plan Developed</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures6' name='HHACarePlan_SafetyMeasures' value='6' {0} />", safetyMeasure != null && safetyMeasure.Contains("6") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures6" class="radio">Neutropenic Precautions</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures10' name='HHACarePlan_SafetyMeasures' value='10' {0} />", safetyMeasure != null && safetyMeasure.Contains("10") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures10" class="radio">Seizure Precautions</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures14' name='HHACarePlan_SafetyMeasures' value='14' {0} />", safetyMeasure != null && safetyMeasure.Contains("14") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label></td>
            </tr><tr>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures3' name='HHACarePlan_SafetyMeasures' value='3' {0} />", safetyMeasure != null && safetyMeasure.Contains("3") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures3" class="radio">Fall Precautions</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures7' name='HHACarePlan_SafetyMeasures' value='7' {0} />", safetyMeasure != null && safetyMeasure.Contains("7") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures7" class="radio">O<sub>2</sub> Precautions</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures11' name='HHACarePlan_SafetyMeasures' value='11' {0} />", safetyMeasure != null && safetyMeasure.Contains("11") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures11" class="radio">Sharps Safety</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures15' name='HHACarePlan_SafetyMeasures' value='15' {0} />", safetyMeasure != null && safetyMeasure.Contains("15") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures15" class="radio">Use of Assistive Devices</label></td>
            </tr><tr>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures4' name='HHACarePlan_SafetyMeasures' value='4' {0} />", safetyMeasure != null && safetyMeasure.Contains("4") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures4" class="radio">Keep Pathway Clear</label></td>
                <td><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures8' name='HHACarePlan_SafetyMeasures' value='8' {0} />", safetyMeasure != null && safetyMeasure.Contains("8") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures8" class="radio">Proper Position During Meals</label></td>
                <td colspan="2"><%= string.Format("<input class='radio float-left' type='checkbox' id='HHACarePlan_SafetyMeasures12' name='HHACarePlan_SafetyMeasures' value='12' {0} />", safetyMeasure != null && safetyMeasure.Contains("12") ? "checked='checked'" : "")%><label for="HHACarePlan_SafetyMeasures12" class="radio">Slow Position Change</label></td>
            </tr><tr>
                <td colspan="4" class="align-center"><label class="float-left">Other (Specify):</label><%= Html.TextArea("HHACarePlan_OtherSafetyMeasures", data.ContainsKey("OtherSafetyMeasures") ? data["OtherSafetyMeasures"].Answer : string.Empty, new { @id = "HHACarePlan_OtherSafetyMeasures", @class = "fill" })%></td>
            </tr>
        </tbody></table>
    </td></tr>
    <tr><th colspan="2">Functional Limitations</th><th colspan="2">Activities Permitted</th></tr>
    <tr>
        <td colspan="2">
            <table class="fixed align-left"><tbody>
                <tr>
                    <td><% string[] functionLimitations = data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null; %><input name="HHACarePlan_FunctionLimitations" value=" " type="hidden" />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations1' name='HHACarePlan_FunctionLimitations' value='1' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations1" class="radio">Amputation</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations2' name='HHACarePlan_FunctionLimitations' value='2' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations3' name='HHACarePlan_FunctionLimitations' value='3' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations3" class="radio">Contracture</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations4' name='HHACarePlan_FunctionLimitations' value='4' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations4" class="radio">Hearing</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations5' name='HHACarePlan_FunctionLimitations' value='5' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations5" class="radio">Paralysis</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitationsB' name='HHACarePlan_FunctionLimitations' value='B' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitationsB" class="radio">Other</label><%= Html.TextBox("HHACarePlan_FunctionLimitationsOther", data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : string.Empty, new { @id = "HHACarePlan_FunctionLimitationsOther" })%>
                    </td><td>
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations6' name='HHACarePlan_FunctionLimitations' value='6' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations6" class="radio">Endurance</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations7' name='HHACarePlan_FunctionLimitations' value='7' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations7" class="radio">Ambulation</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations8' name='HHACarePlan_FunctionLimitations' value='8' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations8" class="radio">Speech</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitations9' name='HHACarePlan_FunctionLimitations' value='9' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitations9" class="radio">Legally Blind</label><br />
                        <%= string.Format("<input id='HHACarePlan_FunctionLimitationsA' name='HHACarePlan_FunctionLimitations' value='A' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "" ) %><label for="HHACarePlan_FunctionLimitationsA" class="radio">Dyspnea with Minimal Exertion</label>
                    </td>
                </tr>                    
            </tbody></table>
        </td><td colspan="2">
            <table class="fixed align-left"><tbody>
                <tr>
                    <td><% string[] activitiesPermitted = data.ContainsKey("ActivitiesPermitted") && data["ActivitiesPermitted"].Answer != "" ? data["ActivitiesPermitted"].Answer.Split(',') : null; %><input type="hidden" name="HHACarePlan_ActivitiesPermitted" value="" />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted1' name='HHACarePlan_ActivitiesPermitted' value='1' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("1") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted1" class="radio">Complete bed rest</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted2' name='HHACarePlan_ActivitiesPermitted' value='2' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("2") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted2" class="radio">Bed rest with BRP</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted3' name='HHACarePlan_ActivitiesPermitted' value='3' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("3") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted3" class="radio">Up as tolerated</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted4' name='HHACarePlan_ActivitiesPermitted' value='4' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("4") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted4" class="radio">Transfer bed-chair</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted5' name='HHACarePlan_ActivitiesPermitted' value='5' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("5") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted5" class="radio">Exercise prescribed</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted6' name='HHACarePlan_ActivitiesPermitted' value='6' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("6") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted6" class="radio">Partial weight bearing</label>
                    </td><td>
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted7' name='HHACarePlan_ActivitiesPermitted' value='7' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("7") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted7" class="radio">Independent at home</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted8' name='HHACarePlan_ActivitiesPermitted' value='8' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("8") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted8" class="radio">Crutches</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted9' name='HHACarePlan_ActivitiesPermitted' value='9' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("9") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermitted9" class="radio">Cane</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted10' name='HHACarePlan_ActivitiesPermitted' value='10' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("10") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermittedA" class="radio">Wheelchair</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted11' name='HHACarePlan_ActivitiesPermitted' value='11' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("11") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermittedB" class="radio">Walker</label><br />
                        <%= string.Format("<input id='HHACarePlan_ActivitiesPermitted12' name='HHACarePlan_ActivitiesPermitted' value='12' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("12") ? "checked='checked'" : "")%><label for="HHACarePlan_ActivitiesPermittedOther">Other:</label><%= Html.TextBox("HHACarePlan_ActivitiesPermittedOther", data.ContainsKey("ActivitiesPermittedOther") ? data["ActivitiesPermittedOther"].Answer : string.Empty, new { @id = "HHACarePlan_ActivitiesPermittedOther" })%>
                    </td>
                </tr>                    
            </tbody></table>
        </td>
    </tr>
    <tr><th colspan="4">Plan Details<em>* QV = Every Visit; QW = Every Week; PR = Patient Request; N/A = Not Applicable</em></th></tr>
    <tr>
        <td colspan="2">
            <table class="fixed">
                <thead>
                    <tr><th colspan="4">Assignment</th><th colspan="4">Status</th></tr>
                    <tr><th colspan="4">Vital Signs</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th></tr>
                    <tr>
                        <td colspan="4">Temperature</td>
                        <td><%= Html.Hidden("HHACarePlan_VitalSignsTemperature", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_VitalSignsTemperature", "3", data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsTemperature", "2", data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsTemperature", "1", data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsTemperature", "0", data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Blood Pressure</td>
                        <td><%= Html.Hidden("HHACarePlan_VitalSignsBloodPressure", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_VitalSignsBloodPressure", "3", data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsBloodPressure", "2", data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsBloodPressure", "1", data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsBloodPressure", "0", data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Heart Rate</td>
                        <td><%= Html.Hidden("HHACarePlan_VitalSignsHeartRate", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_VitalSignsHeartRate", "3", data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsHeartRate", "2", data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsHeartRate", "1", data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsHeartRate", "0", data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Respirations</td>
                        <td><%= Html.Hidden("HHACarePlan_VitalSignsRespirations", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_VitalSignsRespirations", "3", data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsRespirations", "2", data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsRespirations", "1", data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsRespirations", "0", data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Weight</td>
                        <td><%= Html.Hidden("HHACarePlan_VitalSignsWeight", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_VitalSignsWeight", "3", data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsWeight", "2", data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsWeight", "1", data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_VitalSignsWeight", "0", data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <th colspan="4">Personal Care</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th>
                    </tr><tr>
                        <td colspan="4">Bed Bath</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareBedBath", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareBedBath", "3", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareBedBath", "2", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareBedBath", "1", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareBedBath", "0", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with Chair Bath</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareAssistWithChairBath", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithChairBath", "3", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithChairBath", "2", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithChairBath", "1", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithChairBath", "0", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Tub Bath</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareTubBath", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareTubBath", "3", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareTubBath", "2", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareTubBath", "1", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareTubBath", "0", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Shower</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareShower", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareShower", "3", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShower", "2", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShower", "1", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShower", "0", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Shower w/Chair</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareShowerWithChair", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareShowerWithChair", "3", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShowerWithChair", "2", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShowerWithChair", "1", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShowerWithChair", "0", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Shampoo Hair</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareShampooHair", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareShampooHair", "3", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShampooHair", "2", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShampooHair", "1", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShampooHair", "0", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Hair Care/Comb Hair</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareHairCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareHairCare", "3", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareHairCare", "2", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareHairCare", "1", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareHairCare", "0", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Oral Care</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareOralCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareOralCare", "3", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareOralCare", "2", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareOralCare", "1", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareOralCare", "0", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Skin Care</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareSkinCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareSkinCare", "3", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareSkinCare", "2", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareSkinCare", "1", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareSkinCare", "0", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Pericare</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCarePericare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCarePericare", "3", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCarePericare", "2", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCarePericare", "1", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCarePericare", "0", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Nail Care</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareNailCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareNailCare", "3", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareNailCare", "2", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareNailCare", "1", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareNailCare", "0", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Shave</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareShave", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareShave", "3", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShave", "2", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShave", "1", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareShave", "0", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with Dressing</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareAssistWithDressing", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithDressing", "3", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithDressing", "2", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithDressing", "1", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareAssistWithDressing", "0", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Medication Reminder</td>
                        <td><%= Html.Hidden("HHACarePlan_PersonalCareMedicationReminder", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_PersonalCareMedicationReminder", "3", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareMedicationReminder", "2", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareMedicationReminder", "1", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_PersonalCareMedicationReminder", "0", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr>
                </thead>
            </table>
        </td><td colspan="2">
            <table class="fixed">
                <thead>
                    <tr><th colspan="4">Assignment</th><th colspan="4">Status</th></tr>
                    <tr><th colspan="4">Elimination</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th></tr>
                    <tr>
                        <td colspan="4">Assist with Bed Pan/Urinal</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationAssistWithBedPan", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationAssistWithBedPan", "3", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistWithBedPan", "2", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistWithBedPan", "1", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistWithBedPan", "0", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with BSC</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationAssistBSC", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationAssistBSC", "3", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistBSC", "2", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistBSC", "1", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationAssistBSC", "0", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Incontinence Care</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationIncontinenceCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationIncontinenceCare", "3", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationIncontinenceCare", "2", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationIncontinenceCare", "1", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationIncontinenceCare", "0", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Empty Drainage Bag</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationEmptyDrainageBag", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationEmptyDrainageBag", "3", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationEmptyDrainageBag", "2", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationEmptyDrainageBag", "1", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationEmptyDrainageBag", "0", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Record Bowel Movement</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationRecordBowelMovement", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationRecordBowelMovement", "3", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationRecordBowelMovement", "2", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationRecordBowelMovement", "1", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationRecordBowelMovement", "0", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Catheter Care</td>
                        <td><%= Html.Hidden("HHACarePlan_EliminationCatheterCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_EliminationCatheterCare", "3", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationCatheterCare", "2", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationCatheterCare", "1", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_EliminationCatheterCare", "0", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <th colspan="4">Activity</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th>
                    </tr><tr>
                        <td colspan="4">Dangle on Side of Bed</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityDangleOnSideOfBed", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityDangleOnSideOfBed", "3", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityDangleOnSideOfBed", "2", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityDangleOnSideOfBed", "1", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityDangleOnSideOfBed", "0", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Turn &#38; Position</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityTurnPosition", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityTurnPosition", "3", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityTurnPosition", "2", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityTurnPosition", "1", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityTurnPosition", "0", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with Transfer</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityAssistWithTransfer", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityAssistWithTransfer", "3", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithTransfer", "2", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithTransfer", "1", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithTransfer", "0", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Range of Motion</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityRangeOfMotion", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityRangeOfMotion", "3", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityRangeOfMotion", "2", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityRangeOfMotion", "1", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityRangeOfMotion", "0", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with Ambulation</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityAssistWithAmbulation", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityAssistWithAmbulation", "3", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithAmbulation", "2", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithAmbulation", "1", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityAssistWithAmbulation", "0", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Equipment Care</td>
                        <td><%= Html.Hidden("HHACarePlan_ActivityEquipmentCare", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_ActivityEquipmentCare", "3", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityEquipmentCare", "2", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityEquipmentCare", "1", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_ActivityEquipmentCare", "0", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <th colspan="4">Household Task</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th>
                    </tr><tr>
                        <td colspan="4">Make Bed</td>
                        <td><%= Html.Hidden("HHACarePlan_HouseholdTaskMakeBed", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_HouseholdTaskMakeBed", "3", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskMakeBed", "2", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskMakeBed", "1", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskMakeBed", "0", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Change Linen</td>
                        <td><%= Html.Hidden("HHACarePlan_HouseholdTaskChangeLinen", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_HouseholdTaskChangeLinen", "3", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskChangeLinen", "2", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskChangeLinen", "1", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskChangeLinen", "0", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Light Housekeeping</td>
                        <td><%= Html.Hidden("HHACarePlan_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_HouseholdTaskLightHousekeeping", "3", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskLightHousekeeping", "2", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskLightHousekeeping", "1", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_HouseholdTaskLightHousekeeping", "0", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <th colspan="4">Nutrition</th><th>QV</th><th>QW</th><th>PR</th><th>N/A</th>
                    </tr><tr>
                        <td colspan="4">Meal Set-up</td>
                        <td><%= Html.Hidden("HHACarePlan_NutritionMealSetUp", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_NutritionMealSetUp", "3", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritionMealSetUp", "2", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritionMealSetUp", "1", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritionMealSetUp", "0", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr><tr>
                        <td colspan="4">Assist with Feeding</td>
                        <td><%= Html.Hidden("HHACarePlan_NutritioAssistWithFeeding", " ", new { @id = "" })%><%= Html.RadioButton("HHACarePlan_NutritioAssistWithFeeding", "3", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "3" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritioAssistWithFeeding", "2", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritioAssistWithFeeding", "1", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                        <td><%= Html.RadioButton("HHACarePlan_NutritioAssistWithFeeding", "0", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
    <tr><th colspan="4">Comments/Additional Instructions</th></tr>
    <tr><td colspan="4"><%= Html.TextArea("HHACarePlan_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "HHACarePlan_Comment", @class = "fill" })%></td></tr>
    <tr><th colspan="4">Notifications</th></tr>
    <tr>
        <td colspan="4">
            <div class="requirednotification"><%= string.Format("<input id='HHACarePlan_ReviewedWithHHA' name='HHACarePlan_ReviewedWithHHA' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("ReviewedWithHHA") && data["ReviewedWithHHA"] != null && data["ReviewedWithHHA"].Answer.IsNotNullOrEmpty() && data["ReviewedWithHHA"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%>
            <label for="HHACarePlan_ReviewedWithHHA" class="radio">Reviewed with Home Health Aide</label></div>
            <div class="requirednotification"><%= string.Format("<input id='HHACarePlan_ReviewedWithPatient' name='HHACarePlan_ReviewedWithPatient' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("ReviewedWithPatient") && data["ReviewedWithPatient"] != null && data["ReviewedWithPatient"].Answer.IsNotNullOrEmpty() && data["ReviewedWithPatient"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%>
            <label for="HHACarePlan_ReviewedWithPatient" class="radio">Patient oriented with Care Plan</label></div>
        </td>
    </tr>
</table>