﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>TOB</th>
				<th>Episode</th>
			</tr>
        </thead>
	    <tbody id="claimsWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div  id="claimsWidgetMore" > <div class="widget-more float-right" style="right:190px" > <a href="javascript:void(0);" onclick="UserInterface.ShowBillingCenterRAP();">more RAPs &#187;</a></div> <div  class="widget-more float-right" style="right:100px"><a href="javascript:void(0);" onclick="UserInterface.ShowBillingCenterFinal();">More Finals &#187;</a></div><div  class="widget-more float-right"><a href="javascript:void(0);" onclick="UserInterface.ShowBillingCenterAllClaim();">All Claims &#187;</a></div></div>