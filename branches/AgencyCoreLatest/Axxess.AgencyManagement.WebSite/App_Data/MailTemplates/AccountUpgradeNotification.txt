﻿<html>
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <style type="text/css" media="screen">
		    p {
			    margin: 0 0 10px 0;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
			    font-family: arial, sans-serif;
			    font-size: 13px;
		    }

		    body, div, td, th, textarea, input, h2, h3 {
			    font-family: arial, sans-serif;
			    font-size: 12px;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
		    }
		    ol, ul {
 	            list-style: none;
 	            margin: 0;
            }
	    </style>
	</head>
	<body>
		<p>Accounting,</p>
		<p><a href="mailto:<%=authorizedemail%>"><%=authorizeduser%></a>, an authorized user of <%=agencyname%> has requested a subscription plan change.</p>
		<p><strong>Old subscription Plan: <%=previouspackage%></strong></p>
		<p><strong>New Subscription Plan: <%=requestedpackage%></strong></p>
		<p><strong>Current Pricing: <%=currentpricing%></strong></p>
		<p><strong>New Pricing: <%=newpricing%></strong></p>
		<p>Please update our payment systems to reflect the subscription plan change above.</p>
		<p><%=authorizeduser%> made the comments below:</p>
		<hr />
		<p><%=comments%></p>
		<hr />
		<p style="font-family: arial, sans-serif; font-size: 11px;">This is an automated e-mail, please do not reply.</p>
		<p style="font-family: arial, sans-serif; font-size: 11px;">This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>