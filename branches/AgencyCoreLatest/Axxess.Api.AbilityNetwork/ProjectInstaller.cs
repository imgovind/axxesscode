﻿namespace Axxess.Api.AbilityNetwork
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller abilityNetworkInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.abilityNetworkInstaller = new ServiceInstaller();
            this.abilityNetworkInstaller.StartType = ServiceStartMode.Automatic;
            this.abilityNetworkInstaller.ServiceName = "AbilityNetworkService";
            this.abilityNetworkInstaller.DisplayName = "AbilityNetwork Service";
            this.abilityNetworkInstaller.Description = "Provides DDE Information through AbilityNetwork ACCESS API.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.abilityNetworkInstaller });
        }
    }
}
