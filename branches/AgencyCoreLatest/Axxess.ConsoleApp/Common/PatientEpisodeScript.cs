﻿namespace Axxess.ConsoleApp
{
    using System;
    using System.Text;

    public class PatientEpisodeScript
    {
        private PatientData patientData;
        private const string EPISODE_INSERT = "INSERT INTO `patientepisodes`(`Id`, `AgencyId`, `PatientId`, `Details`, `StartDate`, `EndDate`," +
            "`Schedule`, `Created`, `Modified`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8});";

        public const string RAP_INSERT = "INSERT INTO `raps`(`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`PatientIdNumber`,`EpisodeStartDate`, " +
            "`EpisodeEndDate`,`IsOasisComplete`,`IsFirstBillableVisit`,`FirstBillableVisitDate`,`MedicareNumber`,`Modified`,`Created`,`FirstName`,`LastName`," +
            "`DOB`,`Gender`,`AddressLine1`,`AddressLine2`,`AddressCity`,`AddressStateCode`,`AddressZipCode`,`StartofCareDate`,`DiagnosisCode`," +
            "`HippsCode`,`ClaimKey`,`PrimaryInsuranceId`,`Status`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8}, '{9}', '{10}', {11}, {12}, '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', {26}, {27});";

        public const string FINAL_INSERT = "INSERT INTO `finals`(`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`PatientIdNumber`,`EpisodeStartDate`," +
            "`EpisodeEndDate`,`FirstBillableVisitDate`,`MedicareNumber`,`FirstName`,`LastName`,`DOB`,`Gender`," +
            "`AddressLine1`,`AddressLine2`,`AddressCity`,`AddressStateCode`,`AddressZipCode`,`StartofCareDate`," +
            "`DiagnosisCode`,`HippsCode`,`ClaimKey`,`Modified`,`Created`,`PrimaryInsuranceId`,`Status`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', {22}, {23}, {24}, {25});";

        public PatientEpisodeScript(PatientData patientData)
        {
            this.patientData = patientData;
            this.CreateRapAndFinal = false;
        }

        public bool CreateRapAndFinal { get; set; }

        public override string ToString()
        {
            if (CreateRapAndFinal)
            {
                var scriptBuilder = new StringBuilder();
                scriptBuilder.AppendFormat(EPISODE_INSERT, patientData.EpisodeId, patientData.AgencyId, patientData.PatientId, patientData.EpisodeDetails,
                    patientData.EpisodeStart, patientData.EpisodeEnd, patientData.EpisodeSchedule, patientData.Created, patientData.Modified);
                scriptBuilder.AppendLine();
                scriptBuilder.AppendFormat(RAP_INSERT, patientData.EpisodeId, patientData.AgencyId, patientData.PatientId, patientData.EpisodeId, patientData.PatientNumber, patientData.EpisodeStart,
                    patientData.EpisodeEnd, 0, 0, patientData.FirstBillableDate, patientData.MedicareNumber, patientData.Modified, patientData.Created, patientData.FirstName, patientData.LastName, patientData.BirthDate, patientData.Gender,
                    patientData.AddressLine1, patientData.AddressLine2, patientData.AddressCity, patientData.AddressState, patientData.AddressZipCode, patientData.StartofCareDate, patientData.Diagnosis, patientData.HippsCode, patientData.ClaimKey,
                    0, patientData.ClaimStatusId);
                scriptBuilder.AppendLine();
                scriptBuilder.AppendFormat(FINAL_INSERT, patientData.EpisodeId, patientData.AgencyId, patientData.PatientId, patientData.EpisodeId, patientData.PatientNumber, patientData.EpisodeStart,
                    patientData.EpisodeEnd, patientData.FirstBillableDate, patientData.MedicareNumber, patientData.FirstName, patientData.LastName, patientData.BirthDate, patientData.Gender,
                    patientData.AddressLine1, patientData.AddressLine2, patientData.AddressCity, patientData.AddressState, patientData.AddressZipCode, patientData.StartofCareDate, patientData.Diagnosis, patientData.HippsCode, patientData.ClaimKey,
                    patientData.Modified, patientData.Created, 0, patientData.ClaimStatusId);

                return scriptBuilder.ToString();
            }

            return string.Format(EPISODE_INSERT, patientData.EpisodeId, patientData.AgencyId, patientData.PatientId, patientData.EpisodeDetails,
                patientData.EpisodeStart, patientData.EpisodeEnd, patientData.EpisodeSchedule, patientData.Created, patientData.Modified);
        }
    }
}
