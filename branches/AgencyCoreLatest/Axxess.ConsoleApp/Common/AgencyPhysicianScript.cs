﻿namespace Axxess.ConsoleApp
{
    public class AgencyPhysicianScript
    {
        private AgencyPhysicianData physicianData;
        private const string AGENCY_PHYSICIAN_INSERT = "INSERT INTO `agencyphysicians`(`Id`,`AgencyId`,`LoginId`,`NPI`," +
            "`FirstName`,`LastName`,`MiddleName`,`Credentials`,`AddressLine1`,`AddressLine2`,`AddressCity`,`AddressStateCode`,`AddressZipCode`," +
            "`PhoneWork`,`FaxNumber`,`Created`,`Modified`, `LicenseNumber`, `Comments`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', {15}, {16}, '{17}', '{18}');";

        public AgencyPhysicianScript(AgencyPhysicianData physicianData)
        {
            this.physicianData = physicianData;
        }

        public override string ToString()
        {
            return string.Format(AGENCY_PHYSICIAN_INSERT, physicianData.Id, physicianData.AgencyId, physicianData.LoginId, physicianData.NPI,
                physicianData.FirstName.Replace("'", "\\'"), physicianData.LastName.Replace("'", "\\'"), physicianData.MiddleName, physicianData.Credentials, physicianData.AddressLine1.Replace("'", "\\'"),
                physicianData.AddressLine2, physicianData.AddressCity, physicianData.AddressState, physicianData.AddressZipCode,
                physicianData.PhoneWork, physicianData.FaxNumber, physicianData.Created, physicianData.Modified, physicianData.LicenseNumber, physicianData.Comments);
        }
    }
}
