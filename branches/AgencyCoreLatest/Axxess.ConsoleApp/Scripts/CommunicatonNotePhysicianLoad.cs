﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;


    public static class CommunicatonNotePhysicianLoad
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\CommunicationNotes_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static readonly IPatientRepository patientRepository = new PatientRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        private static readonly IPhysicianRepository physicianRepository = new PhysicianRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        public static void Run()
        {
            var communicationNotes = patientRepository.GetAllCommunicationNotes();
            if (communicationNotes != null && communicationNotes.Count > 0)
            {
                using (var textWriter = new StreamWriter(output, true))
                {
                    foreach (var note in communicationNotes)
                    {
                        if (!note.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(note.PhysicianId, note.AgencyId);
                            if (physician != null)
                            {
                                var xmlData = new StringBuilder(physician.ToXml());
                                var data = xmlData.Replace("'", @"\'");

                                textWriter.WriteLine(string.Format("UPDATE `communicationnotes` SET `PhysicianData` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;", data, note.Id, note.AgencyId));
                                textWriter.Write(textWriter.NewLine);

                            }
                        }
                    }
                }
            }
        }
    }
}
