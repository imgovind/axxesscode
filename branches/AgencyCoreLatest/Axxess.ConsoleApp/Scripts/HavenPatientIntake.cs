﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Axxess.Core.Extension;

    public static class HavenPatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\siercam.xlsx");
        private static string insertoutput = Path.Combine(App.Root, string.Format("Files\\siercam_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(insertoutput, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int i = 1;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var patientData = new PatientData();
                                    patientData.AgencyId = "e1b39dc7-347b-46c9-949e-5f51cae7d8ed";
                                    patientData.AgencyLocationId = "0a9f4dd7-97d9-4515-8a74-205bb138e1f1";

                                    patientData.PatientStatusId = "1";
                                    patientData.PatientNumber = dataRow.GetValue(1);
                                    patientData.LastName = dataRow.GetValue(2).ToTitleCase();
                                    patientData.FirstName = dataRow.GetValue(3).ToTitleCase();
                                    patientData.MiddleInitial = dataRow.GetValue(4).ToTitleCase();
                                    patientData.AddressState = dataRow.GetValue(6);
                                    patientData.AddressZipCode = dataRow.GetValue(7);
                                    patientData.MedicareNumber = dataRow.GetValue(8);
                                    patientData.SSN = dataRow.GetValue(10);
                                    patientData.MedicaidNumber = dataRow.GetValue(12);
                                    patientData.BirthDate = dataRow.GetValue(15).ToMySqlDate(0);

                                    if (dataRow.GetValue(16).IsEqual("1"))
                                    {
                                        patientData.Gender = "Male";
                                    }
                                    else
                                    {
                                        patientData.Gender = "Female";
                                    }
                                    patientData.StartofCareDate = dataRow.GetValue(19).ToMySqlDate(0);
                                    patientData.Comments += string.Format("M0072_PHYSICIAN_ID: {0}. ", dataRow.GetValue(22));
                                    patientData.Comments += string.Format("M0140_ETHNIC: {0}. ", Convert.ToInt32(dataRow.GetValue(24), 2));
                                    patientData.Comments += string.Format("M0150_CPAY: {0}. ", Convert.ToInt32(dataRow.GetValue(25), 2));
                                    patientData.Comments += string.Format("M0150_CPAY_OTHER: {0}. ", dataRow.GetValue(26));
                                    patientData.Comments = patientData.Comments.Replace("'", "");

                                    textWriter.WriteLine(new PatientScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                    textWriter.Write(textWriter.NewLine);

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
