﻿using System;
using System.IO;
using System.Linq;
using System.Data;

using HtmlAgilityPack;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class VisiTrackPatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\everwellinactive.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\everwellinactive_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.Load(input);

                int counter = 1;
                PatientData patientData = null;
                htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                {
                    if (htmlNode.Name.IsEqual("html"))
                    {
                        htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                        {
                            if (bodyNode.Name.IsEqual("body"))
                            {
                                bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                {
                                    if (tableNode.IsDataRow())
                                    {
                                        int dataCounter = 1;
                                        if (counter % 3 == 1)
                                        {
                                            // Table 1
                                            patientData = new PatientData();
                                            patientData.AgencyId = "b6dbdfa2-9628-4ad2-821e-a2d4fb325590";
                                            patientData.AgencyLocationId = "92fd2e62-9f7a-47c7-b9fa-5469bdf1aa79";
                                            patientData.PatientStatusId = "2";
                                            tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                            {
                                                if (tRowNode.Name.IsEqual("tr"))
                                                {
                                                    tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                    {
                                                        if (dataNode.Name.IsEqual("td"))
                                                        {
                                                            Console.WriteLine("{0}: {1}", dataCounter, dataNode.InnerText);
                                                            dataNode.SetPatientData(1, dataCounter, patientData);
                                                            dataCounter++;
                                                        }
                                                    });
                                                }
                                            });
                                            Console.WriteLine("Table {0}", counter);
                                        }
                                        else
                                        {
                                            if (counter % 3 == 0)
                                            {
                                                // Table 3
                                                tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                {
                                                    if (tRowNode.Name.IsEqual("tr"))
                                                    {
                                                        tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                        {
                                                            if (dataNode.Name.IsEqual("td"))
                                                            {
                                                                Console.WriteLine("{0}: {1}", dataCounter, dataNode.InnerText);
                                                                dataNode.SetPatientData(3, dataCounter, patientData);
                                                                dataCounter++;
                                                            }
                                                        });
                                                    }
                                                });


                                                Console.WriteLine("Table {0}", counter);
                                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                                textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                                textWriter.Write(textWriter.NewLine);
                                            }
                                            else
                                            {
                                                // Table 2
                                                tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                {
                                                    if (tRowNode.Name.IsEqual("tr"))
                                                    {
                                                        tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                        {
                                                            if (dataNode.Name.IsEqual("td"))
                                                            {
                                                                Console.WriteLine("{0}: {1}", dataCounter, dataNode.InnerText);
                                                                dataNode.SetPatientData(2, dataCounter, patientData);
                                                                dataCounter++;
                                                            }
                                                        });
                                                    }
                                                });

                                                Console.WriteLine("Table {0}", counter);
                                            }
                                        }
                                        counter++;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }
}
