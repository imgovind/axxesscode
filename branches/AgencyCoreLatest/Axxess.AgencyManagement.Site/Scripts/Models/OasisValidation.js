﻿var OasisValidation = {
    _EpisodeId: "",
    GetEpisodeId: function() {
        return OasisValidation._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        OasisValidation._EpisodeId = EpisodeId;
    },
    //    Validate: function(id, assessmentType) {
    //        $("#validation").clearForm();
    //        var control = $('tbody', $("#validationErrorTable"));
    //        control.empty();
    //        var data = 'Id=' + id + "&episodeId=" + OasisValidation._EpisodeId + "&assessmentType=" + assessmentType;
    //        $.ajax({
    //            url: '/Oasis/Validate',
    //            type: 'POST',
    //            dataType: 'json',
    //            data: data,
    //            success: function(result) {
    //                $("#numberOfErrors").text('');
    //                $("#numberOfErrors").text(result.Count);
    //                if (result.Count == 0) {
    //                    control.append('<tr align="center"><td colspan="2" ><b>' + result.Message + '</b></td></tr>');
    //                }
    //                else if (result.Count > 0) {
    //                    $.each(result.validationError, function(index, itemData) {
    //                        control.append('<tr><td>' + itemData.ErrorDup + '</td><td>' + itemData.Description + '</td></tr>');
    //                    });
    //                }
    //            }
    //        });
    //    }
    //    ,
    Validate: function(id, assessmentType) {
        $('#validationResult').load('Oasis/Validate', { Id: id, episodeId: OasisValidation._EpisodeId, assessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#validationResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#validation');
            }
            else if (textStatus == "success") {
                JQD.open_window('#validation');
            }
        }
);
    }
}