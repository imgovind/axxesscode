﻿var Patient = {
    _patientId: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    GetId: function() {
        return Patient._patientId;
    },
    SetId: function(patientId) {
        Patient._patientId = patientId;
    },
    SetPatientRowIndex: function(patientRowIndex) {
        Patient._patientRowIndex = patientRowIndex;
    }
    ,
    GetEpisodeId: function() {
        return Patient._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        Patient._EpisodeId = EpisodeId;
    },
    CalculateGridHeight: function() {
        setTimeout(function() {
            $('#patient_Splitter1').children().height($('#patient_Splitter1').height() + 4);
            var gridContentHeight = $('#PatientLeftSide').height() - $('#PatientFilterContainer').height() - 25 - 20 + 7;

            if ($.browser.msie) {
                $('#PatientSelectionGrid').find(".t-grid-content").height(gridContentHeight);
            }
            else {
                $('#PatientSelectionGrid').find(".t-grid-content").height(gridContentHeight);
            }
        }, 500);
    },
    CalculateActivityGrid: function() {
        setTimeout(function() {
            var activityGridRow = $('#patientBottomPanel').height() - 219;
            $('#PatientActivityGrid').find(".t-grid-content").height(activityGridRow);
        }, 500);
    },
    Init: function() {


        jQuery.event.add(window, "load", function() {
            Patient.CalculateGridHeight();
            Patient.CalculateActivityGrid();
        });
        jQuery.event.add(window, "resize", function() {
            Patient.CalculateGridHeight();
            Patient.CalculateActivityGrid();
        });

        $("select.patientStatusDropDown").change(function() {
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });

        });

        $("select.patientPaymentDropDown").change(function() {
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });
        });

        $('#txtSearch_Patient_Selection').keypress(function() {
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });
        });

        $("select.patientActivityDropDown").change(function() {
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });

        if ($("select.patientActivityDateDropDown").val() == "DateRange") {
            $("#dateRangeText").hide();
            $("div.CustomDateRange").show();
        }
        else if ($("select.patientActivityDateDropDown").val() == "All") {
            $("#dateRangeText").hide();
            $("div.CustomDateRange").hide();
        }
        else {
            $("#dateRangeText").show();
            $("div.CustomDateRange").hide();
        }
        $("select.patientActivityDateDropDown").change(function() {

            if ($("select.patientActivityDateDropDown").val() == "DateRange") {
                $("#dateRangeText").hide();
                $("div.CustomDateRange").show();
            }
            else if ($("select.patientActivityDateDropDown").val() == "All") {
                Patient.DateRange($("select.patientActivityDateDropDown").val());
                $("#dateRangeText").hide();
                $("div.CustomDateRange").hide();
            }
            else {
                Patient.DateRange($("select.patientActivityDateDropDown").val());
                $("#dateRangeText").show();
                $("div.CustomDateRange").hide();
            }

            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });
        Patient.CalculateActivityGrid();
        $('#txtAdd_Referral_HomePhone1').autotab({ target: 'txtAdd_Referral_HomePhone2', format: 'numeric' });
        $('#txtAdd_Referral_HomePhone2').autotab({ target: 'txtAdd_Referral_HomePhone3', format: 'numeric', previous: 'txtAdd_Referral_HomePhone1' });
        $('#txtAdd_Referral_HomePhone3').autotab({ target: 'txtAdd_Referral_Email', format: 'numeric', previous: 'txtAdd_Referral_HomePhone2' });
        $('#txtAdd_Referral_PhysicianPhone1').autotab({ target: 'txtAdd_Referral_PhysicianPhone2', format: 'numeric' });
        $('#txtAdd_Referral_PhysicianPhone2').autotab({ target: 'txtAdd_Referral_PhysicianPhone3', format: 'numeric', previous: 'txtAdd_Referral_PhysicianPhone1' });
        $('#txtAdd_Referral_PhysicianPhone3').autotab({ target: 'txtAdd_Referral_PhysicianFax', format: 'numeric', previous: 'txtAdd_Referral_PhysicianPhone2' });

        $("#referralForm").click(function() {
            $("#patientValidaton").empty();
            $("#patientValidaton").hide();
        });

        $("#txtNew_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {

            $('tbody', $(this).closest('table')).clearForm();
            if (row) {
                $("#txtNew_Patient_FirstNamePhysicianContact").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#txtNew_Patient_LastNamePhysicianContact").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#txtNew_Patient_PhysicianContactNPINo").val(row.Id != null ? row.Id : '');
                $("#txtNew_Patient_PhysicianContactAddressLine1").val(row.ProviderFirstLineBusinessPracticeLocationAddress != null ? row.ProviderFirstLineBusinessPracticeLocationAddress : '');
                $("#txtNew_Patient_PhysicianContactAddressLine2").val(row.ProviderSecondLineBusinessPracticeLocationAddress != null ? row.ProviderSecondLineBusinessPracticeLocationAddress : '');
                $("#txtNew_Patient_PhysicianContactAddressCity").val(row.ProviderBusinessPracticeLocationAddressCityName != null ? row.ProviderBusinessPracticeLocationAddressCityName : '');
                $("#txtNew_Patient_PhysicianContactAddressStateCode").val(row.ProviderBusinessPracticeLocationAddressStateName != null ? row.ProviderBusinessPracticeLocationAddressStateName : '');
                if (row.ProviderBusinessPracticeLocationAddressPostalCode !== null) {
                    var zipCode = row.ProviderBusinessPracticeLocationAddressPostalCode;
                    $("#txtNew_Patient_PhysicianContactAddressZipCode").val(zipCode.substring(0, 5));
                }

                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                    $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                    $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#txtNew_Patient_PhysicianContactFax1").val(primaryPhone.substring(0, 3));
                    $("#txtNew_Patient_PhysicianContactFax2").val(primaryPhone.substring(3, 6));
                    $("#txtNew_Patient_PhysicianContactFax3").val(primaryPhone.substring(6, 10));
                }
            }
            $("#txtNew_Patient_PhysicianDropDown").val('0');
        });
        $("#txtNew_Patient_PhysicianDropDown").change(function() {

            var data = 'PhysicianContactId=' + $(this).val();
            $.ajax({
                url: '/Patient/GetPhysicianContact',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result) {
                    $('tbody', $(this).closest('table')).clearForm();
                    $("#txtNew_NpiNumber").val('');
                    var data = eval(result);
                    $("#txtNew_Patient_FirstNamePhysicianContact").val(data.FirstName != null ? data.FirstName : '');
                    $("#txtNew_Patient_LastNamePhysicianContact").val(data.LastName != null ? data.LastName : '');
                    $("#txtNew_Patient_PhysicianContactNPINo").val(data.NPI != null ? data.NPI : '');
                    $("#txtNew_Patient_PhysicianContactAddressLine1").val(data.MailingAddressLine1 != null ? data.MailingAddressLine1 : '');
                    $("#txtNew_Patient_PhysicianContactAddressLine2").val(data.MailingAddressLine2 != null ? data.MailingAddressLine2 : '');
                    $("#txtNew_Patient_PhysicianContactAddressCity").val(data.MailingAddressCity != null ? data.MailingAddressCity : '');
                    $("#txtNew_Patient_PhysicianContactAddressStateCode").val(data.MailingAddressStateCode != null ? data.MailingAddressStateCode : '');
                    if (data.MailingAddressZipCode !== null) {
                        var zipCode = data.MailingAddressZipCode;
                        $("#txtNew_Patient_PhysicianContactAddressZipCode").val(zipCode.substring(0, 5));
                    }

                    if (data.PhoneWork !== null) {
                        var primaryPhone = data.PhoneWork;
                        $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                        $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                        $("#txtNew_Patient_PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                    }
                    if (data.FaxNumber !== null) {
                        var primaryPhone = data.FaxNumber;
                        $("#txtNew_Patient_PhysicianContactFax1").val(primaryPhone.substring(0, 3));
                        $("#txtNew_Patient_PhysicianContactFax2").val(primaryPhone.substring(3, 6));
                        $("#txtNew_Patient_PhysicianContactFax3").val(primaryPhone.substring(6, 10));
                    }
                    $("#txtNew_Patient_PhysicianContactEmail").val(data.EmailAddress != null ? data.EmailAddress : '');
                }
            });

        });


        $("#txtNew_PhysicianContact_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $('tbody', $(this).closest('table')).clearForm();
                $("#txtNew_PhysicianContact_FirstName").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#txtNew_PhysicianContact_LastName").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#txtNew_PhysicianContact_NPINo").val(row.Id != null ? row.Id : '');
                $("#txtNew_PhysicianContact_AddressLine1").val(row.ProviderFirstLineBusinessPracticeLocationAddress != null ? row.ProviderFirstLineBusinessPracticeLocationAddress : '');
                $("#txtNew_PhysicianContact_AddressLine2").val(row.ProviderSecondLineBusinessPracticeLocationAddress != null ? row.ProviderSecondLineBusinessPracticeLocationAddress : '');
                $("#txtNew_PhysicianContact_AddressCity").val(row.ProviderBusinessPracticeLocationAddressCityName != null ? row.ProviderBusinessPracticeLocationAddressCityName : '');
                $("#txtNew_PhysicianContact_AddressStateCode").val(row.ProviderBusinessPracticeLocationAddressStateName != null ? row.ProviderBusinessPracticeLocationAddressStateName : '');
                if (row.ProviderBusinessPracticeLocationAddressPostalCode !== null) {
                    var zipCode = row.ProviderBusinessPracticeLocationAddressPostalCode;
                    $("#txtNew_PhysicianContact_AddressZipCode").val(zipCode.substring(0, 5));
                }
                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                    $("#PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                    $("#PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#PhysicianContactFaxArray1").val(primaryPhone.substring(0, 3));
                    $("#PhysicianContactFaxArray2").val(primaryPhone.substring(3, 6));
                    $("#PhysicianContactFaxArray3").val(primaryPhone.substring(6, 10));
                }
                $("#txtNew_PhysicianContact_PhysicianDropDown").val("0");
            }
        });

        $("#txtNew_PhysicianContact_PhysicianDropDown").change(function() {
            var data = 'PhysicianContactId=' + $(this).val();
            $.ajax({
                url: '/Patient/GetPhysicianContact',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result) {
                    $('tbody', $(this).closest('table')).clearForm();
                    $("#txtNew_PhysicianContact_NpiNumber").val('');
                    var data = eval(result);
                    $("#txtNew_PhysicianContact_FirstName").val(data.FirstName != null ? data.FirstName : '');
                    $("#txtNew_PhysicianContact_LastName").val(data.LastName != null ? data.LastName : '');
                    $("#txtNew_PhysicianContact_NPINo").val(data.NPI != null ? data.NPI : '');
                    $("#txtNew_PhysicianContact_AddressLine1").val(data.PracticeLocationAddressLine1 != null ? data.PracticeLocationAddressLine1 : '');
                    $("#txtNew_PhysicianContact_AddressLine2").val(data.PracticeLocationAddressLine2 != null ? data.PracticeLocationAddressLine2 : '');
                    $("#txtNew_PhysicianContact_AddressCity").val(data.PracticeLocationAddressCity != null ? data.PracticeLocationAddressCity : '');
                    $("#txtNew_PhysicianContact_AddressStateCode").val(data.PracticeLocationAddressStateCode != null ? data.PracticeLocationAddressStateCode : '');
                    if (data.PracticeLocationAddressZipCode !== null) {
                        var zipCode = data.PracticeLocationAddressZipCode;
                        $("#txtNew_PhysicianContact_AddressZipCode").val(zipCode.substring(0, 5));
                    }
                    if (data.PhoneWork !== null) {
                        var primaryPhone = data.PhoneWork;
                        $("#PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                        $("#PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                        $("#PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                    }
                    if (data.FaxNumber !== null) {
                        var primaryPhone = data.FaxNumber;
                        $("#PhysicianContactFaxArray1").val(primaryPhone.substring(0, 3));
                        $("#PhysicianContactFaxArray2").val(primaryPhone.substring(3, 6));
                        $("#PhysicianContactFaxArray3").val(primaryPhone.substring(6, 10));
                    }
                    $("#txtNew_PhysicianContact_Email").val(data.EmailAddress != null ? data.EmailAddress : '');
                }
            });

        });


        $("#txtAdmit_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $('tbody', $(this).closest('table')).clearForm();
                $("#txtAdmit_Patient_FirstNamePhysicianContact").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#txtAdmit_Patient_LastNamePhysicianContact").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#txtAdmit_Patient_PhysicianContactNPINo").val(row.Id != null ? row.Id : '');
                $("#txtAdmit_Patient_PhysicianContactAddressLine1").val(row.ProviderFirstLineBusinessPracticeLocationAddress != null ? row.ProviderFirstLineBusinessPracticeLocationAddress : '');
                $("#txtAdmit_Patient_PhysicianContactAddressLine2").val(row.ProviderSecondLineBusinessPracticeLocationAddress != null ? row.ProviderSecondLineBusinessPracticeLocationAddress : '');
                $("#txtAdmit_Patient_PhysicianContactAddressCity").val(row.ProviderBusinessPracticeLocationAddressCityName != null ? row.ProviderBusinessPracticeLocationAddressCityName : '');
                $("#txtAdmit_Patient_PhysicianContactAddressStateCode").val(row.ProviderBusinessPracticeLocationAddressStateName != null ? row.ProviderBusinessPracticeLocationAddressStateName : '');
                if (row.ProviderBusinessPracticeLocationAddressPostalCode !== null) {
                    var zipCode = row.ProviderBusinessPracticeLocationAddressPostalCode;
                    $("#txtAdmit_Patient_PhysicianContactAddressZipCode").val(zipCode.substring(0, 5));
                }
                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                    $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                    $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#txtAdmit_Patient_PhysicianContactFax1").val(primaryPhone.substring(0, 3));
                    $("#txtAdmit_Patient_PhysicianContactFax2").val(primaryPhone.substring(3, 6));
                    $("#txtAdmit_Patient_PhysicianContactFax3").val(primaryPhone.substring(6, 10));
                }
                $("#txtAdmit_Patient_PhysicianDropDown").val("0");
            }
        });


        $("#txtAdmit_Patient_PhysicianDropDown").change(function() {
            var data = 'PhysicianContactId=' + $(this).val();
            $.ajax({
                url: '/Patient/GetPhysicianContact',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result) {
                    $('tbody', $(this).closest('table')).clearForm();
                    $("#txtAdmit_NpiNumber").val('');
                    var data = eval(result);
                    $("#txtAdmit_Patient_FirstNamePhysicianContact").val(data.FirstName != null ? data.FirstName : '');
                    $("#txtAdmit_Patient_LastNamePhysicianContact").val(data.LastName != null ? data.LastName : '');
                    $("#txtAdmit_Patient_PhysicianContactNPINo").val(data.NPI != null ? data.NPI : '');
                    $("#txtAdmit_Patient_PhysicianContactAddressLine1").val(data.PracticeLocationAddressLine1 != null ? data.PracticeLocationAddressLine1 : '');
                    $("#txtAdmit_Patient_PhysicianContactAddressLine2").val(data.PracticeLocationAddressLine2 != null ? data.PracticeLocationAddressLine2 : '');
                    $("#txtAdmit_Patient_PhysicianContactAddressCity").val(data.PracticeLocationAddressCity != null ? data.PracticeLocationAddressCity : '');
                    $("#txtAdmit_Patient_PhysicianContactAddressStateCode").val(data.PracticeLocationAddressStateCode != null ? data.PracticeLocationAddressStateCode : '');
                    if (data.PracticeLocationAddressZipCode !== null) {
                        var zipCode = data.PracticeLocationAddressZipCode;
                        $("#txtAdmit_Patient_PhysicianContactAddressZipCode").val(zipCode.substring(0, 5));
                    }
                    if (data.PhoneWork !== null) {
                        var primaryPhone = data.PhoneWork;
                        $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray1").val(primaryPhone.substring(0, 3));
                        $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray2").val(primaryPhone.substring(3, 6));
                        $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray3").val(primaryPhone.substring(6, 10));
                    }
                    if (data.FaxNumber !== null) {
                        var primaryPhone = data.FaxNumber;
                        $("#txtAdmit_Patient_PhysicianContactFax1").val(primaryPhone.substring(0, 3));
                        $("#txtAdmit_Patient_PhysicianContactFax2").val(primaryPhone.substring(3, 6));
                        $("#txtAdmit_Patient_PhysicianContactFax3").val(primaryPhone.substring(6, 10));
                    }
                    $("#txtAdmit_Patient_PhysicianContactEmail").val(data.EmailAddress != null ? data.EmailAddress : '');
                }
            });

        });

        $("#AddNewPatient_PaymentSource").click(function() {
            var q = $('#AddNewPatient_PaymentSource:checked').is(':checked');
            if (!q) {
                $("#txtAdd_Patient_OtherPaymentSource").hide();
            }
            else {
                $("#txtAdd_Patient_OtherPaymentSource").show();
            }
        });


        $("#newPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newPatientValidaton").empty();
                            $("#newPatientValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>" + resultObject.errorMessage + "!</strong></li></ul></div>";
                            $("#newPatientValidaton").append(text);
                            $("#newPatientValidaton").show();
                            Patient.RebindPatient();
                            Patient.Close($("#newPatientForm"));
                        }
                        else {
                            $("#newPatientValidaton").empty();
                            $("#newPatientValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newPatientValidaton").append(text);
                            $("#newPatientValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $("#admitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#admitPatientValidaton").empty();
                            $("#admitPatientValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>" + resultObject.errorMessage + "!</strong></li></ul></div>";
                            $("#admitPatientValidaton").append(text);
                            $("#admitPatientValidaton").show();
                            Patient.RebindPatient();
                            Patient.Close($("#admitPatientValidaton"));
                        }
                        else {
                            $("#admitPatientValidaton").empty();
                            $("#admitPatientValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#admitPatientValidaton").append(text);
                            $("#admitPatientValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#newEmergencyContactForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        var resultObject = eval(result);

                        if (resultObject.isSuccessful) {
                            $("#newEmergencyContactValidaton").empty();
                            $("#newEmergencyContactValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#newEmergencyContactValidaton").append(text);
                            $("#newEmergencyContactValidaton").show();
                            Patient.Rebind();
                            Patient.Close($("#newEmergencyContactForm"));
                        }
                        else {
                            $("#newEmergencyContactValidaton").empty();
                            $("#newEmergencyContactValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newEmergencyContactValidaton").append(text);
                            $("#newEmergencyContactValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editEmergencyContactForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#editEmergencyContactValidaton").empty();
                            $("#editEmergencyContactValidaton").hide();
                            var text = "<div style=\"color:green;\"><ul class='system_messages'><li class='green sent' ><span class='ico'></span><strong class='system_title'>Your data is successfully saved</strong></li></ul></div>";
                            $("#editEmergencyContactValidaton").append(text);
                            $("#editEmergencyContactValidaton").show();
                            Patient.LoadPatientInfo();
                            Patient.Rebind();
                            Patient.Close($("#editEmergencyContactForm"));
                        }
                        else {
                            $("#editEmergencyContactValidaton").empty();
                            $("#editEmergencyContactValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editEmergencyContactValidaton").append(text);
                            $("#editEmergencyContactValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#newPhysicianContactForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newPhysicianContactValidaton").empty();
                            $("#newPhysicianContactValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#newPhysicianContactValidaton").append(text);
                            $("#newPhysicianContactValidaton").show();
                            Patient.Rebind();
                            Patient.Close($("#newPhysicianContactForm"));
                        }
                        else {
                            $("#newPhysicianContactValidaton").empty();
                            $("#newPhysicianContactValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newPhysicianContactValidaton").append(text);
                            $("#newPhysicianContactValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#newPhysicianForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newPhysicianContactValidaton").empty();
                            $("#newPhysicianContactValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#newPhysicianContactValidaton").append(text);
                            $("#newPhysicianContactValidaton").show();
                            Patient.Rebind();
                            Patient.Close($("#newPhysicianContactForm"));
                        }
                        else {
                            $("#newPhysicianContactValidaton").empty();
                            $("#newPhysicianContactValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newPhysicianContactValidaton").append(text);
                            $("#newPhysicianContactValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });


        $("#editPhysicianContactForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#editPhysicianContactValidaton").empty();
                            $("#editPhysicianContactValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#editPhysicianContactValidaton").append(text);
                            $("#editPhysicianContactValidaton").show();
                            Patient.LoadPatientInfo();
                            Patient.Rebind();
                            Patient.Close($("#editPhysicianContactForm"));
                        }
                        else {
                            $("#editPhysicianContactValidaton").empty();
                            $("#editPhysicianContactValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editPhysicianContactValidaton").append(text);
                            $("#editPhysicianContactValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {

                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#editPatientValidaton").empty();
                            $("#editPatientValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#editPatientValidaton").append(text);
                            $("#editPatientValidaton").show();
                            Patient.RebindPatient();
                            Patient.Close($("#editPatientForm"));
                        }
                        else {
                            $("#editPatientValidaton").empty();
                            $("#editPatientValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editPatientValidaton").append(text);
                            $("#editPatientValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });




        $("#editOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {


                    },
                    success: function(result) {

                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#editOrderValidaton").empty();
                            $("#editOrderValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>" + resultObject.errorMessage + "!</strong></li></ul></div>";
                            $("#editOrderValidaton").append(text);
                            $("#editOrderValidaton").show();
                            Patient.RebindActivity();
                            Patient.Close($("#editOrderForm"));
                        }
                        else {
                            $("#editOrderValidaton").empty();
                            $("#editOrderValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editOrderValidaton").append(text);
                            $("#editOrderValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });




        $("#editCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#editCommunicationNoteValidaton").empty();
                            $("#editCommunicationNoteValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                            $("#editCommunicationNoteValidaton").append(text);
                            $("#editCommunicationNoteValidaton").show();
                            Patient.RebindActivity();
                            Patient.Close($("#editCommunicationNoteForm"));
                        }
                        else {
                            $("#editCommunicationNoteValidaton").empty();
                            $("#editCommunicationNoteValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editCommunicationNoteValidaton").append(text);
                            $("#editCommunicationNoteValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#patientNoteForm").validate({

            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Patient.GetNote();
                            Patient.Close($("#patientNoteForm"));
                        }
                        else {

                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });


    }
    ,
    New: function() {
        $("#newPatientValidaton").empty();
        $("#newPatientValidaton").hide();
        $("#newPatientForm label.error").hide();
        $("#newPatientForm").clearForm();
        $("#txtNew_Patient_DOB").data("tDatePicker").value(new Date());
        $("#txtNew_Patient_StartOfCareDate").data("tDatePicker").value(new Date());
        $("#txtNew_Patient_EpisodeStartDate").data("tDatePicker").value(new Date());
        $("#txtNew_Patient_PatientReferralDate").data("tDatePicker").value(new Date());
    }
        ,
    LoadPatientEditInfo: function(id) {
        var data = 'id=' + id;
        $("#editPatientValidaton").empty();
        $("#editPatientValidaton").hide();
        $("#editPatientForm label.error").hide();
        $("#editPatientForm").clearForm();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getData(result, id);
            }
        });

        getData = function(data) {
            $("input[name='Id'][id='txtEdit_PatientID']").val(id.toString());
            $("#txtEdit_Patient_FirstName").val((data.FirstName !== null ? data.FirstName : " "));
            $("#txtEdit_Patient_MiddleInitial").val((data.MiddleInitial !== null ? data.MiddleInitial : " "));
            $("#txtEdit_Patient_LastName").val((data.LastName !== null ? data.LastName : " "));
            $('input[name=Gender][value=' + data.Gender.toString() + ']').attr('checked', true);
            $("#txtEdit_Patient_DOB").data("tDatePicker").value((data.DOB !== null ? (new Date(data.DOB)) : new Date()));
            $("#txtEdit_Patient_MaritalStatus").val((data.MaritalStatus !== null ? data.MaritalStatus : " "));
            $("#txtEdit_Patient_PatientID").val((data.PatientIdNumber !== null ? data.PatientIdNumber : " "));
            $("#txtEdit_Patient_MedicareNumber").val((data.MedicareNumber !== null ? data.MedicareNumber : " "));
            $("#txtEdit_Patient_MedicaidNumber").val((data.MedicaidNumber !== null ? data.MedicaidNumber : " "));
            $("#txtEdit_Patient_SSN").val((data.SSN !== null ? data.SSN : " "));
            $("#txtEdit_Patient_StartOfCareDate").data("tDatePicker").value(data.StartOfCareDate !== null ? (new Date(data.StartOfCareDate)) : new Date());

            if (data.EthnicRace !== null) {

                var EthnicRaceArray = (data.EthnicRace).split(';');
                var i = 0;
                for (i = 0; i < EthnicRaceArray.length; i++) {

                    $('input[name=EthnicRaces][value=' + EthnicRaceArray[i].toString() + ']').attr('checked', true);

                }
            }
            if (data.PaymentSource !== null) {

                var PaymentSourceArray = (data.PaymentSource).split(';');
                var i = 0;

                for (i = 0; i < PaymentSourceArray.length; i++) {

                    $('input[name=PaymentSources][value=' + PaymentSourceArray[i].toString() + ']').attr('checked', true);
                    if (PaymentSourceArray[i] == 12) {

                        $("#txtEdit_Patient_OtherPaymentSource").val((data.OtherPaymentSource !== null ? data.OtherPaymentSource : " ")).show();
                    }
                }
            }

            $("#txtEdit_Patient_AddressLine1").val((data.AddressLine1 !== null ? data.AddressLine1 : ""));
            $("#txtEdit_Patient_AddressLine2").val((data.AddressLine2 !== null ? data.AddressLine2 : ""));
            $("#txtEdit_Patient_AddressCity").val((data.AddressCity !== null ? data.AddressCity : ""));
            $("#txtEdit_Patient_AddressStateCode").val((data.AddressStateCode !== null ? data.AddressStateCode : ""));
            $("#txtEdit_Patient_AddressZipCode").val((data.AddressZipCode !== null ? data.AddressZipCode : ""));
            $("#txtEdit_Patient_HomePhone1").val((data.PhoneHome !== null ? data.PhoneHome.substring(0, 3) : ""));
            $("#txtEdit_Patient_HomePhone2").val((data.PhoneHome !== null ? data.PhoneHome.substring(3, 6) : ""));
            $("#txtEdit_Patient_HomePhone3").val((data.PhoneHome !== null ? data.PhoneHome.substring(6) : ""));
            $("#txtEdit_Patient_MobilePhone1").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(0, 3) : ""));
            $("#txtEdit_Patient_MobilePhone2").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(3, 6) : ""));
            $("#txtEdit_Patient_MobilePhone3").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(6) : ""));
            $("#txtEdit_Patient_Email").val((data.Email !== null ? data.Email : ""));
            $("#txtEdit_Patient_PharmacyName").val((data.PharmacyName !== null ? data.PharmacyName : ""));
            $("#txtEdit_Patient_PharmacyPhone1").val((data.PharmacyPhone !== null ? data.PharmacyPhone.substring(0, 3) : ""));
            $("#txtEdit_Patient_PharmacyPhone2").val((data.PharmacyPhone !== null ? data.PharmacyPhone.substring(3, 6) : ""));
            $("#txtEdit_Patient_PharmacyPhone3").val((data.PharmacyPhone !== null ? data.PharmacyPhone.substring(6) : ""));
            $("#txtEdit_Patient_PrimaryInsurance").val((data.PrimaryInsurance !== null ? data.PrimaryInsurance : ""));
            $("#txtEdit_Patient_SecondaryInsurance").val((data.SecondaryInsurance !== null ? data.SecondaryInsurance : ""));
            $("#txtEdit_Patient_TertiaryInsurance").val((data.TertiaryInsurance !== null ? data.TertiaryInsurance : ""));
            $("#txtEdit_Patient_ReferralPhysicianFirstName").val((data.ReferralPhysician !== null ? data.ReferralPhysician : ""));
            $("#txtEdit_Patient_AdmissionSource").val((data.AdmissionSource !== null ? data.AdmissionSource : ""));
            $("#txtEdit_Patient_PatientReferralDate").data("tDatePicker").value(data.PatientReferralDate !== null ? (new Date(data.PatientReferralDate)) : new Date());
            $("#txtEdit_Patient_OtherReferralSource").val((data.OtherReferralSource !== null ? data.OtherReferralSource : ""));
            $("#txtEdit_Patient_OtherSourceFirstName").val((data.OtherSourceFirstName !== null ? data.OtherSourceFirstName : ""));
            $("#txtEdit_Patient_OtherSourceLastName").val((data.OtherSourceLastName !== null ? data.OtherSourceLastName : ""));
        }
    }
    ,
    Edit: function() {
        var id = Patient._patientId;
        Patient.LoadPatientEditInfo(id);
    }
    ,
    EditFromGrid: function(id) {
        Patient.LoadPatientEditInfo(id);
    }
    ,
    Rebind: function() {
        // rebind the related grid
        var emergencyContactGrid = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
        var physicianContactGrid = $('#Edit_patient_PhysicianContact_Grid').data('tGrid');
        emergencyContactGrid.rebind({
            PatientId: Patient._patientId
        });
        physicianContactGrid.rebind({
            PatientId: Patient._patientId
        });
    }
    ,
    NewEmergencyContact: function(id) {

        $('#newEmergencyContactResult').load('Patient/NewEmergencyContactContent', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#newEmergencyContactResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#newEmergencyContact');
            }
            else if (textStatus == "success") {
                JQD.open_window('#newEmergencyContact');
                Patient.Init();
            }
        }
);
    }
    ,
    EditEmergencyContact: function(id) {

        $('#editEmergencyContactResult').load('Patient/EditEmergencyContactContent', { Id: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#editEmergencyContactResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#editEmergencyContact');
            }
            else if (textStatus == "success") {
                JQD.open_window('#editEmergencyContact');
                Patient.Init();
            }
        }
);

    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeleteEmergencyContact",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        Patient.Rebind();
                    }
                }
            }
            );
        }
    },
    NewPhysicianContact: function(id) {
        $('#newPhysicianResult').load('Patient/NewPhysicianContactContent', { patientID: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#newPhysicianResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#newPhysicianContact');
            }
            else if (textStatus == "success") {
                JQD.open_window('#newPhysicianContact');
                Patient.Init();

            }
        }
);
    },
    EditPhysicianContact: function(id) {
        $("#editPhysicianContactForm label.error").hide();
        $("#editPhysicianContactForm").clearForm();
        $("#editPhysicianContactValidaton").empty();
        $("#editPhysicianContactValidaton").hide();
        var data = 'PhysicianContactId=' + id + "&PatientId=" + Patient._patientId;
        $.ajax({
            url: '/Patient/GetPatientPhysicianContact',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                getNewRows(reportObject);
            }
        });
        getNewRows = function(data) {

            $("#txtEdit_PhysicianContactID").val(id);
            $("#txtEdit_PhysicianContactPatientID").val(Patient._patientId);
            $("#txtEdit_PhysicianContact_FirstName").val((data.FirstName !== null ? data.FirstName : ""));
            $("#txtEdit_PhysicianContact_LastName").val((data.LastName !== null ? data.LastName : ""));
            $("#txtEdit_PhysicianContact_AddressLine1").val((data.AddressLine1 !== null ? data.MailingAddressLine1 : ""));
            $("#txtEdit_PhysicianContact_AddressLine2").val((data.AddressLine2 !== null ? data.MailingAddressLine2 : ""));
            $("#txtEdit_PhysicianContact_AddressCity").val((data.AddressCity !== null ? data.MailingAddressCity : ""));
            $("#txtEdit_PhysicianContact_AddressStateCode").val((data.AddressStateCode !== null ? data.MailingAddressStateCode : ""));
            $("#txtEdit_PhysicianContact_AddressZipCode").val((data.AddressZipCode !== null ? data.MailingAddressZipCode : ""));
            $("#txtEdit_PhysicianContact_PrimaryPhoneArray1").val((data.PhoneWork !== null ? data.PhoneWork.substring(0, 3) : ""));
            $("#txtEdit_PhysicianContact_PrimaryPhoneArray2").val((data.PhoneWork !== null ? data.PhoneWork.substring(3, 6) : ""));
            $("#txtEdit_PhysicianContact_PrimaryPhoneArray3").val((data.PhoneWork !== null ? data.PhoneWork.substring(6) : ""));
            //            $("#txtEdit_PhysicianContact_AltPhoneArray1").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(0, 3) : ""));
            //            $("#txtEdit_PhysicianContact_AltPhoneArray2").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(3, 6) : ""));
            //            $("#txtEdit_PhysicianContact_AltPhoneArray3").val((data.PhoneMobile !== null ? data.PhoneMobile.substring(6) : ""));
            $("#txtEdit_Patient_PhysicianContactFax1").val((data.FaxNumber !== null ? data.FaxNumber.substring(0, 3) : ""));
            $("#txtEdit_Patient_PhysicianContactFax2").val((data.FaxNumber !== null ? data.FaxNumber.substring(3, 6) : ""));
            $("#txtEdit_Patient_PhysicianContactFax3").val((data.FaxNumber !== null ? data.FaxNumber.substring(6) : ""));
            $("#txtEdit_PhysicianContact_Email").val((data.EmailAddress !== null ? data.EmailAddress : ""));
            $("#txtEdit_PhysicianContact_NPINo").val((data.NPI !== null ? data.NPI : ""));

            if (data.Primary == true) {

                $('input[name=Primary][value=true]').attr('checked', true);
            }
        };
    },
    DeletePhysicianContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeletePhysicianContact",
                data: "id=" + id + "&patientID=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        Patient.Rebind();
                    }
                }
            });
        }
    },
    OnPatientRowSelected: function(e) {
        var patientId = e.row.cells[3].innerHTML;
        Patient.SetId(patientId);
        Patient.SetPatientRowIndex(e.row.rowIndex);
        Patient.loadPatientTopContent(patientId);
        Patient.RebindActivity(patientId);
    },
    LoadPatientInfo: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/Get",
            data: "id=" + Patient._patientId,
            beforeSend: function() {
                //U.blockUI('#patient_Splitter12');
            },
            success: function(result) {
                var data = eval(result);
                //             $("#patientId").text((data.PatientIdNumber !== null ? data.PatientIdNumber : ""));
                //             $("#patientName").text((data.FirstName !== null ? data.FirstName : "") + " " + (data.LastName != null ? data.LastName : ""));
                $("#patientLandingName").text((data.FirstName !== null ? data.FirstName : "") + " " + (data.LastName != null ? data.LastName : ""));
                $("#patientLandingName").val(data.Id);
                //            $("#patientAddress1").text(data.AddressLine1 !== null ? data.AddressLine1 : "");
                //                $("#patientAddress2").text(data.AddressLine2 !== null ? data.AddressLine2 : "");
                //                $("#patientCityStateZip").text(data.AddressCity + ", " + data.AddressStateCode + " " + data.AddressZipCode);
                //                $("#patientHomePhone").text(data.PhoneHome !== null && data.PhoneHome !== "" ? (data.PhoneHome.substring(0, 3) + "-" + data.PhoneHome.substring(3, 6) + "-" + data.PhoneHome.substring(6)) : "");
                //                $("#patientAltPhone").text(data.PhoneMobile !== null && data.PhoneHome !== "" ? (data.PhoneMobile.substring(0, 3) + "-" + data.PhoneMobile.substring(3, 6) + "- " + data.PhoneMobile.substring(6)) : " ");
                //                $("#patientGender").text(data.Gender !== null ? data.Gender : "");
                //                $("#patientBirthDate").text(data.DOBFormatted !== null ? data.DOBFormatted : "");
                //                $("#patientMedicareNumber").text(data.MedicareNumber !== null ? data.MedicareNumber : "");
                //                $("#patientMedicaidNumber").text(data.MedicaidNumber !== null ? data.MedicaidNumber : "");
                //                $("#patientSocDate").text(data.StartOfCareDateFormatted !== null ? data.StartOfCareDateFormatted : "");

                //                $('#patient_map').googleMaps({
                //                    geocode: data.AddressFull
                //                });

                //                var i = 0;
                //                var paymentSource = "";
                //                var paymentSourceArray = data.PaymentSource.split(";");

                //                for (i = 0; i < paymentSourceArray.length; i++) {

                //                    if (paymentSourceArray[i] != null || paymentSourceArray[i] == "")
                //                        paymentSource = paymentSource + Patient.PaymentSource(paymentSourceArray[i], data) + ",";
                //               }
                // $("#patientPaymentSource").text(paymentSource);
                //                var physicianContact = data.PhysicianContacts;

                //                if (physicianContact !== null && physicianContact.length > 0) {
                //                    var j = 0;
                //                    for (j = 0; j < physicianContact.length; j++) {
                //                        if (physicianContact[j].Primary == true) {
                //                            $("#patientPhysicianName").text((physicianContact[j].FirstName !== null ? physicianContact[j].FirstName : "") + " " + (physicianContact[j].LastName != null ? physicianContact[j].LastName : ""));
                //                            $("#patientPhysicianEmail").text(physicianContact[j].Email !== null ? physicianContact[j].Email : "");
                //                            $("#patientPhysicianPhone").text(physicianContact[j].PhoneWork !== null ? (physicianContact[j].PhoneWork.substring(0, 3) + "-" + physicianContact[j].PhoneWork.substring(3, 6) + "-" + physicianContact[j].PhoneWork.substring(6)) : "");
                //                            break;
                //                        }
                //                        else {
                //                            $("#patientPhysicianName").text("");
                //                            $("#patientPhysicianEmail").text("");
                //                            $("#patientPhysicianPhone").text("");
                //                        }
                //                    }
                //                }
                //                else {
                //                    $("#patientPhysicianName").text("");
                //                    $("#patientPhysicianEmail").text("");
                //                    $("#patientPhysicianPhone").text("");
                //                }
                //                var emergencyContact = data.EmergencyContacts;
                //                if (emergencyContact !== null && emergencyContact.length > 0) {
                //                    var j = 0;
                //                    for (j = 0; j < emergencyContact.length; j++) {
                //                        if (emergencyContact[j].Primary == true) {
                //                            $("#patientEmergencyContactName").text((emergencyContact[j].FirstName !== null ? emergencyContact[j].FirstName : "") + " " + (emergencyContact[j].LastName != null ? emergencyContact[j].LastName : ""));
                //                            $("#patientEmergencyContactPhone").text(emergencyContact[j].PrimaryPhone !== null ? (emergencyContact[j].PrimaryPhone.substring(0, 3) + "-" + emergencyContact[j].PrimaryPhone.substring(3, 6) + "-" + emergencyContact[j].PrimaryPhone.substring(6)) : "");
                //                            $("#patientEmergencyContactEmail").text(emergencyContact[j].EmailAddress !== null ? emergencyContact[j].EmailAddress : "");
                //                            break;
                //                        }
                //                        else {
                //                            $("#patientEmergencyContactName").text("");
                //                            $("#patientEmergencyContactPhone").text("");
                //                            $("#patientEmergencyContactEmail").text("");
                //                        }
                //                    }
                //                }
                //                else {
                //                    $("#patientEmergencyContactName").text("");
                //                    $("#patientEmergencyContactPhone").text("");
                //                    $("#patientEmergencyContactEmail").text("");
                //                }
                //                Patient.GetNote();
                //                U.unBlockUI('#patient_Splitter12');
                // Patient.RebindActivity();
            }
        });
    }
    ,
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/GetPhysicianContact",
            data: "PhysicianContactId=" + id,
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text((resultObject.FirstName !== null ? resultObject.FirstName : "") + " " + (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (resultObject.PhoneWork.substring(0, 3) + "-" + resultObject.PhoneWork.substring(3, 6) + "-" + resultObject.PhoneWork.substring(6)) : "");
            }
        });
    }
    ,
    GetPrimaryEmergencyContact: function(id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/GetEmergencyContact",
            data: "EmergencyContactId=" + id,
            success: function(result) {
                var resultObject = eval(result);
                $("#patientEmergencyContactName").text((resultObject.FirstName !== null ? resultObject.FirstName : "") + " " + (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientEmergencyContactPhone").text(resultObject.PrimaryPhone !== null ? (resultObject.PrimaryPhone.substring(0, 3) + "-" + resultObject.PrimaryPhone.substring(3, 6) + "-" + resultObject.PrimaryPhone.substring(6)) : "");
                $("#patientEmergencyContactEmail").text(resultObject.EmailAddress !== null ? resultObject.EmailAddress : "");
            }
        });
    },

    NoPatientBind: function(id) {
        Patient.loadPatientTopContent(id);
        Patient.RebindActivity(id);
    }
    ,

    RebindPatient: function() {
        var patientGrid = $('#PatientSelectionGrid').data('tGrid');
        patientGrid.rebind();
    },
    RebindActivity: function(id) {
        var patientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        patientActivityGrid.rebind({ patientId: id, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
    },
    loadPatientTopContent: function(id) {

        $('#patientMaintopResult').load('Patient/Info', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#patientMaintopResult').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {

            }
        }
);
    }
    ,
    loadNewOrder: function(id) {
        $("#orderNoteResult").load('Patient/NewOrderView', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#orderNoteResult").html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#orderNote');
            }
            else if (textStatus == "success") {
                JQD.open_window('#orderNote');

                $("#newOrderForm").validate({
                    submitHandler: function(form) {
                        var options = {
                            dataType: 'json',
                            beforeSubmit: function(values, form, options) {

                            },
                            success: function(result) {

                                var resultObject = eval(result);
                                if (resultObject.isSuccessful) {
                                    $("#newOrderValidaton").empty();
                                    $("#newOrderValidaton").hide();
                                    var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>" + resultObject.errorMessage + "!</strong></li></ul></div>";
                                    $("#newOrderValidaton").append(text);
                                    $("#newOrderValidaton").show();
                                    Patient.RebindActivity();
                                    Patient.Close($("#newOrderForm"));
                                }
                                else {
                                    $("#newOrderValidaton").empty();
                                    $("#newOrderValidaton").hide();
                                    var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                                    $("#newOrderValidaton").append(text);
                                    $("#newOrderValidaton").show();
                                }
                            }
                        };
                        $(form).ajaxSubmit(options);
                        return false;
                    }
                });
                $("#new_order_checkbox").attr("disabled", true);
                $('#new_order_save').removeAttr('disabled');
                $('#new_order_sendElectronically').attr('disabled', 'disabled');
                $("#txtNew_Order_Physician").change(function() {

                    var data = 'PhysicianContactId=' + $(this).val();
                    $.ajax({
                        url: '/Patient/GetPhysicianContact',
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        success: function(result) {
                            var reportObject = eval(result);
                            if (reportObject.Email !== null && reportObject.Email !== "") {
                                $("#new_order_checkbox").attr("disabled", false);
                            }
                            else {
                                $("#new_order_checkbox").attr("disabled", true);
                            }
                        }
                    });
                    if ($(this).val() == 0) {
                        $("#new_order_checkbox").attr("disabled", true);
                    }
                }
         );

                $("#new_order_checkbox").click(function() {
                    var q = $('#new_order_checkbox:checked').is(':checked');
                    if (q != true) {
                        $('#new_order_save').removeAttr('disabled');
                        $('#new_order_sendElectronically').attr('disabled', 'disabled');

                    }
                    else {
                        $('#new_order_save').attr('disabled', 'disabled');
                        $('#new_order_sendElectronically').removeAttr('disabled');
                    }
                });

            }
        }
);
    },
    loadNewCommunicationNote: function(id) {

        $("#communicationNoteResult").load('Patient/NewCommunicationNoteView', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#communicationNoteResult").html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#communicationNote');
            }
            else if (textStatus == "success") {
                JQD.open_window('#communicationNote');
                $("#newCommunicationNoteForm").validate({
                    submitHandler: function(form) {
                        var options = {
                            dataType: 'json',
                            beforeSubmit: function(values, form, options) {

                            },
                            success: function(result) {
                                var resultObject = eval(result);
                                if (resultObject.isSuccessful) {
                                    $("#newCommunicationNoteValidaton").empty();
                                    $("#newCommunicationNoteValidaton").hide();
                                    var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved !</strong></li></ul></div>";
                                    $("#newCommunicationNoteValidaton").append(text);
                                    $("#newCommunicationNoteValidaton").show();
                                    Patient.RebindActivity();
                                    Patient.Close($("#newCommunicationNoteForm"));
                                }
                                else {
                                    $("#newCommunicationNoteValidaton").empty();
                                    $("#newCommunicationNoteValidaton").hide();
                                    var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                                    $("#newCommunicationNoteValidaton").append(text);
                                    $("#newCommunicationNoteValidaton").show();
                                }
                            }
                        };
                        $(form).ajaxSubmit(options);
                        return false;
                    }
                });
            }
        });
    }
    ,
    Close: function(control) {
        control.closest('div.window').hide();
    },
    DateRange: function(id) {

        var data = 'dateRangeId=' + id + "&patientId=" + Patient._patientId;
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                $("#dateRangeText").empty();
                $("#dateRangeText").append(reportObject.StartDateFormatted + "-" + reportObject.EndDateFormatted);
            }
        });
    },
    CustomDateRange: function() {

        var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val(), rangeStartDate: $("#patientActivityFromDate-input").val(), rangeEndDate: $("#patientActivityToDate-input").val() });

    }
    ,
    NewPhysician: function() {
        var refer = $('#add_New_Physician');
        href = "javascript:void(0);";
        refer.attr('href', href);
    }
    ,
    NewPatient: function() {

        var refer = $('#add_New_Patient');
        href = "javascript:void(0);";
        refer.attr('href', href);
    },
    GetNote: function() {
        var data = "patientId=" + Patient._patientId;
        $.ajax({
            url: '/Patient/GetNote',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {

                var reportObject = eval(result);


                $("#txt_PatientLanding_Note").val("");

                if (reportObject.isSuccessful) {

                    $("#txt_PatientLanding_Note").val(reportObject.Note);
                }

                else {

                    $("#txt_PatientLanding_Note").val("");
                }

            }
        });
    }
    ,
    Note: function() {

        var data = "patientId=" + Patient._patientId;
        $.ajax({
            url: '/Patient/GetNote',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $("#notes").val("");
            },
            success: function(result) {
                var reportObject = eval(result);
                $("#EditNoteTitle").text("Customer Notes : " + Patient._patientName);
                $("#editNotePatientName").text(Patient._patientName);
                $("#txt_PatientNote_PatientID").val(Patient._patientId);
                var date = new Date();
                var note = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear() + ":\n";
                if (reportObject.isSuccessful) {
                    $("#notes").val(note + reportObject.Note);
                }
                else {
                    $("#notes").val(note);

                }
            }
        });

    },
    NoteTimeStamp: function() {
        var text = $("#notes").val();
        var date = new Date();
        var note = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear() + ":\n";
        $("#notes").val(note + text);
    },
    EditOrder: function(cont, id, patientId) {
        $("#editOrderForm label.error").hide();
        $("#editOrderForm").clearForm();
        $("#editOrderValidaton").empty();
        $("#editOrderValidaton").hide();
        var episodeId = $('td:last', cont.closest('tr')).text();
        var data = "Id=" + id + "&patientId=" + patientId;
        $.ajax({
            url: '/Patient/GetOrder',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
            },
            success: function(result) {
                var reportObject = eval(result);
                $("#edit_Order_Title").text("Edit Order - " + reportObject.DispalyName);
                $("#txtEdit_Order_EpisodeId").val(episodeId);
                $("#txtEdit_Order_Patient").text(reportObject.DispalyName);
                $("#txtEdit_OrderID").val(reportObject.Id);
                $("#txtEdit_Order_PatientID").val(reportObject.PatientId);
                $("#txtEdit_Order_Physician").val(reportObject.PhysicianId);
                $("#txtEdit_Order_OldEpisodeId").val(reportObject.EmployeeId);
                $("#txtEdit_Order_Assign").val(reportObject.EmployeeId);
                $("#txtEdit_Order_Date").data("tDatePicker").value(reportObject.OrderDate !== null ? (new Date(reportObject.OrderDate)) : new Date());
                $("#txtEdit_Order_Summary").val(reportObject.OrderSummary);
                $("#txtEdit_Order_text").val(reportObject.OrderText);
            }
        });
    }
    ,

    loadNewPatient: function() {

        $('#newPatientResult').load('Patient/NewContent', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#newPatientResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#newpatient');
            }
            else if (textStatus == "success") {
                JQD.open_window('#newpatient');              
                Patient.Init();
                Lookup.loadAdmissionSources();
                Lookup.loadStates();
                Lookup.loadRaces();
                Lookup.loadPhysicians();
                Lookup.loadUsers();
                Lookup.loadReferralSources();
            }
        }
);
    },
    loadEditPatient: function(id) {

        $('#editPatientResult').load('Patient/EditPatientContent', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#editPatientResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#editPatient');
            }
            else if (textStatus == "success") {
                JQD.open_window('#editPatient');
                Patient.Init();
                Lookup.loadAdmissionSources();
                Lookup.loadRaces();
                Lookup.loadUsers();
                Lookup.loadReferralSources();
            }
        }
);
    }
    ,
    EditCommunicationNote: function(id) {
        $("#editCommunicationNoteForm label.error").hide();
        $("#editCommunicationNoteForm").clearForm();
        $("#editCommunicationNoteValidaton").empty();
        $("#editCommunicationNoteValidaton").hide();
        var data = "Id=" + id + "&patientId=" + Patient._patientId;
        $.ajax({
            url: '/Patient/GetCommunicationNote',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
            },
            success: function(result) {
                var reportObject = eval(result);
                $("#edit_CommunicationNote_Title").text("Edit Communication Note - " + Patient._patientName);
                $("#txtEdit_CommunicationNote_Patient").text(Patient._patientName);
                $("#edit_CommunicationNote_ID").val(reportObject.Id);
                $("#txtEdit_CommunicationNote_PatientID").val(reportObject.PatientId);
                $("#txtEdit_CommunicationNote_Date").data("tDatePicker").value(reportObject.CommunicationNoteDate !== null ? (new Date(reportObject.CommunicationNoteDate)) : new Date());
                $("#txtEdit_CommunicationNote_text").val(reportObject.CommunicationNoteText);
            }
        });
    },
    PatientInfo: function(control, value) {
        Patient.PatientInfoCenter(control, value);
    },
    PatientInfoCenter: function(control, id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/Get",
            data: "id=" + id,
            beforeSend: function() {

            },
            success: function(result) {
                var data = eval(result);
                $("#patientId").text((data.PatientIdNumber !== null ? data.PatientIdNumber : ""));
                $("#patientName").text((data.FirstName !== null ? data.FirstName : "") + " " + (data.LastName != null ? data.LastName : ""));
                $("#patientAddress1").text(data.AddressLine1 !== null ? data.AddressLine1 : "");
                $("#patientAddress2").text(data.AddressLine2 !== null ? data.AddressLine2 : "");
                $("#patientCityStateZip").text(data.AddressCity + ", " + data.AddressStateCode + " " + data.AddressZipCode);
                $("#patientHomePhone").text(data.PhoneHome !== null && data.PhoneHome !== "" ? (data.PhoneHome.substring(0, 3) + "-" + data.PhoneHome.substring(3, 6) + "-" + data.PhoneHome.substring(6)) : "");
                $("#patientAltPhone").text(data.PhoneMobile !== null && data.PhoneHome !== "" ? (data.PhoneMobile.substring(0, 3) + "-" + data.PhoneMobile.substring(3, 6) + "- " + data.PhoneMobile.substring(6)) : " ");
                $("#patientGender").text(data.Gender !== null ? data.Gender : "");
                $("#patientBirthDate").text(data.DOBFormatted !== null ? data.DOBFormatted : "");
                $("#patientMedicareNumber").text(data.MedicareNumber !== null ? data.MedicareNumber : "");
                $("#patientMedicaidNumber").text(data.MedicaidNumber !== null ? data.MedicaidNumber : "");
                $("#patientSocDate").text(data.StartOfCareDateFormatted !== null ? data.StartOfCareDateFormatted : "");
                var physicianContact = data.PhysicianContacts;

                if (physicianContact !== null && physicianContact.length > 0) {
                    var j = 0;

                    for (j = 0; j < physicianContact.length; j++) {
                        if (physicianContact[j].Primary == true) {
                            $("#patientPhysicianName").text((physicianContact[j].FirstName !== null ? physicianContact[j].FirstName : "") + " " + (physicianContact[j].LastName != null ? physicianContact[j].LastName : ""));
                            $("#patientPhysicianEmail").text(physicianContact[j].Email !== null ? physicianContact[j].Email : "");
                            $("#patientPhysicianPhone").text(physicianContact[j].PhoneWork !== null ? (physicianContact[j].PhoneWork.substring(0, 3) + "-" + physicianContact[j].PhoneWork.substring(3, 6) + "-" + physicianContact[j].PhoneWork.substring(6)) : "");
                            break;
                        }
                        else {
                            $("#patientPhysicianName").text("");
                            $("#patientPhysicianEmail").text("");
                            $("#patientPhysicianPhone").text("");
                        }
                    }
                }
                else {
                    $("#patientPhysicianName").text("");
                    $("#patientPhysicianEmail").text("");
                    $("#patientPhysicianPhone").text("");
                }
                var emergencyContact = data.EmergencyContacts;
                if (emergencyContact !== null && emergencyContact.length > 0) {
                    var j = 0;
                    for (j = 0; j < emergencyContact.length; j++) {
                        if (emergencyContact[j].IsPrimary == true) {
                            $("#patientEmergencyContactName").text((emergencyContact[j].FirstName !== null ? emergencyContact[j].FirstName : "") + " " + (emergencyContact[j].LastName != null ? emergencyContact[j].LastName : ""));
                            $("#patientEmergencyContactPhone").text(emergencyContact[j].PrimaryPhone !== null ? (emergencyContact[j].PrimaryPhone.substring(0, 3) + "-" + emergencyContact[j].PrimaryPhone.substring(3, 6) + "-" + emergencyContact[j].PrimaryPhone.substring(6)) : "");
                            $("#patientEmergencyContactEmail").text(emergencyContact[j].EmailAddress !== null ? emergencyContact[j].EmailAddress : "");
                            break;
                        }
                        else {
                            $("#patientEmergencyContactName").text("");
                            $("#patientEmergencyContactPhone").text("");
                            $("#patientEmergencyContactEmail").text("");
                        }
                    }
                }
                else {
                    $("#patientEmergencyContactName").text("");
                    $("#patientEmergencyContactPhone").text("");
                    $("#patientEmergencyContactEmail").text("");
                }

            }
        });
        var postionLink = $(control).position();
        var $dialog = $("#patientInfo");
        $dialog.dialog({
            width: 600,
            position: [$(control).offset().left, $(control).offset().top],
            modal: false,

            resizable: false,
            close: function() {
                $dialog.dialog("destroy");
                $dialog.hide();
            }
        }).show();
    },
    DeleteSchedule: function(cont, eventId, employeeId) {
        if (confirm("Are you sure you want to delete")) {
            var episodeId = $('td:last', cont.closest('tr')).text();
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Schedule/Delete",
                data: "patientId=" + Patient._patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        Patient.RebindActivity();
                    }
                }
            });
        }
    },
    loadPatientCenter: function() {

        $("#PatientCenterResult").load('Patient/Center', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#PatientCenterResult").html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#patient_window');
            }
            else if (textStatus == "success") {
                var s6;                /*
                Note:
                Calling xAddEventListener before window.onload means that you must include
                the xAddEventListener source code "before" this application code.
                */


                jQuery.event.add(window, "resize", win_onresize);


                var cw = xClientWidth();
                var w = $(".window").width() - 2;
                var h = $(".window").height() - 55.5;
                s6 = new xSplitter('patient_Splitter1', 0, 0, w, h, true, 6, 200, 200, h / 5, true, 2, null, 0);


                function win_onresize() {
                    var cw = xClientWidth();
                    var w = $(".window").width() - 2;
                    var h = $(".window").height() - 55.5;
                    s6.paint(w, h, 200, 200, w / 4);

                }

                JQD.open_window('#patient_window');
                Patient.Init();

            }
        }
);
    }
}

