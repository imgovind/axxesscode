﻿var Recertification = {
    _RecertificationId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return Recertification._patientId;
    },
    SetId: function(patientId) {
        Recertification._patientId = patientId;
    },
    GetRecertificationId: function() {
        return Recertification._RecertificationId;
    },
    SetRecertificationId: function(RecertificationId) {
        Recertification._RecertificationId = RecertificationId;
    },
    GetSOCEpisodeId: function() {
        return Recertification._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        Recertification._EpisodeId = EpisodeId;
    },
    Init: function() {

        $("input[name=Recertification_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {
                $("#recertification_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#recertification_M1308").find(':input').each(function() { $(this).val(''); });
            }
            else if ($(this).val() == 1) {
                $("#recertification_M1308").unblock();
            }
        });

        $("input[name=Recertification_M1330StasisUlcer]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "03") {
                $("#recertification_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=Recertification_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=Recertification_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#recertification_M1332AndM1334").unblock();
            }
        });
        $("input[name=Recertification_M1340SurgicalWound]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#recertification_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=Recertification_M1342SurgicalWoundStatus]").attr('checked', false);
            }
            else {
                $("#recertification_M1342").unblock();
            }
        });
        Oasis.BradenScaleOnchange('Recertification', '#recetBradenScale');

        $("input[name=StartOfCare_GenericNutritionalHealth]").click(function() {
            Oasis.CalculateNutritionScore('Recertification');
        });


    },
    Last: function(control) {
        var formId = control.closest("form").attr("id");
        $("#" + formId).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("input[name='Recertification_Id']").val(resultObject.Assessment.Id);
                            $("input[name='Recertification_PatientGuid']").val(resultObject.Assessment.PatientId);
                            $("input[name='Recertification_Action']").val('Edit');
                        }
                        else {
                            alert(resultObject.errorMessage);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {

                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='Recertification_Id']").val(resultObject.Assessment.Id);
                        $("input[name='Recertification_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='Recertification_Action']").val('Edit');
                        Oasis.NextTab("#editRecertificationTabs.tabs");

                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='Recertification_Id']").val(resultObject.Assessment.Id);
                        $("input[name='Recertification_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='Recertification_Action']").val('Edit');
                    }


                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, formType) {

        var form = control.closest("form");
        form.validate();
        Recertification.HandlerHelper(form, control);

    },
    Validate: function(id) {
        OasisValidation.Validate(id, "Recertification");
    },
    loadRecertification: function(id, patientId, assessmentType) {
        $('#recertWindowResult').load('Oasis/NewRecertContent', { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
        if (textStatus == 'error') {           
                $('#recertWindowResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#recertWindow');
            }
            else if (textStatus == "success") {               
                JQD.open_window('#recertWindow');
                $("#editRecertificationTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#editRecertificationTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
                $("#editRecertificationTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                    Recertification.loadRecertificationParts(event, ui);
                });
                Recertification.Init();
            }
        }
);
    }
    ,
    loadRecertificationParts: function(event, ui) {

        $($(ui.tab).attr('href')).load('Oasis/NewRecertificationPartContent', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
            }
        }
);
    }
    ,
    Recertification: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#editRecertification").clearForm();
        $("#editRecertification div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#RecertificationTitle").text("New Recertification - " + patientName);
                $("input[name='Recertification_Id']").val("");
                $("input[name='Recertification_PatientGuid']").val(id);
                $("input[name='Recertification_Action']").val('New');
                $("#Recertification_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#Recertification_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#Recertification_M0040FirstName").val(patient.FirstName);
                $("#Recertification_M0040MI").val(patient.MiddleInitial);
                $("#Recertification_M0040LastName").val(patient.LastName);
                $("#Recertification_M0050PatientState").val(patient.AddressStateCode);
                $("#Recertification_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#Recertification_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#Recertification_M0064PatientSSN").val(patient.SSN);
                $("#Recertification_M0065PatientMedicaidNumber").val(patient.MedicaidNumber);
                $("#Recertification_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=Recertification_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='Recertification_M0100AssessmentType'][value='04']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=Recertification_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=Recertification_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=Recertification_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=Recertification_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=Recertification_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=Recertification_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=Recertification_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=Recertification_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=Recertification_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=Recertification_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=Recertification_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=Recertification_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=Recertification_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=Recertification_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=Recertification_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=Recertification_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=Recertification_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=Recertification_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=Recertification_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#Recertification_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                    }
                }
                Oasis.ClearRows($("#suppliesTableRece"));
                Oasis.addTableRow("#suppliesTableRece");
            }
        });
    }
    ,
    EditRecertification: function(id, patientId, assessmentType) {
        //var patientId = Oasis.GetId();
        $("#editRecertification").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {

            var patient = eval(result);
            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#RecertificationTitle").text("Edit Recertification - " + firstName + " " + lastName);
            Recertification.SetRecertificationId(id);
            $("input[name='Recertification_Id']").val(id);
            $("input[name='Recertification_Action']").val('Edit');
            $("input[name='Recertification_PatientGuid']").val(patientId);
            $("#Recertification_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#Recertification_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#Recertification_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=Recertification_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#Recertification_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=Recertification_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#Recertification_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                $("#Recertification_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#Recertification_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#Recertification_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#Recertification_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");
            var rOCDateNotApplicable = result["M0032ROCDateNotApplicable"];
            if (rOCDateNotApplicable != null && rOCDateNotApplicable != undefined) {
                if (rOCDateNotApplicable.Answer == 1) {

                    $('input[name=Recertification_M0032ROCDateNotApplicable][value=1]').attr('checked', true);
                    $("#Recertification_M0032ROCDate").val(" ");
                }
                else {
                    $('input[name=Recertification_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                    $("#Recertification_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                $("#Recertification_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
            }
            $("#Recertification_M0040FirstName").val(firstName);
            $("#Recertification_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#Recertification_M0040LastName").val(lastName);
            $("#Recertification_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#Recertification_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=Recertification_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#Recertification_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=Recertification_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#Recertification_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                $("#Recertification_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=Recertification_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#Recertification_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=Recertification_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#Recertification_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                $("#Recertification_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=Recertification_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#Recertification_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=Recertification_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#Recertification_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                $("#Recertification_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }

            $("#Recertification_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=Recertification_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='Recertification_M0100AssessmentType'][value='04']").attr('checked', true);
            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=Recertification_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#Recertification_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=Recertification_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#Recertification_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=Recertification_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#Recertification_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }
            $("#Recertification_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=Recertification_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=Recertification_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#Recertification_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=Recertification_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=Recertification_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);

            var ICD9M = result["M1020ICD9M"];
            if (ICD9M != null && ICD9M != undefined && ICD9M.Answer != null) {
                $("#Recertification_M1020ICD9M").val(result["M1020ICD9M"] != null && result["M1020ICD9M"] != undefined ? result["M1020ICD9M"].Answer : "");
                $("#Recertification_M1020PrimaryDiagnosis").val(result["M1020PrimaryDiagnosis"] != null && result["M1020PrimaryDiagnosis"] != undefined ? result["M1020PrimaryDiagnosis"].Answer : "");
                $("#Recertification_M1020SymptomControlRating").val(result["M1020SymptomControlRating"] != null && result["M1020SymptomControlRating"] != undefined ? result["M1020SymptomControlRating"].Answer : "");
            }
            var ICD9MA3 = result["M1024ICD9MA3"];
            if (ICD9MA3 != null && ICD9MA3 != undefined && ICD9MA3.Answer != null) {
                $("#Recertification_M1024ICD9MA3").val(result["M1024ICD9MA3"] != null && result["M1024ICD9MA3"] != undefined ? result["M1024ICD9MA3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesA3").val(result["M1024PaymentDiagnosesA3"] != null && result["M1024PaymentDiagnosesA3"] != undefined ? result["M1024PaymentDiagnosesA3"].Answer : "");
            }
            var ICD9MA4 = result["M1024ICD9MA4"];
            if (ICD9MA4 != null && ICD9MA4 != undefined && ICD9MA4.Answer != null) {
                $("#Recertification_M1024ICD9MA4").val(result["M1024ICD9MA4"] != null && result["M1024ICD9MA4"] != undefined ? result["M1024ICD9MA4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesA4").val(result["M1024PaymentDiagnosesA4"] != null && result["M1024PaymentDiagnosesA4"] != undefined ? result["M1024PaymentDiagnosesA4"].Answer : "");
            }

            var ICD9M1 = result["M1022ICD9M1"];
            if (ICD9M1 != null && ICD9M1 != undefined && ICD9M1.Answer != null) {
                $("#Recertification_M1022ICD9M1").val(result["M1022ICD9M1"] != null && result["M1022ICD9M1"] != undefined ? result["M1022ICD9M1"].Answer : "");
                $("#Recertification_M1022PrimaryDiagnosis1").val(result["M1022PrimaryDiagnosis1"] != null && result["M1022PrimaryDiagnosis1"] != undefined ? result["M1022PrimaryDiagnosis1"].Answer : "");
                $("#Recertification_M1022OtherDiagnose1Rating").val(result["M1022OtherDiagnose1Rating"] != null && result["M1022OtherDiagnose1Rating"] != undefined ? result["M1022OtherDiagnose1Rating"].Answer : "");
            }
            var ICD9MB3 = result["M1024ICD9MB3"];
            if (ICD9MB3 != null && ICD9MB3 != undefined && ICD9MB3.Answer != null) {
                $("#Recertification_M1024ICD9MB3").val(result["M1024ICD9MB3"] != null && result["M1024ICD9MB3"] != undefined ? result["M1024ICD9MB3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesB3").val(result["M1024PaymentDiagnosesB3"] != null && result["M1024PaymentDiagnosesB3"] != undefined ? result["M1024PaymentDiagnosesB3"].Answer : "");
            }
            var ICD9MB4 = result["M1024ICD9MB4"];
            if (ICD9MB4 != null && ICD9MB4 != undefined && ICD9MB4.Answer != null) {
                $("#Recertification_M1024ICD9MB4").val(result["M1024ICD9MB4"] != null && result["M1024ICD9MB4"] != undefined ? result["M1024ICD9MB4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesB4").val(result["M1024PaymentDiagnosesB4"] != null && result["M1024PaymentDiagnosesB4"] != undefined ? result["M1024PaymentDiagnosesB4"].Answer : "");
            }
            var ICD9M2 = result["M1022ICD9M2"];
            if (ICD9M2 != null && ICD9M2 != undefined && ICD9M2.Answer != null) {
                $("#Recertification_M1022ICD9M2").val(result["M1022ICD9M2"] != null && result["M1022ICD9M2"] != undefined ? result["M1022ICD9M2"].Answer : "");
                $("#Recertification_M1022PrimaryDiagnosis2").val(result["M1022PrimaryDiagnosis2"] != null && result["M1022PrimaryDiagnosis2"] != undefined ? result["M1022PrimaryDiagnosis2"].Answer : "");
                $("#Recertification_M1022OtherDiagnose2Rating").val(result["M1022OtherDiagnose2Rating"] != null && result["M1022OtherDiagnose2Rating"] != undefined ? result["M1022OtherDiagnose2Rating"].Answer : "");
            }
            var ICD9MC3 = result["M1024ICD9MC3"];
            if (ICD9MC3 != null && ICD9MC3 != undefined && ICD9MC3.Answer != null) {
                $("#Recertification_M1024ICD9MC3").val(result["M1024ICD9MC3"] != null && result["M1024ICD9MC3"] != undefined ? result["M1024ICD9MC3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesC3").val(result["M1024PaymentDiagnosesC3"] != null && result["M1024PaymentDiagnosesC3"] != undefined ? result["M1024PaymentDiagnosesC3"].Answer : "");
            }
            var ICD9MC4 = result["M1024ICD9MC4"];
            if (ICD9MC4 != null && ICD9MC4 != undefined && ICD9MC4.Answer != null) {
                $("#Recertification_M1024ICD9MC4").val(result["M1024ICD9MC4"] != null && result["M1024ICD9MC4"] != undefined ? result["M1024ICD9MC4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesC4").val(result["M1024PaymentDiagnosesC4"] != null && result["M1024PaymentDiagnosesC4"] != undefined ? result["M1024PaymentDiagnosesC4"].Answer : "");
            }

            var ICD9M3 = result["M1022ICD9M3"];
            if (ICD9M3 != null && ICD9M3 != undefined && ICD9M3.Answer != null) {
                $("#Recertification_M1022ICD9M3").val(result["M1022ICD9M3"] != null && result["M1022ICD9M3"] != undefined ? result["M1022ICD9M3"].Answer : "");
                $("#Recertification_M1022PrimaryDiagnosis3").val(result["M1022PrimaryDiagnosis3"] != null && result["M1022PrimaryDiagnosis3"] != undefined ? result["M1022PrimaryDiagnosis3"].Answer : "");
                $("#Recertification_M1022OtherDiagnose3Rating").val(result["M1022OtherDiagnose3Rating"] != null && result["M1022OtherDiagnose3Rating"] != undefined ? result["M1022OtherDiagnose3Rating"].Answer : "");
            }
            var ICD9MD3 = result["M1024ICD9MD3"];
            if (ICD9MD3 != null && ICD9MD3 != undefined && ICD9MD3.Answer != null) {
                $("#Recertification_M1024ICD9MD3").val(result["M1024ICD9MD3"] != null && result["M1024ICD9MD3"] != undefined ? result["M1024ICD9MD3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesD3").val(result["M1024PaymentDiagnosesD3"] != null && result["M1024PaymentDiagnosesD3"] != undefined ? result["M1024PaymentDiagnosesD3"].Answer : "");
            }
            var ICD9MD4 = result["M1024ICD9MD4"];
            if (ICD9MD4 != null && ICD9MD4 != undefined && ICD9MD4.Answer != null) {
                $("#Recertification_M1024ICD9MD4").val(result["M1024ICD9MD4"] != null && result["M1024ICD9MD4"] != undefined ? result["M1024ICD9MD4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesD4").val(result["M1024PaymentDiagnosesD4"] != null && result["M1024PaymentDiagnosesD4"] != undefined ? result["M1024PaymentDiagnosesD4"].Answer : "");
            }

            var ICD9M4 = result["M1022ICD9M4"];
            if (ICD9M4 != null && ICD9M4 != undefined && ICD9M4.Answer != null) {
                $("#Recertification_M1022ICD9M4").val(result["M1022ICD9M4"] != null && result["M1022ICD9M4"] != undefined ? result["M1022ICD9M4"].Answer : "");
                $("#Recertification_M1022PrimaryDiagnosis4").val(result["M1022PrimaryDiagnosis4"] != null && result["M1022PrimaryDiagnosis4"] != undefined ? result["M1022PrimaryDiagnosis4"].Answer : "");
                $("#Recertification_M1022OtherDiagnose4Rating").val(result["M1022OtherDiagnose4Rating"] != null && result["M1022OtherDiagnose4Rating"] != undefined ? result["M1022OtherDiagnose4Rating"].Answer : "");
            }
            var ICD9ME3 = result["M1024ICD9ME3"];
            if (ICD9ME3 != null && ICD9ME3 != undefined && ICD9ME3.Answer != null) {
                $("#Recertification_M1024ICD9ME3").val(result["M1024ICD9ME3"] != null && result["M1024ICD9ME3"] != undefined ? result["M1024ICD9ME3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesE3").val(result["M1024PaymentDiagnosesE3"] != null && result["M1024PaymentDiagnosesE3"] != undefined ? result["M1024PaymentDiagnosesE3"].Answer : "");
            }
            var ICD9ME4 = result["M1024ICD9ME4"];
            if (ICD9ME4 != null && ICD9ME4 != undefined && ICD9ME4.Answer != null) {
                $("#Recertification_M1024ICD9ME4").val(result["M1024ICD9ME4"] != null && result["M1024ICD9ME4"] != undefined ? result["M1024ICD9ME4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesE4").val(result["M1024PaymentDiagnosesE4"] != null && result["M1024PaymentDiagnosesE4"] != undefined ? result["M1024PaymentDiagnosesE4"].Answer : "");
            }

            var ICD9M5 = result["M1022ICD9M5"];
            if (ICD9M5 != null && ICD9M5 != undefined && ICD9M5.Answer != null) {
                $("#Recertification_M1022ICD9M5").val(result["M1022ICD9M5"] != null && result["M1022ICD9M5"] != undefined ? result["M1022ICD9M5"].Answer : "");
                $("#Recertification_M1022PrimaryDiagnosis5").val(result["M1022PrimaryDiagnosis5"] != null && result["M1022PrimaryDiagnosis5"] != undefined ? result["M1022PrimaryDiagnosis5"].Answer : "");
                $("#Recertification_M1022OtherDiagnose5Rating").val(result["M1022OtherDiagnose5Rating"] != null && result["M1022OtherDiagnose5Rating"] != undefined ? result["M1022OtherDiagnose5Rating"].Answer : "");
            }
            var ICD9MF3 = result["M1024ICD9MF3"];
            if (ICD9MF3 != null && ICD9MF3 != undefined && ICD9MF3.Answer != null) {
                $("#Recertification_M1024ICD9MF3").val(result["M1024ICD9MF3"] != null && result["M1024ICD9MF3"] != undefined ? result["M1024ICD9MF3"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesF3").val(result["M1024PaymentDiagnosesF3"] != null && result["M1024PaymentDiagnosesF3"] != undefined ? result["M1024PaymentDiagnosesF3"].Answer : "");
            }
            var ICD9MF4 = result["M1024ICD9MF4"];
            if (ICD9MF4 != null && ICD9MF4 != undefined && ICD9MF4.Answer != null) {
                $("#Recertification_M1024ICD9MF4").val(result["M1024ICD9MF4"] != null && result["M1024ICD9MF4"] != undefined ? result["M1024ICD9MF4"].Answer : "");
                $("#Recertification_M1024PaymentDiagnosesF4").val(result["M1024PaymentDiagnosesF4"] != null && result["M1024PaymentDiagnosesF4"] != undefined ? result["M1024PaymentDiagnosesF4"].Answer : "");
            }

            $('input[name=Recertification_M1030HomeTherapiesInfusion][value=' + (result["M1030HomeTherapiesInfusion"] != null && result["M1030HomeTherapiesInfusion"] != undefined ? result["M1030HomeTherapiesInfusion"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M1030HomeTherapiesParNutrition][value=' + (result["M1030HomeTherapiesParNutrition"] != null && result["M1030HomeTherapiesParNutrition"] != undefined ? result["M1030HomeTherapiesParNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M1030HomeTherapiesEntNutrition][value=' + (result["M1030HomeTherapiesEntNutrition"] != null && result["M1030HomeTherapiesEntNutrition"] != undefined ? result["M1030HomeTherapiesEntNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M1030HomeTherapiesNone][value=' + (result["M1030HomeTherapiesNone"] != null && result["M1030HomeTherapiesNone"] != undefined ? result["M1030HomeTherapiesNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=Recertification_M1200Vision][value=' + (result["M1200Vision"] != null && result["M1200Vision"] != undefined ? result["M1200Vision"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1242PainInterferingFrequency][value=' + (result["M1242PainInterferingFrequency"] != null && result["M1242PainInterferingFrequency"] != undefined ? result["M1242PainInterferingFrequency"].Answer : "") + ']').attr('checked', true);

            var unhealedPressureUlcers = "";
            if (result["M1306UnhealedPressureUlcers"] != null && result["M1306UnhealedPressureUlcers"] != undefined) {
                unhealedPressureUlcers = result["M1306UnhealedPressureUlcers"].Answer
            }
            $('input[name=Recertification_M1306UnhealedPressureUlcers][value=' + (unhealedPressureUlcers != "" ? unhealedPressureUlcers : "") + ']').attr('checked', true);

            if (unhealedPressureUlcers == "0") {
                $("#recertification_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (unhealedPressureUlcers == "1") {
                $("#Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent").val(result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission").val(result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission").val(result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission").val(result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : "");
                $("#Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "");
                $("#recertification_M1308").unblock();
            }

            $('input[name=Recertification_M1322CurrentNumberStageIUlcer][value=' + (result["M1322CurrentNumberStageIUlcer"] != null && result["M1322CurrentNumberStageIUlcer"] != undefined ? result["M1322CurrentNumberStageIUlcer"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1324MostProblematicUnhealedStage][value=' + (result["M1324MostProblematicUnhealedStage"] != null && result["M1324MostProblematicUnhealedStage"] != undefined ? result["M1324MostProblematicUnhealedStage"].Answer : "") + ']').attr('checked', true);


            var stasisUlcer = "";
            if (result["M1330StasisUlcer"] != null && result["M1330StasisUlcer"] != undefined) {
                stasisUlcer = result["M1330StasisUlcer"].Answer
            }
            $('input[name=Recertification_M1330StasisUlcer][value=' + (stasisUlcer != "" ? stasisUlcer : "") + ']').attr('checked', true);

            if (stasisUlcer == "00" || stasisUlcer == "03") {
                $("#recertification_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=Recertification_M1332CurrentNumberStasisUlcer][value=' + (result["M1332CurrentNumberStasisUlcer"] != null && result["M1332CurrentNumberStasisUlcer"] != undefined ? result["M1332CurrentNumberStasisUlcer"].Answer : "") + ']').attr('checked', true);
                $('input[name=Recertification_M1334StasisUlcerStatus][value=' + (result["M1334StasisUlcerStatus"] != null && result["M1334StasisUlcerStatus"] != undefined ? result["M1334StasisUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#recertification_M1332AndM1334").unblock();
            }

            var surgicalWound = "";
            if (result["M1340SurgicalWound"] != null && result["M1340SurgicalWound"] != undefined) {
                surgicalWound = result["M1340SurgicalWound"].Answer
            }
            $('input[name=Recertification_M1340SurgicalWound][value=' + (surgicalWound != "" ? surgicalWound : "") + ']').attr('checked', true);

            if (surgicalWound == "00" || surgicalWound == "02") {
                $("#recertification_M1342").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=Recertification_M1342SurgicalWoundStatus][value=' + (result["M1342SurgicalWoundStatus"] != null && result["M1342SurgicalWoundStatus"] != undefined ? result["M1342SurgicalWoundStatus"].Answer : "") + ']').attr('checked', true);
                $("#recertification_M1342").unblock();
            }

            $('input[name=Recertification_M1350SkinLesionOpenWound][value=' + (result["M1350SkinLesionOpenWound"] != null && result["M1350SkinLesionOpenWound"] != undefined ? result["M1350SkinLesionOpenWound"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1400PatientDyspneic][value=' + (result["M1400PatientDyspneic"] != null && result["M1400PatientDyspneic"] != undefined ? result["M1400PatientDyspneic"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1610UrinaryIncontinence][value=' + (result["M1610UrinaryIncontinence"] != null && result["M1610UrinaryIncontinence"] != undefined ? result["M1610UrinaryIncontinence"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1620BowelIncontinenceFrequency][value=' + (result["M1620BowelIncontinenceFrequency"] != null && result["M1620BowelIncontinenceFrequency"] != undefined ? result["M1620BowelIncontinenceFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1630OstomyBowelElimination][value=' + (result["M1630OstomyBowelElimination"] != null && result["M1630OstomyBowelElimination"] != undefined ? result["M1630OstomyBowelElimination"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1810CurrentAbilityToDressUpper][value=' + (result["M1810CurrentAbilityToDressUpper"] != null && result["M1810CurrentAbilityToDressUpper"] != undefined ? result["M1810CurrentAbilityToDressUpper"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1820CurrentAbilityToDressLower][value=' + (result["M1820CurrentAbilityToDressLower"] != null && result["M1820CurrentAbilityToDressLower"] != undefined ? result["M1820CurrentAbilityToDressLower"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1830CurrentAbilityToBatheEntireBody][value=' + (result["M1830CurrentAbilityToBatheEntireBody"] != null && result["M1830CurrentAbilityToBatheEntireBody"] != undefined ? result["M1830CurrentAbilityToBatheEntireBody"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1840ToiletTransferring][value=' + (result["M1840ToiletTransferring"] != null && result["M1840ToiletTransferring"] != undefined ? result["M1840ToiletTransferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1850Transferring][value=' + (result["M1850Transferring"] != null && result["M1850Transferring"] != undefined ? result["M1850Transferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M1860AmbulationLocomotion][value=' + (result["M1860AmbulationLocomotion"] != null && result["M1860AmbulationLocomotion"] != undefined ? result["M1860AmbulationLocomotion"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");
            var therapyNeed = result["M2200TherapyNeed"];
            if (therapyNeed !== null && therapyNeed != undefined) {
                if (therapyNeed.Answer == 1) {

                    $('input[name=Recertification_M2200TherapyNeed][value=1]').attr('checked', true);
                    $("#Recertification_M2200NumberOfTherapyNeed").val("");
                }
                else {
                    $('input[name=Recertification_M2200TherapyNeed][value=1]').attr('checked', false);
                    $("#Recertification_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");
                }
            }
            $('input[name=Recertification_485Allergies][value=' + (result["485Allergies"] != null && result["485Allergies"] != undefined ? result["485Allergies"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_485AllergiesDescription").val(result["485AllergiesDescription"] != null && result["485AllergiesDescription"] != undefined ? result["485AllergiesDescription"].Answer : "");

            $("#Recertification_GenericPulseApical").val(result["GenericPulseApical"] != null && result["GenericPulseApical"] != undefined ? result["GenericPulseApical"].Answer : "");
            $('input[name=Recertification_GenericPulseApicalRegular][value=' + (result["GenericPulseApicalRegular"] != null && result["GenericPulseApicalRegular"] != undefined ? result["GenericPulseApicalRegular"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericPulseRadial").val(result["GenericPulseRadial"] != null && result["GenericPulseRadial"] != undefined ? result["GenericPulseRadial"].Answer : "");
            $('input[name=Recertification_GenericPulseRadialRegular][value=' + (result["GenericPulseRadialRegular"] != null && result["GenericPulseRadialRegular"] != undefined ? result["GenericPulseRadialRegular"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericHeight").val(result["GenericHeight"] != null && result["GenericHeight"] != undefined ? result["GenericHeight"].Answer : "");
            $("#Recertification_GenericWeight").val(result["GenericWeight"] != null && result["GenericWeight"] != undefined ? result["GenericWeight"].Answer : "");

            $("#Recertification_GenericBPLeftLying").val(result["GenericBPLeftLying"] != null && result["GenericBPLeftLying"] != undefined ? result["GenericBPLeftLying"].Answer : "");
            $("#Recertification_GenericBPLeftSitting").val(result["GenericBPLeftSitting"] != null && result["GenericBPLeftSitting"] != undefined ? result["GenericBPLeftSitting"].Answer : "");
            $("#Recertification_GenericBPLeftStanding").val(result["GenericBPLeftStanding"] != null && result["GenericBPLeftStanding"] != undefined ? result["GenericBPLeftStanding"].Answer : "");

            $("#Recertification_GenericBPRightLying").val(result["GenericBPRightLying"] != null && result["GenericBPRightLying"] != undefined ? result["GenericBPRightLying"].Answer : "");
            $("#Recertification_GenericBPRightSitting").val(result["GenericBPRightSitting"] != null && result["GenericBPRightSitting"] != undefined ? result["GenericBPRightSitting"].Answer : "");
            $("#Recertification_GenericBPRightStanding").val(result["GenericBPRightStanding"] != null && result["GenericBPRightStanding"] != undefined ? result["GenericBPRightStanding"].Answer : "");

            $("#Recertification_GenericTemp").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericResp").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");
            $('input[name=Recertification_GenericVitalSignsActualStated][value=' + (result["GenericVitalSignsActualStated"] != null && result["GenericVitalSignsActualStated"] != undefined ? result["GenericVitalSignsActualStated"].Answer : "") + ']').attr('checked', true);


            $("#Recertification_GenericTempGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericTempLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericPulseGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericPulseLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericRespirationGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericRespirationLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericSystolicBPGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericSystolicBPLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericDiastolicBPGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericDiastolicBPLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");


            $("#Recertification_Generic02SatLessThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");


            $("#Recertification_GenericFastingBloodSugarGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericFastingBloodSugarLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericRandomBloddSugarGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericRandomBloddSugarLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#Recertification_GenericWeightGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#Recertification_GenericWeightLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");


            var medicalHistory = result["GenericMedicalHistory"];
            if (medicalHistory !== null && medicalHistory != undefined && medicalHistory.Answer != null) {
                var medicalHistoryArray = (medicalHistory.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicalHistoryArray.length; i++) {
                    $('input[name=Recertification_GenericMedicalHistory][value=' + medicalHistoryArray[i] + ']').attr('checked', true);
                    if (medicalHistoryArray[i] == 10) {
                        $("#Recertification_GenericMedicalHistoryCancerType").val((result["GenericMedicalHistoryCancerType"] !== null && result["GenericMedicalHistoryCancerType"] !== undefined ? result["GenericMedicalHistoryCancerType"].Answer : " "));
                        $('input[name=Recertification_GenericMedicalHistoryCancerRemission][value=' + (result["GenericMedicalHistoryCancerRemission"] != null && result["GenericMedicalHistoryCancerRemission"] != undefined ? result["GenericMedicalHistoryCancerRemission"].Answer : "") + ']').attr('checked', true);
                    }
                    if (medicalHistoryArray[i] == 11) {
                        $("#Recertification_GenericMedicalHistoryOsteoarthritisSites").val((result["GenericMedicalHistoryOsteoarthritisSites"] !== null && result["GenericMedicalHistoryOsteoarthritisSites"] !== undefined ? result["GenericMedicalHistoryOsteoarthritisSites"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 16) {
                        $("#Recertification_GenericMedicalHistoryJointReplacementLocation").val((result["GenericMedicalHistoryJointReplacementLocation"] !== null && result["GenericMedicalHistoryJointReplacementLocation"] !== undefined ? result["GenericMedicalHistoryJointReplacementLocation"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 30) {
                        $("#Recertification_GenericMedicalHistoryLiverGallBladderDesc").val((result["GenericMedicalHistoryLiverGallBladderDesc"] !== null && result["GenericMedicalHistoryLiverGallBladderDesc"] !== undefined ? result["GenericMedicalHistoryLiverGallBladderDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 35) {
                        $("#Recertification_GenericMedicalHistorySubstanceAbuseDesc").val((result["GenericMedicalHistorySubstanceAbuseDesc"] !== null && result["GenericMedicalHistorySubstanceAbuseDesc"] !== undefined ? result["GenericMedicalHistorySubstanceAbuseDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 36) {
                        $("#Recertification_GenericMedicalHistoryMentalDisorderDesc").val((result["GenericMedicalHistoryMentalDisorderDesc"] !== null && result["GenericMedicalHistoryMentalDisorderDesc"] !== undefined ? result["GenericMedicalHistoryMentalDisorderDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 41) {
                        $("#Recertification_GenericMedicalHistoryOtherDetails").val((result["GenericMedicalHistoryOtherDetails"] !== null && result["GenericMedicalHistoryOtherDetails"] !== undefined ? result["GenericMedicalHistoryOtherDetails"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 60) {
                        $("#Recertification_GenericMedicalHistoryHepatitisDetails").val((result["GenericMedicalHistoryHepatitisDetails"] !== null && result["GenericMedicalHistoryHepatitisDetails"] !== undefined ? result["GenericMedicalHistoryHepatitisDetails"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 61) {
                        $("#Recertification_GenericMedicalHistoryInfectiousDiseaseDetails").val((result["GenericMedicalHistoryInfectiousDiseaseDetails"] !== null && result["GenericMedicalHistoryInfectiousDiseaseDetails"] !== undefined ? result["GenericMedicalHistoryInfectiousDiseaseDetails"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 62) {
                        $("#Recertification_GenericMedicalHistoryTobaccoDependenceType").val((result["GenericMedicalHistoryTobaccoDependenceType"] !== null && result["GenericMedicalHistoryTobaccoDependenceType"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceType"].Answer : " "));
                        $("#Recertification_GenericMedicalHistoryTobaccoDependenceAmt").val((result["GenericMedicalHistoryTobaccoDependenceAmt"] !== null && result["GenericMedicalHistoryTobaccoDependenceAmt"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceAmt"].Answer : " "));
                        $("#Recertification_GenericMedicalHistoryTobaccoDependenceTypeLength").val((result["GenericMedicalHistoryTobaccoDependenceTypeLength"] !== null && result["GenericMedicalHistoryTobaccoDependenceTypeLength"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceTypeLength"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 65) {
                        $("#Recertification_GenericMedicalHistoryExtraDetails").val((result["GenericMedicalHistoryExtraDetails"] !== null && result["GenericMedicalHistoryExtraDetails"] !== undefined ? result["GenericMedicalHistoryExtraDetails"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 66) {
                        $("#Recertification_GenericMedicalHistoryPastSuguriesDetails").val((result["GenericMedicalHistoryPastSuguriesDetails"] !== null && result["GenericMedicalHistoryPastSuguriesDetails"] !== undefined ? result["GenericMedicalHistoryPastSuguriesDetails"].Answer : " "));

                    }

                }
            }

            $('input[name=Recertification_485Pnemonia][value=' + (result["485Pnemonia"] != null && result["485Pnemonia"] != undefined ? result["485Pnemonia"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485Pnemonia]:checked').val() == 'Yes') {
                $("#Recertification_485PnemoniaDate").val((result["485PnemoniaDate"] !== null && result["485PnemoniaDate"] !== undefined ? result["485PnemoniaDate"].Answer : " "));

            }

            $('input[name=Recertification_485Flu][value=' + (result["485Flu"] != null && result["485Flu"] != undefined ? result["485Flu"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485Flu]:checked').val() == 'Yes') {
                $("#Recertification_485FluDate").val((result["485FluDate"] !== null && result["485FluDate"] !== undefined ? result["485FluDate"].Answer : " "));

            }

            $('input[name=Recertification_485Tetanus][value=' + (result["485Tetanus"] != null && result["485Tetanus"] != undefined ? result["485Tetanus"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485Tetanus]:checked').val() == 'Yes') {
                $("#Recertification_485TetanusDate").val((result["485TetanusDate"] !== null && result["485TetanusDate"] !== undefined ? result["485TetanusDate"].Answer : " "));

            }

            $('input[name=Recertification_485TB][value=' + (result["485TB"] != null && result["485TB"] != undefined ? result["485TB"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485TB]:checked').val() == 'Yes') {
                $("#Recertification_485TBDate").val((result["485TBDate"] !== null && result["485TBDate"] !== undefined ? result["485TBDate"].Answer : " "));

            }

            $('input[name=Recertification_485TBExposure][value=' + (result["485TBExposure"] != null && result["485TBExposure"] != undefined ? result["485TBExposure"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485TBExposure]:checked').val() == 'Yes') {
                $("#Recertification_485TBExposureDate").val((result["485TBExposureDate"] !== null && result["485TBExposureDate"] !== undefined ? result["485TBExposureDate"].Answer : " "));

            }

            $('input[name=Recertification_485HepatitisB][value=' + (result["485HepatitisB"] != null && result["485HepatitisB"] != undefined ? result["485HepatitisB"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485HepatitisB]:checked').val() == 'Yes') {
                $("#Recertification_485HepatitisBDate").val((result["485HepatitisBDate"] !== null && result["485HepatitisBDate"] !== undefined ? result["485HepatitisBDate"].Answer : " "));

            }

            $("#Recertification_485AdditionalImmunization1Name").val((result["485AdditionalImmunization1Name"] !== null && result["485AdditionalImmunization1Name"] !== undefined ? result["485AdditionalImmunization1Name"].Answer : " "));
            $('input[name=Recertification_485AdditionalImmunization1][value=' + (result["485AdditionalImmunization1"] != null && result["485AdditionalImmunization1"] != undefined ? result["485AdditionalImmunization1"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485AdditionalImmunization1]:checked').val() == 'Yes') {
                $("#Recertification_485AdditionalImmunization1Date").val((result["485AdditionalImmunization1Date"] !== null && result["485AdditionalImmunization1Date"] !== undefined ? result["485AdditionalImmunization1Date"].Answer : " "));

            }

            $("#Recertification_485AdditionalImmunization2Name").val((result["485AdditionalImmunization2Name"] !== null && result["485AdditionalImmunization2Name"] !== undefined ? result["485AdditionalImmunization2Name"].Answer : " "));
            $('input[name=Recertification_485AdditionalImmunization2][value=' + (result["485AdditionalImmunization2"] != null && result["485AdditionalImmunization2"] != undefined ? result["485AdditionalImmunization2"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485AdditionalImmunization2]:checked').val() == 'Yes') {
                $("#Recertification_485AdditionalImmunization2Date").val((result["485AdditionalImmunization2Date"] !== null && result["485AdditionalImmunization2Date"] !== undefined ? result["485AdditionalImmunization2Date"].Answer : " "));

            }

            $("#Recertification_485ImmunizationComments").val((result["485ImmunizationComments"] !== null && result["485ImmunizationComments"] !== undefined ? result["485ImmunizationComments"].Answer : " "));

            $("#Recertification_485LastCholesterolLevel").val((result["485LastCholesterolLevel"] !== null && result["485LastCholesterolLevel"] !== undefined ? result["485LastCholesterolLevel"].Answer : " "));
            $("#Recertification_485LastMammogram").val((result["485LastMammogram"] !== null && result["485LastMammogram"] !== undefined ? result["485LastMammogram"].Answer : " "));
            $('input[name=Recertification_485PerformMonthlySelfBreastExams][value=' + (result["485PerformMonthlySelfBreastExams"] != null && result["485PerformMonthlySelfBreastExams"] != undefined ? result["485PerformMonthlySelfBreastExams"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_485LastPapSmear").val((result["485LastPapSmear"] !== null && result["485LastPapSmear"] !== undefined ? result["485LastPapSmear"].Answer : " "));
            $("#Recertification_485LastPSA").val((result["485LastPSA"] !== null && result["485LastPSA"] !== undefined ? result["485LastPSA"].Answer : " "));
            $("#Recertification_485LastProstateExam").val((result["485LastProstateExam"] !== null && result["485LastProstateExam"] !== undefined ? result["485LastProstateExam"].Answer : " "));
            $("#Recertification_485LastColonoscopy").val((result["485LastColonoscopy"] !== null && result["485LastColonoscopy"] !== undefined ? result["485LastColonoscopy"].Answer : " "));

            var riskAssessmentIntervention = result["485RiskAssessmentIntervention"];
            if (riskAssessmentIntervention !== null && riskAssessmentIntervention != undefined && riskAssessmentIntervention.Answer != null) {
                var riskAssessmentInterventionArray = (riskAssessmentIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < riskAssessmentInterventionArray.length; i++) {
                    $('input[name=Recertification_485RiskAssessmentIntervention][value=' + riskAssessmentInterventionArray[i] + ']').attr('checked', true);
                    if (riskAssessmentInterventionArray[i] == 4) {
                        $("#Recertification_485AdministerVaccinationDetails").val((result["485AdministerVaccinationDetails"] !== null && result["485AdministerVaccinationDetails"] !== undefined ? result["485AdministerVaccinationDetails"].Answer : " "));
                    }
                    if (riskAssessmentInterventionArray[i] == 5) {
                        $("#Recertification_485AdministerPneumococcalVaccineDetails").val((result["485AdministerPneumococcalVaccineDetails"] !== null && result["485AdministerPneumococcalVaccineDetails"] !== undefined ? result["485AdministerPneumococcalVaccineDetails"].Answer : " "));

                    }
                }
            }

            $("#Recertification_485RiskInterventionTemplates").val(result["485RiskInterventionTemplates"] != null && result["485RiskInterventionTemplates"] != undefined ? result["485RiskInterventionTemplates"].Answer : "");
            $("#Recertification_485RiskInterventionComments").val((result["485RiskInterventionComments"] !== null && result["485RiskInterventionComments"] !== undefined ? result["485RiskInterventionComments"].Answer : " "));

            var riskAssessmentGoals = result["485RiskAssessmentGoals"];
            if (riskAssessmentGoals !== null && riskAssessmentGoals != undefined && riskAssessmentGoals.Answer != null) {
                var riskAssessmentGoalsArray = (riskAssessmentGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < riskAssessmentGoalsArray.length; i++) {
                    $('input[name=Recertification_485RiskAssessmentGoals][value=' + riskAssessmentGoalsArray[i] + ']').attr('checked', true);
                    if (riskAssessmentGoalsArray[i] == 2) {
                        $("#Recertification_485VerbalizeEmergencyPlanPerson").val((result["485VerbalizeEmergencyPlanPerson"] !== null && result["485VerbalizeEmergencyPlanPerson"] !== undefined ? result["485VerbalizeEmergencyPlanPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeEmergencyPlanDate").val((result["485VerbalizeEmergencyPlanDate"] !== null && result["485VerbalizeEmergencyPlanDate"] !== undefined ? result["485VerbalizeEmergencyPlanDate"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485RiskGoalTemplates").val(result["485RiskGoalTemplates"] != null && result["485RiskGoalTemplates"] != undefined ? result["485RiskGoalTemplates"].Answer : "");
            $("#Recertification_485RiskGoalComments").val((result["485RiskGoalComments"] !== null && result["485RiskGoalComments"] !== undefined ? result["485RiskGoalComments"].Answer : " "));

            $('input[name=Recertification_485Prognosis][value=' + (result["485Prognosis"] != null && result["485Prognosis"] != undefined ? result["485Prognosis"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_485AdvancedDirectives][value=' + (result["485AdvancedDirectives"] != null && result["485AdvancedDirectives"] != undefined ? result["485AdvancedDirectives"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_485AdvancedDirectivesIntent][value=' + (result["485AdvancedDirectivesIntent"] != null && result["485AdvancedDirectivesIntent"] != undefined ? result["485AdvancedDirectivesIntent"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_485AdvancedDirectivesIntent]:checked').val() == 'Other') {
                $("#Recertification_485AdvancedDirectivesIntentOther").val((result["485AdvancedDirectivesIntentOther"] !== null && result["485AdvancedDirectivesIntentOther"] !== undefined ? result["485AdvancedDirectivesIntentOther"].Answer : " "));
            }

            $('input[name=Recertification_485AdvancedDirectivesCopyOnFile][value=' + (result["485AdvancedDirectivesCopyOnFile"] != null && result["485AdvancedDirectivesCopyOnFile"] != undefined ? result["485AdvancedDirectivesCopyOnFile"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_485AdvancedDirectivesWrittenAndVerbal][value=' + (result["485AdvancedDirectivesWrittenAndVerbal"] != null && result["485AdvancedDirectivesWrittenAndVerbal"] != undefined ? result["485AdvancedDirectivesWrittenAndVerbal"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericPatientDNR][value=' + (result["GenericPatientDNR"] != null && result["GenericPatientDNR"] != undefined ? result["GenericPatientDNR"].Answer : "") + ']').attr('checked', true);

            var functionLimitations = result["485FunctionLimitations"];
            if (functionLimitations !== null && functionLimitations != undefined && functionLimitations.Answer != null) {
                var functionLimitationsArray = (functionLimitations.Answer).split(',');
                var i = 0;
                for (i = 0; i < functionLimitationsArray.length; i++) {
                    $('input[name=Recertification_485FunctionLimitations][value=' + functionLimitationsArray[i] + ']').attr('checked', true);

                    if (functionLimitationsArray[i] == "Other") {

                        $("#Recertification_485FunctionLimitationsOther").val((result["485FunctionLimitationsOther"] !== null && result["485FunctionLimitationsOther"] !== undefined ? result["485FunctionLimitationsOther"].Answer : " "));
                    }
                }
            }

            $('input[name=Recertification_M1100LivingSituation][value=' + (result["M1100LivingSituation"] != null && result["M1100LivingSituation"] != undefined ? result["M1100LivingSituation"].Answer : "") + ']').attr('checked', true);
            var ADLTypeOfAssistance = result["GenericADLTypeOfAssistance"];
            if (ADLTypeOfAssistance !== null && ADLTypeOfAssistance != undefined && ADLTypeOfAssistance.Answer != null) {
                var ADLTypeOfAssistanceArray = (ADLTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < ADLTypeOfAssistanceArray.length; i++) {
                    $('input[name=Recertification_GenericADLTypeOfAssistance][value=' + ADLTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var IADLTypeOfAssistance = result["GenericIADLTypeOfAssistance"];
            if (IADLTypeOfAssistance !== null && IADLTypeOfAssistance != undefined && IADLTypeOfAssistance.Answer != null) {
                var IADLTypeOfAssistanceArray = (IADLTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < IADLTypeOfAssistanceArray.length; i++) {
                    $('input[name=Recertification_GenericIADLTypeOfAssistance][value=' + IADLTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var psychoSupportTypeOfAssistance = result["GenericPsychoSupportTypeOfAssistance"];
            if (psychoSupportTypeOfAssistance !== null && psychoSupportTypeOfAssistance != undefined && psychoSupportTypeOfAssistance.Answer != null) {
                var psychoSupportTypeOfAssistanceArray = (psychoSupportTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < psychoSupportTypeOfAssistanceArray.length; i++) {
                    $('input[name=Recertification_GenericPsychoSupportTypeOfAssistance][value=' + psychoSupportTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var medicalApptTypeOfAssistance = result["GenericMedicalApptTypeOfAssistance"];
            if (medicalApptTypeOfAssistance !== null && medicalApptTypeOfAssistance != undefined && medicalApptTypeOfAssistance.Answer != null) {
                var medicalApptTypeOfAssistanceArray = (medicalApptTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicalApptTypeOfAssistanceArray.length; i++) {
                    $('input[name=Recertification_GenericMedicalApptTypeOfAssistance][value=' + medicalApptTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var financeManagmentTypeOfAssistance = result["GenericFinanceManagmentTypeOfAssistance"];
            if (financeManagmentTypeOfAssistance !== null && financeManagmentTypeOfAssistance != undefined && financeManagmentTypeOfAssistance.Answer != null) {
                var financeManagmentTypeOfAssistanceArray = (financeManagmentTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < financeManagmentTypeOfAssistanceArray.length; i++) {
                    $('input[name=Recertification_GenericFinanceManagmentTypeOfAssistance][value=' + financeManagmentTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            $("#Recertification_GenericTypeOfAssistanceComments").val((result["GenericTypeOfAssistanceComments"] !== null && result["GenericTypeOfAssistanceComments"] !== undefined ? result["GenericTypeOfAssistanceComments"].Answer : " "));
            $("#Recertification_GenericOrgProvidingAssistanceNames").val((result["GenericOrgProvidingAssistanceNames"] !== null && result["GenericOrgProvidingAssistanceNames"] !== undefined ? result["GenericOrgProvidingAssistanceNames"].Answer : " "));

            $('input[name=Recertification_GenericCommunityResourceInfoNeeded][value=' + (result["GenericCommunityResourceInfoNeeded"] != null && result["GenericCommunityResourceInfoNeeded"] != undefined ? result["GenericCommunityResourceInfoNeeded"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericAbilityHandleFinance][value=' + (result["GenericAbilityHandleFinance"] != null && result["GenericAbilityHandleFinance"] != undefined ? result["GenericAbilityHandleFinance"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericAlteredAffect][value=' + (result["GenericAlteredAffect"] != null && result["GenericAlteredAffect"] != undefined ? result["GenericAlteredAffect"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericAlteredAffectComments").val((result["GenericAlteredAffectComments"] !== null && result["GenericAlteredAffectComments"] !== undefined ? result["GenericAlteredAffectComments"].Answer : " "));
            $('input[name=Recertification_GenericSuicidalIdeation][value=' + (result["GenericSuicidalIdeation"] != null && result["GenericSuicidalIdeation"] != undefined ? result["GenericSuicidalIdeation"].Answer : "") + ']').attr('checked', true);
            var suspected = result["GenericSuspected"];
            if (suspected !== null && suspected != undefined && suspected.Answer != null) {
                var suspectedArray = (suspected.Answer).split(',');
                var i = 0;
                for (i = 0; i < suspectedArray.length; i++) {
                    $('input[name=Recertification_GenericSuspected][value=' + suspectedArray[i] + ']').attr('checked', true);
                }
            }

            $("#Recertification_GenericMSWIndicatedForWhat").val((result["GenericMSWIndicatedForWhat"] !== null && result["GenericMSWIndicatedForWhat"] !== undefined ? result["GenericMSWIndicatedForWhat"].Answer : " "));
            $('input[name=Recertification_GenericMSWIndicatedFor][value=' + (result["GenericMSWIndicatedFor"] != null && result["GenericMSWIndicatedFor"] != undefined ? result["GenericMSWIndicatedFor"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericCoordinatorNotified][value=' + (result["GenericCoordinatorNotified"] != null && result["GenericCoordinatorNotified"] != undefined ? result["GenericCoordinatorNotified"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericNoHazardsIdentified][value=' + (result["GenericNoHazardsIdentified"] != null && result["GenericNoHazardsIdentified"] != undefined ? result["GenericNoHazardsIdentified"].Answer : "") + ']').attr('checked', true);
            var hazardsIdentified = result["GenericHazardsIdentified"];
            if (hazardsIdentified !== null && hazardsIdentified != undefined && hazardsIdentified.Answer != null) {
                var hazardsIdentifiedArray = (hazardsIdentified.Answer).split(',');
                var i = 0;
                for (i = 0; i < hazardsIdentifiedArray.length; i++) {
                    $('input[name=Recertification_GenericHazardsIdentified][value=' + hazardsIdentifiedArray[i] + ']').attr('checked', true);

                }
            }

            $("#Recertification_GenericOtherHazards").val((result["GenericOtherHazards"] !== null && result["GenericOtherHazards"] !== undefined ? result["GenericOtherHazards"].Answer : " "));
            $("#Recertification_GenericHazardsComments").val((result["GenericHazardsComments"] !== null && result["GenericHazardsComments"] !== undefined ? result["GenericHazardsComments"].Answer : " "));

            $('input[name=Recertification_GenericUsingOxygen][value=' + (result["GenericUsingOxygen"] != null && result["GenericUsingOxygen"] != undefined ? result["GenericUsingOxygen"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericNoSmokingSigns][value=' + (result["GenericNoSmokingSigns"] != null && result["GenericNoSmokingSigns"] != undefined ? result["GenericNoSmokingSigns"].Answer : "") + ']').attr('checked', true);
            var noSmokingSignsPerson = result["GenericNoSmokingSignsPerson"];
            if (noSmokingSignsPerson !== null && noSmokingSignsPerson != undefined && noSmokingSignsPerson.Answer != null) {
                var noSmokingSignsPersonArray = (noSmokingSignsPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < noSmokingSignsPersonArray.length; i++) {
                    $('input[name=Recertification_GenericNoSmokingSignsPerson][value=' + noSmokingSignsPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericSmokeWithOxygenInUser][value=' + (result["GenericSmokeWithOxygenInUser"] != null && result["GenericSmokeWithOxygenInUser"] != undefined ? result["GenericSmokeWithOxygenInUser"].Answer : "") + ']').attr('checked', true);
            var smokeWithOxygenInUserPerson = result["GenericSmokeWithOxygenInUserPerson"];
            if (smokeWithOxygenInUserPerson !== null && smokeWithOxygenInUserPerson != undefined && smokeWithOxygenInUserPerson.Answer != null) {
                var smokeWithOxygenInUserPersonArray = (smokeWithOxygenInUserPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < smokeWithOxygenInUserPersonArray.length; i++) {
                    $('input[name=Recertification_GenericSmokeWithOxygenInUserPerson][value=' + smokeWithOxygenInUserPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericSmokeDetectorsWorking][value=' + (result["GenericSmokeDetectorsWorking"] != null && result["GenericSmokeDetectorsWorking"] != undefined ? result["GenericSmokeDetectorsWorking"].Answer : "") + ']').attr('checked', true);
            var smokeDetectorsWorkingPerson = result["GenericSmokeDetectorsWorkingPerson"];
            if (smokeDetectorsWorkingPerson !== null && smokeDetectorsWorkingPerson != undefined && smokeDetectorsWorkingPerson.Answer != null) {
                var smokeDetectorsWorkingPersonArray = (smokeDetectorsWorkingPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < smokeDetectorsWorkingPersonArray.length; i++) {
                    $('input[name=Recertification_GenericSmokeDetectorsWorkingPerson][value=' + smokeDetectorsWorkingPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericFireExtinguisherWorking][value=' + (result["GenericFireExtinguisherWorking"] != null && result["GenericFireExtinguisherWorking"] != undefined ? result["GenericFireExtinguisherWorking"].Answer : "") + ']').attr('checked', true);
            var fireExtinguisherWorkingPerson = result["GenericFireExtinguisherWorkingPerson"];
            if (fireExtinguisherWorkingPerson !== null && fireExtinguisherWorkingPerson != undefined && fireExtinguisherWorkingPerson.Answer != null) {
                var fireExtinguisherWorkingPersonArray = (fireExtinguisherWorkingPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < fireExtinguisherWorkingPersonArray.length; i++) {
                    $('input[name=Recertification_GenericFireExtinguisherWorkingPerson][value=' + fireExtinguisherWorkingPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericOxygenCyclindersSafe][value=' + (result["GenericOxygenCyclindersSafe"] != null && result["GenericOxygenCyclindersSafe"] != undefined ? result["GenericOxygenCyclindersSafe"].Answer : "") + ']').attr('checked', true);
            var oxygenCyclindersSafePerson = result["GenericOxygenCyclindersSafePerson"];
            if (oxygenCyclindersSafePerson !== null && oxygenCyclindersSafePerson != undefined && oxygenCyclindersSafePerson.Answer != null) {
                var oxygenCyclindersSafePersonArray = (oxygenCyclindersSafePerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < oxygenCyclindersSafePersonArray.length; i++) {
                    $('input[name=Recertification_GenericOxygenCyclindersSafePerson][value=' + oxygenCyclindersSafePersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericElectricalCordsIntact][value=' + (result["GenericElectricalCordsIntact"] != null && result["GenericElectricalCordsIntact"] != undefined ? result["GenericElectricalCordsIntact"].Answer : "") + ']').attr('checked', true);
            var electricalCordsIntactPerson = result["GenericElectricalCordsIntactPerson"];
            if (electricalCordsIntactPerson !== null && electricalCordsIntactPerson != undefined && electricalCordsIntactPerson.Answer != null) {
                var electricalCordsIntactPersonArray = (electricalCordsIntactPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < electricalCordsIntactPersonArray.length; i++) {
                    $('input[name=Recertification_GenericElectricalCordsIntactPerson][value=' + electricalCordsIntactPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericEvacuationPlan][value=' + (result["GenericEvacuationPlan"] != null && result["GenericEvacuationPlan"] != undefined ? result["GenericEvacuationPlan"].Answer : "") + ']').attr('checked', true);
            var evacuationPlanPerson = result["GenericEvacuationPlanPerson"];
            if (evacuationPlanPerson !== null && evacuationPlanPerson != undefined && evacuationPlanPerson.Answer != null) {
                var evacuationPlanPersonArray = (evacuationPlanPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < evacuationPlanPersonArray.length; i++) {
                    $('input[name=Recertification_GenericEvacuationPlanPerson][value=' + evacuationPlanPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericAerosolsSafeWhenOxygenInUse][value=' + (result["GenericAerosolsSafeWhenOxygenInUse"] != null && result["GenericAerosolsSafeWhenOxygenInUse"] != undefined ? result["GenericAerosolsSafeWhenOxygenInUse"].Answer : "") + ']').attr('checked', true);
            var aerosolsSafeWhenOxygenInUsePerson = result["GenericAerosolsSafeWhenOxygenInUsePerson"];
            if (aerosolsSafeWhenOxygenInUsePerson !== null && aerosolsSafeWhenOxygenInUsePerson != undefined && aerosolsSafeWhenOxygenInUsePerson.Answer != null) {
                var aerosolsSafeWhenOxygenInUsePersonArray = (aerosolsSafeWhenOxygenInUsePerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < aerosolsSafeWhenOxygenInUsePersonArray.length; i++) {
                    $('input[name=Recertification_GenericAerosolsSafeWhenOxygenInUsePerson][value=' + aerosolsSafeWhenOxygenInUsePersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericPetroleumNearOxygen][value=' + (result["GenericPetroleumNearOxygen"] != null && result["GenericPetroleumNearOxygen"] != undefined ? result["GenericPetroleumNearOxygen"].Answer : "") + ']').attr('checked', true);
            var petroleumNearOxygenPerson = result["GenericPetroleumNearOxygenPerson"];
            if (petroleumNearOxygenPerson !== null && petroleumNearOxygenPerson != undefined && petroleumNearOxygenPerson.Answer != null) {
                var petroleumNearOxygenPersonArray = (petroleumNearOxygenPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < petroleumNearOxygenPersonArray.length; i++) {
                    $('input[name=Recertification_GenericPetroleumNearOxygenPerson][value=' + petroleumNearOxygenPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericWaterbasedBodyLotion][value=' + (result["GenericWaterbasedBodyLotion"] != null && result["GenericWaterbasedBodyLotion"] != undefined ? result["GenericWaterbasedBodyLotion"].Answer : "") + ']').attr('checked', true);
            var waterbasedBodyLotionPerson = result["GenericWaterbasedBodyLotionPerson"];
            if (waterbasedBodyLotionPerson !== null && waterbasedBodyLotionPerson != undefined && waterbasedBodyLotionPerson.Answer != null) {
                var waterbasedBodyLotionPersonArray = (waterbasedBodyLotionPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < waterbasedBodyLotionPersonArray.length; i++) {
                    $('input[name=Recertification_GenericWaterbasedBodyLotionPerson][value=' + waterbasedBodyLotionPersonArray[i] + ']').attr('checked', true);

                }
            }

            var safetyMeasures = result["485SafetyMeasures"];
            if (safetyMeasures !== null && safetyMeasures != undefined && safetyMeasures.Answer != null) {
                var safetyMeasuresArray = (safetyMeasures.Answer).split(',');
                var i = 0;
                for (i = 0; i < safetyMeasuresArray.length; i++) {
                    $('input[name=Recertification_485SafetyMeasures][value=' + safetyMeasuresArray[i] + ']').attr('checked', true);

                }
            }

            $("#Recertification_485OtherSafetyMeasures").val((result["485OtherSafetyMeasures"] !== null && result["485OtherSafetyMeasures"] !== undefined ? result["485OtherSafetyMeasures"].Answer : " "));
            $("#Recertification_485TriageRiskCode").val((result["485TriageRiskCode"] !== null && result["485TriageRiskCode"] !== undefined ? result["485TriageRiskCode"].Answer : " "));
            $("#Recertification_485DisasterCode").val((result["485DisasterCode"] !== null && result["485DisasterCode"] !== undefined ? result["485DisasterCode"].Answer : " "));
            $("#Recertification_485SafetyMeasuresComments").val((result["485SafetyMeasuresComments"] !== null && result["485SafetyMeasuresComments"] !== undefined ? result["485SafetyMeasuresComments"].Answer : " "));

            $("#Recertification_GenericPrimaryLanguage").val((result["GenericPrimaryLanguage"] !== null && result["GenericPrimaryLanguage"] !== undefined ? result["GenericPrimaryLanguage"].Answer : " "));
            $('input[name=Recertification_GenericCulturalPractices][value=' + (result["GenericCulturalPractices"] != null && result["GenericCulturalPractices"] != undefined ? result["GenericCulturalPractices"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericCulturalPracticesDetails").val((result["GenericCulturalPracticesDetails"] !== null && result["GenericCulturalPracticesDetails"] !== undefined ? result["GenericCulturalPracticesDetails"].Answer : " "));
            $('input[name=Recertification_GenericReligionImportant][value=' + (result["GenericReligionImportant"] != null && result["GenericReligionImportant"] != undefined ? result["GenericReligionImportant"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericReligiousPreference").val((result["GenericReligiousPreference"] !== null && result["GenericReligiousPreference"] !== undefined ? result["GenericReligiousPreference"].Answer : " "));

            var useOfInterpreter = result["GenericUseOfInterpreter"];
            if (useOfInterpreter !== null && useOfInterpreter != undefined && useOfInterpreter.Answer != null) {
                var useOfInterpreterArray = (useOfInterpreter.Answer).split(',');
                var i = 0;
                for (i = 0; i < useOfInterpreterArray.length; i++) {
                    $('input[name=Recertification_GenericUseOfInterpreter][value=' + useOfInterpreterArray[i] + ']').attr('checked', true);
                    if (useOfInterpreterArray[i] == 4) {
                        $("#Recertification_GenericUseOfInterpreterOtherDetails").val((result["GenericUseOfInterpreterOtherDetails"] !== null && result["GenericUseOfInterpreterOtherDetails"] !== undefined ? result["GenericUseOfInterpreterOtherDetails"].Answer : " "));

                    }
                }
            }

            $("#Recertification_GenericPrimaryEmotionalSupport").val((result["GenericPrimaryEmotionalSupport"] !== null && result["GenericPrimaryEmotionalSupport"] !== undefined ? result["GenericPrimaryEmotionalSupport"].Answer : " "));
            $('input[name=Recertification_GenericIsHomeBound][value=' + (result["GenericIsHomeBound"] != null && result["GenericIsHomeBound"] != undefined ? result["GenericIsHomeBound"].Answer : "") + ']').attr('checked', true);
            var homeBoundReason = result["GenericHomeBoundReason"];
            if (homeBoundReason !== null && homeBoundReason != undefined && homeBoundReason.Answer != null) {
                var homeBoundReasonArray = (homeBoundReason.Answer).split(',');
                var i = 0;
                for (i = 0; i < homeBoundReasonArray.length; i++) {
                    $('input[name=Recertification_GenericHomeBoundReason][value=' + homeBoundReasonArray[i] + ']').attr('checked', true);
                    if (homeBoundReasonArray[i] == 6) {
                        $("#Recertification_GenericOtherHomeBoundDetails").val((result["GenericOtherHomeBoundDetails"] !== null && result["GenericOtherHomeBoundDetails"] !== undefined ? result["GenericOtherHomeBoundDetails"].Answer : " "));

                    }
                }
            }

            var eyes = result["GenericEyes"];
            if (eyes !== null && eyes != undefined && eyes.Answer != null) {
                var eyesArray = (eyes.Answer).split(',');
                var i = 0;
                for (i = 0; i < eyesArray.length; i++) {
                    $('input[name=Recertification_GenericEyes][value=' + eyesArray[i] + ']').attr('checked', true);
                    if (eyesArray[i] == 13) {
                        $("#Recertification_GenericEyesOtherDetails").val((result["GenericEyesOtherDetails"] !== null && result["GenericEyesOtherDetails"] !== undefined ? result["GenericEyesOtherDetails"].Answer : " "));

                    }
                }
            }
            $("#Recertification_GenericEyesLastEyeExamDate").val((result["GenericEyesLastEyeExamDate"] !== null && result["GenericEyesLastEyeExamDate"] !== undefined ? result["GenericEyesLastEyeExamDate"].Answer : " "));

            var ears = result["GenericEars"];
            if (ears !== null && ears != undefined && ears.Answer != null) {
                var earsArray = (ears.Answer).split(',');
                var i = 0;
                for (i = 0; i < earsArray.length; i++) {
                    $('input[name=Recertification_GenericEars][value=' + earsArray[i] + ']').attr('checked', true);
                    if (earsArray[i] == 2) {

                        var earsHearingImpairedPosition = result["GenericEarsHearingImpairedPosition"];
                        if (earsHearingImpairedPosition !== null && earsHearingImpairedPosition != undefined && earsHearingImpairedPosition.Answer != null) {
                            var earsHearingImpairedPositionArray = (earsHearingImpairedPosition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < earsHearingImpairedPositionArray.length; j++) {
                                $('input[name=Recertification_GenericEarsHearingImpairedPosition][value=' + earsHearingImpairedPositionArray[j] + ']').attr('checked', true);
                            }
                        }
                    }
                    if (earsArray[i] == 6) {

                        var earsHearingAidsPosition = result["GenericEarsHearingAidsPosition"];
                        if (earsHearingAidsPosition !== null && earsHearingAidsPosition != undefined && earsHearingAidsPosition.Answer != null) {
                            var earsHearingAidsPositionArray = (earsHearingAidsPosition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < earsHearingAidsPositionArray.length; j++) {
                                $('input[name=Recertification_GenericEarsHearingAidsPosition][value=' + earsHearingAidsPositionArray[j] + ']').attr('checked', true);
                            }
                        }
                    }

                }
            }

            var nose = result["GenericNose"];
            if (nose !== null && nose != undefined && nose.Answer != null) {
                var noseArray = (nose.Answer).split(',');
                var i = 0;
                for (i = 0; i < noseArray.length; i++) {
                    $('input[name=Recertification_GenericNose][value=' + noseArray[i] + ']').attr('checked', true);
                    if (noseArray[i] == 4) {
                        $("#Recertification_GenericNoseBleedsFrequency").val((result["GenericNoseBleedsFrequency"] !== null && result["GenericNoseBleedsFrequency"] !== undefined ? result["GenericNoseBleedsFrequency"].Answer : " "));

                    }
                    if (noseArray[i] == 5) {
                        $("#Recertification_GenericNoseOtherDetails").val((result["GenericNoseOtherDetails"] !== null && result["GenericNoseOtherDetails"] !== undefined ? result["GenericNoseOtherDetails"].Answer : " "));

                    }
                }
            }

            var sensoryStatusIntervention = result["485SensoryStatusIntervention"];
            if (sensoryStatusIntervention !== null && sensoryStatusIntervention != undefined && sensoryStatusIntervention.Answer != null) {
                var sensoryStatusInterventionArray = (sensoryStatusIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < sensoryStatusInterventionArray.length; i++) {
                    $('input[name=Recertification_485SensoryStatusIntervention][value=' + sensoryStatusInterventionArray[i] + ']').attr('checked', true);
                    if (sensoryStatusInterventionArray[i] == 1) {
                        $("#Recertification_485AdministerEarMedicationOrder").val((result["485AdministerEarMedicationOrder"] !== null && result["485AdministerEarMedicationOrder"] !== undefined ? result["485AdministerEarMedicationOrder"].Answer : " "));
                    }
                    if (sensoryStatusInterventionArray[i] == 2) {
                        $("#Recertification_485InstillOphathalmicMedicationOrders").val((result["485InstillOphathalmicMedicationOrders"] !== null && result["485InstillOphathalmicMedicationOrders"] !== undefined ? result["485InstillOphathalmicMedicationOrders"].Answer : " "));

                    }
                    if (sensoryStatusInterventionArray[i] == 3) {
                        $("#Recertification_485STEvaluateSensoryStatusFrequency").val((result["485STEvaluateSensoryStatusFrequency"] !== null && result["485STEvaluateSensoryStatusFrequency"] !== undefined ? result["485STEvaluateSensoryStatusFrequency"].Answer : " "));
                        $("#Recertification_485STEvaluateSensoryStatusEvalDate").val((result["485STEvaluateSensoryStatusEvalDate"] !== null && result["485STEvaluateSensoryStatusEvalDate"] !== undefined ? result["485STEvaluateSensoryStatusEvalDate"].Answer : " "));

                    }
                }
            }

            $("#Recertification_485SensoryStatusOrderTemplates").val(result["485SensoryStatusOrderTemplates"] != null && result["485SensoryStatusOrderTemplates"] != undefined ? result["485SensoryStatusOrderTemplates"].Answer : "");
            $("#Recertification_485SensoryStatusInterventionComments").val((result["485SensoryStatusInterventionComments"] !== null && result["485SensoryStatusInterventionComments"] !== undefined ? result["485SensoryStatusInterventionComments"].Answer : " "));

            $("#Recertification_485SensoryStatusGoalTemplates").val(result["485SensoryStatusGoalTemplates"] != null && result["485SensoryStatusGoalTemplates"] != undefined ? result["485SensoryStatusGoalTemplates"].Answer : "");
            $("#Recertification_485SensoryStatusGoalComments").val((result["485SensoryStatusGoalComments"] !== null && result["485SensoryStatusGoalComments"] !== undefined ? result["485SensoryStatusGoalComments"].Answer : " "));

            $("#Recertification_GenericPainOnSetDate").val(result["GenericPainOnSetDate"] != null && result["GenericPainOnSetDate"] != undefined ? result["GenericPainOnSetDate"].Answer : "");
            $("#Recertification_GenericLocationOfPain").val((result["GenericLocationOfPain"] !== null && result["GenericLocationOfPain"] !== undefined ? result["GenericLocationOfPain"].Answer : " "));
            $("#Recertification_GenericIntensityOfPain").val((result["GenericIntensityOfPain"] !== null && result["GenericIntensityOfPain"] !== undefined ? result["GenericIntensityOfPain"].Answer : " "));
            $("#Recertification_GenericDurationOfPain").val((result["GenericDurationOfPain"] !== null && result["GenericDurationOfPain"] !== undefined ? result["GenericDurationOfPain"].Answer : " "));

            $("#Recertification_GenericQualityOfPain").val(result["GenericQualityOfPain"] != null && result["GenericQualityOfPain"] != undefined ? result["GenericQualityOfPain"].Answer : "");
            $("#Recertification_GenericWhatMakesPainWorse").val((result["GenericWhatMakesPainWorse"] !== null && result["GenericWhatMakesPainWorse"] !== undefined ? result["GenericWhatMakesPainWorse"].Answer : " "));
            $("#Recertification_GenericWhatMakesPainBetter").val((result["GenericWhatMakesPainBetter"] !== null && result["GenericWhatMakesPainBetter"] !== undefined ? result["GenericWhatMakesPainBetter"].Answer : " "));
            $("#Recertification_GenericReliefRatingOfPain").val((result["GenericReliefRatingOfPain"] !== null && result["GenericReliefRatingOfPain"] !== undefined ? result["GenericReliefRatingOfPain"].Answer : " "));

            $("#Recertification_GenericPainMedication").val(result["GenericPainMedication"] != null && result["GenericPainMedication"] != undefined ? result["GenericPainMedication"].Answer : "");
            $("#Recertification_GenericMedicationEffectiveness").val((result["GenericMedicationEffectiveness"] !== null && result["GenericMedicationEffectiveness"] !== undefined ? result["GenericMedicationEffectiveness"].Answer : " "));
            $("#Recertification_GenericMedicationAdverseEffects").val((result["GenericMedicationAdverseEffects"] !== null && result["GenericMedicationAdverseEffects"] !== undefined ? result["GenericMedicationAdverseEffects"].Answer : " "));
            $("#Recertification_GenericPatientPainGoal").val((result["GenericPatientPainGoal"] !== null && result["GenericPatientPainGoal"] !== undefined ? result["GenericPatientPainGoal"].Answer : " "));

            var painIntervention = result["485PainInterventions"];
            if (painIntervention !== null && painIntervention != undefined && painIntervention.Answer != null) {
                var painInterventionArray = (painIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < painInterventionArray.length; i++) {
                    $('input[name=Recertification_485PainInterventions][value=' + painInterventionArray[i] + ']').attr('checked', true);
                    if (painInterventionArray[i] == 5) {
                        $("#Recertification_485PainTooGreatLevel").val((result["485PainTooGreatLevel"] !== null && result["485PainTooGreatLevel"] !== undefined ? result["485PainTooGreatLevel"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485PainOrderTemplates").val((result["485PainOrderTemplates"] !== null && result["485PainOrderTemplates"] !== undefined ? result["485PainOrderTemplates"].Answer : " "));
            $("#Recertification_485PainInterventionComments").val((result["485PainInterventionComments"] !== null && result["485PainInterventionComments"] !== undefined ? result["485PainInterventionComments"].Answer : " "));

            var painGoals = result["485PainGoals"];
            if (painGoals !== null && painGoals != undefined && painGoals.Answer != null) {
                var painGoalsArray = (painGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < painGoalsArray.length; i++) {
                    $('input[name=Recertification_485PainGoals][value=' + painGoalsArray[i] + ']').attr('checked', true);
                    if (painGoalsArray[i] == 1) {
                        $("#Recertification_485PatientVerbalizeMedUseDate").val((result["485PatientVerbalizeMedUseDate"] !== null && result["485PatientVerbalizeMedUseDate"] !== undefined ? result["485PatientVerbalizeMedUseDate"].Answer : " "));
                    }
                    if (painGoalsArray[i] == 2) {
                        $("#Recertification_485AchievePainLevelLessThan").val((result["485AchievePainLevelLessThan"] !== null && result["485AchievePainLevelLessThan"] !== undefined ? result["485AchievePainLevelLessThan"].Answer : " "));
                        $("#Recertification_485AchievePainLevelWeeks").val((result["485AchievePainLevelWeeks"] !== null && result["485AchievePainLevelWeeks"] !== undefined ? result["485AchievePainLevelWeeks"].Answer : " "));
                    }

                }
            }
            $("#Recertification_485PainGoalTemplates").val((result["485PainGoalTemplates"] !== null && result["485PainGoalTemplates"] !== undefined ? result["485PainGoalTemplates"].Answer : " "));
            $("#Recertification_485PainGoalComments").val((result["485PainGoalComments"] !== null && result["485PainGoalComments"] !== undefined ? result["485PainGoalComments"].Answer : " "));

            $("#Recertification_485BradenScaleSensory").val((result["485BradenScaleSensory"] !== null && result["485BradenScaleSensory"] !== undefined ? result["485BradenScaleSensory"].Answer : " "));
            $("#Recertification_485BradenScaleMoisture").val((result["485BradenScaleMoisture"] !== null && result["485BradenScaleMoisture"] !== undefined ? result["485BradenScaleMoisture"].Answer : " "));
            $("#Recertification_485BradenScaleActivity").val(result["485BradenScaleActivity"] != null && result["485BradenScaleActivity"] != undefined ? result["485BradenScaleActivity"].Answer : "");
            $("#Recertification_485BradenScaleMobility").val((result["485BradenScaleMobility"] !== null && result["485BradenScaleMobility"] !== undefined ? result["485BradenScaleMobility"].Answer : " "));
            $("#Recertification_485BradenScaleNutrition").val((result["485BradenScaleNutrition"] !== null && result["485BradenScaleNutrition"] !== undefined ? result["485BradenScaleNutrition"].Answer : " "));
            $("#Recertification_485BradenScaleFriction").val((result["485BradenScaleFriction"] !== null && result["485BradenScaleFriction"] !== undefined ? result["485BradenScaleFriction"].Answer : " "));
            // $("#Recertification_485BradenScaleTotal").val((result["485BradenScaleTotal"] !== null && result["485BradenScaleTotal"] !== undefined ? result["485BradenScaleTotal"].Answer : " "));
            Oasis.BradenScaleScore('Recertification');
            $('input[name=Recertification_GenericSkinTurgor][value=' + (result["GenericSkinTurgor"] != null && result["GenericSkinTurgor"] != undefined ? result["GenericSkinTurgor"].Answer : "") + ']').attr('checked', true);

            var skinColor = result["GenericSkinColor"];
            if (skinColor !== null && skinColor != undefined && skinColor.Answer != null) {
                var skinColorArray = (skinColor.Answer).split(',');
                var i = 0;
                for (i = 0; i < skinColorArray.length; i++) {
                    $('input[name=Recertification_GenericSkinColor][value=' + skinColorArray[i] + ']').attr('checked', true);

                }
            }

            var skin = result["GenericSkin"];
            if (skin !== null && skin != undefined && skin.Answer != null) {
                var skinArray = (skin.Answer).split(',');
                var i = 0;
                for (i = 0; i < skinArray.length; i++) {
                    $('input[name=Recertification_GenericSkin][value=' + skinArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=Recertification_GenericInstructedControlInfections][value=' + (result["GenericInstructedControlInfections"] != null && result["GenericInstructedControlInfections"] != undefined ? result["GenericInstructedControlInfections"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericNails][value=' + (result["GenericNails"] != null && result["GenericNails"] != undefined ? result["GenericNails"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericPressureRelievingDevice][value=' + (result["GenericPressureRelievingDevice"] != null && result["GenericPressureRelievingDevice"] != undefined ? result["GenericPressureRelievingDevice"].Answer : "") + ']').attr('checked', true);

            $("#Recertification_GenericPressureRelievingDeviceType").val((result["GenericPressureRelievingDeviceType"] !== null && result["GenericPressureRelievingDeviceType"] !== undefined ? result["GenericPressureRelievingDeviceType"].Answer : " "));
            $("#Recertification_GenericIntegumentaryStatusComments").val((result["GenericIntegumentaryStatusComments"] !== null && result["GenericIntegumentaryStatusComments"] !== undefined ? result["GenericIntegumentaryStatusComments"].Answer : " "));

            $("#Recertification_GenericWoundLocation1").val((result["GenericWoundLocation1"] !== null && result["GenericWoundLocation1"] !== undefined ? result["GenericWoundLocation1"].Answer : " "));
            $("#Recertification_GenericWoundLocation2").val((result["GenericWoundLocation2"] !== null && result["GenericWoundLocation2"] !== undefined ? result["GenericWoundLocation2"].Answer : " "));
            $("#Recertification_GenericWoundLocation3").val(result["GenericWoundLocation3"] != null && result["GenericWoundLocation3"] != undefined ? result["GenericWoundLocation3"].Answer : "");
            $("#Recertification_GenericWoundLocation4").val((result["GenericWoundLocation4"] !== null && result["GenericWoundLocation4"] !== undefined ? result["GenericWoundLocation4"].Answer : " "));
            $("#Recertification_GenericWoundLocation5").val((result["GenericWoundLocation5"] !== null && result["GenericWoundLocation5"] !== undefined ? result["GenericWoundLocation5"].Answer : " "));

            $("#Recertification_GenericWoundOnsetDate1").val((result["GenericWoundOnsetDate1"] !== null && result["GenericWoundOnsetDate1"] !== undefined ? result["GenericWoundOnsetDate1"].Answer : " "));
            $("#Recertification_GenericWoundOnsetDate2").val((result["GenericWoundOnsetDate2"] !== null && result["GenericWoundOnsetDate2"] !== undefined ? result["GenericWoundOnsetDate2"].Answer : " "));
            $("#Recertification_GenericWoundOnsetDate3").val((result["GenericWoundOnsetDate3"] !== null && result["GenericWoundOnsetDate3"] !== undefined ? result["GenericWoundOnsetDate3"].Answer : " "));
            $("#Recertification_GenericWoundOnsetDate4").val((result["GenericWoundOnsetDate4"] !== null && result["GenericWoundOnsetDate4"] !== undefined ? result["GenericWoundOnsetDate4"].Answer : " "));
            $("#Recertification_GenericWoundOnsetDate5").val(result["GenericWoundOnsetDate5"] != null && result["GenericWoundOnsetDate5"] != undefined ? result["GenericWoundOnsetDate5"].Answer : "");

            $("#Recertification_GenericWoundSize1").val((result["GenericWoundSize1"] !== null && result["GenericWoundSize1"] !== undefined ? result["GenericWoundSize1"].Answer : " "));
            $("#Recertification_GenericWoundSize2").val((result["GenericWoundSize2"] !== null && result["GenericWoundSize2"] !== undefined ? result["GenericWoundSize2"].Answer : " "));
            $("#Recertification_GenericWoundSize3").val((result["GenericWoundSize3"] !== null && result["GenericWoundSize3"] !== undefined ? result["GenericWoundSize3"].Answer : " "));
            $("#Recertification_GenericWoundSize4").val((result["GenericWoundSize4"] !== null && result["GenericWoundSize4"] !== undefined ? result["GenericWoundSize4"].Answer : " "));
            $("#Recertification_GenericWoundSize5").val((result["GenericWoundSize5"] !== null && result["GenericWoundSize5"] !== undefined ? result["GenericWoundSize5"].Answer : " "));

            $("#Recertification_GenericWoundDrainage1").val((result["GenericWoundDrainage1"] !== null && result["GenericWoundDrainage1"] !== undefined ? result["GenericWoundDrainage1"].Answer : " "));
            $("#Recertification_GenericWoundDrainage2").val(result["GenericWoundDrainage2"] != null && result["GenericWoundDrainage2"] != undefined ? result["GenericWoundDrainage2"].Answer : "");
            $("#Recertification_GenericWoundDrainage3").val((result["GenericWoundDrainage3"] !== null && result["GenericWoundDrainage3"] !== undefined ? result["GenericWoundDrainage3"].Answer : " "));
            $("#Recertification_GenericWoundDrainage4").val((result["GenericWoundDrainage4"] !== null && result["GenericWoundDrainage4"] !== undefined ? result["GenericWoundDrainage4"].Answer : " "));
            $("#Recertification_GenericWoundDrainage5").val((result["GenericWoundDrainage5"] !== null && result["GenericWoundDrainage5"] !== undefined ? result["GenericWoundDrainage5"].Answer : " "));

            $("#Recertification_GenericWoundOdor1").val((result["GenericWoundOdor1"] !== null && result["GenericWoundOdor1"] !== undefined ? result["GenericWoundOdor1"].Answer : " "));
            $("#Recertification_GenericWoundOdor2").val((result["GenericWoundOdor2"] !== null && result["GenericWoundOdor2"] !== undefined ? result["GenericWoundOdor2"].Answer : " "));
            $("#Recertification_GenericWoundOdor3").val((result["GenericWoundOdor3"] !== null && result["GenericWoundOdor3"] !== undefined ? result["GenericWoundOdor3"].Answer : " "));
            $("#Recertification_GenericWoundOdor4").val(result["GenericWoundOdor4"] != null && result["GenericWoundOdor4"] != undefined ? result["GenericWoundOdor4"].Answer : "");
            $("#Recertification_GenericWoundOdor5").val((result["GenericWoundOdor5"] !== null && result["GenericWoundOdor5"] !== undefined ? result["GenericWoundOdor5"].Answer : " "));

            $("#Recertification_GenericWoundEtiology1").val((result["GenericWoundEtiology1"] !== null && result["GenericWoundEtiology1"] !== undefined ? result["GenericWoundEtiology1"].Answer : " "));
            $("#Recertification_GenericWoundEtiology2").val((result["GenericWoundEtiology2"] !== null && result["GenericWoundEtiology2"] !== undefined ? result["GenericWoundEtiology2"].Answer : " "));
            $("#Recertification_GenericWoundEtiology3").val((result["GenericWoundEtiology3"] !== null && result["GenericWoundEtiology3"] !== undefined ? result["GenericWoundEtiology3"].Answer : " "));
            $("#Recertification_GenericWoundEtiology4").val((result["GenericWoundEtiology4"] !== null && result["GenericWoundEtiology4"] !== undefined ? result["GenericWoundEtiology4"].Answer : " "));
            $("#Recertification_GenericWoundEtiology5").val((result["GenericWoundEtiology5"] !== null && result["GenericWoundEtiology5"] !== undefined ? result["GenericWoundEtiology5"].Answer : " "));

            $("#Recertification_GenericWoundStage1").val(result["GenericWoundStage1"] != null && result["GenericWoundStage1"] != undefined ? result["GenericWoundStage1"].Answer : "");
            $("#Recertification_GenericWoundStage2").val((result["GenericWoundStage2"] !== null && result["GenericWoundStage2"] !== undefined ? result["GenericWoundStage2"].Answer : " "));
            $("#Recertification_GenericWoundStage3").val((result["GenericWoundStage3"] !== null && result["GenericWoundStage3"] !== undefined ? result["GenericWoundStage3"].Answer : " "));
            $("#Recertification_GenericWoundStage4").val((result["GenericWoundStage4"] !== null && result["GenericWoundStage4"] !== undefined ? result["GenericWoundStage4"].Answer : " "));
            $("#Recertification_GenericWoundStage5").val((result["GenericWoundStage5"] !== null && result["GenericWoundStage5"] !== undefined ? result["GenericWoundStage5"].Answer : " "));

            $("#Recertification_GenericWoundUndermining1").val((result["GenericWoundUndermining1"] !== null && result["GenericWoundUndermining1"] !== undefined ? result["GenericWoundUndermining1"].Answer : " "));
            $("#Recertification_GenericWoundUndermining2").val((result["GenericWoundUndermining2"] !== null && result["GenericWoundUndermining2"] !== undefined ? result["GenericWoundUndermining2"].Answer : " "));
            $("#Recertification_GenericWoundUndermining3").val(result["GenericWoundUndermining3"] != null && result["GenericWoundUndermining3"] != undefined ? result["GenericWoundUndermining3"].Answer : "");
            $("#Recertification_GenericWoundUndermining4").val((result["GenericWoundUndermining4"] !== null && result["GenericWoundUndermining4"] !== undefined ? result["GenericWoundUndermining4"].Answer : " "));
            $("#Recertification_GenericWoundUndermining5").val((result["GenericWoundUndermining5"] !== null && result["GenericWoundUndermining5"] !== undefined ? result["GenericWoundUndermining5"].Answer : " "));

            $("#Recertification_GenericWoundInflamation1").val((result["GenericWoundInflamation1"] !== null && result["GenericWoundInflamation1"] !== undefined ? result["GenericWoundInflamation1"].Answer : " "));
            $("#Recertification_GenericWoundInflamation2").val((result["GenericWoundInflamation2"] !== null && result["GenericWoundInflamation2"] !== undefined ? result["GenericWoundInflamation2"].Answer : " "));
            $("#Recertification_GenericWoundInflamation3").val((result["GenericWoundInflamation3"] !== null && result["GenericWoundInflamation3"] !== undefined ? result["GenericWoundInflamation3"].Answer : " "));
            $("#Recertification_GenericWoundInflamation4").val((result["GenericWoundInflamation4"] !== null && result["GenericWoundInflamation4"] !== undefined ? result["GenericWoundInflamation4"].Answer : " "));
            $("#Recertification_GenericWoundInflamation5").val(result["GenericWoundInflamation5"] != null && result["GenericWoundInflamation5"] != undefined ? result["GenericWoundInflamation5"].Answer : "");

            $("#Recertification_GenericWoundComment").val((result["GenericWoundComment"] !== null && result["GenericWoundComment"] !== undefined ? result["GenericWoundComment"].Answer : " "));

            var integumentaryInterventions = result["485IntegumentaryInterventions"];
            if (integumentaryInterventions !== null && integumentaryInterventions != undefined && integumentaryInterventions.Answer != null) {
                var integumentaryInterventionsArray = (integumentaryInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < integumentaryInterventionsArray.length; i++) {
                    $('input[name=Recertification_485IntegumentaryInterventions][value=' + integumentaryInterventionsArray[i] + ']').attr('checked', true);
                    if (integumentaryInterventionsArray[i] == 1) {
                        $("#Recertification_485InstructTurningRepositionPerson").val((result["485InstructTurningRepositionPerson"] !== null && result["485InstructTurningRepositionPerson"] !== undefined ? result["485InstructTurningRepositionPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 2) {
                        $("#Recertification_485InstructFloatHeelsPerson").val((result["485InstructFloatHeelsPerson"] !== null && result["485InstructFloatHeelsPerson"] !== undefined ? result["485InstructFloatHeelsPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 3) {
                        $("#Recertification_485InstructReduceFrictionPerson").val((result["485InstructReduceFrictionPerson"] !== null && result["485InstructReduceFrictionPerson"] !== undefined ? result["485InstructReduceFrictionPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 4) {
                        $("#Recertification_485InstructPadBonyProminencesPerson").val((result["485InstructPadBonyProminencesPerson"] !== null && result["485InstructPadBonyProminencesPerson"] !== undefined ? result["485InstructPadBonyProminencesPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 5) {
                        $("#Recertification_485InstructWoundCarePerson").val((result["485InstructWoundCarePerson"] !== null && result["485InstructWoundCarePerson"] !== undefined ? result["485InstructWoundCarePerson"].Answer : " "));
                        $("#Recertification_485InstructWoundCarePersonFrequency").val((result["485InstructWoundCarePersonFrequency"] !== null && result["485InstructWoundCarePersonFrequency"] !== undefined ? result["485InstructWoundCarePersonFrequency"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 6) {
                        $("#Recertification_485InstructWoundOtherDetails").val((result["485InstructWoundOtherDetails"] !== null && result["485InstructWoundOtherDetails"] !== undefined ? result["485InstructWoundOtherDetails"].Answer : " "));
                    }

                    if (integumentaryInterventionsArray[i] == 8) {
                        $("#Recertification_485AssessWoundDressingChangeScale").val((result["485AssessWoundDressingChangeScale"] !== null && result["485AssessWoundDressingChangeScale"] !== undefined ? result["485AssessWoundDressingChangeScale"].Answer : " "));

                    }
                    if (integumentaryInterventionsArray[i] == 9) {
                        $("#Recertification_485InstructSignsWoundInfectionPerson").val((result["485InstructSignsWoundInfectionPerson"] !== null && result["485InstructSignsWoundInfectionPerson"] !== undefined ? result["485InstructWoundOtherDetails"].Answer : " "));
                        $("#Recertification_485InstructSignsWoundInfectionScale").val((result["485InstructSignsWoundInfectionScale"] !== null && result["485InstructSignsWoundInfectionScale"] !== undefined ? result["485InstructSignsWoundInfectionScale"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485IntegumentaryOrderTemplates").val((result["485IntegumentaryOrderTemplates"] !== null && result["485IntegumentaryOrderTemplates"] !== undefined ? result["485IntegumentaryOrderTemplates"].Answer : " "));
            $("#Recertification_485IntegumentaryInterventionComments").val((result["485IntegumentaryInterventionComments"] !== null && result["485IntegumentaryInterventionComments"] !== undefined ? result["485IntegumentaryInterventionComments"].Answer : " "));

            var integumentaryGoals = result["485IntegumentaryGoals"];
            if (integumentaryGoals !== null && integumentaryGoals != undefined && integumentaryGoals.Answer != null) {
                var integumentaryGoalsArray = (integumentaryGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < integumentaryGoalsArray.length; i++) {
                    $('input[name=Recertification_485IntegumentaryGoals][value=' + integumentaryGoalsArray[i] + ']').attr('checked', true);
                    if (integumentaryGoalsArray[i] == 1) {
                        $("#Recertification_485HealWithoutComplicationDate").val((result["485HealWithoutComplicationDate"] !== null && result["485HealWithoutComplicationDate"] !== undefined ? result["485HealWithoutComplicationDate"].Answer : " "));
                    }
                    if (integumentaryGoalsArray[i] == 3) {
                        $("#Recertification_485WoundSizeDecreasePercent").val((result["485WoundSizeDecreasePercent"] !== null && result["485WoundSizeDecreasePercent"] !== undefined ? result["485WoundSizeDecreasePercent"].Answer : " "));
                        $("#Recertification_485WoundSizeDecreasePercentDate").val((result["485WoundSizeDecreasePercentDate"] !== null && result["485WoundSizeDecreasePercentDate"] !== undefined ? result["485WoundSizeDecreasePercentDate"].Answer : " "));
                    }

                }
            }
            $("#Recertification_485IntegumentaryGoalTemplates").val((result["485IntegumentaryGoalTemplates"] !== null && result["485IntegumentaryGoalTemplates"] !== undefined ? result["485IntegumentaryGoalTemplates"].Answer : " "));
            $("#Recertification_485IntegumentaryGoalComments").val((result["485IntegumentaryGoalComments"] !== null && result["485IntegumentaryGoalComments"] !== undefined ? result["485IntegumentaryGoalComments"].Answer : " "));

            var respiratoryCondition = result["GenericRespiratoryCondition"];
            if (respiratoryCondition !== null && respiratoryCondition != undefined && respiratoryCondition.Answer != null) {
                var respiratoryConditionArray = (respiratoryCondition.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryConditionArray.length; i++) {
                    $('input[name=Recertification_GenericRespiratoryCondition][value=' + respiratoryConditionArray[i] + ']').attr('checked', true);
                    if (respiratoryConditionArray[i] == 2) {
                        var respiratorySounds = result["GenericRespiratorySounds"];
                        if (respiratorySounds !== null && respiratorySounds != undefined && respiratorySounds.Answer != null) {
                            var respiratorySoundsArray = (respiratorySounds.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < respiratorySoundsArray.length; j++) {
                                $('input[name=Recertification_GenericRespiratorySounds][value=' + respiratorySoundsArray[j] + ']').attr('checked', true);
                                if (respiratorySoundsArray[j] == 1) {
                                    $("#Recertification_GenericLungSoundsCTAText").val((result["GenericLungSoundsCTAText"] !== null && result["GenericLungSoundsCTAText"] !== undefined ? result["GenericLungSoundsCTAText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 2) {
                                    $("#Recertification_GenericLungSoundsRalesText").val((result["GenericLungSoundsRalesText"] !== null && result["GenericLungSoundsRalesText"] !== undefined ? result["GenericLungSoundsRalesText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 3) {
                                    $("#Recertification_GenericLungSoundsRhonchiText").val((result["GenericLungSoundsRhonchiText"] !== null && result["GenericLungSoundsRhonchiText"] !== undefined ? result["GenericLungSoundsRhonchiText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 4) {
                                    $("#Recertification_GenericLungSoundsWheezesText").val((result["GenericLungSoundsWheezesText"] !== null && result["GenericLungSoundsWheezesText"] !== undefined ? result["GenericLungSoundsWheezesText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 5) {
                                    $("#Recertification_GenericLungSoundsCracklesText").val((result["GenericLungSoundsCracklesText"] !== null && result["GenericLungSoundsCracklesText"] !== undefined ? result["GenericLungSoundsCracklesText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 6) {
                                    $("#Recertification_GenericLungSoundsDiminishedText").val((result["GenericLungSoundsDiminishedText"] !== null && result["GenericLungSoundsDiminishedText"] !== undefined ? result["GenericLungSoundsDiminishedText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 7) {
                                    $("#Recertification_GenericLungSoundsAbsentText").val((result["GenericLungSoundsAbsentText"] !== null && result["GenericLungSoundsAbsentText"] !== undefined ? result["GenericLungSoundsAbsentText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 8) {
                                    $("#Recertification_GenericLungSoundsStridorText").val((result["GenericLungSoundsStridorText"] !== null && result["GenericLungSoundsStridorText"] !== undefined ? result["GenericLungSoundsStridorText"].Answer : " "));
                                }

                            }
                        }
                    }
                    if (respiratoryConditionArray[i] == 3) {
                        $("#Recertification_GenericSputumAmount").val((result["GenericSputumAmount"] !== null && result["GenericSputumAmount"] !== undefined ? result["GenericSputumAmount"].Answer : " "));
                        $("#Recertification_GenericSputumColorConsistencyOdor").val((result["GenericSputumColorConsistencyOdor"] !== null && result["GenericSputumColorConsistencyOdor"] !== undefined ? result["GenericSputumColorConsistencyOdor"].Answer : " "));
                    }

                    if (respiratoryConditionArray[i] == 4) {
                        $("#Recertification_Generic02AtText").val((result["Generic02AtText"] !== null && result["Generic02AtText"] !== undefined ? result["Generic02AtText"].Answer : " "));
                        $("#Recertification_GenericLPMVia").val((result["GenericLPMVia"] !== null && result["GenericLPMVia"] !== undefined ? result["GenericLPMVia"].Answer : " "));
                    }
                    if (respiratoryConditionArray[i] == 5) {
                        $("#Recertification_Generic02SatText").val((result["Generic02SatText"] !== null && result["Generic02SatText"] !== undefined ? result["Generic02SatText"].Answer : " "));
                        $("#Recertification_Generic02SatList").val((result["Generic02SatList"] !== null && result["Generic02SatList"] !== undefined ? result["Generic02SatList"].Answer : " "));
                    }

                    if (respiratoryConditionArray[i] == 6) {
                        $("#Recertification_GenericNebulizerText").val((result["GenericNebulizerText"] !== null && result["GenericNebulizerText"] !== undefined ? result["GenericNebulizerText"].Answer : " "));
                    }
                    if (respiratoryConditionArray[i] == 7) {
                        $("#Recertification_GenericCoughList").val((result["GenericCoughList"] !== null && result["GenericCoughList"] !== undefined ? result["GenericCoughList"].Answer : " "));
                        $("#Recertification_GenericRespiratoryComments").val((result["GenericRespiratoryComments"] !== null && result["GenericRespiratoryComments"] !== undefined ? result["GenericRespiratoryComments"].Answer : " "));
                    }

                }
            }


            var respiratoryInterventions = result["485RespiratoryInterventions"];
            if (respiratoryInterventions !== null && respiratoryInterventions != undefined && respiratoryInterventions.Answer != null) {
                var respiratoryInterventionsArray = (respiratoryInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryInterventionsArray.length; i++) {
                    $('input[name=Recertification_485RespiratoryInterventions][value=' + respiratoryInterventionsArray[i] + ']').attr('checked', true);
                    if (respiratoryInterventionsArray[i] == 1) {
                        $("#Recertification_485InstructPulmonaryToiletFrequency").val((result["485InstructPulmonaryToiletFrequency"] !== null && result["485InstructPulmonaryToiletFrequency"] !== undefined ? result["485InstructPulmonaryToiletFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 2) {
                        $("#Recertification_485PerformPulmonaryToiletFrequency").val((result["485PerformPulmonaryToiletFrequency"] !== null && result["485PerformPulmonaryToiletFrequency"] !== undefined ? result["485PerformPulmonaryToiletFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 3) {
                        $("#Recertification_485InstructNebulizerUsePerson").val((result["485InstructNebulizerUsePerson"] !== null && result["485InstructNebulizerUsePerson"] !== undefined ? result["485InstructNebulizerUsePerson"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 4) {
                        $("#Recertification_485AssessOxySaturationFrequency").val((result["485AssessOxySaturationFrequency"] !== null && result["485AssessOxySaturationFrequency"] !== undefined ? result["485AssessOxySaturationFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 5) {
                        $("#Recertification_485AssessOxySatOnOxyAt").val((result["485AssessOxySatOnOxyAt"] !== null && result["485AssessOxySatOnOxyAt"] !== undefined ? result["485AssessOxySatOnOxyAt"].Answer : " "));
                        $("#Recertification_485AssessOxySatLPM").val((result["485AssessOxySatLPM"] !== null && result["485AssessOxySatLPM"] !== undefined ? result["485AssessOxySatLPM"].Answer : " "));
                        $("#Recertification_485AssessOxySatOnOxyFrequency").val((result["485AssessOxySatOnOxyFrequency"] !== null && result["485AssessOxySatOnOxyFrequency"] !== undefined ? result["485AssessOxySatOnOxyFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 6) {
                        $("#Recertification_485InstructSobFactorsPerson").val((result["485InstructSobFactorsPerson"] !== null && result["485InstructSobFactorsPerson"] !== undefined ? result["485InstructSobFactorsPerson"].Answer : " "));
                        $("#Recertification_485InstructSobFactorsTemp").val((result["485InstructSobFactorsTemp"] !== null && result["485InstructSobFactorsTemp"] !== undefined ? result["485InstructSobFactorsTemp"].Answer : " "));
                    }

                    if (respiratoryInterventionsArray[i] == 7) {
                        $("#Recertification_485InstructAvoidSmokingPerson").val((result["485InstructAvoidSmokingPerson"] !== null && result["485InstructAvoidSmokingPerson"] !== undefined ? result["485InstructAvoidSmokingPerson"].Answer : " "));

                    }
                    if (respiratoryInterventionsArray[i] == 10) {
                        $("#Recertification_485InstructNebulizerTreatmentType").val((result["485InstructNebulizerTreatmentType"] !== null && result["485InstructNebulizerTreatmentType"] !== undefined ? result["485InstructNebulizerTreatmentType"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 11) {
                        $("#Recertification_485InstructProperUseOfType").val((result["485InstructProperUseOfType"] !== null && result["485InstructProperUseOfType"] !== undefined ? result["485InstructProperUseOfType"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 13) {
                        $("#Recertification_485RecognizePulmonaryDysfunctionPerson").val((result["485RecognizePulmonaryDysfunctionPerson"] !== null && result["485RecognizePulmonaryDysfunctionPerson"] !== undefined ? result["485RecognizePulmonaryDysfunctionPerson"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 14) {
                        $("#Recertification_485OxySaturationLessThanPercent").val((result["485OxySaturationLessThanPercent"] !== null && result["485OxySaturationLessThanPercent"] !== undefined ? result["485OxySaturationLessThanPercent"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485RespiratoryOrderTemplates").val((result["485RespiratoryOrderTemplates"] !== null && result["485RespiratoryOrderTemplates"] !== undefined ? result["485RespiratoryOrderTemplates"].Answer : " "));
            $("#Recertification_485RespiratoryInterventionComments").val((result["485RespiratoryInterventionComments"] !== null && result["485RespiratoryInterventionComments"] !== undefined ? result["485RespiratoryInterventionComments"].Answer : " "));
            var respiratoryGoals = result["485RespiratoryGoals"];
            if (respiratoryGoals !== null && respiratoryGoals != undefined && respiratoryGoals.Answer != null) {
                var respiratoryGoalsArray = (respiratoryGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryGoalsArray.length; i++) {
                    $('input[name=Recertification_485RespiratoryGoals][value=' + respiratoryGoalsArray[i] + ']').attr('checked', true);
                    if (respiratoryGoalsArray[i] == 3) {
                        $("#Recertification_485VerbalizeFactorsSobDate").val((result["485VerbalizeFactorsSobDate"] !== null && result["485VerbalizeFactorsSobDate"] !== undefined ? result["485VerbalizeFactorsSobDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 4) {
                        $("#Recertification_485DemonstrateLipBreathingDate").val((result["485DemonstrateLipBreathingDate"] !== null && result["485DemonstrateLipBreathingDate"] !== undefined ? result["485DemonstrateLipBreathingDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 5) {
                        $("#Recertification_485VerbalizeEnergyConserveDate").val((result["485VerbalizeEnergyConserveDate"] !== null && result["485VerbalizeEnergyConserveDate"] !== undefined ? result["485VerbalizeEnergyConserveDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 6) {
                        $("#Recertification_485VerbalizeSafeOxyManagementPerson").val((result["485VerbalizeSafeOxyManagementPerson"] !== null && result["485VerbalizeSafeOxyManagementPerson"] !== undefined ? result["485VerbalizeSafeOxyManagementPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeSafeOxyManagementPersonDate").val((result["485VerbalizeSafeOxyManagementPersonDate"] !== null && result["485VerbalizeSafeOxyManagementPersonDate"] !== undefined ? result["485VerbalizeSafeOxyManagementPersonDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 7) {
                        $("#Recertification_485DemonstrateNebulizerUseDate").val((result["485DemonstrateNebulizerUseDate"] !== null && result["485DemonstrateNebulizerUseDate"] !== undefined ? result["485DemonstrateNebulizerUseDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 8) {
                        $("#Recertification_485DemonstrateProperUseOfType").val((result["485DemonstrateProperUseOfType"] !== null && result["485DemonstrateProperUseOfType"] !== undefined ? result["485DemonstrateProperUseOfType"].Answer : " "));
                        $("#Recertification_485DemonstrateProperUseOfTypeDate").val((result["485DemonstrateProperUseOfTypeDate"] !== null && result["485DemonstrateProperUseOfTypeDate"] !== undefined ? result["485DemonstrateProperUseOfTypeDate"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485RespiratoryGoalTemplates").val((result["485RespiratoryGoalTemplates"] !== null && result["485RespiratoryGoalTemplates"] !== undefined ? result["485RespiratoryGoalTemplates"].Answer : " "));
            $("#Recertification_485RespiratoryGoalComments").val((result["485RespiratoryGoalComments"] !== null && result["485RespiratoryGoalComments"] !== undefined ? result["485RespiratoryGoalComments"].Answer : " "));


            var endocrineWithinNormalLimits = result["GenericEndocrineWithinNormalLimits"];
            if (endocrineWithinNormalLimits != null && endocrineWithinNormalLimits != undefined && endocrineWithinNormalLimits.Answer != null) {
                if (endocrineWithinNormalLimits.Answer == "1") {
                    $('input[name=Recertification_GenericEndocrineWithinNormalLimits][value="1"]').attr('checked', true);
                }
                else {
                    $('input[name=Recertification_GenericEndocrineWithinNormalLimits][value="1"]').attr('checked', false);

                }
            }
            $('input[name=Recertification_GenericPatientDiabetic][value=' + (result["GenericPatientDiabetic"] != null && result["GenericPatientDiabetic"] != undefined ? result["GenericPatientDiabetic"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericInsulinDependent][value=' + (result["GenericInsulinDependent"] != null && result["GenericInsulinDependent"] != undefined ? result["GenericInsulinDependent"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=Recertification_GenericInsulinDependent]:checked').val() == '1') {
                $("#Recertification_GenericInsulinDependencyDuration").val((result["GenericInsulinDependencyDuration"] !== null && result["GenericInsulinDependencyDuration"] !== undefined ? result["GenericInsulinDependencyDuration"].Answer : " "));

            }
            $('input[name=Recertification_GenericDrawUpInsulinDose][value=' + (result["GenericDrawUpInsulinDose"] != null && result["GenericDrawUpInsulinDose"] != undefined ? result["GenericDrawUpInsulinDose"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericAdministerOwnInsulin][value=' + (result["GenericAdministerOwnInsulin"] != null && result["GenericAdministerOwnInsulin"] != undefined ? result["GenericAdministerOwnInsulin"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericOralHypoglycemicAgent][value=' + (result["GenericOralHypoglycemicAgent"] != null && result["GenericOralHypoglycemicAgent"] != undefined ? result["GenericOralHypoglycemicAgent"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericGlucometerUseIndependent][value=' + (result["GenericGlucometerUseIndependent"] != null && result["GenericGlucometerUseIndependent"] != undefined ? result["GenericGlucometerUseIndependent"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericCareGiverDrawUpInsulin][value=' + (result["GenericCareGiverDrawUpInsulin"] != null && result["GenericCareGiverDrawUpInsulin"] != undefined ? result["GenericCareGiverDrawUpInsulin"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericCareGiverGlucometerUse][value=' + (result["GenericCareGiverGlucometerUse"] != null && result["GenericCareGiverGlucometerUse"] != undefined ? result["GenericCareGiverGlucometerUse"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericCareGiverInspectLowerExtremities][value=' + (result["GenericCareGiverInspectLowerExtremities"] != null && result["GenericCareGiverInspectLowerExtremities"] != undefined ? result["GenericCareGiverInspectLowerExtremities"].Answer : "") + ']').attr('checked', true);

            $("#Recertification_485RespiratoryInterventionComments").val((result["485RespiratoryInterventionComments"] !== null && result["485RespiratoryInterventionComments"] !== undefined ? result["485RespiratoryInterventionComments"].Answer : " "));
            var patientEdocrineProblem = result["GenericPatientEdocrineProblem"];
            if (patientEdocrineProblem !== null && patientEdocrineProblem != undefined && patientEdocrineProblem.Answer != null) {
                var patientEdocrineProblemArray = (patientEdocrineProblem.Answer).split(',');
                var i = 0;
                for (i = 0; i < patientEdocrineProblemArray.length; i++) {
                    $('input[name=Recertification_GenericPatientEdocrineProblem][value=' + patientEdocrineProblemArray[i] + ']').attr('checked', true);

                }
            }
            $("#Recertification_GenericBloodSugarLevelText").val((result["GenericBloodSugarLevelText"] !== null && result["GenericBloodSugarLevelText"] !== undefined ? result["485RespiratoryGoalTemplates"].Answer : " "));
            $('input[name=Recertification_GenericBloodSugarLevel][value=' + (result["GenericBloodSugarLevel"] != null && result["GenericBloodSugarLevel"] != undefined ? result["GenericBloodSugarLevel"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericBloodSugarCheckedBy").val((result["GenericBloodSugarCheckedBy"] !== null && result["GenericBloodSugarCheckedBy"] !== undefined ? result["GenericBloodSugarCheckedBy"].Answer : " "));
            $("#Recertification_GenericBloodSugarSite").val((result["GenericBloodSugarSite"] !== null && result["GenericBloodSugarSite"] !== undefined ? result["GenericBloodSugarSite"].Answer : " "));
            $("#Recertification_GenericEndocrineComments").val((result["GenericEndocrineComments"] !== null && result["GenericEndocrineComments"] !== undefined ? result["GenericEndocrineComments"].Answer : " "));

            var endocrineInterventions = result["485EndocrineInterventions"];
            if (endocrineInterventions !== null && endocrineInterventions != undefined && endocrineInterventions.Answer != null) {
                var endocrineInterventionsArray = (endocrineInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < endocrineInterventionsArray.length; i++) {
                    $('input[name=Recertification_485EndocrineInterventions][value=' + endocrineInterventionsArray[i] + ']').attr('checked', true);
                    if (endocrineInterventionsArray[i] == 1) {
                        $("#Recertification_485InstructDiabeticManagementPerson").val((result["485InstructDiabeticManagementPerson"] !== null && result["485InstructDiabeticManagementPerson"] !== undefined ? result["485InstructDiabeticManagementPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 2) {
                        $("#Recertification_485InstructInspectFeetDailyPerson").val((result["485InstructInspectFeetDailyPerson"] !== null && result["485InstructInspectFeetDailyPerson"] !== undefined ? result["485InstructInspectFeetDailyPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 3) {
                        $("#Recertification_485InstructWashFeetWarmPerson").val((result["485InstructWashFeetWarmPerson"] !== null && result["485InstructWashFeetWarmPerson"] !== undefined ? result["485InstructWashFeetWarmPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 4) {
                        $("#Recertification_485InstructMoisturizerPerson").val((result["485InstructMoisturizerPerson"] !== null && result["485InstructMoisturizerPerson"] !== undefined ? result["485InstructMoisturizerPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 6) {
                        $("#Recertification_485InstructNailCarePerson").val((result["485InstructNailCarePerson"] !== null && result["485InstructNailCarePerson"] !== undefined ? result["485InstructNailCarePerson"].Answer : " "));

                    }
                    if (endocrineInterventionsArray[i] == 7) {
                        $("#Recertification_485InstructNeverWalkBareFootPerson").val((result["485InstructNeverWalkBareFootPerson"] !== null && result["485InstructNeverWalkBareFootPerson"] !== undefined ? result["485InstructNeverWalkBareFootPerson"].Answer : " "));

                    }

                    if (endocrineInterventionsArray[i] == 8) {
                        $("#Recertification_485InstructElevateFeetPerson").val((result["485InstructElevateFeetPerson"] !== null && result["485InstructElevateFeetPerson"] !== undefined ? result["485InstructElevateFeetPerson"].Answer : " "));

                    }
                    if (endocrineInterventionsArray[i] == 9) {
                        $("#Recertification_485InstructProtectFeetPerson").val((result["485InstructProtectFeetPerson"] !== null && result["485InstructProtectFeetPerson"] !== undefined ? result["485InstructProtectFeetPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 10) {
                        $("#Recertification_485InstructNeverCutCornsPerson").val((result["485InstructNeverCutCornsPerson"] !== null && result["485InstructNeverCutCornsPerson"] !== undefined ? result["485InstructNeverCutCornsPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 12) {
                        $("#Recertification_485GiveJuiceIfBloodSugarLevel").val((result["485GiveJuiceIfBloodSugarLevel"] !== null && result["485GiveJuiceIfBloodSugarLevel"] !== undefined ? result["485GiveJuiceIfBloodSugarLevel"].Answer : " "));
                        $("#Recertification_485GiveJuiceIfBloodSugarLevelRemains").val((result["485GiveJuiceIfBloodSugarLevelRemains"] !== null && result["485GiveJuiceIfBloodSugarLevelRemains"] !== undefined ? result["485GiveJuiceIfBloodSugarLevelRemains"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 13) {
                        $("#Recertification_485PrepareAdministerInsulinType").val((result["485PrepareAdministerInsulinType"] !== null && result["485PrepareAdministerInsulinType"] !== undefined ? result["485PrepareAdministerInsulinType"].Answer : " "));
                        $("#Recertification_485PrepareAdministerInsulinFrequency").val((result["485PrepareAdministerInsulinFrequency"] !== null && result["485PrepareAdministerInsulinFrequency"] !== undefined ? result["485PrepareAdministerInsulinFrequency"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 15) {
                        $("#Recertification_485PrefillInsulinSyringesType").val((result["485PrefillInsulinSyringesType"] !== null && result["485PrefillInsulinSyringesType"] !== undefined ? result["485PrefillInsulinSyringesType"].Answer : " "));
                        $("#Recertification_485PrefillInsulinSyringesTypeFrequency").val((result["485PrefillInsulinSyringesTypeFrequency"] !== null && result["485PrefillInsulinSyringesTypeFrequency"] !== undefined ? result["485PrefillInsulinSyringesTypeFrequency"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485EndocrineInterventionTemplates").val((result["485EndocrineInterventionTemplates"] !== null && result["485EndocrineInterventionTemplates"] !== undefined ? result["485EndocrineInterventionTemplates"].Answer : " "));
            $("#Recertification_485EndocrineInterventionComments").val((result["485EndocrineInterventionComments"] !== null && result["485EndocrineInterventionComments"] !== undefined ? result["485EndocrineInterventionComments"].Answer : " "));

            var endocrineGoals = result["485EndocrineGoals"];
            if (endocrineGoals !== null && endocrineGoals != undefined && endocrineGoals.Answer != null) {
                var endocrineGoalsArray = (endocrineGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < endocrineGoalsArray.length; i++) {
                    $('input[name=Recertification_485EndocrineGoals][value=' + endocrineGoalsArray[i] + ']').attr('checked', true);
                    if (endocrineGoalsArray[i] == 1) {
                        $("#Recertification_485FastingBloodSugarBetween").val((result["485FastingBloodSugarBetween"] !== null && result["485FastingBloodSugarBetween"] !== undefined ? result["485FastingBloodSugarBetween"].Answer : " "));
                        $("#Recertification_485FastingBloodSugarAnd").val((result["485FastingBloodSugarAnd"] !== null && result["485FastingBloodSugarAnd"] !== undefined ? result["485FastingBloodSugarAnd"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 2) {
                        $("#Recertification_485RandomBloodSugarBetween").val((result["485RandomBloodSugarBetween"] !== null && result["485RandomBloodSugarBetween"] !== undefined ? result["485RandomBloodSugarBetween"].Answer : " "));
                        $("#Recertification_485RandomBloodSugarAnd").val((result["485RandomBloodSugarAnd"] !== null && result["485RandomBloodSugarAnd"] !== undefined ? result["485RandomBloodSugarAnd"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 4) {
                        $("#Recertification_485GlucometerUseIndependencePerson").val((result["485GlucometerUseIndependencePerson"] !== null && result["485GlucometerUseIndependencePerson"] !== undefined ? result["485GlucometerUseIndependencePerson"].Answer : " "));
                        $("#Recertification_485GlucometerUseIndependenceDate").val((result["485GlucometerUseIndependenceDate"] !== null && result["485GlucometerUseIndependenceDate"] !== undefined ? result["485GlucometerUseIndependenceDate"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 5) {
                        $("#Recertification_485VerbalizeSkinConditionUnderstandingPerson").val((result["485VerbalizeSkinConditionUnderstandingPerson"] !== null && result["485VerbalizeSkinConditionUnderstandingPerson"] !== undefined ? result["485VerbalizeSkinConditionUnderstandingPerson"].Answer : " "));

                    }
                    if (endocrineGoalsArray[i] == 6) {
                        $("#Recertification_485IndependentInsulinAdministrationPerson").val((result["485IndependentInsulinAdministrationPerson"] !== null && result["485IndependentInsulinAdministrationPerson"] !== undefined ? result["485IndependentInsulinAdministrationPerson"].Answer : " "));
                        $("#Recertification_485IndependentInsulinAdministrationDate").val((result["485IndependentInsulinAdministrationDate"] !== null && result["485IndependentInsulinAdministrationDate"] !== undefined ? result["485IndependentInsulinAdministrationDate"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 7) {
                        $("#Recertification_485VerbalizeProperFootCarePerson").val((result["485VerbalizeProperFootCarePerson"] !== null && result["485VerbalizeProperFootCarePerson"] !== undefined ? result["485VerbalizeProperFootCarePerson"].Answer : " "));
                        $("#Recertification_485VerbalizeProperFootCareDate").val((result["485VerbalizeProperFootCareDate"] !== null && result["485VerbalizeProperFootCareDate"] !== undefined ? result["485VerbalizeProperFootCareDate"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485EndocrineGoalTemplates").val((result["485EndocrineGoalTemplates"] !== null && result["485EndocrineGoalTemplates"] !== undefined ? result["485EndocrineGoalTemplates"].Answer : " "));
            $("#Recertification_485EndocrineGoalComments").val((result["485EndocrineGoalComments"] !== null && result["485EndocrineGoalComments"] !== undefined ? result["485EndocrineGoalComments"].Answer : " "));

            var cardioVascular = result["GenericCardioVascular"];
            if (cardioVascular !== null && cardioVascular != undefined && cardioVascular.Answer != null) {
                var cardioVascularArray = (cardioVascular.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardioVascularArray.length; i++) {
                    $('input[name=Recertification_GenericCardioVascular][value=' + cardioVascularArray[i] + ']').attr('checked', true);
                    if (cardioVascularArray[i] == 2) {
                        $("#Recertification_GenericDizzinessDesc").val((result["GenericDizzinessDesc"] !== null && result["GenericDizzinessDesc"] !== undefined ? result["GenericDizzinessDesc"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 3) {
                        $("#Recertification_GenericChestPainDesc").val((result["GenericChestPainDesc"] !== null && result["GenericChestPainDesc"] !== undefined ? result["GenericChestPainDesc"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 4) {
                        $("#Recertification_GenericEdemaDesc1").val((result["GenericEdemaDesc1"] !== null && result["GenericEdemaDesc1"] !== undefined ? result["GenericEdemaDesc1"].Answer : " "));
                        $("#Recertification_GenericEdemaList1").val((result["GenericEdemaList1"] !== null && result["GenericEdemaList1"] !== undefined ? result["GenericEdemaList1"].Answer : " "));
                        $("#Recertification_GenericEdemaDesc2").val((result["GenericEdemaDesc2"] !== null && result["GenericEdemaDesc2"] !== undefined ? result["GenericEdemaDesc2"].Answer : " "));
                        $("#Recertification_GenericEdemaList2").val((result["GenericEdemaList2"] !== null && result["GenericEdemaList2"] !== undefined ? result["GenericEdemaList2"].Answer : " "));
                        $("#Recertification_GenericEdemaDesc3").val((result["GenericEdemaDesc3"] !== null && result["GenericEdemaDesc3"] !== undefined ? result["GenericEdemaDesc3"].Answer : " "));
                        $("#Recertification_GenericEdemaList3").val((result["GenericEdemaList3"] !== null && result["GenericEdemaList3"] !== undefined ? result["GenericEdemaList3"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 5) {
                        var pittingEdemaType = result["GenericPittingEdemaType"];
                        if (pittingEdemaType !== null && pittingEdemaType != undefined && pittingEdemaType.Answer != null) {
                            var pittingEdemaTypeArray = (pittingEdemaType.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < pittingEdemaTypeArray.length; j++) {
                                $('input[name=Recertification_GenericPittingEdemaType][value=' + pittingEdemaTypeArray[j] + ']').attr('checked', true);
                            }
                        }

                    }
                    if (cardioVascularArray[i] == 6) {
                        var heartSoundsType = result["GenericHeartSoundsType"];
                        if (heartSoundsType !== null && heartSoundsType != undefined && heartSoundsType.Answer != null) {
                            var heartSoundsTypeArray = (heartSoundsType.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < heartSoundsTypeArray.length; j++) {
                                $('input[name=Recertification_GenericHeartSoundsType][value=' + heartSoundsTypeArray[j] + ']').attr('checked', true);
                            }
                        }
                    }
                    if (cardioVascularArray[i] == 7) {
                        $("#Recertification_GenericNeckVeinDistentionDesc").val((result["GenericNeckVeinDistentionDesc"] !== null && result["GenericNeckVeinDistentionDesc"] !== undefined ? result["GenericNeckVeinDistentionDesc"].Answer : " "));

                    }

                    if (cardioVascularArray[i] == 8) {
                        $("#Recertification_GenericPeripheralPulsesDesc").val((result["GenericPeripheralPulsesDesc"] !== null && result["GenericPeripheralPulsesDesc"] !== undefined ? result["GenericPeripheralPulsesDesc"].Answer : " "));

                    }
                    if (cardioVascularArray[i] == 9) {
                        $('input[name=Recertification_GenericCapRefillLessThan3][value=' + (result["GenericCapRefillLessThan3"] != null && result["GenericCapRefillLessThan3"] != undefined ? result["GenericCapRefillLessThan3"].Answer : "") + ']').attr('checked', true);
                    }

                }
            }
            $("#Recertification_GenericPacemakerDate").val((result["GenericPacemakerDate"] !== null && result["GenericPacemakerDate"] !== undefined ? result["GenericPacemakerDate"].Answer : " "));
            $("#Recertification_GenericAICDDate").val((result["GenericAICDDate"] !== null && result["GenericAICDDate"] !== undefined ? result["GenericAICDDate"].Answer : " "));
            $("#Recertification_GenericCardiovascularComments").val((result["GenericCardiovascularComments"] !== null && result["GenericCardiovascularComments"] !== undefined ? result["GenericCardiovascularComments"].Answer : " "));

            var cardioVascular = result["485CardiacStatusInterventions"];
            if (cardioVascular !== null && cardioVascular != undefined && cardioVascular.Answer != null) {
                var cardioVascularArray = (cardioVascular.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardioVascularArray.length; i++) {
                    $('input[name=Recertification_485CardiacStatusInterventions][value=' + cardioVascularArray[i] + ']').attr('checked', true);
                    if (cardioVascularArray[i] == 1) {
                        $('input[name=Recertification_485WeightSelfMonitorGain][value=' + (result["485WeightSelfMonitorGain"] != null && result["485WeightSelfMonitorGain"] != undefined ? result["485WeightSelfMonitorGain"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_485WeightSelfMonitorGainDay").val((result["485WeightSelfMonitorGainDay"] !== null && result["485WeightSelfMonitorGainDay"] !== undefined ? result["485WeightSelfMonitorGainDay"].Answer : " "));
                        $("#Recertification_485WeightSelfMonitorGainWeek").val((result["485WeightSelfMonitorGainWeek"] !== null && result["485WeightSelfMonitorGainWeek"] !== undefined ? result["485WeightSelfMonitorGainWeek"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 3) {
                        $("#Recertification_485InstructRecognizeCardiacDysfunctionPerson").val((result["485InstructRecognizeCardiacDysfunctionPerson"] !== null && result["485InstructRecognizeCardiacDysfunctionPerson"] !== undefined ? result["485InstructRecognizeCardiacDysfunctionPerson"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 7) {
                        $("#Recertification_485NoBloodPressureArm").val((result["485NoBloodPressureArm"] !== null && result["485NoBloodPressureArm"] !== undefined ? result["485NoBloodPressureArm"].Answer : " "));

                    }
                }
            }
            $("#Recertification_485CardiacOrderTemplates").val((result["485CardiacOrderTemplates"] !== null && result["485CardiacOrderTemplates"] !== undefined ? result["485CardiacOrderTemplates"].Answer : " "));
            $("#Recertification_485CardiacInterventionComments").val((result["485CardiacInterventionComments"] !== null && result["485CardiacInterventionComments"] !== undefined ? result["485CardiacInterventionComments"].Answer : " "));

            var cardiacStatusGoals = result["485CardiacStatusGoals"];
            if (cardiacStatusGoals !== null && cardiacStatusGoals != undefined && cardiacStatusGoals.Answer != null) {
                var cardiacStatusGoalsArray = (cardiacStatusGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardiacStatusGoalsArray.length; i++) {
                    $('input[name=Recertification_485CardiacStatusGoals][value=' + cardiacStatusGoalsArray[i] + ']').attr('checked', true);
                    if (cardiacStatusGoalsArray[i] == 1) {
                        $("#Recertification_485WeightMaintainedMin").val((result["485WeightMaintainedMin"] !== null && result["485WeightMaintainedMin"] !== undefined ? result["485WeightMaintainedMin"].Answer : " "));
                        $("#Recertification_485WeightMaintainedMax").val((result["485WeightMaintainedMax"] !== null && result["485WeightMaintainedMax"] !== undefined ? result["485WeightMaintainedMax"].Answer : " "));
                    }
                    if (cardiacStatusGoalsArray[i] == 5) {
                        $("#Recertification_485VerbalizeCardiacSymptomsPerson").val((result["485VerbalizeCardiacSymptomsPerson"] !== null && result["485VerbalizeCardiacSymptomsPerson"] !== undefined ? result["485VerbalizeCardiacSymptomsPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeCardiacSymptomsDate").val((result["485VerbalizeCardiacSymptomsDate"] !== null && result["485VerbalizeCardiacSymptomsDate"] !== undefined ? result["485VerbalizeCardiacSymptomsDate"].Answer : " "));
                    }
                    if (cardiacStatusGoalsArray[i] == 6) {
                        $("#Recertification_485VerbalizeEdemaRelieverPerson").val((result["485VerbalizeEdemaRelieverPerson"] !== null && result["485VerbalizeEdemaRelieverPerson"] !== undefined ? result["485VerbalizeEdemaRelieverPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeEdemaRelieverDate").val((result["485VerbalizeEdemaRelieverDate"] !== null && result["485VerbalizeEdemaRelieverDate"] !== undefined ? result["485VerbalizeEdemaRelieverDate"].Answer : " "));

                    }
                }
            }
            $("#Recertification_485CardiacGoalTemplates").val((result["485CardiacGoalTemplates"] !== null && result["485CardiacGoalTemplates"] !== undefined ? result["485CardiacGoalTemplates"].Answer : " "));
            $("#Recertification_485CardiacGoalComments").val((result["485CardiacGoalComments"] !== null && result["485CardiacGoalComments"] !== undefined ? result["485CardiacGoalComments"].Answer : " "));

            var gU = result["GenericGU"];
            if (gU !== null && gU != undefined && gU.Answer != null) {
                var gUArray = (gU.Answer).split(',');
                var i = 0;
                for (i = 0; i < gUArray.length; i++) {
                    $('input[name=Recertification_GenericGU][value=' + gUArray[i] + ']').attr('checked', true);
                    if (gUArray[i] == 10) {
                        $("#Recertification_GenericGUCatheterList").val((result["GenericGUCatheterList"] !== null && result["GenericGUCatheterList"] !== undefined ? result["GenericGUCatheterList"].Answer : " "));
                        $("#Recertification_GenericGUCatheterLastChanged").val((result["GenericGUCatheterLastChanged"] !== null && result["GenericGUCatheterLastChanged"] !== undefined ? result["GenericGUCatheterLastChanged"].Answer : " "));
                        $("#Recertification_GenericGUCatheterFrequency").val((result["GenericGUCatheterFrequency"] !== null && result["GenericGUCatheterFrequency"] !== undefined ? result["GenericGUCatheterFrequency"].Answer : " "));
                        $("#Recertification_GenericGUCatheterAmount").val((result["GenericGUCatheterAmount"] !== null && result["GenericGUCatheterAmount"] !== undefined ? result["GenericGUCatheterAmount"].Answer : " "));
                    }
                    if (gUArray[i] == 11) {
                        var gUUrine = result["GenericGUUrine"];
                        if (gUUrine !== null && gUUrine != undefined && gUUrine.Answer != null) {
                            var gUUrineArray = (gUUrine.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < gUUrineArray.length; j++) {
                                $('input[name=Recertification_GenericGUUrine][value=' + gUUrineArray[j] + ']').attr('checked', true);
                                if (gUUrineArray[j] == 5) {
                                    $("#Recertification_GenericGUOtherText").val((result["GenericGUOtherText"] !== null && result["GenericGUOtherText"] !== undefined ? result["GenericGUOtherText"].Answer : " "));
                                }
                            }
                        }
                    }
                    if (gUArray[i] == 12) {
                        $('input[name=Recertification_GenericGUNormal][value=' + (result["GenericGUNormal"] != null && result["GenericGUNormal"] != undefined ? result["GenericGUNormal"].Answer : "") + ']').attr('checked', true);
                        $('input[name=Recertification_GenericGUClinicalAssessment][value=' + (result["GenericGUClinicalAssessment"] != null && result["GenericGUClinicalAssessment"] != undefined ? result["GenericGUClinicalAssessment"].Answer : "") + ']').attr('checked', true);
                    }

                }
            }
            var digestive = result["GenericDigestive"];
            if (digestive !== null && digestive != undefined && digestive.Answer != null) {
                var digestiveArray = (digestive.Answer).split(',');
                var i = 0;
                for (i = 0; i < digestiveArray.length; i++) {
                    $('input[name=Recertification_GenericDigestive][value=' + digestiveArray[i] + ']').attr('checked', true);
                    if (digestiveArray[i] == 8) {
                        $('input[name=Recertification_GenericDigestiveBowelSoundsType][value=' + (result["GenericDigestiveBowelSoundsType"] != null && result["GenericDigestiveBowelSoundsType"] != undefined ? result["GenericDigestiveBowelSoundsType"].Answer : "") + ']').attr('checked', true);
                    }
                    if (digestiveArray[i] == 9) {
                        $("#Recertification_GenericDigestiveAbdGirthLength").val((result["GenericDigestiveAbdGirthLength"] !== null && result["GenericDigestiveAbdGirthLength"] !== undefined ? result["GenericDigestiveAbdGirthLength"].Answer : " "));
                    }
                    if (digestiveArray[i] == 10) {
                        $("#Recertification_GenericDigestiveLastBMDate").val((result["GenericDigestiveLastBMDate"] !== null && result["GenericDigestiveLastBMDate"] !== undefined ? result["GenericDigestiveLastBMDate"].Answer : " "));
                        $('input[name=Recertification_GenericDigestiveLastBMAsper][value=' + (result["GenericDigestiveLastBMAsper"] != null && result["GenericDigestiveLastBMAsper"] != undefined ? result["GenericDigestiveLastBMAsper"].Answer : "") + ']').attr('checked', true);
                        var digestiveLastBM = result["GenericDigestiveLastBM"];
                        if (digestiveLastBM !== null && digestiveLastBM != undefined && digestiveLastBM.Answer != null) {
                            var digestiveLastBMArray = (digestiveLastBM.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < digestiveLastBMArray.length; j++) {
                                $('input[name=Recertification_GenericDigestiveLastBM][value=' + digestiveLastBMArray[j] + ']').attr('checked', true);
                                if (digestiveLastBMArray[j] == 2) {
                                    var digestiveLastBMAbnormalStool = result["GenericDigestiveLastBMAbnormalStool"];
                                    if (digestiveLastBMAbnormalStool !== null && digestiveLastBMAbnormalStool != undefined && digestiveLastBMAbnormalStool.Answer != null) {
                                        var digestiveLastBMAbnormalStoolArray = (digestiveLastBMAbnormalStool.Answer).split(',');
                                        var k = 0;
                                        for (k = 0; k < digestiveLastBMAbnormalStoolArray.length; k++) {
                                            $('input[name=Recertification_GenericDigestiveLastBMAbnormalStool][value=' + digestiveLastBMAbnormalStoolArray[k] + ']').attr('checked', true);

                                        }
                                    }
                                }
                                if (digestiveLastBMArray[j] == 3) {
                                    $('input[name=Recertification_GenericDigestiveLastBMConstipationType][value=' + (result["GenericDigestiveLastBMConstipationType"] != null && result["GenericDigestiveLastBMConstipationType"] != undefined ? result["GenericDigestiveLastBMAsper"].Answer : "") + ']').attr('checked', true);
                                }
                                if (digestiveLastBMArray[j] == 4) {
                                    $("#Recertification_GenericDigestiveLaxEnemaUseDesc").val((result["GenericDigestiveLaxEnemaUseDesc"] !== null && result["GenericDigestiveLaxEnemaUseDesc"] !== undefined ? result["GenericDigestiveLaxEnemaUseDesc"].Answer : " "));
                                }
                                if (digestiveLastBMArray[j] == 5) {
                                    $('input[name=Recertification_GenericDigestiveHemorrhoidsType][value=' + (result["GenericDigestiveHemorrhoidsType"] != null && result["GenericDigestiveHemorrhoidsType"] != undefined ? result["GenericDigestiveHemorrhoidsType"].Answer : "") + ']').attr('checked', true);
                                }
                            }
                        }
                    }
                    if (digestiveArray[i] == 11) {
                        $("#Recertification_GenericDigestiveOstomyType").val((result["GenericDigestiveOstomyType"] !== null && result["GenericDigestiveOstomyType"] !== undefined ? result["GenericDigestiveOstomyType"].Answer : " "));
                        var digestiveOstomy = result["GenericDigestiveOstomy"];
                        if (digestiveOstomy !== null && digestiveOstomy != undefined && digestiveOstomy.Answer != null) {
                            var digestiveOstomyArray = (digestiveOstomy.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < digestiveOstomyArray.length; j++) {
                                $('input[name=Recertification_GenericDigestiveOstomy][value=' + digestiveOstomyArray[j] + ']').attr('checked', true);

                                if (digestiveOstomyArray[j] == 1) {
                                    $("#Recertification_GenericDigestiveStomaType").val((result["GenericDigestiveStomaType"] !== null && result["GenericDigestiveStomaType"] !== undefined ? result["GenericDigestiveStomaType"].Answer : " "));
                                }
                                if (digestiveOstomyArray[j] == 2) {
                                    $("#Recertification_GenericDigestiveStoolAppearanceDesc").val((result["GenericDigestiveStoolAppearanceDesc"] !== null && result["GenericDigestiveStoolAppearanceDesc"] !== undefined ? result["GenericDigestiveStoolAppearanceDesc"].Answer : " "));
                                }
                                if (digestiveOstomyArray[j] == 3) {
                                    $("#Recertification_GenericDigestiveSurSkinType").val((result["GenericDigestiveSurSkinType"] !== null && result["GenericDigestiveSurSkinType"] !== undefined ? result["GenericDigestiveSurSkinType"].Answer : " "));
                                    $('input[name=Recertification_GenericDigestiveSurSkinIntact][value=' + (result["GenericDigestiveSurSkinIntact"] != null && result["GenericDigestiveSurSkinIntact"] != undefined ? result["GenericDigestiveSurSkinIntact"].Answer : "") + ']').attr('checked', true);
                                }
                            }
                        }
                    }

                }
            }
            $("#Recertification_GenericGUDigestiveComments").val((result["GenericGUDigestiveComments"] !== null && result["GenericGUDigestiveComments"] !== undefined ? result["GenericGUDigestiveComments"].Answer : " "));

            $('input[name=Recertification_GenericPatientOnDialysis][value=' + (result["GenericPatientOnDialysis"] != null && result["GenericPatientOnDialysis"] != undefined ? result["GenericPatientOnDialysis"].Answer : "") + ']').attr('checked', true);

            var dialysis = result["GenericDialysis"];
            if (dialysis !== null && dialysis != undefined && dialysis.Answer != null) {
                var dialysisArray = (dialysis.Answer).split(',');
                var i = 0;
                for (i = 0; i < dialysisArray.length; i++) {
                    $('input[name=Recertification_GenericDialysis][value=' + dialysisArray[i] + ']').attr('checked', true);
                    if (dialysisArray[i] == 2) {
                        $("#Recertification_GenericAVGraftSite").val((result["GenericAVGraftSite"] !== null && result["GenericAVGraftSite"] !== undefined ? result["GenericAVGraftSite"].Answer : " "));
                    }
                    if (dialysisArray[i] == 3) {
                        $("#Recertification_GenericVenousCatheterSite").val((result["GenericVenousCatheterSite"] !== null && result["GenericVenousCatheterSite"] !== undefined ? result["GenericVenousCatheterSite"].Answer : " "));
                    }
                    if (dialysisArray[i] == 9) {
                        $("#Recertification_GenericDialysisOtherDesc").val((result["GenericDialysisOtherDesc"] !== null && result["GenericDialysisOtherDesc"] !== undefined ? result["GenericDialysisOtherDesc"].Answer : " "));
                    }

                }
            }
            $("#Recertification_GenericDialysisCenter").val((result["GenericDialysisCenter"] !== null && result["GenericDialysisCenter"] !== undefined ? result["GenericDialysisCenter"].Answer : " "));
            $("#Recertification_GenericDialysisCenterPhone").val((result["GenericDialysisCenterPhone"] !== null && result["GenericDialysisCenterPhone"] !== undefined ? result["GenericDialysisCenterPhone"].Answer : " "));
            $("#Recertification_GenericDialysisCenterContact").val((result["GenericDialysisCenterContact"] !== null && result["GenericDialysisCenterContact"] !== undefined ? result["GenericDialysisCenterContact"].Answer : " "));


            var eliminationStatusInterventions = result["485EliminationStatusInterventions"];
            if (eliminationStatusInterventions !== null && eliminationStatusInterventions != undefined && eliminationStatusInterventions.Answer != null) {
                var eliminationStatusInterventionsArray = (eliminationStatusInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < eliminationStatusInterventionsArray.length; i++) {
                    $('input[name=Recertification_485EliminationStatusInterventions][value=' + eliminationStatusInterventionsArray[i] + ']').attr('checked', true);
                    if (eliminationStatusInterventionsArray[i] == 2) {
                        $("#Recertification_485InstructUTISymptomsPerson").val((result["485InstructUTISymptomsPerson"] !== null && result["485InstructUTISymptomsPerson"] !== undefined ? result["485InstructUTISymptomsPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 3) {
                        $("#Recertification_485ChangeFoleyCatheterWith").val((result["485ChangeFoleyCatheterWith"] !== null && result["485ChangeFoleyCatheterWith"] !== undefined ? result["485ChangeFoleyCatheterWith"].Answer : " "));
                        $("#Recertification_485ChangeFoleyCatheterQuantity").val((result["485ChangeFoleyCatheterQuantity"] !== null && result["485ChangeFoleyCatheterQuantity"] !== undefined ? result["485ChangeFoleyCatheterQuantity"].Answer : " "));
                        $("#Recertification_485ChangeFoleyCatheterFrequency").val((result["485ChangeFoleyCatheterFrequency"] !== null && result["485ChangeFoleyCatheterFrequency"] !== undefined ? result["485ChangeFoleyCatheterFrequency"].Answer : " "));
                        $("#Recertification_485ChangeFoleyCatheterDate").val((result["485ChangeFoleyCatheterDate"] !== null && result["485ChangeFoleyCatheterDate"] !== undefined ? result["485ChangeFoleyCatheterDate"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 4) {
                        $("#Recertification_485ChangeSuprapubicTubeWith").val((result["485ChangeSuprapubicTubeWith"] !== null && result["485ChangeSuprapubicTubeWith"] !== undefined ? result["485ChangeSuprapubicTubeWith"].Answer : " "));
                        $("#Recertification_485ChangeSuprapubicTubeQuantity").val((result["485ChangeSuprapubicTubeQuantity"] !== null && result["485ChangeSuprapubicTubeQuantity"] !== undefined ? result["485ChangeSuprapubicTubeQuantity"].Answer : " "));
                        $("#Recertification_485ChangeSuprapubicTubeFrequency").val((result["485ChangeSuprapubicTubeFrequency"] !== null && result["485ChangeSuprapubicTubeFrequency"] !== undefined ? result["485ChangeSuprapubicTubeFrequency"].Answer : " "));
                        $("#Recertification_485ChangeSuprapubicTubeDate").val((result["485ChangeSuprapubicTubeDate"] !== null && result["485ChangeSuprapubicTubeDate"] !== undefined ? result["485ChangeSuprapubicTubeDate"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 7) {
                        $("#Recertification_485EliminationStatusInterventions").val((result["485EliminationStatusInterventions"] !== null && result["485EliminationStatusInterventions"] !== undefined ? result["485EliminationStatusInterventions"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 8) {
                        $("#Recertification_485AllowFoleyVisitsAmount").val((result["485AllowFoleyVisitsAmount"] !== null && result["485AllowFoleyVisitsAmount"] !== undefined ? result["485AllowFoleyVisitsAmount"].Answer : " "));

                    }
                    if (eliminationStatusInterventionsArray[i] == 9) {
                        $("#Recertification_485InstructOstomyManagementDesc").val((result["485InstructOstomyManagementDesc"] !== null && result["485InstructOstomyManagementDesc"] !== undefined ? result["485InstructOstomyManagementDesc"].Answer : " "));

                    }

                    if (eliminationStatusInterventionsArray[i] == 10) {
                        $("#Recertification_485PerformOstomyCareDesc").val((result["485PerformOstomyCareDesc"] !== null && result["485PerformOstomyCareDesc"] !== undefined ? result["485PerformOstomyCareDesc"].Answer : " "));

                    }
                    if (eliminationStatusInterventionsArray[i] == 11) {
                        $("#Recertification_485DigitalDisimpactDays").val((result["485DigitalDisimpactDays"] !== null && result["485DigitalDisimpactDays"] !== undefined ? result["485DigitalDisimpactDays"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 12) {
                        $("#Recertification_485InstructMeasureIntakeOutputPerson").val((result["485InstructMeasureIntakeOutputPerson"] !== null && result["485InstructMeasureIntakeOutputPerson"] !== undefined ? result["485InstructMeasureIntakeOutputPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 14) {
                        $("#Recertification_485AdministerEnemaType").val((result["485AdministerEnemaType"] !== null && result["485AdministerEnemaType"] !== undefined ? result["485AdministerEnemaType"].Answer : " "));
                        $("#Recertification_485AdministerEnemaDays").val((result["485AdministerEnemaDays"] !== null && result["485AdministerEnemaDays"] !== undefined ? result["485AdministerEnemaDays"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 15) {
                        $("#Recertification_485InstructConstipationSignsPerson").val((result["485InstructConstipationSignsPerson"] !== null && result["485InstructConstipationSignsPerson"] !== undefined ? result["485InstructConstipationSignsPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 16) {
                        $("#Recertification_485InstructAcidRefluxFoodsPerson").val((result["485InstructAcidRefluxFoodsPerson"] !== null && result["485InstructAcidRefluxFoodsPerson"] !== undefined ? result["485InstructAcidRefluxFoodsPerson"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485EliminationInterventionTemplates").val((result["485EliminationInterventionTemplates"] !== null && result["485EliminationInterventionTemplates"] !== undefined ? result["485EliminationInterventionTemplates"].Answer : " "));
            $("#Recertification_485EliminationInterventionComments").val((result["485EliminationInterventionComments"] !== null && result["485EliminationInterventionComments"] !== undefined ? result["485EliminationInterventionComments"].Answer : " "));

            var eliminationStatusGoals = result["485EliminationStatusGoals"];
            if (eliminationStatusGoals !== null && eliminationStatusGoals != undefined && eliminationStatusGoals.Answer != null) {
                var eliminationStatusGoalsArray = (eliminationStatusGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < eliminationStatusGoalsArray.length; i++) {
                    $('input[name=Recertification_485EliminationStatusGoals][value=' + eliminationStatusGoalsArray[i] + ']').attr('checked', true);
                    if (eliminationStatusGoalsArray[i] == 4) {
                        $("#Recertification_485IndependentOstomyManagementPerson").val((result["485IndependentOstomyManagementPerson"] !== null && result["485IndependentOstomyManagementPerson"] !== undefined ? result["485IndependentOstomyManagementPerson"].Answer : " "));
                        $("#Recertification_485IndependentOstomyManagementDate").val((result["485IndependentOstomyManagementDate"] !== null && result["485IndependentOstomyManagementDate"] !== undefined ? result["485IndependentOstomyManagementDate"].Answer : " "));
                    }
                    if (eliminationStatusGoalsArray[i] == 6) {
                        $("#Recertification_485VerbalizeAcidRefluxFoodPerson").val((result["485VerbalizeAcidRefluxFoodPerson"] !== null && result["485VerbalizeAcidRefluxFoodPerson"] !== undefined ? result["485VerbalizeAcidRefluxFoodPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeAcidRefluxFoodDate").val((result["485VerbalizeAcidRefluxFoodDate"] !== null && result["485VerbalizeAcidRefluxFoodDate"] !== undefined ? result["485VerbalizeAcidRefluxFoodDate"].Answer : " "));
                    }
                    if (eliminationStatusGoalsArray[i] == 7) {
                        $("#Recertification_485VerbalizeReduceAcidRefluxDate").val((result["485VerbalizeReduceAcidRefluxDate"] !== null && result["485VerbalizeReduceAcidRefluxDate"] !== undefined ? result["485VerbalizeReduceAcidRefluxDate"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485EliminationGoalTemplates").val((result["485EliminationGoalTemplates"] !== null && result["485EliminationGoalTemplates"] !== undefined ? result["485EliminationGoalTemplates"].Answer : " "));
            $("#Recertification_485EliminationGoalComments").val((result["485EliminationGoalComments"] !== null && result["485EliminationGoalComments"] !== undefined ? result["485EliminationGoalComments"].Answer : " "));

            var nutrition = result["GenericNutrition"];
            if (nutrition !== null && nutrition != undefined && nutrition.Answer != null) {
                var nutritionArray = (nutrition.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionArray.length; i++) {
                    $('input[name=Recertification_GenericNutrition][value=' + nutritionArray[i] + ']').attr('checked', true);
                    if (nutritionArray[i] == 4) {
                        $('input[name=Recertification_GenericNutritionWeightGainLoss][value=' + (result["GenericNutritionWeightGainLoss"] != null && result["GenericNutritionWeightGainLoss"] != undefined ? result["GenericNutritionWeightGainLoss"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_GenericNutritionWeightAmount").val((result["GenericNutritionWeightAmount"] !== null && result["GenericNutritionWeightAmount"] !== undefined ? result["GenericNutritionWeightAmount"].Answer : " "));
                        $("#Recertification_GenericNutritionWeightAmountIn").val((result["GenericNutritionWeightAmountIn"] !== null && result["GenericNutritionWeightAmountIn"] !== undefined ? result["GenericNutritionWeightAmountIn"].Answer : " "));
                    }
                    if (nutritionArray[i] == 6) {
                        $('input[name=Recertification_GenericNutritionDietAdequate][value=' + (result["GenericNutritionDietAdequate"] != null && result["GenericNutritionDietAdequate"] != undefined ? result["GenericNutritionDietAdequate"].Answer : "") + ']').attr('checked', true);
                        var nutritionDiet = result["GenericNutritionDiet"];
                        if (nutritionDiet !== null && nutritionDiet != undefined && nutritionDiet.Answer != null) {
                            var nutritionDietArray = (nutritionDiet.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < nutritionDietArray.length; j++) {
                                $('input[name=Recertification_GenericNutritionDiet][value=' + nutritionDietArray[j] + ']').attr('checked', true);

                            }
                        }
                    }

                    if (nutritionArray[i] == 7) {
                        $("#Recertification_GenericNutritionResidualCheckedAmount").val((result["GenericNutritionResidualCheckedAmount"] !== null && result["GenericNutritionResidualCheckedAmount"] !== undefined ? result["GenericNutritionResidualCheckedAmount"].Answer : " "));
                        var nutritionResidualProblem = result["GenericNutritionResidualProblem"];
                        if (nutritionResidualProblem !== null && nutritionResidualProblem != undefined && nutritionResidualProblem.Answer != null) {
                            var nutritionResidualProblemArray = (nutritionResidualProblem.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < nutritionResidualProblemArray.length; j++) {
                                $('input[name=Recertification_GenericNutritionResidualProblem][value=' + nutritionResidualProblemArray[j] + ']').attr('checked', true);

                                if (nutritionResidualProblemArray[j] == 7) {
                                    $("#Recertification_GenericNutritionOtherDetails").val((result["GenericNutritionOtherDetails"] !== null && result["GenericNutritionOtherDetails"] !== undefined ? result["GenericNutritionOtherDetails"].Answer : " "));
                                }
                            }
                        }
                    }

                }
            }
            $("#Recertification_GenericNutritionComments").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));

            var nutritionalHealth = result["GenericNutritionalHealth"];
            if (nutritionalHealth !== null && nutritionalHealth != undefined && nutritionalHealth.Answer != null) {
                var nutritionalHealthArray = (nutritionalHealth.Answer).split(',');
                var j = 0;
                for (j = 0; j < nutritionalHealthArray.length; j++) {
                    $('input[name=Recertification_GenericNutritionalHealth][value=' + nutritionalHealthArray[j] + ']').attr('checked', true);

                }
            }
            Oasis.CalculateNutritionScore('Recertification');
            $("#Recertification_GenericNutritionalStatusComments").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));
            var nutritionDiffect = result["GenericNutritionDiffect"];
            if (nutritionDiffect !== null && nutritionDiffect != undefined && nutritionDiffect.Answer != null) {
                var nutritionDiffectArray = (nutritionDiffect.Answer).split(',');
                var j = 0;
                for (j = 0; j < nutritionDiffectArray.length; j++) {
                    $('input[name=Recertification_GenericNutritionDiffect][value=' + nutritionDiffectArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_GenericMealsPreparedBy").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));

            var requirements = result["GenericDietRequirements"];
            if (requirements !== null && requirements != undefined && requirements.Answer != null) {
                var requirementsArray = (requirements.Answer).split(',');
                var i = 0;
                for (i = 0; i < requirementsArray.length; i++) {
                    $('input[name=Recertification_GenericDietRequirements][value=' + requirementsArray[i] + ']').attr('checked', true);
                    if (requirementsArray[i] == 1) {
                        $("#Recertification_GenericNutritionSodiumAmount").val((result["GenericNutritionSodiumAmount"] !== null && result["GenericNutritionSodiumAmount"] !== undefined ? result["GenericNutritionSodiumAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 3) {
                        $("#Recertification_GenericCalorieADADietAmount").val((result["GenericCalorieADADietAmount"] !== null && result["GenericCalorieADADietAmount"] !== undefined ? result["GenericCalorieADADietAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 5) {
                        $("#Recertification_GenericNutritionHighProteinAmount").val((result["GenericNutritionHighProteinAmount"] !== null && result["GenericNutritionHighProteinAmount"] !== undefined ? result["GenericNutritionHighProteinAmount"].Answer : " "));

                    }
                    if (requirementsArray[i] == 6) {
                        $("#Recertification_GenericNutritionLowProteinAmount").val((result["GenericNutritionLowProteinAmount"] !== null && result["GenericNutritionLowProteinAmount"] !== undefined ? result["GenericNutritionLowProteinAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 7) {
                        $('input[name=Recertification_GenericCarbohydrateLevel][value=' + (result["GenericCarbohydrateLevel"] != null && result["GenericCarbohydrateLevel"] != undefined ? result["GenericCarbohydrateLevel"].Answer : "") + ']').attr('checked', true);
                    }
                    if (requirementsArray[i] == 10) {
                        $("#Recertification_GenericNutritionSupplementType").val((result["GenericNutritionSupplementType"] !== null && result["GenericNutritionSupplementType"] !== undefined ? result["GenericNutritionSupplementType"].Answer : " "));

                    }
                    if (requirementsArray[i] == 13) {
                        $("#Recertification_GenericFluidRestrictionAmount").val((result["GenericFluidRestrictionAmount"] !== null && result["GenericFluidRestrictionAmount"] !== undefined ? result["GenericFluidRestrictionAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 14) {
                        $("#Recertification_GenericPhysicianDietOtherName").val((result["GenericPhysicianDietOtherName"] !== null && result["GenericPhysicianDietOtherName"] !== undefined ? result["GenericPhysicianDietOtherName"].Answer : " "));
                    }

                    if (requirementsArray[i] == 19) {
                        $("#Recertification_GenericEnteralNutritionDesc").val((result["GenericEnteralNutritionDesc"] !== null && result["GenericEnteralNutritionDesc"] !== undefined ? result["GenericEnteralNutritionDesc"].Answer : " "));
                        $("#Recertification_GenericEnteralNutritionAmount").val((result["GenericEnteralNutritionAmount"] !== null && result["GenericEnteralNutritionAmount"] !== undefined ? result["GenericEnteralNutritionAmount"].Answer : " "));
                        var enteralNutrition = result["GenericEnteralNutrition"];
                        if (enteralNutrition !== null && enteralNutrition != undefined && enteralNutrition.Answer != null) {
                            var enteralNutritionArray = (enteralNutrition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < enteralNutritionArray.length; j++) {
                                $('input[name=Recertification_GenericEnteralNutrition][value=' + enteralNutritionArray[j] + ']').attr('checked', true);
                                if (enteralNutritionArray[j] == 1) {
                                    $("#Recertification_GenericEnteralNutritionPumpType").val((result["GenericEnteralNutritionPumpType"] !== null && result["GenericEnteralNutritionPumpType"] !== undefined ? result["GenericEnteralNutritionPumpType"].Answer : " "));
                                }
                            }
                        }

                    }

                    if (requirementsArray[i] == 20) {
                        $("#Recertification_GenericTPNAmount").val((result["GenericTPNAmount"] !== null && result["GenericTPNAmount"] !== undefined ? result["GenericTPNAmount"].Answer : " "));
                        $("#Recertification_GenericTPNVia").val((result["GenericTPNVia"] !== null && result["GenericTPNVia"] !== undefined ? result["GenericTPNVia"].Answer : " "));
                    }
                }
            }

            var nutritionInterventions = result["485NutritionInterventions"];
            if (nutritionInterventions !== null && nutritionInterventions != undefined && nutritionInterventions.Answer != null) {
                var nutritionInterventionsArray = (nutritionInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionInterventionsArray.length; i++) {
                    $('input[name=Recertification_485NutritionInterventions][value=' + nutritionInterventionsArray[i] + ']').attr('checked', true);
                    if (nutritionInterventionsArray[i] == 1) {
                        $("#Recertification_485InstructOnDietPerson").val((result["485InstructOnDietPerson"] !== null && result["485InstructOnDietPerson"] !== undefined ? result["485InstructOnDietPerson"].Answer : " "));
                        $("#Recertification_485InstructOnDietDesc").val((result["485InstructOnDietDesc"] !== null && result["485InstructOnDietDesc"] !== undefined ? result["485InstructOnDietDesc"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 3) {
                        $("#Recertification_485InstructKeepDietLogPerson").val((result["485InstructKeepDietLogPerson"] !== null && result["485InstructKeepDietLogPerson"] !== undefined ? result["485InstructKeepDietLogPerson"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 4) {
                        $("#Recertification_485InstructPromoteOralIntakePerson").val((result["485InstructPromoteOralIntakePerson"] !== null && result["485InstructPromoteOralIntakePerson"] !== undefined ? result["485InstructPromoteOralIntakePerson"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 5) {
                        $("#Recertification_485InstructParenteralNutritionPerson").val((result["485InstructParenteralNutritionPerson"] !== null && result["485InstructParenteralNutritionPerson"] !== undefined ? result["485InstructParenteralNutritionPerson"].Answer : " "));
                        $("#Recertification_485InstructParenteralNutritionInclude").val((result["485InstructParenteralNutritionInclude"] !== null && result["485InstructParenteralNutritionInclude"] !== undefined ? result["485InstructParenteralNutritionInclude"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 6) {
                        $("#Recertification_485InstructEnteralNutritionPerson").val((result["485InstructEnteralNutritionPerson"] !== null && result["485InstructEnteralNutritionPerson"] !== undefined ? result["485InstructEnteralNutritionPerson"].Answer : " "));
                        $("#Recertification_485InstructEnteralNutritionInclude").val((result["485InstructEnteralNutritionInclude"] !== null && result["485InstructEnteralNutritionInclude"] !== undefined ? result["485InstructEnteralNutritionInclude"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 7) {
                        $("#Recertification_485InstructCareOfTubePerson").val((result["485InstructCareOfTubePerson"] !== null && result["485InstructCareOfTubePerson"] !== undefined ? result["485InstructCareOfTubePerson"].Answer : " "));
                        $("#Recertification_485InstructCareOfTubeDesc").val((result["485InstructCareOfTubeDesc"] !== null && result["485InstructCareOfTubeDesc"] !== undefined ? result["485InstructCareOfTubeDesc"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 8) {
                        $("#Recertification_485ChangeTubeEveryType").val((result["485ChangeTubeEveryType"] !== null && result["485ChangeTubeEveryType"] !== undefined ? result["485ChangeTubeEveryType"].Answer : " "));
                        $("#Recertification_485ChangeTubeEveryFreq").val((result["485ChangeTubeEveryFreq"] !== null && result["485ChangeTubeEveryFreq"] !== undefined ? result["485ChangeTubeEveryFreq"].Answer : " "));
                        $("#Recertification_485ChangeTubeEveryDate").val((result["485ChangeTubeEveryDate"] !== null && result["485ChangeTubeEveryDate"] !== undefined ? result["485ChangeTubeEveryDate"].Answer : " "));

                    }

                    if (nutritionInterventionsArray[i] == 9) {
                        $("#Recertification_485IrrigateTubeWithType").val((result["485IrrigateTubeWithType"] !== null && result["485IrrigateTubeWithType"] !== undefined ? result["485IrrigateTubeWithType"].Answer : " "));
                        $("#Recertification_485IrrigateTubeWithAmount").val((result["485IrrigateTubeWithAmount"] !== null && result["485IrrigateTubeWithAmount"] !== undefined ? result["485IrrigateTubeWithAmount"].Answer : " "));
                        $("#Recertification_485IrrigateTubeWithDesc").val((result["485IrrigateTubeWithDesc"] !== null && result["485IrrigateTubeWithDesc"] !== undefined ? result["485IrrigateTubeWithDesc"].Answer : " "));
                        $("#Recertification_485IrrigateTubeWithEvery").val((result["485IrrigateTubeWithEvery"] !== null && result["485IrrigateTubeWithEvery"] !== undefined ? result["485IrrigateTubeWithEvery"].Answer : " "));
                        $("#Recertification_485IrrigateTubeWithAsNeeded").val((result["485IrrigateTubeWithAsNeeded"] !== null && result["485IrrigateTubeWithAsNeeded"] !== undefined ? result["485IrrigateTubeWithAsNeeded"].Answer : " "));
                        $('input[name=Recertification_485IrrigateTubeWithFreq][value=' + (result["485IrrigateTubeWithFreq"] != null && result["485IrrigateTubeWithFreq"] != undefined ? result["485IrrigateTubeWithFreq"].Answer : "") + ']').attr('checked', true);
                    }
                    if (nutritionInterventionsArray[i] == 10) {
                        $("#Recertification_485InstructFreeWaterPerson").val((result["485InstructFreeWaterPerson"] !== null && result["485InstructFreeWaterPerson"] !== undefined ? result["485InstructFreeWaterPerson"].Answer : " "));
                        $("#Recertification_485InstructFreeWaterAmount").val((result["485InstructFreeWaterAmount"] !== null && result["485InstructFreeWaterAmount"] !== undefined ? result["485InstructFreeWaterAmount"].Answer : " "));
                        $("#Recertification_485InstructFreeWaterEvery").val((result["485InstructFreeWaterEvery"] !== null && result["485InstructFreeWaterEvery"] !== undefined ? result["485InstructFreeWaterEvery"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485NutritionOrderTemplates").val((result["485NutritionOrderTemplates"] !== null && result["485NutritionOrderTemplates"] !== undefined ? result["485NutritionOrderTemplates"].Answer : " "));
            $("#Recertification_485NutritionComments").val((result["485NutritionComments"] !== null && result["485NutritionComments"] !== undefined ? result["485NutritionComments"].Answer : " "));

            var nutritionGoals = result["485NutritionGoals"];
            if (nutritionGoals !== null && nutritionGoals != undefined && nutritionGoals.Answer != null) {
                var nutritionGoalsArray = (nutritionGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionGoalsArray.length; i++) {
                    $('input[name=Recertification_485NutritionGoals][value=' + nutritionGoalsArray[i] + ']').attr('checked', true);
                    if (nutritionGoalsArray[i] == 1) {
                        $("#Recertification_485MaintainDietComplianceType").val((result["485MaintainDietComplianceType"] !== null && result["485MaintainDietComplianceType"] !== undefined ? result["485MaintainDietComplianceType"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 2) {
                        $("#Recertification_485DemonstrateDietCompliancePerson").val((result["485DemonstrateDietCompliancePerson"] !== null && result["485DemonstrateDietCompliancePerson"] !== undefined ? result["485DemonstrateDietCompliancePerson"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 3) {
                        $("#Recertification_485DemonstrateEnteralNutritionPerson").val((result["485DemonstrateEnteralNutritionPerson"] !== null && result["485DemonstrateEnteralNutritionPerson"] !== undefined ? result["485DemonstrateEnteralNutritionPerson"].Answer : " "));
                        $("#Recertification_485DemonstrateEnteralNutritionDate").val((result["485DemonstrateEnteralNutritionDate"] !== null && result["485DemonstrateEnteralNutritionDate"] !== undefined ? result["485DemonstrateEnteralNutritionDate"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 4) {
                        $("#Recertification_485DemonstrateParenteralNutritionPerson").val((result["485DemonstrateParenteralNutritionPerson"] !== null && result["485DemonstrateParenteralNutritionPerson"] !== undefined ? result["485DemonstrateParenteralNutritionPerson"].Answer : " "));
                        $("#Recertification_485DemonstrateParenteralNutritionDate").val((result["485DemonstrateParenteralNutritionDate"] !== null && result["485DemonstrateParenteralNutritionDate"] !== undefined ? result["485DemonstrateParenteralNutritionDate"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 5) {
                        $("#Recertification_485DemonstrateCareOfTubePerson").val((result["485DemonstrateCareOfTubePerson"] !== null && result["485DemonstrateCareOfTubePerson"] !== undefined ? result["485DemonstrateCareOfTubePerson"].Answer : " "));
                        $("#Recertification_485DemonstrateCareOfTubeType").val((result["485DemonstrateCareOfTubeType"] !== null && result["485DemonstrateCareOfTubeType"] !== undefined ? result["485DemonstrateCareOfTubeType"].Answer : " "));
                        $("#Recertification_485DemonstrateCareOfTubeDate").val((result["485DemonstrateCareOfTubeDate"] !== null && result["485DemonstrateCareOfTubeDate"] !== undefined ? result["485DemonstrateCareOfTubeDate"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485NutritionGoalTemplates").val((result["485NutritionGoalTemplates"] !== null && result["485NutritionGoalTemplates"] !== undefined ? result["485NutritionGoalTemplates"].Answer : " "));
            $("#Recertification_485NutritionGoalComments").val((result["485NutritionGoalComments"] !== null && result["485NutritionGoalComments"] !== undefined ? result["485NutritionGoalComments"].Answer : " "));

            var neurologicalOriented = result["GenericNeurologicalOriented"];
            if (neurologicalOriented !== null && neurologicalOriented != undefined && neurologicalOriented.Answer != null) {
                var neurologicalOrientedArray = (neurologicalOriented.Answer).split(',');
                var j = 0;
                for (j = 0; j < neurologicalOrientedArray.length; j++) {
                    $('input[name=Recertification_GenericNeurologicalOriented][value=' + neurologicalOrientedArray[j] + ']').attr('checked', true);

                }
            }
            var neurologicalStatus = result["GenericNeurologicalStatus"];
            if (neurologicalStatus !== null && neurologicalStatus != undefined && neurologicalStatus.Answer != null) {
                var neurologicalStatusArray = (neurologicalStatus.Answer).split(',');
                var j = 0;
                for (j = 0; j < neurologicalStatusArray.length; j++) {
                    $('input[name=Recertification_GenericNeurologicalStatus][value=' + neurologicalStatusArray[j] + ']').attr('checked', true);
                    if (neurologicalStatusArray[j] == 5) {
                        $("#Recertification_GenericNeurologicalTremorsLocation").val((result["GenericNeurologicalTremorsLocation"] !== null && result["GenericNeurologicalTremorsLocation"] !== undefined ? result["GenericNeurologicalTremorsLocation"].Answer : " "));
                    }

                }
            }

            var psychosocial = result["GenericPsychosocial"];
            if (psychosocial !== null && psychosocial != undefined && psychosocial.Answer != null) {
                var psychosocialArray = (psychosocial.Answer).split(',');
                var j = 0;
                for (j = 0; j < psychosocialArray.length; j++) {
                    $('input[name=Recertification_GenericPsychosocial][value=' + psychosocialArray[j] + ']').attr('checked', true);

                }
            }

            $("#Recertification_GenericNeuroEmoBehaviorComments").val((result["GenericNeuroEmoBehaviorComments"] !== null && result["GenericNeuroEmoBehaviorComments"] !== undefined ? result["GenericNeuroEmoBehaviorComments"].Answer : " "));

            var behaviorInterventions = result["485BehaviorInterventions"];
            if (behaviorInterventions !== null && behaviorInterventions != undefined && behaviorInterventions.Answer != null) {
                var behaviorInterventionsArray = (behaviorInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < behaviorInterventionsArray.length; i++) {
                    $('input[name=Recertification_485BehaviorInterventions][value=' + behaviorInterventionsArray[i] + ']').attr('checked', true);
                    if (behaviorInterventionsArray[i] == 4) {
                        $("#Recertification_485InstructSeizurePrecautionPerson").val((result["485InstructSeizurePrecautionPerson"] !== null && result["485InstructSeizurePrecautionPerson"] !== undefined ? result["485InstructSeizurePrecautionPerson"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 6) {
                        $('input[name=Recertification_485MSWProvideServiceNumberVisits][value=' + (result["485MSWProvideServiceNumberVisits"] != null && result["485MSWProvideServiceNumberVisits"] != undefined ? result["485MSWProvideServiceNumberVisits"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_485MSWProvideServiceVisitAmount").val((result["485MSWProvideServiceVisitAmount"] !== null && result["485MSWProvideServiceVisitAmount"] !== undefined ? result["485MSWProvideServiceVisitAmount"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 7) {
                        $('input[name=Recertification_485MSWLongTermPlanningVisits][value=' + (result["485MSWLongTermPlanningVisits"] != null && result["485MSWLongTermPlanningVisits"] != undefined ? result["485MSWLongTermPlanningVisits"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_485MSWLongTermPlanningVisitAmount").val((result["485MSWLongTermPlanningVisitAmount"] !== null && result["485MSWLongTermPlanningVisitAmount"] !== undefined ? result["485MSWLongTermPlanningVisitAmount"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 8) {
                        $('input[name=Recertification_485MSWCommunityAssistanceVisits][value=' + (result["485MSWCommunityAssistanceVisits"] != null && result["485MSWCommunityAssistanceVisits"] != undefined ? result["485MSWCommunityAssistanceVisits"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_485MSWCommunityAssistanceVisitAmount").val((result["485MSWCommunityAssistanceVisitAmount"] !== null && result["485MSWCommunityAssistanceVisitAmount"] !== undefined ? result["485MSWCommunityAssistanceVisitAmount"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485BehaviorOrderTemplates").val((result["485BehaviorOrderTemplates"] !== null && result["485BehaviorOrderTemplates"] !== undefined ? result["485BehaviorOrderTemplates"].Answer : " "));
            $("#Recertification_485BehaviorComments").val((result["485BehaviorComments"] !== null && result["485BehaviorComments"] !== undefined ? result["485BehaviorComments"].Answer : " "));

            var behaviorGoals = result["485BehaviorGoals"];
            if (behaviorGoals !== null && behaviorGoals != undefined && behaviorGoals.Answer != null) {
                var behaviorGoalsArray = (behaviorGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < behaviorGoalsArray.length; i++) {
                    $('input[name=Recertification_485BehaviorGoals][value=' + behaviorGoalsArray[i] + ']').attr('checked', true);
                    if (behaviorGoalsArray[i] == 2) {
                        $("#Recertification_485VerbalizeSeizurePrecautionsPerson").val((result["485VerbalizeSeizurePrecautionsPerson"] !== null && result["485VerbalizeSeizurePrecautionsPerson"] !== undefined ? result["485VerbalizeSeizurePrecautionsPerson"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485BehaviorGoalTemplates").val((result["485BehaviorGoalTemplates"] !== null && result["485BehaviorGoalTemplates"] !== undefined ? result["485BehaviorGoalTemplates"].Answer : " "));
            $("#Recertification_485BehaviorGoalComments").val((result["485BehaviorGoalComments"] !== null && result["485BehaviorGoalComments"] !== undefined ? result["485BehaviorGoalComments"].Answer : " "));

            var activitiesPermitted = result["485ActivitiesPermitted"];
            if (activitiesPermitted !== null && activitiesPermitted != undefined && activitiesPermitted.Answer != null) {
                var activitiesPermittedArray = (activitiesPermitted.Answer).split(',');
                var j = 0;
                for (j = 0; j < activitiesPermittedArray.length; j++) {
                    $('input[name=Recertification_485ActivitiesPermitted][value=' + activitiesPermittedArray[j] + ']').attr('checked', true);
                    if (activitiesPermittedArray[j] == 11) {
                        $("#Recertification_485ActivitiesPermittedOther").val((result["485ActivitiesPermittedOther"] !== null && result["485ActivitiesPermittedOther"] !== undefined ? result["485ActivitiesPermittedOther"].Answer : " "));
                    }

                }
            }

            var musculoskeletal = result["GenericMusculoskeletal"];
            if (musculoskeletal !== null && musculoskeletal != undefined && musculoskeletal.Answer != null) {
                var musculoskeletalArray = (musculoskeletal.Answer).split(',');
                var j = 0;
                for (j = 0; j < musculoskeletalArray.length; j++) {
                    $('input[name=Recertification_GenericMusculoskeletal][value=' + musculoskeletalArray[j] + ']').attr('checked', true);
                    if (musculoskeletalArray[j] == 4) {
                        $("#Recertification_GenericLimitedMobilityLocation").val((result["GenericLimitedMobilityLocation"] !== null && result["GenericLimitedMobilityLocation"] !== undefined ? result["GenericLimitedMobilityLocation"].Answer : " "));
                    }
                    if (musculoskeletalArray[j] == 5) {
                        $("#Recertification_GenericJointPainLocation").val((result["GenericJointPainLocation"] !== null && result["GenericJointPainLocation"] !== undefined ? result["GenericJointPainLocation"].Answer : " "));
                    }
                    if (musculoskeletalArray[j] == 7) {
                        $('input[name=Recertification_GenericGripStrengthEqual][value=' + (result["GenericGripStrengthEqual"] != null && result["GenericGripStrengthEqual"] != undefined ? result["GenericGripStrengthEqual"].Answer : "") + ']').attr('checked', true);
                        $("#Recertification_GenericGripStrengthDesc").val((result["GenericGripStrengthDesc"] !== null && result["GenericGripStrengthDesc"] !== undefined ? result["GenericGripStrengthDesc"].Answer : " "));
                    }

                }
            }

            var boundType = result["GenericBoundType"];
            if (boundType !== null && boundType != undefined && boundType.Answer != null) {
                var boundTypeArray = (boundType.Answer).split(',');
                var j = 0;
                for (j = 0; j < boundTypeArray.length; j++) {
                    $('input[name=Recertification_GenericBoundType][value=' + boundTypeArray[j] + ']').attr('checked', true);
                    if (boundTypeArray[j] == 3) {
                        $("#Recertification_GenericContractureLocation").val((result["GenericContractureLocation"] !== null && result["GenericContractureLocation"] !== undefined ? result["GenericContractureLocation"].Answer : " "));
                    }
                    if (boundTypeArray[j] == 4) {
                        $("#Recertification_GenericParalysisLocation").val((result["GenericParalysisLocation"] !== null && result["GenericParalysisLocation"] !== undefined ? result["GenericParalysisLocation"].Answer : " "));
                        $('input[name=Recertification_GenericParalysisDominant][value=' + (result["GenericParalysisDominant"] != null && result["GenericParalysisDominant"] != undefined ? result["GenericParalysisDominant"].Answer : "") + ']').attr('checked', true);
                    }
                    if (boundTypeArray[j] == 5) {

                        $("#Recertification_GenericAssistiveDeviceType").val((result["GenericAssistiveDeviceType"] !== null && result["GenericAssistiveDeviceType"] !== undefined ? result["GenericAssistiveDeviceType"].Answer : " "));
                    }

                }
            }
            $("#Recertification_GenericMusculoskeletalComments").val((result["GenericMusculoskeletalComments"] !== null && result["GenericMusculoskeletalComments"] !== undefined ? result["GenericMusculoskeletalComments"].Answer : " "));

            var nursingInterventions = result["485NursingInterventions"];
            if (nursingInterventions !== null && nursingInterventions != undefined && nursingInterventions.Answer != null) {
                var nursingInterventionsArray = (nursingInterventions.Answer).split(',');
                var j = 0;
                for (j = 0; j < nursingInterventionsArray.length; j++) {
                    $('input[name=Recertification_485NursingInterventions][value=' + nursingInterventionsArray[j] + ']').attr('checked', true);
                    if (nursingInterventionsArray[j] == 1) {
                        $("#Recertification_485PhysicalTherapyFreq").val((result["485PhysicalTherapyFreq"] !== null && result["485PhysicalTherapyFreq"] !== undefined ? result["485PhysicalTherapyFreq"].Answer : " "));
                        $("#Recertification_485PhysicalTherapyDate").val((result["485PhysicalTherapyDate"] !== null && result["485PhysicalTherapyDate"] !== undefined ? result["485PhysicalTherapyDate"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 2) {
                        $("#Recertification_485OccupationalTherapyFreq").val((result["485OccupationalTherapyFreq"] !== null && result["485OccupationalTherapyFreq"] !== undefined ? result["485OccupationalTherapyFreq"].Answer : " "));
                        $("#Recertification_485OccupationalTherapyDate").val((result["485OccupationalTherapyDate"] !== null && result["485OccupationalTherapyDate"] !== undefined ? result["485OccupationalTherapyDate"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 3) {

                        $("#Recertification_485HomeHealthAideFreq").val((result["485HomeHealthAideFreq"] !== null && result["485HomeHealthAideFreq"] !== undefined ? result["485HomeHealthAideFreq"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 6) {

                        $("#Recertification_485InstructRomExcercisePerson").val((result["485InstructRomExcercisePerson"] !== null && result["485InstructRomExcercisePerson"] !== undefined ? result["485InstructRomExcercisePerson"].Answer : " "));
                    }

                }
            }

            $("#Recertification_485ADLOrderTemplates").val((result["485ADLOrderTemplates"] !== null && result["485ADLOrderTemplates"] !== undefined ? result["485ADLOrderTemplates"].Answer : " "));
            $("#Recertification_485ADLComments").val((result["485ADLComments"] !== null && result["485ADLComments"] !== undefined ? result["485ADLComments"].Answer : " "));

            var nursingGoals = result["485NursingGoals"];
            if (nursingGoals !== null && nursingGoals != undefined && nursingGoals.Answer != null) {
                var nursingGoalsArray = (nursingGoals.Answer).split(',');
                var j = 0;
                for (j = 0; j < nursingGoalsArray.length; j++) {
                    $('input[name=Recertification_485NursingGoals][value=' + nursingGoalsArray[j] + ']').attr('checked', true);
                    if (nursingGoalsArray[j] == 4) {
                        $("#Recertification_485DemonstrateROMExcercisePerson").val((result["485DemonstrateROMExcercisePerson"] !== null && result["485DemonstrateROMExcercisePerson"] !== undefined ? result["485DemonstrateROMExcercisePerson"].Answer : " "));

                    }

                }
            }
            $("#Recertification_485ADLGoalTemplates").val((result["485ADLGoalTemplates"] !== null && result["485ADLGoalTemplates"] !== undefined ? result["485ADLGoalTemplates"].Answer : " "));
            $("#Recertification_485ADLGoalComments").val((result["485ADLGoalComments"] !== null && result["485ADLGoalComments"] !== undefined ? result["485ADLGoalComments"].Answer : " "));

            $('input[name=Recertification_GenericAge65Plus][value=' + (result["GenericAge65Plus"] != null && result["GenericAge65Plus"] != undefined ? result["GenericAge65Plus"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericHypotensionDiagnosis][value=' + (result["GenericHypotensionDiagnosis"] != null && result["GenericHypotensionDiagnosis"] != undefined ? result["GenericHypotensionDiagnosis"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericPriorFalls][value=' + (result["GenericPriorFalls"] != null && result["GenericPriorFalls"] != undefined ? result["GenericPriorFalls"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericFallIncontinence][value=' + (result["GenericFallIncontinence"] != null && result["GenericFallIncontinence"] != undefined ? result["GenericFallIncontinence"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericVisualImpairment][value=' + (result["GenericVisualImpairment"] != null && result["GenericVisualImpairment"] != undefined ? result["GenericVisualImpairment"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericImpairedFunctionalMobility][value=' + (result["GenericImpairedFunctionalMobility"] != null && result["GenericImpairedFunctionalMobility"] != undefined ? result["GenericImpairedFunctionalMobility"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericEnvHazards][value=' + (result["GenericEnvHazards"] != null && result["GenericEnvHazards"] != undefined ? result["GenericEnvHazards"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericPolyPharmacy][value=' + (result["GenericPolyPharmacy"] != null && result["GenericPolyPharmacy"] != undefined ? result["GenericPolyPharmacy"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericPainAffectingFunction][value=' + (result["GenericPainAffectingFunction"] != null && result["GenericPainAffectingFunction"] != undefined ? result["GenericPainAffectingFunction"].Answer : "") + ']').attr('checked', true);
            $('input[name=Recertification_GenericCognitiveImpairment][value=' + (result["GenericCognitiveImpairment"] != null && result["GenericCognitiveImpairment"] != undefined ? result["GenericCognitiveImpairment"].Answer : "") + ']').attr('checked', true);

            var instructInterventions = result["485InstructInterventions"];
            if (instructInterventions !== null && instructInterventions != undefined && instructInterventions.Answer != null) {
                var instructInterventionsArray = (instructInterventions.Answer).split(',');
                var j = 0;
                for (j = 0; j < instructInterventionsArray.length; j++) {
                    $('input[name=Recertification_485InstructInterventions][value=' + instructInterventionsArray[j] + ']').attr('checked', true);
                    if (instructInterventionsArray[j] == 4) {
                        $("#Recertification_485InstructRemoveRugsPerson").val((result["485InstructRemoveRugsPerson"] !== null && result["485InstructRemoveRugsPerson"] !== undefined ? result["485InstructRemoveRugsPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 5) {
                        $("#Recertification_485InstructRemoveClutterPerson").val((result["485InstructRemoveClutterPerson"] !== null && result["485InstructRemoveClutterPerson"] !== undefined ? result["485InstructRemoveClutterPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 6) {

                        $("#Recertification_485InstructContactForDizzinessPerson").val((result["485InstructContactForDizzinessPerson"] !== null && result["485InstructContactForDizzinessPerson"] !== undefined ? result["485InstructContactForDizzinessPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 10) {

                        $("#Recertification_485InstructAdequateLightingPerson").val((result["485InstructAdequateLightingPerson"] !== null && result["485InstructAdequateLightingPerson"] !== undefined ? result["485InstructAdequateLightingPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 11) {

                        $("#Recertification_485InstructContactForFallPerson").val((result["485InstructContactForFallPerson"] !== null && result["485InstructContactForFallPerson"] !== undefined ? result["485InstructContactForFallPerson"].Answer : " "));
                    }

                }
            }
            $("#Recertification_485IADLOrderTemplates").val((result["485IADLOrderTemplates"] !== null && result["485IADLOrderTemplates"] !== undefined ? result["485IADLOrderTemplates"].Answer : " "));
            $("#Recertification_485IADLComments").val((result["485IADLComments"] !== null && result["485IADLComments"] !== undefined ? result["485IADLComments"].Answer : " "));

            var instructGoals = result["485InstructGoals"];
            if (instructGoals !== null && instructGoals != undefined && instructGoals.Answer != null) {
                var instructGoalsArray = (instructGoals.Answer).split(',');
                var j = 0;
                for (j = 0; j < instructGoalsArray.length; j++) {
                    $('input[name=Recertification_485InstructGoals][value=' + instructGoalsArray[j] + ']').attr('checked', true);
                    if (instructGoalsArray[j] == 3) {
                        $("#Recertification_485VerbalizeAnnualEyeExamPerson").val((result["485VerbalizeAnnualEyeExamPerson"] !== null && result["485VerbalizeAnnualEyeExamPerson"] !== undefined ? result["485VerbalizeAnnualEyeExamPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeAnnualEyeExamDate").val((result["485VerbalizeAnnualEyeExamDate"] !== null && result["485VerbalizeAnnualEyeExamDate"] !== undefined ? result["485VerbalizeAnnualEyeExamDate"].Answer : " "));
                    }
                    if (instructGoalsArray[j] == 4) {

                        $("#Recertification_485RemoveClutterFromPathPerson").val((result["485RemoveClutterFromPathPerson"] !== null && result["485RemoveClutterFromPathPerson"] !== undefined ? result["485RemoveClutterFromPathPerson"].Answer : " "));
                        $("#Recertification_485RemoveClutterFromPathDate").val((result["485RemoveClutterFromPathDate"] !== null && result["485RemoveClutterFromPathDate"] !== undefined ? result["485RemoveClutterFromPathDate"].Answer : " "));
                    }
                    if (instructGoalsArray[j] == 5) {

                        $("#Recertification_485RemoveThrowRugsAndSecurePerson").val((result["485RemoveThrowRugsAndSecurePerson"] !== null && result["485RemoveThrowRugsAndSecurePerson"] !== undefined ? result["485RemoveThrowRugsAndSecurePerson"].Answer : " "));
                        $("#Recertification_485RemoveThrowRugsAndSecureDate").val((result["485RemoveThrowRugsAndSecureDate"] !== null && result["485RemoveThrowRugsAndSecureDate"] !== undefined ? result["485RemoveThrowRugsAndSecureDate"].Answer : " "));
                    }

                }
            }
            $("#Recertification_485IADLGoalTemplates").val((result["485IADLGoalTemplates"] !== null && result["485IADLGoalTemplates"] !== undefined ? result["485IADLGoalTemplates"].Answer : " "));
            $("#Recertification_485IADLGoalComments").val((result["485IADLGoalComments"] !== null && result["485IADLGoalComments"] !== undefined ? result["485IADLGoalComments"].Answer : " "));
            var dME = result["485DME"];
            if (dME !== null && dME != undefined && dME.Answer != null) {
                var dMEArray = (dME.Answer).split(',');
                var j = 0;
                for (j = 0; j < dMEArray.length; j++) {
                    $('input[name=Recertification_485DME][value=' + dMEArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_485DMEComments").val((result["485DMEComments"] !== null && result["485DMEComments"] !== undefined ? result["485DMEComments"].Answer : " "));

            var supplies = result["GenericSupplies"];
            if (supplies !== null && supplies != undefined && supplies.Answer != null) {
                var suppliesArray = (supplies.Answer).split(',');
                var j = 0;
                for (j = 0; j < suppliesArray.length; j++) {
                    $('input[name=Recertification_GenericSupplies][value=' + suppliesArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_GenericSuppliesComment").val((result["GenericSuppliesComment"] !== null && result["GenericSuppliesComment"] !== undefined ? result["GenericSuppliesComment"].Answer : " "));

            $("#Recertification_GenericDMEProviderName").val((result["GenericDMEProviderName"] !== null && result["GenericDMEProviderName"] !== undefined ? result["GenericDMEProviderName"].Answer : " "));
            $("#Recertification_GenericDMEProviderAddress").val((result["GenericDMEProviderAddress"] !== null && result["GenericDMEProviderAddress"] !== undefined ? result["GenericDMEProviderAddress"].Answer : " "));
            $("#Recertification_GenericDMEProviderPhone").val((result["GenericDMEProviderPhone"] !== null && result["GenericDMEProviderPhone"] !== undefined ? result["GenericDMEProviderPhone"].Answer : " "));
            $("#Recertification_GenericDMESuppliesProvided").val((result["GenericDMESuppliesProvided"] !== null && result["GenericDMESuppliesProvided"] !== undefined ? result["GenericDMESuppliesProvided"].Answer : " "));


            var supply = result["GenericSupply"];
            if (supply != null && supply.Answer != null) {
                var jsonData = supply.Answer;
                var data = eval("(" + jsonData + ")");
                if (data != null && data.Supply != null) {
                    if (data.Supply.length > 0) {
                        Oasis.addToSupplyTable(data.Supply, $("#suppliesTableRece"));
                    }
                    else {
                        Oasis.ClearRows($("#suppliesTableRece"));
                        Oasis.addTableRow('#suppliesTableRece');
                    }
                }
                else {
                    Oasis.ClearRows($("#suppliesTableRece"));
                    Oasis.addTableRow('#suppliesTableRece');
                }
            } else {
                Oasis.ClearRows($("#suppliesTableRece"));
                Oasis.addTableRow('#suppliesTableRece');
            }








            var newMedications = result["485NewMedications"];
            if (newMedications !== null && newMedications != undefined && newMedications.Answer != null) {
                var newMedicationsArray = (newMedications.Answer).split(',');
                var j = 0;
                for (j = 0; j < newMedicationsArray.length; j++) {
                    $('input[name=Recertification_485NewMedications][value=' + newMedicationsArray[j] + ']').attr('checked', true);
                    if (newMedicationsArray[j] == 1) {
                        $("#Recertification_485NewMedicationsLS1").val((result["485NewMedicationsLS1"] !== null && result["485NewMedicationsLS1"] !== undefined ? result["485NewMedicationsLS1"].Answer : " "));
                        $("#Recertification_485NewMedicationsDosage1").val((result["485NewMedicationsDosage1"] !== null && result["485NewMedicationsDosage1"] !== undefined ? result["485NewMedicationsDosage1"].Answer : " "));
                        $("#Recertification_485NewMedicationsClassification1").val((result["485NewMedicationsClassification1"] !== null && result["485NewMedicationsClassification1"] !== undefined ? result["485NewMedicationsClassification1"].Answer : " "));
                        $("#Recertification_485NewMedicationsFrequency1").val((result["485NewMedicationsFrequency1"] !== null && result["485NewMedicationsFrequency1"] !== undefined ? result["485NewMedicationsFrequency1"].Answer : " "));
                    }
                    if (newMedicationsArray[j] == 2) {

                        $("#Recertification_485NewMedicationsLS2").val((result["485NewMedicationsLS2"] !== null && result["485NewMedicationsLS2"] !== undefined ? result["485NewMedicationsLS2"].Answer : " "));
                        $("#Recertification_485NewMedicationsDosage2").val((result["485NewMedicationsDosage2"] !== null && result["485NewMedicationsDosage2"] !== undefined ? result["485NewMedicationsDosage2"].Answer : " "));
                        $("#Recertification_485NewMedicationsClassification2").val((result["485NewMedicationsClassification2"] !== null && result["485NewMedicationsClassification2"] !== undefined ? result["485NewMedicationsClassification2"].Answer : " "));
                        $("#Recertification_485NewMedicationsFrequency2").val((result["485NewMedicationsFrequency2"] !== null && result["485NewMedicationsFrequency2"] !== undefined ? result["485NewMedicationsFrequency2"].Answer : " "));
                    }
                    if (newMedicationsArray[j] == 3) {

                        $("#Recertification_485NewMedicationsLS3").val((result["485NewMedicationsLS3"] !== null && result["485NewMedicationsLS3"] !== undefined ? result["485NewMedicationsLS3"].Answer : " "));
                        $("#Recertification_485NewMedicationsDosage3").val((result["485NewMedicationsDosage3"] !== null && result["485NewMedicationsDosage3"] !== undefined ? result["485NewMedicationsDosage3"].Answer : " "));
                        $("#Recertification_485NewMedicationsClassification3").val((result["485NewMedicationsClassification3"] !== null && result["485NewMedicationsClassification3"] !== undefined ? result["485NewMedicationsClassification3"].Answer : " "));
                        $("#Recertification_485NewMedicationsFrequency3").val((result["485NewMedicationsFrequency3"] !== null && result["485NewMedicationsFrequency3"] !== undefined ? result["485NewMedicationsFrequency3"].Answer : " "));
                    }
                }
            }
            $("#Recertification_GenericMedRecTime").val((result["GenericMedRecTime"] !== null && result["GenericMedRecTime"] !== undefined ? result["GenericMedRecTime"].Answer : " "));
            $("#Recertification_GenericMedRecMedication").val((result["GenericMedRecMedication"] !== null && result["GenericMedRecMedication"] !== undefined ? result["GenericMedRecMedication"].Answer : " "));
            $("#Recertification_GenericMedRecDose").val((result["GenericMedRecDose"] !== null && result["GenericMedRecDose"] !== undefined ? result["GenericMedRecDose"].Answer : " "));
            $("#Recertification_GenericMedRecRoute").val((result["GenericMedRecRoute"] !== null && result["GenericMedRecRoute"] !== undefined ? result["GenericMedRecRoute"].Answer : " "));
            $("#Recertification_GenericMedRecFrequency").val((result["GenericMedRecFrequency"] !== null && result["GenericMedRecFrequency"] !== undefined ? result["GenericMedRecFrequency"].Answer : " "));
            $("#Recertification_GenericMedRecPRN").val((result["GenericMedRecPRN"] !== null && result["GenericMedRecPRN"] !== undefined ? result["GenericMedRecPRN"].Answer : " "));
            $("#Recertification_GenericMedRecLocation").val((result["GenericMedRecLocation"] !== null && result["GenericMedRecLocation"] !== undefined ? result["GenericMedRecLocation"].Answer : " "));
            $("#Recertification_GenericMedRecResponse").val((result["GenericMedRecResponse"] !== null && result["GenericMedRecResponse"] !== undefined ? result["GenericMedRecResponse"].Answer : " "));
            $("#Recertification_GenericMedRecComments").val((result["GenericMedRecComments"] !== null && result["GenericMedRecComments"] !== undefined ? result["GenericMedRecComments"].Answer : " "));

            $('input[name=Recertification_GenericIVAccess][value=' + (result["GenericIVAccess"] != null && result["GenericIVAccess"] != undefined ? result["GenericIVAccess"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_GenericIVAccessType").val((result["GenericIVAccessType"] !== null && result["GenericIVAccessType"] !== undefined ? result["GenericIVAccessType"].Answer : " "));
            $("#Recertification_GenericIVAccessDate").val((result["GenericIVAccessDate"] !== null && result["GenericIVAccessDate"] !== undefined ? result["GenericIVAccessDate"].Answer : " "));
            $("#Recertification_GenericIVAccessDressingChange").val((result["GenericIVAccessDressingChange"] !== null && result["GenericIVAccessDressingChange"] !== undefined ? result["GenericIVAccessDressingChange"].Answer : " "));


            var medicationInterventions = result["485MedicationInterventions"];
            if (medicationInterventions !== null && medicationInterventions != undefined && medicationInterventions.Answer != null) {
                var medicationInterventionsArray = (medicationInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicationInterventionsArray.length; i++) {
                    $('input[name=Recertification_485MedicationInterventions][value=' + medicationInterventionsArray[i] + ']').attr('checked', true);
                    if (medicationInterventionsArray[i] == 3) {
                        $("#Recertification_485DetermineFrequencEachMedPerson").val((result["485DetermineFrequencEachMedPerson"] !== null && result["485DetermineFrequencEachMedPerson"] !== undefined ? result["485DetermineFrequencEachMedPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 4) {
                        $("#Recertification_485AssessIndicationEachMedPerson").val((result["485AssessIndicationEachMedPerson"] !== null && result["485AssessIndicationEachMedPerson"] !== undefined ? result["485AssessIndicationEachMedPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 6) {
                        $("#Recertification_485AssessOpenMedContainersPerson").val((result["485AssessOpenMedContainersPerson"] !== null && result["485AssessOpenMedContainersPerson"] !== undefined ? result["485AssessOpenMedContainersPerson"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 7) {
                        $("#Recertification_485InstructMedicationRegimen").val((result["485InstructMedicationRegimen"] !== null && result["485InstructMedicationRegimen"] !== undefined ? result["485InstructMedicationRegimen"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 10) {
                        $("#Recertification_485AssessAdminInjectMedsPerson").val((result["485AssessAdminInjectMedsPerson"] !== null && result["485AssessAdminInjectMedsPerson"] !== undefined ? result["485AssessAdminInjectMedsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 12) {
                        $("#Recertification_485InstructHighRiskMedsPerson").val((result["485InstructHighRiskMedsPerson"] !== null && result["485InstructHighRiskMedsPerson"] !== undefined ? result["485InstructHighRiskMedsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 13) {
                        $("#Recertification_485InstructSignsSymptomsIneffectiveDrugPerson").val((result["485InstructSignsSymptomsIneffectiveDrugPerson"] !== null && result["485InstructSignsSymptomsIneffectiveDrugPerson"] !== undefined ? result["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 14) {
                        $("#Recertification_485InstructMedSideEffectsPerson").val((result["485InstructMedSideEffectsPerson"] !== null && result["485InstructMedSideEffectsPerson"] !== undefined ? result["485InstructMedSideEffectsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 15) {
                        $("#Recertification_485InstructMedReactionsPerson").val((result["485InstructMedReactionsPerson"] !== null && result["485InstructMedReactionsPerson"] !== undefined ? result["485InstructMedReactionsPerson"].Answer : " "));

                    }

                    if (medicationInterventionsArray[i] == 16) {
                        $("#Recertification_485AdministerIVType").val((result["485AdministerIVType"] !== null && result["485AdministerIVType"] !== undefined ? result["485AdministerIVType"].Answer : " "));
                        $("#Recertification_485AdministerIVRate").val((result["485AdministerIVRate"] !== null && result["485AdministerIVRate"] !== undefined ? result["485AdministerIVRate"].Answer : " "));
                        $("#Recertification_485AdministerIVVia").val((result["485AdministerIVVia"] !== null && result["485AdministerIVVia"] !== undefined ? result["485AdministerIVVia"].Answer : " "));
                        $("#Recertification_485AdministerIVEvery").val((result["485AdministerIVEvery"] !== null && result["485AdministerIVEvery"] !== undefined ? result["485AdministerIVEvery"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 17) {
                        $("#Recertification_485InstructAdministerIVPerson").val((result["485InstructAdministerIVPerson"] !== null && result["485InstructAdministerIVPerson"] !== undefined ? result["485InstructAdministerIVPerson"].Answer : " "));
                        $("#Recertification_485InstructAdministerIVRate").val((result["485InstructAdministerIVRate"] !== null && result["485InstructAdministerIVRate"] !== undefined ? result["485InstructAdministerIVRate"].Answer : " "));
                        $("#Recertification_485InstructAdministerIVVia").val((result["485InstructAdministerIVVia"] !== null && result["485InstructAdministerIVVia"] !== undefined ? result["485InstructAdministerIVVia"].Answer : " "));
                        $("#Recertification_485InstructAdministerIVEvery").val((result["485InstructAdministerIVEvery"] !== null && result["485InstructAdministerIVEvery"] !== undefined ? result["485InstructAdministerIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 18) {
                        $("#Recertification_485ChangePeripheralIVGauge").val((result["485ChangePeripheralIVGauge"] !== null && result["485ChangePeripheralIVGauge"] !== undefined ? result["485ChangePeripheralIVGauge"].Answer : " "));
                        $("#Recertification_485ChangePeripheralIVWidth").val((result["485ChangePeripheralIVWidth"] !== null && result["485ChangePeripheralIVWidth"] !== undefined ? result["485ChangePeripheralIVWidth"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 19) {
                        $("#Recertification_485FlushPeripheralIVWith").val((result["485FlushPeripheralIVWith"] !== null && result["485FlushPeripheralIVWith"] !== undefined ? result["485FlushPeripheralIVWith"].Answer : " "));
                        $("#Recertification_485FlushPeripheralIVOf").val((result["485FlushPeripheralIVOf"] !== null && result["485FlushPeripheralIVOf"] !== undefined ? result["485FlushPeripheralIVOf"].Answer : " "));
                        $("#Recertification_485FlushPeripheralIVEvery").val((result["485FlushPeripheralIVEvery"] !== null && result["485FlushPeripheralIVEvery"] !== undefined ? result["485FlushPeripheralIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 20) {
                        $("#Recertification_485InstructFlushPerpheralIVPerson").val((result["485InstructFlushPerpheralIVPerson"] !== null && result["485InstructFlushPerpheralIVPerson"] !== undefined ? result["485InstructFlushPerpheralIVPerson"].Answer : " "));
                        $("#Recertification_485InstructFlushPerpheralIVWith").val((result["485InstructFlushPerpheralIVWith"] !== null && result["485InstructFlushPerpheralIVWith"] !== undefined ? result["485InstructFlushPerpheralIVWith"].Answer : " "));
                        $("#Recertification_485InstructFlushPerpheralIVOf").val((result["485InstructFlushPerpheralIVOf"] !== null && result["485InstructFlushPerpheralIVOf"] !== undefined ? result["485InstructFlushPerpheralIVOf"].Answer : " "));
                        $("#Recertification_485InstructFlushPerpheralIVEvery").val((result["485InstructFlushPerpheralIVEvery"] !== null && result["485InstructFlushPerpheralIVEvery"] !== undefined ? result["485InstructFlushPerpheralIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 21) {
                        $("#Recertification_485ChangeCentralLineEvery").val((result["485ChangeCentralLineEvery"] !== null && result["485ChangeCentralLineEvery"] !== undefined ? result["485ChangeCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 22) {
                        $("#Recertification_485InstructChangeCentralLinePerson").val((result["485InstructChangeCentralLinePerson"] !== null && result["485InstructChangeCentralLinePerson"] !== undefined ? result["485InstructChangeCentralLinePerson"].Answer : " "));
                        $("#Recertification_485InstructChangeCentralLineEvery").val((result["485InstructChangeCentralLineEvery"] !== null && result["485InstructChangeCentralLineEvery"] !== undefined ? result["485InstructChangeCentralLineEvery"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 23) {
                        $("#Recertification_485FlushCentralLineWith").val((result["485FlushCentralLineWith"] !== null && result["485FlushCentralLineWith"] !== undefined ? result["485FlushCentralLineWith"].Answer : " "));
                        $("#Recertification_485FlushCentralLineOf").val((result["485FlushCentralLineOf"] !== null && result["485FlushCentralLineOf"] !== undefined ? result["485FlushCentralLineOf"].Answer : " "));
                        $("#Recertification_485FlushCentralLineEvery").val((result["485FlushCentralLineEvery"] !== null && result["485FlushCentralLineEvery"] !== undefined ? result["485FlushCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 24) {
                        $("#Recertification_485InstructFlushCentralLinePerson").val((result["485InstructFlushCentralLinePerson"] !== null && result["485InstructFlushCentralLinePerson"] !== undefined ? result["485InstructFlushCentralLinePerson"].Answer : " "));
                        $("#Recertification_485InstructFlushCentralLineWith").val((result["485InstructFlushCentralLineWith"] !== null && result["485InstructFlushCentralLineWith"] !== undefined ? result["485InstructFlushCentralLineWith"].Answer : " "));
                        $("#Recertification_485InstructFlushCentralLineOf").val((result["485InstructFlushCentralLineOf"] !== null && result["485InstructFlushCentralLineOf"] !== undefined ? result["485InstructFlushCentralLineOf"].Answer : " "));
                        $("#Recertification_485InstructFlushCentralLineEvery").val((result["485InstructFlushCentralLineEvery"] !== null && result["485InstructFlushCentralLineEvery"] !== undefined ? result["485InstructFlushCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 25) {
                        $("#Recertification_485AccessPortType").val((result["485AccessPortType"] !== null && result["485AccessPortType"] !== undefined ? result["485AccessPortType"].Answer : " "));
                        $("#Recertification_485AccessPortTypeEvery").val((result["485AccessPortTypeEvery"] !== null && result["485AccessPortTypeEvery"] !== undefined ? result["485AccessPortTypeEvery"].Answer : " "));
                        $("#Recertification_485AccessPortTypeWith").val((result["485AccessPortTypeWith"] !== null && result["485AccessPortTypeWith"] !== undefined ? result["485AccessPortTypeWith"].Answer : " "));
                        $("#Recertification_485AccessPortTypeOf").val((result["485AccessPortTypeOf"] !== null && result["485AccessPortTypeOf"] !== undefined ? result["485AccessPortTypeOf"].Answer : " "));
                        $("#Recertification_485AccessPortTypeFrequency").val((result["485AccessPortTypeFrequency"] !== null && result["485AccessPortTypeFrequency"] !== undefined ? result["485AccessPortTypeFrequency"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 26) {
                        $("#Recertification_485ChangePortDressingType").val((result["485ChangePortDressingType"] !== null && result["485ChangePortDressingType"] !== undefined ? result["485ChangePortDressingType"].Answer : " "));
                        $("#Recertification_485ChangePortDressingEvery").val((result["485ChangePortDressingEvery"] !== null && result["485ChangePortDressingEvery"] !== undefined ? result["485ChangePortDressingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 27) {
                        $("#Recertification_485InstructPortDressingPerson").val((result["485InstructPortDressingPerson"] !== null && result["485InstructPortDressingPerson"] !== undefined ? result["485InstructPortDressingPerson"].Answer : " "));
                        $("#Recertification_485InstructPortDressingType").val((result["485InstructPortDressingType"] !== null && result["485InstructPortDressingType"] !== undefined ? result["485InstructPortDressingType"].Answer : " "));
                        $("#Recertification_485InstructPortDressingEvery").val((result["485InstructPortDressingEvery"] !== null && result["485InstructPortDressingEvery"] !== undefined ? result["485InstructPortDressingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 28) {
                        $("#Recertification_485ChangeIVTubingEvery").val((result["485ChangeIVTubingEvery"] !== null && result["485ChangeIVTubingEvery"] !== undefined ? result["485ChangeIVTubingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 29) {
                        $("#Recertification_485InstructInfectionSignsSymptomsPerson").val((result["485InstructInfectionSignsSymptomsPerson"] !== null && result["485InstructInfectionSignsSymptomsPerson"] !== undefined ? result["485InstructInfectionSignsSymptomsPerson"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485MedicationInterventionTemplates").val((result["485MedicationInterventionTemplates"] !== null && result["485MedicationInterventionTemplates"] !== undefined ? result["485MedicationInterventionTemplates"].Answer : " "));
            $("#Recertification_485MedicationInterventionComments").val((result["485MedicationInterventionComments"] !== null && result["485MedicationInterventionComments"] !== undefined ? result["485MedicationInterventionComments"].Answer : " "));


            var medicationGoals = result["485MedicationGoals"];
            if (medicationGoals !== null && medicationGoals != undefined && medicationGoals.Answer != null) {
                var medicationGoalsArray = (medicationGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicationGoalsArray.length; i++) {
                    $('input[name=Recertification_485MedicationGoals][value=' + medicationGoalsArray[i] + ']').attr('checked', true);
                    if (medicationGoalsArray[i] == 2) {
                        $("#Recertification_485MedManagementIndependentPerson").val((result["485MedManagementIndependentPerson"] !== null && result["485MedManagementIndependentPerson"] !== undefined ? result["485MedManagementIndependentPerson"].Answer : " "));
                        $("#Recertification_485MedManagementIndependentDate").val((result["485MedManagementIndependentDate"] !== null && result["485MedManagementIndependentDate"] !== undefined ? result["485MedManagementIndependentDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 3) {
                        $("#Recertification_485VerbalizeMedRegimenUnderstandingPerson").val((result["485VerbalizeMedRegimenUnderstandingPerson"] !== null && result["485VerbalizeMedRegimenUnderstandingPerson"] !== undefined ? result["485VerbalizeMedRegimenUnderstandingPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeMedRegimenUnderstandingDate").val((result["485VerbalizeMedRegimenUnderstandingDate"] !== null && result["485VerbalizeMedRegimenUnderstandingDate"] !== undefined ? result["485VerbalizeMedRegimenUnderstandingDate"].Answer : " "));

                    }

                    if (medicationGoalsArray[i] == 4) {
                        $("#Recertification_485MedAdminIndependentPerson").val((result["485MedAdminIndependentPerson"] !== null && result["485MedAdminIndependentPerson"] !== undefined ? result["485MedAdminIndependentPerson"].Answer : " "));
                        $("#Recertification_485MedAdminIndependentWith").val((result["485MedAdminIndependentWith"] !== null && result["485MedAdminIndependentWith"] !== undefined ? result["485MedAdminIndependentWith"].Answer : " "));
                        $("#Recertification_485MedAdminIndependentDate").val((result["485MedAdminIndependentDate"] !== null && result["485MedAdminIndependentDate"] !== undefined ? result["485MedAdminIndependentDate"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 5) {
                        $("#Recertification_485MedSetupIndependentPerson").val((result["485MedSetupIndependentPerson"] !== null && result["485MedSetupIndependentPerson"] !== undefined ? result["485MedSetupIndependentPerson"].Answer : " "));
                        $("#Recertification_485MedSetupIndependentDate").val((result["485MedSetupIndependentDate"] !== null && result["485MedSetupIndependentDate"] !== undefined ? result["485MedSetupIndependentDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 6) {
                        $("#Recertification_485VerbalizeEachMedIndicationPerson").val((result["485VerbalizeEachMedIndicationPerson"] !== null && result["485VerbalizeEachMedIndicationPerson"] !== undefined ? result["485VerbalizeEachMedIndicationPerson"].Answer : " "));
                        $("#Recertification_485VerbalizeEachMedIndicationDate").val((result["485VerbalizeEachMedIndicationDate"] !== null && result["485VerbalizeEachMedIndicationDate"] !== undefined ? result["485VerbalizeEachMedIndicationDate"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 7) {
                        $("#Recertification_485CorrectDoseIdentifyPerson").val((result["485CorrectDoseIdentifyPerson"] !== null && result["485CorrectDoseIdentifyPerson"] !== undefined ? result["485CorrectDoseIdentifyPerson"].Answer : " "));
                        $("#Recertification_485CorrectDoseIdentifyDate").val((result["485CorrectDoseIdentifyDate"] !== null && result["485CorrectDoseIdentifyDate"] !== undefined ? result["485CorrectDoseIdentifyDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 9) {
                        $("#Recertification_485DemonstrateCentralLineFlushPerson").val((result["485DemonstrateCentralLineFlushPerson"] !== null && result["485DemonstrateCentralLineFlushPerson"] !== undefined ? result["485DemonstrateCentralLineFlushPerson"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 10) {
                        $("#Recertification_485DemonstratePeripheralIVLineFlushPerson").val((result["485DemonstratePeripheralIVLineFlushPerson"] !== null && result["485DemonstratePeripheralIVLineFlushPerson"] !== undefined ? result["485DemonstratePeripheralIVLineFlushPerson"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 11) {
                        $("#Recertification_485DemonstrateSterileDressingTechniquePerson").val((result["485DemonstrateSterileDressingTechniquePerson"] !== null && result["485DemonstrateSterileDressingTechniquePerson"] !== undefined ? result["485DemonstrateSterileDressingTechniquePerson"].Answer : " "));
                        $("#Recertification_485DemonstrateSterileDressingTechniqueType").val((result["485DemonstrateSterileDressingTechniqueType"] !== null && result["485DemonstrateSterileDressingTechniqueType"] !== undefined ? result["485DemonstrateSterileDressingTechniqueType"].Answer : " "));
                    }

                    if (medicationGoalsArray[i] == 12) {
                        $("#Recertification_485DemonstrateAdministerIVPerson").val((result["485DemonstrateAdministerIVPerson"] !== null && result["485DemonstrateAdministerIVPerson"] !== undefined ? result["485DemonstrateAdministerIVPerson"].Answer : " "));
                        $("#Recertification_485DemonstrateAdministerIVType").val((result["485DemonstrateAdministerIVType"] !== null && result["485DemonstrateAdministerIVType"] !== undefined ? result["485DemonstrateAdministerIVType"].Answer : " "));
                        $("#Recertification_485DemonstrateAdministerIVRate").val((result["485DemonstrateAdministerIVRate"] !== null && result["485DemonstrateAdministerIVRate"] !== undefined ? result["485DemonstrateAdministerIVRate"].Answer : " "));
                        $("#Recertification_485DemonstrateAdministerIVVia").val((result["485DemonstrateAdministerIVVia"] !== null && result["485DemonstrateAdministerIVVia"] !== undefined ? result["485DemonstrateAdministerIVVia"].Answer : " "));
                        $("#Recertification_485DemonstrateAdministerIVEvery").val((result["485DemonstrateAdministerIVEvery"] !== null && result["485DemonstrateAdministerIVEvery"] !== undefined ? result["485DemonstrateAdministerIVEvery"].Answer : " "));
                    }
                }
            }
            $("#Recertification_485MedicationGoalTemplates").val((result["485MedicationGoalTemplates"] !== null && result["485MedicationGoalTemplates"] !== undefined ? result["485MedicationGoalTemplates"].Answer : " "));
            $("#Recertification_485MedicationGoalComments").val((result["485MedicationGoalComments"] !== null && result["485MedicationGoalComments"] !== undefined ? result["485MedicationGoalComments"].Answer : " "));


            $("#Recertification_485SNFrequency").val((result["485SNFrequency"] !== null && result["485SNFrequency"] !== undefined ? result["485SNFrequency"].Answer : " "));
            $("#Recertification_485PTFrequency").val((result["485PTFrequency"] !== null && result["485PTFrequency"] !== undefined ? result["485PTFrequency"].Answer : " "));
            $("#Recertification_485OTFrequency").val((result["485OTFrequency"] !== null && result["485OTFrequency"] !== undefined ? result["485OTFrequency"].Answer : " "));
            $("#Recertification_485STFrequency").val((result["485STFrequency"] !== null && result["485STFrequency"] !== undefined ? result["485STFrequency"].Answer : " "));
            $("#Recertification_485MSWFrequency").val((result["485MSWFrequency"] !== null && result["485MSWFrequency"] !== undefined ? result["485MSWFrequency"].Answer : " "));
            $("#Recertification_485HHAFrequency").val((result["485HHAFrequency"] !== null && result["485HHAFrequency"] !== undefined ? result["485HHAFrequency"].Answer : " "));
            $('input[name=Recertification_485Dietician][value=' + (result["485Dietician"] != null && result["485Dietician"] != undefined ? result["485Dietician"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_485OrdersDisciplineInterventionTemplates").val((result["485OrdersDisciplineInterventionTemplates"] !== null && result["485OrdersDisciplineInterventionTemplates"] !== undefined ? result["485OrdersDisciplineInterventionTemplates"].Answer : " "));
            $("#Recertification_485OrdersDisciplineInterventionComments").val((result["485OrdersDisciplineInterventionComments"] !== null && result["485OrdersDisciplineInterventionComments"] !== undefined ? result["485OrdersDisciplineInterventionComments"].Answer : " "));

            var rehabilitationPotential = result["485RehabilitationPotential"];
            if (rehabilitationPotential !== null && rehabilitationPotential != undefined && rehabilitationPotential.Answer != null) {
                var rehabilitationPotentialArray = (rehabilitationPotential.Answer).split(',');
                var j = 0;
                for (j = 0; j < rehabilitationPotentialArray.length; j++) {
                    $('input[name=Recertification_485RehabilitationPotential][value=' + rehabilitationPotentialArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_485AchieveGoalsTemplates").val((result["485AchieveGoalsTemplates"] !== null && result["485AchieveGoalsTemplates"] !== undefined ? result["485AchieveGoalsTemplates"].Answer : " "));
            $("#Recertification_485AchieveGoalsComments").val((result["485AchieveGoalsComments"] !== null && result["485AchieveGoalsComments"] !== undefined ? result["485AchieveGoalsComments"].Answer : " "));

            var dischargePlans = result["485DischargePlans"];
            if (dischargePlans !== null && dischargePlans != undefined && dischargePlans.Answer != null) {
                var dischargePlansArray = (dischargePlans.Answer).split(',');
                var j = 0;
                for (j = 0; j < dischargePlansArray.length; j++) {
                    $('input[name=Recertification_485DischargePlans][value=' + dischargePlansArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_485DischargePlanTemplates").val((result["485DischargePlanTemplates"] !== null && result["485DischargePlanTemplates"] !== undefined ? result["485DischargePlanTemplates"].Answer : " "));
            $("#Recertification_485DischargePlanComments").val((result["485DischargePlanComments"] !== null && result["485DischargePlanComments"] !== undefined ? result["485DischargePlanComments"].Answer : " "));

            var patientStrengths = result["485PatientStrengths"];
            if (patientStrengths !== null && patientStrengths != undefined && patientStrengths.Answer != null) {
                var patientStrengthsArray = (patientStrengths.Answer).split(',');
                var j = 0;
                for (j = 0; j < patientStrengthsArray.length; j++) {
                    $('input[name=Recertification_485PatientStrengths][value=' + patientStrengthsArray[j] + ']').attr('checked', true);

                }
            }
            $("#Recertification_485PatientStrengthOther").val((result["485PatientStrengthOther"] !== null && result["485PatientStrengthOther"] !== undefined ? result["485PatientStrengthOther"].Answer : " "));

            var conclusions = result["485Conclusions"];
            if (conclusions !== null && conclusions != undefined && conclusions.Answer != null) {
                var conclusionsArray = (conclusions.Answer).split(',');
                var j = 0;
                for (j = 0; j < conclusionsArray.length; j++) {
                    $('input[name=Recertification_485Conclusions][value=' + conclusionsArray[j] + ']').attr('checked', true);
                }
            }
            $("#Recertification_485ConclusionOther").val((result["485ConclusionOther"] !== null && result["485ConclusionOther"] !== undefined ? result["485ConclusionOther"].Answer : " "));

            $("#Recertification_485SkilledInterventionTemplate").val((result["485SkilledInterventionTemplate"] !== null && result["485SkilledInterventionTemplate"] !== undefined ? result["485SkilledInterventionTemplate"].Answer : " "));
            $("#Recertification_485SkilledInterventionComments").val((result["485SkilledInterventionComments"] !== null && result["485SkilledInterventionComments"] !== undefined ? result["485SkilledInterventionComments"].Answer : " "));

            var response = result["485SIResponse"];
            if (response != null && response != undefined) {
                if (response.Answer == 1) {

                    $('input[name=Recertification_485SIResponse][value=1]').attr('checked', true);

                    var sIVerbalizedUnderstandingPT = result["485SIVerbalizedUnderstandingPT"];
                    if (sIVerbalizedUnderstandingPT != null && sIVerbalizedUnderstandingPT != undefined) {
                        if (sIVerbalizedUnderstandingPT.Answer == 1) {

                            $('input[name=Recertification_485SIVerbalizedUnderstandingPT][value=1]').attr('checked', true);
                            $("#Recertification_485SIVerbalizedUnderstandingPTPercent").val((result["485SIVerbalizedUnderstandingPTPercent"] !== null && result["485SIVerbalizedUnderstandingPTPercent"] !== undefined ? result["485SIVerbalizedUnderstandingPTPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=Recertification_485SIVerbalizedUnderstandingPT][value=1]').attr('checked', false);
                        }
                    }

                    var sIVerbalizedUnderstandingCG = result["485SIVerbalizedUnderstandingCG"];
                    if (sIVerbalizedUnderstandingCG != null && sIVerbalizedUnderstandingCG != undefined) {
                        if (sIVerbalizedUnderstandingCG.Answer == 1) {

                            $('input[name=Recertification_485SIVerbalizedUnderstandingCG][value=1]').attr('checked', true);
                            $("#Recertification_485SIVerbalizedUnderstandingCGPercent").val((result["485SIVerbalizedUnderstandingCGPercent"] !== null && result["485SIVerbalizedUnderstandingCGPercent"] !== undefined ? result["485SIVerbalizedUnderstandingCGPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=Recertification_485SIVerbalizedUnderstandingCG][value=1]').attr('checked', false);
                        }
                    }
                    var sIReturnDemonstrationPT = result["485SIReturnDemonstrationPT"];
                    if (sIReturnDemonstrationPT != null && sIReturnDemonstrationPT != undefined) {
                        if (sIReturnDemonstrationPT.Answer == 1) {

                            $('input[name=Recertification_485SIReturnDemonstrationPT][value=1]').attr('checked', true);
                            $("#Recertification_485SIReturnDemonstrationPTPercent").val((result["485SIReturnDemonstrationPTPercent"] !== null && result["485SIReturnDemonstrationPTPercent"] !== undefined ? result["485SIReturnDemonstrationPTPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=Recertification_485SIReturnDemonstrationPT][value=1]').attr('checked', false);
                        }
                    }
                    var sIReturnDemonstrationCG = result["485SIReturnDemonstrationCG"];
                    if (sIReturnDemonstrationCG != null && sIReturnDemonstrationCG != undefined) {
                        if (sIReturnDemonstrationCG.Answer == 1) {

                            $('input[name=Recertification_485SIReturnDemonstrationCG][value=1]').attr('checked', true);
                            $("#Recertification_485SIReturnDemonstrationCGPercent").val((result["485SIReturnDemonstrationCGPercent"] !== null && result["485SIReturnDemonstrationCGPercent"] !== undefined ? result["485SIReturnDemonstrationCGPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=Recertification_485SIReturnDemonstrationCG][value=1]').attr('checked', false);
                        }
                    }
                    var sIFurtherTeachReqPT = result["485SIFurtherTeachReqPT"];
                    if (sIFurtherTeachReqPT != null && sIFurtherTeachReqPT != undefined) {
                        if (sIFurtherTeachReqPT.Answer == 1) {

                            $('input[name=Recertification_485SIFurtherTeachReqPT][value=1]').attr('checked', true);

                        }
                        else {
                            $('input[name=Recertification_485SIFurtherTeachReqPT][value=1]').attr('checked', false);
                        }
                    }
                    var sIFurtherTeachReqCG = result["485SIFurtherTeachReqCG"];
                    if (sIFurtherTeachReqCG != null && sIFurtherTeachReqCG != undefined) {
                        if (sIFurtherTeachReqCG.Answer == 1) {

                            $('input[name=Recertification_485SIFurtherTeachReqCG][value=1]').attr('checked', true);

                        }
                        else {
                            $('input[name=Recertification_485SIFurtherTeachReqCG][value=1]').attr('checked', false);
                        }
                    }
                    $("#Recertification_485SIResponseComments").val((result["485SIResponseComments"] !== null && result["485SIResponseComments"] !== undefined ? result["485SIResponseComments"].Answer : " "));
                }
                else {
                    $('input[name=Recertification_485SIResponse][value=1]').attr('checked', false);
                }
            }
            $("#Recertification_485TeachingToolUsed").val((result["485TeachingToolUsed"] !== null && result["485TeachingToolUsed"] !== undefined ? result["485TeachingToolUsed"].Answer : " "));
            $("#Recertification_485ProgressToGoals").val((result["485ProgressToGoals"] !== null && result["485ProgressToGoals"] !== undefined ? result["485ProgressToGoals"].Answer : " "));
            $("#Recertification_485ConferencedWith").val((result["485ConferencedWith"] !== null && result["485ConferencedWith"] !== undefined ? result["485ConferencedWith"].Answer : " "));
            $("#Recertification_485ConferencedWithName").val((result["485ConferencedWithName"] !== null && result["485ConferencedWithName"] !== undefined ? result["485ConferencedWithName"].Answer : " "));
            $("#Recertification_485SkilledInterventionRegarding").val((result["485SkilledInterventionRegarding"] !== null && result["485SkilledInterventionRegarding"] !== undefined ? result["485SkilledInterventionRegarding"].Answer : " "));
            $("#Recertification_485SkilledInterventionPhysicianContacted").val((result["485SkilledInterventionPhysicianContacted"] !== null && result["485SkilledInterventionPhysicianContacted"] !== undefined ? result["485SkilledInterventionPhysicianContacted"].Answer : " "));
            $("#Recertification_485SkilledInterventionOrderChanges").val((result["485SkilledInterventionOrderChanges"] !== null && result["485SkilledInterventionOrderChanges"] !== undefined ? result["485SkilledInterventionOrderChanges"].Answer : " "));
            $("#Recertification_485SkilledInterventionNextVisit").val((result["485SkilledInterventionNextVisit"] !== null && result["485SkilledInterventionNextVisit"] !== undefined ? result["485SkilledInterventionNextVisit"].Answer : " "));
            $("#Recertification_485SkilledInterventionNextPhysicianVisit").val((result["485SkilledInterventionNextPhysicianVisit"] !== null && result["485SkilledInterventionNextPhysicianVisit"] !== undefined ? result["485SkilledInterventionNextPhysicianVisit"].Answer : " "));
            $("#Recertification_485SkilledInterventionDischargePlanning").val((result["485SkilledInterventionDischargePlanning"] !== null && result["485SkilledInterventionDischargePlanning"] !== undefined ? result["485SkilledInterventionDischargePlanning"].Answer : " "));
            $('input[name=Recertification_485SkilledInterventionWrittenNotice][value=' + (result["485SkilledInterventionWrittenNotice"] != null && result["485SkilledInterventionWrittenNotice"] != undefined ? result["485SkilledInterventionWrittenNotice"].Answer : "") + ']').attr('checked', true);
            $("#Recertification_485SkilledInterventionWrittenNoticeDate").val((result["485SkilledInterventionWrittenNoticeDate"] !== null && result["485SkilledInterventionWrittenNoticeDate"] !== undefined ? result["485SkilledInterventionWrittenNoticeDate"].Answer : " "));

        };
    }

}