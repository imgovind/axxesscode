﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpSensoryStatusForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1200) Vision (with corrective lenses if the patient usually wears them):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1200Vision", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1200Vision", "00", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Normal vision: sees adequately in most situations; can see medication
                labels, newsprint.<br />
                <%=Html.RadioButton("FollowUp_M1200Vision", "01", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? true : false, new { @id = "" })%>
                &nbsp;1 - Partially impaired: cannot see medication labels or newsprint, but can
                see obstacles in path, and the surrounding layout; can count fingers at arm's length.<br />
                <%=Html.RadioButton("FollowUp_M1200Vision", "02", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Severely impaired: cannot locate objects without hearing or touching them or patient
                nonresponsive.
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
