﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedPainForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2300) Emergent Care: Since the last time OASIS data were collected, has the patient
                utilized a hospital emergency department (includes holding/observation)?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M2300EmergentCare", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2300EmergentCare", "00", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No [ Go to M2400 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2300EmergentCare", "01", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes, used hospital emergency department WITHOUT hospital admission<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2300EmergentCare", "02", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Yes, used hospital emergency department WITH hospital admission<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2300EmergentCare", "UK", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
            - Unknown [ Go to M2400 ]
        </div>
    </div>
</div>
<div class="rowOasis" id="transferNotDischarge_M2310">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent
                care (with or without hospitalization)? (Mark all that apply.)
            </div>
        </div>
        <div class="insideCol">
            <div class="margin">
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMed" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMed" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareMed") && data["M2310ReasonForEmergentCareMed"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;1
                - Improper medication administration, medication side effects, toxicity, anaphylaxis<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareFall" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareFall" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareFall") && data["M2310ReasonForEmergentCareFall"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;2
                - Injury caused by fall<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareResInf" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareResInf" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareResInf") && data["M2310ReasonForEmergentCareResInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;3
                - Respiratory infection (e.g., pneumonia, bronchitis)<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOtherResInf" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOtherResInf" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareOtherResInf") && data["M2310ReasonForEmergentCareOtherResInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;4
                - Other respiratory problem<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartFail" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartFail" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareHeartFail") && data["M2310ReasonForEmergentCareHeartFail"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;5
                - Heart failure (e.g., fluid overload)<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareCardiac" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareCardiac" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareCardiac") && data["M2310ReasonForEmergentCareCardiac"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;6
                - Cardiac dysrhythmia (irregular heartbeat)<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMyocardial" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMyocardial" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareMyocardial") && data["M2310ReasonForEmergentCareMyocardial"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;7
                - Myocardial infarction or chest pain<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartDisease"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartDisease"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareHeartDisease") && data["M2310ReasonForEmergentCareHeartDisease"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;8
                - Other heart disease<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareStroke" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareStroke" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareStroke") && data["M2310ReasonForEmergentCareStroke"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;9
                - Stroke (CVA) or TIA<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHypo" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHypo" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareHypo") && data["M2310ReasonForEmergentCareHypo"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;10
                - Hypo/Hyperglycemia, diabetes out of control
            </div>
        </div>
        <div class="insideCol">
            <div class="margin">
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareGI" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareGI" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareGI") && data["M2310ReasonForEmergentCareGI"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;11
                - GI bleeding, obstruction, constipation, impaction<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDehMal" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDehMal" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareDehMal") && data["M2310ReasonForEmergentCareDehMal"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;12
                - Dehydration, malnutrition<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUrinaryInf" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUrinaryInf" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareUrinaryInf") && data["M2310ReasonForEmergentCareUrinaryInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;13
                - Urinary tract infection<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareIV" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareIV" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareIV") && data["M2310ReasonForEmergentCareIV"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;14
                - IV catheter-related infection or complication<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareWoundInf" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareWoundInf" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareWoundInf") && data["M2310ReasonForEmergentCareWoundInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;15
                - Wound infection or deterioration<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUncontrolledPain"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUncontrolledPain"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain") && data["M2310ReasonForEmergentCareUncontrolledPain"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;16
                - Uncontrolled pain<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMental" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMental" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareMental") && data["M2310ReasonForEmergentCareMental"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;17
                - Acute mental/behavioral health problem<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDVT" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDVT" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareDVT") && data["M2310ReasonForEmergentCareDVT"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;18
                - Deep vein thrombosis, pulmonary embolus<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOther" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOther" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareOther") && data["M2310ReasonForEmergentCareOther"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;19
                - Other than above reasons<br />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUK" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUK" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2310ReasonForEmergentCareUK") && data["M2310ReasonForEmergentCareUK"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;UK
                - Reason unknown<br />
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
