﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editDeath" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="DischargeFromAgencyDeathTitle" class="float_left">Death at home</span>
            <span class="float_right"><a href="javascript:void(0);" class="window_min"></a><a
                href="javascript:void(0);" class="window_resize"></a><a href="javascript:void(0);"
                    class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form oasisAssWindowContent">
            <div class="oasisAssWindowContainer">
                <div id="editDeathTabs" class=" tabs vertical-tabs vertical-tabs-left OasisContainer">
                    <ul>
                        <li><a href="#editClinicalRecord_death">Clinical Record Items</a></li>
                        <li><a href="#editDeathAdd_death">Death</a></li>
                    </ul>
                    <div style="width: 179px;">
                        <input id="dischargeFromAgencyDeathValidation" type="button" value="Validate" onclick="DeathAtHome.Validate(); JQD.open_window('#validation');" /></div>
                    <div id="editClinicalRecord_death" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDeathAtHomeDemographicsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgencyDeath_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgencyDeath_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgencyDeath_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgencyDeath")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0010) CMS Certification Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0010CertificationNumber" name="DischargeFromAgencyDeath_M0010CertificationNumber"
                                        type="text" class="text" value="" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0014) Branch State:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="DischargeFromAgencyDeath_M0014BranchState"
                                        id="DischargeFromAgencyDeath_M0014BranchState">
                                        <option value=" " selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0016) Branch ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0016BranchId" name="DischargeFromAgencyDeath_M0016BranchId"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0018) National Provider Identifier (NPI)</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0018NationalProviderId" name="DischargeFromAgencyDeath_M0018NationalProviderId"
                                        type="text" class="text" /><br />
                                    <input type="hidden" value=" " name="DischargeFromAgencyDeath_M0018NationalProviderIdUnknown" />
                                    <input type="checkbox" id="DischargeFromAgencyDeath_M0018NationalProviderIdUnknown"
                                        name="DischargeFromAgencyDeath_M0018NationalProviderIdUnknown" value="1" />&nbsp;UK
                                    – Unknown or Not Available</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0020) Patient ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0020PatientIdNumber" name="DischargeFromAgencyDeath_M0020PatientIdNumber"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0030) Start of Care Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0030SocDate" name="DischargeFromAgencyDeath_M0030SocDate"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        Episode Start Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_GenericEpisodeStartDate" name="DischargeFromAgencyDeath_GenericEpisodeStartDate"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0032) Resumption of Care Date:
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="DischargeFromAgencyDeath_M0032ROCDate" name="DischargeFromAgencyDeath_M0032ROCDate"
                                        type="text" class="text" />
                                    <br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0032ROCDateNotApplicable" value="" />
                                    <input id="DischargeFromAgencyDeath_M0032ROCDateNotApplicable" name="DischargeFromAgencyDeath_M0032ROCDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA - Not Applicable
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0040) Patient Name:</div>
                                </div>
                                <div class="right marginOasis">
                                    <div class="padding">
                                        Suffix :
                                        <input id="DischargeFromAgencyDeath_M0040Suffix" name="DischargeFromAgencyDeath_M0040Suffix"
                                            style="width: 20px;" type="text" class="text" />
                                        &nbsp; First :
                                        <input id="DischargeFromAgencyDeath_M0040FirstName" name="DischargeFromAgencyDeath_M0040FirstName"
                                            type="text" class="text" />
                                        <br />
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MI :
                                        <input id="DischargeFromAgencyDeath_M0040MI" name="DischargeFromAgencyDeath_M0040MI"
                                            style="width: 20px;" type="text" class="text" />&nbsp; &nbsp;Last:
                                        <input id="DischargeFromAgencyDeath_M0040LastName" name="DischargeFromAgencyDeath_M0040LastName"
                                            type="text" class="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0050) Patient State of Residence:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="DischargeFromAgencyDeath_M0050PatientState"
                                        id="DischargeFromAgencyDeath_M0050PatientState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0060) Patient Zip Code:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0060PatientZipCode" name="DischargeFromAgencyDeath_M0060PatientZipCode"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0063) Medicare Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0063PatientMedicareNumber" name="DischargeFromAgencyDeath_M0063PatientMedicareNumber"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown" name="DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicare</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0064) Social Security Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0064PatientSSN" name="DischargeFromAgencyDeath_M0064PatientSSN"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgencyDeath_M0064PatientSSNUnknown" type="hidden" value=" " />
                                    <input id="DischargeFromAgencyDeath_M0064PatientSSNUnknown" name="DischargeFromAgencyDeath_M0064PatientSSNUnknown"
                                        type="checkbox" value="1" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0065) Medicaid Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0065PatientMedicaidNumber" name="DischargeFromAgencyDeath_M0065PatientMedicaidNumber"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown" name="DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicaid</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0066) Birth Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0066PatientDoB" name="DischargeFromAgencyDeath_M0066PatientDoB"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow  title">
                                    <div class="padding">
                                        (M0069) Gender:</div>
                                </div>
                                <div class="right marginOasis">
                                    <%=Html.Hidden("DischargeFromAgencyDeath_M0069Gender", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0069Gender", "1", new { @id = "" })%>Male
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0069Gender", "2", new { @id = "" })%>Female
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0080) Discipline of Person Completing Assessment:
                                    </div>
                                </div>
                                <div class="padding">
                                    <%=Html.Hidden("DischargeFromAgencyDeath_M0080DisciplinePerson", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0080DisciplinePerson", "01", new { @id = "" })%>&nbsp;1
                                    - RN
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0080DisciplinePerson", "02", new { @id = "" })%>&nbsp;2
                                    - PT
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0080DisciplinePerson", "03", new { @id = "" })%>&nbsp;3
                                    - SLP/ST
                                    <%=Html.RadioButton("DischargeFromAgencyDeath_M0080DisciplinePerson", "04", new { @id = "" })%>&nbsp;4
                                    - OT
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0090) Date Assessment Completed:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgencyDeath_M0090AssessmentCompleted" name="DischargeFromAgencyDeath_M0090AssessmentCompleted"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis assessmentType">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow margin">
                                    <u>Start/Resumption of Care</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="01" />&nbsp;1
                                    – Start of care—further visits planned<br />
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="3" />&nbsp;3
                                    – Resumption of care (after inpatient stay)<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow">
                                    <u>Follow-Up</u></div>
                                <div class="insiderow">
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="04" />&nbsp;4
                                    – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="05" />&nbsp;5
                                    – Other follow-up [ Go to M0110 ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Transfer to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="06" />&nbsp;6
                                    – Transferred to an inpatient facility—patient not discharged from agency [ Go to
                                    M1040]<br />
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="07" />&nbsp;7
                                    – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="08"
                                        checked="checked" />&nbsp;8 – Death at home [ Go to M0903 ]<br />
                                    <input name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" value="09" />&nbsp;9
                                    – Discharge from agency [ Go to M1040 ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0102) Date of Physician-ordered Start of Care (Resumption of Care): If the physician
                                        indicated a specific start of care (resumption of care) date when the patient was
                                        referred for home health services, record the date specified.</div>
                                </div>
                                <div class="padding">
                                    <input id="DischargeFromAgencyDeath_M0102PhysicianOrderedDate" name="DischargeFromAgencyDeath_M0102PhysicianOrderedDate"
                                        type="text" class="text" />
                                    [ Go to M0110, if date entered ]<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable"
                                        value="" />
                                    <input id="DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable" name="DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA –No specific SOC date ordered by physician
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0104) Date of Referral: Indicate the date that the written or verbal referral
                                        for initiation or resumption of care was received by the HHA.
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="DischargeFromAgencyDeath_M0104ReferralDate" name="DischargeFromAgencyDeath_M0104ReferralDate"
                                        type="text" class="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this
                                        assessment will define a case mix group an “early” episode or a “later” episode
                                        in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    </div>
                                </div>
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0110EpisodeTiming" value="" />
                                    <input name="DischargeFromAgencyDeath_M0110EpisodeTiming" type="radio" value="01" />&nbsp;1
                                    - Early
                                    <input name="DischargeFromAgencyDeath_M0110EpisodeTiming" type="radio" value="02" />&nbsp;2
                                    - Later
                                    <input name="DischargeFromAgencyDeath_M0110EpisodeTiming" type="radio" value="UK" />&nbsp;UK
                                    - Unknown
                                    <input name="DischargeFromAgencyDeath_M0110EpisodeTiming" type="radio" value="NA" />&nbsp;NA
                                    - Not Applicable: No Medicare case mix group
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0140) Race/Ethnicity: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceAMorAN" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceAMorAN" type="checkbox" value="1" />&nbsp;1
                                    - American Indian or Alaska Native<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceAsia" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceAsia" type="checkbox" value="1" />&nbsp;2
                                    - Asian<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceBalck" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceBalck" type="checkbox" value="1" />&nbsp;3
                                    - Black or African-American<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceHispanicOrLatino" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceHispanicOrLatino" type="checkbox"
                                        value="1" />&nbsp;4 - Hispanic or Latino<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceNHOrPI" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceNHOrPI" type="checkbox" value="1" />&nbsp;5
                                    - Native Hawaiian or Pacific Islander<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0140RaceWhite" value="" />
                                    <input name="DischargeFromAgencyDeath_M0140RaceWhite" type="checkbox" value="1" />&nbsp;6
                                    - White
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceNone" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceNone" type="checkbox" value="1" />&nbsp;0
                                    - None; no charge for current services<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceMCREFFS" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceMCREFFS" type="checkbox"
                                        value="1" />&nbsp;1 - Medicare (traditional fee-for-service)<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceMCREHMO" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceMCREHMO" type="checkbox"
                                        value="1" />&nbsp;2 - Medicare (HMO/managed care/Advantage plan)<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceMCAIDFFS" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceMCAIDFFS" type="checkbox"
                                        value="1" />&nbsp;3 - Medicaid (traditional fee-for-service)<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceMACIDHMO" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceMACIDHMO" type="checkbox"
                                        value="1" />&nbsp;4 - Medicaid (HMO/managed care)
                                    <br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceWRKCOMP" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceWRKCOMP" type="checkbox"
                                        value="1" />&nbsp;5 - Workers' compensation<br />
                                    <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceTITLPRO" value="" />
                                    <input name="DischargeFromAgencyDeath_M0150PaymentSourceTITLPRO" type="checkbox"
                                        value="1" />&nbsp;6 - Title programs (e.g., Title III, V, or XX)<br />
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceOTHGOVT" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourceOTHGOVT" type="checkbox"
                                    value="1" />&nbsp;7 - Other government (e.g., TriCare, VA, etc.)<br />
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourcePRVINS" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourcePRVINS" type="checkbox" value="1" />&nbsp;8
                                - Private insurance<br />
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourcePRVHMO" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourcePRVHMO" type="checkbox" value="1" />&nbsp;9
                                - Private HMO/managed care<br />
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceSelfPay" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourceSelfPay" type="checkbox"
                                    value="1" />&nbsp;10 - Self-pay<br />
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceOtherSRS" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourceOtherSRS" type="checkbox"
                                    value="1" />&nbsp;11 - Other (specify)&nbsp;&nbsp;&nbsp;<input type="text" name="DischargeFromAgencyDeath_M0150PaymentSourceOther"
                                        id="DischargeFromAgencyDeath_M0150PaymentSourceOther" /><br />
                                <input type="hidden" name="DischargeFromAgencyDeath_M0150PaymentSourceUnknown" value="" />
                                <input name="DischargeFromAgencyDeath_M0150PaymentSourceUnknown" type="checkbox"
                                    value="1" />&nbsp;UK - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="DeathAtHome.FormSubmit($(this));"
                                        class="SaveContinue" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="DeathAtHome.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editDeathAdd_death" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDeathAtHomeDeathForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgencyDeath_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgencyDeath_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgencyDeath_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgencyDeath")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0903) Date of Last (Most Recent) Home Visit:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input id="DischargeFromAgencyDeath_M0903LastHomeVisitDate" name="DischargeFromAgencyDeath_M0903LastHomeVisitDate"
                                        type="text" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                                        or death (at home) of the patient.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input id="DischargeFromAgencyDeath_M0906DischargeDate" name="DischargeFromAgencyDeath_M0906DischargeDate"
                                        type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" class="SaveContinue" onclick="DeathAtHome.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="DeathAtHome.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
            Patient Landing Screen
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/DeathAtHome.js"))
       .OnDocumentReady(() =>
        {%>
DeathAtHome.Init();
<%}); 
%>
