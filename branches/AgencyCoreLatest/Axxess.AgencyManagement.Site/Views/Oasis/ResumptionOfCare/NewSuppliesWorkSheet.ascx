﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>

    <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareSuppliesWorksheetForm" }))%>
    <%  { %>
    <%= Html.Hidden("ResumptionOfCare_Id", "")%>
    <%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
    <%= Html.Hidden("ResumptionOfCare_PatientGuid", "")%>
    <%= Html.Hidden("assessment", "ResumptionOfCare")%>
    <div class="rowOasisButtons">
        <div class="row485">
            <table id="suppliesTable" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="4">
                            Current Supplies
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Supplies Description
                        </th>
                        <th>
                            Code
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <input type="hidden" name="ResumptionOfCare_GenericSupply" id="ResumptionOfCare_GenericSupply"
                value="" class="SupplyValue" />
            <input value="Add Row" onclick="Oasis.addTableRow('#suppliesTable');" type="button" />
        </div>
        <ul>
            <li style="float: left">
                <input type="button" value="Save/Continue" class="SaveContinue" onclick="Oasis.supplyInputFix('ResumptionOfCare','#suppliesTable'); ROC.FormSubmit($(this));" /></li>
            <li style="float: left">
                <input type="button" value="Save/Exit" onclick="Oasis.supplyInputFix('ResumptionOfCare','#suppliesTable'); ROC.FormSubmit($(this));" /></li>
        </ul>
    </div>
    <%} %>
