﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareIntegumentaryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<div class="row485">
    <table id="socBradenScale" cellspacing="0" cellpadding="0" style="line-height: 12px">
        <tr>
            <th colspan="7">
                Braden Scale<br />
                <i>for Predicting Pressure Sore Risk in Home Care</i>
            </th>
        </tr>
        <tr>
            <td width="19%">
                <strong>SENSORY PERCEPTION</strong>
                <br />
                ability to respond meaningfully to pressure-related discomfort
            </td>
            <td width="19%">
                <strong>1. Completely Limited</strong> Unresponsive (does not moan, flinch, or grasp)
                to painful stimuli, due to diminished level of consciousness or sedation <strong>OR</strong>
                limited ability to feel pain over most of body.
            </td>
            <td width="19%">
                <strong>2. Very Limited</strong> Responds only to painful stimuli. Cannot communicate
                discomfort except by moaning or restlessness <strong>OR</strong> has a sensory impairment
                which limits the ability to feel pain or discomfort over 1/2 of body.
            </td>
            <td width="19%">
                <strong>3. Slightly Limited</strong> Responds to verbal commands, but cannot always
                communicate discomfort or the need to be turned <strong>OR</strong> has some sensory
                impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.
            </td>
            <td width="19%">
                <strong>4. No Impairment</strong> Responds to verbal commands. Has no sensory deficit
                which would limit ability to feel or voice pain or discomfort.
            </td>
            <td width="15px">
                <%var bradenScaleSensory = new SelectList(new[]
               { 
                   new SelectListItem { Text = "4", Value = "4" },
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer != "" ? data["485BradenScaleSensory"].Answer : "4");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleSensory", bradenScaleSensory, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>MOISTURE</strong>
                <br />
                degree to which skin is exposed to moisture
            </td>
            <td>
                <strong>1. Constantly Moist</strong> Skin is kept moist almost constantly by perspiration,
                urine, etc. Dampness is detected every time patient is moved or turned.
            </td>
            <td>
                <strong>2. Often Moist</strong> Skin is often, but not always moist. Linen must
                be changed as often as 3 times in 24 hours.
            </td>
            <td>
                <strong>3. Occasionally Moist</strong> Skin is occasionally moist, requiring an
                extra linen change approximately once a day.
            </td>
            <td>
                <strong>4. Rarely Moist</strong> Skin is usually dry; Linen only requires changing
                at routine intervals.
            </td>
            <td>
                <%var bradenScaleMoisture = new SelectList(new[]
               { 
                   new SelectListItem { Text = "4", Value = "4" },
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer != "" ? data["485BradenScaleMoisture"].Answer : "4");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleMoisture", bradenScaleMoisture, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>ACTIVITY</strong>
                <br />
                degree of physical activity
            </td>
            <td>
                <strong>1. Bedfast</strong> Confined to bed.
            </td>
            <td>
                <strong>2. Chairfast</strong> Ability to walk severely limited or non-existent.
                Cannot bear own weight and/or must be assisted into chair or wheelchair.
            </td>
            <td>
                <strong>3. Walks Occasionally</strong> Walks occasionally during day, but for very
                short distances, with or without assistance. Spends majority of day in bed or chair.
            </td>
            <td>
                <strong>4. Walks Frequently</strong> Walks outside bedroom twice a day and inside
                room at least once every two hours during waking hours.
            </td>
            <td>
                <%var bradenScaleActivity = new SelectList(new[]
               { 
                   new SelectListItem { Text = "4", Value = "4" },
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer != "" ? data["485BradenScaleActivity"].Answer : "4");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleActivity", bradenScaleActivity, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>MOBILITY</strong>
                <br />
                ability to change and control body position
            </td>
            <td>
                <strong>1. Completely Immobile</strong> Does not make even slight changes in body
                or extremity position without assistance.
            </td>
            <td>
                <strong>2. Very Limited</strong> Makes occasional slight changes in body or extremity
                position but unable to make frequent or significant changes independently.
            </td>
            <td>
                <strong>3. Slightly Limited</strong> Makes frequent though slight changes in body
                or extremity position independently.
            </td>
            <td>
                <strong>4. No Limitation</strong> Makes major and frequent changes in position without
                assistance.
            </td>
            <td>
                <%var bradenScaleMobility = new SelectList(new[]
               { 
                   new SelectListItem { Text = "4", Value = "4" },
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer != "" ? data["485BradenScaleMobility"].Answer : "4");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleMobility", bradenScaleMobility, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>NUTRITION</strong>
                <br />
                usual food intake pattern
            </td>
            <td>
                <strong>1. Very Poor</strong> Never eats a complete meal. Rarely eats more than
                1/3 of any food offered. Eats 2 servings or less of protein (meat or dairy products)
                per day. Takes fluids poorly. Does not take a liquid dietary supplement <strong>OR</strong>
                is NPO and/or maintained on clear liquids or IVs for more than 5 days.
            </td>
            <td>
                <strong>2. Probably Inadequate</strong> Rarely eats a complete meal and generally
                eats only about 1/2 of any food offered. Protein intake includes only 3 servings
                of meat or dairy products per day. Occasionally will take a dietary supplement <strong>
                    OR</strong> receives less than optimum amount of liquid diet or tube feeding.
            </td>
            <td>
                <strong>3. Adequate</strong> Eats over half of most meals. Eats a total of 4 servings
                of protein (meat, dairy products) per day. Occasionally will refuse a meal, but
                will usually take a supplement when offered <strong>OR</strong> is on a tube feeding
                or TPN regimen which probably meets most of nutritional needs.
            </td>
            <td>
                <strong>4. Excellent</strong> Eats most of every meal. Never refuses a meal. Usually
                eats a total of 4 or more servings of meat and dairy products. Occasionally eats
                between meals. Does not require supplementation.
            </td>
            <td>
                <%var bradenScaleNutrition = new SelectList(new[]
               { 
                   new SelectListItem { Text = "4", Value = "4" },
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer != "" ? data["485BradenScaleNutrition"].Answer : "4");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleNutrition", bradenScaleNutrition, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>FRICTION & SHEAR</strong>
            </td>
            <td>
                <strong>1. Problem</strong> Requires moderate to maximum assistance in moving. Complete
                lifting without sliding against sheets is impossible. Frequently slides down in
                bed or chair, requiring frequent repositioning with maximum assistance. Spasticity,
                contractures or agitation leads to almost constant friction.
            </td>
            <td>
                <strong>2. Potential Problem</strong> Moves feebly or requires minimum assistance.
                During a move skin probably slides to some extent against sheets, chair, restraints
                or other devices. Maintains relatively good position in chair or bed most of the
                time but occasionally slides down.
            </td>
            <td>
                <strong>3. No Apparent Problem</strong> Moves in bed and in chair independently
                and has sufficient muscle strength to lift up completely during move. Maintains
                good position in bed or chair.
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <%var bradenScaleFriction = new SelectList(new[]
               { 
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "1", Value = "1" }
                   
               }
                   , "Value", "Text", data.ContainsKey("485BradenScaleFriction") && data["485BradenScaleFriction"].Answer != "" ? data["485BradenScaleFriction"].Answer : "3");%>
                <%= Html.DropDownList("ResumptionOfCare_485BradenScaleFriction", bradenScaleFriction, new { @class = "scale" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <strong>Total:</strong>
            </td>
            <td>
                <div>
                    <% =Html.TextBox("ResumptionOfCare_485BradenScaleTotal", data.ContainsKey("485BradenScaleTotal") ? data["485BradenScaleTotal"].Answer : "", new { @id = "ResumptionOfCare_485BradenScaleTotal", @size = "1",@readonly="readonly"})%>
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                <div style="float: right" id="ResumptionOfCare_485ResultBox">
                    &nbsp;</div>
                &nbsp;<br />
                <br />
                <div class="textCenter">
                    <strong>Braden Scale Scoring:</strong> Risk of developing pressure ulcers: <i><strong>
                        15-18:</strong> At risk; <strong>13-14:</strong> Moderate risk; <strong>10-12:</strong>
                        High risk; <strong>9 or below:</strong> Very high risk</i>
                    <br />
                    Copyright. Barbara Braden and Nancy Bergstrom, 1988. Reprinted with permission.
                    All Rights Reserved.
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Integumentary Status
            </th>
        </tr>
        <tr>
            <td>
                <strong>Skin Turgor:</strong>
            </td>
            <td>
                <%=Html.Hidden("ResumptionOfCare_GenericSkinTurgor", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_GenericSkinTurgor", "Good", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Good" ? true : false, new { @id = "" })%>
                &nbsp; Good
                <%=Html.RadioButton("ResumptionOfCare_GenericSkinTurgor", "Fair", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Fair" ? true : false, new { @id = "" })%>
                &nbsp; Fair
                <%=Html.RadioButton("ResumptionOfCare_GenericSkinTurgor", "Poor", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Poor" ? true : false, new { @id = "" })%>&nbsp;
                Poor
            </td>
        </tr>
        <tr>
            <td>
                <strong>Skin Color:</strong>
            </td>
            <td>
                <%string[] skinColor = data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer != "" ? data["GenericSkinColor"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericSkinColor" value="" />
                <input name="ResumptionOfCare_GenericSkinColor" value="1" type="checkbox" '<% if(  skinColor!=null && skinColor.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                Pink/WNL
                <input name="ResumptionOfCare_GenericSkinColor" value="2" type="checkbox" '<% if(  skinColor!=null && skinColor.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                Pale
                <input name="ResumptionOfCare_GenericSkinColor" value="3" type="checkbox" '<% if(  skinColor!=null && skinColor.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                Jaundice
                <input name="ResumptionOfCare_GenericSkinColor" value="4" type="checkbox" '<% if(  skinColor!=null && skinColor.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                Cyanotic
            </td>
        </tr>
        <tr>
            <td>
                <strong>Skin:</strong>
            </td>
            <td>
                <%string[] skin = data.ContainsKey("GenericSkin") && data["GenericSkin"].Answer != "" ? data["GenericSkin"].Answer.Split(',') : null;  %>
                <input type="hidden" name="ResumptionOfCare_GenericSkin" value=" " />
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="1" type="checkbox" '<% if(  skin!=null && skin.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Dry </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="2" type="checkbox" '<% if(  skin!=null && skin.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Diaphoretic </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="3" type="checkbox" '<% if(  skin!=null && skin.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Warm </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="4" type="checkbox" '<% if(  skin!=null && skin.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cool </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="5" type="checkbox" '<% if(  skin!=null && skin.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Wound </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="6" type="checkbox" '<% if(  skin!=null && skin.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Ulcer </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="7" type="checkbox" '<% if(  skin!=null && skin.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Incision </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="8" type="checkbox" '<% if(  skin!=null && skin.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Rash </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="9" type="checkbox" '<% if(  skin!=null && skin.Contains("9")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Ostomy </li>
                    <li>
                        <input name="ResumptionOfCare_GenericSkin" value="10" type="checkbox" '<% if(  skin!=null && skin.Contains("10")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Instructed on measures to control infections?
            </td>
            <td>
                <%=Html.Hidden("ResumptionOfCare_GenericInstructedControlInfections", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_GenericInstructedControlInfections", "1", data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("ResumptionOfCare_GenericInstructedControlInfections", "0", data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                No
            </td>
        </tr>
        <tr>
            <td>
                <strong>Nails:</strong>
            </td>
            <td>
                <%=Html.Hidden("ResumptionOfCare_GenericNails", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_GenericNails", "Good", data.ContainsKey("GenericNails") && data["GenericNails"].Answer == "Good" ? true : false, new { @id = "" })%>
                &nbsp; Good&nbsp;
                <%=Html.RadioButton("ResumptionOfCare_GenericNails", "Bad", data.ContainsKey("GenericNails") && data["GenericNails"].Answer == "Bad" ? true : false, new { @id = "" })%>
                &nbsp; Bad
            </td>
        </tr>
        <tr>
            <td>
                Is patient using pressure-relieving device(s)?
            </td>
            <td>
                <%=Html.Hidden("ResumptionOfCare_GenericPressureRelievingDevice", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_GenericPressureRelievingDevice", "1", data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("ResumptionOfCare_GenericPressureRelievingDevice", "0", data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "0" ? true : false, new { @id = "" })%>
                <input name="ResumptionOfCare_GenericPressureRelievingDevice" value="0" type="radio" />&nbsp;
                No
                <br />
                Type:
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Comments:<br />
                <%=Html.TextArea("ResumptionOfCare_GenericIntegumentaryStatusComments", data.ContainsKey("GenericIntegumentaryStatusComments") ? data["GenericIntegumentaryStatusComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericIntegumentaryStatusComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1300) Pressure Ulcer Assessment: Was this patient assessed for Risk of Developing
                Pressure Ulcers?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1300PressureUlcerAssessment", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1300PressureUlcerAssessment", "00", data.ContainsKey("M1300PressureUlcerAssessment") && data["M1300PressureUlcerAssessment"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No assessment conducted [ Go to M1306 ]<br />
            <%=Html.RadioButton("ResumptionOfCare_M1300PressureUlcerAssessment", "01", data.ContainsKey("M1300PressureUlcerAssessment") && data["M1300PressureUlcerAssessment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes, based on an evaluation of clinical factors, e.g., mobility, incontinence,
            nutrition, etc., without use of standardized tool<br />
            <%=Html.RadioButton("ResumptionOfCare_M1300PressureUlcerAssessment", "02", data.ContainsKey("M1300PressureUlcerAssessment") && data["M1300PressureUlcerAssessment"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Yes, using a standardized tool, e.g., Braden, Norton, other<br />
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol" id="soc_M1302">
        <div class="insiderow title">
            <div class="padding">
                (M1302) Does this patient have a Risk of Developing Pressure Ulcers?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1302RiskDevelopingPressureUlcers", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1302RiskDevelopingPressureUlcers", "0", data.ContainsKey("M1302RiskDevelopingPressureUlcers") && data["M1302RiskDevelopingPressureUlcers"].Answer == "0" ? true : false, new { @id = "" })%>
            &nbsp;0 - No<br />
            <%=Html.RadioButton("ResumptionOfCare_M1302RiskDevelopingPressureUlcers", "1", data.ContainsKey("M1302RiskDevelopingPressureUlcers") && data["M1302RiskDevelopingPressureUlcers"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II
                or Higher or designated as "unstageable"?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1306UnhealedPressureUlcers", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1306UnhealedPressureUlcers", "0", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1322 ]<br />
            <%=Html.RadioButton("ResumptionOfCare_M1306UnhealedPressureUlcers", "1", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each
                Stage: (Enter “0” if none; excludes Stage I pressure ulcers)
            </div>
        </div>
    </div>
</div>
<div class="row485" id="roc_M1308">
    <table border="0" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    Column 1<br />
                    Complete at SOC/ROC/FU & D/C
                </th>
                <th>
                    Column 2<br />
                    Complete at FU & D/C
                </th>
            </tr>
            <tr>
                <th>
                    Stage description – unhealed pressure ulcers
                </th>
                <th>
                    Number Currently Present
                </th>
                <th>
                    Number of those listed in Column 1 that were present on admission (most recent SOC
                    / ROC)
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer
                    with red pink wound bed, without slough. May also present as an intact or open/ruptured
                    serum-filled blister.
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone,
                    tendon, or muscles are not exposed. Slough may be present but does not obscure the
                    depth of tissue loss. May include undermining and tunneling.
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough
                    or eschar may be present on some parts of the wound bed. Often includes undermining
                    and tunneling.
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageFourUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedStageIVUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or
                    device
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by
                    slough and/or eschar.
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.3 Unstageable: Suspected deep tissue injury in evolution.
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "", new { @id = "ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission" })%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="rowOasis" id="roc_M13010_12_14">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                Directions for M1310, M1312, and M1314: If the patient has one or more unhealed
                (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or
                IV pressure ulcer with the largest surface dimension (length x width) and record
                in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.
            </div>
        </div>
        <br />
        <div class="insiderow">
            (M1310) Pressure Ulcer Length: Longest length "head-to-toe" &nbsp;&nbsp;
            <%=Html.TextBox("ResumptionOfCare_M1310PressureUlcerLength", data.ContainsKey("M1310PressureUlcerLength") ? data["M1310PressureUlcerLength"].Answer : "", new { @id = "ResumptionOfCare_M1310PressureUlcerLength" })%>
        </div>
        <div class="insiderow">
            (M1312) Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular
            to the length &nbsp;&nbsp;
            <%=Html.TextBox("ResumptionOfCare_M1312PressureUlcerWidth", data.ContainsKey("M1312PressureUlcerWidth") ? data["M1312PressureUlcerWidth"].Answer : "", new { @id = "ResumptionOfCare_M1312PressureUlcerWidth" })%>
        </div>
        <div class="insiderow">
            (M1314) Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface
            to the deepest area &nbsp;&nbsp;
            <%=Html.TextBox("ResumptionOfCare_M1314PressureUlcerDepth", data.ContainsKey("M1314PressureUlcerDepth") ? data["M1314PressureUlcerDepth"].Answer : "", new { @id = "ResumptionOfCare_M1314PressureUlcerDepth" })%>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol" id="roc_M1320">
        <div class="insiderow title">
            <div class="padding">
                (M1320) Status of Most Problematic (Observable) Pressure Ulcer:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", "00", data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Newly epithelialized<br />
            <%=Html.RadioButton("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", "01", data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Fully granulating<br />
            <%=Html.RadioButton("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", "02", data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Early/partial granulation<br />
            <%=Html.RadioButton("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", "03", data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Not healing<br />
            <%=Html.RadioButton("ResumptionOfCare_M1320MostProblematicPressureUlcerStatus", "NA", data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No observable pressure ulcer
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable
                redness of a localized area usually over a bony prominence. The area may be painful,
                firm, soft, warmer or cooler as compared to adjacent tissue.
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("ResumptionOfCare_M1322CurrentNumberStageIUlcer", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_M1322CurrentNumberStageIUlcer", "00", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0
                <br />
                <%=Html.RadioButton("ResumptionOfCare_M1322CurrentNumberStageIUlcer", "01", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? true : false, new { @id = "" })%>
                &nbsp;1
                <br />
                <%=Html.RadioButton("ResumptionOfCare_M1322CurrentNumberStageIUlcer", "02", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("ResumptionOfCare_M1322CurrentNumberStageIUlcer", "03", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3<br />
                <%=Html.RadioButton("ResumptionOfCare_M1322CurrentNumberStageIUlcer", "04", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                or more
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1324MostProblematicUnhealedStage", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1324MostProblematicUnhealedStage", "01", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? true : false, new { @id = "" })%>
            - Stage I<br />
            <%=Html.RadioButton("ResumptionOfCare_M1324MostProblematicUnhealedStage", "02", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Stage II<br />
            <%=Html.RadioButton("ResumptionOfCare_M1324MostProblematicUnhealedStage", "03", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Stage III<br />
            <%=Html.RadioButton("ResumptionOfCare_M1324MostProblematicUnhealedStage", "04", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Stage IV<br />
            <%=Html.RadioButton("ResumptionOfCare_M1324MostProblematicUnhealedStage", "NA", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No observable pressure ulcer or unhealed pressure ulcer
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1330) Does this patient have a Stasis Ulcer?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1330StasisUlcer", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1330StasisUlcer", "00", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1340 ]
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1330StasisUlcer", "01", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes, patient has BOTH observable and unobservable stasis ulcers
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1330StasisUlcer", "02", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Yes, patient has observable stasis ulcers ONLY<br />
            <%=Html.RadioButton("ResumptionOfCare_M1330StasisUlcer", "03", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Yes, patient has unobservable stasis ulcers ONLY (known but not observable due
            to non-removable dressing) [ Go to M1340 ]
        </div>
    </div>
</div>
<div class="rowOasis" id="soc_M1332AndM1334">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1332) Current Number of (Observable) Stasis Ulcer(s):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1332CurrentNumberStasisUlcer", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1332CurrentNumberStasisUlcer", "01", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - One<br />
            <%=Html.RadioButton("ResumptionOfCare_M1332CurrentNumberStasisUlcer", "02", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Two<br />
            <%=Html.RadioButton("ResumptionOfCare_M1332CurrentNumberStasisUlcer", "03", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Three<br />
            <%=Html.RadioButton("ResumptionOfCare_M1332CurrentNumberStasisUlcer", "04", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Four or more<br />
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1334) Status of Most Problematic (Observable) Stasis Ulcer:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1334StasisUlcerStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1334StasisUlcerStatus", "00", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Newly epithelialized
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1334StasisUlcerStatus", "01", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Fully granulating
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1334StasisUlcerStatus", "02", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Early/partial granulation<br />
            <%=Html.RadioButton("ResumptionOfCare_M1334StasisUlcerStatus", "03", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Not healing
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1340) Does this patient have a Surgical Wound?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1340SurgicalWound", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1340SurgicalWound", "00", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1350 ]<br />
            <%=Html.RadioButton("ResumptionOfCare_M1340SurgicalWound", "01", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - Yes, patient has at least one (observable) surgical wound<br />
            <%=Html.RadioButton("ResumptionOfCare_M1340SurgicalWound", "02", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? true : false, new { @id = "" })%>
            &nbsp;2 - Surgical wound known but not observable due to non-removable dressing
            [ Go to M1350 ]
        </div>
    </div>
    <div class="insideCol" id="soc_M1342">
        <div class="insiderow title">
            <div class="padding">
                (M1342) Status of Most Problematic (Observable) Surgical Wound:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1342SurgicalWoundStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1342SurgicalWoundStatus", "00", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Newly epithelialized
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1342SurgicalWoundStatus", "01", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Fully granulating
            <br />
            <%=Html.RadioButton("ResumptionOfCare_M1342SurgicalWoundStatus", "02", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Early/partial granulation<br />
            <%=Html.RadioButton("ResumptionOfCare_M1342SurgicalWoundStatus", "03", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Not healing
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy,
                other than those described above that is receiving intervention by the home health
                agency?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1350SkinLesionOpenWound", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1350SkinLesionOpenWound", "0", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? true : false, new { @id = "" })%>
            &nbsp;0 - No &nbsp;&nbsp;
            <%=Html.RadioButton("ResumptionOfCare_M1350SkinLesionOpenWound", "1", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="6">
                Wound Graph
            </th>
        </tr>
        <tr>
            <td colspan="6">
                <div align="center">
                    <img src="/Images/body_outline.jpg" alt="body outline" />
                </div>
            </td>
        </tr>
        <tr>
            <th width="10%">
                &nbsp;
            </th>
            <th>
                Wound One
            </th>
            <th>
                Wound Two
            </th>
            <th>
                Wound Three
            </th>
            <th>
                Wound Four
            </th>
            <th>
                Wound Five
            </th>
        </tr>
        <tr>
            <td>
                Location:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundLocation1", data.ContainsKey("GenericWoundLocation1") ? data["GenericWoundLocation1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundLocation1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundLocation2", data.ContainsKey("GenericWoundLocation2") ? data["GenericWoundLocation2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundLocation2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundLocation3", data.ContainsKey("GenericWoundLocation3") ? data["GenericWoundLocation3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundLocation3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundLocation4", data.ContainsKey("GenericWoundLocation4") ? data["GenericWoundLocation4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundLocation4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundLocation5", data.ContainsKey("GenericWoundLocation5") ? data["GenericWoundLocation5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundLocation5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Onset Date:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOnsetDate1", data.ContainsKey("GenericWoundOnsetDate1") ? data["GenericWoundOnsetDate1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOnsetDate1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOnsetDate2", data.ContainsKey("GenericWoundOnsetDate2") ? data["GenericWoundOnsetDate2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOnsetDate2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOnsetDate3", data.ContainsKey("GenericWoundOnsetDate3") ? data["GenericWoundOnsetDate3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOnsetDate3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOnsetDate4", data.ContainsKey("GenericWoundOnsetDate4") ? data["GenericWoundOnsetDate4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOnsetDate4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOnsetDate5", data.ContainsKey("GenericWoundOnsetDate5") ? data["GenericWoundOnsetDate5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOnsetDate5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Size:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundSize1", data.ContainsKey("GenericWoundSize1") ? data["GenericWoundSize1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundSize1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundSize2", data.ContainsKey("GenericWoundSize2") ? data["GenericWoundSize2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundSize2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundSize3", data.ContainsKey("GenericWoundSize3") ? data["GenericWoundSize3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundSize3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundSize4", data.ContainsKey("GenericWoundSize4") ? data["GenericWoundSize4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundSize4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundSize5", data.ContainsKey("GenericWoundSize5") ? data["GenericWoundSize5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundSize5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Drainage:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundDrainage1", data.ContainsKey("GenericWoundDrainage1") ? data["GenericWoundDrainage1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundDrainage1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundDrainage2", data.ContainsKey("GenericWoundDrainage2") ? data["GenericWoundDrainage2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundDrainage2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundDrainage3", data.ContainsKey("GenericWoundDrainage3") ? data["GenericWoundDrainage3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundDrainage3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundDrainage4", data.ContainsKey("GenericWoundDrainage4") ? data["GenericWoundDrainage4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundDrainage4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundDrainage5", data.ContainsKey("GenericWoundDrainage5") ? data["GenericWoundDrainage5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundDrainage5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Odor:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOdor1", data.ContainsKey("GenericWoundOdor1") ? data["GenericWoundOdor1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOdor1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOdor2", data.ContainsKey("GenericWoundOdor2") ? data["GenericWoundOdor2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOdor2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOdor3", data.ContainsKey("GenericWoundOdor3") ? data["GenericWoundOdor3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOdor3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOdor4", data.ContainsKey("GenericWoundOdor4") ? data["GenericWoundOdor4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOdor4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundOdor5", data.ContainsKey("GenericWoundOdor5") ? data["GenericWoundOdor5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundOdor5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Etiology:
            </td>
            <td>
                <%var genericWoundEtiology1 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Burn", Value = "1" },
                   new SelectListItem { Text = "Infection", Value = "2"} ,
                   new SelectListItem { Text = "Pressure", Value = "3" },
                   new SelectListItem { Text = "Surgical", Value = "4" },
                   new SelectListItem { Text = "Traumatic", Value = "5"}, 
                   new SelectListItem { Text = "Diabetic", Value = "6" },
                   new SelectListItem { Text = "Venous Stasis", Value = "7" },
                   new SelectListItem { Text = "Arterial", Value = "8" }   
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundEtiology1") && data["GenericWoundEtiology1"].Answer != "" ? data["GenericWoundEtiology1"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundEtiology1", genericWoundEtiology1)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundEtiology2 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Burn", Value = "1" },
                   new SelectListItem { Text = "Infection", Value = "2"} ,
                   new SelectListItem { Text = "Pressure", Value = "3" },
                   new SelectListItem { Text = "Surgical", Value = "4" },
                   new SelectListItem { Text = "Traumatic", Value = "5"}, 
                   new SelectListItem { Text = "Diabetic", Value = "6" },
                   new SelectListItem { Text = "Venous Stasis", Value = "7" },
                   new SelectListItem { Text = "Arterial", Value = "8" }  
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundEtiology2") && data["GenericWoundEtiology2"].Answer != "" ? data["GenericWoundEtiology2"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundEtiology2", genericWoundEtiology2)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundEtiology3 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Burn", Value = "1" },
                   new SelectListItem { Text = "Infection", Value = "2"} ,
                   new SelectListItem { Text = "Pressure", Value = "3" },
                   new SelectListItem { Text = "Surgical", Value = "4" },
                   new SelectListItem { Text = "Traumatic", Value = "5"}, 
                   new SelectListItem { Text = "Diabetic", Value = "6" },
                   new SelectListItem { Text = "Venous Stasis", Value = "7" },
                   new SelectListItem { Text = "Arterial", Value = "8" }   
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundEtiology3") && data["GenericWoundEtiology3"].Answer != "" ? data["GenericWoundEtiology3"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundEtiology3", genericWoundEtiology3)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundEtiology4 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Burn", Value = "1" },
                   new SelectListItem { Text = "Infection", Value = "2"} ,
                   new SelectListItem { Text = "Pressure", Value = "3" },
                   new SelectListItem { Text = "Surgical", Value = "4" },
                   new SelectListItem { Text = "Traumatic", Value = "5"}, 
                   new SelectListItem { Text = "Diabetic", Value = "6" },
                   new SelectListItem { Text = "Venous Stasis", Value = "7" },
                   new SelectListItem { Text = "Arterial", Value = "8" }   
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundEtiology4") && data["GenericWoundEtiology4"].Answer != "" ? data["GenericWoundEtiology4"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundEtiology4", genericWoundEtiology4)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundEtiology5 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Burn", Value = "1" },
                   new SelectListItem { Text = "Infection", Value = "2"} ,
                   new SelectListItem { Text = "Pressure", Value = "3" },
                   new SelectListItem { Text = "Surgical", Value = "4" },
                   new SelectListItem { Text = "Traumatic", Value = "5"}, 
                   new SelectListItem { Text = "Diabetic", Value = "6" },
                   new SelectListItem { Text = "Venous Stasis", Value = "7" },
                   new SelectListItem { Text = "Arterial", Value = "8" }   
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundEtiology5") && data["GenericWoundEtiology5"].Answer != "" ? data["GenericWoundEtiology5"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundEtiology5", genericWoundEtiology5)%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Stage:
            </td>
            <td>
                <%var genericWoundStage1 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundStage1") && data["GenericWoundStage1"].Answer != "" ? data["GenericWoundStage1"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundStage1", genericWoundStage1)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundStage2 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundStage2") && data["GenericWoundStage2"].Answer != "" ? data["GenericWoundStage2"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundStage2", genericWoundStage2)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundStage3 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundStage3") && data["GenericWoundStage3"].Answer != "" ? data["GenericWoundStage3"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundStage3", genericWoundStage3)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundStage4 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundStage4") && data["GenericWoundStage4"].Answer != "" ? data["GenericWoundStage4"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundStage4", genericWoundStage4)%>
                &nbsp;
            </td>
            <td>
                <%var genericWoundStage5 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericWoundStage5") && data["GenericWoundStage5"].Answer != "" ? data["GenericWoundStage5"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericWoundStage5", genericWoundStage5)%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Undermining:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundUndermining1", data.ContainsKey("GenericWoundUndermining1") ? data["GenericWoundUndermining1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundUndermining1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundUndermining2", data.ContainsKey("GenericWoundUndermining2") ? data["GenericWoundUndermining2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundUndermining2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundUndermining3", data.ContainsKey("GenericWoundUndermining3") ? data["GenericWoundUndermining3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundUndermining3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundUndermining4", data.ContainsKey("GenericWoundUndermining4") ? data["GenericWoundUndermining4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundUndermining4", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundUndermining5", data.ContainsKey("GenericWoundUndermining5") ? data["GenericWoundUndermining5"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundUndermining5", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Inflammation:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundInflamation1", data.ContainsKey("GenericWoundInflamation1") ? data["GenericWoundInflamation1"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundInflamation1", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundInflamation1", data.ContainsKey("GenericWoundInflamation2") ? data["GenericWoundInflamation2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundInflamation2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundInflamation2", data.ContainsKey("GenericWoundInflamation2") ? data["GenericWoundInflamation2"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundInflamation2", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundInflamation3", data.ContainsKey("GenericWoundInflamation3") ? data["GenericWoundInflamation3"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundInflamation3", @size = "", @maxlength = "" })%>
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericWoundInflamation4", data.ContainsKey("GenericWoundInflamation4") ? data["GenericWoundInflamation4"].Answer : "", new { @id = "ResumptionOfCare_GenericWoundInflamation4", @size = "", @maxlength = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Comments:
            </td>
            <td colspan="5">
                <%=Html.TextArea("ResumptionOfCare_GenericWoundComment", data.ContainsKey("GenericWoundComment") ? data["GenericWoundComment"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericWoundComment", @style = "width: 99%;" })%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <div>
                    <strong>Attachments</strong>
                </div>
                <div>
                    <ul>
                        <li>No files uploaded</li>
                        <li>Upload File:</li>
                        <li>
                            <input id="attachment" accept="" /></li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <input type="button" onclick="addFile('attachment')" value="Upload Attachment" />
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <%string[] integumentaryInterventions = data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer != "" ? data["485IntegumentaryInterventions"].Answer.Split(',') : null; %>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485IntegumentaryInterventions" value=" " />
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="1" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructTurningRepositionPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructTurningRepositionPerson") && data["485InstructTurningRepositionPerson"].Answer != "" ? data["485InstructTurningRepositionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructTurningRepositionPerson", instructTurningRepositionPerson)%>
                on turning/repositioning every 2 hours
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="2" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructFloatHeelsPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructFloatHeelsPerson") && data["485InstructFloatHeelsPerson"].Answer != "" ? data["485InstructFloatHeelsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructFloatHeelsPerson", instructFloatHeelsPerson)%>
                to float heels
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="3" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructReduceFrictionPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructReduceFrictionPerson") && data["485InstructReduceFrictionPerson"].Answer != "" ? data["485InstructReduceFrictionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructReduceFrictionPerson", instructReduceFrictionPerson)%>
                on methods to reduce friction and shear
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="4" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructPadBonyProminencesPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructPadBonyProminencesPerson") && data["485InstructPadBonyProminencesPerson"].Answer != "" ? data["485InstructPadBonyProminencesPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructPadBonyProminencesPerson", instructPadBonyProminencesPerson)%>
                to pad all bony prominences
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="5" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructWoundCarePerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructWoundCarePerson") && data["485InstructWoundCarePerson"].Answer != "" ? data["485InstructWoundCarePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructWoundCarePerson", instructWoundCarePerson)%>
                on wound care as follows:
                <br />
                <%=Html.TextArea("ResumptionOfCare_485InstructWoundCarePersonFrequency", data.ContainsKey("485InstructWoundCarePersonFrequency") ? data["485InstructWoundCarePersonFrequency"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485InstructWoundCarePersonFrequency", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="6" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Other:
                <br />
                <%=Html.TextArea("ResumptionOfCare_485InstructWoundOtherDetails", data.ContainsKey("485InstructWoundOtherDetails") ? data["485InstructWoundOtherDetails"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485InstructWoundOtherDetails", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="7" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess skin for breakdown every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="8" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("8")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess/evaluate wound(s) at each dressing change and PRN for signs/symptoms
                of infection. Report to physician increased temp >100.5, chills, increase in drainage,
                foul odor, redness, unrelieved pain >
                <%=Html.TextBox("ResumptionOfCare_485AssessWoundDressingChangeScale", data.ContainsKey("485AssessWoundDressingChangeScale") ? data["485AssessWoundDressingChangeScale"].Answer : "", new { @id = "ResumptionOfCare_485AssessWoundDressingChangeScale", @size = "3", @maxlength = "3" })%>
                on 0-10 scale, and any other significant changes
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="9" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("9")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSignsWoundInfectionPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructSignsWoundInfectionPerson") && data["485InstructSignsWoundInfectionPerson"].Answer != "" ? data["485InstructSignsWoundInfectionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructSignsWoundInfectionPerson", instructSignsWoundInfectionPerson)%>
                on signs/symptoms of wound infection to report to physician, to include increased
                temp >100.5, chills, increase in drainage, foul odor, redness, unrelieved pain >
                <%=Html.TextBox("ResumptionOfCare_485InstructSignsWoundInfectionScale", data.ContainsKey("485InstructSignsWoundInfectionScale") ? data["485InstructSignsWoundInfectionScale"].Answer : "", new { @id = "ResumptionOfCare_485InstructSignsWoundInfectionScale", @size = "3", @maxlength = "3" })%>
                on 0-10 scale, and any other significant changes
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryInterventions" value="10" type="checkbox" '<% if(  integumentaryInterventions!=null && integumentaryInterventions.Contains("10")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                May discontinue wound care when wound(s) have healed
            </td>
        </tr>
        <tr>
            <%var integumentaryOrderTemplates = new SelectList(new[] { new SelectListItem { Text = " ", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485IntegumentaryOrderTemplates") && data["485IntegumentaryOrderTemplates"].Answer != "" ? data["485IntegumentaryOrderTemplates"].Answer : "0");%>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%= Html.DropDownList("ResumptionOfCare_485IntegumentaryOrderTemplates", integumentaryOrderTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485IntegumentaryInterventionComments", data.ContainsKey("485IntegumentaryInterventionComments") ? data["485IntegumentaryInterventionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485IntegumentaryInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <%string[] integumentaryGoals = data.ContainsKey("485IntegumentaryGoals") && data["485IntegumentaryGoals"].Answer != "" ? data["485IntegumentaryGoals"].Answer.Split(',') : null; %>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485IntegumentaryGoals" value=" " />
                <input name="ResumptionOfCare_485IntegumentaryGoals" value="1" type="checkbox" '<% if(  integumentaryGoals!=null && integumentaryGoals.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Wound(s) will heal without complication by:
                <%=Html.TextBox("ResumptionOfCare_485HealWithoutComplicationDate", data.ContainsKey("485HealWithoutComplicationDate") ? data["485HealWithoutComplicationDate"].Answer : "", new { @id = "ResumptionOfCare_485HealWithoutComplicationDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryGoals" value="2" type="checkbox" '<% if(  integumentaryGoals!=null && integumentaryGoals.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Wound(s) will be free from signs and symptoms of infection during 60 day episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryGoals" value="3" type="checkbox" '<% if(  integumentaryGoals!=null && integumentaryGoals.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Wound(s) will decrease in size by
                <%=Html.TextBox("ResumptionOfCare_485WoundSizeDecreasePercent", data.ContainsKey("485WoundSizeDecreasePercent") ? data["485WoundSizeDecreasePercent"].Answer : "", new { @id = "ResumptionOfCare_485WoundSizeDecreasePercent", @size = "4", @maxlength = "4" })%>
                % by
                <%=Html.TextBox("ResumptionOfCare_485WoundSizeDecreasePercentDate", data.ContainsKey("485WoundSizeDecreasePercentDate") ? data["485WoundSizeDecreasePercentDate"].Answer : "", new { @id = "ResumptionOfCare_485WoundSizeDecreasePercentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485IntegumentaryGoals" value="4" type="checkbox" '<% if(  integumentaryGoals!=null && integumentaryGoals.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient skin integrity will remain intact during this episode
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var integumentaryGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485IntegumentaryGoalTemplates") && data["485IntegumentaryGoalTemplates"].Answer != "" ? data["485IntegumentaryGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485IntegumentaryGoalTemplates", integumentaryGoalTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485IntegumentaryGoalComments", data.ContainsKey("485IntegumentaryGoalComments") ? data["485IntegumentaryGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485IntegumentaryGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="ROC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="ROC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
