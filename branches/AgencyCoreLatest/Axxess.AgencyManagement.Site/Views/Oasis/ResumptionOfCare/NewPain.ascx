﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCarePainForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Pain Scale
            </th>
        </tr>
        <tr>
            <td width="25%">
                Onset Date:
                <%=Html.TextBox("ResumptionOfCare_GenericPainOnSetDate", data.ContainsKey("GenericPainOnSetDate") ? data["GenericPainOnSetDate"].Answer : "", new { @id = "ResumptionOfCare_GenericPainOnSetDate", @size = "80", @maxlength = "80" })%>
            </td>
            <td>
                Location of Pain:
                <%=Html.TextBox("ResumptionOfCare_GenericLocationOfPain", data.ContainsKey("GenericLocationOfPain") ? data["GenericLocationOfPain"].Answer : "", new { @id = "ResumptionOfCare_GenericLocationOfPain", @size = "80", @maxlength = "80" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2" style='background-color: #FFFFFF;'>
                <div align="center">
                    <img src="/Images/painscale.png" border="30" />
                </div>
                <div style="font-size: 10px; font-style: italic" align="center">
                    From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532"
                        target="_blank">Wong's essentials of pediatric nursing</a>, ed. 8, St. Louis,
                    2009, Mosby. Used with permission. Copyright Mosby.
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Intensity of pain:
            </td>
            <td>
                <%var genericIntensityOfPain = new SelectList(new[]
               { 
                   new SelectListItem { Text = "0", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } ,
                   new SelectListItem { Text = "5", Value = "5" },
                   new SelectListItem { Text = "6", Value = "6" },
                   new SelectListItem { Text = "7", Value = "7"} ,
                   new SelectListItem { Text = "8", Value = "8" },
                   new SelectListItem { Text = "9", Value = "9" } ,
                   new SelectListItem { Text = "10", Value = "10" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer != "" ? data["GenericIntensityOfPain"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericIntensityOfPain", genericIntensityOfPain)%>
            </td>
        </tr>
        <tr>
            <td>
                Duration:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericDurationOfPain", data.ContainsKey("GenericDurationOfPain") ? data["GenericDurationOfPain"].Answer : "", new { @id = "ResumptionOfCare_GenericDurationOfPain", @size = "80", @maxlength = "80" })%>
            </td>
        </tr>
        <tr>
            <td>
                Quality:
            </td>
            <td>
                <%=Html.TextBox("ResumptionOfCare_GenericQualityOfPain", data.ContainsKey("GenericQualityOfPain") ? data["GenericQualityOfPain"].Answer : "", new { @id = "ResumptionOfCare_GenericQualityOfPain", @size = "80", @maxlength = "80" })%>
            </td>
        </tr>
        <tr>
            <td>
                What makes pain worse:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericWhatMakesPainWorse", data.ContainsKey("GenericWhatMakesPainWorse") ? data["GenericWhatMakesPainWorse"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericWhatMakesPainWorse", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                What makes pain better:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericWhatMakesPainBetter", data.ContainsKey("GenericWhatMakesPainBetter") ? data["GenericWhatMakesPainBetter"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericWhatMakesPainBetter", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                Relief rating of pain, i.e., pain level after medications:
            </td>
            <td>
                <%var genericReliefRatingOfPain = new SelectList(new[]
               { 
                   new SelectListItem { Text = "0", Value = "0" },
                   new SelectListItem { Text = "1", Value = "1" },
                   new SelectListItem { Text = "2", Value = "2"} ,
                   new SelectListItem { Text = "3", Value = "3" },
                   new SelectListItem { Text = "4", Value = "4" } ,
                   new SelectListItem { Text = "5", Value = "5" },
                   new SelectListItem { Text = "6", Value = "6" },
                   new SelectListItem { Text = "7", Value = "7"} ,
                   new SelectListItem { Text = "8", Value = "8" },
                   new SelectListItem { Text = "9", Value = "9" } ,
                   new SelectListItem { Text = "10", Value = "10" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericReliefRatingOfPain") && data["GenericReliefRatingOfPain"].Answer != "" ? data["GenericReliefRatingOfPain"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_GenericReliefRatingOfPain", genericReliefRatingOfPain)%>
            </td>
        </tr>
        <tr>
            <td>
                Medications patient takes for pain:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericPainMedication", data.ContainsKey("GenericPainMedication") ? data["GenericPainMedication"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericPainMedication", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;Medication effectiveness:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericMedicationEffectiveness", data.ContainsKey("GenericMedicationEffectiveness") ? data["GenericMedicationEffectiveness"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericMedicationEffectiveness", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;Medication adverse side effects:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericMedicationAdverseEffects", data.ContainsKey("GenericMedicationAdverseEffects") ? data["GenericMedicationAdverseEffects"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericMedicationAdverseEffects", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                Patient's pain goal:
            </td>
            <td>
                <%=Html.TextArea("ResumptionOfCare_GenericPatientPainGoal", data.ContainsKey("GenericPatientPainGoal") ? data["GenericPatientPainGoal"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_GenericPatientPainGoal", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1240) Has this patient had a formal Pain Assessment using a standardized pain
                assessment tool (appropriate to the patient’s ability to communicate the severity
                of pain)?
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("ResumptionOfCare_M1240FormalPainAssessment", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_M1240FormalPainAssessment", "00", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - No standardized assessment conducted<br />
                <%=Html.RadioButton("ResumptionOfCare_M1240FormalPainAssessment", "01", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Yes, and it does not indicate severe pain<br />
                <%=Html.RadioButton("ResumptionOfCare_M1240FormalPainAssessment", "02", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Yes, and it indicates severe pain<br />
            </div>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1242) Frequency of Pain Interfering with patient's activity or movement:<br />
                &nbsp;
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1242PainInterferingFrequency", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1242PainInterferingFrequency", "00", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient has no pain<br />
            <%=Html.RadioButton("ResumptionOfCare_M1242PainInterferingFrequency", "01", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient has pain that does not interfere with activity or movement<br />
            <%=Html.RadioButton("ResumptionOfCare_M1242PainInterferingFrequency", "02", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Less often than daily<br />
            <%=Html.RadioButton("ResumptionOfCare_M1242PainInterferingFrequency", "03", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Daily, but not constantly<br />
            <%=Html.RadioButton("ResumptionOfCare_M1242PainInterferingFrequency", "04", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - All of the time
        </div>
    </div>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] painInterventions = data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer != "" ? data["485PainInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485PainInterventions" value=" " />
                <input name="ResumptionOfCare_485PainInterventions" value="1" type="checkbox" '<% if(  painInterventions!=null && painInterventions.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess pain level and effectiveness of pain medications and current pain management
                therapy every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485PainInterventions" value="2" type="checkbox" '<% if(  painInterventions!=null && painInterventions.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to take pain medication before pain becomes severe to achieve
                better pain control
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485PainInterventions" value="3" type="checkbox" '<% if(  painInterventions!=null && painInterventions.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on nonpharmacologic pain relief measures, including relaxation
                techniques, massage, stretching, positioning, and hot/cold packs
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485PainInterventions" value="4" type="checkbox" '<% if(  painInterventions!=null && painInterventions.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient's willingness to take pain medications and/or barriers to compliance,
                e.g., patient is unable to tolerate side effects such as drowsiness, dizziness,
                constipation
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485PainInterventions" value="5" type="checkbox" '<% if(  painInterventions!=null && painInterventions.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to report to physician if patient experiences pain level not acceptable to patient,
                pain level greater than
                <%=Html.TextBox("ResumptionOfCare_485PainTooGreatLevel", data.ContainsKey("485PainTooGreatLevel") ? data["485PainTooGreatLevel"].Answer : "", new { @id = "ResumptionOfCare_485PainTooGreatLevel", @size = "10", @maxlength = "10" })%>
                , pain medications not effective, patient unable to tolerate pain medications, pain
                affecting ability to perform patient's normal activities
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var painOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485PainOrderTemplates") && data["485PainOrderTemplates"].Answer != "" ? data["485PainOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485PainOrderTemplates", painOrderTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485PainInterventionComments", data.ContainsKey("485PainInterventionComments") ? data["485PainInterventionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485PainInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] painGoals = data.ContainsKey("485PainGoals") && data["485PainGoals"].Answer != "" ? data["485PainGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485PainGoals" value=" " />
                <input name="ResumptionOfCare_485PainGoals" value="1" type="checkbox" '<% if(  painGoals!=null && painGoals.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will verbalize understanding of proper use of pain medication by
                <%=Html.TextBox("ResumptionOfCare_485PatientVerbalizeMedUseDate", data.ContainsKey("485PatientVerbalizeMedUseDate") ? data["485PatientVerbalizeMedUseDate"].Answer : "", new { @id = "ResumptionOfCare_485PatientVerbalizeMedUseDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485PainGoals" value="2" type="checkbox" '<% if(  painGoals!=null && painGoals.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will achieve pain level less than
                <%=Html.TextBox("ResumptionOfCare_485AchievePainLevelLessThan", data.ContainsKey("485AchievePainLevelLessThan") ? data["485AchievePainLevelLessThan"].Answer : "", new { @id = "ResumptionOfCare_485AchievePainLevelLessThan", @size = "10", @maxlength = "10" })%>
                within
                <%=Html.TextBox("ResumptionOfCare_485AchievePainLevelWeeks", data.ContainsKey("485AchievePainLevelWeeks") ? data["485AchievePainLevelWeeks"].Answer : "", new { @id = "ResumptionOfCare_485AchievePainLevelWeeks", @size = "3", @maxlength = "3" })%>
                weeks
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var painGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485PainGoalTemplates") && data["485PainGoalTemplates"].Answer != "" ? data["485PainGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485PainGoalTemplates", painGoalTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485PainGoalComments", data.ContainsKey("485PainGoalComments") ? data["485PainGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485PainGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="ROC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="ROC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
