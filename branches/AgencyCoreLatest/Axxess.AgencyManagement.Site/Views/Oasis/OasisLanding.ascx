﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="Oasis_window" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Oasis</span><span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content ">
            <div class="temp-container">
                <div id='Oasis_Spliter1' class="xSplitter ">
                    <div class="xsp-pane sidepanel">
                        <div id="OasisLeftSide" class="side">
                            <div class="oasisMenu">
                            </div>
                            <div id="OasisFilterContainer" class="sideTop">
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            View:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="oasisPatientStatusDropDown sideDropDown">
                                                <option value="0">All Patients</option>
                                                <option value="1">Active Patients</option>
                                                <option value="2">Discharged Patients</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Filter:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="oasisPatientPaymentDropDown sideDropDown">
                                                <option value="0">All</option>
                                                <option value="1">Medicare (traditional)</option>
                                                <option value="2">Medicare (HMO/managed care)</option>
                                                <option value="3">Medicaid (traditional)</option>
                                                <option value="4">Medicaid (HMO/managed care) </option>
                                                <option value="5">Workers' compensation</option>
                                                <option value="6">Title programs </option>
                                                <option value="7">Other government</option>
                                                <option value="8">Private</option>
                                                <option value="9">Private HMO/managed care</option>
                                                <option value="10">Self Pay</option>
                                                <option value="11">Unknown</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Find:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper">
                                            <input class="text" id="txtSearch_Oasis_Patient_Selection" name="" value="" type="text"
                                                size="18" style="width: 184px;" /></span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                            </div>
                            <div id="OasisSelectionGridContainer" class="sideBottom">
                                <%Html.Telerik().Grid<PatientSelectionViewData>()
                                                .Name("OasisPatientSelectionGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(p => p.LastName);
                                                    columns.Bound(p => p.FirstName);
                                                    columns.Bound(p => p.PatientIdNumber).Title("Patient #");
                                                    columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { statusId = 0, paymentSourceId = 0, name = string.Empty }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false)
                                                .ClientEvents(events => events.OnLoad(
                                                    () =>
                                                    {%>
                                function () { $('#OasisPatientSelectionGrid .t-grid-content tbody tr:has(td):first').click()
                                }
                                <% }
                                                    ).OnRowSelected("Oasis.OnPatientRowSelected")
                                                    .OnLoad("Oasis.CalculateGridHeight()")).Render();                                                
                                                                                                
                                %>
                            </div>
                        </div>
                    </div>
                    <div class="abs window_main xsp-pane">
                        <div id='Oasis_Spliter12' class='abs xSplitter'>
                            <div class='abs xsp-pane mainTop'>
                                <div class="mainTopContainer">
                                    <div class="oasisMenu">
                                        <ul>
                                            <li>New Orders |</li>
                                            <li>Progress Notes |</li>
                                            <li>Communication Notes |</li>
                                            <li><a href="javascript:void(0);" onclick="Oasis.Generate485('35F2152D-FB86-4AD3-AF25-49B965CF85FE', 'StartOfCare');JQD.open_window('#edit485');">Create 485</a></li>
                                        </ul>
                                    </div>
                                    <div class="mainTopContainerLeft">
                                        <div style="padding: 5px;">
                                            <div>
                                                <label>
                                                    <b style="color: #53868B;">Patient Information&nbsp;&nbsp;&nbsp;</b></label></div>
                                            <div class="underLine">
                                            </div>
                                        </div>
                                        <div class="mainTopContent">
                                            <div class="mainTopContentLeft">
                                                <div class="row">
                                                    <div class="fixedRow">
                                                        <label>
                                                            Patient ID #:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Oasis_PatientID" class="input_wrapper blank">345632&nbsp;</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Patient Name:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Oasis_PatientName" class="input_wrapper blank">Paul Jones&nbsp;</span>
                                                    </div>
                                                    <br />
                                                    <div class="breakLineRow">
                                                    </div>
                                                    <div class="fixedRow">
                                                        <label>
                                                            Gender:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Oasis_PatientGender" class="input_wrapper blank">Male</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Medicare #:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Oasis_PatientMedicareNo" class="input_wrapper blank">1234566&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Medicaid #:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Oasis_PatientMedicaidNo" class="input_wrapper blank">234-45-5767&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="row">
                                                        <div class="fixedRow">
                                                            <label>
                                                                Branch Location:&nbsp;&nbsp;&nbsp;</label></div>
                                                        <div class="inputs">
                                                            <span id="Oasis_BranchLocation" class="input_wrapper blank">Dallas, TX</span>
                                                        </div>
                                                    </div>
                                                    <div class="inputs" style="float: right;">
                                                        <br />
                                                        <span class="input_wrapper blank"><a href="http://maps.google.com/maps?hl=en&tab=wl">
                                                            Map </a>| <a href="javascript:void(0);">Direction</a>&nbsp;</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mainTopContainerRight">
                                            </div>
                                        </div>
                                        <div class="NoteContainer">
                                            <div class="underLine">
                                            </div>
                                            <div class="NoteContainerLeft">
                                                <div class="fixedRow">
                                                    <label>
                                                        Notes&nbsp;&nbsp;&nbsp;</label></div>
                                                <div style="width: 320px; float: left;">
                                                    <textarea id="txt_OasisLanding_Note" cols="" rows="" readonly="readonly" style="border: 0px;
                                                        width: 100%; height: 200px; overflow: hidden;"></textarea>
                                                </div>
                                                <div class="NoteContainerRight">
                                                    <input id="Button1" type="button" onclick="Oasis.Note(); JQD.open_window('#editNote');"
                                                        value="Edit Notes" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainTopContainerRight">
                                        <div style="padding: 5px;">
                                            <b>Oasis Assessment For This Patient</b>
                                            <div class="underLine">
                                            </div>
                                            <ul>
                                                <li><a href="javascript:void(0);" onclick="SOC.NewStartOfCare();JQD.open_window('#soc');">
                                                    Start of Care</a></li>
                                                <li><a href="javascript:void(0);" onclick="ROC.ResumptionOfCare(); JQD.open_window('#roc');">
                                                    Resumption of Care</a></li>
                                                <li><a href="javascript:void(0);" onclick="Discharge.DischargeFromAgency(); JQD.open_window('#editDischarge');">
                                                    Discharge from Agency</a></li>
                                                <li><a href="javascript:void(0);" onclick="Recertification.Recertification(); JQD.open_window('#editRecertification');">
                                                    Recertification</a></li>
                                                <li><a href="javascript:void(0);" onclick="FollowUp.FollowUp(); JQD.open_window('#editFollowUp');">
                                                    Other follow-up</a></li>
                                                <li><a href="javascript:void(0);" onclick="DeathAtHome.DischargeFromAgencyDeath(); JQD.open_window('#editDeath');">
                                                    Death at Home</a></li>
                                                <li><a href="javascript:void(0);" onclick="TransferForDischarge.TransferInPatientDischarged(); JQD.open_window('#editTransferdischarge');">
                                                    Transfer to inpatient facility - Discharged</a></li>
                                                <li><a href="javascript:void(0);" onclick="TransferNotDischarge.TransferInPatientNotDischarged(); JQD.open_window('#editTransfernotdischarge');">
                                                    Transfer to inpatient facility -Not Discharged</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="oasisBottomPanel" class='abs xsp-pane' style="min-width: 800px;">
                                <div class="submenu">
                                    <div style="margin-left: 10px; float: left;">
                                        <label>
                                            Show&nbsp;&nbsp;&nbsp;</label></div>
                                    <div style="float: left; margin-left: 10px; margin-right: 10px;">
                                        <select name="oasisActivityDropDown" class="oasisActivityDropDown">
                                            <option value="All" selected="selected">All</option>
                                            <option value="SOC">SOC</option>
                                            <option value="ROC">ROC</option>
                                            <option value="DischargeFromAgency">Discharge From Agency</option>
                                            <option value="Recertification">Recertification</option>
                                            <option value="OtherFollowUp">Other follow-up</option>
                                            <option value="DeathAtHome">Death at Home</option>
                                            <option value="TransferNotDischarged">Transfer to inpatient facility - Not Discharged</option>
                                            <option value="TransferDischarged">Transfer to inpatient facility - Discharged</option>
                                        </select>
                                    </div>
                                    <div style="float: left;">
                                        <label>
                                            Date&nbsp;&nbsp;&nbsp;</label></div>
                                    <div style="float: left; margin-left: 10px; margin-right: 10px;">
                                        <select name="oasisActivityDateDropdown" class="oasisActivityDateDropDown">
                                            <option value="All" selected="selected">All</option>
                                            <option disabled="disabled">---------------------------------------------</option>
                                            <option value="DateRange">Date Range</option>
                                            <option disabled="disabled">---------------------------------------------</option>
                                            <option value="ThisEpisode">This Episode</option>
                                            <option value="LastEpisode">Last Episode</option>
                                            <option value="NextEpisode">Next Episode</option>
                                            <option disabled="disabled">---------------------------------------------</option>
                                            <option value="Today">Today</option>
                                            <option value="ThisWeek">This Week</option>
                                            <option value="ThisWeekToDate">This Week-to-date</option>
                                            <option value="LastMonth">Last Month</option>
                                            <option value="ThisMonth">This Month</option>
                                            <option value="ThisMonthToDate">This Month-to-date</option>
                                            <option value="ThisFiscalQuarter">This Fiscal Quarter</option>
                                            <option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option>
                                            <option value="ThisFiscalYear">This Fiscal Year</option>
                                            <option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option>
                                            <option value="Yesterday">Yesterday</option>
                                            <option value="LastWeek">Last Week</option>
                                            <option value="LastWeekToDate">Last Week-to-date</option>
                                            <option value="LastMonthToDate">Last Month-to-date</option>
                                            <option value="LastFiscalQuarter">Last Fiscal Quarter</option>
                                            <option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option>
                                            <option value="LastFiscalYear">Last Fiscal Year</option>
                                            <option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option>
                                            <option value="NextWeek">Next Week</option>
                                            <option value="Next4Weeks">Next 4 Weeks</option>
                                            <option value="NextMonth">Next Month</option>
                                            <option value="NextFiscalQuarter">Next Fiscal Quarter</option>
                                            <option value="NextFiscalYear">Next Fiscal Year</option>
                                        </select>
                                    </div>
                                    <div id="oasisDateRangeText" style="display: none;">
                                    </div>
                                    <div class="oasisCustomDateRange" style="display: none;">
                                        <div style="float: left;">
                                            <label>
                                                From&nbsp;&nbsp;&nbsp;</label></div>
                                        <div style="float: left; margin-left: 10px; margin-right: 10px;">
                                            <%= Html.Telerik().DatePicker()
                                          .Name("From")
                                        .Value(DateTime.Now.Subtract(TimeSpan.FromDays(60)))
                                        .HtmlAttributes(new {@id = "oasisActivityFromDate", @class = "date" })
                                            %>
                                        </div>
                                        <div style="float: left;">
                                            <label>
                                                To&nbsp;&nbsp;&nbsp;</label></div>
                                        <div style="float: left;">
                                            <%= Html.Telerik().DatePicker()
                                          .Name("To")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new {@id = "patientActivityToDate", @class = "date" })
                                            %>
                                        </div>
                                        <div style="float: left;">
                                            &nbsp;&nbsp;&nbsp;<input type="button" value="Search" onclick="Patient.CustomDateRange();" />
                                        </div>
                                    </div>
                                </div>
                                <%Html.Telerik().Grid<OasisActivityViewData>()
                                                .Name("OasisActivityGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(p => p.Id)
                                              .ClientTemplate("<input type='checkbox' name='checkedRecords' value='<#= Id #>' />")
                                              .Title("Select")
                                              .Width(50)
                                              .HtmlAttributes(new { style = "text-align:center" });
                                                    columns.Bound(p => p.Type).Title("Task");
                                                    columns.Bound(p => p.Date).Format("{0:MM/dd/yyyy}").Title("Target Date").Width(100);
                                                    columns.Bound(p => p.Employee);
                                                    columns.Bound(p => p.Status);
                                                    columns.Bound(p => p.URL)
                              .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"<#=URL#>\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Oasis.Delete($(this),'<#=Id#>');\" >Delete</a> ")
                              .Title("Action").Width(180);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Oasis", new { patientId = "9c9347fe-be74-41bb-b0e4-de441641361c", discipline = "All", dateRangeId = "All", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now }))
                                                .Pageable()
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false).HtmlAttributes(new { Style = "min-width:100px;" })
                                                .Render();                                                
                                                                                                
                                %>
                            </div>
                            <div class='abs xsp-dragbar xsp-dragbar-hframe'>
                                &nbsp;</div>
                        </div>
                    </div>
                    <div class='abs xsp-dragbar xsp-dragbar-vframe'>
                        &nbsp;</div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
            Oasis Landing Screen
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Oasis.js"))
       .OnDocumentReady(() =>
        {%>
Oasis.Init();
<%}); 
%>