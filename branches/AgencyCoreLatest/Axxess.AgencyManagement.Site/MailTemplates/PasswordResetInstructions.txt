﻿Hi,
<br /><br />
You requested a password change.
<br /><br />
To complete your password change request, go to:<br />
<a href='http://agencycore.axxessweb.com/Login'>http://agencycore.axxessweb.com/Login</a>
<br /><br />
Enter this temporary passcode:
<br />
<%=password%>
<br /><br />
You will be required to change your password after you sign in. Please choose a password you can easily remember.
<br /><br />
Thanks,
<br /><br />
The Axxess&trade; team
<br /><br />
This is an automated e-mail, please do not reply.