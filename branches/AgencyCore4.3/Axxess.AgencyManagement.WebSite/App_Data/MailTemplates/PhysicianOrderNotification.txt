﻿<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css" media="screen">
		p {
			margin: 0 0 10px 0;
			font-family: arial, sans-serif;
			font-size: 12px;
		}

		body, div, td, th, textarea, input, h2, h3 {
			font-family: arial, sans-serif;
			font-size: 11px;
		}
	</style>
	<body style="font-family: arial, sans-serif; font-size: 12px;">
		<p>Hi Dr.<%=recipientlastname%>,</p>
		<p><%=senderfullname%> sent you an order.</p>
		<p>To review and sign this order, please login at:&nbsp;<a href='http://md.axxessweb.com/Login'>http://md.axxessweb.com/Login</a></p>
		<p>Thanks,<br />
		The Axxess&trade; team</p>
		<p>This is an automated e-mail, please do not reply.</p>
		<p>This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>