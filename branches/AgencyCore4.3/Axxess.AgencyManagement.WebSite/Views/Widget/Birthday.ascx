﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
                <th>Birth Day</th>
                <th>Age</th>
                <th>Name</th>
                <th>Home Phone</th>
            </tr>
        </thead>
        <tbody id="birthdayWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div onclick="UserInterface.ShowReportCenter('Report/Patient/Birthdays')" id="birthdayWidgetContentMore" class="widget-more"><a href="javascript:void(0)">More &#187;</a></div>
