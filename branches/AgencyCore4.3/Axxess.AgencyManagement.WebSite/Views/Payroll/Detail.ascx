﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollDetail>" %>
<div class="payroll">
    <ul>
        <li class="align-center">
            <span id="payrollUserId" class="very-hidden"><%= Model.Id %></span>
            <%= Model.Name.IsNotNullOrEmpty() ? Model.Name : "" %>
            [ <a href="javascript:void(0);" onclick="$('#payrollSearchResultDetails').hide(); $('#payrollSearchResultDetail').hide(); $('#payrollMarkAsPaidButton').hide();$('#payrollSearchResult').show();">Back to Search Results</a> ]
            [ <%= Html.ActionLink("Export to Excel", "PayrollDetail", "Export", new { UserId = Model.Id, StartDate = Model.StartDate, EndDate = Model.EndDate, payrollStatus= Model.PayrollStatus }, new { id = "ExportPayrollDetail_ExportLink" })%> ]
            [ <a href="javascript:void(0);" onclick="U.GetAttachment('Payroll/SummaryDetailPdf', { 'userId': '<%=Model.Id %>', 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });">Print PDF</a> ]
        </li>
        <li>
            <span class="payroll-checkbox"></span>
            <span class="payrolldate strong" onclick="Payroll.Sort('payrollSearchResultDetail', 'payrolldate', event);">Schedule Date</span>
            <span class="payrolldate prd strong" onclick="Payroll.Sort('payrollSearchResultDetail', 'prd', event);">Visit Date</span>
            <span class="payrollname strong" onclick="Payroll.Sort('payrollSearchResultDetail', 'payrollname', event);">Patient Name</span>
            <span class="payrolltask strong" onclick="Payroll.Sort('payrollSearchResultDetail', 'payrolltask', event);">Task</span>
            <span class="payrolltime strong">Time</span>
            <span class="payrolltotaltime strong">Total Min.</span>
            <span class="payrollmileage strong">Mileage</span>
            <span class="payrollstatus strong" onclick="Payroll.Sort('payrollSearchResultDetail', 'payrollstatus', event);">Status</span>
            <span class="payrollicon strong">Paid</span>
        </li>
    </ul>
    <% if(!Current.IsAgencyFrozen) { %>
    <input class="payroll-check-all" type="checkbox" />
    <label class="payroll-check-all-label">Select All</label>
    <% } %>
    <ol>
<%  if (Model.Visits != null && Model.Visits.Count > 0) { %>
    <%  int i = 1; %>
    <%  var totalTime = 0; %>
    <%  foreach (var visit in Model.Visits) { %>
        <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
            <span class="payroll-checkbox"><% if(!Current.IsAgencyFrozen) { %><%= Html.CheckBox("visitSelected", false, new { @id = "visitSelected" + i, @class = "radio payroll-check", @value = string.Format("{0}", visit.Id) })%><% } %></span>
            <span class="payrolldate"><%= visit.ScheduleDate.ToString("MM/dd/yyyy") %></span>
            <span class="payrolldate prd"><%= visit.VisitDate.ToString("MM/dd/yyyy")%></span>
            <span class="payrollname"><%= visit.PatientName %></span>
            <span class="payrolltask"><%= visit.TaskName %></span>
            <span class="payrolltime"><%= visit.TimeIn.IsNotNullOrEmpty() && visit.TimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", visit.TimeIn, visit.TimeOut) : string.Empty %></span>
            <span class="payrolltotaltime"><%= visit.MinSpent %></span>
            <span class="payrollmileage"><%= visit.AssociatedMileage %></span>
            <span class="payrollstatus"><%= visit.StatusName %></span>
            <span class="payrollicon"><span class='img icon <%= visit.IsVisitPaid ? "success-small" : "error-small" %>'></span></span>
        </li>
        <%  i++; %>
        <%  totalTime += visit.MinSpent; %>
    <%  } %>
        <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
            <span class="payrolltotaltimetitle">Total Time :</span>
            <span class="payrolltotalhour"><%=string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)%></span>
        </li>
<%  } else { %>
        <li class="align-center">No Results found</li>
<%  } %>
    </ol>
</div>
<script type="text/javascript">
    $(".payroll ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
    $(".payroll-check-all").unbind("change").change(function() {
        if ($(this).prop("checked")) {
            $(this).next("label").text("Deselect All");
            $(this).closest(".payroll").find(".payroll-check").prop("checked", true);
        } else {
            $(this).next("label").text("Select All");
            $(this).closest(".payroll").find(".payroll-check").prop("checked", false);
        }
    })
    $(".payroll-check-all-label").unbind("click").click(function() {
        var checkbox = $(this).prev(".payroll-check-all"),
            checked = checkbox.prop("checked");
        checkbox.prop("checked", !checked).change();
    });
</script>