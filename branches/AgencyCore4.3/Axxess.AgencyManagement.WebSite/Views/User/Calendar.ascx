﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<span class="wintitle">My Monthly Calendar | <%= Current.AgencyName%></span>
<%if (Model != null) { if (Model.UserEvents != null) { var userEvents = Model.UserEvents; var startdate = Model.FromDate; var enddate = Model.ToDate; var currentdate = startdate.AddDays(-(int)startdate.DayOfWeek); %>
<div class="trical">
    <span class="abs-left"><a onclick="User.NavigateMonthlyCalendar('<%= Model.FromDate.AddDays(-1).Month%>','<%=Model.FromDate.AddDays(-1).Year %>');"><span class="largefont">&#171;</span> Previous Month</a></span>
    <span class="abs-right"><a onclick="User.NavigateMonthlyCalendar('<%= Model.ToDate.AddDays(1).Month%>','<%=Model.ToDate.AddDays(1).Year %>');">Next Month <span class="largefont">&#187;</span></a></span>
    <div class="clear"></div>
    <span class="strong"><%= Current.UserFullName %></span>
    <div class="buttons editeps"><ul><li><a href="javascript:void(0);" onclick="User.NavigateMonthlyCalendar('<%= Model.FromDate.Month %>','<%= Model.FromDate.Year %>');">Refresh</a></li><%= String.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('User/UserCalendarPdf', {{ 'month': '{0}' }});\">Print</a></li>", startdate)%></ul></div>
    <div class="clear"></div>
    <div class="cal bigtd" style="width:100%; margin:0px;">
        <table>
            <thead>
                <tr>
                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate)%></td>
                </tr><tr>
                    <th>Sunday</th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                </tr>
            </thead><tbody>
            <% var weekNumber=DateUtilities.Weeks(startdate.Month, startdate.Year);
               for (int i = 0; i <= weekNumber; i++){ %>
                <tr><%
                string tooltip = "";
                int addedDate = (i) * 7;
                for (int j = 0; j <= 6; j++) {
                    var specificDate = currentdate.AddDays(j + addedDate);
                    if ( specificDate.Date < startdate.Date || specificDate.Date > enddate.Date) { %>
                    <td class="inactive ui-droppable"></td><%
                    } else {
                        var events = userEvents.FindAll(e => e.VisitDate.IsValid() && e.VisitDate.Date == specificDate.Date);
                        var count = events.Count;
                        if (count > 1) {
                            var allevents = "<br />";
                            events.ForEach(e => { allevents += string.Format("{0}- <em>{1}</em><br />", e.TaskName, e.PatientName); }); %>
                    <td class="multi ui-droppable" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                            tooltip = specificDate.ToShortDateString() + allevents;
                        } else if (count == 1) {
                            var evnt = events.First();
                            var missed = (evnt.IsMissedVisit) ? "missed" : "";
                            var status = evnt.Status; %>
                    <td class="status<%= status %> scheduled <%= missed %> ui-droppable" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                            tooltip = specificDate.ToShortDateString() + string.Format("<br />{0}- <em>{1}</em>", evnt.TaskName, evnt.PatientName.Clean());
                        } else { %>
                    <td class="ui-droppable" date='<%= specificDate %>' onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                            tooltip = "";
                        }
                        var dayOnClick = Current.HasRight(Permissions.ScheduleVisits) ? string.Format("Schedule.Add('{0}');", specificDate.ToShortDateString()) : ""; %>
                        <%= string.Format("<div class=\"datelabel\"{0}>{1}</div></td>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day)%><%
                    }
                } %>
                </tr><%
            } %>
            </tbody>
        </table>
    </div>
</div>
<div class="clear"></div>
<fieldset class="calendar-legend">
    <ul>
        <li><div class="scheduled">&#160;</div>Scheduled</li>
        <li><div class="completed">&#160;</div>Completed</li>
        <li><div class="multi">&#160;</div>Multiple</li>
    </ul>
</fieldset>
<div class="clear"></div>
<div id="UserCalendar_GridContainer"><% Html.RenderPartial("~/Views/User/CalendarGrid.ascx", Model); %></div>
<%} %>
<%}%>