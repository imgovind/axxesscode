﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div id="New_User_Content">
    <fieldset>
    
        <legend>Roles</legend>
        <table class="form">
            <tbody>
                <tr class="firstrow">
                    <td>
                        <input id="New_User_Role_1" type="checkbox" class="radio required" name="AgencyRoleList" value="1" <%= Model.Roles.IsAgencyAdmin().ToChecked() %>/>
                        <label for="New_User_Role_1">Administrator</label>
                    </td>
                    <td>
                        <input id="New_User_Role_2" type="checkbox" class="radio required" name="AgencyRoleList" value="2" <%= Model.Roles.IsDirectorOfNursing().ToChecked() %>/>
                        <label for="New_User_Role_2">Director of Nursing</label>
                    </td>
                    <td>
                        <input id="New_User_Role_3" type="checkbox" class="radio required" name="AgencyRoleList" value="3" <%= Model.Roles.IsCaseManager().ToChecked() %>/>
                        <label for="New_User_Role_3">Case Manager</label>
                    </td>
                    <td>
                        <input id="New_User_Role_4" type="checkbox" class="radio required" name="AgencyRoleList" value="4" <%= Model.Roles.IsNurse().ToChecked() %> />
                        <label for="New_User_Role_4">Nursing</label>
                    </td>
                    <td>
                        <input id="New_User_Role_5" type="checkbox" class="radio required" name="AgencyRoleList" value="5" <%= Model.Roles.IsClerk().ToChecked() %>/>
                        <label for="New_User_Role_5">Clerk (non-clinical)</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="New_User_Role_6" type="checkbox" class="radio required" name="AgencyRoleList" value="6" <%= Model.Roles.IsPT().ToChecked() %>/>
                        <label for="New_User_Role_6">Physical Therapist</label>
                    </td>
                    <td>
                        <input id="New_User_Role_7" type="checkbox" class="radio required" name="AgencyRoleList" value="7" <%= Model.Roles.IsOT().ToChecked() %>/>
                        <label for="New_User_Role_7">Occupational Therapist</label>
                    </td>
                    <td>
                        <input id="New_User_Role_8" type="checkbox" class="radio required" name="AgencyRoleList" value="8" <%= Model.Roles.IsRole(AgencyRoles.SpeechTherapist).ToChecked() %>/>
                        <label for="New_User_Role_8">Speech Therapist</label>
                    </td>
                    <td>
                        <input id="New_User_Role_9" type="checkbox" class="radio required" name="AgencyRoleList" value="9" <%= Model.Roles.IsRole(AgencyRoles.MedicalSocialWorker).ToChecked() %>/>
                        <label for="New_User_Role_9">Medical Social Worker</label>
                    </td>
                    <td>
                        <input id="New_User_Role_10" type="checkbox" class="radio required" name="AgencyRoleList" value="10" <%= Model.Roles.IsHHA().ToChecked() %>/>
                        <label for="New_User_Role_10">Home Health Aide</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="New_User_Role_11" type="checkbox" class="radio required" name="AgencyRoleList" value="11" <%= Model.Roles.IsScheduler().ToChecked() %>/>
                        <label for="New_User_Role_11">Scheduler</label>
                    </td>
                    <td>
                        <input id="New_User_Role_12" type="checkbox" class="radio required" name="AgencyRoleList" value="12" <%= Model.Roles.IsBiller().ToChecked() %>/>
                        <label for="New_User_Role_12">Biller</label>
                    </td>
                    <td>
                        <input id="New_User_Role_13" type="checkbox" class="radio required" name="AgencyRoleList" value="13" <%= Model.Roles.IsQA().ToChecked() %>/>
                        <label for="New_User_Role_13">Quality Assurance</label>
                    </td>
                    <td>
                        <input id="New_User_Role_14" type="checkbox" class="radio required" name="AgencyRoleList" value="14" <%= Model.Roles.IsRole(AgencyRoles.Physician).ToChecked() %>/>
                        <label for="New_User_Role_14">Physician</label>
                    </td>
                    <td>
                        <input id="New_User_Role_15" type="checkbox" class="radio required" name="AgencyRoleList" value="15" <%= Model.Roles.IsOfficeManager().ToChecked() %>/>
                        <label for="New_User_Role_15">Office Manager</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="New_User_Role_16" type="checkbox" class="radio required" name="AgencyRoleList" value="16" <%= Model.Roles.IsCommunityLiason().ToChecked() %>/>
                        <label for="New_User_Role_16">Community Liason Officer/Marketer</label>
                    </td>
                    <td>
                        <input id="New_User_Role_17" type="checkbox" class="radio required" name="AgencyRoleList" value="17" <%= Model.Roles.IsRole(AgencyRoles.ExternalReferralSource).ToChecked() %>/>
                        <label for="New_User_Role_17">External Referral Source</label>
                    </td>
                    <td>
                        <input id="New_User_Role_18" type="checkbox" class="radio required" name="AgencyRoleList" value="18" <%= Model.Roles.IsRole(AgencyRoles.DriverAndTransportation).ToChecked() %>/>
                        <label for="New_User_Role_18">Driver/Transportation</label>
                    </td>
                    <td>
                        <input id="New_User_Role_19" type="checkbox" class="radio required" name="AgencyRoleList" value="19" <%= Model.Roles.IsRole(AgencyRoles.OfficeStaff).ToChecked() %>/>
                        <label for="New_User_Role_19">Office Staff</label>
                    </td>
                    <td>
                        <input id="New_User_Role_20" type="checkbox" class="radio required" name="AgencyRoleList" value="20" <%= Model.Roles.IsRole(AgencyRoles.Auditor).ToChecked() %>/>
                        <label for="New_User_Role_20">State Surveyor/Auditor</label>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide-column">
            <input id="New_User_AllPermissions" type="checkbox" class="radio" value="" />
            <label for="New_User_AllPermissions">Select all Permissions</label>
        </div>
        <div class="column">
            <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
            <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
            <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
            <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
        </div>
        <div class="column">
            <%= Html.PermissionList("QA", Model.PermissionsArray)%>
            <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
            <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
            <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
            <%= Html.PermissionList("State Survey/Audit", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset>
        <legend>Access &#38; Restrictions</legend>
        <div class="column">
            <div class="row">
                <label for="New_User_AllowWeekendAccess" class="float-left">Allow Weekend Access?</label>
                <div class="float-right"><%= Html.CheckBox("AllowWeekendAccess", Model.AllowWeekendAccess, new { @id="New_User_AllowWeekendAccess" })%></div>
            </div>
            <div class="row">
                <label for="New_User_EarliestLoginTime" class="float-left">Earliest Login Time:</label>
                <div class="float-right"><input type="text" size="10" id="New_User_EarliestLoginTime" name="EarliestLoginTime" class="time-picker" value="<%= Model.EarliestLoginTime %>"/></div>
            </div>
            <div class="row">
                <label for="New_User_AutomaticLogoutTime" class="float-left">Automatic Logout Time:</label>
                <div class="float-right"><input type="text" size="10" id="New_User_AutomaticLogoutTime" name="AutomaticLogoutTime" class="time-picker"  value="<%= Model.AutomaticLogoutTime %>"/></div>
            </div>
        </div>
    </fieldset>
</div>