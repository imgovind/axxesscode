﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<%var data = ViewData["UserScheduleGroupName"].ToString();%>
<%= Html.Hidden("UserSchedule_GroupName", data)%>
<%= Html.Telerik().Grid(Model).Name("List_User_Schedule").HtmlAttributes(new  { @style="top:49px;"}).Columns(columns =>
{
    columns.Bound(v => v.PatientName);
    columns.Bound(v => v.Url).Template(t => t.Url).Title("Task").Width(250);
    columns.Bound(v => v.VisitDate).Format("{0:MM/dd/yyyy}").Title("Date").Width(100);
    columns.Bound(v => v.StatusName).Width(200).Title("Status");
    columns.Bound(v => v.StatusComment).Title(" ").Sortable(false).Width(30).Template(v => v.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\">{0}</a>", v.StatusComment) : string.Empty);
    columns.Bound(v => v.VisitNotes).Title(" ").Sortable(false).Width(30).Template(v => v.VisitNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\">{0}</a>", v.VisitNotes) : string.Empty);
    columns.Bound(v => v.EpisodeNotes).Title(" ").Sortable(false).Width(30).Template(v => v.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\">{0}</a>", v.EpisodeNotes) : string.Empty);
    columns.Bound(v => v.Id).Template(v => v.IsMissedVisitReady ? string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('{0}','{1}','{2}');\">Missed Visit Form</a>", v.EpisodeId, v.PatientId, v.Id) : string.Empty).Title(" ").Width(150).Sortable(false).Visible(!Current.IsAgencyFrozen);
})
    .Groupable(settings => settings.Groups(groups =>
    {
        if (data == "PatientName")
        {
            groups.Add(s => s.PatientName);
        }
        else if (data == "VisitDate")
        {
            groups.Add(s => s.VisitDate);
        }
        else if (data == "TaskName")
        {
            groups.Add(s => s.TaskName);
        }
        else
        {
            groups.Add(s => s.VisitDate);
        }
    }))
        .Scrollable().Sortable(sorting =>
                    sorting.SortMode(GridSortMode.SingleColumn)
                        .OrderBy(order =>
                        {
                            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                            if (sortName == "PatientName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.PatientName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.PatientName).Descending();
                                }
                            }
                            else if (sortName == "VisitDate")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.VisitDate).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.VisitDate).Descending();
                                }
                            }
                            else if (sortName == "Url")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.Url).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.Url).Descending();
                                }
                            }
                            else if (sortName == "StatusName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.StatusName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.StatusName).Descending();
                                }
                            }
                        })
                )%>
<script type="text/javascript">
    $("#List_User_Schedule .t-group-indicator").hide();
    $("#List_User_Schedule .t-grouping-header").remove();
    $("#List_User_Schedule .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
    $("#List_User_Schedule div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "User.LoadUserSchedule('<%=data %>','" + U.ParameterByName(link, 'List_User_Schedule-orderBy') + "');");
    });
    Agency.ToolTip("#List_User_Schedule");
</script>