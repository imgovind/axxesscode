﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<span class="wintitle">List Users | <%= Current.AgencyName %></span>
<div class="fixed-grid-height">
<% using (Html.BeginForm("ActiveUsers", "Export", FormMethod.Post)) { %>
  <div id="ActiveUsers-GridContainer">
    <% Html.RenderPartial("ActiveContent", Model != null && Model.Count > 0 ? Model.Where(u => u.Status == (int)UserStatus.Active).ToList() : new List<User>()); %>
  </div>
<%} %>    
</div><div id="InactiveUsers-GridContainer" class="absolute-grid">
<% using (Html.BeginForm("InactiveUsers", "Export", FormMethod.Post)) { %>
     <% Html.RenderPartial("InActiveContent", Model != null && Model.Count > 0 ? Model.Where(u => u.Status == (int)UserStatus.Inactive).ToList() : new List<User>()); %>
<%} %>    
</div>
