﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Change Account - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true).CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="changepassword-wrapper">
        <div id="changepassword-window">
            <% if (Model.LoginId != Guid.Empty) { %>
                <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Account Reset</span></div>
                <div class="box">
                    <div id="messages"></div>
                    <h1>Welcome to Axxess&#8482; Home Health Management System.</h1>
                    To change your password/signature, follow the steps below.
                    <div class="row">&#160;</div>
                    <h2><strong>Step 1</strong> - Verify your information below.</h2>
                     <% using (Html.BeginForm("Reset", "Account", FormMethod.Post, new { @id = "changePasswordForm", @class = "changepassword" })) %>
                    <% { %>
                    <%= Html.Hidden("LoginId", Model.LoginId, new { @id = "ChangePassword_LoginId" })%>
                    <%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "ChangePassword_EmailAddress" })%>
                    <%= Html.Hidden("ResetType", Model.ResetType.ToString(), new { @id = "ChangePassword_ResetType" })%>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Name) %>
                        <%= Model.Name %>
                    </div>
                    <div class="row">
                        <%= Html.LabelFor(a => a.EmailAddress) %>
                        <%= Model.EmailAddress %>
                    </div>
                    <div class="row">&#160;</div>
                    <% if (Model.ResetType == Change.Password) { %>
                    <h2><strong>Step 2</strong> - Enter a new password.</h2>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Password)%>
                        <%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength="20" })%>
                    </div>
                    <% } else { %>
                    <h2><strong>Step 2</strong> - Enter a new signature.</h2>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Signature)%>
                        <%= Html.PasswordFor(a => a.Signature, new { @class = "required", @maxlength="20" })%>
                    </div>
                    <% } %>
                    <div class="bottom-right">
                        <input type="submit" value="Change" class="button" style="clear: both; width: 120px!important;" />
                    </div>
                    <% } %>
                </div>
            <% } else { %>
                <div class="box-header"><span class="title">The page you requested was not found.</span></div>
                <div class="box">
                    <div class="notification warning">You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.
                    </div>
                </div>
            <% } %>
        </div>
    </div>
        <% Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            ChangePassword.Init();
        <% }).Render(); %>
</body>
</html>