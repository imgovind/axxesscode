﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'Save');"  >Save</a></li>
            <% if (Model.IsLastTab)
               { %>
              <%  if (Model.AssessmentTypeNum.ToInteger() < 10){ %><li><a href="javascript:void(0)" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveCheckError',function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Save &#38; Check for Errors</a></li>
                  <%  }else{ %><li><a href="javascript:void(0);" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveComplete',function(){Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>');});">Save &#38; Complete</a></li><%  } %>
                         <% }
               else{%><li><a href="javascript:void(0)" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveContinue',function(){});" >Save &#38; Continue</a></li><%} %>
               <li><a href="javascript:void(0)" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveExit',function(){});" >Save &#38; Exit</a></li>
             <% var hintText=string.Empty; %>
              <% if (Current.HasRight(Permissions.AccessCaseManagement) && !(Model.Status == (int)ScheduleStatus.OasisCompletedExportReady || Model.Status == (int)ScheduleStatus.OasisCompletedNotExported || Model.Status == (int)ScheduleStatus.OasisExported))
                 {  %>
            <li><a href="javascript:void(0);" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'Approve',function(){});">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString()) && (Model.Status == (int)ScheduleStatus.OasisCompletedPendingReview))
               { hintText="Note: The Approve and Return buttons"; %><li><a href="javascript:void(0);" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this));">Return</a></li><% }
               else
               {hintText="Note: The Approve button"; %>
        <% }
                 } %>
        </ul>
         
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10 && !Model.IsLastTab) { %>
        <ul class="float-right">
            <li><a href="javascript:void(0)" onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'CheckError',function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>',<%= Model.ShowOasisVendorButton.ToString().ToLower() %>)})" >Check for Errors</a></li>
        </ul>
        <%  } %>
         <% if (Current.HasRight(Permissions.AccessCaseManagement) && !(Model.Status == (int)ScheduleStatus.OasisCompletedExportReady || Model.Status == (int)ScheduleStatus.OasisCompletedNotExported || Model.Status == (int)ScheduleStatus.OasisExported))
                 {  %>
                <br /> <em><% = string.Format("{0} will not save your current changes. Click the Save button first.",hintText) %> </em>
      <% } %>
</div>
