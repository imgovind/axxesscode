﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Exported OASIS Assessments | <%= Current.AgencyName %></span>
<% var visible = Current.HasRight(Permissions.ReopenDocuments) && !Current.IsAgencyFrozen; %>
<% string pagename = "ExportedOasis"; %>
<% using (Html.BeginForm("ExportedOasis", "Export", FormMethod.Post)) { %>
<div class="wrapper grid-bg">
    <table><tr><td><label class="float-left">Branch:</label><div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = pagename+"_BranchId" })%></div></td><td><label class="float-left">Status:</label><div class="float-left"><select id="<%= pagename %>_Status" name="StatusId" ><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></td><td><div class="row"><label  class="float-left">Exported Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div></td> <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Oasis.RebindExported();">Generate</a></li></ul></div></td><td><div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename+"_ExportLink" })%></li></ul></div></td></tr></table>   
    <%  Html.Telerik().Grid<AssessmentExport>().Name(pagename + "_Grid").HtmlAttributes(new { @style = "top:30px;" }).Columns(columns =>
        {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDateFormatted).Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.ExportedDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Exported Date").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Cancel").Width(110).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('{0}','{1}');\" >Generate Cancel</a>", o.AssessmentId,  o.AssessmentType)%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('<#= AssessmentId#>','<#= AssessmentType#>');\" >Generate Cancel</a>").Sortable(false).Visible(visible).Sortable(false);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(60).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');\" >Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');\" >Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Exported", "Oasis", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
</div>
<script type="text/javascript">$("#window_oasisExported #<%= pagename %>_Grid .t-grid-content").css({ "height": "auto"});</script>
<% } %>

