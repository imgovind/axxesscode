﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Transfer for Discharge | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class=" tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M0010 &#8211; M0150'>Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M1045 &#8211; M1055'>Risk Assessment</a>", Model.TypeName, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M1500 &#8211; M1510' > Cardiac</a>", Model.TypeName, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M2004 &#8211; M2015'>Medications</a>", Model.TypeName, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M2300 &#8211; M2310'>Emergent Care</a>", Model.TypeName, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' tooltip='M0903 &#8211; M0906<br />M2400 &#8211; M2440'>Transfer</a>", Model.TypeName, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.RiskAssessment.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Cardiac.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Medications.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.EmergentCare.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.TransferDischargeDeath.ToString()) %>
</div>