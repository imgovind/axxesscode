﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle"><%= Model.TaskName.IsNotNullOrEmpty() ? Model.TaskName : " Non-OASIS Recertification"%>  | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' >Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Patient History &#38; Diagnoses</a>", Model.TypeName, AssessmentCategory.PatientHistory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Prognosis</a>", Model.TypeName, AssessmentCategory.Prognosis.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Supportive Assistance</a>", Model.TypeName, AssessmentCategory.SupportiveAssistance.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Sensory Status</a>", Model.TypeName, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Pain</a>", Model.TypeName, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Integumentary Status</a>", Model.TypeName, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Respiratory Status</a>", Model.TypeName, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Endocrine</a>", Model.TypeName, AssessmentCategory.Endocrine.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Cardiac</a>", Model.TypeName, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Elimination Status</a>", Model.TypeName, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Nutrition</a>", Model.TypeName, AssessmentCategory.Nutrition.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Neuro/Behavioral</a>", Model.TypeName, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >ADL/IADLs</a>", Model.TypeName, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Supplies Worksheet</a>", Model.TypeName, AssessmentCategory.SuppliesDme.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Medications</a>", Model.TypeName, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Therapy Need &#38; Plan Of Care</a>", Model.TypeName, AssessmentCategory.TherapyNeed.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Orders for Discipline and Treatment</a>", Model.TypeName, AssessmentCategory.OrdersDisciplineTreatment.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.PatientHistory.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Prognosis.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.SupportiveAssistance.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Sensory.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Pain.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Integumentary.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Respiratory.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Endocrine.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Cardiac.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Elimination.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Nutrition.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.NeuroBehavioral.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.AdlIadl.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.SuppliesDme.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Medications.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.TherapyNeed.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.OrdersDisciplineTreatment.ToString()) %>
</div>