<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<% string[] RiskAssessmentInterventions = data.AnswerArray("485RiskAssessmentIntervention"); %>
<% string[] RiskAssessmentGoals = data.AnswerArray("485RiskAssessmentGoals"); %>
<%  if (RiskAssessmentInterventions.Length > 0 || (data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RiskAssessmentInterventions.Contains("1")) { %>
    printview.checkbox("SN to assist patient to obtain ERS button.",true) +
    <%  } %>
    <%  if (RiskAssessmentInterventions.Contains("2")) { %>
    printview.checkbox("SN to develop individualized emergency plan with patient.",true) +
    <%  } %>
    <%  if (RiskAssessmentInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RiskInterventionComments").Clean()%>") +
    <%  } %>
    "","Risk Assessment Interventions");
<%  } %>
<%  if (RiskAssessmentGoals.Length > 0 || (data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RiskAssessmentGoals.Contains("1")) { %>
    printview.checkbox("The patient will have no hospitalizations during the episode.",true) +
    <%  } %>
    <%  if (RiskAssessmentGoals.Contains("2")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485VerbalizeEmergencyPlanPerson", "Patient/Caregiver")%> will verbalize understanding of individualized emergency plan by the end of the episode.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RiskGoalComments").Clean()%>") +
    <%  } %>
    "","Risk Assessment Goals");
<%  } %>