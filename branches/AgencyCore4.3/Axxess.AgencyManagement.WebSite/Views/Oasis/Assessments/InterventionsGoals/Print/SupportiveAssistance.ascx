<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var SupportiveAssistanceInterventions = data.AnswerArray("485SupportiveAssistanceInterventions"); %>
<%  if (SupportiveAssistanceInterventions.Length > 0 || (data.ContainsKey("485SupportiveAssistanceComments") && data["485SupportiveAssistanceComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    
    <%  if (data.ContainsKey("485SupportiveAssistanceComments") && data["485SupportiveAssistanceComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.checkbox("MSW to assess psychosocial needs, environment and assist with community referrals and resources.",<%=data.AnswerArray("485SupportiveAssistanceInterventions").Contains("1").ToString().ToLower()%>)+
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SupportiveAssistanceComments").Clean()%>") +
    <%  } %>
    "","Supportive Assistance Interventions");
<%  } %>
<%  if (data.ContainsKey("485SupportiveAssistanceGoalsComments") && data["485SupportiveAssistanceGoalsComments"].Answer.IsNotNullOrEmpty()) { %>
printview.addsection(
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SupportiveAssistanceGoalsComments").Clean()%>"),"Supportive Assistance Goals");
<%  } %>
