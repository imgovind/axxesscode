<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var MedicationInterventions = data.AnswerArray("485MedicationInterventions"); %>
<%  var MedicationGoals = data.AnswerArray("485MedicationGoals"); %>
<%  if (MedicationInterventions.Length > 0 || (data.ContainsKey("485MedicationInterventionComments") && data["485MedicationInterventionComments"].Answer.IsNotNullOrEmpty()))
    { %>
printview.addsection(
    <%  if (MedicationInterventions.Contains("1")) { %>
    printview.checkbox("SN to assess patient filling medication box to determine if patient is preparing correctly.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("2")) { %>
    printview.checkbox("SN to assess caregiver filling medication box to determine if caregiver is preparing correctly.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("3")) { %>
    printview.checkbox("SN to determine if the <%= data.AnswerOrDefault("485DetermineFrequencEachMedPerson", "Patient/Caregiver")%> is able to identify the correct dose, route, and frequency of each medication.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("4")) { %>
    printview.checkbox("SN to assess if the <%= data.AnswerOrDefault("485AssessIndicationEachMedPerson", "Patient/Caregiver")%> can verbalize an understanding of the indication for each medication.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("5")) { %>
    printview.checkbox("SN to establish reminders to alert patient to take medications at correct times.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("6")) { %>
    printview.checkbox("SN to assess the <%= data.AnswerOrDefault("485AssessAdminInjectMedsPerson", "Patient/Caregiver")%> administering injectable medications to determine if proper technique is utilized.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("7")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructHighRiskMedsPerson", "Patient/Caregiver")%> on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets, sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle relaxants.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("8")) { %>
    printview.checkbox("SN to administer IV <%= data.AnswerOrDefault("485AdministerIVType", "<span class='blank'></span>")%> at rate of <%= data.AnswerOrDefault("485AdministerIVRate", "<span class='blank'></span>")%> via <%= data.AnswerOrDefault("485AdministerIVVia", "<span class='blank'></span>")%> every <%= data.AnswerOrDefault("485AdministerIVEvery", "<span class='blank'></span>")%>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("9")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructAdministerIVPerson", "Patient/Caregiver")%> to administer IV at rate of <%= data.AnswerOrDefault("485InstructAdministerIVRate", "<span class='blank'></span>")%> via <%= data.AnswerOrDefault("485InstructAdministerIVVia", "<span class='blank'></span>")%> every <%= data.AnswerOrDefault("485InstructAdministerIVEvery", "<span class='blank'></span>")%>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("10")) { %>
    printview.checkbox("SN to flush peripheral IV with <%= data.AnswerOrDefault("485FlushPeripheralIVWith", "<span class='short blank'></span>")%>cc of <%= data.AnswerOrDefault("485FlushPeripheralIVOf", "<span class='blank'></span>")%> every <%= data.AnswerOrDefault("485FlushPeripheralIVEvery", "<span class='blank'></span>")%>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("11")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructFlushPerpheralIVPerson", "Patient/Caregiver")%> to flush peripheral IV with <%= data.AnswerOrDefault("485InstructFlushPerpheralIVWith", "<span class='short blank'></span>")%>cc of <%= data.AnswerOrDefault("485InstructFlushPerpheralIVOf", "<span class='blank'></span>") %> every <%= data.AnswerOrDefault("485InstructFlushPerpheralIVEvery", "<span class='blank'></span>")%>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("12")) { %>
    printview.checkbox("SN to flush central line with <%= data.AnswerOrDefault("485FlushCentralLineWith", "<span class='short blank'></span>") %>cc of <%= data.AnswerOrDefault("485FlushCentralLineOf", "<span class='blank'></span>") %> every <%= data.AnswerOrDefault("485FlushCentralLineEvery", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("13")) { %>
    printview.checkbox("SN to instruct <%= data.AnswerOrDefault("485InstructFlushCentralLinePerson", "Patient/Caregiver") %> to flush central line with <%= data.AnswerOrDefault("485InstructFlushCentralLineWith", "<span class='short blank'></span>") %>cc of <%= data.AnswerOrDefault("485InstructFlushCentralLineOf", "<span class='blank'></span>") %> every <%= data.AnswerOrDefault("485InstructFlushCentralLineEvery", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("14")) { %>
    printview.checkbox("SN to access <%= data.AnswerOrDefault("485AccessPortType", "<span class='blank'></span>") %> port every <%= data.AnswerOrDefault("485AccessPortTypeEvery", "<span class='blank'></span>") %> and flush with <%= data.AnswerOrDefault("485AccessPortTypeWith", "<span class='short blank'></span>") %>cc of <%= data.AnswerOrDefault("485AccessPortTypeOf", "<span class='blank'></span>") %> every <%= data.AnswerOrDefault("485AccessPortTypeFrequency", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (MedicationInterventions.Contains("15")) { %>
    printview.checkbox("SN to change IV tubing every <%= data.AnswerOrDefault("485ChangeIVTubingEvery", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485MedicationInterventionComments") && data["485MedicationInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485MedicationInterventionComments").Clean()%>") +
    <%  } %>
    "","Medication Interventions");
<%  } %>
<%  if (MedicationGoals.Length > 0 || (data.ContainsKey("485MedicationGoalComments") && data["485MedicationGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (MedicationGoals.Contains("1")) { %>
    printview.checkbox("Patient will remain free of adverse medication reactions during the episode.",true) +
    <%  } %>
    <%  if (MedicationGoals.Contains("2")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485VerbalizeMedRegimenUnderstandingPerson", "Patient/Caregiver") %> will verbalize understanding of medication regimen, dose, route, frequency, indications, and side effects by: <%= data.AnswerOrDefault("485VerbalizeMedRegimenUnderstandingDate", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (MedicationGoals.Contains("3")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485DemonstratePeripheralIVLineFlushPerson", "Patient/Caregiver") %> will demonstrate understanding of flushing peripheral IV line.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485MedicationGoalComments") && data["485MedicationGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485MedicationGoalComments").Clean()%>") +
    <%  } %>
    "","Medication Goals");
<%  } %>