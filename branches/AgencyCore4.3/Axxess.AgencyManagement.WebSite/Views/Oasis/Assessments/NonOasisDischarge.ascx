﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">Non-OASIS Discharge | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty )%></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' >Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Risk Assessment</a>", Model.TypeName, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Sensory Status</a>", Model.TypeName, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Pain</a>", Model.TypeName, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Integumentary Status</a>", Model.TypeName, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Respiratory Status</a>", Model.TypeName, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Cardiac</a>", Model.TypeName, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Elimination Status</a>", Model.TypeName, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Neuro/Behavioral</a>", Model.TypeName, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >ADL/IADLs</a>", Model.TypeName, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Medications</a>", Model.TypeName, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Care Management</a>", Model.TypeName, AssessmentCategory.CareManagement.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Emergent Care</a>", Model.TypeName, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='#{0}_{1}' >Discharge</a>", Model.TypeName, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.RiskAssessment.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Sensory.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Pain.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Integumentary.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Respiratory.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Cardiac.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Elimination.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.NeuroBehavioral.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.AdlIadl.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.Medications.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.CareManagement.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.EmergentCare.ToString()) %>
    <%= string.Format("<div id='{0}_{1}' class='general loading'></div>",Model.TypeName,AssessmentCategory.TransferDischargeDeath.ToString()) %>
</div>