<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Demographics.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/PatientHistory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Sensory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Pain.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Integumentary.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Elimination.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/AdlIadl.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Medications.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/TherapyNeed.ascx", Model); %>
