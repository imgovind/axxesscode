<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() != 5 || Model.AssessmentTypeNum.ToInteger() != 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "CardiacForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Cardiac.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
    <fieldset class="oasis">
        <legend>Symptoms in Heart Failure Patients</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1500">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1500')">(M1500)</a>
                    Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1500HeartFailureSymptons", "", new { @id = Model.TypeName + "_M1500HeartFailureSymptonsHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1500HeartFailureSymptons", "00", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("00"), new { @id = Model.TypeName + "_M1500HeartFailureSymptons0" })%>
                        <label for="<%= Model.TypeName %>_M1500HeartFailureSymptons0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1500HeartFailureSymptons", "01", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("01"), new { @id = Model.TypeName + "_M1500HeartFailureSymptons1" })%>
                        <label for="<%= Model.TypeName %>_M1500HeartFailureSymptons1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1500HeartFailureSymptons", "02", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("02"), new { @id = Model.TypeName + "_M1500HeartFailureSymptons2" })%>
                        <label for="<%= Model.TypeName %>_M1500HeartFailureSymptons2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Not assessed</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1500HeartFailureSymptons", "NA", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("NA"), new { @id = Model.TypeName + "_M1500HeartFailureSymptonsNA" })%>
                        <label for="<%= Model.TypeName %>_M1500HeartFailureSymptonsNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient does not have diagnosis of heart failure</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1500')">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1510">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1510')">(M1510)</a>
                    Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond?
                    <em>(Mark all that apply)</em>
                </label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupNoAction", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupNoActionHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupNoAction' class='M1510' name='{0}_M1510HeartFailureFollowupNoAction' type='checkbox' value='0' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("0").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupNoAction">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No action taken</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupPhysicianCon", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupPhysicianConHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupPhysicianCon' class='M1510' name='{0}_M1510HeartFailureFollowupPhysicianCon' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupPhysicianCon").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupPhysicianCon">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient&#8217;s physician (or other primary care practitioner) contacted the same day</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupAdvisedEmg", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupAdvisedEmgHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupAdvisedEmg' class='M1510' name='{0}_M1510HeartFailureFollowupAdvisedEmg' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupAdvisedEmg").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupAdvisedEmg">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupParameters", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupParametersHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupParameters' class='M1510' name='{0}_M1510HeartFailureFollowupParameters' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupParameters").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupParameters">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Implemented physician-ordered patient-specific established parameters for treatment</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupInterventions", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupInterventionsHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupInterventions' class='M1510' name='{0}_M1510HeartFailureFollowupInterventions' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupInterventions").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupInterventions">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Patient education or other clinical interventions</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupChange", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupChangeHidden" })%>
                        <%= string.Format("<input id='{0}_M1510HeartFailureFollowupChange' class='M1510' name='{0}_M1510HeartFailureFollowupChange' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1510HeartFailureFollowupChange").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1510HeartFailureFollowupChange">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1510');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Cardiovascular</legend>
        <%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericCardioVascular", "", new { @id = Model.TypeName + "_GenericCardioVascularHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Within Normal Limits' id='{0}_GenericCardioVascular1' name='{0}_GenericCardioVascular' value='1' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Heart Rhythm' id='{0}_GenericCardioVascular2' name='{0}_GenericCardioVascular' value='2' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular2" class="radio">Heart Rhythm:</label>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular2More" class="float-right">
                            <%  var heartRhythm = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Regular/WNL", Value = "1" },
                                    new SelectListItem { Text = "Tachycardia", Value = "2" },
                                    new SelectListItem { Text = "Bradycardia", Value = "3" },
                                    new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
                                    new SelectListItem { Text = "Other", Value = "5" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericHeartSoundsType", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericHeartSoundsType", heartRhythm, new { @id = Model.TypeName + "_GenericHeartSoundsType", @title = "(Optional) Cardiovascular, Heart Rhythm", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Cap Refill' id='{0}_GenericCardioVascular3' name='{0}_GenericCardioVascular' value='3' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular3" class="radio">Cap Refill:</label>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular3More">
                            <%= Html.Hidden(Model.TypeName + "_GenericCapRefillLessThan3", "", new { @id = Model.TypeName + "_GenericCapRefillLessThan3Hidden" })%>
                            <div class="float-right">
                                <%= Html.RadioButton(Model.TypeName + "_GenericCapRefillLessThan3", "1", data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("1"), new { @id = Model.TypeName + "_GenericCapRefillLessThan31", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Cap Refill Less Than 3 Seconds" })%>
                                <label for="<%= Model.TypeName %>_GenericCapRefillLessThan31" class="inline-radio">Less than (&#60;) 3 sec</label>
                                <br />
                                <%= Html.RadioButton(Model.TypeName + "_GenericCapRefillLessThan3", "0", data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("0"), new { @id = Model.TypeName + "_GenericCapRefillLessThan30", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Cap Refill Greater Than 3 Seconds" })%>
                                <label for="<%= Model.TypeName %>_GenericCapRefillLessThan30" class="inline-radio">Greater than (&#62;) 3 sec</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Pulses' id='{0}_GenericCardioVascular4' name='{0}_GenericCardioVascular' value='4' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular4" class="radio">Pulses:</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular4More" class="margin">
                            <label for="<%= Model.TypeName %>_GenericCardiovascularRadial" class="float-left">Radial:</label>
                            <div class="float-right">
                                <%  var radialPulse = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1+ Weak", Value = "1" },
                                        new SelectListItem { Text = "2+ Normal", Value = "2" },
                                        new SelectListItem { Text = "3+ Strong", Value = "3" },
                                        new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularRadial", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularRadial", radialPulse, new { @id = Model.TypeName + "_GenericCardiovascularRadial", @title = "(Optional) Cardiovascular, Radial Pulse", @class = "oe" })%>
                            </div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularRadialPosition", "0", data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("0"), new { @id = Model.TypeName + "_GenericCardiovascularRadialPosition0", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Radial Pulse Bilateral" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularRadialPosition0" class="inline-radio">Bilateral</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularRadialPosition", "1", data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("1"), new { @id = Model.TypeName + "_GenericCardiovascularRadialPosition1", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Radial Pulse Left" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularRadialPosition1" class="inline-radio">Left</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularRadialPosition", "2", data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("2"), new { @id = Model.TypeName + "_GenericCardiovascularRadialPosition2", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Radial Pulse Right" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularRadialPosition2" class="inline-radio">Right</label>
                            </div>
                            <div class="clear"></div>
                            <label for="<%= Model.TypeName %>_GenericCardiovascularPedal" class="float-left">Pedal:</label>
                            <div class="float-right">
                                <%  var pedalPulse = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1+ Weak", Value = "1" },
                                        new SelectListItem { Text = "2+ Normal", Value = "2" },
                                        new SelectListItem { Text = "3+ Strong", Value = "3" },
                                        new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularPedal", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularPedal", pedalPulse, new { @id = Model.TypeName + "_GenericCardiovascularPedal", @title = "(Optional) Cardiovascular, Pedal Pulse", @class = "oe" })%>
                            </div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularPedalPosition", "0", data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("0"), new { @id = Model.TypeName + "_GenericCardiovascularPedalPosition0", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Pedal Pulse Bilateral" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularPedalPosition0" class="inline-radio">Bilateral</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularPedalPosition", "1", data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("1"), new { @id = Model.TypeName + "_GenericCardiovascularPedalPosition1", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Pedal Pulse Left" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularPedalPosition1" class="inline-radio">Left</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericCardiovascularPedalPosition", "2", data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("2"), new { @id = Model.TypeName + "_GenericCardiovascularPedalPosition2", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Pedal Pulse Right" })%>
                                <label for="<%= Model.TypeName %>_GenericCardiovascularPedalPosition2" class="inline-radio">Right</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Edema' id='{0}_GenericCardioVascular5' name='{0}_GenericCardioVascular' value='5' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular5" class="radio">Edema:</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular5More" class="margin">
                            <label for="<%= Model.TypeName %>_GenericEdemaLocation" class="float-left">Location:</label>
                            <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericEdemaLocation", data.AnswerOrEmptyString("GenericEdemaLocation"), new { @id = Model.TypeName + "_GenericEdemaLocation", @maxlength = "50", @title = "(Optional) Cardiovascular, Edema Location", @class = "oe" })%></div>
                            <div class="clear"></div>
                            <%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericPittingEdemaType", "", new { @id = Model.TypeName + "_GenericPittingEdemaTypeHidden" })%>
                            <%= string.Format("<input title='(Optional) Cardiovascular, Pitting Edema' id='{0}_GenericPittingEdemaType1' name='{0}_GenericPittingEdemaType' value='1' type='checkbox' {1} />", Model.TypeName, genericPittingEdemaType.Contains("1").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericPittingEdemaType1">Pitting</label>
                            <div id="<%= Model.TypeName %>_GenericPittingEdemaType1More" class="float-right">
                                <%  var pitting = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1+", Value = "1" },
                                        new SelectListItem { Text = "2+", Value = "2" },
                                        new SelectListItem { Text = "3+", Value = "3" },
                                        new SelectListItem { Text = "4+", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericEdemaPitting", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericEdemaPitting", pitting, new { @id = Model.TypeName + "_GenericEdemaPitting", @title = "(Optional) Cardiovascular, Pitting Edema Severity", @class = "oe" })%>
                            </div>
                            <div class="clear"></div>
                            <%= string.Format("<input title='(Optional) Cardiovascular, Nonpitting Edema' id='{0}_GenericPittingEdemaType2' name='{0}_GenericPittingEdemaType' value='2' type='checkbox' {1} />", Model.TypeName, genericPittingEdemaType.Contains("2").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericPittingEdemaType2">Nonpitting</label>
                            <div class="<%= Model.TypeName %>_GenericPittingEdemaType2More float-right">
                                <%  var nonPitting = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "N/A", Value = "1" },
                                        new SelectListItem { Text = "Mild", Value = "2" },
                                        new SelectListItem { Text = "Moderate", Value = "3" },
                                        new SelectListItem { Text = "Severe", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericEdemaNonPitting", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericEdemaNonPitting", nonPitting, new { @id = Model.TypeName + "_GenericEdemaNonPitting", @title = "(Optional) Cardiovascular, Nonpitting Edema Severity", @class = "oe" })%>
                            </div>
                            <div class="clear"></div>
                            <div class="<%= Model.TypeName %>_GenericPittingEdemaType2More float-right">
                                <%= Html.RadioButton(Model.TypeName + "_GenericEdemaNonPittingPosition", "0", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0"), new { @id = Model.TypeName + "_GenericEdemaNonPittingPosition0", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Nonpitting Edema Bilateral" })%>
                                <label for="<%= Model.TypeName %>_GenericEdemaNonPittingPosition0" class="inline-radio">Bilateral</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericEdemaNonPittingPosition", "1", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1"), new { @id = Model.TypeName + "_GenericEdemaNonPittingPosition1", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Nonpitting Edema Left" })%>
                                <label for="<%= Model.TypeName %>_GenericEdemaNonPittingPosition1" class="inline-radio">Left</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericEdemaNonPittingPosition", "2", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2"), new { @id = Model.TypeName + "_GenericEdemaNonPittingPosition2", @class = "no_float radio deselectable", @title = "(Optional) Cardiovascular, Nonpitting Edema Right" })%>
                                <label for="<%= Model.TypeName %>_GenericEdemaNonPittingPosition2" class="inline-radio">Right</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Dizziness' id='{0}_GenericCardioVascular6' name='{0}_GenericCardioVascular' value='6' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular6" class="radio">Dizziness:</label>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular6More" class="float-right">
                            <%= Html.TextBox(Model.TypeName + "_GenericDizzinessDesc", data.AnswerOrEmptyString("GenericDizzinessDesc"), new { @id = Model.TypeName + "_GenericDizzinessDesc", @class = "st", @maxlength = "20", @title = "(Optional) Cardiovascular, Dizziness" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Chest Pain' id='{0}_GenericCardioVascular7' name='{0}_GenericCardioVascular' value='7' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular7" class="radio">Chest Pain:</label>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular7More" class="float-right">
                            <%= Html.TextBox(Model.TypeName + "_GenericChestPainDesc", data.AnswerOrEmptyString("GenericChestPainDesc"), new { @id = Model.TypeName + "_GenericChestPainDesc", @class = "st", @maxlength = "20", @title = "(Optional) Cardiovascular, Chest Pain" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Cardiovascular, Neck Vein Distention' id='{0}_GenericCardioVascular8' name='{0}_GenericCardioVascular' value='8' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCardioVascular8" class="radio">Neck Vein Distention:</label>
                        <div id="<%= Model.TypeName %>_GenericCardioVascular8More" class="float-right">
                            <%= Html.TextBox(Model.TypeName + "_GenericNeckVeinDistentionDesc", data.AnswerOrEmptyString("GenericNeckVeinDistentionDesc"), new { @id = Model.TypeName + "_GenericNeckVeinDistentionDesc", @class = "st", @maxlength = "20", @title = "(Optional) Cardiovascular, Neck Vein Distention" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPacemakerDate" class="float-left">Pacemaker Insertion Date:</label>
                <div class="float-right">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericPacemakerDate" value="<%= data.AnswerOrEmptyString("GenericPacemakerDate") %>" id="<%= Model.TypeName %>_GenericPacemakerDate" title="(Optional) Cardiovascular, Pacemaker Insertion Date" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPacemakerDate" class="float-left">AICD Insertion Date:</label>
                <div class="float-right">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericAICDDate" value="<%= data.AnswerOrEmptyString("GenericAICDDate") %>" id="<%= Model.TypeName %>_GenericAICDDate" title="(Optional) Cardiovascular, AICD Insertion Date" />
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericAICDDate" class="strong">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericCardiovascularComments", data.AnswerOrEmptyString("GenericCardiovascularComments"), 5, 70, new { @id = Model.TypeName + "_GenericCardiovascularComments", @title = "(Optional) Cardiovascular Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Cardiac.ascx", Model); %>
    <%  } %>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular2"),
        $("#<%= Model.TypeName %>_GenericCardioVascular2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular3"),
        $("#<%= Model.TypeName %>_GenericCardioVascular3More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular4"),
        $("#<%= Model.TypeName %>_GenericCardioVascular4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular5"),
        $("#<%= Model.TypeName %>_GenericCardioVascular5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericPittingEdemaType1"),
        $("#<%= Model.TypeName %>_GenericPittingEdemaType1More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericPittingEdemaType2"),
        $(".<%= Model.TypeName %>_GenericPittingEdemaType2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericPittingEdemaType2"),
        $("#<%= Model.TypeName %>_GenericPittingEdemaType2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular6"),
        $("#<%= Model.TypeName %>_GenericCardioVascular6More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular7"),
        $("#<%= Model.TypeName %>_GenericCardioVascular7More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericCardioVascular8"),
        $("#<%= Model.TypeName %>_GenericCardioVascular8More"));
<%  } %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1500HeartFailureSymptons", "00|NA",
        $("#<%= Model.TypeName %>_M1510"));
<%  } %>
</script>