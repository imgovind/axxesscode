<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "AdlForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.AdlIadl.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset class="loc485">
	    <legend>Activities Permitted (Locator #18.B)</legend>
        <%  string[] activitiesPermitted = data.AnswerArray("485ActivitiesPermitted"); %>
        <%= Html.Hidden(Model.TypeName + "_485ActivitiesPermitted", "", new { @id = Model.TypeName + "_485ActivitiesPermittedHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Complete Bed Rest' id='{0}_485ActivitiesPermitted1' name='{0}_485ActivitiesPermitted' value='1' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted1" class="radio">Complete bed rest</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Bed Rest with BRP' id='{0}_485ActivitiesPermitted2' name='{0}_485ActivitiesPermitted' value='2' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted2" class="radio">Bed rest with BRP</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Up as Tolerated' id='{0}_485ActivitiesPermitted3' name='{0}_485ActivitiesPermitted' value='3' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted3" class="radio">Up as tolerated</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Transfer Bed-Chair' id='{0}_485ActivitiesPermitted4' name='{0}_485ActivitiesPermitted' value='4' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted4" class="radio">Transfer bed-chair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Exercise Prescribed' id='{0}_485ActivitiesPermitted5' name='{0}_485ActivitiesPermitted' value='5' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted5" class="radio">Exercise prescribed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Partial Weight Bearing' id='{0}_485ActivitiesPermitted6' name='{0}_485ActivitiesPermitted' value='6' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted6" class="radio">Partial weight bearing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Independent at Home' id='{0}_485ActivitiesPermitted7' name='{0}_485ActivitiesPermitted' value='7' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted7" class="radio">Independent at home</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Crutches' id='{0}_485ActivitiesPermitted8' name='{0}_485ActivitiesPermitted' value='8' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted8" class="radio">Crutches</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Cane' id='{0}_485ActivitiesPermitted9' name='{0}_485ActivitiesPermitted' value='9' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("9").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermitted9" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Wheelchair' id='{0}_485ActivitiesPermittedA' name='{0}_485ActivitiesPermitted' value='A' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("A").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermittedA" class="radio">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 18B) Activities Permitted, Walker' id='{0}_485ActivitiesPermittedB' name='{0}_485ActivitiesPermitted' value='B' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("B").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermittedB" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_485ActivitiesPermittedC' name='{0}_485ActivitiesPermitted' value='C' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("C").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermittedC" class="radio">No Restrictions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_485ActivitiesPermittedD' name='{0}_485ActivitiesPermitted' value='D' class='radio float-left' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("D").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485ActivitiesPermittedD" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_485ActivitiesPermittedDMore" class="normal margin">
                            <label for="<%= Model.TypeName %>_485ActivitiesPermittedOther"><em>(Specify)</em></label>
                            <%= Html.TextArea(Model.TypeName + "_485ActivitiesPermittedOther", data.AnswerOrEmptyString("485ActivitiesPermittedOther"), new { @id = Model.TypeName + "_485ActivitiesPermittedOther" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Musculoskeletal</legend>
        <%  string[] genericMusculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericMusculoskeletal", "", new { @id = Model.TypeName + "_GenericMusculoskeletalHidden" })%>
	    <div class="wide-column">
            <div class="row">
                <%= Html.Hidden(Model.TypeName + "_GenericMusculoskeletal", "", new { @id = Model.TypeName + "_GenericMusculoskeletalHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Musculoskeletal, Within Normal Limits' id='{0}_GenericMusculoskeletal1' class='radio float-left' name='{0}_GenericMusculoskeletal' value='1' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericMusculoskeletal1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Grip Strength' id='{0}_GenericMusculoskeletal2' class='radio float-left' name='{0}_GenericMusculoskeletal' value='2' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal2" class="radio">Grip Strength:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal2More" class="rel float-right">
                            <div class="float-right">
                                <%  var handGrips = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Strong", Value = "1" },
                                        new SelectListItem { Text = "Weak", Value = "2" },
                                        new SelectListItem { Text = "Other", Value = "3" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalHandGrips", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalHandGrips", handGrips, new { @id = Model.TypeName + "_GenericMusculoskeletalHandGrips", @title = "(Optional) Musculoskeletal, Grip Strength Level" })%>
                            </div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <%= Html.RadioButton(Model.TypeName + "_GenericMusculoskeletalHandGripsPosition", "0", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("0"), new { @id = Model.TypeName + "_GenericMusculoskeletalHandGripsPosition0", @class = "radio deselectable no_float", @title = "(Optional) Musculoskeletal, Grip Strength Location, Bilateral" })%>
                                <label for="<%= Model.TypeName %>_GenericMusculoskeletalHandGripsPosition0" class="inline-radio">Bilateral</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericMusculoskeletalHandGripsPosition", "1", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("1"), new { @id = Model.TypeName + "_GenericMusculoskeletalHandGripsPosition1", @class = "radio deselectable no_float", @title = "(Optional) Musculoskeletal, Grip Strength Location, Left" })%>
                                <label for="<%= Model.TypeName %>_GenericMusculoskeletalHandGripsPosition1" class="inline-radio">Left</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericMusculoskeletalHandGripsPosition", "2", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("2"), new { @id = Model.TypeName + "_GenericMusculoskeletalHandGripsPosition2", @class = "radio deselectable no_float", @title = "(Optional) Musculoskeletal, Grip Strength Location, Right" })%>
                                <label for="<%= Model.TypeName %>_GenericMusculoskeletalHandGripsPosition2" class="inline-radio">Right</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Impaired Motor Skill' id='{0}_GenericMusculoskeletal3' class='radio float-left' name='{0}_GenericMusculoskeletal' value='3' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal3" class="radio">Impaired Motor Skill:</label>
                        </div>
                        <div class="float-right">
                            <%  var impairedMotorSkills = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Fine", Value = "2" },
                                    new SelectListItem { Text = "Gross", Value = "3" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalImpairedMotorSkills", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalImpairedMotorSkills", impairedMotorSkills, new { @id = Model.TypeName + "_GenericMusculoskeletalImpairedMotorSkills", @title = "(Optional) Musculoskeletal, Impaired Motor Skill LeveL" })%>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Limited ROM' id='{0}_GenericMusculoskeletal4' class='radio float-left' name='{0}_GenericMusculoskeletal' value='4' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal4" class="radio">Limited ROM:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal4More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericLimitedROMLocation", data.AnswerOrEmptyString("GenericLimitedROMLocation"), new { @id = Model.TypeName + "_GenericLimitedROMLocation", @class = "loc", @maxlength = "24", @title = "(Optional) Musculoskeletal, Limited ROM Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Mobility' id='{0}_GenericMusculoskeletal5' class='radio float-left' name='{0}_GenericMusculoskeletal' value='5' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal5" class="radio">Mobility:</label>
                        </div>
                        <div class="float-right">
                            <%  var mobility = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "WNL (Within Normal Limits)", Value = "1" },
                                    new SelectListItem { Text = "Ambulatory", Value = "2" },
                                    new SelectListItem { Text = "Ambulatory w/assistance", Value = "3" },
                                    new SelectListItem { Text = "Chair fast", Value = "4" },
                                    new SelectListItem { Text = "Bedfast", Value = "5" },
                                    new SelectListItem { Text = "Non-ambulatory", Value = "6" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalMobility", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalMobility", mobility, new { @id = Model.TypeName + "_GenericMusculoskeletalMobility", @title = "(Optional) Musculoskeletal, Mobility Level" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device' id='{0}_GenericMusculoskeletal6' class='radio float-left' name='{0}_GenericMusculoskeletal' value='6' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericMusculoskeletal6" class="radio">Type Assistive Device:</label>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal6More" class="float-right">
                            <% string[] genericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device, Cane' id='{0}_GenericAssistiveDevice1' class='radio float-left' name='{0}_GenericAssistiveDevice' value='1' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericAssistiveDevice1" class="fixed inline-radio">Cane</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device, Crutches' id='{0}_GenericAssistiveDevice2' class='radio float-left' name='{0}_GenericAssistiveDevice' value='2' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericAssistiveDevice2" class="fixed inline-radio">Crutches</label>
                            </div>
                            <div class="clear"></div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device, Walker' id='{0}_GenericAssistiveDevice3' class='radio float-left' name='{0}_GenericAssistiveDevice' value='3' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("3").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericAssistiveDevice3" class="fixed inline-radio">Walker</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device, Wheelchair' id='{0}_GenericAssistiveDevice4' class='radio float-left' name='{0}_GenericAssistiveDevice' value='4' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("4").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericAssistiveDevice4" class="fixed inline-radio">Wheelchair</label>
                            </div>
                            <div class="clear"></div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Musculoskeletal, Type Assistive Device, Other' id='{0}_GenericAssistiveDevice5' class='radio float-left' name='{0}_GenericAssistiveDevice' value='5' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("5").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericAssistiveDevice5" class="fixed inline-radio">Other</label>
                            </div>
                            <div class="float-left">
                                <%= Html.TextBox(Model.TypeName + "_GenericAssistiveDeviceOther", data.AnswerOrEmptyString("GenericAssistiveDeviceOther"), new { @id = Model.TypeName + "_GenericAssistiveDeviceOther", @class = "st", @title = "(Optional) Musculoskeletal, Type Assistive Device, Specify Other" })%>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Contracture' id='{0}_GenericMusculoskeletal7' class='radio float-left' name='{0}_GenericMusculoskeletal' value='7' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal7" class="radio">Contracture:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal7More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericContractureLocation", data.AnswerOrEmptyString("GenericContractureLocation"), new { @id = Model.TypeName + "_GenericContractureLocation", @class = "loc", @maxlength = "24", @title = "(Optional) Musculoskeletal, Contracture Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Musculoskeletal, Weakness' id='{0}_GenericMusculoskeletal8' class='radio float-left' name='{0}_GenericMusculoskeletal' value='8' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericMusculoskeletal8" class="radio">Weakness</label>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Joint Pain' id='{0}_GenericMusculoskeletal9' class='radio float-left' name='{0}_GenericMusculoskeletal' value='9' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal9" class="radio">Joint Pain:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal9More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericJointPainLocation", data.AnswerOrEmptyString("GenericJointPainLocation"), new { @id = Model.TypeName + "_GenericJointPainLocation", @class = "loc", @maxlength = "24", @title = "(Optional) Musculoskeletal, Joint Pain Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Musculoskeletal, Poor Balance' id='{0}_GenericMusculoskeletal10' class='radio float-left' name='{0}_GenericMusculoskeletal' value='10' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("10").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericMusculoskeletal10" class="radio">Poor Balance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Musculoskeletal, Joint Stiffness' id='{0}_GenericMusculoskeletal11' class='radio float-left' name='{0}_GenericMusculoskeletal' value='11' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("11").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericMusculoskeletal11" class="radio">Joint Stiffness</label>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Amputation' id='{0}_GenericMusculoskeletal12' class='radio float-left' name='{0}_GenericMusculoskeletal' value='12' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("12").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal12" class="radio">Amputation:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal12More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericAmputationLocation", data.AnswerOrEmptyString("GenericAmputationLocation"), new { @id = Model.TypeName + "_GenericAmputationLocation", @class = "loc", @maxlength = "24", @title = "(Optional) Musculoskeletal, Amputation Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Musculoskeletal, Weight Bearing Restrictions' id='{0}_GenericMusculoskeletal13' class='radio float-left' name='{0}_GenericMusculoskeletal' value='13' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("13").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal13" class="radio">Weight Bearing Restrictions:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericMusculoskeletal13More" class="rel float-right">
                            <div class="float-right">
                                <div class="float-left">
                                    <%= Html.RadioButton(Model.TypeName + "_GenericWeightBearingRestriction", "0", data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("0"), new { @id = Model.TypeName + "_GenericWeightBearingRestriction0", @class = "deselectable", @title = "(Optional) Musculoskeletal, Full Weight Bearing Restrictions" })%>
                                    <label for="<%= Model.TypeName %>_GenericWeightBearingRestriction0">Full</label>
                                </div>
                                <div class="float-left">
                                    <%= Html.RadioButton(Model.TypeName + "_GenericWeightBearingRestriction", "1", data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("1"), new { @id = Model.TypeName + "_GenericWeightBearingRestriction1", @class = "deselectable", @title = "(Optional) Musculoskeletal, Partial Weight Bearing Restrictions" })%>
                                    <label for="<%= Model.TypeName %>_GenericWeightBearingRestriction">Partial</label>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <em>(location)</em>
                                <%= Html.TextBox(Model.TypeName + "_GenericWeightBearingRestrictionLocation", data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation"), new { @id = Model.TypeName + "_GenericWeightBearingRestrictionLocation", @class = "loc", @maxlength = "24", @title = "(Optional) Musculoskeletal, Weight Bearing Restrictions Location" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMusculoskeletalComments" class="float-left">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericMusculoskeletalComments", data.AnswerOrEmptyString("GenericMusculoskeletalComments"), 5, 70, new { @id = Model.TypeName + "_GenericMusculoskeletalComments", @title = "(Optional) Musculoskeletal, Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Grooming</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1800">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1800" class="green" onclick="Oasis.ToolTip('M1800')">(M1800)</a>
                    Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1800Grooming", "", new { @id = Model.TypeName + "_M1800GroomingHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1800Grooming", "00", data.AnswerOrEmptyString("M1800Grooming").Equals("00"), new { @id = Model.TypeName + "_M1800Grooming0", @title = "(OASIS M1800) Grooming, Able to Groom Self Unaided" })%>
                        <label for="<%= Model.TypeName %>_M1800Grooming0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to groom self unaided, with or without the use of assistive devices or adapted methods.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1800Grooming", "01", data.AnswerOrEmptyString("M1800Grooming").Equals("01"), new { @id = Model.TypeName + "_M1800Grooming1", @title = "(OASIS M1800) Grooming, Utensils Must be Placed within Reach" })%>
                        <label for="<%= Model.TypeName %>_M1800Grooming1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Grooming utensils must be placed within reach before able to complete grooming activities.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1800Grooming", "02", data.AnswerOrEmptyString("M1800Grooming").Equals("02"), new { @id = Model.TypeName + "_M1800Grooming2", @title = "(OASIS M1800) Grooming, Someone Must Assist" })%>
                        <label for="<%= Model.TypeName %>_M1800Grooming2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Someone must assist the patient to groom self.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1800Grooming", "03", data.AnswerOrEmptyString("M1800Grooming").Equals("03"), new { @id = Model.TypeName + "_M1800Grooming3", @title = "(OASIS M1800) Grooming, Depends Entirely upon Someone Else" })%>
                        <label for="<%= Model.TypeName %>_M1800Grooming3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient depends entirely upon someone else for grooming needs.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1800')" title="More Information about M1800">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum != "11" && Model.AssessmentTypeNum != "14") { %>
    <fieldset class="oasis">
        <legend>Dressing Ability</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1810">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1810" class="green" onclick="Oasis.ToolTip('M1810')">(M1810)</a>
                    Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "", new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpperHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "00", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("00"), new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpper0", @title = "(OASIS M1810) Ability to Dress Upper Body, Without Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1810CurrentAbilityToDressUpper0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "01", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("01"), new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpper1", @title = "(OASIS M1810) Ability to Dress Upper Body, Clothing must be Laid out or Handed to" })%>
                        <label for="<%= Model.TypeName %>_M1810CurrentAbilityToDressUpper1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "02", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("02"), new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpper2", @title = "(OASIS M1810) Ability to Dress Upper Body, Help the Patient Put on Clothes" })%>
                        <label for="<%= Model.TypeName %>_M1810CurrentAbilityToDressUpper2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Someone must help the patient put on upper body clothing.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "03", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("03"), new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpper3", @title = "(OASIS M1810) Ability to Dress Upper Body, Depends Entirely upon Another Person" })%>
                        <label for="<%= Model.TypeName %>_M1810CurrentAbilityToDressUpper3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient depends entirely upon another person to dress the upper body.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1810')" title="More Information about M1810">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1820">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1820" class="green" onclick="Oasis.ToolTip('M1820')">(M1820)</a>
                    Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1820CurrentAbilityToDressLower", "", new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLowerHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1820CurrentAbilityToDressLower", "00", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("00"), new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLower0", @title = "(OASIS M1820) Ability to Dress Lower Body, Without Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1820CurrentAbilityToDressLower0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to obtain, put on, and remove clothing and shoes without assistance.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1820CurrentAbilityToDressLower", "01", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("01"), new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLower1", @title = "(OASIS M1820) Ability to Dress Lower Body, Clothing must be Laid out or Handed to" })%>
                        <label for="<%= Model.TypeName %>_M1820CurrentAbilityToDressLower1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1820CurrentAbilityToDressLower", "02", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("02"), new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLower2", @title = "(OASIS M1820) Ability to Dress Lower Body, Help the Patient Put on Clothes" })%>
                        <label for="<%= Model.TypeName %>_M1820CurrentAbilityToDressLower2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1820CurrentAbilityToDressLower", "03", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("03"), new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLower3", @title = "(OASIS M1820) Ability to Dress Lower Body, Depends Entirely upon Another Person" })%>
                        <label for="<%= Model.TypeName %>_M1820CurrentAbilityToDressLower3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient depends entirely upon another person to dress lower body.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1820')" title="More Information about M1820">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Bath/Toilet</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1830">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1830" class="green" onclick="Oasis.ToolTip('M1830')">(M1830)</a>
                    Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "", new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBodyHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "00", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("00"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody0", @title = "(OASIS M1830) Bathing, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "01", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("01"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody1", @title = "(OASIS M1830) Bathing, With the use of Devices" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "02", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("02"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody2", @title = "(OASIS M1830) Bathing, Intermittent Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">
                                Able to bathe in shower or tub with the intermittent assistance of another person:
                                <ul>
                                    <li>
                                        <span class="float-left">(a)</span>
                                        <span class="radio">for intermittent supervision or encouragement or reminders, OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(b)</span>
                                        <span class="radio">to get in and out of the shower or tub, OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(c)</span>
                                        <span class="radio">for washing difficult to reach areas.</span>
                                    </li>
                                </ul>
                            </span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "03", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("03"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody3", @title = "(OASIS M1830) Bathing, Requires Presence of Another Person" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "04", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("04"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody4", @title = "(OASIS M1830) Bathing, Independent at Sink/Chair/Commode" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "05", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("05"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody5", @title = "(OASIS M1830) Bathing, Able to Participate at Sink/Chair/Commode" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "06", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("06"), new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody6", @title = "(OASIS M1830) Bathing, Total Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1830CurrentAbilityToBatheEntireBody6">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">Unable to participate effectively in bathing and is bathed totally by another person.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1830')" title="More Information about M1830">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1840">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1840" class="green" onclick="Oasis.ToolTip('M1840')">(M1840)</a>
                    Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1840ToiletTransferring", "", new { @id = Model.TypeName + "_M1840ToiletTransferringHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1840ToiletTransferring", "00", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("00"), new { @id = Model.TypeName + "_M1840ToiletTransferring0", @title = "(OASIS M1840) Toilet Transfer, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1840ToiletTransferring0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to get to and from the toilet and transfer independently with or without a device.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1840ToiletTransferring", "01", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("01"), new { @id = Model.TypeName + "_M1840ToiletTransferring1", @title = "(OASIS M1840) Toilet Transfer, Needs Reminder/Assistance/Supervision" })%>
                        <label for="<%= Model.TypeName %>_M1840ToiletTransferring1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1840ToiletTransferring", "02", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("02"), new { @id = Model.TypeName + "_M1840ToiletTransferring2", @title = "(OASIS M1840) Toilet Transfer, Bedside Commode" })%>
                        <label for="<%= Model.TypeName %>_M1840ToiletTransferring2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1840ToiletTransferring", "03", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("03"), new { @id = Model.TypeName + "_M1840ToiletTransferring3", @title = "(OASIS M1840) Toilet Transfer, Bedpan/Urinal" })%>
                        <label for="<%= Model.TypeName %>_M1840ToiletTransferring3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1840ToiletTransferring", "04", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("04"), new { @id = Model.TypeName + "_M1840ToiletTransferring4", @title = "(OASIS M1840) Toilet Transfer, Totally Dependent" })%>
                        <label for="<%= Model.TypeName %>_M1840ToiletTransferring4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Is totally dependent in toileting.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1840')" title="More Information about M1840">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M1845">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1845" class="green" onclick="Oasis.ToolTip('M1845')">(M1845)</a>
                    Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1845ToiletingHygiene", "", new { @id = Model.TypeName + "_M1845ToiletingHygieneHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1845ToiletingHygiene", "00", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("00"), new { @id = Model.TypeName + "_M1845ToiletingHygiene0", @title = "(OASIS M1845) Toileting Hygiene, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1845ToiletingHygiene0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to manage toileting hygiene and clothing management without assistance.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1845ToiletingHygiene", "01", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("01"), new { @id = Model.TypeName + "_M1845ToiletingHygiene1", @title = "(OASIS M1845) Toileting Hygiene, Supplies/Implements must be Laid Out" })%>
                        <label for="<%= Model.TypeName %>_M1845ToiletingHygiene1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1845ToiletingHygiene", "02", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("02"), new { @id = Model.TypeName + "_M1845ToiletingHygiene2", @title = "(OASIS M1845) Toileting Hygiene, Needs Help" })%>
                        <label for="<%= Model.TypeName %>_M1845ToiletingHygiene2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Someone must help the patient to maintain toileting hygiene and/or adjust clothing.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1845ToiletingHygiene", "03", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("03"), new { @id = Model.TypeName + "_M1845ToiletingHygiene3", @title = "(OASIS M1845) Toileting Hygiene, Totally Dependent" })%>
                        <label for="<%= Model.TypeName %>_M1845ToiletingHygiene3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient depends entirely upon another person to maintain toileting hygiene.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1845')" title="More Information about M1845">?</div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Transferring</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1850">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1850" class="green" onclick="Oasis.ToolTip('M1850')">(M1850)</a>
                    Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1850Transferring", "", new { @id = Model.TypeName + "_M1850TransferringHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "00", data.AnswerOrEmptyString("M1850Transferring").Equals("00"), new { @id = Model.TypeName + "_M1850Transferring0", @title = "(OASIS M1850) Transferring, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to independently transfer.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "01", data.AnswerOrEmptyString("M1850Transferring").Equals("01"), new { @id = Model.TypeName + "_M1850Transferring1", @title = "(OASIS M1850) Transferring, Minimal Sssistance" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to transfer with minimal human assistance or with use of an assistive device.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "02", data.AnswerOrEmptyString("M1850Transferring").Equals("02"), new { @id = Model.TypeName + "_M1850Transferring2", @title = "(OASIS M1850) Transferring, Unable to Transfer Self" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Able to bear weight and pivot during the transfer process but unable to transfer self.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "03", data.AnswerOrEmptyString("M1850Transferring").Equals("03"), new { @id = Model.TypeName + "_M1850Transferring3", @title = "(OASIS M1850) Transferring, Unable to Bear Weight or Pivot" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "04", data.AnswerOrEmptyString("M1850Transferring").Equals("04"), new { @id = Model.TypeName + "_M1850Transferring4", @title = "(OASIS M1850) Transferring, Able to Turn and Position Self in Bed" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Bedfast, unable to transfer but is able to turn and position self in bed.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1850Transferring", "05", data.AnswerOrEmptyString("M1850Transferring").Equals("05"), new { @id = Model.TypeName + "_M1850Transferring5", @title = "(OASIS M1850) Transferring, Bedfast" })%>
                        <label for="<%= Model.TypeName %>_M1850Transferring5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Bedfast, unable to transfer and is unable to turn and position self.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1850')" title="More Information about M1850">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Ambulation/Locomotion</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1860">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1860" class="green" onclick="Oasis.ToolTip('M1860')">(M1860)</a>
                    Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1860AmbulationLocomotion", "", new { @id = Model.TypeName + "_M1860AmbulationLocomotionHidden" })%>
                    <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "00", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("00"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion0", @title = "(OASIS M1860) Ambulation/Locomotion, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "01", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("01"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion1", @title = "(OASIS M1860) Ambulation/Locomotion, Requires use of a One-handed Device" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "02", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("02"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion2", @title = "(OASIS M1860) Ambulation/Locomotion, Requires use of a Two-handed Device" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "03", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("03"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion3", @title = "(OASIS M1860) Ambulation/Locomotion, Walk with Supervision or Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Able to walk only with the supervision or assistance of another person at all times.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "04", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("04"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion4", @title = "(OASIS M1860) Ambulation/Locomotion, Wheel Self Independently" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Chairfast, unable to ambulate but is able to wheel self independently.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "05", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("05"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion5", @title = "(OASIS M1860) Ambulation/Locomotion, Chairfast" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Chairfast, unable to ambulate and is unable to wheel self.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1860AmbulationLocomotion", "06", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("06"), new { @id = Model.TypeName + "_M1860AmbulationLocomotion6", @title = "(OASIS M1860) Ambulation/Locomotion, Bedfast" })%>
                        <label for="<%= Model.TypeName %>_M1860AmbulationLocomotion6">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">Bedfast, unable to ambulate or be up in a chair.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1860')" title="More Information about M1860">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Feeding/Eating</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1870">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1870" class="green" onclick="Oasis.ToolTip('M1870')">(M1870)</a>
                    Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1870FeedingOrEating", "", new { @id = Model.TypeName + "_M1870FeedingOrEatingHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "00", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("00"), new { @id = Model.TypeName + "_M1870FeedingOrEating0", @title = "(OASIS M1870) Eating, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to independently feed self.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "01", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("01"), new { @id = Model.TypeName + "_M1870FeedingOrEating1", @title = "(OASIS M1870) Eating, Intermittent Assistance or Supervision" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">
                                Able to feed self independently but requires:
                                <ul>
                                    <li>
                                        <span class="float-left">(a)</span>
                                        <span class="radio">meal set-up; OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(b)</span>
                                        <span class="radio">intermittent assistance or supervision from another person; OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(c)</span>
                                        <span class="radio">a liquid, pureed or ground meat diet.</span>
                                    </li>
                                </ul>
                            </span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "02", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("02"), new { @id = Model.TypeName + "_M1870FeedingOrEating2", @title = "(OASIS M1870) Eating, Must be Assisted/Supervised" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Unable to feed self and must be assisted or supervised throughout the meal/snack.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "03", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("03"), new { @id = Model.TypeName + "_M1870FeedingOrEating3", @title = "(OASIS M1870) Eating, Orally and Nasogastric Tube/Gastrostomy" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "04", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("04"), new { @id = Model.TypeName + "_M1870FeedingOrEating4", @title = "(OASIS M1870) Eating, Nasogastric Tube/Gastrostomy" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1870FeedingOrEating", "05", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("05"), new { @id = Model.TypeName + "_M1870FeedingOrEating5", @title = "(OASIS M1870) Eating, Unable to Take in Nutrients Orally or by Tube" })%>
                        <label for="<%= Model.TypeName %>_M1870FeedingOrEating5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Unable to take in nutrients orally or by tube feeding.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1870')" title="More Information about M1870">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1880">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1880" class="green" onclick="Oasis.ToolTip('M1880')">(M1880)</a>
                    Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "", new { @id = Model.TypeName + "_M1880AbilityToPrepareLightMealHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "00", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("00"), new { @id = Model.TypeName + "_M1880AbilityToPrepareLightMeal0", @title = "(OASIS M1880) Plan/Prepare Light Meals, Independent" })%>
                        <label for="<%= Model.TypeName %>_M1880AbilityToPrepareLightMeal0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">
                                <ul>
                                    <li>
                                        <span class="float-left">(a)</span>
                                        <span class="radio">Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(b)</span>
                                        <span class="radio">Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</span>
                                    </li>
                                </ul>
                            </span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "01", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("01"), new { @id = Model.TypeName + "_M1880AbilityToPrepareLightMeal1", @title = "(OASIS M1880) Plan/Prepare Light Meals, Unable to Prepare Light Meals" })%>
                        <label for="<%= Model.TypeName %>_M1880AbilityToPrepareLightMeal1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "02", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("02"), new { @id = Model.TypeName + "_M1880AbilityToPrepareLightMeal2", @title = "(OASIS M1880) Plan/Prepare Light Meals, Unable to Prepare Light Meals or Reheat Delivered Meals" })%>
                        <label for="<%= Model.TypeName %>_M1880AbilityToPrepareLightMeal2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Unable to prepare any light meals or reheat any delivered meals.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1880')" title="More Information about M1880">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Telephone Ability</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1890">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1890" class="green" onclick="Oasis.ToolTip('M1890')">(M1890)</a>
                    Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1890AbilityToUseTelephone", "", new { @id = Model.TypeName + "_M1890AbilityToUseTelephoneHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "00", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("00"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone0", @title = "(OASIS M1890) Telephone, Able to Dial and Answer as Desired" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to dial numbers and answer calls appropriately and as desired.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "01", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("01"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone1", @title = "(OASIS M1890) Telephone, Able to Use Specially Adapted Telephone/Call Essential Numbers" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "02", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("02"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone2", @title = "(OASIS M1890) Telephone, Able to Answer but Difficulty with Placing Calls" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "03", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("03"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone3", @title = "(OASIS M1890) Telephone, Able to Answer Sometimes/Limited Conversation." })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Able to answer the telephone only some of the time or is able to carry on only a limited conversation.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "04", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("04"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone4", @title = "(OASIS M1890) Telephone, Unable to Answer but Can Listen if Assisted" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unable to answer the telephone at all but can listen if assisted with equipment.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "05", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("05"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone5", @title = "(OASIS M1890) Telephone, Unable to use Telephone" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Totally unable to use the telephone.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1890AbilityToUseTelephone", "NA", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("NA"), new { @id = Model.TypeName + "_M1890AbilityToUseTelephone6", @title = "(OASIS M1890) Telephone, Patient does not have a Telephone" })%>
                        <label for="<%= Model.TypeName %>_M1890AbilityToUseTelephone6">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient does not have a telephone.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1890')" title="More Information about M1890">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Adl.ascx", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1900">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1900" class="green" onclick="Oasis.ToolTip('M1900')">(M1900)</a>
                    Prior Functioning ADL/IADL: Indicate the patient&#8217;s usual ability with everyday activities prior to this current illness, exacerbation, or injury. Check only one box in each row.
                </label>
                <table class="form align-center">
                    <thead>
                        <tr>
                            <th colspan="2">Functional Area</th>
                            <th>Independent</th>
                            <th>Needed Some Help</th>
                            <th>Dependent</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup padfix">
                        <tr>
                            <td colspan="2" class="align-left strong">
                                a. Self-Care (e.g., grooming, dressing, and bathing)
                                <%= Html.Hidden(Model.TypeName + "_M1900SelfCareFunctioning", "", new { @id = Model.TypeName + "_M1900SelfCareFunctioningHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900SelfCareFunctioning", "00", data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("00"), new { @id = Model.TypeName + "_M1900SelfCareFunctioning0", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Self-Care Independent" })%>
                                    <label for="<%= Model.TypeName %>_M1900SelfCareFunctioning0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900SelfCareFunctioning", "01", data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("01"), new { @id = Model.TypeName + "_M1900SelfCareFunctioning1", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Self-Care Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M1900SelfCareFunctioning1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900SelfCareFunctioning", "02", data.AnswerOrEmptyString("M1900SelfCareFunctioning").Equals("02"), new { @id = Model.TypeName + "_M1900SelfCareFunctioning2", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Self-Care Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M1900SelfCareFunctioning2" class="radio">2</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="align-left strong">
                                b. Ambulation
                                <%= Html.Hidden(Model.TypeName + "_M1900Ambulation", "", new { @id = Model.TypeName + "_M1900AmbulationHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Ambulation", "00", data.AnswerOrEmptyString("M1900Ambulation").Equals("00"), new { @id = Model.TypeName + "_M1900Ambulation0", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Ambulation Independent" })%>
                                    <label for="<%= Model.TypeName %>_M1900Ambulation0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Ambulation", "01", data.AnswerOrEmptyString("M1900Ambulation").Equals("01"), new { @id = Model.TypeName + "_M1900Ambulation1", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Ambulation Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M1900Ambulation1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Ambulation", "02", data.AnswerOrEmptyString("M1900Ambulation").Equals("02"), new { @id = Model.TypeName + "_M1900Ambulation2", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Ambulation Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M1900Ambulation2" class="radio">2</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="align-left strong">
                                c. Transfer
                                <%= Html.Hidden(Model.TypeName + "_M1900Transfer", "", new { @id = Model.TypeName + "_M1900TransferHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Transfer", "00", data.AnswerOrEmptyString("M1900Transfer").Equals("00"), new { @id = Model.TypeName + "_M1900Transfer0", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Transfer Independent" })%>
                                    <label for="<%= Model.TypeName %>_M1900Transfer0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Transfer", "01", data.AnswerOrEmptyString("M1900Transfer").Equals("01"), new { @id = Model.TypeName + "_M1900Transfer1", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Transfer Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M1900Transfer1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900Transfer", "02", data.AnswerOrEmptyString("M1900Transfer").Equals("02"), new { @id = Model.TypeName + "_M1900Transfer2", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Transfer Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M1900Transfer2" class="radio">2</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="align-left strong">
                                d. Household tasks (e.g., light meal preparation, laundry, shopping)
                                <%= Html.Hidden(Model.TypeName + "_M1900HouseHoldTasks", "", new { @id = Model.TypeName + "_M1900HouseHoldTasksHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900HouseHoldTasks", "00", data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("00"), new { @id = Model.TypeName + "_M1900HouseHoldTasks0", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Household Tasks Independent" })%>
                                    <label for="<%= Model.TypeName %>_M1900HouseHoldTasks0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900HouseHoldTasks", "01", data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("01"), new { @id = Model.TypeName + "_M1900HouseHoldTasks1", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Household Tasks Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M1900HouseHoldTasks1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M1900HouseHoldTasks", "02", data.AnswerOrEmptyString("M1900HouseHoldTasks").Equals("02"), new { @id = Model.TypeName + "_M1900HouseHoldTasks2", @title = "(OASIS M1900) Prior Functioning ADL/IADL, Household Tasks Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M1900HouseHoldTasks2" class="radio">2</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1900')" title="More Information about M1900">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Fall Assessment</legend>
        <div id="<%= Model.TypeName %>_FallAssessment" class="wide-column">
            <div class="row">
                <em>One point is assessed for each one selected</em>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericAge65Plus", "", new { @id = Model.TypeName + "_GenericAge65PlusHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Age 65 and Over' id='{0}_GenericAge65Plus' name='{0}_GenericAge65Plus' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericAge65Plus").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericAge65Plus" class="radio">
                            <strong>Age 65+</strong>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericHypotensionDiagnosis", "", new { @id = Model.TypeName + "_GenericHypotensionDiagnosisHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, 3 or more Diagnoses' id='{0}_GenericHypotensionDiagnosis' name='{0}_GenericHypotensionDiagnosis' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericHypotensionDiagnosis").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericHypotensionDiagnosis" class="radio">
                            <strong>Diagnosis (3 or more co-existing)</strong><br />
                            <em>Assess for hypotension.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericPriorFalls", "", new { @id = Model.TypeName + "_GenericPriorFallsHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Prior history of falls' id='{0}_GenericPriorFalls' name='{0}_GenericPriorFalls' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericPriorFalls").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPriorFalls" class="radio">
                            <strong>Prior history of falls within 3 months</strong><br />
                            <em>Fall definition: &#8220;An unintentional change in position resulting in coming to rest on the ground or at a lower level.&#8221;</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericFallIncontinence", "", new { @id = Model.TypeName + "_GenericFallIncontinenceHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Incontinence' id='{0}_GenericFallIncontinence' name='{0}_GenericFallIncontinence' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericFallIncontinence").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericFallIncontinence" class="radio">
                            <strong>Incontinence</strong><br />
                            <em>Inability to make it to the bathroom or commode in timely manner. Includes frequency, urgency, and/or nocturia.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericVisualImpairment", "", new { @id = Model.TypeName + "_GenericVisualImpairmentHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Visual impairment' id='{0}_GenericVisualImpairment' name='{0}_GenericVisualImpairment' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericVisualImpairment").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericVisualImpairment" class="radio">
                            <strong>Visual impairment</strong><br />
                            <em>Includes macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericImpairedFunctionalMobility", "", new { @id = Model.TypeName + "_GenericImpairedFunctionalMobilityHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Impaired functional mobility' id='{0}_GenericImpairedFunctionalMobility' name='{0}_GenericImpairedFunctionalMobility' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericImpairedFunctionalMobility").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericImpairedFunctionalMobility" class="radio">
                            <strong>Impaired functional mobility</strong><br />
                            <em>May include patients who need help with IADL’s or ADL’s or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericEnvHazards", "", new { @id = Model.TypeName + "_GenericEnvHazardsHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Environmental hazards' id='{0}_GenericEnvHazards' name='{0}_GenericEnvHazards' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericEnvHazards").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEnvHazards" class="radio">
                            <strong>Environmental hazards</strong><br />
                            <em>May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericPolyPharmacy", "", new { @id = Model.TypeName + "_GenericPolyPharmacyHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Poly Pharmacy' id='{0}_GenericPolyPharmacy' name='{0}_GenericPolyPharmacy' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericPolyPharmacy").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPolyPharmacy" class="radio">
                            <strong>Poly Pharmacy (4 or more prescriptions)</strong><br />
                            <em>Drugs highly associated with fall risk include but are not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericPainAffectingFunction", "", new { @id = Model.TypeName + "_GenericPainAffectingFunctionHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Pain affecting level of function' id='{0}_GenericPainAffectingFunction' name='{0}_GenericPainAffectingFunction' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericPainAffectingFunction").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPainAffectingFunction" class="radio">
                            <strong>Pain affecting level of function</strong><br />
                            <em>Pain often affects an individual’s desire or ability to move or pain can be a factor in depression or compliance with safety recommendations.</em>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_GenericCognitiveImpairment", "", new { @id = Model.TypeName + "_GenericCognitiveImpairmentHidden" })%>
                        <%= string.Format("<input title='(Optional) Fall Assessment, Cognitive impairment' id='{0}_GenericCognitiveImpairment' name='{0}_GenericCognitiveImpairment' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericCognitiveImpairment").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericCognitiveImpairment" class="radio">
                            <strong>Cognitive impairment</strong><br />
                            <em>Could include patients with dementia, Alzheimer’s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patient’s ability to adhere to the plan of care.</em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="float-right">
                    <h5 class="fixed">Total:</h5>
                    <%= Html.TextBox(Model.TypeName + "_485FallAssessmentScore", data.AnswerOrEmptyString("485FallAssessmentScore"), new { @id = Model.TypeName + "_485FallAssessmentScore", @class="sn", @size = "10", @maxlength = "10" })%>
                    <em>A score of 4 or more is considered at risk for falling</em>
                </div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
    <fieldset id="<%= Model.TypeName %>_Tinetti">
        <legend>Tinetti Assessment</legend>    
        <div class="column">
            <div class="row">
                <label class="strong">Balance Test</label>
                <p>With the patient seated in a hard, armless chair, test on the following maneuvers:</p>
            </div>
            <div class="row">
                <label class="strong">1. Sitting balance</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSitting", "0", data.AnswerOrEmptyString("GenericTinettiSitting").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiSitting0", @title = "(Optional) Tinetti Assessment, Sitting Balance, Leans/Slides in Chair", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSitting0">Leans or slides in chair</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSitting", "1", data.AnswerOrEmptyString("GenericTinettiSitting").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiSitting1", @title = "(Optional) Tinetti Assessment, Sitting Balance, Steady, safe", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSitting1">Steady, safe</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">2. Arises</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiArises", "0", data.AnswerOrEmptyString("GenericTinettiArises").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiArises0", @title = "(Optional) Tinetti Assessment, Arises, Unable without help", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiArises0">Unable without Help</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiArises", "1", data.AnswerOrEmptyString("GenericTinettiArises").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiArises1", @title = "(Optional) Tinetti Assessment, Arises, Able, uses arms to help", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiArises1">Able, uses arms to help</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiArises", "2", data.AnswerOrEmptyString("GenericTinettiArises").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiArises2", @title = "(Optional) Tinetti Assessment, Arises, Able without using arms", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiArises2">Able without using arms</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">3. Attempts to arise</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiAttemptsToArise", "0", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiAttemptsToArise0", @title = "(Optional) Tinetti Assessment, Able to Arise, Unable without help", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiAttemptsToArise0">Unable without Help</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiAttemptsToArise", "1", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiAttemptsToArise1", @title = "(Optional) Tinetti Assessment, Able to Arise, Able, requires greater than 1 attempt", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiAttemptsToArise1">Able, requires greater than 1 attempt</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiAttemptsToArise", "2", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiAttemptsToArise2", @title = "(Optional) Tinetti Assessment, Able to Arise, Able to rise, 1 attempt", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiAttemptsToArise2">Able to rise, 1 attempt</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">4. Immediate standing balance (first 5 seconds)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiImmediateStanding", "0", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiImmediateStanding0", @title = "(Optional) Tinetti Assessment, Immediate Standing Balance, Unsteady (swaggers, moves feet, trunk sway)", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiImmediateStanding0">Unsteady (swaggers, moves feet, trunk sway)</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiImmediateStanding", "1", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiImmediateStanding1", @title = "(Optional) Tinetti Assessment, Immediate Standing Balance, Steady but uses walker or other support", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiImmediateStanding1">Steady but uses walker or other support</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiImmediateStanding", "2", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiImmediateStanding2", @title = "(Optional) Tinetti Assessment, Immediate Standing Balance, Steady without walker or other support", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiImmediateStanding2">Steady without walker or other support</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">5. Standing balance</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiStanding", "0", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiStanding0", @title = "(Optional) Tinetti Assessment, Standing Balance, Unsteady", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiStanding0">Unsteady</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiStanding", "1", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiStanding1", @title = "(Optional) Tinetti Assessment, Standing Balance, Steady but wide stance", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiStanding1">Steady but wide stance (medial heels greater than 4 inches apart) and uses cane or other support</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiStanding", "2", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiStanding2", @title = "(Optional) Tinetti Assessment, Standing Balance, Narrow stance without support", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiStanding2">Narrow stance without support</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">6. Nudged (subject at max position with feet as close together as possible, examiner pushes lightly on subject&#8217;s sternum with palm of hand 3 times)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiNudged", "0", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiNudged0", @title = "(Optional) Tinetti Assessment, Nudged, Begins to fall", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiNudged0">Begins to fall</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiNudged", "1", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiNudged1", @title = "(Optional) Tinetti Assessment, Nudged, Staggers, grabs, catches self", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiNudged1">Staggers, grabs, catches self</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiNudged", "2", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiNudged2", @title = "(Optional) Tinetti Assessment, Nudged, Steady", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiNudged2">Steady</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">7. Eyes closed (at maximum position #6)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiEyesClosed", "0", data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiEyesClosed0", @title = "(Optional) Tinetti Assessment, Eyes Closed, Unsteady", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiEyesClosed0">Unsteady</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiEyesClosed", "1", data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiEyesClosed1", @title = "(Optional) Tinetti Assessment, Eyes Closed, Steady", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiEyesClosed1">Steady</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">8. Turning 360 degrees</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTurningContinuous", "0", data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiTurningContinuous0", @title = "(Optional) Tinetti Assessment, Turning, Discontinuous steps", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTurningContinuous0">Discontinuous steps</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTurningContinuous", "1", data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiTurningContinuous1", @title = "(Optional) Tinetti Assessment, Turning, Continuous steps", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTurningContinuous1">Continuous steps</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTurningSteady", "0", data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("00"), new { @id = Model.TypeName + "_GenericTinettiTurningSteady0", @title = "(Optional) Tinetti Assessment, Turning, Unsteady (grabs, swaggers)", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTurningSteady0">Unsteady (grabs, swaggers)</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTurningSteady", "1", data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("01"), new { @id = Model.TypeName + "_GenericTinettiTurningSteady1", @title = "(Optional) Tinetti Assessment, Turning, Steady", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTurningSteady1">Steady</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">9. Sitting down</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSittingDown", "0", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiSittingDown0", @title = "(Optional) Tinetti Assessment, Sitting Down, Unsafe", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSittingDown0">Unsafe (misjudged distance, falls into chair)</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSittingDown", "1", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiSittingDown1", @title = "(Optional) Tinetti Assessment, Sitting Down, Uses arms or not a smooth motion", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSittingDown1">Uses arms or not a smooth motion</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSittingDown", "2", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiSittingDown2", @title = "(Optional) Tinetti Assessment, Sitting Down, Safe, smooth motion", @class = "TinettiBalance" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSittingDown2">Safe, smooth motion</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="float-right">
                    <label class="strong">Balance Total:</label>
                    <input type="text" name="<%= Model.TypeName %>_TinettiBalanceTotal" value="" class="sn" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="strong">Gait Test</label>
                <p>Patient stands, walks down hallway or across the room, first at &#8220;unusual&#8221; pace, then back at &#8220;rapid, but safe&#8221; pace (using usual walking aids):</p>
            </div>
            <div class="row">
                <label class="strong">10. Initiation of gait (immediately after told to &#8220;go&#8221;)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiGait", "0", data.AnswerOrEmptyString("GenericTinettiGait").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiGait0", @title = "(Optional) Tinetti Assessment, Initiation of Gait, Any hesitancy", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiGait0">Any hesitancy or multiple attempts to start</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiGait", "1", data.AnswerOrEmptyString("GenericTinettiGait").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiGait1", @title = "(Optional) Tinetti Assessment, Initiation of Gait, No hesitancy", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiGait1">No hesitancy</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">11. Step length and height</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiRightPassLeft", "0", data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiRightPassLeft0", @title = "(Optional) Tinetti Assessment, Step Length/Height, Right swing foot does not pass left stance foot", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiRightPassLeft0">a. Right swing foot does not pass left stance foot with step</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiRightPassLeft", "1", data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiRightPassLeft1", @title = "(Optional) Tinetti Assessment, Step Length/Height, Right foot passes left stance foot", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiRightPassLeft1">b. Right foot passes left stance foot</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiRightClear", "0", data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiRightClear0", @title = "(Optional) Tinetti Assessment, Step Length/Height, Right foot does not clear floor", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiRightClear0">c. Right foot does not clear floor completely with step</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiRightClear", "1", data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiRightClear1", @title = "(Optional) Tinetti Assessment, Step Length/Height, Right foot completely clears floor", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiRightClear1">d. Right foot completely clears floor</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiLeftPassRight", "0", data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiLeftPassRight0", @title = "(Optional) Tinetti Assessment, Step Length/Height, Left swing foot does not pass right stance foot", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiLeftPassRight0">e. Left swing foot does not pass right stance foot with step</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiLeftPassRight", "1", data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiLeftPassRight1", @title = "(Optional) Tinetti Assessment, Step Length/Height, Left foot passes right stance foot", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiLeftPassRight1">f. Left foot passes right stance foot</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiLeftClear", "0", data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiLeftClear0", @title = "(Optional) Tinetti Assessment, Step Length/Height, Left foot does not clear floor", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiLeftClear0">g. Left foot does not clear floor completely with step</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiLeftClear", "1", data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiLeftClear1", @title = "(Optional) Tinetti Assessment, Step Length/Height, Left foot completely clears floor", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiLeftClear1">h. Left foot completely clears floor</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">12. Step symmetry</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSymmetry", "0", data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiSymmetry0", @title = "(Optional) Tinetti Assessment, Step Symmetry, Right and left step length not equal", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSymmetry0">Right and left step length not equal (estimate)</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiSymmetry", "1", data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiSymmetry1", @title = "(Optional) Tinetti Assessment, Step Symmetry, Right and left step appear equal", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiSymmetry1">Right and left step appear equal</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">13. Step continually</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiContinually", "0", data.AnswerOrEmptyString("GenericTinettiContinually").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiContinually0", @title = "(Optional) Tinetti Assessment, Step Continually, Stopping or discontinuity between steps", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiContinually0">Stopping or discontinuity between steps</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiContinually", "1", data.AnswerOrEmptyString("GenericTinettiContinually").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiContinually1", @title = "(Optional) Tinetti Assessment, Step Continually, Steps appear continuous", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiContinually1">Steps appear continuous</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">14. Path (estimated in relation to floor tiles, 12-inch diameter; observe excursion of 1 foot over about 10 feet of the course)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiPath", "0", data.AnswerOrEmptyString("GenericTinettiPath").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiPath0", @title = "(Optional) Tinetti Assessment, Path, Marked deviation", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiPath0">Marked deviation</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiPath", "1", data.AnswerOrEmptyString("GenericTinettiPath").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiPath1", @title = "(Optional) Tinetti Assessment, Path, Mild/moderate deviation", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiPath1">Mild/moderate deviation or uses walking aid</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiPath", "2", data.AnswerOrEmptyString("GenericTinettiPath").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiPath2", @title = "(Optional) Tinetti Assessment, Path, Straight", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiPath2">Straight without walking aid</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">15. Trunk</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTrunk", "0", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiTrunk0", @title = "(Optional) Tinetti Assessment, Trunk, Marked sway", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTrunk0">Marked sway or uses walking aid</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTrunk", "1", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiTrunk1", @title = "(Optional) Tinetti Assessment, Trunk, No sway but flexion of knees or back", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTrunk1">No sway but flexion of knees or back, or spreads arms out while walking</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiTrunk", "2", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("2"), new { @id = Model.TypeName + "_GenericTinettiTrunk2", @title = "(Optional) Tinetti Assessment, Trunk, No sway, no flexion", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiTrunk2">No sway, no flexion, no use of arms, and no use of walking aid</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">16. Walking stance</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiWalking", "0", data.AnswerOrEmptyString("GenericTinettiWalking").Equals("0"), new { @id = Model.TypeName + "_GenericTinettiWalking0", @title = "(Optional) Tinetti Assessment, Walking Stance, Heels apart", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiWalking0">Heels apart</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_GenericTinettiWalking", "1", data.AnswerOrEmptyString("GenericTinettiWalking").Equals("1"), new { @id = Model.TypeName + "_GenericTinettiWalking1", @title = "(Optional) Tinetti Assessment, Walking Stance, Heels almost touching", @class = "TinettiGait" })%>
                        <label for="<%= Model.TypeName %>_GenericTinettiWalking1">Heels almost touching while walking</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="float-right">
                    <label class="strong">Gait Total:</label>
                    <input type="text" name="<%= Model.TypeName %>_TinettiGaitTotal" value="" class="sn" />
                </div>
            </div>
            <div class="row">
                <label class="strong">Key for Scoring:</label>
                <ul class="tinetti_score">
                    <li>Maximum score is 28</li>
                    <li>Score below 19 &#8220;High risk of falling&#8221;</li>
                    <li>Score in 19&#8211;24 range &#8220;Greater chance of falling&#8221; but is not at &#8220;High risk&#8221;</li>
                    <li>Score in 24&#8211;28 range &#8220;Low risk of falling&#8221;</li>
                </ul>
            </div>
            <div class="row">
                <div class="float-right">
                    <label class="strong">Total (Balance and Gait):</label>
                    <input type="text" name="<%= Model.TypeName %>_TinettiTotal" value="" class="sn" />
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Timed Up and Go</legend>
        <div class="wide-column">
            <div class="float-left">
                <label class="strong">Timed Up and Go Findings:</label>
                <%= Html.TextBox(Model.TypeName + "_GenericTimedUpAndFindings", data.AnswerOrEmptyString("GenericTimedUpAndFindings"), new { @id = Model.TypeName + "_GenericTimedUpAndFindings", @class = "sn", @size = "3", @maxlength = "3" })%>
                <em>&#60; 10 seconds= Normal, &#60; 14 seconds= not a falls risk, &#62; 14 seconds = increased risk for falls</em>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Fall Risk Assessment</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1910">
                <div class="strong">
                    <a href="javascript:void(0)" title="More Information about M1910" class="green" onclick="Oasis.ToolTip('M1910')">(M1910)</a>
                    Has this patient had a multi-factor Fall Risk Assessment (such as falls history, use of multiple medications, mental impairment, toileting frequency, general mobility/transferring impairment, environmental hazards)?
                </div>
                <%= Html.Hidden(Model.TypeName + "_M1910FallRiskAssessment", "", new { @id = Model.TypeName + "_M1910FallRiskAssessmentHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1910FallRiskAssessment", "00", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("00"), new { @id = Model.TypeName + "_M1910FallRiskAssessment0", @title = "(OASIS M1910) Fall Risk Assessment, No" })%>
                        <label for="<%= Model.TypeName %>_M1910FallRiskAssessment0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No multi-factor falls risk assessment conducted.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1910FallRiskAssessment", "01", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("01"), new { @id = Model.TypeName + "_M1910FallRiskAssessment1", @title = "(OASIS M1910) Fall Risk Assessment, Yes but Not at Risk" })%>
                        <label for="<%= Model.TypeName %>_M1910FallRiskAssessment1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, and it does not indicate a risk for falls.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1910FallRiskAssessment", "02", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("02"), new { @id = Model.TypeName + "_M1910FallRiskAssessment2", @title = "(OASIS M1910) Fall Risk Assessment, Yes and at Risk" })%>
                        <label for="<%= Model.TypeName %>_M1910FallRiskAssessment2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, and it indicates a risk for falls.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1910')" title="More Information about M1910">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Iadl.ascx", Model); %>
     <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485ActivitiesPermittedD"),
        $("#<%= Model.TypeName %>_485ActivitiesPermittedDMore"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal2"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal3"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletalImpairedMotorSkills"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal4"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal5"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletalMobility"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal6"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal6More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericAssistiveDevice5"),
        $("#<%= Model.TypeName %>_GenericAssistiveDeviceOther"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal7"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal7More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal9"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal9More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal12"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal12More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericMusculoskeletal13"),
        $("#<%= Model.TypeName %>_GenericMusculoskeletal13More"));
    Oasis.FallAssessment("<%= Model.TypeName %>");
    <%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
    Oasis.TinettiAssessment("<%= Model.TypeName %>");
    <%  } %>
<%  } %>
</script>