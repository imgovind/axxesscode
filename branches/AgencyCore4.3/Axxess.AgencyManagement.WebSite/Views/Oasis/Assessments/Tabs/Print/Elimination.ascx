<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var GU = data.AnswerArray("GenericGU"); %>
<%  var GUUrine = data.AnswerArray("GenericGUUrine"); %>
<%  var Dialysis = data.AnswerArray("GenericDialysis"); %>
<%  var DialysisHemodialysis = data.AnswerArray("GenericDialysisHemodialysis"); %>
<%  var Digestive = data.AnswerArray("GenericDigestive"); %>
<%  var DigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
<%  var DigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
<%  var DigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 6 || Model.AssessmentTypeNum % 10 > 8) { %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.checkbox("WNL (Within Normal Limits)",<%= GU.Contains("1").ToString().ToLower() %>,true) +
                printview.checkbox("Incontinence",<%= GU.Contains("2").ToString().ToLower() %>,true)) +
            printview.col(2,
                printview.checkbox("Bladder Distention",<%= GU.Contains("3").ToString().ToLower() %>,true) +
                printview.checkbox("Discharge",<%= GU.Contains("4").ToString().ToLower() %>,true)) +
            printview.col(2,
                printview.checkbox("Frequency",<%= GU.Contains("5").ToString().ToLower() %>,true) +
                printview.checkbox("Dysuria",<%= GU.Contains("6").ToString().ToLower() %>,true)) +
            printview.col(2,
                printview.checkbox("Retention",<%= GU.Contains("7").ToString().ToLower() %>,true) +
                printview.checkbox("Urgency",<%= GU.Contains("8").ToString().ToLower() %>,true)) +
            printview.col(2,
                printview.checkbox("Oliguria",<%= GU.Contains("9").ToString().ToLower() %>,true) +
                printview.checkbox("Catheter/Device:",<%= GU.Contains("10").ToString().ToLower() %>,true)) +
            printview.col(3,
                printview.span("<%= GU.Contains("10") ? (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("2") ? "Foley Catheter" : string.Empty) + (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("3") ? "Condom Catheter" : string.Empty) + (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("4") ? "Suprapubic Catheter" : string.Empty) + (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("5") ? "Urostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericGUCatheterList").Equals("6") ? "Other" : string.Empty) : string.Empty %>",false,1) +
                printview.span("Changed: <%= GU.Contains("10") ? data.AnswerOrEmptyString("GenericGUCatheterLastChanged").Clean() : string.Empty %>") +
                printview.span("<%= GU.Contains("10") ? data.AnswerOrEmptyString("GenericGUCatheterFrequency").Clean() + "Fr " + data.AnswerOrEmptyString("GenericGUCatheterAmount").Clean() + "cc" : string.Empty %>",false,1))) +
        printview.col(4,
            printview.checkbox("Urine:",<%= GU.Contains("11").ToString().ToLower() %>,true) +
            printview.checkbox("Clear",<%= (GU.Contains("11") && GUUrine.Contains("6")).ToString().ToLower() %>) +
            printview.checkbox("Cloudy",<%= (GU.Contains("11") && GUUrine.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("Odorous",<%= (GU.Contains("11") && GUUrine.Contains("2")).ToString().ToLower() %>) +
            printview.span("") +
            printview.checkbox("Sediment",<%= (GU.Contains("11") && GUUrine.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Hematuria",<%= (GU.Contains("11") && GUUrine.Contains("4")).ToString().ToLower() %>) +
            printview.checkbox("Other <%= GU.Contains("11") && GUUrine.Contains("5") ? data.AnswerOrEmptyString("GenericGUOtherText").Clean() : string.Empty %>",<%= (GU.Contains("11") && GUUrine.Contains("5")).ToString().ToLower() %>) +
            printview.checkbox("External Genitalia:",<%= GU.Contains("12").ToString().ToLower() %>,true) +
            printview.span("") +
            printview.checkbox("Normal",<%= (GU.Contains("12") && data.AnswerOrEmptyString("GenericGUNormal").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Abnormal",<%= (GU.Contains("12") && data.AnswerOrEmptyString("GenericGUNormal").Equals("0")).ToString().ToLower() %>) +
            printview.span("") +
            printview.span("As per:") +
            printview.checkbox("Clinician Assessment",<%= (GU.Contains("12") && data.AnswerOrEmptyString("GenericGUClinicalAssessment").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Pt/CG Report",<%= (GU.Contains("12") && data.AnswerOrEmptyString("GenericGUClinicalAssessment").Equals("0")).ToString().ToLower() %>)) +
        printview.checkbox("Nocturia",<%= GU.Contains("13").ToString().ToLower() %>,true),
        "GU");
    printview.addsection(
        printview.col(2,
            printview.span("Is patient on dialysis?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPatientOnDialysis").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPatientOnDialysis").Equals("0").ToString().ToLower() %>))) +
        printview.span("Dialysis Type",true)+
        printview.col(2,
            printview.checkbox("Peritoneal Dialysis",<%= Dialysis.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("CCPD (Continuous Cyclic Peritoneal Dialysis)",<%= Dialysis.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("IPD (Intermittent Peritoneal Dialysis)",<%= Dialysis.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("CAPD (Continuous Ambulatory Peritoneal Dialysis)",<%= Dialysis.Contains("4").ToString().ToLower() %>,true) +
            printview.checkbox("Hemodialysis",<%= Dialysis.Contains("5").ToString().ToLower() %>,true) +
            <%if(Dialysis.Contains("5")){ %>
            printview.col(2,
                printview.checkbox("AV Graft/ Fistula Site:",<%= (Dialysis.Contains("5") && DialysisHemodialysis.Contains("1")).ToString().ToLower() %>) +
                printview.span("<%= Dialysis.Contains("5") && DialysisHemodialysis.Contains("1") ? data.AnswerOrEmptyString("GenericDialysisHemodialysisGriftAV").Clean() : string.Empty %>",false,1) +
                printview.checkbox("Central Venous Catheter Site:",<%= (Dialysis.Contains("5") && DialysisHemodialysis.Contains("2")).ToString().ToLower() %>) +
                printview.span("<%= Dialysis.Contains("5") && DialysisHemodialysis.Contains("2") ? data.AnswerOrEmptyString("GenericDialysisHemodialysisCentralVenousAV").Clean() : string.Empty %>",false,1)) +
            <%}else{ %>
            printview.span("")+
            <%} %>
            printview.checkbox("Catheter site free from signs and symptoms of infection",<%= Dialysis.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("<strong>Other, specify</strong> <%= Dialysis.Contains("7") ? data.AnswerOrEmptyString("GenericDialysisOtherDesc").Clean() : string.Empty %>",<%= Dialysis.Contains("7").ToString().ToLower() %>)) +
        printview.span("Dialysis Center:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericDialysisCenter").Clean() %>",false,2) +
        
        "","Dialysis");
    <%  } %>
    <%  if (Model.AssessmentTypeNum != 11 || Model.AssessmentTypeNum != 14) { %>
        <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1600) " : string.Empty %>Has this patient been treated for a Urinary Tract Infection in the past 14 days?",true) +
        printview.col(4,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Prophylactic treatment",<%= data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("NA").ToString().ToLower() %>)+
            printview.checkbox("UK &#8211; Unknown",<%=data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("UK").ToString().ToLower() %>)),
        "OASIS M1600");
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1610) " : string.Empty %>Urinary Incontinence or Urinary Catheter Presence:",true) +
        printview.col(3,
        printview.checkbox("0 &#8211; No incontinence or catheter",<%= data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Patient is incontinent",<%= data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Patient requires a urinary catheter",<%= data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("02").ToString().ToLower() %>)),
    "OASIS M1610");
        <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1615) " : string.Empty %>When does Urinary Incontinence occur?",true) +
        printview.col(5,
            printview.checkbox("0 &#8211; Timed-voiding defers",<%= data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Occasional stress",<%= data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; During the night only",<%= data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; During the day only",<%= data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; During day and night",<%= data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("04").ToString().ToLower() %>)),
        "OASIS M1615");
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1620) " : string.Empty %>Bowel Incontinence Frequency:",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; Very rarely or never",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Less than once weekly",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; One to three times weekly",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Four to six times weekly",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; On a daily basis",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("04").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; More often than once daily",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("05").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Patient has ostomy for bowel elimination",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("NA").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("UK").ToString().ToLower() %>)),
        "OASIS M1620");
        <%  if (Model.AssessmentTypeNum % 10 < 6) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1630) " : string.Empty %>Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?",true) +
        printview.checkbox("0 &#8211; Patient does not have an ostomy for bowel elimination.",<%= data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Patient&#8217;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.",<%= data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.",<%= data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("02").ToString().ToLower() %>),
    "OASIS M1630");
        <%  } %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= Digestive.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Bowel Sounds:<%=Digestive.Contains("2")?(data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("1") ? "Present/WNL x4 quadrants" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("2") ? "Hyperactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("3") ? "Hypoactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("4") ? "Absent" : string.Empty) : string.Empty %>",<%= Digestive.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Abdominal Palpation:<%= Digestive.Contains("3") ? (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("1") ? "Soft/ WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("2") ? "Firm" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("3") ? "Tender" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("4") ? "Other" : string.Empty) : string.Empty %>",<%= Digestive.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Bowel Incontinence",<%= Digestive.Contains("4").ToString().ToLower() %>,true) +
            printview.checkbox("Nausea",<%= Digestive.Contains("5").ToString().ToLower() %>,true) +
            printview.checkbox("Vomiting",<%= Digestive.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("GERD",<%= Digestive.Contains("7").ToString().ToLower() %>,true) +
            printview.checkbox("Abd Girth: <%= Digestive.Contains("8") ? data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength").Clean() : string.Empty %>",<%= Digestive.Contains("8").ToString().ToLower() %>,true) +
            printview.span(""))+
        printview.span("Elimination",true)+
        printview.col(2,
            printview.checkbox("Last BM:",<%= Digestive.Contains("11").ToString().ToLower() %>,true) +
            printview.span("<%= Digestive.Contains("11") ?"Date:"+ data.AnswerOrEmptyString("GenericDigestiveLastBMDate").Clean() : string.Empty %>")) +
        printview.checkbox("WNL (Within Normal Limits)",<%= DigestiveLastBM.Contains("1").ToString().ToLower() %>,true) +
        <%if(DigestiveLastBM.Contains("2")){ %>
        printview.col(3,
            printview.checkbox("Abnormal Stool:",true) +
            printview.checkbox("Gray",<%= (DigestiveLastBM.Contains("2") && DigestiveLastBMAbnormalStool.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("Tarry",<%= (DigestiveLastBM.Contains("2") && DigestiveLastBMAbnormalStool.Contains("2")).ToString().ToLower() %>) +
            printview.span("")+
            printview.checkbox("Fresh Blood",<%= (DigestiveLastBM.Contains("2") && DigestiveLastBMAbnormalStool.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Black",<%= (DigestiveLastBM.Contains("2") && DigestiveLastBMAbnormalStool.Contains("4")).ToString().ToLower() %>)) +
       <%}else{ %>
        printview.checkbox("Abnormal Stool:",false) +
       <%} %>
       <%if(DigestiveLastBM.Contains("3")){ %>
        printview.col(4,
            printview.checkbox("Constipation:",<%= DigestiveLastBM.Contains("3").ToString().ToLower() %>,true) +
            printview.checkbox("Chronic",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic")).ToString().ToLower() %>) +
            printview.checkbox("Acute",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute")).ToString().ToLower() %>) +
            printview.checkbox("Occasional",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional")).ToString().ToLower() %>)) +
       <%}else{ %>
        printview.checkbox("Constipation:",false) +
       <%} %>
       <%if(DigestiveLastBM.Contains("4")){ %>
       printview.col(4,
            printview.checkbox("Diarrhea:",true) +
            printview.checkbox("Chronic",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic")).ToString().ToLower() %>) +
            printview.checkbox("Acute",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute")).ToString().ToLower() %>) +
            printview.checkbox("Occasional",<%= (DigestiveLastBM.Contains("3") && data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional")).ToString().ToLower() %>)) +
       <%}else{ %>
       printview.checkbox("Diarrhea:",false) +
       <%} %>
       printview.span("Ostomy",true) +
        printview.col(3,
            printview.checkbox("Ostomy Type:<%= DigestiveOstomy.Contains("1") ? (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("2") ? "Ileostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("3") ? "Colostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("4") ? "Other" : string.Empty) : string.Empty %>",<%= DigestiveOstomy.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Appearance:<%= DigestiveOstomy.Contains("2") ? data.AnswerOrEmptyString("GenericDigestiveStomaAppearance").Clean() : string.Empty %>",<%= DigestiveOstomy.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Sur. Skin:<%= DigestiveOstomy.Contains("3") ? data.AnswerOrEmptyString("GenericDigestiveSurSkinType").Clean() : string.Empty %>",<%= DigestiveOstomy.Contains("3").ToString().ToLower() %>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericGUDigestiveComments").Clean() %>",false,2),
        "GI");
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Elimination.ascx", Model); %>
    <%  } %>
<%  } %>
</script>