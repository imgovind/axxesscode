<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 > 5 && Model.AssessmentTypeNum != 8) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2300) " : string.Empty %>Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M2300EmergentCare").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes, used hospital emergency department w/o hospital admission",<%= data.AnswerOrEmptyString("M2300EmergentCare").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Yes, used hospital emergency department with hospital admission",<%= data.AnswerOrEmptyString("M2300EmergentCare").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M2300EmergentCare").Equals("UK").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M2310) " : string.Empty %>Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)? (Mark all that apply.)",true) +
        printview.col(2,
            printview.checkbox("1 &#8211; Improper medication administration, medication side effects, toxicity, anaphylaxis",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareMed").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Injury caused by fall",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareFall").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Respiratory infection (e.g., pneumonia, bronchitis)",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareResInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Other respiratory problem",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareOtherResInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Heart failure (e.g., fluid overload)",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartFail").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; Cardiac dysrhythmia (irregular heartbeat)",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareCardiac").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &#8211; Myocardial infarction or chest pain",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareMyocardial").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("8 &#8211; Other heart disease",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartDisease").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("9 &#8211; Stroke (CVA) or TIA",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareStroke").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("10 &#8211; Hypo/Hyperglycemia, diabetes out of control",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareHypo").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("11 &#8211; GI bleeding, obstruction, constipation, impaction",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareGI").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("12 &#8211; Dehydration, malnutrition",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareDehMal").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("13 &#8211; Urinary tract infection",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareUrinaryInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("14 &#8211; IV catheter-related infection or complication",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareIV").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("15 &#8211; Wound infection or deterioration",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareWoundInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("16 &#8211; Uncontrolled pain",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareUncontrolledPain").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("17 &#8211; Acute mental/behavioral health problem",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareMental").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("18 &#8211; Deep vein thrombosis, pulmonary embolus",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareDVT").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("19 &#8211; Other than above reasons",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareOther").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Reason unknown",<%= data.AnswerOrEmptyString("M2310ReasonForEmergentCareUK").Equals("1").ToString().ToLower() %>)));
<%  } %>
</script>