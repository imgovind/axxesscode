<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<script type="text/javascript">
printview.addsection(
"","Therapy Need & Plan of Care");
<%  if (Model.AssessmentTypeNum % 10 < 6) { %>
    <%  if (Model.AssessmentTypeNum < 6) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2200) " : string.Empty %>Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?",true) +
        printview.span("Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined). <%= data.AnswerOrEmptyString("M2200NumberOfTherapyNeed").Clean() %>") +
        printview.checkbox("NA &#8211; Not Applicable: No case mix group defined by this assessment.",<%= data.AnswerOrEmptyString("M2200TherapyNeedNA").Equals("1").ToString().ToLower() %>),
        "OASIS M2200");
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%224%22%3E" +
        printview.span("(M2250) Plan of Care Synopsis: (Check only one box in each row.) Does the physician-ordered plan of care include the following:",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3E" +
        printview.span("Plan/Intervention",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("&#160;No ",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("&#160;Yes ",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Not Applicable",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Patient-specific parameters for notifying physician of changes in vital signs or other clinical findings") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250PatientParameters").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250PatientParameters").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Physician has chosen not to establish patient-specific parameters for this patient. Agency will use standardized clinical guidelines accessible for all care providers to reference",<%= data.AnswerOrEmptyString("M2250PatientParameters").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not diabetic or is bilateral amputee",<%= data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Falls prevention interventions") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250FallsPrevention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250FallsPrevention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not assessed to be at risk for falls",<%= data.AnswerOrEmptyString("M2250FallsPrevention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient has no diagnosis or symptoms of depression",<%= data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Intervention(s) to monitor and mitigate pain") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("No pain identified",<%= data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Intervention(s) to prevent pressure ulcers") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not assessed to be at risk for pressure ulcers",<%= data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("g. Pressure ulcer treatment based on principles of moist wound healing OR order for treatment based on moist wound healing has been requested from physician") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient has no pressure ulcers with need for moist wound healing",<%= data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/TherapyNeed.ascx", Model); %>
    <%  if (Model.AssessmentTypeNum == 5) { %>
    printview.addsection(printview.span('<%= data.AnswerOrEmptyString("GenericDischargeNarrative").Clean() %>', false,2), "Narrative");
    <%  } %>
<%  } %>
</script>