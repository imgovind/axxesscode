﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Not Exported OASIS Assessments | <%= Current.AgencyName %></span>
<% var visible = Current.HasRight(Permissions.ReopenDocuments) && !Current.IsAgencyFrozen; %>
<% string pageName = "NotExportedOasis"; %>
<% using (Html.BeginForm("NotExportedOasis", "Export", FormMethod.Post)) { %>
<div class="wrapper grid-bg">
    <div class="buttons float-right">
        <ul>
            <li><%= Html.ActionLink("Export to Excel", "NotExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "NotExportedOasis_ExportLink" })%></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
                <div class="buttons float-right">
                    <ul>
                        <li>
                        <a href="javascript:void(0);" onclick="Agency.RebindNotExportedOasis();">Generate</a>
                        </li>
                    </ul>
                </div>
                <label class="float-left">Branch:</label>
                
                    <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = pageName+"_BranchId" })%>
                
            
                <label class="strong">Status:</label>
                    <select id="<%= pageName %>_Status" name="StatusId">
                        <option value="0">All</option>
                        <option value="1" selected>Active</option>
                        <option value="2">Discharged</option>
                    </select>
                    <br />
                    <label  class="float-left">Exported Date Range:</label>
                    <div class="float-right">
                        <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
                    </div>
                </div>
            </td> 
            <td>
                <div class="buttons">
                    <ul>
                        <li>
                        <a href="javascript:void(0);" onclick="Oasis.RebindNotExported();">Generate</a>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <div class="buttons">
                    <ul>
                        <li><%= Html.ActionLink("Export to Excel", "NotExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id =  pageName+"_ExportLink" })%></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>   
    <%  Html.Telerik().Grid<AssessmentExport>().Name(pageName+"_Grid").HtmlAttributes(new { @style = "top:30px;" }).Columns(columns =>
        {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(80).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');\" >Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');\" >Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("NotExported", "Oasis", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
</div>
<script type="text/javascript"> $("#window_oasisNotExported #<%= pageName %>_Grid .t-grid-content").css({ "height": "auto", "top": "26px" });</script>
<% } %>

