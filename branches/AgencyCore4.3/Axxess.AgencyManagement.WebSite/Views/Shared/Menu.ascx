﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop();
// -- Build Menu -->
Acore.AddMenu({ Name: "Home", Id: "home", IconX: "121", IconY: "86" });
<% if (!Current.IsAgencyFrozen) { %>
    Acore.AddMenu({ Name: "My Account", Id: "account", Parent: "home" });
    Acore.AddMenu({ Name: "Create", Id: "create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "New", Id: "createnew", Parent: "create" });
    <% } %>
Acore.AddMenu({ Name: "View", Id: "view", IconX: "161", IconY: "108" });
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "viewlist", Parent: "view" });
    <% } %>
Acore.AddMenu({ Name: "Patients", Id: "patients", IconX: "183", IconY: "108" });
Acore.AddMenu({ Name: "Schedule", Id: "schedule", IconX: "205", IconY: "108" });
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddMenu({ Name: "Billing", Id: "billing", IconX: "227", IconY: "108" });
<% } %>
<% if (Current.IsAgencyAdmin || (Current.HasRight(Permissions.ManageUsers) || Current.HasRight(Permissions.ManagePayroll) || Current.HasRight(Permissions.ManageAgencyInformation))) { %>
    Acore.AddMenu({ Name: "Admin", Id: "admin", IconX: "142", IconY: "86" });
    <% if (!Current.IsAgencyFrozen) { %>
    Acore.AddMenu({ Name: "New", Id: "adminadd", Parent: "admin" });
    <% } %>
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "adminlist", Parent: "admin" });
    <% } %>
<% } %>
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddMenu({ Name: "Reports", Id: "reports", IconX: "164", IconY: "86" });
<% } %>
Acore.AddMenu({ Name: "Help", Id: "help", IconX: "186", IconY: "86" });
Acore.AddMenu({ Name: "Training", Id: "training", Parent: "help" });
Acore.AddMenu({ Name: "Support", Id: "support", Parent: "help" });
// -- Home Menu -->
<% if (!Current.IsAgencyFrozen) { %>
Acore.AddWindow({ Name: "Edit Profile", Id: "editprofile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account" });
Acore.AddWindow({ Name: "Reset Signature", Id: "forgotsignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "225px", Width: "500px" });
<% } %>
Acore.AddWindow({ Name: "Dashboard", MenuName: "My Dashboard", Id: "homepage", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Messages", Id: "messageinbox", Url: "Message/Inbox", OnLoad: Message.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Schedule/Tasks", Id: "listuserschedule", Url: "User/Schedule", Menu: [ "home", "schedule" ] });
Acore.AddWindow({ Name: "My Monthly Calendar", Id: "userschedulemonthlycalendar", Url: "User/UserCalendar", Menu: "home" });
<% if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
Acore.AddWindow({ Name: "Quality Assurance (QA) Center", Id: "caseManagement", Url: "Agency/CaseManagement", Menu: "home" });
//Acore.AddWindow({ Name: "Quality Assurance (QA) Center", Id: "caseManagementcenter", Url: "Agency/CaseManagementCenter", OnLoad: Agency.InitQaCenter, Menu: "home" });
//Acore.AddMenu({ Name: "Quality Assurance (QA)", Id: "qa", Parent: "home" });
//Acore.AddWindow({ Name: "List", Id: "caseManagement", Url: "Agency/CaseManagement", Menu: "qa" });
//Acore.AddWindow({ Name: "Center", Id: "caseManagementcenter", Url: "Agency/CaseManagementCenter", Menu: "qa" });
<% } %>
// -- Create Menu (Some Duplicated to Admin Menu) -->
<% if (Current.HasRight(Permissions.ManageReferrals)) { %>
Acore.AddWindow({ Name: "New Referral", MenuName: "Referral", Id: "newreferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePatients)) { %>
Acore.AddWindow({ Name: "New Patient", MenuName: "Patient", Id: "newpatient", Url: "Patient/New", OnLoad: Patient.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.EditEpisode)) { %>
Acore.AddWindow({ Name: "New Episode", MenuName: "Episode", Id: "newepisode", Url: "Schedule/NewPatientEpisode", OnLoad: Schedule.InitTopMenuNewEpisode, Menu: [ "createnew", "adminadd" ] });
<% } %>
Acore.AddWindow({ Name: "Compose Message", MenuName: "Message", Id: "newmessage", Url: "Message/New", OnLoad: Message.InitNew, Menu: [ "createnew", "adminadd" ] });
Acore.AddWindow({ Name: "New Communication Note", MenuName: "Communication Note", Id: "newcommnote", Url: "CommunicationNote/New", OnLoad: Patient.InitNewCommunicationNote, Menu: [ "createnew", "adminadd" ] });
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>
Acore.AddWindow({ Name: "New Authorization", MenuName: "Authorization", Id: "newauthorization", Url: "Authorization/New", OnLoad: Patient.InitNewAuthorization, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
Acore.AddWindow({ Name: "New Order", MenuName: "Order", Id: "neworder", Url: "Order/New", OnLoad: Patient.InitNewOrder, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
Acore.AddWindow({ Name: "New Physician Face-to-face Encounter", MenuName: "Physician Face-to-face Encounter", Id: "newfacetofaceencounter", Url: "FaceToFaceEncounter/New", OnLoad: Patient.InitNewFaceToFaceEncounter, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageHospital)) { %>
Acore.AddWindow({ Name: "New Hospital", MenuName: "Hospital", Id: "newhospital", Url: "Hospital/New", OnLoad: Hospital.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>
Acore.AddWindow({ Name: "New Insurance/Payor", MenuName: "Insurance/Payor", Id: "newinsurance", Url: "Insurance/New", OnLoad: Insurance.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>
Acore.AddWindow({ Name: "New Physician", MenuName: "Physician", Id: "newphysician", Url: "Physician/New", OnLoad: Physician.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageContact)) { %>
Acore.AddWindow({ Name: "New Contact", MenuName: "Contact", Id: "newcontact", Url: "Contact/New", OnLoad: Contact.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
Acore.AddWindow({ Name: "New Incident/Accident Log", MenuName: "Incident/Accident Log", Id: "newincidentreport", Url: "Incident/New", OnLoad: IncidentReport.InitNew, Menu: "createnew" });
Acore.AddWindow({ Name: "New Infection Log", MenuName: "Infection Log", Id: "newinfectionreport", Url: "Infection/New", OnLoad: InfectionReport.InitNew, Menu: "createnew" });
<% } %>
<% if (Current.HasRight(Permissions.CreateOasisSubmitFile)) { %>
Acore.AddWindow({ Name: "OASIS Export", Id: "oasisExport", Url: "Oasis/ExportView", Menu: "create" });
<% } %>
// -- View Menu (Some Duplicated to Admin Menu) -->
<% if (Current.HasRight(Permissions.ViewLists)) { %>
Acore.AddWindow({ Name: "Patient List", MenuName: "Patients", Id: "listpatients", Url: "Patient/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Referral List", MenuName: "Referrals", Id: "listreferrals", Url: "Referral/List", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Contact List", MenuName: "Contacts", Id: "listcontacts", Url: "Contact/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Template List", MenuName: "Templates", Id: "listtemplates", Url: "Template/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Supply List", MenuName: "Supplies", Id: "listsupplies", Url: "Supply/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Hospital List", MenuName: "Hospital", Id: "listhospitals", Url: "Hospital/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Insurance/Payor List", MenuName: "Insurances/Payors", Id: "listinsurances", Url: "Insurance/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Physician List", MenuName: "Physicians", Id: "listphysicians", Url: "Physician/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "User List", MenuName: "Users", Id: "listusers", Url: "User/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Incident Log List", MenuName: "Incident Logs", Id: "listincidents", Url: "Incident/Grid", Menu: "viewlist" });
Acore.AddWindow({ Name: "Infection Log List", MenuName: "Infection Logs", Id: "listinfections", Url: "Infection/Grid", Menu: "viewlist" });
Acore.AddWindow({ Name: "Communication Note List", MenuName: "Communication Notes", Id: "communicationnoteslist", Url: "CommunicationNote/List", Menu: "viewlist" });
Acore.AddWindow({ Name: "Missed Visit List", MenuName: "Missed Visits", Id: "listmissedvisits", Url: "MissedVisits/List", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "Adjustment Code List", MenuName: "Adjustment Codes", Id: "listadjustmentcodes", Url: "AdjustmentCode/Grid", Menu: [ "adminlist" ]});
<% } %>
Acore.AddWindow({ Name: "Blank Forms", Id: "blankforms", Url: "Agency/Blankforms", Menu: "view" });
<% if (Current.HasRight(Permissions.ViewExportedOasis)) { %>
Acore.AddWindow({ Name: "Exported OASIS", Id: "oasisExported", Url: "Oasis/ExportedView", Menu: "view" });
Acore.AddWindow({ Name: "Not Exported OASIS", Id: "oasisNotExported", Url: "Oasis/NotExportedView", Menu: "view" });
<% } %>
<% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
Acore.AddWindow({ Name: "Past Due Recerts", Id: "listpastduerecerts", Url: "Agency/RecertsPastDueGrid", Menu: "view" });
Acore.AddWindow({ Name: "Upcoming Recerts", Id: "listupcomingrecerts", Url: "Agency/RecertsUpcomingGrid", Menu: "view" });
<% } %>
<% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>
Acore.AddMenu({ Name: "Orders Management", Id: "ordersmanagement", Parent: "view" });
Acore.AddWindow({ Name: "Orders To Be Sent", Id: "orderstobesent", Url: "Agency/OrdersToBeSentView", Menu: "ordersmanagement" });
Acore.AddWindow({ Name: "Orders Pending Signature", Id: "orderspendingsignature", Url: "Agency/OrdersPendingSignatureView", Menu: "ordersmanagement" });
Acore.AddWindow({ Name: "Orders History", Id: "ordersHistory", Url: "Agency/OrdersHistory", Menu: "ordersmanagement" });
<% } %>
<% if (Current.HasRight(Permissions.PrintClinicalDocuments)) { %>
Acore.AddWindow({ Name: "Print Queue", Id: "printqueue", Url: "Agency/PrintQueue", Menu: "view" });
<% } %>
//<% if (Current.HasRight(Permissions.ReceiveEligibilityReport)) { %>
//    Acore.AddWindow({ Name: "Medicare Eligibility Summaries", Id: "medicareEligibility", Url: "Agency/MedicareEligibilitySummary", Menu: "viewlist" });
//<% } %>
// -- Patient Menu -->
Acore.AddWindow({ Name: "Patient Charts", Id: "patientcenter", Url: "Patient/Center/1/<%= Guid.Empty %>", OnLoad: Patient.InitCenter, Menu: "patients" });
<% if (Current.HasRight(Permissions.ViewExisitingReferrals)) { %>
Acore.AddWindow({ Name: "Existing Referrals", Id: "existingreferrals", Url: "Referral/List", Menu: "patients" });
<% } %>
<% if (Current.HasRight(Permissions.ViewLists)) { %>
Acore.AddWindow({ Name: "Pending Admissions", Id: "listpendingpatients", Url: "Patient/PendingGrid", Menu: "patients" });
Acore.AddWindow({ Name: "Non-Admissions", Id: "listnonadmit", Url: "Patient/NonAdmitGrid", Menu: "patients" });
Acore.AddWindow({ Name: "Deleted Patients", Id: "listdeletedpatients", Url: "Patient/DeletedPatientGrid", Menu: "patients" });
Acore.AddWindow({ Name: "Hospitalization Logs", Id: "listhospitalization", Url: "Patient/HospitalizationGrid", Menu: "patients" });
<% } %>
// -- Schedule Menu -->
Acore.AddWindow({ Name: "Schedule Center", Id: "schedulecenter", Url: "Schedule/Center/1/<%= Guid.Empty %>", OnLoad: Schedule.InitCenter, Menu: "schedule" });
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddWindow({ Name: "Schedule Deviation Report", Id: "scheduledeviation", Url: "Schedule/Deviation", Menu: "schedule" });
<% } %>

<% if (Current.HasRight(Permissions.ScheduleVisits) && !Current.IsAgencyFrozen) { %>
Acore.AddWindow({ Name: "Reassign Schedule", Id: "schedulereassign", Url: "Schedule/ReAssignSchedules", OnLoad: function () { Schedule.reassignScheduleInit("All") }, Menu: "schedule" });
<% } %>

// -- Billing Menu -->
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddMenu({ Name: "Medicare / Medicare HMO", Id: "medicareclaim", Parent: "billing" });
Acore.AddWindow({ Name: "Create RAP Claims", Id: "billingcenterrap", Url: "Billing/RAPCenter", OnLoad: function(){}, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Create Final Claims", Id: "billingcenterfinal", Url: "Billing/FinalCenter",  OnLoad: function(){}, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Pending Claims", Id: "pendingClaim", Url: "Billing/PendingClaims", OnLoad: function(){}, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Billing/Claims History", Id: "billingHistory", Url: "Billing/History", OnLoad: Billing.InitCenter, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Remittance Advice", Id: "remittances", Url: "Billing/Remittance", OnLoad:Billing.InitRemittance, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Eligibility Report", Id: "medicareeligibilityreport", Url: "Billing/EligibilityReport", Menu: "medicareclaim" });
Acore.AddMenu({ Name: "Managed Care/Other Insurances", Id: "managedcareclaim", Parent: "billing" });
Acore.AddWindow({ Name: "Create Claims", Id: "createmanagedclaims", Url: "Billing/ManagedCenter", OnLoad: ManagedBilling.InitCenter, Menu: "managedcareclaim" });
Acore.AddWindow({ Name: "Claims History", Id: "managedclaims", Url: "Billing/ManagedHistory", OnLoad: ManagedBilling.InitCenter, Menu: "managedcareclaim" });
Acore.AddWindow({ Name: "All Insurances/Payors", Id: "allbillingclaims", Url:"Billing/AllClaims", Menu: "billing" });
Acore.AddWindow({ Name: "Claim Submission History", Id: "submittedclaims", Url: "Billing/SubmittedList", Menu: "billing" });
Acore.AddWindow({ Name: "Submitted Claim Details", Id: "submittedclaimdetail", Url: "Billing/SubmittedClaimDetail" });
Acore.AddWindow({ Name: "Claim Response", Id: "billingclaimresponse", Url:"Billing/ClaimResponse", Resize: false, Center: true, IgnoreMinSize: true, Width: "625px" });
<% } %>
// -- Admin Menu -->
Acore.AddWindow({ Name: "New Template", MenuName: "Template", Id: "newtemplate", Url: "Template/New", OnLoad: Template.InitNew, Menu: "adminadd" });
Acore.AddWindow({ Name: "New Supply", MenuName: "Supply", Id: "newsupply", Url: "Supply/New", OnLoad: Template.InitNew, Menu: "adminadd", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "290px", Width: "750px" });
<% if (Current.HasRight(Permissions.ManageUsers)) { %>
Acore.AddWindow({ Name: "New User", MenuName: "User", Id: "newuser", Url: "User/New", OnLoad: User.InitNew, Menu: "adminadd" });
<% } %>
<% if (Current.HasRight(Permissions.ManagePayroll)) { %>
Acore.AddWindow({ Name: "Payroll Summary", Id: "payrollsummary", Url: "Payroll/Search", OnLoad: Payroll.InitSearch, Menu: "admin" });
<% } %>
<% if (Current.HasRight(Permissions.ManageUsers)) { %>
Acore.AddWindow({ Name: "License Manager", Id: "licensemanager", Url: "User/LicenseManager", OnLoad: User.InitLicenseManager, Menu: "admin" });
<% } %>
<% if (Current.HasRight(Permissions.ManageAgencyInformation) && !Current.IsAgencyFrozen) { %>
Acore.AddWindow({ Name: "Manage Company Information", Id: "checksignature", Url: "Agency/Signature", Height: "210px", Width: "600px", Menu: "admin", Modal: true, Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true });
Acore.AddWindow({ Name: "Edit Company Information", Id: "agencyinfo", Resize: false, Center: true, IgnoreMinSize: true, Height: "640px", Width: "900px" });
<% } %>
// -- Reports Menu -->
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddWindow({ Name: "Report Center", Id: "reportcenter", Url: "Report/Center", OnLoad: Report.Init, Menu: "reports" });
Acore.AddWindow({ Name: "Completed Reports", Id: "listreports", Url: "Report/Completed", Menu: "reports" });
<% } %>
// -- Help Menu -->
Acore.AddMenuItem({ Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/help?u={0}", SessionStore.SessionId) %>', Parent: "training" });
Acore.AddMenuItem({ Name: "Free Webinars", Href: '<%= string.Format("http://axxessweb.com/help/webinars?u={0}", SessionStore.SessionId) %>', Parent: "training" });
Acore.AddMenuItem({ Name: "Discussion Forum", Href: "/Forum", Parent: "support" });
Acore.AddMenuItem({ Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "support" });
Acore.AddMenuItem({ Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "support" });
Acore.AddWindow({ Name: "After-Hours Support", Id: "afterhourssupport", Url: "Agency/AfterHoursSupport", Menu: "support", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "300px", Width: "600px" });
Acore.AddMenuItem({ Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "help" });
Acore.AddMenuItem({ Name: "Follow us on LinkedIn", Href: "http://www.linkedin.com/company/axxess-consult", Parent: "help" });
Acore.AddWindow({ Name: "Join GoToMeeting", Id: "GoToMeeting", Url: "User/GoToMeeting", Menu: "support" });
Acore.AddWindow({ Name: "Recent Software Updates", Id: "RecentUpdates", Url: "Home/Updates", Menu: "help" });


// -- Windows not found in the menus -->
Acore.AddWindow({ Name: "New Hospitalization Log", Id: "newhospitalizationlog", Url: "Patient/NewHospitalizationLog" });
Acore.AddWindow({ Name: "Edit Hospitalization Log", Id: "edithospitalizationlog", Url: "Patient/EditHospitalizationLog" });
Acore.AddWindow({ Name: "Hospitalization Logs", Id: "patienthospitalizationlogs", Url: "Patient/HospitalizationLogs" });
Acore.AddWindow({ Name: "Allergy Profile", Id: "allergyprofile", Url: "Patient/AllergyProfile" });
Acore.AddWindow({ Name: "Medicare Eligibility Reports", Id: "medicareeligibilitylist", Url: "Patient/MedicareEligibilityList" });
Acore.AddWindow({ Name: "Medication Profile SnapShot", Id: "medicationprofilesnapshot", Url: "Patient/MedicationProfileSnapShot", OnLoad: Patient.InitMedicationProfileSnapshot });
Acore.AddWindow({ Name: "Medication Profile", Id: "medicationprofile", Url: "Patient/MedicationHistoryView" });
Acore.AddWindow({ Name: "Signed Medication Profiles", Id: "medicationprofilesnapshothistory", Url: "Patient/MedicationHistorySnapshotView" });
Acore.AddWindow({ Name: "Master Calendar", Id: "masterCalendarMain", Url: "Schedule/MasterCalendarMain" });
Acore.AddWindow({ Name: "Wound Care Flowsheet", Id: "woundcare", Url: "Schedule/WoundCare", OnLoad: Schedule.WoundCareInit });
Acore.AddWindow({ Name: "SN Visit", Id: "snVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "PT Visit", Id: "PTVisit", Url: "Schedule/NoteView", OnLoad: Visit.PTVisit.Init });
Acore.AddWindow({ Name: "PTA Visit", Id: "PTAVisit", Url: "Schedule/NoteView", OnLoad: Visit.PTVisit.Init });
Acore.AddWindow({ Name: "PT Evaluation", Id: "PTEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.PTEvaluation.Init });
Acore.AddWindow({ Name: "PT Re-Evaluation", Id: "PTReEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.PTEvaluation.Init });
Acore.AddWindow({ Name: "PT Re-Assessment", Id: "PTReassessment", Url: "Schedule/NoteView",OnLoad: Visit.PTReassessment.Init});
Acore.AddWindow({ Name: "PT Maintenance", Id: "PTMaintenance", Url: "Schedule/NoteView", OnLoad: Visit.PTEvaluation.Init });
Acore.AddWindow({ Name: "PT Discharge", Id: "PTDischarge", Url: "Schedule/NoteView", OnLoad: Visit.PTDischarge.Init });
Acore.AddWindow({ Name: "PT Supervisory Visit", Id: "PTSupervisoryVisit", Url: "Schedule/NoteView", OnLoad: Visit.PTSupervisoryVisit.Init });
Acore.AddWindow({ Name: "OT Evaluation", Id: "OTEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.OTEvaluation.Init });
Acore.AddWindow({ Name: "OT Re-Evaluation", Id: "OTReEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.OTEvaluation.Init });
Acore.AddWindow({ Name: "OT Re-Assessment", Id: "OTReassessment", Url: "Schedule/NoteView", OnLoad: Visit.OTReassessment.Init });
Acore.AddWindow({ Name: "OT Maintenance", Id: "OTMaintenance", Url: "Schedule/NoteView", OnLoad: Visit.OTEvaluation.Init });
Acore.AddWindow({ Name: "OT Discharge", Id: "OTDischarge", Url: "Schedule/NoteView", OnLoad: Visit.OTEvaluation.Init });
Acore.AddWindow({ Name: "OT Visit", Id: "OTVisit", Url: "Schedule/NoteView", OnLoad: Visit.OTVisit.Init });
Acore.AddWindow({ Name: "OT Supervisory Visit", Id: "OTSupervisoryVisit", Url: "Schedule/NoteView", OnLoad: Visit.OTSupervisoryVisit.Init });
Acore.AddWindow({ Name: "COTA Visit", Id: "COTAVisit", Url: "Schedule/NoteView", OnLoad: Visit.OTVisit.Init });
Acore.AddWindow({ Name: "ST Visit", Id: "STVisit", Url: "Schedule/NoteView", OnLoad: Visit.STVisit.Init });
Acore.AddWindow({ Name: "ST Evaluation", Id: "STEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.STEvaluation.Init });
Acore.AddWindow({ Name: "ST Re-Evaluation", Id: "STReEvaluation", Url: "Schedule/NoteView", OnLoad: Visit.STEvaluation.Init });
Acore.AddWindow({ Name: "ST Reassessment", Id: "STReassessment", Url: "Schedule/NoteView", OnLoad: Visit.STReassessment.Init });
Acore.AddWindow({ Name: "ST Maintenance", Id: "STMaintenance", Url: "Schedule/NoteView", OnLoad: Visit.STEvaluation.Init });
Acore.AddWindow({ Name: "ST Discharge", Id: "STDischarge", Url: "Schedule/NoteView", OnLoad: Visit.STEvaluation.Init });
Acore.AddWindow({ Name: "Initial Summary Of Care", Id: "ISOC", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "MSW Evaluation", Id: "MSWEvaluationAssessment", Url: "Schedule/NoteView", OnLoad: Visit.MSWEvaluationAssessment.Init });
Acore.AddWindow({ Name: "MSW Assessment", Id: "MSWAssessment", Url: "Schedule/NoteView", OnLoad: Visit.MSWEvaluationAssessment.Init });
Acore.AddWindow({ Name: "MSW Discharge", Id: "MSWDischarge", Url: "Schedule/NoteView", OnLoad: Visit.MSWEvaluationAssessment.Init });
Acore.AddWindow({ Name: "MSW Visit", Id: "MSWVisit", Url: "Schedule/NoteView", OnLoad: Visit.MSWVisit.Init });
Acore.AddWindow({ Name: "MSW Progress Note", Id: "MSWProgressNote", Url: "Schedule/NoteView", OnLoad: Visit.MSWProgressNote.Init });
Acore.AddWindow({ Name: "OASIS-C Start of Care", Id: "StartOfCare", Url: "Oasis/StartOfCare" });
Acore.AddWindow({ Name: "Non-OASIS Start of Care", Id: "NonOasisStartOfCare", Url: "Oasis/NonOasisStartOfCare" });
Acore.AddWindow({ Name: "OASIS-C Recertification", Id: "Recertification", Url: "Oasis/Recertification" });
Acore.AddWindow({ Name: "Non-OASIS Recertification", Id: "NonOasisRecertification", Url: "Oasis/NonOasisRecertification" });
Acore.AddWindow({ Name: "OASIS-C Resumption of Care", Id: "ResumptionOfCare", Url: "Oasis/ResumptionOfCare" });
Acore.AddWindow({ Name: "OASIS-C Follow Up", Id: "FollowUp", Url: "Oasis/FollowUp" });
Acore.AddWindow({ Name: "OASIS-C Death at Home", Id: "DischargeFromAgencyDeath", Url: "Oasis/DischargeFromAgencyDeath" });
Acore.AddWindow({ Name: "OASIS-C Discharge from Agency", Id: "DischargeFromAgency", Url: "Oasis/DischargeFromAgency" });
Acore.AddWindow({ Name: "Non-OASIS Discharge", Id: "NonOasisDischarge", Url: "Oasis/NonOasisDischarge" });
Acore.AddWindow({ Name: "OASIS-C Transfer For Discharge", Id: "TransferInPatientDischarged", Url: "Oasis/TransferInPatientDischarged" });
Acore.AddWindow({ Name: "OASIS-C Transfer Not Discharge", Id: "TransferInPatientNotDischarged", Url: "Oasis/TransferInPatientNotDischarged" });
Acore.AddWindow({ Name: "LVN/LPN Supervisory Visit", Id: "lvnsVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "Diabetic Daily Visit Nursing Note", Id: "snDiabeticDailyVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "Pediatric Visit Nursing Note", Id: "snPediatricVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "SN Pediatric Assessment Note", Id: "snPediatricAssessment", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "Psychiatric Visit Nursing Note", Id: "snPsychVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "SN Psychiatric Visit", Id: "snPsychVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "SN Psychiatric Assessment Nursing Note", Id: "snPsychAssessment", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "HHA Supervisory Visit", Id: "hhasVisit", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "Discharge Summary", Id: "dischargeSummary", Url: "Schedule/NoteView", OnLoad: dischargeSummary.Init });
Acore.AddWindow({ Name: "Home Health Aide Progress Note", Id: "HHAideVisit", Url: "Schedule/NoteView", OnLoad: Visit.HHAideVisit.Init });
Acore.AddWindow({ Name: "Home Health Aide Care Plan", Id: "hhaCarePlan", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "60 Day Summary/Case Conference", Id: "sixtyDaySummary", Url: "Schedule/NoteView" ,OnLoad:sixtyDaySummary.Init});
Acore.AddWindow({ Name: "Transfer Summary", Id: "transferSummary", Url: "Schedule/NoteView", OnLoad: transferSummary.Init});
Acore.AddWindow({ Name: "Coordination of Care", Id: "coordinationofcare", Url: "Schedule/NoteView", OnLoad: coordinationofcare.Init });
Acore.AddWindow({ Name: "Personal Care Services Progress Note", Id: "PASVisit", Url: "Schedule/NoteView", OnLoad: Visit.PASVisit.Init });
Acore.AddWindow({ Name: "Personal Care Services Travel Note", Id: "PASTravel", Url: "Schedule/NoteView", OnLoad: Visit.PASTravel.Init });
Acore.AddWindow({ Name: "Personal Care Services Care Plan", Id: "PASCarePlan", Url: "Schedule/NoteView", OnLoad: Visit.PASCarePlan.Init });
Acore.AddWindow({ Name: "UAP Wound Care Note", Id: "UAPWoundCareVisit", Url: "Schedule/NoteView", OnLoad: Visit.UAPWoundCareVisit.Init });
Acore.AddWindow({ Name: "UAP Insulin Prep-Aministration Note", Id: "UAPInsulinPrepAdminVisit", Url: "Schedule/NoteView", OnLoad: Visit.UAPInsulinPrepAdminVisit.Init });
Acore.AddWindow({ Name: "Home Maker Note", Id: "HomeMakerNote", Url: "Schedule/NoteView", OnLoad: Visit.HomeMakerNote.Init });
Acore.AddWindow({ Name: "PT Discharge Summary", Id: "PTDischargeSummary", Url: "Schedule/NoteView", OnLoad: PTDischargeSummary.Init });
Acore.AddWindow({ Name: "OT Discharge Summary", Id: "OTDischargeSummary", Url: "Schedule/NoteView",OnLoad:OTDischargeSummary.Init });
Acore.AddWindow({ Name: "Labs", Id: "snLabs", Url: "Schedule/NoteView" });
Acore.AddWindow({ Name: "Edit Infection Log", Id: "editinfectionreport" });
Acore.AddWindow({ Name: "Edit Incident/Accident Log", Id: "editincidentreport" });
Acore.AddWindow({ Name: "Authorization List", Id: "listauthorizations" });
Acore.AddWindow({ Name: "Communication Notes", Id: "patientcommunicationnoteslist" });
Acore.AddWindow({ Name: "Patient Orders History", Id: "patientordershistory" });
Acore.AddWindow({ Name: "Patient Deleted Tasks History", Id: "patientdeletedtaskhistory" });
Acore.AddWindow({ Name: "Patient 60 Day Summary", Id: "patientsixtydaysummary" });
Acore.AddWindow({ Name: "Driver / Transportation Note", Id: "transportationnote" ,Url: "Schedule/NoteView",OnLoad:transportationLog.Init});
Acore.AddWindow({ Name: "Patient Vital Signs", Id: "patientvitalsigns" });
Acore.AddWindow({ Name: "RAP", Id: "rap" });
Acore.AddWindow({ Name: "Final", Id: "final" });
Acore.AddWindow({ Name: "Managed Claim", Id: "managedclaimedit" });
Acore.AddWindow({ Name: "Task Details", Id: "scheduledetails" });
Acore.AddWindow({ Name: "Edit Episode", Id: "editepisode" });
Acore.AddWindow({ Name: "Edit Order", Id: "editorder" });
Acore.AddWindow({ Name: "Edit Contact", Id: "editcontact" });
Acore.AddWindow({ Name: "Edit Template", Id: "edittemplate" });
Acore.AddWindow({ Name: "Edit Supply", Id: "editsupply", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "290px", Width: "750px" });
Acore.AddWindow({ Name: "Edit Hospital", Id: "edithospital" });
Acore.AddWindow({ Name: "Edit Location", Id: "editlocation" });
Acore.AddWindow({ Name: "Edit Physician", Id: "editphysician" });
Acore.AddWindow({ Name: "Edit Insurance", Id: "editinsurance" });
Acore.AddWindow({ Name: "Edit Adjustment Code", Id: "editadjustmentcode", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "230px", Width: "500px" });
Acore.AddWindow({ Name: "Edit User", Id: "edituser" });
Acore.AddWindow({ Name: "Edit Patient", Id: "editpatient" });
Acore.AddWindow({ Name: "Admit Patient", Id: "admitpatient" });
Acore.AddWindow({ Name: "Edit Patient Information", Id: "editpatientadmission" });
Acore.AddWindow({ Name: "New Patient Information", Id: "newpatientadmission" });
Acore.AddWindow({ Name: "Patient Non-Admission", Id: "nonadmitpatient" });
Acore.AddWindow({ Name: "Edit Referral", Id: "editreferral" });
Acore.AddWindow({ Name: "Validation Result", Id: "validation" });
Acore.AddWindow({ Name: "Missed Visit Report", Id: "newmissedvisit" });
Acore.AddWindow({ Name: "Supply Worksheet", Id: "notessupplyworksheet", Url: "Schedule/SupplyWorksheet" });
Acore.AddWindow({ Name: "Edit Emergency Contact", Id: "editemergencycontact", Url: "Patient/EditEmergencyContactContent" });
Acore.AddWindow({ Name: "New Emergency Contact", Id: "newemergencycontact", Url: "Patient/NewEmergencyContactContent" });
Acore.AddWindow({ Name: "Edit 485 - Plan of Care (From Assessment)", Id: "editplanofcare" });
Acore.AddWindow({ Name: "Plan of Treatment/Care", Id: "newplanofcare" });
Acore.AddWindow({ Name: "Edit Communication Note", Id: "editcommunicationnote" });
Acore.AddWindow({ Name: "Edit Authorization", Id: "editauthorization" });
Acore.AddWindow({ Name: "Remittance Detail", Id: "remittancedetail" });
Acore.AddWindow({ Name: "Schedule Event Logs", Id: "schdeuleeventlogs" });
Acore.AddWindow({ Name: "List of In-active episodes", Id: "inactiveepisode" });
Acore.AddWindow({ Name: "List of Episode Logs", Id: "episodelogs" });
Acore.AddWindow({ Name: "List of Patient Logs", Id: "patientlogs" });
Acore.AddWindow({ Name: "List of Patient Medication Logs", Id: "medicationlogs" });
Acore.AddWindow({ Name: "List of Physician logs", Id: "physicianlogs" });
Acore.AddWindow({ Name: "List of User Logs", Id: "userlogs" });
Acore.AddWindow({ Name: "List of Referral Logs", Id: "referrallogs" });
Acore.AddWindow({ Name: "List of Contact Logs", Id: "contactlogs" });
Acore.AddWindow({ Name: "List of Insurance Logs", Id: "insurancelogs" });
Acore.AddWindow({ Name: "List of Location Logs", Id: "locationlogs" });
Acore.AddWindow({ Name: "List of Hospital Logs", Id: "hospitallogs" });
Acore.AddWindow({ Name: "List of Template Logs", Id: "templatelogs" });
Acore.AddWindow({ Name: "List of Supply Logs", Id: "supplylogs" });
Acore.AddWindow({ Name: "List of Claim Logs", Id: "claimlogs" });
Acore.AddWindow({ Name: "List of Adjustment Code Logs", Id: "adjustmentcodelogs" });
Acore.AddWindow({ Name: "Edit Visit Rates", Id: "visitrates"});
Acore.AddWindow({ Name: "Patient Admission Periods", Id: "patientmanageddates"});
Acore.AddWindow({ Name: "Episode Orders", Id: "patientepisodeorders"});
Acore.AddWindow({ Name: "Edit Missed Visit Form", Id: "editmissedvisit", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "475px", Width: "800px" });

Acore.AddWindow({ Name: "Claim Remittances", Id: "claimremittances"});

Acore.AddWindow({ Name: "Managed Claim Payments", Id: "managedclaimpayments"});
Acore.AddWindow({ Name: "Managed Claim Adjustments", Id: "managedclaimadjustments"});
Acore.AddWindow({ Name: "Patient Managed Claim Payments", Id: "allmanagedclaimpayments"});

Acore.AddWindow({ Name: "New Adjustment Code", Id: "newadjustmentcode",  Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "200px", Width: "500px" });

<% if (Current.HasRight(Permissions.DeleteTasks) && !Current.IsAgencyFrozen) { %>
Acore.AddWindow({ Name: "Delete Multiple Tasks", Id: "scheduledelete", OnLoad: function () { Schedule.deleteScheduleInit() } });
<% } %>

<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddWindow({ Name: "Claim Summary", Id: "claimSummaryrap", Url: "Billing/ClaimSummary" });
Acore.AddWindow({ Name: "Claim Summary", Id: "claimSummaryfinal", Url: "Billing/ClaimSummary" });
Acore.AddWindow({ Name: "Managed Claim Summary", Id: "managedclaimsummary", Url: "Billing/ManagedClaimSummary" });
<% } %>
$("#mainmenu").Menu();
Acore.GetRemoteContent = <%= AppSettings.GetRemoteContent %>;
Acore.Open("homepage");
</script>

