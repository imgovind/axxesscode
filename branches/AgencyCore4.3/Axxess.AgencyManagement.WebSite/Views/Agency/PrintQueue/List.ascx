﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<span class="wintitle">Print Queue | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "printQueueForm" })) { %>
 <%  var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]); %>
<div class="buttons wrapper">
<div class="float-right">
    <ul>
        <li><a href="javascript:void(0);" onclick="Agency.PrintQueueExport();">Export to Excel</a></li>
    </ul>
</div>
<fieldset class="orders-filter">
    <label class="strong">Branch:</label><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "Print_BranchCode" })%>
    <label class="strong">Event Date:</label><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-7).ToShortDateString() %>" id="Print_StartDate" /><label class="strong"> To:</label><input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="Print_EndDate" />

    <ul class="float-right">
        <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('<%=ViewData["GroupName"]%>','<%=sortParams %>');">Generate</a></li>
        
    </ul><ul class="float-left">
        <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('PatientName','<%=sortParams %>');">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('EventDate','<%=sortParams %>');">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('DisciplineTaskName','<%=sortParams %>');">Group By Task</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('UserName','<%=sortParams %>');">Group By Clinician</a></li>
    </ul>
</fieldset>
</div>
<div id="printQueueContentId" style='<%= Current.IsAgencyFrozen ? "bottom:0px;" : "" %>'>
<% Html.RenderPartial("~/Views/Agency/PrintQueue/Content.ascx", Model); %>
</div>
    <% if(!Current.IsAgencyFrozen) { %>
    <div class="buttons abs_bottom">
        <%= Html.Hidden("CommandType", "Print", new {@id="PrintQueueUpdate_Type" })%>
        <ul>
            <li><a href="javascript:void(0);" onclick="MarkAsPrinted('Print');">Mark As Printed</a></li>
        </ul>
    </div>
    <% } %>
<% } %>
<script type="text/javascript">
    $("#window_printqueue_content").css({
        "background-color": "#d6e5f3"
    });
    function MarkAsPrinted(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            Schedule.BulkUpdate('#printQueueForm', function(data) {
                Agency.LoadPrintQueue($("#PrintQueue_GroupName").val(), $("#PrintQueue_SortName").val());
            });
        } else { U.Growl("Select at least one item to mark as printed.", "error"); }
    }
</script>