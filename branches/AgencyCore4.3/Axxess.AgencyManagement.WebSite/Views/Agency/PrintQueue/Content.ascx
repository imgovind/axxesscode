﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<% var groupName = ViewData["GroupName"].ToString(); %>
<%= Html.Hidden("PrintQueue_GroupName", groupName, new { @id = "PrintQueue_GroupName" })%>
<%= Html.Hidden("PrintQueue_SortName", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]), new { @id = "PrintQueue_SortName" })%>
<%= Html.Telerik().Grid(Model).Name("printQueueGrid").Columns(columns =>
{
    columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}'/>",s.CustomValue)).ClientTemplate("<input name='CustomValue' type='checkbox' value='<#= CustomValue #>'/>").Title("").Width(30).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
    columns.Bound(s => s.PatientName);
    columns.Bound(s => s.EventDate).Width(100);
    columns.Bound(s => s.TaskName).Title("Task").Width(250);
    columns.Bound(s => s.Status).Width(200);
    columns.Bound(s => s.UserName).Width(150);
    columns.Bound(s => s.PrintUrl).Template(s =>s.PrintUrl).Width(30).Title("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false);
})
        .Groupable(settings => settings.Groups(groups =>
        {

            if (groupName == "PatientName")
            {
                groups.Add(s => s.PatientName);
            }
            else if (groupName == "EventDate")
            {
                groups.Add(s => s.EventDate);
            }
            else if (groupName == "DisciplineTaskName")
            {
                groups.Add(s => s.TaskName);
            }
            else if (groupName == "UserName")
            {
                groups.Add(s => s.UserName);
            }
            else
            {
                groups.Add(s => s.EventDate);
            }
        })).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order =>
        {
            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
            if (sortName == "PatientName")
            {
                if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
            }
            else if (sortName == "EventDate")
            {
                if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
            }
            else if (sortName == "TaskName")
            {
                if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
            }
            else if (sortName == "UserName")
            {
                if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
            }
        }))
        .Scrollable().Sortable().Footer(false)%>
<script type="text/javascript">
    $("#printQueueGrid .t-group-indicator").hide();
    $("#printQueueGrid .t-grouping-header").remove();
    $(".t-grid-content", "#printQueueGrid").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    $("#printQueueGrid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Agency.LoadPrintQueue('<%=groupName %>','" + U.ParameterByName(link, 'printQueueGrid-orderBy') + "');");
    });
    Agency.AfterPrintQueueDataBound('<%=Model!=null? Model.Count: 0 %>');
</script>