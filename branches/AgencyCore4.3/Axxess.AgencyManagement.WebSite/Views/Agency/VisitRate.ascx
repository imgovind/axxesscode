﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Visit Rates | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("EditCost", "Agency", FormMethod.Post, new { @id = "editVisitCostForm" })){ %>
<div class="wrapper main">
    <fieldset>
        <div class="align-center">
            <div class="row"><label for="Edit_VisitRate_LocationId" class="float-left">Agency Branch:</label><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.Id.ToString(), new { @id = "Edit_VisitRate_LocationId", @class = "BranchLocation required" }) %></div>
        </div>
    </fieldset>
    <div id="Edit_VisitRate_Container" class="row"><% Html.RenderPartial("~/Views/Agency/VisitRateContent.ascx", Model); %></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('visitrates');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Agency.LoadVisitRateContent($('#Edit_VisitRate_LocationId').val());
    $('#Edit_VisitRate_LocationId').change(function() { Agency.LoadVisitRateContent($(this).val());});
    $("#editVisitCostForm .row :input.required").closest(".row").prepend("<span class='req-red abs-right'>*</span>");
</script>