﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Order History | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg" >
        <div class="buttons float-right"><ul><li><%= Html.ActionLink("Export to Excel", "OrdersHistory", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersHistory_ExportLink", @class = "excel" })%></li></ul></div>
            <fieldset class="orders-filter">
            <div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindOrdersHistory();">Generate</a></li></ul></div>
                <label class="float-left">Branch:</label><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersHistory_BranchId" })%>
                <label class="strong">Date Range:</label><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersHistory_StartDate" /><label class="strong">To</label><input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersHistory_EndDate" />
            </fieldset>    
        
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersHistory").HtmlAttributes(new { @style = "top:48px;" }).DataKeys(keys =>
{
            keys.Add(o => o.Id).RouteKey("id");
            keys.Add(o => o.Type).RouteKey("type");
            keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
            keys.Add(o => o.SendDate).RouteKey("sendDate");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.HasPhysicianAccess).Title("Electronic").Width(65).Sortable(true).ReadOnly();
            columns.Bound(o => o.OrderDate).Title("Order Date").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.SendDateFormatted).Format("{0:MM/dd/yyyy}").Title("Sent Date").Width(70).Sortable(true);
            columns.Bound(o => o.ReceivedDateFormatted).Format("{0:MM/dd/yyyy}").Title("Received Date").Width(80).Sortable(true);
            columns.Bound(o => o.PhysicianSignatureDateFormatted).Format("{0:MM/dd/yyyy}").Title("MD Sign Date").Width(80).Sortable(true);
            columns.Bound(o => o.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowOrdersHistoryModal('<#=Id#>','<#=PatientId#>','<#=EpisodeId#>','<#=Type#>');\">Edit</a>").Title("Action").Width(45).Visible(!Current.IsAgencyFrozen);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryList", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<div id="OrdersHistory_Edit_Container" class="ordershistoryeditmodal hidden"></div>
<script type="text/javascript">
    $("#List_OrdersHistory .t-grid-content").css({ 'height': 'auto' });
    $("#List_OrdersHistory .t-toolbar.t-grid-toolbar").empty();
    $("#window_ordersHistory_content").css({
        "background-color": "#d6e5f3"
    });
</script>