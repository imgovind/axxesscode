﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateBillData", "Agency", FormMethod.Post, new { @id = "editInsuranceBillData" })) { %>
    <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = "Edit_BillData_InsuranceId" })%>
    <fieldset class="newmed">
        <legend>Edit Visit Information</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Edit_BillData_Task" class="float-left">Task:</label>
                <div class="float-right"><%=Html.InsuranceDisciplineTask("Id", Model.Id, Model.InsuranceId, true, new { @id = "Edit_BillData_Task", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_Description" class="float-left">Preferred Description:</label>
                <div class="float-right"><%= Html.TextBox("PreferredDescription", Model.PreferredDescription, new { @id = "Edit_BillData_Description", @class = "text input_wrapper required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_RevenueCode" class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "Edit_BillData_RevenueCode", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_HCPCS" class="float-left">HCPCS Code:</label>
                <div class="float-right"><%= Html.TextBox("Code", Model.Code, new { @id = "Edit_BillData_HCPCS", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row" id="Edit_BillData_ExpectedRate_Div">
                <label for="Edit_BillData_ExpectedRate" class="float-left">Expected Rate:</label>
                <div class="float-right"><%= Html.TextBox("ExpectedRate", Model.ExpectedRate, new { @id = "Edit_BillData_ExpectedRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_ChargeRate" id="Edit_BillData_ChargeRate_Label" class="float-left">Rate:</label>
                <div class="float-right"><%= Html.TextBox("Charge", Model.Charge, new { @id = "Edit_BillData_ChargeRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_BillData_Modifier" class="float-left">Modifier:</label>
                <div class="float-right">
                    <%= Html.TextBox("Modifier", Model.Modifier, new { @id = "Edit_BillData_Modifier", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", Model.Modifier2, new { @id = "Edit_BillData_Modifier2", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", Model.Modifier3, new { @id = "Edit_BillData_Modifier3", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", Model.Modifier4, new { @id = "Edit_BillData_Modifier4", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_BillData_ChargeType" class="float-left">Service Unit Type:</label>
                <div class="float-right"><%=Html.UnitType("ChargeType", Model.ChargeType, new { @id = "Edit_BillData_ChargeType", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class='row <%= Model.ChargeType=="1" ? "" : "hidden" %>' id="Edit_BillData_PerVisitUnitContent">
                <label for="Edit_BillData_PerVisitUnit" class="float-left">Service Units per Visit:</label>
                <div class="float-right"><%= Html.TextBox("Unit", Model.Unit, new { @id = "Edit_BillData_PerVisitUnit", @class = string.Format("text input_wrapper sn {0}", (Model.ChargeType=="1" ? "required" : "" )), @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit.</em>
            </div>
            <% if (Model.IsMedicareHMO)
               { %>
            <% var isRateHidden = (Model.ChargeType == "2" || Model.ChargeType == "3") ? false : true;%>
            <div class='row <%= isRateHidden ? "hidden" : "" %>' id="Edit_BillData_MedicareHMORateContent">
                <label for="Edit_BillData_ChargeRate" class="float-left">Medicare HMO Rate:</label>
                <div class="float-right"><%= Html.TextBox("MedicareHMORate", Model.MedicareHMORate, new { @id = "Edit_BillData_MedicareHMORate", @class = string.Format("text input_wrapper {0}", !isRateHidden ? "required" : ""), @maxlength = "100" })%></div>
                <div class="clear"></div>
                <em>This rate applies for the Medicare final claim per visit if the service unit type is not per a visit.</em>
            </div>
            <%} %>
            <div id="Edit_BillData_IsTimeLimitContent" class='<%= (Model.ChargeType == "3" || Model.ChargeType == "2") ? "" : "hidden" %>'>
                <div class="row">
                    <%= Html.CheckBox("IsTimeLimit", Model.IsTimeLimit, new { @id = "Edit_BillData_IsTimeLimit", @class = "radio float-left" })%>
                    <label for="Edit_BillData_IsTimeLimit">Check if a time limit applies per unit.</label>
                </div>
                <div class='margin <%= Model.IsTimeLimit ? "" : "hidden" %>' id="Edit_BillData_TimeLimitContent">
                    <div class="row">
                        <label for="Edit_BillData_TimeLimit" class="float-left">Time Limit:</label>
                        <div class="float-right">
                            Hour:<%= Html.TextBox("TimeLimitHour", Model.TimeLimitHour, new { @id = "Edit_BillData_TimeLimitHour", @class = "text  sn", @maxlength = "2" })%>
                            MIN:<%= Html.TextBox("TimeLimitMin", Model.TimeLimitMin, new { @id = "Edit_BillData_TimeLimitMin", @class = "text input_wrapper sn", @maxlength = "2" })%>
                        </div>
                        <div class="clear"></div>
                        <em>This applies for visit times which exceed the insurance provider limit</em>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondDescription" class="float-left">Description:</label>
                        <div class="float-right"><%= Html.TextBox("SecondDescription", Model.SecondDescription, new { @id = "Edit_BillData_SecondDescription", @class = "text", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondRevenueCode" class="float-left">Revenue Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondRevenueCode", Model.SecondRevenueCode, new { @id = "Edit_BillData_SecondRevenueCode", @class = "text sn ", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondHCPCS" class="float-left">HCPCS Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondCode", Model.SecondCode, new { @id = "Edit_BillData_SecondHCPCS", @class = "text sn", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsSecondChargeDifferent", Model.IsSecondChargeDifferent, new { @id = "Edit_BillData_IsSecondChargeDifferent", @class = "radio float-left" })%>
                         <label for="Edit_BillData_IsSecondChargeDifferent">Check if a rate is different.</label>
                    </div>
                    <div id="Edit_BillData_SecondRateContent" class="<%= Model.IsSecondChargeDifferent ? "" : "hidden" %>">
                        <div class='row'  id="Edit_BillData_SecondExpectedRateContent">
                             <label for="Edit_BillData_SecondExpectedRate" class="float-left">Expected Rate:</label>
                             <div class="float-right"><%= Html.TextBox("SecondExpectedRate", Model.SecondExpectedRate, new { @id = "Edit_BillData_SecondExpectedRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
                        </div>
                        <div class='row'  id="Edit_BillData_SecondChargeContent">
                             <label for="Edit_BillData_SecondCharge" class="float-left" id="Edit_BillData_SecondChargeLabel">Rate:</label>
                             <div class="float-right"><%= Html.TextBox("SecondCharge", Model.SecondCharge, new { @id = "Edit_BillData_SecondCharge", @class = "text input_wrapper", @maxlength = "100" })%></div>
                        </div>
                     </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondModifier" class="float-left">Modifier:</label>
                        <div class="float-right">
                            <%= Html.TextBox("SecondModifier", Model.SecondModifier, new { @id = "Edit_BillData_SecondModifier", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", Model.SecondModifier2, new { @id = "Edit_BillData_SecondModifier2", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", Model.SecondModifier3, new { @id = "Edit_BillData_SecondModifier3", @class = "text insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", Model.SecondModifier4, new { @id = "Edit_BillData_SecondModifier4", @class = "text insurance-modifier", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Edit_BillData_SecondChargeType" class="float-left">Unit Type:</label>
                        <div class="float-right"><%=Html.UnitType("SecondChargeType", Model.SecondChargeType, new { @id = "Edit_BillData_SecondChargeType", @class = "text input_wrapper" })%></div>
                   </div>
                    <div class="row <%= Model.SecondChargeType=="1" ? "" : "hidden" %>" id="Edit_BillData_SecondPerVisitUnitContent">
                        <label for="Edit_BillData_SecondPerVisitUnit" class="float-left">Service Units per Visit:</label>
                        <div class="float-right"><%= Html.TextBox("SecondUnit", Model.SecondUnit, new { @id = "Edit_BillData_SecondPerVisitUnit", @class = string.Format("text input_wrapper sn {0}", (Model.SecondChargeType == "1" ? "required" : "")), @maxlength = "1" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsUnitPerALineItem", Model.IsUnitPerALineItem, new { @id = "Edit_BillData_IsUnitPerALineItem", @class = "radio float-left" })%>
                         <label for="Edit_BillData_IsUnitPerALineItem">Check if a unit apply per a line item.</label>
                    </div>
                </div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false'>Save &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#Edit_BillData_ChargeType").change(function() {
        var chargeTypevalue=$(this).val();
        if (chargeTypevalue == 3 || chargeTypevalue == 2 ) 
        {$("#Edit_BillData_IsTimeLimitContent").show();}
        else 
        {$("#Edit_BillData_IsTimeLimitContent").hide();}
        
        if (chargeTypevalue == 1) {
            $("#Edit_BillData_PerVisitUnitContent").show();
            $("#Edit_BillData_PerVisitUnit").addClass("required");
             <% if(Model.IsMedicareHMO){ %>$("#Edit_BillData_MedicareHMORateContent").hide().find("input[name=MedicareHMORate]").removeClass("required");<%} %>
        } else {
            $("#Edit_BillData_PerVisitUnitContent").hide();
            $("#Edit_BillData_PerVisitUnit").removeClass("required");
            if(chargeTypevalue!=0){<% if(Model.IsMedicareHMO){ %>$("#Edit_BillData_MedicareHMORateContent").show().find("input[name=MedicareHMORate]").addClass("required");<%} %>}
        }
    });
    
    if ($("#Edit_BillData_IsTimeLimit").is(":checked")) 
    {$("#window_ModalWindow").css({ height: 540, "margin-top": -80 });}
    
    $("#Edit_BillData_IsTimeLimit").click(function() {
        if ($(this).is(":checked"))
         {$("#window_ModalWindow").animate({ height: 540, "margin-top": -62 }, 500, function() {$("#Edit_BillData_TimeLimitContent").show();})} 
        else
         {$("#Edit_BillData_TimeLimitContent").hide();$("#window_ModalWindow").animate({ height: 385, "margin-top": 0 }, 500)}
    });

    if ($("#Edit_BillData_IsSecondChargeDifferent").is(":checked"))
     {$("#window_ModalWindow").css({ height: 570, "margin-top": -80 });}
     
    $("#Edit_BillData_IsSecondChargeDifferent").click(function() {
        if ($(this).is(":checked")) 
        {$("#window_ModalWindow").animate({ height: 570, "margin-top": -62 }, 500, function() {$("#Edit_BillData_SecondRateContent").show();})}
        else 
        {$("#Edit_BillData_SecondRateContent").hide();$("#window_ModalWindow").animate({ height: 540, "margin-top": -62 }, 500)}
    });
    
    $("#Edit_BillData_SecondChargeType").change(function() {
         var secondChargeTypevalue=$(this).val();
        if (secondChargeTypevalue == 1) 
        {$("#Edit_BillData_SecondPerVisitUnitContent").show();$("#Edit_BillData_SecondPerVisitUnit").addClass("required");} 
        else 
        {$("#Edit_BillData_SecondPerVisitUnitContent").hide();$("#Edit_BillData_SecondPerVisitUnit").removeClass("required");}
    });
    $("#Edit_BillData_Task").change(function (e) { 
       var value = $("#Edit_BillData_Task option:selected").text();
       $("#Edit_BillData_Description").val(value);
    });
    U.ShowIfChecked($("#Edit_Insurance_HasContractWithAgency"),$("#Edit_BillData_ExpectedRate_Div"));
    U.ShowIfChecked($("#Edit_Insurance_HasContractWithAgency"),$("#Edit_BillData_SecondExpectedRateContent"));
    if($("#Edit_BillData_ExpectedRate_Div:visible").length > 0)
    {
        $("#Edit_BillData_ChargeRate_Label").text("Billed Rate");
        $("#Edit_BillData_SecondChargeLabel").text("Billed Rate");
    }
</script>