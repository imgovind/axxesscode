﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTemplate>" %>
<span class="wintitle">Edit Template | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Update", "Template", FormMethod.Post, new { @id = "editTemplateForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Template_Id" }) %>
<div class="wrapper main">
    
    <fieldset>
       <div class="wide-column">
         <div><label for="Edit_Template_Title" class="strong">Name</label><br /><%= Html.TextBox("Title", Model.Title, new { @id = "Edit_Template_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%><span class='required-red'>*</span></div><br />
         <div><label for="Edit_Template_Text" class="strong">Text</label><br /><textarea id="Edit_Template_Text" name="Text" cols="5" rows="6" style="height:200px" class="fill" maxcharacters="5000"><%= Model.Text %></textarea></div>
       </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadTemplateLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edittemplate');">Exit</a></li>
    </ul></div>
</div>
<% } %>
