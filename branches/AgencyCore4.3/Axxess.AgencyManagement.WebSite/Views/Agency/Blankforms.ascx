﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="form-wrapper">
    <fieldset>
        <legend>OASIS Forms</legend>
        <div class="column">
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','Nursing');return false" >OASIS-C Start of Care</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a  href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','Nursing');return false">OASIS-C Resumption of Care</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','Nursing');return false">OASIS-C Recertification</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','Nursing');return false">OASIS-C Follow-Up</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferInPatientNotDischarged %>','Nursing');return false">OASIS-C Transfer Not Discharged</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferInPatientNotDischarged %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferInPatientNotDischarged %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferInPatientDischarged %>','Nursing');return false">OASIS-C Transfer and Discharge</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferInPatientDischarged %>','PT');return false">(PT)</a>
            </div>
        </div><div class="column">
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgencyDeath %>','Nursing');return false">OASIS-C Death at Home</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgencyDeath %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgencyDeath %>','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgency %>','Nursing');return false">OASIS-C Discharge from Agency</a>
                &#160;&#8212;&#160;
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgency %>','PT');return false">(PT)</a>
                <a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.DischargeFromAgency %>','OT');return false">(OT)</a>
            </div>
            <div class="row"><a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOasisStartOfCare %>','Nursing');return false">Non-OASIS Start of Care</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOasisRecertification %>','Nursing');return false">Non-OASIS Recertification</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOasisDischarge %>','Nursing');return false">Non-OASIS Discharge from Agency</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Forms</legend>
        <div class="column">
            <div class="row"><a href="Patient/ViewFaceToFaceEncounterPdfBlank" target="_blank">Face to Face Encounter</a></div>
        </div><div class="column">
            <div class="row"><a href="Patient/PhysicianOrderPdfBlank" target="_blank">Physician Order</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/TransferSummaryPdfBlank" target="_blank">Transfer Summary</a></div>
            <div class="row"><a href="Schedule/CoordinationOfCarePdfBlank" target="_blank">Coordination of Care</a></div>
            <div class="row"><a href="Schedule/SixtyDaySummaryPdfBlank" target="_blank">60 Day Summary</a></div>
            <div class="row"><a href="Schedule/MissedVisitPdfBlank" target="_blank">Missed Visit Form</a></div>
            <div class="row"><a href="Schedule/SNPsychVisitPdfBlank" target="_blank">Skilled Nurse Psych Visit</a></div>
            <div class="row"><a href="Schedule/SNPediatricVisitPdfBlank" target="_blank">Skilled Nurse Pediatric Visit</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/LVNSVisitPdfBlank" target="_blank">LVN Supervisory Visit</a></div>
            <div class="row"><a href="Schedule/DischargeSummaryPdfBlank" target="_blank">SN Discharge Summary</a></div>
            <div class="row"><a href="Schedule/SNVisitPdfBlank" target="_blank">Skilled Nurse Visit</a></div>
            <div class="row"><a href="Schedule/WoundCarePdfBlank" target="_blank">Wound Care</a></div>
            <div class="row"><a href="Schedule/SNPsychAssessmentPdfBlank" target="_blank">Skilled Nurse Psych Assessment</a></div>
            <div class="row"><a href="Schedule/SNPediatricAssessmentPdfBlank" target="_blank">Skilled Nurse Pediatric Assessment</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Home Health Aide Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/HHACarePlanPdfBlank" target="_blank">HHA Care Plan</a></div>
            <div class="row"><a href="Schedule/HHAideVisitBlank" target="_blank">HHA Visit Note</a></div>
            <div class="row"><a href="Schedule/HHASVisitPdfBlank" target="_blank">HHA Supervisory Visit</a></div>
            <div class="row"><a href="Schedule/HomeMakerNotePdfBlank" target="_blank">Home Maker Note</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/PASCarePlanBlank" target="_blank">PAS Care Plan</a></div>
            <div class="row"><a href="Schedule/PASVisitBlank" target="_blank">PAS Note</a></div>
            <div class="row"><a href="Schedule/PASTravelBlank" target="_blank">PAS Travel Note</a></div>
            <div class="row"><a href="Schedule/UAPWoundCareVisitBlank" target="_blank">UAP Wound Care Visit</a></div>
            <div class="row"><a href="Schedule/UAPInsulinPrepAdminVisitBlank" target="_blank">UAP Insulin Prep/Admin Visit</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Medical Social Work Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/MSWEvaluationAssessmentBlank" target="_blank">MSW Evaluation</a></div>
            <div class="row"><a href="Schedule/MSWProgressNotePdfBlank" target="_blank">MSW Progress Note</a></div>
            <div class="row"><a href="Schedule/MSWVisitPdfBlank" target="_blank">MSW Visit</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/MSWAssessmentPdfBlank" target="_blank">MSW Assessment</a></div>
            <div class="row"><a href="Schedule/MSWDischargePdfBlank" target="_blank">MSW Discharge</a></div>
            <div class="row"><a href="Schedule/TransportationNotePdfBlank" target="_blank">Driver/Transportation Log</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Therapy Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/PTDischargeBlank" target="_blank">PT Discharge</a></div>
            <div class="row"><a href="Schedule/PTEvaluationBlank" target="_blank">PT Evaluation</a></div>
            <div class="row"><a href="Schedule/PTReEvaluationBlank" target="_blank">PT Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/PTMaintenanceBlank" target="_blank">PT Maintenance</a></div>
            <div class="row"><a href="Schedule/PTVisitBlank" target="_blank">PT Visit</a></div>
            <div class="row"><a href="Schedule/PTReassessmentBlank" target="_blank">PT Reassessment</a></div>
            <div class="row"><a href="Schedule/PTAVisitBlank" target="_blank">PTA Visit</a></div>
            <div class="row"><a href="Schedule/OTEvaluationBlank" target="_blank">OT Evaluation</a></div>
            <div class="row"><a href="Schedule/OTReEvaluationBlank" target="_blank">OT Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/OTDischargeBlank" target="_blank">OT Discharge</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/OTMaintenanceBlank" target="_blank">OT Maintenance</a></div>
            <div class="row"><a href="Schedule/OTVisitBlank" target="_blank">OT Visit</a></div>
            <div class="row"><a href="Schedule/OTReassessmentBlank" target="_blank">OT Reassessment</a></div>
            <div class="row"><a href="Schedule/COTAVisitBlank" target="_blank">COTA Visit</a></div>
            <div class="row"><a href="Schedule/STEvaluationBlank" target="_blank">ST Evaluation</a></div>
            <div class="row"><a href="Schedule/STReEvaluationBlank" target="_blank">ST Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/STReassessmentBlank" target="_blank">ST Reassessment</a></div>
            <div class="row"><a href="Schedule/STMaintenanceBlank" target="_blank">ST Maintenance</a></div>
            <div class="row"><a href="Schedule/STDischargeBlank" target="_blank">ST Discharge</a></div>
            <div class="row"><a href="Schedule/STVisitBlank" target="_blank">ST Visit</a></div>
            <div class="row"><a href="Schedule/PTDischargeSummaryPdfBlank" target="_blank">PT Discharge Summary</a></div>
            
        </div>
    </fieldset>
</div>