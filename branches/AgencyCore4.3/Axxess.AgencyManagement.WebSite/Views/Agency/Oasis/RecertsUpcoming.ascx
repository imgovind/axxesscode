﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Upcoming Recerts | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg">
<div class="buttons float-right" ><ul> <li><%if (Current.HasRight(Permissions.ExportListToExcel)){ %><li><%= Html.ActionLink("Export to Excel", "RecertsUpcoming", "Export", new { BranchId = Guid.Empty, InsuranceId = Model }, new { id = "AgencyUpcomingRecet_ExportLink" })%></li><%} %></ul></div>
    
<fieldset class="orders-filter">
    <div class="buttons float-right" ><ul> <li><a href="javascript:void(0);" onclick="Agency.RebindAgencyUpcomingRecet();">Generate</a></li></ul></div>
        <label class="float-left">Branch:</label><div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyUpcomingRecet_BranchCode" })%></div><label class="float-left">Insurance:</label><div class="float-left"><%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyUpcomingRecet_InsuranceId", @class = "Insurances" })%></div> 
    <div class="clear" />
    <div id="AgencyUpcomingRecet_Search"></div> <br /><br />
    </fieldset>
    
    <%= Html.Telerik()
        .Grid<RecertEvent>().Name("List_UpcomingRecerts").HtmlAttributes(new { @style = "top:85px;" })
        .Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.StatusName).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsUpcoming", "Agency", new { BranchId = Guid.Empty, InsuranceId = Model })).Footer(false).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
 </div>
<script type="text/javascript">
    $("#AgencyUpcomingRecet_Search").append($("<div/>").GridSearchById("#List_UpcomingRecerts"));
    $("#window_listupcomingrecerts #List_UpcomingRecerts .t-grid-content").css({ "height": "auto", "top": "26px" });
    $("#window_listupcomingrecerts_content").css({
        "background-color": "#d6e5f3"
    });
    //$('.grid-search').removeClass().css({ 'position': '' });
</script>
