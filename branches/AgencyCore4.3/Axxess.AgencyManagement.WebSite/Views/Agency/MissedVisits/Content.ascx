﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<%var data = ViewData["MissedVisitsGroupName"].ToString();%>
<%= Html.Hidden("MissedVisits_GroupName", data)%>
<%= Html.Hidden("MissedVisits_SortColumn", ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : "")%>
<%= Html.Hidden("MissedVisits_SortDirection", ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : "")%>
<%= Html.Telerik().Grid(Model).Name("List_MissedVisits").Columns(columns =>
{
    columns.Bound(v => v.PatientIdNumber).Title("MRN").Width(90);
    columns.Bound(v => v.PatientName).Title("Patient");
    columns.Bound(v => v.Url).Template(v => v.Url).Title("Task").Width(220);
    columns.Bound(v => v.UserName).Title("Employee");
    columns.Bound(v => v.Status).Title("Original Status").Template(v => Enum.IsDefined(typeof(ScheduleStatus), v.Status) ? ((ScheduleStatus)v.Status).GetDescription() : "");
    columns.Bound(v => v.EventDate).Format("{0:MM/dd/yyyy}").Title("Scheduled Date").Width(105);
    columns.Bound(v => v.VisitDate).Format("{0:MM/dd/yyyy}").Title("Visit Date").Width(80);
    columns.Bound(v => v.StatusComment).Title(" ").Width(30).Template(v => v.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" >{0} </a>", v.StatusComment) : string.Empty).Sortable(false);
    columns.Bound(v => v.Comments).Title(" ").Width(30).Template(v => v.Comments.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", v.Comments) : string.Empty).Sortable(false);
    columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).Template(v => v.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", v.EpisodeNotes) : string.Empty).Sortable(false);
    columns.Bound(v => v.ActionUrl).Template(v => v.ActionUrl).Title("Action").Width(110).Visible(!Current.IsAgencyFrozen).Sortable(false);
}).ClientEvents(c => c.OnRowDataBound("Agency.MissedVisitRowDataBound"))
    .Groupable(settings => settings.Groups(groups =>
    {
        if (data == "PatientName")
        {
            groups.Add(s => s.PatientName);
        }
        else if (data == "EventDate")
        {
            groups.Add(s => s.EventDate);
        }
        else if (data == "VisitDate")
        {
            groups.Add(s => s.VisitDate);
        }
        else if (data == "UserName")
        {
            groups.Add(s => s.UserName);
        }
        else if (data == "DisciplineTaskName")
        {
            groups.Add(s => s.DisciplineTaskName);
        }
        else if (data == "UserName")
        {
            groups.Add(s => s.UserName);
        }
        else
        {
            groups.Add(s => s.VisitDate);
        }
    }))
        .Scrollable().Sortable(sorting =>
                    sorting.SortMode(GridSortMode.SingleColumn)
                        .OrderBy(order =>
                        {
                            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                            if (sortName == "PatientName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.PatientName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.PatientName).Descending();
                                }
                            }
                            else if (sortName == "PatientIdNumber")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.PatientIdNumber).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.PatientIdNumber).Descending();
                                }
                            }
                            else if (sortName == "UserName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.UserName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.UserName).Descending();
                                }
                            }
                            else if (sortName == "EventDate")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.EventDate).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.EventDate).Descending();
                                }
                            }
                            else if (sortName == "VisitDate")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.VisitDate).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.VisitDate).Descending();
                                }
                            }
                            else if (sortName == "Url")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.Url).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.Url).Descending();
                                }
                            }
                            else if (sortName == "Status")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.Status).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.Status).Descending();
                                }
                            }
                        })
                )%>
<script type="text/javascript">

    $("#List_MissedVisits .t-group-indicator").hide();
    $("#List_MissedVisits .t-grouping-header").remove();
    $("#List_MissedVisits .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
    $("#List_MissedVisits div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Agency.LoadMissedVisits({ BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() },'<%=data %>','" + U.ParameterByName(link, 'List_MissedVisits-orderBy') + "');");
    });
    Agency.ToolTip("#List_MissedVisits");
</script>