﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibilitySummary>>" %>
<% string pagename = "MedicareEligibility"; %>

<%= Html.Telerik().Grid(Model).Name("MedicareEligibilityGrid").Columns(columns =>
{
    columns.Bound(i => i.Text).Sortable(false).Title("");
    columns.Bound(i => i.CreatedFormatted).Title("Created").Sortable(true);
    columns.Bound(i => i.PrintUrl).ClientTemplate("<#=PrintUrl#>").Encoded(false).Sortable(false).Title("").Width(40);
    
}).Scrollable(scrolling => scrolling.Enabled(true)).Footer(false).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
            if (sortName == "CreatedFormatted")
            {
                if (sortDirection == "ASC")
                {
                    order.Add(o => o.CreatedFormatted).Ascending();
                }
                else if (sortDirection == "DESC")
                {
                    order.Add(o => o.CreatedFormatted).Descending();
                }
            }
        })
    )
%>

<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Agency.RebindMedicareEligibilitySummaryGridContent('<%= pagename %>','MedicareEligibilityContent',{ StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
</script>