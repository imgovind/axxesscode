﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<% 
    Html.Telerik().Grid(Model).Name("QACenterActivityGrid").Columns(columns => {
        columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}'/>", s.CustomValue)).Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
        columns.Bound(s => s.PatientName);
        columns.Bound(s => s.EventDate).Width(100);
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").Title("Task").Width(250);
        columns.Bound(s => s.Status).Width(200).Sortable(false);
        columns.Bound(s => s.RedNote).Title(" ").Width(30).Template(s => s.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", s.RedNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.YellowNote).Title(" ").Width(30).Template(s => s.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.YellowNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.BlueNote).Title(" ").Width(30).Template(s => s.BlueNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.BlueNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.UserName).Width(150);
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).Scrollable().Footer(false).Render(); %>
   <%--Html.Telerik().Grid<ScheduleEvent>().Name("QACenterActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDateSortable).Title("Scheduled Date").ClientTemplate("<#=EventDateSortable#>").Width(105);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.OasisProfileUrl).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(30);
       columns.Bound(s => s.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
       columns.Bound(s => s.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
       columns.Bound(s => s.AttachmentUrl).Title(" ").ClientTemplate("<#=AttachmentUrl#>").Width(30);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200);
       columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Agency.ActivityRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("CaseManagementActivity", "Agency", new { patientId = val, discipline = "All", dateRangeId = "ThisEpisode", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now })).Sortable().Scrollable().Footer(false).Render();--%>
%>
