﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<%  using (Html.BeginForm("Update", "Referral", FormMethod.Post, new { @id = "editReferralForm" })) { %>
<span class="wintitle">Edit Referral | <%= Model.DisplayName %></span>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Referral_Id" }) %>
<div class="wrapper main">
    
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_Physician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "Edit_Referral_Physician", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "Edit_Referral_AdmissionSource", @class = "AdmissionSource requireddropdown required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_Date" class="float-left">Referral Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="ReferralDate" value="<%= Model.ReferralDate.ToShortDateString() %>" id="Edit_Referral_Date" /></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_InternalReferral" class="float-left">Internal Referral Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Referral_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Referral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_MiddleInitial" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial", Model.MiddleInitial, new { @id = "Edit_Referral_MiddleInitial", @class = "text input_wrapper mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Referral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "Edit_Referral_Gender_F", @class = "radio required" }) %>
                    <label for="Edit_Referral_Gender_F" class="inlineradio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "Edit_Referral_Gender_M", @class = "radio required" })%>
                    <label for="Edit_Referral_Gender_M" class="inlineradio">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Referral_DateOfBirth" class="float-left">Date of Birth:</label>
                <div class="float-right"><%= Html.TextBox("DOB", Model.DOB.ToShortDateString(), new { @id = "Edit_Referral_DateOfBirth", @class = "required date" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right">
                     <%= Html.MartialStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "** Select **", "0", new { @id = "Edit_Referral_MaritalStatus", @class = "input_wrapper" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Height</label>
                <div class="float-right">
                    <%= Html.TextBox("Height", Model.Height, new { @id = "Edit_Referral_Height", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", Model.HeightMetric == 0, new { @id = "Edit_Referral_HeightMetric0", @class = "radio" })%>
                    <label for="Edit_Referral_HeightMetric0" style="display:inline-block;width:20px!important">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model.HeightMetric == 1, new { @id = "Edit_Referral_HeightMetric1", @class = "radio" })%>
                    <label for="Edit_Referral_HeightMetric1" style="display:inline-block;width:20px!important">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Weight</label>
                <div class="float-right">
                    <%= Html.TextBox("Weight", Model.Weight, new { @id = "Edit_Referral_Weight", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model.WeightMetric == 0, new { @id = "Edit_Referral_WeightMetric0", @class = "radio" })%>
                    <label for="Edit_Referral_WeightMetric0" style="display:inline-block;width:20px!important">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model.WeightMetric == 1, new { @id = "Edit_Referral_WeightMetric1", @class = "radio" })%>
                    <label for="Edit_Referral_WeightMetric1" style="display:inline-block;width:20px!important">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Referral_Assign" class="float-left">Assign to Clinician:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Edit_Referral_Assign", @class = "Users required valid" })%></div>
            </div>
             <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowPatientEligibility($('#Edit_Referral_MedicareNo').val(), $('#Edit_Referral_LastName').val(),$('#Edit_Referral_FirstName').val(),$('#Edit_Referral_DateOfBirth').val(),$('input[name=Gender]:checked').val());">Verify Medicare Eligibility</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_MedicareNo" class="float-left">Medicare No:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Referral_MedicareNo", @maxlength = "12", @class = "text MedicareNo" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_MedicaidNo" class="float-left">Medicaid No:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Referral_MedicaidNo", @maxlength = "20", @class = "text MedicaidNo" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_SSN" class="float-left">SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Referral_SSN", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Referral_AddressLine1", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Referral_AddressLine2", @maxlength = "50", @class = "text input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Referral_AddressCity", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Referral_AddressStateCode", @class = "AddressStateCode required valid" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Referral_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 3 ? Model.PhoneHome.Substring(0, 3) : string.Empty, new { @id = "Edit_Referral_HomePhone1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" }) %>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 6 ? Model.PhoneHome.Substring(3, 3) : string.Empty, new { @id = "Edit_Referral_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10 ? Model.PhoneHome.Substring(6, 4) : string.Empty, new { @id = "Edit_Referral_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Referral_Email" class="float-left">Email Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Referral_Email", @class = "text email input_wrapper" })%></div>
            </div>
            <div class="row">
                <label class="float-left">DNR:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsDNR", "true", Model.IsDNR, new { @id = "Edit_Referral_IsDNR1", @class = "radio" })%>
                    <label for="Edit_Patient_IsDNR1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsDNR", "false", !Model.IsDNR, new { @id = "Edit_Referral_IsDNR2", @class = "radio" })%>
                    <label for="Edit_Patient_IsDNR2" class="inline-radio">No</label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <table class="form">
            <tbody>
                <%  string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null; %>
                <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <tr>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%>
                        <label for="ServicesRequiredCollection0" class="radio">SN</label>
                    </td>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection1" class="radio">HHA</label>
                    </td>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection2" class="radio">PT</label>
                    </td>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection3" class="radio">OT</label>
                    </td>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection4" class="radio">SP</label>
                    </td>
                    <td>
                        <%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection5" class="radio">MSW</label>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <table class="form">
            <%  string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null; %>
            <input type="hidden" value=" " class="radio" name="DMECollection" />
            <tbody>
                <tr class="firstrow">
                    <td>
                        <%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                        <label for="DMECollection0" class="radio">Bedside Commode</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                        <label for="DMECollection1" class="radio">Cane</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                        <label for="DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                        <label for="DMECollection3" class="radio">Grab Bars</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                        <label for="DMECollection4" class="radio">Hospital Bed</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                        <label for="DMECollection5" class="radio">Nebulizer</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                        <label for="DMECollection6" class="radio">Oxygen</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                        <label for="DMECollection7" class="radio">Tub/Shower Bench</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                        <label for="DMECollection8" class="radio">Walker</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                        <label for="DMECollection9" class="radio">Wheelchair</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                        <label for="DMECollection10" class="radio">Other</label>
                        <%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Referral_DMEOther", @class = "text", @style = "display:none;" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset class="">
        <%= Model.EmergencyContact != null ? Html.Hidden("EmergencyContact.Id", Model.EmergencyContact.Id) : MvcHtmlString.Empty %>
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_EmergencyContactFirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.FirstName", Model.EmergencyContact != null ? Model.EmergencyContact.FirstName : "", new { @id = "Edit_Referral_EmergencyContactFirstName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_EmergencyContactLastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.LastName", Model.EmergencyContact != null ? Model.EmergencyContact.LastName : "", new { @id = "Edit_Referral_EmergencyContactLastName", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_EmergencyContactRelationship" class="float-left">Relationship:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.Relationship", Model.EmergencyContact != null ? Model.EmergencyContact.Relationship : "", new { @id = "Edit_Referral_EmergencyContactRelationship", @class = "text input_wrapper" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_EmergencyContactPhonePrimary1" class="float-left">Primary Phone:</label>
                <div class="float-right">
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 3 ? Model.EmergencyContact.PrimaryPhone.Substring(0, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="Edit_Referral_EmergencyContactPhonePrimary1" maxlength="3" /> 
                    - 
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 6 ? Model.EmergencyContact.PrimaryPhone.Substring(3, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="Edit_Referral_EmergencyContactPhonePrimary2" maxlength="3" /> 
                    - 
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 10 ? Model.EmergencyContact.PrimaryPhone.Substring(6, 4) : "" %>" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhonePrimaryArray" id="Edit_Referral_EmergencyContactPhonePrimary3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_EmergencyContactPhoneAlternate1" class="float-left">Alternate Phone:</label>
                <div class="float-right">
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 3 ? Model.EmergencyContact.AlternatePhone.Substring(0, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="Edit_Referral_EmergencyContactPhoneAlternate1" maxlength="3" />
                    - 
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 6 ? Model.EmergencyContact.AlternatePhone.Substring(3, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="Edit_Referral_EmergencyContactPhoneAlternate2" maxlength="3" />
                    -
                    <input type="text" value="<%= Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 10 ? Model.EmergencyContact.AlternatePhone.Substring(6, 4) : "" %>" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhoneAlternateArray" id="Edit_Referral_EmergencyContactPhoneAlternate3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_EmergencyContactEmail" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.EmailAddress", Model.EmergencyContact != null ? Model.EmergencyContact.EmailAddress : "", new { @id = "Edit_Referral_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="medication">
        <legend>Physicians</legend>
        <div class="wide-column">
            <div class="row">
                <div class="float-left">
                    <%= Html.TextBox("Physicians", "", new { @id = "EditReferral_PhysicianSelector", @class = "Physicians" }) %>
                </div>
                <div class="buttons float-left">
                    <ul>
                        <li>
                            <a href="javascript:void(0);" id="EditReferral_NewPhysician">Add Selected Physician</a>
                        </li>
                    </ul>
                </div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <%= Html.Telerik().Grid<ReferalPhysician>().Name("EditReferral_PhysicianGrid").Columns(columns => {
                columns.Bound(p => p.FirstName);
                columns.Bound(p => p.LastName);
                columns.Bound(p => p.WorkPhone);
                columns.Bound(p => p.FaxNumber);
                columns.Bound(p => p.EmailAddress);
                columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Referral.DeletePhysician('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');\" class=<#= !IsPrimary ? \"\" : \"hidden\" #>><#=IsPrimary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Referral", new { ReferralId = Model.Id })).Sortable().Footer(false) %>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <%= Html.Templates("Edit_Referral_CommentsTemplates", new { @class = "Templates", @template = "#Edit_Referral_Comments" })%>
            <div class="row">
                <p class="charsRemaining"></p>
                <textarea style="height:150px;" id="Edit_Referral_Comments" name="Comments" cols="5" rows="6" maxcharacters="2000"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadReferralLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editreferral');">Exit</a></li>
    </ul></div>
</div>
<%= string.Format("<script type='text/javascript'> if({0}==1) {{ $(\"#Edit_Referral_DMEOther\").show();}} else {{ $(\"#Edit_Referral_DMEOther\").hide(); }}</script>", DME != null && DME.Contains("9") ? 1 : 0) %>
<% } %>
<!--[if !IE]>end forms<![endif]-->
<script type="text/javascript">
    $("#DMECollection10").click(function() {
        var otherDme = $('#DMECollection10:checked').is(':checked');
        if (!otherDme) $("#Edit_Referral_DMEOther").hide();
        else $("#Edit_Referral_DMEOther").show();
    })
</script>