﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<span class="wintitle">Schedule Center | <%= Current.AgencyName %></span>
<% string[] stabs = new string[] { "Nursing", "HHA","Therapy"}; %>
<div class="wrapper layout">
    <div class="layout-left">
        <div class="top">
            <div class="buttons heading"><ul><% if (Current.HasRight(Permissions.ManagePatients) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="javascript:Acore.Open('newpatient');" title="Add New Patient">Add New Patient</a></li><% } %></ul></div>
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @id = "ScheduleCenter_BranchId", @class = "ScheduleBranchCode filterInput" })%></div></div>
            <div class="row"><label>View:</label><div><%=Html.PatientStatusList("StatusId", Model.PatientListStatus.ToString(), true, false, "", new { @id = "ScheduleCenter_StatusId", @class = "ScheduleStatusDropDown filterInput" })%> </div></div>
            <div class="row"><label>Filter:</label><div><select name="PaymentSourceId" id = "ScheduleCenter_PaymentSourceId" class="SchedulePaymentDropDown filterInput"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs</option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Schedule_Selection" class="text filterInput" name="SearchText" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("/Views/Schedule/Patients.ascx", Model.Patients); %></div>
    </div>
    <div id="ScheduleMainResult" class="layout-main"><% if (Model != null || Model.Count > 0)
                                                        { %>
     <%  Html.RenderPartial("/Views/Schedule/Data.ascx", Model.CalendarData); %>
    <script type="text/javascript">
        Schedule.PatientListFirstDataBound('<%= Model.PatientId %>', 'ScheduleSelectionGrid');
    </script>
    
    <% }else{%><div class="abs center"><p>No Patients found that fit your search criteria.</p></div><%} %></div>
</div>