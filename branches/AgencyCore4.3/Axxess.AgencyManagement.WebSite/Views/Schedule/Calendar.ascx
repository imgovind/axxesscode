﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  if (Model != null) { %>
    <%  if (Model.IsEpisodeExist) { %>
        <%  var scheduleEvents = Model.ScheduleEvents != null  && Model.ScheduleEvents.Count>0 ? Model.ScheduleEvents.Where(s => s.EventDate.Date >= Model.StartDate.Date && s.EventDate.Date <= Model.EndDate.Date).OrderBy(o => o.EventDate.Date).ToList() : new List<ScheduleEvent>(); %>
        <%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
        <%  startdate[0] = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
        <%  enddate[0] = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
        <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
        <%  startdate[1] = enddate[0].AddDays(1); %>
        <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
        <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
        <%  startdate[2] = enddate[1].AddDays(1); %>
        <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
        <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
        <%= Html.Hidden("SchedulePatientID", Model.PatientId, new { @id = "SchedulePatientID" })%>
        <%= Html.Hidden("ScheduleEpisodeID", Model.EpisodeId, new { @id = "ScheduleEpisodeID" })%>
<div class="trical">
     <div class="window-menu">
        <ul class="">
            <li>
                <%  if (!Model.IsDischarged && Current.HasRight(Permissions.EditEpisode) && !Current.IsAgencyFrozen) { %>
                    <li>
                        <a href="javascript:void(0);" onclick="UserInterface.ShowNewEpisodeModal('<%= Model.PatientId %>');" title="Add New Episode">New Episode</a>
                    </li>
                <% } %>
            </li>
            <li>
                <%  if (Current.HasRight(Permissions.ScheduleVisits)&& !Current.IsAgencyFrozen) { %>
                <li>
                    <a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDayScheduleModal('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');" title="Multiple Employee">Schedule Employee</a>
                </li>
                <%  } %>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-trigger">Episode Manager</a>
                <ul class="menu">
                    <%  if (Current.HasRight(Permissions.EditEpisode)) { %>
                    <li>
                        <a href="javascript:void(0);" onclick="Schedule.loadInactiveEpisodes('<%= Model.PatientId %>');">Inactive Episodes</a>
                    </li>
                    <%  } %>
                    <li>
                        <a href="javascript:void(0);" onclick="UserInterface.ShowFrequencies('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');">Episode Frequencies</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="Schedule.loadMasterCalendar('<%= Model.PatientId %>','<%= Model.EpisodeId %>');">Master Calendar</a>
            </li>
            <%  if (Current.HasRight(Permissions.ScheduleVisits)&& !Current.IsAgencyFrozen) { %>
            <li>
                <a href="javascript:void(0);" onclick="UserInterface.ShowMultipleReassignModal('<%= Model.EpisodeId %>', '<%= Model.PatientId %>','Episode');" title="Reassign Schedules">Reassign Schedules</a>
                <div id="reassign_Episode_Container" class="reassignschedulepatientmodal hidden"></div>
            </li>
            <%  } %>
            <%  if (Current.HasRight(Permissions.DeleteTasks)&& !Current.IsAgencyFrozen) { %>
            <li>
                <a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDelete('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');" title="Delete Schedules">Delete Multiple Tasks</a>
            </li>
            <%  } %>
        </ul>
     
    </div>
        <%  if (Model.HasPrevious) { %>
    <span class="abs-left">
        <a onclick="Schedule.NavigateEpisode('<%= Model.PreviousEpisode%>','<%=Model.PatientId %>');">
            <span class="largefont">&#171;</span>
            Previous Episode
        </a>
    </span>
        <%  } %>
        <%  if (Model.HasNext) { %>
    <span class="abs-right">
        <a onclick="Schedule.NavigateEpisode('<%= Model.NextEpisode%>','<%=Model.PatientId %>');">
            Next Episode
            <span class="largefont">&#187;</span>
        </a>
    </span>
        <%  } %>
    <div class="clear"></div>
    <span class="strong"><%= Model.DisplayName%></span>
    <%= Html.PatientEpisodes("EpisodeList", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "calendar-episode-list" }) %>
    <div class="buttons editeps">
        <ul>
        <%  if (Current.HasRight(Permissions.EditEpisode) && !Current.IsAgencyFrozen) { %>
            <li>
                <a href="javascript:void(0);" onclick="UserInterface.ShowEditEpisodeModal('<%= Model.EpisodeId %>','<%= Model.PatientId %>',function(){});">Manage Episode</a>
            </li>
        <%  } %>
            <li>
                <a href="javascript:void(0);" onclick="Schedule.RefreshCurrentEpisode('<%= Model.PatientId %>','<%= Model.EpisodeId %>','All');">Refresh</a>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
        <%  for (int c = 0; c < 3; c++) { %>
    <div class="cal">
        <table>
            <thead>
                <tr>
                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                </tr>
                <tr>
                    <th>Su</th>
                    <th>Mo</th>
                    <th>Tu</th>
                    <th>We</th>
                    <th>Th</th>
                    <th>Fr</th>
                    <th>Sa</th>
                </tr>
            </thead>
            <tbody>
               <% var maxWeek=DateUtilities.Weeks(startdate[c].Month, startdate[c].Year);
                for (int i = 0; i <= maxWeek; i++) { %>
                   <tr><%
                    string tooltip = "";
                    int addedDate = (i) * 7;
                    for (int j = 0; j <= 6; j++) {
                        var specificDate = currentdate[c].AddDays(j + addedDate);
                        if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate.Date > Model.EndDate.Date) { %>
                        <td class="inactive"></td><%
                        } else {
                            var events = scheduleEvents.FindAll(e => e.EventDate.ToZeroFilled() == specificDate.ToShortDateString().ToZeroFilled());
                            var count = events.Count;
                            if (count > 1) {
                                var allevents = "<br />";
                                events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.DisciplineTaskName, e.UserName); }); %>
                        <td class="multi" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                                tooltip = specificDate.ToShortDateString() + allevents;
                            } else if (count == 1) {
                                var evnt = events.First();
                                var missed = (evnt.IsMissedVisit) ? "missed" : "";
                                var status = evnt.Status; %>
                        <td class="status<%= status %> scheduled <%= missed %>" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><% 
                                tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.DisciplineTaskName, evnt.UserName.IsNotNullOrEmpty() || evnt.UserId.IsEmpty() ? evnt.UserName :evnt.UserName);
                            } else { %>
                        <td date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                                tooltip = "";
                            }
                            var dayOnClick = Current.HasRight(Permissions.ScheduleVisits) && !Current.IsAgencyFrozen ? string.Format("Schedule.Add('{0}','{1}','{2}');", Model.EpisodeId, Model.PatientId, specificDate.ToShortDateString()) : ""; %>
                            <%= string.Format("<div class=\"datelabel\" onclick=\"{0}\"{1}><a>{2}</a></div></td>", dayOnClick, tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day)%>
                            <%
                        }
                    } %>
                </tr>
            <%  } %>
            </tbody>
        </table>
    </div>
        <% } %>
</div>
<div class="clear"></div>
<% if (Current.HasRight(Permissions.ManagePatients)) { %><div class="buttons float-left"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowPatientChart('<%= Model.PatientId %>', '<%= Model.IsDischarged %>');">View Patient Chart</a></li></ul></div><% } %>
<fieldset class="calendar-legend" style="margin-left: 25%;">
    <ul>
        <li>
            <div class="scheduled">&#160;</div>
            Scheduled
        </li>
        <li>
            <div class="completed">&#160;</div>
            Completed
        </li>
        <li>
            <div class="missed">&#160;</div>
            Missed
        </li>
        <li>
            <div class="multi">&#160;</div>
            Multiple
        </li>
    </ul>
</fieldset>
<div class="buttons float-right"><ul></ul></div>
<script type="text/javascript">
    $(".datelabel").each(function() {
        if ($(this).attr("tooltip") != undefined) {
            $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: "calday",
                bodyHandler: function() { return $(this).attr("tooltip") }
            })
        }
    });
    $("#calendar-episode-list").change(function() {
        Schedule.RefreshCurrentEpisode("<%= Model.PatientId %>", $(this).val(), 'All');
    });
    $("#scheduleBottomPanel").show()
    $("#ScheduleMainResult .window-menu ul").Menu();
</script>
    <%  }  %>
<%  } %>