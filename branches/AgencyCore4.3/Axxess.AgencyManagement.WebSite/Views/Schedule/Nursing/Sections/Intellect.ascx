﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Above Normal", "Below Normal", "Paucity of Knowledge", "Vocabulary Poor", "Serial Sevens Done Poorly", "Poor Abstraction" }; %>
<div id="<%= Model.Type %>_IntellectContainer">
    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericIntellect<%= count %>"><%= question %>:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericIntellect" + count, data.AnswerOrEmptyString("GenericIntellect" + count), new { @id = Model.Type + "_GenericIntellect" + count, @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <% count++; %>
    <% } %>
    <label for="<%= Model.Type %>_GenericIntellectComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericIntellectComment", data.AnswerOrEmptyString("GenericIntellectComment"), new { @id = Model.Type + "_GenericIntellectComment", @class = "fill" })%></div>
</div>