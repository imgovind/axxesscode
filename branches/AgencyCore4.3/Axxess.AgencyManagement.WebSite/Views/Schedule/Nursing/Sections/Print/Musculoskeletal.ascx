﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericMusculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
<%  string[] genericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
<%  string[] genericBoundType = data.AnswerArray("GenericBoundType"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericMusculoskeletal.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Grip Strength:",<%= genericMusculoskeletal.Contains("2").ToString().ToLower() %>) +
<%if(genericMusculoskeletal.Contains("2")){ %>    
printview.span("<%= (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("1") ? "Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("2") ? "Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalHandGrips").Equals("3") ? "Other" : string.Empty) %>") +

printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("2").ToString().ToLower() %>)) +
<%} %>
printview.col(2,
    printview.checkbox("Impared Motor Skill",<%= genericMusculoskeletal.Contains("3").ToString().ToLower() %>) +
    <%if(genericMusculoskeletal.Contains("3")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("2") ? "Fine" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalImpairedMotorSkills").Equals("3") ? "Gross" : string.Empty) %>") +
    <%}else{ %>
    printview.span("")+
    <%} %>
    printview.checkbox("Limited ROM",<%= genericMusculoskeletal.Contains("4").ToString().ToLower() %>) +
    printview.span("<%=genericMusculoskeletal.Contains("4")?"Location:"+ data.AnswerOrEmptyString("GenericLimitedROMLocation").Clean():string.Empty %>") +
    printview.checkbox("Mobility",<%= genericMusculoskeletal.Contains("5").ToString().ToLower() %>) +
    <%if(genericMusculoskeletal.Contains("5")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("1") ? "WNL (Within Normal Limits)" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("2") ? "Ambulatory" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("3") ? "Ambulatory w/assistance" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("4") ? "Chair fast" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("5") ? "Bedfast" : string.Empty) + (data.AnswerOrEmptyString("GenericMusculoskeletalMobility").Equals("6") ? "Non-ambulatory" : string.Empty)%>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Type Assistive Device",<%= genericMusculoskeletal.Contains("6").ToString().ToLower() %>) +
<%if(genericMusculoskeletal.Contains("6")){ %>
printview.col(2,
    printview.checkbox("Cane",<%= genericAssistiveDevice.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Crutches",<%= genericAssistiveDevice.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Walker",<%= genericAssistiveDevice.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Wheelchair",<%= genericAssistiveDevice.Contains("4").ToString().ToLower() %>)) +
printview.checkbox("Other: <%=genericAssistiveDevice.Contains("5")?data.AnswerOrEmptyString("GenericAssistiveDeviceOther").Clean():string.Empty %>",<%= genericAssistiveDevice.Contains("5").ToString().ToLower() %>) +
<%} %>
printview.col(2,
    printview.checkbox("Contracture",<%= genericMusculoskeletal.Contains("7").ToString().ToLower() %>) +
    <%if(genericMusculoskeletal.Contains("7")){ %>
    printview.span("Location:<%=genericMusculoskeletal.Contains("7")?data.AnswerOrEmptyString("GenericContractureLocation").Clean():string.Empty %>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Weakness",<%= genericMusculoskeletal.Contains("8").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Joint Pain",<%= genericMusculoskeletal.Contains("9").ToString().ToLower() %>) +
    <%if(genericMusculoskeletal.Contains("9")){ %>
    printview.span("Location:<%= data.AnswerOrEmptyString("GenericJointPainLocation").Clean() %>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Poor Balance",<%= genericMusculoskeletal.Contains("10").ToString().ToLower() %>) +
printview.checkbox("Joint Stiffness",<%= genericMusculoskeletal.Contains("11").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Amputation",<%= genericMusculoskeletal.Contains("12").ToString().ToLower() %>) +
    printview.span("<%=genericMusculoskeletal.Contains("12")?"Location:"+data.AnswerOrEmptyString("GenericAmputationLocation").Clean():string.Empty %>")) +
printview.checkbox("Weight Bearing Restrictions",<%= genericMusculoskeletal.Contains("13").ToString().ToLower() %>) +
<%if(genericMusculoskeletal.Contains("13")){ %>
printview.col(2,
    printview.checkbox("Full",<%= data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Partial",<%= data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("1").ToString().ToLower()%>) +
    printview.span("Location",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation").Clean() %>")) +
    <%} %>
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericMusculoskeletalComment").Clean() %>"),
"Musculoskeletal"