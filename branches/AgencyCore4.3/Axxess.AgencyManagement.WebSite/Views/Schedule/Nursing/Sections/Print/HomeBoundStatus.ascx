﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] homeBoundReason = data.AnswerArray("HomeBoundReason"); %>
printview.col(1,
    printview.checkbox("Exhibits considerable &#38; taxing effort to leave home",<%= homeBoundReason.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Requires the assistance of another to get up and moving safely",<%= homeBoundReason.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Severe Dyspnea",<%= homeBoundReason.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Unable to safely leave home unassisted",<%= homeBoundReason.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Unsafe to leave home due to cognitive or psychiatric impairments",<%= homeBoundReason.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Unable to leave home due to medical restriction(s)",<%= homeBoundReason.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Other <%= data.AnswerOrEmptyString("OtherHomeBoundDetails")%>",<%= homeBoundReason.Contains("8").ToString().ToLower() %>)) +
printview.col(2,
    printview.span("Home Environment",true) +
    printview.span("<%= (data.AnswerOrEmptyString("HomeEnvironment").Equals("1") ? "No issues identified" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("2") ? "Lack of Finances" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("3") ? "Lack of CG/Family Support" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("4") ? "Poor Home Environment" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("5") ? "Cluttered/Soiled Living Conditions" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("6") ? "Other" : string.Empty) %>")),
"Homebound Status"