﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Above Normal", "Below Normal", "Paucity of Knowledge", "Vocabulary Poor", "Serial Sevens Done Poorly", "Poor Abstraction" }; %>
<% if (data.AnswerOrEmptyString("GenericIsIntellectApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsIntellectApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Intellect"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericIntellect" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericIntellectComment").Clean()%>"),
"Intellect"
<% } %>
