﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">60 Day Summary/Case Conference | <%= Model.Patient.DisplayName %></span>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "SixtyDaySummaryForm" })) {
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string sendAsOrder = data.ContainsKey("SendAsOrder") && data["SendAsOrder"].Answer.IsNotNullOrEmpty() ? data["SendAsOrder"].Answer : "0";%>

<%= Html.Hidden("SixtyDaySummary_PatientId", Model.PatientId)%>
<%= Html.Hidden("SixtyDaySummary_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("SixtyDaySummary_EventId", Model.EventId)%>
<%= Html.Hidden("SixtyDaySummary_VisitDate",Model.VisitDate) %>
<%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
<%= Html.Hidden("DisciplineTask", "85")%>
<%= Html.Hidden("Type", "SixtyDaySummary")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                    60 Day Summary/Case Conference
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" ><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span>
                    <span class="float-right">
                        <%= string.Format("<input id='{1}_SendAsOrder' class='radio' name='{1}_SendAsOrder' value='1' type='checkbox' {0} />", sendAsOrder.Contains("1").ToChecked(), Model.Type)%>Send as an order
                
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label for="SixtyDaySummary_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox("SixtyDaySummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "SixtyDaySummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td>
                    <div>
                        <label for="SixtyDaySummary_PhysicianId" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox("SixtyDaySummary_PhysicianId", data.ContainsKey("PhysicianId") ? data["PhysicianId"].Answer : Model.PhysicianId.ToString(), new { @id = "SixtyDaySummary_Physician", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="SixtyDaySummary_DNR" class="float-left">DNR:</label>
                        <div class="float-left">
                            <%= Html.RadioButton("SixtyDaySummary_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "SixtyDaySummary_DNR1", @class = "radio" })%>
                            <label for="SixtyDaySummary_DNR1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("SixtyDaySummary_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "SixtyDaySummary_DNR2", @class = "radio" })%>
                            <label for="SixtyDaySummary_DNR2" class="inline-radio">No</label>
                        </div>
                        
                        
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Homebound Status</th>
            </tr>
            <tr>
                <td colspan="2"><%
        string[] homeboundStatus = data.ContainsKey("HomeboundStatus") && data["HomeboundStatus"].Answer != "" ? data["HomeboundStatus"].Answer.Split(',') : null; %>
                    <input name="SixtyDaySummary_HomeboundStatus" value=" " type="hidden" />
                    <div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusLeave' name='SixtyDaySummary_HomeboundStatus' value='2' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("2") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusLeave" class="radio normal">Exhibits considerable &#38; taxing effort to leave home</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusAssistRequired' name='SixtyDaySummary_HomeboundStatus' value='3' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("3") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusAssistRequired" class="radio normal">Requires the assistance of another to get up and move safely</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusDyspnea' name='SixtyDaySummary_HomeboundStatus' value='4' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("4") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusDyspnea" class="radio normal">Severe Dyspnea</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusUnableToLeave' name='SixtyDaySummary_HomeboundStatus' value='5' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("5") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusUnableToLeave" class="radio normal">Unable to safely leave home unassisted</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusUnsafeToLeave' name='SixtyDaySummary_HomeboundStatus' value='6' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("6") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusUnsafeToLeave" class="radio normal">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusMedReason' name='SixtyDaySummary_HomeboundStatus' value='7' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("7") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusMedReason" class="radio normal">Unable to leave home due to medical restriction(s)</label>
                    </div><div class="third">
                        <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusOtherCheck' name='SixtyDaySummary_HomeboundStatus' value='8' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("8") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_HomeboundStatusOtherCheck" class="radio normal">Other</label>
                    </div><div class="third">
                        <%= Html.TextBox("SixtyDaySummary_HomeboundStatusOther", data.ContainsKey("HomeboundStatusOther") ? data["HomeboundStatusOther"].Answer : "", new { @id = "SixtyDaySummary_HomeboundStatusOther", @class = "float-left" })%>
                    </div><div class="third">&nbsp;</div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Patient Condition</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody>
                            <tr><% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="SixtyDaySummary_PatientCondition" value=" " type="hidden" />
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionStable' name='SixtyDaySummary_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_PatientConditionStable" class="radio">Stable</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionImproved' name='SixtyDaySummary_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionImproved" class="radio">Improved</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionUnchanged' name='SixtyDaySummary_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionUnstable' name='SixtyDaySummary_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionUnstable" class="radio">Unstable</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionDeclined' name='SixtyDaySummary_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionDeclined" class="radio">Declined</label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="2">Service(s) Provided</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="SixtyDaySummary_ServiceProvided" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedSN' name='SixtyDaySummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_ServiceProvidedSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedPT' name='SixtyDaySummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedPT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOT' name='SixtyDaySummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedST' name='SixtyDaySummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedMSW' name='SixtyDaySummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedHHA' name='SixtyDaySummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedHHA" class="radio">HHA</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOther' name='SixtyDaySummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedOther" class="radio">Other</label> <%= Html.TextBox("SixtyDaySummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = "SixtyDaySummary_ServiceProvidedOtherValue", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Vital Sign Ranges</th></tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <tbody>
                            <tr><th></th><th>BP Systolic</th><th>BP Diastolic</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BS</th><th>Pain</th></tr>
                            <tr>
                                <th>Lowest</th>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPDiaMin", data.ContainsKey("VitalSignBPDiaMin") ? data["VitalSignBPDiaMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPDiaMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignPainMin", data.ContainsKey("VitalSignPainMin") ? data["VitalSignPainMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignPainMin", @class = "fill" })%></td>
                            </tr><tr>
                                <th>Highest</th>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPDiaMax", data.ContainsKey("VitalSignBPDiaMax") ? data["VitalSignBPDiaMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPDiaMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignPainMax", data.ContainsKey("VitalSignPainMax") ? data["VitalSignPainMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignPainMax", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Summary of Care Provided</th></tr>
            <tr><td colspan="2"><%= Html.Templates("SixtyDaySummary_SummaryOfCareProvidedTemplates", new { @class = "Templates", @template = "#SixtyDaySummary_SummaryOfCareProvided" })%><%= Html.TextArea("SixtyDaySummary_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_SummaryOfCareProvided", @rows = "15" })%></td></tr>
            <tr><th colspan="2">Patient&#8217;s Current Condition</th></tr>
            <tr><td colspan="2"><%= Html.Templates("SixtyDaySummary_PatientCurrentConditionTemplates", new { @class = "Templates", @template = "#SixtyDaySummary_PatientCurrentCondition" })%><%= Html.TextArea("SixtyDaySummary_PatientCurrentCondition", data.ContainsKey("PatientCurrentCondition") ? data["PatientCurrentCondition"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_PatientCurrentCondition", @rows = "10" })%></td></tr>
            <tr><th colspan="2">Goals</th></tr>
            <tr><td colspan="2"><%= Html.Templates("SixtyDaySummary_GoalsTemplates", new { @class = "Templates", @template = "#SixtyDaySummary_Goals" })%><%= Html.TextArea("SixtyDaySummary_Goals", data.ContainsKey("Goals") ? data["Goals"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_Goals", @rows = "10" })%></td></tr>
            <tr><th colspan="2">Recomended Services</th></tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %><input name="SixtyDaySummary_RecommendedService" value="" type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceSN' name='SixtyDaySummary_RecommendedService' value='1' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("1") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServicePT' name='SixtyDaySummary_RecommendedService' value='2' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServicePT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOT' name='SixtyDaySummary_RecommendedService' value='3' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceST' name='SixtyDaySummary_RecommendedService' value='4' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceMSW' name='SixtyDaySummary_RecommendedService' value='5' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceHHA' name='SixtyDaySummary_RecommendedService' value='6' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("6") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceHHA" class="radio">HHA</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOther' name='SixtyDaySummary_RecommendedService' value='7' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("7") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceOther" class="radio">Other</label> <%= Html.TextBox("SixtyDaySummary_RecommendedServiceOtherValue", data.ContainsKey("RecommendedServiceOtherValue") ? data["RecommendedServiceOtherValue"].Answer:string.Empty, new { @id = "SixtyDaySummary_RecommendedServiceOtherValue", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Notifications</th></tr>
            <tr><td colspan="2">
                <div class="third"><%= string.Format("<input id='SixtyDaySummary_SummarySentToPhysician' name='SixtyDaySummary_SummarySentToPhysician' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("SummarySentToPhysician") && data["SummarySentToPhysician"] != null && data["SummarySentToPhysician"].Answer.IsNotNullOrEmpty() && data["SummarySentToPhysician"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%><label for="SixtyDaySummary_SummarySentToPhysician" class="radio">Summary Sent To Physician</label></div>
                <div class="third"><label for="SixtyDaySummary_SummarySentBy">Sent By:</label><div class="float-right"><%= Html.Users("SixtyDaySummary_SummarySentBy", data.ContainsKey("SummarySentBy") ? data["SummarySentBy"].Answer : string.Empty, new { @id = "SixtyDaySummary_SummarySentBy", @class = "fill" })%></div></div>
                <div class="third"><label for="SixtyDaySummary_SummarySentDate">Date Sent:</label><div class="float-right"><input type="text" class="date-picker" name="SixtyDaySummary_SummarySentDate" value="<%= data.ContainsKey("SummarySentDate") && data["SummarySentDate"] != null && data["SummarySentDate"].Answer.IsNotNullOrEmpty() && data["SummarySentDate"].Answer.IsValidDate() ? data["SummarySentDate"].Answer : string.Empty %>" id="SixtyDaySummary_SummarySentDate" /></div></div>
                </td>
            </tr>
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="SixtyDaySummary_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician", "", new { @id = "SixtyDaySummary_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="SixtyDaySummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate" /></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignature2" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician2", "", new { @id = "SixtyDaySummary_Clinician2" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignatureDate2" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="SixtyDaySummary_SignatureDate2" value="<%= data.ContainsKey("SignatureDate2") && data["SignatureDate2"].Answer.IsNotNullOrEmpty() && data["SignatureDate2"].Answer.IsValidDate() ? data["SignatureDate2"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate2" /></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignature3" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician3", "", new { @id = "SixtyDaySummary_Clinician3" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignatureDate3" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="SixtyDaySummary_SignatureDate3" value="<%= data.ContainsKey("SignatureDate3") && data["SignatureDate3"].Answer.IsNotNullOrEmpty() && data["SignatureDate3"].Answer.IsValidDate() ? data["SignatureDate3"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate3" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="SixtyDaySummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove();sixtyDaySummary.Submit($(this),'<%=Model.EpisodeId %>','<%= Model.PatientId %>');">Save</a></li>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryAdd();sixtyDaySummary.Submit($(this),'<%=Model.EpisodeId %>','<%= Model.PatientId %>');">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); sixtyDaySummary.Submit($(this),'<%=Model.EpisodeId %>','<%= Model.PatientId %>');">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); sixtyDaySummary.Submit($(this),'<%=Model.EpisodeId %>','<%= Model.PatientId %>');">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); UserInterface.CloseWindow('sixtyDaySummary');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#SixtyDaySummary_MR").attr('readonly', true);
    $("#SixtyDaySummary_EpsPeriod").attr('readonly', true);
    function SixtyDaySummaryAdd() {
        $("#SixtyDaySummary_Clinician").removeClass('required').addClass('required');
        $("#SixtyDaySummary_SignatureDate").removeClass('required').addClass('required');
    }
    function SixtyDaySummaryRemove() {
        $("#SixtyDaySummary_Clinician").removeClass('required');
        $("#SixtyDaySummary_SignatureDate").removeClass('required');
    }
    U.ShowIfSelectEquals($("#SixtyDaySummary_NotificationDate"),"3",$("#SixtyDaySummary_NotificationDateOther"));
    U.ShowIfChecked($("#SixtyDaySummary_HomeboundStatusOtherCheck"),$("#SixtyDaySummary_HomeboundStatusOther"));
    U.ShowIfChecked($("#SixtyDaySummary_ServiceProvidedOther"), $("#SixtyDaySummary_ServiceProvidedOtherValue"));
    U.ShowIfChecked($("#SixtyDaySummary_RecommendedServiceOther"), $("#SixtyDaySummary_RecommendedServiceOtherValue"));
</script>