﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSelection>>" %>
<%  Html.Telerik().Grid(Model).Name("ScheduleSelectionGrid").Columns(columns => {
        columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" });
        columns.Bound(p => p.ShortName).Title("First Name").HtmlAttributes(new { @class = "searchF" });
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    })
    .DataBinding(dataBinding => dataBinding.Ajax().Select("AllSort", "Schedule", new { BranchId = Guid.Empty, StatusId = 1, PaymentSourceId = 0 }))
     .ClientEvents(events => events.OnDataBinding("Schedule.OnPatientDataBinding").OnDataBound("Schedule.PatientListDataBound").OnRowSelected("Schedule.OnPatientRowSelected"))
    .Sortable()
    .Selectable()
    .Scrollable()
    .Footer(false)
    .Sortable(sr => sr.OrderBy(so => so.Add(s => s.LastName).Ascending()))
    .Render(); %>