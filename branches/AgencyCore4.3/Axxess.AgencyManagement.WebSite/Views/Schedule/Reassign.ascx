﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<% using (Html.BeginForm("Reassign"+Model.Type+"Schedules" , "Schedule", FormMethod.Post, new { @id = "reassign"+Model.Type+"Form" })){ %>
<% if (!Model.Type.IsEqual("All")){ %><%= Html.Hidden("PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%><%} %>
<% DateTime startDate = Model != null && Model.StartDate != DateTime.MinValue ? Model.StartDate : DateTime.Now.AddDays(-59); %>
<% DateTime endDate = Model != null && Model.EndDate != DateTime.MinValue ? Model.EndDate : DateTime.Now; %>
<div class="wrapper">
    <fieldset>
    <legend>Reassign Tasks</legend>
        <div class="wide-column">
        <% if (!Model.Type.IsEqual("All")){ %>
            <div class="row">
                <label class="float-left">Patient Name:</label>
                <div class="float-right">
                    <label><%= Model.PatientDisplayName%></label> 
                </div> 
            </div>
        <%} %>
            <div class="row">
                <label class="float-left">Employee From:</label>
                <div class="float-right">
                    <%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeOldId", "", Guid.Empty, 0, new { @id = Model.Type + "_EmployeeOldId", @class = "requireddropdown valid" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Employee To:</label>
                <div class="float-right">
                    <%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeId", "", Guid.Empty, 1, new { @id = Model.Type + "_EmployeeId", @class = "requireddropdown valid" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Date From:</label>
                <div class="float-right">
                    <input type="text" class="date-picker shortdate required" name="StartDate" value="<%= startDate.ToShortDateString() %>" id="<%= Model.Type %>_StartDate" />
                </div>
            </div>
            <div class="row">
                <label class="float-left">Date To:</label>
                <div class="float-right">
                    <input type="text" class="date-picker shortdate required" name="EndDate" value="<%= endDate.ToShortDateString() %>" id="<%= Model.Type %>_EndDate" />
                </div>
            </div>
            <div class="row"><span>Note:<em>&nbsp;Only tasks that are not started and not yet due will be reassigned.</em></span></div>
            </div>
        <div class="clear"></div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Reassign</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li></ul></div>
    </fieldset>
</div>
<%} %>
