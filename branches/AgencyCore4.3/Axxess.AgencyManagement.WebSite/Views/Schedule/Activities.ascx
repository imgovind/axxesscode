﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<% Html.Telerik().Grid(Model.ScheduleEvents).Name("ScheduleActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).Template(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDateSortable).Template(s => s.EventDateSortable).ClientTemplate("<#=EventDateSortable#>").Title("Scheduled Date").Width(105);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.OasisProfileUrl).Template(s => s.OasisProfileUrl).ClientTemplate("<#=OasisProfileUrl#>").Width(30).Title(" ").Sortable(false);
       columns.Bound(s => s.StatusComment).Template(s => s.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", s.StatusComment) : string.Empty).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.Comments).Template(s => s.Comments.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", s.Comments) : string.Empty).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.EpisodeNotes).Template(s => s.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.EpisodeNotes) : string.Empty).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.PrintUrl).Template(s => s.PrintUrl).ClientTemplate("<#=PrintUrl#>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.AttachmentUrl).Template(s => s.AttachmentUrl).ClientTemplate("<#=AttachmentUrl#>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.ActionUrl).Template(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200).Visible(!Current.IsAgencyFrozen).Sortable(false);
       columns.Bound(s => s.IsComplete).Visible(false);
   })
   .ClientEvents(c => c.OnDataBinding("Schedule.ActivityOnDataBinding").OnDataBound("Schedule.ActivityOnBound"))
   .DataBinding(dataBinding => dataBinding.Ajax().Select("ActivitySort", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline }))
     .RowAction(row =>
     {
         if (row.DataItem.IsComplete)
         {
             if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed"))
             {
                 row.HtmlAttributes["class"] += " completed";
             }
             else
             {
                 row.HtmlAttributes.Add("class", "completed");
             }
         }
         if (row.DataItem.IsOrphaned)
         {

             if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned"))
             {
                 row.HtmlAttributes["class"] += " orphaned";
             }
             else
             {
                 row.HtmlAttributes.Add("class", "orphaned");
             }
         }
     })
   .Sortable(sr => sr.OrderBy(so => so.Add(s => s.EventDateSortable).Descending()))
   .Scrollable()
   .Footer(false)
   .Render();
%>
<script type="text/javascript">
    $('#ScheduleActivityGrid .t-grid-content').css({ 'height': 'auto' });
    if ($('#schedulecenter_showall').length) $('#schedulecenter_showall').remove();
    Schedule.SetEpisodeId('<%=Model.EpisodeId%>');
    Schedule.SetId('<%=Model.PatientId%>');
    U.ServerBindScheduleActivityInit('ScheduleActivityGrid');
    $('#schedule-tab-strip').prepend(unescape("%3Cdiv id=%22schedulecenter_showall%22 class=%22abs buttons%22 style=%22left:550px;line-height:25px;%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22Schedule.showAll('<%=Model.EpisodeId%>','<%=Model.PatientId%>');%22%3EShow all%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
</script>