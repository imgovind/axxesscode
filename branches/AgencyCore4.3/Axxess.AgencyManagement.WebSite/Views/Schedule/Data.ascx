﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  string[] stabs = new string[] { "Nursing", "HHA","MSW", "Therapy" }; %>
<%  if (Model != null) { %>
<%  if (Model.IsEpisodeExist) { %>
<div class="top">
    <div id="scheduleTop"><%  Html.RenderPartial("Calendar", Model); %></div>
    <%  if (Current.HasRight(Permissions.ScheduleVisits) && !Current.IsAgencyFrozen) { %>
    <div id="schedule-collapsed">
        <a href="javascript:void(0);" onclick="Schedule.ShowScheduler('<%=Model.EpisodeId%>','<%=Model.PatientId%>')" class="show-scheduler">Show Scheduler</a>
    </div>
        <%  Html.Telerik().TabStrip().Name("schedule-tab-strip").ClientEvents(events => events.OnSelect("Schedule.OnSelect")).Items(tabstrip => { %>
            <%  for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
                <%  string stitle = stabs[sindex]; %>
                <%  string tabname = stitle == "MSW" ? "MSW / Other" : stitle; %>
                <%  tabstrip.Add().Text(tabname).HtmlAttributes(new { id = stitle + "_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                    <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
    <div class="tab-contents">
        <table id="<%= stitle %>ScheduleTable" data="<%= stitle %>" class="schedule-tables purgable">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div class="buttons">
            <ul>
                <li><%= String.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ScheduleInputFix($(this),'#{0}ScheduleTable','{1}','{2}');\">Save</a>", stitle,Model.PatientId,Model.EpisodeId) %></li>
                <li><a href="javascript:void(0);" onclick="Schedule.RefreshCurrentEpisode('<%= Model.PatientId %>','<%= Model.EpisodeId %>','All');">Cancel</a></li>
            </ul>
        </div>
    </div>
                    <%  } %>
                <%  }); %>
            <%  } %>
            <%  tabstrip.Add().Text("Orders/Care Plans").HtmlAttributes(new { id = "Orders_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
    <div class="tab-contents">
        <table id="OrdersScheduleTable" data="Orders" class="schedule-tables purgable">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Schedule.ScheduleInputFix($(this),'#OrdersScheduleTable','<%= Model.PatientId %>','<%= Model.EpisodeId %>');">Save</a></li>
                <li><a href="javascript:void(0);" onclick="Schedule.RefreshCurrentEpisode('<%= Model.PatientId %>','<%= Model.EpisodeId %>','All');">Cancel</a></li>
            </ul>
        </div>
    </div>
                <%  } %>
            <%  }); %>
            <%  tabstrip.Add().Text("Daily/Outlier").ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() =>
                { %>
                <%  using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post)) { %>
    <div class="tab-contents">
        <table id="multiple-schedule-table" data="Multiple" class="schedule-tables">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <%  var htmlAttributes = new Dictionary<string, string>(); %>
                <%  htmlAttributes.Add("id", "multipleDisciplineTask"); %>
                <%  htmlAttributes.Add("class", "MultipleDisciplineTask"); %>
                    <td><%= Html.MultipleDisciplineTasks("DisciplineTask", "", htmlAttributes)%></td>
                    <td><%= Html.Users("userId", "", new { @class = "Users" }) %></td>
                    <td class="date-range">
                        <input type="text" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" id="outlierStartDate" />
                        <span>to</span>
                        <input type="text" name="EndDate" value="<%= Model.StartDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" id="outlierEndDate" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Schedule.FormSubmitMultiple($(this),'<%= Model.PatientId %>','<%= Model.EpisodeId %>');">Save</a></li>
                <li><a href="javascript:void(0);" onclick="Schedule.RefreshCurrentEpisode('<%= Model.PatientId %>','<%= Model.EpisodeId %>','All');">Cancel</a></li>
            </ul>
        </div>
    </div>
                <%  } %>
            <%  }); %>
        <%  }).SelectedIndex(0).Render(); %>
    <%  } %>
</div>
<div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Activities", Model); %></div>
<script type="text/javascript">
    Schedule.positionBottom();
    $("#outlierStartDate,#outlierEndDate").DatePicker();
</script>
<%  } else { %>
    <%  if (!Model.IsDischarged) { %>
<div class="ajaxerror"><h1>No Episodes found for this patient.</h1><div class="heading buttons"><ul><li><a href="javascript:void(0)" onclick=" UserInterface.ShowNewEpisodeModal('<%= Model.PatientId %>');">Add New Episode</a></li><li><a href="javascript:void(0)" onclick="Schedule.loadInactiveEpisodes('<%= Model.PatientId %>');">Inactive Episodes</a></li></ul></div></div>
    <%  } else { %>
    <div class="ajaxerror"><h1>No Episodes found for this discharged patient. Re-admit the patient to create new episodes.</h1></div>
    <%  } %>
<%  } %>
<%  } %>