﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<% using (Html.BeginForm("AddMultiDaySchedule", "Schedule", FormMethod.Post, new { @id = "multiDayScheduleForm" })) { %>
<%  DateTime[] startdate = new DateTime[3];
    DateTime[] enddate = new DateTime[3];
    DateTime[] currentdate = new DateTime[3];
    startdate[0] = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year);
    enddate[0] = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year);
    currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek);
    startdate[1] = enddate[0].AddDays(1);
    enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year);
    currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek);
    startdate[2] = enddate[1].AddDays(1);
    enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year);
    currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
<%= Html.Hidden("episodeId", Model.EpisodeId, new { @id = "multiDayScheduleEpisodeId" })%>
<%= Html.Hidden("patientId", Model.PatientId, new { @id = "multiDaySchedulePatientId" })%>
<%= Html.Hidden("visitDates", "", new { @id = "multiDayScheduleVisitDates" })%>
    <div class="bigtext">Quick Employee Scheduler</div>
    <div class="wrapper main">
        <fieldset>
            <legend><span class="strong"><%= Model.DisplayName%></span> | <%= Model.StartDate.ToShortDateString()%> &#8211; <%= Model.EndDate.ToShortDateString()%></legend>
            <div class="column">
                <div class="row"><label class="float-left" for="multiDayScheduleUserID">User/Employee: </label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "userId", "", new { @id = "multiDayScheduleUserID", @class = "Users requireddropdown" })%></div></div>
                <% var htmlAttributes = new Dictionary<string, string>(); htmlAttributes.Add("id", "multiDayScheduleDisciplineTask"); htmlAttributes.Add("class", "MultipleDisciplineTask required");%>
                <div class="row"><label class="float-left" for="multiDayScheduleDisciplineTask">Task: </label><div class="float-right"><%= Html.MultipleDisciplineTasks("disciplineTaskId", "", htmlAttributes)%></div></div>
            </div>
        </fieldset>
        <div class="strong">To schedule visits for the selected user, click on the desired dates in the calendar below:</div>
        <div class="trical">
        <% for (int c = 0; c < 3; c++) { %>
            <div class="cal"><table class="multiDayScheduleTable"><thead><tr><td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><%
        for (int i = 0; i <= DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); i++)
        { %><tr><%
        string tooltip = "";
        int addedDate = (i) * 7;
        for (int j = 0; j <= 6; j++)
        {
            var specificDate = currentdate[c].AddDays(j + addedDate);
            if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate > Model.EndDate.AddDays(1))
            { %><td class="inactive ui-droppable"></td><%}
            else{%><td class="ui-droppable" date="<%= specificDate.ToShortDateString() %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%tooltip = "";%>
                    <% var dayOnClick = "$(this).closest('td').toggleClass('selectdate');"; %>
                    <%= string.Format("<div class=\"datelabel\" onclick=\"{0}\"{1}><a>{2}</a></div></td>", dayOnClick, tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day)%><%
        } } %></tr><%} %></tbody></table></div><% } %></div>
        <div class="clear"></div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Schedule.gatherMultiSchedulerDates();$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
    </div>
<% } %>
