﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">
                Vital Signs
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Diagnosis
            </th>
            <th colspan="2">
                PLOF and Medical History
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Living Situation
            </th>
            <th colspan="2">
                Homebound Reason
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">
                Functional Mobility Key
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                ADLs/Functional Mobility Level/Level of Assist
            </th>
            <th colspan="2">
                Physical Assessment
            </th>
        </tr>
        <tr>
            <td colspan="2"  rowspan="3">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Pain Assessment
            </th>
            
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
            </td>
            
        </tr>
        <tr>
            <th colspan="4">Sensory/Perceptual Skills</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Cognitive Status/Comprehension</th>
            <th colspan="2">Motor Components (Enter Appropriate Response)</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">
                Assessment
            </th>
            <th colspan="2">
                Narrative
            </th>
        </tr>
        <tr>
            <td colspan="2">
            <%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
                <%= Html.Templates(Model.Type + "_GenericAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericAssessmentMore" })%>
                <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"),4, 20, new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericNarrativeTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericNarrative" })%>
                <%= Html.TextArea(Model.Type + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), 4, 20, new { @id = Model.Type + "_GenericNarrative", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="4">
                Treatment Plan
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev3.ascx", Model); %>
                
            </td>
        </tr>
        
        <tr>
            <th colspan="4">
                OT Goals
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTGoals/FormRev1.ascx", Model); %>
                
            </td>
        </tr>
        <tr>
            <th colspan="4">Standardized test</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTStandardizedTest/FormRev1.ascx", Model); %>
                </td>
        </tr>
        <tr>
            <th colspan="2">
                Care Coordination
            </th>
            <th colspan="2">
                Skilled Treatment Provided This Visit
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericCareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCareCoordination" })%>
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericSkilledTreatmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSkilledTreatmentProvided" })%>
                <%= Html.TextArea(Model.Type + "_GenericSkilledTreatmentProvided", data.ContainsKey("GenericSkilledTreatmentProvided") ? data["GenericSkilledTreatmentProvided"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericSkilledTreatmentProvided", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="4">Notification</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev1.ascx", Model); %>
            </td>
        </tr>
    </tbody>
</table>
