﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                I = Independent
                            </td>
                            <td>
                                MI = Modified Independent
                            </td>
                            <td>
                                S = Supervision
                            </td>
                            <td>
                                VC = Verbal Cue
                            </td>
                            <td>
                                CGA = Contact Guard Assist
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Min A = 25% Assist
                            </td>
                            <td>
                                Mod A = 50% Assist
                            </td>
                            <td>
                                Max A = 75% Assist
                            </td>
                            <td>
                                Total = 100% Assist
                            </td>
                            <td>
                                Not Tested
                            </td>
                        </tr>
                    </tbody>
                </table>