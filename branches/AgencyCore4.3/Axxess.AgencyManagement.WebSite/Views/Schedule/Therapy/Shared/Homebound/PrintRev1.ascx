﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] homeboundStatusAssist = data.AnswerArray("GenericHomeboundStatusAssist"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("Gait",<%= homeboundStatusAssist.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Leaving the Home",<%= homeboundStatusAssist.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfers",<%= homeboundStatusAssist.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("SOB/Endurance",<%= homeboundStatusAssist.Contains("4").ToString().ToLower() %>)),
        "Homebound Status");
</script>