﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %><%
 var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
 string[] genericLivingSituationChanges = data.ContainsKey("GenericLivingSituationChanges") && data["GenericLivingSituationChanges"].Answer != "" ? data["GenericLivingSituationChanges"].Answer.Split(',') : null;
 string[] genericMentalStatus = data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer != "" ? data["GenericMentalStatus"].Answer.Split(',') : null;
 string[] genericEmotionalStatus = data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer != "" ? data["GenericEmotionalStatus"].Answer.Split(',') : null;
 string[] genericVisitGoals = data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer != "" ? data["GenericVisitGoals"].Answer.Split(',') : null;
 string[] genericPlannedInterventions = data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer != "" ? data["GenericPlannedInterventions"].Answer.Split(',') : null;
 string[]genericFollowUpPlan = data.ContainsKey("GenericFollowUpPlan") && data["GenericFollowUpPlan"].Answer != "" ? data["GenericFollowUpPlan"].Answer.Split(',') : null;                                                                                          
%>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Living Situation</th>
        </tr><tr>
            <td class="align-left">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.RadioButton(Model.Type + "_GenericIsLivingSituationChange", "2", data.ContainsKey("GenericIsLivingSituationChange") && data["GenericIsLivingSituationChange"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericIsLivingSituationChange2", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsLivingSituationChange2">Unchanged from Last Visit</label>
                            </td><td>
                                <div class="float-left">
                                    <%= Html.RadioButton(Model.Type + "_GenericIsLivingSituationChange", "1", data.ContainsKey("GenericIsLivingSituationChange") && data["GenericIsLivingSituationChange"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericIsLivingSituationChange1", @class = "radio" })%>
                                    <label for="<%= Model.Type %>_GenericIsLivingSituationChange1">Changed</label>
                                </div>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLivingSituationChanged", data.ContainsKey("GenericLivingSituationChanged") ? data["GenericLivingSituationChanged"].Answer : "", new { @id = Model.Type + "_GenericLivingSituationChanged", @class = "" })%></div>
                            </td><td>
                                <input type="hidden" name="<%= Model.Type %>_GenericLivingSituationChanges" value="" />
                                <div class="float-left">
                                    <%= string.Format("<input id='{1}_GenericLivingSituationChanges1' class='radio float-left' name='{1}_GenericLivingSituationChanges' value='1' type='checkbox' {0} />", genericLivingSituationChanges != null && genericLivingSituationChanges.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericLivingSituationChanges1">Update to Primary Caregiver</label>
                                </div>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericUpdateToPrimaryCaregiver", data.ContainsKey("GenericUpdateToPrimaryCaregiver") ? data["GenericUpdateToPrimaryCaregiver"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericUpdateToPrimaryCaregiver" })%></div>
                            </td><td>
                                <div class="float-left">
                                    <%= string.Format("<input id='{1}_GenericLivingSituationChanges2' class='radio float-left' name='{1}_GenericLivingSituationChanges' value='2' type='checkbox' {0} />", genericLivingSituationChanges != null && genericLivingSituationChanges.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericLivingSituationChanges2">Changes to Environmental Conditions</label>
                                </div>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChangesToEnvironmentalConditions", data.ContainsKey("GenericChangesToEnvironmentalConditions") ? data["GenericChangesToEnvironmentalConditions"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericChangesToEnvironmentalConditions" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Psychosocial Assessment</th>
        </tr><tr>
            <td>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <div class="padnoterow">
                                    <input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
                                    <div class="align-left strong">Mental Status (check all that apply)</div>
                                    <table class="align-left">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus1' class='radio' name='{1}_GenericMentalStatus' value='1' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus1">Alert</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus2' class='radio' name='{1}_GenericMentalStatus' value='2' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus2">Forgetful</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus3' class='radio' name='{1}_GenericMentalStatus' value='3' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus3">Confused</label>
                                                </td>
                                            </tr><tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus4' class='radio' name='{1}_GenericMentalStatus' value='4' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus4">Disoriented</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus5' class='radio' name='{1}_GenericMentalStatus' value='5' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus5">Oriented</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus6' class='radio' name='{1}_GenericMentalStatus' value='6' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus6">Lethargic</label>
                                                </td>
                                            </tr><tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus7' class='radio' name='{1}_GenericMentalStatus' value='7' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus7">Poor Short Term Memory</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus8' class='radio' name='{1}_GenericMentalStatus' value='8' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus8">Unconscious</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericMentalStatus9' class='radio' name='{1}_GenericMentalStatus' value='9' type='checkbox' {0} />", genericMentalStatus != null && genericMentalStatus.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericMentalStatus9">Cannot Determine</label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td><td>
                                <div class="padnoterow">
                                    <input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
                                    <div class="align-left strong">Emotional Status (check all that apply)</div>
                                    <table class="align-left">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus1' class='radio' name='{1}_GenericEmotionalStatus' value='1' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus1">Stable</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus2' class='radio' name='{1}_GenericEmotionalStatus' value='2' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus2">Tearful</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus3' class='radio' name='{1}_GenericEmotionalStatus' value='3' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus3">Stressed</label>
                                                </td>
                                            </tr><tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus4' class='radio' name='{1}_GenericEmotionalStatus' value='4' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus4">Angry</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus5' class='radio' name='{1}_GenericEmotionalStatus' value='5' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus5">Sad</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus6' class='radio' name='{1}_GenericEmotionalStatus' value='6' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus6">Withdrawn</label>
                                                </td>
                                            </tr><tr>
                                                <td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus7' class='radio' name='{1}_GenericEmotionalStatus' value='7' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus7">Fearful</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus8' class='radio' name='{1}_GenericEmotionalStatus' value='8' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus8">Anxious</label>
                                                </td><td>
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus9' class='radio' name='{1}_GenericEmotionalStatus' value='9' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus9">Flat Affect</label>
                                                </td>
                                            </tr><tr>
                                                <td colspan="3">
                                                    <%= string.Format("<input id='{1}_GenericEmotionalStatus10' class='radio float-left' name='{1}_GenericEmotionalStatus' value='10' type='checkbox' {0} />", genericEmotionalStatus != null && genericEmotionalStatus.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                                    <label for="<%= Model.Type %>_GenericEmotionalStatus10">Other:</label>
                                                    <%= Html.TextBox(Model.Type + "_GenericEmotionalStatusOther", data.ContainsKey("GenericEmotionalStatusOther") ? data["GenericEmotionalStatusOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericEmotionalStatusOther" })%>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.ContainsKey("GenericPsychosocialAssessment") ? data["GenericPsychosocialAssessment"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericPsychosocialAssessment", @class = "fill" })%>
            </td>
        </tr><tr>
            <th>Visit Goals</th>
        </tr><tr>
            <td style="vertical-align:top;">
                <input type="hidden" name="<%= Model.Type %>_GenericVisitGoals" value="" />
                <table class="align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals1' class='radio' name='{1}_GenericVisitGoals' value='1' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals1">Adequate Support System</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals2' class='radio' name='{1}_GenericVisitGoals' value='2' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals2">Improved Client/Family Coping</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals3' class='radio' name='{1}_GenericVisitGoals' value='3' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals3">Normal Grieving Process</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals4' class='radio' name='{1}_GenericVisitGoals' value='4' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals4">Appropriate Goals for Care Set by Client/Family</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals5' class='radio' name='{1}_GenericVisitGoals' value='5' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals5">Appropriate Community Resource Referrals</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals6' class='radio' name='{1}_GenericVisitGoals' value='6' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals6">Stable Placement Setting</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals7' class='radio' name='{1}_GenericVisitGoals' value='7' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals7">Mobilization of Financial Resources</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericVisitGoals8' class='radio' name='{1}_GenericVisitGoals' value='8' type='checkbox' {0} />", genericVisitGoals != null && genericVisitGoals.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericVisitGoals8">Other (enter details below)</label>
                                <%= Html.TextBox(Model.Type + "_GenericVisitGoalsOther", data.ContainsKey("GenericVisitGoalsOther") ? data["GenericVisitGoalsOther"].Answer : "", new { @id = Model.Type + "_GenericVisitGoalsOther", @class = "" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-left strong"><label for="<%= Model.Type %>_GenericVisitGoalDetails">Visit Goal Details</label></div>
                <div><%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.ContainsKey("GenericVisitGoalDetails") ? data["GenericVisitGoalDetails"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "fill" })%></div>
                <div class="align-left strong">Visit Interventions (check all that apply)</div>
                <input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
                <table class="align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions1' class='radio' name='{1}_GenericPlannedInterventions' value='1' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions1">Psychosocial Assessment</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions2' class='radio' name='{1}_GenericPlannedInterventions' value='2' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions2">Develop Appropriate Support System</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions3' class='radio' name='{1}_GenericPlannedInterventions' value='3' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions3">Counseling re Disease Process &#38; Management</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions4' class='radio' name='{1}_GenericPlannedInterventions' value='4' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions4">Community Resource Planning &#38; Outreach</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions5' class='radio' name='{1}_GenericPlannedInterventions' value='5' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions5">Counseling re Family Coping</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions6' class='radio' name='{1}_GenericPlannedInterventions' value='6' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions6">Stabilize Current Placement</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions7' class='radio' name='{1}_GenericPlannedInterventions' value='7' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions7">Crisis Intervention</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions8' class='radio' name='{1}_GenericPlannedInterventions' value='8' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions8">Determine/Locate Alternative Placement</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions9' class='radio' name='{1}_GenericPlannedInterventions' value='9' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions9">Long-range Planning &#38; Decision Making</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions10' class='radio' name='{1}_GenericPlannedInterventions' value='10' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions10">Financial Counseling and/or Referrals</label>
                            </td><td colspan="2">
                                <%= string.Format("<input id='{1}_GenericPlannedInterventions11' class='radio' name='{1}_GenericPlannedInterventions' value='11' type='checkbox' {0} />", genericPlannedInterventions != null && genericPlannedInterventions.Contains("11") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlannedInterventions11">Other (enter details below)</label>
                                <%= Html.TextBox(Model.Type + "_GenericPlannedInterventionsOther", data.ContainsKey("GenericPlannedInterventionsOther") ? data["GenericPlannedInterventionsOther"].Answer : "", new { @id = Model.Type + "_GenericPlannedInterventionsOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div><label for="<%= Model.Type %>_GenericInterventionDetails">Intervention Details</label></div>
                <div><%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.ContainsKey("GenericInterventionDetails") ? data["GenericInterventionDetails"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericInterventionDetails", @class = "fill" })%></div>
            </td>
        </tr><tr>
            <th>Progress Towards Goals</th>
        </tr><tr>
            <td style="vertical-align:top;">
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericIntensityOfPain" class="float-left">Progress Towards Goals</label>
                    <div class="float-right"><%
                        var genericProgressTowardsGoals = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "None", Value = "None" },
                            new SelectListItem { Text = "Slight", Value = "Slight" },
                            new SelectListItem { Text = "Fair", Value = "Fair" },
                            new SelectListItem { Text = "Moderate", Value = "Moderate" },
                            new SelectListItem { Text = "Good", Value = "Good" },
                            new SelectListItem { Text = "Excellent", Value = "Excellent" }
                        }, "Value", "Text", data.ContainsKey("GenericProgressTowardsGoals") ? data["GenericProgressTowardsGoals"].Answer : "");%>
                        <%= Html.DropDownList(Model.Type + "_GenericProgressTowardsGoals", genericProgressTowardsGoals, new { @id = Model.Type + "_GenericProgressTowardsGoals", @class = "oe" })%>
                    </div>
                </div>
                <div><%= Html.TextArea(Model.Type + "_GenericProgressTowardsGoalsDetails", data.ContainsKey("GenericProgressTowardsGoalsDetails") ? data["GenericProgressTowardsGoalsDetails"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericProgressTowardsGoalsDetails", @class = "fill" })%></div>
                <div class="align-left strong">Follow-up Plan (check all that apply)</div>
                <input type="hidden" name="<%= Model.Type %>_GenericFollowUpPlan" value="" />
                <table class="align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericFollowUpPlan1' class='radio' name='{1}_GenericFollowUpPlan' value='1' type='checkbox' {0} />", genericFollowUpPlan != null && genericFollowUpPlan.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFollowUpPlan1">Follow-up Visit</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericFollowUpPlan2' class='radio' name='{1}_GenericFollowUpPlan' value='2' type='checkbox' {0} />", genericFollowUpPlan != null && genericFollowUpPlan.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFollowUpPlan2">Confer with Team</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericFollowUpPlan3' class='radio' name='{1}_GenericFollowUpPlan' value='3' type='checkbox' {0} />", genericFollowUpPlan != null && genericFollowUpPlan.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFollowUpPlan3">Provide External Referral</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericFollowUpPlan4' class='radio' name='{1}_GenericFollowUpPlan' value='4' type='checkbox' {0} />", genericFollowUpPlan != null && genericFollowUpPlan.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFollowUpPlan4">Discharge SW Services</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
         </tbody>
</table>