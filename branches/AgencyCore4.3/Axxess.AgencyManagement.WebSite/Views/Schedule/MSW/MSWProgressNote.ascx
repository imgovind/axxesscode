﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<%  var dictonary = new Dictionary<string, string>() { { DisciplineTasks.MSWProgressNote.ToString(), "MSWProgressNote" } }; %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                    <%= string.Format("{0}", Model.TypeName) %>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                     <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="Text1" /></div>
                    </div>
                    <div class="clear"/>       
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @id = Model.Type + "_TimeIn", @class = "time-picker" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @id = Model.Type + "_TimeOut", @class = "time-picker" })%></div>
                    </div>
                </td>
                <td>       
                    <div>&nbsp;
                        <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %><label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div><% } %>
                    </div>  
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PhysicianDropDown" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : data.AnswerOrEmptyString("PhysicianId"), new { @id = Model.Type + "_PhysicianDropDown", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"/>
                    <div >
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"/>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="mswProgressNoteContentId"><% Html.RenderPartial("~/Views/Schedule/MSW/MSWProgressNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate" value="<%= date.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
       <ul>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Save" onclick="MSWProgressNoteRemove();Visit.MSWProgressNote.Submit($(this),false,'<%= Model.Type %>','<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Save</a></li>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Submit" onclick="MSWProgressNoteAdd();Visit.MSWProgressNote.Submit($(this),true,'<%= Model.Type %>','<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Approve" onclick="MSWProgressNoteRemove();Visit.MSWProgressNote.Submit($(this),false,'<%= Model.Type %>','<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Return" onclick="MSWProgressNoteRemove();Visit.MSWProgressNote.Submit($(this),false,'<%= Model.Type %>','<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Cancel" onclick="MSWProgressNoteRemove();$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
</div><%
} %>
<script type="text/javascript">
    //Schedule.WarnTimeInOut("<%= Model.Type %>");
    $("#<%= Model.Type %>_MR").attr('readonly', true);
    function MSWProgressNoteAdd() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function MSWProgressNoteRemove() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
  
    $("#<%= Model.Type %>_PreviousNotes").change(function() {
        $("#mswProgressNoteContentId").load("/Schedule/MSWProgressNoteContent", { patientId: $("#<%= Model.Type %>_PatientId").val(), noteId: $("#<%= Model.Type %>_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
            }
        });
    });
</script>