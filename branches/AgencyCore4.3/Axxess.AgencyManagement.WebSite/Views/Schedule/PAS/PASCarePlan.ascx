﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] diet = data.AnswerArray("IsDiet"); %>
<%  string[] allergies = data.AnswerArray("Allergies"); %>
<span class="wintitle">PAS Care Plan | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "PASCarePlanForm" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", "100")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                     Personal Assistance Services Care Plan
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
            </tr>
            <tr>
                <td>
                     <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PASFrequency" class="float-left">Frequency:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_PASFrequency", data.AnswerOrEmptyString("PASFrequency"), new { @id = Model.Type + "_PASFrequency" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td>
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <label for="PASCarePlan_PreviousNotes" class="float-left">Previous Care Plans:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "PASCarePlan_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <label for="<%= Model.Type %>_DNR" class="float-left">DNR:</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_DNR", "1", data.AnswerOrEmptyString("DNR").Equals("1"), new { @id = Model.Type + "_DNR1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_DNR1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_DNR", "0", data.AnswerOrEmptyString("DNR").Equals("0"), new { @id = Model.Type + "_DNR2", @class = "radio" })%>
                            <label for="<%= Model.Type %>_DNR2" class="inline-radio">No</label>
                        </div>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <input name="<%= Model.Type %>_IsDiet" value="" type="hidden" />
                        <div class="float-left">
                            <%= string.Format("<input class='radio' id='{0}_IsDiet' name='{0}_IsDiet' value='1' type='checkbox' {1} />", Model.Type, diet.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_IsDiet" class="strong">Diet:</label>
                        </div>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Diet", data.AnswerOrEmptyString("Diet"), new { @id = Model.Type + "_Diet" })%></div>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <input name="<%= Model.Type %>_Allergies" value="" type="hidden" />
                        <div class="float-left">
                            <%= string.Format("<input class='radio' id='{0}_Allergies' name='{0}_Allergies' value='Yes' type='checkbox' {1} />", Model.Type, allergies.Contains("Yes").ToChecked())%>
                            <label for="<%= Model.Type %>_Allergies" class="strong">Allergies:</label>
                        </div>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AllergiesDescription", data.AnswerOrEmptyString("AllergiesDescription"), new { @id = Model.Type + "_AllergiesDescription" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_Content"><% Html.RenderPartial("~/Views/Schedule/PAS/PASCarePlanContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : "" %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="2">
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this),false,'<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this),true,'<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this),false,'<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.<%= Model.Type %>.Submit($(this),false,'<%=Model.EpisodeId %>','<%= Model.PatientId %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>