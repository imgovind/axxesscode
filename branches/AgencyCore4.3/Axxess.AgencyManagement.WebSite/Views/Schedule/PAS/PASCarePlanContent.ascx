﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%  string[] safetyMeasure = data.AnswerArray("SafetyMeasures"); %>
<%  string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
<%  string[] activitiesPermitted = data.AnswerArray("ActivitiesPermitted"); %>
<table class="fixed nursing">
    <tr>
        <th colspan="2">
            <input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
            Vital Sign Parameters
            &#8212;
            <%= string.Format("<input class='radio' id='{0}_IsVitalSignParameter' name='{0}_IsVitalSignParameter' value='1' type='checkbox' {1} />", Model.Type, isVitalSignParameter.Contains("1").ToChecked())%>
            <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
        </th>
    </tr>
    <tr class="vitalsigns">
        <td colspan="2">
            <table id="<%= Model.Type %>_IsVitalSignParameterMore" class="fixed">
                <tbody>
                    <tr>
                        <th></th>
                        <th>SBP</th>
                        <th>DBP</th>
                        <th>HR</th>
                        <th>Resp</th>
                        <th>Temp</th>
                        <th>Weight</th>
                    </tr>
                    <tr>
                        <th>greater than (&#62;)</th>
                        <td><%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.AnswerOrEmptyString("SystolicBPGreaterThan"), new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.AnswerOrEmptyString("DiastolicBPGreaterThan"), new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.AnswerOrEmptyString("PulseGreaterThan"), new { @id = Model.Type + "_PulseGreaterThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.AnswerOrEmptyString("RespirationGreaterThan"), new { @id = Model.Type + "_RespirationGreaterThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_TempGreaterThan", data.AnswerOrEmptyString("TempGreaterThan"), new { @id = Model.Type + "_TempGreaterThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.AnswerOrEmptyString("WeightGreaterThan"), new { @id = Model.Type + "_WeightGreaterThan", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <th>less than (&#60;)</th>
                        <td><%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.AnswerOrEmptyString("SystolicBPLessThan"), new { @id = Model.Type + "_SystolicBPLessThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.AnswerOrEmptyString("DiastolicBPLessThan"), new { @id = Model.Type + "_DiastolicBPLessThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_PulseLessThan", data.AnswerOrEmptyString("PulseLessThan"), new { @id = Model.Type + "_PulseLessThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_RespirationLessThan", data.AnswerOrEmptyString("RespirationLessThan"), new { @id = Model.Type + "_RespirationLessThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_TempLessThan", data.AnswerOrEmptyString("TempLessThan"), new { @id = Model.Type + "_TempLessThan", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_WeightLessThan", data.AnswerOrEmptyString("WeightLessThan"), new { @id = Model.Type + "_WeightLessThan", @class = "fill" })%></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="2">Safety Precautions</th>
    </tr>
    <tr>
        <td colspan="2">
            <input type="hidden" name="<%= Model.Type %>_SafetyMeasures" value="" />
            <table class="fixed align-left">
                <tbody>
                    <tr>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures1' name='{0}_SafetyMeasures' value='1' {1} />", Model.Type, safetyMeasure.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures1" class="radio">Anticoagulant Precautions</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures5' name='{0}_SafetyMeasures' value='5' {1} />", Model.Type, safetyMeasure.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures5" class="radio">Keep Side Rails Up</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures9' name='{0}_SafetyMeasures' value='9' {1} />", Model.Type, safetyMeasure.Contains("9").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures9" class="radio">Safety in ADLs</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures13' name='{0}_SafetyMeasures' value='13' {1} />", Model.Type, safetyMeasure.Contains("13").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures2' name='{0}_SafetyMeasures' value='2' {1} />", Model.Type, safetyMeasure.Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures2" class="radio">Emergency Plan Developed</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures6' name='{0}_SafetyMeasures' value='6' {1} />", Model.Type, safetyMeasure.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures6" class="radio">Neutropenic Precautions</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures10' name='{0}_SafetyMeasures' value='10' {1} />", Model.Type, safetyMeasure.Contains("10").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures10" class="radio">Seizure Precautions</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures14' name='{0}_SafetyMeasures' value='14' {1} />", Model.Type, safetyMeasure.Contains("14").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures3' name='{0}_SafetyMeasures' value='3' {1} />", Model.Type, safetyMeasure.Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures3" class="radio">Fall Precautions</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures7' name='{0}_SafetyMeasures' value='7' {1} />", Model.Type, safetyMeasure.Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures7" class="radio">O<sub>2</sub> Precautions</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures11' name='{0}_SafetyMeasures' value='11' {1} />", Model.Type, safetyMeasure.Contains("11").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures11" class="radio">Sharps Safety</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures15' name='{0}_SafetyMeasures' value='15' {1} />", Model.Type, safetyMeasure.Contains("15").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures15" class="radio">Use of Assistive Devices</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures4' name='{0}_SafetyMeasures' value='4' {1} />", Model.Type, safetyMeasure.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures4" class="radio">Keep Pathway Clear</label>
                        </td>
                        <td>
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures8' name='{0}_SafetyMeasures' value='8' {1} />", Model.Type, safetyMeasure.Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures8" class="radio">Proper Position During Meals</label>
                        </td>
                        <td colspan="2">
                            <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures12' name='{0}_SafetyMeasures' value='12' {1} />", Model.Type, safetyMeasure.Contains("12").ToChecked())%>
                            <label for="<%= Model.Type %>_SafetyMeasures12" class="radio">Slow Position Change</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="align-center">
                            <label class="float-left">Other (Specify):</label>
                            <%= Html.TextArea(Model.Type + "_OtherSafetyMeasures", data.AnswerOrEmptyString("OtherSafetyMeasures"), new { @id = Model.Type + "_OtherSafetyMeasures", @class = "fill" })%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th>Functional Limitations</th>
        <th>Activities Permitted</th>
    </tr>
    <tr>
        <td>
            <input name="<%= Model.Type %>_FunctionLimitations" value=" " type="hidden" />
            <table class="fixed align-left">
                <tbody>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations1' name='{0}_FunctionLimitations' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("1").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations1" class="radio">Amputation</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations2' name='{0}_FunctionLimitations' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("2").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations3' name='{0}_FunctionLimitations' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("3").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations3" class="radio">Contracture</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations4' name='{0}_FunctionLimitations' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("4").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations4" class="radio">Hearing</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations5' name='{0}_FunctionLimitations' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("5").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations5" class="radio">Paralysis</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations6' name='{0}_FunctionLimitations' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("6").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations6" class="radio">Endurance</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations7' name='{0}_FunctionLimitations' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("7").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations7" class="radio">Ambulation</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations8' name='{0}_FunctionLimitations' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("8").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations8" class="radio">Speech</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitations9' name='{0}_FunctionLimitations' value='9' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("9").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitations9" class="radio">Legally Blind</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_FunctionLimitationsA' name='{0}_FunctionLimitations' value='A' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("A").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitationsA" class="radio">Dyspnea with Minimal Exertion</label>
                        </td>
                        <td colspan="2">
                            <%= string.Format("<input id='{0}_FunctionLimitationsB' name='{0}_FunctionLimitations' value='B' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("B").ToChecked()) %>
                            <label for="<%= Model.Type %>_FunctionLimitationsB" class="radio">Other</label>
                            <div class="float-right"><%= Html.TextBox(Model.Type + "_FunctionLimitationsOther", data.AnswerOrEmptyString("FunctionLimitationsOther"), new { @id = Model.Type + "_FunctionLimitationsOther" })%></div>
                        </td>
                    </tr>                    
                </tbody>
            </table>
        </td>
        <td>
            <input type="hidden" name="<%= Model.Type %>_ActivitiesPermitted" value="" />
            <table class="fixed align-left">
                <tbody>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted1' name='{0}_ActivitiesPermitted' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted1" class="radio">Complete bed rest</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted2' name='{0}_ActivitiesPermitted' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted2" class="radio">Bed rest with BRP</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted3' name='{0}_ActivitiesPermitted' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted3" class="radio">Up as tolerated</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted4' name='{0}_ActivitiesPermitted' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted4" class="radio">Transfer bed-chair</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted5' name='{0}_ActivitiesPermitted' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted5" class="radio">Exercise prescribed</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted6' name='{0}_ActivitiesPermitted' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted6" class="radio">Partial weight bearing</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted7' name='{0}_ActivitiesPermitted' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted7" class="radio">Independent at home</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted8' name='{0}_ActivitiesPermitted' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted8" class="radio">Crutches</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted9' name='{0}_ActivitiesPermitted' value='9' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("9").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted9" class="radio">Cane</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted10' name='{0}_ActivitiesPermitted' value='10' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("10").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted10" class="radio">Wheelchair</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted11' name='{0}_ActivitiesPermitted' value='11' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("11").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted11" class="radio">Walker</label>
                        </td>
                        <td>
                            <%= string.Format("<input id='{0}_ActivitiesPermitted12' name='{0}_ActivitiesPermitted' value='12' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("12").ToChecked())%>
                            <label for="<%= Model.Type %>_ActivitiesPermitted12">Other:</label>
                            <div class="float-right"><%= Html.TextBox(Model.Type + "_ActivitiesPermittedOther", data.AnswerOrEmptyString("ActivitiesPermittedOther"), new { @id = Model.Type + "_ActivitiesPermittedOther" })%></div>
                        </td>
                    </tr>                    
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Plan Details
            <em>* QV = Every Visit; QW = Every Week; PR = Patient Request; N/A = Not Applicable</em>
        </th>
    </tr>
    <tr>
        <td>
            <table class="fixed">
                <thead>
                    <tr>
                        <th colspan="4">Assignment</th>
                        <th colspan="4">Status</th>
                    </tr>
                    <tr>
                        <th colspan="4">Vital Signs</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Temperature<%= Html.Hidden(Model.Type + "_VitalSignsTemperature", " ", new { @id = "_VitalSignsTemperatureHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "3", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("3"), new { @id = Model.Type + "_VitalSignsTemperature3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "2", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("2"), new { @id = Model.Type + "_VitalSignsTemperature2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "1", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("1"), new { @id = Model.Type + "_VitalSignsTemperature1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "0", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("0"), new { @id = Model.Type + "_VitalSignsTemperature0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Blood Pressure<%= Html.Hidden(Model.Type + "_VitalSignsBloodPressure", " ", new { @id = "_VitalSignsBloodPressureHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "3", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("3"), new { @id = Model.Type + "_VitalSignsBloodPressure3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "2", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("2"), new { @id = Model.Type + "_VitalSignsBloodPressure2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "1", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("1"), new { @id = Model.Type + "_VitalSignsBloodPressure1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "0", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("0"), new { @id = Model.Type + "_VitalSignsBloodPressure0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Heart Rate<%= Html.Hidden(Model.Type + "_VitalSignsHeartRate", " ", new { @id = "_VitalSignsHeartRateHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "3", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("3"), new { @id = Model.Type + "_VitalSignsHeartRate3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "2", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("2"), new { @id = Model.Type + "_VitalSignsHeartRate2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "1", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("1"), new { @id = Model.Type + "_VitalSignsHeartRate1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "0", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("0"), new { @id = Model.Type + "_VitalSignsHeartRate0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Respirations<%= Html.Hidden(Model.Type + "_VitalSignsRespirations", " ", new { @id = "_VitalSignsRespirationsHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "3", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("3"), new { @id = Model.Type + "_VitalSignsRespirations3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "2", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("2"), new { @id = Model.Type + "_VitalSignsRespirations2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "1", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("1"), new { @id = Model.Type + "_VitalSignsRespirations1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "0", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("0"), new { @id = Model.Type + "_VitalSignsRespirations0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Weight<%= Html.Hidden(Model.Type + "_VitalSignsWeight", " ", new { @id = "_VitalSignsWeightHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "3", data.AnswerOrEmptyString("VitalSignsWeight").Equals("3"), new { @id = Model.Type + "_VitalSignsWeight3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "2", data.AnswerOrEmptyString("VitalSignsWeight").Equals("2"), new { @id = Model.Type + "_VitalSignsWeight2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "1", data.AnswerOrEmptyString("VitalSignsWeight").Equals("1"), new { @id = Model.Type + "_VitalSignsWeight1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "0", data.AnswerOrEmptyString("VitalSignsWeight").Equals("0"), new { @id = Model.Type + "_VitalSignsWeight0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <th colspan="4">Personal Care</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Bed Bath<%= Html.Hidden(Model.Type + "_PersonalCareBedBath", " ", new { @id = "_PersonalCareBedBathHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "3", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("3"), new { @id = Model.Type + "_PersonalCareBedBath3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "2", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("2"), new { @id = Model.Type + "_PersonalCareBedBath2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "1", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("1"), new { @id = Model.Type + "_PersonalCareBedBath1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "0", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("0"), new { @id = Model.Type + "_PersonalCareBedBath0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Chair Bath<%= Html.Hidden(Model.Type + "_PersonalCareAssistWithChairBath", " ", new { @id = "_PersonalCareAssistWithChairBathHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "3", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("3"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "2", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "1", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "0", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Tub Bath<%= Html.Hidden(Model.Type + "_PersonalCareTubBath", " ", new { @id = "_PersonalCareTubBathHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "3", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("3"), new { @id = Model.Type + "_PersonalCareTubBath3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "2", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("2"), new { @id = Model.Type + "_PersonalCareTubBath2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "1", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("1"), new { @id = Model.Type + "_PersonalCareTubBath1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "0", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("0"), new { @id = Model.Type + "_PersonalCareTubBath0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Shower<%= Html.Hidden(Model.Type + "_PersonalCareShower", " ", new { @id = "_PersonalCareShowerHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "3", data.AnswerOrEmptyString("PersonalCareShower").Equals("3"), new { @id = Model.Type + "_PersonalCareShower3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "2", data.AnswerOrEmptyString("PersonalCareShower").Equals("2"), new { @id = Model.Type + "_PersonalCareShower2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "1", data.AnswerOrEmptyString("PersonalCareShower").Equals("1"), new { @id = Model.Type + "_PersonalCareShower1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "0", data.AnswerOrEmptyString("PersonalCareShower").Equals("0"), new { @id = Model.Type + "_PersonalCareShower0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Shower w/Chair<%= Html.Hidden(Model.Type + "_PersonalCareShowerWithChair", " ", new { @id = "_PersonalCareShowerWithChairHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "3", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("3"), new { @id = Model.Type + "_PersonalCareShowerWithChair3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "2", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("2"), new { @id = Model.Type + "_PersonalCareShowerWithChair2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "1", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("1"), new { @id = Model.Type + "_PersonalCareShowerWithChair1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "0", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("0"), new { @id = Model.Type + "_PersonalCareShowerWithChair0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Shampoo Hair<%= Html.Hidden(Model.Type + "_PersonalCareShampooHair", " ", new { @id = "_PersonalCareShampooHairHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "3", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("3"), new { @id = Model.Type + "_PersonalCareShampooHair3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "2", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("2"), new { @id = Model.Type + "_PersonalCareShampooHair2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "1", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("1"), new { @id = Model.Type + "_PersonalCareShampooHair1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "0", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("0"), new { @id = Model.Type + "_PersonalCareShampooHair0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Hair Care/Comb Hair<%= Html.Hidden(Model.Type + "_PersonalCareHairCare", " ", new { @id = "_PersonalCareHairCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "3", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("3"), new { @id = Model.Type + "_PersonalCareHairCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "2", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("2"), new { @id = Model.Type + "_PersonalCareHairCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "1", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("1"), new { @id = Model.Type + "_PersonalCareHairCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "0", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("0"), new { @id = Model.Type + "_PersonalCareHairCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Oral Care<%= Html.Hidden(Model.Type + "_PersonalCareOralCare", " ", new { @id = "_PersonalCareOralCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "3", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("3"), new { @id = Model.Type + "_PersonalCareOralCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "2", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("2"), new { @id = Model.Type + "_PersonalCareOralCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "1", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("1"), new { @id = Model.Type + "_PersonalCareOralCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "0", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("0"), new { @id = Model.Type + "_PersonalCareOralCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Skin Care<%= Html.Hidden(Model.Type + "_PersonalCareSkinCare", " ", new { @id = "_PersonalCareSkinCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "3", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("3"), new { @id = Model.Type + "_PersonalCareSkinCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "2", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("2"), new { @id = Model.Type + "_PersonalCareSkinCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "1", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("1"), new { @id = Model.Type + "_PersonalCareSkinCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "0", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("0"), new { @id = Model.Type + "_PersonalCareSkinCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Pericare<%= Html.Hidden(Model.Type + "_PersonalCarePericare", " ", new { @id = "_PersonalCarePericareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "3", data.AnswerOrEmptyString("PersonalCarePericare").Equals("3"), new { @id = Model.Type + "_PersonalCarePericare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "2", data.AnswerOrEmptyString("PersonalCarePericare").Equals("2"), new { @id = Model.Type + "_PersonalCarePericare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "1", data.AnswerOrEmptyString("PersonalCarePericare").Equals("1"), new { @id = Model.Type + "_PersonalCarePericare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "0", data.AnswerOrEmptyString("PersonalCarePericare").Equals("0"), new { @id = Model.Type + "_PersonalCarePericare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Nail Care<%= Html.Hidden(Model.Type + "_PersonalCareNailCare", " ", new { @id = "_PersonalCareNailCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "3", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("3"), new { @id = Model.Type + "_PersonalCareNailCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "2", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("2"), new { @id = Model.Type + "_PersonalCareNailCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "1", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("1"), new { @id = Model.Type + "_PersonalCareNailCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "0", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("0"), new { @id = Model.Type + "_PersonalCareNailCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Shave<%= Html.Hidden(Model.Type + "_PersonalCareShave", " ", new { @id = "_PersonalCareShaveHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "3", data.AnswerOrEmptyString("PersonalCareShave").Equals("3"), new { @id = Model.Type + "_PersonalCareShave3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "2", data.AnswerOrEmptyString("PersonalCareShave").Equals("2"), new { @id = Model.Type + "_PersonalCareShave2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "1", data.AnswerOrEmptyString("PersonalCareShave").Equals("1"), new { @id = Model.Type + "_PersonalCareShave1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "0", data.AnswerOrEmptyString("PersonalCareShave").Equals("0"), new { @id = Model.Type + "_PersonalCareShave0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Dressing<%= Html.Hidden(Model.Type + "_PersonalCareAssistWithDressing", " ", new { @id = "_PersonalCareAssistWithDressingHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "3", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("3"), new { @id = Model.Type + "_PersonalCareAssistWithDressing3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "2", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithDressing2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "1", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithDressing1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "0", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithDressing0", @class = "radio" })%></td>
                    </tr>
                </thead>
            </table>
        </td>
        <td>
            <table class="fixed">
                <thead>
                    <tr>
                        <th colspan="4">Assignment</th>
                        <th colspan="4">Status</th>
                    </tr>
                    <tr>
                        <th colspan="4">Elimination</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Bed Pan/Urinal<%= Html.Hidden(Model.Type + "_EliminationAssistWithBedPan", " ", new { @id = "_EliminationAssistWithBedPanHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("3"), new { @id = Model.Type + "_EliminationAssistWithBedPan3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("2"), new { @id = Model.Type + "_EliminationAssistWithBedPan2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "1", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("1"), new { @id = Model.Type + "_EliminationAssistWithBedPan1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "0", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("0"), new { @id = Model.Type + "_EliminationAssistWithBedPan0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with BSC<%= Html.Hidden(Model.Type + "_EliminationAssistBSC", " ", new { @id = "_EliminationAssistBSCHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "3", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("3"), new { @id = Model.Type + "_EliminationAssistBSC3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "2", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("2"), new { @id = Model.Type + "_EliminationAssistBSC2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "1", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("1"), new { @id = Model.Type + "_EliminationAssistBSC1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "0", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("0"), new { @id = Model.Type + "_EliminationAssistBSC0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Incontinence Care<%= Html.Hidden(Model.Type + "_EliminationIncontinenceCare", " ", new { @id = "_EliminationIncontinenceCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "3", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("3"), new { @id = Model.Type + "_EliminationIncontinenceCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "2", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("2"), new { @id = Model.Type + "_EliminationIncontinenceCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "1", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("1"), new { @id = Model.Type + "_EliminationIncontinenceCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "0", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("0"), new { @id = Model.Type + "_EliminationIncontinenceCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Empty Drainage Bag<%= Html.Hidden(Model.Type + "_EliminationEmptyDrainageBag", " ", new { @id = "_EliminationEmptyDrainageBagHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "3", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("3"), new { @id = Model.Type + "_EliminationEmptyDrainageBag3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "2", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("2"), new { @id = Model.Type + "_EliminationEmptyDrainageBag2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "1", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("1"), new { @id = Model.Type + "_EliminationEmptyDrainageBag1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "0", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("0"), new { @id = Model.Type + "_EliminationEmptyDrainageBag0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Record Bowel Movement<%= Html.Hidden(Model.Type + "_EliminationRecordBowelMovement", " ", new { @id = "_EliminationRecordBowelMovementHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "3", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("3"), new { @id = Model.Type + "_EliminationRecordBowelMovement3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "2", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("2"), new { @id = Model.Type + "_EliminationRecordBowelMovement2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "1", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("1"), new { @id = Model.Type + "_EliminationRecordBowelMovement1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "0", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("0"), new { @id = Model.Type + "_EliminationRecordBowelMovement0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Catheter Care<%= Html.Hidden(Model.Type + "_EliminationCatheterCare", " ", new { @id = "_EliminationCatheterCareHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "3", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("3"), new { @id = Model.Type + "_EliminationCatheterCare3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "2", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("2"), new { @id = Model.Type + "_EliminationCatheterCare2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "1", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("1"), new { @id = Model.Type + "_EliminationCatheterCare1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "0", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("0"), new { @id = Model.Type + "_EliminationCatheterCare0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <th colspan="4">Activity</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Dangle on Side of Bed<%= Html.Hidden(Model.Type + "_ActivityDangleOnSideOfBed", " ", new { @id = "_ActivityDangleOnSideOfBedHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "3", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("3"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "2", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("2"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "1", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("1"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "0", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("0"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Turn &#38; Position<%= Html.Hidden(Model.Type + "_ActivityTurnPosition", " ", new { @id = "_ActivityTurnPositionHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "3", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("3"), new { @id = Model.Type + "_ActivityTurnPosition3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "2", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("2"), new { @id = Model.Type + "_ActivityTurnPosition2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "1", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("1"), new { @id = Model.Type + "_ActivityTurnPosition1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "0", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("0"), new { @id = Model.Type + "_ActivityTurnPosition0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Transfer<%= Html.Hidden(Model.Type + "_ActivityAssistWithTransfer", " ", new { @id = "_ActivityAssistWithTransferHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "3", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("3"), new { @id = Model.Type + "_ActivityAssistWithTransfer3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "2", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithTransfer2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "1", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithTransfer1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "0", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithTransfer0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Range of Motion<%= Html.Hidden(Model.Type + "_ActivityRangeOfMotion", " ", new { @id = "_ActivityRangeOfMotionHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "3", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("3"), new { @id = Model.Type + "_ActivityRangeOfMotion3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "2", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("2"), new { @id = Model.Type + "_ActivityRangeOfMotion2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "1", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("1"), new { @id = Model.Type + "_ActivityRangeOfMotion1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "0", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("0"), new { @id = Model.Type + "_ActivityRangeOfMotion0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Ambulation<%= Html.Hidden(Model.Type + "_ActivityAssistWithAmbulation", " ", new { @id = "_ActivityAssistWithAmbulationHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "3", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("3"), new { @id = Model.Type + "_ActivityAssistWithAmbulation3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "2", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithAmbulation2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "1", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithAmbulation1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "0", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithAmbulation0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <th colspan="4">Household Task</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Make Bed<%= Html.Hidden(Model.Type + "_HouseholdTaskMakeBed", " ", new { @id = "_HouseholdTaskMakeBedHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "3", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("3"), new { @id = Model.Type + "_HouseholdTaskMakeBed3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "2", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("2"), new { @id = Model.Type + "_HouseholdTaskMakeBed2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "1", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("1"), new { @id = Model.Type + "_HouseholdTaskMakeBed1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "0", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("0"), new { @id = Model.Type + "_HouseholdTaskMakeBed0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Change Linen<%= Html.Hidden(Model.Type + "_HouseholdTaskChangeLinen", " ", new { @id = "_HouseholdTaskChangeLinenHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "3", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("3"), new { @id = Model.Type + "_HouseholdTaskChangeLinen3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "2", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("2"), new { @id = Model.Type + "_HouseholdTaskChangeLinen2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "1", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("1"), new { @id = Model.Type + "_HouseholdTaskChangeLinen1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "0", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("0"), new { @id = Model.Type + "_HouseholdTaskChangeLinen0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Light Housekeeping<%= Html.Hidden(Model.Type + "_HouseholdTaskLightHousekeeping", " ", new { @id = "_HouseholdTaskLightHousekeepingHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "3", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("3"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "2", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("2"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "1", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("1"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "0", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("0"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <th colspan="4">Nutrition</th>
                        <th>QV</th>
                        <th>QW</th>
                        <th>PR</th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <td colspan="4">Meal Set-up<%= Html.Hidden(Model.Type + "_NutritionMealSetUp", " ", new { @id = "_NutritionMealSetUpHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "3", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("3"), new { @id = Model.Type + "_NutritionMealSetUp3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "2", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("2"), new { @id = Model.Type + "_NutritionMealSetUp2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "1", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("1"), new { @id = Model.Type + "_NutritionMealSetUp1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "0", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("0"), new { @id = Model.Type + "_NutritionMealSetUp0", @class = "radio" })%></td>
                    </tr>
                    <tr>
                        <td colspan="4">Assist with Feeding<%= Html.Hidden(Model.Type + "_NutritioAssistWithFeeding", " ", new { @id = "_NutritioAssistWithFeedingHidden" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "3", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("3"), new { @id = Model.Type + "_NutritioAssistWithFeeding3", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "2", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("2"), new { @id = Model.Type + "_NutritioAssistWithFeeding2", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "1", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("1"), new { @id = Model.Type + "_NutritioAssistWithFeeding1", @class = "radio" })%></td>
                        <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "0", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("0"), new { @id = Model.Type + "_NutritioAssistWithFeeding0", @class = "radio" })%></td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="2">Comments</th>
    </tr>
    <tr>
        <td colspan="2"><%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "fill" })%></td>
    </tr>
</table>