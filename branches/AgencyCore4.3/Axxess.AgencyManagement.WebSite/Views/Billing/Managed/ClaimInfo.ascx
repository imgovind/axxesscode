﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimSnapShotViewData>" %>
<div class="two-thirds">
    <table width="100%" height="100%">
        <tbody>
            <tr style="vertical-align:top;">
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Patient Name:</td>
                                <td><label class="strong"><%= Model.PatientName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient ID #:</td>
                                <td><label class="strong"><%= Model.PatientIdNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient Insurance Id #:</td>
                                <td><label class="strong"><%= Model.IsuranceIdNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Payment Date:</td>
                                <td><label class="strong"><%= Model.PaymentDateFormatted %></label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <%  if (Model.Visible) { %>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Insurance/Payer:</td>
                                <td><label class="strong"><%= Model.PayorName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Health Plan Id:</td>
                                <td class="align-left"><label class="strong"><%= Model.HealthPlainId %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Authorization Number:</td>
                                <td class="align-left"><label class="strong"><%= Model.AuthorizationNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Billed Date:</td>
                                <td><label class="strong"><%= Model.ClaimDateFormatted %></label></td>
                            </tr>
                        </tbody>
                    </table>
                    <%  } %>
                </td>
            </tr>
            
            <tr style="vertical-align:bottom;">
                <td colspan="2">
                    <div class="buttons float-left">
                        <ul>
                            <li class='add-payment-button'>
                                <a href='javascript:void(0);' onclick='UserInterface.ShowNewManagedClaimPaymentModal();'>Post Payment</a>
                            </li>
                            <li class='add-adjustment-button'>
                                <a href='javascript:void(0);' onclick='UserInterface.ShowNewManagedClaimAdjustmentModal();'>Post Adjustment</a>
                            </li>
                            <li>
                                <a href='javascript:void(0);' onclick="U.GetAttachment('Billing/InvoicePdf', { 'patientId': '<%= Model.PatientId %>', 'Id': '<%= Model.Id %>', 'isForPatient': 'true' });" >Create Invoice</a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="one-third encapsulation">
    <h4>Quick Reports</h4>
    <ul>
        <li>
            <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowManagedClaimPayments('{0}', '{1}');\" >View Payments</a>", Model.Id, Model.PatientId)%>
        </li>
        <li>
            <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowManagedClaimAdjustments('{0}', '{1}');\" >View Adjustments</a>", Model.Id, Model.PatientId)%>
        </li>
        <li>
            <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadClaimLog('ManagedClaim','{0}','{1}');\" >Activity Logs</a>", Model.Id, Model.PatientId)%>
        </li>
    </ul>
</div>