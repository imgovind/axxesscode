﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PaymentInformation>>" %>
<table class="remittance">
        <%  int zebra = 0, count = 0; %>
        <%  if (Model != null && Model.Count > 0) { %>
            <%  foreach (var claimPaymentInfo in Model)
                { %>
                  <tr class="<%= zebra % 2 > 0 ? "even" : "odd" %>">
                     <td colspan="4">
                        <table class="claim">
                           <tbody>
                              <tr class="top">
                                <td><label class="float-left">Patient Name:</label><label class="float-right"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty%></label></td>
                                <td><%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %><label class="float-left"><%= claimPaymentInfo.Patient.IdQualifierName%>:</label><label class="float-right"><%= claimPaymentInfo.Patient.Id%></label><%  } %></td>
                                <td><label class="float-left">Patient Control Number:</label><label class="float-right"><%= claimPaymentInfo.PatientControlNumber%></label></td>
                                <td><label class="float-left">ICN Number:</label><label class="float-right"><%= claimPaymentInfo.PayerClaimControlNumber%></label></td>
                              </tr>
                              <tr>
                                <td><label class="float-left">Start Date:</label><label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label></td>
                                <td><label class="float-left">End Date:</label><label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label></td>
                                <td><label class="float-left">Type Of Bill:</label><label class="float-right"><%= claimPaymentInfo.TypeOfBill%></label></td>
                                <td><label class="float-left">Claim Status:</label><label class="float-right"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode)%></label></td>
                              </tr>
                              <tr>
                                <td><label class="float-left">Claim Number:</label><label class="float-right"><%= count + 1 %></label></td>
                                <td><label class="float-left">Reported Charge:</label><label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label></td>
                                <td><label class="float-left">Remittance:</label><label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label></td>
                                <td><label class="float-left">Line Adjustment Amount:</label><label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal)%></label></td>
                              </tr>
                              <tr class="bottom">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><label class="float-left">Paid Amount:</label><label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount) %></label></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                <%  count++; %>
                <%  zebra++; %>
            <%  } %>
        <%  } %>
</table>
