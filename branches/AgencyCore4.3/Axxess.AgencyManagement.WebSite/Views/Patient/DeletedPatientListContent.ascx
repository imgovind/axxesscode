﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<%= Html
        .Telerik()
            .Grid(Model)
                    .Name("List_Patient_Deleted_Grid")
        .HtmlAttributes(new { @style = "top:50px" }) 
        .Columns(columns =>
        {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
            columns.Bound(p => p.DisplayName).Title("Patient").Width(180);
            columns.Bound(p => p.Address).Title("Address").Sortable(false);
            columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(100).Sortable(true);
            columns.Bound(p => p.Gender).Width(80).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
            columns.Bound(p => p.Status).Title("Last Status").Width(80).Sortable(false);
            columns.Bound(p => p.Id).Width(90).Sortable(false).Template(s => string.Format("<a href=\"javascript:void(0);\" onclick=\"Patient.RestoreDeleted('{0}');\">Restore</a>", s.Id)).Title("Action").Visible(!Current.IsAgencyFrozen);
        })
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)%>
 
 <script type="text/javascript">
    $("#List_Patient_Deleted_Grid .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    $("#List_Patient_Deleted_Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "U.RebindDataGridContent('List_Patient_Deleted','Patient/DeletedPatientContent',{ BranchId : \"" + $('#List_Patient_Deleted_BranchId').val() + "\" },'" + U.ParameterByName(link, 'List_Patient_Deleted_Grid-orderBy') + "');");
    });
</script>