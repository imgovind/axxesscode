﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<span class="wintitle">List Patients | <%= Current.AgencyName %></span>
<% var pageName = "PatientList";  %>
<div class="wrapper grid-bg" style="height:100%">
    <div class="wide-column">
        <div class="row">
            <% if(!Current.IsAgencyFrozen) { %>
            <div class="float-left">
                <div class="buttons">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="UserInterface.ShowNewPatient();">New Patient</a></li>
                    </ul>
                </div>
            </div>
            <% } %>
            <label class="float-left">Branch:</label>
            <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName+"_BranchId" })%></div>
            <div class="row">
                <label for="<%= pageName %>_Status" class="float-left">Status:</label>
                <div class="float-left">
                    <select id="<%= pageName %>_Status" name="Status" >
                        <option value="0">All</option>
                        <option value="1" selected>Active</option>
                        <option value="2">Discharged</option>
                        <option value="3">Pending</option>
                        <option value="4">Non-Admit</option>
                    </select>
                </div>
            </div>
            <%  var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]); %>
            <div class="float-left">
                <div class="buttons">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="Patient.LoadPatientListContent(Patient.PatientListInput('<%= sortParams %>'));">Generate</a></li>
                    </ul>
                </div>
            </div>
            <div class="buttons">
                <ul class="float-right">
                    <li><%= Html.ActionLink("Excel Export", "Patients", "Export", new { BranchId = Guid.Empty, Status = 1 }, new { @id = pageName +"_ExportLink", @class = "excel" })%></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div id="<%= pageName %>_Search" ></div>
        </div>
    </div>
    <div id="<%= pageName %>GridContainer" class="list-container"><% Html.RenderPartial("ListContent", Model); %></div>
</div>