﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEligibility>" %>
<div class="wrapper main">
<% if (Model != null) { %>
    <% if (Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() && Model.Request_Validation.success.IsEqual("yes"))  { %>
    <fieldset>
        <div class="wide-column">
            <div class="row align-center">Server Error: <%= Model.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? Model.Request_Validation.reject_reason_code : string.Empty %><%= Model.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? Model.Request_Validation.follow_up_action_code : string.Empty %></div>
        </div>
    </fieldset>
    <% } else if(Model.Request_Result != null && Model.Request_Result.success.IsNotNullOrEmpty() && Model.Request_Result.success.IsEqual("no")) { %>
    <fieldset>
        <div class="wide-column">
            <div class="row align-center">Data Error: <%= Model.Request_Result.response.IsNotNullOrEmpty() ? Model.Request_Result.response.Contains("input error:") ? Model.Request_Result.response.Remove(0,12) : Model.Request_Result.response : string.Empty %></div>
        </div>
    </fieldset>
    <% } else { %>
        <% if (Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() && Model.Functional_Acknowledgment.success.IsEqual("no"))
           { %>
            <%  if (Model.Subscriber != null && Model.Subscriber.success.IsNotNullOrEmpty() && Model.Subscriber.success.IsEqual("yes")) { %>
                <fieldset>
                <legend>Patient Information</legend>
                <div class="column">
                    <div class="row">
                        <div class="float-left">Medicare Number:</div>
                        <div class="float-right strong"><%= Model.Subscriber.identification_code.IsNotNullOrEmpty() ? Model.Subscriber.identification_code : string.Empty %></div>
                        <div class="clear"></div>
                        <div class="float-left">Date of Birth:</div>
                        <div class="float-right strong"><%= Model.Subscriber.date.IsNotNullOrEmpty() ? Model.Subscriber.date.ToDateTimeString() : string.Empty %></div>
                    </div>
                </div><div class="column">
                    <div class="row">
                        <div class="float-left">Name:</div>
                        <div class="float-right strong"><%= Model.Subscriber.last_name.IsNotNullOrEmpty() ? Model.Subscriber.last_name : string.Empty%>, <%= Model.Subscriber.first_name.IsNotNullOrEmpty() ? Model.Subscriber.first_name : string.Empty%> <%= Model.Subscriber.middle_name.IsNotNullOrEmpty() ? Model.Subscriber.middle_name : string.Empty%></div>
                        <div class="clear"></div>
                        <div class="float-left">Gender:</div>
                        <div class="float-right strong"><%= Model.Subscriber.gender.IsNotNullOrEmpty() ? Model.Subscriber.gender : string.Empty %></div>
                    </div>
                </div>
            </fieldset>
            <% } %>
            <fieldset>
                <legend>Insurance Coverage Information</legend>
                <h3>Medicare</h3>
                <% if (Model.Medicare_Part_A != null && Model.Medicare_Part_A.success.IsNotNullOrEmpty() && Model.Medicare_Part_A.success.IsEqual("yes")) { %>
                    <div class="column">
                    <div class="row">
                        <div class="float-left">Part A:</div>
                        <div class="float-right strong"><%= Model.Medicare_Part_A.eligibility_or_benefit_information.IsNotNullOrEmpty() && Model.Medicare_Part_A.eligibility_or_benefit_information == "Inactive" ? "Inactive" : Model.Medicare_Part_A.date.IsNotNullOrEmpty() ? Model.Medicare_Part_A.date.ToDateTimeString() + " - Current" : string.Empty%></div>
                    </div>
                </div>
                <% } %>
                <% if (Model.Medicare_Part_B != null && Model.Medicare_Part_B.success.IsNotNullOrEmpty() && Model.Medicare_Part_B.success.IsEqual("yes")) { %>
                    <div class="column">
                        <div class="row">
                            <div class="float-left">Part B:</div>
                            <div class="float-right strong"><%= Model.Medicare_Part_B.eligibility_or_benefit_information.IsNotNullOrEmpty() && Model.Medicare_Part_B.eligibility_or_benefit_information == "Inactive" ? "Inactive" : Model.Medicare_Part_B.date.IsNotNullOrEmpty() ? Model.Medicare_Part_B.date.ToDateTimeString() + " - Current" : string.Empty%></div>
                        </div>
                    </div>
                    <% if (Model.Health_Benefit_Plan_Coverage != null && !Model.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) { %>
                        <div class="wide-column">
                            <div class="row">
                                <div class="float-left">Primary Payer:</div>
                                <div class="float-right strong">Medicare</div>
                            </div>
                        </div>
                    <% } %>
                <% } %>
                
                <% if (Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() && Model.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) { %>
                    <h3>Health Benefit Plan Coverage</h3>
                    <div class="column">
                        <div class="row">
                            <div class="float-left">Health Benefit Plan Coverage:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.name : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Insurance Type:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.insurance_type : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Reference Id Qualifier:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Address:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.address1 : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Phone Number:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.phone : string.Empty %></div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="row">
                            <div class="float-left">Health Benefit Plan Payer:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.payer : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Reference Id:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">Date:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.date.ToDateTimeString() : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">City:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.city : string.Empty %></div>
                            <div class="clear"></div>
                            <div class="float-left">State:</div>
                            <div class="float-right strong"><%= Model.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.state : string.Empty %></div>
                        </div>
                    </div>
                <% } %>
            </fieldset>
            <% if (Model.Episode != null && Model.Episode.name.IsNotNullOrEmpty()) { %>
            <fieldset>
                <legend>Last Home Care Episode</legend>
                <div class="column">
                    <div class="row">
                        <div class="float-left">Payer:</div>
                        <div class="float-right strong"><%= Model.Episode.name.IsNotNullOrEmpty() ? Model.Episode.name : string.Empty%></div>
                        <div class="clear"></div>
                        <div class="float-left">NPI#:</div>
                        <div class="float-right strong"><%= Model.Episode.reference_id.IsNotNullOrEmpty() ? Model.Episode.reference_id : string.Empty%></div>
                        <% if (Model.Other_Agency_Data != null) { %>
                        <div class="clear"></div>
                        <div class="float-left">Name:</div>
                        <div class="float-right strong"><%= Model.Other_Agency_Data.name.IsNotNullOrEmpty() ? Model.Other_Agency_Data.name : string.Empty%></div>
                        <div class="clear"></div>
                        <div class="float-left">Address:</div>
                        <div class="float-right strong"><%= Model.Other_Agency_Data.address1 + " " + Model.Other_Agency_Data.address2 + "<br />" + Model.Other_Agency_Data.city + ", " + Model.Other_Agency_Data.state + " " + Model.Other_Agency_Data.zip %></div>
                        <% } %>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <%--<div class="float-left">Provider Code:</div>
                        <div class="float-right strong"><%= Model.Episode.provider_code.IsNotNullOrEmpty() ? Model.Episode.provider_code : string.Empty%></div>
                        <div class="clear"></div>--%>
                        <div class="float-left">Episode Date Range:</div>
                        <div class="float-right strong"><%= Model.Episode.period_start.IsNotNullOrEmpty() ? Model.Episode.period_start.ToDateTimeString() : string.Empty %><%= Model.Episode.period_end.IsNotNullOrEmpty() ? " &#8211; " + Model.Episode.period_end.ToDateTimeString() : string.Empty %></div>
                    <% if (Model.Other_Agency_Data != null) { %>
                        <div class="clear"></div>
                        <div class="float-left">Phone:</div>
                        <div class="float-right strong"><%= Model.Other_Agency_Data.phone.IsNotNullOrEmpty() ? Model.Other_Agency_Data.phone.ToPhone() : string.Empty%></div>
                        <div class="clear"></div>
                        <div class="float-left">Fax:</div>
                        <div class="float-right strong"><%= Model.Other_Agency_Data.fax.IsNotNullOrEmpty() ? Model.Other_Agency_Data.fax.ToPhone() : string.Empty%></div>
                        <% } %>
                    </div>
                </div>
            </fieldset>
            <% } %>
            
        <% } else { %>
            <div class="wide-column">
                <div class="row align-center">The Medicare Eligibility Service is not available at this time.</div>
            </div>
        <% } %>
    <% } %>    
<% } else { %>
    <div class="wide-column">
        <div class="row align-center">The Medicare Eligibility Service is not available at this time.</div>
    </div>
<% } %>
    <form method="post" action="Patient/MedicareEligibilityPdf">
        <%= Html.Hidden("Eligibility.Request_Result.success", Model != null && Model.Request_Result != null && Model.Request_Result.success.IsNotNullOrEmpty() ? Model.Request_Result.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Result.response", Model != null && Model.Request_Result != null && Model.Request_Result.response.IsNotNullOrEmpty() ? Model.Request_Result.response : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.success", Model != null && Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() ? Model.Request_Validation.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.yes_no_response_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.yes_no_response_code.IsNotNullOrEmpty() ? Model.Request_Validation.yes_no_response_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.reject_reason_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? Model.Request_Validation.reject_reason_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.follow_up_action_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? Model.Request_Validation.follow_up_action_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Functional_Acknowledgment.success", Model != null && Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() ? Model.Functional_Acknowledgment.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_A.success", Model != null && Model.Medicare_Part_A != null && Model.Medicare_Part_A.success.IsNotNullOrEmpty() ? Model.Medicare_Part_A.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_A.date", Model != null && Model.Medicare_Part_A != null && Model.Medicare_Part_A.date.IsNotNullOrEmpty() ? Model.Medicare_Part_A.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_B.success", Model != null && Model.Medicare_Part_B != null && Model.Medicare_Part_B.success.IsNotNullOrEmpty() ? Model.Medicare_Part_B.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_B.date", Model != null && Model.Medicare_Part_B != null && Model.Medicare_Part_B.date.IsNotNullOrEmpty() ? Model.Medicare_Part_B.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.success", Model != null && Model.Subscriber != null && Model.Subscriber.success.IsNotNullOrEmpty() ? Model.Subscriber.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.date", Model != null && Model.Subscriber != null && Model.Subscriber.date.IsNotNullOrEmpty() ? Model.Subscriber.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.last_name", Model != null && Model.Subscriber != null && Model.Subscriber.last_name.IsNotNullOrEmpty() ? Model.Subscriber.last_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.first_name", Model != null && Model.Subscriber != null && Model.Subscriber.first_name.IsNotNullOrEmpty() ? Model.Subscriber.first_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.middle_name", Model != null && Model.Subscriber != null && Model.Subscriber.middle_name.IsNotNullOrEmpty() ? Model.Subscriber.middle_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.identification_code", Model != null && Model.Subscriber != null && Model.Subscriber.identification_code.IsNotNullOrEmpty() ? Model.Subscriber.identification_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.gender", Model != null && Model.Subscriber != null && Model.Subscriber.gender.IsNotNullOrEmpty() ? Model.Subscriber.gender : string.Empty)%>
        <%= Html.Hidden("Eligibility.Dependent.success", Model != null && Model.Dependent != null && Model.Dependent.success.IsNotNullOrEmpty() ? Model.Dependent.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.success", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.payer", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.payer : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.name", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.insurance_type", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.insurance_type : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.reference_id_qualifier", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.reference_id", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.date", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.address1", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.address1 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.city", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.city : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.state", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.state : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.zip", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.zip.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.zip : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.phone", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.phone : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.success", Model != null && Model.Episode != null && Model.Episode.success.IsNotNullOrEmpty() ? Model.Episode.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.payer", Model != null && Model.Episode != null && Model.Episode.payer.IsNotNullOrEmpty() ? Model.Episode.payer : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.name", Model != null && Model.Episode != null && Model.Episode.name.IsNotNullOrEmpty() ? Model.Episode.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.reference_id_qualifier", Model != null && Model.Episode != null && Model.Episode.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Episode.reference_id_qualifier : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.reference_id", Model != null && Model.Episode != null && Model.Episode.reference_id.IsNotNullOrEmpty() ? Model.Episode.reference_id : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.period_start", Model != null && Model.Episode != null && Model.Episode.period_start.IsNotNullOrEmpty() ? Model.Episode.period_start : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.period_end", Model != null && Model.Episode != null && Model.Episode.period_end.IsNotNullOrEmpty() ? Model.Episode.period_end : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.name", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.name.IsNotNullOrEmpty() ? Model.Other_Agency_Data.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.address1", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.address1.IsNotNullOrEmpty() ? Model.Other_Agency_Data.address1 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.address2", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.address2.IsNotNullOrEmpty() ? Model.Other_Agency_Data.address2 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.city", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.city.IsNotNullOrEmpty() ? Model.Other_Agency_Data.city : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.state", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.state.IsNotNullOrEmpty() ? Model.Other_Agency_Data.state : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.zip", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.zip.IsNotNullOrEmpty() ? Model.Other_Agency_Data.zip : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.phone", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.phone.IsNotNullOrEmpty() ? Model.Other_Agency_Data.phone : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.fax", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.fax.IsNotNullOrEmpty() ? Model.Other_Agency_Data.fax : string.Empty)%>
        <div class="buttons">
            <ul>
<% if (Model != null && Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() && Model.Request_Validation.success.IsEqual("no")) { %>
    <% if (Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() && Model.Functional_Acknowledgment.success.IsEqual("no")) { %>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Print</a></li>
    <% } %>
<% } %>
                <li><a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a></li>
            </ul>
        </div>
    </form>
</div>