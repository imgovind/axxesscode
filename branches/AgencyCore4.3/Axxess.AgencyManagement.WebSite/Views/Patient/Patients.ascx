﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<% Html.Telerik().Grid(Model.Patients).Name("PatientSelectionGrid").Columns(columns =>
   {
       columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" });
       columns.Bound(p => p.ShortName).Title("First Name").HtmlAttributes(new { @class = "searchF" });
       columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(p => p.PatientIdNumber).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select("AllSort", "Patient", new { branchId = Guid.Empty, statusId = Model.PatientListStatus, paymentSourceId = 0 }))
   .ClientEvents(events => events.OnDataBinding("Patient.OnPatientDataBinding").OnDataBound("Patient.PatientListDataBound").OnRowSelected("Patient.OnPatientRowSelected"))
   .Sortable()
   .Selectable()
   .Scrollable()
   .Footer(false)
   .Sortable(sr => sr.OrderBy(so => so.Add(s => s.LastName).Ascending()))
   .Render(); %>