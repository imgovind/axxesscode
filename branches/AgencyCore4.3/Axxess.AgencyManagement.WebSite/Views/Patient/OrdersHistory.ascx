﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Order History | <%= Model.DisplayName %></span>
<div class="wrapper">
<%= Html.Hidden("PatientOrdersHistory_PatientId", Model.Id, new { @id = "PatientOrdersHistory_PatientId" })%>
 <fieldset><div class="wide-column"><div class="row"><label class="float-left">Date Range:</label><div class="float-left"><%= Html.Telerik().DatePicker().Name("StartDate").Value(DateTime.Now.AddDays(-59)).HtmlAttributes(new { @id = "PatientOrdersHistory_StartDate", @class = "shortdate date" })%>To<%= Html.Telerik().DatePicker().Name("EndDate").Value(DateTime.Now).HtmlAttributes(new { @id = "PatientOrdersHistory_EndDate", @class = "shortdate date" })%></div><div class="float-left"> <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Patient.RebindOrdersHistory('<%=Model.Id %>');">Generate</a></li></ul></div></div></div><div class="buttons"><ul class="float-right"><li><%= Html.ActionLink("Excel Export", "PatientOrdersHistory", "Export", new { patientId = Model.Id ,StartDate=DateTime.Now.AddDays(-59), EndDate=DateTime.Now}, new { @id = "PatientOrdersHistory_ExportLink", @class = "excel" })%></li></ul></div></div></fieldset>
<%= Html.Telerik().Grid<Order>().Name("List_PatientOrdersHistory").DataKeys(keys => {
        keys.Add(o => o.Id).RouteKey("id");
    }).Columns(columns => {
        columns.Bound(o => o.Number).Title("Order Number").Width(110).Sortable(false).ReadOnly();
        columns.Bound(o => o.Text).Title("Type").Sortable(false).ReadOnly();
        columns.Bound(o => o.StatusName).Title("Status").Sortable(false).ReadOnly();
        columns.Bound(o => o.PhysicianName).Title("Physician").Width(150).Sortable(false).ReadOnly();
        columns.Bound(o => o.HasPhysicianAccess).Title("Electronic").Width(80).Sortable(true).ReadOnly();
        columns.Bound(o => o.OrderDate).Title("Order Date").Width(90).Sortable(true).ReadOnly();
        columns.Bound(o => o.SendDateFormatted).Title("Sent Date").Width(90).Sortable(false);
        columns.Bound(o => o.ReceivedDateFormatted).Title("Received Date").Width(100).Sortable(false);
        columns.Bound(o => o.PrintUrl).Title(" ").Width(100).Sortable(false);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryList", "Patient", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Pageable(paging => paging.PageSize(50)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
%>
</div>
<script type="text/javascript">
    $("#List_PatientOrdersHistory .t-grid-content").css({ 'height': 'auto', 'top': '26px','bottom':'' });
    $("#List_PatientOrdersHistory").css({ 'top': '80px' });
    $("#List_PatientOrdersHistory .t-toolbar.t-grid-toolbar").empty();
</script>

