﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<% using (Html.BeginForm("SignMedicationHistory", "Patient", FormMethod.Post, new { @id = "newMedicationProfileSnapShotForm" }))
   { %>
<%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<% = Html.Hidden("MedId",Model.MedicationProfile.Id) %>
<% = Html.Hidden("PatientId", Model.Patient.Id)%>
<% = Html.Hidden("EpisodeId", Model.EpisodeId)%>
<div class="visitContainer">
    <table class="nursing">
        <tr>
            <th class="telerik-back">
                <div class="third">
                    <span class="float-left strong">Patient Name:</span>
                    <span class="float-right normal"><%= Html.TextBox("Name", Model.Patient.FirstName+" "+ Model.Patient.LastName, new { @readonly = "readonly" } ) %></span>
                </div><div class="third">
                    <span class="float-left strong">Patient ID:</span>
                    <span class="float-right normal"><%= Html.TextBox("ID", Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber : string.Empty, new { @readonly = "readonly" } ) %></span>
                </div><div class="third">
                    <span class="float-left strong">Episode/Period:</span>
                    <span class="float-right normal"><%= Html.TextBox("Eps", string.Format(" {0} – {1}", Model != null && Model.StartDate != null ? Model.StartDate.ToShortDateString() : string.Empty, Model != null && Model.StartDate != null ? Model.EndDate.ToShortDateString() : string.Empty), new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float-left strong">Allergies:</span>
                    <span class="float-right normal"><%= Html.TextBox("Allergies", Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty, new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float-left strong">Pharmacy Name:</span>
                    <span class="float-right normal"><%= Html.TextBox("PharmacyName", Model.Patient != null ? Model.Patient.PharmacyName : string.Empty, new { @id = "MedicationProfile_PharmacyName", @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float-left strong">Pharmacy Phone:</span>
                    <span class="float-right normal"><%= Html.TextBox("PharmacyPhone", Model.Patient != null ? Model.Patient.PharmacyPhone : string.Empty, new { @id = "MedicationProfile_PharmacyPhone", @readonly = "readonly" })%></span>
                </div>
            </th>
        </tr><tr>
            <th class="telerik-back">
                <div class="newRow one medication align-left">
                    <%= Html.Telerik().Grid<Medication>().Name("MedicatonProfileGrid").DataKeys(keys => { keys.Add(M => M.Id); })
                        .DataBinding(dataBinding => { dataBinding.Ajax().Select("MedicationSnapshot", "Patient", new { MedId = Model.MedicationProfile.Id }); })
                        .Columns(columns => { 
                            columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
                            columns.Bound(M => M.StartDate).Width(100);
                            columns.Bound(M => M.MedicationDosage).Title("Medication &#38; Dosage");
                            columns.Bound(M => M.Frequency).Title("Frequency").Width(120);
                            columns.Bound(M => M.Route).Title("Route").Width(120);
                            columns.Bound(M => M.MedicationType).Width(65).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
                            columns.Bound(M => M.Classification).Width(180);
                    }).ClientEvents(e => e.OnRowDataBound("Patient.OnMedicationProfileSnapShotRowDataBound")).Scrollable().Sortable().Footer(false)   %>
                </div>   
            </th>
        </tr><tr>
            <th class="telerik-back">
                <p class="align-left normal"><strong>Drug Regimen Review Acknowledgment</strong>: I have reviewed all the listed medications for potential adverse effects, drug reactions, including ineffective drug therapy, significant side effects, significant drug interactions, duplicate drug therapy, and noncompliance with drug therapy.</p>
                <div class="third">
                    <label for="Signature" class="float-left">Clinician Signature:</label>
                    <div class="float-right"><%= Html.Password("Signature", "", new { @id = "MedicationProfile_ClinicianSignature", @class="required" }) %></div>
                </div><div class="third"></div><div class="third">
                    <label for="SignedDate" class="float-left">Date:</label>
                    <div class="float-right"><input type="text" class="date-picker required" name="SignedDate" value="<%= DateTime.Today.ToString("MM/dd/yyy") %>" id="MedicationProfile_ClinicianSignatureDate" /></div>
                </div>
             </th>
        </tr>
    </table>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Sign</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('medicationprofilesnapshot');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>

 