﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<div id="MedProfile_activeMeds" class="medprofile">
    <ul>
        <li class="align-center"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="longstanding">LS</span>
            <span class="startdate">Start Date</span>
            <span class="medicationdosage">Medication & Dosage</span>
            <span class="frequency">Frequency</span>
            <span class="route">Route</span>
            <span class="type">Type</span>
            <span class="classification">Classification</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable);
        if (medications != null && medications.Count() > 0) {
            foreach (var med in medications) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                    <span class="longstanding"><input name="LongStanding" class="radio" disabled="true" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
                    <span class="startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                    <span class="medicationdosage"><%= med.MedicationDosage %></span>
                    <span class="frequency"><%= med.Frequency %></span>
                    <span class="route"><%= med.Route %></span>
                    <span class="type"><%= med.MedicationType.Text %></span>
                    <span class="classification"><%= med.Classification %></span>
                    <span class="action"><% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %><a href="javascript:void(0);" onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>');" >Delete</a> | <a href="javascript:void(0);" onclick="Medication.Discontinue('<%= Model.Id %>','<%= med.Id %>');">Discontinue</a><% } %></span>
            </li><%
                i++;
            }
        } else { %>
                <li class="align-center"><span class="darkred">No Active Medications</span></li>
        <% } } %>
    </ol>
</div>
<div id="MedProfile_dischargeMeds" class="medprofile">
    <ul>
        <li class="align-center"><h3>Discontinued Medication(s)</h3></li>
        <li>
            <span class="longstanding">LS</span>
            <span class="startdate">Start Date</span>
            <span class="dcmedicationdosage">Medication & Dosage</span>
            <span class="dcfrequency">Frequency</span>
            <span class="dcroute">Route</span>
            <span class="type">Type</span>
            <span class="dcclassification">Classification</span>
            <span class="dischargedate">D/C Date</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int j = 1;
        var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable);
        if (medications != null && medications.Count() > 0) {
            foreach (var med in medications) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (j % 2 != 0 ? "odd" : "even")) %>
                    <span class="longstanding"><input name="LongStanding" class="radio" disabled="true" type="checkbox" value="<%= med.Id %>" id="DcLongStanding<%= j %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
                    <span class="startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                    <span class="dcmedicationdosage"><%= med.MedicationDosage %></span>
                    <span class="dcfrequency"><%= med.Frequency %></span>
                    <span class="dcroute"><%= med.Route %></span>
                    <span class="type"><%= med.MedicationType.Text %></span>
                    <span class="dcclassification"><%= med.Classification %></span>
                    <span class="dischargedate"><%= med.DCDateFormated %></span>
                    <span class="action"><% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %><a href="javascript:void(0);" onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>');" >Delete</a> | <a href="javascript:void(0);" onclick="Medication.Activate('<%= Model.Id %>','<%= med.Id %>');">Activate</a><% } %></span>
            </li><%
                j++;
            }
        } else { %>
                <li class="align-center"><span class="darkred">No D/C Medications</span></li>
        <% } } %>
    </ol>
</div>