﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<%  var sortName = ViewData["SortColumn"]!=null? ViewData["SortColumn"].ToString():string.Empty; %>
<%  var sortDirection = ViewData["SortDirection"]!=null?ViewData["SortDirection"].ToString():string.Empty; %>
<%  var branchId = ViewData["BranchId"] != null ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<%  var status = ViewData["Status"] != null ? ViewData["Status"].ToString() : "1"; %>
<% var pageName = "PatientList";  %>
<%= Html.Telerik().Grid(Model).Name(pageName+"_Grid").HtmlAttributes(new { @style = "top:0px;" }).Columns(columns => {
        columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
        columns.Bound(p => p.DisplayName).Title("Patient").Width(180);
        columns.Bound(p => p.InsuranceName).Title("Insurance").Width(105).Sortable(false);
        columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(105).Sortable(false);
        columns.Bound(p => p.Address).Title("Address").Sortable(false);
        columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(85).Sortable(true);
        columns.Bound(p => p.Gender).Width(60).Sortable(true);
        columns.Bound(p => p.Phone).Title("Phone").Width(105).Sortable(false);
        columns.Bound(p => p.Status).Title("Status").Width(80).Sortable(false);
        columns.Bound(p => p.Id).Width(90).Sortable(false).Template(s => string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('{0}');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.Delete('{0}');\" class=\"deletePatient\">Delete</a>", s.Id)).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.Delete('<#=Id#>');\" class=\"deletePatient\">Delete</a>").Title("Action").Visible(!Current.IsAgencyFrozen);
    }).ClientEvents(events => events.OnDataBound("Patient.BindGridButton")).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            if (sortName == "PatientIdNumber") {
                if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
            } else if (sortName == "DisplayName") {
                if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
            } else if (sortName == "DateOfBirth") {
                if (sortDirection == "ASC") order.Add(o => o.DateOfBirth).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DateOfBirth).Descending();
            } else if (sortName == "Gender") {
                if (sortDirection == "ASC") order.Add(o => o.Gender).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.Gender).Descending();
            }
        })
    ).Scrollable(scrolling => scrolling.Enabled(true)) %>
<script type="text/javascript">
    $("#<%= pageName %>_Grid .t-grid-content").css({ height: "auto", position: "absolute", top: "25px" });
    $("#<%= pageName %>_Grid .t-grid-header .t-link, #<%= pageName %>_Grid .t-grid-footer .t-refresh").each(function() {
        if ($(this).attr("href")) {
            var link = $(this).attr("href");
            $(this).attr("href", "javascript:void(0)").click(function() { Patient.LoadPatientListContent(Patient.PatientListInput(U.ParameterByName(link, "<%= pageName %>_Grid-orderBy"))); })
        }
    });
    $("#<%= pageName %>_Search").append($("<div/>").addClass("no-left-margin").GridSearchById("#<%= pageName %>_Grid"));
</script>