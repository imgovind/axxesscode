﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<AllergyProfileViewData>" %><%
var allergies = Model != null && Model.AllergyProfile != null && Model.AllergyProfile.Allergies.IsNotNullOrEmpty() ? Model.AllergyProfile.Allergies.ToObject<List<Allergy>>().ToList() : new List<Allergy>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + " | " : "" %>Allergy Profile<%= Model != null && Model.Patient != null ? (" | " + (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.Clean().ToTitleCase() : "") + ", " + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.Clean().ToTitleCase() : "") + " " + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.Clean().ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/allergyprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") : "") : "").Clean() %>",
            "patientname": "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "mr": "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>"
        };
        PdfPrint.BuildSections([
<%
bool first = true;
if (allergies.Count > 0) foreach (var allergy in allergies) if (allergy != null && !allergy.IsDeprecated) {
    if (first) first = false;
    else { %> , <% } %>
            {
                Content: [
                    [
                        "<p><%= allergy.Name.IsNotNullOrEmpty() ? allergy.Name.Clean() : string.Empty %></p>",
                        "<p><%= allergy.Type.IsNotNullOrEmpty() ? allergy.Type.Clean() : string.Empty %></p>"
                    ]
                ]
            } <%
} %>
<% if (first) { %>{Content:[["",""]]}<% } %>
        ]);
    <% }).Render(); %>
</body>
</html>