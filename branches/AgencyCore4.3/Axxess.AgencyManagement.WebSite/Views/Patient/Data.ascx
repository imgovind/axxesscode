﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientScheduleEventViewData>" %>
<%  if (Model!=null && Model.Patient != null) { %>
    <div class="top">
        <%  Html.RenderPartial("/Views/Patient/Info.ascx", Model.Patient); %>
    </div>
    <div class="bottom">
        <%  Html.RenderPartial("/Views/Patient/Activities.ascx", Model); %>
    </div>
    <%  Html.RenderPartial("/Views/Patient/Popup.ascx", Model.Patient); %>
    <%= Html.Hidden("PatientCenter_PatientId", Model.Patient.Id) %>
   
<%  } else { %>
    <div class="abs center">No Patient Data Found</div>
<%  } %>
