﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientScheduleEventViewData>" %>
<div class="abs above activityFilterInputContainer">
    <input type="hidden" class="activityFilterInput" name="PatientId" value="<%=Model.Patient.Id %>" id="patient-activity-patientId" />
    <div class="float-left">
        <label>Show</label>
         <% var htmlAttributes = new Dictionary<string, string>(); 
             htmlAttributes.Add("id", "patient-activity-drop-down");
          htmlAttributes.Add("class", "patient-activity-drop-down activityFilterInput");%>
         <%= Html.PatientSchedulesDisciplines("Discipline", Model.DisciplineFilterType, htmlAttributes)%>
        <label>Date</label>
         <% var htmlAttributesDate = new Dictionary<string, string>();
             htmlAttributesDate.Add("id", "patient-activity-date-drop-down");
             htmlAttributesDate.Add("class", "patient-activity-date-drop-down activityFilterInput");%>
         <%= Html.PatientSchedulesDateRangeFilter("DateRangeId", Model.DateFilterType, htmlAttributesDate)%>
    </div>
    <div id="patient-center-activity-filter" class="float-left">
        <%= string.Format("<div id='date-range-text' class='loat-right {0}'>{1} </div>",Model.DateFilterType.IsEqual("All")|| Model.DateFilterType.IsEqual("DateRange")?"hidden":string.Empty,Model.Range) %>
        <div class="custom-date-range <%= Model.DateFilterType.IsEqual("DateRange")?string.Empty: "hidden" %>">
            <div class="buttons float-right" style="margin-top:-3px">
                <ul>
                    <li><a onclick="Patient.RebindScheduleActivity(false);return false">Search</a></li>
                </ul>
            </div>
            <label>From</label>
            <input type="text" class="date-picker activityFilterInput" name="RangeStartDate" value="<%=DateTime.Now.ToString("MM/dd/yyyy") %>" id="patient-activity-from-date" />
            <label>To</label>
            <input type="text" class="date-picker activityFilterInput" name="RangeEndDate" value="<%=DateTime.Now.AddDays(60).ToString("MM/dd/yyyy") %>" id="patient-activity-to-date" />
        </div>
    </div>
</div>
<% var val = Model != null && Model.Patient!=null  ? Model.Patient.Id : Guid.Empty;
   Html.Telerik().Grid(Model.ScheduleEvents).Name("PatientActivityGrid").Columns(columns =>
   {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).Template(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDateSortable).Template(s => s.EventDateSortable).ClientTemplate("<#=EventDateSortable#>").Title("Scheduled Date").Width(105);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.OasisProfileUrl).Template(s => s.OasisProfileUrl).ClientTemplate("<#=OasisProfileUrl#>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.StatusComment).Template(s => s.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", s.StatusComment) : string.Empty).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.Comments).Template(s => s.Comments.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.Comments) : string.Empty).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"></a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.EpisodeNotes).Template(s => s.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"{0}\"></a>", s.EpisodeNotes) : string.Empty).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.PrintUrl).Template(s => s.PrintUrl).ClientTemplate("<#=PrintUrl#>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.AttachmentUrl).Template(s => s.AttachmentUrl).ClientTemplate("<#=AttachmentUrl#>").Title(" ").Width(30).Sortable(false);
       columns.Bound(s => s.ActionUrl).Template(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200).Sortable(false);
       columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(s => s.IsComplete).Visible(false);
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select("ActivitySort", "Patient", new { PatientId = val, Discipline = Model.DisciplineFilterType, DateRangeId = Model.DateFilterType, RangeStartDate = Model.FilterStartDate, RangeEndDate = Model.FilterEndDate }))
   .ClientEvents(c => c.OnDataBinding("Patient.ActivityOnDataBinding").OnDataBound("Patient.ActivityOnBound"))
   .RowAction(row =>
   {
       if (row.DataItem.IsComplete)
       {
           if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed"))
           {
               row.HtmlAttributes["class"] += " completed";
           }
           else
           {
               row.HtmlAttributes.Add("class", "completed");
           }
       }
       if (row.DataItem.IsOrphaned)
       {

           if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned"))
           {
               row.HtmlAttributes["class"] += " orphaned";
           }
           else
           {
               row.HtmlAttributes.Add("class", "orphaned");
           }
       }
   })
   .Sortable(sr => sr.OrderBy(so => so.Add(s => s.EventDateSortable).Descending()))
   .Scrollable()
   .Footer(false)
   .Render();
%>
<script type="text/javascript">
    var currentDateFilter = Patient._dateFilter != "" ? Patient._dateFilter : <% if (Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) || Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) || Current.IfOnlyRole(AgencyRoles.Auditor)) { %> "All" <% } else { %> "ThisEpisode" <% } %>;
    $("#patient-activity-from-date , #patient-activity-to-date").DatePicker();
    $("select.patient-activity-drop-down").change(function() {Patient.RebindScheduleActivity(false);});
    $("select.patient-activity-date-drop-down").change(function() {Patient.RebindScheduleActivity(true);   });
    Patient.SetId("<%= val %>");
    U.ServerBindScheduleActivityInit('PatientActivityGrid');
</script>