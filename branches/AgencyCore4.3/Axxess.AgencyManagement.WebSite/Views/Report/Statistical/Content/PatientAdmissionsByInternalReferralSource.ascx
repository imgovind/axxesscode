﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientAdmission>>" %>
<% string pagename = "StatisticalPatientAdmissionsByInternalReferral"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.DisplayName).Title("Patient").Sortable(false);
               columns.Bound(s => s.PatientId).Title("MRN").Sortable(false);
               columns.Bound(s => s.PhoneHome).Title("Phone").Width(150);
               columns.Bound(s => s.Admit).Title("Admit").Width(50);
               columns.Bound(s => s.StartOfCareDateFormatted).Title("SOC").Width(100);
               columns.Bound(s => s.DischargedDateFormatted).Title("D/C").Width(100);
               columns.Bound(s => s.InsuranceName).Title("Insurance");
               columns.Bound(s => s.PhysicianName).Title("Primary Physician");
           }).Groupable(groups => groups.Groups(group => group.Add(g => g.InternalReferralName)))
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { UserId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                                       .Sortable(sorting =>
                                                 sorting.SortMode(GridSortMode.SingleColumn)
                                                     .OrderBy(order =>
                                                     {
                                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                         if (sortName == "DisplayName")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "PatientIdNumber")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "InsuranceName")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.InsuranceName).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.InsuranceName).Descending();
                                                             }
                                                         }
                                                     })
                                             )
                                   .Scrollable()
                                           .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','StatisticalPatientAdmissionsByInternalReferral',{  BranchId : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto', 'top': '30px' });
    $('#<%= pagename %>Grid .t-grouping-header').remove();
    $('#<%= pagename %>Grid .t-grouping-row p').each(function(index, element) {
        element.lastChild.deleteData(0, 24);
    });
</script>