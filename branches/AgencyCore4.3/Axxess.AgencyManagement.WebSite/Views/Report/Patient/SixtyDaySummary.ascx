﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientSixtyDaySummary"; %>
<div class="wrapper">
    <fieldset>
        <legend> Sixty Day Summary </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid", @tabindex = "3" })%></div> </div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Patient:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_PatientId", @class = "report_input valid", @tabindex = "1" })%></div> </div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="column">
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','SixtyDaySummaryContent',{{ PatientId: $('#{0}_PatientId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSixtyDaySummary", new { PatientId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Patient/Content/SixtyDaySummaryContent", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>GridContainer").css({ 'top': '160px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
