﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByPhysicians"; %>

         <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Sortable(false).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Sortable(false).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Sortable(false).Title("State");
                columns.Bound(r => r.PatientAddressZipCode).Sortable(false).Title("Zip Code");
                columns.Bound(r => r.PatientPhone).Sortable(false).Title("Home Phone");
                columns.Bound(r => r.PatientGender).Sortable(false).Title("Gender");
            })
                     //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { AgencyPhysicianId = Guid.Empty }))
                        .Sortable(sorting =>
                                                                 sorting.SortMode(GridSortMode.SingleColumn)
                                                                     .OrderBy(order =>
                                                                     {
                                                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                         if (sortName == "PatientDisplayName")
                                                                         {
                                                                             if (sortDirection == "ASC")
                                                                             {
                                                                                 order.Add(o => o.PatientDisplayName).Ascending();
                                                                             }
                                                                             else if (sortDirection == "DESC")
                                                                             {
                                                                                 order.Add(o => o.PatientDisplayName).Descending();
                                                                             }

                                                                         }
                                                                        
                                                                     })
                                                                     )
                    .Scrollable().Footer(false)
        %>
  
<script type="text/javascript">
    var physicianId = $('#<%= pagename %>_PhysicianId').next().val() != "" ? $('#<%= pagename %>_PhysicianId').next().val() : '00000000-0000-0000-0000-000000000000';
    var statusId = $('#<%= pagename %>_StatusId').val();
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PhysicianContent',{  PhysicianId : \"" + physicianId + "\",StatusId : \"" + statusId + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });

    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>