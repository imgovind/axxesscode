﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VisitNoteViewData>>" %>
<% string pagename = "PatientSixtyDaySummary"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(v => v.UserDisplayName).Title("Employee Name");
               columns.Bound(v => v.VisitDate).Title("Visit Date");
               columns.Bound(v => v.SignatureDate).Title("Signature Date");
               columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false);
               columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name");
           })
          // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty}))
                              .Sortable(sorting =>
                                                         sorting.SortMode(GridSortMode.SingleColumn)
                                                             .OrderBy(order =>
                                                             {
                                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                 if (sortName == "UserDisplayName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.UserDisplayName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.UserDisplayName).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "VisitDate")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.VisitDate).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.VisitDate).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "SignatureDate")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.SignatureDate).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.SignatureDate).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "PhysicianDisplayName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.PhysicianDisplayName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.PhysicianDisplayName).Descending();
                                                                     }
                                                                 }

                                                             }))
                           .Scrollable()
                                   .Footer(false)
        %>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','SixtyDaySummaryContent',{  PatientId : \"" + $('#<%= pagename %>_PatientId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
