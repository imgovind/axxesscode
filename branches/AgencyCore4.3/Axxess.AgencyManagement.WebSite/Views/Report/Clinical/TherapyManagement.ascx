﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Therapy Management Report</legend>
        <div class="column">
            <div class="row"><label for="TherapyManagement_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "TherapyManagement_BranchId" })%></div></div>
            <div class="row">
                <label for="TherapyManagement_DateRange" class="float-left">Date Range:</label>
                <div class="float-right">
                    <input id="TherapyManagement_StartDate" class="date-picker oe" value="<%= DateTime.Today.AddDays(-59).ToZeroFilled() %>"/>
                    <input id="TherapyManagement_EndDate" class="date-picker oe" value="<%= DateTime.Today.ToZeroFilled() %>"/>
                </div>
            </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReportWithRange('/Request/TherapyManagementReport', '#TherapyManagement_BranchId', '#TherapyManagement_StartDate', '#TherapyManagement_EndDate');">Request Report</a></li></ul></div>
    </fieldset>
</div>
