﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ManagedClaimsByStatus"; %>
<div class="wrapper">
    <div class="wide-column"><em>Note:The date range calculated based on the end date of the claim.</em></div>
    <fieldset>
        <legend>Managed Care Claims History</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Bill Status:</label><div class="float-right"><%= Html.ManagedBillStatus("Status", ((int)ManagedClaimStatus.ClaimCreated).ToString(), false, new { @id = pagename + "_Status" })%></div></div> 
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ManagedClaimsByStatusContent',{{ BranchCode: $('#{0}_BranchCode').val(),Status: $('#{0}_Status').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportManagedClaimsByStatus", new { BranchCode = Guid.Empty, Status = 300, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/ManagedClaimByStatus", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
</script>
