﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "UnbilledManagedClaims"; %>
<div class="wrapper">
    <fieldset>
        <legend>Unbilled Managed Care Claims</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','UnbilledManagedClaimsContent',{{ BranchCode: $('#{0}_BranchCode').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportUnbilledManagedClaims", new { BranchCode = Guid.Empty, Status = 300, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/UnbilledManagedClaim", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
</script>
