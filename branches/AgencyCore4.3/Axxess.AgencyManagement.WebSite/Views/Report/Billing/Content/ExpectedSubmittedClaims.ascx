﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ExpectedSubmittedClaims"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode").Width(150);
               columns.Bound(p => p.Type).Sortable(false).Width(60);
               columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
               columns.Bound(p => p.ClaimAmount).Sortable(false).Format("${0:0.00}").Title("Claim Amount").Width(95);
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, type = "All",  StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                                      .Sortable(sorting =>
                                                                  sorting.SortMode(GridSortMode.SingleColumn)
                                                                      .OrderBy(order =>
                                                                      {
                                                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                          if (sortName == "PatientIdNumber")
                                                                          {
                                                                              if (sortDirection == "ASC")
                                                                              {
                                                                                  order.Add(o => o.PatientIdNumber).Ascending();
                                                                              }
                                                                              else if (sortDirection == "DESC")
                                                                              {
                                                                                  order.Add(o => o.PatientIdNumber).Descending();
                                                                              }
                                                                          }
                                                                          else if (sortName == "DisplayName")
                                                                          {
                                                                              if (sortDirection == "ASC")
                                                                              {
                                                                                  order.Add(o => o.DisplayName).Ascending();
                                                                              }
                                                                              else if (sortDirection == "DESC")
                                                                              {
                                                                                  order.Add(o => o.DisplayName).Descending();
                                                                              }
                                                                          }
                                                                          else if (sortName == "Type")
                                                                          {
                                                                              if (sortDirection == "ASC")
                                                                              {
                                                                                  order.Add(o => o.Type).Ascending();
                                                                              }
                                                                              else if (sortDirection == "DESC")
                                                                              {
                                                                                  order.Add(o => o.Type).Descending();
                                                                              }
                                                                          }

                                                                          else if (sortName == "PaymentDate")
                                                                          {
                                                                              if (sortDirection == "ASC")
                                                                              {
                                                                                  order.Add(o => o.PaymentDate).Ascending();
                                                                              }
                                                                              else if (sortDirection == "DESC")
                                                                              {
                                                                                  order.Add(o => o.PaymentDate).Descending();
                                                                              }
                                                                          }
                                                                          else if (sortName == "ClaimDate")
                                                                          {
                                                                              if (sortDirection == "ASC")
                                                                              {
                                                                                  order.Add(o => o.ClaimDate).Ascending();
                                                                              }
                                                                              else if (sortDirection == "DESC")
                                                                              {
                                                                                  order.Add(o => o.ClaimDate).Descending();
                                                                              }
                                                                          }

                                                                      })
                                                              )
                                   .Scrollable()
                                           .Footer(false)
        %>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ExpectedSubmittedClaimsContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", Type : \"" + $('#<%= pagename %>_Type').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>

