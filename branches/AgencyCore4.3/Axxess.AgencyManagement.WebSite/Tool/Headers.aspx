﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Headers</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
        <% for(int x = 0; x < HttpContext.Current.Request.ServerVariables.Count; x++) { %>
            <%= string.Format("Variable: {0} Value: {1} <br />", HttpContext.Current.Request.ServerVariables.GetKey(x), HttpContext.Current.Request.ServerVariables.Get(x)) %>
        <% } %>
    <% } %>
</asp:Content>
    