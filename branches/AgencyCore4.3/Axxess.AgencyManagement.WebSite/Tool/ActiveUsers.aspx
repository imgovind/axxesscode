﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Active Users Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
<% if (!Current.IsIpAddressRestricted) { %>
    <script runat="server">
        private static readonly Axxess.Api.AuthenticationAgent authenticationAgent = new Axxess.Api.AuthenticationAgent();
        public void KickButton_Click(Object sender, EventArgs e)
        {
            if (txtEmailAddress.Text.IsNotNullOrEmpty())
            {
                authenticationAgent.Logout(txtEmailAddress.Text.Trim());
            }
        }
    </script>
    <%= string.Format("AXDALCEWEB1001 (10.0.5.41) with {0} users", Current.ActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1001").Count())%><br />
    <%= string.Format("AXDALCEWEB1002 (10.0.5.42) with {0} users", Current.ActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1002").Count())%><br />
    <%= string.Format("AXDALCEWEB1003 (10.0.5.43) with {0} users", Current.ActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1003").Count())%><br /><br />
    <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
    <asp:Button ID="btnSubmit" runat="server" Text="Kick" OnClick="KickButton_Click" />
    <br /><br />
    <table id="newspaper-a">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Email</th>
                    <th style="width: 15%; text-align: left;">Name</th>
                    <th style="width: 20%; text-align: left;">Agency</th>
                    <th style="width: 10%; text-align: left;">IP Address</th>
                    <th style="width: 10%; text-align: left;">Server</th>
                    <th style="width: 10%; text-align: left;">Session Id</th>
                    <th style="width: 15%; text-align: left;">Last Activity</th>
                </tr>
            </thead>
            <tbody>
            <% var activeUsers = Current.ActiveUsers; %>
            <% if (activeUsers != null && activeUsers.Count > 0) {
                activeUsers.ForEach(user => { %>
                    <tr><td><%= user.EmailAddress %></td><td><%= user.FullName%></td><td><%= user.AgencyName %></td><td><%= user.IpAddress %></td><td><%= user.ServerName %></td><td><%= user.SessionId %></td><td><%= user.LastSecureActivity.ToString("MM/dd/yyyy @ hh:mm:ss:tt") %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There are no active users in the system.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= activeUsers != null ? activeUsers.Count : 0 %></em></td></tr>
            </tfoot>
        </table>
    <table id="newspaper-b">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Email</th>
                    <th style="width: 15%; text-align: left;">Name</th>
                    <th style="width: 20%; text-align: left;">Agency</th>
                    <th style="width: 10%; text-align: left;">IP Address</th>
                    <th style="width: 10%; text-align: left;">Server</th>
                    <th style="width: 10%; text-align: left;">Session Id</th>
                    <th style="width: 15%; text-align: left;">Last Activity</th>
                </tr>
            </thead>
            <tbody>
            <% var inActiveUsers = Current.InActiveUsers; %>
            <% if (inActiveUsers != null && inActiveUsers.Count > 0) {
                inActiveUsers.ForEach(user => { %>
                    <tr><td><%= user.EmailAddress %></td><td><%= user.FullName%></td><td><%= user.AgencyName %></td><td><%= user.IpAddress %></td><td><%= user.ServerName %></td><td><%= user.SessionId %></td><td><%= user.LastSecureActivity.ToString("MM/dd/yyyy @ hh:mm:ss:tt") %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There are no inactive users in the system.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= inActiveUsers != null ? inActiveUsers.Count : 0 %></em></td></tr>
            </tfoot>
        </table>
<% } %>
</asp:Content>