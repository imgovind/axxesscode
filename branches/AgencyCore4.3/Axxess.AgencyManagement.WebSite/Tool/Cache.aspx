﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<script runat="server" type="text/C#">
    public void RemoveButton_Click(Object sender, EventArgs e)
    {
        if (txtCacheKey.Text.IsNotNullOrEmpty())
        {
            Cacher.Remove(txtCacheKey.Text.Trim());
            lblConfirmation.Text = string.Format("{0} has been removed", txtCacheKey.Text);
            lblConfirmation.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            lblConfirmation.Text = "Please enter a cache key";
            lblConfirmation.ForeColor = System.Drawing.Color.Red;
        }
    }
</script>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Cache</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
        <asp:TextBox ID="txtCacheKey" runat="server"></asp:TextBox>
        <asp:Button ID="btnRemove" runat="server" Text="Remove from Cache" OnClick="RemoveButton_Click" />
        <br /><br />
        <asp:Label ID="lblConfirmation" runat="server"></asp:Label>
        <table id="newspaper-a">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Key</th>
                </tr>
            </thead>
            <tbody>
            <% var keys = Cacher.GetKeys(); %>
            <% if (keys != null && keys.Count > 0)
               {
                keys.ForEach(key => { %>
                    <tr><td><%= key %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There is nothing cached in Membase Cacher.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= keys != null ? keys.Count : 0%></em></td></tr>
            </tfoot>
        </table>
        <table id="newspaper-b">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Key</th>
                </tr>
            </thead>
            <tbody>
            <% var inProcKeys = InProcCacher.GetKeys(); %>
            <% if (inProcKeys != null && inProcKeys.Count > 0)
               {
                inProcKeys.ForEach(key => { %>
                    <tr><td><%= key %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There is nothing cached in InProc Cacher.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= inProcKeys != null ? inProcKeys.Count : 0%></em></td></tr>
            </tfoot>
        </table>
    <% } %>
</asp:Content>
    