﻿var IncidentReport = {
    Delete: function(patientId, episodeId, id) { U.DeleteTemplateWithInput("IncidentReport", { PatientId: patientId, EpisodeId: episodeId, Id: id }, function() { UserInterface.RefreshActivity(patientId, episodeId); }); },
    InitEdit: function() {
        var form = $("#editIncidentReportForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("editincidentreport"); UserInterface.RefreshCenterQAMySchedule(result); });
        $("#window_editincidentreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        var form = $("#newIncidentReportForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("newincidentreport"); UserInterface.RefreshCenterQAMySchedule(result); });
        $("#window_newincidentreport .Physicians").PhysicianInput();
        U.ShowIfOtherChecked($("#New_Incident_IndividualInvolved4"), $("#New_Incident_IndividualInvolvedOther"));
    },
    RebindList: function() { U.RebindTGrid($('#List_IncidentReport')); },
    ProcessIncident: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        UserInterface.CloseModal();
                        UserInterface.RefreshCenterQAMySchedule(result);
                        //                        if (result.IsIncidentAccidentRefresh) {
                        //                            IncidentReport.RebindList();
                        //                        }
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    UserInterface.CloseModal();
                    UserInterface.RefreshCenterQAMySchedule(result);
                    //                    if (result.IsIncidentAccidentRefresh) {
                    //                        IncidentReport.RebindList();
                    //                    }
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }

}

var InfectionReport = {
    Delete: function(patientId, episodeId, id) { U.DeleteTemplateWithInput("InfectionReport", { PatientId: patientId, EpisodeId: episodeId, Id: id }, function() { UserInterface.RefreshActivity(patientId, episodeId);  });},
    InitEdit: function() {
        var form = $("#editInfectionReportForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("editinfectionreport"); UserInterface.RefreshCenterQAMySchedule(result); });
        $("#window_editinfectionreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        var form = $("#newInfectionReportForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("newinfectionreport"); UserInterface.RefreshCenterQAMySchedule(result); });
        $("#window_newinfectionreport .Physicians").PhysicianInput();
        U.ShowIfOtherChecked($("#New_Infection_InfectionType6"), $("#New_Infection_InfectionTypeOther"));
    },
    RebindList: function() { U.RebindTGrid($('#List_InfectionReport')); },
    ProcessInfection: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        UserInterface.CloseModal();
                        UserInterface.RefreshCenterQAMySchedule(result);
                        //                    if (result.IsInfectionRefresh) {
                        //                        InfectionReport.RebindList();
                        //                    }

                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    UserInterface.CloseModal();
                    UserInterface.RefreshCenterQAMySchedule(result);
                    //                    if (result.IsInfectionRefresh) {
                    //                        InfectionReport.RebindList();
                    //                    }

                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}