var UserInterface = {
    ShowNoteModal: function(Text, Color) {
        if (Color != "red" && Color != "blue") Color = "yellow";
        $("body").append(unescape("%3Cdiv id=%22shade%22%3E%3C/div%3E%3Cdiv class=%22note_modal bottom " + Color + "%22%3E%3C/div%3E%3Cdiv class=%22not" +
            "e_modal middle " + Color + "%22%3E%3C/div%3E%3Cdiv class=%22note_modal top " + Color + "%22%3E%3Cdiv id=%22note_close%22%3EX%3C/div%3E%3Cp" +
            "%3E" + Text + "%3C/p%3E%3C/div%3E"));
        $("#note_close").click(function() { $("#shade").remove(); $(".note_modal").remove() });
    },
    ShowPatientChart: function(Id, patientStatus) {
        if (Id != undefined && U.IsGuid(Id)) {
            if (Acore.Windows.patientcenter.IsOpen) {
                var selectedTd = $('td:contains(' + Id + ')', $('#PatientSelectionGrid'));
                if (selectedTd != undefined && selectedTd.length > 0) { selectedTd.closest('tr').click(); }
                else {
                    U.PostUrl("/Patient/Get", { id: Id }, function(result) {
                        if (result.Status == 1 || result.Status == 2) {
                            var input = { BranchId: "00000000-0000-0000-0000-000000000000", StatusId: result.Status, PaymentSourceId: 0 };
                            Patient._patientId = Id;
                            Patient.RebindPatientsSelection(input);
                            $("#PatientCenter_BranchId").val("00000000-0000-0000-0000-000000000000");
                            $("#PatientCenter_StatusId").val(result.Status);
                            $("#PatientCenter_PaymentSourceId").val(0);
                        }
                    });
                }
                $("#window_patientcenter").WinFocus();
            } else { Acore.Open("patientcenter", "Patient/Center/1/" + Id, function() { Patient.InitCenter(); }); }
        }
    },
    ShowScheduleCenter: function(Id, patientStatus) {
        if (Id != undefined && U.IsGuid(Id)) {
            if (Acore.Windows.schedulecenter.IsOpen) {
                var selectedTd = $('td:contains(' + Id + ')', $('#ScheduleSelectionGrid'));
                if (selectedTd != undefined && selectedTd.length > 0) { selectedTd.closest('tr').click(); }
                else {
                    U.PostUrl("/Patient/Get", { id: Id }, function(result) {
                        if (result != null && (result.Status == 1 || result.Status == 2)) {
                            var input = { BranchId: "00000000-0000-0000-0000-000000000000", StatusId: result.Status, PaymentSourceId: 0 };
                            Schedule._patientId = Id;
                            Schedule.RebindPatientsSelection(input);
                            $("#ScheduleCenter_BranchId").val("00000000-0000-0000-0000-000000000000");
                            $("#ScheduleCenter_StatusId").val(result.Status);
                            $("#ScheduleCenter_PaymentSourceId").val(0);
                        }
                    });
                }
                $("#window_schedulecenter").WinFocus();
            } else { Acore.Open("schedulecenter", "Schedule/Center/1/" + Id, function() { Schedule.InitCenter(); }); }
        }
    },
    ShowMessages: function(messageId) {
        Acore.Open("messageinbox", 'Message/Inbox', function() { Message.Init(); Message._MessageId = messageId; });
    },
    ShowNewMessage: function() {
        Acore.Open("newmessage", 'Message/New', function() { Message.InitNew(); });
    },
    ShowUserTasks: function() {
        Acore.Open("listuserschedule", 'User/Schedule', function() { });
    },
    ShowPastDueRecerts: function() {
        Acore.Open("listpastduerecerts", 'Agency/RecertsPastDueGrid', function() { });
    },
    ShowUpcomingRecerts: function() {
        Acore.Open("listupcomingrecerts", 'Agency/RecertsUpcomingGrid', function() { });
    },
    ShowPatientCenter: function() {
        Acore.Open("patientcenter", 'Patient/Center', function() { Patient.InitCenter(); });
    },
    ShowNewPatient: function() {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); });
    },
    ShowEditPatient: function(Id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() { Patient.InitEdit(); }, { patientId: Id });
    },
    ShowNewReferral: function() {
        Acore.Open("newreferral", 'Referral/New', function() { Referral.InitNew(); });
    },
    ShowEditReferral: function(Id) {
        Acore.Open("editreferral", 'Referral/Edit', function() { Referral.InitEdit(); }, { Id: Id });
    },
    ShowNewContact: function() {
        Acore.Open("newcontact", 'Contact/New', function() { Contact.InitNew(); });
    },
    ShowEditContact: function(Id) {
        Acore.Open("editcontact", "Contact/Edit", function() { Contact.InitEdit(); }, { Id: Id });
    },
    ShowNewLocation: function() {
        Acore.Open("newlocation", 'Location/New', function() { Location.InitNew(); });
    },
    ShowEditLocation: function(Id) {
        Acore.Open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { Id: Id });
    },
    ShowNewPhysician: function() {
        Acore.Open("newphysician", 'Physician/New', function() { Physician.InitNew(); });
    },
    ShowEditPhysician: function(Id) {
        Acore.Open("editphysician", 'Physician/Edit', function() { Physician.InitEdit(); }, { Id: Id });
    },
    ShowNewHospital: function() {
        Acore.Open("newhospital", "Hospital/New", function() { Hospital.InitNew(); });
    },
    ShowEditHospital: function(Id) {
        Acore.Open("edithospital", "Hospital/Edit", function() { Hospital.InitEdit(); }, { Id: Id });
    },
    ShowNewInsurance: function() {
        Acore.Open("newinsurance", "Insurance/New", function() { Insurance.InitNew(); });
    },
    ShowEditInsurance: function(Id) {
        Acore.Open("editinsurance", "Insurance/Edit", function() { Insurance.InitEdit(); }, { Id: Id });
    },
    ShowNewTemplate: function() {
        Acore.Open("newtemplate", 'Template/New', function() { Template.InitNew(); });
    },
    ShowEditTemplate: function(id) {
        Acore.Open("edittemplate", "Template/Edit", function() { Template.InitEdit(); }, { Id: id });
    },
    ShowNewSupply: function() {
        Acore.Open("newsupply", 'Supply/New', function() { Supply.InitNew(); });
    },
    ShowEditSupply: function(id) {
        Acore.Open("editsupply", "Supply/Edit", function() { Supply.InitEdit(); }, { Id: id });
    },
    ShowRap: function(episodeId, patientId) {
        Acore.Open("rap", "Billing/Rap", function() { Billing.InitRap(); }, { episodeId: episodeId, patientId: patientId });
    },
    ShowFinal: function(episodeId, patientId) {
        Acore.Open("final", "Billing/Final", function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowEditPlanofCare: function(episodeId, patientId, eventId) {
        Acore.Open("editplanofcare", 'Oasis/Edit485', function() { PlanOfCare.InitEdit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditPlanofCareStandAlone: function(episodeId, patientId, eventId) {
        Acore.Open("newplanofcare", 'Oasis/New485', function() { PlanOfCare.InitNew(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowNewUser: function() {
        Acore.Open("newuser", "User/New", function() { User.InitNew(); });
    },
    ShowEditUser: function(Id) {
        Acore.Open("edituser", "User/Edit", function() { User.InitEdit(); }, { Id: Id });
    },
    ShowEditOrder: function(id, patientId) {
        Acore.Open("editorder", "Order/Edit", function() { Patient.InitEditOrder(); }, { id: id, patientId: patientId });
    },
    ShowReportCenter: function(DefaultReport) {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); Report.Show(DefaultReport); });
    },
    ShowBillingCenterRAP: function() {
        Acore.Open("billingcenterrap", "Billing/RAPCenter", function() { });
    },
    ShowBillingCenterFinal: function() {
        Acore.Open("billingcenterfinal", "Billing/FinalCenter", function() { });
    },
    ShowBillingCenterAllClaim: function() {
        Acore.Open("allbillingclaims", "Billing/AllClaims", function() { });
    },
    ShowPatientPrompt: function() {
        Acore.Modal({
            Name: "Next Step",
            Url: "Patient/NextStep",
            Height: "200px",
            Width: "350px",
            WindowFrame: false
        })
    },
    ShowNewIncidentReport: function() {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); });
    },
    ShowEditIncident: function(Id) {
        Acore.Open("editincidentreport", 'Incident/Edit', function() { IncidentReport.InitEdit(); }, { Id: Id });
    },
    ShowNewInfectionReport: function() {
        Acore.Open("newinfectionreport", 'Infection/New', function() { InfectionReport.InitNew(); });
    },
    ShowEditInfection: function(Id) {
        Acore.Open("editinfectionreport", 'Infection/Edit', function() { InfectionReport.InitEdit(); }, { Id: Id });
    },
    ShowPatientBirthdays: function() {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); Report.Show(1, '#patient_reports', '/Report/Patient/Birthdays'); return false; });
    },
    ShowPatientAuthorizations: function(patientId) {
        Acore.Open("listauthorizations", 'Authorization/Grid', function() { }, { patientId: patientId });
    },
    ShowDeletedTaskHistory: function(patientId) {
        Acore.Open("patientdeletedtaskhistory", 'Patient/DeletedTaskHistory', function() { }, { patientId: patientId });
    },
    ShowPatientOrdersHistory: function(patientId) {
        Acore.Open("patientordershistory", 'Patient/OrdersHistory', function() { }, { patientId: patientId });
    },
    ShowPatientSixtyDaySummary: function(patientId) {
        Acore.Open("patientsixtydaysummary", 'Patient/SixtyDaySummary', function() { }, { patientId: patientId });
    },
    ShowPatientVitalSigns: function(patientId) {
        Acore.Open("patientvitalsigns", 'Patient/VitalSigns', function() { }, { PatientId: patientId });
    },
    ShowMedicareEligibilityReports: function(patientId) {
        Acore.Open("medicareeligibilitylist", 'Patient/MedicareEligibilityList', function() { }, { patientId: patientId });
    },
    ShowPatientAllergies: function(patientId) {
        Acore.Open("allergyprofile", 'Patient/AllergyProfile', function() { }, { patientId: patientId });
    },
    ShowPatientHospitalizationLogs: function(Id) {
        Acore.Open("patienthospitalizationlogs", 'Patient/HospitalizationLogs', function() { }, { patientId: Id });
    },
    RefreshPlanofCare: function(episodeId, patientId, eventId) {
        $("#planofCareContentId").empty().addClass("loading").load('Oasis/PlanofCareContent', { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('This page could not be refreshed. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#planofCareContentId").removeClass("loading");
        });
    },
    ShowAgencySelectionModal: function(loginId) {
        $("#Agency_Selection_Container").load("/User/Agencies", { loginId: loginId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") U.ShowDialog("#Agency_Selection_Container", function() { User.InitChangeSignature() })
        })
    },
    ShowEpisodeOrders: function(episodeId, patientId) {
        Acore.Open("patientepisodeorders", 'Patient/EpisodeOrdersView', function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowPatientEligibility: function(medicareNumber, lastName, firstName, dob, gender) {
        if (medicareNumber == "" || lastName == "" || firstName == "" || dob == "" || gender == undefined) {
            var error = "Unable to process Medicare eligibility request:";
            if (medicareNumber == "") error += "<br /> &#8226; Medicare Number is Required";
            if (lastName == "") error += "<br /> &#8226; Last Name is Required";
            if (firstName == "") error += "<br /> &#8226; First Name is Required";
            if (dob == "") error += "<br /> &#8226; Date of Birth is Required";
            if (gender == undefined) error += "<br /> &#8226; Gender is Required";
            U.Growl(error, "error");
        } else {
            Acore.Modal({
                Name: "Medicare Eligibility",
                Url: "Patient/Verify",
                Input: {
                    medicareNumber: medicareNumber,
                    lastName: lastName,
                    firstName: firstName,
                    dob: dob,
                    gender: gender
                },
                Width: "800px",
                Height: "400px",
                WindowFrame: false
            })
        }
    },
    ShowMedicareEligibility: function(medEligibilityId, patientId) {
        Acore.Modal({
            Name: "Medicare Eligibility",
            Url: "Patient/MedicareEligibility",
            Input: {
                medicareEligibilityId: medEligibilityId,
                patientId: patientId
            },
            Width: "800px",
            Height: "400px",
            WindowFrame: false
        })
    },
    ShowMissedVisitModal: function(episodeId, patientId, eventId) {
        Acore.Modal({
            Name: "New Physician",
            Url: "Visit/Miss",
            Input: { episodeId: episodeId, patientId: patientId, eventId: eventId },
            OnLoad: User.InitMissedVisit,
            Width: "800px",
            Height: "475px",
            WindowFrame: false
        })
    },
    ShowMissedVisitEdit: function(eventId) {
        Acore.Open("editmissedvisit", "Schedule/EditMissedVisitInfo", function() { User.InitEditMissedVisit(); }, { Id: eventId });
    },
    ShowNewPhysicianModal: function() {
        Acore.Modal({
            Name: "New Physician",
            Url: "Physician/New",
            OnLoad: Physician.InitNewModal,
            Width: "900px",
            Height: "540px",
            WindowFrame: false
        })
    },
    ShowNewPhotoModal: function(patientId) {
        Acore.Modal({
            Name: "New Photo",
            Url: "Patient/NewPhoto",
            Input: { patientId: patientId },
            OnLoad: function() { Patient.InitNewPhoto(patientId); },
            Width: "315px",
            Height: "343px",
            WindowFrame: false
        })
    },
    ShowMultipleDayScheduleModal: function(episodeId, patientId) {
        Acore.Modal({
            Name: "Quick Employee Scheduler",
            Url: "Schedule/MultiDay",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: function() { Schedule.InitMultiDayScheduler(patientId, episodeId); },
            Width: "900px",
            Height: "450px",
            WindowFrame: false
        })
    },
    ShowNewEpisodeModal: function(Id) {
        Acore.Modal({
            Name: "New Episode",
            Url: "Schedule/NewEpisode",
            Input: { patientId: Id },
            OnLoad: Schedule.InitNewEpisode,
            Width: "800px",
            Height: "515px",
            WindowFrame: false
        })
    },
    ShowEditEpisodeModal: function(episodeId, patientId, action) {
        Acore.Modal({
            Name: "Edit Episode",
            Url: "Schedule/EditEpisode",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: function() { Schedule.InitEpisode(episodeId, patientId, action); },
            Width: "800px",
            Height: "585px",
            WindowFrame: false
        })
    },
    ShowAdmitPatientModal: function(id, type) {
        Acore.Modal({
            Name: "Admit Patient",
            Url: "Patient/NewAdmit",
            Input: { id: id, type: type },
            OnLoad: Patient.InitAdmit,
            Width: "850px",
            Height: "345px",
            WindowFrame: false
        })
    },
    ShowNonAdmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Patient",
            Url: "Patient/NewNonAdmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitNonAdmit,
            Width: "800px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowNonAdmitReferralModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Referral",
            Url: "Referral/NewNonAdmit",
            Input: { referralId: Id },
            OnLoad: Referral.InitNonAdmit,
            Width: "800px",
            Height: "410px",
            WindowFrame: false
        })
    },
    ShowMedicationModal: function(patientId, isnew) {
        Acore.Modal({
            Name: "Medications",
            Url: "485/Medication",
            Input: { patientId: patientId },
            OnLoad: function() {
                $("#PlanofCareMedicationIsNew").val(isnew);
                $("window_ModalWindow_content").css("overflow", "hidden");
                $('#MedicationGrid485 .t-grid-content').css("top", "60px")
            },
            Width: "800px",
            Height: "286px",
            WindowFrame: false
        })
    },
    ShowOasisValidationModal: function(Id, patientId, episodeId, assessmentType, showPpsPlus) {
        OasisValidation.Validate(Id, patientId, episodeId, assessmentType, showPpsPlus);
    },
    ShowModalEditClaim: function(patientId, Id, type) {
        Acore.Modal({
            Name: "Edit Claim",
            Url: "Billing/Update",
            Input: { patientId: patientId, id: Id, type: type },
            OnLoad: Billing.InitClaim,
            Width: "800px",
            Height: "270px",
            WindowFrame: false
        })
    },
    ShowNewClaimModal: function(type) {
        Acore.Modal({
            Name: "New Claim",
            Url: "Billing/NewClaim",
            Input: { patientId: Billing._patientId, type: type },
            OnLoad: Billing.InitNewClaim,
            Width: "500px",
            Height: "140px",
            WindowFrame: false
        })
    },
    ShowChangeStatusModal: function(Id) {
        Acore.Modal({
            Name: "Change Status",
            Url: "Patient/Status",
            Input: { patientId: Id },
            OnLoad: Patient.InitChangePatientStatus,
            Width: "800px",
            Height: "520px",
            WindowFrame: false
        })
    },
    ShowReadmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Patient Re-admit",
            Url: "Patient/Readmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitPatientReadmitted,
            Width: "800px",
            Height: "475px",
            WindowFrame: false
        })
    },
    ShowNewManagedClaimModal: function() {
        Acore.Modal({
            Name: "New Managed Claim",
            Url: "Billing/NewManagedClaim",
            Input: { patientId: ManagedBilling._patientId },
            OnLoad: ManagedBilling.InitNewClaim,
            Width: "500px",
            Height: "175px",
            WindowFrame: false
        })
    },
    ShowModalUpdateStatusManagedClaim: function(patientId, Id) {
        Acore.Modal({
            Name: "Update Status of Managed Claim",
            Url: "Billing/UpdateManagedClaim",
            Input: { patientId: patientId, id: Id },
            OnLoad: ManagedBilling.InitClaim,
            Width: "800px",
            Height: "250px",
            WindowFrame: false
        })
    },
    ShowNewManagedClaimPaymentModal: function() {
        Acore.Modal({
            Name: "Add Managed Claim Payment",
            Url: "Billing/NewManagedClaimPayment",
            Input: { id: ManagedBilling._ClaimId },
            OnLoad: ManagedBilling.InitNewPayment,
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowModalUpdatePaymentManagedClaim: function(Id) {
        Acore.Modal({
            Name: "Update Managed Claim Payment",
            Url: "Billing/UpdateManagedClaimPayment",
            Input: { id: Id },
            OnLoad: ManagedBilling.InitEditPayment,
            Width: "400px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowManagedClaimPayments: function(Id, patientId) { Acore.Open("managedclaimpayments", "Billing/ManagedClaimPayments", function() { }, { ClaimId: Id, patientId: patientId }); },
    ShowNewManagedClaimAdjustmentModal: function() {
        Acore.Modal({
            Name: "Add Managed Claim Adjustment",
            Url: "Billing/NewManagedClaimAdjustment",
            Input: { id: ManagedBilling._ClaimId },
            OnLoad: ManagedBilling.InitNewAdjustment,
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowModalUpdateAdjustmentManagedClaim: function(Id) {
        Acore.Modal({
            Name: "Update Managed Claim Adjustment",
            Url: "Billing/UpdateManagedClaimAdjustment",
            Input: { id: Id },
            OnLoad: ManagedBilling.InitEditAdjustment,
            Width: "400px",
            Height: "310px",
            WindowFrame: false
        })
    },
    ShowManagedClaimAdjustments: function(Id, patientId) {
        Acore.Open("managedclaimadjustments", "Billing/ManagedClaimAdjustments", function() { }, { ClaimId: Id, patientId: patientId });
    },
    ShowManagedClaim: function(Id, patientId) {
        Acore.Open("managedclaimedit", "Billing/ManagedClaim", function() { }, { Id: Id, patientId: patientId });
    },
    ShowMultipleReassignModal: function(episodeId, patientId, type) {
        Acore.Modal({
            Name: "Reasign Multiple Tasks",
            Url: "Schedule/ReAssignSchedulesContent",
            Input: { episodeId: episodeId, patientId: patientId, type: type },
            OnLoad: function() { Schedule.reassignScheduleInit(type, episodeId, patientId) },
            Width: "500px",
            Height: "280px",
            WindowFrame: false
        })
    },
    ShowCorrectionNumberModal: function(Id, patientId, episodeId, type, correctionNumber) {
        Acore.Modal({
            Name: "Correction Number",
            Url: "Oasis/Correction",
            Input: { Id: Id, PatientId: patientId, EpisodeId: episodeId, Type: type, CorrectionNumber: correctionNumber },
            OnLoad: Oasis.InitEditCorrectionNumber,
            Width: "675px",
            Height: "245px",
            WindowFrame: false
        })
    },
    ShowRemittanceDetail: function(Id) {
        Acore.Open("remittancedetail", 'Billing/RemittanceDetail', function() { }, { Id: Id });
    },
    ShowOrdersHistoryModal: function(id, patientId, episodeId, type) {
        Acore.Modal({
            Name: "Orders History",
            Url: "Agency/OrderHistoryEdit",
            Input: { id: id, patientId: patientId, episodeId: episodeId, type: type },
            OnLoad: Agency.OrderHistoryEditInit,
            Width: "500px",
            Height: "200px",
            WindowFrame: false
        })
    },
    Refresh: function() {
        Patient.RebindScheduleActivity(false);
        Schedule.RebindPatientsSelection(Schedule.RebindInput());
    },
    RefreshActivity: function(patientId, episodeId) {
        if (Acore.Windows.schedulecenter.IsOpen && U.IsGuid(patientId) && Schedule._patientId == patientId && Schedule._EpisodeId == episodeId) {
            Schedule.RefreshCurrentEpisode(patientId, episodeId, "All");
        }
        if (Acore.Windows.patientcenter.IsOpen && U.IsGuid(patientId) && Patient._patientId == patientId) {
            Patient.RebindScheduleActivity(false);
        }
    },
    RefreshData: function(patientId) {
        if (Acore.Windows.schedulecenter.IsOpen && Schedule._patientId == patientId) {
            Schedule.loadCalendarAndActivities(patientId);
        }
        if (Acore.Windows.patientcenter.IsOpen && Patient._patientId == patientId) {
            Patient.RebindScheduleActivity(false);
        }
    },
    RefreshOrders: function(data) {
        if (data.IsOrdersToBeSentRefresh) {
            Agency.RebindOrdersToBeSent();
        }
        if (data.IsOrdersPendingRefresh) {
            Agency.RebindPendingOrders();
        }
        if (data.IsOrdersHistoryRefresh) {
            Agency.RebindOrdersHistory();
        }
        if (data.IsPhysicianOrderPOCRefresh && Patient._patientId == data.PatientId) {
            Patient.RebindOrdersHistory(data.PatientId);
        }
    },
    RefreshCommunicationNotes: function(data) {
        if (data.IsCommunicationNoteRefresh) {
            Agency.RebindAgencyCommunicationNotes();
        }
        if (data.IsCommunicationNoteRefresh && U.IsGuid(data.PatientId) && Patient._patientId == data.PatientId) {
            Patient.RebindCommunicationNotes(data.PatientId);
        }
    },
    CloseModal: function() { if (Acore.Windows.ModalWindow != null && Acore.Windows.ModalWindow.IsOpen) $("#window_ModalWindow").Close(); },
    CloseAndRefreshActivity: function(window, patientId, episodeId) {
        UserInterface.RefreshActivity(patientId, episodeId);
        $("#window_" + window).Close();
    },
    CloseWindow: function(window) {
        if (Acore.Windows[window] && Acore.Windows[window].IsOpen) $("#window_" + window).Close();
    },
    RefreshCaseManagementMyScheduleTask: function(isCaseManagementBind, isMyScheduleTaskBind) {
        if (isCaseManagementBind && Acore.Windows.caseManagement.IsOpen) {
            Agency.RebindCaseManagement();
        }
        if (isMyScheduleTaskBind && Acore.Windows.listuserschedule.IsOpen) {
            User.RebindScheduleList();
        }
    },
    RefreshCenterQAMySchedule: function(data) {
        if (data.IsCenterRefresh) {
            Patient.RebindPatientsSelection(Patient.RebindInput());
            Schedule.RebindPatientsSelection(Schedule.RebindInput());
        }
        else {
            if (data.IsDataCentersRefresh) {
                UserInterface.RefreshActivity(data.PatientId, data.EpisodeId);
            }
        }
        UserInterface.RefreshCaseManagementMyScheduleTask(data.IsCaseManagementRefresh, data.IsMyScheduleTaskRefresh);
    },
    ShowNewBillData: function(insuranceId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/NewBillData",
            Input: { InsuranceId: insuranceId },
            OnLoad: Agency.InitBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowEditBillData: function(insuranceId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/EditBillData",
            Input: { InsuranceId: insuranceId, Id: Id },
            OnLoad: Agency.InitEditBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowNewBillDataInBilling: function(claimId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Billing/ManagedClaimNewBillData",
            Input: { ClaimId: claimId },
            OnLoad: ManagedBilling.InitBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowEditBillDataInBilling: function(claimId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Billing/ManagedClaimEditBillData",
            Input: { ClaimId: claimId, Id: Id },
            OnLoad: ManagedBilling.InitEditBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowNewLocationBillData: function(locationId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/NewLocationBillData",
            Input: { LocationId: locationId },
            OnLoad: Agency.InitLocationBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowEditLocationBillData: function(locationId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/EditLocationBillData",
            Input: { LocationId: locationId, Id: Id },
            OnLoad: Agency.InitLocationEditBillDate,
            Width: "500px",
            Height: "385px",
            WindowFrame: false
        })
    },
    ShowClaimResponse: function(Id) {
        Acore.Open("billingclaimresponse", 'Billing/ClaimResponse', function() { }, { Id: Id });
    },
    ShowEditPatientAdmission: function(patientId, Id, type) {
        Acore.Open(type.toLowerCase() + "patientadmission", 'Patient/AdmissionPatientInfo', function() { Patient.InitAdmissionPatient(patientId, type); }, { patientId: patientId, Id: Id, Type: type });
    },
    ShowClaimRemittance: function(Id, type) {
        Acore.Open("claimremittances", 'Billing/ClaimRemittance', function() { }, { Id: Id, Type: type });
    },
    ShowFrequencies: function(Id, patientId) {
        Acore.Modal({
            Name: "Visit Frequencies and Count",
            Url: "/Schedule/Frequencies",
            Input: { episodeId: Id, patientId: patientId },
            Width: 600,
            Height: 290,
            WindowFrame: false
        });
    },
    ShowNewAdjustmentCode: function() {
        Acore.Open("newadjustmentcode", 'AdjustmentCode/New', function() { AdjustmentCode.InitNew(); });
    },
    ShowEditAdjustmentCode: function(id) {
        Acore.Open("editadjustmentcode", "AdjustmentCode/Edit", function() { AdjustmentCode.InitEdit(); }, { Id: id });
    },
    MissedVisitPopup: function(e, missedVisitId) {
        Acore.Modal({
            Name: "Missed Visit",
            Url: "Schedule/MissedVisitInfo",
            Input: { id: missedVisitId },
            Width: "500px",
            Height: "300px"
        })
    }, ShowMultipleDelete: function(episodeId, patientId) {
        Acore.Open("scheduledelete", 'Schedule/DeleteSchedules', function() { }, { episodeId: episodeId, patientId: patientId });
    },
    EditReturnComments: function(id, existing, element) {
        Acore.Modal({
            Name: "Edit Return Comment",
            Content: $("<div/>").addClass("main wrapper").append(
                $("<fieldset/>").append(
                    $("<legend/>").text("Edit Return Comment")).append(
                    $("<div/>").addClass("wide-column").append(
                        $("<div/>").addClass("row").append(
                            $("<textarea>").addClass("fill tall").val(existing))))).append(
                $("<div/>").Buttons([
                    {
                        Text: "Update",
                        Click: function() {
                            U.PostUrl("Schedule/EditReturnReason", { id: id, comment: $(this).closest(".main").find("textarea").val() }, function(Result) {
                                U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                if (Result.isSuccessful) {
                                    $(".return-comments-container").each(function() { $(this).ReturnComments("Refresh") });
                                    $("#window_ModalWindow").Close()
                                }
                            })
                        }
                    }, {
                        Text: "Cancel",
                        Click: function() { $("#window_ModalWindow").Close() }
                    }
                ])
            ),
            Width: 400,
            Height: 240,
            WindowFrame: false
        })
    }
}
