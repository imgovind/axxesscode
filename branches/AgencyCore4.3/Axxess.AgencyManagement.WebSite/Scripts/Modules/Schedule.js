﻿var Schedule = {
    _scheduleWindow: "",
    _patientId: "",
    _patientStatus: "",
    _EpisodeId: "",
    _patientName: "",
    _EpisodeStartDate: "",
    _EpisodeEndDate: "",
    _tableId: "NursingScheduleTable",
    _Discipline: "all",
    _DisciplineIndex: 0,
    _isRebind: false,
    GetTableId: function() { return Schedule._tableId; },
    SetTableId: function(tableId) { Schedule._tableId = tableId; },
    GetId: function() { return Schedule._patientId; },
    SetId: function(patientId) { Schedule._patientId = patientId; },
    SetName: function(patientName) { Schedule._patientName = patientName; },
    SetPatientRowIndex: function(patientRowIndex) { Schedule._patientRowIndex = patientRowIndex; },
    GetEpisodeId: function() { return Schedule._EpisodeId; },
    SetEpisodeId: function(episodeId) { Schedule._EpisodeId = episodeId; },
    SetStartDate: function(episodeStartDate) { Schedule._EpisodeStartDate = episodeStartDate; },
    SetEndDate: function(episodeEndDate) { Schedule._EpisodeEndDate = episodeEndDate; },
    SetDiscipline: function(discipline) { Schedule._Discipline = discipline; },
    SetDisciplineIndex: function(disciplineIndex) { Schedule._DisciplineIndex = disciplineIndex; },
    Filter: function(text, gridContent) {
        if (gridContent != undefined) {
            if (text != undefined && text.length > 0) {
                text = $.trim(text).toUpperCase();
                var search = text.split(" ");
                if (search != undefined && search.length > 0) {
                    var gridTr = $("tbody  tr", gridContent);
                    if (gridTr != undefined && gridTr.length > 0) {
                        gridTr.removeClass("match").hide();
                        var matchedTrs = gridTr.filter(function(index) {
                            var tdL = $(this).find("td.searchL").text();
                            var tdF = $(this).find("td.searchF").text();
                            var result = $.grep(search, function(s, i) {
                                return (tdL.indexOf(s) > -1 || tdF.indexOf(s) > -1);
                            })
                            return result.length > 0;
                        });
                        matchedTrs.removeClass("t-alt").addClass("match").show().filter(":even").addClass("t-alt");
                    }
                }
            }
        }
    },
    InitCenter: function() {
        var container = $("#window_schedulecenter_content");
        if (container != undefined) {
            $(".top input[name=SearchText]", container).keyup(function() {
                var girdContent = $("#ScheduleSelectionGrid div.t-grid-content", container);
                if ($(this).val() != undefined && $(this).val().length) {
                    Schedule.Filter($(this).val(), girdContent)
                    if ($("tr.match:even", girdContent).length) {
                        $("tr.match:first", girdContent).click();
                    }
                    else $("#ScheduleMainResult", container).html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3" +
                    "Cdiv class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javas" +
                    "cript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
                }
                else {
                    $("tr", girdContent).removeClass("match t-alt").show();
                    $("tr:even", girdContent).addClass("t-alt");
                    $("tr:first", girdContent).click();
                }
            });
            $(".top select", container).change(function() { Schedule.RebindPatientsSelection(Schedule.RebindInput()); });
            $('.layout', container).layout();
            $('.t-grid-content', container).css({ height: 'auto' });
            Schedule.positionBottom();
            //            var status = Schedule._patientStatus;
            //            Schedule._patientStatus = status;
        }
    },
    Add: function(episodeId, patientId, date) {
        if ($('#schedule-tab-strip').is(':hidden')) { Schedule.ShowScheduler(episodeId, patientId, date); }
        else {
            if (Schedule._Discipline == "Multiple") { return; }
            else { Schedule.AddTableRow("#" + Schedule._Discipline + "ScheduleTable", date, Schedule._Discipline); }
        }
    },
    AllTableAdd: function(discipline, date) { Schedule.AddTableRow("#" + discipline + "ScheduleTable", date != undefined ? date : '', discipline); },
    AddTableRow: function(table, currentday, disp) {
        var tbody = $("tbody", $(table));
        if (tbody != undefined) {
            if ($("tr", tbody).length) {
                var firstTr = $("tr:first", tbody);
                if ($("input.current-date", firstTr).val() == "") {
                    $("input.current-date", firstTr).val(currentday);
                }
                else {
                    var cloneTr = firstTr.clone();
                    cloneTr.find("input.current-date").val(currentday);
                    cloneTr.find("input.DisciplineTask").val('');
                    cloneTr.find("input.Users ").val('');
                    cloneTr.appendTo(tbody);
                    Schedule.positionBottom();
                }
            }
            else {
                var row = "%3Ctr%3E%3Ctd%3E%3Cselect class=%22DisciplineTask requireddropdown%22%3E%3Coption value=%220%22%3E-- Select Discipline --%3C/option%3E%3C/select%3E%3C/td%3" +
                "E%3Ctd%3E%3Cselect class=%22supplies Code Users requireddropdown%22%3E%3Coption value=%2200000000-0000-0000-0000-000000000000%22%3ESelect Employee%3C/option%3E%3" +
                "C/select%3E%3C/td%3E%3Ctd%3E%3Cinput onclick=%22javascript:void(0);%22 type=%22text%22 class=%22current-date%22 value=%22" + currentday + "%22 re" +
                "adonly=%22readonly%22 /%3E%3C/td%3E%3Ctd%3E%3Ca href=%22javascript:void(0);%22 class=%22action%22 onclick=%22Schedule.DeleteRow($(this));%22%3EDele" +
                "te%3C/a%3E%3C/td%3E%3C/tr%3E";
                $(table).find('tbody').append(unescape(row));
                Lookup.LoadDiscipline(table, disp);
                Lookup.LoadUsers(table);
                Schedule.positionBottom();
            }
        }
    },
    DeleteRow: function(control) {
        if ($(control).closest("tbody").find("tr").length == 1) {
            $(".schedule-tables.purgable", control).each(function() { $(this).find("tbody").empty() });
            Schedule.ShowAll(Schedule._EpisodeId, Schedule._patientId);
        } else $(control).closest("tr").remove();
        Schedule.positionBottom();
    },
    RebindInput: function() { return $("#window_schedulecenter_content .layout-left .top .filterInput").serializeArray(); },
    RebindPatientsSelection: function(input) {
        var grid = $('#ScheduleSelectionGrid');
        if (grid != undefined) {
            var scheduleSelectionGridData = grid.data('tGrid');
            if (scheduleSelectionGridData != undefined) {
                $.ajax({
                    url: 'Schedule/All',
                    type: "POST",
                    data: input,
                    dataType: 'json',
                    success: function(dataResult) {
                        // Schedule.SetId(patientId);
                        scheduleSelectionGridData.dataBind(dataResult);
                    }
                });
            }
        }
    },
    OnPatientDataBinding: function(e) { U.OnDataBindingStopAction(e, Schedule.RebindInput(), "/Schedule/AllSort"); },
    PatientListDataBound: function() {
        var leftContainer = $("#window_schedulecenter_content");
        if (leftContainer != undefined) {
            var filterBoxText = $(" .layout-left .top input[name=SearchText]", leftContainer).val();
            var gridContentTrs = $(" .layout-left .bottom #ScheduleSelectionGrid .t-grid-content tr", leftContainer);
            if (gridContentTrs != undefined && gridContentTrs.length) {
                if (Schedule._patientId == "") {
                    if (filterBoxText.length) {
                        gridContentTrs.filter(".match").eq(0).click()
                    } else {
                        gridContentTrs.eq(0).click();
                    }
                }
                else {
                    var tdWithPatientId = $('td:contains(' + Schedule._patientId + ')', gridContentTrs);
                    if (tdWithPatientId != undefined && tdWithPatientId.length > 0) {
                        tdWithPatientId.eq(0).closest('tr').click();
                    }
                    else {
                        gridContentTrs.eq(0).click();
                    }
                }
                var selectedRows = gridContentTrs.filter(".t-state-selected");
                if (selectedRows != undefined && selectedRows.length > 0) {
                    selectedRows.eq(0).closest('.t-grid-content').scrollTop(selectedRows.eq(0).position().top - 50);
                }
            }
            else {
                $("#ScheduleMainResult", leftContainer).removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
            }
        }
    },
    PatientListFirstDataBound: function(Id, gridId) {
        var selectedTr = $('td:contains(' + Id + ')', $('#' + gridId)).closest('tr');
        if (selectedTr != undefined) {
            selectedTr.addClass("t-state-selected");
            var gridContent = selectedTr.closest(".t-grid-content");
            if (gridContent != undefined) {
                var position = selectedTr.position();
                if (position != undefined) {
                    var scroll = position.top + gridContent.scrollTop() - 24;
                    if (scroll != undefined && scroll > 24) {
                        gridContent.animate({ scrollTop: scroll }, 'slow');
                    }
                }
            }
        }
        Schedule.SetId(Id);
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            if (!Schedule._isRebind) {
                Schedule._EpisodeId = "";
            }
            Schedule._Discipline = "all";
            Schedule._DisciplineIndex = 0;
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Schedule.loadCalendarAndActivities(patientId);
        }
    },
    RebindActivity: function(episodeId, patientId, discipline) {
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        if (scheduleActivityGrid != undefined && scheduleActivityGrid != null) {
            $.ajax({
                url: 'Schedule/Activity',
                type: "POST",
                data: { episodeId: episodeId, patientId: patientId, discipline: discipline },
                dataType: 'json',
                success: function(dataResult) {
                    scheduleActivityGrid.dataBind(dataResult.ScheduleEvents);
                    // U.ClientBindScheduleActivityInit('ScheduleActivityGrid');
                }
            });
        }
    },
    ActivityOnBound: function(e) { U.ClientBindScheduleActivityInit('ScheduleActivityGrid'); },
    ActivityOnDataBinding: function(e) {
        var parameters = {
            episodeId: Schedule._EpisodeId,
            patientId: Schedule._patientId,
            discipline: Schedule._Discipline
        };
        var url = "/Schedule/ActivitySort";
        U.OnDataBindingStopAction(e, parameters, url);
    },
    loadCalendarAndActivities: function(patientId) {
        $('#ScheduleMainResult').empty().addClass('loading').load('Schedule/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#ScheduleMainResult').removeClass('loading');
            Schedule.SetId(patientId);
            if (textStatus == 'error') $('#ScheduleMainResult').html(U.AjaxError);
            $('#window_schedulecenter_content .t-grid-content').css({ height: 'auto' });
        });
        Schedule._isRebind = false;
    },
    RefreshCurrentEpisode: function(patientId, episodeId, discipline, action) {
        $('#ScheduleMainResult').empty().addClass('loading').load('Schedule/RefreshData', { patientId: patientId, episodeId: episodeId, discipline: discipline }, function(responseText, textStatus, XMLHttpRequest) {
            $('#ScheduleMainResult').removeClass('loading');
            if (textStatus == 'error') {
                $('#ScheduleMainResult').html(U.AjaxError);
            }
            else if (textStatus == "success") {
                Schedule.SetDiscipline(discipline);
                Schedule.SetId(patientId);
                Schedule.SetEpisodeId(episodeId);
                $('#window_schedulecenter_content .t-grid-content').css({ height: 'auto' });
                if (action != undefined && typeof (action) == 'function') {
                    action();
                }
            }
        });
    },
    NavigateEpisode: function(episodeId, patientId) { Schedule.RefreshCurrentEpisode(patientId, episodeId, "All"); },
    ShowScheduler: function(episodeId, patientId, date) {
        if ($('#schedule-tab-strip').is(':visible')) {
            $('#schedule-collapsed').find(".show-scheduler").html("Show Scheduler");
            $('#schedule-tab-strip').hide();
            Schedule.ShowAll(episodeId, patientId);
        }
        else {
            $('#schedule-tab-strip').show();
            if ($("#Nursing_Tab").hasClass("t-state-active")) { }
            else { $("#Nursing_Tab").click(); }
            Schedule.SetDiscipline('Nursing');
            Schedule.loadCalendarNavigation(episodeId, patientId);
            Schedule.RebindActivity(episodeId, patientId, 'Nursing');
            if ($(".schedule-tables.purgable tbody tr").length == 0)
            { Schedule.AllTableAdd('Nursing', date != undefined ? date : ''); }
            else {
                $('#schedule-tab-strip').show();
                $('#schedule-collapsed').find(".show-scheduler").html("Hide Scheduler");
            }
        }
        Schedule.positionBottom();
    },
    loadCalendarNavigation: function(episodeId, patientId) {
        $('#scheduleTop').load('Schedule/CalendarNav', { patientId: patientId, episodeId: episodeId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleTop').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere was an er" +
                    "ror loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further ass" +
                    "istance.%3C/div%3E"));
            }
            Schedule.positionBottom();
        });
    },
    ShowAll: function(episodeId, patientId) {
        Schedule.SetDiscipline('All');
        Schedule.RefreshCurrentEpisode(patientId, episodeId, "All");
    },
    ScheduleInputFix: function(control, tableName, patientId, episodeId) {
        var submit = true;
        var tableTr = $('tbody tr', $(tableName));
        if (tableTr != undefined) {
            var len = tableTr.length;
            var i = 1;
            var jsonData = '[';
            $(tableTr).each(function() {
                var disciplineTask = $(this).find("select.DisciplineTask").val();
                var userId = $(this).find("select.Users").val();
                var date = $(this).find("input.current-date").val();
                if (disciplineTask != "0" && disciplineTask != "" && userId != "" && userId != "00000000-0000-0000-0000-000000000000" && date != "") {
                    if (len + 1 > i) jsonData += '{"DisciplineTask":"' + disciplineTask + '","UserId":"' + userId + '","EventDate":"' + date + '","Discipline":"' + $(this).find('.DisciplineTask').find(":selected").attr("data") + '","IsBillable":"' + $(this).find('.DisciplineTask').find(":selected").attr("isbillable") + ' "}';
                    if (len > i) jsonData += ',';
                    i++
                } else {
                    U.Growl("Unable to schedule this task. Make sure all required fields are entered in row " + ($(this).parent().children().index($(this)) + 1) + ". ", "error");
                    submit = false;
                }
            });
            jsonData += ']';

            if (submit) {
                var form = control.closest("form");
                if (form != undefined) {
                    Schedule.HandlerHelper(form, control, { patientId: patientId, episodeId: episodeId, schedule: jsonData });
                }
            }
        }
    },
    HandlerHelper: function(form, control, input) {
        var options = {
            data: input,
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    if (input != undefined) {
                        if (Schedule._patientId == input.patientId && Schedule._EpisodeId == input.episodeId) {
                            Schedule.RefreshCurrentEpisode(input.patientId, input.episodeId, "All");
                        }
                        if (Patient._patientId == input.patientId) {
                            Patient.RebindScheduleActivity(false);
                        }
                    }
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(result.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmitMultiple: function(control, patientId, episodeId) {
        var form = control.closest("form")
        if (form != undefined) {
            Schedule.HandlerHelperMultiple(form, control, { patientId: patientId, episodeId: episodeId, Discipline: form.find('#multipleDisciplineTask').find(":selected").attr("data"), IsBillable: form.find('#multipleDisciplineTask').find(":selected").attr("IsBillable") });
        }
    },
    HandlerHelperMultiple: function(form, control, input) {
        var options = {
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    if (Schedule._patientId == input.patientId && Schedule._EpisodeId == input.episodeId) {
                        Schedule.RefreshCurrentEpisode(input.patientId, input.episodeId, "All");
                    }
                    if (Patient._patientId == input.patientId) {
                        Patient.RebindScheduleActivity(false);
                    }
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(result.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    SubmitReassign: function(control, patientId, episodeId) {
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    if (Schedule._patientId == patientId && Schedule._EpisodeId == episodeId) {
                        Schedule.RefreshCurrentEpisode(patientId, episodeId, "All");
                    }
                    if (Patient._patientId == patientId) {
                        Patient.RebindScheduleActivity(true);
                    }
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            },
            error: function() { U.Growl("Unable to reassign task to this user", "error") }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    OnSelect: function(e) {
        var content = $(e.contentElement);
        var tableControl = $('table', content);
        if ($(tableControl).attr('id') == "multiple-schedule-table") {
            Schedule.SetDiscipline($(tableControl).attr('data'));
            return true;
        }
        Schedule.SetDisciplineIndex($(e.item).index());
        Schedule.SetTableId($(tableControl).attr('id'));
        Schedule.SetDiscipline($(tableControl).attr('data'));
        if (Schedule._Discipline == "Multiple") {
            return;
        }
        else {
            var patientId = $("#SchedulePatientID").val();
            var episodeId = $("#ScheduleEpisodeID").val()
            Schedule.loadCalendarNavigation(episodeId, patientId);
            Schedule.RebindActivity(episodeId, patientId, Schedule._Discipline);
            var name = "#" + Schedule.GetTableId() + ".schedule-tables.purgable ";
            var $table = $(name);
            if ($("tbody tr", $table).length == 0)
            { Schedule.AllTableAdd(Schedule._Discipline, ''); }
        }
    },
    Delete: function(patientId, episodeId, eventId, employeeId, task) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Schedule/Delete", { episodeId: episodeId, patientId: patientId, eventId: eventId, employeeId: employeeId, task: task }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    if (Schedule._patientId == patientId && Schedule._EpisodeId == episodeId) {
                        Schedule.RefreshCurrentEpisode(patientId, episodeId, "All");
                    }
                    if (Patient._patientId == patientId) {
                        Patient.RebindScheduleActivity(false);
                    }
                    Agency.RebindMissedVisitList();
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    Restore: function(episodeId, patientId, eventId) {
        if (confirm("Are you sure you want to restore this task?")) {
            U.PostUrl("/Schedule/Restore", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
                if (result.isSuccessful) {
                    if (Schedule._patientId == patientId && Schedule._EpisodeId == episodeId) {
                        Schedule.RefreshCurrentEpisode(patientId, episodeId, "All");
                    }
                    if (Patient._patientId == patientId) {
                        Patient.RebindScheduleActivity(false);
                    }
                    Agency.RebindMissedVisitList();
                    var deletedGrid = $('#List_Patient_DeletedTasks').data('tGrid');
                    if (deletedGrid != null) {
                        deletedGrid.rebind({ patientId: patientId });
                    }
                    U.Growl(result.errorMessage, "success");
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    ReAssign: function(control, episodeId, patientId, id, oldEmployeeId) {
        control.hide();
        control.parent().append(unescape("%3Cform action=%22/Schedule/ReAssign%22 method=%22post%22%3E%3Cinput name=%22episodeId%22 type=%22hidden%22 value=%22" +
            episodeId + "%22 /%3E%3Cinput name=%22oldUserId%22 type=%22hidden%22 value=%22" + oldEmployeeId + "%22 /%3E%3Cinput name=%22patientId%22 type=%22hidd" +
            "en%22 value=%22" + patientId + "%22 /%3E%3Cinput name=%22eventId%22 type=%22hidden%22 value=%22" + id + "%22 /%3E%3Cselect class=%22Users%22 name=%2" +
            "2userId%22 style=%22width:130px;%22 class=%22Users%22%3E%3C/select%3E%3Cinput type=%22button%22 value=%22Save%22 onclick=%22Schedule.SubmitReassign(" +
            "$(this),'" + patientId + "','" + episodeId + "');%22/%3E %3Cinput type=%22button%22 value=%22Cancel%22 onclick=%22Schedule.CancelReassign($(this));%" +
            "22 /%3E%3C/form%3E"));
        Lookup.LoadUsers("#" + control.closest(".t-grid").attr("id"));
    },
    ReOpen: function(episodeId, patientId, eventId) {
        U.PostUrl("/Schedule/Reopen", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            if (result.isSuccessful) {
                UserInterface.RefreshCenterQAMySchedule(result);
                UserInterface.RefreshOrders(result);
                Oasis.RefreshGrids(result);
                UserInterface.RefreshCommunicationNotes(result);
            }
            else
            { U.Growl(result.errorMessage, "error"); }
        })
    },
    CancelReassign: function(control) {
        var reassignLink = control.parent().parent().find('a.reassign').show();
        control.parent().remove();
    },
    loadMasterCalendar: function(patientId, episodeId) {
        Acore.Open("masterCalendarMain", 'Schedule/MasterCalendar', function() {
            $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                $(this).css({
                    position: 'relative',
                    left: -100,
                    top: 0
                });
            });
        }, { patientId: patientId, episodeId: episodeId });
    },
    loadMasterCalendarNavigation: function(EpisodeId, PatientId) {
        $('#window_masterCalendarMain_content').load('/Schedule/MasterCalendar', { patientId: PatientId, episodeId: EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere " +
                    "was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fu" +
                    "rther assistance.%3C/div%3E"));
            }
        });
    },
    InitEpisode: function(episodeId, patientId, action) {
        $("#Edit_Episode_StartDate").blur(function() { setTimeout(Schedule.editEpisodeStartDateOnChange, 200) });
        var form = $("#editEpisodeForm");
        U.InitValidation(form, function(result) {
            UserInterface.CloseModal();
            if (result.IsDataCentersRefresh) {
                UserInterface.RefreshData(result.PatientId);
                Schedule.ReLoadInactiveEpisodes(result.PatientId);
            }
            if (action != null && action != undefined && typeof (action) == "function") {
                action();
            }
        });
    },
    gatherMultiSchedulerDates: function() {
        var visitDates = '';
        $('.multiDayScheduleTable td.selectdate').each(function() {
            visitDates += $(this).attr("date") + ",";
        });
        $("#multiDayScheduleVisitDates").val(visitDates);
    },
    InitMultiDayScheduler: function(patientId, episodeId) {
        var form = $("#multiDayScheduleForm");
        U.InitValidation(form, function(result) { UserInterface.CloseModal(); UserInterface.RefreshActivity(patientId, episodeId); });
    },
    InitNewEpisode: function() {
        $("#New_Episode_StartDate").blur(function() { setTimeout(Schedule.newEpisodeStartDateOnChange, 200) });
        $('#New_Episode_PatientId').change(function() {
            $("#New_Episode_PrimaryPhysician").PhyscianInput();
            var patientId = $(this).val();
            U.PostUrl("/Patient/GetPatientForEpisode", "patientId=" + patientId, function(data) {
                if (data != null) {
                    $("#newEpisodeTargetDateDiv").show();
                    $("#newEpisodeTargetDate").html(data.StartOfCareDateFormatted);
                    $("#newEpisodeTip").html("<label class=\"bold\">Tip:</label><em> Last Episode end date is: " + data.EndDateFormatted + "</em>");
                } else {
                    $("#newEpisodeTip").html("");
                    $("#newEpisodeTargetDate").html("");
                    $("#newEpisodeTargetDateDiv").hide();
                }
            });
        });
        var form = $("#newEpisodeForm");
        U.InitValidation(form, function(result) { UserInterface.CloseModal(); UserInterface.CloseWindow("newepisode"); UserInterface.RefreshData(result.PatientId); });
    },
    InitTopMenuNewEpisode: function() {
        $('#TopMenuNew_Episode_PatientId').change(function() {
            var patientId = $(this).val();
            $('#topMenuNewEpisodeContent').Load('Schedule/NewPatientEpisodeContent', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
                if (textStatus == 'error') {
                    $('#topMenuNewEpisodeContent').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere w" +
                    "as an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fur" +
                    "ther assistance.%3C/div%3E"));
                }
            });
        });
        var form = $("#topMenuNewEpisodeForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("newepisode"); UserInterface.RefreshData(result.PatientId); });
    },
    InitTaskDetails: function() {
        var form = $("#Schedule_DetailForm");
        U.InitValidation(form, function(result) { UserInterface.CloseWindow("scheduledetails"); UserInterface.RefreshCenterQAMySchedule(result); });
    },
    GetTaskDetails: function(episodeId, patientId, eventId) {
        Acore.Open("scheduledetails", 'Schedule/EditDetails', function() { Schedule.InitTaskDetails(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    GetTaskAttachments: function(episodeId, patientId, eventId) {
        Acore.Modal({ Url: 'Schedule/Attachments', Input: { episodeId: episodeId, patientId: patientId, eventId: eventId }, Width: 500, Height: 225, WindowFrame: false });
    },
    positionBottom: function() {
        $('#window_schedulecenter .layout-main .bottom').css({ top: $('#window_schedulecenter .layout-main .top').height() + "px" });
    },
    loadNoteSupplyWorkSheet: function(episodeId, patientId, eventId) {
        Acore.Open("notessupplyworksheet", 'Schedule/SupplyWorksheet', function() { }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ProcessNote: function(button, episodeId, patientId, eventId) {
        U.PostUrl("/Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseModal();
                UserInterface.RefreshCenterQAMySchedule(result);
                if (result.IsOrdersToBeSentRefresh) {
                    Agency.RebindOrdersToBeSent();
                }
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    ToolTip: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    WoundCareInit: function() {
        $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
        $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#WoundCare_EpisodeId").val() + "&patientId=" + $("#WoundCare_PatientId").val() + "&eventId=" + $("#WoundCare_EventId").val() + "&name=" + name + "&assetId=" + assetId;
            U.PostUrl("/Schedule/DeleteWoundCareAsset", input, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"float-left uploadWidth\" />");
                }
            });
        }
    },
    DeleteScheduleEventAsset: function(control, patientId, episodeId, eventId, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            U.PostUrl("/Schedule/DeleteScheduleEventAsset", { patientId: patientId, episodeId: episodeId, eventId: eventId, assetId: assetId }, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('span').remove();
                    $("#scheduleEvent_Assest_Count").html($("#scheduleEvent_Assest_Count").html() - 1);
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                }
            });
        }
    },
    newEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("New")
    },
    editEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("Edit")
    },
    EpisodeStartDateChange: function(prefix) {
        var startDate = $("#" + prefix + "_Episode_StartDate"), endDate = $("#" + prefix + "_Episode_EndDate");
        if (startDate.val()) {
            var newStartDate = new Date(startDate.val());
            var newEndDate = new Date(startDate.val());
            newEndDate.setDate(newStartDate.getDate() + 59);
            var month = newEndDate.getMonth() + 1, day = newEndDate.getDate(), year = newEndDate.getFullYear();
            endDate.val(month + "/" + day + "/" + year);
        }
    },
    PhlebotomyLab: function(selector) {
        $(selector).Autocomplete({ source: ["CBC", "Chem 7", "BMP", "PT/INR", "PT", "Chem 8", "K+", "Urinalysis", "TSH", "CBC W/ diff", "Hemoglobin A1c", "Lipid Panel", "Comp Metabolic Panel  (CMP 14)", "TSH", "ALT/SGOT", "ALT/SGBOT", "Iron (Fe)"] })
    },
    reassignScheduleInit: function(identifier, episodeId, patientId) {
        var form = $("#reassign" + identifier + "Form");
        U.InitValidation(form, function(result) {
            if (identifier == "All")
            { UserInterface.CloseWindow('schedulereassign'); }
            else { UserInterface.CloseModal(); }
            UserInterface.RefreshActivity(patientId, episodeId);
            UserInterface.RefreshCommunicationNotes(result);
            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
        });
    },
    loadEpisodeDropDown: function(dropDown, control) {
        U.PostUrl("/Schedule/EpisodeRangeList", { patientId: $(control).val() }, function(data) {
            var s = $("select#" + dropDown);
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Episode --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Range, itemData.Id, false, false);
            });
        });
    },
    BulkUpdate: function(control, action) {
        var fields = $(":input", $(control)).serializeArray();
        U.PostUrl("Schedule/BulkUpdate", fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                if (action != null && action != undefined && typeof (action) == "function") {
                    action(data);
                }
            } else U.Growl(data.errorMessage, "error");
        });
    },
    LoadLog: function(patientId, eventId, task) {
        Acore.Open("schdeuleeventlogs", 'Schedule/ScheduleLogs', function() { }, { patientId: patientId, eventId: eventId, task: task });
    },
    GetPlanofCareUrl: function(episodeId, patientId, eventId) {
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
        U.PostUrl("/Oasis/GetPlanofCareUrl", input, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { 'episodeId': result.episodeId, 'patientId': result.patientId, 'eventId': result.eventId });
            else { U.Growl(result.errorMessage, "error"); }
        });
    },
    GetHHACarePlanUrl: function(episodeId, patientId, eventId) {
        U.GetAttachment("/Schedule/HHACarePlanPdf", { 'episodeId': episodeId, 'patientId': patientId, 'eventId': eventId });
    },
    GetHHACarePlanUrlFromHHAVisit: function(episodeId, patientId, eventId) {
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
        U.PostUrl("/Schedule/HHACarePlanPdfFromHHAVisit", input, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { 'episodeId': result.episodeId, 'patientId': result.patientId, 'eventId': result.eventId });
            else { U.Growl(result.errorMessage, "error"); }
        });
    },
    loadInactiveEpisodes: function(patientId) { Acore.Open("inactiveepisode", 'Schedule/Inactive', function() { }, { patientId: patientId }); },
    ActivateEpisode: function(episodeId, patientId) {
        U.PostUrl('Schedule/ActivateEpisode', { 'episodeId': episodeId, 'patientId': patientId }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                UserInterface.RefreshData(patientId);
                UserInterface.RefreshCaseManagementMyScheduleTask(true, true);
                Schedule.ReLoadInactiveEpisodes(patientId);
            } else U.Growl(data.errorMessage, "error");
        });
    },
    ReLoadInactiveEpisodes: function(patientId) {
        $("#InactiveEpisodesContent ol").addClass('loading');
        $("#InactiveEpisodesContent").load('Schedule/InactiveGrid', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#InactiveEpisodesContent ol").removeClass("loading");
            if (textStatus == 'error') $('#InactiveEpisodesContent').html(U.AjaxError);
        });
    },
    LoadEpisodeLog: function(episodeId, patientId) { Acore.Open("episodelogs", 'Schedule/EpisodeLogs', function() { }, { episodeId: episodeId, patientId: patientId }); },
    ConvertToTime: function(time) {
        if (time != undefined) {
            var hours = +time.substring(0, 2);
            var minutes = +time.substring(3, 5);
            var pm = time.indexOf("P") != -1;
            if (pm && hours !== 12) {
                hours += 12;
            }
            var date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);
            return date;
        }
        else
            return new Date();
    },
    CheckTimeInOut: function(prefix, showMessage) {
        var tempTimeIn = $("#" + prefix + "_TimeIn").val();
        var tempTimeOut = $("#" + prefix + "_TimeOut").val();
        var timeIn = Schedule.ConvertToTime(tempTimeIn);
        var timeOut = Schedule.ConvertToTime(tempTimeOut);
        var timeOutNextDay = Schedule.ConvertToTime(tempTimeOut);
        timeOutNextDay.setDate(timeOutNextDay.getDate() + 1);
        if (timeIn && timeOut) {
            var timeDiff = (timeOut - timeIn) / 60 / 60 / 1000;
            var timeDiffNextDay = (timeOutNextDay - timeIn) / 60 / 60 / 1000;
            var over3Hours = timeDiff > 3 || timeDiff < 0;
            var over3HoursNextDay = timeDiffNextDay > 3 || timeDiffNextDay < 0;
            if (over3Hours && !over3HoursNextDay)
                over3Hours = false;
            var defaultClick = "$('#" + prefix + "Form').focus();";
            if (over3Hours == true) {
                if (showMessage == true) {
                    var growl = $(".jGrowl-notification").text();
                    if (growl == "") {
                        U.Growl("WARNING: The Time In and Out are over 3 hours apart.", "warning");
                    }
                }
                else {
                    var answer = confirm('The time out is 3 hours greater than the time in. Are you sure you want to continue?');
                    return !answer;
                }
            } else {
                return false;
            }
        }
    },
    WarnTimeInOut: function(prefix, type) {
        Schedule.CheckTimeInOut(prefix, true);
        if (type === "Details") {
            var saveBtn = $("form#" + prefix + "Form a:contains('Save')");
            var saveBtnClick = saveBtn.attr('onClick');
            saveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + saveBtnClick + "}else{return false}");
        }
        else if (type === "Oasis") {
            var finishBtn = $("div#print-controls a:contains('Finish')");
            var finishBtnClick = finishBtn.attr('onClick');
            finishBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + finishBtnClick + "}else{return false}");
        }
        else {
            var saveBtn = $("form#" + prefix + "Form a:contains('Save')");
            var completeBtn = $("form#" + prefix + "Form a:contains('Complete')");
            var approveBtn = $("form#" + prefix + "Form a:contains('Approve')");
            var returnBtn = $("form#" + prefix + "Form a:contains('Return')");
            var saveBtnClick = saveBtn.attr('onClick');
            var completeBtnClick = completeBtn.attr('onClick');
            var approveBtnClick = approveBtn.attr('onClick');
            var returnBtnClick = returnBtn.attr('onClick');
            saveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + saveBtnClick + "}");
            completeBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + completeBtnClick + "}");
            approveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + approveBtnClick + "}");
            returnBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + returnBtnClick + "}");
        }
    },
    loadDiagnosisData: function(patientId, episodeId) {
        U.PostUrl("Schedule/GetDiagnosisData", { patientId: patientId, episodeId: episodeId }, function(data) {
            $("#PrimaryDiagnosis").text(data.diag1);
            $("#SecondaryDiagnosis").text(data.diag2);
            if (data.code1 === '') {
                $("#ICD9M").remove();
            } else {
                $("#PrimaryDiagnosis").append("<a id='ICD9M' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code1 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
            }
            if (data.code2 === '') {
                $("#ICD9M1").remove();
            } else {
                $("#SecondaryDiagnosis").append("<a id='ICD9M1' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code2 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
            }
        });
    },
    WarnEpisodeGap: function(prefix) {
        $("#" + prefix + "_StartDate").bind("ValueSet", function(event) {
            Schedule.CheckEpisodeStartDate(prefix, true)
        });
        var formPrefix = "";
        if (prefix == "Edit_Episode")
            formPrefix = "editEpisode";
        else if (prefix == "New_Episode")
            formPrefix = "newEpisode";
        else if (prefix == "TopMenuNew_Episode")
            formPrefix = "topMenuNewEpisode";
        $("#" + formPrefix + "Form a:contains('Save')").unbind('click');
        $("#" + formPrefix + "Form a:contains('Save')").click(function() {
            if (Schedule.CheckEpisodeStartDate(prefix, false)) {
                var answer = confirm("There is a gap between the start date and the last episode's end date. Are you sure you want to save?");
                if (answer) $(this).closest('form').submit()
            }
            else {
                $(this).closest('form').submit()
            }
        });
    },
    CheckEpisodeStartDate: function(prefix, showGrowl) {
        var d1 = $("#" + prefix + "_StartDate").val();
        var d2 = $("#" + prefix + "_PreviousEndDate").val();
        if (d1 != "" && d2 != "") {
            var startDate = $.datepicker.parseDate("mm/dd/yy", d1);
            var previousEndDate = $.datepicker.parseDate("mm/dd/yy", d2);
            previousEndDate.setDate(previousEndDate.getDate() + 1)
            if (startDate.getTime() != previousEndDate.getTime()) {
                if (showGrowl) U.Growl("WARNING: There is a gap between the start date and the last episode's end date.", "warning");
                return true;
            }
        }
        return false;
    },
    RestoreMissedVisit: function(episodeId, patientId, eventId) {
        var answer = confirm("Are you sure you want to restore this missed visit?");
        if (answer) {
            U.PostUrl("Schedule/MissedVisitRestore", {
                episodeId: episodeId,
                patientId: patientId,
                eventId: eventId
            }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.RefreshActivity(patientId, episodeId);
                    Agency.RebindMissedVisitList();
                    Agency.RebindOrders();
                } else U.Growl(data.errorMessage, 'error');
            })
        }
    },
    SelectPatient: function(patientId, status) {
        var selectionFilter = $("#txtSearch_Schedule_Selection");
        if (selectionFilter.val() != "") {
            $("#txtSearch_Schedule_Selection").val("");
            $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
        }

        var statusNumber = !isNaN(status) ? status : (status.toLowerCase() == "false" ? "1" : "2");
        var statusDropDown = $("select.ScheduleStatusDropDown");
        if (statusNumber != statusDropDown.val()) {
            statusDropDown.val(statusNumber);
            Schedule.SetId(patientId);
            U.RebindTGrid($('#ScheduleSelectionGrid'), { branchId: $("select.ScheduleBranchCode").val(), statusId: $("select.ScheduleStatusDropDown").val(), paymentSourceId: $("select.SchedulePaymentDropDown").val() });
        }
        else {
            var row = $("#ScheduleSelectionGrid .t-grid-content tr td.t-last:contains('" + patientId + "')").parent();
            $("#ScheduleSelectionGrid tr.t-state-selected").removeClass("t-state-selected");
            row.addClass("t-state-selected");
            var scroll = $(row).position().top + $(row).closest(".t-grid-content").scrollTop() - 24;
            $(row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            Schedule.loadCalendarAndActivities(patientId, null);
        }
    },
    DeleteAction: function() {
        var data = $("#DeleteMultipleActivityGrid .t-grid-content tr input:checked").serializeArray();
        if (!data.length) {
            U.Growl("You must select at least one task to delete.", 'error');
        }
        else {
            if (confirm("Are you sure you want to delete these event(s)?")) {
                data.push({ name: "PatientId", value: Schedule.GetId() });
                data.push({ name: "EpisodeId", value: Schedule.GetEpisodeId() });
                U.PostUrl("Schedule/DeleteMultiple", data,
                    function(data) {
                        if (data.isSuccessful) {
                            U.Growl(data.errorMessage, 'success');
                            Agency.RebindMissedVisitList();
                            Agency.RebindOrders();
                            Patient.Rebind();
                            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                            if (scheduleActivityGrid != null) {
                                scheduleActivityGrid.rebind({ episodeId: Schedule.GetEpisodeId(), patientId: Schedule.GetId(), discipline: Schedule._Discipline });
                            }
                            Schedule.loadCalendarNavigation(Schedule.GetEpisodeId(), Schedule.GetId());
                            UserInterface.CloseWindow('scheduledelete');
                        } else {
                            U.Growl(data.errorMessage, 'error');
                        }
                    }, null);
            }
        }

    },
    DeleteActivityRowSelected: function(e) {
        var checkbox = $(e.row).find("input");
        checkbox.prop("checked", !checkbox.prop("checked"));
    }
}

