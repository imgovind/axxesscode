﻿var Patient = {
    _patientId: "",
    _patientStatus: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    _dateFilter: "",
    _showFilter: "",
    _isRebind: false,
    _fromDate: "",
    _toDate: "",
    _IsPatientCenterFirstLoad: true,
    GetId: function() {
        return Patient._patientId;
    },
    SetId: function(patientId) {
        Patient._patientId = patientId;
    },
    SetPatientRowIndex: function(patientRowIndex) {
        Patient._patientRowIndex = patientRowIndex;
    },
    GetEpisodeId: function() {
        return Patient._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        Patient._EpisodeId = EpisodeId;
    },
    InitNew: function() {
        var $triage = $('input.Triage');
        $triage.click(function() {
            $triage.removeAttr('checked');
            $(this).attr('checked', true);
        });
        U.PhoneAutoTab("New_Patient_HomePhone");
        U.PhoneAutoTab("New_Patient_MobilePhone");
        U.PhoneAutoTab("New_Patient_PharmacyPhone");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhoneAlternate");
        U.ShowIfChecked($("#New_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        U.ShowIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));

        var form = $("#newPatientForm");
        U.InitValidation(form, function(result) { Patient.RebindPatientGrids(result); UserInterface.ShowPatientPrompt(); });
        $("#window_newpatient .Physicians").PhysicianInput();
    },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Patient_HomePhone");
        U.PhoneAutoTab("Edit_Patient_MobilePhone");
        U.PhoneAutoTab("Edit_Patient_PharmacyPhone");
        $("#window_editpatient .Physicians").PhysicianInput();
        $("#EditPatient_NewPhysician").click(function() {
            var PhysId = $("#EditPatient_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Patient_Id").val());
            else U.Growl("Please Select a Physician", "error");
        });
        var form = $("#editPatientForm");
        U.InitValidation(form, function(result) { Patient.RebindPatientGrids(result); })
    },
    Verify: function(medicareNumber, lastName, firstName, dob, gender) {
        var input = "medicareNumber=" + medicareNumber + "&lastName=" + lastName + "&firstName=" + firstName + "&dob=" + dob + "&gender=" + gender;
        U.PostUrl("/Patient/Verify", input, function(result) {
            alert(result);
        });
    },
    Filter: function(text, gridContent) {
        if (gridContent != undefined) {
            if (text != undefined && text.length > 0) {
                text = $.trim(text).toUpperCase();
                var search = text.split(" ");
                if (search != undefined && search.length > 0) {
                    var gridTr = $("tbody  tr", gridContent);
                    if (gridTr != undefined && gridTr.length > 0) {
                        gridTr.removeClass("match").hide();
                        var matchedTrs = gridTr.filter(function(index) {
                            var tdL = $(this).find("td.searchL").text();
                            var tdF = $(this).find("td.searchF").text();
                            var result = $.grep(search, function(s, i) {
                                return (tdL.indexOf(s) > -1 || tdF.indexOf(s) > -1);
                            })
                            return result.length > 0;
                        });
                        matchedTrs.removeClass("t-alt").addClass("match").show().filter(":even").addClass("t-alt");
                    }
                }
            }
        }
    },
    InitCenter: function() {
        var container = $("#window_patientcenter_content");
        if (container != undefined) {
            $(".top input[name=SearchText]", container).keyup(function() {
                var girdContent = $(container).find("#PatientSelectionGrid div.t-grid-content");
                if ($(this).val() != undefined && $(this).val().length) {
                    Patient.Filter($(this).val(), girdContent);
                    if ($("tr.match", girdContent).length) {
                        $("tr.match:first", girdContent).click();
                    }
                    else {
                        $("#PatientMainResult", container).html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3" +
                    "Cdiv class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javas" +
                    "cript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
                    }
                }
                else {
                    $("tr", girdContent).removeClass("match t-alt").show();
                    $("tr:even", girdContent).addClass("t-alt");
                    girdContent.find("tr:first").click();
                }
            });
            $(".top select", container).change(function() {
                Patient.RebindPatientsSelection(Patient.RebindInput());
            });
            $('.t-grid-content', container).css({ height: 'auto' });
            $('.layout', container).layout({ west__paneSelector: '.layout-left' });
        }
    },
    InitChangePatientStatus: function() { U.InitForm($("#changePatientStatusForm"), function(data) { UserInterface.CloseModal(); Patient.RebindPatientGrids(data); }); },
    InitDischargePatient: function() { U.InitTemplate($("#dischargePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is discharged successfully."); },
    InitActivatePatient: function() { U.InitTemplate($("#activatePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is activated successfully."); },
    InitNewPhoto: function(patientId) { U.InitValidation($("#changePatientPhotoForm"), function(result) { UserInterface.CloseModal(); Patient.LoadInfoAndActivity(result.PatientId); }); },
    RemovePhoto: function(patientId) {
        if (confirm("Are you sure you want to remove this photo?")) {
            U.PostUrl("/Patient/RemovePhoto", "patientId=" + patientId, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    U.Growl("Patient photo has been removed successfully.", "success");
                    Patient.LoadInfoAndActivity(patientId);
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    RebindInput: function() { return $("#window_patientcenter_content .layout-left .top .filterInput").serializeArray(); },
    RebindPatientsSelection: function(input) {
        var grid = $('#PatientSelectionGrid');
        if (grid != undefined) {
            var patientSelectionGridData = grid.data('tGrid');
            if (patientSelectionGridData != undefined) {
                $.ajax({
                    url: 'Patient/All',
                    type: "POST",
                    data: input,
                    dataType: 'json',
                    success: function(dataResult) {
                        // Patient.SetId(patientId);
                        patientSelectionGridData.dataBind(dataResult);
                    }
                });
            }
        }
    },
    PatientListFirstDataBound: function(Id, gridId) {
        var selectedTr = $('td:contains(' + Id + ')', $('#' + gridId)).closest('tr');
        if (selectedTr != undefined && selectedTr != null) {
            selectedTr.addClass("t-state-selected");
            var gridContent = selectedTr.closest(".t-grid-content");
            if (gridContent != undefined && gridContent != null) {
                var position = selectedTr.position();
                if (position != undefined) {
                    var scroll = position.top + gridContent.scrollTop() - 24;
                    if (scroll != undefined && scroll > 24) {
                        gridContent.animate({ scrollTop: scroll }, 'slow');
                    }
                }
            }
        }
        Patient.SetId(Id);
    },
    SelectByPatientIdOrFirst: function(patientId, grid) {
        var trs = $('.t-grid-content tr', grid)
        if (trs != undefined && trs.length > 0) {
            if (patientId != "" && U.IsGuid(patientId)) {
                var selectedTr = $('td:contains(' + patientId + ')', trs).closest('tr');
                if (selectedTr != undefined) { selectedTr.click(); }
                else {
                    var firstTr = trs.eq(0);
                    if (firstTr != undefined && firstTr.length > 0) { firstTr.click(); }
                }
            }
            else {
                var firstTr = trs.eq(0);
                if (firstTr != undefined && firstTr.length > 0) { firstTr.click(); }
            }
        }
    },
    OnPatientDataBinding: function(e) { U.OnDataBindingStopAction(e, Patient.RebindInput(), "/Patient/AllSort"); },
    PatientListDataBound: function() {
        var container = $("#window_patientcenter_content");
        if (container != undefined) {
            var leftContainer = $(".layout-left", container);
            if (leftContainer != undefined) {
                var filterBoxText = $(".top input[name=SearchText]", leftContainer).val();
                var gridContent = $(".bottom #PatientSelectionGrid .t-grid-content", leftContainer);
                if (gridContent != undefined) {
                    var gridContentTrs = $("tr", gridContent);
                    if (gridContentTrs != undefined && gridContentTrs.length > 0) {
                        if (Patient._patientId == "") {
                            if (filterBoxText.length) { gridContentTrs.filter(".match").eq(0).click() }
                            else { gridContentTrs.eq(0).click(); }
                        }
                        else {
                            var tdWithPatientId = $('td:contains(' + Patient._patientId + ')', gridContentTrs);
                            if (tdWithPatientId != undefined && tdWithPatientId.length > 0) { tdWithPatientId.eq(0).closest('tr').click(); }
                            else { gridContentTrs.eq(0).click(); }
                        }
                        var selectedRows = gridContentTrs.filter(".t-state-selected");
                        if (selectedRows != undefined && selectedRows.length > 0) {
                            selectedRows.eq(0).closest('.t-grid-content').scrollTop(selectedRows.eq(0).position().top - 50);
                        }
                    }
                }
                else {
                    $("#PatientMainResult", container).removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
                }
            }
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            if (patientId != undefined && patientId != null && patientId != "" && U.IsGuid(patientId)) {
                Patient.LoadInfoAndActivity(patientId);
            }
        }
    },
    RebindScheduleInput: function() { return $("#window_patientcenter_content .layout-main .bottom .activityFilterInputContainer").find(".activityFilterInput").serializeArray(); },
    RebindScheduleActivity: function(isHideShow) {
        var patientActivityGrid = $('#PatientActivityGrid');
        if (patientActivityGrid != undefined) {
            var patientActivityGridData = patientActivityGrid.data('tGrid');
            if (patientActivityGridData != undefined && patientActivityGridData != null) {
                $.ajax({
                    url: 'Patient/Activity',
                    type: "POST",
                    data: Patient.RebindScheduleInput(),
                    dataType: 'json',
                    beforeSend: function(jqXHR, settings) { patientActivityGrid.addClass("loading"); },
                    success: function(dataResult) {
                    patientActivityGridData.dataBind(dataResult.ScheduleEvents);
                    patientActivityGrid.removeClass("loading");
                        // U.ClientBindScheduleActivityInit('PatientActivityGrid');
                        if (isHideShow) {
                            $("#date-range-text").empty().append(dataResult.Range);
                            if ($("select.patient-activity-date-drop-down").val() == "DateRange") {
                                $("#date-range-text").hide();
                                $("div.custom-date-range").show();
                            } else if ($("select.patient-activity-date-drop-down").val() == "All") {
                                $("#date-range-text").hide();
                                $("div.custom-date-range").hide();
                            } else {
                                $("#date-range-text").show();
                                $("div.custom-date-range").hide();
                            }
                        }
                    }
                });
            }
        }
    },
    ActivityOnBound: function(e) { U.ClientBindScheduleActivityInit('PatientActivityGrid'); },
    ActivityOnDataBinding: function(e) { U.OnDataBindingStopAction(e, Patient.RebindScheduleInput(), "/Patient/ActivitySort"); },
    LoadInfoAndActivity: function(patientId) {
        $("#patient-info").remove();
        $('#PatientMainResult').empty().addClass('loading').load('Patient/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#PatientMainResult').removeClass('loading');
            Patient.SetId(patientId);
            if (textStatus == 'error') $('#PatientMainResult').html(U.AjaxError);
            $('#window_patientcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    LoadEditPatient: function(id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() {
            Patient.InitEdit();
            Lookup.LoadAdmissionSources();
            Lookup.LoadRaces();
            Lookup.LoadUsers();
            Lookup.LoadReferralSources();
        }, { patientId: id });
    },
    LoadEditAuthorization: function(patientId, id) {
        Acore.Open("editauthorization", 'Authorization/Edit', function() {
            Patient.InitEditAuthorization(patientId);
        }, { patientId: patientId, Id: id });
    },
    InitNewOrder: function() {
        Template.OnChangeInit();
        $("#New_Order_PhysicianDropDown").PhysicianInput();
        U.InitValidation($("#newOrderForm"), function(result) { UserInterface.CloseAndRefreshActivity('neworder', result.PatientId, result.EpisodeId); Agency.RebindOrders(); });
    },
    SubmitOrder: function(control, page) {
        var form = control.closest("form");
        U.InitInlineValidation(form, function(result) {
            if (control.html() == "Save &amp; Close") { UserInterface.CloseWindow(page); }
            else if (control.html() == "Complete") { UserInterface.CloseWindow(page); }
            UserInterface.RefreshCenterQAMySchedule(result);
            UserInterface.RefreshOrders(result);
        });
    },
    InitNewFaceToFaceEncounter: function() { U.InitValidation($("#newFaceToFaceEncounterForm"), function(result) { UserInterface.CloseWindow('newfacetofaceencounter'); UserInterface.RefreshCenterQAMySchedule(result); if (result.IsOrdersToBeSentRefresh) { Agency.RebindOrdersToBeSent(); } }); },
    InitNewCommunicationNote: function() {
        Template.OnChangeInit();
        var form = $("#newCommunicationNoteForm");
        U.InitPreValidation(form);
        U.InitValidation(form, function(result) {
            UserInterface.CloseWindow('newcommnote');
            UserInterface.RefreshCenterQAMySchedule(result);
            UserInterface.RefreshCommunicationNotes(result);
            U.UnBlockButton($(form).find(".blocked-button"));
        });
    },
    InitNewAuthorization: function() {
        var form = $("#newAuthorizationForm");
        U.InitPreValidation(form);
        U.InitValidation(form, function(result) { var patientId = $("#New_Authorization_PatientId").val(); UserInterface.CloseWindow('newauthorization'); Patient.RebindAuthorizationGrid(patientId); });
    },
    InitEditAuthorization: function(patientId) {
        var form = $("#editAuthorizationForm");
        U.InitPreValidation(form);
        U.InitValidation(form, function(result) { UserInterface.CloseWindow('editauthorization'); Patient.RebindAuthorizationGrid(patientId); });
    },
    InitEditOrder: function() { Template.OnChangeInit(); $("#Edit_Order_PhysicianDropDown").PhysicianInput(); },
    LoadNewOrder: function(patientId) { Acore.Open("neworder", 'Order/New', function() { Patient.InitNewOrder(); }, { patientId: patientId }); },
    DeleteOrder: function(id, patientId, episodeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteOrder", { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.RebindOrdersHistory(patientId);
                    var grid = $('#List_EpisodeOrders').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId, episodeId: episodeId }); }

                    if ($("#Billing_CenterContent").val() != null && $("#Billing_CenterContent").val() != undefined) {
                        Billing.ReLoadUnProcessedClaim('#Billing_CenterContentFinal', $('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val(), 'FinalGrid', 'Final');
                    }

                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    LoadNewCommunicationNote: function(patientId) { Acore.Open("newcommnote", 'CommunicationNote/New', function() { Patient.InitNewCommunicationNote(); }, { patientId: patientId }); },
    LoadNewAuthorization: function(patientId) {
        Acore.Open("newauthorization", 'Authorization/New', function() { Patient.InitNewAuthorization(); }, { patientId: patientId });
    },
    LoadNewInfectionReport: function(patientId) {
        Acore.Open("newinfectionreport", 'Infection/New', function() { InfectionReport.InitNew(); }, { patientId: patientId });
    },
    LoadNewIncidentReport: function(patientId) {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); }, { patientId: patientId });
    },
    InfoPopup: function(e) {
        var $dialog = $("#patient-info");
        $dialog.dialog({
            width: 500,
            position: [$(e).offset().left, $(e).offset().top],
            modal: false,
            resizable: false,
            close: function() {
                $dialog.dialog("destroy");
                $dialog.hide();
            }
        }).show();
        $('#ui-dialog-title-patientInfo').html('Patient Info');
    },
    AddPhysRow: function(el) {
        $(el).closest('.column').find('.row.hidden').first().removeClass('hidden').find("input:first").blur()
    },
    RemPhysRow: function(el) {
        $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000')
    },
    InitMedicationProfileSnapshot: function(assessmentType) {
        U.PhoneAutoTab("MedProfile_PharmacyPhone");
        $("#window_medicationprofilesnapshot .Physicians").PhysicianInput();
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        if (assessmentType != undefined) {
                            var assessmentId = $(assessmentType + "_Id").val();
                            if (assessmentId != undefined && assessmentId != "") { values.push({ name: "AssociatedAssessment", value: assessmentId }); }
                        }
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadMedicationProfileSnapshot: function(patientId, assessmentType) {
        Acore.Open("medicationprofilesnapshot", 'Patient/MedicationProfileSnapShot', function() { Patient.InitMedicationProfileSnapshot(assessmentType); }, { patientId: patientId });
    },
    LoadMedicationProfile: function(patientId) {
        Acore.Open("medicationprofile", 'Patient/MedicationProfile', function() { }, { patientId: patientId });
    },
    LoadMedicationProfileSnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", 'Patient/MedicationProfileSnapShotHistory', function() { }, { patientId: patientId });
    },
    LoadPatientCommunicationNotes: function(patientId) {
        Acore.Open("patientcommunicationnoteslist", 'Patient/PatientCommunicationNotesView', function() { }, { patientId: patientId });
    },
    RebindCommunicationNotes: function(patientId) {
        var communicationNotes = $('#List_PatientCommunicationNote').data('tGrid');
        if (communicationNotes != null && communicationNotes != undefined) { communicationNotes.rebind({ patientId: patientId }); }
    },
    OnMedicationProfileEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 7) {
                    if (e.dataItem['MedicationCategory'] != 'DC') {
                        $(this).html('');
                    }
                }
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 6) {
                    if (e.dataItem['Classification'] == null || e.dataItem['Classification'] == '') {
                        $(this).find("input[name=MedicationClassification]").val('');
                    }
                }
            });
        }
    },
    OnPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
        }
    },
    OnMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        }
        else {
            e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" href="javascript:void(0);" onclick="' + dataItem.DischargeUrl + '">D/C</a>';
        }

        if (dataItem.DrugId != null && dataItem.DrugId.length > 0) {
            var medline = 'http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c=' + dataItem.DrugId;
            e.row.cells[2].innerHTML = '<a href="' + medline + '" target="_blank">' + dataItem.MedicationDosage + '</a>';
        }

        e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    OnMedicationProfileSnapShotRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined) {
            e.row.cells[1].innerHTML = dataItem.StartDateSortable;
        }
    },
    OnMedProfileSnapShotHistoryRowDataBound: function(e) {
        var dataItem = e.dataItem;
        e.row.cells[1].innerHTML = dataItem.SignedDateFormatted;
    },
    OnDataBound: function(e) {
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        if (medicatonProfileGridDC != null && medicatonProfileGridDC != undefined) {
            var id = $("#medicationProfileHistroyId").val();
            medicatonProfileGridDC.rebind({ medId: id, medicationCategry: "DC", assessmentType: "" });
        }
    },
    DischargeMed: function(medId, patientId, assessmentType) {
        $("#Discharge_Medication_Container").load("/Patient/MedicationDischarge", function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl("Could not load the Medication Discharge page.", "error"); }
            else if (textStatus == "success") {
                U.ShowDialog("#Discharge_Medication_Container", function() {
                    if (assessmentType != undefined || assessmentType != null && assessmentType.length > 0) { Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshOasisMedProfile(medId, assessmentType); }); }
                    else { Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshMedProfile(medId); }); }
                });
            }
        });
    },
    InitDischargeMedication: function(medId, patientId, callback) {
        $("#dischargeMedicationProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    data: { medId: medId, patientId: patientId },
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            UserInterface.CloseModal();
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(result.errorMessage, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RefreshMedProfile: function(medId) {
        var medicatonProfileGridActive = $('#MedicatonProfileGridActive').data('tGrid');
        if (medicatonProfileGridActive != null && medicatonProfileGridActive != undefined) {
            medicatonProfileGridActive.rebind({ medId: medId, medicationCategry: "Active", assessmentType: "" });
        }
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        if (medicatonProfileGridDC != null && medicatonProfileGridDC != undefined) {
            medicatonProfileGridDC.rebind({ medId: medId, medicationCategry: "DC", assessmentType: "" });
        }
    },
    RefreshOasisMedProfile: function(medId, assessmentType) {
        var oasisMedGrid = $('#Oasis' + assessmentType + 'MedicationGrid').data('tGrid');
        if (oasisMedGrid != null && oasisMedGrid != undefined) {
            oasisMedGrid.rebind({ medId: medId, medicationCategry: "Active", assessmentType: assessmentType });
        }
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?")) {
            U.PostUrl("/Patient/Delete", { patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.RebindPatientGrids(result);
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    LoadDeletedPatientListContent: function() {
        U.RebindDataGridContent('List_Patient_Deleted', 'Patient/DeletedPatientContent', { BranchId: $('#List_Patient_Deleted_BranchId').val() }, 'DisplayName-ASC');
    },
    InitAdmit: function(patientId) {
        U.InitValidation($("#newAdmitPatientForm"), function(data) {
            UserInterface.CloseWindow('admitpatient');
            UserInterface.CloseModal();
            Patient.RebindPatientGrids(data);
            UserInterface.ShowPatientPrompt();
        });
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").datepicker("hide");
        U.InitValidation($("#newNonAdmitPatientForm"), function(data) {
            UserInterface.CloseWindow('nonadmitpatient');
            UserInterface.CloseModal();
            Patient.RebindPatientGrids(data);
        });
    },
    LoadEditCommunicationNote: function(patientId, id) {
        Acore.Open("editcommunicationnote", 'Patient/EditCommunicationNote', function() {
            U.InitValidation($("#editCommunicationNoteForm"), function(data) {
                UserInterface.CloseWindow('editcommunicationnote');
                UserInterface.RefreshCenterQAMySchedule(data);
                UserInterface.RefreshCommunicationNotes(data);
            });
        }, { patientId: patientId, Id: id });
    },
    LoadEditEmergencyContact: function(patientId, id) {
        Acore.Open("editemergencycontact", 'Patient/EditEmergencyContactContent', function() {
            U.PhoneAutoTab("Edit_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("Edit_EmergencyContact_AlternatePhoneArray");
            U.InitValidation($("#editEmergencyContactForm"), function() { UserInterface.CloseWindow('editemergencycontact'); Patient.RebindEmergencyContact(patientId); }, function() { });
        }, { patientId: patientId, Id: id });
    },
    LoadNewEmergencyContact: function(patientId) {
        Acore.Open("newemergencycontact", 'Patient/NewEmergencyContactContent', function() {
            U.PhoneAutoTab("New_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("New_EmergencyContact_AlternatePhoneArray");
            U.InitValidation($("#newEmergencyContactForm"), function() { UserInterface.CloseWindow('newemergencycontact'); Patient.RebindEmergencyContact(patientId); }, function() { });
        }, { patientId: patientId });
    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            U.PostUrl("/Patient/DeleteEmergencyContact", { id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) { Patient.RebindEmergencyContact(patientId); }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    RebindEmergencyContact: function(patientId) {
        var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
        if (emergencyContact != null && emergencyContact != undefined) { emergencyContact.rebind({ PatientId: patientId }); }
    },
    DeleteCommunicationNote: function(Id, patientId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteCommunicationNote", { Id: Id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.RebindCommunicationNotes(patientId);
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    AddPhysician: function(id, patientId) {
        U.TGridAjax("/Patient/AddPatientPhysicain", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
        $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, patientId) {
        if (confirm("Are you sure you want to delete this physician?")) U.TGridAjax("/Patient/DeletePhysicianContact", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    DeleteAuthorization: function(patientId, id) {
        if (confirm("Are you sure you want to delete this authorization?")) {
            U.PostUrl("/Patient/DeleteAuthorization", { Id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    Patient.RebindAuthorizationGrid(patientId);
                    U.Growl(result.errorMessage, "success");
                }
                else { U.Growl(result.errorMessage, "error"); }
            });

        }

    },
    SetPrimaryPhysician: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary?")) {
            U.PostUrl("/Physician/SetPrimary", { "id": id, "patientId": patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    U.RebindTGrid($('#EditPatient_PhysicianGrid'), { PatientId: result.PatientId });
                    Patient.LoadInfoAndActivity(result.PatientId);
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    OnSocChange: function() {
        var date = $("#New_Patient_StartOfCareDate").datepicker("getDate");
        $("#New_Patient_EpisodeStartDate").datepicker("setDate", date)
        $("#New_Patient_EpisodeStartDate").datepicker("option", "mindate", date);
    },
    UpdateOrderStatus: function(eventId, patientId, episodeId, orderType, actionType) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        U.Growl(data.errorMessage, "success");
                        UserInterface.CloseModal();
                        UserInterface.RefreshCenterQAMySchedule(data);
                        if (data.IsPhysicianOrderPOCRefresh && Patient._patientId == data.PatientId) { Patient.RebindOrdersHistory(data.PatientId); }

                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    U.Growl(data.errorMessage, "success");
                    UserInterface.CloseModal();
                    UserInterface.RefreshCenterQAMySchedule(data);
                    if (data.IsOrdersToBeSentRefresh) { Agency.RebindOrdersToBeSent(); }
                    if (data.IsPhysicianOrderPOCRefresh && Patient._patientId == data.PatientId) { Patient.RebindOrdersHistory(data.PatientId); }
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    ProcessCommunicationNote: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        UserInterface.CloseModal();
                        UserInterface.RefreshCenterQAMySchedule(result);
                        UserInterface.RefreshCommunicationNotes(result);
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    UserInterface.CloseModal();
                    UserInterface.RefreshCenterQAMySchedule(result);
                    UserInterface.RefreshCommunicationNotes(result);
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    RebindOrdersHistory: function(patientId) {
        var grid = $('#List_PatientOrdersHistory').data('tGrid');
        if (grid != null && grid != undefined) {
            grid.rebind({ patientId: patientId, StartDate: $("#PatientOrdersHistory_StartDate-input").val(), EndDate: $("#PatientOrdersHistory_EndDate-input").val() });
            var $exportLink = $('#PatientOrdersHistory_ExportLink');
            if ($exportLink != undefined) {
                var href = $exportLink.attr('href');
                href = href.replace(/patientId=([^&]*)/, 'patientId=' + patientId);
                href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientOrdersHistory_StartDate-input").val());
                href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientOrdersHistory_EndDate-input").val());
                $exportLink.attr('href', href);
            }
        }
    },
    LoadPatientAdmissionPeriods: function(Id) { Acore.Open("patientmanageddates", 'Patient/AdmissionPeriod', function() { }, { patientId: Id }); },
    LoadInsuranceContent: function(contentId, patientId, insuranceId, action, insuranceType) { $(contentId).load('Patient/InsuranceInfoContent', { PatientId: patientId, InsuranceId: insuranceId, Action: action, InsuranceType: insuranceType }, function(responseText, textStatus, XMLHttpRequest) { }); },
    PatientListInput: function(SortParams) {
        var inputs = [];
        inputs[inputs.length] = { name: "BranchId", value: $('#PatientList_BranchId').val() };
        inputs[inputs.length] = { name: "Status", value: $('#PatientList_Status').val() };
        inputs[inputs.length] = { name: "SortParams", value: SortParams != undefined ? SortParams : 'DisplayName-ASC' };
        return inputs;
    },
    LoadPatientListContent: function(input) { U.RebindDataGridContent('PatientList', 'Patient/ListContent', input); },
    LoadPatientAdmissionContent: function(contentId, patientId) {
        $(contentId).empty().addClass("loading").load('Patient/AdmissionPeriodContent', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl("Could not load this page. Try again.", "error"); }
            else if (textStatus == "success") { $(contentId).removeClass("loading"); }
        });
    },
    DeletePatientAdmission: function(patientId, Id) {
        if (confirm("Are you sure you want to delete this admission period?")) {
            U.PostUrl("/Patient/DeletePatientAdmission", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    InitAdmissionPatient: function(patientId, type) {
        U.PhoneAutoTab(type + "_Admission_HomePhone");
        U.PhoneAutoTab(type + "_Admission_MobilePhone");
        U.PhoneAutoTab(type + "_Admission_PharmacyPhone");
        $("#window_" + type.toLowerCase() + "patientadmission .Physicians").PhysicianInput();
        U.InitValidation($("#" + type.toLowerCase() + "PatientAdmissionForm"), function(data) {
            Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
            UserInterface.CloseWindow(type.toLowerCase() + 'patientadmission');
        });
    },
    MarkPatientAdmissionCurrent: function(patientId, Id) {
        if (confirm("Are you sure you want to mark this admission active? This will update this admission with the current patient information.")) {
            U.PostUrl("/Patient/MarkPatientAdmissionCurrent", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    RestoreDeleted: function(patientId) {
        if (confirm("Are you sure you want to restore this patient?")) {
            U.PostUrl("/Patient/RestoreDeleted", { PatientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.RebindPatientGrids(result);
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    RebindPatientGrids: function(data) {
        if (data.IsCenterRefresh) {
            Patient.RebindPatientsSelection(Patient.RebindInput());
            Schedule.RebindPatientsSelection(Schedule.RebindInput());
        }
        if (data.IsPatientListRefresh) { Patient.LoadPatientListContent(Patient.PatientListInput()); }
        if (data.IsNonAdmitPatientListRefresh) { U.RebindTGrid($('#List_Patient_NonAdmit_Grid')); }
        if (data.IsPendingPatientListRefresh) { U.RebindTGrid($('#List_PatientPending_Grid')); }
        if (data.IsReferralListRefresh) { Referral.RebindList(); }
        if (data.IsDeletedPatientListRefresh) { Patient.LoadDeletedPatientListContent(); }
    },
    RebindAuthorizationGrid: function(patientId) {
        var authorizationGrid = $('#List_Authorizations').data('tGrid');
        if (authorizationGrid != null) {
            var selectUrl = authorizationGrid.ajax.selectUrl;
            var match = $('#List_Authorizations').data('tGrid').ajax.selectUrl.match("patientId=(.*)");
            if (match.length > 0 && match[1] == patientId) {
                authorizationGrid.rebind({ patientId: patientId });
            }
        }
    }
}
