﻿var PlanOfCare = {
    InitNew: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#New_485_PhysicianId").PhysicianInput();
    },
    Submit: function(control, page) {
        var form = control.closest("form");
        U.InitInlineValidation(form, function(result) {
            if (control.html() == "Save &amp; Close") { UserInterface.CloseWindow(page); }
            else if (control.html() == "Complete") { UserInterface.CloseWindow(page); }
            UserInterface.RefreshCenterQAMySchedule(result);
            UserInterface.RefreshOrders(result);
        });
    },
    InitEdit: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#Edit_485_PhysicianId").PhysicianInput();
        U.InitEditTemplate("485");
    },
    SaveMedications: function(patientId) {
        U.PostUrl("/Patient/LastestMedications", { patientId: patientId }, function(data) {
            if ($("#PlanofCareMedicationIsNew").val() == "true") $("#New_485_Medications").val(data);
            else $("#Edit_485_Medications").val(data);
            UserInterface.CloseModal();
        });
    },
    GetAlleries: function(patientId) {
        U.PostUrl("/Patient/LastestAllergies", { patientId: patientId }, function(data) {
            $("#485AllergiesDescription").val(data);
        });
    }
}