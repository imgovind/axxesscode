﻿var Supply = {
    Delete: function(id) { U.DeleteTemplate("Supply", id); },
    InitEdit: function() {
        U.InitEditTemplate("Supply");
    },
    InitNew: function() {
        U.InitNewTemplate("Supply");
    },
    RebindList: function() { U.RebindTGrid($('#List_Supply')); },
    OnSupplyEdit: function(e) {
        if (e != null && e != undefined && e.mode == 'edit') {
            $(e.form).find("input[name=RevenueCode]").val(e.dataItem.RevenueCode);
            $(e.form).find("input[name=Code]").val(e.dataItem.Code);
        }
        $(e.form).find("input[name=Description]").AjaxAutocomplete({
            ExtraParameters: { 'limit': 50 },
            minLength: 1,
            SourceUrl: "Agency/SuppliesSearch",
            Format: function(json) { return json.Description },
            Select: function(json, input) {
                input.val(json.Description);
                $(e.form).find("input[name=Code]").val(json.Code);
                $(e.form).find("input[name=UnitCost]").val(json.UnitCost);
                $(e.form).find("input[name=UniqueIdentifier]").val(json.Id);
                $(e.form).find("input[name=RevenueCode]").val(json.RevenueCode);
            }
        });
    },
    RowSelected: function(e) {
        $("[type=checkbox]", e.row).click()
    }
}