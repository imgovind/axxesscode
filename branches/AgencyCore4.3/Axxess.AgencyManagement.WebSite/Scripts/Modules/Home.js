﻿var Home = {
    Init: function() {
        Message.InitWidget();

        if (Acore.GetRemoteContent) {
            U.PostUrl("/Message/CustomWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(message) {
                if (message != undefined && message.Text != null && message.Text.length > 0) {
                    $('#custom-message-widget').html(message.Text);
                }
                else {
                    $('#custom-message-widget').html("<div class='align-center'><span class='bigtext'>Welcome to</span><br /><span class='img acorelogo'></span></div><div>AgencyCore&#8482; is a secure Enterprise Agency Management software designed from the ground up to provide powerful online capabilities that enables real-time collaboration for you and your team.</div>");
                }
            });
        }

        U.PostUrl("/Report/PatientBirthdayWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Data != undefined) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#birthdayWidgetContent').append("<tr><td>" + data.Data[i].BirthDay + "</td><td>" + data.Data[i].Age + "</td><td><a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientChart('" + data.Data[i].Id + "', '" + data.Data[i].IsDischarged + "');\">" + data.Data[i].Name + "</a></td><td>" + data.Data[i].PhoneHomeFormatted + "</td></tr>");
            } else {
                $('#birthdayWidgetContent').append("<tr><td colspan='5' class='align-center'>No Messages found.</td></tr>");
            }
        });

        U.PostUrl("/User/ScheduleWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined) {
                for (var i = 0; i < data.length && i < 5; i++) $('#scheduleWidgetContent').append("<tr><td><a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientChart('" + data[i].PatientId + "', '" + data[i].IsDischarged + "');\">" + data[i].PatientName + "</a></td><td>" + data[i].TaskName + "</td><td>" + data[i].EventDate + "</td></tr>");
            }
            else {
                $('#scheduleWidgetContent').append("<tr><td colspan='5' class='align-center'><h1>No Scheduled Tasks found.</h1></td></tr>");
                $('#userScheduleWidgetMore').hide();
            }
        });

        U.PostUrl("/Agency/RecertsPastDueWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertPastDueWidgetContent').append("<tr><td><a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', 'false');\">" + data[i].PatientName + "</a></td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertPastDueWidgetContent').append("<tr><td colspan='3' class='align-center'><h1>No Past Due Recertifications found.</h1></td></tr>");
                $('#pastDueRecertsMore').hide();
            }
        });

        U.PostUrl("/Agency/RecertsUpcomingWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertUpcomingWidgetContent').append("<tr><td><a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', 'false');\">" + data[i].PatientName + "</a></td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertUpcomingWidgetContent').append("<tr><td colspan='3' class='align-center'><h1>No Upcoming Recertifications found.</h1></td></tr>");
                $('#upcomingRecertsMore').hide();
            }
        });

        U.PostUrl("/Billing/Unprocessed", "", function(data) {
        if (data != undefined && data.length > 0) for (var i = 0; i < data.length && i < 5; i++) $('#claimsWidgetContent').append("<tr><td><a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', '" + data[i].IsDischarged + "');\">" + data[i].PatientDisplayName + "</a></td><td>" + data[i].Type + "</td><td>" + data[i].EpisodeRange + "</td></tr>");
            else $('#claimsWidgetContent').append("<tr><td colspan='3' class='align-center'><h1>No Bills found.</h1></td></tr>").closest('widget').find('#claimsWidgetMore').hide();
        });
    }
}