﻿var V = {
    Init: function() { Template.OnChangeInit(); },
    HandleSubmit: function(page, form, control, episodeId, patientId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl("The note has been saved.", "success");
                        if (result.IsDataCentersRefresh) {
                            UserInterface.RefreshActivity(patientId, episodeId);
                        }
                    } else if (control.html() == "Complete") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been saved and completed.", "success");
                    } else if (control.html() == "Approve") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been approved.", "success");
                    } else if (control.html() == "Return") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been returned.", "success");
                    }
                } else U.Growl(result.errorMessage, "error");
            },
            error: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl("The note has been saved.", "success");
                        UserInterface.RefreshActivity(patientId, episodeId);
                    } else if (control.html() == "Complete") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been saved and completed.", "success");
                    } else if (control.html() == "Approve") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been approved.", "success");
                    } else if (control.html() == "Return") {
                        if (result.IsDataCentersRefresh) {
                            UserInterface.CloseAndRefreshActivity(page, patientId, episodeId);
                            UserInterface.RefreshCaseManagementMyScheduleTask(result.IsCaseManagementRefresh, result.IsMyScheduleTaskRefresh);
                        }
                        U.Growl("The note has been returned.", "success");
                    }
                }
                else if (result.errorMessage) {
                    U.Growl(result.errorMessage, "error");
                }
                else {
                    U.Growl("The note contains potentially dangerous values. Ensure that special characters such as #,$,<,&...etc are not next to each other.", "error");
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandleWoundSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') U.Growl("Wound care saved successfully.", "success");
                else U.Growl($.trim(result.responseText), "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') U.Growl("Wound care saved successfully.", "success");
                else U.Growl($.trim(result.responseText), "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
}
var snVisit = {
    Init: function(episodeId, patientId, eventId) { V.Init(); },
    Submit: function(control, episodeId, patientId) {
        $('#SNVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) { Acore.Open("snVisit", 'Schedule/NoteView', function() { snVisit.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId }); }
}
var sixtyDaySummary = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#SixtyDaySummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("sixtyDaySummary", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) { Acore.Open("sixtyDaySummary", 'Schedule/NoteView', function() { sixtyDaySummary.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId }); }
}
var dischargeSummary = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#DischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("dischargeSummary", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("dischargeSummary", 'Schedule/NoteView', function() { dischargeSummary.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var PTDischargeSummary = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#PTDischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("PTDischargeSummary", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("PTDischargeSummary", { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var OTDischargeSummary = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#OTDischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("OTDischargeSummary", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("OTDischargeSummary", { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var lvnSupVisit = {
    Submit: function(control, episodeId, patientId) {
        $('#LVNSVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("lvnsVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("lvnsVisit", 'Schedule/LVNSVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transferSummary = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#TransferSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transferSummary", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transferSummary", { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var coordinationofcare = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        $(".Physicians").PhysicianInput();
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#CoordinationOfCare_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("coordinationofcare", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("coordinationofcare", { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var woundCare = {
    Submit: function(control) {
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleWoundSubmit("woundcare", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("woundcare", 'Schedule/WoundCare', function() { V.Init(); Schedule.WoundCareInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaCarePlan = {
    Init: function() {
        var Prefix = "#HHACarePlan_";
        U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
        U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
        U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
        U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
        U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
        U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
        Visit.Shared.Init("HHACarePlan", hhaCarePlan.Init);
    },
    Submit: function(control, episodeId, patientId) {
        $('#HHACarePlan_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhaCarePlan", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhaCarePlan", 'Schedule/HHACarePlan', function() { hhaCarePlan.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaSupVisit = {
    Submit: function(control, episodeId, patientId) {
        $('#HHASVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhasVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhasVisit", 'Schedule/HHASVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transportationLog = {
    Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
        V.Init();
    },
    Submit: function(control, episodeId, patientId) {
        $('#DriverOrTransportationNote_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transportationnote", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transportationnote",  { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snDiabeticDailyVisit = {
    Submit: function(control, episodeId, patientId) {
        $('#SNDiabeticDailyVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snDiabeticDailyVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snDiabeticDailyVisit", 'Schedule/SNDiabeticDailyVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPediatricVisit = {
    Submit: function(control, episodeId, patientId) {
        $('#SNPediatricVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPediatricVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPediatricVisit", 'Schedule/SNPediatricVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPediatricAssessment = {
    Submit: function(control, episodeId, patientId) {
        $('#SNPediatricAssessment_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPediatricAssessment", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPediatricAssessment", 'Schedule/SNPediatricAssessment', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPsychVisit = {
    Submit: function(control, episodeId, patientId) {
        $('#SNPsychVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPsychVisit", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPsychVisit", 'Schedule/SNPsychVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPsychAssessment = {
    Submit: function(control, episodeId, patientId) {
        $('#SNPsychAssessment_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPsychAssessment", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPsychAssessment", 'Schedule/SNPsychAssessment', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snLabs = {
    Submit: function(control, episodeId, patientId) {
        $('#SNLabs_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snLabs", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snLabs", 'Schedule/SNLabs', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var ISOC = {
    Submit: function(control, episodeId, patientId) {
        $('#SNVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("ISOC", control.closest("form"), control, episodeId, patientId);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("ISOC", 'Schedule/InitialSummaryOfCare', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
