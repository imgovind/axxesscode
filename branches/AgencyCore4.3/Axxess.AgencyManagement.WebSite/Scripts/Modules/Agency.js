﻿var Agency = {
    _patientId: "",
    _dateFilter: "",
    _showFilter: "",
    _isRebind: false,
    _fromDate: "",
    _toDate: "",
    Edit: function(Id) { Acore.Open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    InitEdit: function() { U.InitEditTemplate("Agency"); },
    InitEditCost: function() { U.InitTemplate($("#editVisitCostForm"), function() { UserInterface.CloseWindow('visitrates'); }, "Visit rate successfully updated"); },
    CheckSignature: function() {
        if ($("#checkSignature").val().length > 0) {
            U.PostUrl("/Agency/CheckSignature", { signature: $("#checkSignature").val() }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, 'success');
                    Acore.Open("agencyinfo", 'Agency/Info', function() {
                        U.InitTemplate($("#editAgencyForm"), function() { }, "Agency successfully updated");
                    })
                    $("#checkSignature").closest('.window').Close()
                }
                else U.Growl(result.errorMessage, 'error');
            });
        } else U.Growl("Please provide your signature before proceeding", 'error');
    },
    InitNew: function() {
        Lookup.LoadStates();
        U.PhoneAutoTab('Edit_Agency_Phone');
        U.PhoneAutoTab('Edit_Agency_Fax');
        U.PhoneAutoTab('Edit_Agency_SubmitterPhone');
        U.PhoneAutoTab('Edit_Agency_SubmitterFax');
        U.PhoneAutoTab('Edit_Agency_ContactPhone');
        $(".names").alpha({ nocaps: false });

        $("input[name=Edit_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#Edit_Agency_ContactPersonEmail").val($("#Edit_Agency_AdminUsername").val());
                $("#Edit_Agency_ContactPersonFirstName").val($("#Edit_Agency_AdminFirstName").val());
                $("#Edit_Agency_ContactPersonLastName").val($("#Edit_Agency_AdminLastName").val());
            } else $("#Edit_Agency_ContactPersonEmail").add("#Edit_Agency_ContactPersonFirstName").add("#Edit_Agency_ContactPersonLastName").val('');
        });
        $("input[name=Edit_Agency_AxxessBiller]").click(function() {
            if ($(this).is(':checked')) {
                $("#Edit_Agency_SubmitterId").val("SW23071");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                $("#Edit_Agency_SubmitterPhone1").val("214");
                $("#Edit_Agency_SubmitterPhone2").val("575");
                $("#Edit_Agency_SubmitterPhone3").val("7711");
                $("#Edit_Agency_SubmitterFax1").val("214");
                $("#Edit_Agency_SubmitterFax2").val("575");
                $("#Edit_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#Edit_Agency_SubmitterId").add("#Edit_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#Edit_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#Edit_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    InitBillDate: function() { U.InitTemplate($("#newInsuranceBillData"), function() { Agency.RebindBillDataList(); UserInterface.CloseModal(); }, "Insurance rate successfully saved"); },
    InitEditBillDate: function() { U.InitTemplate($("#editInsuranceBillData"), function() { Agency.RebindBillDataList(); UserInterface.CloseModal(); }, "Insurance rate successfully updated"); },
    InitLocationBillDate: function() { U.InitTemplate($("#newLocationBillData"), function() { Agency.RebindLocationBillDataList(); UserInterface.CloseModal(); }, "Insurance rate successfully saved"); },
    InitLocationEditBillDate: function() { U.InitTemplate($("#editLocationBillData"), function() { Agency.RebindLocationBillDataList(); UserInterface.CloseModal(); }, "Insurance rate successfully updated"); },
    DeleteBillData: function(insuranceId, Id) {
        if (confirm("Are you sure you want to delete this visit rate?")) {
            U.PostUrl("/Agency/DeleteBillData", { InsuranceId: insuranceId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Agency.RebindBillDataList();
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    DeleteLocationBillData: function(locationId, Id) {
        if (confirm("Are you sure you want to delete this visit rate?")) {
            U.PostUrl("/Agency/DeleteLocationBillData", { LocationId: locationId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Agency.RebindLocationBillDataList();
                }
                else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    LoadCaseManagement: function(groupName, SortParams) {
        $("#caseManagementContentId").empty().addClass("loading").load('Agency/CaseManagementContent', { groupName: groupName, BranchId: $("#CaseManagement_BranchCode").val(), Status: $("#CaseManagement_Status").val(), StartDate: $("#CaseManagement_StartDate").val(), EndDate: $("#CaseManagement_EndDate").val(), SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl('Case Management List could not be grouped. Please close this window and try again.', 'error');
            }
            else if (textStatus == "success") {
                $("#caseManagementContentId").removeClass("loading");
                $("#caseManagementContentId .t-group-indicator").hide();
                $("#caseManagementContentId .t-grouping-header").remove();
                $(".t-grid-content", "#window_caseManagement").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
                var $exportLink = $('#CaseManagement_ExportLink');
                var href = $exportLink.attr('href');
                if (href != null) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#CaseManagement_BranchCode").val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $("#CaseManagement_Status").val());
                    href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#CaseManagement_StartDate").val());
                    href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#CaseManagement_EndDate").val());
                    $exportLink.attr('href', href);
                }
            }
        });
    },
    ToolTip: function(Id) {
        $("a.tooltip", $(Id)).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            } else $(this).hide();
        });
    },
    LoadPrintQueue: function(groupName, SortParams) {
        var PrintStartDate = $('#Print_StartDate').val();
        var PrintEndDate = $('#Print_EndDate').val();
        var PrintBranchId = $('#Print_BranchCode').val();
        $("#printQueueContentId").empty().addClass("loading").load('Agency/PrintQueueContent', { groupName: groupName, branchId: PrintBranchId, startDate: PrintStartDate, endDate: PrintEndDate, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl('Print Queue could not be grouped. Please close this window and try again.', 'error');
            }
            else if (textStatus == "success") {
            $("#printQueueContentId").removeClass("loading");
                $("#printQueueContentId .t-group-indicator").hide();
                $("#printQueueContentId .t-grouping-header").remove();
                $(".t-grid-content", "#window_printqueue").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
            }
        });
    },
    AfterPrintQueueDataBound: function(total) {
        $("#window_printqueue").Status("Total: " + total);
        
    },
    LoadVisitRate: function() { Acore.Open("visitrates", 'Agency/VisitRates', function() { Agency.InitEditCost(); }); },
    LoadVisitRateContent: function(branchId) {
        $("#Edit_VisitRate_Container").empty().addClass("loading").load('Agency/VisitRateContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Visit rates could not be loaded based on the selected branch. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#Edit_VisitRate_Container").removeClass("loading");
        });
    },
    LoadBranchContent: function(branchId) {
        $("#Edit_AgencyInfo_Container").empty().addClass("loading").load('Agency/InfoContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Branch Information could not be loaded based on the selected branch. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#Edit_AgencyInfo_Container").removeClass("loading");
        });
    },
    PrintQueueExport: function() {
        var PrintStartDate = $('#Print_StartDate').val();
        var PrintEndDate = $('#Print_EndDate').val();
        var PrintBranchId = $('#Print_BranchCode').val();
        U.GetAttachment("/Export/PrintQueueList", {
            BranchId: PrintBranchId,
            StartDate: PrintStartDate,
            EndDate: PrintEndDate
        });
    },
    MarkOrder: function(mark, control) {
        fields = $("select.SendAutomatically, input.OrdersToBeSent:checked", $(control)).serializeArray();
        U.PostUrl("/Agency/MarkOrdersAs" + mark, fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, 'success');
                Patient.RebindPatientsSelection(Patient.RebindInput());
                Schedule.RebindPatientsSelection(Schedule.RebindInput());
                UserInterface.RefreshOrders(data);
            } else U.Growl(data.errorMessage, 'error');
        });
    },
    MarkOrdersAsReturned: function(container) { Agency.MarkOrder("Returned", container); },
    MarkOrdersAsSent: function(container) { Agency.MarkOrder("Sent", container); },
    RebindCaseManagement: function() { Agency.LoadCaseManagement($("#CaseManagement_GroupName").val()); },
    RebindPrintQueue: function() {
        var PrintStartDate = $('#Print_StartDate').val();
        var PrintEndDate = $('#Print_EndDate').val();
        var PrintBranchId = $('#Print_BranchCode').val();
        U.RebindTGrid($('#printQueueGrid'), {
            BranchId: PrintBranchId,
            StartDate: PrintStartDate,
            EndDate: PrintEndDate
        });
    },
    RebindList: function() { U.RebindTGrid($('#List_Agencies')); },
    RebindOrders: function() { Agency.RebindOrdersToBeSent(); Agency.RebindPendingOrders(); Agency.RebindOrdersHistory(); },
    OrderHistoryEditInit: function() { U.InitTemplate($("#updateOrderHistry"), function() { Agency.RebindOrdersHistory(); UserInterface.CloseModal(); }, "Order successfully updated"); },
    PendingSignatureOrdersOnEdit: function(e) {
        var form = e.form;
        var dataItem = e.dataItem;
        if (dataItem != null) {
            dataItem.StartDate = $("#OrdersPendingSignature_StartDate").val();
            dataItem.EndDate = $("#OrdersPendingSignature_EndDate").val();
            dataItem.BranchId = $("#OrdersPendingSignature_BranchId").val();
            e.dateItem = dataItem;
        }
    },
    OrderCenterOnload: function(e) { $('.t-grid-edit', e.row).html('Receive Order'); },
    RebindOrdersHistory: function() { Agency.GenerateClick("OrdersHistory"); },
    RebindPendingOrders: function() { Agency.GenerateClick("OrdersPendingSignature"); },
    RebindOrdersToBeSent: function() { Agency.GenerateClick("OrdersToBeSent"); },
    GenerateClick: function(type) {
        var grid = $("#List_" + type).data('tGrid'),
            data = { BranchId: $("#" + type + "_BranchId").val(), StartDate: $("#" + type + "_StartDate").val(), EndDate: $("#" + type + "_EndDate").val() };
        if (type == "OrdersToBeSent") data.sendAutomatically = $("#List_OrdersToBeSent_SendType").val();
        if (grid != null) grid.rebind(data);
        var link = $("#" + type + "_ExportLink").attr("href");
        if (link) {
            link = link.replace(/BranchId=([^&]*)/, "BranchId=" + $("#" + type + "_BranchId").val());
            link = link.replace(/StartDate=([^&]*)/, "StartDate=" + escape($("#" + type + "_StartDate").val()));
            link = link.replace(/EndDate=([^&]*)/, "EndDate=" + escape($("#" + type + "_EndDate").val()));
            if (type == "OrdersToBeSent") { link = link.replace(/sendAutomatically=([^&]*)/, "sendAutomatically=" + $("#List_OrdersToBeSent_SendType").val()); }
            $("#" + type + "_ExportLink").attr("href", link);
        }
    },
    RebindAgencyCommunicationNotes: function() {
        var grid = $('#List_CommunicationNote').data('tGrid');
        if (grid != null && grid != undefined) {
            { grid.rebind({ BranchId: $("#AgencyCommunicationNote_BranchCode").val(), Status: $("#AgencyCommunicationNote_Status").val(), StartDate: $("#AgencyCommunicationNote_StartDate-input").val(), EndDate: $("#AgencyCommunicationNote_EndDate-input").val() }); }
            var $exportLink = $('#AgencyCommunicationNote_ExportLink');
            if ($exportLink != undefined) {
                var href = $exportLink.attr('href');
                if (href != undefined) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyCommunicationNote_BranchCode").val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $("#AgencyCommunicationNote_Status").val());
                    href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyCommunicationNote_StartDate-input").val());
                    href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyCommunicationNote_EndDate-input").val());
                    $exportLink.attr('href', href);
                }
            }
        }
    },
    RebindAgencyPastDueRecet: function() {
        var grid = $('#List_PastDueRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyPastDueRecet_BranchCode").val(), InsuranceId: $("#AgencyPastDueRecet_InsuranceId").val(), StartDate: $("#AgencyPastDueRecet_StartDate").val() }); }
        var $exportLink = $('#AgencyPastDueRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyPastDueRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyPastDueRecet_InsuranceId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyPastDueRecet_StartDate").val());
        $exportLink.attr('href', href);
    },
    RebindAgencyUpcomingRecet: function() {
        var grid = $('#List_UpcomingRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyUpcomingRecet_BranchCode").val(), InsuranceId: $("#AgencyUpcomingRecet_InsuranceId").val() }); }
        var $exportLink = $('#AgencyUpcomingRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyUpcomingRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyUpcomingRecet_InsuranceId").val());
        $exportLink.attr('href', href);
    },
    RebindOasisToExport: function(page) {
        var container = $("#" + page + "_GridContainer");
        if (container.length) {
            var sources = [], checkbox = container.closest(".window-content").find("input[name=paymentSources]:checked");
            checkbox.each(function() { sources.push($(this).val()); });
            if (sources != null && sources.length > 0) {
                $(container).empty().addClass("loading").css({ "position": "absolute", "width": "100%", "height": "85%" }).load('Oasis/Export', { BranchId: $("#" + page + "_BranchId").val(), paymentSources: sources.join() }, function(responseText, textStatus, XMLHttpRequest) {
                    $(this).css({ "position": "", "width": "", "height": "" })
                    if (textStatus == 'error') { U.Growl('This page could not be loaded. Please close this window and try again.', "error"); }
                    else if (textStatus == "success") {
                        $(container).removeClass("loading");
                        var $exportLink = $('#' + page + '_ExportLink');
                        var href = $exportLink.attr('href');
                        if (href != null) {
                            href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#" + page + "_BranchId").val());
                            href = href.replace(/paymentSources=([^&]*)/, 'paymentSources=' + sources);
                            $exportLink.attr('href', href);
                        }
                    }
                });
            }
            else {
                U.Growl('This page could not be loaded. Please select a payment source.', "error");
            }
        }
    },
    RebindBillDataList: function() { var grid = $("#Edit_Insurance_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ InsuranceId: $("#Edit_Insurance_Id").val() }); } },
    RebindLocationBillDataList: function() { var grid = $("#Edit_Location_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ InsuranceId: $("#Edit_Insurance_Id").val() }); } },
    LoadAgencyInsurances: function(branchId, insuranceControlId) {
        U.PostUrl("Agency/PatientInsurances", {
            branchId: branchId
        }, function(data) {
            var s = $("select#" + insuranceControlId);
            s.children('option').remove();
            s.append(data);
        })
    },
    RebindMedicareEligibilitySummaryGridContent: function(reportId, action, jsonData, sortParams) {
        var input = jsonData;
        if (sortParams != null && sortParams != undefined) {
            $.extend(input, { SortParams: sortParams });
        }
        $("#" + reportId + "GridContainer").empty().addClass("loading").load('Agency/' + action, input, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("This report can't be loaded. Please close this window and try again.", 'error');
            }
            else if (textStatus == "success") {
                $(this).removeClass("loading");
                var $exportLink = $('#' + reportId + '_ExportLink');
                if ($exportLink != null && $exportLink != undefined) {
                    var href = $exportLink.attr('href');
                    if (href != null && href != undefined) {
                        $.each(jsonData, function(key, value) {
                            var filter = new RegExp(key + "=([^&]*)");
                            href = href.replace(filter, key + '=' + value);
                        });
                        $exportLink.attr('href', href);
                    }
                }
            }
        });
    },
    LoadMissedVisits: function(jsonData, groupName, SortParams) {
        var input = jsonData;
        $("#MissedVisitsContentId").empty().addClass("loading").load('Agency/MissedVisitsContent', { branchId: input.BranchCode, startDate: input.StartDate, endDate: input.EndDate, groupName: groupName, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl('Missed Visits could not be loaded. Please close this window and try again.', "error"); }
            else if (textStatus == "success") {
                $("#MissedVisitsContentId").removeClass("loading");
                var $exportLink = $('#MissedVisits_ExportLink');
                var href = $exportLink.attr('href');
                href = href.replace(/branchId=([^&]*)/, 'branchId=' + input.BranchCode);
                href = href.replace(/startDate=([^&]*)/, 'startDate=' + input.StartDate);
                href = href.replace(/endDate=([^&]*)/, 'endDate=' + input.EndDate);
                $exportLink.attr('href', href);
            }
        });
    },
    MissedVisitRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) { $(e.row).addClass('darkgreen'); }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
    },
    RebindMissedVisitList: function() {
        if ($("#window_listmissedvisits").length == 1) {
            var sortParams = $("#MissedVisits_SortColumn").val() + "-" + $("#MissedVisits_SortDirection").val();
            var input = { BranchCode: $('#MissedVisits_BranchCode').val(), StartDate: $('#MissedVisits_StartDate').val(), EndDate: $('#MissedVisits_EndDate').val() };
            Agency.LoadMissedVisits(input, $("#MissedVisits_GroupName").val(), sortParams);
        }
    },
    InitQaCenter: function() {
        $('#window_caseManagementcenter_content .t-grid-content').css({ height: 'auto' });
        $('#window_caseManagementcenter .layout').layout({ west__paneSelector: '.layout-left' });
    },
    QaListDataBound: function() {
        Patient.Filter($("#txtSearch_Qa_Selection").val());
        if ($("#CaseManagerSelectionGrid .t-grid-content tr").length) {
            if (Agency._patientId == "") $('#CaseManagerSelectionGrid .t-grid-content tr' + ($("#txtSearch_Qa_Selection").val().length ? ".match" : "")).eq(0).click();
            else $('td:contains(' + Agency._patientId + ')', $('#CaseManagerSelectionGrid')).closest('tr').click();
        } else $("#QaMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#CaseManagerSelectionGrid .t-state-selected").length) $("#CaseManagerSelectionGrid .t-grid-content").scrollTop($("#CaseManagerSelectionGrid .t-state-selected").position().top - 50);
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            if (!Agency._isRebind) {
                Agency._dateFilter = "";
                Agency._showFilter = "";
                Agency._fromDate = "";
                Agency._toDate = "";
            }
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Agency.LoadInfoAndActivity(patientId);
        }
    },
    LoadInfoAndActivity: function(patientId) {
        $("#qacenterpatient-info").remove();
        $('#QaMainResult').empty().addClass('loading').load('Agency/CaseManagementData', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#QaMainResult').removeClass('loading');
            if (textStatus == 'error') $('#QaMainResult').html(U.AjaxError);
            $('#window_caseManagementcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    ActivityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
            if (dataItem.StatusName == "Missed Visit(complete)") {
                $(e.row).removeClass('darkgreen');
                $(e.row).addClass('darkred');
            }
        }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append($("<li/>", { "text": "Reopen Task" }).click(function() {
                $(e.row).find("a:contains('Reopen Task')").click();
            }));
            else if (!dataItem.IsOrphaned) Menu.append($("<li/>", { "text": "Edit Note" }).click(function() {
                $(e.row).find("a:first").click();
            }));
            Menu.append($("<li/>", { "text": "Details" }).click(function() {
                $(e.row).find("a:contains('Details')").click();
            })).append($("<li/>", { "text": "Delete" }).click(function() {
                $(e.row).find("a:contains('Delete')").click();
            })).append($("<li/>", { "text": "Print" }).click(function() {
                $(e.row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        });
    }
}