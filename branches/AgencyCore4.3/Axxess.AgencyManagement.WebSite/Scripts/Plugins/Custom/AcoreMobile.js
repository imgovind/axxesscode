﻿(function($) {
    $.extend($.fn, {
        AcoreMobileDesktop: function() {
            return this.each(function() {
                // Build DOM for Mobile
                $(this).addClass("desktop").append(
                    $("<div/>", { "id": "bar-top", "style": "z-index:2" }).append(
                        $("<div/>", { "id": "tab" })).append(
                        $("<span/>", { "class": "float-right" }).append(
                            $("<a/>", { "href": "Logout", "text": "Logout" }).prepend(
                                $("<span/>", { "class": "img icon", "style": "background-position:-208px -84px" })))).append(
                        $("<ul/>", { "id": "mainmenu" }))).append(
                    $("<div/>", { "id": "bar-bottom" }).append(
                        $("<ul/>", { "id": "taskbar" })).append(
                        $("<span/>", { "class": "float-right", "html": "&#169; 2009 &#8211; " + (new Date).getFullYear() + " Axxess&#8482; Technology Solutions, All Rights Reserved" }))).append(
                    $("<div/>", { "id": "window-top", "class": "" })
                );
                // Bind mouse and keyboard events to trigger resetting idle counter
                $(document).bind("touchmove keydown touchstart mousedown", function() {
                    Acore.IdleTime = 0
                })

            })
        },
        // Initialize idle modal
        AcoreIdle: function() {
            return this.each(function() {
                // Stop thread to count idle time
                clearInterval(Acore.TimeoutTimer);
                // Build idle countsown modal
                $(this).append(
                    $("<div/>", { "id": "idlemodal", "text": "Due to inactivity, you will automatically be logged off in " }).append(
                        $("<span/>", { "id": "timeoutsec" })
                    ).append(
                        $("<div/>", { "class": "buttons" }).append(
                            $("<ul/>").append(
                                $("<li/>").append(
                                    $("<a/>", { "href": "javascript:void(0);", "id": "idlemodal_stay", "text": "Stay Logged In" }).click(function() {
                                        // Stop thread for second by second updates
                                        clearInterval(IdleCountdown);
                                        // Close idle countdown modal
                                        $(this).closest(".window").Close();
                                        // Reset thread to count idle time
                                        Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                                    })
                                )
                            ).append(
                                $("<li/>").append(
                                    $("<a/>", { "href": "javascript:void(0);", "id": "idlemodal_logout", "text": "Logout" }).click(function() {
                                        // Logout of software
                                        $(location).attr("href", "/Logout");
                                    })
                                )
                            )
                        )
                    )
                );
                // Calculate auto-logout time
                var logout = new Date();
                logout.setTime(logout.getTime() + Acore.TimeoutCount * Acore.TimeoutInterval);
                // Set thread for second by second updates
                IdleCountdown = setInterval(function() {
                    // Calulate time remaining till auto-logout
                    var currentTime = new Date();
                    var remaining = new Date();
                    remaining.setTime(logout.getTime() - currentTime.getTime());
                    // If time is up, logout of software
                    if (parseInt(remaining.getMinutes()) == 59) $(location).attr("href", "/Logout");
                    // Else display amount of time remaining
                    else $("#timeoutsec").html(remaining.getMinutes() + ":" + (remaining.getSeconds() < 10 ? "0" + remaining.getSeconds() : remaining.getSeconds()));
                }, 1000);
            });
        },
        AcorePrintView: function(Options) {
            return this.each(function() {
                // Build basic print view
                $(this).css("overflow", "hidden").append(
                    $("<div/>", { "id": "print-box" }).append(
                        $("<iframe/>", { "name": "printview" + Acore.PrintId, "id": "printview", "class": "loading", "src": Options.Url }).load(function() {
                            $(this).removeClass("loading");
                        })
                    )).append(
                    $("<div/>", { "id": "print-controls", "class": "buttons" }).append(
                        $("<ul/>").append(
                            $("<li/>").append(
                                $("<a/>", { "id": "printbutton", "text": "Print" })
                            )).append(
                            $("<li/>").append(
                                $("<a/>", { "href": "javascript:void(0);", "text": "Close" }).click(function() {
                                    $(this).closest(".window").Close();
                                })
                            )
                        )
                    )
                );
                // If PDF Print, set click function
                if (Options.PdfUrl != null) $("#printbutton").click(function() {
                    U.GetAttachment(Options.PdfUrl, Options.PdfData);
                });
                // If HTML Print, set click function
                else
                    $("#printbutton").attr("href", "javascript:void(0);").click(function() {
                        window.frames["printview" + Acore.PrintId].focus();
                        window.frames["printview" + Acore.PrintId].print();
                        Acore.PrintId++;
                    });
                // If return button is enabled, add return reason field and proper button functionality
                if (Options.ReturnClick != null && typeof (Options.ReturnClick) == "function") {
                    $(this).find("#print-box").after(
                        $("<div/>", { "id": "print-return-reason" }).append(
                            $("<label/>", { "class": "strong", "text": "Reason for Return:" })).append(
                            $("<textarea/>")
                        )
                    );
                    $("#print-controls ul").prepend(
                        $("<li/>", { "class": "very-hidden" }).append(
                            $("<a/>", { "href": "javascript:void(0);", "id": "printreturncancel", "text": "Cancel" }).click(function() {
                                $("#print-controls li").removeClass("very-hidden");
                                $("#print-return-reason textarea").text("");
                                $("#print-return-reason").slideUp("slow");
                                $("#printreturncancel").parent().addClass("very-hidden");
                            })
                        )).prepend(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0);", "id": "printreturn", "text": "Return" }).click(function() {
                                if ($("#printreturncancel").parent().hasClass("very-hidden")) {
                                    $("#print-return-reason").show("slow");
                                    $("#print-controls li").addClass("very-hidden");
                                    $("#printreturn").parent().add($("#printreturncancel").parent()).removeClass("very-hidden");
                                } else Options.ReturnClick();
                            })
                        )
                    );
                }
                // If custom buttons are defined, add them to the start of the button list
                if (Options.Buttons != null) for (i = Options.Buttons.length; i > 0; i--) {
                    $("#print-controls ul").prepend(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0);", "text": Options.Buttons[i - 1].Text }).click(Options.Buttons[i - 1].Click)
                        )
                    );
                }
            });
        },
        AcoreTask: function(Options) {
            return this.each(function() {
                // Build task bar item
                $(this).attr({
                    "id": "task_" + Options.Id,
                    "class": "task active",
                    "style": "width:" + 100 / Acore.MaxWindows + "%;"
                }).append(
                    $("<a/>", { "href": "javascript:void(0);", "title": Options.TaskName, "text": Options.TaskName }).click(function() {
                        if ($(this).closest(".task").GetWindow().hasClass("active")) $(this).closest(".task").GetWindow().Minimize();
                        else $(this).closest(".task").GetWindow().WinFocus();
                    }).prepend(
                        $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                    )
                ).bind("contextmenu", function(Event) {
                    // Build context menu
                    var Menu = $("<ul/>");
                    var AcoreWindow = $(this).GetWindow();
                    // If not active window, add option to bring to front
                    if (!AcoreWindow.hasClass("active")) Menu.append(
                        $("<li/>", { "text": "Bring to Front" }).click(function() {
                            AcoreWindow.WinFocus();
                        })
                    );
                    // If not minimized, add option to minimize
                    if (!AcoreWindow.hasClass("minimized")) Menu.append(
                        $("<li/>", { "text": "Minimize" }).click(function() {
                            AcoreWindow.Minimize();
                        })
                    );
                    // If not maximized, add option to maximize
                    if (!AcoreWindow.hasClass("maximized")) Menu.append(
                        $("<li/>", { "text": "Maximize" }).click(function() {
                            AcoreWindow.Maximize();
                        })
                    );
                    // If maximized, add option to restore
                    else Menu.append(
                        $("<li/>", { "text": "Restore" }).click(function() {
                            AcoreWindow.Restore();
                        })
                    );
                    // Add option to close
                    Menu.append(
                        $("<li/>", { "text": "Close" }).click(function() {
                            AcoreWindow.Close();
                        })
                    );
                    // Enable context menu
                    Menu.ContextMenu(Event);
                });
            });
        },
        AcoreWindow: function(Overrides) {
            // Window default values
            var Defaults = {
                "Width": "85%",
                "Height": "85%",
                "IgnoreMinSize": false,
                "WindowFrame": true,
                "StatusBar": true,
                "Resize": true,
                "Center": false,
                "Modal": false,
                "IconX": 220,
                "IconY": 12
            };
            // Merge defaults with overrides
            var Options = $.extend({}, Defaults, Overrides);
            return this.each(function() {
                if (Acore.Mobile) {
                    // Minimize all windows
                    $(".window").Minimize();
                    // Build basic mobile window
                    $(this).attr({ "id": "window_" + Options.Id, "class": "window" }).append(
                        $("<div/>", { "class": "window-top" }).append(
                            $("<span/>", { "class": "float-left" }).append(
                                $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                            )).append(
                            $("<span/>", { "class": "abs", "text": Options.WindowName })).append(
                            $("<span/>", { "class": "float-right" }).append(
                                $("<a/>", { "href": "javascript:void(0);", "class": "window-min" }).click(function() { $(this).closest(".window").Minimize() })).append(
                                $("<a/>", { "href": "javascript:void(0);", "class": "window-close" }).click(function() { $(this).closest(".window").Close() })
                            )
                        )).append(
                        $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "abs window-content loading" })
                    );
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).GetData(Options.Url, Options.OnLoad, Options.Inputs);
                    else $(".window-content", this).removeClass("loading");
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                } else {
                    // Build basic window
                    $(this).attr({
                        "id": "window_" + Options.Id,
                        "class": "abs window"
                    }).css({
                        "position": "absolute",
                        "z-index": ++Acore.OpenWindows
                    }).mousedown(function() {
                        if (!$(this).hasClass("active")) $(this).WinFocus();
                    }).append(
                        $("<div/>", { "class": "abs window-inner" }).append(
                            $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "abs window-content loading" }).scroll(function() {
                                // Collapse date picker on scroll
                                $(".ui-datepicker").hide();
                            })
                        )
                    );
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).GetData(Options.Url, Options.OnLoad, Options.Inputs);
                    else $(".window-content", this).removeClass("loading");
                    // If window has frame, add top bar
                    if (Options.WindowFrame)
                        $(".window-inner", this).prepend(
                            $("<div/>", { "class": "window-top" }).dblclick(function() {
                                if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                else $(this).closest(".window").Maximize();
                            }).append(
                                $("<span/>", { "class": "float-left" }).append(
                                    $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" }).dblclick(function() {
                                        $(this).closest(".window").Close();
                                    })
                                )).append(
                                $("<span/>", { "class": "abs", "text": Options.WindowName })).append(
                                $("<span/>", { "class": "float-right" }).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window-min" }).click(function() {
                                        $(this).closest(".window").Minimize();
                                    })).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window-resize" }).click(function() {
                                        if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                        else $(this).closest(".window").Maximize();
                                    })).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window-close" }).click(function() {
                                        $(this).closest(".window").Close();
                                    })
                                )
                            )
                        );
                    // If unframed window, set content top accordingly
                    else
                        $(".window-content", this).css({
                            "top": "0",
                            "bottom": "0"
                        });
                    // If window is resizable, enable resizing
                    if (Options.Resize)
                        $(this).resizable({
                            containment: "parent",
                            handles: "n, ne, e, se, s, sw, w, nw",
                            minWidth: Acore.MinWinWidth,
                            minHeight: Acore.MinWinHeight
                        });
                    // If window is not resizable and framed, remove resize button
                    else if (Options.WindowFrame) $(".window-resize", this).remove();
                    // If framed window has status bar, add in status bar and set content bottom accordingly
                    if (Options.WindowFrame && Options.StatusBar) {
                        $(".window-inner", this).append($("<div/>", { "class": "abs window-bottom", "text": Options.Status }));
                        $(".window-content", this).css("bottom", "21px");
                    }
                    // Set window size
                    if (parseInt(Options.Width) > 0) $(this).css("width", Options.Width);
                    if (parseInt(Options.Height) > 0) $(this).css("height", Options.Height);
                    // If modal window, shade out the background and increase the z-index
                    if (Options.Modal) {
                        $("body").append(
                            $("<div/>", { "id": "shade", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                            })
                        );
                        $(this).css("z-index", parseInt(Acore.MaxWindows + 2));
                        // If framed non-modal window, enable draggable
                    } else if (Options.WindowFrame)
                        $(this).draggable({
                            containment: "parent",
                            handle: ".window-top .abs"
                        });
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                }
            });
        },
        Close: function() {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // If CKEditor found in window, remove instance
                    if ($(".cke_editor", this).length && !Acore.Mobile) CKEDITOR.remove(CKEDITOR.instances["New_Message_Body"]);
                    // If background is shaded, remove shade
                    if ($("#shade").length) $("#shade").remove();
                    // Set IsOpen flag to false
                    Acore.Windows[$(this).GetAcoreId()].IsOpen = false;
                    // Decrement number of open windows
                    Acore.OpenWindows--;
                    // If temporary modal window, remove window from registry
                    if ($(this).GetAcoreId() == "ModalWindow") Acore.RemoveWindow("ModalWindow");
                    // Remove task bar
                    $(this).GetTask().remove();
                    // Remove window
                    $(this).remove();
                } else U.Growl("Error: Unexpected call to close non-window element. Cannot close element that is not a window.", "error");
            });
        },
        ContextMenu: function(Event) {
            return this.each(function() {
                // If no other context menus are open and the DOM focus is not upon an input of type text nor textarea
                if ($(".context-menu").length == 0 && $("input.t-input:focus,input[type=text]:focus,textarea:focus").length == 0) {
                    // If no text is highlighted
                    if ((document.getSelection && document.getSelection() == "") || (document.selection && document.selection.createRange().text == "")) {
                        // Prevent the default context menu from popping up
                        Event.preventDefault();
                        // Build context menu into the DOM
                        $("body").append(
                            $("<div/>").css({
                                "position": "fixed",
                                "top": "0",
                                "left": "0",
                                "width": "100%",
                                "height": "100%",
                                "z-index": "999"
                            }).click(function() {
                                $(this).remove();
                            }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                                // If menu is too tall to display below the cursor, then display above
                                if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css({ "bottom": $("body").height() - Event.pageY + "px", "top": "auto" });
                                else $(".context-menu").css({ "top": Event.pageY + "px", "bottom": "auto" });
                                // If menu is too wide to display to the right of the cursor, then display to the left
                                if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css({ "right": $("body").width() - Event.pageX + "px", "left": "auto" });
                                else $(".context-menu").css({ "left": Event.pageX + "px", "right": "auto" });
                            }).append(
                                $(this).addClass("context-menu").css("z-index", "1000")
                            )
                        );
                        // If menu is too tall to display below the cursor, then display above
                        if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css("bottom", $("body").height() - Event.pageY + "px");
                        else $(".context-menu").css("top", Event.pageY + "px");
                        // If menu is too wide to display to the right of the cursor, then display to the left
                        if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css("right", $("body").width() - Event.pageX + "px");
                        else $(".context-menu").css("left", Event.pageX + "px");
                        // If context menu has submenus, enable superfish plugin
                        if ($(".context-menu ul").length) $(".context-menu").Menu();
                        // Apply hover class on mouse over and remove on mouse out
                        $(".context-menu li").each(function() { $(this).mouseover(function() { $(this).addClass("hover") }) });
                        $(".context-menu li").each(function() { $(this).mouseout(function() { $(this).removeClass("hover") }) });
                    }
                }
            });
        },
        GetAcoreId: function() {
            // If object is window or task, extrapolate the string literal for the Acore Id and return it
            if (($(this).hasClass("window") || $(this).hasClass("task") != null) && $(this).attr("id") != null) return $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
        },
        GetData: function(Url, OnLoad, Inputs) {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // Load window content
                    $(".window-content", this).Load(Url, Inputs, function(ResponseText, TextStatus, XMLHttpRequest) {
                        $(this).removeClass("loading");
                        // If it was returned an error, display the generic AJAX error message
                        if (TextStatus == "error") $(this).html(U.AjaxError);
                        // If OnLoad function defined, launch it
                        if (OnLoad != null && typeof (OnLoad) == "function") OnLoad();
                    });
                }
            });
        },
        GetTask: function() {
            // If object is a window, find and return its corresponding task bar item
            if ($(this).hasClass("window") && $(this).attr("id") != null) {
                var task = "#task_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(task).length) return $(task);
            }
            return false;
        },
        GetWindow: function() {
            // If object is a task bar item, find and return its corresponding window
            if (($(this).hasClass("context") || $(this).hasClass("task")) && $(this).attr("id") != null) {
                var window = "#window_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(window).length) return $(window);
            }
            return false;
        },
        Maximize: function() {
            return this.each(function() {
                // If object is a window, set restore attributes, and then set CSS to maximize
                if ($(this).hasClass("window")) {
                    $(this).attr({
                        "restore-top": $(this).css("top"),
                        "restore-left": $(this).css("left"),
                        "restore-right": $(this).css("right"),
                        "restore-bottom": $(this).css("bottom"),
                        "restore-width": $(this).css("width"),
                        "restore-height": $(this).css("height")
                    }).addClass("maximized").removeClass("minimized").css({
                        "top": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "left": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "right": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "bottom": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "width": "auto",
                        "height": "auto"
                    });
                    // If not the active window, then bring to front
                    if (!$(this).hasClass("active")) $(this).WinFocus();
                } else U.Growl("Error: Unexpected call to maximize non-window element. Cannot maximize element that is not a window.", "error");
            });
        },
        Minimize: function() {
            return this.each(function() {
                // If object is a window, minimize and ensure not active
                if ($(this).hasClass("window")) $(this).removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active");
                else U.Growl("Error: Unexpected call to minimize non-window element. Cannot minimize element that is not a window.", "error");
            });
        },
        Rename: function(NewName) {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // If window has frame, rename at the top of the window
                    if ($(".window-top .abs", this).length) $(".window-top .abs", this).html(NewName);
                    // Rename task bar item
                    $(this).GetTask().find("a").html(
                        $(this).GetTask().find(".img")
                    ).append(NewName).attr("title", $(this).GetTask().text());
                } else U.Growl("Error: Unexpected call to rename non-framed window element. Cannot rename element that is not a window with a frame.", "error");
            });
        },
        Restore: function() {
            return this.each(function() {
                // If object is a window and maximized, restore css to old numbers
                if ($(this).hasClass("window") && $(this).hasClass("maximized")) {
                    $(this).removeClass("maximized").css({
                        "top": $(this).attr("restore-top"),
                        "left": $(this).attr("restore-left"),
                        "right": $(this).attr("restore-right"),
                        "bottom": $(this).attr("restore-bottom"),
                        "width": $(this).attr("restore-width"),
                        "height": $(this).attr("restore-height")
                    });
                } else U.Growl("Error: Unexpected call to restore non-window element. Cannot restore element that is not a window.", "error");
            });
        },
        Status: function(Message) {
            return this.each(function() {
                // If object is a window with a status bar, update message
                if ($(this).hasClass("window") && $(".window-bottom", this).length) $(".window-bottom", this).text(Message);
                else U.Growl("Error: Unexpected call to status update invalid element. Cannot update status bar on non-window elements or windows that do not contain status bars.", "error");
            });
        },
        WinFocus: function() {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // Clear current active window
                    $(".window.active", "#desktop").removeClass("active");
                    $(".active", "#task").removeClass("active");
                    $(".ui-menu").hide();
                    // Make current window active
                    $(this).addClass("active").removeClass("minimized");
                    $(this).GetTask().addClass("active");
                    // Recalculate windows' z-index
                    $(".window:not(.active)").each(function() {
                        if ($(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    });
                    $(".window.active").css("z-index", Acore.OpenWindows);
                } else U.Growl("Error: Unexpected call to window focus non-window element. Cannot restore element that is not a window.", "error");
            });
        }
    })
})(jQuery);