﻿$.extend(Visit, {
    UAPInsulinPrepAdminVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
            U.ShowIfChecked($(Prefix + "FFBS"), $(Prefix + "FFBSMore"));
            U.ShowIfChecked($(Prefix + "NFFBS"), $(Prefix + "NFFBSMore"));
            U.ShowIfChecked($(Prefix + "AsepticTech1"), $(Prefix + "AsepticTech1More"));
            U.ShowIfChecked($(Prefix + "AsepticTechSlide1"), $(Prefix + "AsepticTechSlide1More"));
            U.ShowIfChecked($(Prefix + "PADueTo6"), $(Prefix + "PADueToOther"));
            U.ShowIfChecked($(Prefix + "Supplies5"), $(Prefix + "SuppliesOther"));
            Visit.Shared.Init(Type, Visit.UAPInsulinPrepAdminVisit.Init);
        },
        Submit: function(Button, Completing, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, "UAPInsulinPrepAdminVisit", EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("UAPInsulinPrepAdminVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "UAPInsulinPrepAdminVisit") }
    },
    UAPWoundCareVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
            U.ShowIfChecked($(Prefix + "MedicationApplied7"), $(Prefix + "MedicationAppliedOther"));
            U.ShowIfChecked($(Prefix + "DressingApplied10"), $(Prefix + "DressingAppliedOther"));
            U.ShowIfChecked($(Prefix + "DressingSecured3"), $(Prefix + "DressingSecuredOther"));
            U.ShowIfChecked($(Prefix + "Supplies11"), $(Prefix + "SuppliesOther"));
            Visit.Shared.Init(Type, Visit.UAPWoundCareVisit.Init);
        },
        Submit: function(Button, Completing, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, "UAPWoundCareVisit", EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("UAPWoundCareVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "UAPWoundCareVisit") }
    }
});