﻿$.extend(Visit, {
    COTAVisit: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("COTAVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTVisit") }
    },
    OTDischarge: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTDischarge", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTDischarge") }
    },
    OTEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.OTVisit.Init, "Schedule/OTEvaluationContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTEvaluation") }
    },
    OTMaintenance: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTMaintenance", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTMaintenance") }
    },
    OTReEvaluation: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTReEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTReEvaluation") }
    },
    OTReassessment: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.OTReassessment.Init, "Schedule/OTEvaluationContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTReassessment", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTReassessment") }
    },
    OTVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.OTVisit.Init, "Schedule/OTVisitContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTVisit") }
    },
    PTAVisit: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTAVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTVisit") }
    },
    PTDischarge: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.PTEvaluation.Init, "Schedule/PTDischargeContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTDischarge", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTDischarge") }
    },
    PTEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.PTEvaluation.Init, "Schedule/PTEvaluationContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTEvaluation") }
    },
    PTMaintenance: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTMaintenance", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTMaintenance") }
    },
    PTReEvaluation: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTReEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTReEvaluation") }
    },
    PTReassessment: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.PTReassessment.Init, "Schedule/PTEvaluationContent");
        },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTReassessment", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTReassessment") }
    },
    PTVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {

            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.PTVisit.Init, "Schedule/PTVisitContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTVisit") }
    },
    STDischarge: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STEvaluation") }
    },
    STEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
            U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
            U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
            U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
            U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
            U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
            U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
            Visit.Shared.Init(Type, Visit.PTEvaluation.Init, "Schedule/STEvaluationContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STEvaluation") }
    },
    STMaintenance: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STMaintenance", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STMaintenance") }
    },
    STReEvaluation: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STReEvaluation", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STReEvaluation") }
    },
    STReassessment: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
            U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
            U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
            U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
            U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
            U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
            U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
            Visit.Shared.Init(Type, Visit.STReassessment.Init, "Schedule/STReassessmentContent");
        },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STReassessment", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STReassessment") }
    },
    STVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {

            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            $(Prefix + "PhysicianDropDown").PhysicianInput();
            Visit.Shared.Init(Type, Visit.STVisit.Init, "Schedule/STVisitContent");
        },
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("STVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "STVisit") }
    },
    PTSupervisoryVisit: {
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PTSupervisoryVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PTSupervisoryVisit") }
    },
    OTSupervisoryVisit: {
        Submit: function(Button, Completing, Type, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, Type, EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("OTSupervisoryVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "OTSupervisoryVisit") }
    }
});