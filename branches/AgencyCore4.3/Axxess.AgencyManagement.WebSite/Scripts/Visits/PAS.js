﻿$.extend(Visit, {
    PASCarePlan: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Template.OnChangeInit();
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
            U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
            U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
            U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
            Visit.Shared.Init(Type, Visit.PASCarePlan.Init);
        },
        Submit: function(Button, Completing, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, "PASCarePlan", EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PASCarePlan", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PASCarePlan") }
    },
    PASVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.ShowIfSelectEquals($(Prefix + "HomeboundStatus"), "8", $(Prefix + "HomeboundStatusOther"));
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
            Visit.Shared.Init(Type, Visit.PASVisit.Init);
        },
        Submit: function(Button, Completing, EpisodeId, PatientId) { Visit.Shared.Submit(Button, Completing, "PASVisit", EpisodeId, PatientId) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PASVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PASVisit") }
    },
    PASTravel: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Visit.Shared.Init(Type, Visit.PASTravel.Init);
        },
        Submit: function(Button, Completing) { Visit.Shared.Submit(Button, Completing, "PASTravel") },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("PASTravel", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "PASTravel") }
    }
});