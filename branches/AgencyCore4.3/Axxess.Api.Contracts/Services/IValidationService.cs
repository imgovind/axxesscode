﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IValidationService : IService
    {
        [OperationContract]
        List<ValidationError> ValidateAssessment(string oasisDataString);
        [OperationContract]
        List<LogicalError> LogicalInconsistencyCheck(string oasisDataString);
    }
}
