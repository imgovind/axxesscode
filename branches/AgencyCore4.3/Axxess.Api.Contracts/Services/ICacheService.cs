﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2011/10/")]
    public interface ICacheService : IService
    {
        [OperationContract]
        int Count();
        [OperationContract]
        void Remove(string key);
        [OperationContract]
        bool Contains(string key);
        [OperationContract]
        void Set<T>(string key, T value);
        [OperationContract]
        void Set<T>(string key, T value, DateTime absoluteExpiration);
        [OperationContract]
        void Set<T>(string key, T value, TimeSpan slidingExpiration);
        [OperationContract]
        bool TryGet<T>(string key, out T value);
        [OperationContract]
        T Get<T>(string key);
        [OperationContract]
        List<string> CachedKeys();

        //[OperationContract]
        //void RefreshAgency(Guid agencyId);
        //[OperationContract]
        //string GetAgencyXml(Guid agencyId);


        //[OperationContract]
        //void RefreshPhysicians(Guid agencyId);
        //[OperationContract]
        //string GetPhysicianXml(Guid physicianId, Guid agencyId);
        //[OperationContract]
        //List<string> GetPhysicians(Guid agencyId);


        //[OperationContract]
        //void RefreshUsers(Guid agencyId);
        //[OperationContract]
        //string GetUserDisplayName(Guid userId, Guid agencyId);
        //[OperationContract]
        //List<UserData> GetUsers();
    }
}
