﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/11/")]
    public class ScheduleEvent
    {
        //public ScheduleEvent()
        //{
        //    this.UserId = Guid.Empty;
        //    this.EventId = Guid.Empty;
        //    this.EventDate = string.Empty;
        //    this.Assets = new List<Guid>();
        //}
        public Guid AgencyId { get; set; }
        public Guid EventId { get; set; }
        public int DisciplineTask { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime VisitDate { get; set; }
        public string Status { get; set; }
        public string Discipline { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMissedVisit { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Note { get; set; }
        public Guid NewEpisodeId { get; set; }
        public string EpisodeNotes { get; set; }
        public Guid AdmissionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string HippsCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public DateTime DOB { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public int PatientStatus { get; set; }
        public string SubmissionFormat { get; set; }

        //public string AssociatedMileage { get; set; }
        //public string ReturnReason { get; set; }
        //public string Comments { get; set; }
       
        //public bool IsOrderForNextEpisode { get; set; }
       
    }
}