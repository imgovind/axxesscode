﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/01/")]
    public class HippsAndHhrg
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string HHRG { get; set; }
        [DataMember]
        public string HIPPS { get; set; }
        [DataMember]
        public double HHRGWeight { get; set; }
        [DataMember]
        public DateTime Time { get; set; }
    }
}
