﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;
    using Axxess.OasisC.Enums;

    internal static class Reports
    {
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();

        #region Static Helper Methods

        internal static AgencyData GetAgency(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator`  FROM `agencies` WHERE `Id` = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static AgencyData GetAgencyLocation(Guid agencyId, Guid agencyLocationId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `AgencyId`, `Name`, `Payor`, `NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator` , `IsLocationStandAlone`, `AddressZipCode` FROM `agencylocations` WHERE `Id` = @agencylocationid  AND `AgencyId` = @agencyid ;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("AgencyId"),
                    LocationId = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    Payor = reader.GetInt("Payor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    AddressZipCode = reader.GetString("AddressZipCode")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static string NumberEligible(Guid agencyId, Guid agencyLocationId, DateTime sampleMonthLast)
        {
            int count = 0;
            var script = @"SELECT Count(`Id`) FROM `patients` WHERE `AgencyId` = @agencyid AND `AgencyLocationId` = @agencylocationid  AND `IsDeprecated` = 0 AND `Status` in (1, 2) " +
                "AND `PrimaryInsurance` in (1,2,3,4) AND (`Created` <= @month || `StartofCareDate` <= @month)";

            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("month", sampleMonthLast)
                .AsScalar();
            }

            return count.ToString();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsWithPossibleAssessmentsLean(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] { 13, 61, 89, 112, 8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.DOB as DOB,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate,
                agencymanagement.patientepisodes.Details as EpisodeNotes 
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate)) OR ( (DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdateback) AND DATE(@enddateback)) AND agencymanagement.scheduleevents.DisciplineTask IN ({0}) ))AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", oasisDisciplineTasks);


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddateback", startDate.AddDays(-1))
                .AddDateTime("startdateback", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    DOB = reader.GetDateTime("DOB"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsLean(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, bool isBillableOnly)
        {
            var billableScript = string.Empty;
            if (isBillableOnly)
            {
                 billableScript = " agencymanagement.scheduleevents.IsBillable = 1 AND ";
            }
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                   
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate) AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    {0}
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", billableScript);


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsWithPerviousAssessment(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] { 13, 61, 89, 112, 8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patientepisodes.Id as EpisodeId,
                agencymanagement.patients.Id as PatientId, 
                agencymanagement.patients.DOB as DOB,
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.Status as PatientStatus,
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.DischargeDate as DischargeDate,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patients.AddressLine1 as AddressLine1, 
                agencymanagement.patients.AddressLine2 as AddressLine2,
                agencymanagement.patients.AddressCity as AddressCity,
                agencymanagement.patients.PhoneHome as PhoneHome, 
                agencymanagement.patients.AddressStateCode as AddressStateCode, 
                agencymanagement.patients.AddressZipCode as AddressZipCode, 
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber, 
                agencymanagement.patients.StartofCareDate as StartofCareDate,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate ,
                oasisc.assessments.OasisData as Note , 
                oasisc.assessments.HippsCode as HippsCode
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                            LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate)) OR ( (DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdateback) AND DATE(@enddateback)) AND agencymanagement.scheduleevents.DisciplineTask IN ({0}))) AND
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0",oasisDisciplineTasks);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddateback", startDate.AddDays(-1))
                .AddDateTime("startdateback", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    DOB = reader.GetDateTime("DOB"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    PatientStatus = reader.GetInt("PatientStatus"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Note = reader.GetStringNullable("Note"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static string SkilledVisitCount(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();

            episodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (schedule != null && schedule.Count > 0)
                    {
                        var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCare() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            list.AddRange(scheduledEvents);
                        }
                    }
                }
            });

            return list.Count.ToString();
        }

        internal static string SkilledVisitCount(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var episodes = GetEpisodesBetweenByPatientId(agencyId, patientId, startDate, endDate);
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCare() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                            if (scheduledEvents != null && scheduledEvents.Count > 0)
                            {
                                list.AddRange(scheduledEvents);
                            }
                        }
                    }
                });
            }

            return list.Count.ToString();
        }

        internal static List<ScheduleEvent> AllSkilledVisits(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var patientEpisodes = episodes.Where(e =>
                 (e.StartDate.Date >= startDate.Date && e.StartDate.Date <= endDate.Date)
                || (e.EndDate.Date >= startDate.Date && e.EndDate.Date <= endDate.Date)
                || (startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date)
                || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date));
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW())
                               && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                    if (schedule != null && schedule.Count > 0)
                    {
                        list.AddRange(schedule);
                    }
                }
            });

            return list;
        }

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT 
                    patientepisodes.Id as EpisodeId,
                    patients.Id as PatientId, 
                    patients.FirstName as FirstName,
                    patients.LastName as LastName, 
                    patients.Status as Status, 
                    patients.MiddleInitial as MiddleInitial,
                    patients.DischargeDate as DischargeDate, 
                    patients.MedicareNumber as MedicareNumber,
                    patients.MedicaidNumber as MedicaidNumber,
                    patients.AddressLine1 as AddressLine1, 
                    patients.AddressLine2 as AddressLine2,
                    patients.AddressCity as AddressCity,
                    patients.PhoneHome as PhoneHome, 
                    patients.AddressStateCode as AddressStateCode,
                    patients.AddressZipCode as AddressZipCode,
                    patientepisodes.EndDate as EndDate,
                    patientepisodes.StartDate as StartDate,
                    patientepisodes.Schedule as Schedule,
                    patientepisodes.Details as Details  
                        FROM 
                            patientepisodes
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
                                    WHERE 
                                        
                                        patientepisodes.AgencyId = @agencyid AND
                                        patients.AgencyLocationId = @agencylocationid AND
                                        (patients.Status = 1 || patients.Status = 2) AND 
                                        patients.IsDeprecated = 0 AND
                                        patientepisodes.IsActive = 1 AND
                                        patientepisodes.IsDischarged = 0 AND 
                                        Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(patients.DOB as DATETIME)))) >= 18 AND 
                                        (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesBetweenByPatientId(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT
                patientepisodes.Id as EpisodeId,
                patients.Id as PatientId,
                patients.FirstName as FirstName,
                patients.LastName as LastName, 
                patients.Status as Status,
                patients.MiddleInitial as MiddleInitial,
                patients.DischargeDate as DischargeDate, 
                patients.MedicareNumber as MedicareNumber,
                patients.MedicaidNumber as MedicaidNumber,
                patients.AddressLine1 as AddressLine1, 
                patients.AddressLine2 as AddressLine2,
                patients.AddressCity as AddressCity,
                patients.PhoneHome as PhoneHome, 
                patients.AddressStateCode as AddressStateCode,
                patients.AddressZipCode as AddressZipCode,
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate ,
                patientepisodes.Schedule as Schedule 
                    FROM 
                        patientepisodes
                            INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
                                WHERE
                                    patientepisodes.AgencyId = @agencyid AND 
                                    patients.Id = @patientid AND
                                    patients.IsDeprecated = 0 AND 
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND 
                                    (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate BETWEEN @startdate AND @enddate))
                                         ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsWithPossibleAssessmentsLeanForcahps(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] { 13, 61, 89, 112, 8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.DOB as DOB,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate,
                agencymanagement.patientepisodes.Details as EpisodeNotes 
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate)) OR ( (DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdateback) AND DATE(@enddateback)) AND agencymanagement.scheduleevents.DisciplineTask IN ({0}) ))AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", oasisDisciplineTasks);


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddateback", startDate.AddDays(-1))
                .AddDateTime("startdateback", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    DOB = reader.GetDateTime("DOB"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes")
                })
                .AsList();
            }
            return list.ToList();
        }



        //internal static List<EpisodeData> GetEpisodesByAdmissionPeriodId(List<Guid> admissionPeriodIds)
        //{
        //    var list = new List<EpisodeData>();
        //    var script = string.Format("select `Id`, `EndDate`, `StartDate`, `Schedule` from `patientepisodes` where `AdmissionId` in ({0});", admissionPeriodIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

        //    using (var cmd = new FluentCommand<EpisodeData>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .SetMap(reader => new EpisodeData
        //        {
        //            Id = reader.GetGuid("Id"),
        //            EndDate = reader.GetDateTime("EndDate"),
        //            StartDate = reader.GetDateTime("StartDate"),
        //            Schedule = reader.GetStringNullable("Schedule")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, List<int> paymentSources)
        {
            var items = new List<EpisodeData>();
            var results = new List<EpisodeData>();
            var script =
                @"SELECT 
                patientepisodes.Id as EpisodeId,
                patients.Id as PatientId, 
                patients.FirstName as FirstName, 
                patients.LastName as LastName, 
                patients.Status as Status,
                patients.PaymentSource as PaymentSource,
                patients.MiddleInitial as MiddleInitial,
                patients.DischargeDate as DischargeDate, 
                patients.MedicareNumber as MedicareNumber,
                patients.MedicaidNumber as MedicaidNumber,
                patients.AddressLine1 as AddressLine1, 
                patients.AddressLine2 as AddressLine2,
                patients.AddressCity as AddressCity, 
                patients.PhoneHome as PhoneHome, 
                patients.AddressStateCode as AddressStateCode,
                patients.AddressZipCode as AddressZipCode,
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate,
                patientepisodes.Schedule as Schedule
                    FROM 
                        patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
                            WHERE 
                                patientepisodes.AgencyId = @agencyid AND 
                                patients.AgencyLocationId = @agencylocationid AND
                                patients.IsDeprecated = 0 AND
                                (patients.Status = 1 || patients.Status = 2) AND 
                                patientepisodes.IsActive = 1 AND 
                                patientepisodes.IsDischarged = 0 AND
                                (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate BETWEEN @startdate AND @enddate))
                                    ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                items = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PaymentSources = reader.GetStringNullable("PaymentSource")
                })
                .AsList();
            }

            if (items != null && items.Count > 0)
            {
                items.ForEach(i =>
                {
                    if (i.PaymentSources.IsNotNullOrEmpty())
                    {
                        paymentSources.ForEach(p =>
                        {
                            if (i.PaymentSources.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Contains(p.ToString()))
                            {
                                results.Add(i);
                                return;
                            }
                        });
                    }
                    else
                    {
                        Windows.EventLog.WriteEntry(string.Format("Patient {0} has no Payment Sources: Id: {1}", i.DisplayName, i.PatientId), System.Diagnostics.EventLogEntryType.Information);
                    }
                });
            }
            return results;
        }

        //internal static List<EpisodeData> GetEpisodesBetweenForPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        //{
        //    var list = new List<EpisodeData>();
        //    var script =
        //        @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
        //        "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
        //        "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
        //        "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
        //        "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
        //        "patientepisodes.StartDate as StartDate , patientepisodes.Schedule as Schedule " +
        //        "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
        //        "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
        //        "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
        //        "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
        //        "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
        //        "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
        //        "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

        //    using (var cmd = new FluentCommand<EpisodeData>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddGuid("patientId", patientId)
        //        .AddDateTime("enddate", endDate)
        //        .AddDateTime("startdate", startDate)
        //        .SetMap(reader => new EpisodeData
        //        {
        //            AgencyId = agencyId,
        //            Id = reader.GetGuid("EpisodeId"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EndDate = reader.GetDateTime("EndDate"),
        //            StartDate = reader.GetDateTime("StartDate"),
        //            Schedule = reader.GetStringNullable("Schedule"),
        //            Status = reader.GetInt("Status"),
        //            LastName = reader.GetString("LastName"),
        //            FirstName = reader.GetString("FirstName"),
        //            MiddleInitial = reader.GetStringNullable("MiddleInitial"),
        //            MedicareNumber = reader.GetStringNullable("MedicareNumber"),
        //            MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
        //            AddressLine1 = reader.GetString("AddressLine1"),
        //            AddressLine2 = reader.GetStringNullable("AddressLine2"),
        //            AddressCity = reader.GetString("AddressCity"),
        //            PhoneHome = reader.GetStringNullable("PhoneHome"),
        //            AddressStateCode = reader.GetString("AddressStateCode"),
        //            AddressZipCode = reader.GetString("AddressZipCode"),
        //            DischargeDate = reader.GetDateTime("DischargeDate")
        //        })
        //        .AsList();
        //    }
        //    return list.ToList();
        //}

        //internal static Assessment GetEpisodeAssessment(EpisodeData episode)
        //{
        //    Assessment assessment = null;
        //    if (episode != null && episode.Schedule.IsNotNullOrEmpty())
        //    {
        //        var sampleMonthEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
        //            .Where(e => !e.EventId.IsEmpty() 
        //                    && e.EventDate.IsValidDate() 
        //                    && !e.IsMissedVisit
        //                    && !e.IsDeprecated
        //                    && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date
        //                    && (e.IsStartofCareAssessment())).OrderBy(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

        //        if (sampleMonthEvent != null)
        //        {
        //            assessment = oasisDataProvider.OasisAssessmentRepository.Get(sampleMonthEvent.EventId, sampleMonthEvent.GetAssessmentType(), episode.AgencyId);
        //        }
        //        else
        //        {
        //            var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.StartDate);
        //            if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
        //            {
        //                var samplePreviousEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
        //                    .Where(e => !e.EventId.IsEmpty()
        //                        && e.EventDate.IsValidDate()
        //                        && !e.IsMissedVisit 
        //                        && !e.IsDeprecated 
        //                        && e.EventDate.ToDateTime().Date >= episode.StartDate.AddDays(-6).Date
        //                        && e.EventDate.ToDateTime().Date <= episode.StartDate.AddDays(-1).Date
        //                        && previousEpisode.EndDate.AddDays(1).Date == episode.StartDate.Date
        //                        && (e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
        //                    .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

        //                if (samplePreviousEvent != null)
        //                {
        //                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(samplePreviousEvent.EventId, samplePreviousEvent.GetAssessmentType(), episode.AgencyId);
        //                }
        //            }
        //        }
        //    }
        //    return assessment;
        //}

        internal static Assessment GetEpisodeAssessment(EpisodeData episode, DateTime end)
        {
            Assessment assessment = null;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var sampleMonthEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                    .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                if (sampleMonthEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(sampleMonthEvent.EventId, sampleMonthEvent.GetAssessmentType(), episode.AgencyId);
                }
                else
                {
                    var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.Id);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var samplePreviousEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                            .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                        if (samplePreviousEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(samplePreviousEvent.EventId, samplePreviousEvent.GetAssessmentType(), episode.AgencyId);
                        }
                    }
                }
            }
            Windows.EventLog.WriteEntry(string.Format("Assessment found: {0}. Agency: {1}. Patient: {2}. Episode {3}.", assessment != null, episode.AgencyId, episode.PatientId, episode.Id), System.Diagnostics.EventLogEntryType.Information);
            return assessment;
        }

        internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var episodes = GetPatientEpisodes(agencyId, patientId);
            if (episodes != null && episodes.Count > 0)
            {
                var episode = episodes.Where(e => e.Id == episodeId).FirstOrDefault();
                if (episode != null)
                {
                    return episodes.Where(e => e.EndDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).FirstOrDefault();
                }
            }
            return null;
        }

        //internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, DateTime date)
        //{
        //    var script = @"SELECT patientepisodes.Id as Id, patientepisodes.PatientId as PatientId, patientepisodes.AdmissionId as AdmissionId, " +
        //        "patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate, patientepisodes.Schedule as Schedule " +
        //        "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
        //        "AND DATE(patientepisodes.EndDate) < DATE(@date) AND patientepisodes.isactive = 1 AND patientepisodes.IsDischarged = 0 " +
        //        "ORDER BY patientepisodes.StartDate DESC LIMIT 1;";

        //    var patientEpisode = new EpisodeData();
        //    using (var cmd = new FluentCommand<EpisodeData>(script))
        //    {
        //        patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddGuid("patientid", patientId)
        //        .AddDateTime("date", date)
        //        .SetMap(reader => new EpisodeData
        //        {
        //            AgencyId = agencyId,
        //            Id = reader.GetGuid("Id"),
        //            AdmissionId = reader.GetGuid("AdmissionId"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EndDate = reader.GetDateTime("EndDate"),
        //            StartDate = reader.GetDateTime("StartDate"),
        //            Schedule = reader.GetStringNullable("Schedule")
        //        })
        //        .AsSingle();
        //    }
        //    return patientEpisode;
        //}

        internal static List<EpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            var list = new List<EpisodeData>();
            var script = @"SELECT patientepisodes.Id, patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
                "AND patientepisodes.IsActive = 1";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenNoOASIS(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, bool isBillableOnly)
        {
            var billableScript = string.Empty;
            if (isBillableOnly)
            {
                billableScript = " agencymanagement.scheduleevents.IsBillable = 1 AND ";
            }
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable 
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    (Date(agencymanagement.patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(agencymanagement.patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) AND 
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", billableScript);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] {  8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber,
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patients.AddressStateCode as AddressStateCode,
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                oasisc.assessments.OasisData as Note , 
                oasisc.assessments.HippsCode as HippsCode
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                            LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((Date(agencymanagement.patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(agencymanagement.patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) OR ( DATE(agencymanagement.patientepisodes.EndDate) between DATE(@backstartDate) and DATE(@startdate) AND DATE(agencymanagement.scheduleevents.EventDate) between DATE(DATE_SUB(agencymanagement.patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(agencymanagement.patientepisodes.EndDate) AND agencymanagement.scheduleevents.DisciplineTask IN ( {0}) ) ) AND 
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", oasisDisciplineTasks);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartDate", startDate.AddDays(-59))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressZipCode=reader.GetStringNullable("AddressStateCode"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Note = reader.GetStringNullable("Note"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASISForHHRG(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] { 8, 66, 73, 90, 9, 62, 69 };
         
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber,
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patients.AddressStateCode as AddressStateCode,
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patientepisodes.Details as EpisodeNotes ,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                oasisc.assessments.OasisData as Note , 
                oasisc.assessments.SubmissionFormat as SubmissionFormat , 
                oasisc.assessments.HippsCode as HippsCode
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                            LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((Date(agencymanagement.patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(agencymanagement.patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) OR ( DATE(agencymanagement.patientepisodes.EndDate) between DATE(@backstartDate) and DATE(@startdate) AND DATE(agencymanagement.scheduleevents.EventDate) between DATE(DATE_SUB(agencymanagement.patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(agencymanagement.patientepisodes.EndDate) AND agencymanagement.scheduleevents.DisciplineTask IN ( {0}) ) ) AND 
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", oasisDisciplineTasks);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartDate", startDate.AddDays(-59))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressStateCode"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Note = reader.GetStringNullable("Note"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASISLean(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = new int[] { 8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                oasisc.assessments.OasisData as Note , 
                oasisc.assessments.HippsCode as HippsCode
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                            LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                WHERE
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    ((Date(agencymanagement.patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(agencymanagement.patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) OR ( DATE(agencymanagement.patientepisodes.EndDate) between DATE(@backstartDate) and DATE(@startdate) AND DATE(agencymanagement.scheduleevents.EventDate) between DATE(DATE_SUB(agencymanagement.patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(agencymanagement.patientepisodes.EndDate) AND agencymanagement.scheduleevents.DisciplineTask IN ( {0}) ) ) AND 
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0", oasisDisciplineTasks);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartDate", startDate.AddDays(-59))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable")
                })
                .AsList();
            }
            return list.ToList();
        }

//        internal static List<EpisodeData> GetEpisodesExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
//        {
//            var list = new List<EpisodeData>();
//            var script =
//                @"SELECT 
//                        patientepisodes.Id as EpisodeId, 
//                        patients.Id as PatientId, 
//                        patients.DOB as DOB,
//                        patients.FirstName as FirstName,
//                        patients.LastName as LastName, 
//                        patients.Status as Status, 
//                        patients.MiddleInitial as MiddleInitial, 
//                        patients.DischargeDate as DischargeDate,
//                        patientepisodes.AdmissionId as AdmissionId,
//                        patients.MedicareNumber as MedicareNumber,
//                        patients.MedicaidNumber as MedicaidNumber,
//                        patients.AddressLine1 as AddressLine1, 
//                        patients.AddressLine2 as AddressLine2,
//                        patients.AddressCity as AddressCity,
//                        patients.PhoneHome as PhoneHome, 
//                        patients.AddressStateCode as AddressStateCode,
//                        patients.AddressZipCode as AddressZipCode, 
//                        patientepisodes.EndDate as EndDate, 
//                        patientepisodes.StartDate as StartDate,
//                        patientepisodes.Schedule as Schedule,
//                        patientepisodes.Details as Details, 
//                        patients.PatientIdNumber,
//                        patients.StartofCareDate
//                            FROM 
//                                patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                    WHERE 
//                                       
//                                        patientepisodes.AgencyId = @agencyid AND
//                                        patients.AgencyLocationId = @agencylocationid  AND
//                                        (patients.Status = 1 || patients.Status = 2) AND 
//                                        patients.IsDeprecated = 0 AND
//                                        patientepisodes.IsActive = 1 AND
//                                        patientepisodes.IsDischarged = 0 AND 
//                                        (Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) ORDER BY patients.LastName ASC";

//            using (var cmd = new FluentCommand<EpisodeData>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("agencylocationid", agencyLocationId)
//                .AddDateTime("enddate", endDate)
//                .AddDateTime("startdate", startDate)
//                .SetMap(reader => new EpisodeData
//                {
//                    AgencyId = agencyId,
//                    Status = reader.GetInt("Status"),
//                    Id = reader.GetGuid("EpisodeId"),
//                    AdmissionId = reader.GetGuid("AdmissionId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    DOB = reader.GetDateTime("DOB"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
//                    Schedule = reader.GetStringNullable("Schedule"),
//                    Details = reader.GetStringNullable("Details"),
//                    LastName = reader.GetString("LastName"),
//                    FirstName = reader.GetString("FirstName"),
//                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
//                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
//                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
//                    DischargeDate = reader.GetDateTime("DischargeDate"),
//                    MRN = reader.GetStringNullable("PatientIdNumber"),
//                    AddressZipCode = reader.GetStringNullable("AddressZipCode")
//                })
//                .AsList();
//            }
//            return list.ToList();
//        }

//        internal static List<EpisodeData> GetEpisodesExactlyBetweenLean(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
//        {
//            var list = new List<EpisodeData>();
//            var script =
//                @"SELECT 
//                        patientepisodes.Id as EpisodeId, 
//                        patients.Id as PatientId, 
//                        patients.FirstName as FirstName,
//                        patients.LastName as LastName, 
//                        patients.Status as Status, 
//                        patients.MiddleInitial as MiddleInitial, 
//                        patientepisodes.EndDate as EndDate, 
//                        patientepisodes.StartDate as StartDate,
//                        patientepisodes.Details as Details, 
//                        patients.PatientIdNumber
//                            FROM 
//                                patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                    WHERE 
//                                       
//                                        patientepisodes.AgencyId = @agencyid AND
//                                        patients.AgencyLocationId = @agencylocationid  AND
//                                        patients.Status IN (1,2) AND 
//                                        patients.IsDeprecated = 0 AND
//                                        patientepisodes.IsActive = 1 AND
//                                        patientepisodes.IsDischarged = 0 AND 
//                                        (Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate))";

//            using (var cmd = new FluentCommand<EpisodeData>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("agencylocationid", agencyLocationId)
//                .AddDateTime("enddate", endDate)
//                .AddDateTime("startdate", startDate)
//                .SetMap(reader => new EpisodeData
//                {
//                    AgencyId = agencyId,
//                    Status = reader.GetInt("Status"),
//                    Id = reader.GetGuid("EpisodeId"),
//                    AdmissionId = reader.GetGuid("AdmissionId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    DOB = reader.GetDateTime("DOB"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
//                    Schedule = reader.GetStringNullable("Schedule"),
//                    Details = reader.GetStringNullable("Details"),
//                    LastName = reader.GetString("LastName"),
//                    FirstName = reader.GetString("FirstName"),
//                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
//                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
//                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
//                    DischargeDate = reader.GetDateTime("DischargeDate"),
//                    MRN = reader.GetStringNullable("PatientIdNumber"),
//                    AddressZipCode = reader.GetStringNullable("AddressZipCode")
//                })
//                .AsList();
//            }
//            return list.ToList();
//        }


//        internal static List<EpisodeData> GetEpisodesStartDateExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
//        {
//            var list = new List<EpisodeData>();
//            var script =
//                @"SELECT
//                    patientepisodes.Id as EpisodeId,
//                    patients.Id as PatientId, 
//                    patients.DOB as DOB,
//                    patients.FirstName as FirstName, 
//                    patients.LastName as LastName, 
//                    patients.Status as Status,
//                    patients.MiddleInitial as MiddleInitial,
//                    patients.DischargeDate as DischargeDate,
//                    patientepisodes.AdmissionId as AdmissionId,
//                    patients.MedicareNumber as MedicareNumber,
//                    patients.MedicaidNumber as MedicaidNumber,
//                    patients.AddressLine1 as AddressLine1, 
//                    patients.AddressLine2 as AddressLine2,
//                    patients.AddressCity as AddressCity, 
//                    patients.PhoneHome as PhoneHome, 
//                    patients.AddressStateCode as AddressStateCode,
//                    patients.AddressZipCode as AddressZipCode,
//                    patientepisodes.EndDate as EndDate, 
//                    patientepisodes.StartDate as StartDate, 
//                    patientepisodes.Schedule as Schedule,
//                    patientepisodes.Details as Details,
//                    patients.PatientIdNumber, patients.StartofCareDate
//                        FROM
//                            patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                WHERE 
//                                    patientepisodes.AgencyId = @agencyid AND
//                                    patients.AgencyLocationId = @agencylocationid  AND
//                                    (patients.Status = 1 || patients.Status = 2) AND
//                                    patients.IsDeprecated = 0 AND
//                                    patientepisodes.IsActive = 1 AND
//                                    patientepisodes.IsDischarged = 0 AND 
//                                    (Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate)) ORDER BY patients.LastName ASC";

//            using (var cmd = new FluentCommand<EpisodeData>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("agencylocationid", agencyLocationId)
//                .AddDateTime("enddate", endDate)
//                .AddDateTime("startdate", startDate)
//                .SetMap(reader => new EpisodeData
//                {
//                    AgencyId = agencyId,
//                    Status = reader.GetInt("Status"),
//                    Id = reader.GetGuid("EpisodeId"),
//                    AdmissionId = reader.GetGuid("AdmissionId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    DOB = reader.GetDateTime("DOB"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
//                    Schedule = reader.GetStringNullable("Schedule"),
//                    Details = reader.GetStringNullable("Details"),
//                    LastName = reader.GetString("LastName"),
//                    FirstName = reader.GetString("FirstName"),
//                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
//                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
//                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
//                    DischargeDate = reader.GetDateTime("DischargeDate"),
//                    MRN = reader.GetStringNullable("PatientIdNumber"),
//                    AddressZipCode = reader.GetStringNullable("AddressZipCode")
//                })
//                .AsList();
//            }
//            return list.ToList();
//        }

        internal static CbsaCode GetCbsaCode(string zipCode)
        {
            var cbsaCode = new CbsaCode();
            var script = string.Format("SELECT * FROM `cbsacodes` WHERE `zip` = '{0}' limit 0, 1;", zipCode);

            using (var cmd = new FluentCommand<CbsaCode>(script))
            {
                cbsaCode = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new CbsaCode
                    {
                        CBSA = reader.GetStringNullable("CBSA"),
                        WITwoTen = reader.GetDouble("WITwoTen"),
                        WITwoEleven = reader.GetDouble("WITwoEleven"),
                        WITwoTwelve = reader.GetDouble("WITwoTwelve")
                    }).AsSingle();
            }

            return cbsaCode;
        }

        internal static HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            var result = new HippsAndHhrg();
            var list = new List<HippsAndHhrg>();
            var script = string.Format("SELECT * FROM `hippsandhhrgs` WHERE `HIPPS` = '{0}';", hippsCode);

            using (var cmd = new FluentCommand<HippsAndHhrg>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new HippsAndHhrg
                    {
                        HHRG = reader.GetStringNullable("HHRG"),
                        HIPPS = reader.GetStringNullable("HIPPS"),
                        HHRGWeight = reader.GetDouble("HHRGWeight"),
                        Time = reader.GetDateTime("Time")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }
            return result;
        }

        internal static PPSStandard GetPPSStandardByYear(int year)
        {
            var result = new PPSStandard();
            var list = new List<PPSStandard>();

            var script = @"SELECT * FROM `ppsstandards`;";
            using (var cmd = new FluentCommand<PPSStandard>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new PPSStandard
                    {
                        Time = reader.GetDateTime("Time"),
                        UrbanRate = reader.GetDouble("UrbanRate"),
                        RuralRate = reader.GetDouble("RuralRate"),
                        Labor = reader.GetDouble("Labor"),
                        NonLabor = reader.GetDouble("NonLabor"),
                        S = reader.GetDouble("S"),
                        RuralS = reader.GetDouble("RuralS"),
                        T = reader.GetDouble("T"),
                        RuralT = reader.GetDouble("RuralT"),
                        U = reader.GetDouble("U"),
                        RuralU = reader.GetDouble("RuralU"),
                        V = reader.GetDouble("V"),
                        RuralV = reader.GetDouble("RuralV"),
                        W = reader.GetDouble("W"),
                        RuralW = reader.GetDouble("RuralW"),
                        X = reader.GetDouble("X"),
                        RuralX = reader.GetDouble("RuralX")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }

            return result;
        }

        internal static Claim GetRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var rap = new Claim();
            var script = string.Format("SELECT * FROM `raps` WHERE `agencyid` = '{0}' AND `patientid` = '{1}' AND `episodeid` = '{2}' limit 0, 1;", agencyId, patientId, episodeId);

            using (var cmd = new FluentCommand<Claim>(script))
            {
                rap = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsSingle();
            }

            return rap;
        }

        internal static Claim GetFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var final = new Claim();
            var script = string.Format("SELECT * FROM `finals` WHERE `agencyid` = '{0}' AND `patientid` = '{1}' AND `episodeid` = '{2}' limit 0, 1;", agencyId, patientId, episodeId);

            using (var cmd = new FluentCommand<Claim>(script))
            {
                final = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsSingle();
            }

            return final;
        }

        internal static List<Claim> GetRaps(Guid agencyId, List<Guid> rapIds)
        {
            var raps = new List<Claim>();
            var script = string.Format("SELECT * FROM `raps` WHERE `agencyid` = '{0}' AND `Id` IN ( {1}) ;", agencyId, rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                raps = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return raps;
        }

        internal static List<Claim> GetFinals(Guid agencyId, List<Guid> finalIds)
        {
            var finals = new List<Claim>();
            var script = string.Format("SELECT * FROM `finals` WHERE `agencyid` = '{0}' AND `Id` IN ( {1} ) ;", agencyId, finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                finals = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return finals;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var list = new List<AdmissionPeriod>();
            var script = @"select * from patientadmissiondates WHERE `AgencyId` = @agencyid and `StartOfCareDate` between @start and @end";

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("start", startDate)
                .AddDateTime("end", endDate)
                .AsList();
            }

            return list;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, List<Guid> Ids)
        {
            var list = new List<AdmissionPeriod>();
            var script = string.Format(@"select Id , PatientId , DischargedDate , StartOfCareDate , Status  from patientadmissiondates WHERE `AgencyId` = @agencyid AND `Id` IN ({0}) ", Ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new AdmissionPeriod
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     DischargedDate = reader.GetDateTime("DischargedDate"),
                     StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                     Status = reader.GetInt("Status")
                 })
                .AsList();
            }

            return list;
        }


        internal static List<InsuranceData> GetAgencyInsurances(Guid agencyId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }

        internal static List<InsuranceData> GetAgencyInsurancesByBranch(Guid agencyId, Guid branchId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }

        internal static InsuranceData GetMedicareInsurance(int id)
        {
            var script = string.Format("SELECT * FROM `insurances` WHERE `Id` = '{0}';", id);
            var insurance = new InsuranceData();
            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                insurance = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name")
                    }).AsSingle();
            }
            return insurance;
        }

//        internal static List<EpisodeData> GetEpisodesBetweenByInsurance(Guid agencyId, Guid agencyLocationId, int insurance ,DateTime startDate, DateTime endDate)
//        {
//            var list = new List<EpisodeData>();
//            var script =
//                @"SELECT
//                    patientepisodes.Id as EpisodeId,
//                    patients.Id as PatientId, 
//                    patients.FirstName as FirstName,
//                    patients.LastName as LastName,
//                    patients.Status as Status,
//                    patients.PatientIdNumber as PatientIdNumber,
//                    patients.MiddleInitial as MiddleInitial, 
//                    patients.DischargeDate as DischargeDate, 
//                    patients.MedicareNumber as MedicareNumber,
//                    patients.MedicaidNumber as MedicaidNumber, 
//                    patients.AddressLine1 as AddressLine1, 
//                    patients.AddressLine2 as AddressLine2,
//                    patients.AddressCity as AddressCity, 
//                    patients.PhoneHome as PhoneHome, 
//                    patients.AddressStateCode as AddressStateCode,
//                    patients.AddressZipCode as AddressZipCode, 
//                    patientepisodes.EndDate as EndDate, 
//                    patientepisodes.StartDate as StartDate,
//                    patientepisodes.Schedule as Schedule,
//                    patientepisodes.Details as Details  
//                        FROM
//                            patientepisodes 
//                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id
//                                    WHERE
//                                        
//                                        patientepisodes.AgencyId = @agencyid AND 
//                                        patients.AgencyLocationId = @agencylocationid  AND
//                                        (patients.Status = 1 || patients.Status = 2) AND 
//                                        patients.IsDeprecated = 0 AND
//                                        patientepisodes.IsActive = 1 AND
//                                        patientepisodes.IsDischarged = 0 AND
//                                        Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(patients.DOB as DATETIME)))) >= 18 AND
//                                        (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate BETWEEN @startdate AND @enddate)) AND
//                                        (patients.PrimaryInsurance = @insurance OR patients.SecondaryInsurance = @insurance OR patients.TertiaryInsurance = @insurance)
//                                           ORDER BY patientepisodes.StartDate ASC";

//            using (var cmd = new FluentCommand<EpisodeData>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("agencylocationid", agencyLocationId)
//                .AddInt("insurance", insurance)
//                .AddDateTime("enddate", endDate)
//                .AddDateTime("startdate", startDate)
//                .SetMap(reader => new EpisodeData
//                {
//                    AgencyId = agencyId,
//                    Id = reader.GetGuid("EpisodeId"),
//                    MRN = reader.GetStringNullable("PatientIdNumber"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    Schedule = reader.GetStringNullable("Schedule"),
//                    Details = reader.GetStringNullable("Details"),
//                    Status = reader.GetInt("Status"),
//                    LastName = reader.GetString("LastName"),
//                    FirstName = reader.GetString("FirstName"),
//                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
//                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
//                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
//                    AddressLine1 = reader.GetString("AddressLine1"),
//                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
//                    AddressCity = reader.GetString("AddressCity"),
//                    PhoneHome = reader.GetStringNullable("PhoneHome"),
//                    AddressStateCode = reader.GetString("AddressStateCode"),
//                    AddressZipCode = reader.GetString("AddressZipCode"),
//                    DischargeDate = reader.GetDateTime("DischargeDate")
//                })
//                .AsList();
//            }
//            return list.ToList();
//        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsForManagedClaimBill(Guid agencyId, Guid agencyLocationId, int insurance, DateTime startDate, DateTime endDate, bool isBillableOnly, List<ScheduleEvent> allBilledVisitsIntheRange)
        {
            var isbillableScript = string.Empty;
            if (isBillableOnly)
            {
                isbillableScript = " agencymanagement.scheduleevents.IsBillable = 1 AND ";
            }
            var billedVisitScript = string.Empty;
            if (allBilledVisitsIntheRange != null && allBilledVisitsIntheRange.Count > 0)
            {
                billedVisitScript = string.Format(" agencymanagement.scheduleevents.EventId NOT IN ( {0}) AND ", allBilledVisitsIntheRange.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", "));
            }
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.UserId as UserId , 
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                   
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    {0}
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    (agencymanagement.patients.PrimaryInsurance = @insurance OR agencymanagement.patients.SecondaryInsurance = @insurance OR agencymanagement.patients.TertiaryInsurance = @insurance) AND 
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate) AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    {1}
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0 ", billedVisitScript, isbillableScript);


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddInt("insurance", insurance)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ManagedClaimData> GetManagedClaimsByInsurance(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                            managedclaims.Id as Id,
                            managedclaims.PatientId as PatientId,
                            managedclaims.EpisodeStartDate as StartDate,
                            managedclaims.EpisodeEndDate as EndDate,
                            managedclaims.VerifiedVisits as VerifiedVisits,
                                FROM 
                                    managedclaims 
                                        INNER JOIN patients ON managedclaims.PatientId = patients.Id 
                                            WHERE 
                                                managedclaims.AgencyId = @agencyid {0} AND
                                                patients.Status IN (1,2) AND
                                                patients.IsDeprecated = 0 AND
                                                (patients.PrimaryInsurance = @insurance OR patients.SecondaryInsurance = @insurance OR patients.TertiaryInsurance = @insurance) AND
                                                DATE(managedclaims.EpisodeEndDate) between DATE(@startdate) and DATE(@enddate)
                                                    ORDER BY  managedclaims.EpisodeStartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            var list = new List<ManagedClaimData>();
            using (var cmd = new FluentCommand<ManagedClaimData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insurance", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ManagedClaimData
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     VerifiedVisits = reader.GetStringNullable("VerifiedVisits")
                     
                 })
                 .AsList();
            }
            return list;
        }

        internal static List<CbsaCode> CbsaCodesByZip(string[] zipCodes)
        {
            var list = new List<CbsaCode>();
            if (zipCodes != null && zipCodes.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        Zip as Zip, 
                                        CBSA as CBSA, 
                                        WITwoSeven as WITwoSeven,
                                        WITwoEight as WITwoEight ,
                                        WITwoNine as WITwoNine , 
                                        WITwoTen as WITwoTen,
                                        WITwoEleven as WITwoEleven,
                                        WITwoTwelve as WITwoTwelve  
                                          FROM 
                                               cbsacodes 
                                                        WHERE Zip IN ( {0} ) ", zipCodes.Select(z => "\'" + z + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<CbsaCode>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new CbsaCode
                    {
                        Id = reader.GetInt("Id"),
                        Zip = reader.GetStringNullable("Zip"),
                        CBSA = reader.GetStringNullable("CBSA"),
                        WITwoSeven = reader.GetDouble("WITwoSeven"),
                        WITwoEight = reader.GetDouble("WITwoEight"),
                        WITwoNine = reader.GetDouble("WITwoNine"),
                        WITwoTen = reader.GetDouble("WITwoTen"),
                        WITwoEleven = reader.GetDouble("WITwoEleven"),
                        WITwoTwelve = reader.GetDouble("WITwoTwelve")
                    })
                    .AsList();
                }
            }
            return list;
        }

        internal static List<PPSStandard> PPSStandards(int[] years)
        {
            var list = new List<PPSStandard>();
            if (years != null && years.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        Time as Time, 
                                        UrbanRate as UrbanRate, 
                                        RuralRate as RuralRate,
                                        Labor as Labor,
                                        NonLabor as NonLabor , 
                                        S as S,
                                        RuralS as RuralS,
                                        T as T, 
                                        RuralT as RuralT,
                                        U as U,
                                        RuralU as RuralU , 
                                        V as V,
                                        RuralV as RuralV,
                                        W as W,
                                        RuralW as RuralW , 
                                        X as X,
                                        RuralX as RuralX  
                                          FROM 
                                               ppsstandards 
                                                        WHERE year(Time) IN ( {0} ) ", years.Select(z => z.ToString()).ToArray().Join(","));

                using (var cmd = new FluentCommand<PPSStandard>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new PPSStandard
                    {
                        Id = reader.GetInt("Id"),
                        Time = reader.GetDateTime("Time"),
                        UrbanRate = reader.GetDouble("UrbanRate"),
                        RuralRate = reader.GetDouble("RuralRate"),
                        Labor = reader.GetDouble("Labor"),
                        NonLabor = reader.GetDouble("NonLabor"),
                        S = reader.GetDouble("S"),
                        RuralS = reader.GetDouble("RuralS"),
                        T = reader.GetDouble("T"),
                        RuralT = reader.GetDouble("RuralT"),
                        U = reader.GetDouble("U"),
                        RuralU = reader.GetDouble("RuralU"),
                        V = reader.GetDouble("V"),
                        RuralV = reader.GetDouble("RuralV"),
                        W = reader.GetDouble("W"),
                        RuralW = reader.GetDouble("RuralW"),
                        X = reader.GetDouble("X"),
                        RuralX = reader.GetDouble("RuralX")
                    })
                    .AsList();
                }
            }
            return list;
        }

        internal static List<HippsAndHhrg> GetHhrgByHippsCodeAndYear(string[] hippsAndYears)
        {
            var list = new List<HippsAndHhrg>();
            if (hippsAndYears != null && hippsAndYears.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        HHRG as HHRG, 
                                        HIPPS as HIPPS, 
                                        HHRGWeight as HHRGWeight,
                                        Time as Time 
                                          FROM 
                                               hippsandhhrgs 
                                                        WHERE concat(HIPPS,year(Time)) IN ( {0} ) ", hippsAndYears.Select(z => "\'" + z + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<HippsAndHhrg>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new HippsAndHhrg
                    {
                        Id = reader.GetInt("Id"),
                        HHRG = reader.GetStringNullable("HHRG"),
                        HIPPS = reader.GetStringNullable("HIPPS"),
                        HHRGWeight = reader.GetDouble("HHRGWeight"),
                        Time = reader.GetDateTime("Time")
                    })
                    .AsList();
                }
            }
            return list;
        }


        #endregion

        #region Helper Functions

        internal static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month, 1, 0, 0, 0, 0);
        }

        internal static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month,
               DateTime.DaysInMonth(Year, (int)Month), 23, 59, 59, 999);
        }

        internal static DateTime GetStartOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetStartOfMonth(12, Year - 1);
            else
                return GetStartOfMonth(Month - 1, Year);
        }

        internal static DateTime GetEndOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetEndOfMonth(12, Year - 1);
            else
                return GetEndOfMonth(Month - 1, Year);
        }

        public static IDictionary<string, Question> ToOASISDictionary( ScheduleEvent scheduleEvent)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
            {
                var key = string.Empty;
                var oasisQuestions = scheduleEvent.Note.ToObject<List<Question>>();
                if (oasisQuestions != null && oasisQuestions.Count > 0)
                {
                    oasisQuestions.ForEach(question =>
                    {
                        if (question.Type == QuestionType.Moo)
                        {
                            key = string.Format("{0}{1}", question.Code, question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.PlanofCare)
                        {
                            key = string.Format("485{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.Generic)
                        {
                            key = string.Format("Generic{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else
                        {
                            key = string.Format("{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                    });
                }
            }
            return questions;
        }

        #endregion
    }
}
