﻿namespace Axxess.Membership.Enums
{
    public enum LoginAttemptType
    {
        Failed,
        Success,
        Locked,
        Deactivated,
        TrialPeriodOver,
        AccountSuspended,
        TooManyAttempts,
        WeekendAccessRestricted,
        AccountInUse,
        AgencyFrozen
    }
}
