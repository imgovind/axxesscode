﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class SettingRepository : ISettingRepository
    {
        #region Constructor

        //private ICoreDataContext db;

        //public SettingRepository(ICoreDataContext dataContext)
        //{
        //    Check.Argument.IsNotNull(dataContext, "dataContext");
        //    this.db = dataContext;
        //}

        #endregion

        #region ISettingRepository Members

        //public Guid Add(Guid applicationId, string name, byte categoryId, string description, XElement data)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationID");
        //    Check.Argument.IsNotNull(name, "name");
        //    Check.Argument.IsNotNegative(categoryId, "categoryId");

        //    Setting settingInfo = new Setting()
        //    {
        //        Name = name,
        //        ApplicationId = applicationId,
        //        CategoryId = categoryId,
        //        Description = description,
        //        EnvironmentData = data,
        //        Created = DateTime.Now,
        //        Modified = DateTime.Now
        //    };

        //    db.Insert<Setting>(settingInfo);
        //    db.SubmitChanges();

        //    return settingInfo.Id;
        //}

        //public ICollection<Setting> GetSettings(Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationID");

        //    return db.SettingsDataSource.Where(s => s.ApplicationId == applicationId)
        //        .Cast<Setting>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<Setting> GetSettings(byte categoryId)
        //{
        //    Check.Argument.IsNotNegative(categoryId, "categoryId");

        //    return db.SettingsDataSource.Where(s => s.CategoryId == categoryId)
        //        .Cast<Setting>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public bool Update(Guid settingId, Guid applicationId, string name, byte categoryId, string description, XElement data)
        //{
        //    Check.Argument.IsNotEmpty(settingId, "settingId");
        //    Check.Argument.IsNotEmpty(applicationId, "applicationID");
        //    Check.Argument.IsNotNull(name, "name");
        //    Check.Argument.IsNotNegative(categoryId, "categoryId");

        //    bool result = false;
        //    Setting setting = db.SettingsDataSource.SingleOrDefault(s => s.Id == settingId);

        //    if (setting != null)
        //    {
        //        setting.ApplicationId = applicationId;
        //        setting.Name = name;
        //        setting.CategoryId = categoryId;
        //        setting.Description = description;
        //        setting.EnvironmentData = data;
        //        setting.Modified = DateTime.Now;
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool Delete(Guid settingId)
        //{
        //    Check.Argument.IsNotEmpty(settingId, "settingId");

        //    bool result = false;
        //    Setting setting = db.SettingsDataSource.SingleOrDefault(s => s.Id == settingId);
        //    if (setting != null)
        //    {
        //        db.Delete(setting);
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public ICollection<Setting> GetAll()
        //{
        //    return db.SettingsDataSource
        //        .Cast<Setting>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public Setting Find(Guid settingId)
        //{
        //    Check.Argument.IsNotEmpty(settingId, "settingId");

        //    return db.SettingsDataSource.SingleOrDefault(s => s.Id == settingId);
        //}

        //public ICollection<Setting> GetByName(string name)
        //{
        //    Check.Argument.IsNotNull(name, "name");

        //    return db.SettingsDataSource.Where(s => s.Name == name)
        //        .Cast<Setting>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        #endregion
    }
}
