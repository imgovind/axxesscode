﻿namespace Axxess.Membership.Domain
{
    using System;

    public class Audit
    {
        public long Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid ApplicationId { get; set; }
        public string ActionLog { get; set; }
        public DateTime Created { get; set; }
    }
}
