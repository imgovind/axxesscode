﻿namespace Axxess.Api.Membership
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller authenticationInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.authenticationInstaller = new ServiceInstaller();
            this.authenticationInstaller.StartType = ServiceStartMode.Automatic;
            this.authenticationInstaller.ServiceName = "AuthenticationService";
            this.authenticationInstaller.DisplayName = "Authentication Service";
            this.authenticationInstaller.Description = "Provides Single Sign-on services for Axxess Applications.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.authenticationInstaller });
        }
    }
}
