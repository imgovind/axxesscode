﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;

    public class PhysicianService : IPhysicianService
    {
        #region Constructor

        private readonly ILoginRepository loginRepository;
        private readonly IPhysicianRepository physicianRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Public Methods

        public bool CreatePhysician(AgencyPhysician physician)
        {
            physician.Id = Guid.NewGuid();
            if (physician.PhysicianAccess)
            {
                var isNewLogin = false;
                var login = loginRepository.Find(physician.EmailAddress.Trim());
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = physician.DisplayName;
                    login.EmailAddress = physician.EmailAddress.Trim();
                    login.Role = Roles.Physician.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                physician.LoginId = login.Id;

                if (physicianRepository.Add(physician))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", Current.AgencyName);

                    if (isNewLogin)
                    {
                        var parameters = string.Format("loginid={0}", physician.LoginId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName);
                    }

                    Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianAdded, string.Empty);
                    return true;
                }
            }
            else
            {
                if (physicianRepository.Add(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianAdded, string.Empty);
                    return true;
                }
            }

            return false;
        }

        public bool UpdatePhysician(AgencyPhysician physician)
        {
            if (physician.PhysicianAccess)
            {
                var isNewLogin = false;
                var login = loginRepository.Find(physician.EmailAddress.Trim());
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = physician.DisplayName;
                    login.EmailAddress = physician.EmailAddress.Trim();
                    login.Role = Roles.Physician.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                physician.LoginId = login.Id;

                if (physicianRepository.Update(physician))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use Axxess Home Health Software", Current.AgencyName);

                    if (isNewLogin)
                    {
                        var parameters = string.Format("loginid={0}", physician.LoginId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName);
                    }

                    Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianUpdated, string.Empty);
                    return true;
                }
            }
            else
            {
                if (physicianRepository.Update(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianUpdated, string.Empty);
                    return true;
                }
            }

            return false;
        }

        public bool AddLicense(PhysicainLicense license)
        {
            var result = false;
            var physician = physicianRepository.Get(license.PhysicainId, Current.AgencyId);
            if (physician != null)
            {
                license.Id = Guid.NewGuid();
                license.Created = DateTime.Now;
                license.Modified = DateTime.Now;
                if (physician.Licenses.IsNotNullOrEmpty())
                {
                    physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                }
                else
                {
                    physician.LicensesArray = new List<PhysicainLicense>();
                }
                physician.LicensesArray.Add(license);
                physician.Licenses = physician.LicensesArray.ToXml();
                if (physicianRepository.Update(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateLicense(Guid id, Guid physicianId, DateTime ExpirationDate)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    var license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        license.ExpirationDate = ExpirationDate;
                        physician.Licenses = physician.LicensesArray.ToXml();
                        if (physicianRepository.Update(physician))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseUpdated, string.Empty);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    var license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        physician.LicensesArray.Remove(license);
                        physician.Licenses = physician.LicensesArray.ToXml();
                        if (physicianRepository.Update(physician))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseDeleted, string.Empty);
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
