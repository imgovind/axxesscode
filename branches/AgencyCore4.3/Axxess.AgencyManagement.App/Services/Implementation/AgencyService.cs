﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Extensions;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;

    using Enums;
    using Domain;
    using Common;
    using ViewData;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Common;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IAssessmentService assessmentService;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAssessmentService assessmentService, ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
            this.assessmentService = assessmentService;
        }
       
        #region Agency

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    if (zipCode != null)
                    {
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    }
                    var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                    if (defaultDisciplineTasks != null)
                    {
                        var costRates = new List<ChargeRate>();
                        defaultDisciplineTasks.ForEach(r =>
                        {
                            if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
                            {
                                costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
                            }
                        });
                        location.BillData = costRates.ToXml();
                    }
                    //location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
                    location.Id = Guid.NewGuid();
                    if (agencyRepository.AddLocation(location))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AllowWeekendAccess = true,
                            AgencyName = agency.Name,
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        #endregion

        #region Location

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        #endregion

        #region Contact

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        #endregion

        #region Case Manager

        public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var events = new List<PatientEpisodeEvent>();
            var scheduleEvents = scheduleRepository.GetScheduleByBranchDateRangeAndStatus(Current.AgencyId, branchId,startDate,endDate, status, ScheduleStatusFactory.CaseManagerStatus().ToArray(), true);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
                var userIds = new List<Guid>();
                var users = new List<User>();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        userIds.AddRange(returnUserIds);
                    }
                }
                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                {
                    userIds.AddRange(scheduleUserIds);

                }
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                }
                scheduleEvents.ForEach(s =>
                {
                    var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == s.EpisodeId).ToList() ?? new List<ReturnComment>();
                    var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

                    var userName = string.Empty;
                    if (!s.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            userName = user.DisplayName;
                        }
                    }
                    var details = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    
                    events.Add(new PatientEpisodeEvent
                    {
                        EventId = s.EventId,
                        EpisodeId = s.EpisodeId,
                        PatientId = s.PatientId,
                        PrintUrl = Common.Url.Print(s, false),
                        Status = s.StatusName,
                        PatientName = s.PatientName,
                        TaskName = s.DisciplineTaskName,
                        UserName = userName,
                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                        RedNote = redNote,
                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                        EventDate = s.EventDate.IsValid() ? s.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)

                    });
                });
            }

            //var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, branchId, status, startDate, endDate);
            //if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
            //{
            //    var episodeWithEventsDictionary = agencyPatientEpisodes.ToDictionary(g => g.Id,
            //        g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
            //              !s.IsDeprecated
            //              && s.EventDate.IsValidDate()
            //              && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
            //              && s.EventDate.ToDateTime().IsBetween(startDate, endDate)
            //              && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
            //                            || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
            //                            || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
            //                            || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
            //                            || s.IsMissedVisit)
            //          ).ToList() : new List<ScheduleEvent>()
            //         );
            //    if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            //    {
            //        var episodeIds = episodeWithEventsDictionary.Keys.ToList();
            //        var userIds = new List<Guid>();
            //        var users = new List<User>();
            //        var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
            //        if (returnComments != null && returnComments.Count > 0)
            //        {
            //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
            //            if (returnUserIds != null && returnUserIds.Count > 0)
            //            {
            //                userIds.AddRange(returnUserIds);
            //            }
            //        }
            //        var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
            //        if (scheduleUserIds != null && scheduleUserIds.Count > 0)
            //        {
            //            userIds.AddRange(scheduleUserIds);

            //        }
            //        if (userIds != null && userIds.Count > 0)
            //        {
            //            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
            //        }
            //        var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
            //        var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

            //        episodeWithEventsDictionary.ForEach((key, value) =>
            //        {
            //            if (value != null && value.Count > 0)
            //            {
            //                var episode = agencyPatientEpisodes.FirstOrDefault(e => e.Id == key);
            //                if (episode != null)
            //                {
            //                    value.ForEach(s =>
            //                    {
            //                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
            //                        var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

            //                        var userName = string.Empty;
            //                        if (!s.UserId.IsEmpty())
            //                        {
            //                            var user = users.FirstOrDefault(u => u.Id == s.UserId);
            //                            if (user != null)
            //                            {
            //                                userName = user.DisplayName;
            //                            }
            //                        }
            //                        if (s.IsMissedVisit)
            //                        {
            //                            var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
            //                            if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
            //                            {
            //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
            //                                Common.Url.Set(s, false, false);
            //                                events.Add(new PatientEpisodeEvent
            //                                {
            //                                    EventId = s.EventId,
            //                                    EpisodeId = s.EpisodeId,
            //                                    PatientId = s.PatientId,
            //                                    PrintUrl = s.PrintUrl,
            //                                    Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
            //                                    PatientName = episode.DisplayName,
            //                                    TaskName = s.DisciplineTaskName,
            //                                    UserName = userName,
            //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
            //                                    RedNote = redNote,
            //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
            //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                                });
            //                            }
            //                        }
            //                        else
            //                        {
            //                            if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
            //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
            //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
            //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
            //                            {
            //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
            //                                Common.Url.Set(s, false, false);
            //                                events.Add(new PatientEpisodeEvent
            //                                {
            //                                    EventId = s.EventId,
            //                                    EpisodeId = s.EpisodeId,
            //                                    PatientId = s.PatientId,
            //                                    PrintUrl = s.PrintUrl,
            //                                    Status = s.StatusName,
            //                                    PatientName = episode.DisplayName,
            //                                    TaskName = s.DisciplineTaskName,
            //                                    UserName = userName,
            //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
            //                                    RedNote = redNote,
            //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
            //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                                });
            //                            }
            //                        }
            //                    });
            //                }
            //            }
            //        });
            //    }
            //}
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        //public List<PatientEpisodeEvent> GetPatientCaseManagerSchedule(Guid patientId)
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var patientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, patientId);
        //    if (patientEpisodes != null && patientEpisodes.Count > 0)
        //    {
        //        var episodeWithEventsDictionary = patientEpisodes.ToDictionary(g => g.Id,
        //            g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
        //                  !s.IsDeprecated
        //                  && s.EventDate.IsValidDate()
        //                  && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
        //                  && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
        //                                || s.IsMissedVisit)
        //              ).ToList() : new List<ScheduleEvent>()
        //             );
        //        if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
        //        {
        //            var episodeIds = episodeWithEventsDictionary.Keys.ToList();
        //            var userIds = new List<Guid>();
        //            var users = new List<User>();
        //            var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //            if (returnComments != null && returnComments.Count > 0)
        //            {
        //                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //                if (returnUserIds != null && returnUserIds.Count > 0)
        //                {
        //                    userIds.AddRange(returnUserIds);
        //                }
        //            }
        //            var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //            if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //            {
        //                userIds.AddRange(scheduleUserIds);

        //            }
        //            if (userIds != null && userIds.Count > 0)
        //            {
        //                users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();// userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
        //            }
        //            var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

        //            episodeWithEventsDictionary.ForEach((key, value) =>
        //            {
        //                if (value != null && value.Count > 0)
        //                {
        //                    var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
        //                    if (episode != null)
        //                    {
        //                        value.ForEach(s =>
        //                        {
        //                            var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
        //                            var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

        //                            var userName = string.Empty;
        //                            if (!s.UserId.IsEmpty())
        //                            {
        //                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                                if (user != null)
        //                                {
        //                                    userName = user.DisplayName;
        //                                }
        //                            }
        //                            if (s.IsMissedVisit)
        //                            {
        //                                var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
        //                                if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
        //                                {
        //                                    var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                    Common.Url.Set(s, false, false);
        //                                    events.Add(new PatientEpisodeEvent
        //                                    {
        //                                        EventId = s.EventId,
        //                                        EpisodeId = s.EpisodeId,
        //                                        PatientId = s.PatientId,
        //                                        PrintUrl = s.PrintUrl,
        //                                        Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
        //                                        TaskName = s.DisciplineTaskName,
        //                                        UserName = userName,
        //                                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                        RedNote = redNote,
        //                                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                        EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                    });
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
        //                                {
        //                                    var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                    Common.Url.Set(s, false, false);
        //                                    events.Add(new PatientEpisodeEvent
        //                                    {
        //                                        EventId = s.EventId,
        //                                        EpisodeId = s.EpisodeId,
        //                                        PatientId = s.PatientId,
        //                                        PrintUrl = s.PrintUrl,
        //                                        Status = s.StatusName,
        //                                        TaskName = s.DisciplineTaskName,
        //                                        UserName = userName,
        //                                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                        RedNote = redNote,
        //                                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                        EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                    });
        //                                }
        //                            }
        //                        });
        //                    }
        //                }
        //            });
        //        }
        //    }
        //    return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        //}

        #endregion

        #region  Past Due Recerts

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            return this.GetRecertsPastDue(Guid.Empty, 0, DateTime.Now.AddMonths(-4), DateTime.Now, true, 5);
            //return patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            //var pastDueRecerts = new List<RecertEvent>();
            //var recertEpisodes = patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            //if (recertEpisodes != null && recertEpisodes.Count > 0)
            //{
            //    recertEpisodes.ForEach(r =>
            //    {
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) &&
            //                    (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
            //                     s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome ||
            //                    s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() ||
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() ||
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
            //                {
            //                }
            //                else
            //                {
            //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
            //                    if (episodeRecertSchedule != null)
            //                    {
            //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                        {
            //                        }
            //                        else
            //                        {
            //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < DateTime.Today.Date)
            //                            {
            //                                if (!episodeRecertSchedule.UserId.IsEmpty())
            //                                {
            //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
            //                                }
            //                                r.Status = episodeRecertSchedule.StatusName;
            //                                r.Schedule = string.Empty;
            //                                r.DateDifference = DateTime.Today.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
            //                                pastDueRecerts.Add(r);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if (r.TargetDate.Date < DateTime.Today.Date)
            //                        {
            //                            r.AssignedTo = "Unassigned";
            //                            r.Status = "Not Scheduled";
            //                            r.Schedule = string.Empty;
            //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                            pastDueRecerts.Add(r);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.Date < DateTime.Today.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                    pastDueRecerts.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (r.TargetDate.Date < DateTime.Today.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                pastDueRecerts.Add(r);
            //            }
            //        }
            //    });
            //}
            //return pastDueRecerts.Take(5).ToList();
        }

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        {
            var pastDueRecerts = new List<RecertEvent>();
            var scheduleEvents = scheduleRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds != null && episodeIds.Count > 0)
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };
                    var recertAndROCStatus = dischargeStatus;
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
                    var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
                    var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    episodeIds.ForEach(e =>
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules != null)
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules.FirstOrDefault();
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5),evnt.EndDate)))
                                {
                                }
                                else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        if (user != null)
                                        {
                                            recert.AssignedTo = user.DisplayName;
                                        }
                                        else
                                        {
                                            recert.AssignedTo = "Unassigned";
                                        }
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    pastDueRecerts.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status)&& s.EventDate.IsBetween(s.EndDate.AddDays(-5),s.EndDate))))
                                {
                                }
                                else
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                        }
                                        else
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                if (user != null)
                                                {
                                                    recert.AssignedTo = user.DisplayName;
                                                }
                                                else
                                                {
                                                    recert.AssignedTo = "Unassigned";
                                                }
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            pastDueRecerts.Add(recert);
                                        }
                                    }

                                }
                            }
                        }
                        if (isLimitForWidget && pastDueRecerts.Count >= limit)
                        {
                            return;
                        }
                    });

                }
            }
            return pastDueRecerts;

            //var recertEpisodes = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate.AddDays(5));
            //var pastDueRecerts = new List<RecertEvent>();
            //if (recertEpisodes != null && recertEpisodes.Count > 0)
            //{
            //    recertEpisodes.ForEach(r =>
            //    {
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false 
            //                    && s.EventDate.IsValidDate() 
            //                    && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date)
            //                    &&((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
            //                    || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
            //                    && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() ||
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
            //                {
            //                }
            //                else
            //                {
            //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
            //                    if (episodeRecertSchedule != null)
            //                    {
            //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                        {
            //                        }
            //                        else
            //                        {
            //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < endDate.Date)
            //                            {
            //                                if (!episodeRecertSchedule.UserId.IsEmpty())
            //                                {
            //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
            //                                }
            //                                r.Status = episodeRecertSchedule.StatusName;
            //                                r.Schedule = string.Empty;
            //                                r.DateDifference = endDate.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
            //                                pastDueRecerts.Add(r);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if (r.TargetDate.Date < endDate.Date)
            //                        {
            //                            r.AssignedTo = "Unassigned";
            //                            r.Status = "Not Scheduled";
            //                            r.Schedule = string.Empty;
            //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                            pastDueRecerts.Add(r);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.Date < endDate.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                    pastDueRecerts.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (r.TargetDate.Date < endDate.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                pastDueRecerts.Add(r);
            //            }
            //        }
            //    });
            //}
            //return pastDueRecerts;
        }

        #endregion

        #region  Up Coming Recerts

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return this.GetRecertsUpcoming(Guid.Empty, 0, DateTime.Now, DateTime.Now.AddDays(24), true, 5);

            //return patientRepository.GetUpcomingRecertsWidgetLean(Current.AgencyId);
        }

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        {
            var upcomingRecets = new List<RecertEvent>();
            var scheduleEvents = scheduleRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds != null && episodeIds.Count > 0)
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCStatus = dischargeStatus; //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();//new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
                    var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
                    var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    episodeIds.ForEach(e =>
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules != null)
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules.FirstOrDefault();
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5), evnt.EndDate)))
                                {
                                }
                                else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        if (user != null)
                                        {
                                            recert.AssignedTo = user.DisplayName;
                                        }
                                        else
                                        {
                                            recert.AssignedTo = "Unassigned";
                                        }
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    upcomingRecets.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status) && s.EventDate.IsBetween(s.EndDate.AddDays(-5), s.EndDate))))
                                {
                                }
                                else
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                        }
                                        else
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                if (user != null)
                                                {
                                                    recert.AssignedTo = user.DisplayName;
                                                }
                                                else
                                                {
                                                    recert.AssignedTo = "Unassigned";
                                                }
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            upcomingRecets.Add(recert);
                                        }
                                    }
                                }
                            }
                        }
                        if (isLimitForWidget && upcomingRecets.Count >= limit)
                        {
                            return;
                        }
                    });
                }
            }
            return upcomingRecets;

            //var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            //var upcomingRecets = new List<RecertEvent>();
            //if (recetEpisodes != null && recetEpisodes.Count > 0)
            //{
            //    recetEpisodes.ForEach(r =>
            //    {
            //        //var lastFiveDaysStart=r.TargetDate.AddDays(-6);
            //        //if ((r.TargetDate.AddDays(-1).Date <= endDate && r.TargetDate.AddDays(-1).Date >= startDate) || (lastFiveDaysStart.Date >= startDate.Date && lastFiveDaysStart.Date <= endDate.Date))
            //        //{
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            var scheduleEvents = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.IsDeprecated
            //                && !s.IsMissedVisit
            //                && s.EventDate.IsValidDate()
            //                && s.EventDate.ToDateTime().Date >= r.StartDate.Date
            //                && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date
            //                && s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date
            //                && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
            //            if (scheduleEvents != null)
            //            {
            //                if ((scheduleEvents.Status == ((int)ScheduleStatus.OasisExported).ToString()
            //                    || scheduleEvents.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
            //                    || scheduleEvents.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()
            //                    || scheduleEvents.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                {
            //                }
            //                else
            //                {
            //                    if (scheduleEvents.EventDate.ToDateTime().Date >= startDate.Date && scheduleEvents.EventDate.ToDateTime().Date <= endDate.Date)
            //                    {
            //                        if (!scheduleEvents.UserId.IsEmpty())
            //                        {
            //                            r.AssignedTo = UserEngine.GetName(scheduleEvents.UserId, Current.AgencyId);
            //                        }
            //                        r.Status = scheduleEvents.StatusName;
            //                        r.Schedule = string.Empty;
            //                        r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                        upcomingRecets.Add(r);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.AddDays(-1).Date < endDate.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                    upcomingRecets.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (r.TargetDate.AddDays(-1).Date < endDate.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                upcomingRecets.Add(r);
            //            }
            //        }
            //        //}
            //    });
            //}
            //return upcomingRecets;
        }

        #endregion

        #region Orders

        public Order GetOrder(Guid id, Guid patientId, Guid episodeId, string type)
        {
            var order = new Order();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
            {
                var typeEnum = ((OrderType)type.ToInteger()).ToString();
                if (typeEnum.IsNotNullOrEmpty())
                {
                    switch (typeEnum)
                    {
                        case "PhysicianOrder":
                            var physicianOrder = patientRepository.GetOrder(id, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                order = new Order
                                {
                                    Id = physicianOrder.Id,
                                    PatientId = physicianOrder.PatientId,
                                    EpisodeId = physicianOrder.EpisodeId,
                                    Type = OrderType.PhysicianOrder,
                                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                    Number = physicianOrder.OrderNumber,
                                    PatientName = physicianOrder.DisplayName,
                                    PhysicianName = physicianOrder.PhysicianName,
                                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                                    SendDate = physicianOrder.SentDate,
                                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "FaceToFaceEncounter":
                            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                            if (faceToFaceEncounter != null)
                            {
                                order = new Order
                                {
                                    Id = faceToFaceEncounter.Id,
                                    PatientId = faceToFaceEncounter.PatientId,
                                    EpisodeId = faceToFaceEncounter.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = faceToFaceEncounter.OrderNumber,
                                    PatientName = faceToFaceEncounter.DisplayName,
                                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                                    SendDate = faceToFaceEncounter.RequestDate,
                                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                                };
                            }
                            break;
                        case "HCFA485":
                        case "NonOasisHCFA485":
                            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                            if (planofCare != null)
                            {
                                order = new Order
                                {
                                    Id = planofCare.Id,
                                    PatientId = planofCare.PatientId,
                                    EpisodeId = planofCare.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
                                    Number = planofCare.OrderNumber,
                                    PatientName = planofCare.PatientName,
                                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                                    SendDate = planofCare.SentDate,
                                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "HCFA485StandAlone":
                            var planofCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                            if (planofCareStandAlone != null)
                            {
                                order = new Order
                                {
                                    Id = planofCareStandAlone.Id,
                                    PatientId = planofCareStandAlone.PatientId,
                                    EpisodeId = planofCareStandAlone.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = planofCareStandAlone.OrderNumber,
                                    PatientName = planofCareStandAlone.PatientName,
                                    ReceivedDate = planofCareStandAlone.ReceivedDate > DateTime.MinValue ? planofCareStandAlone.ReceivedDate : planofCareStandAlone.SentDate,
                                    SendDate = planofCareStandAlone.SentDate,
                                    PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtEvaluation":
                            var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptEval.Id,
                                    PatientId = ptEval.PatientId,
                                    EpisodeId = ptEval.EpisodeId,
                                    Type = OrderType.PtEvaluation,
                                    Text = DisciplineTasks.PTEvaluation.GetDescription(),
                                    Number = ptEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
                                    SendDate = ptEval.SentDate,
                                    PhysicianSignatureDate = ptEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtReEvaluation":
                            var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptReEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptReEval.Id,
                                    PatientId = ptReEval.PatientId,
                                    EpisodeId = ptReEval.EpisodeId,
                                    Type = OrderType.PtReEvaluation,
                                    Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                                    Number = ptReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
                                    SendDate = ptReEval.SentDate,
                                    PhysicianSignatureDate = ptReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtEvaluation":
                            var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otEval != null)
                            {
                                order = new Order
                                {
                                    Id = otEval.Id,
                                    PatientId = otEval.PatientId,
                                    EpisodeId = otEval.EpisodeId,
                                    Type = OrderType.OtEvaluation,
                                    Text = DisciplineTasks.OTEvaluation.GetDescription(),
                                    Number = otEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
                                    SendDate = otEval.SentDate,
                                    PhysicianSignatureDate = otEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtReEvaluation":
                            var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otReEval != null)
                            {
                                order = new Order
                                {
                                    Id = otReEval.Id,
                                    PatientId = otReEval.PatientId,
                                    EpisodeId = otReEval.EpisodeId,
                                    Type = OrderType.OtReEvaluation,
                                    Text = DisciplineTasks.OTReEvaluation.GetDescription(),
                                    Number = otReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
                                    SendDate = otReEval.SentDate,
                                    PhysicianSignatureDate = otReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StEvaluation":
                            var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stEval != null)
                            {
                                order = new Order
                                {
                                    Id = stEval.Id,
                                    PatientId = stEval.PatientId,
                                    EpisodeId = stEval.EpisodeId,
                                    Type = OrderType.StEvaluation,
                                    Text = DisciplineTasks.STEvaluation.GetDescription(),
                                    Number = stEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
                                    SendDate = stEval.SentDate,
                                    PhysicianSignatureDate = stEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StReEvaluation":
                            var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stReEval != null)
                            {
                                order = new Order
                                {
                                    Id = stReEval.Id,
                                    PatientId = stReEval.PatientId,
                                    EpisodeId = stReEval.EpisodeId,
                                    Type = OrderType.StReEvaluation,
                                    Text = DisciplineTasks.STReEvaluation.GetDescription(),
                                    Number = stReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
                                    SendDate = stReEval.SentDate,
                                    PhysicianSignatureDate = stReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PTDischarge":
                            var ptDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = ptDischarge.Id,
                                    PatientId = ptDischarge.PatientId,
                                    EpisodeId = ptDischarge.EpisodeId,
                                    Type = OrderType.PTDischarge,
                                    Text = DisciplineTasks.PTDischarge.GetDescription(),
                                    Number = ptDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptDischarge.ReceivedDate > DateTime.MinValue ? ptDischarge.ReceivedDate : ptDischarge.SentDate,
                                    SendDate = ptDischarge.SentDate,
                                    PhysicianSignatureDate = ptDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "SixtyDaySummary":
                            var sixtyDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (sixtyDaySummary != null)
                            {
                                order = new Order
                                {
                                    Id = sixtyDaySummary.Id,
                                    PatientId = sixtyDaySummary.PatientId,
                                    EpisodeId = sixtyDaySummary.EpisodeId,
                                    Type = OrderType.SixtyDaySummary,
                                    Text = DisciplineTasks.SixtyDaySummary.GetDescription(),
                                    Number = sixtyDaySummary.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = sixtyDaySummary.ReceivedDate > DateTime.MinValue ? sixtyDaySummary.ReceivedDate : sixtyDaySummary.SentDate,
                                    SendDate = sixtyDaySummary.SentDate,
                                    PhysicianSignatureDate = sixtyDaySummary.PhysicianSignatureDate
                                };
                            }
                            break;
                    }
                }
            }

            return order;
        }

        public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
            var schedules = scheduleRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {

                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId = po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                CreatedDate = po.OrderDateFormatted

                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!ffe.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (ffe.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                                });
                            }
                        });
                    }
                }
                var evalOrdersSchedule = schedules.Where(s =>
                        (s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary
                        || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment) && s.Status==status[1]).ToList();

                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                EpisodeId = eval.EpisodeId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                SendDate = eval.SentDate
                            });
                        });
                    }
                }
            }
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var orders = new List<Order>();
            var schedules = scheduleRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0 && status.Count > 1)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId = po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = po.PhysicianName.IsNotNullOrEmpty() ? po.PhysicianName : physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
                                SendDate = po.SentDate,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate,
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);

                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                                    SendDate = ffe.RequestDate,
                                    PhysicianSignatureDate = ffe.SignatureDate
                                });
                            }
                        });
                    }
                }

                var evalOrdersSchedule = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.FirstOrDefault(s => s.EventId == eval.Id && s.EpisodeId == eval.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    EpisodeId = eval.EpisodeId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                    SendDate = eval.SentDate,
                                    PhysicianSignatureDate = eval.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var schedules = scheduleRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId = po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = po.OrderDateFormatted,
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }

                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    SentDate = ffe.SentDate.ToString("MM/dd/yyyy"),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = DateTime.Today,
                                    PhysicianSignatureDate = ffe.SignatureDate

                                });
                            });
                    }
                }

                var evalSchedules = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
                        || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();

                if (evalSchedules != null && evalSchedules.Count > 0)
                {
                    var evalIds = evalSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int>() { (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically }, evalIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                EpisodeId = eval.EpisodeId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = eval.PhysicianSignatureDate
                            });
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;

            var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
            formCollection.Remove("SendAutomatically");

            var status = (int)ScheduleStatus.OrderSentToPhysician;
            var physician = new AgencyPhysician();

            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                        if (order.PhysicianData.IsNotNullOrEmpty())
                        {
                            physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        }
                        if (order != null)
                        {
                            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                                        }

                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }

                            }

                        }

                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, pocId);
                        if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {
                            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;

                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (planofCareRepository.Update(planofCare))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }

                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }
                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocOrderId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, pocOrderId);
                        if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {

                            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (planofCareRepository.UpdateStandAlone(planofCare))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }

                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var facetofaceId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                        if (faceToFaceEncounter.PhysicianData.IsNotNullOrEmpty()) physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (faceToFaceEncounter != null)
                        {

                            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;

                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation
                    || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation
                    || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation
                    || key.ToInteger() == (int)OrderType.MSWEvaluation) || key.ToInteger() == (int)OrderType.PTDischarge
                    || key.ToInteger() == (int)OrderType.SixtyDaySummary)
                {
                    status = (int)ScheduleStatus.EvalSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var evalId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                        if (evalOrder.PhysicianData.IsNotNullOrEmpty()) physician = evalOrder.PhysicianData.ToObject<AgencyPhysician>();
                        if (evalOrder != null)
                        {
                            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, evalOrder.PatientId, evalOrder.EpisodeId, evalOrder.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    evalOrder.Status = status;
                                    evalOrder.SentDate = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(evalOrder))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }

                        }
                    });
                }
                if (result && physician != null)
                {
                    string subject = string.Format("{0} has sent you an order", Current.AgencyName);
                    var pLastName = physician.LastName.IsNotNullOrEmpty() && physician.LastName.Trim().IsNotNullOrEmpty() && physician.LastName.Trim().Length >= 1 ? physician.LastName.Trim().Substring(0, 1) : string.Empty;
                    string lastName = pLastName.ToUpperCase() + pLastName.ToLowerCase();//physician.LastName.Trim().Substring(0, 1).ToUpper() + physician.LastName.Trim().Substring(1).ToLower();
                    string bodyText = MessageBuilder.PrepareTextFrom("PhysicianOrderNotification", "recipientlastname", lastName, "senderfullname", Current.AgencyName);
                    if (physician.EmailAddress.IsNotNullOrEmpty() && physician.EmailAddress.Trim().IsNotNullOrEmpty())
                    {
                        Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    }
                }
            }

            return result;
        }

        public void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            if (!physicianSignatureDate.IsValid())
            {
                physicianSignatureDate = DateTime.Today;
            }
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;

                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!order.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            order.ReceivedDate = receivedDate;
                            order.PhysicianSignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateOrderModel(order))
                            {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                if (planofCare != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!planofCare.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            planofCare.ReceivedDate = receivedDate;
                            planofCare.PhysicianSignatureDate = physicianSignatureDate;
                            if (planofCareRepository.Update(planofCare))
                            {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                if (pocStandAlone != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, pocStandAlone.PatientId, pocStandAlone.EpisodeId, pocStandAlone.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!pocStandAlone.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            pocStandAlone.ReceivedDate = receivedDate;
                            pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                            if (planofCareRepository.UpdateStandAlone(pocStandAlone))
                            {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        
                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            faceToFaceEncounter.ReceivedDate = receivedDate;
                            faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter))
                            {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }
            if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
                || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
                || type == OrderType.SixtyDaySummary)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, eval.PatientId, eval.EpisodeId, eval.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!eval.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                            eval.ReceivedDate = receivedDate;
                            eval.PhysicianSignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateVisitNote(eval))
                            {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }
        }

        public bool UpdateOrderDates(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    order.ReceivedDate = receivedDate;
                    order.SentDate = sendDate;
                    order.PhysicianSignatureDate = physicianSignatureDate;
                    if (order.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!order.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateOrderModel(order);
                }
            }
            else if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                if (planofCare != null)
                {
                    planofCare.ReceivedDate = receivedDate;
                    planofCare.SentDate = sendDate;
                    planofCare.PhysicianSignatureDate = physicianSignatureDate;
                    if (planofCare.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!planofCare.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.Update(planofCare);
                }
            }
            else if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                if (pocStandAlone != null)
                {
                    pocStandAlone.ReceivedDate = receivedDate;
                    pocStandAlone.SentDate = sendDate;
                    pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                    if (pocStandAlone.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!pocStandAlone.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.UpdateStandAlone(pocStandAlone);
                }
            }
            else if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    faceToFaceEncounter.RequestDate = sendDate;
                    faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                    if (faceToFaceEncounter.SignatureText.IsNullOrEmpty())
                    {
                        var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
                || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
                || type == OrderType.SixtyDaySummary)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    eval.ReceivedDate = receivedDate;
                    eval.SentDate = sendDate;
                    eval.PhysicianSignatureDate = physicianSignatureDate;
                    if (eval.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateVisitNote(eval);
                }
            }
            return result;
        }

        #endregion

        #region Infections

        public List<Infection> GetInfections(Guid agencyId)
        {
            var infections = agencyRepository.GetInfections(Current.AgencyId).ToList();
            if (infections != null && infections.Count > 0)
            {
                infections.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, i.PatientId, i.EpisodeId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return infections;
        }

        public Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
            if (infection != null)
            {
                infection.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                infection.Patient = patientRepository.GetPatientOnly(infection.PatientId, Current.AgencyId);

                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                        Assessment assessment = assessmentService.GetEpisodeAssessment(episode);
                        infection.Diagnosis = new Dictionary<string, string>();
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                            infection.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "");
                            infection.Diagnosis.Add("ICD9M", oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "");
                            infection.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "");
                            infection.Diagnosis.Add("ICD9M2", oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "");
                        }
                    }
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(infection.PhysicianId, Current.AgencyId);
                    if (physician != null) infection.PhysicianName = physician.DisplayName;
                }
            }
            return infection;
        }

        public JsonViewData ProcessInfections(string button, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };

            bool isNoteUpdates = true;
            bool isActionSet = false;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                if (infection != null)
                {
                    if (!infection.EpisodeId.IsEmpty())
                    {
                        var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, infection.EpisodeId, eventId);
                        if (scheduleEvent != null)
                        {
                            var oldStatus = scheduleEvent.Status;
                            var oldPrintQueue = scheduleEvent.InPrintQueue;
                            var description = string.Empty;

                            if (button == "Approve")
                            {
                                infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                                scheduleEvent.Status = infection.Status;
                                scheduleEvent.InPrintQueue = true;
                                description = "Approved By:" + Current.UserFullName;
                                isActionSet = true;

                            }
                            else if (button == "Return")
                            {
                                infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                                scheduleEvent.Status = infection.Status;
                                infection.SignatureText = string.Empty;
                                infection.SignatureDate = DateTime.MinValue;
                                description = "Returned By:" + Current.UserFullName;
                                isActionSet = true;
                            }
                            else if (button == "Print")
                            {
                                scheduleEvent.InPrintQueue = false;
                                isNoteUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (isNoteUpdates)
                                    {
                                        infection.Modified = DateTime.Now;
                                        if (agencyRepository.UpdateInfectionModal(infection))
                                        {
                                            viewData.IsDataCentersRefresh = oldStatus!=scheduleEvent.Status;
                                            viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature;
                                            viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            viewData.IsInfectionRefresh = viewData.IsDataCentersRefresh;
                                            viewData.EpisodeId = scheduleEvent.EpisodeId;
                                            viewData.PatientId = scheduleEvent.PatientId;
                                            viewData.isSuccessful = true;

                                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                            {
                                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                            }
                                        }
                                        else
                                        {
                                            scheduleEvent.Status = oldStatus;
                                            scheduleEvent.InPrintQueue = oldPrintQueue;
                                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                            viewData.isSuccessful = false;
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        #endregion

        #region Incidents

        public List<Incident> GetIncidents(Guid agencyId)
        {
            var incidents = agencyRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents != null && incidents.Count > 0)
            {
                incidents.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }

                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, i.PatientId, i.EpisodeId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return incidents;
        }

        public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
            if (incident != null)
            {
                incident.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                incident.Patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);

                incident.PatientName = incident.Patient.LastName + ", " + incident.Patient.FirstName;
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!incident.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(incident.PhysicianId, Current.AgencyId);
                    if (physician != null) incident.PhysicianName = physician.DisplayName;
                }
            }
            return incident;
        }

        public JsonViewData ProcessIncidents(string button, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };

            bool isNoteUpdates = true;
            bool isActionSet = false;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                if (incident != null)
                {
                    if (!incident.EpisodeId.IsEmpty())
                    {
                        var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, incident.EpisodeId, eventId);
                        if (scheduleEvent != null)
                        {

                            var oldStatus = scheduleEvent.Status;
                            var oldPrintQueue = scheduleEvent.InPrintQueue;
                            var description = string.Empty;
                            var action = new Actions();

                            if (button == "Approve")
                            {
                                incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = incident.Status;
                                description = "Approved by " + Current.UserFullName;
                                action = Actions.Approved;
                                isActionSet = true;
                            }
                            else if (button == "Return")
                            {
                                incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                                scheduleEvent.Status = incident.Status;
                                incident.SignatureText = string.Empty;
                                incident.SignatureDate = DateTime.MinValue;
                                description = "Returned by " + Current.UserFullName;
                                action = Actions.Returned;
                                isActionSet = true;
                            }
                            else if (button == "Print")
                            {
                                scheduleEvent.InPrintQueue = false;
                                isNoteUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (isNoteUpdates)
                                    {
                                        incident.Modified = DateTime.Now;
                                        if (agencyRepository.UpdateIncidentModal(incident))
                                        {
                                            viewData.IsDataCentersRefresh = oldStatus!=scheduleEvent.Status;
                                            viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
                                            viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            viewData.IsIncidentAccidentRefresh = viewData.IsDataCentersRefresh;
                                            viewData.EpisodeId = scheduleEvent.EpisodeId;
                                            viewData.PatientId = scheduleEvent.PatientId;
                                            viewData.isSuccessful = true;

                                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                            {
                                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        #endregion

        public List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            return insuranceList;
        }

        public List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (IsMedicareTradIncluded)
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });
                        }
                    }
                }
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "All",
                    Value = "0"
                });
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return items;
        }

        public List<SelectListItem> Branchs(string value, bool IsAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Branches --",
                    Value = Guid.Empty.ToString(),
                });
            }
            return items;
        }

        public List<PatientEpisodeEvent> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var events = new List<PatientEpisodeEvent>();
            var scheduleEvents = scheduleRepository.GetPrintQueueEvents(Current.AgencyId,BranchId,StartDate,EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var users = new List<User>();
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                scheduleEvents.ForEach(e =>
                {
                    var user = users.FirstOrDefault(u => u.Id == e.UserId);
                    events.Add(new PatientEpisodeEvent
                    {
                        Status = e.StatusName,
                        PatientName = e.PatientName,
                        TaskName = e.DisciplineTaskName,
                        PrintUrl = Url.Download(e, false),
                        UserName = user != null ? user.DisplayName : string.Empty,
                        EventDate = e.EventDate.IsValid() ? e.EventDate.ToString("MM/dd/yyyy") : "",
                        CustomValue = string.Format("{0}|{1}|{2}|{3}", e.EpisodeId, e.PatientId, e.EventId, e.DisciplineTask)
                    });
                });
            }
            return events;
            //var events = new List<PatientEpisodeEvent>();
            //var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, BranchId, StartDate, EndDate);
            //if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
            //{
            //    var episodeWithEventsDictionary = agencyPatientEpisodes.ToDictionary(g => g.Id,
            //         g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate()
            //               && !s.IsDeprecated
            //               && !s.IsMissedVisit
            //               && s.InPrintQueue
            //               && s.EventDate.ToDateTime().Date.IsBetween(StartDate.Date, EndDate.Date)).ToList() : new List<ScheduleEvent>());

            //        if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            //        {
            //            var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
            //            var users = UserEngine.GetUsers(Current.AgencyId, scheduleUserIds) ?? new List<User>();
            //            episodeWithEventsDictionary.ForEach((key, value) =>
            //           {
            //               if (value != null && value.Count > 0)
            //               {
            //                   var episode = agencyPatientEpisodes.FirstOrDefault(e => e.Id == key);
            //                   if (episode != null)
            //                   {
            //                       value.ForEach(s =>
            //                               {
            //                                   var userName = string.Empty;
            //                                   if (!s.UserId.IsEmpty())
            //                                   {
            //                                       var user = users.FirstOrDefault(u => u.Id == s.UserId);
            //                                       if (user != null)
            //                                       {
            //                                           userName = user.DisplayName;
            //                                       }
            //                                   }
            //                                   events.Add(new PatientEpisodeEvent
            //                                        {
            //                                            Status = s.StatusName,
            //                                            PatientName = episode.PatientName,
            //                                            TaskName = s.DisciplineTaskName,
            //                                            PrintUrl = Url.Download(s, true),
            //                                            UserName = userName,
            //                                            EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                                            CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                                        });
            //                               });
            //                   }
            //               }
            //           });
            //        }
                    
            //}
            //return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }


        #region Private 

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        task = DisciplineTasks.MSWEvaluationAssessment;
                        break;
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        task = DisciplineTasks.SixtyDaySummary;
                        break;
                }
            }
            return task;
        }

        #endregion
    }
}
