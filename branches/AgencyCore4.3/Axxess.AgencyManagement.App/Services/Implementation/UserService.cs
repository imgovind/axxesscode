﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Common;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Repositories;
   

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly ILookupRepository lookupRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService, ILookUpDataProvider lookupDataProvider)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.assetRepository = agencyManagmentDataProvider.AssetRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.scheduleRepository = agencyManagmentDataProvider.ScheduleRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
        }

        #region User

        public bool CreateUser(User user)
        {
            try
            {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login)) isNewLogin = true;
                }
                user.LoginId = login.Id;
                //user.Profile = user.Profile;
                user.Profile.EmailWork = user.EmailAddress;
                if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0) user.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0) user.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0) user.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                if (userRepository.Add(user))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
                    if (isNewLogin)
                    {
                        var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
                   // UserEngine.Refresh(Current.AgencyId);
                    Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return false;
        }

        public bool DeleteUser(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            var result = false;
            if (user != null)
            {
                var accounts = userRepository.GetUsersByLoginId(user.LoginId);
                if (accounts != null)
                {
                    if (userRepository.Delete(Current.AgencyId, userId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }
       
        public IList<User> GetUsersByStatus(Guid branchId, int status)
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, branchId, status);
            if (users != null && users.Count > 0)
            {
                users.ForEach(user =>
                {
                    if (user.ProfileData.IsNotNullOrEmpty())
                    {
                        user.Profile = user.ProfileData.ToObject<UserProfile>();
                        if (user.Profile != null)
                        {
                            user.EmailAddress = user.Profile.EmailWork;

                        }
                        user.ProfileData = string.Empty;
                    }

                });
            }
            return users;
        }

        public bool UpdateProfile(User user)
        {
            var result = false;
            if (user != null)
            {
                user.AgencyId = Current.AgencyId;
                if (userRepository.UpdateProfile(user))
                {
                    result = true;
                    if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                    {
                        User userInfo = userRepository.Get(user.Id, Current.AgencyId);
                        Login login = loginRepository.Find(userInfo.LoginId);
                        if (userInfo != null && login != null)
                        {
                            if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                            {
                                string passwordsalt = string.Empty;
                                string passwordHash = string.Empty;

                                var saltedHash = new SaltedHash();
                                saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                                login.PasswordSalt = passwordsalt;
                                login.PasswordHash = passwordHash;
                            }

                            if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                            {
                                string signaturesalt = string.Empty;
                                string signatureHash = string.Empty;

                                var saltedHash = new SaltedHash();
                                saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                                login.SignatureSalt = signaturesalt;
                                login.SignatureHash = signatureHash;
                            }

                            if (!loginRepository.Update(login))
                            {
                                result = false;
                            }
                            else
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                            }
                        }
                    }

                }
            }

            return result;
        }

        #endregion

        #region User Validate

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var users = userRepository.GetUsersByLoginId(login.Id, Current.AgencyId);
                if (users.Count > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.Get(userId, Current.AgencyId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        #endregion

        #region License

        public bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            var user = userRepository.GetUserOnly(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);

                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                license.AssetId = asset.Id;
                            }
                            else
                            {
                                isAssetSaved = false;
                                break;
                            }
                        }
                    }
                }
                if (isAssetSaved)
                {
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        user.LicensesArray = user.Licenses.ToObject<List<License>>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }

                    //if (user.LicensesArray != null)
                    //{
                    //    user.LicensesArray.Add(license);
                    //    user.Licenses = user.LicensesArray.ToXml();
                    //}
                    //else
                    //{
                    //    user.LicensesArray = new List<License>();
                    //    user.LicensesArray.Add(license);
                    //    user.Licenses = user.LicensesArray.ToXml();
                    //}
                    if (userRepository.UpdateModel(user,false))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime expirationDate, string LicenseNumber)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                    if (user.LicensesArray != null && user.LicensesArray.Count > 0)
                    {
                        var license = user.LicensesArray.Find(l => l.Id == id);
                        if (license != null)
                        {
                            license.ExpirationDate = expirationDate;
                            license.LicenseNumber = LicenseNumber;
                            user.Licenses = user.LicensesArray.ToXml();
                            if (userRepository.UpdateModel(user, false))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime initiationDate, DateTime expirationDate)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                     user.LicensesArray = user.Licenses.ToObject<List<License>>();
                     if (user.LicensesArray != null && user.LicensesArray.Count > 0)
                     {
                         var license = user.LicensesArray.Find(l => l.Id == id);
                         if (license != null)
                         {
                             license.InitiationDate = initiationDate;
                             license.ExpirationDate = expirationDate;
                             user.Licenses = user.LicensesArray.ToXml();
                             if (userRepository.UpdateModel(user,false))
                             {
                                 Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                                 return true;
                             }
                         }
                     }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                    if (user.LicensesArray != null && user.LicensesArray.Count > 0)
                    {
                        var license = user.LicensesArray.Find(l => l.Id == id);
                        if (license != null)
                        {
                            if (!license.AssetId.IsEmpty())
                            {
                                assetRepository.Delete(license.AssetId);
                            }
                            user.LicensesArray.RemoveAll(l => l.Id == id);
                            user.Licenses = user.LicensesArray.ToXml();
                            if (userRepository.UpdateModel(user,false))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                                return true;
                            }

                        }
                    }
                }
            }
            return false;
        }

        public IList<License> GetUserLicenses(Guid branchId, int status)
        {
            var list = new List<License>();

            var users = userRepository.GetAllUsers(Current.AgencyId, branchId, status);
            if (users != null && users.Count > 0)
            {
                users.ForEach(u =>
                {
                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                l.UserDisplayName = string.Format("{0}, {1}", u.LastName, u.FirstName);
                                if (u.CustomId.IsNotNullOrEmpty())
                                {
                                    Console.WriteLine();
                                }
                                l.CustomId = u.CustomId;
                                list.Add(l);
                            }
                                );
                        }
                    }
                });
            }
            return list;
        }

        #endregion

        #region License Item

        public bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var binaryReader = new BinaryReader(file.InputStream);

                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset))
                        {
                            licenseItem.AssetId = asset.Id;
                        }
                        else
                        {
                            isAssetSaved = false;
                            break;
                        }
                    }
                }
            }
            if (isAssetSaved)
            {
                licenseItem.Id = Guid.NewGuid();
                licenseItem.Created = DateTime.Now;
                licenseItem.Modified = DateTime.Now;
                licenseItem.IsDeprecated = false;
                licenseItem.AgencyId = Current.AgencyId;

                if (licenseItem.OtherLicenseType.IsNotNullOrEmpty())
                {
                    licenseItem.LicenseType = licenseItem.OtherLicenseType;
                }
                if (userRepository.AddNonUserLicense(licenseItem))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.Id.ToString(), LogType.NonUserLicense, LogAction.UserLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<LicenseItem> GetUserLicenses()
        {
            var list = new List<LicenseItem>();

            var nonuserLicenses = userRepository.GetNonUserLicenses(Current.AgencyId);
            if (nonuserLicenses != null && nonuserLicenses.Count > 0)
            {
                nonuserLicenses.ForEach(license =>
                {
                    list.Add(license);
                });
            }

            var userLicenses = userRepository.GetSoftwareUserLicenses(Current.AgencyId);
            if (userLicenses != null && userLicenses.Count > 0)
            {
                userLicenses.ForEach(license =>
                {
                    license.DisplayName = UserEngine.GetName(license.UserId, license.AgencyId);
                    list.Add(license);
                });
            }
            return list.OrderBy(l => l.FirstName).ToList();
        }

        #endregion

        #region Permissions

        public bool UpdatePermissions(FormCollection formCollection)
        {
            var result = false;
            try
            {
                var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
                var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
                if (!userId.IsEmpty() && permissionArray != null && permissionArray.Count > 0)
                {
                    var user = userRepository.GetUserOnly(userId, Current.AgencyId);
                    if (user != null)
                    {
                        user.PermissionsArray = permissionArray;
                        if (user.PermissionsArray != null && user.PermissionsArray.Count > 0)
                        {
                            user.Permissions = user.PermissionsArray.ToXml();
                        }
                        if (userRepository.UpdateModel(user, false))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        #endregion

        #region User Rate

        public bool LoadUserRate(Guid fromId, Guid toId)
        {
            bool result = false;
            var fromUser = userRepository.GetUserOnly(fromId, Current.AgencyId);
            if (fromUser != null)
            {
                var toUser = userRepository.GetUserOnly(toId, Current.AgencyId);
                if (toUser != null)
                {
                    toUser.Rates = fromUser.Rates;
                    if (userRepository.UpdateModel(toUser, false))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public IList<UserRate> GetUserRates(Guid userId)
        {
            var list = new List<UserRate>();
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                list = user.Rates.ToObject<List<UserRate>>();
                list.ForEach(r => { r.InsuranceName = patientService.GetInsurance(r.Insurance); });
            }
            return list;
        }

        #endregion

        #region User Visits

        public IList<UserVisitWidget> GetScheduleWidget(Guid userId, DateTime from, DateTime to)
        {
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var aditionalFilter = string.Format(" AND scheduleevents.Status NOT IN ( {0} ) ", status.Select(s => s.ToString()).ToArray().Join(","));
            var userVisits = userRepository.GetScheduleWidget(Current.AgencyId, userId, from, to, 5, false, aditionalFilter);
            if (userVisits != null && userVisits.Count > 0)
            {
                userVisits.ForEach(uv =>
                {
                    var scheduledEvent = new ScheduleEvent { EventId = uv.EventId, EpisodeId = uv.EventId, PatientId = uv.PatientId, DisciplineTask = uv.DisciplineTask, Status = uv.Status, UserId = userId };
                    var emptyData = string.Empty;
                    Common.Url.SetNewUrl(scheduledEvent, ref emptyData);
                    uv.TaskName = scheduledEvent.Url;
                });
            }
            return userVisits;
        }

        public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var aditionalFilter = string.Format(" AND scheduleevents.Status NOT IN ( {0} ) ", status.Select(s => s.ToString()).ToArray().Join(","));
            var scheduleEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, aditionalFilter);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var users = new List<User>();
                var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        var scheduleUsers = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, returnUserIds) ?? new List<User>();
                        if (scheduleUsers != null && scheduleUsers.Count > 0)
                        {
                            users.AddRange(scheduleUsers);
                        }
                    }
                }
                scheduleEvents.ForEach(s =>
                {
                    var visitNote = string.Empty;
                    var statusComments = string.Empty;

                    var episodeDetail = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    if (s.Comments.IsNotNullOrEmpty())
                    {
                        visitNote = s.Comments.Clean();
                    }

                    var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                    statusComments = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

                    var emptyData = string.Empty;
                    Common.Url.SetNewUrl(s, ref emptyData);
                    userVisits.Add(new UserVisit
                    {
                        Id = s.EventId,
                        VisitNotes = visitNote,
                        Url = s.Url,
                        Status = s.Status,
                        PatientName = s.PatientName,
                        StatusComment = statusComments,
                        DisciplineTask = s.DisciplineTask,
                        EpisodeId = s.EpisodeId,
                        PatientId = s.PatientId,
                        EpisodeNotes = episodeDetail.Comments.Clean(),
                        VisitDate = s.VisitDate,
                        ScheduleDate = s.EventDate,
                        IsMissedVisit = s.IsMissedVisit
                    });
                });
            }
            return userVisits;
        }

        public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var scheduledEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, string.Empty);
            if (scheduledEvents != null && scheduledEvents.Count > 0)
            {
                var emptyData = string.Empty;
                var episodeIds = scheduledEvents.Where(r => !r.EpisodeId.IsEmpty()).Select(r => r.EpisodeId).Distinct().ToList();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                var users = new List<User>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        users = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<User>();
                    }
                }
                scheduledEvents.ForEach(scheduledEvent =>
                {
                    var visitNote = string.Empty;
                    var statusComments = string.Empty;

                    var episodeDetail = scheduledEvent.EpisodeNotes.IsNotNullOrEmpty() ? scheduledEvent.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                    {
                        visitNote = scheduledEvent.Comments.Clean();
                    }
                    var eventReturnReasons = returnComments.Where(r => r.EpisodeId == scheduledEvent.EpisodeId && r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
                    statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);

                    Common.Url.SetNewUrl(scheduledEvent, ref emptyData);
                    userVisits.Add(new UserVisit
                    {
                        Id = scheduledEvent.EventId,
                        VisitNotes = visitNote,
                        Url = scheduledEvent.Url,
                        Status = scheduledEvent.Status,
                        PatientName = scheduledEvent.PatientName,
                        StatusComment = statusComments,
                        DisciplineTask = scheduledEvent.DisciplineTask,
                        EpisodeId = scheduledEvent.EpisodeId,
                        PatientId = scheduledEvent.PatientId,
                        EpisodeNotes = episodeDetail.Comments.Clean(),
                        VisitDate = scheduledEvent.VisitDate,
                        ScheduleDate = scheduledEvent.EventDate,
                        IsMissedVisit = scheduledEvent.IsMissedVisit
                    });
                });
            }
            return userVisits;
        }

        #endregion 

        #region Private

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        #endregion

    }
}
