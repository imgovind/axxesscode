﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using System.Linq;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Enums;
    using Common;
    using Domain;
    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Log.Domain;
    using Axxess.AgencyManagement.Common;

    public class PatientService : IPatientService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IScheduleRepository scheduleRepository;

        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.assessmentService = assessmentService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
        }

        #endregion

        #region Patient 

        public bool AddPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            patient.AdmissionId = admissionDateId;
            if (patientRepository.Add(patient, out patientOut))
            {
                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, StartOfCareDate = patient.StartofCareDate, IsActive = true, IsDeprecated = false, Status = patientOut.Status }))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
                    result = true;
                }
            }
            return result;
        }

        public bool EditPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                var oldAdmissionDateId = patientToEdit.AdmissionId;
                patient.AdmissionId = patientToEdit.AdmissionId.IsEmpty() ? admissionDateId : patientToEdit.AdmissionId;
                if (patientRepository.Edit(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientToEdit.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patientToEdit);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientToEdit.Id, oldAdmissionDateId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.StartOfCareDate = patientOut.StartofCareDate;
                            if (patient.Status == (int)PatientStatus.Discharged)
                            {
                                admissionData.DischargedDate = patientOut.DischargeDate;
                            }
                            admissionData.PatientData = patientOut.ToXml();
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public JsonViewData DeletePatient(Guid Id, bool isDeprecated)
        {
            var data = new JsonViewData { isSuccessful = false };
            var patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
            if (patient != null)
            {
                patient.IsDeprecated = isDeprecated;
                if (patientRepository.Update(patient))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, Id, Id.ToString(), LogType.Patient, isDeprecated ? LogAction.PatientDeleted : LogAction.PatientRestored, string.Empty);
                    data.isSuccessful = true;
                    data.PatientId = patient.Id;
                    data.IsDeletedPatientListRefresh = true;
                    data.IsCenterRefresh = patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged;
                    data.IsPatientListRefresh = true;
                    data.PatientStatus = patient.Status;
                    data.IsNonAdmitPatientListRefresh = patient.Status == (int)PatientStatus.NonAdmission;
                    data.IsPendingPatientListRefresh = patient.Status == (int)PatientStatus.Pending;
                }
            }
            return data;
        }

        public JsonViewData AdmitPatient(PendingPatient pending)
        {
            var data = new JsonViewData { isSuccessful = false };
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            if (pending.Type == NonAdmitTypes.Patient)
            {
                var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                    var oldAdmissionDateId = patient.AdmissionId;
                    pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            if (!pending.PrimaryPhysician.IsEmpty())
                            {
                                var physicians = patient.PhysicianContacts.ToList();
                                var physician = physicians.FirstOrDefault(p => p.Id == pending.PrimaryPhysician);
                                if (physician != null)
                                {
                                    if (!physician.Primary)
                                    {
                                        physicianRepository.SetPrimary(patient.Id, pending.PrimaryPhysician);
                                    }
                                }
                                else
                                {
                                    physicianRepository.Link(patient.Id, pending.PrimaryPhysician, true);
                                    //physicianRepository.SetPrimary(patient.Id, pending.PrimaryPhysician);
                                }
                            }

                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                {
                                    var medId = Guid.NewGuid();
                                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                                    if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                        data.isSuccessful = true;
                                    }
                                    else
                                    {
                                        patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                if (admissionData != null)
                                {
                                    var oldPatientData = admissionData.PatientData;
                                    var oldSoc = admissionData.StartOfCareDate;
                                    var oldAdmissionStatus = admissionData.Status;
                                    admissionData.PatientData = patientOut.ToXml();
                                    admissionData.StartOfCareDate = pending.StartofCareDate;
                                    admissionData.Status = (int)PatientStatus.Active;
                                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                                            data.isSuccessful = true;
                                        }
                                        else
                                        {
                                            admissionData.Status = oldAdmissionStatus;
                                            admissionData.PatientData = oldPatientData;
                                            admissionData.StartOfCareDate = oldSoc;
                                            patientRepository.UpdatePatientAdmissionDate(admissionData);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                            data.isSuccessful = true;
                                        }
                                        else
                                        {
                                            patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                            }
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                    if (data.isSuccessful)
                    {
                        data.IsCenterRefresh = true;

                        data.IsPatientListRefresh = true;
                        data.PatientStatus = (int)PatientStatus.Active;
                        data.IsReferralListRefresh = false;
                        data.IsNonAdmitPatientListRefresh = patient.Status == (int)PatientStatus.NonAdmission;
                        data.IsPendingPatientListRefresh = patient.Status == (int)PatientStatus.Pending;
                    }
                }
            }
            else if (pending.Type == NonAdmitTypes.Referral)
            {
                var referral = referralRepository.Get(Current.AgencyId, pending.Id);
                if (referral != null)
                {
                    pending.AdmissionId = admissionDateId;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            var physicains = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
                            if (!pending.PrimaryPhysician.IsEmpty() && !physicains.Exists(p => p.Id == pending.PrimaryPhysician))
                            {
                                physicains.ForEach(p => { p.IsPrimary = false; });
                                physicains.Add(new Physician() { Id = pending.PrimaryPhysician, IsPrimary = true });
                            }
                            if (physicains != null && physicains.Count > 0)
                            {
                                physicains.ForEach(p =>
                                {
                                    physicianRepository.Link(referral.Id, p.Id, p.IsPrimary);
                                });
                            }
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Referral Admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                var medId = Guid.NewGuid();
                                patientOut.EpisodeStartDate = pending.EpisodeStartDate;
                                patientOut.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                                if (this.CreateMedicationProfile(patientOut, medId) && this.CreateEpisodeAndClaims(patientOut))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, referral.Id, referral.Id.ToString(), LogType.Patient, LogAction.ReferralAdmitted, string.Empty);
                                    data.isSuccessful = true;
                                }
                                else
                                {
                                    patientRepository.DeletePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
                                    referralRepository.UpdateModal(referral);
                                }
                            }
                            else
                            {
                                referralRepository.UpdateModal(referral);
                            }
                        }
                        else
                        {
                            referralRepository.UpdateModal(referral);
                        }
                    }
                    if (data.isSuccessful)
                    {
                        data.IsCenterRefresh = true;
                        data.IsNonAdmitPatientListRefresh = false;
                        data.IsPatientListRefresh = true;
                        data.PatientStatus = (int)PatientStatus.Active;
                        data.IsReferralListRefresh = true;
                    }
                }
            }
            return data;
        }

        public JsonViewData NonAdmitPatient(PendingPatient pending)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.NonAdmitPatient(pending, out patientOut))
                {
                    if (patient.AdmissionId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                            viewData.isSuccessful = true;
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patientOut.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {

                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                viewData.isSuccessful = true;

                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                    }
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCenterRefresh = false;
                        viewData.IsNonAdmitPatientListRefresh = (int)PatientStatus.NonAdmission != patient.Status;
                        viewData.IsPatientListRefresh = viewData.IsNonAdmitPatientListRefresh;
                        viewData.PatientStatus = (int)PatientStatus.NonAdmission;
                        viewData.IsPendingPatientListRefresh = false;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData SetPatientPending(Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Patient patientOut = null;
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Pending;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                            viewData.isSuccessful = true;
                        }
                        else
                        {
                            patient.Status = oldStatus;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                        viewData.IsPendingPatientListRefresh = (int)PatientStatus.Pending != oldStatus;
                        viewData.IsPatientListRefresh = viewData.IsPendingPatientListRefresh;
                        viewData.PatientStatus = (int)PatientStatus.Pending;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                patient.DischargeReasonId = dischargeReasonId;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, DischargeReasonId = dischargeReasonId, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisodeDateInBetween(Current.AgencyId, patientId, dischargeDate);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            viewData.isSuccessful = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisodeDateInBetween(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                viewData.isSuccessful = true;

                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, DischargeReasonId = dischargeReasonId, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisodeDateInBetween(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                viewData.isSuccessful = true;

                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }

                        }
                    }
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                        viewData.IsPatientListRefresh = oldStatus != patient.Status;
                        viewData.PatientStatus = (int)PatientStatus.Discharged;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                //patient.DischargeReason = dischargeReason;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            viewData.isSuccessful = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                        viewData.IsPatientListRefresh = oldStatus != patient.Status;
                        viewData.PatientStatus = (int)PatientStatus.Discharged;
                    }
                }

            }
            return viewData;
        }

        public JsonViewData ActivatePatient(Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Active;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (patientOut != null)
                    {
                        if (oldAdmissionDateId.IsEmpty())
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                            {
                                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                                viewData.isSuccessful = true;
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);

                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                            if (admissionData != null)
                            {
                                admissionData.PatientData = patientOut.ToXml();
                                admissionData.Status = patient.Status;
                                admissionData.StartOfCareDate = patient.StartofCareDate;
                                if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                {
                                    viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                                    viewData.isSuccessful = true;
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                                {
                                    viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                                    viewData.isSuccessful = true;
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                        }
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                        viewData.IsPatientListRefresh = oldStatus != patient.Status;
                        viewData.PatientStatus = (int)PatientStatus.Active;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ActivatePatient(Guid patientId, DateTime startOfCareDate)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            Patient patientOut = null;
            if (patient != null)
            {
                var oldStatus = patient.Status;
                var oldStartOfCareDate = patient.StartofCareDate;
                patient.Status = (int)PatientStatus.Active;
                patient.StartofCareDate = startOfCareDate;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = admissionDateId;
                if (patientRepository.Update(patient, out patientOut))
                {

                    if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                        viewData.isSuccessful = true;
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.StartofCareDate = oldStartOfCareDate;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }

                    //else
                    //{
                    //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                    //    if (admissionData != null && patientOut != null)
                    //    {
                    //        admissionData.PatientData = patientOut.ToXml();
                    //        admissionData.Status = patient.Status;
                    //        admissionData.StartOfCareDate = patient.StartofCareDate;
                    //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    //        {
                    //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                    //            result = true;
                    //        }
                    //        else
                    //        {
                    //            patient.Status = oldStatus;
                    //            patient.StartofCareDate = oldStartOfCareDate;
                    //            patient.AdmissionId = oldAdmissionDateId;
                    //            patientRepository.Update(patient);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        patient.Status = oldStatus;
                    //        patient.StartofCareDate = oldStartOfCareDate;
                    //        patient.AdmissionId = oldAdmissionDateId;
                    //        patientRepository.Update(patient);
                    //    }
                    //}
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
                        viewData.IsPatientListRefresh = oldStatus != patient.Status;
                        viewData.PatientStatus = (int)PatientStatus.Active;
                    }
                }
            }
            return viewData;
        }

        public PatientProfile GetProfile(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var patientProfile = new PatientProfile();
                patientProfile.Patient = patient;
                patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

                patientProfile.Allergies = GetAllergies(patientId);
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    patientProfile.Physician = physician;
                }
                patient.EmergencyContacts = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId);
                if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                {
                    patientProfile.EmergencyContact = patient.EmergencyContacts.OrderByDescending(e => e.IsPrimary).FirstOrDefault();
                }
                if (!patient.CaseManagerId.IsEmpty())
                {
                    patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
                }
                if (!patient.UserId.IsEmpty())
                {
                    patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
                }

                SetInsurance(patient);

                var episode = patientRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId, DateTime.Now);
                if (episode != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episode);
                    var freq = this.GetFrequencyForAssessment(assessment);
                    patientProfile.Frequencies = freq;
                    patientProfile.CurrentEpisode = episode;
                    patientProfile.CurrentAssessment = assessment;//assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                }
                return patientProfile;
            }

            return null;
        }

        public List<NonAdmit> GetNonAdmits()
        {
            var list = new List<NonAdmit>();
            var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
            var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    InsuranceCache cache = null;
                    if (p.PrimaryInsurance.IsNotNullOrEmpty() && p.PrimaryInsurance.IsInteger())
                    {
                        cache = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
                    }
                    list.Add(new NonAdmit
                    {
                        Id = p.Id,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        DateOfBirth = p.DOBFormatted,
                        Phone = p.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Patient,
                        MiddleInitial = p.MiddleInitial,
                        PatientIdNumber = p.PatientIdNumber,
                        NonAdmissionReason = p.NonAdmissionReason,
                        NonAdmitDate = p.NonAdmissionDateFormatted,
                        Gender = p.Gender,
                        AddressCity = p.AddressCity,
                        AddressLine1 = p.AddressLine1,
                        AddressLine2 = p.AddressLine2,
                        AddressStateCode = p.AddressStateCode,
                        AddressZipCode = p.AddressZipCode,
                        MedicareNumber = p.MedicaidNumber,
                        InsuranceName = cache != null ? cache.Name : string.Empty,
                        InsuranceNumber = p.PrimaryHealthPlanId,
                        Comments = p.Comments
                    });
                });
            }

            if (referrals != null && referrals.Count > 0)
            {
                referrals.ForEach(r =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = r.Id,
                        LastName = r.LastName,
                        FirstName = r.FirstName,
                        DateOfBirth = r.DOBFormatted,
                        Phone = r.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Referral,
                        PatientIdNumber = string.Empty,
                        NonAdmissionReason = r.NonAdmissionReason,
                        NonAdmitDate = r.NonAdmissionDateFormatted,
                        Comments = r.Comments
                    });
                });
            }

            return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        }

        public List<PatientData> GetPatients(Guid agencyId, Guid branchId, int status)
        {
            IList<PatientData> patients = patientRepository.All(agencyId, branchId, status);
            patients.ForEach((PatientData patient) =>
            {
                patient.InsuranceName = GetInsurance(patient.InsuranceId);
                if (patient.InsuranceName.IsNotNullOrEmpty())
                {
                    patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                }
            });
            return patients.ToList();
        }

        public List<PatientData> GetDeletedPatients(Guid agencyId, Guid branchId)
        {
            IList<PatientData> patients = patientRepository.AllDeleted(agencyId, branchId);
            patients.ForEach((PatientData patient) =>
            {
                patient.InsuranceName = GetInsurance(patient.InsuranceId);
                if (patient.InsuranceName.IsNotNullOrEmpty())
                {
                    patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                }
            });
            return patients.ToList();
        }

        public List<PendingPatient> GetPendingPatients()
        {
            var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    if (p.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
                        if (insurance != null && insurance.Name.IsNotNullOrEmpty())
                        {
                            p.PrimaryInsuranceName = insurance.Name;
                        }
                    }
                });
            }

            return patients;
        }

        public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType)
        {
            var viewData = new PatientInsuranceInfoViewData();
            viewData.InsuranceType = insuranceType.ToTitleCase();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    if (insuranceType.IsEqual("Primary"))
                    {
                        if (patient.PrimaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.PrimaryHealthPlanId;
                            viewData.GroupName = patient.PrimaryGroupName;
                            viewData.GroupId = patient.PrimaryGroupId;
                            viewData.Relationship = patient.PrimaryRelationship;
                        }
                        viewData.InsuranceType = "Primary";
                    }
                    else if (insuranceType.IsEqual("Secondary"))
                    {
                        if (patient.SecondaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.SecondaryHealthPlanId;
                            viewData.GroupName = patient.SecondaryGroupName;
                            viewData.GroupId = patient.SecondaryGroupId;
                            viewData.Relationship = patient.SecondaryRelationship;
                        }
                        viewData.InsuranceType = "Secondary";

                    }
                    else if (insuranceType.IsEqual("Tertiary"))
                    {
                        if (patient.TertiaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.TertiaryHealthPlanId;
                            viewData.GroupId = patient.TertiaryGroupId;
                            viewData.GroupName = patient.TertiaryGroupName;
                            viewData.Relationship = patient.TertiaryRelationship;
                        }
                        viewData.InsuranceType = "Tertiary";
                    }
                }
            }
            return viewData;
        }


        #endregion

        #region Patient Photo

        public bool IsValidImage(HttpFileCollectionBase httpFiles)
        {
            var result = false;
            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                    {
                        var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (allowedExtensions != null && allowedExtensions.Length > 0)
                        {
                            allowedExtensions.ForEach(extension =>
                            {
                                if (fileExtension.IsEqual(extension))
                                {
                                    result = true;
                                    return;
                                }
                            });
                        }
                    }
                }
            }
            return result;
        }

        public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        {
            var result = false;

            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (httpFiles.Count > 0)
                {
                    HttpPostedFileBase file = httpFiles.Get("Photo1");
                    if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var photo = Image.FromStream(file.InputStream);
                        var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                        if (resizedImageStream != null)
                        {
                            var binaryReader = new BinaryReader(resizedImageStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                Patient patientOut = null;
                                var oldPhotoId = patient.PhotoId;
                                var admissionDateId = Guid.NewGuid();
                                patient.PhotoId = asset.Id;
                                var oldAdmissionDateId = patient.AdmissionId;
                                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                                if (patientRepository.Update(patient, out patientOut))
                                {
                                    if (oldAdmissionDateId.IsEmpty())
                                    {
                                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                            result = true;
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                        if (admissionData != null && patientOut != null)
                                        {
                                            admissionData.PatientData = patientOut.ToXml();
                                            admissionData.Status = patient.Status;
                                            admissionData.StartOfCareDate = patient.StartofCareDate;
                                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                            {
                                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                                result = true;
                                            }
                                            else
                                            {
                                                patient.PhotoId = oldPhotoId;
                                                patient.AdmissionId = oldAdmissionDateId;
                                                patientRepository.Update(patient);
                                            }
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePatientForPhotoRemove(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var photoId = patient.PhotoId;
            patient.PhotoId = Guid.Empty;
            if (patientRepository.Update(patient, out patientOut))
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientOut.Id, patientOut.AdmissionId);
                if (admissionData != null && patientOut != null)
                {
                    admissionData.PatientData = patientOut.ToXml();
                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                        result = true;
                    }
                    else
                    {
                        patient.PhotoId = photoId;
                        patientRepository.Update(patient);
                    }
                }
                else
                {
                    patient.PhotoId = photoId;
                    patientRepository.Update(patient);
                }
            }
            return result;
        }

        #endregion

        #region Admission

        public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var admission = new PatientAdmissionDate();
            if (patient != null)
            {
                var admissionId = Guid.NewGuid();
                var oldAdmissionId = patient.AdmissionId;
                patient.AdmissionId = admissionId;
                admission = new PatientAdmissionDate
               {
                   Id = admissionId,
                   AgencyId = Current.AgencyId,
                   PatientId = patient.Id,
                   StartOfCareDate = patient.StartofCareDate,
                   DischargedDate = patient.DischargeDate,
                   Status = patient.Status,
                   IsActive = true,
                   IsDeprecated = false,
                   Created = DateTime.Now,
                   Modified = DateTime.Now,
                   PatientData = patient.ToXml()
               };
                if (patientRepository.Update(patient))
                {
                    if (patientRepository.AddPatientAdmissionDate(admission))
                    {
                        return admission;
                    }
                    else
                    {
                        patient.AdmissionId = oldAdmissionId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return admission;
        }

        public bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if (a.Id == admissionId)
                        {
                            continue;
                        }
                        else
                        {
                            if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var oldAdmissionId = patient.AdmissionId;
                    var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                    if (admission != null)
                    {
                        patient.AdmissionId = admission.Id;
                        admission.PatientData = patient.ToXml();
                        if (patientRepository.Update(patient))
                        {
                            if (patientRepository.UpdatePatientAdmissionDateModal(admission))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmissionPeriodEdited, string.Empty);
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodSetCurrent, string.Empty);

                                return true;
                            }
                            else
                            {
                                patient.AdmissionId = oldAdmissionId;
                                patientRepository.Update(patient);
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        #endregion

        #region Emergency Contact

        public bool AddPrimaryEmergencyContact(Patient patient)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(patient.EmergencyContact) && patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, patient.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            bool result = false;
            if (patient != null)
            {
                emergencyContact.PatientId = patientId;
                emergencyContact.AgencyId = Current.AgencyId;
                if (patientRepository.AddEmergencyContact(emergencyContact))
                {
                    if (emergencyContact.IsPrimary)
                    {
                        patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, emergencyContact.Id);
                    }
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool EditEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            bool result = false;
            if (patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.EmergencyContactEdited, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                result = true;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            if (patientRepository.DeleteEmergencyContact(Id, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.EmergencyContactDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        #endregion

        #region Allergy

        public bool AddAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    allergy.Id = Guid.NewGuid();
                    if (allergyProfile.Allergies.IsNullOrEmpty())
                    {
                        var newList = new List<Allergy>() { allergy };
                        allergyProfile.Allergies = newList.ToXml();
                    }
                    else
                    {
                        var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                        existingList.Add(allergy);
                        allergyProfile.Allergies = existingList.ToXml();
                    }
                    if (patientRepository.UpdateAllergyProfile(allergyProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyAdded, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool UpdateAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergy.Id))
                    {
                        var exisitingAllergy = existingList.Single(m => m.Id == allergy.Id);
                        if (exisitingAllergy != null)
                        {
                            exisitingAllergy.Name = allergy.Name;
                            exisitingAllergy.Type = allergy.Type;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyUpdated, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted)
        {
            var result = false;
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                if (existingList.Exists(a => a.Id == allergyId))
                {
                    var allergy = existingList.Single(m => m.Id == allergyId);
                    if (allergy != null)
                    {
                        allergy.IsDeprecated = isDeleted;
                        allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                        if (patientRepository.UpdateAllergyProfile(allergyProfile))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfileId.ToString(), LogType.AllergyProfile, LogAction.AllergyDeleted, string.Empty);
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public string GetAllergies(Guid patientId)
        {
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                return allergyProfile.ToString();
            }
            return string.Empty;
        }

        public AllergyProfileViewData GetAllergyProfilePrint(Guid Id)
        {
            var profile = new AllergyProfileViewData();
            var allergies = patientRepository.GetAllergyProfileByPatient(Id, Current.AgencyId);
            if (allergies != null) profile.AllergyProfile = allergies;
            var patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
            if (patient != null) profile.Patient = patient;
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            if (agency != null) profile.Agency = agency;
            return profile;
        }

        #endregion

        #region Medication Profile

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, medicationProfile.Id.ToString(), LogType.MedicationProfile, LogAction.MedicationProfileAdded, string.Empty);
                return true;
            }
            return false;
        }

        public bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    medication.Id = Guid.NewGuid();
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                    if (medicationProfile.Medication.IsNullOrEmpty())
                    {
                        var newList = new List<Medication>() { medication };
                        medicationProfile.Medication = newList.ToXml();
                    }
                    else
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        existingList.Add(medication);
                        medicationProfile.Medication = existingList.ToXml();
                    }
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
                {
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == medication.Id))
                    {
                        var exisitingMedication = existingList.Single(m => m.Id == medication.Id);
                        if (exisitingMedication != null)
                        {
                            var logAction = LogAction.MedicationUpdated;
                            if (exisitingMedication.StartDate != medication.StartDate || exisitingMedication.Route != medication.Route || exisitingMedication.Frequency != medication.Frequency || exisitingMedication.MedicationDosage != medication.MedicationDosage)
                            {
                                medication.Id = Guid.NewGuid();
                                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                                medication.MedicationType = new MedicationType { Value = "C", Text = MedicationTypeEnum.C.GetDescription() };
                                medication.MedicationCategory = exisitingMedication.MedicationCategory;
                                existingList.Add(medication);

                                exisitingMedication.MedicationCategory = "DC";
                                exisitingMedication.DCDate = DateTime.Now;
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                                logAction = LogAction.MedicationUpdatedWithDischarge;
                            }
                            else
                            {
                                exisitingMedication.IsLongStanding = medication.IsLongStanding;
                                exisitingMedication.MedicationType = medication.MedicationType;
                                exisitingMedication.Classification = medication.Classification;
                                if (exisitingMedication.MedicationCategory == "DC")
                                {
                                    exisitingMedication.DCDate = medication.DCDate;
                                    logAction = LogAction.MedicationDischarged;
                                }
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            }

                            if (patientRepository.UpdateMedication(medicationProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                var medication = existingMedications != null ? existingMedications.SingleOrDefault(m => m.Id == medicationId) : null;
                if (medication != null)
                {
                    var logAction = new LogAction();
                    if (medicationCategory.IsEqual(MedicationCategoryEnum.DC.ToString()))
                    {
                        medication.DCDate = dischargeDate;
                        medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        logAction = LogAction.MedicationDischarged;
                    }
                    else
                    {
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        logAction = LogAction.MedicationActivated;
                    }
                    medication.LastChangedDate = DateTime.Now;
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DischargeAllMedicationOfPatient(Guid patientId, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingMedications.Count > 0)
                {
                    var logAction = new LogAction();
                    existingMedications.ForEach((Medication medication) =>
                    {
                        if (medication.MedicationCategory == MedicationCategoryEnum.Active.ToString())
                        {
                            medication.DCDate = dischargeDate;
                            medication.LastChangedDate = DateTime.Now;
                            medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                            logAction = LogAction.MedicationDischarged;
                        }

                    });
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteMedication(Guid medicationProfileId, Guid medicationId)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medicationId))
                {
                    existingList.RemoveAll(m => m.Id == medicationId);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                }
                if (patientRepository.UpdateMedication(medicationProfile))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileHistory.ProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                medicationProfileHistory.Id = Guid.NewGuid();
                medicationProfileHistory.UserId = Current.UserId;
                medicationProfileHistory.AgencyId = Current.AgencyId;
                medicationProfileHistory.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                medicationProfileHistory.Created = DateTime.Now;
                medicationProfileHistory.Modified = DateTime.Now;
                medicationProfileHistory.Medication = medicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                if (medicationProfileHistory.PharmacyPhoneArray != null && medicationProfileHistory.PharmacyPhoneArray.Count == 3)
                {
                    medicationProfileHistory.PharmacyPhone = medicationProfileHistory.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                var physician = physicianRepository.Get(medicationProfileHistory.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    medicationProfileHistory.PhysicianData = physician.ToXml();
                }
                if (patientRepository.AddNewMedicationHistory(medicationProfileHistory))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationProfileSigned, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfile != null && medProfile.Count > 0)
            {
                var users = new List<User>();
                var userIds = medProfile.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                medProfile.ForEach(m =>
                {
                    if (!m.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == m.UserId);
                        if (user != null)
                        {
                            m.UserName = user.DisplayName;
                        }
                    }
                }
                );
            }
            return medProfile.OrderByDescending(m => m.SignedDate.ToShortDateString().ToOrderedDate()).ToList();
        }

        public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        {
            var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var medicationList = new List<Medication>();
            if (medicationHistory != null)
            {
                medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
            }
            return medicationList;
        }
      
        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var profile = new MedicationProfileSnapshotViewData();
            if (med != null)
            {
                profile.MedicationProfile = med;
                profile.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                profile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                profile.Allergies = GetAllergies(patientId);
                if (profile.Patient != null)
                {
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        profile.PhysicianName = physician.DisplayName;
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty()) profile.PharmacyName = med.PharmacyName;
                    else profile.PharmacyName = profile.Patient.PharmacyName;
                    if (med.PharmacyPhone.IsNotNullOrEmpty()) profile.PharmacyPhone = med.PharmacyPhone;
                    else profile.PharmacyPhone = profile.Patient.PharmacyPhone;
                    var currentEpisode = patientRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId,DateTime.Now);
                    if (currentEpisode != null)
                    {
                        profile.EpisodeId = currentEpisode.Id;
                        profile.StartDate = currentEpisode.StartDate;
                        profile.EndDate = currentEpisode.EndDate;
                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = assessmentService.GetEpisodeAssessment(currentEpisode);
                            if (assessment != null)
                            {
                                var questions = assessment.ToDictionary();
                                if (questions != null)
                                {
                                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                                    {
                                        profile.PrimaryDiagnosis = questions["M1020PrimaryDiagnosis"].Answer;
                                    }
                                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                                    {
                                        profile.SecondaryDiagnosis = questions["M1022PrimaryDiagnosis1"].Answer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return profile;
        }

        public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        {
            var viewData = new MedicationProfileSnapshotViewData();
            var snapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (snapShot != null)
            {
                viewData.MedicationProfile = snapShot.ToProfile();
                viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                viewData.Patient = patientRepository.GetPatientOnly(snapShot.PatientId, Current.AgencyId);
                viewData.Allergies = snapShot.Allergies;
                viewData.PrimaryDiagnosis = snapShot.PrimaryDiagnosis;
                viewData.SecondaryDiagnosis = snapShot.SecondaryDiagnosis;
                AgencyPhysician physician = null;
                if (snapShot.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = snapShot.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        viewData.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        if (!snapShot.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                viewData.PhysicianName = physician.DisplayName;
                            }
                        }
                    }
                }
                else
                {
                    if (!snapShot.PhysicianId.IsEmpty())
                    {
                        physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            viewData.PhysicianName = physician.DisplayName;
                        }
                    }
                }

                if (snapShot.SignatureText.IsNotNullOrEmpty())
                {
                    viewData.SignatureText = snapShot.SignatureText;
                    viewData.SignatureDate = snapShot.SignedDate;
                }
                else
                {
                    if (!snapShot.UserId.IsEmpty())
                    {
                        var displayName = UserEngine.GetName(snapShot.UserId, Current.AgencyId);
                        if (displayName.IsNotNullOrEmpty())
                        {
                            viewData.SignatureText = string.Format("Electronically Signed by: {0}", displayName);
                            viewData.SignatureDate = snapShot.SignedDate;
                        }
                    }
                }
                if (viewData.Patient != null)
                {
                    if (snapShot.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = snapShot.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }

                    if (snapShot.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = snapShot.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }

                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, snapShot.EpisodeId, snapShot.PatientId);
                    if (episode != null)
                    {
                        viewData.EpisodeId = episode.Id;
                        viewData.StartDate = episode.StartDate;
                        viewData.EndDate = episode.EndDate;
                    }
                }
            }
            return viewData;
        }

        #endregion
     
        #region Physician

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (var agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !physicianRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
                if (i > 0 && result)
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PhysicianLinked, "One of the physician is set as primary.");
                }
            }
            return result;
        }

        public bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary)
        {
            if (physicianRepository.Link(patientId, physicianId, isPrimary))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianLinked, isPrimary ? "Primary" : string.Empty);
                return true;
            }
            return false;
        }

        public bool UnlinkPhysician(Guid patientId, Guid physicianId)
        {
            if (patientRepository.DeletePhysicianContact(physicianId, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianUnLinked, string.Empty);
                return true;
            }
            return false;
        }

        #endregion

        #region Physician Order

        public JsonViewData ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isOrderUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {

                    var physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                    if (physicianOrder != null)
                    {
                        viewData.PatientId = patientId;
                        viewData.EpisodeId = episodeId;

                        var oldStatus = scheduleEvent.Status;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;

                        if (actionType == "Approve")
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            scheduleEvent.Status = physicianOrder.Status;
                            scheduleEvent.InPrintQueue = true;
                            description = "Approved By:" + Current.UserFullName;
                            isActionSet = true;
                        }
                        else if (actionType == "Return")
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            scheduleEvent.Status = physicianOrder.Status;
                            description = "Returned By: " + Current.UserFullName;
                            isActionSet = true;

                        }
                        else if (actionType == "Print")
                        {
                            scheduleEvent.InPrintQueue = false;
                            isOrderUpdates = false;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isOrderUpdates)
                                {
                                    physicianOrder.Modified = DateTime.Now;
                                    if (patientRepository.UpdateOrderModel(physicianOrder))
                                    {
                                        viewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
                                        viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == (int)ScheduleStatus.OrderSubmittedPendingReview;
                                        viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                        viewData.IsOrdersToBeSentRefresh = viewData.IsDataCentersRefresh && scheduleEvent.Status == (int)ScheduleStatus.OrderToBeSentToPhysician;
                                        viewData.IsPhysicianOrderPOCRefresh = viewData.IsDataCentersRefresh;
                                        viewData.isSuccessful = true;
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, description);
                                        }
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                        viewData.isSuccessful = false;
                                    }
                                }
                                else
                                {
                                    viewData.isSuccessful = true;
                                }
                            }
                        }
                    }
                }
            }

            return viewData;
        }

        public List<Order> GetOrdersFromScheduleEvents(Guid patientId, DateTime startDate, DateTime endDate, List<ScheduleEvent> schedules)
        {
            var orders = new List<Order>();
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds, startDate, endDate);
                    physicianOrders.ForEach(po =>
                    {
                        var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                        var deleteUrl = !Current.IfOnlyRole(AgencyRoles.Auditor) ? string.Format(" | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteOrder('{0}','{1}','{2}');\">Delete</a>", po.Id, po.PatientId, po.EpisodeId) : string.Empty;
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            PatientId = po.PatientId,
                            EpisodeId = po.EpisodeId,
                            Type = OrderType.PhysicianOrder,
                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                            Number = po.OrderNumber,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                            PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true) + deleteUrl,
                            CreatedDate = po.OrderDateFormatted.ToZeroFilled(),
                            ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue,
                            SendDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue,
                            Status = po.Status
                        });
                    });
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
                    planofCareOrders.ForEach(poc =>
                    {
                        var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485,
                                Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician) || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPatientPlanofCaresStandAlones(Current.AgencyId, patientId, planofCareStandAloneOrdersIds);
                    planofCareStandAloneOrders.ForEach(poc =>
                    {
                        var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485StandAlone,
                                Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician) || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
                    faceToFaceEncounters.ForEach(ffe =>
                    {
                        var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = ffe.Id,
                                PatientId = ffe.PatientId,
                                EpisodeId = ffe.EpisodeId,
                                Type = OrderType.FaceToFaceEncounter,
                                Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                Number = ffe.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician) || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? ffe.RequestDate : DateTime.MinValue,
                                Status = ffe.Status
                            });
                        }
                    });
                }

                var evalOrdersSchedule = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.SingleOrDefault(s => s.EventId == eval.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    EpisodeId = eval.EpisodeId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue,
                                    SendDate = evnt.Status == ((int)ScheduleStatus.EvalSentToPhysician) || evnt.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? eval.SentDate : DateTime.MinValue,
                                    Status = eval.Status,

                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return GetOrdersFromScheduleEvents(patientId, startDate, endDate, scheduleRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate));
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        {
            var orders = new List<Order>();
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var disciplineTasks = DisciplineTaskFactory.AllPhysicianOrders().ToArray();
                var schedules = scheduleRepository.GetCurrentAndPerviousOrders(Current.AgencyId, episode, disciplineTasks);
                if (schedules != null && schedules.Count > 0)
                {
                    orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
                }
            }
            //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //if (episode != null)
            //{
            //    var schedules = new List<ScheduleEvent>();
            //    if (episode.Schedule.IsNotNullOrEmpty())
            //    {
            //        var currentSchedules = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && !s.IsOrderForNextEpisode).ToList();
            //        if (currentSchedules != null && currentSchedules.Count > 0)
            //        {
            //            schedules.AddRange(currentSchedules);
            //        }
            //        var previousEpisode = patientRepository.GetPreviousEpisodeFluent(Current.AgencyId, patientId, episode.StartDate);
            //        if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && s.IsOrderForNextEpisode).ToList();
            //            if (previousSchedules != null && previousSchedules.Count > 0)
            //            {
            //                schedules.AddRange(previousSchedules);
            //            }
            //        }
            //    }
            //    orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
            //}
            return orders;
        }

        public bool DeletePhysicianOrder(Guid orderId, Guid patientId, Guid episodeId)
        {
            bool result = false;
            try
            {
                result = ToggleScheduleStatusNew(episodeId, patientId, orderId, true);
                //var physicianOrder = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                //if (physicianOrder != null && physicianOrder.EpisodeId == episodeId && physicianOrder.PatientId == patientId)
                //{
                //    if (patientRepository.MarkOrderAsDeleted(orderId, patientId, Current.AgencyId, true))
                //    {
                //        if (!physicianOrder.EpisodeId.IsEmpty())
                //        {
                //            var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId);
                //            if (scheduleEvent != null)
                //            {
                //                if (patientRepository.DeleteScheduleEvent(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId, (int)DisciplineTasks.PhysicianOrder))
                //                {
                //                    if (!scheduleEvent.UserId.IsEmpty())
                //                    {
                //                        userRepository.RemoveScheduleEvent(Current.AgencyId, patientId, orderId, scheduleEvent.UserId);
                //                    }
                //                    Auditor.Log(physicianOrder.EpisodeId, physicianOrder.PatientId, physicianOrder.Id, Actions.Deleted, DisciplineTasks.PhysicianOrder);
                //                }
                //            }
                //        }
                //        result = true;
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public PhysicianOrder GetOrderPrint()
        {
            var order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, Current.AgencyId);
            if (order == null) order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            order.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
            {
                var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    order.Physician = physician;
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
            }
            else
            {
                order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
            }

            order.Allergies = GetAllergies(patientId);
            var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
            if (episode != null)
            {
                if (order.IsOrderForNextEpisode)
                {
                    order.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    order.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
                if (order.Patient != null)
                {
                    order.Patient.StartofCareDate = episode.StartOfCareDate;
                }
            }
            return order;
        }

        #endregion

        #region Communication Note

        public JsonViewData ProcessCommunicationNotes(string button, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isNoteUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (communicationNote != null)
                {
                    if (!communicationNote.EpisodeId.IsEmpty())
                    {
                        var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, communicationNote.EpisodeId, eventId);
                        if (scheduleEvent != null)
                        {
                            viewData.PatientId = scheduleEvent.PatientId;
                            viewData.EpisodeId = scheduleEvent.EpisodeId;

                            var oldStatus = scheduleEvent.Status;
                            var oldPrintQueue = scheduleEvent.InPrintQueue;
                            var description = string.Empty;
                            var action = new Actions();

                            if (button == "Approve")
                            {
                                communicationNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = communicationNote.Status;
                                isActionSet = true;
                                description = "Approved By:" + Current.UserFullName;
                                action = Actions.Approved;
                            }
                            else if (button == "Return")
                            {
                                communicationNote.Status = ((int)ScheduleStatus.NoteReturned);
                                communicationNote.SignatureText = string.Empty;
                                communicationNote.SignatureDate = DateTime.MinValue;
                                scheduleEvent.Status = communicationNote.Status;
                                isActionSet = true;
                                description = "Returned By:" + Current.UserFullName;
                                action = Actions.Returned;
                            }
                            else if (button == "Print")
                            {
                                scheduleEvent.InPrintQueue = false;
                                isNoteUpdates = false;
                                isActionSet = true;
                            }

                            if (isActionSet)
                            {
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (isNoteUpdates)
                                    {
                                        communicationNote.Modified = DateTime.Now;
                                        if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                                        {
                                            viewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
                                            viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature);
                                            viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            viewData.IsCommunicationNoteRefresh = viewData.IsDataCentersRefresh;
                                            viewData.isSuccessful = true;
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, DisciplineTasks.CommunicationNote, description);
                                        }
                                        else
                                        {
                                            scheduleEvent.Status = oldStatus;
                                            scheduleEvent.InPrintQueue = oldPrintQueue;
                                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                            viewData.isSuccessful = false;
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var communicationNotes = new List<CommunicationNote>();
            var schedules = scheduleRepository.GetScheduleEventsByStatusDisciplineAndRange(Current.AgencyId, branchId, patientStatus, startDate, endDate, new int[] { }, new int[] { (int)DisciplineTasks.CommunicationNote });
            if (schedules != null && schedules.Count > 0)
            {
                var communicationNoteIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                communicationNotes = patientRepository.GetCommunicationNoteByIds(Current.AgencyId, communicationNoteIds);
                if (communicationNotes != null && communicationNotes.Count > 0)
                {
                    var userIds = schedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                    communicationNotes.ForEach(c =>
                    {
                        var evnt = schedules.FirstOrDefault(e => e.EventId == c.Id);
                        if (evnt != null)
                        {
                            c.DisplayName = evnt.PatientName;
                            if (!evnt.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == evnt.UserId);
                                if (user != null)
                                {
                                    c.UserDisplayName = user.DisplayName;
                                }
                            }
                        }

                    });
                }
            }
            return communicationNotes;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid patientId)
        {
            var commNotes = patientRepository.GetCommunicationNotes(Current.AgencyId, patientId);
            if (commNotes != null)
            {
                commNotes.ForEach(c =>
                {
                    if (!c.UserId.IsEmpty())
                    {
                        var userName = UserEngine.GetName(c.UserId, Current.AgencyId);
                        if (userName.IsNotNullOrEmpty())
                        {
                            c.UserDisplayName = userName;
                        }
                    }
                    var physician = PhysicianEngine.Get(c.PhysicianId, Current.AgencyId);
                    c.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, c.PatientId, c.EpisodeId, c.Id);
                    if (scheduleEvent != null)
                    {
                        c.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return commNotes;
        }

        public bool DeleteCommunicationNote(Guid Id, Guid patientId)
        {
            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            bool result = false;
            if (communicationNote != null)
            {
                if (!communicationNote.EpisodeId.IsEmpty())
                {
                    result = ToggleScheduleStatusNew(communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, true);
                    //DeleteSchedule(communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, communicationNote.UserId, (int)DisciplineTasks.CommunicationNote);
                    //result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                }
                //else
                //{
                //    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                //}
            }
            return result;
        }

        public CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId)
        {
            var note = new CommunicationNote();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {

                note = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (note != null)
                {
                    if ((note.Status == (int)ScheduleStatus.NoteCompleted || note.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !note.PhysicianId.IsEmpty() && note.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = note.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            note.Physician = physician;
                        }
                        else
                        {
                            note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }

                    note.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                }
            }
            return note;
        }

        #endregion

        #region Face To FaceEncounter

        public bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                faceToFaceEncounter.UserId = Current.UserId;
                faceToFaceEncounter.AgencyId = Current.AgencyId;
                faceToFaceEncounter.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                faceToFaceEncounter.OrderNumber = patientRepository.GetNextOrderNumber();
                faceToFaceEncounter.Modified = DateTime.Now;
                faceToFaceEncounter.Created = DateTime.Now;
                if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        faceToFaceEncounter.PhysicianData = physician.ToXml();
                    }
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    AgencyId = Current.AgencyId,
                    EventId = faceToFaceEncounter.Id,
                    UserId = faceToFaceEncounter.UserId,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Status = faceToFaceEncounter.Status,
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = faceToFaceEncounter.RequestDate,
                    VisitDate = faceToFaceEncounter.RequestDate,
                    DisciplineTask = (int)DisciplineTasks.FaceToFaceEncounter
                };
                if (patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter))
                {
                    if (scheduleRepository.AddScheduleEvent(newScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.FaceToFaceEncounter);
                        result = true;
                    }
                    else
                    {
                        patientRepository.RemoveModel<FaceToFaceEncounter>(faceToFaceEncounter.Id);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFacePrint()
        {
            var order = new FaceToFaceEncounter();
            order.Location = agencyRepository.GetMainLocation(Current.AgencyId);
            return order;
        }

        public FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            var order = new FaceToFaceEncounter();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
                if (order != null)
                {
                    order.Patient = patient;
                    if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.Physician = physician;
                        }
                        else
                        {
                            order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                    var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                        if (order.Patient != null)
                        {
                            order.Patient.StartofCareDate = episode.StartOfCareDate;
                        }
                    }
                }
                else
                {
                    order = new FaceToFaceEncounter();
                }
                order.Location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
            }
            // order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        #endregion

        #region Episode

        public PatientEpisode CreateEpisode(Guid patientId, DateTime startDate)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(59);
            patientEpisode.IsActive = true;
            return patientEpisode;
        }

        public PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = episode.StartDate;
            patientEpisode.EndDate = episode.EndDate;
            patientEpisode.IsActive = true;
            patientEpisode.Detail = episode.Detail;
            patientEpisode.StartOfCareDate = episode.StartOfCareDate;
            patientEpisode.Details = patientEpisode.Detail.ToXml();
            return patientEpisode;
        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            bool result = false;
            if (patientRepository.AddEpisode(patientEpisode))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public JsonViewData UpdateEpisode(PatientEpisode episode)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            episode.Details = episode.Detail.ToXml();
            var episodeToEdit = patientRepository.GetEpisodeOnly(Current.AgencyId, episode.Id, episode.PatientId);
            if (episodeToEdit != null)
            {
                var oldEpisodeStatus = episodeToEdit.IsActive;
                var oldStartDate = episodeToEdit.StartDate;
                var oldEndDate = episodeToEdit.EndDate;

                var logAction = episodeToEdit.IsActive == episode.IsActive ? LogAction.EpisodeEdited : (!episode.IsActive ? LogAction.EpisodeDeactivated : LogAction.EpisodeActivated);
                episodeToEdit.IsActive = episode.IsActive;
                episodeToEdit.StartDate = episode.StartDate;
                episodeToEdit.EndDate = episode.EndDate;
                episodeToEdit.Detail = episodeToEdit.Details.ToObject<EpisodeDetail>();
                episode.Detail.Assets.AddRange(episodeToEdit.Detail.Assets);
                episodeToEdit.StartOfCareDate = episode.StartOfCareDate;
                episodeToEdit.Details = episode.Detail.ToXml();
                episodeToEdit.AdmissionId = episode.AdmissionId;
                if (patientRepository.UpdateEpisode(Current.AgencyId, episodeToEdit))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, episodeToEdit.PatientId, episodeToEdit.Id.ToString(), LogType.Episode, logAction, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.PatientId = episodeToEdit.PatientId;
                    viewData.EpisodeId = episodeToEdit.Id;
                    viewData.IsDataCentersRefresh = oldEpisodeStatus != episodeToEdit.IsActive;
                    viewData.IsActivityRefresh = oldStartDate.Date != episodeToEdit.StartDate.Date || oldEndDate.Date != episodeToEdit.EndDate.Date;
                }
            }
            return viewData;
        }

        public bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if (e.Id == episodeId)
                    {
                        continue;
                    }
                    else
                    {
                        if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        {
            var result = false;
            try
            {
                var patientEpisodes = new List<PatientEpisode>();

                if (episode != null)
                {
                    episode.EndDate = dischargeDate;
                    episode.IsLinkedToDischarge = true;
                    episode.Modified = DateTime.Now;
                    patientEpisodes.Add(episode);
                }
                var episodes = patientRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);//  database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate.Date >= dischargeDate.Date);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(es =>
                    {
                        es.Modified = DateTime.Now;
                        es.IsDischarged = true;
                    });
                    patientEpisodes.AddRange(episodes);
                }
                if (patientEpisodes.Count > 0)
                {
                    if (patientRepository.UpdateEpisodesForDischarge(Current.AgencyId, patientId, episode, patientEpisodes))
                    {
                        if (episode != null)
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
                        }
                        if (episodes != null && episodes.Count > 0)
                        {

                            //episodes.ForEach(e =>
                            //{
                            Auditor.AddGeneralMulitLog(LogDomain.Patient, episode.PatientId, episodes.Select(e => e.Id.ToString()).Distinct().ToList(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
                            //});
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool CreateEpisodeAndClaims(Patient patient)
        {
            bool result = false;
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                if (patient.StartofCareDate.Date == patient.EpisodeStartDate.Date)
                {
                    var newEvent = new ScheduleEvent
                    {
                        AgencyId = Current.AgencyId,
                        EventId = Guid.NewGuid(),
                        PatientId = patient.Id,
                        UserId = patient.UserId,
                        Discipline = Disciplines.Nursing.ToString(),
                        Status = ((int)ScheduleStatus.OasisNotYetDue),
                        DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                        EventDate = patient.EpisodeStartDate,
                        VisitDate = patient.EpisodeStartDate,
                        IsBillable = true
                    };
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (patient.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        episode.Detail.PrimaryInsurance = patient.PrimaryInsurance;
                    }
                    if (patient.SecondaryInsurance.IsNotNullOrEmpty())
                    {
                        episode.Detail.SecondaryInsurance = patient.SecondaryInsurance;
                    }

                    if (this.AddEpisode(episode))
                    {
                        newEvent.EpisodeId = episode.Id;

                        if (scheduleRepository.AddScheduleEvent(newEvent))
                        {
                            if (this.ProcessSchedule(newEvent, patientToEdit, episode))
                            {
                                var isClaimsAdded = false;
                                var IsFaceToFaceAdded = false;
                                if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
                                {
                                    var insurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                                    if ((patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000) || (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO))
                                    {
                                        isClaimsAdded = true;
                                    }
                                }

                                if (patient.IsFaceToFaceEncounterCreated)
                                {
                                    IsFaceToFaceAdded = true;
                                }
                                if (isClaimsAdded || IsFaceToFaceAdded)
                                {
                                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientToEdit.Id);
                                    if (isClaimsAdded)
                                    {
                                        //var rap = this.CreateRap(patientToEdit, episode, 0, physician);
                                        //if (rap != null)
                                        //{
                                        if (billingRepository.AddRap(patientToEdit, episode, 0, physician))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patientToEdit.Id, episode.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                        }
                                       // }
                                        //var final = this.CreateFinal(patientToEdit, episode, 0, physician);
                                        //if (final != null)
                                        //{
                                        if (billingRepository.AddFinal(patientToEdit, episode, 0, physician))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patientToEdit.Id, episode.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                        }
                                        //}
                                    }
                                    if (physician != null && IsFaceToFaceAdded)
                                    {
                                        var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = DateTime.Now };
                                        this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                                    }
                                }
                                if (Enum.IsDefined(typeof(DisciplineTasks), newEvent.DisciplineTask))
                                {
                                    Auditor.Log(newEvent.EpisodeId, newEvent.PatientId, newEvent.EventId, Actions.Add, (DisciplineTasks)newEvent.DisciplineTask);
                                }
                                result = true;
                            }
                            else
                            {
                                scheduleRepository.RemoveScheduleEventNew(newEvent);
                            }
                        }
                    }

                }
                else if (patient.EpisodeStartDate.Date > patient.StartofCareDate.Date)
                {
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        var isClaimsAdded = false;
                        var IsFaceToFaceAdded = false;
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
                        {
                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                            if ((patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000) ||
                                    (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO))
                            {
                                isClaimsAdded = true;
                            }
                        }
                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            IsFaceToFaceAdded = true;
                        }
                        if (isClaimsAdded || IsFaceToFaceAdded)
                        {
                            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientToEdit.Id);
                            if (isClaimsAdded)
                            {
                                //var rap = this.CreateRap(patientToEdit, episode, 0, physician);

                                //if (rap != null)
                                //{
                                if (billingRepository.AddRap(patientToEdit, episode, 0, physician))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientToEdit.Id, episode.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                }
                                //}
                                //var final = this.CreateFinal(patientToEdit, episode, 0, physician);
                                //if (final != null)
                                //{
                                if (billingRepository.AddFinal(patientToEdit, episode, 0, physician))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientToEdit.Id, episode.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                }
                                //}
                            }
                            if (physician != null && IsFaceToFaceAdded)
                            {
                                var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = patient.EpisodeStartDate };
                                this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                            }
                        }
                        result = true;
                    }
                }
            }

            return result;
        }

        public void DeleteEpisodeAndClaims(Patient patient)
        {
            PatientEpisode episodeDeleted = null;
            patientRepository.DeleteEpisode(Current.AgencyId, patient, out episodeDeleted);
            if (episodeDeleted != null)
            {
                billingRepository.DeleteRap(Current.AgencyId, patient.Id, episodeDeleted.Id);
                billingRepository.DeleteFinal(Current.AgencyId, patient.Id, episodeDeleted.Id);
            }
        }

        #endregion

        #region Episode Detail

        public ScheduleEvent GetScheduledEventForDetail(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);

            if (scheduleEvent != null && scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
                {
                    var order = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                    if (order != null)
                    {
                        scheduleEvent.PhysicianId = order.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
                {
                    var planofcare = assessmentService.GetPlanofCare(episodeId, patientId, eventId);
                    if (planofcare != null)
                    {
                        scheduleEvent.PhysicianId = planofcare.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
                {
                    var planofcareStandalone = assessmentService.GetPlanofCareStandAlone(episodeId, patientId, eventId);
                    if (planofcareStandalone != null)
                    {
                        scheduleEvent.PhysicianId = planofcareStandalone.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
                {
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        scheduleEvent.PhysicianId = faceToFace.PhysicianId;
                    }
                }
            }

            return scheduleEvent;
        }

        public JsonViewData UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var assetsSaved = true;
            var jsonViewData = new JsonViewData { isSuccessful = false };
            var scheduleEventToEdit = scheduleRepository.GetScheduleEventNew(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId);
            if (scheduleEventToEdit != null)
            {
                var oldStatus = scheduleEventToEdit.Status;
                var scheduleBuffer = new ScheduleEvent
                {
                    Asset = scheduleEventToEdit.Asset,
                    UserId = scheduleEventToEdit.UserId,
                    Comments = scheduleEventToEdit.Comments,
                    Status = scheduleEventToEdit.Status,
                    EventDate = scheduleEventToEdit.EventDate,
                    VisitDate = scheduleEventToEdit.VisitDate,
                    IsBillable = scheduleEventToEdit.IsBillable,
                    Surcharge = scheduleEventToEdit.Surcharge,
                    AssociatedMileage = scheduleEventToEdit.AssociatedMileage,
                    TimeIn = scheduleEventToEdit.TimeIn,
                    TimeOut = scheduleEventToEdit.TimeOut,
                    IsMissedVisit = scheduleEventToEdit.IsMissedVisit,
                    DisciplineTask = scheduleEventToEdit.DisciplineTask,
                    PhysicianId = scheduleEventToEdit.PhysicianId,
                };

                if (httpFiles != null && httpFiles.Count > 0)
                {
                    scheduleEventToEdit.Assets = (scheduleEventToEdit.Asset.IsNotNullOrEmpty() ? scheduleEventToEdit.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();

                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                scheduleEventToEdit.Assets.Add(asset.Id);
                            }
                            else
                            {
                                assetsSaved = false;
                                break;
                            }
                        }
                    }
                }
                Guid userId = scheduleEventToEdit.UserId;
                scheduleEventToEdit.UserId = scheduleEvent.UserId;
                scheduleEventToEdit.Comments = scheduleEvent.Comments;
                scheduleEventToEdit.Status = scheduleEvent.Status;
                scheduleEventToEdit.EventDate = scheduleEvent.EventDate;
                scheduleEventToEdit.VisitDate = scheduleEvent.VisitDate;
                scheduleEventToEdit.IsBillable = scheduleEvent.IsBillable;
                scheduleEventToEdit.Surcharge = scheduleEvent.Surcharge;
                scheduleEventToEdit.AssociatedMileage = scheduleEvent.AssociatedMileage;
                scheduleEventToEdit.TimeIn = scheduleEvent.TimeIn;
                scheduleEventToEdit.TimeOut = scheduleEvent.TimeOut;
                scheduleEventToEdit.IsMissedVisit = scheduleEvent.IsMissedVisit;
                scheduleEventToEdit.DisciplineTask = scheduleEvent.DisciplineTask;
                scheduleEventToEdit.PhysicianId = scheduleEvent.PhysicianId;
                if (assetsSaved)
                {
                    scheduleEventToEdit.Asset = scheduleEventToEdit.Assets.ToXml();
                }

                if (assetsSaved && scheduleRepository.UpdateScheduleEventNew(scheduleEventToEdit))
                {
                    if (ProcessEditDetail(scheduleEventToEdit, scheduleEventToEdit.EpisodeId))
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEventToEdit.DisciplineTask))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEventToEdit.EpisodeId, scheduleEventToEdit.PatientId, scheduleEventToEdit.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEventToEdit.DisciplineTask);
                        }
                        var isStatusChange = oldStatus != scheduleEventToEdit.Status;
                        jsonViewData.isSuccessful = true;
                        jsonViewData.EpisodeId = scheduleEventToEdit.EpisodeId;
                        jsonViewData.PatientId = scheduleEventToEdit.PatientId;
                        jsonViewData.IsCaseManagementRefresh = isStatusChange && (oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview) || oldStatus == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                        jsonViewData.IsDataCentersRefresh = oldStatus != scheduleEventToEdit.Status;
                        jsonViewData.IsMyScheduleTaskRefresh = jsonViewData.IsDataCentersRefresh && scheduleEventToEdit.UserId == Current.UserId && !scheduleEventToEdit.IsComplete && scheduleEventToEdit.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEventToEdit.EventDate.Date <= DateTime.Now.AddDays(14);

                    }
                    else
                    {
                        scheduleEventToEdit.UserId = scheduleBuffer.UserId;
                        scheduleEventToEdit.Comments = scheduleBuffer.Comments;
                        scheduleEventToEdit.Status = scheduleBuffer.Status;
                        scheduleEventToEdit.EventDate = scheduleBuffer.EventDate;
                        scheduleEventToEdit.VisitDate = scheduleBuffer.VisitDate;
                        scheduleEventToEdit.IsBillable = scheduleBuffer.IsBillable;
                        scheduleEventToEdit.Surcharge = scheduleBuffer.Surcharge;
                        scheduleEventToEdit.AssociatedMileage = scheduleBuffer.AssociatedMileage;
                        scheduleEventToEdit.TimeIn = scheduleBuffer.TimeIn;
                        scheduleEventToEdit.TimeOut = scheduleBuffer.TimeOut;
                        scheduleEventToEdit.DisciplineTask = scheduleBuffer.DisciplineTask;
                        scheduleEventToEdit.PhysicianId = scheduleBuffer.PhysicianId;
                        scheduleEventToEdit.Asset = scheduleBuffer.Asset;
                        scheduleRepository.UpdateScheduleEventNew(scheduleEventToEdit);
                        jsonViewData.isSuccessful = false;
                    }
                }

            }
            return jsonViewData;
        }

        public JsonViewData UpdateScheduleEventDetailForMoveToOtherEpisode(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var assetsSaved = true;
            var jsonViewData = new JsonViewData { isSuccessful = false };
            if (scheduleEvent != null && !scheduleEvent.NewEpisodeId.IsEmpty())
            {
                var newPatientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
                if (newPatientEpisode != null)
                {
                    var scheduleEventToEdit = scheduleRepository.GetScheduleEventNew(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId); //oldScheduleEvents.FirstOrDefault(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                    if (scheduleEventToEdit != null)
                    {
                        var oldStatus = scheduleEventToEdit.Status;
                        var scheduleBuffer = new ScheduleEvent
                        {
                            Asset = scheduleEventToEdit.Asset,
                            UserId = scheduleEventToEdit.UserId,
                            Comments = scheduleEventToEdit.Comments,
                            Status = scheduleEventToEdit.Status,
                            EventDate = scheduleEventToEdit.EventDate,
                            VisitDate = scheduleEventToEdit.VisitDate,
                            IsBillable = scheduleEventToEdit.IsBillable,
                            Surcharge = scheduleEventToEdit.Surcharge,
                            AssociatedMileage = scheduleEventToEdit.AssociatedMileage,
                            TimeIn = scheduleEventToEdit.TimeIn,
                            TimeOut = scheduleEventToEdit.TimeOut,
                            IsMissedVisit = scheduleEventToEdit.IsMissedVisit,
                            DisciplineTask = scheduleEventToEdit.DisciplineTask,
                            PhysicianId = scheduleEventToEdit.PhysicianId,
                            EpisodeId = scheduleEventToEdit.EpisodeId
                        };

                        if (httpFiles != null && httpFiles.Count > 0)
                        {
                            scheduleEventToEdit.Assets = (scheduleEventToEdit.Asset.IsNotNullOrEmpty() ? scheduleEventToEdit.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();

                            foreach (string key in httpFiles.AllKeys)
                            {
                                var file = httpFiles.Get(key);
                                if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                {
                                    var binaryReader = new BinaryReader(file.InputStream);
                                    var asset = new Asset
                                    {
                                        FileName = file.FileName,
                                        AgencyId = Current.AgencyId,
                                        ContentType = file.ContentType,
                                        FileSize = file.ContentLength.ToString(),
                                        Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                    };
                                    if (assetRepository.Add(asset))
                                    {
                                        scheduleEventToEdit.Assets.Add(asset.Id);
                                    }
                                    else
                                    {
                                        assetsSaved = false;
                                        break;
                                    }
                                }
                            }
                        }
                        var oldEpisodeId = scheduleEventToEdit.EpisodeId;
                        Guid userId = scheduleEventToEdit.UserId;
                        scheduleEventToEdit.EpisodeId = newPatientEpisode.Id;
                        scheduleEventToEdit.StartDate = newPatientEpisode.StartDate;
                        scheduleEventToEdit.EndDate = newPatientEpisode.EndDate;
                        scheduleEventToEdit.UserId = scheduleEvent.UserId;
                        scheduleEventToEdit.Comments = scheduleEvent.Comments;
                        scheduleEventToEdit.Status = scheduleEvent.Status;
                        scheduleEventToEdit.EventDate = scheduleEvent.EventDate;
                        scheduleEventToEdit.VisitDate = scheduleEvent.VisitDate;
                        scheduleEventToEdit.IsBillable = scheduleEvent.IsBillable;
                        scheduleEventToEdit.Surcharge = scheduleEvent.Surcharge;
                        scheduleEventToEdit.AssociatedMileage = scheduleEvent.AssociatedMileage;
                        scheduleEventToEdit.TimeIn = scheduleEvent.TimeIn;
                        scheduleEventToEdit.TimeOut = scheduleEvent.TimeOut;
                        // schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
                        scheduleEventToEdit.DisciplineTask = scheduleEvent.DisciplineTask;
                        scheduleEventToEdit.PhysicianId = scheduleEvent.PhysicianId;

                        if (assetsSaved)
                        {
                            scheduleEventToEdit.Asset = scheduleEventToEdit.Assets.ToXml();
                        }

                        if (assetsSaved && scheduleRepository.UpdateScheduleEventNew(scheduleEventToEdit))
                        {
                            if (ProcessEditDetailForReassign(scheduleEventToEdit, oldEpisodeId))
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEventToEdit.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEventToEdit.EpisodeId, scheduleEventToEdit.PatientId, scheduleEventToEdit.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEventToEdit.DisciplineTask, "Reassigned from other episode.");
                                }
                                var isStatusChange = oldStatus != scheduleEventToEdit.Status;
                                jsonViewData.isSuccessful = true;
                                jsonViewData.EpisodeId = scheduleBuffer.EpisodeId;
                                jsonViewData.PatientId = scheduleBuffer.PatientId;
                                jsonViewData.IsCaseManagementRefresh = isStatusChange && (oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview) || oldStatus == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                                jsonViewData.IsDataCentersRefresh = oldStatus != scheduleEventToEdit.Status;
                                jsonViewData.IsMyScheduleTaskRefresh = jsonViewData.IsDataCentersRefresh && scheduleEventToEdit.UserId == Current.UserId && !scheduleEventToEdit.IsComplete && scheduleEventToEdit.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEventToEdit.EventDate.Date <= DateTime.Now.AddDays(14);

                            }
                            else
                            {
                                scheduleEventToEdit.UserId = scheduleBuffer.UserId;
                                scheduleEventToEdit.Comments = scheduleBuffer.Comments;
                                scheduleEventToEdit.Status = scheduleBuffer.Status;
                                scheduleEventToEdit.EventDate = scheduleBuffer.EventDate;
                                scheduleEventToEdit.VisitDate = scheduleBuffer.VisitDate;
                                scheduleEventToEdit.IsBillable = scheduleBuffer.IsBillable;
                                scheduleEventToEdit.Surcharge = scheduleBuffer.Surcharge;
                                scheduleEventToEdit.AssociatedMileage = scheduleBuffer.AssociatedMileage;
                                scheduleEventToEdit.TimeIn = scheduleBuffer.TimeIn;
                                scheduleEventToEdit.TimeOut = scheduleBuffer.TimeOut;
                                scheduleEventToEdit.DisciplineTask = scheduleBuffer.DisciplineTask;
                                scheduleEventToEdit.PhysicianId = scheduleBuffer.PhysicianId;
                                scheduleEventToEdit.EpisodeId = scheduleBuffer.EpisodeId;
                                scheduleEventToEdit.Asset = scheduleBuffer.Asset;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEventToEdit);
                                jsonViewData.isSuccessful = false;
                            }
                        }
                    }
                }
            }
            return jsonViewData;
        }

        #endregion

        //public List<ScheduleEvent> GetScheduledEventsForCurrentEpisode(PatientEpisode patientEpisode, string discipline)
        //{
        //    var patientEvents = new List<ScheduleEvent>();
        //    if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
        //    {
        //        var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true && s.DisciplineTask > 0 && (discipline.IsEqual("ALL") ? true : s.Discipline.IsEqual(discipline))).ToList(); ;
        //        if (scheduleEvents != null && scheduleEvents.Count > 0)
        //        {
        //            ProcessScheduleEventsActions(patientEpisode.PatientId, scheduleEvents, patientEpisode, discipline);
        //            patientEvents.AddRange(scheduleEvents);
        //            //if (scheduleEvents != null && scheduleEvents.Count > 0)
        //            //{
        //            //    var userIds = new List<Guid>();
        //            //    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
        //            //    if (returnComments != null && returnComments.Count > 0)
        //            //    {
        //            //        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //            //        if (returnUserIds != null && returnUserIds.Count > 0)
        //            //        {
        //            //            userIds.AddRange(returnUserIds);
        //            //        }
        //            //    }
        //            //    var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //            //    if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //            //    {
        //            //        userIds.AddRange(scheduleUserIds);
        //            //    }
        //            //    if (userIds != null && userIds.Count > 0)
        //            //    {
        //            //        userIds = userIds.Distinct().ToList();
        //            //    }
        //            //    var users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);
        //            //    var eventIds = scheduleEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //            //    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);

        //            //    scheduleEvents.ForEach(s =>
        //            //    {
        //            //        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
        //            //        s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
        //            //        var hideTask = false;

        //            //        if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
        //            //        {
        //            //            hideTask = true;

        //            //            if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA)
        //            //            {
        //            //                hideTask = false;
        //            //            }
        //            //        }

        //            //        if (!hideTask)
        //            //        {
        //            //            if (!s.UserId.IsEmpty())
        //            //            {
        //            //                var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //            //                if (user != null)
        //            //                {
        //            //                    s.UserName = user.DisplayName;
        //            //                }
        //            //            }
        //            //            s.EpisodeNotes = detail.Comments.Clean();
        //            //            if (s.IsMissedVisit)
        //            //            {
        //            //                var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId);
        //            //                if (missedVisit != null)
        //            //                {
        //            //                    s.MissedVisitComments = missedVisit.ToString();
        //            //                    s.Status = missedVisit.Status.ToString();
        //            //                }
        //            //            }
        //            //            s.EventDate = s.EventDate.ToZeroFilled();
        //            //            s.PatientId = patientEpisode.PatientId;
        //            //            s.EndDate = patientEpisode.EndDate;
        //            //            s.StartDate = patientEpisode.StartDate;
        //            //            Common.Url.Set(s, true, true);
        //            //            patientEvents.Add(s);
        //            //        }
        //            //    });
        //            //}
        //        }
        //    }
        //    return patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
        //}

             

        //public bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    bool result = false;
        //    if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
        //    {
        //        var deletedItem = patientRepository.GetDeletedItem(Current.AgencyId, episodeId, patientId);
        //        if (deletedItem != null && deletedItem.Schedule.IsNotNullOrEmpty())
        //        {
        //            var deletedEvents = deletedItem.Schedule.ToObject<List<ScheduleEvent>>();
        //            if (deletedEvents != null && deletedEvents.Count > 0)
        //            {
        //                var deletedEvent = deletedEvents.Find(e => e.EventId == eventId);

        //                if (deletedEvent != null)
        //                {
        //                    deletedEvents.RemoveAll(e => e.EventId == eventId);
        //                }
        //                deletedItem.Schedule = deletedEvents.ToXml();
        //                if (patientRepository.UpdateDeletedItem(deletedItem))
        //                {
        //                    if (UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), false))
        //                    {
        //                        if (patientRepository.AddNewScheduleEvent(Current.AgencyId, patientId, episodeId, deletedEvent))
        //                        {
        //                            if (!deletedEvent.UserId.IsEmpty())
        //                            {
        //                                patientRepository.AddNewUserEvent(Current.AgencyId, patientId, deletedEvent.ToUserEvent());
        //                            }
        //                            Auditor.Log(episodeId, patientId, eventId, Actions.Restored, (DisciplineTasks)deletedEvent.DisciplineTask);
        //                            result = true;
        //                        }
        //                        else
        //                        {
        //                            UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), true);
        //                            if (deletedEvent != null)
        //                            {
        //                                deletedEvents.Add(deletedEvent);
        //                                deletedItem.Schedule = deletedEvents.ToXml();
        //                                patientRepository.UpdateDeletedItem(deletedItem);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (deletedEvent != null)
        //                        {
        //                            deletedEvents.Add(deletedEvent);
        //                            deletedItem.Schedule = deletedEvents.ToXml();
        //                            patientRepository.UpdateDeletedItem(deletedItem);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return result;
        //}

       

        //public bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int task)
        //{
        //    bool result = false;
        //    if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && task >= 0 && Enum.IsDefined(typeof(DisciplineTasks), task))
        //    {
        //        if (patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, eventId, task))
        //        {
        //            result = true;

        //            UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)task).ToString(), true);

        //            if (!employeeId.IsEmpty())
        //            {
        //                if (userRepository.DeleteScheduleEvent(patientId, eventId, employeeId))
        //                {
        //                    result = true;
        //                }
        //            }
        //            Auditor.Log(episodeId, patientId, eventId, Actions.Deleted, (DisciplineTasks)task);
        //        }
        //    }

        //    return result;
        //}

        #region Schedule Event Action

        public NoteJsonViewData SaveNotes(string button, FormCollection formCollection)
        {
            var returnData = new NoteJsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                string type = keys.Contains("Type") ? formCollection["Type"] : string.Empty;
                if (type.IsNotNullOrEmpty())
                {
                    Guid eventId = keys.Contains(string.Format("{0}_EventId", type)) ? formCollection.Get(string.Format("{0}_EventId", type)).ToGuid() : Guid.Empty;
                    Guid episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                    Guid patientId = keys.Contains(string.Format("{0}_PatientId", type)) ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;
                    var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
                    var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
                    var sendAsOrder = keys.Contains(string.Format("{0}_SendAsOrder", type)) && formCollection[string.Format("{0}_SendAsOrder", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_SendAsOrder", type)] : string.Empty;
                    var surcharge = keys.Contains(string.Format("{0}_Surcharge", type)) && formCollection[string.Format("{0}_Surcharge", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_Surcharge", type)] : string.Empty;
                    var mileage = keys.Contains(string.Format("{0}_AssociatedMileage", type)) && formCollection[string.Format("{0}_AssociatedMileage", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_AssociatedMileage", type)] : string.Empty;
                    var date = keys.Contains(string.Format("{0}_VisitDate", type)) ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime() : DateTime.MinValue;
                    int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;

                    bool isActionSet = false;


                    if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);

                        if (scheduleEvent != null)
                        {
                            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                            if (patientVisitNote != null)
                            {
                                patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                                patientVisitNote.Note = patientVisitNote.Questions.ToXml();

                                patientVisitNote.PhysicianId = keys.Contains(string.Format("{0}_PhysicianId", type)) ? formCollection.Get(string.Format("{0}_PhysicianId", type)).ToGuid() : Guid.Empty;

                                var statusAction = Actions.StatusChange;

                                var oldStatus = scheduleEvent.Status;
                                var oldPrintQueue = scheduleEvent.InPrintQueue;
                                var oldTimeIn = scheduleEvent.TimeIn;
                                var oldTimeOut = scheduleEvent.TimeOut;
                                var oldDisciplineTask = scheduleEvent.DisciplineTask;
                                var oldVisitDate = scheduleEvent.VisitDate;
                                var oldSurcharge = scheduleEvent.Surcharge;
                                var oldAssociatedMileage = scheduleEvent.AssociatedMileage;
                                var oldSendAsOrder = scheduleEvent.SendAsOrder;

                                scheduleEvent.SendAsOrder = sendAsOrder;

                                var description = string.Empty;
                                var isCaseManagementRefresh = false;

                                if (button == "Save")
                                {
                                    if (!(Current.HasRight(Permissions.AccessCaseManagement) && oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature)))
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                    }
                                    else
                                    {

                                    }
                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();

                                    scheduleEvent.Status = patientVisitNote.Status;
                                    scheduleEvent.TimeIn = timeIn;
                                    scheduleEvent.TimeOut = timeOut;
                                    scheduleEvent.Surcharge = surcharge;
                                    scheduleEvent.AssociatedMileage = mileage;
                                    scheduleEvent.DisciplineTask = disciplineTask;
                                    if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary) && date.Date > DateTime.MinValue.Date)
                                    {
                                        scheduleEvent.VisitDate = date;
                                    }
                                    isActionSet = true;

                                }
                                else if (button == "Complete")
                                {
                                    isCaseManagementRefresh = true;
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                    if (Current.HasRight(Permissions.BypassCaseManagement))
                                    {
                                        isCaseManagementRefresh = false;
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                                        {
                                            if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                            else if (sendAsOrder.Equals("1"))
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                            else
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                            }
                                        }
                                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                                        {
                                            if (sendAsOrder.Equals("1"))
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                        }
                                    }

                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString()
                                        || type == DisciplineTasks.SixtyDaySummary.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }

                                    patientVisitNote.UserId = Current.UserId;
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                    scheduleEvent.Status = patientVisitNote.Status;
                                    scheduleEvent.Surcharge = surcharge;
                                    scheduleEvent.AssociatedMileage = mileage;
                                    scheduleEvent.TimeIn = timeIn;
                                    scheduleEvent.TimeOut = timeOut;
                                    scheduleEvent.DisciplineTask = disciplineTask;
                                    scheduleEvent.InPrintQueue = true;
                                    scheduleEvent.SendAsOrder = sendAsOrder;
                                    if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary) && date.Date > DateTime.MinValue.Date)
                                    {
                                        scheduleEvent.VisitDate = date;
                                    }
                                    isActionSet = true;
                                }
                                else if (button == "Approve")
                                {
                                    isCaseManagementRefresh = oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview) || oldStatus == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);

                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                                    {
                                        if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                        else if (sendAsOrder.Equals("1"))
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                        else
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                        }
                                    }

                                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                                    {
                                        if (sendAsOrder.Equals("1"))
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                    }

                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString()
                                        || type == DisciplineTasks.SixtyDaySummary.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }

                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                    scheduleEvent.TimeIn = timeIn;
                                    scheduleEvent.TimeOut = timeOut;
                                    scheduleEvent.AssociatedMileage = mileage;
                                    scheduleEvent.Surcharge = surcharge;
                                    scheduleEvent.InPrintQueue = true;
                                    scheduleEvent.Status = patientVisitNote.Status;
                                    scheduleEvent.ReturnReason = string.Empty;
                                    scheduleEvent.SendAsOrder = sendAsOrder;

                                    isActionSet = true;
                                }
                                else if (button == "Return")
                                {
                                    isCaseManagementRefresh = oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview) || oldStatus == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);

                                    var returnSignature = keys.Contains(string.Format("{0}_ReturnForSignature", type)) && formCollection[string.Format("{0}_ReturnForSignature", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_ReturnForSignature", type)] : string.Empty;
                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                    if (returnSignature.IsNotNullOrEmpty())
                                    {
                                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                    }
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    scheduleEvent.Status = patientVisitNote.Status;
                                    isActionSet = true;
                                }
                                if (isActionSet)
                                {
                                    if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                    {
                                        patientVisitNote.Modified = DateTime.Now;
                                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                                        {
                                            returnData.IsDataCentersRefresh = oldStatus != (scheduleEvent.Status);
                                            returnData.IsCaseManagementRefresh = returnData.IsDataCentersRefresh && isCaseManagementRefresh;
                                            returnData.IsMyScheduleTaskRefresh = returnData.IsDataCentersRefresh && scheduleEvent.UserId == Current.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            returnData.isSuccessful = true;
                                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                            {
                                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                            }
                                        }
                                        else
                                        {
                                            scheduleEvent.Status = oldStatus;
                                            scheduleEvent.InPrintQueue = oldPrintQueue;

                                            scheduleEvent.TimeIn = oldTimeIn;
                                            scheduleEvent.TimeOut = oldTimeOut;
                                            scheduleEvent.DisciplineTask = oldDisciplineTask;

                                            scheduleEvent.VisitDate = oldVisitDate;

                                            scheduleEvent.Surcharge = oldSurcharge;
                                            scheduleEvent.AssociatedMileage = oldAssociatedMileage;
                                            scheduleEvent.SendAsOrder = oldSendAsOrder;

                                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                            returnData.isSuccessful = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return returnData;
        }

        public JsonViewData ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId)
        {
            //var result = false;
            var viewData = new JsonViewData { isSuccessful = false };
            bool isNoteUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    viewData.PatientId = patientId;
                    viewData.EpisodeId = episodeId;
                    var isMissedVisit = scheduleEvent.IsMissedVisit;
                    if (isMissedVisit)
                    {
                        // Process Missed Visit Form 
                        var mv = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
                        if (mv != null)
                        {
                            var oldMVStatus = mv.Status;
                            if (button == "Approve")
                            {
                                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                            }
                            else if (button == "Return")
                            {
                                //scheduleEvent.MissedVisitFormReturnReason = reason;
                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
                            }
                            if (patientRepository.UpdateMissedVisit(mv))
                            {
                                viewData.IsDataCentersRefresh = oldMVStatus != mv.Status;
                                viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && (oldMVStatus == (int)ScheduleStatus.NoteMissedVisitPending);
                                viewData.isSuccessful = true;
                                //result = true;
                            }
                        }
                    }
                    else
                    {
                        // Process regular notes
                        var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            var oldStatus = scheduleEvent.Status;
                            var oldPrintQueue = scheduleEvent.InPrintQueue;

                            if (button == "Approve")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                                {
                                    if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        viewData.IsOrdersToBeSentRefresh = true;
                                    }
                                    else if (scheduleEvent.SendAsOrder == "1")
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        viewData.IsOrdersToBeSentRefresh = true;
                                    }
                                    else
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                    }
                                }
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge)
                                {
                                    if (scheduleEvent.SendAsOrder == "1")
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        viewData.IsOrdersToBeSentRefresh = true;
                                    }
                                }
                                scheduleEvent.Status = patientVisitNote.Status;
                                scheduleEvent.InPrintQueue = true;
                                isActionSet = true;

                                patientVisitNote.Modified = DateTime.Now;

                            }
                            else if (button == "Return")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                scheduleEvent.Status = patientVisitNote.Status;
                                isActionSet = true;

                            }
                            else if (button == "Print")
                            {
                                scheduleEvent.InPrintQueue = false;
                                isNoteUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (isNoteUpdates)
                                    {
                                        patientVisitNote.Modified = DateTime.Now;
                                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                                        {
                                            viewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
                                            viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && (oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                                            viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            viewData.isSuccessful = true;
                                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                            {
                                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                            }
                                        }
                                        else
                                        {
                                            scheduleEvent.Status = oldStatus;
                                            scheduleEvent.InPrintQueue = oldPrintQueue;
                                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                            viewData.isSuccessful = false;
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduleEvent != null)
            {
                var oldUserId = scheduleEvent.UserId;
                var oldStatus = scheduleEvent.Status;
                var description = string.Empty;
                //if (oldUserId != Current.UserId)
                //{
                //    description = "( From " + UserEngine.GetName(oldUserId, Current.AgencyId) + " To " + UserEngine.GetName(Current.UserId, Current.AgencyId) + " )";
                //}

                viewData.PatientId = patientId;
                viewData.EpisodeId = episodeId;


                switch (scheduleEvent.DisciplineTask)
                {
                    case (int)DisciplineTasks.NonOASISDischarge:
                    case (int)DisciplineTasks.NonOASISStartofCare:
                    case (int)DisciplineTasks.NonOASISRecertification:

                    case (int)DisciplineTasks.OASISCStartofCare:
                    case (int)DisciplineTasks.OASISCStartofCarePT:
                    case (int)DisciplineTasks.OASISCStartofCareOT:
                    case (int)DisciplineTasks.OASISCResumptionofCare:
                    case (int)DisciplineTasks.OASISCResumptionofCarePT:
                    case (int)DisciplineTasks.OASISCResumptionofCareOT:
                    case (int)DisciplineTasks.OASISCFollowUp:
                    case (int)DisciplineTasks.OASISCFollowupPT:
                    case (int)DisciplineTasks.OASISCFollowupOT:
                    case (int)DisciplineTasks.OASISCRecertification:
                    case (int)DisciplineTasks.OASISCRecertificationPT:
                    case (int)DisciplineTasks.OASISCRecertificationOT:
                    case (int)DisciplineTasks.OASISCDeath:
                    case (int)DisciplineTasks.OASISCDeathOT:
                    case (int)DisciplineTasks.OASISCDeathPT:
                    case (int)DisciplineTasks.OASISCDischarge:
                    case (int)DisciplineTasks.OASISCDischargeOT:
                    case (int)DisciplineTasks.OASISCDischargePT:
                    case (int)DisciplineTasks.OASISCTransfer:
                    case (int)DisciplineTasks.OASISCTransferDischarge:
                    case (int)DisciplineTasks.OASISCTransferOT:
                    case (int)DisciplineTasks.OASISCTransferPT:
                    case (int)DisciplineTasks.SNAssessment:
                    case (int)DisciplineTasks.SNAssessmentRecert:
                        {
                            //scheduleEvent.UserId = Current.UserId;
                            scheduleEvent.Status = ((int)ScheduleStatus.OasisReopened);
                            //if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            //{
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduleEvent.DisciplineTask)).ToString();
                            var data = assessmentService.UpdateAssessmentStatus(eventId, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisReopened, string.Empty);
                            viewData.isSuccessful = data.isSuccessful;

                            if (data.isSuccessful && !((DisciplineTasks)scheduleEvent.DisciplineTask).ToString().IsEqual("NonOASIS"))
                            {
                                viewData.IsExportOASISRefresh = oldStatus == ((int)ScheduleStatus.OasisCompletedExportReady);
                                viewData.IsExportedOASISRefresh = oldStatus == ((int)ScheduleStatus.OasisExported);
                            }
                            //}
                        }
                        break;
                    case (int)DisciplineTasks.SkilledNurseVisit:
                    case (int)DisciplineTasks.Labs:
                    case (int)DisciplineTasks.InitialSummaryOfCare:
                    case (int)DisciplineTasks.SNInsulinAM:
                    case (int)DisciplineTasks.SNInsulinPM:
                    case (int)DisciplineTasks.SNInsulinNoon:
                    case (int)DisciplineTasks.SNInsulinHS:
                    case (int)DisciplineTasks.FoleyCathChange:
                    case (int)DisciplineTasks.SNB12INJ:
                    case (int)DisciplineTasks.SNBMP:
                    case (int)DisciplineTasks.SNCBC:
                    case (int)DisciplineTasks.SNHaldolInj:
                    case (int)DisciplineTasks.PICCMidlinePlacement:
                    case (int)DisciplineTasks.PRNFoleyChange:
                    case (int)DisciplineTasks.PRNSNV:
                    case (int)DisciplineTasks.PRNVPforCMP:
                    case (int)DisciplineTasks.PTWithINR:
                    case (int)DisciplineTasks.PTWithINRPRNSNV:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case (int)DisciplineTasks.SNDC:
                    case (int)DisciplineTasks.SNEvaluation:
                    case (int)DisciplineTasks.SNFoleyLabs:
                    case (int)DisciplineTasks.SNFoleyChange:
                    case (int)DisciplineTasks.SNInjection:
                    case (int)DisciplineTasks.SNInjectionLabs:
                    case (int)DisciplineTasks.SNLabsSN:
                    case (int)DisciplineTasks.SNVPsychNurse:
                    case (int)DisciplineTasks.SNVwithAideSupervision:
                    case (int)DisciplineTasks.SNVDCPlanning:
                    case (int)DisciplineTasks.SNDiabeticDailyVisit:
                    case (int)DisciplineTasks.SNPediatricVisit:
                    case (int)DisciplineTasks.SNPediatricAssessment:
                    case (int)DisciplineTasks.SNPsychAssessment:

                    case (int)DisciplineTasks.SNVTeachingTraining:
                    case (int)DisciplineTasks.SNVManagementAndEvaluation:
                    case (int)DisciplineTasks.SNVObservationAndAssessment:

                    case (int)DisciplineTasks.HomeMakerNote:
                    case (int)DisciplineTasks.HHAideVisit:
                    case (int)DisciplineTasks.HHAideCarePlan:
                    case (int)DisciplineTasks.HHAideSupervisoryVisit:

                    case (int)DisciplineTasks.PASVisit:
                    case (int)DisciplineTasks.PASTravel:
                    case (int)DisciplineTasks.PASCarePlan:

                    case (int)DisciplineTasks.PTAVisit:
                    case (int)DisciplineTasks.PTDischarge:
                    case (int)DisciplineTasks.PTEvaluation:
                    case (int)DisciplineTasks.PTReEvaluation:
                    case (int)DisciplineTasks.PTReassessment:
                    case (int)DisciplineTasks.PTVisit:
                    case (int)DisciplineTasks.PTSupervisoryVisit:

                    case (int)DisciplineTasks.STDischarge:
                    case (int)DisciplineTasks.STEvaluation:
                    case (int)DisciplineTasks.STVisit:
                    case (int)DisciplineTasks.STReassessment:

                    case (int)DisciplineTasks.OTReEvaluation:
                    case (int)DisciplineTasks.OTEvaluation:
                    case (int)DisciplineTasks.OTReassessment:
                    case (int)DisciplineTasks.OTVisit:
                    case (int)DisciplineTasks.COTAVisit:
                    case (int)DisciplineTasks.OTSupervisoryVisit:
                    case (int)DisciplineTasks.OTDischarge:

                    case (int)DisciplineTasks.SixtyDaySummary:
                    case (int)DisciplineTasks.TransferSummary:
                    case (int)DisciplineTasks.CoordinationOfCare:
                    case (int)DisciplineTasks.DischargeSummary:
                    case (int)DisciplineTasks.PTDischargeSummary:
                    case (int)DisciplineTasks.OTDischargeSummary:

                    case (int)DisciplineTasks.LVNSupervisoryVisit:

                    case (int)DisciplineTasks.MSWAssessment:
                    case (int)DisciplineTasks.MSWDischarge:
                    case (int)DisciplineTasks.MSWEvaluationAssessment:
                    case (int)DisciplineTasks.MSWProgressNote:
                    case (int)DisciplineTasks.MSWVisit:

                    case (int)DisciplineTasks.UAPWoundCareVisit:
                    case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened);
                        var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {

                                patientVisitNote.UserId = scheduleEvent.UserId;
                                patientVisitNote.Status = (int)ScheduleStatus.NoteReopened;
                                patientVisitNote.SignatureText = string.Empty;
                                patientVisitNote.SignatureDate = DateTime.MinValue;
                                patientVisitNote.PhysicianSignatureText = string.Empty;
                                patientVisitNote.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
                                    viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.EvalToBeSentToPhysician);
                                    viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.EvalSentToPhysician) || oldStatus == ((int)ScheduleStatus.EvalSentToPhysicianElectronically);

                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }

                        }
                        break;

                    case (int)DisciplineTasks.HCFA485:
                    case (int)DisciplineTasks.NonOasisHCFA485:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                        var planOfCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
                        if (planOfCare != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                planOfCare.UserId = scheduleEvent.UserId;
                                planOfCare.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCare.SignatureText = string.Empty;
                                planOfCare.SignatureDate = DateTime.MinValue;
                                planOfCare.PhysicianSignatureText = string.Empty;
                                planOfCare.PhysicianSignatureDate = DateTime.MinValue;
                                planOfCare.ReceivedDate = DateTime.MinValue;
                                if (planofCareRepository.Update(planOfCare))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsPhysicianOrderPOCRefresh = true;
                                    viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                    viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                    viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);

                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }

                        }
                        break;
                    case (int)DisciplineTasks.HCFA485StandAlone:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                        var planOfCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
                        if (planOfCareStandAlone != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                planOfCareStandAlone.UserId = scheduleEvent.UserId;
                                planOfCareStandAlone.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCareStandAlone.SignatureText = string.Empty;
                                planOfCareStandAlone.SignatureDate = DateTime.MinValue;
                                planOfCareStandAlone.PhysicianSignatureText = string.Empty;
                                planOfCareStandAlone.PhysicianSignatureDate = DateTime.MinValue;
                                planOfCareStandAlone.ReceivedDate = DateTime.MinValue;
                                if (planofCareRepository.UpdateStandAlone(planOfCareStandAlone))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsPhysicianOrderPOCRefresh = true;
                                    viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                    viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                    viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }
                        break;
                    case (int)DisciplineTasks.PhysicianOrder:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                        var physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                physicianOrder.UserId = scheduleEvent.UserId;
                                physicianOrder.Status = (int)ScheduleStatus.OrderReopened;
                                physicianOrder.SignatureText = string.Empty;
                                physicianOrder.SignatureDate = DateTime.MinValue;
                                physicianOrder.PhysicianSignatureText = string.Empty;
                                physicianOrder.PhysicianSignatureDate = DateTime.MinValue;
                                physicianOrder.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateOrderModel(physicianOrder))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsPhysicianOrderPOCRefresh = true;
                                    viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                    viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                    viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }
                        break;
                    case (int)DisciplineTasks.FaceToFaceEncounter:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                        var faceToFaceOrder = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                        if (faceToFaceOrder != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                faceToFaceOrder.UserId = scheduleEvent.UserId;
                                faceToFaceOrder.Status = (int)ScheduleStatus.OrderReopened;
                                faceToFaceOrder.SignatureText = string.Empty;
                                faceToFaceOrder.SignatureDate = DateTime.MinValue;
                                faceToFaceOrder.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceOrder))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsFaceToFaceEncounterRefresh = true;
                                    viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                    viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                    viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }
                        break;
                    case (int)DisciplineTasks.CommunicationNote:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened);
                        var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                        if (communicationNote != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                communicationNote.UserId = scheduleEvent.UserId;
                                communicationNote.Status = (int)ScheduleStatus.NoteReopened;
                                communicationNote.SignatureText = string.Empty;
                                communicationNote.SignatureDate = DateTime.MinValue;
                                communicationNote.Modified = DateTime.Now;

                                if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsCommunicationNoteRefresh = true;
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }

                        break;

                    case (int)DisciplineTasks.IncidentAccidentReport:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen);
                        var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                        if (incident != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                incident.UserId = scheduleEvent.UserId;
                                incident.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                incident.SignatureText = string.Empty;
                                incident.SignatureDate = DateTime.MinValue;
                                incident.Modified = DateTime.Now;
                                if (agencyRepository.UpdateIncidentModal(incident))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsIncidentAccidentRefresh = true;
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }
                        break;

                    case (int)DisciplineTasks.InfectionReport:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen);
                        var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                        if (infection != null)
                        {
                            if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                            {

                                infection.UserId = scheduleEvent.UserId;
                                infection.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                infection.SignatureText = string.Empty;
                                infection.SignatureDate = DateTime.MinValue;
                                infection.Modified = DateTime.Now;
                                if (agencyRepository.UpdateInfectionModal(infection))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.IsInfectionRefresh = true;
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                    viewData.isSuccessful = false;
                                }
                            }
                        }
                        break;
                }
                if (viewData.isSuccessful)
                {
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = Current.UserId == scheduleEvent.UserId && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                }
            }
            return viewData;
        }

        public bool ToggleScheduleStatusNew(Guid episodeId, Guid patientId, Guid eventId, bool isDelete)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.IsDeprecated;
                    scheduleEvent.IsDeprecated = isDelete;
                    if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (UpdateScheduleEntity(eventId, episodeId, patientId, scheduleEvent.DisciplineTask, isDelete))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, episodeId, patientId, eventId, isDelete ? Actions.Deleted : Actions.Restored, (DisciplineTasks)scheduleEvent.DisciplineTask);
                            result = true;
                        }
                        else
                        {
                            scheduleEvent.IsDeprecated = oldStatus;
                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public JsonViewData Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            try
            {
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var isUserDifferent = oldEmployeeId != employeeId;
                    var oldUserId = scheduleEvent.UserId;
                    scheduleEvent.UserId = employeeId;
                    if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (ReassignScheduleEntity(episodeId, patientId, eventId, employeeId, scheduleEvent.DisciplineTask))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, patientId, scheduleEvent.EventId, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(oldEmployeeId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                            viewData.isSuccessful = true;
                            if (isUserDifferent)
                            {
                                viewData.PatientId = patientId;
                                viewData.EpisodeId = episodeId;
                                viewData.IsDataCentersRefresh = true;
                                viewData.IsMyScheduleTaskRefresh = Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                viewData.IsCaseManagementRefresh = (scheduleEvent.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview) || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview) || scheduleEvent.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature) || scheduleEvent.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                                viewData.IsCommunicationNoteRefresh = scheduleEvent.DisciplineTask == (int)DisciplineTasks.CommunicationNote;
                            }
                        }
                        else
                        {
                            scheduleEvent.UserId = oldUserId;
                            scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return viewData;
            }
            return viewData;
        }

        public JsonViewData ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var scheduleEvents = scheduleRepository.GetScheduleByUserIdLean(Current.AgencyId, patientId, employeeOldId, ScheduleStatusFactory.AllNoteNotYetStarted().ToArray(), new string[] { }, new int[] { }, true, startDate, endDate, false);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var eventsDictionary = scheduleEvents.GroupBy(s => s.DisciplineTask).ToDictionary(g => g.Key, g => g.ToList());
                if (eventsDictionary != null && eventsDictionary.Count > 0)
                {
                    var script = string.Empty;
                    eventsDictionary.ForEach((key, value) =>
                    {
                        var table = this.TableName(key);
                        if (table.IsNotNullOrEmpty())
                        {
                            script += string.Format(@"UPDATE {0} set UserId = @userId WHERE AgencyId = @agencyid AND Id IN ( {1});",table, value.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", "));
                        }
                    });
                    if (script.IsNotNullOrEmpty())
                    {
                        var ids = scheduleEvents.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                        if (scheduleRepository.UpdateScheduleEventsForReassign(Current.AgencyId, employeeId, ids))
                        {
                            if (patientRepository.UpdateEntityForReassingUser(Current.AgencyId, employeeId, script))
                            {
                                viewData.PatientId = patientId;
                                viewData.IsDataCentersRefresh = true;
                                viewData.isSuccessful = true;
                                viewData.IsMyScheduleTaskRefresh = (Current.UserId == employeeOldId || Current.UserId == employeeId) && (scheduleEvents.Exists(s => s.EventDate.IsBetween(DateTime.Now.AddDays(-89), DateTime.Now.AddDays(14))));
                                viewData.IsCommunicationNoteRefresh = scheduleEvents.Exists(s => s.DisciplineTask == (int)DisciplineTasks.CommunicationNote);
                                Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Reassigned, ScheduleStatus.NoStatus, string.Format("( From {0} To {1} )", UserEngine.GetName(employeeOldId, Current.AgencyId), UserEngine.GetName(employeeId, Current.AgencyId)));
                            }
                            else
                            {
                                scheduleRepository.UpdateScheduleEventsForReassign(Current.AgencyId, employeeOldId, ids);
                            }
                        }
                    }

                }
            }
            return viewData;
        }

        public bool DeleteSchedules(Guid patientId, Guid episodeId, List<Guid> eventsToBeDeletedIds)
        {
            bool result = false;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty() && eventsToBeDeletedIds != null && eventsToBeDeletedIds.Count > 0)
            {
                var toBeDeletedEvents = scheduleRepository.GetScheduleEventsVeryLeanNonOptionalPar(Current.AgencyId, patientId, episodeId, new int[] { }, new int[] { }, DateTime.MinValue, DateTime.MaxValue, false, true);
                if (toBeDeletedEvents != null && toBeDeletedEvents.Count > 0)
                {
                    var eventsDictionary = toBeDeletedEvents.GroupBy(s => s.DisciplineTask).ToDictionary(g => g.Key, g => g.ToList());
                    if (eventsDictionary != null && eventsDictionary.Count > 0)
                    {
                        var script = string.Empty;
                        eventsDictionary.ForEach((key, value) =>
                        {
                            var table = this.TableName( key);
                            if (table.IsNotNullOrEmpty())
                            {
                                script += string.Format(@"UPDATE {0} set IsDeprecated = 1 , Modified = @modified WHERE AgencyId = @agencyid AND  PatientId = @patientId  AND EpisodeId = @episodeid  AND Id IN ( {1});", table, value.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", "));
                            }
                        });
                        if (script.IsNotNullOrEmpty())
                        {
                             var filteredEventsToBeDeletedIds = toBeDeletedEvents.Select(s => s.EventId).Distinct().ToList();
                             if (filteredEventsToBeDeletedIds != null && filteredEventsToBeDeletedIds.Count > 0)
                             {
                                 if (scheduleRepository.ToggleScheduledEventsDelete(Current.AgencyId, patientId, episodeId, filteredEventsToBeDeletedIds, true))
                                 {
                                     if (patientRepository.UpdateEntityForDelete(Current.AgencyId, patientId, episodeId, script))
                                     {
                                         result = true;
                                         Auditor.MultiLog(Current.AgencyId, toBeDeletedEvents, Actions.Deleted, ScheduleStatus.NoStatus, string.Empty);
                                     }
                                     else
                                     {
                                         scheduleRepository.ToggleScheduledEventsDelete(Current.AgencyId, patientId, episodeId, filteredEventsToBeDeletedIds, false);
                                     }
                                 }
                             }

                        }

                        //if (toBeDeletedEvents != null && toBeDeletedEvents.Count > 0)
                        //{
                        //    var filteredEventsToBeDeletedIds = toBeDeletedEvents.Select(s => s.EventId).Distinct().ToList();
                        //    if (filteredEventsToBeDeletedIds != null && filteredEventsToBeDeletedIds.Count > 0)
                        //    {
                        //        if (scheduleRepository.ToggleScheduledEvents(Current.AgencyId, patientId, episodeId, filteredEventsToBeDeletedIds, true))
                        //        {
                        //            result = true;
                        //            foreach (var evnt in toBeDeletedEvents)
                        //            {
                        //                UpdateScheduleEntity(evnt.EventId, episodeId, patientId, evnt.DisciplineTask, true);
                        //                Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Deleted, (DisciplineTasks)evnt.DisciplineTask);
                        //            }
                        //        }

                        //    }
                        //}
                    }
                }
            }

            return result;
        }

        public bool DeleteScheduleEventAsset(Guid episodeId, Guid patientId, Guid eventId, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !assetId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                {
                    var assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                    if (assets != null && assets.Count > 0 && assets.Exists(id => id == assetId))
                    {
                        assets.Remove(assetId);
                        scheduleEvent.Asset = assets.ToXml();
                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            if (scheduleEvent.IsStartofCareAssessment() || scheduleEvent.IsRecertificationAssessment() || scheduleEvent.IsResumptionofCareAssessment() || (scheduleEvent.IsOASISTransferDischarge() && scheduleEvent.Version == 2))
                            {
                                if (assessmentService.DeleteOnlyWoundCareAsset(episodeId, patientId, eventId, scheduleEvent.GetAssessmentType().ToString(), assetId))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                                    result = true;
                                    assetRepository.Delete(assetId);
                                }
                                else
                                {
                                    result = false;
                                    assets.Add(assetId);
                                    scheduleEvent.Asset = assets.ToXml();
                                    scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                }
                            }
                            else
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                                result = true;
                                assetRepository.Delete(assetId);
                            }

                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        #endregion
     
        #region Schedule Center and Master Calendar 

        public CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, DateTime date, string discipline, bool IsAfterEpisode, bool IsBeforeEpisode, bool IsScheduleEventsActionIncluded)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            var calendarData = new CalendarViewData();
            calendarData.PatientId = patientId;
            calendarData.Discpline = discipline;
            var episode = patientRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId, date);
            if (episode != null && !episode.Id.IsEmpty())
            {
                calendarData.StartDate = episode.StartDate;
                calendarData.EndDate = episode.EndDate;
                calendarData.EpisodeId = episode.Id;
                calendarData.IsEpisodeExist = true;
                calendarData.EpisodeAdmissionId = episode.AdmissionId;

                var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { episode.Id }, episode.StartDate, episode.EndDate, discipline, true, false);// FilterForScheduleCenter(episode, discipline, true);
                if (events != null && events.Count > 0)
                {
                    if (IsScheduleEventsActionIncluded)
                    {
                        this.ProcessScheduleEventsActions(patientId, events, episode, discipline);
                    }
                    else
                    {
                        PrecessScheduleEventForUsersOnly(events);
                    }
                    calendarData.ScheduleEvents = events;//.OrderByDescending(s => s.EventDateSortable).ToList();
                }


                if (episode.Details.IsNotNullOrEmpty())
                {
                    episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                }
                if (IsAfterEpisode)
                {
                    if (episode.EndDate.IsValid())
                    {
                        var nextEpisode = patientRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, episode.EndDate);
                        if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                        {
                            calendarData.NextEpisode = nextEpisode.Id;
                        }
                    }
                }
                if (IsBeforeEpisode)
                {
                    if (episode.StartDate.IsValid())
                    {
                        var previousEpisode = patientRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, episode.StartDate);
                        if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                        {
                            calendarData.PreviousEpisode = previousEpisode.Id;
                        }
                    }
                }
            }
            else
            {
                var futureEpisode = patientRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, date); //allEpisodes.Where(e => e.IsActive).LastOrDefault();
                if (futureEpisode != null)
                {

                    var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { futureEpisode.Id }, futureEpisode.StartDate, futureEpisode.EndDate, discipline, true, false); //FilterForScheduleCenter(futureEpisode, discipline, true);
                    if (events != null && events.Count > 0)
                    {
                        if (IsScheduleEventsActionIncluded)
                        {
                            this.ProcessScheduleEventsActions(patientId, events, futureEpisode, discipline);
                        }
                        else
                        {
                            PrecessScheduleEventForUsersOnly(events);
                        }
                        calendarData.ScheduleEvents = events.OrderByDescending(s => s.EventDateSortable).ToList();
                    }

                    if (futureEpisode.Details.IsNotNullOrEmpty())
                    {
                        futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
                    }
                    if (IsAfterEpisode)
                    {
                        if (futureEpisode.EndDate.IsValid())
                        {
                            var nextEpisode = patientRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, episode.EndDate);
                            if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                            {
                                calendarData.NextEpisode = nextEpisode.Id;
                            }
                        }
                    }
                    calendarData.PreviousEpisode = Guid.Empty;
                    calendarData.IsEpisodeExist = true;
                    calendarData.StartDate = futureEpisode.StartDate;
                    calendarData.EndDate = futureEpisode.EndDate;
                    calendarData.EpisodeId = futureEpisode.Id;
                    calendarData.EpisodeAdmissionId = futureEpisode.AdmissionId;
                }
            }
            return calendarData;
        }

        public CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, Guid episodeId, string discipline, bool IsAfterEpisode, bool IsBeforeEpisode, bool IsScheduleEventsActionIncluded, bool IsFrequencyIncluded)
        {
            var calendarData = new CalendarViewData();
            calendarData.PatientId = patientId;
            calendarData.Discpline = discipline;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null && !episode.Id.IsEmpty())
            {
                calendarData.StartDate = episode.StartDate;
                calendarData.EndDate = episode.EndDate;
                calendarData.EpisodeId = episode.Id;
                calendarData.IsEpisodeExist = true;
                calendarData.EpisodeAdmissionId = episode.AdmissionId;
                //if (episode.Schedule.IsNotNullOrEmpty())
                //{
                var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { episode.Id }, episode.StartDate, episode.EndDate, discipline, true, false);  //FilterForScheduleCenter(episode, discipline, true);
                if (events != null && events.Count > 0)
                {
                    if (IsScheduleEventsActionIncluded)
                    {
                        this.ProcessScheduleEventsActions(patientId, events, episode, discipline);
                    }
                    else
                    {
                        PrecessScheduleEventForUsersOnly(events);
                    }
                    calendarData.ScheduleEvents = events.OrderByDescending(s => s.EventDateSortable).ToList();
                }
                //}
                if (episode.Details.IsNotNullOrEmpty())
                {
                    episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                }
                if (IsAfterEpisode)
                {
                    if (episode.EndDate.IsValid())
                    {
                        var nextEpisode = patientRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, episode.EndDate);
                        if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                        {
                            calendarData.NextEpisode = nextEpisode.Id;
                        }
                    }
                }
                PatientEpisode previousEpisode = null;
                if (IsBeforeEpisode)
                {
                    if (episode.StartDate.IsValid())
                    {
                        previousEpisode = patientRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, episode.StartDate);
                        if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                        {
                            calendarData.PreviousEpisode = previousEpisode.Id;
                        }
                    }
                }
                if (IsFrequencyIncluded)
                {
                    calendarData.FrequencyList = this.GetFrequencyForCalendar(episode, previousEpisode);
                }
            }
            return calendarData;
        }

        #endregion

        #region Patient Center Schedule Activities

        public PatientScheduleEventViewData GetPatientScheduleEventViewData(Guid patientId, string discipline, DateRange range)
        {
            var patientWithSchedule = new PatientScheduleEventViewData();
            patientWithSchedule.DisciplineFilterType = discipline;
            patientWithSchedule.DateFilterType = range.Id;
            //var patientEvents = new List<ScheduleEvent>();
            if (range.Id.IsEqual("ThisEpisode"))
            {
                var patientEpisode = patientRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                if (patientEpisode != null)
                {
                    patientWithSchedule.FilterStartDate = patientEpisode.StartDate;
                    patientWithSchedule.FilterEndDate = patientEpisode.EndDate;
                    var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, false);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        this.ProcessScheduleEventsActions(patientId, patientEvents, patientEpisode, discipline);
                        patientWithSchedule.ScheduleEvents = patientEvents;
                        // patientEvents.AddRange(patientEvents);
                    }
                    //if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    //{
                    //    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                    //    if (schedule != null && schedule.Count > 0)
                    //    {
                    //        FilterForPatientCenter(schedule, discipline);
                    //        ProcessScheduleEventsActions(patientId, schedule, patientEpisode, discipline);
                    //        patientEvents.AddRange(schedule);
                    //    }
                    //}
                }
            }
            else if (range.Id.IsEqual("NextEpisode"))
            {
                var patientEpisode = patientRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, DateTime.Now);
                if (patientEpisode != null)
                {
                    patientWithSchedule.FilterStartDate = patientEpisode.StartDate;
                    patientWithSchedule.FilterEndDate = patientEpisode.EndDate;
                    var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, false);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        this.ProcessScheduleEventsActions(patientId, patientEvents, patientEpisode, discipline);
                        patientWithSchedule.ScheduleEvents = patientEvents;
                        // patientEvents.AddRange(patientEvents);
                    }
                    //if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    //{
                    //    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                    //    if (schedule != null && schedule.Count > 0)
                    //    {
                    //        FilterForPatientCenter(schedule, discipline);
                    //        ProcessScheduleEventsActions(patientId, schedule, patientEpisode, discipline);
                    //        patientEvents.AddRange(schedule);
                    //    }
                    //}
                }
            }
            else if (range.Id.IsEqual("LastEpisode"))
            {
                var patientEpisode = patientRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, DateTime.Now);
                if (patientEpisode != null)
                {
                    patientWithSchedule.FilterStartDate = patientEpisode.StartDate;
                    patientWithSchedule.FilterEndDate = patientEpisode.EndDate;
                    var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, false);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        this.ProcessScheduleEventsActions(patientId, patientEvents, patientEpisode, discipline);
                        patientWithSchedule.ScheduleEvents = patientEvents;
                        //patientEvents.AddRange(schedule);
                    }
                    //if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    //{
                    //    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                    //    if (schedule != null && schedule.Count > 0)
                    //    {
                    //        FilterForPatientCenter(schedule, discipline);
                    //        ProcessScheduleEventsActions(patientId, schedule, patientEpisode, discipline);
                    //        patientEvents.AddRange(schedule);
                    //    }
                    //}
                }
            }
            else if (range.Id.IsEqual("all"))
            {
                var patientEpisodes = patientRepository.GetPatientActiveEpisodesLean(Current.AgencyId, patientId);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, patientEpisodes.Select(e => e.Id).Distinct().ToArray(), range.StartDate, range.EndDate, discipline, false, false);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        this.ProcessScheduleEventsActions(patientEpisodes, patientEvents, discipline);
                        patientWithSchedule.ScheduleEvents = patientEvents;
                        //patientEvents.AddRange(schedule);
                    }
                    //patientEvents = ProcessScheduleEventsActions(patientEpisodes, discipline);
                }

                //patientEpisodes.ForEach(patientEpisode =>
                //{
                //    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                //    {
                //        var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                //        if (schedule != null && schedule.Count > 0)
                //        {
                //            FilterForPatientCenter(schedule, discipline);
                //            ProcessScheduleEventsActions(patientId, schedule, patientEpisode, discipline);
                //            patientEvents.AddRange(schedule);

                //        }
                //    }
                //});
            }
            else
            {
                if ((range.StartDate.Date.IsValid() && range.EndDate.Date.IsValid()))
                {
                    patientWithSchedule.FilterStartDate = range.StartDate;
                    patientWithSchedule.FilterEndDate = range.EndDate;
                    var patientEpisodes = patientRepository.GetPatientActiveEpisodesLeanByDateRange(Current.AgencyId, patientId, range.StartDate, range.EndDate);
                    if (patientEpisodes != null && patientEpisodes.Count > 0)
                    {
                        var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, patientEpisodes.Select(e => e.Id).Distinct().ToArray(), range.StartDate, range.EndDate, discipline, false, true);
                        if (patientEvents != null && patientEvents.Count > 0)
                        {
                            this.ProcessScheduleEventsActions(patientEpisodes, patientEvents, discipline);
                            patientWithSchedule.ScheduleEvents = patientEvents;
                            //patientEvents.AddRange(schedule);
                        }
                        //patientEvents = ProcessScheduleEventsActions(patientEpisodes, discipline);
                        //patientEpisodes.ForEach(patientEpisode =>
                        //{
                        //    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= range.StartDate.Date && s.EventDate.ToDateTime().Date <= range.EndDate.Date && s.IsDeprecated != true).ToList();
                        //    if (schedule != null && schedule.Count > 0)
                        //    {
                        //        FilterForPatientCenter(schedule, discipline);
                        //        ProcessScheduleEventsActions(patientId, schedule, patientEpisode, discipline);
                        //        patientEvents.AddRange(schedule);
                        //    }
                        //});
                    }
                }
            }
            //patientWithSchedule.ScheduleEvents = patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
            return patientWithSchedule;
        }

        public PatientScheduleEventViewData CurrentEpisodePatientWithScheduleEvent(Patient patient, string discipline)
        {
            var patientScheduleEventViewData = new PatientScheduleEventViewData();
            patientScheduleEventViewData.DateFilterType = "ThisEpisode";
            if (patient != null)
            {
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                if (physician != null)
                {
                    patient.Physician = physician;
                }
                var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patient.Id);
                if (emergencyContact != null)
                {
                    patient.EmergencyContact = emergencyContact;
                }

                this.SetInsurance(patient);
                patientScheduleEventViewData.Patient = patient;
                patientScheduleEventViewData.DisciplineFilterType = discipline;
                var patientEpisode = patientRepository.GetCurrentEpisodeLeanNoPatientInfo(Current.AgencyId, patient.Id);
                if (patientEpisode != null)
                {
                    patientScheduleEventViewData.FilterStartDate = patientEpisode.StartDate;
                    patientScheduleEventViewData.FilterEndDate = patientEpisode.EndDate;
                    var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patient.Id, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, true);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        this.ProcessScheduleEventsActions(patient.Id, patientEvents, patientEpisode, discipline);
                        patientScheduleEventViewData.ScheduleEvents = patientEvents;// this.GetScheduledEventsForCurrentEpisode(patientEpisode, discipline);
                    }

                }
            }
            return patientScheduleEventViewData;
        }

        #endregion

        #region Add Schedule Events 

        public bool AddSchedules(PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (episode != null)
            {
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(ev =>
                    {
                        var date = ev.EventDate;
                        ev.EventId = Guid.NewGuid();
                        ev.PatientId = episode.PatientId;
                        ev.EpisodeId = episode.Id;
                        ev.AgencyId = episode.AgencyId;
                        ev.EventDate = date;
                        ev.VisitDate = date;
                        ev.StartDate = episode.StartDate;
                        ev.EndDate = episode.EndDate;
                        ev.IsDeprecated = false;
                    });
                    result = this.UpdateEpisodeNew(episode, scheduleEvents);
                }
            }
            return result;
        }

        public bool AddMultiDateRangeScheduleNew(PatientEpisode episode, int disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            if (episode != null)
            {
                var newEvents = new List<ScheduleEvent>();
                int dateDifference = endDate.Subtract(startDate).Days + 1;
                for (int i = 0; i < dateDifference; i++)
                {
                    newEvents.Add(
                        new ScheduleEvent
                        {
                            EventId = Guid.NewGuid(),
                            PatientId = episode.PatientId,
                            EpisodeId = episode.Id,
                            AgencyId = episode.AgencyId,
                            UserId = userId,
                            DisciplineTask = disciplineTask,
                            Discipline = discipline,
                            EventDate = startDate.AddDays(i),
                            VisitDate = startDate.AddDays(i),
                            StartDate = episode.StartDate,
                            EndDate = episode.EndDate,
                            IsBillable = isBillable,
                            IsDeprecated = false
                        });
                }
                if (newEvents != null && newEvents.Count > 0)
                {
                    result = this.UpdateEpisodeNew(episode, newEvents);
                }
            }
            return result;
        }

        public bool AddMultiDayScheduleNew(PatientEpisode episode, Guid userId, int disciplineTaskId, string visitDates)
        {
            bool result = false;
            if (episode != null)
            {
                var scheduledEvents = new List<ScheduleEvent>();
                var visitDateArray = visitDates.Split(',');
                if (visitDateArray != null && visitDateArray.Length > 0)
                {
                    visitDateArray.ForEach(date =>
                    {
                        if (date.IsDate())
                        {
                            var newScheduledEvent = new ScheduleEvent
                            {
                                EventId = Guid.NewGuid(),
                                PatientId = episode.PatientId,
                                EpisodeId = episode.Id,
                                AgencyId = episode.AgencyId,
                                UserId = userId,
                                DisciplineTask = disciplineTaskId,
                                EventDate = date.ToDateTime(),
                                VisitDate = date.ToDateTime(),
                                StartDate = episode.StartDate,
                                EndDate = episode.EndDate,
                                IsDeprecated = false
                            };
                            scheduledEvents.Add(newScheduledEvent);
                        }
                    });
                }
                if (scheduledEvents != null && scheduledEvents.Count > 0)
                {
                    result = this.UpdateEpisodeNew(episode, scheduledEvents);
                }
            }
            return result;
        }

        #endregion 

        #region  Schedule Events

        public List<ScheduleEvent> GetScheduledEventsByStatus(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int status)
        {
            var patientEvents = scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, clinicianId, new int[] { status }, new string[] { }, new int[] { }, true, startDate, endDate, true, false, -1);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        v.UserName = user.DisplayName;
                    }
                });
            }
            return patientEvents;

            //var patientEvents = new List<ScheduleEvent>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranchAndPatient(Current.AgencyId, branchId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    var users = agencyRepository.GetUserNames(Current.AgencyId);

            //    if (status == (int)ScheduleStatus.NoteMissedVisit || status == (int)ScheduleStatus.NoteMissedVisitComplete || status == (int)ScheduleStatus.NoteMissedVisitPending || status == (int)ScheduleStatus.NoteMissedVisitReturn)
            //    {
            //        var missedVisitEvents = patientEpisodes.ToDictionary(g => g.Id, g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.IsDeprecated
            //                && s.EventDate.IsValidDate()
            //                && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
            //                && (clinicianId.IsEmpty() || clinicianId == s.UserId)
            //                && s.IsMissedVisit
            //            ).ToList() : new List<ScheduleEvent>());

            //        if (missedVisitEvents != null && missedVisitEvents.Count > 0)
            //        {
            //            var missedVisitIds = missedVisitEvents.SelectMany(s => s.Value).Select(s => s.EventId).ToList();
            //            var missedVisits = patientRepository.GetMissedVisitsByIdsAndStatus(Current.AgencyId, status, missedVisitIds);

            //            foreach (var patientEpisode in patientEpisodes)
            //            {
            //                var scheduleEvents = missedVisitEvents[patientEpisode.Id];
            //                foreach (var missedVisit in missedVisits)
            //                {
            //                    var scheduledEvent = scheduleEvents.FirstOrDefault(s => s.EventId == missedVisit.Id);
            //                    if (scheduledEvent != null)
            //                    {
            //                        scheduledEvent.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                        scheduledEvent.PatientName = patientEpisode.PatientName;
            //                        scheduledEvent.EventDate = scheduledEvent.EventDate.ToZeroFilled();
            //                        scheduledEvent.VisitDate = scheduledEvent.VisitDate.ToZeroFilled();
            //                        var user = users.FirstOrDefault(u => u.UserId == scheduledEvent.UserId);
            //                        scheduledEvent.UserName = user != null ? user.DisplayName : string.Empty;
            //                        patientEvents.Add(scheduledEvent);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        patientEpisodes.ForEach(patientEpisode =>
            //        {
            //            if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
            //            {
            //                var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().ToList();
            //                if (schedule != null && schedule.Count > 0)
            //                {
            //                    foreach (var s in schedule)
            //                    {
            //                        if (!s.IsDeprecated
            //                            && s.EventDate.IsValidDate()
            //                            && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
            //                            && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
            //                            && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
            //                            && s.Status == status.ToString()
            //                            && (clinicianId.IsEmpty() || clinicianId == s.UserId))
            //                        {
            //                            s.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                            s.PatientName = patientEpisode.PatientName;
            //                            s.EventDate = s.EventDate.ToZeroFilled();
            //                            s.VisitDate = s.VisitDate.ToZeroFilled();
            //                            var user = users.FirstOrDefault(u => u.UserId == s.UserId);
            //                            s.UserName = user != null ? user.DisplayName : string.Empty;
            //                            patientEvents.Add(s);
            //                        }
            //                    }
            //                }

            //            }
            //        });
            //    }
            //}
            //return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<ScheduleEvent> GetScheduledEventsByType(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int type)
        {
            var patientEvents = scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, clinicianId, new int[] { }, new string[] { }, new int[] { type }, true, startDate, endDate, true, false, -1);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        v.UserName = user.DisplayName;
                    }
                });
            }
            return patientEvents;

            //var patientEvents = new List<ScheduleEvent>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranchAndPatient(Current.AgencyId, branchId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    var users = agencyRepository.GetUserNames(Current.AgencyId);
            //    patientEpisodes.ForEach(patientEpisode =>
            //    {
            //        if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true).ToList();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                schedule.ForEach(s =>
            //                {
            //                    if (s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
            //                    {
            //                        if (s.DisciplineTask == type && (clinicianId.IsEmpty() || clinicianId == s.UserId))
            //                        {
            //                            s.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                            s.EventDate = s.EventDate.ToZeroFilled();
            //                            s.VisitDate = s.VisitDate.ToZeroFilled();
            //                            s.StartDate = patientEpisode.StartDate.ToDateTime();
            //                            s.EndDate = patientEpisode.EndDate.ToDateTime();
            //                            s.PatientName = patientEpisode.PatientName;
            //                            var user = users.FirstOrDefault(u => u.UserId == s.UserId);
            //                            s.UserName = user != null ? user.DisplayName : string.Empty;
            //                            patientEvents.Add(s);
            //                        }
            //                    }
            //                });
            //            }
            //        }
            //    });
            //}
            //return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = scheduleRepository.GetPatientScheduledEventsOnlyLeanNew(Current.AgencyId, episodeId, patientId, new int[] { }, discipline, new int[] { }, true);
            //var patientEpisode = patientRepository.GetPatientEpisodeLean(Current.AgencyId, episodeId, patientId);
            //if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            //{
            //    patientEvents = FilterForScheduleCenter(patientEpisode, discipline, false);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        v.UserName = user.DisplayName;
                    }
                    if (v.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        v.UserName = "Axxess";
                    }
                });
                //ProcessScheduleEventsActions(patientId, patientEvents, patientEpisode, discipline);
            }
            //}
            return patientEvents;//.OrderByDescending(e => e.EventDateSortable).ToList();
        }

        public List<ScheduleEvent> GetDeletedTasks(Guid patientId)
        {
            var scheduledEvents = scheduleRepository.GetDeletedItemsNew(Current.AgencyId, patientId);
            if (scheduledEvents != null && scheduledEvents.Count > 0)
            {
                var userIds = scheduledEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                scheduledEvents.ForEach(s =>
                {
                    if (!s.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            s.UserName = user.DisplayName;
                        }
                    }
                    if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        s.UserName = "Axxess";
                    }
                });
            }
            //var deletedItems = patientRepository.GetDeletedItems(Current.AgencyId, patientId);
            //if (deletedItems != null)
            //{
            //    deletedItems.ForEach(item =>
            //    {
            //        if (item != null && item.Schedule.IsNotNullOrEmpty())
            //        {
            //            var deletedEvents = item.Schedule.ToObject<List<ScheduleEvent>>();
            //            if (deletedEvents != null)
            //            {
            //                deletedEvents.ForEach(e =>
            //                {
            //                    e.EventDate = e.EventDate.ToZeroFilled();
            //                    e.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
            //                    scheduledEvents.Add(e);
            //                });
            //            }
            //        }
            //    });
            //}
            return scheduledEvents;
        }

        public List<ScheduleEvent> GetPreviousNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var schedules = new List<ScheduleEvent>();
            if (DisciplineTaskFactory.SkilledNurseSharedFile().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.SkilledNurseSharedFile().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.PTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.PTEvalDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTEvalDisciplineTasks().ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.PTDischargeDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTDischargeDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.OTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.OTNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.OTEvalDisciplineTasks(true).Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.OTEvalDisciplineTasks(true).ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.STNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.STNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.STEvalDisciplineTasks(true).Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.STEvalDisciplineTasks(true).ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.PT.ToString() || scheduledEvent.Discipline == Disciplines.OT.ToString() || scheduledEvent.Discipline == Disciplines.ST.ToString())
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, new string[] { scheduledEvent.Discipline }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.HHA.ToString())
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, new string[] { Disciplines.HHA.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.Nursing.ToString())
            {
                schedules = scheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, new string[] { Disciplines.Nursing.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }

            return schedules ?? new List<ScheduleEvent>();
        }

        #endregion

        #region Wound Care

        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                var patientVisitNote = new PatientVisitNote();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldAsset = scheduleEvent.Asset;
                        var scheduleAssets = new List<Guid>();
                        patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                            var assetsTobeadded = new List<Asset>();
                            if (patientVisitNote.Questions != null)
                            {
                                if (httpFiles.Count > 0)
                                {
                                    if (scheduleEvent.Asset.IsNotNullOrEmpty())
                                    {
                                        scheduleAssets = scheduleEvent.Asset.ToObject<List<Guid>>();
                                    }
                                    //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                    //var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                    //var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);

                                    foreach (string key in httpFiles.AllKeys)
                                    {
                                        var keyArray = key.Split('_');
                                        HttpPostedFileBase file = httpFiles.Get(key);
                                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                        {
                                            BinaryReader binaryReader = new BinaryReader(file.InputStream);
                                            var asset = new Asset
                                             {
                                                 Id = Guid.NewGuid(),
                                                 FileName = file.FileName,
                                                 AgencyId = Current.AgencyId,
                                                 ContentType = file.ContentType,
                                                 FileSize = file.ContentLength.ToString(),
                                                 Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length)),
                                                 Created = DateTime.Now,
                                                 Modified = DateTime.Now

                                             };
                                            assetsTobeadded.Add(asset);

                                            //if (assetRepository.Add(asset))
                                            //{
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                            }
                                            if (!scheduleAssets.Contains(asset.Id))
                                            {
                                                scheduleAssets.Add(asset.Id);
                                            }
                                            //if (!scheduleEvent.Assets.Contains(asset.Id))
                                            //{


                                            //    scheduleEvent.Assets.Add(asset.Id);
                                            //    scheduleEvents.RemoveAll(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                                            //    scheduleEvents.Add(scheduleEvent);
                                            //    episode.Schedule = scheduleEvents.ToXml();
                                            //    patientRepository.UpdateEpisode(episode);
                                            //}
                                            //}
                                            //else
                                            //{
                                            //    if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            //    {
                                            //        patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                            //    }
                                            //    else
                                            //    {
                                            //        patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                            //    }
                                            //    assetsSaved = false;
                                            //    break;
                                            //}
                                        }
                                        //else
                                        //{
                                        //    if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                        //    {
                                        //        patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                        //    }
                                        //    else
                                        //    {
                                        //        patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                        //    }
                                        //}
                                    }
                                }
                               
                                if (assetsTobeadded != null && assetsTobeadded.Count > 0)
                                {
                                    patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                                    patientVisitNote.IsWoundCare = true;
                                    scheduleEvent.Asset = scheduleAssets.ToXml();
                                    if (assetRepository.AddMany(assetsTobeadded))
                                    {
                                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                        {
                                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                                            {
                                                result = true;
                                                Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care is added/updated.");
                                            }
                                            else
                                            {
                                                scheduleEvent.Asset = oldAsset;
                                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                                                assetRepository.DeleteMany(Current.AgencyId, assetsTobeadded.Select(a => a.Id).ToList());
                                                result = false;
                                            }
                                        }
                                        else
                                        {
                                            assetRepository.DeleteMany(Current.AgencyId, assetsTobeadded.Select(a => a.Id).ToList());
                                            result = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                                {
                                    scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                                    if (scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0 && scheduleEvent.Assets.Contains(assetId))
                                    {
                                        scheduleEvent.Assets.Remove(assetId);
                                        scheduleEvent.Asset = scheduleEvent.Assets.ToXml();
                                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                                        {
                                            result = true;
                                        }
                                        else
                                        {
                                            result = false;
                                        }
                                    }
                                    else
                                    {
                                        result = true;
                                    }

                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care Asset is deleted.");
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        #endregion

        #region Note Supply

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                var supplies = new List<Supply>();
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                }
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies != null && supplies.Count > 0)
                    {
                        var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (existingSupply != null)
                        {
                            existingSupply.Date = supply.Date;
                            existingSupply.Quantity = supply.Quantity;
                            existingSupply.Description = supply.Description;
                        }
                        else
                        {
                            supplies.Add(supply);
                        }
                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                patientVisitNote.Supply = supplies.ToXml();
                patientVisitNote.IsSupplyExist = true;
                if (patientRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }

        #endregion

        #region Eligibility

        public PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                var jsonData = new
                {
                    input_medicare_number = medicareNumber,
                    input_last_name = lastName,
                    input_first_name = firstName.Substring(0, 1),
                    input_date_of_birth = dob.ToString("MM/dd/yyyy"),
                    input_gender_id = gender.Substring(0, 1)
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(AppSettings.PatientEligibilityUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                var jsonResult = string.Empty;
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }

                    if (jsonResult.IsNotNullOrEmpty())
                    {
                        patientEligibility = javaScriptSerializer.Deserialize<PatientEligibility>(jsonResult);
                        if (patientEligibility != null && patientEligibility.Episode != null && patientEligibility.Episode.reference_id.IsNotNullOrEmpty())
                        {
                            var npiData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(patientEligibility.Episode.reference_id.Trim());
                            if (npiData != null)
                            {
                                patientEligibility.Other_Agency_Data = new OtherAgencyData()
                                {
                                    name = npiData.ProviderOrganizationName,
                                    address1 = npiData.ProviderFirstLineBusinessMailingAddress,
                                    address2 = npiData.ProviderSecondLineBusinessMailingAddress,
                                    city = npiData.ProviderBusinessMailingAddressCityName,
                                    state = npiData.ProviderBusinessMailingAddressStateName,
                                    zip = npiData.ProviderBusinessMailingAddressPostalCode,
                                    phone = npiData.ProviderBusinessMailingAddressTelephoneNumber,
                                    fax = npiData.ProviderBusinessMailingAddressFaxNumber
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return patientEligibility;
        }

        public List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId)
        {
            var eligibilities = patientRepository.GetMedicareEligibilities(Current.AgencyId, patientId);
            if (eligibilities != null)
            {
                eligibilities.ForEach(e =>
                {
                    if (!e.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, e.EpisodeId, e.PatientId);
                        if (episode != null)
                        {
                            e.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                        else
                        {
                            e.EpisodeRange = "Not in an episode";
                        }
                    }
                    e.AssignedTo = "Axxess";
                    e.TaskName = "Medicare Eligibility Report";
                    e.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + e.PatientId + "', 'mcareEligibilityId': '" + e.Id + "' });\"><span class='img icon print'></span></a>";
                });
            }
            return eligibilities.OrderBy(e => e.Created.ToShortDateString().ToZeroFilled()).ToList();
        }

        #endregion

        #region Vital Signs

        public List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSign>();
            var noteDisciplineTasks = new List<int>();
            var disciplineTasksSNVList = DisciplineTaskFactory.ALLSkilledNurseDisciplineTasks(false);
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments(true);
            var disciplineTasksUAP = DisciplineTaskFactory.AllUAP();
            var disciplineTasksTherapy = DisciplineTaskFactory.AllTherapy(false);
            noteDisciplineTasks.AddRange(disciplineTasksSNVList);
            noteDisciplineTasks.AddRange(disciplineTasksOASISList);
            noteDisciplineTasks.AddRange(disciplineTasksUAP);
            noteDisciplineTasks.AddRange(disciplineTasksTherapy);
            noteDisciplineTasks.Add((int)DisciplineTasks.HHAideVisit);
            var scheduleEvents = scheduleRepository.GetScheduledEventsForOASISAndNotesByPatient(Current.AgencyId, patientId, startDate, endDate, new int[] { }, noteDisciplineTasks.ToArray(), new int[] { }, disciplineTasksOASISList.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = VitalSignFromList(scheduleEvents, disciplineTasksSNVList, disciplineTasksOASISList, disciplineTasksUAP, disciplineTasksTherapy);
            }
            return vitalSigns;

            //var vitalSigns = new List<VitalSign>();
            //var scheduleEvents = this.GetScheduledEventsWithUsers(patientId, startDate, endDate);
            //if (scheduleEvents != null && scheduleEvents.Count > 0)
            //{
            //    scheduleEvents.ForEach(s =>
            //    {
            //        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
            //        {
            //            if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
            //            {
            //                var vitalSign = new VitalSign();
            //                vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
            //                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
            //                {
            //                    var questions = assessment.ToDictionary();
            //                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                    if (questions != null)
            //                    {
            //                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


            //                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
            //                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

            //                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
            //                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

            //                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
            //                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

            //                        vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
            //                        vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
            //                        vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
            //                        var bs = new List<double>(); // var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
            //                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSAM > 0)
            //                        {
            //                            bs.Add(BSAM);
            //                        }
            //                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSNoon > 0)
            //                        {
            //                            bs.Add(BSNoon);
            //                        }
            //                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSPM > 0)
            //                        {
            //                            bs.Add(BSPM);
            //                        }
            //                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSHS > 0)
            //                        {
            //                            bs.Add(BSHS);
            //                        }
            //                        var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
            //                        var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
            //                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
            //                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

            //                        vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
            //                        vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
            //                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
            //                    }
            //                    var user = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    if (user.IsNotNullOrEmpty())
            //                    {
            //                        vitalSign.UserDisplayName = user;
            //                    }
            //                    vitalSigns.Add(vitalSign);
            //                }
            //            }
            //            else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.HHAideVisit ||
            //                ((DisciplineTasks)s.DisciplineTask).GetFormGroup() == "UAP")
            //            {
            //                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                {
            //                    var vitalSign = new VitalSign();
            //                    vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                    var questions = visitNote.ToDictionary();
            //                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                    if (questions != null)
            //                    {
            //                        vitalSign.BPLying = "";
            //                        vitalSign.BPSitting = questions.ContainsKey("VitalSignBPVal") ? questions["VitalSignBPVal"].Answer : string.Empty;
            //                        vitalSign.BPStanding = "";
            //                        vitalSign.ApicalPulse = "";
            //                        vitalSign.RadialPulse = "";
            //                        vitalSign.PainLevel = "";
            //                        vitalSign.BSMax = "";
            //                        vitalSign.BSMin = "";
            //                        vitalSign.Weight = questions.ContainsKey("VitalSignWeightVal") ? questions["VitalSignWeightVal"].Answer : string.Empty;
            //                        vitalSign.Temp = questions.ContainsKey("VitalSignTempVal") ? questions["VitalSignTempVal"].Answer : string.Empty;
            //                        vitalSign.Resp = questions.ContainsKey("VitalSignRespVal") ? questions["VitalSignRespVal"].Answer : string.Empty;
            //                    }
            //                    var user = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    if (user.IsNotNullOrEmpty())
            //                    {
            //                        vitalSign.UserDisplayName = user;
            //                    }
            //                    vitalSigns.Add(vitalSign);
            //                }
            //            }
            //            else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTVisit
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTAVisit

            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STVisit
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STEvaluation

            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTVisit)
            //            {
            //                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                {
            //                    var vitalSign = new VitalSign();
            //                    vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                    var questions = visitNote.ToDictionary();
            //                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                    if (questions != null)
            //                    {
            //                        string SBP = questions.ContainsKey("GenericBloodPressure") ? questions["GenericBloodPressure"].Answer : string.Empty;
            //                        string DBP = questions.ContainsKey("GenericBloodPressurePer") ? questions["GenericBloodPressurePer"].Answer : string.Empty;

            //                        vitalSign.BPLying = "";
            //                        vitalSign.BPSitting = SBP.IsNotNullOrEmpty() && DBP.IsNotNullOrEmpty() ? string.Format("{0}/{1}", SBP, DBP) : string.Empty;
            //                        vitalSign.BPStanding = "";
            //                        vitalSign.ApicalPulse = "";
            //                        vitalSign.RadialPulse = "";
            //                        vitalSign.PainLevel = questions.ContainsKey("GenericPainLevel") ? questions["GenericPainLevel"].Answer : string.Empty;
            //                        vitalSign.OxygenSaturation = questions.ContainsKey("GenericO2Sat") ? questions["GenericO2Sat"].Answer : string.Empty;
            //                        vitalSign.BSMax = questions.ContainsKey("GenericBloodSugar") ? questions["GenericBloodSugar"].Answer : string.Empty;
            //                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;
            //                    }
            //                    var user = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    if (user.IsNotNullOrEmpty())
            //                    {
            //                        vitalSign.UserDisplayName = user;
            //                    }
            //                    vitalSigns.Add(vitalSign);
            //                }
            //            }
            //            else if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
            //            {
            //                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                {
            //                    var vitalSign = new VitalSign();
            //                    vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                    var questions = visitNote.ToDictionary();
            //                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                    if (questions != null)
            //                    {
            //                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


            //                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
            //                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

            //                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
            //                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

            //                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
            //                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;


            //                        vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
            //                        vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
            //                        vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");


            //                        var bs = new List<double>(); //new double[] { BSAM, BSNoon, BSPM, BSHS };
            //                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSAM > 0)
            //                        {
            //                            bs.Add(BSAM);
            //                        }
            //                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSNoon > 0)
            //                        {
            //                            bs.Add(BSNoon);
            //                        }
            //                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSPM > 0)
            //                        {
            //                            bs.Add(BSPM);
            //                        }
            //                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
            //                        if (BSHS > 0)
            //                        {
            //                            bs.Add(BSHS);
            //                        }
            //                        var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
            //                        var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
            //                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
            //                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
            //                        vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;// questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
            //                        vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
            //                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
            //                    }
            //                    var user = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    if (user.IsNotNullOrEmpty())
            //                    {
            //                        vitalSign.UserDisplayName = user;
            //                    }
            //                    vitalSigns.Add(vitalSign);
            //                }
            //            }
            //        }
            //    });
            //}
            //return vitalSigns;
        }

        public List<VitalSign> GetVitalSignsForSixtyDaySummary(PatientEpisode episode, DateTime date)
        {
            var noteDisciplineTasks = new List<int>();
            var disciplineTasksSNVList = DisciplineTaskFactory.ALLSkilledNurseDisciplineTasks(false);
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments(true);
            var disciplineTasksUAP = DisciplineTaskFactory.AllUAP();
            var disciplineTasksTherapy = DisciplineTaskFactory.AllTherapy(false);
            noteDisciplineTasks.AddRange(disciplineTasksSNVList);
            noteDisciplineTasks.AddRange(disciplineTasksOASISList);
            noteDisciplineTasks.AddRange(disciplineTasksUAP);
            noteDisciplineTasks.AddRange(disciplineTasksTherapy);
            noteDisciplineTasks.Add((int)DisciplineTasks.HHAideVisit);
            var vitalSigns = new List<VitalSign>();
            var scheduleEvents = scheduleRepository.GetScheduledEventsForOASISAndNotesByEpisode(Current.AgencyId, episode.PatientId, episode.Id, episode.StartDate, date > episode.EndDate ? episode.EndDate : date, new int[] { }, noteDisciplineTasks.ToArray(), new int[] { }, disciplineTasksOASISList.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = VitalSignFromList(scheduleEvents, disciplineTasksSNVList, disciplineTasksOASISList, disciplineTasksUAP, disciplineTasksTherapy);
            }

            return vitalSigns;

            //var vitalSigns = new List<VitalSign>();
            //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            //{
            //    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => Enum.IsDefined(typeof(DisciplineTasks), e.DisciplineTask) && e.EventDate.IsValidDate() && e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate && e.EventDate.ToDateTime().Date <= date.Date && !e.IsMissedVisit && e.IsDeprecated == false).ToList();
            //    if (scheduleEvents != null && scheduleEvents.Count > 0)
            //    {
            //        scheduleEvents.ForEach(s =>
            //        {
            //            if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
            //            {
            //                var vitalSign = new VitalSign();
            //                if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
            //                {
            //                    var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                    if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                    {
            //                        var questions = visitNote.ToDictionary();

            //                        vitalSign.VisitDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                        if (questions != null)
            //                        {
            //                            vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                            vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;

            //                            vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
            //                            vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

            //                            vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
            //                            vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

            //                            vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
            //                            vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

            //                            vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
            //                            vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
            //                            var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
            //                            var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSAM > 0)
            //                            {
            //                                bs.Add(BSAM);
            //                            }
            //                            var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSNoon > 0)
            //                            {
            //                                bs.Add(BSNoon);
            //                            }
            //                            var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSPM > 0)
            //                            {
            //                                bs.Add(BSPM);
            //                            }
            //                            var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSHS > 0)
            //                            {
            //                                bs.Add(BSHS);
            //                            }

            //                            var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
            //                            var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
            //                            vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;//questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
            //                            vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
            //                            vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                            vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
            //                        }
            //                        vitalSigns.Add(vitalSign);
            //                    }
            //                }
            //                else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.HHAideVisit ||
            //                ((DisciplineTasks)s.DisciplineTask).GetFormGroup() == "UAP")
            //                {
            //                    var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                    if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                    {
            //                        var questions = visitNote.ToDictionary();
            //                        vitalSign.VisitDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                        if (questions != null)
            //                        {
            //                            vitalSign.BPLying = "";
            //                            vitalSign.BPSitting = questions.ContainsKey("VitalSignBPVal") ? questions["VitalSignBPVal"].Answer : string.Empty;
            //                            vitalSign.BPStanding = "";

            //                            vitalSign.ApicalPulse = questions.ContainsKey("VitalSignHRVal") ? questions["VitalSignHRVal"].Answer : string.Empty;
            //                            vitalSign.RadialPulse = questions.ContainsKey("VitalSignHRVal") ? questions["VitalSignHRVal"].Answer : string.Empty;

            //                            vitalSign.PainLevel = "";

            //                            vitalSign.BSMax = "";
            //                            vitalSign.BSMin = "";

            //                            vitalSign.Weight = questions.ContainsKey("VitalSignWeightVal") ? questions["VitalSignWeightVal"].Answer : string.Empty;
            //                            vitalSign.Temp = questions.ContainsKey("VitalSignTempVal") ? questions["VitalSignTempVal"].Answer : string.Empty;
            //                            vitalSign.Resp = questions.ContainsKey("VitalSignRespVal") ? questions["VitalSignRespVal"].Answer : string.Empty;
            //                        }
            //                        vitalSigns.Add(vitalSign);
            //                    }
            //                }
            //                else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTVisit
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTAVisit
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STVisit
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTMaintenance
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReassessment
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReEvaluation
            //                || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTVisit)
            //                {
            //                    var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
            //                    if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
            //                    {
            //                        vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                        var questions = visitNote.ToDictionary();
            //                        vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                        if (questions != null)
            //                        {
            //                            string SBP = questions.ContainsKey("GenericBloodPressure") ? questions["GenericBloodPressure"].Answer : string.Empty;
            //                            string DBP = questions.ContainsKey("GenericBloodPressurePer") ? questions["GenericBloodPressurePer"].Answer : string.Empty;

            //                            vitalSign.BPLying = "";
            //                            vitalSign.BPSitting = SBP.IsNotNullOrEmpty() && DBP.IsNotNullOrEmpty() ? string.Format("{0}/{1}", SBP, DBP) : string.Empty;
            //                            vitalSign.BPStanding = "";
            //                            vitalSign.ApicalPulse = "";
            //                            vitalSign.RadialPulse = "";
            //                            vitalSign.PainLevel = questions.ContainsKey("GenericPainLevel") ? questions["GenericPainLevel"].Answer : string.Empty;
            //                            vitalSign.OxygenSaturation = questions.ContainsKey("GenericO2Sat") ? questions["GenericO2Sat"].Answer : string.Empty;
            //                            vitalSign.BSMax = questions.ContainsKey("GenericBloodSugar") ? questions["GenericBloodSugar"].Answer : string.Empty;
            //                            vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                            vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                            vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;
            //                        }
            //                        var user = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                        if (user.IsNotNullOrEmpty())
            //                        {
            //                            vitalSign.UserDisplayName = user;
            //                        }
            //                        vitalSigns.Add(vitalSign);
            //                    }
            //                }
            //                else if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
            //                {
            //                    vitalSign.DisciplineTask = s.DisciplineTaskName;
            //                    var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
            //                    if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
            //                    {
            //                        var questions = assessment.ToDictionary();
            //                        vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
            //                        if (questions != null)
            //                        {
            //                            vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
            //                            vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


            //                            vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
            //                            vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

            //                            vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
            //                            vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

            //                            vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
            //                            vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

            //                            var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
            //                            var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSAM > 0)
            //                            {
            //                                bs.Add(BSAM);
            //                            }
            //                            var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSNoon > 0)
            //                            {
            //                                bs.Add(BSNoon);
            //                            }
            //                            var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSPM > 0)
            //                            {
            //                                bs.Add(BSPM);
            //                            }
            //                            var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
            //                            if (BSHS > 0)
            //                            {
            //                                bs.Add(BSHS);
            //                            }

            //                            var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
            //                            var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
            //                            vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
            //                            vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

            //                            vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
            //                            vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
            //                            vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
            //                            vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
            //                        }
            //                        vitalSigns.Add(vitalSign);
            //                    }
            //                }
            //            }
            //        });
            //    }
            //}
            //return vitalSigns;
        }

        #endregion

        #region Therapy Exception

        public List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = scheduleRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId, new DateTime(2011, 04, 01), DateTime.Now);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };
                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length == 1)
                                    {
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        if (scheduleEvents.Count >= 13)
                                        {
                                            var evnt13 = scheduleEvents[12];
                                            if (evnt13.EventDate.IsValid())
                                            {
                                                var date13 = evnt13.EventDate;
                                                var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.Date == date13.Date)).ToList();
                                                if (scheduleEvents13 != null)
                                                {
                                                    if (evnt13.Discipline == "PT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "OT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "ST")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (evnt13 != null)
                                            {
                                                episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                            }
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                if (evnt19.EventDate.IsValid())
                                                {
                                                    var date19 = evnt19.EventDate;
                                                    var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.Date == date19.Date)).ToList();
                                                    if (scheduleEvents19 != null)
                                                    {
                                                        if (evnt19.Discipline == "PT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "OT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "ST")
                                                        {
                                                            evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }

                                            }
                                            else
                                            {
                                                episode.NineteenVisit = "NA";
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                    else if (disciplines.Length > 1)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        var first = true;
                                        if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
                                        {
                                            var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
                                            var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
                                            if (scheduleEvents13End != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    first = false;
                                                    evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (!evnt13check)
                                            {
                                                var evnt13 = scheduleEvents[12];
                                                if (evnt13 != null)
                                                {
                                                    episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            first = true;
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
                                                var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
                                                if (scheduleEvents19End != null)
                                                {
                                                    if (discipline.Contains("PT"))
                                                    {
                                                        first = false;
                                                        evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

                                                    }

                                                    if (discipline.Contains("OT"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                    }
                                                    if (discipline.Contains("ST"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                }
                            }

                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();

            //var episodes = patientRepository.GetAllEpisodeAfterApril(Current.AgencyId, branchId, patientId, startDate, endDate);
            //var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            //if (episodes != null && episodes.Count > 0)
            //{
            //    episodes.ForEach(episode =>
            //    {
            //        if (episode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date && (e.IsDeprecated == false)) && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).OrderBy(e => e.EventDate.ToDateTime().Date).ToList();
            //            if (scheduleEvents != null && scheduleEvents.Count > 0)
            //            {
            //                var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
            //                if (disciplines.Length == 1)
            //                {
            //                    var evnt19check = true;
            //                    var evnt13check = true;
            //                    if (scheduleEvents.Count >= 13)
            //                    {
            //                        var evnt13 = scheduleEvents[12];
            //                        if (evnt13.EventDate.IsValidDate())
            //                        {
            //                            var date13 = evnt13.EventDate.ToDateTime();
            //                            var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date13.Date)).ToList();
            //                            if (scheduleEvents13 != null)
            //                            {
            //                                if (evnt13.Discipline == "PT")
            //                                {
            //                                    evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
            //                                }
            //                                else if (evnt13.Discipline == "OT")
            //                                {
            //                                    evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                }
            //                                else if (evnt13.Discipline == "ST")
            //                                {
            //                                    evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                }
            //                            }
            //                        }
            //                        if (evnt13 != null)
            //                        {
            //                            episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
            //                        }
            //                        if (scheduleEvents.Count >= 19)
            //                        {
            //                            var evnt19 = scheduleEvents[18];
            //                            if (evnt19.EventDate.IsValidDate())
            //                            {
            //                                var date19 = evnt19.EventDate.ToDateTime();
            //                                var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date19.Date)).ToList();
            //                                if (scheduleEvents19 != null)
            //                                {
            //                                    if (evnt19.Discipline == "PT")
            //                                    {
            //                                        evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
            //                                    }
            //                                    else if (evnt19.Discipline == "OT")
            //                                    {
            //                                        evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                    }
            //                                    else if (evnt19.Discipline == "ST")
            //                                    {
            //                                        evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                    }
            //                                }
            //                            }
            //                            if (evnt19 != null)
            //                            {
            //                                episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
            //                            }

            //                        }
            //                        else
            //                        {
            //                            episode.NineteenVisit = "NA";
            //                        }
            //                        if (!evnt19check || !evnt13check)
            //                        {
            //                            episode.ScheduledTherapy = scheduleEvents.Count;
            //                            var completedSchedules = scheduleEvents.Where(s => (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                            if (completedSchedules != null)
            //                            {
            //                                episode.CompletedTherapy = completedSchedules.Count;
            //                            }
            //                            episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
            //                            episode.Schedule = string.Empty;
            //                            therapyEpisodes.Add(episode);
            //                        }
            //                    }
            //                }
            //                else if (disciplines.Length > 1)
            //                {
            //                    var discipline = string.Join(";", disciplines);
            //                    var evnt19check = true;
            //                    var evnt13check = true;
            //                    var first = true;
            //                    if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
            //                    {
            //                        var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
            //                        var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
            //                        if (scheduleEvents13End != null)
            //                        {
            //                            if (discipline.Contains("PT"))
            //                            {
            //                                first = false;
            //                                evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
            //                            }
            //                            if (discipline.Contains("OT"))
            //                            {
            //                                if (first)
            //                                {
            //                                    first = false;
            //                                    evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                }
            //                                else
            //                                {
            //                                    evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                }
            //                            }
            //                            if (discipline.Contains("ST"))
            //                            {
            //                                if (first)
            //                                {
            //                                    first = false;
            //                                    evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                }
            //                                else
            //                                {
            //                                    evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                }
            //                            }
            //                        }
            //                        if (!evnt13check)
            //                        {
            //                            var evnt13 = scheduleEvents[12];
            //                            if (evnt13 != null)
            //                            {
            //                                episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
            //                            }
            //                        }
            //                        first = true;
            //                        if (scheduleEvents.Count >= 19)
            //                        {
            //                            var evnt19 = scheduleEvents[18];
            //                            var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
            //                            var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
            //                            if (scheduleEvents19End != null)
            //                            {
            //                                if (discipline.Contains("PT"))
            //                                {
            //                                    first = false;
            //                                    evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

            //                                }

            //                                if (discipline.Contains("OT"))
            //                                {
            //                                    if (first)
            //                                    {
            //                                        first = false;
            //                                        evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                    }
            //                                    else
            //                                    {
            //                                        evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
            //                                    }
            //                                }
            //                                if (discipline.Contains("ST"))
            //                                {
            //                                    if (first)
            //                                    {
            //                                        first = false;
            //                                        evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                    }
            //                                    else
            //                                    {
            //                                        evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
            //                                    }
            //                                }
            //                            }
            //                            if (evnt19 != null)
            //                            {
            //                                episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
            //                            }
            //                        }
            //                        if (!evnt19check || !evnt13check)
            //                        {
            //                            episode.ScheduledTherapy = scheduleEvents.Count;
            //                            var completedSchedules = scheduleEvents.Where(s => (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                            if (completedSchedules != null)
            //                            {
            //                                episode.CompletedTherapy = completedSchedules.Count;
            //                            }
            //                            episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
            //                            episode.Schedule = string.Empty;
            //                            therapyEpisodes.Add(episode);
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    });
            //}
            //return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int count)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = scheduleRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count >= count)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };

                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length > 0)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        if (discipline.IsNotNullOrEmpty())
                                        {
                                            var evntCommon = scheduleEvents[count - 1];
                                            var scheduleEventsCommon = scheduleEvents.Take(count).Reverse().ToList();
                                            var scheduleEventsCommonEnd = scheduleEventsCommon.Take(3).ToList();
                                            if (scheduleEventsCommonEnd != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    var ptEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation);
                                                    if (ptEval != null && ptEval.EventDate.IsValid())
                                                    {
                                                        episode.PTEval = ptEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.PTEval = "NA";
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    var otEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation);
                                                    if (otEval != null && otEval.EventDate.IsValid())
                                                    {
                                                        episode.OTEval = otEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.OTEval = "NA";
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    var stEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation);
                                                    if (stEval != null && stEval.EventDate.IsValid())
                                                    {
                                                        episode.STEval = stEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.STEval = "NA";
                                                }
                                            }
                                            episode.ScheduledTherapy = scheduleEvents.Count;
                                            var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                            if (completedSchedules != null)
                                            {
                                                episode.CompletedTherapy = completedSchedules.Count;
                                            }
                                            episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                            therapyEpisodes.Add(episode);
                                        }
                                    }

                                }
                            }
                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

        #region Hospitalization Log

        public bool AddHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var hospitalizationLog = new HospitalizationLog
            {
                Id = Guid.NewGuid(),
                UserId = formCollection.GetGuid("UserId"),
                PatientId = formCollection.GetGuid("PatientId"),
                EpisodeId = formCollection.GetGuid("EpisodeId"),
                HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue,
                Data = ProcessHospitalizationData(formCollection),
                Created = DateTime.Now,
                AgencyId = Current.AgencyId,
                SourceId = (int)TransferSourceTypes.User,
                Modified = DateTime.Now
            };

            if (patientRepository.AddHospitalizationLog(hospitalizationLog))
            {
                var patient = patientRepository.GetPatientOnly(hospitalizationLog.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsHospitalized = true;
                    patient.HospitalizationId = hospitalizationLog.Id;
                    if (patientRepository.Update(patient))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var logId = formCollection.GetGuid("Id");
            var patientId = formCollection.GetGuid("PatientId");
            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, logId);
            if (hospitalizationLog != null)
            {
                hospitalizationLog.UserId = formCollection.GetGuid("UserId");
                hospitalizationLog.EpisodeId = formCollection.GetGuid("EpisodeId");
                hospitalizationLog.Modified = DateTime.Now;
                hospitalizationLog.HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.Data = ProcessHospitalizationData(formCollection);

                if (patientRepository.UpdateHospitalizationLog(hospitalizationLog))
                {
                    result = true;
                }
            }

            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId)
        {
            var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(l =>
                {
                    l.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/HospitalizationLogPdf', { 'patientId': '" + l.PatientId + "', 'hospitalizationLogId': '" + l.Id + "' });\"><span class=\"img icon print\"></span></a>";
                });
            }
            return logs;
        }

        public HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                var patient = patientRepository.GetPatientOnly(log.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    log.Patient = patient;
                    log.Location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                    if (!log.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, log.EpisodeId, log.PatientId);
                        if (episode != null)
                        {
                            log.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                    }
                }
            }
            return log;
        }

        public List<PatientHospitalizationData> GetHospitalizationLogs(Guid agencyId)
        {
            var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
            if (patientList != null && patientList.Count > 0)
            {
                var userIds = patientList.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    if (users != null && users.Count > 0)
                    {
                        patientList.ForEach(d =>
                        {
                            if (!d.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == d.UserId);
                                if (user != null)
                                {
                                    d.User = user.DisplayName;
                                }
                            }
                        });
                    }
                }
            }
            return patientList;
        }

        #endregion

        #region return comment

        public string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId)
        {
            string CommentString = this.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in NewComments)
            {
                if (comment.IsDeprecated) continue;
                if (CommentString.IsNotNullOrEmpty()) CommentString += "<hr/>";
                if (comment.UserId == Current.UserId) CommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                CommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", UserEngine.GetName(comment.UserId, Current.AgencyId), comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return CommentString;
        }

        private string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId)
        {
            ScheduleEvent task = scheduleRepository.GetScheduleEventNew(agencyId, patientId, episodeId, eventId);
            return task.ReturnReason.IsNotNullOrEmpty() ? task.ReturnReason : string.Empty;
        }

        public bool AddReturnComments(Guid eventId, Guid episodeId, string comment)
        {
            return patientRepository.AddReturnComment(new ReturnComment
            {
                AgencyId = Current.AgencyId,
                Comments = comment,
                Created = DateTime.Now,
                EpisodeId = episodeId,
                EventId = eventId,
                Modified = DateTime.Now,
                UserId = Current.UserId
            });
        }

        public bool EditReturnComments(int id, string comment)
        {
            ReturnComment ExistingComment = patientRepository.GetReturnComment(Current.AgencyId, id);
            ExistingComment.Modified = DateTime.Now;
            ExistingComment.Comments = comment;
            return patientRepository.UpdateReturnComment(ExistingComment);
        }

        public bool DeleteReturnComments(int id)
        {
            return patientRepository.DeleteReturnComments(Current.AgencyId, id);
        }

        #endregion

        #region  Visit Note

        public VisitNoteViewData GetVisitNote(ScheduleEvent scheduledEvent, NoteArguments arguments)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (scheduledEvent != null)
            {
                var patient = patientRepository.GetPatientOnly(arguments.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, arguments.EpisodeId, arguments.PatientId);
                    if (episode != null)
                    {
                        var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, arguments.EpisodeId, arguments.PatientId, arguments.EventId);
                        if (patientvisitNote != null)
                        {
                            IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                            noteViewData.StatusComment = scheduledEvent.StatusComment;
                            noteViewData.UserId = scheduledEvent.UserId;
                            if (arguments.IsDiagnosisNeeded || arguments.IsPlanOfCareNeeded)
                            {
                                var assessment = assessmentService.GetEpisodeAssessment(arguments.EpisodeId, arguments.PatientId, scheduledEvent.EventDate);
                                if (assessment != null)
                                {
                                    if (arguments.IsDiagnosisNeeded)
                                    {
                                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                                    }
                                    if (arguments.IsPlanOfCareNeeded)
                                    {
                                        noteViewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(arguments.EpisodeId, arguments.PatientId, assessment.Id, assessment.Type.ToString());
                                    }
                                }
                            }

                            if (ScheduleStatusFactory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status))
                            {
                                if (arguments.IsHHACarePlanNeeded)
                                {
                                    var pocEvent = patientRepository.GetHHAPlanOfCareVisitNote(arguments.EpisodeId, arguments.PatientId);
                                    if (pocEvent != null)
                                    {
                                        noteViewData.Questions = pocEvent.ToHHADefaults();
                                    }
                                }

                                if (arguments.IsPASCarePlanNeeded)
                                {
                                    var notePASMerger=noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                    this.PASVisitNoteMerger(arguments.PatientId, arguments.EpisodeId, ref notePASMerger);
                                     noteViewData.Questions = notePASMerger;
                                    //var pocEvent = patientRepository.GetVisitNoteByType(arguments.EpisodeId, arguments.PatientId, DisciplineTasks.PASCarePlan);
                                    //if (pocEvent != null)
                                    //{
                                    //    noteViewData.Questions = pocEvent.ToDictionary();
                                    //}
                                }

                                if (arguments.IsVitalSignMerged)
                                {
                                    var noteQuestionBeforeVital = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                    this.MergeVitalSignWithNote(episode, scheduledEvent.EventDate, ref noteQuestionBeforeVital);
                                    noteViewData.Questions = noteQuestionBeforeVital;
                                }


                                noteViewData.Questions = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                                    if (physician != null)
                                    {
                                        noteViewData.PhysicianId = physician.Id;
                                        noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                            }
                            else
                            {
                                noteViewData.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    if (patientvisitNote.PhysicianId.IsEmpty())
                                    {
                                        var physicianId = this.GetPhysicianFromNotes(scheduledEvent.DisciplineTask, noteViewData.Questions);
                                        if (physicianId != null)
                                        {
                                            noteViewData.PhysicianId = physicianId;
                                        }
                                    }
                                    else
                                    {
                                        noteViewData.PhysicianId = patientvisitNote.PhysicianId;
                                    }
                                }
                            }

                            noteViewData.EndDate = episode.EndDate;
                            noteViewData.StartDate = episode.StartDate;
                            noteViewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                            noteViewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                            noteViewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                            noteViewData.PatientId = patientvisitNote.PatientId;
                            noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                            noteViewData.EventId = patientvisitNote.Id;
                            noteViewData.DisciplineTask = scheduledEvent.DisciplineTask;
                            noteViewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                            noteViewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                            if (arguments.IsAllergyNeeded)
                            {
                                noteViewData.Allergies = this.GetAllergies(arguments.PatientId);
                            }
                            if (arguments.IsEvalNeeded)
                            {
                                noteViewData.CarePlanOrEvalUrl = this.GetScheduledEventUrl(scheduledEvent, arguments.EvalTask);
                            }
                            if (arguments.IsPreviousNoteNeeded)
                            {
                                noteViewData.PreviousNotes = this.GetPreviousNotes(arguments.PatientId, scheduledEvent);
                            }
                            if (arguments.IsDiagnosisNeeded)
                            {
                                noteViewData.Questions = this.GetDiagnosisMergedFromOASIS(noteViewData.Questions, oasisQuestions);
                            }
                            noteViewData.Patient = patient;
                            noteViewData.Version = patientvisitNote.Version;
                        }
                        else
                        {
                            noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return noteViewData;
        }

        public VisitNoteViewData GetVisitNoteForContent(Guid patientId, Guid noteId, Guid previousNoteId, string type)
        {
            var viewData = new VisitNoteViewData();
            viewData.Type = type;
            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, noteId);
            if (scheduleEvent != null)
            {
                var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
                if (patientvisitNote != null)
                {
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = scheduleEvent.EpisodeId;
                    viewData.EventId = scheduleEvent.EventId;
                    viewData.Version = scheduleEvent.Version > 0 ? scheduleEvent.Version : 1;
                    viewData.TypeName = viewData.Type.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), viewData.Type) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), viewData.Type)).GetDescription() : "";
                    if (DisciplineTaskFactory.SkilledNurseSharedFile().Contains(scheduleEvent.DisciplineTask))
                    {
                        viewData = SkilledNurseVisitContent(scheduleEvent, patientvisitNote, viewData);
                    }
                    else
                    {
                        viewData.Questions = patientvisitNote.ToDictionary();
                    }
                }
            }
            return viewData;
        }

        public VisitNoteViewData GetVisitNotePrint()
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(int type)
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var task = Enum.IsDefined(typeof(DisciplineTasks), type) ? ((DisciplineTasks)type) : DisciplineTasks.NoDiscipline;
            note.Type = task.ToString();
            note.TypeName = task.GetDescription();
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(ScheduleEvent scheduledEvent, NoteArguments arguments)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patient = patientRepository.GetPatientOnly(scheduledEvent.PatientId, Current.AgencyId);
            if (patient != null)
            {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                if (episode != null)
                {
                    if (scheduledEvent != null)
                    {
                        var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId);
                        if (patientvisitNote != null)
                        {
                            if (arguments.IsAllergyNeeded)
                            {
                                var allergyProfile = patientRepository.GetAllergyProfileByPatient(scheduledEvent.PatientId, Current.AgencyId);
                                if (allergyProfile != null)
                                {
                                    noteViewData.Allergies = allergyProfile.ToString();
                                }
                            }
                            noteViewData.SignatureText = patientvisitNote.SignatureText;
                            noteViewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                            noteViewData.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();

                            IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                            if (arguments.IsAssessmentNeeded)
                            {
                                var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventDate);
                                if (assessment != null)
                                {
                                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                    oasisQuestions = assessment.ToNotesQuestionDictionary();
                                }
                            }
                            if (ScheduleStatusFactory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status))
                            {
                                if (arguments.IsPhysicainNeeded)
                                {
                                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                                    if (physician != null)
                                    {
                                        noteViewData.PhysicianId = physician.Id;
                                        noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                   
                                }
                            }
                            else
                            {
                                
                                if (arguments.IsPhysicainNeeded)
                                {
                                    var physicianInfoSet = false;
                                    if (patientvisitNote.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                                        if (physician != null)
                                        {
                                            noteViewData.PhysicianId = physician.Id;
                                            noteViewData.PhysicianDisplayName = physician.DisplayName;
                                            physicianInfoSet = true;
                                        }

                                    }
                                    if (!physicianInfoSet)
                                    {
                                        if (patientvisitNote.PhysicianId.IsEmpty())
                                        {
                                            var physicianId = this.GetPhysicianFromNotes(scheduledEvent.DisciplineTask, noteViewData.Questions);
                                            if (!physicianId.IsEmpty())
                                            {
                                                noteViewData.PhysicianId = physicianId;
                                                // note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                            }
                                        }
                                        else
                                        {
                                            noteViewData.PhysicianId = patientvisitNote.PhysicianId;
                                        }
                                        if (!noteViewData.PhysicianId.IsEmpty())
                                        {
                                            var physician = PhysicianEngine.Get(noteViewData.PhysicianId, Current.AgencyId);
                                            if (physician != null)
                                            {
                                                noteViewData.PhysicianId = physician.Id;
                                                noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                            }
                                        }
                                    }
                                }
                            }


                            var selectedEpisodeId = noteViewData.Questions != null && noteViewData.Questions.ContainsKey("SelectedEpisodeId") && noteViewData.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? noteViewData.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                            if (!selectedEpisodeId.IsEmpty())
                            {
                                var episodeSelected = patientRepository.GetEpisodeOnly(Current.AgencyId, selectedEpisodeId, scheduledEvent.PatientId);
                                if (episodeSelected != null)
                                {
                                    noteViewData.EndDate = episodeSelected.EndDate;
                                    noteViewData.StartDate = episodeSelected.StartDate;
                                }
                            }
                            else
                            {
                                noteViewData.EndDate = episode.EndDate;
                                noteViewData.StartDate = episode.StartDate;
                            }
                            noteViewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                            if (noteViewData.IsWoundCareExist)
                            {
                                noteViewData.WoundCare = patientvisitNote.ToWoundCareDictionary();
                            }
                            noteViewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                            noteViewData.PatientId = patientvisitNote.PatientId;
                            noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                            noteViewData.EventId = patientvisitNote.Id;
                            noteViewData.DisciplineTask = scheduledEvent.DisciplineTask;
                            noteViewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                            noteViewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : string.Empty;
                            noteViewData.Version = patientvisitNote.Version;
                            if (arguments.IsAssessmentNeeded)
                            {
                                noteViewData.Questions = this.GetDiagnosisMergedFromOASIS(noteViewData.Questions, oasisQuestions);
                            }
                            noteViewData.Patient = patient;
                        }
                        else
                        {
                            noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return noteViewData;
        }

        #endregion

        #region  Missed Visit

        public bool ProcessMissedVisitNotes(string button, Guid Id)
        {
            var mv = patientRepository.GetMissedVisit(Current.AgencyId, Id);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (patientRepository.UpdateMissedVisit(mv))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessMissedVisitForm(string button, Guid eventId, string reason)
        {
            var result = false;
            var mv = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (patientRepository.UpdateMissedVisit(mv))
            {
                result = true;
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            bool result = false;
            //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
            //if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            //{
            //    missedVisit.AgencyId = Current.AgencyId;
            //    var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
            if (missedVisit != null)
            {
                missedVisit.AgencyId = Current.AgencyId;
                var scheduledEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, missedVisit.PatientId, missedVisit.EpisodeId, missedVisit.Id);// events.Find(e => e.EventId == missedVisit.Id);
                if (scheduledEvent != null)
                {
                    scheduledEvent.IsMissedVisit = true;
                    scheduledEvent.MissedVisitFormReturnReason = "";//Clean up return reason if any. 

                    var existing = patientRepository.GetMissedVisit(Current.AgencyId, scheduledEvent.EventId);
                    if (existing != null)
                    {
                        existing.EventDate = missedVisit.EventDate;
                        existing.IsOrderGenerated = missedVisit.IsOrderGenerated;
                        existing.IsPhysicianOfficeNotified = missedVisit.IsPhysicianOfficeNotified;
                        existing.Reason = missedVisit.Reason;
                        existing.SignatureDate = missedVisit.SignatureDate;
                        existing.SignatureText = missedVisit.SignatureText;
                        existing.Comments = missedVisit.Comments;
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            existing.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                        }
                        else
                        {
                            existing.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                        }
                        result = patientRepository.UpdateMissedVisit(existing);
                    }
                    else
                    {
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                        }
                        else
                        {
                            missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                        }
                        result = patientRepository.AddMissedVisit(missedVisit);
                    }
                    if (result)
                    {
                        if (scheduleRepository.UpdateScheduleEventNew(scheduledEvent))
                        {
                            result = true;
                            if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Set as a missed visit by " + Current.UserFullName);
                            }
                        }
                    }
                }
                //}
            }
            return result;
        }

        public JsonViewData MissedVisitRestore(Guid patientId, Guid episodeId, Guid eventId)
        {
            var jsonViewData = new JsonViewData { isSuccessful = false };
            var evnt = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (evnt != null)
            {
                evnt.IsMissedVisit = false;
                if (scheduleRepository.UpdateScheduleEventNew(evnt))
                {
                    
                    jsonViewData.isSuccessful = true;
                    jsonViewData.EpisodeId = evnt.EpisodeId;
                    jsonViewData.PatientId = evnt.PatientId;
                    jsonViewData.IsCaseManagementRefresh = evnt.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview) || evnt.Status == ((int)ScheduleStatus.OasisCompletedPendingReview) || evnt.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature) || evnt.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
                    //jsonViewData.IsDataCentersRefresh = oldStatus != scheduleEventToEdit.Status;
                    jsonViewData.IsMyScheduleTaskRefresh = jsonViewData.IsDataCentersRefresh && evnt.UserId == Current.UserId && !evnt.IsComplete && evnt.EventDate.Date >= DateTime.Now.AddDays(-89) && evnt.EventDate.Date <= DateTime.Now.AddDays(14);

                }
                else
                {
                    jsonViewData.isSuccessful = false;
                }
            }
            return jsonViewData;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid Id)
        {
            var missedVisit = patientRepository.GetMissedVisit(agencyId, Id);
            if (missedVisit != null)
            {
                var patient = patientRepository.GetPatientOnly(missedVisit.PatientId, agencyId);
                if (patient != null)
                {
                    missedVisit.PatientName = patient.DisplayName;
                }
                var scheduledEvent = scheduleRepository.GetScheduleEventNew(agencyId, missedVisit.PatientId, missedVisit.EpisodeId, Id);
                if (scheduledEvent != null)
                {
                    missedVisit.EventDate = scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                    missedVisit.EndDate = scheduledEvent.EndDate;
                    missedVisit.StartDate = scheduledEvent.StartDate;
                    missedVisit.VisitType = scheduledEvent.DisciplineTaskName;
                    var user = UserEngine.GetUser(scheduledEvent.UserId, agencyId);
                    if (user != null)
                    {
                        missedVisit.UserName = user.DisplayName;
                    }
                }
            }
            return missedVisit;
        }

        public MissedVisit GetMissedVisitPrint()
        {
            var note = new MissedVisit();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId)
        {
            var note = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
            if (note != null)
            {
                note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                note.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, note.EpisodeId, eventId);
                if (scheduleEvent != null)
                {
                    note.EventDate = scheduleEvent.EventDate.ToString("MM/dd/yyyy");
                    note.DisciplineTaskName = scheduleEvent != null && scheduleEvent.DisciplineTaskName.IsNotNullOrEmpty() ? scheduleEvent.DisciplineTaskName : string.Empty;
                }
            }
            return note;
        }

        public List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, bool isExcel)
        {
            var patientEvents = scheduleRepository.GetMissedScheduledEvents(agencyId, branchId, startDate, endDate);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        v.UserName = user.DisplayName;
                    }
                    if (v.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        v.UserName = "Axxess";
                    }
                    if (!isExcel)
                    {
                        Common.Url.SetNew(v, true, true, false, false);
                    }
                });
            }
            return patientEvents;
            //DateTime now = DateTime.Now;
            //var patientEvents = new List<ScheduleEvent>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(agencyId, branchId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    var missedVisits = patientRepository.GetMissedVisitsOnly(agencyId, startDate, endDate);
            //    patientEpisodes.ForEach(patientEpisode =>
            //    {
            //        if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var detail = new EpisodeDetail();
            //            if (patientEpisode.Details.IsNotNullOrEmpty())
            //            {
            //                detail = patientEpisode.Details.ToObject<EpisodeDetail>();
            //            }
            //            var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true).ToList();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                schedule.ForEach(s =>
            //                {
            //                    if (s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
            //                    {
            //                        if (s.IsMissedVisit)
            //                        {
            //                            var missedVisit = missedVisits.FirstOrDefault(f => f.Id == s.EventId);
            //                            if (missedVisit != null)
            //                            {
            //                                s.MissedVisitComments = missedVisit.ToString();
            //                            }
            //                            s.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                            if (detail.Comments.IsNotNullOrEmpty())
            //                            {
            //                                s.EpisodeNotes = detail.Comments;
            //                            }
            //                            s.EventDate = s.EventDate.ToZeroFilled();
            //                            s.VisitDate = s.VisitDate.ToZeroFilled();
            //                            s.StartDate = patientEpisode.StartDate.ToDateTime();
            //                            s.EndDate = patientEpisode.EndDate.ToDateTime();
            //                            s.PatientName = patientEpisode.PatientName;
            //                            s.UserName = !s.UserId.IsEmpty() ? UserEngine.GetName(s.UserId, agencyId).ToUpperCase() : string.Empty;
            //                            Common.Url.Set(s, true, true);
            //                            patientEvents.Add(s);
            //                        }
            //                    }
            //                });
            //            }
            //        }
            //    });
            //}
            //DateTime then = DateTime.Now;
            //DateTime diff = new DateTime(then.Ticks - now.Ticks);
            //return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        #endregion

        #region More Members

        public List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId)
        {
            var visitNoteviewDatas = new List<VisitNoteViewData>();
            var visits = patientRepository.GetVisitNotesByDisciplineTaskWithStatus(Current.AgencyId, patientId, new int[] { (int)ScheduleStatus.NoteCompleted }, new int[] { (int)DisciplineTasks.SixtyDaySummary });
            if (visits != null && visits.Count > 0)
            {
                var userIds = visits.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                visits.ForEach(v =>
                {
                    var visitNoteviewData = new VisitNoteViewData();
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        visitNoteviewData.UserDisplayName = user.DisplayName;
                    }
                    var questions = v.ToDictionary();
                    if (questions != null && questions.Count > 0)
                    {
                        var physicianId = v.PhysicianId.IsEmpty() ? questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty : v.PhysicianId;
                        visitNoteviewData.VisitDate = v.VisitDate.Date > DateTime.MinValue.Date ? v.VisitDate.ToString("MM/dd/yyyy") : v.EventDate.ToString("MM/dd/yyyy");
                        if (!physicianId.IsEmpty())
                        {
                            var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                            }
                        }
                        visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                    }
                    visitNoteviewData.StartDate = v.StartDate;
                    visitNoteviewData.EndDate = v.EndDate;
                    visitNoteviewData.PrintUrl = Url.Print(new ScheduleEvent { DisciplineTask = (int)DisciplineTasks.SixtyDaySummary, EventId = v.Id, EpisodeId = v.EpisodeId, PatientId = v.PatientId }, true);
                    visitNoteviewDatas.Add(visitNoteviewData);
                });
                //visits.ForEach(v =>
                //{
                //    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, v.EpisodeId, patientId);
                //    if (episode != null && episode.IsDischarged == false && episode.IsActive == true)
                //    {
                //        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //        if (scheduleEvents != null && scheduleEvents.Count > 0)
                //        {
                //            var scheduleEvent = scheduleEvents.SingleOrDefault(s => s.EventId == v.Id);
                //            if (scheduleEvent != null)
                //            {
                //                var visitNoteviewData = new VisitNoteViewData();
                //                var user = userRepository.GetUserOnly(v.UserId, Current.AgencyId);
                //                if (user != null)
                //                {
                //                    visitNoteviewData.UserDisplayName = user.DisplayName;
                //                }
                //                var questions = v.ToDictionary();
                //                if (questions != null && questions.Count > 0)
                //                {
                //                    var physicianId = questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty;
                //                    visitNoteviewData.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate.ToZeroFilled() : scheduleEvent.EventDate.ToZeroFilled();
                //                    if (!physicianId.IsEmpty())
                //                    {
                //                        var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                //                        if (physician != null)
                //                        {
                //                            visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                //                        }
                //                    }
                //                }
                //                visitNoteviewData.StartDate = episode.StartDate;
                //                visitNoteviewData.EndDate = episode.EndDate;
                //                visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                //                visitNoteviewData.PrintUrl = Url.Print(scheduleEvent, true);
                //                visitNoteviewDatas.Add(visitNoteviewData);
                //            }
                //        }
                //    }
                //});
            }
            return visitNoteviewDatas;
        }

        public FrequenciesViewData GetPatientEpisodeFrequencyData(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            FrequenciesViewData frequencyViewData = new FrequenciesViewData();
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var assessment = assessmentService.GetEpisodeAssessment(patientEpisode);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    var tmpString = new List<string>();

                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        string assessmentFrequency = string.Format("485{0}Frequency", taskType);
                        if (assessmentQuestions.ContainsKey(assessmentFrequency) && assessmentQuestions[assessmentFrequency].Answer.IsNotNullOrEmpty())
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData(assessmentQuestions[assessmentFrequency].Answer));
                        }
                        else
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData());
                        }
                    }
                }
                else
                {
                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        frequencyViewData.Visits.Add(taskType, new VisitData());
                    }
                }
                //if (!string.IsNullOrEmpty(patientEpisode.Schedule))
                //{
                var patientActivities = scheduleRepository.GetScheduleEventsVeryLeanNonOptionalPar(Current.AgencyId, patientId, episodeId, new int[] { }, new int[] { }, patientEpisode.StartDate, patientEpisode.EndDate, true, false);// patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                if (patientActivities != null && patientActivities.Count > 0)
                {
                    foreach (var item in patientActivities)
                    {
                        if (!item.IsDeprecated && item.IsBillable)
                        {
                            if (DisciplineTaskFactory.ALLSkilledNurseDisciplineTasks(true).Contains(item.DisciplineTask))  //(item.IsSkilledNurseNote())
                            {
                                frequencyViewData.Visits["SN"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.HHA.ToString()))
                            {
                                frequencyViewData.Visits["HHA"].Count++;
                            }
                            else if (DisciplineTaskFactory.PTNoteDisciplineTasks().Contains(item.DisciplineTask))// (item.IsPTNote())
                            {
                                frequencyViewData.Visits["PT"].Count++;
                            }
                            else if (DisciplineTaskFactory.OTNoteDisciplineTasks().Contains(item.DisciplineTask))//(item.IsOTNote())
                            {
                                frequencyViewData.Visits["OT"].Count++;
                            }
                            else if (DisciplineTaskFactory.STNoteDisciplineTasks().Contains(item.DisciplineTask))//(item.IsSTNote())
                            {
                                frequencyViewData.Visits["ST"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.MSW.ToString()))//(item.IsMSW())
                            {
                                frequencyViewData.Visits["MSW"].Count++;
                            }
                        }
                    }
                }
                //}
            }
            return frequencyViewData;
        }

        public string GetInsurance(string insurance)
        {
            var name = string.Empty;
            if (insurance.IsNotNullOrEmpty() && insurance.IsInteger())
            {
                if (insurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(insurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        name = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, insurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        name = standardInsurance.Name;
                    }
                }
            }
            return name;
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return lookupRepository.GetDisciplineTask(disciplineTaskId);
        }

        public string GetScheduledEventUrl(ScheduleEvent evnt, DisciplineTasks task)
        {
            var url = string.Empty;
            var tasks = new List<int>();
            var preUrl = string.Empty;
            switch (task)
            {
                case DisciplineTasks.HHAideCarePlan:
                    tasks.Add((int)DisciplineTasks.HHAideCarePlan);
                    break;

                case DisciplineTasks.PTEvaluation:
                    tasks.Add((int)DisciplineTasks.PTEvaluation);
                    tasks.Add((int)DisciplineTasks.PTReEvaluation);
                    break;
                case DisciplineTasks.STEvaluation:
                    tasks.Add((int)DisciplineTasks.STEvaluation);
                    tasks.Add((int)DisciplineTasks.STReEvaluation);
                    break;
                case DisciplineTasks.OTEvaluation:
                    tasks.Add((int)DisciplineTasks.OTEvaluation);
                    tasks.Add((int)DisciplineTasks.OTReEvaluation);
                    break;
                default:
                    break;

            }
            if (tasks != null && tasks.Count > 0)
            {
                var scheduleEvent = scheduleRepository.GetLastScheduledEvent(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.StartDate, evnt.EndDate, evnt.StartDate, evnt.EventDate.AddDays(-1), tasks.ToArray());
                if (scheduleEvent != null)
                {
                    switch (scheduleEvent.DisciplineTask)
                    {
                        case (int)DisciplineTasks.HHAideCarePlan:
                            url = new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
                            .AppendFormat("Url: '/HHAideCarePlan/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                            .Append("})\">View Care Plan</a>").ToString();
                            break;
                        case (int)DisciplineTasks.PTEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", DisciplineTasks.PTEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        case (int)DisciplineTasks.PTReEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.PTReEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        case (int)DisciplineTasks.STEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", DisciplineTasks.STEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        case (int)DisciplineTasks.STReEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.STReEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        case (int)DisciplineTasks.OTEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", DisciplineTasks.OTEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        case (int)DisciplineTasks.OTReEvaluation:
                            url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.OTReEvaluation.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            break;
                        default:
                            break;
                    }
                }
            }

            return url;

            //var url = string.Empty;
            //if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            //{
            //    var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>().OrderBy(e => e.EventDate).ToList();
            //    if (schedule != null && schedule.Count > 0)
            //    {
            //        switch (task)
            //        {
            //            case DisciplineTasks.HHAideCarePlan:
            //                var hhaCarePlan = GetCarePlanBySelectedEpisode(episode.PatientId, episode.Id, DisciplineTasks.HHAideCarePlan);
            //                if (hhaCarePlan != null)
            //                {
            //                    url = new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
            //                    .AppendFormat("Url: '/HHACarePlan/View/{0}/{1}/{2}'", hhaCarePlan.EpisodeId, hhaCarePlan.PatientId, hhaCarePlan.Id)
            //                    .Append("})\">View Care Plan</a>").ToString();
            //                }
            //                break;
            //            case DisciplineTasks.PTEvaluation:
            //                var ptEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
            //                if (ptEval != null)
            //                {
            //                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), ptEval.EpisodeId, ptEval.PatientId, ptEval.EventId);
            //                }
            //                else
            //                {
            //                    var ptReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation).FirstOrDefault();
            //                    if (ptReEval != null)
            //                    {
            //                        url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.PTReEvaluation.ToString(), ptReEval.EpisodeId, ptReEval.PatientId, ptReEval.EventId);
            //                    }
            //                }
            //                break;
            //            case DisciplineTasks.STEvaluation:
            //                var stEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
            //                if (stEval != null)
            //                {
            //                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), stEval.EpisodeId, stEval.PatientId, stEval.EventId);
            //                }
            //                else
            //                {
            //                    var stReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation).FirstOrDefault();
            //                    if (stReEval != null)
            //                    {
            //                        url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.STReEvaluation.ToString(), stReEval.EpisodeId, stReEval.PatientId, stReEval.EventId);
            //                    }
            //                }
            //                break;
            //            case DisciplineTasks.OTEvaluation:
            //                var otEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
            //                if (otEval != null)
            //                {
            //                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), otEval.EpisodeId, otEval.PatientId, otEval.EventId);
            //                }
            //                else
            //                {
            //                    var otReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation).FirstOrDefault();
            //                    if (otReEval != null)
            //                    {
            //                        url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.OTReEvaluation.ToString(), otReEval.EpisodeId, otReEval.PatientId, otReEval.EventId);
            //                    }
            //                }
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            //}
            //return url;
        }

        public DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            var viewData = new DrugDrugInteractionsViewData();
            if (drugsSelected != null && drugsSelected.Count > 0)
            {
                viewData.DrugDrugInteractions = drugService.GetDrugDrugInteractions(drugsSelected);
            }
            viewData.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

            return viewData;
        }

        public PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline)
        {
            IDictionary<string, NotesQuestion> garbageNotes = null;
            return GetCarePlanBySelectedEpisode(patientId, episodeId, discipline, out garbageNotes);
        }

        public PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline, out IDictionary<string, NotesQuestion> pocQuestions)
        {
            var pocEvents = patientRepository.GetVisitNotesByDisciplineTask(patientId, Current.AgencyId, discipline);
            foreach (var poc in pocEvents)
            {
                pocQuestions = poc.ToDictionary();
                if (pocQuestions.ContainsKey("SelectedEpisodeId") && pocQuestions["SelectedEpisodeId"] != null && pocQuestions["SelectedEpisodeId"].Answer == episodeId.ToString())
                {
                    return poc;
                }
            }
            pocQuestions = null;
            return null;
        }

        public void SetInsurance(Patient patient)
        {
            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                if (patient.PrimaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger())
            {
                if (patient.SecondaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger())
            {
                if (patient.TertiaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
        }

        public Guid GetPrimaryPhysicianId(Guid patientId, Guid agencyId)
        {
            var physicianId = Guid.Empty;
            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
            if (physician != null)
            {
                physicianId = physician.Id;
            }
            return physicianId;
        }

        #endregion

        #region Event Log

        public List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task)
        {
            var taskLogs = new List<TaskLog>();
            var taskAudit = logRepository.GetTaskAudit(Current.AgencyId, patientId, eventId, task);
            if (taskAudit != null && taskAudit.Log.IsNotNullOrEmpty())
            {
                var logs = taskAudit.Log.ToObject<List<TaskLog>>();
                if (logs != null && logs.Count > 0)
                {
                    var userIds = logs.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    logs.ForEach(log =>
                    {

                        if (!log.UserId.IsEmpty())
                        {
                            var user = users.FirstOrDefault(u => u.Id == log.UserId);
                            if (user != null)
                            {
                                log.UserName = user.DisplayName;
                            }
                        }
                        taskLogs.Add(log);
                    });
                }
            }
            return taskLogs;
        }

        public List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetGeneralAudits(Current.AgencyId, logDomain.ToString(), logType.ToString(), domainId, entityId.ToString());
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetMedicationAudits(Current.AgencyId, logDomain.ToString(), domainId);

            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        #endregion

        #region Private Methods

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");
            var questions = new Dictionary<string, NotesQuestion>();
            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }
                }
            }
            return questions.Values.ToList();
        }

        private static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        private string ProcessHospitalizationData(FormCollection formCollection)
        {
            formCollection.Remove("Id");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("UserId");
            formCollection.Remove("Templates");
            formCollection.Remove("M0903LastHomeVisitDate");
            formCollection.Remove("M0906DischargeDate");

            var questions = new List<Question>();
            foreach (var key in formCollection.AllKeys)
            {
                questions.Add(Question.Create(key, formCollection.GetValues(key).Join(",")));
            }
            return questions.ToXml();
        }

        private void ProcessScheduleEventsActions(Guid patientId, List<ScheduleEvent> scheduleEvents, PatientEpisode patientEpisode, string discipline)
        {
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = new List<Guid>();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        userIds.AddRange(returnUserIds);
                    }
                }
                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                {
                    userIds.AddRange(scheduleUserIds);
                }
                if (userIds != null && userIds.Count > 0)
                {
                    userIds = userIds.Distinct().ToList();
                }
                //var userIds = scheduleEvents.Where(s=>!s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();// userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);
                var eventIds = scheduleEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);
                var detail = new EpisodeDetail();
                if (patientEpisode.Details.IsNotNullOrEmpty())
                {
                    detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(s =>
                    {
                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                        s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
                        var hideTask = false;

                        if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
                        {
                            hideTask = true;

                            if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA)
                            {
                                hideTask = false;
                            }
                        }

                        if (!hideTask)
                        {
                            if (!s.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                if (user != null)
                                {
                                    s.UserName = user.DisplayName;
                                }
                            }

                            s.EpisodeNotes = detail.Comments.Clean();
                            if (s.IsMissedVisit)
                            {
                                var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId);
                                if (missedVisit != null)
                                {
                                    s.MissedVisitComments = missedVisit.ToString();
                                    s.Status = missedVisit.Status;
                                }
                            }
                            s.EventDate = s.EventDate;
                            s.PatientId = patientId;
                            s.EndDate = patientEpisode.EndDate;
                            s.StartDate = patientEpisode.StartDate;
                            Common.Url.SetNew(s, true, true,true,true);
                        }
                    });
                }
            }

        }

        private void PrecessScheduleEventForUsersOnly(List<ScheduleEvent> scheduleEvents)
        {
            var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
            var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();// userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);
            scheduleEvents.ForEach(evnt =>
            {
                var user = users.FirstOrDefault(u => u.Id == evnt.UserId);
                if (user != null)
                {
                    evnt.UserName = user.DisplayName;
                }
            });
        }

        private List<ScheduleEvent> ProcessScheduleEventsActions(List<PatientEpisode> patientEpisodes, List<ScheduleEvent> scheduleEvents, string discipline)
        {
            var episodeWithEventsDictionary = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.Key, g => g.ToList());//, g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != null && s.DisciplineTask > 0 && (discipline.IsEqual("All") ? true : s.Discipline.IsEqual(discipline))).ToList() : new List<ScheduleEvent>());
            if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            {
                var episodeIds = episodeWithEventsDictionary.Keys.ToList();
                var userIds = new List<Guid>();
                var users = new List<User>();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        userIds.AddRange(returnUserIds);
                    }
                }
                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                {
                    userIds.AddRange(scheduleUserIds);

                }
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();// userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                }
                var eventIds = scheduleEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

                episodeWithEventsDictionary.ForEach((key, value) =>
                  {
                      if (value != null && value.Count > 0)
                      {
                          var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
                          if (episode != null)
                          {
                              var detail = new EpisodeDetail();
                              if (episode.Details.IsNotNullOrEmpty())
                              {
                                  detail = episode.Details.ToObject<EpisodeDetail>();
                              }
                              value.ForEach(s =>
                              {
                                  var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
                                  s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
                                  var hideTask = false;

                                  if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
                                  {
                                      hideTask = true;

                                      if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA)
                                      {
                                          hideTask = false;
                                      }
                                  }

                                  if (!hideTask)
                                  {
                                      if (!s.UserId.IsEmpty())
                                      {
                                          var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                          if (user != null)
                                          {
                                              s.UserName = user.DisplayName;
                                          }
                                      }
                                      s.EpisodeNotes = detail.Comments.Clean();
                                      if (s.IsMissedVisit)
                                      {
                                          var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId);
                                          if (missedVisit != null)
                                          {
                                              s.MissedVisitComments = missedVisit.ToString();
                                              s.Status = missedVisit.Status;
                                          }
                                      }
                                      s.EventDate = s.EventDate;
                                      s.PatientId = episode.PatientId;
                                      s.EndDate = episode.EndDate;
                                      s.StartDate = episode.StartDate;
                                      Common.Url.SetNew(s, true, true, true, true);
                                      scheduleEvents.Add(s);
                                  }

                              });
                          }
                      }
                  });

            }
            return scheduleEvents;
        }

        private string GetFrequencyForCalendar(PatientEpisode episode, PatientEpisode previousEpisode)
        {
            var assessment = assessmentService.GetEpisodeAssessment(episode, previousEpisode);
            var frequencyData = this.GetFrequencyForAssessment(assessment);
            return frequencyData;
        }

        private string GetFrequencyForAssessment(Assessment assessment)
        {
            var frequencyData = string.Empty;
            if (assessment != null)
            {
                var assessmentQuestions = assessment.ToDictionary();
                var frequencyList = new List<string>();
                if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"].Answer != "") frequencyList.Add("SN Frequency:" + assessmentQuestions["485SNFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"].Answer != "") frequencyList.Add("PT Frequency:" + assessmentQuestions["485PTFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"].Answer != "") frequencyList.Add("OT Frequency:" + assessmentQuestions["485OTFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"].Answer != "") frequencyList.Add("ST Frequency:" + assessmentQuestions["485STFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"].Answer != "") frequencyList.Add("MSW Frequency:" + assessmentQuestions["485MSWFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"].Answer != "") frequencyList.Add("HHA Frequency:" + assessmentQuestions["485HHAFrequency"].Answer);
                if (frequencyList.Count > 0)
                {
                    frequencyData = frequencyList.ToArray().Join(", ") + ".";
                }
            }
            return frequencyData;
        }

        private bool UpdateEpisodeNew(PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (episode != null)
            {
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var patient = patientRepository.GetPatientOnly(episode.PatientId, Current.AgencyId);
                    if (!episode.AdmissionId.IsEmpty())
                    {
                        var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                        if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                        {
                            patient = admissinData.PatientData.ToObject<Patient>();
                        }
                    }
                    if (patient != null)
                    {
                        episode.StartOfCareDate = patient.StartofCareDate;
                        scheduleEvents.ForEach(ev =>
                        {
                            if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                            {
                                ProcessScheduleFactory(ev);
                                if (scheduleRepository.AddScheduleEvent(ev))
                                {
                                    if (ProcessSchedule(ev, patient, episode))
                                    {
                                        result = true;
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                                    }
                                    else
                                    {
                                        scheduleRepository.RemoveScheduleEventNew(ev);
                                    }
                                }
                            }
                        });
                    }
                }
            }
            return result;
        }

        private bool UpdateScheduleEntity(Guid eventId, Guid episodeId, Guid patientId, int task, bool isDeprecated)
        {
            bool result = false;
            switch (task)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    result = assessmentService.MarkAsDeleted(eventId, episodeId, patientId, ((DisciplineTasks)task).ToString(), isDeprecated);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.Labs:
                case (int)DisciplineTasks.InitialSummaryOfCare:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STReassessment:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPediatricAssessment:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                    result = patientRepository.MarkVisitNoteAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    result = patientRepository.MarkOrderAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.MarkPlanOfCareAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.HCFA485StandAlone:
                    result = assessmentService.MarkPlanOfCareStandAloneAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    result = agencyRepository.MarkIncidentAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    result = agencyRepository.MarkInfectionsAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, eventId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = patientRepository.DeleteFaceToFaceEncounter(Current.AgencyId, patientId, eventId, isDeprecated);
                    break;
            }
            return result;
        }

        private bool ReassignScheduleEntity(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int task)
        {
            bool result = false;
            switch (task)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    result = assessmentService.ReassignUser(episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.Labs:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.InitialSummaryOfCare:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPediatricAssessment:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:

                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STReassessment:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                    result = patientRepository.ReassignNotesUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    result = patientRepository.ReassignOrdersUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.ReassignPlanOfCaresUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    result = agencyRepository.ReassignIncidentUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    result = agencyRepository.ReassignInfectionsUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    result = patientRepository.ReassignCommunicationNoteUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = patientRepository.ReassignFaceToFaceEncounterUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
            }
            return result;
        }

        private string TableName( int task)
        {
            var result = string.Empty;
            switch (task)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    result = "oasisc.assessments";

                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.Labs:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.InitialSummaryOfCare:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPediatricAssessment:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:

                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STReassessment:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                    result = "agencymanagement.patientvisitnotes";
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    result = "agencymanagement.physicianorders";
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = "oasisc.planofcares";
                    break;
                case (int)DisciplineTasks.HCFA485StandAlone:
                    result = "oasisc.planofcarestandalones";
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    result = "agencymanagement.incidents";
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    result = "agencymanagement.infections";
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    result = "agencymanagement.communicationnotes";
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = "agencymanagement.facetofaceencounters";
                    break;
            }
            return result;
        }

        private bool ProcessEditDetail(ScheduleEvent schedule, Guid oldEpisodeId)
        {
            bool result = false;
            var type = schedule.DisciplineTask;
            switch (type)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                        || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview)
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady)
                            || schedule.Status == ((int)ScheduleStatus.OasisExported)
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported))
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPediatricAssessment:
                case (int)DisciplineTasks.Labs:
                case (int)DisciplineTasks.InitialSummaryOfCare:
                case (int)DisciplineTasks.SNPsychAssessment:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = ((DisciplineTasks)type).ToString();
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.STReassessment:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = ((DisciplineTasks)type).ToString() }); }
                                notesQuestions.Values.ForEach(q => { q.Type = ((DisciplineTasks)type).ToString(); });
                                visitNote.NoteType = ((DisciplineTasks)type).ToString();
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.OTSupervisoryVisit:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            physicianOrder.Status = schedule.Status;
                            if (schedule.VisitDate.IsValid())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate;
                            }
                            if (physicianOrder.Status != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule, oldEpisodeId);
                    break;
                case (int)DisciplineTasks.HCFA485StandAlone:
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        faceToFace.Status = schedule.Status;
                        if (faceToFace.Status != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate;
                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsValid())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate;
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            accidentReport.Status = schedule.Status;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsValid())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate;
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            infectionReport.Status = schedule.Status;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    {
                        var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (comNote != null)
                        {
                            comNote.IsDeprecated = schedule.IsDeprecated;
                            comNote.Status = schedule.Status;
                            if (schedule.VisitDate.IsValid())
                            {
                                comNote.Created = schedule.VisitDate;
                            }
                            if (comNote.Status != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    comNote.PhysicianData = physician.ToXml();
                                }
                            }
                            result = patientRepository.UpdateCommunicationNoteModal(comNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private bool ProcessEditDetailForReassign(ScheduleEvent schedule, Guid oldEpisodeId)
        {
            bool result = false;
            var type = schedule.DisciplineTask;
            switch (type)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview)
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady)
                            || schedule.Status == ((int)ScheduleStatus.OasisExported)
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported))
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = ((DisciplineTasks)type).ToString();
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STReassessment:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                notesQuestions.Values.ForEach(q => { q.Type = ((DisciplineTasks)type).ToString(); });
                                visitNote.NoteType = ((DisciplineTasks)type).ToString();
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = ((DisciplineTasks)type).ToString() }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                //case (int)DisciplineTasks.SNDischargeSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToString("MM/dd/yyyy"); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToString("MM/dd/yyyy"), Name = "VisitDate", Type = ((DisciplineTasks)type).ToString() }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            if (schedule.VisitDate.IsValid())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate;
                            }
                            physicianOrder.Status = schedule.Status;
                            if (physicianOrder.Status != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            physicianOrder.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule, oldEpisodeId);
                    break;
                case (int)DisciplineTasks.HCFA485StandAlone:
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        faceToFace.Status = schedule.Status;
                        if (faceToFace.Status != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate;
                        faceToFace.EpisodeId = schedule.EpisodeId;

                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsValid())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate;
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            accidentReport.EpisodeId = schedule.EpisodeId;
                            accidentReport.Status = schedule.Status;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsValid())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate;
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            infectionReport.EpisodeId = schedule.EpisodeId;
                            infectionReport.Status = schedule.Status;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    {
                        var communicationNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (communicationNote != null)
                        {
                            communicationNote.IsDeprecated = schedule.IsDeprecated;
                            if (schedule.VisitDate.IsValid())
                            {
                                communicationNote.Created = schedule.VisitDate;
                            }
                            communicationNote.Status = schedule.Status;
                            if (communicationNote.Status != schedule.Status && communicationNote.Status == (int)ScheduleStatus.NoteCompleted && !communicationNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(communicationNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    communicationNote.PhysicianData = physician.ToXml();
                                }
                            }
                            communicationNote.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateCommunicationNoteModal(communicationNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private void ProcessScheduleFactory(ScheduleEvent scheduleEvent)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
            {
                case DisciplineTasks.OASISCDeath:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargeOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOASISDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCFollowUp:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessmentRecert:
                case DisciplineTasks.NonOASISRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.NonOASISStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCTransferDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransfer:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.LVNSupervisoryVisit:
                case DisciplineTasks.HHAideSupervisoryVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PTEvaluation:
                case DisciplineTasks.PTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                //case DisciplineTasks.PTSupervisoryVisit:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.PT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    scheduleEvent.Version = 1;
                //    break;
                case DisciplineTasks.PTVisit:
                case DisciplineTasks.PTAVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.Version = 2;
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PTDischarge:
                case DisciplineTasks.PTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                //case DisciplineTasks.OTSupervisoryVisit:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.OT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    scheduleEvent.Version = 1;
                //    break;
                case DisciplineTasks.OTEvaluation:
                case DisciplineTasks.OTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                //case DisciplineTasks.OTReassessment:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.OT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    break;
                case DisciplineTasks.OTVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                case DisciplineTasks.OTDischarge:
                case DisciplineTasks.COTAVisit:
                case DisciplineTasks.OTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                case DisciplineTasks.STVisit:
                case DisciplineTasks.STDischarge:
                case DisciplineTasks.STMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.MSWEvaluationAssessment:
                case DisciplineTasks.MSWVisit:
                case DisciplineTasks.MSWDischarge:
                case DisciplineTasks.MSWAssessment:
                case DisciplineTasks.MSWProgressNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DriverOrTransportationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DieticianVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HHAideVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.HHAideCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PASVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PASCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.DischargeSummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PhysicianOrder:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485StandAlone:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOasisHCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA486:
                case DisciplineTasks.PostHospitalizationOrder:
                case DisciplineTasks.MedicaidPOC:
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    break;
                case DisciplineTasks.IncidentAccidentReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.InfectionReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SixtyDaySummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.TransferSummary:
                case DisciplineTasks.CoordinationOfCare:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.CommunicationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.FaceToFaceEncounter:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.UAPWoundCareVisit:
                case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
            }
        }

        private bool ProcessSchedule(ScheduleEvent scheduleEvent, Patient patient, PatientEpisode episode)
        {
            bool result = false;
            switch (scheduleEvent.DisciplineTask)
            {
                case (int)DisciplineTasks.OASISCDeath:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case (int)DisciplineTasks.OASISCDeathOT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case (int)DisciplineTasks.OASISCDeathPT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case (int)DisciplineTasks.OASISCDischarge:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Version = 2;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case (int)DisciplineTasks.OASISCDischargeOT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case (int)DisciplineTasks.OASISCDischargePT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case (int)DisciplineTasks.NonOASISDischarge:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisDischarge, episode);
                    break;

                case (int)DisciplineTasks.OASISCFollowUp:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case (int)DisciplineTasks.OASISCFollowupPT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case (int)DisciplineTasks.OASISCFollowupOT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case (int)DisciplineTasks.OASISCRecertification:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertification);
                    break;

                case (int)DisciplineTasks.OASISCRecertificationPT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationPT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationPT);
                    break;

                case (int)DisciplineTasks.OASISCRecertificationOT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationOT);
                    break;

                case (int)DisciplineTasks.SNAssessmentRecert:
                case (int)DisciplineTasks.NonOASISRecertification:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedNonOasisRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisRecertification, episode, currentMedNonOasisRecertification);
                    break;

                case (int)DisciplineTasks.OASISCResumptionofCare:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCStartofCare:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCStartofCarePT:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCStartofCareOT:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.NonOASISStartofCare:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedNonOasisStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisStartOfCare, episode, currentMedNonOasisStartofCare);
                    }
                    break;

                case (int)DisciplineTasks.OASISCTransferDischarge:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case (int)DisciplineTasks.OASISCTransfer:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case (int)DisciplineTasks.OASISCTransferPT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case (int)DisciplineTasks.OASISCTransferOT:
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    result = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.Labs:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNote);
                    break;
                case (int)DisciplineTasks.SNPediatricVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var SNPediatricVisitNote = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(SNPediatricVisitNote);
                    break;
                case (int)DisciplineTasks.SNPediatricAssessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var SNPediatricAssessment = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(SNPediatricAssessment);
                    break;
                case (int)DisciplineTasks.SNVPsychNurse:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 3;
                    var snPsychNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, Version = scheduleEvent.Version };
                    result = patientRepository.AddVisitNote(snPsychNote);
                    break;
                case (int)DisciplineTasks.SNPsychAssessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snPsychAssessment = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, Version = scheduleEvent.Version };
                    result = patientRepository.AddVisitNote(snPsychAssessment);
                    break;
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.InitialSummaryOfCare:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableNursing = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableNursing);
                    break;
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTReEvaluation:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var ptEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(ptEval);
                    break;
                case (int)DisciplineTasks.PTReassessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var ptReEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(ptReEval);
                    break;
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTAVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var ptVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(ptVisit);
                    break;
                case (int)DisciplineTasks.PTSupervisoryVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 1;
                    var ptSupVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(ptSupVisit);
                    break;
                case (int)DisciplineTasks.OTSupervisoryVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 1;
                    var otSupVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(otSupVisit);
                    break;
                case (int)DisciplineTasks.PTDischarge:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillablePT = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillablePT);
                    break;
                case (int)DisciplineTasks.PTMaintenance:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snNoteNonebillablePTM = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillablePTM);
                    break;
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var otEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(otEval);
                    break;
                case (int)DisciplineTasks.OTReassessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var otReassessment = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(otReassessment);
                    break;
                case (int)DisciplineTasks.OTVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snNoteNonebillableOTVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableOTVisit);
                    break;
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.OTMaintenance:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableOT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableOT);
                    break;
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STDischarge:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var stEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(stEval);
                    break;
                case (int)DisciplineTasks.STReassessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var stReassessment = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(stReassessment);
                    break;
                case (int)DisciplineTasks.STVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var stNote = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(stNote);
                    break;

                case (int)DisciplineTasks.STMaintenance:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableST = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableST);
                    break;
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableMSW = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableMSW);
                    break;
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableMSW2 = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableMSW2);
                    break;
                case (int)DisciplineTasks.DriverOrTransportationNote:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var driverOrTransportationNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(driverOrTransportationNote);
                    break;
                case (int)DisciplineTasks.DieticianVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableDietician = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableDietician);
                    break;
                case (int)DisciplineTasks.HHAideVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var hhAideVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(hhAideVisit);
                    break;
                case (int)DisciplineTasks.HHAideCarePlan:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var hhAideCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(hhAideCarePlan);
                    break;
                case (int)DisciplineTasks.PASTravel:
                case (int)DisciplineTasks.PASVisit:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var pasVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(pasVisit);
                    break;
                case (int)DisciplineTasks.PASCarePlan:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    var pasCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(pasCarePlan);
                    break;
                case (int)DisciplineTasks.HomeMakerNote:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var homeMakerNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(homeMakerNote);
                    break;
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.DischargeSummary:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                        //scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = false;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        var dischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, PhysicianId = physician != null ? physician.Id : Guid.Empty };
                        if (physician != null)
                        {
                            var questions = new List<NotesQuestion>();
                            questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                            dischargeSummary.Note = questions.ToXml();
                            result = patientRepository.AddVisitNote(dischargeSummary);
                        }
                        else
                        {
                            result = patientRepository.AddVisitNote(dischargeSummary);
                        }
                    }
                    break;
                //case "PTDischargeSummary":
                //    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                //    scheduleEvent.Discipline = Disciplines.PT.ToString();
                //    scheduleEvent.IsBillable = false;
                //    var PTphysician = physicianRepository.GetPatientPhysicians(patient.Id, Current.AgencyId).SingleOrDefault(p => p.Primary);
                //    var PTdischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    if (PTphysician != null)
                //    {
                //        var questions = new List<NotesQuestion>();
                //        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(PTphysician.Id), Type = "DischargeSummary" });
                //        scheduleEvent.Questions = questions;
                //        PTdischargeSummary.Note = questions.ToXml();
                //        patientRepository.AddVisitNote(PTdischargeSummary);
                //    }
                //    else
                //    {
                //        patientRepository.AddVisitNote(PTdischargeSummary);
                //    }
                //    break;

                case (int)DisciplineTasks.PhysicianOrder:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue);
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate = scheduleEvent.EventDate, Created = DateTime.Now, Text = "", Summary = "", Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            order.PhysicianId = physician.Id;
                        }
                        result = patientRepository.AddOrder(order);
                    }
                    break;

                case (int)DisciplineTasks.HCFA485StandAlone:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved);
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var planofCare = new PlanofCareStandAlone { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                        planofCare.Questions = new List<Question>();
                        planofCare.Data = planofCare.Questions.ToXml();
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            planofCare.PhysicianId = physician.Id;
                        }
                        result = planofCareRepository.AddStandAlone(planofCare);
                    }
                    break;

                case (int)DisciplineTasks.NonOasisHCFA485:
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved);
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    result = true;
                    break;

                case (int)DisciplineTasks.HCFA485:
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved);
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    result = true;
                    break;

                case (int)DisciplineTasks.HCFA486:
                case (int)DisciplineTasks.PostHospitalizationOrder:
                case (int)DisciplineTasks.MedicaidPOC:
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue);
                    result = true;
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated);
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var incidentReport = new Incident { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, IncidentDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    result = agencyRepository.AddIncident(incidentReport);
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated);
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var infectionReport = new Infection { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, InfectionDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    result = agencyRepository.AddInfection(infectionReport);
                    break;


                case (int)DisciplineTasks.SixtyDaySummary:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var sixtyDaySummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(sixtyDaySummary);
                    break;

                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var transferSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(transferSummary);
                    break;


                case (int)DisciplineTasks.CommunicationNote:
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue);
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var comNote = new CommunicationNote { Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, AgencyId = Current.AgencyId, UserId = scheduleEvent.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = scheduleEvent.EventDate, Modified = DateTime.Now };
                    result = patientRepository.AddCommunicationNote(comNote);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue);
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var faceToFaceEncounter = new FaceToFaceEncounter { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, RequestDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            faceToFaceEncounter.PhysicianId = physician.Id;
                        }
                        result = patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                    }
                    break;
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    var uapNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(uapNote);
                    break;

            }
            return result;
        }

        private List<VitalSign> VitalSignFromList(List<ScheduleEvent> scheduleEvents, List<int> disciplineTasksSNVList, List<int> disciplineTasksOASISList, List<int> disciplineTasksUAP, List<int> disciplineTasksTherapy)
        {
            var vitalSigns = new List<VitalSign>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    if (s.Note.IsNotNullOrEmpty())
                    {
                        if (disciplineTasksOASISList.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToOASISDictionary();// assessment.ToDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
                                var bs = new List<double>(); // var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSAM > 0)
                                {
                                    bs.Add(BSAM);
                                }
                                var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                if (BSNoon > 0)
                                {
                                    bs.Add(BSNoon);
                                }
                                var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSPM > 0)
                                {
                                    bs.Add(BSPM);
                                }
                                var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                if (BSHS > 0)
                                {
                                    bs.Add(BSHS);
                                }
                                var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                var user = users.SingleOrDefault(u => u.Id == s.UserId);
                                if (user != null)
                                {
                                    vitalSign.UserDisplayName = user.DisplayName;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                        else if (s.DisciplineTask == (int)DisciplineTasks.HHAideVisit || disciplineTasksUAP.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToVisitNoteDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.BPLying = "";
                                vitalSign.BPSitting = questions.ContainsKey("VitalSignBPVal") ? questions["VitalSignBPVal"].Answer : string.Empty;
                                vitalSign.BPStanding = "";
                                vitalSign.ApicalPulse = "";
                                vitalSign.RadialPulse = "";
                                vitalSign.PainLevel = "";
                                vitalSign.BSMax = "";
                                vitalSign.BSMin = "";
                                vitalSign.Weight = questions.ContainsKey("VitalSignWeightVal") ? questions["VitalSignWeightVal"].Answer : string.Empty;
                                vitalSign.Temp = questions.ContainsKey("VitalSignTempVal") ? questions["VitalSignTempVal"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("VitalSignRespVal") ? questions["VitalSignRespVal"].Answer : string.Empty;
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                        else if (disciplineTasksTherapy.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToVisitNoteDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null)
                            {
                                string SBP = questions.ContainsKey("GenericBloodPressure") ? questions["GenericBloodPressure"].Answer : string.Empty;
                                string DBP = questions.ContainsKey("GenericBloodPressurePer") ? questions["GenericBloodPressurePer"].Answer : string.Empty;

                                vitalSign.BPLying = "";
                                vitalSign.BPSitting = SBP.IsNotNullOrEmpty() && DBP.IsNotNullOrEmpty() ? string.Format("{0}/{1}", SBP, DBP) : string.Empty;
                                vitalSign.BPStanding = "";
                                vitalSign.ApicalPulse = "";
                                vitalSign.RadialPulse = "";
                                vitalSign.PainLevel = questions.ContainsKey("GenericPainLevel") ? questions["GenericPainLevel"].Answer : string.Empty;
                                vitalSign.OxygenSaturation = questions.ContainsKey("GenericO2Sat") ? questions["GenericO2Sat"].Answer : string.Empty;
                                vitalSign.BSMax = questions.ContainsKey("GenericBloodSugar") ? questions["GenericBloodSugar"].Answer : string.Empty;
                                vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }

                        else if (disciplineTasksSNVList.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToVisitNoteDictionary();//visitNote.ToDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;


                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");


                                var bs = new List<double>(); //new double[] { BSAM, BSNoon, BSPM, BSHS };
                                var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSAM > 0)
                                {
                                    bs.Add(BSAM);
                                }
                                var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                if (BSNoon > 0)
                                {
                                    bs.Add(BSNoon);
                                }
                                var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSPM > 0)
                                {
                                    bs.Add(BSPM);
                                }
                                var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                if (BSHS > 0)
                                {
                                    bs.Add(BSHS);
                                }
                                var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;// questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                    }
                });
            }
            return vitalSigns;
        }

        private void MergeVitalSignWithNote(PatientEpisode episode, DateTime eventDate, ref IDictionary<string, NotesQuestion> noteQuestions)
        {
            noteQuestions = noteQuestions ?? new Dictionary<string, NotesQuestion>();
            var vitalSigns = this.GetVitalSignsForSixtyDaySummary(episode, eventDate);
            if (vitalSigns != null && vitalSigns.Count > 0)
            {
                #region BP

                var bp = new List<string>();
                var bpSysLow = int.MinValue;
                var bpSysHigh = int.MaxValue;
                var bpDiaLow = int.MinValue;
                var bpDiaHigh = int.MaxValue;

                var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty()).Select(v => v.BPSittingLeft).ToList();
                var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty()).Select(v => v.BPSittingRight).ToList();
                var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty()).Select(v => v.BPStandingLeft).ToList();
                var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty()).Select(v => v.BPStandingRight).ToList();
                var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty()).Select(v => v.BPLyingLeft).ToList();
                var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty()).Select(v => v.BPLyingRight).ToList();

                if (bpSitLeft != null && bpSitLeft.Count > 0) bp.AddRange(bpSitLeft.AsEnumerable());
                if (bpSitRight != null && bpSitRight.Count > 0) bp.AddRange(bpSitRight.AsEnumerable());
                if (bpStandLeft != null && bpStandLeft.Count > 0) bp.AddRange(bpStandLeft.AsEnumerable());
                if (bpStandRight != null && bpStandRight.Count > 0) bp.AddRange(bpStandRight.AsEnumerable());
                if (bpLyLeft != null && bpLyLeft.Count > 0) bp.AddRange(bpLyLeft.AsEnumerable());
                if (bpLyRight != null && bpLyRight.Count > 0) bp.AddRange(bpLyRight.AsEnumerable());

                if (bp != null && bp.Count > 0)
                {
                    bpSysLow = bp.Select(v =>
                    {
                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (min != null && min.Length > 1)
                        {
                            if (min[0].IsInteger())
                            {
                                return min[0].ToInteger();
                            }
                        }
                        return int.MinValue;
                    }).Min();

                    bpSysHigh = bp.Select(v =>
                    {
                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (max != null && max.Length > 1)
                        {
                            if (max[0].IsInteger())
                            {
                                return max[0].ToInteger();
                            }
                        }
                        return int.MaxValue;
                    }).Max();

                    bpDiaLow = bp.Select(v =>
                    {
                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (min != null && min.Length > 1)
                        {
                            if (min[1].IsInteger())
                            {
                                return min[1].ToInteger();
                            }
                        }
                        return int.MinValue;
                    }).Min();

                    bpDiaHigh = bp.Select(v =>
                    {
                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (max != null && max.Length > 1)
                        {
                            if (max[1].IsInteger())
                            {
                                return max[1].ToInteger();
                            }
                        }
                        return int.MaxValue;
                    }).Max();
                }

                if (noteQuestions.ContainsKey("VitalSignBPMax"))
                {
                    noteQuestions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPMin"))
                {
                    noteQuestions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPDiaMax"))
                {
                    noteQuestions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPDiaMin"))
                {
                    noteQuestions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty });
                }

                #endregion

                #region HR

                var apicalPulseMax = int.MinValue;
                var apicalPulseMin = int.MaxValue;
                var apicalPulse = vitalSigns.Where(v => v.ApicalPulse.IsNotNullOrEmpty() && v.ApicalPulse.IsInteger()).Select(v => v.ApicalPulse.ToInteger()).ToList();
                if (apicalPulse != null && apicalPulse.Count > 0)
                {
                    apicalPulseMax = apicalPulse.Max();
                    apicalPulseMin = apicalPulse.Min();
                }

                var radialPulseMax = int.MinValue;
                var radialPulseMin = int.MaxValue;
                var radialPulse = vitalSigns.Where(v => v.RadialPulse.IsNotNullOrEmpty() && v.RadialPulse.IsInteger()).Select(v => v.RadialPulse.ToInteger()).ToList();
                if (radialPulse != null && radialPulse.Count > 0)
                {
                    radialPulseMax = radialPulse.Max();
                    radialPulseMin = radialPulse.Min();
                }

                var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                if (noteQuestions.ContainsKey("VitalSignHRMax"))
                {
                    noteQuestions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                }

                var minHR = Math.Min(apicalPulseMin, radialPulseMin);

                if (noteQuestions.ContainsKey("VitalSignHRMin"))
                {
                    noteQuestions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                }
                #endregion

                #region Resp

                var respMax = int.MaxValue;
                var respMin = int.MinValue;
                var resp = vitalSigns.Where(v => v.Resp.IsNotNullOrEmpty() && v.Resp.IsInteger()).Select(v => v.Resp.ToInteger()).ToList();
                if (resp != null && resp.Count > 0)
                {
                    respMin = resp.Min();
                    respMax = resp.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignRespMax"))
                {
                    noteQuestions["VitalSignRespMax"].Answer = respMax != int.MaxValue ? respMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != int.MaxValue ? respMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignRespMin"))
                {
                    noteQuestions["VitalSignRespMin"].Answer = respMin != int.MinValue ? respMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != int.MinValue ? respMin.ToString() : "" });
                }

                #endregion

                #region Temp

                var tempMax = double.MaxValue;
                var tempMin = double.MinValue;
                var temp = vitalSigns.Where(v => v.Temp.IsNotNullOrEmpty() && v.Temp.IsDouble()).Select(v => v.Temp.ToDouble()).ToList();
                if (temp != null && temp.Count > 0)
                {
                    tempMax = temp.Max();
                    tempMin = temp.Min();
                }

                if (noteQuestions.ContainsKey("VitalSignTempMax"))
                {
                    noteQuestions["VitalSignTempMax"].Answer = tempMax != double.MaxValue ? tempMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue ? tempMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignTempMin"))
                {
                    noteQuestions["VitalSignTempMin"].Answer = tempMin != double.MinValue ? tempMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue ? tempMin.ToString() : "" });
                }

                #endregion

                #region BS

                var bsMax = int.MaxValue;
                var bsMin = int.MinValue;
                var bsAllMax = vitalSigns.Where(v => v.BSMax.IsNotNullOrEmpty() && v.BSMax.IsInteger() && v.BSMax.ToInteger() > 0).Select(v => v.BSMax.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                var bsAllMin = vitalSigns.Where(v => v.BSMin.IsNotNullOrEmpty() && v.BSMin.IsInteger() && v.BSMin.ToInteger() > 0).Select(v => v.BSMin.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                if (bsAllMax != null && bsAllMax.Count > 0)
                {
                    bsMax = bsAllMax.Max();
                }
                if (bsAllMin != null && bsAllMin.Count > 0)
                {
                    bsMin = bsAllMin.Min();
                }

                if (noteQuestions.ContainsKey("VitalSignBGMax"))
                {
                    noteQuestions["VitalSignBGMax"].Answer = bsMax != int.MaxValue ? bsMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != int.MaxValue ? bsMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignBGMin"))
                {
                    noteQuestions["VitalSignBGMin"].Answer = bsMin != int.MinValue ? bsMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != int.MinValue ? bsMin.ToString() : "" });
                }
                #endregion

                #region Weight

                var weightMax = double.MaxValue;
                var weightMin = double.MinValue;
                var weight = vitalSigns.Where(v => v.Weight.IsNotNullOrEmpty() && v.Weight.IsDouble()).Select(v => v.Weight.ToDouble()).ToList();
                if (weight != null && weight.Count > 0)
                {
                    weightMin = weight.Min();
                    weightMax = weight.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignWeightMax"))
                {
                    noteQuestions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue ? weightMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue ? weightMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignWeightMin"))
                {
                    noteQuestions["VitalSignWeightMin"].Answer = weightMin != double.MinValue ? weightMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue ? weightMin.ToString() : "" });
                }

                #endregion

                #region Pain

                var painMax = int.MaxValue;
                var painMin = int.MinValue;
                var pain = vitalSigns.Where(v => v.PainLevel.IsNotNullOrEmpty() && v.PainLevel.IsInteger()).Select(v => v.PainLevel.ToInteger()).ToList();
                if (pain != null && pain.Count > 0)
                {
                    painMin = pain.Min();
                    painMax = pain.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignPainMax"))
                {
                    noteQuestions["VitalSignPainMax"].Answer = painMax != int.MaxValue ? painMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue ? painMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignPainMin"))
                {
                    noteQuestions["VitalSignPainMin"].Answer = painMin != int.MinValue ? painMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue ? painMin.ToString() : "" });
                }

                #endregion
            }
        }

        private Guid GetPhysicianFromNotes(int task, IDictionary<string, NotesQuestion> questions)
        {
            var physicianId = Guid.Empty;


            NotesQuestion physicianQuestion = null;
            if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == task))
            {
                if (questions.TryGetValue("PhysicianId", out physicianQuestion))
                {
                    if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                    {
                        physicianId = physicianQuestion.Answer.ToGuid();
                    }
                }
            }
            else
            {
                if (questions.TryGetValue("Physician", out physicianQuestion))
                {
                    if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                    {
                        physicianId = physicianQuestion.Answer.ToGuid();
                    }
                }
            }
            return physicianId;
        }

        private IDictionary<string, NotesQuestion> GetDiagnosisMergedFromOASIS(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            NotesQuestion primaryDiagnosis = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis", out primaryDiagnosis))
            {
                if (primaryDiagnosis != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis"))
                    {
                        noteQuestions["PrimaryDiagnosis"].Answer = primaryDiagnosis != null ? primaryDiagnosis.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis", primaryDiagnosis);
                    }
                }
            }

            NotesQuestion ICD9M = null;
            if (oasisQuestions.TryGetValue("ICD9M", out ICD9M))
            {
                if (ICD9M != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M"))
                    {
                        noteQuestions["ICD9M"].Answer = ICD9M != null ? ICD9M.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M", ICD9M);
                    }
                }
            }

            NotesQuestion primaryDiagnosis1 = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis1", out primaryDiagnosis1))
            {
                if (primaryDiagnosis1 != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                    {
                        noteQuestions["PrimaryDiagnosis1"].Answer = primaryDiagnosis1 != null ? primaryDiagnosis1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis1", primaryDiagnosis1);
                    }
                }
            }

            NotesQuestion ICD9M1 = null;
            if (oasisQuestions.TryGetValue("ICD9M1", out ICD9M1))
            {
                if (ICD9M1 != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M1"))
                    {
                        noteQuestions["ICD9M1"].Answer = ICD9M1 != null ? ICD9M1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M1", ICD9M1);
                    }
                }
            }

            return noteQuestions;
        }

        private VisitNoteViewData SkilledNurseVisitContent(ScheduleEvent scheduleEvent, PatientVisitNote previousNote, VisitNoteViewData viewData)
        {
            viewData.IsWoundCareExist = previousNote.IsWoundCare;
            viewData.IsSupplyExist = previousNote.IsSupplyExist;

            var noteItems = previousNote.ToDictionary();
            var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
            nameArray.ForEach(name => { noteItems.Remove(name); });
            viewData.Questions = noteItems;
            var currentNote = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EventId);
            if (currentNote != null)
            {
                currentNote.Questions = noteItems.Values.ToList();
                currentNote.Note = currentNote.Questions.ToXml();
                currentNote.IsWoundCare = previousNote.IsWoundCare;
                currentNote.WoundNote = previousNote.WoundNote;
                currentNote.Modified = DateTime.Now;

                var oldStatus = scheduleEvent.Status;
                scheduleEvent.Status = (int)ScheduleStatus.NoteSaved;
                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                {
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.LoadPreviousNote, (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                    }
                    else
                    {
                        scheduleEvent.Status = oldStatus;
                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
                    }
                }
            }
            return viewData;
        }

        private void HHAVisitNoteMerger(Guid patientId, Guid episodeId, ref IDictionary<string, NotesQuestion> noteQuestions)
        {
            IDictionary<string, NotesQuestion> pocQuestions = null;
            var pocEvent = this.GetCarePlanBySelectedEpisode(patientId, episodeId, DisciplineTasks.HHAideCarePlan, out pocQuestions);
            if (pocEvent != null)
            {
                if (pocQuestions.ContainsKey("HHAFrequency"))
                {
                    noteQuestions.Add("HHAFrequency", pocQuestions["HHAFrequency"]);
                }
                if (pocQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                }
                if (pocQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", pocQuestions["ICD9M"]);
                }
                if (pocQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                }
                if (pocQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", pocQuestions["ICD9M1"]);
                }
                if (pocQuestions.ContainsKey("DNR"))
                {
                    noteQuestions.Add("DNR", pocQuestions["DNR"]);
                }
                if (pocQuestions.ContainsKey("DiastolicBPGreaterThan"))
                {
                    noteQuestions.Add("DiastolicBPGreaterThan", pocQuestions["DiastolicBPGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("SystolicBPGreaterThan"))
                {
                    noteQuestions.Add("SystolicBPGreaterThan", pocQuestions["SystolicBPGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("PulseGreaterThan"))
                {
                    noteQuestions.Add("PulseGreaterThan", pocQuestions["PulseGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("RespirationGreaterThan"))
                {
                    noteQuestions.Add("RespirationGreaterThan", pocQuestions["RespirationGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("TempGreaterThan"))
                {
                    noteQuestions.Add("TempGreaterThan", pocQuestions["TempGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("WeightGreaterThan"))
                {
                    noteQuestions.Add("WeightGreaterThan", pocQuestions["WeightGreaterThan"]);
                }
                if (pocQuestions.ContainsKey("DiastolicBPLessThan"))
                {
                    noteQuestions.Add("DiastolicBPLessThan", pocQuestions["DiastolicBPLessThan"]);
                }
                if (pocQuestions.ContainsKey("SystolicBPLessThan"))
                {
                    noteQuestions.Add("SystolicBPLessThan", pocQuestions["SystolicBPLessThan"]);
                }
                if (pocQuestions.ContainsKey("PulseLessThan"))
                {
                    noteQuestions.Add("PulseLessThan", pocQuestions["PulseLessThan"]);
                }
                if (pocQuestions.ContainsKey("RespirationLessThan"))
                {
                    noteQuestions.Add("RespirationLessThan", pocQuestions["RespirationLessThan"]);
                }
                if (pocQuestions.ContainsKey("TempLessThan"))
                {
                    noteQuestions.Add("TempLessThan", pocQuestions["TempLessThan"]);
                }
                if (pocQuestions.ContainsKey("WeightLessThan"))
                {
                    noteQuestions.Add("WeightLessThan", pocQuestions["WeightLessThan"]);
                }
                // viewData.Questions = noteQuestions;
            }

        }

        private void PASVisitNoteMerger(Guid patientId, Guid episodeId, ref IDictionary<string, NotesQuestion> noteQuestions)
        {
            var pocEvent = patientRepository.GetVisitNoteByType(episodeId, patientId, DisciplineTasks.PASCarePlan);
            if (pocEvent != null)
            {
                var pocQuestions = pocEvent.ToDictionary();
                if (pocQuestions != null && pocQuestions.Count > 0)
                {
                    if (pocQuestions.ContainsKey("DiastolicBPGreaterThan"))
                    {
                        noteQuestions.Add("DiastolicBPGreaterThan", pocQuestions["DiastolicBPGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("SystolicBPGreaterThan"))
                    {
                        noteQuestions.Add("SystolicBPGreaterThan", pocQuestions["SystolicBPGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("PulseGreaterThan"))
                    {
                        noteQuestions.Add("PulseGreaterThan", pocQuestions["PulseGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("RespirationGreaterThan"))
                    {
                        noteQuestions.Add("RespirationGreaterThan", pocQuestions["RespirationGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("TempGreaterThan"))
                    {
                        noteQuestions.Add("TempGreaterThan", pocQuestions["TempGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("WeightGreaterThan"))
                    {
                        noteQuestions.Add("WeightGreaterThan", pocQuestions["WeightGreaterThan"]);
                    }

                    if (pocQuestions.ContainsKey("DiastolicBPLessThan"))
                    {
                        noteQuestions.Add("DiastolicBPLessThan", pocQuestions["DiastolicBPLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("SystolicBPLessThan"))
                    {
                        noteQuestions.Add("SystolicBPLessThan", pocQuestions["SystolicBPLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("PulseLessThan"))
                    {
                        noteQuestions.Add("PulseLessThan", pocQuestions["PulseLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("RespirationLessThan"))
                    {
                        noteQuestions.Add("RespirationLessThan", pocQuestions["RespirationLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("TempLessThan"))
                    {
                        noteQuestions.Add("TempLessThan", pocQuestions["TempLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("WeightLessThan"))
                    {
                        noteQuestions.Add("WeightLessThan", pocQuestions["WeightLessThan"]);
                    }

                    if (pocQuestions.ContainsKey("PrimaryDiagnosis"))
                    {
                        noteQuestions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                    }
                    if (pocQuestions.ContainsKey("ICD9M"))
                    {
                        noteQuestions.Add("ICD9M", pocQuestions["ICD9M"]);
                    }

                    if (pocQuestions.ContainsKey("PrimaryDiagnosis1"))
                    {
                        noteQuestions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                    }
                    if (pocQuestions.ContainsKey("ICD9M1"))
                    {
                        noteQuestions.Add("ICD9M1", pocQuestions["ICD9M1"]);
                    }
                    if (pocQuestions.ContainsKey("DNR"))
                    {
                        noteQuestions.Add("DNR", pocQuestions["DNR"]);
                    }
                    if (pocQuestions.ContainsKey("IsVitalSignParameter"))
                    {
                        noteQuestions.Add("IsVitalSignParameter", pocQuestions["IsVitalSignParameter"]);
                    }
                }
                //viewData.Questions = noteQuestions;
            }
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }
       
        #endregion
    }
}
