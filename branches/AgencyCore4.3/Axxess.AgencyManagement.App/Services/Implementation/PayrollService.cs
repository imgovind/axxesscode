﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using ViewData;
    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Common;

    public class PayrollService : IPayrollService
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IScheduleRepository scheduleRepository;

        public PayrollService(IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
        }

        public bool MarkAsPaid(List<Guid> itemList)
        {
            var result = false;

            if (itemList != null && itemList.Count > 0)
            {
                if (scheduleRepository.ToggleScheduledEventsPaid(Current.AgencyId, itemList, true))
                {
                    result = true;
                    Auditor.MultiLogOnlyUpdate(Current.AgencyId, itemList, Actions.MarkedPaid, ScheduleStatus.NoStatus, "This visit is marked as paid.");
                }
            }
            return result;
        }

        public bool MarkAsUnpaid(List<Guid> itemList)
        {
            var result = false;

            if (itemList != null && itemList.Count > 0)
            {
                if (scheduleRepository.ToggleScheduledEventsPaid(Current.AgencyId, itemList, false))
                {
                    result = true;
                    Auditor.MultiLogOnlyUpdate(Current.AgencyId, itemList, Actions.MarkedUnpaid, ScheduleStatus.NoStatus, "The visit is marked as unpaid.");
                }
            }
            return result;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status)
        {
            var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
            var list = scheduleRepository.GetPayrollSummmaryLean(Current.AgencyId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
            if (list != null && list.Count > 0)
            {
                var userIds = list.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                list.ForEach(p =>
                {
                    var user = users.SingleOrDefault(u => u.Id == p.UserId);
                    if (user != null)
                    {
                        p.UserName = user.DisplayName;
                    }
                });
            }
            return list;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status) {
            var list = new List<PayrollDetail>();
            if (Status.IsEqual("false") || Status.IsEqual("true"))
            {
                var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
                var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                var payrollVisits = scheduleRepository.GetPayrollSummmaryVisits(Current.AgencyId, Guid.Empty, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
                if (payrollVisits != null && payrollVisits.Count > 0)
                {
                    var userIds = payrollVisits.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (userIds != null && userIds.Count > 0)
                    {
                        var users = UserEngine.GetUsers(Current.AgencyId, userIds)?? new List<User>();
                        userIds.ForEach(u =>
                        {
                            var user = users.SingleOrDefault(us => us.Id == u);
                            list.Add(new PayrollDetail
                            {
                                Id = u,
                                Name = user != null ? user.DisplayName : string.Empty,
                                Visits = payrollVisits.Where(p => p.UserId == u).ToList(),
                                StartDate = startDate,
                                EndDate = endDate,
                                PayrollStatus = Status
                            }
                            );
                        });
                    }
                }
            }
            return list;
        }

        public PayrollDetail GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            var payrollDetail = new PayrollDetail();
            if (!userId.IsEmpty())
            {
                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
                if (user != null)
                {
                    payrollDetail.Id = userId;
                    payrollDetail.Name = user.DisplayName;
                    payrollDetail.StartDate = startDate;
                    payrollDetail.EndDate = endDate;
                    payrollDetail.PayrollStatus = Status;
                    var userVisits = new List<UserVisit>();
                    if (Status.IsEqual("false") || Status.IsEqual("true"))
                    {
                        var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
                        var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                        userVisits = scheduleRepository.GetPayrollSummmaryVisits(Current.AgencyId, userId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
                        if (userVisits != null && userVisits.Count > 0)
                        {
                            var userRates = user.Rates.IsNotNullOrEmpty() ? user.Rates.ToObject<List<UserRate>>() : new List<UserRate>();
                            userVisits.ForEach(uv =>
                            {
                                var detail = uv.Details.ToObject<EpisodeDetail>();
                                var userRate = new UserRate();
                                if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                {
                                    userRate = userRates.FirstOrDefault(r => r.Insurance == detail.PrimaryInsurance && r.Id == uv.DisciplineTask);
                                }
                                uv.VisitRate = userRate != null ? userRate.ToString() : "none";
                                uv.MileageRate = userRate != null && userRate.MileageRate > 0 ? string.Format("${0:#0.00} / mile", userRate.MileageRate) : "0";
                                uv.Surcharge = uv.Surcharge.IsNotNullOrEmpty() && uv.Surcharge.IsDouble() ? string.Format("${0:#0.00}", uv.Surcharge.ToDouble()) : "0";

                                var visitPay = GetVisitPayment(uv, userRate);
                                uv.VisitPayment = string.Format("${0:#0.00}", visitPay);

                                uv.Total = uv.Surcharge.IsNotNullOrEmpty() && uv.Surcharge.IsDouble() ? uv.Surcharge.ToDouble() + visitPay : visitPay;
                                uv.TotalPayment = string.Format("${0:#0.00}", uv.Total);


                            });
                        }
                    }
                    payrollDetail.Visits = userVisits;
                }
            }

            return payrollDetail;
        }

        private static double GetVisitPayment(UserVisit visit, UserRate rate)
        {
            var total = 0.0;
            var hours = (double)visit.MinSpent / 60;
            if (visit != null && rate != null)
            {
                if (rate.Rate > 0)
                {
                    if (rate.RateType == (int)UserRateTypes.Hourly)
                    {
                        total = rate.Rate * hours;
                    }
                    else if (rate.RateType == (int)UserRateTypes.PerVisit)
                    {
                        total = rate.Rate;
                    }
                }
                if (visit.AssociatedMileage.IsNotNullOrEmpty() && visit.AssociatedMileage.IsDouble() && rate.MileageRate > 0)
                {
                    total += visit.AssociatedMileage.ToDouble() * rate.MileageRate;
                }
            }
            return total;
        }
    }
}
