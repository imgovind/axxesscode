﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;

    public interface IPayrollService
    {
        bool MarkAsPaid(List<Guid> itemList);
        bool MarkAsUnpaid(List<Guid> itemList);
        List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status);
        List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status);
        PayrollDetail GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status);
    }
}
