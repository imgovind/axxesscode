﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public interface IUserService
    {
        bool CreateUser(User user);
        bool DeleteUser(Guid userId);
        List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to);
        List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to);
        IList<UserVisitWidget> GetScheduleWidget(Guid userId, DateTime from, DateTime to);
        bool IsEmailAddressInUse(string emailAddress);

        bool UpdateProfile(User user);
        bool IsPasswordCorrect(Guid userId, string password);
        bool IsSignatureCorrect(Guid userId, string signature);
        bool LoadUserRate(Guid fromId, Guid toId);
        bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles);
        bool UpdatePermissions(FormCollection formCollection);
        bool DeleteLicense(Guid Id, Guid userId);
        bool UpdateLicense(Guid Id, Guid userId, DateTime expirationDate, string LicenseNumber);
        bool UpdateLicense(Guid Id, Guid userId, DateTime initiationDate, DateTime expirationDate);

        IList<LicenseItem> GetUserLicenses();
        IList<License> GetUserLicenses(Guid branchId, int status);
        bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles);

       IList<UserRate> GetUserRates(Guid userId);
       IList<User> GetUsersByStatus(Guid branchId, int status);
    }
}
