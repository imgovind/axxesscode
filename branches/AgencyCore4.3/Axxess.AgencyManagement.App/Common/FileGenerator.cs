﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net;
    using System.IO;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.iTextExtension;
    using System.Web.Mvc;
    using System.Web;

    public static class FileGenerator
    {
        public static string RemoteDownload(string serverIP, string remoteDir, string file)
        {
            string result = string.Empty;
            try
            {
                if (serverIP.IsNotNullOrEmpty() && remoteDir.IsNotNullOrEmpty() && file.IsNotNullOrEmpty())
                {
                    string uri = string.Format("ftp://{0}/{1}/{2}", serverIP, remoteDir, file);
                    Uri serverUri = new Uri(uri);
                    if (serverUri.Scheme != Uri.UriSchemeFtp)
                    {
                        return result;
                    }
                    FtpWebRequest reqFTP;
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(serverUri);
                    reqFTP.Credentials = new NetworkCredential("AxxessAdmin", "29Eiyztygt");
                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.Proxy = null;
                    reqFTP.UsePassive = false;
                    FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    var readStream = new StreamReader(responseStream);
                    result = readStream.ReadToEnd();
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Download Error");
            }
            return result;
        }

        public static FileResult Pdf<T>(T inputDocument, string fileName) where T : AxxessPdf
        {
            var file = new FileStreamResult(new MemoryStream(), "application/pdf");
            try
            {
                if (inputDocument != null)
                {
                    var stream = inputDocument.GetStream();
                    if (stream != null)
                    {
                        stream.Position = 0;
                        file =   new FileStreamResult(stream, "application/pdf");
                        file.FileDownloadName = string.Format("{0}_{1}.pdf", fileName, DateTime.Now.Ticks.ToString());

                    }
                }
            }
            catch (Exception ex)
            {
                return file;
            }
            return file;
        }

        public static FileResult PlainText(string inputDocument, string fileName)
        {
            var fileStream = new MemoryStream();
            var file = new FileStreamResult(fileStream, "Text/Plain");
            try
            {
                if (inputDocument.IsNotNullOrEmpty() && inputDocument.Length > 0)
                {
                    var encoding = new UTF8Encoding();
                    byte[] buffer = encoding.GetBytes(inputDocument);
                    fileStream.Write(buffer, 0, inputDocument.Length);
                    fileStream.Position = 0;
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}{1}.txt", fileName, DateTime.Now.ToString("MMddyyyy")));
                    file = new FileStreamResult(fileStream, "Text/Plain");
                }
            }
            catch (Exception ex)
            {
                return file;
            }
            return file;
        }




    }
}
