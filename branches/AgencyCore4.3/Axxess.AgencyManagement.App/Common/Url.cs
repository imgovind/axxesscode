﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Enums;
    using Services;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Enums;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Common;

    public static class Url
    {
        //public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        //{
        //    if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
        //    {
        //        string onclick = string.Empty;
        //        string reopenUrl = string.Empty;
        //        string printUrl = Print(scheduleEvent, usePrintIcon);

        //        string detailUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskDetails('{0}', '{1}', '{2}');\">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //        string deleteUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}','{4}');\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.UserId, scheduleEvent.DisciplineTask);
        //        string reassignUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this), '{0}','{1}','{2}','{3}');\" class=\"reassign\">Reassign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.UserId);
        //        string oasisProfileUrl = string.Empty;
        //        DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

        //        switch (task)
        //        {
        //            case DisciplineTasks.OASISCStartofCare:
        //            case DisciplineTasks.OASISCStartofCarePT:
        //            case DisciplineTasks.OASISCStartofCareOT:
        //                onclick = string.Format("StartOfCare.Load('{0}','{1}','StartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                if (ScheduleStatusFactory.OASISCompleted(true).Contains(scheduleEvent.Status))
        //                {
        //                    oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
        //                        "Acore.OpenPrintView({ " +
        //                            "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.StartOfCare.ToString() + "'," +
        //                            "PdfUrl: 'Oasis/OasisProfilePdf'," +
        //                            "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.StartOfCare.ToString() + "' }" +
        //                        "})\"><span class=\"img icon money\"></span></a>";
        //                }
        //                break;
        //            case DisciplineTasks.SNAssessment:
        //            case DisciplineTasks.NonOASISStartofCare:
        //                onclick = string.Format("NonOasisStartOfCare.Load('{0}','{1}','NonOasisStartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.OASISCResumptionofCare:
        //            case DisciplineTasks.OASISCResumptionofCarePT:
        //            case DisciplineTasks.OASISCResumptionofCareOT:
        //                onclick = string.Format("ResumptionOfCare.Load('{0}','{1}','ResumptionOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                if (ScheduleStatusFactory.OASISCompleted(true).Contains(scheduleEvent.Status))
        //                {
        //                    oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
        //                        "Acore.OpenPrintView({ " +
        //                            "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.ResumptionOfCare.ToString() + "'," +
        //                            "PdfUrl: 'Oasis/OasisProfilePdf'," +
        //                            "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.ResumptionOfCare.ToString() + "' }" +
        //                        "})\"><span class=\"img icon money\"></span></a>";
        //                }
        //                break;
        //            case DisciplineTasks.OASISCFollowUp:
        //            case DisciplineTasks.OASISCFollowupPT:
        //            case DisciplineTasks.OASISCFollowupOT:
        //                onclick = string.Format("FollowUp.Load('{0}','{1}','FollowUp');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

        //                break;
        //            case DisciplineTasks.OASISCRecertification:
        //            case DisciplineTasks.OASISCRecertificationPT:
        //            case DisciplineTasks.OASISCRecertificationOT:
        //                onclick = string.Format("Recertification.Load('{0}','{1}','Recertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                if (ScheduleStatusFactory.OASISCompleted(true).Contains(scheduleEvent.Status))
        //                {
        //                    oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
        //                        "Acore.OpenPrintView({ " +
        //                            "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.Recertification.ToString() + "'," +
        //                            "PdfUrl: 'Oasis/OasisProfilePdf'," +
        //                            "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.Recertification.ToString() + "' }" +
        //                        "})\"><span class=\"img icon money\"></span></a>";
        //                }
        //                break;
        //            case DisciplineTasks.SNAssessmentRecert:
        //            case DisciplineTasks.NonOASISRecertification:
        //                onclick = string.Format("NonOasisRecertification.Load('{0}','{1}','NonOasisRecertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.OASISCTransfer:
        //            case DisciplineTasks.OASISCTransferPT:
        //            case DisciplineTasks.OASISCTransferOT:
        //                onclick = string.Format("TransferInPatientNotDischarged.Load('{0}','{1}','TransferInPatientNotDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

        //                break;
        //            case DisciplineTasks.OASISCTransferDischarge:
        //            case DisciplineTasks.OASISCTransferDischargePT:
        //                onclick = string.Format("TransferInPatientDischarged.Load('{0}','{1}','TransferInPatientDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

        //                break;
        //            case DisciplineTasks.OASISCDeath:
        //            case DisciplineTasks.OASISCDeathPT:
        //            case DisciplineTasks.OASISCDeathOT:
        //                onclick = string.Format("DischargeFromAgencyDeath.Load('{0}','{1}','DischargeFromAgencyDeath');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

        //                break;
        //            case DisciplineTasks.OASISCDischarge:
        //            case DisciplineTasks.OASISCDischargePT:
        //            case DisciplineTasks.OASISCDischargeOT:
        //                onclick = string.Format("DischargeFromAgency.Load('{0}','{1}','DischargeFromAgency');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.NonOASISDischarge:
        //                onclick = string.Format("NonOasisDischarge.Load('{0}','{1}','NonOasisDischarge');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.PhysicianOrder:
        //                onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.FaceToFaceEncounter:
        //                // onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
        //                scheduleEvent.Url = string.Format("{0}", scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.HCFA485:
        //                onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.HCFA485StandAlone:
        //                onclick = string.Format("UserInterface.ShowEditPlanofCareStandAlone('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.NonOasisHCFA485:
        //                onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.CommunicationNote:
        //                onclick = string.Format("Patient.LoadEditCommunicationNote('{0}','{1}');", scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.DischargeSummary:
        //                onclick = string.Format("dischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.PTDischargeSummary:
        //                onclick = string.Format("PTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.OTDischargeSummary:
        //                onclick = string.Format("OTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SkilledNurseVisit:
        //            case DisciplineTasks.SNInsulinAM:
        //            case DisciplineTasks.SNInsulinPM:
        //            case DisciplineTasks.SNInsulinNoon:
        //            case DisciplineTasks.SNInsulinHS:
        //            case DisciplineTasks.FoleyCathChange:
        //            case DisciplineTasks.SNB12INJ:
        //            case DisciplineTasks.SNBMP:
        //            case DisciplineTasks.SNCBC:
        //            case DisciplineTasks.SNHaldolInj:
        //            case DisciplineTasks.PICCMidlinePlacement:
        //            case DisciplineTasks.PRNFoleyChange:
        //            case DisciplineTasks.PRNSNV:
        //            case DisciplineTasks.PRNVPforCMP:
        //            case DisciplineTasks.PTWithINR:
        //            case DisciplineTasks.PTWithINRPRNSNV:
        //            case DisciplineTasks.SkilledNurseHomeInfusionSD:
        //            case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
        //            case DisciplineTasks.SNDC:
        //            case DisciplineTasks.SNEvaluation:
        //            case DisciplineTasks.SNFoleyLabs:
        //            case DisciplineTasks.SNFoleyChange:
        //            case DisciplineTasks.SNInjection:
        //            case DisciplineTasks.SNInjectionLabs:
        //            case DisciplineTasks.SNLabsSN:
        //            case DisciplineTasks.SNVwithAideSupervision:
        //            case DisciplineTasks.SNVDCPlanning:
        //            case DisciplineTasks.SNVTeachingTraining:
        //            case DisciplineTasks.SNVManagementAndEvaluation:
        //            case DisciplineTasks.SNVObservationAndAssessment:
        //                onclick = string.Format("snVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SNVPsychNurse:
        //                onclick = string.Format("snPsychVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SNPsychAssessment:
        //                onclick = string.Format("snPsychAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.Labs:
        //                onclick = string.Format("snLabs.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.InitialSummaryOfCare:
        //                onclick = string.Format("ISOC.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SNDiabeticDailyVisit:
        //                onclick = string.Format("snDiabeticDailyVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SNPediatricVisit:
        //                onclick = string.Format("snPediatricVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SNPediatricAssessment:
        //                onclick = string.Format("snPediatricAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.SixtyDaySummary:
        //                onclick = string.Format("sixtyDaySummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.TransferSummary:
        //                onclick = string.Format("transferSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.CoordinationOfCare:
        //                onclick = string.Format("coordinationofcare.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.LVNSupervisoryVisit:
        //                onclick = string.Format("lvnSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.HHAideSupervisoryVisit:
        //                onclick = string.Format("hhaSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.HHAideCarePlan:
        //                onclick = string.Format("hhaCarePlan.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.UAPWoundCareVisit:
        //            case DisciplineTasks.UAPInsulinPrepAdminVisit:
        //            case DisciplineTasks.PASVisit:
        //            case DisciplineTasks.PASTravel:
        //            case DisciplineTasks.PASCarePlan:
        //            case DisciplineTasks.HHAideVisit:
        //            case DisciplineTasks.PTVisit:
        //            case DisciplineTasks.PTAVisit:
        //            case DisciplineTasks.PTEvaluation:
        //            case DisciplineTasks.PTReEvaluation:
        //            case DisciplineTasks.PTReassessment:
        //            case DisciplineTasks.PTMaintenance:
        //            case DisciplineTasks.PTDischarge:
        //            case DisciplineTasks.OTVisit:
        //            case DisciplineTasks.COTAVisit:
        //            case DisciplineTasks.OTMaintenance:
        //            case DisciplineTasks.OTEvaluation:
        //            case DisciplineTasks.OTReEvaluation:
        //            case DisciplineTasks.OTReassessment:
        //            case DisciplineTasks.OTDischarge:
        //            case DisciplineTasks.STVisit:
        //            case DisciplineTasks.STEvaluation:
        //            case DisciplineTasks.STReEvaluation:
        //            case DisciplineTasks.STReassessment:
        //            case DisciplineTasks.STMaintenance:
        //            case DisciplineTasks.STDischarge:
        //            case DisciplineTasks.MSWEvaluationAssessment:
        //            case DisciplineTasks.MSWAssessment:
        //            case DisciplineTasks.MSWDischarge:
        //            case DisciplineTasks.HomeMakerNote:
        //            case DisciplineTasks.PTSupervisoryVisit:
        //            case DisciplineTasks.OTSupervisoryVisit:
        //                onclick = string.Format("Visit.{3}.Load('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.IncidentAccidentReport:
        //                onclick = string.Format("UserInterface.ShowEditIncident('{0}');", scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.InfectionReport:
        //                onclick = string.Format("UserInterface.ShowEditInfection('{0}');", scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.MSWProgressNote:
        //                onclick = string.Format("Visit.MSWProgressNote.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.MSWVisit:
        //                onclick = string.Format("Visit.MSWVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //            case DisciplineTasks.DriverOrTransportationNote:
        //                onclick = string.Format("transportationLog.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
        //                break;
        //        }

        //        if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
        //        if (Current.HasRight(Permissions.DeleteTasks))
        //        {
        //            if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
        //            else scheduleEvent.ActionUrl = deleteUrl;
        //        }
        //        if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits))
        //        {
        //            if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + reassignUrl;
        //            else scheduleEvent.ActionUrl = reassignUrl;
        //        }
        //        scheduleEvent.PrintUrl = printUrl;
        //        if (scheduleEvent.IsCompleted())
        //        {
        //            scheduleEvent.ActionUrl = string.Empty;
        //            if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
        //            scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
        //        }
        //        if (scheduleEvent.IsCompletelyFinished())
        //        {
        //            if (Current.HasRight(Permissions.EditTaskDetails))
        //            {
        //                if (scheduleEvent.ActionUrl.IsNotNullOrEmpty() && !scheduleEvent.ActionUrl.Contains(detailUrl)) scheduleEvent.ActionUrl += " | " + detailUrl;
        //                else scheduleEvent.ActionUrl = detailUrl;
        //            }
        //            if (Current.HasRight(Permissions.DeleteTasks))
        //            {
        //                if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
        //                else scheduleEvent.ActionUrl = deleteUrl;
        //            }
        //            if (Current.HasRight(Permissions.ReopenDocuments))
        //            {
        //                if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
        //                {
        //                    reopenUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"if(confirm('Are you sure you would like to reopen this task?')){{Schedule.ReOpen('{0}','{1}','{2}');{3}}}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
        //                    scheduleEvent.ActionUrl += " | " + reopenUrl;
        //                }
        //                else scheduleEvent.ActionUrl = reopenUrl;
        //            }
        //            if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport) scheduleEvent.ActionUrl = string.Empty;
        //        }
        //        if (scheduleEvent.IsMissedVisit)
        //        {
        //            if (ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true).Contains(scheduleEvent.Status) )
        //            {
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
        //            }
        //            else
        //            {
        //                scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitEdit('{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
        //            }
        //            scheduleEvent.ActionUrl = string.Empty;
        //            //if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
        //            if (Current.HasRight(Permissions.DeleteTasks))
        //            {
        //                if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
        //                else scheduleEvent.ActionUrl = deleteUrl;
        //            }
        //            string restoreUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.RestoreMissedVisit('{0}','{1}','{2}');\">Restore</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //            if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
        //            {
        //                scheduleEvent.ActionUrl += " | " + restoreUrl;
        //            }
        //            else
        //            {
        //                scheduleEvent.ActionUrl = restoreUrl;
        //            }
        //        }

        //        if (Current.IsAgencyFrozen)
        //        {
        //            scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
        //            scheduleEvent.ActionUrl = string.Empty;
        //            scheduleEvent.Status = 0;
        //            scheduleEvent.EpisodeNotes = string.Empty;
        //        }
        //        else if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
        //        {
        //        }
        //        else if (Current.IsClinicianOrHHA)
        //        {
        //            var patient = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository.GetPatientOnly(scheduleEvent.PatientId, Current.AgencyId);
        //            if (Current.UserId != scheduleEvent.UserId && (patient != null && !patient.UserId.IsEmpty() && patient.UserId != Current.UserId))
        //            {
        //                scheduleEvent.PrintUrl = string.Empty;
        //                scheduleEvent.ActionUrl = string.Empty;
        //                scheduleEvent.Status = 0;
        //                scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
        //            }
        //        }
        //        else if (Current.IfOnlyRole(AgencyRoles.Auditor))
        //        {
        //            scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
        //            scheduleEvent.ActionUrl = string.Empty;
        //            scheduleEvent.Status = 0;
        //            scheduleEvent.EpisodeNotes = string.Empty;
        //            scheduleEvent.MissedVisitComments = string.Empty;
        //            scheduleEvent.ReturnReason = string.Empty;
        //            scheduleEvent.Comments = string.Empty;
        //        }
        //        else
        //        {
        //            scheduleEvent.PrintUrl = string.Empty;
        //            scheduleEvent.ActionUrl = string.Empty;
        //            scheduleEvent.Status = 0;
        //        }
                    
        //        if (scheduleEvent.IsOrphaned)
        //        {
        //            scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
        //            if (Current.HasRight(Permissions.EditTaskDetails) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //            {
        //                scheduleEvent.ActionUrl = detailUrl;
        //            }
        //            if (Current.HasRight(Permissions.DeleteTasks) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //            {
        //                if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
        //                {
        //                    scheduleEvent.ActionUrl += " | " + deleteUrl;
        //                }
        //                else
        //                {
        //                    scheduleEvent.ActionUrl = deleteUrl;
        //                }
        //            }
        //            if (scheduleEvent.UserName.IsEqual("Axxess"))
        //            {
        //                scheduleEvent.ActionUrl = string.Empty;
        //            }
        //        }
        //        if (Current.HasRight(Permissions.ViewHHRGCalculations) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //        {
        //            scheduleEvent.OasisProfileUrl = oasisProfileUrl;
        //        }
        //        if (scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0)
        //        {
        //            scheduleEvent.AttachmentUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskAttachments('{0}', '{1}', '{2}');\"><span class=\"img icon paperclip\"></span></a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
        //        }
        //    }
        //}

        public static void SetNew(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon,bool IsOasisProfile, bool isAssetUrl)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string onclick = string.Empty;
                if (scheduleEvent.IsOrphaned && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
                {
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    SetAction(scheduleEvent, addReassignLink, onclick);
                    scheduleEvent.PrintUrl = Print(scheduleEvent, usePrintIcon);
                }
                else
                {
                    if (Current.IsAgencyFrozen)
                    {
                        scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                        scheduleEvent.ActionUrl = string.Empty;
                        scheduleEvent.Status = 0;
                        scheduleEvent.EpisodeNotes = string.Empty;
                        scheduleEvent.PrintUrl = Print(scheduleEvent, usePrintIcon);
                    }
                    else if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                    {
                        SetNewUrl(scheduleEvent, ref onclick);
                        SetAction(scheduleEvent, addReassignLink, onclick);
                        scheduleEvent.PrintUrl = Print(scheduleEvent, usePrintIcon);

                    }
                    else if (Current.IsClinicianOrHHA)
                    {
                        var patient = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository.GetPatientOnly(scheduleEvent.PatientId, Current.AgencyId);
                        if (Current.UserId != scheduleEvent.UserId && (patient != null && !patient.UserId.IsEmpty() && patient.UserId != Current.UserId))
                        {
                            scheduleEvent.PrintUrl = string.Empty;
                            scheduleEvent.ActionUrl = string.Empty;
                            scheduleEvent.Status = 0;
                            scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                        }
                        else
                        {
                            SetNewUrl(scheduleEvent, ref onclick);
                            SetAction(scheduleEvent, addReassignLink, onclick);
                            scheduleEvent.PrintUrl = Print(scheduleEvent, usePrintIcon); 
                           
                        }
                    }
                    else if (Current.IfOnlyRole(AgencyRoles.Auditor))
                    {
                        scheduleEvent.PrintUrl = Print(scheduleEvent, usePrintIcon); 
                        scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                        scheduleEvent.ActionUrl = string.Empty;
                        scheduleEvent.Status = 0;
                        scheduleEvent.EpisodeNotes = string.Empty;
                        scheduleEvent.MissedVisitComments = string.Empty;
                        scheduleEvent.ReturnReason = string.Empty;
                        scheduleEvent.Comments = string.Empty;
                    }
                    else
                    {
                        SetNewUrl(scheduleEvent, ref onclick);
                        scheduleEvent.PrintUrl = string.Empty;
                        scheduleEvent.ActionUrl = string.Empty;
                        scheduleEvent.Status = 0;
                    }
                }
                if (IsOasisProfile)
                {
                    if (Current.HasRight(Permissions.ViewHHRGCalculations) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
                    {
                        if (DisciplineTaskFactory.EpisodeAllAssessments(false).Contains(scheduleEvent.DisciplineTask) && ScheduleStatusFactory.OASISCompleted(true).Contains(scheduleEvent.Status))
                        {
                            scheduleEvent.OasisProfileUrl = string.Format("<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({{ Url: 'Oasis/Profile/{0}/{1}/{2}', PdfUrl: 'Oasis/ProfilePdf',PdfData: {{ 'patientId':'{0}','episodeId':'{1}', 'Id': '{2}' }} }})\"><span class=\"img icon money\"></span></a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId);
                        }
                    }
                }
                if (isAssetUrl)
                {
                    if (scheduleEvent.Asset.IsNotNullOrEmpty())
                    {
                        var assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                        if (assets != null && assets.Count > 0)
                        {
                            scheduleEvent.Asset = string.Empty;
                            scheduleEvent.AttachmentUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskAttachments('{0}', '{1}', '{2}');\"><span class=\"img icon paperclip\"></span></a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        }
                    }
                }
            }
        }

        private static void SetAction(ScheduleEvent scheduleEvent, bool addReassignLink, string onclick)
        {
            string reopenUrl = string.Empty;
            var detailUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskDetails('{0}', '{1}', '{2}');\">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
            var deleteUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}','{4}');\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.UserId, scheduleEvent.DisciplineTask);
            string reassignUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this), '{0}','{1}','{2}','{3}');\" class=\"reassign\">Reassign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.UserId);
            var listOfAction = new List<string>();

            if (scheduleEvent.IsMissedVisit)
            {
                if (Current.HasRight(Permissions.DeleteTasks))
                {
                    listOfAction.Add(deleteUrl);
                }
                string restoreUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.RestoreMissedVisit('{0}','{1}','{2}');\">Restore</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                listOfAction.Add(restoreUrl);
            }
           else if (scheduleEvent.IsOrphaned)
            {
                if (Current.HasRight(Permissions.EditTaskDetails) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
                {
                    listOfAction.Add(detailUrl);
                }
                if (Current.HasRight(Permissions.DeleteTasks) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
                {
                    listOfAction.Add(deleteUrl);
                }
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                {
                    scheduleEvent.ActionUrl = string.Empty;
                }
            }
            else
            {
                if (ScheduleStatusFactory.CaseManagerStatus().Contains(scheduleEvent.Status)) //scheduleEvent.IsCompleted()
                {
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        listOfAction.Add(detailUrl);
                        scheduleEvent.ActionUrl = detailUrl;
                    }
                    // scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                }
                else if (scheduleEvent.IsCompletelyFinished())
                {
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        listOfAction.Add(detailUrl);
                    }
                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        listOfAction.Add(deleteUrl);
                    }
                    if (Current.HasRight(Permissions.ReopenDocuments))
                    {
                        reopenUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"if(confirm('Are you sure you would like to reopen this task?')){{Schedule.ReOpen('{0}','{1}','{2}');{3}}}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                        listOfAction.Add(reopenUrl);
                        //if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        //{
                        //    reopenUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"if(confirm('Are you sure you would like to reopen this task?')){{Schedule.ReOpen('{0}','{1}','{2}');{3}}}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                        //    scheduleEvent.ActionUrl += " | " + reopenUrl;
                        //}
                        //else
                        //{
                        //    scheduleEvent.ActionUrl = reopenUrl;
                        //}
                    }
                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        listOfAction.Clear();
                        //scheduleEvent.ActionUrl = string.Empty;
                    }
                }
                else
                {
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        listOfAction.Add(detailUrl);
                        //scheduleEvent.ActionUrl = detailUrl;
                    }

                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        listOfAction.Add(deleteUrl);
                    }

                    if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits))
                    {
                        listOfAction.Add(reassignUrl);
                    }
                }
            }
            if (listOfAction != null && listOfAction.Count > 0)
            {
                scheduleEvent.ActionUrl = listOfAction.ToArray().Join(" | ");
            }
            else
            {
                scheduleEvent.ActionUrl = string.Empty;
            }

        }

        public static void SetNewUrl(ScheduleEvent scheduleEvent, ref string onclick)
        {
            if (scheduleEvent.IsCompleted() || scheduleEvent.IsOrphaned || Current.IsAgencyFrozen || Current.IfOnlyRole(AgencyRoles.Auditor))
            {
                scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
            }
            else
            {
                if (scheduleEvent.IsMissedVisit)
                {
                    if (ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true).Contains(scheduleEvent.Status))
                    {
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                    }
                    else
                    {
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitEdit('{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                    }
                }
                else
                {

                    switch (scheduleEvent.DisciplineTask)
                    {
                        case (int)DisciplineTasks.OASISCStartofCare:
                        case (int)DisciplineTasks.OASISCStartofCarePT:
                        case (int)DisciplineTasks.OASISCStartofCareOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','StartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNAssessment:
                        case (int)DisciplineTasks.NonOASISStartofCare:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','NonOasisStartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCResumptionofCare:
                        case (int)DisciplineTasks.OASISCResumptionofCarePT:
                        case (int)DisciplineTasks.OASISCResumptionofCareOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','ResumptionOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCFollowUp:
                        case (int)DisciplineTasks.OASISCFollowupPT:
                        case (int)DisciplineTasks.OASISCFollowupOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','FollowUp');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCRecertification:
                        case (int)DisciplineTasks.OASISCRecertificationPT:
                        case (int)DisciplineTasks.OASISCRecertificationOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','Recertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNAssessmentRecert:
                        case (int)DisciplineTasks.NonOASISRecertification:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','NonOasisRecertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCTransfer:
                        case (int)DisciplineTasks.OASISCTransferPT:
                        case (int)DisciplineTasks.OASISCTransferOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','TransferInPatientNotDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                            break;
                        case (int)DisciplineTasks.OASISCTransferDischarge:
                        case (int)DisciplineTasks.OASISCTransferDischargePT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','TransferInPatientDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCDeath:
                        case (int)DisciplineTasks.OASISCDeathPT:
                        case (int)DisciplineTasks.OASISCDeathOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','DischargeFromAgencyDeath');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OASISCDischarge:
                        case (int)DisciplineTasks.OASISCDischargePT:
                        case (int)DisciplineTasks.OASISCDischargeOT:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','DischargeFromAgency');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.NonOASISDischarge:
                            onclick = string.Format("Oasis.Assessment.Load('{0}','{1}','NonOasisDischarge');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.PhysicianOrder:
                            onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.FaceToFaceEncounter:
                            // onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                            scheduleEvent.Url = string.Format("{0}", scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.HCFA485:
                            onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.HCFA485StandAlone:
                            onclick = string.Format("UserInterface.ShowEditPlanofCareStandAlone('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.NonOasisHCFA485:
                            onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.CommunicationNote:
                            onclick = string.Format("Patient.LoadEditCommunicationNote('{0}','{1}');", scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.DischargeSummary:
                            onclick = string.Format("dischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.PTDischargeSummary:
                            onclick = string.Format("PTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.OTDischargeSummary:
                            onclick = string.Format("OTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SkilledNurseVisit:
                        case (int)DisciplineTasks.SNInsulinAM:
                        case (int)DisciplineTasks.SNInsulinPM:
                        case (int)DisciplineTasks.SNInsulinNoon:
                        case (int)DisciplineTasks.SNInsulinHS:
                        case (int)DisciplineTasks.FoleyCathChange:
                        case (int)DisciplineTasks.SNB12INJ:
                        case (int)DisciplineTasks.SNBMP:
                        case (int)DisciplineTasks.SNCBC:
                        case (int)DisciplineTasks.SNHaldolInj:
                        case (int)DisciplineTasks.PICCMidlinePlacement:
                        case (int)DisciplineTasks.PRNFoleyChange:
                        case (int)DisciplineTasks.PRNSNV:
                        case (int)DisciplineTasks.PRNVPforCMP:
                        case (int)DisciplineTasks.PTWithINR:
                        case (int)DisciplineTasks.PTWithINRPRNSNV:
                        case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                        case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                        case (int)DisciplineTasks.SNDC:
                        case (int)DisciplineTasks.SNEvaluation:
                        case (int)DisciplineTasks.SNFoleyLabs:
                        case (int)DisciplineTasks.SNFoleyChange:
                        case (int)DisciplineTasks.SNInjection:
                        case (int)DisciplineTasks.SNInjectionLabs:
                        case (int)DisciplineTasks.SNLabsSN:
                        case (int)DisciplineTasks.SNVwithAideSupervision:
                        case (int)DisciplineTasks.SNVDCPlanning:
                        case (int)DisciplineTasks.SNVTeachingTraining:
                        case (int)DisciplineTasks.SNVManagementAndEvaluation:
                        case (int)DisciplineTasks.SNVObservationAndAssessment:
                            onclick = string.Format("snVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNVPsychNurse:
                            onclick = string.Format("snPsychVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNPsychAssessment:
                            onclick = string.Format("snPsychAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.Labs:
                            onclick = string.Format("snLabs.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.InitialSummaryOfCare:
                            onclick = string.Format("ISOC.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNDiabeticDailyVisit:
                            onclick = string.Format("snDiabeticDailyVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNPediatricVisit:
                            onclick = string.Format("snPediatricVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SNPediatricAssessment:
                            onclick = string.Format("snPediatricAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.SixtyDaySummary:
                            onclick = string.Format("sixtyDaySummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.TransferSummary:
                            onclick = string.Format("transferSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.CoordinationOfCare:
                            onclick = string.Format("coordinationofcare.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.LVNSupervisoryVisit:
                            onclick = string.Format("lvnSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.HHAideSupervisoryVisit:
                            onclick = string.Format("hhaSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.HHAideCarePlan:
                            onclick = string.Format("hhaCarePlan.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.UAPWoundCareVisit:
                        case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                        case (int)DisciplineTasks.PASVisit:
                        case (int)DisciplineTasks.PASTravel:
                        case (int)DisciplineTasks.PASCarePlan:
                        case (int)DisciplineTasks.HHAideVisit:
                        case (int)DisciplineTasks.PTVisit:
                        case (int)DisciplineTasks.PTAVisit:
                        case (int)DisciplineTasks.PTEvaluation:
                        case (int)DisciplineTasks.PTReEvaluation:
                        case (int)DisciplineTasks.PTReassessment:
                        case (int)DisciplineTasks.PTMaintenance:
                        case (int)DisciplineTasks.PTDischarge:
                        case (int)DisciplineTasks.OTVisit:
                        case (int)DisciplineTasks.COTAVisit:
                        case (int)DisciplineTasks.OTMaintenance:
                        case (int)DisciplineTasks.OTEvaluation:
                        case (int)DisciplineTasks.OTReEvaluation:
                        case (int)DisciplineTasks.OTReassessment:
                        case (int)DisciplineTasks.OTDischarge:
                        case (int)DisciplineTasks.STVisit:
                        case (int)DisciplineTasks.STEvaluation:
                        case (int)DisciplineTasks.STReEvaluation:
                        case (int)DisciplineTasks.STReassessment:
                        case (int)DisciplineTasks.STMaintenance:
                        case (int)DisciplineTasks.STDischarge:
                        case (int)DisciplineTasks.MSWEvaluationAssessment:
                        case (int)DisciplineTasks.MSWAssessment:
                        case (int)DisciplineTasks.MSWDischarge:
                        case (int)DisciplineTasks.HomeMakerNote:
                        case (int)DisciplineTasks.PTSupervisoryVisit:
                        case (int)DisciplineTasks.OTSupervisoryVisit:
                            onclick = string.Format("Visit.{3}.Load('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString());
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.IncidentAccidentReport:
                            onclick = string.Format("UserInterface.ShowEditIncident('{0}');", scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.InfectionReport:
                            onclick = string.Format("UserInterface.ShowEditInfection('{0}');", scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.MSWProgressNote:
                            onclick = string.Format("Visit.MSWProgressNote.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.MSWVisit:
                            onclick = string.Format("Visit.MSWVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                        case (int)DisciplineTasks.DriverOrTransportationNote:
                            onclick = string.Format("transportationLog.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                            scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                            break;
                    }
                }
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
                printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                    "Url: '/MissedVisit/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                    "PdfUrl: 'Schedule/MissedVisitPdf'," +
                    "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                    (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowMissedVisitEdit('"  + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Return','startofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { StartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','StartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'episodeId': '" + scheduleEvent.EpisodeId + "', 'assessmentType': 'StartOfCare' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Approve','startofcare');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Return','nonoasisstartofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisStartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisStartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Approve','nonoasisstartofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Return','resumptionofcare'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { ResumptionOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','ResumptionOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'episodeId': '" + scheduleEvent.EpisodeId + "', 'assessmentType': 'ResumptionOfCare' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Approve','resumptionofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Return','followup');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { FollowUp.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','FollowUp'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'episodeId': '" + scheduleEvent.EpisodeId + "', 'assessmentType': 'FollowUp' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Approve','followup');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Return','recertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Recertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','Recertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'episodeId': '" + scheduleEvent.EpisodeId + "', 'assessmentType': 'Recertification' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Approve','recertification');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Return','nonoasisrecertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisRecertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisRecertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Approve','nonoasisrecertification');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Return','transfernotdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientNotDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientNotDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Approve','transfernotdischarge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferDischargePT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Return','transferinpatientdischarged');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Approve','transferinpatientdischarged');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Return','DischargeFromAgencyDeath'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgencyDeath.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgencyDeath'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Approve','DischargeFromAgencyDeath'); } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Return','discharge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgency.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgency'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Approve','discharge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Oasis/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Return','nonoasisdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisDischarge.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisDischarge'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Approve','nonoasisdischarge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/FaceToFaceEncounter/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianFaceToFaceEncounterPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Order/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianOrderPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'eventId': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditOrder('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCare('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCareStandAlone('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/CommunicationNote/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Patient/CommunicationNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.ProcessCommunicationNote('Return','" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Patient.LoadEditCommunicationNote('" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.ProcessCommunicationNote('Approve','" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { dischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { PTDischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { OTDischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.Labs:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snLabs.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { ISOC.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'View Plan of Care'," +
                                    "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                         "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                         "PdfUrl: 'Schedule/NotePdf'," +
                         "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                         (!usePrintIcon ?
                             ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                             ", Buttons: [ {" +
                                 "Text: 'View Plan of Care'," +
                                 "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                 "Text: 'Edit'," +
                                 "Click: function() { snPsychVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                 "Text: 'Approve'," +
                                 "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                         : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                         "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                         "PdfUrl: 'Schedule/NotePdf'," +
                         "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                         (!usePrintIcon ?
                             ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                             ", Buttons: [ {" +
                                 "Text: 'View Plan of Care'," +
                                 "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                 "Text: 'Edit'," +
                                 "Click: function() { snPsychAssessment.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                 "Text: 'Approve'," +
                                 "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                         : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snDiabeticDailyVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snPediatricVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snPediatricAssessment.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { sixtyDaySummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { transferSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { coordinationofcare.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { lvnSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaCarePlan.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;

                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASTravel:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTReassessment:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Visit." + task.ToString() + ".Print('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "', " + (!usePrintIcon).ToString().ToLower() + ")\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideVisit:
                        if (!usePrintIcon)
                        {
                            printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                           "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                           "PdfUrl: 'Schedule/NotePdf'," +
                           "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                           (!usePrintIcon ?
                               ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                               ", Buttons: [ {" +
                                   "Text: 'View Care Plan'," +
                                   "Click: function() { Schedule.GetHHACarePlanUrlFromHHAVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                   "Text: 'Edit'," +
                                   "Click: function() { Visit.HHAideVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                   "Text: 'Approve'," +
                                   "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                           : "") + "})\">" + linkText + "</a>";
                        }
                        else
                        {
                            printUrl = "<a href=\"javascript:void(0);\" onclick=\"Visit." + task.ToString() + ".Print('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "', " + (!usePrintIcon).ToString().ToLower() + ")\">" + linkText + "</a>";
                        }
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Visit.MSWProgressNote.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Visit.MSWVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/InfectionReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { InfectionReport.ProcessInfection('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditInfection('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { InfectionReport.ProcessInfection('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/IncidentReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { IncidentReport.ProcessIncident('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditIncident('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { IncidentReport.ProcessIncident('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: 'Note/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/NotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { transportationLog.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }

        public static string Download(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
            {
                printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MissedVisitPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
            }
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisRecertificationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferDischargePT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisDischargePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianFaceToFaceEncounterPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianOrderPdf', {   'patientId': '" + scheduleEvent.PatientId + "', 'eventId':'" + scheduleEvent.EventId + "','episodeId':'"+scheduleEvent.EpisodeId+"' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/CommunicationNotePdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "', 'episodeId': '" + scheduleEvent.EpisodeId +"' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/DischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTDischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTDischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNPsychVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNPsychAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SixtyDaySummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/InitialSummaryOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransferSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransferSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/LVNSVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/HHASVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/" + task.ToString() + "Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTReassessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTReassessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTReassessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTReassessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.COTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/STEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWEvaluationAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PASVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASTravel:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PASTravelPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PASCarePlanPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/HHACarePlanPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWProgressNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Agency/InfectionReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Agency/IncidentReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransportationNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNDiabeticDailyVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNPediatricVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNPediatricAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.UAPWoundCareVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/UAPWoundCareVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/UAPInsulinPrepAdminVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.Labs:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/LabsPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }
    }
}