﻿namespace Axxess.AgencyManagement.App
{
    using System;

    using Exports;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using System.Collections.Generic;

    public static class ReportManager
    {
        private static readonly ILoginRepository loginRepository = Container.Resolve<MembershipDataProvider>().LoginRepository;
        private static readonly IUserRepository userRepository = Container.Resolve<AgencyManagementDataProvider>().UserRepository;
        private static readonly IAssetRepository assetRepository = Container.Resolve<AgencyManagementDataProvider>().AssetRepository;
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<AgencyManagementDataProvider>().AgencyRepository;
        private static readonly IMessageRepository messageRepository = Container.Resolve<AgencyManagementDataProvider>().MessageRepository;

        public static void AddPatientsAndVisitsByAgeReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new PatientsAndVisitsByAgeExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddUnbilledVisitsReport(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new UnbilledVisitsExporter(agencyId, branchId, insuranceId, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddTherapyManagementReport(Guid branchId, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new TherapyManagementExporter(agencyId, branchId, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddHHRGReport(Guid branchId, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new HHRGExporter(agencyId, branchId, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddDischargesByReasonReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new DischargesByReasonExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddVisitsByPrimaryPaymentSourceReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new VisitsByPrimaryPaymentSourceExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddVisitsByStaffTypeReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new VisitsByStaffTypeExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddAdmissionsByReferralSourceReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new AdmissionsByReferralSourceExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddPatientsVisitsByPrincipalDiagnosisReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new PatientsVisitsByPrincipalDiagnosisExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddCostReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new CostReportExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        private static void CompleteReport(Guid agencyId, Guid reportId, BaseExporter export)
        {
            var report = agencyRepository.GetReport(agencyId, reportId);
            if (report != null)
            {
                var bytes = export.Process().GetBuffer();
                var asset = new Asset
                    {
                        FileName = export.FileName,
                        AgencyId = agencyId,
                        ContentType = export.MimeType,
                        FileSize = bytes.Length.ToString(),
                        Bytes = bytes
                    };

                if (assetRepository.Add(asset))
                {
                    report.AssetId = asset.Id;
                    report.Completed = DateTime.Now;
                    report.Status = "Completed";

                    if (agencyRepository.UpdateReport(report))
                    {
                        var user = userRepository.GetUserOnly(report.UserId, agencyId);
                        if (user != null)
                        {
                            if (!user.LoginId.IsEmpty())
                            {
                                var message = new SystemMessage
                                {
                                    CreatedBy = "Axxess",
                                    Created = DateTime.Now,
                                    Subject = "Your report is ready for download",
                                    Body = "<p>Dear User,</p><p>Go to the \"Report Menu\" and click on \"Completed Reports\" to view your requested report.</p><p>The Axxess Team</p>",
                                };
                                if (messageRepository.AddSystemMessage(message))
                                {
                                    var login = loginRepository.Find(user.LoginId);
                                    if (login != null && login.IsActive)
                                    {
                                        if (user.Messages.IsNullOrEmpty())
                                        {
                                            user.SystemMessages = new List<MessageState>();
                                        }
                                        else
                                        {
                                            user.SystemMessages = user.Messages.ToObject<List<MessageState>>();
                                        }
                                        user.SystemMessages.Add(
                                            new MessageState
                                            {
                                                Id = message.Id,
                                                IsRead = false
                                            });

                                        user.Messages = user.SystemMessages.ToXml();
                                        if (userRepository.UpdateModel(user, false))
                                        {
                                            var parameters = new string[4];
                                            parameters[0] = "recipientfirstname";
                                            parameters[1] = user.DisplayName;
                                            parameters[2] = "senderfullname";
                                            parameters[3] = "Axxess";
                                            var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                            Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Axxess Technology Solutions sent you a message.", bodyText);
                                        }
                                    }

                                }
                            }
                        }

                    }
                }
            }
        }
    }
}
