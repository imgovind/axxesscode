﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.App.iTextExtension;

    public class NoteArguments
    {
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EventId { get; set; }
        public bool IsPlanOfCareNeeded { get; set; }
        public bool IsHHACarePlanNeeded { get; set; }
        public bool IsPASCarePlanNeeded { get; set; }
        public bool IsVitalSignMerged { get; set; }
        public bool IsPreviousNoteNeeded { get; set; }
        public bool IsPhysicainNeeded { get; set; }
        public bool IsAllergyNeeded { get; set; }
        public bool IsDiagnosisNeeded { get; set; }
        public bool IsEvalNeeded { get; set; }
        public bool IsReturnCommentNeeded { get; set; }
        public DisciplineTasks EvalTask { get; set; }
        public string ContentPath { get; set; }

        // used for print
        public bool IsAssessmentNeeded { get; set; }
        public bool IsJsonSerialize { get; set; }
        public PdfDoc pdfDoc { get; set; }


    }
}
