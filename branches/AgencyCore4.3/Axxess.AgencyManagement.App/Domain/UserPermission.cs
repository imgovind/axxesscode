﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.Domain
{
    public class UserPermission
    {
        public string Employee { get; set; }
        public string Permission { get; set; }
    }
}
