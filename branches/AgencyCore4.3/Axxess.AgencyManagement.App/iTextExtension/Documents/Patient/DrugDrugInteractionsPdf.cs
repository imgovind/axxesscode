﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;

    class DrugDrugInteractionsPdf : AxxessPdf
    {
        public DrugDrugInteractionsPdf(DrugDrugInteractionsViewData data)
        {
            this.SetType(PdfDocs.DrugDrugInteraction);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (data == null || data.DrugDrugInteractions == null || data.DrugDrugInteractions.Count == 0)
            {
                AxxessCell contentcell = new AxxessCell(padding, borders);
                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "No interactions found during this screening.", fonts[0], 12));
                content[0].AddCell(contentcell);
            }
            else
            {
                data.DrugDrugInteractions.ForEach(d =>
                {
                    AxxessCell contentcell = new AxxessCell(padding, borders);
                    contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], d.InteractionDescription, fonts[0], 12));
                    contentcell.AddElement(new AxxessContentSection("SeverityDescription", fonts[1], d.SeverityDescription, fonts[0], 12));
                    if (d.ConsumerText.Length > 0)
                    {
                        d.ConsumerText.ForEach(ct =>
                        {
                            contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], ct, fonts[0], 10));
                        });
                    }
                    content[0].AddCell(contentcell);
                });
            }

            this.SetContent(content);
            float[] margins = new float[] { 118, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("auditvendor", "Drug Screening provided by LexiComp Lexi-Data");
            fieldmap[0].Add("patientname", data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[0].Add("mrn", data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);

            this.SetFields(fieldmap);
        }
    }
}