﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    class InfectionReportPdf : AxxessPdf {
        private InfectionReportXml xml;
        public InfectionReportPdf(Infection data) {
            this.xml = new InfectionReportXml(data);
            this.SetType(PdfDocs.InfectionReport);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 200, 28.8F, 70, 28.8F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });

            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));

            fieldmap[0].Add("infectiondate", data != null && data.InfectionDate.IsValid() ? data.InfectionDate.ToShortDateString() : String.Empty);
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValidDate() ? data.EpisodeStartDate + "—" : String.Empty) + (data != null && data.EpisodeEndDate.IsValidDate() ? data.EpisodeEndDate : String.Empty));
            fieldmap[0].Add("physician", data != null && data.PhysicianName.IsNotNullOrEmpty() ? data.PhysicianName : String.Empty);
            fieldmap[0].Add("treatment", data != null && data.TreatmentPrescribed.IsNotNullOrEmpty() ? data.TreatmentPrescribed : String.Empty);
            fieldmap[0].Add("mdnotice", data != null && data.MDNotified.IsNotNullOrEmpty() ? data.MDNotified : String.Empty);
            fieldmap[0].Add("neworders", data != null && data.NewOrdersCreated.IsNotNullOrEmpty() ? data.NewOrdersCreated : String.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : String.Empty);
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : String.Empty);
            fieldmap[0].Add("diag1", data != null && data.Diagnosis != null && data.Diagnosis.ContainsKey("PrimaryDiagnosis") ? data.Diagnosis["PrimaryDiagnosis"] : String.Empty);
            fieldmap[0].Add("diag2", data != null && data.Diagnosis != null && data.Diagnosis.ContainsKey("SecondaryDiagnosis") ? data.Diagnosis["SecondaryDiagnosis"] : String.Empty);
            fieldmap[1].Add("patientname", data != null && data.Patient != null && data.Patient.DisplayName.IsNotNullOrEmpty() ? data.Patient.DisplayName : string.Empty);
            fieldmap[1].Add("mrn", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            
            this.SetFields(fieldmap);
        }
    }
}