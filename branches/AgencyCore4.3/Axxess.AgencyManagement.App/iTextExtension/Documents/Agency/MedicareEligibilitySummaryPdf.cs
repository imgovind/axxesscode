﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class MedicareEligibilitySummaryPdf : AxxessPdf
    {
        public MedicareEligibilitySummaryPdf(MedicareEligibilitySummary data)
        {
            this.SetType(PdfDocs.MedicareEligibilitySummary);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            //for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            fonts[1].Size = 10F;
            fonts[0].Size = 9F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data != null && data.Data != null ? data.Data : new MedicareEligibilitySummaryData(), fonts));
            this.SetMargins(new float[] { 85, 28, 90.5F, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", data != null && data.AgencyName.IsNotNullOrEmpty() ? data.AgencyName : string.Empty);
            this.SetFields(fieldmap);
        }

        private AxxessTable[] BuildContent(MedicareEligibilitySummaryData data, List<Font> fonts)
        {
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (data.Errors.Count > 0)
            {
                AxxessTable errors = new AxxessTable(new float[] { 11.67F, 11.67F, 12.91F, 31.97F }, true);
                AxxessCell mrnTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell nameTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell medicareTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell errorTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });

                mrnTitle.AddElement(new Chunk("MRN", fonts[1]));
                nameTitle.AddElement(new Chunk("Name", fonts[1]));
                medicareTitle.AddElement(new Chunk("Medicare Number", fonts[1]));
                errorTitle.AddElement(new Chunk("Error", fonts[1]));

                errors.AddCell(mrnTitle);
                errors.AddCell(nameTitle);
                errors.AddCell(medicareTitle);
                errors.AddCell(errorTitle);
                errors.HeaderRows = 1;
                
                foreach (var error in data.Errors)
                {
                    AxxessCell mrnCell = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell nameCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell medicareCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell errorCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });

                    mrnCell.AddElement(new Chunk(error.PatientIdNumber, fonts[0]));
                    nameCell.AddElement(new Chunk(error.DisplayNameWithMi, fonts[0]));
                    medicareCell.AddElement(new Chunk(error.MedicareNumber, fonts[0]));
                    errorCell.AddElement(new Chunk(error.Error, fonts[0]));

                    errors.AddCell(mrnCell);
                    errors.AddCell(nameCell);
                    errors.AddCell(medicareCell);
                    errors.AddCell(errorCell);
                }
                if (errors.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Errors", fonts[1]));
                    chart.AddElement(errors);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
            }
            else
            {
                AxxessCell title = new AxxessCell();
                title.AddElement(new AxxessTitle("No Errors Found", fonts[1]));
                content[0].AddCell(title);
            }
            if (data.Payors.Count > 0)
            {
                AxxessTable payors = new AxxessTable(new float[] { 11.67F, 11.67F, 10.91F, 18.97F, 12.91F, 9.91F, 9.91F }, true);
                AxxessCell mrnTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell nameTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell medicareTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell payorTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell planTitle = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell statDateTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell endDateTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, 0, .5F, 0 });

                mrnTitle.AddElement(new Chunk("MRN", fonts[1]));
                nameTitle.AddElement(new Chunk("Name", fonts[1]));
                medicareTitle.AddElement(new Chunk("Medicare Number", fonts[1]));
                payorTitle.AddElement(new Chunk("Payor", fonts[1]));
                planTitle.AddElement(new Chunk("Plan Number", fonts[1]));
                statDateTitle.AddElement(new Chunk("Start Date", fonts[1]));
                endDateTitle.AddElement(new Chunk("End Date", fonts[1]));

                payors.AddCell(mrnTitle);
                payors.AddCell(nameTitle);
                payors.AddCell(medicareTitle);
                payors.AddCell(payorTitle);
                payors.AddCell(planTitle);
                payors.AddCell(statDateTitle);
                payors.AddCell(endDateTitle);
                payors.HeaderRows = 1;

                foreach (var payor in data.Payors)
                {
                    AxxessCell mrnCell = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell nameCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell medicareCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell payorCell = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell planCell = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell startDateCell = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessCell endDateCell = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });

                    mrnCell.AddElement(new Chunk(payor.PatientIdNumber, fonts[0]));
                    nameCell.AddElement(new Chunk(payor.DisplayNameWithMi, fonts[0]));
                    medicareCell.AddElement(new Chunk(payor.MedicareNumber, fonts[0]));
                    payorCell.AddElement(new Chunk(payor.Payor + payor.AddressLineOne + payor.AddressLineTwo, fonts[0]));
                    planCell.AddElement(new Chunk(payor.PlanNumber, fonts[0]));
                    startDateCell.AddElement(new Chunk(payor.StartDate.IsValidDate() ? payor.StartDate : string.Empty, fonts[0]));
                    endDateCell.AddElement(new Chunk(payor.EndDate.IsValidDate() ? payor.EndDate : string.Empty, fonts[0]));

                    payors.AddCell(mrnCell);
                    payors.AddCell(nameCell);
                    payors.AddCell(medicareCell);
                    payors.AddCell(payorCell);
                    payors.AddCell(planCell);
                    payors.AddCell(startDateCell);
                    payors.AddCell(endDateCell);
                }
                if (payors.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Payors", fonts[1]));
                    chart.AddElement(payors);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
            }
            else
            {
                AxxessCell title = new AxxessCell();
                title.AddElement(new AxxessTitle("No Payors Found", fonts[1]));
                content[0].AddCell(title);
            }

            return content;
        }
    }
}