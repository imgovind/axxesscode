﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using iTextSharp.text;
    class PlanOfCare487Pdf : AxxessPdf {
        public PlanOfCare487Pdf(IElement[] content, List<Dictionary<String, String>> fieldmap) {
            this.SetType(PdfDocs.PlanOfCare487);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts[0].Size = 7.5F;
            fonts[1].Size = 10F;
            this.SetFonts(fonts);
            this.SetContent(content);
            float[] margins = new float[] { 115, 28.4F, 121, 22.7F };
            this.SetMargins(margins);
            this.SetFields(fieldmap);
        }
    }
}