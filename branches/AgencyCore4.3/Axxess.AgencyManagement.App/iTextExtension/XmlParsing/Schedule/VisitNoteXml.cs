﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    class VisitNoteXml : BaseXml
    {
        private IDictionary<String, NotesQuestion> Data = new Dictionary<string, NotesQuestion>();
        public VisitNoteXml(VisitNoteViewData data, PdfDoc type)
            : base(type)
        {
            this.Init(data, type);
        }
        public VisitNoteXml(VisitNoteViewData data, PdfDoc type, int rev)
            : base(type, rev)
        {
            this.Init(data, type);
        }
        private void Init(VisitNoteViewData data, PdfDoc type)
        {
            if ((type == PdfDocs.SkilledNurseVisit || type == PdfDocs.Psych) && data.IsWoundCareExist)
            {
                this.Data = data.MergeDictionaries();
                if (data.DisciplineTask == 42)
                {
                    this.Type = "woundcare42";
                }
                else
                {
                    this.Type = "woundcare";
                }
            }
            else if (type == PdfDocs.SkilledNurseVisit)
            {
                this.Data = data.Questions != null ? data.Questions : new Dictionary<string, NotesQuestion>();
                if (data.DisciplineTask == 42)
                {
                    this.Type = "42";
                }
            }
            else
            {
                this.Data = data.Questions != null ? data.Questions : new Dictionary<string, NotesQuestion>();
            }
            if (data.PhysicianDisplayName.IsNotNullOrEmpty())
            {
                if (!this.Data.ContainsKey("PhysicianDisplayName")) this.Data.Add(new KeyValuePair<string, NotesQuestion>("PhysicianDisplayName", new NotesQuestion { Name = "PhysicianDisplayName", Answer = data.PhysicianDisplayName }));
            }
            if (data.PhysicianSignatureText.IsNotNullOrEmpty())
            {
                if (!this.Data.ContainsKey("PhysicianSignatureText")) this.Data.Add(new KeyValuePair<string, NotesQuestion>("PhysicianSignatureText", new NotesQuestion()));
                this.Data["PhysicianSignatureText"].Answer = data.PhysicianSignatureText;
            }
            if (data.PhysicianSignatureDate.IsValid())
            {
                if (!this.Data.ContainsKey("PhysicianSignatureDate")) this.Data.Add(new KeyValuePair<string, NotesQuestion>("PhysicianSignatureDate", new NotesQuestion()));
                this.Data["PhysicianSignatureDate"].Answer = data.PhysicianSignatureDate.ToShortDateString();
            }
            this.Init();

            if (data.Questions != null) this.FilterEmptySections();
            if (this.Layout[0].Subsection.Count > 0 && this.Layout[0].Subsection[0].Label == "Homebound Status" && this.Layout[0].Subsection[0].Question.Count == 2)
            {
                if (data.Questions == null || data.Questions.ContainsKey("GenericHomeBoundStatus")) this.Layout[0].Subsection[0].Question.RemoveAt(1);
                else this.Layout[0].Subsection[0].Question.RemoveAt(0);
            }
        }
        private void FilterEmptySections()
        {
            for (int sectionI = 0; sectionI < this.Layout.Count(); sectionI++)
            {
                for (int subsectionI = 0; subsectionI < this.Layout[sectionI].Subsection.Count(); subsectionI++)
                {
                    if (this.Layout[sectionI].Subsection[subsectionI].Type == "goals" || this.Layout[sectionI].Subsection[subsectionI].Type == "interventions")
                    {
                        this.RemoveUnusedOrdersGoals(sectionI, subsectionI);
                    }
                    else if (this.Layout[sectionI].Subsection[subsectionI].Type == "woundgraph")
                    {
                        subsectionI = this.RemoveUnusedWounds(sectionI, subsectionI);
                    }
                }
            }
            this.NotaFilter();
        }
        private int RemoveUnusedWounds(int Section, int Subsection)
        {
            if (this.Layout[Section].Subsection[Subsection].Question[0].Subquestion[0].Data.Trim().IsNullOrEmpty()) this.Layout[Section].Subsection.RemoveAt(Subsection--);
            return Subsection;
        }
        public override string GetData(string Index)
        {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}