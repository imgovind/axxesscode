﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using Services;
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using Axxess.AgencyManagement;
    using Axxess.Core.Extension;
    using System.Collections.Generic;
    class XmlPrintQuestion : XmlElement {
        public String Align;
        public int Cols;
        public int Length;
        public String Data;
        public String Display;
        public String Instructions;
        public String Label;
        public String Oasis;
        public String PlanOfCare;
        public String Type;
        public String Value;
        public int[] ColWidths;
        public List<XmlPrintOption> Option;
        public List<XmlPrintQuestion> Subquestion;
        public XmlPrintQuestion(BaseXml xml, XElement question) : base(xml, question) {
            this.Align = this.GetAttribute("align");
            this.Cols = this.GetIntAttribute("cols");
            this.Length = this.GetIntAttribute("length");
            this.Data = this.GetData();
            this.Display = this.GetAttribute("display").IsNotNullOrEmpty() ? this.GetAttribute("display") : "block";
            this.Instructions = this.GetAttribute("instructions");
            this.Label = this.GetAttribute("label");
            this.Oasis = this.GetAttribute("oasis");
            this.PlanOfCare = this.GetAttribute("planofcare");
            this.Type = this.GetAttribute("type");
            this.Value = this.GetAttribute("value");
            this.ColWidths = this.GetColWidths(this.Cols);
            this.Option = this.GetOptions();
            this.Subquestion = this.GetQuestions();
            if (this.Type == "user") {
                this.Type = "text";
                if (this.Data.IsGuid() && !this.Data.ToGuid().Equals(Guid.Empty)) this.Data = UserEngine.GetName(this.Data.ToGuid(), Current.AgencyId);
                else this.Data = string.Empty;
            } else if (this.Type == "physician") {
                this.Type = "text";
                if (this.Data.IsGuid() && !this.Data.ToGuid().Equals(Guid.Empty)) this.Data = PhysicianEngine.GetName(this.Data.ToGuid(), Current.AgencyId);
                else this.Data = string.Empty;
            }
        }
        public override string GetJson() {
            StringBuilder Json = new StringBuilder();
            switch (this.Type) {
                case "text": case "date": case "state": case "phone":
                    if (this.Label.IsNotNullOrEmpty()) Json.Append("<span class='fl strong'>" + (this.Oasis.IsNotNullOrEmpty() ? "(" + this.CleanForJson(this.Oasis) + ") " : "") + this.CleanForJson(this.Label) + "</span><span class='fr" + (this.Data.IsNullOrEmpty() ? " blank" : string.Empty) + "'>" + (this.Data.IsNotNullOrEmpty() ? this.CleanForJson(this.Data) : "&#160;") + "</span><span class='cb'></span>");
                    else Json.Append("<span class='" + (this.Data.IsNullOrEmpty() ? "blank" : string.Empty) + "'>" + (this.Data.IsNotNullOrEmpty() ? this.CleanForJson(this.Data) : "&#160;") + "</span>");
                    break;
                case "select":
                    String answer = string.Empty;
                    foreach (XmlPrintOption option in this.Option) if (option.Value.Equals(this.Data)) answer = option.Label;
                    if (this.Label.IsNotNullOrEmpty()) Json.Append("<span class='fl strong'>" + (this.Oasis.IsNotNullOrEmpty() ? "(" + this.CleanForJson(this.Oasis) + ") " : "") + this.CleanForJson(this.Label) + "</span><span class='fr" + (this.Data.IsNullOrEmpty() ? " blank" : string.Empty) + "'>");
                    foreach (XmlPrintOption option in this.Option) if (this.Data.Equals(option.Value)) Json.Append(this.CleanForJson(option.Label));
                    Json.Append("</span><span class='cb'></span>");
                    break;
                case "textarea":
                    if (this.Data.IsNullOrEmpty()) {
                        this.Data = "<span class='blank'>&#160; </span>";
                        for (int i = 1; i < this.Length; i++) this.Data += "<span class='blank'>&#160; </span>";
                    }
                    else this.Data = this.CleanForJson(this.Data);
                    if (this.Label.IsNotNullOrEmpty()) Json.Append("<span class='strong'>" + this.CleanForJson(this.Label) + "</span><p>" + this.Data + "</p>");
                    else Json.Append("<p>" + this.Data + "</p>");
                    break;
                case "radio": case "checkgroup":
                    Json.Append("<table><tbody>");
                    if (this.Display == "inline") {
                        Json.Append("<tr>");
                        foreach (XmlPrintOption option in this.Option) Json.Append("<td>" + option.GetJson(this.Data) + "</td>");
                        Json.Append("</tr>");
                    } else if (this.Cols > 1) {
                        Json.Append("<tr>");
                        if (this.Label.IsNotNullOrEmpty()) Json.Append("<td class='strong' colspan='" + this.Cols + "'>" + this.CleanForJson(this.Label) + "</td>");
                        Json.Append("</tr><tr>");
                        for (int i = 0; i < this.Option.Count(); i++) {
                            if (i != 0 && i % this.Cols == 0) Json.Append("</tr><tr>");
                            Json.Append("<td>" + this.Option[i].GetJson(this.Data) + "</td>");
                        }
                        Json.Append("</tr>");
                    } else {
                        if (this.Label.IsNotNullOrEmpty()) Json.Append("<td class='strong'>" + this.CleanForJson(this.Label) + "</td>");
                        foreach (XmlPrintOption option in this.Option) Json.Append("<tr><td>" + option.GetJson(this.Data) + "</td></tr>");
                    }
                    Json.Append("</tbody></table>");
                    break;
                case "multiple":
                    Json.Append("<table><tbody><tr><td class='strong'>" + (this.Oasis.IsNotNullOrEmpty() ? "(" + this.CleanForJson(this.Oasis) + ") " : "") + this.CleanForJson(this.Label) + "</td><td>");
                    for (int i = 0; i < this.Subquestion.Count(); i++) {
                        if (i > 0 && this.Display == "inline") Json.Append("</td><td>");
                        Json.Append(this.Subquestion[i].GetJson());
                    }
                    Json.Append("</td></tr></tbody></table>");
                    break;
                case "chart":
                    Json.Append("<table><tbody><tr>");
                    if (this.Label.IsNotNullOrEmpty()) Json.Append("<td class='strong' colspan='" + this.Cols + "'>" + this.CleanForJson(this.Label) + "</td>");
                    for (int i = 0; i < this.Subquestion.Count(); i++) {
                        if (i != 0 && i % this.Cols == 0) Json.Append("</tr><tr>");
                        Json.Append("<td>" + this.Subquestion[i].GetJson() + "</td>");
                    }
                    Json.Append("</tr></tbody></table>");
                    break;
                case "label":
                    if (this.Label.IsNotNullOrEmpty()) Json.Append("<span class='strong" + (this.Align == "right" ? " ar" : "") + "'>" + this.CleanForJson(this.Label) + "</span>");
                    if (this.Instructions.IsNotNullOrEmpty()) Json.Append("<p>" + this.CleanForJson(this.Instructions) + "</p>");
                    break;
                case "title":
                    Json.Append("<span class='strong'>" + this.CleanForJson(this.Label) + "</span>");
                    break;
                case "notacheck": case "checkbox": case "radioopt":
                    Json.Append("<span class='checkbox'>" + (this.Value.Equals(this.Data) ? "X" : "&#160;") + "</span>" + this.CleanForJson(this.Label));
                    break;
                default: 
                    Json.Append("&#160;");
                    break;
            }
            return Json.ToString();
        }
    }
}