﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public static class PatientVisitNoteExtensions
    {
        public static IDictionary<string, NotesQuestion> ToDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.Note.IsNotNullOrEmpty())
            {
                var noteQuestions = patientVisitNote.Note.ToObject<List<NotesQuestion>>();
                if (noteQuestions != null && noteQuestions.Count > 0)
                {
                    noteQuestions.ForEach(n =>
                    {
                        if (n.Name.IsNotNullOrEmpty() && !questions.ContainsKey(n.Name))
                        {
                            questions.Add(n.Name, n);
                        }
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToWoundCareDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            try
            {
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    var noteQuestions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (noteQuestions != null && noteQuestions.Count > 0)
                    {
                        noteQuestions.ForEach(n =>
                        {
                            if (n.Name.IsNotNullOrEmpty() && !questions.ContainsKey(n.Name))
                            {
                                questions.Add(n.Name, n);
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return questions;
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToHHADefaults(this PatientVisitNote patientVisitNote)
        {
            var vitalSigns = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null)
            {
                var questions = patientVisitNote.ToDictionary();

                var names = new string[] { "HHAFrequency"
                    , "PrimaryDiagnosis"
                    , "ICD9M"
                    , "PrimaryDiagnosis1"
                    , "ICD9M1"
                    , "DNR"
                    , "IsDiet"
                    , "Diet"
                    , "Allergies"
                    , "AllergiesDescription"
                    , "IsVitalSignParameter"
                    , "SystolicBPGreaterThan"
                    , "DiastolicBPGreaterThan"
                    , "PulseGreaterThan"
                    , "RespirationGreaterThan"
                    , "TempGreaterThan"
                    , "WeightGreaterThan"
                    , "SystolicBPLessThan"
                    , "DiastolicBPLessThan"
                    , "PulseLessThan"
                    , "RespirationLessThan"
                    , "TempLessThan"
                    , "WeightLessThan" };

                names.ForEach(name =>
                {
                    if (questions.ContainsKey(name) && questions[name] != null)
                    {
                        vitalSigns.Add(name, questions[name]);
                    }
                });
            }
            return vitalSigns;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        {
            string answer = string.Empty;
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                answer = questions[key].Answer.Unclean();
                if (key.IsEqual("ICD9M") || key.IsEqual("ICD9M1"))
                {
                    answer = answer.TrimEndPeriod();
                }
            }
            return answer;
        }

        public static string AnswerForDropDown(this IDictionary<string, NotesQuestion> questions, string key, Dictionary<string, string> possibleAnswers)
        {
            string answer = string.Empty;
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                answer = questions[key].Answer.Unclean();
                foreach (KeyValuePair<string, string> pa in possibleAnswers)
                {
                    if(answer.Equals(pa.Key))
                    {
                        answer = pa.Value;
                        break;
                    }
                }
            }
            return answer;
        }

        public static string AnswerOrDefault(this IDictionary<string, NotesQuestion> questions, string key, string defaultValue)
        {
            string answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty()) return answer;
            else return defaultValue;
        }

        public static string[] AnswerArray(this IDictionary<string, NotesQuestion> questions, string key, char separator)
        {
            string answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty()) return answer.Split(separator);
            else return new string[] { };
        }

        public static string[] AnswerArray(this IDictionary<string, NotesQuestion> questions, string key)
        {
            string answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty()) return answer.Split(',');
            else return new string[] { };
        }
    }
}
