﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;

    public static class VisitNoteExtensions
    {
        public static IDictionary<string, NotesQuestion> ToNotesQuestionDictionary(this Assessment assessment)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (assessment != null && assessment.Questions != null)
            {
                assessment.Questions.ForEach(question =>
                {
                    if (!questions.ContainsKey(question.Name))
                    {
                        questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToDiagnosisQuestionDictionary(this Assessment assessment)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (assessment != null && assessment.Questions != null)
            {
                assessment.Questions.ForEach(question =>
                {
                    if (question.Name.Equals("PrimaryDiagnosis") || question.Name.Equals("PrimaryDiagnosis1") || question.Name.Equals("ICD9M") || question.Name.Equals("ICD9M1"))
                    {
                        if (!questions.ContainsKey(question.Name))
                        {
                            questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                        }
                        else
                        {
                            questions[question.Name].Answer = question.Answer;
                        }
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToSpecificQuestionDictionary(this Assessment assessment, SectionQuestionType type)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (assessment != null && assessment.Questions != null)
            {
                assessment.Questions.ForEach(question =>
                {
                    if ((type & SectionQuestionType.Diagnoses) == SectionQuestionType.Diagnoses)
                    {
                        if (question.Name.Equals("PrimaryDiagnosis") || question.Name.Equals("PrimaryDiagnosis1") || question.Name.Equals("ICD9M") || question.Name.Equals("ICD9M1"))
                        {
                            if (!questions.ContainsKey(question.Name))
                            {
                                questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                            }
                            else
                            {
                                questions[question.Name].Answer = question.Answer;
                            }
                        }
                    }
                    if ((type & SectionQuestionType.HomeBoundStatus) == SectionQuestionType.HomeBoundStatus)
                    {
                        if (question.Name.Equals("HomeBoundReason") || question.Name.Equals("OtherHomeBoundDetails"))
                        {
                            if (!questions.ContainsKey(question.Name))
                            {
                                questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                            }
                            else
                            {
                                questions[question.Name].Answer = question.Answer;
                            }
                        }
                    }
                    if ((type & SectionQuestionType.PainProfile) == SectionQuestionType.PainProfile)
                    {
                        if (question.Name.Equals("DurationOfPain") || question.Name.Equals("IntensityOfPain") ||
                            question.Name.Equals("QualityOfPain") || question.Name.Equals("LocationOfPain") ||
                            question.Name.Equals("MedicationEffectiveness")) 
                        {
                            if (!questions.ContainsKey(question.Name))
                            {
                                questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                            }
                            else
                            {
                                questions[question.Name].Answer = question.Answer;
                            }
                        }
                    }
                    
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> CombineOasisQuestionsAndNoteQuestions(this IDictionary<string, NotesQuestion> oasisQuestions, IDictionary<string, NotesQuestion> noteQuestions)
        {
            var questions = oasisQuestions;
            if (noteQuestions.ContainsKey("TimeIn") && noteQuestions["TimeIn"] != null)
            {
                questions.Add("TimeIn", noteQuestions["TimeIn"]);
            }
            if (noteQuestions.ContainsKey("TimeOut") && noteQuestions["TimeOut"] != null)
            {
                questions.Add("TimeOut", noteQuestions["TimeOut"]);
            }
            if (noteQuestions.ContainsKey("Surcharge") && noteQuestions["Surcharge"] != null)
            {
                questions.Add("Surcharge", noteQuestions["Surcharge"]);
            }
            if (noteQuestions.ContainsKey("AssociatedMileage") && noteQuestions["AssociatedMileage"] != null)
            {
                questions.Add("AssociatedMileage", noteQuestions["AssociatedMileage"]);
            }
            if (oasisQuestions.ContainsKey("HomeBoundReason") && oasisQuestions["HomeBoundReason"] != null && oasisQuestions["HomeBoundReason"].Answer.IsNotNullOrEmpty())
            {
                var reasons = oasisQuestions["HomeBoundReason"].Answer.ToArray();
                if (reasons != null && reasons.Length > 0)
                {
                    string value = reasons.Aggregate((a, b) => (a.IsInteger() ? (a.ToInteger() + 1).ToString() : a) + "," + (b.ToInteger() + 1));  //Select(s => s.ToInteger() + 1).tostr;
                    questions.Remove("HomeBoundReason");
                    questions.Add("HomeBoundReason", NotesQuestion.Create("HomeBoundReason", value));
                }
            }
            return questions;
        }
    }
}

