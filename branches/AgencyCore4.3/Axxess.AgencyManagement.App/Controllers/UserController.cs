﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Security;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.iTextExtension;

    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.Log.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IMembershipService membershipService;
        private readonly IPatientService patientService;

        public UserController(IAgencyManagementDataProvider dataProvider, IUserService userService, IMembershipService membershipService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.userRepository = dataProvider.UserRepository;
            this.patientService = patientService;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount)
            {
                return PartialView(true);
            }
            return PartialView(false);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewContent(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            }
            else
            {
                user = new User();
            }
            return PartialView("NewContent", user);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");
            var viewData = new JsonViewData();
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User cannot be added because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
            }
            else if (!userService.IsEmailAddressInUse(user.EmailAddress))
            {
                if (user.IsValid)
                {
                    user.AgencyId = Current.AgencyId;
                    user.AgencyName = Current.AgencyName;
                    if (userService.CreateUser(user))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User was saved successfully";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the new User.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = user.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "This E-mail Address is already in use for your agency";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(userRepository.Get(Id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();

            if (user.IsValid)
            {
                user.AgencyId = Current.AgencyId;
                if (userRepository.Update(user))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User was saved successfully";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the new User.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicense([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.AddLicense(license, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return PartialView("JsonResult", viewData.ToJson());
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLicense(Guid Id, Guid userId, DateTime ExpirationDate, string LicenseNumber)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.UpdateLicense(Id, userId, ExpirationDate, LicenseNumber))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId, true)));
            }
            return View(new GridModel(new List<License>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicense(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (userService.DeleteLicense(Id, userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User license has been successfully deleted.";
                return Json(viewData);
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicenseFromGrid(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.DeleteLicense(Id, userId))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId, true)));
            }
            return View(new GridModel(new List<License>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePermissions(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (userService.UpdatePermissions(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permissions updated successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Inactive))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeactivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Try Again." };

            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User can't be activated because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
            }
            else
            {
                if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Active))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User has been activated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Try Again." };
            if (userService.DeleteUser(userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            ViewData["UserScheduleGroupName"] = "VisitDate";
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleGrouped(string groupName, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["UserScheduleGroupName"] = groupName;
            return PartialView("ScheduleGrid", userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList()
        {
            var pageNumber = this.HttpContext.Request.Params["page"];
            return View(new GridModel(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
            return Json(userService.GetScheduleWidget(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile()
        {
            return PartialView("Profile/Edit", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Profile([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your profile could not be saved." };

            if (user.IsValid)
            {
                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }
                else
                {
                    if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !userService.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature provided does not match the one on file.";
                    }
                    else
                    {
                        if (userService.UpdateProfile(user))
                        {

                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your profile has been updated successfully.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult All()
        {
            return Json(userRepository.GetAgencyUsers(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            var users = userService.GetUsersByStatus(Guid.Empty, -1);
            if (users != null && users.Count() > 0)
            {
                ViewData["SortColumn"] = "PatientName";
                ViewData["SortDirection"] = "ASC";
            }

            return PartialView("List", users);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActiveContent(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(userService.GetUsersByStatus(Guid.Empty, (int)UserStatus.Active));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InActiveContent(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(userService.GetUsersByStatus(Guid.Empty, (int)UserStatus.Inactive));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid branchId, int status)
        {
            var users = userRepository.GetUsersByStatusLean(Current.AgencyId, branchId, status) ?? new List<User>();
            return Json(users.Select(u => new { Id = u.Id, Name = u.DisplayName }).ToList());
        }


        [GridAction]
        public ActionResult LicenseList(Guid userId)
        {
            return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId, true)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotSignature()
        {
            return PartialView("Signature/Forgot", Current.User.Name);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LicenseManager()
        {
            return PartialView("License/Manager", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseManagerList()
        {
            return PartialView("License/List", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLicenseItem()
        {
            return PartialView("License/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicenseItem([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "license");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be added." };

            var entityValidator = new EntityValidator(
               new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
               new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
               new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
            );
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                if (userService.AddLicenseItem(licenseItem, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "License added successfully";
                }
            }
            else
            {
                viewData.errorMessage = entityValidator.Message;
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLicenseItem(Guid licenseId, Guid userId)
        {
            var licenseItem = new LicenseItem();
            if (userId.IsEmpty())
            {
                licenseItem = userRepository.GetNonUserLicense(licenseId, Current.AgencyId);
            }
            else
            {
                licenseItem = userRepository.GetUserLicenseItem(licenseId, userId, Current.AgencyId);
            }
            return PartialView("License/Edit", licenseItem);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateLicenseItem([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "licenseItem");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The License could not be updated. Please try again." };
            if (licenseItem != null)
            {
                if (!licenseItem.UserId.IsEmpty())
                {
                    if (userService.UpdateLicense(licenseItem.Id, licenseItem.UserId, licenseItem.IssueDate, licenseItem.ExpireDate))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.UserId.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The License was updated successfully.";
                    }
                }
                else
                {
                    var entityValidator = new EntityValidator(
                       new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
                       new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
                       new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
                    );
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        licenseItem.AgencyId = Current.AgencyId;
                        if (userRepository.UpdateNonUserLicense(licenseItem))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The License was updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GoToMeeting()
        {
            return PartialView("Help/GoToMeeting", string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Webinars()
        {
            return PartialView("Help/Webinars", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailSignature()
        {
            var viewData = new JsonViewData();

            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Signature could not be reset. Please try again later.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserCalendar()
        {
            var fromDate = DateUtilities.GetStartOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            return PartialView("Calendar", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UserCalendarPdf(DateTime month)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month.Month, month.Year);
            var toDate = DateUtilities.GetEndOfMonth(month.Month, month.Year);
            MonthCalendarPdf doc = new MonthCalendarPdf(new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
            var PdfStream = doc.GetStream();
            PdfStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MonthlyCalendar_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(PdfStream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserCalendarNavigate(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("Calendar", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserVisits(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("CalendarGrid", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserLogs(Guid userId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.User, Current.AgencyId, userId.ToString()));
        }

        [GridAction]
        public ActionResult Rates(Guid userId)
        {
            return View(new GridModel(userService.GetUserRates(userId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewUserRate(Guid userId)
        {
            return PartialView("Rate/New", userId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddUserRate([Bind]UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be added. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "User rate already exists.";
                            }
                            else
                            {
                                rates.Add(userRate);
                                user.Rates = rates.ToXml();
                                if (userRepository.UpdateModel(user,false))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate added successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<UserRate>();
                            rates.Add(userRate);
                            user.Rates = rates.ToXml();
                            if (userRepository.UpdateModel(user, false))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate added successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<UserRate>();
                        rates.Add(userRate);
                        user.Rates = rates.ToXml();
                        if (userRepository.UpdateModel(user, false))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User rate added successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUserRate(Guid userId, int Id, string insurance)
        {
            UserRate userRate = null;
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                var rates = user.Rates.ToObject<List<UserRate>>();
                userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance == insurance);
                if (userRate != null)
                {
                    userRate.UserId = user.Id;
                    userRate.InsuranceName = patientService.GetInsurance(userRate.Insurance);
                }
                else
                {
                    userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                    if (userRate != null)
                    {
                        userRate.UserId = user.Id;
                        userRate.InsuranceName = patientService.GetInsurance(userRate.Insurance);
                    }
                    else
                    {
                        userRate = new UserRate();
                        userRate.Id = Id;
                        userRate.UserId = user.Id;
                        userRate.Insurance = string.Empty;
                    }
                }
            }
            return PartialView("Rate/Edit", userRate);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateUserRate([Bind] UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be updated. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);

                            if (rate == null)
                            {
                                rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                                if (rate != null)
                                {
                                    rate.Rate = userRate.Rate;
                                    rate.RateType = userRate.RateType;
                                    rate.MileageRate = userRate.MileageRate;
                                    rate.Insurance = userRate.Insurance;
                                    user.Rates = rates.ToXml();
                                    if (userRepository.UpdateModel(user, false))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "User rate updated successfully";
                                    }
                                }
                            }
                            else
                            {
                                rate.Rate = userRate.Rate;
                                rate.RateType = userRate.RateType;
                                rate.MileageRate = userRate.MileageRate;
                                rate.Insurance = userRate.Insurance;
                                user.Rates = rates.ToXml();
                                if (userRepository.UpdateModel(user, false))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate updated successfully";
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoadUserRate(Guid fromId, Guid toId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to load rate. Please try again." };
            if (userService.LoadUserRate(fromId, toId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User rate loaded successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteUserRate(Guid userId, int id, string insurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Rates.IsNotNullOrEmpty())
                {
                    var rates = user.Rates.ToObject<List<UserRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == id && r.Insurance == insurance);
                        if (removed > 0)
                        {
                            user.Rates = rates.ToXml();
                            if (userRepository.UpdateModel(user, false))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate deleted successfully";
                            }
                        }
                        else
                        {
                            removed = rates.RemoveAll(r => r.Id == id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                            if (removed > 0)
                            {
                                user.Rates = rates.ToXml();
                                if (userRepository.UpdateModel(user, false))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate deleted successfully";
                                }
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }
        #endregion
    }
}
