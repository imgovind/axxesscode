﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using System.Text;

    using Telerik.Web.Mvc;

    using Services;
    using ViewData;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;


    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

   
    using Axxess.LookUp.Domain;
    using System.Web.Script.Serialization;
    using Axxess.AgencyManagement.App.Common;
    using Axxess.AgencyManagement.App.Domain;
   

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Constructor / Member

        private readonly IUserService userService;
        private readonly IDateService dateService;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientService patientService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly ICachedDataRepository cachedDataRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;
        private readonly IScheduleRepository scheduleRepository;

        public OasisController(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IAssessmentService assessmentService, IPatientService patientService, IUserService userService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.cachedDataRepository = oasisDataProvider.CachedDataRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
        }

        #endregion

        #region OasisController Actions

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Assessment(FormCollection formCollection)
        {
              var oasisViewData = new OasisViewData { isSuccessful = false };
              if (formCollection != null)
              {
                  var keys = formCollection.AllKeys;
                  if (keys != null && keys.Length > 0)
                  {
                      if (keys.Contains("assessment"))
                      {
                          var assessmentType = formCollection["assessment"];
                          if (assessmentType.IsNotNullOrEmpty())
                          {
                              oasisViewData = assessmentService.SaveAssessment(formCollection, Request.Files, assessmentType);
                          }
                          else
                          {
                              oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                          }
                      }
                      else
                      {
                          oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                      }
                  }
                  else
                  {
                      oasisViewData.errorMessage = "There is an error sending the data. Try again.";
                  }
              }
              else
              {
                  oasisViewData.errorMessage = "There is an error sending the data. Try again.";
              }
              var serializer = new JavaScriptSerializer();
              var jsonString = serializer.Serialize(new { PatientId = oasisViewData.Assessment != null ? oasisViewData.Assessment.PatientId : Guid.Empty, EpisodeId = oasisViewData.Assessment != null ? oasisViewData.Assessment.EpisodeId : Guid.Empty, AssessmentId = oasisViewData.assessmentId, IsSuccessful = oasisViewData.isSuccessful, IsCaseManagementRefresh = oasisViewData.IsCaseManagementRefresh, IsMyScheduleTaskRefresh = oasisViewData.IsMyScheduleTaskRefresh, IsDataCentersRefresh = oasisViewData.IsDataCentersRefresh, errorMessage = oasisViewData.errorMessage });
              return PartialView("JsonResult", jsonString);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return Json(assessmentService.GetAssessment(Id, assessmentType).ToDictionary());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult View(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            var assessment = assessmentService.GetAssessmentWithScheduleType(PatientId, Id);
            if (assessment != null)
            {
                assessment.StatusComment = patientService.GetReturnComments(assessment.Id, assessment.EpisodeId, assessment.PatientId);
            }
            return PartialView(string.Format("Assessments/{0}{1}", assessment != null ? assessment.Type.ToString() : string.Empty, assessment.Version > 0 ? assessment.Version.ToString() : string.Empty), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(assessmentId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotEmpty(previousAssessmentId, "previousAssessmentId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Previous Assessment data could not be saved." };
            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, assessmentId);
            if (scheduleEvent != null)
            {
                if (oasisAssessmentRepository.UsePreviousAssessment(Current.AgencyId, episodeId, patientId, assessmentId, assessmentType, previousAssessmentId, previousAssessmentType))
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisSaved);
                    if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Assessment was successfully loaded from the previous assessment.";
                        viewData.IsActivityRefresh = oldStatus != scheduleEvent.Status;
                        viewData.PatientId = scheduleEvent.PatientId;
                        viewData.EpisodeId = scheduleEvent.EpisodeId;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Assessments/OASISPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId)), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PdfLimited(Guid episodeId, Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId),false), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult BlankPdf(int AssessmentType, string Discipline)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(Enum.IsDefined(typeof(AssessmentType), AssessmentType)?(AssessmentType)AssessmentType: Axxess.OasisC.Enums.AssessmentType.None), Discipline), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ProfilePdf(Guid Id, Guid patientId, Guid episodeId)
        {
           return FileGenerator.Pdf<OasisProfilePdf>(new OasisProfilePdf(assessmentService.OASISProfileData(patientId, episodeId, Id)), "OASISProfile");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ProfilePrint(Guid Id, Guid patientId, Guid episodeId)
        {
            var note = assessmentService.OASISProfileData(patientId, episodeId, Id) ?? new AssessmentPrint();
            var xml = new OasisProfileXml(note);
            note.PrintViewJson = xml.GetJson();
            if (note.Data != null)
            {
                note.Data.Clear();
            }
            return View("Profile", note);
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            var assessment = assessmentService.GetAssessmentWithScheduleType(PatientId, Id);
            var version = string.Empty;
            var categoryName = string.Empty;
            if (Category.IsNotNullOrEmpty())
            {
                var categoryArray = Category.Split('_');
                if (categoryArray != null && categoryArray.Length > 0)
                {
                    if (categoryArray.Length >= 2)
                    {
                        categoryName = categoryArray[1];
                        if (categoryArray.Length >= 3)
                        {
                            version = categoryArray[2];
                        }
                    }
                }
            }
            if (assessment != null)
            {
                if (categoryName.IsNotNullOrEmpty())
                {
                    if (categoryName.IsEqual(AssessmentCategory.Medications.ToString()))
                    {
                        var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                        if (medicationProfile != null)
                        {
                            assessment.MedicationProfile = medicationProfile.ToXml();
                        }
                    }
                    else if (categoryName.Contains(AssessmentCategory.PatientHistory.ToString()))
                    {
                        var allergyProfile = patientRepository.GetAllergyProfileByPatient(PatientId, Current.AgencyId);
                        if (allergyProfile != null)
                        {
                            assessment.AllergyProfile = allergyProfile.ToXml();
                        }
                    }
                    if (categoryName.IsEqual(AssessmentCategory.TransferDischargeDeath.ToString()) || categoryName.IsEqual(AssessmentCategory.OrdersDisciplineTreatment.ToString()) || (assessment.Type.IsEqual(OasisC.Enums.AssessmentType.FollowUp.ToString()) && categoryName.IsEqual(AssessmentCategory.TherapyNeed.ToString())))
                    {
                        assessment.IsLastTab = true;
                    }
                }
            }
            return PartialView(string.Format("Assessments/Tabs/{0}{1}", categoryName, version), assessment);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "mooCode")]
        public JsonResult Guide(string mooCode)
        {
            if (mooCode.IsNullOrEmpty())
            {
                return Json(new OasisGuide());
            }
            else
            {
                var result = cachedDataRepository.GetOasisGuide(mooCode);
                return Json(result != null ? result : new OasisGuide());
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Validate(Guid Id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return View("~/Views/Oasis/Validation.aspx", assessmentService.Validate(Id, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Regenerate(Guid Id, Guid patientId, Guid episodeId, string assessmentType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The assessment has an error to validate." };
            var assessment = assessmentService.GetAssessment(episodeId, patientId, Id, assessmentType);
            if (assessment != null)
            {
                if (assessmentService.Validate(assessment))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The assessment data successfully generated.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Inactivate(Guid Id, string type)
        {
            return Json(assessmentService.ValidateInactivate(Id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult GenerateForCancel(Guid Id, string type)
        {
            var assessment = oasisAssessmentRepository.Get(Id, type, Current.AgencyId);
            var agencyLocation = new AgencyLocation();
            if (assessment != null)
            {
                var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    agencyLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                    if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                }
            }
            string generateOasisHeader = assessmentService.OasisHeader(agencyLocation);

            var generateJsonOasis = string.Empty;
            var hl = generateOasisHeader.Length;


            if (assessment != null && assessment.CancellationFormat.IsNotNullOrEmpty())
            {
                generateJsonOasis = assessment.CancellationFormat + "\r\n";
            }

            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(3);
            var fl = generateOasisFooter.Length;
            var encoding = new UTF8Encoding();
            string allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
            byte[] buffer = encoding.GetBytes(allString);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, allString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis{0}.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult Generate(Guid BranchId, List<string> OasisSelected)
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, BranchId);
            if (agency != null && agencyLocation != null)
            {
                if (!agencyLocation.IsLocationStandAlone)
                {
                    agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                    agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                    agencyLocation.Name = agency.Name;
                    agencyLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                    agencyLocation.ContactPersonLastName = agency.ContactPersonLastName;
                    agencyLocation.ContactPersonPhone = agency.ContactPersonPhone;
                    agencyLocation.ContactPersonEmail = agency.ContactPersonEmail;

                    if (!agencyLocation.IsMainOffice)
                    {
                        if (agency.MainLocation != null)
                        {
                            agencyLocation.AddressLine1 = agency.MainLocation.AddressLine1;
                            agencyLocation.AddressLine2 = agency.MainLocation.AddressLine2;
                            agencyLocation.AddressCity = agency.MainLocation.AddressCity;
                            agencyLocation.AddressStateCode = agency.MainLocation.AddressStateCode;
                            agencyLocation.AddressZipCode = agency.MainLocation.AddressZipCode;
                        }
                    }
                }
                else
                {
                    agencyLocation.Name = agency.Name;
                }
            }

            string generateOasisHeader = assessmentService.OasisHeader(agencyLocation);
            var generateJsonOasis = string.Empty;
            int count = 0;
            var hl = generateOasisHeader.Length;
            if (OasisSelected != null && OasisSelected.Count > 0)
            {
                OasisSelected.ForEach(o =>
                {
                    string[] data = o.Split('|');
                    var assessment = oasisAssessmentRepository.Get(data[0].ToGuid(), data[1], Current.AgencyId);
                    if (assessment != null && assessment.SubmissionFormat != null)
                    {
                        generateJsonOasis += assessment.SubmissionFormat + "\r\n";
                        count++;
                    }
                });
            }
            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(count + 2);
            var fl = generateOasisFooter.Length;
            UTF8Encoding encoding = new UTF8Encoding();
            string allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
            byte[] buffer = encoding.GetBytes(allString);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, allString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis{0}.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult GenerateExportFile(Guid assessmentId, string assessmentType)
        {
            var exportString = "Export File could not be created!";
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                var assessment = assessmentService.GetAssessment(assessmentId, assessmentType);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, patient != null ? patient.AgencyLocationId : Guid.Empty);
                            if (agencyLocation != null)
                            {
                                if (!agencyLocation.IsLocationStandAlone)
                                {
                                    agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                    agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                    agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                    agencyLocation.Name = agency.Name;
                                    agencyLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                                    agencyLocation.ContactPersonLastName = agency.ContactPersonLastName;
                                    agencyLocation.ContactPersonPhone = agency.ContactPersonPhone;
                                    agencyLocation.ContactPersonEmail = agency.ContactPersonEmail;

                                    if (!agencyLocation.IsMainOffice)
                                    {
                                        if (agency.MainLocation != null)
                                        {
                                            agencyLocation.AddressLine1 = agency.MainLocation.AddressLine1;
                                            agencyLocation.AddressLine2 = agency.MainLocation.AddressLine2;
                                            agencyLocation.AddressCity = agency.MainLocation.AddressCity;
                                            agencyLocation.AddressStateCode = agency.MainLocation.AddressStateCode;
                                            agencyLocation.AddressZipCode = agency.MainLocation.AddressZipCode;
                                        }
                                    }
                                }
                                else
                                {
                                    agencyLocation.Name = agency.Name;
                                }


                                var header = assessmentService.OasisHeader(agencyLocation);

                                // var submissionBodyFormat = assessmentService.GetOasisSubmissionFormatInstructionsNew();
                                var body = assessmentService.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, agencyLocation);

                                var footer = assessmentService.OasisFooter(3);

                                exportString = header + body + "\r\n" + footer;
                            }
                        }
                    }
                }
            }
            var encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(exportString);
            var fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, exportString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OasisExport.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Correction(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var export = new OasisExport();
            export.AssessmentId = Id;
            export.PatientId = PatientId;
            export.EpisodeId = EpisodeId;
            export.CorrectionNumber = CorrectionNumber;
            export.AssessmentType = Type;
            return PartialView(export);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CorrectionChange(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Correction Number could not be updated." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty() && !EpisodeId.IsEmpty() && Type.IsNotNullOrEmpty())
            {
                if (assessmentService.UpdateAssessmentCorrectionNumber(Id, PatientId, EpisodeId, Type, CorrectionNumber))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Correction Number is updated.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AuditPdf(Guid id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return FileGenerator.Pdf<OasisAuditPdf>(new OasisAuditPdf(assessmentService.Audit(id, patientId, episodeId, assessmentType)), "OASISLogicalCheck");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PpsExport(Guid assessmentId, string assessmentType)
        {
            var result = new JsonViewData { isSuccessful = false, errorMessage = "The OASIS Export File could not be sent to PPS Plus." };
            var exportString = "Export File could not be created!";
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                var assessment = assessmentService.GetAssessment(assessmentId, assessmentType);
                if (assessment != null)
                {
                    var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                            if (patientLocation != null)
                            {
                                if (!patientLocation.IsLocationStandAlone)
                                {
                                    patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                    patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                    patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                }

                                var assessmentQuestions = assessment.ToDictionary();
                                //var submissionBodyFormat = assessmentService.GetOasisSubmissionFormatInstructionsNew();
                                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                {
                                    exportString = assessmentService.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                    if (exportString.IsNotNullOrEmpty())
                                    {
                                        PpsPlus.PPSPlusServiceSoapClient ppsPlusClient = new Axxess.AgencyManagement.App.PpsPlus.PPSPlusServiceSoapClient();
                                        var assessmentXml = string.Format(
                                            "<?xml version=\"1.0\" encoding=\"utf-8\"?><Assessments><Assessment><OASIS><Data>{0}</Data><ID>{1}</ID></OASIS></Assessment></Assessments>",
                                            exportString,
                                            assessment.Id);
                                        var serviceResult = ppsPlusClient.SendAssessments(agency.OasisAuditVendorApiKey, assessmentXml);
                                        if (serviceResult != null)
                                        {
                                            if (serviceResult.ResultCode == Axxess.AgencyManagement.App.PpsPlus.ResultCodes.Success)
                                            {
                                                result.isSuccessful = true;
                                                result.errorMessage = "The OASIS Export file has been successfully sent to PPS Plus.";
                                            }
                                            else
                                            {
                                                result.isSuccessful = false;
                                                result.errorMessage = serviceResult.ErrorMessage;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Submit(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var message = "Your assessment was not submitted.";
            if (actionType == "Submit")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisCompletedPendingReview, string.Empty);
                if (viewData.isSuccessful)
                {
                    if (Current.HasRight(Permissions.BypassCaseManagement))
                    {
                        if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString()
                            || assessmentType == DisciplineTasks.OASISCDischarge.ToString()
                            || assessmentType == DisciplineTasks.OASISCDischargePT.ToString()
                            || assessmentType == "DischargeFromAgency")
                           || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString()
                           || assessmentType == "OASISCTransferDischarge")
                           || (assessmentType == DisciplineTasks.OASISCTransferDischargePT.ToString()
                           || assessmentType == "OASISCTransferDischargePT")
                           || (assessmentType == DisciplineTasks.OASISCDeath.ToString()
                           || assessmentType == DisciplineTasks.OASISCDeathOT.ToString()
                           || assessmentType == DisciplineTasks.OASISCDeathPT.ToString()
                           || assessmentType == "OASISCDeath"))
                        {
                            var assessment = oasisAssessmentRepository.Get(Id, assessmentType, Current.AgencyId);
                            if (assessment != null)
                            {
                                var assessmentData = assessment.ToDictionary();
                                var schedule = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, Id);
                                if (schedule != null)
                                {
                                    var date = DateTime.MinValue;
                                    var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                    if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                    var eventDateSchedule = schedule.EventDate;
                                    if ( eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                    var visitDateSchedule = schedule.VisitDate;
                                    if (visitDateSchedule.IsValid())
                                    {
                                        date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                    }
                                    if (date.Date > DateTime.MinValue.Date)
                                    {
                                        var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to discharge OASIS.");
                                        if (success.isSuccessful)
                                        {
                                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var schedule = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, Id);
                                if (schedule != null)
                                {
                                    var date = DateTime.MinValue;
                                    var eventDateSchedule = schedule.EventDate;
                                    if ( eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                    var visitDateSchedule = schedule.VisitDate;
                                    if ( visitDateSchedule.IsValid())
                                    {
                                        date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                    }
                                    if (date.Date > DateTime.MinValue.Date)
                                    {
                                        var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to Discharge OASIS.");
                                        if (success.isSuccessful)
                                        {
                                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment was submitted successfully.";
                }
            }
            else if (actionType == "Approve")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisCompletedExportReady, string.Empty);
                if (viewData.isSuccessful)
                {
                    if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString()
                        || assessmentType == DisciplineTasks.OASISCDischarge.ToString()
                        || assessmentType == DisciplineTasks.OASISCDischargePT.ToString()
                        || assessmentType == "DischargeFromAgency")
                        || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString()
                        || assessmentType == "OASISCTransferDischarge")
                        || (assessmentType == DisciplineTasks.OASISCTransferDischargePT.ToString()
                        || assessmentType == "OASISCTransferDischargePT")
                        || (assessmentType == DisciplineTasks.OASISCDeath.ToString()
                        || assessmentType == DisciplineTasks.OASISCDeathOT.ToString()
                        || assessmentType == DisciplineTasks.OASISCDeathPT.ToString()
                        || assessmentType == "OASISCDeath"))
                    {
                        var assessment = oasisAssessmentRepository.Get(Id, assessmentType, Current.AgencyId);
                        if (assessment != null)
                        {
                            var assessmentData = assessment.ToDictionary();
                            var schedule = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, Id);
                            if (schedule != null)
                            {
                                var date = DateTime.MinValue;
                                var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                var eventDateSchedule = schedule.EventDate;
                                if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                var visitDateSchedule = schedule.VisitDate;
                                if ( visitDateSchedule.IsValid())
                                {
                                    date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                }
                                if (date.Date > DateTime.MinValue.Date)
                                {
                                    var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to discharge OASIS.");
                                    if (success.isSuccessful)
                                    {
                                        viewData.IsCenterRefresh = success.IsCenterRefresh;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var schedule = scheduleRepository.GetScheduleEventNew (Current.AgencyId, patientId, episodeId, Id);
                            if (schedule != null)
                            {
                                var date = DateTime.MinValue;
                                var eventDateSchedule = schedule.EventDate;
                                if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                var visitDateSchedule = schedule.VisitDate;
                                if (visitDateSchedule.IsValid())
                                {
                                    date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                }
                                if (date.Date > DateTime.MinValue.Date)
                                {
                                    var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to Discharge OASIS.");
                                    if (success.isSuccessful)
                                    {
                                        viewData.IsCenterRefresh = success.IsCenterRefresh;
                                    }
                                }
                            }
                        }
                    }
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been approved.";
                }
            }
            else if (actionType == "Return")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisReturnedForClinicianReview, reason);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been returned.";
                }
            }
            else if (actionType == "Exported")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisExported, string.Empty);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been exported.";
                }
            }
            else if (actionType == "ReOpen")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisReopened, reason);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been reopened.";
                }
            }
            else if (actionType == "CompletedNotExported")
            {
                viewData = assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, (int)ScheduleStatus.OasisCompletedNotExported, string.Empty);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been completed ( not exported ).";
                }
            }
            if (!viewData.isSuccessful)
            {
                viewData.errorMessage = message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SubmitOnly(FormCollection formCollection)
        {
            var viewData = new OasisViewData { isSuccessful = false }; ///{ isSuccessful = false, errorMessage = "Your Assessment could not be submitted." };
            string message = "Your Assessment could not be submitted.";
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                string assessmentType = keys.Contains("OasisValidationType") && formCollection["OasisValidationType"].IsNotNullOrEmpty() ? formCollection["OasisValidationType"] : string.Empty;
                if (assessmentType.IsNotNullOrEmpty())
                {
                    var assessmentId = keys.Contains(string.Format("{0}_Id", assessmentType)) && formCollection[string.Format("{0}_Id", assessmentType)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_Id", assessmentType)].IsGuid() ? formCollection.Get(string.Format("{0}_Id", assessmentType)).ToGuid() : Guid.Empty;
                    var episodeId = keys.Contains(string.Format("{0}_EpisodeId", assessmentType)) && formCollection[string.Format("{0}_EpisodeId", assessmentType)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_EpisodeId", assessmentType)].IsGuid() ? formCollection.Get(string.Format("{0}_EpisodeId", assessmentType)).ToGuid() : Guid.Empty;
                    var patientId = keys.Contains(string.Format("{0}_PatientId", assessmentType)) && formCollection[string.Format("{0}_PatientId", assessmentType)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_PatientId", assessmentType)].IsGuid() ? formCollection.Get(string.Format("{0}_PatientId", assessmentType)).ToGuid() : Guid.Empty;

                    if (!assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        if (episode != null)
                        {
                            rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationClinician", assessmentType)) || (keys.Contains(string.Format("{0}_ValidationClinician", assessmentType)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationClinician", assessmentType)])), "User Signature can't be empty.\n"));
                            rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationClinician", assessmentType)) && formCollection[string.Format("{0}_ValidationClinician", assessmentType)].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[string.Format("{0}_ValidationClinician", assessmentType)]) : false, "User Signature is not correct.\n"));

                            rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationSignatureDate", assessmentType)) || (keys.Contains(string.Format("{0}_ValidationSignatureDate", assessmentType)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)])), "Signature date can't be empty.\n"));
                            rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", assessmentType)) && formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].IsValidDate() ? false : true, "Signature date is not valid.\n"));
                            rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", assessmentType)) && formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].IsValidDate() ? !(formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].ToDateTime() >= episode.StartDate && formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range.\n"));
                            if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                            {
                                rules.Add(new Validation(() => !keys.Contains(assessmentType + "_TimeIn") || (keys.Contains(assessmentType + "_TimeIn") && string.IsNullOrEmpty(formCollection[assessmentType + "_TimeIn"])), "Time-In can't be empty. \n"));
                                rules.Add(new Validation(() => !keys.Contains(assessmentType + "_TimeOut") || (keys.Contains(assessmentType + "_TimeOut") && string.IsNullOrEmpty(formCollection[assessmentType + "_TimeOut"])), "Time-Out can't be empty. \n"));
                            }

                            var entityValidator = new EntityValidator(rules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                viewData = assessmentService.UpdateAssessmentStatusForSubmit(assessmentId, episode, assessmentType, (int)ScheduleStatus.OasisCompletedPendingReview, string.Format("Electronically Signed by: {0}", Current.UserFullName), formCollection[string.Format("{0}_ValidationSignatureDate", assessmentType)].ToDateTime(), formCollection[assessmentType + "_TimeIn"], formCollection[assessmentType + "_TimeOut"]);
                                if (viewData.isSuccessful)
                                {
                                    message = "Your Assessment was submitted successfully.";
                                    if (Current.HasRight(Permissions.BypassCaseManagement))
                                    {
                                        if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString()
                                                || assessmentType == DisciplineTasks.OASISCDischarge.ToString()
                                                || assessmentType == DisciplineTasks.OASISCDischargePT.ToString()
                                                || assessmentType == "DischargeFromAgency")
                                                   || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString()
                                                   || assessmentType == "OASISCTransferDischarge")
                                                   || (assessmentType == DisciplineTasks.OASISCTransferDischargePT.ToString()
                                                   || assessmentType == "OASISCTransferDischargePT")
                                                   || (assessmentType == DisciplineTasks.OASISCDeath.ToString()
                                                   || assessmentType == DisciplineTasks.OASISCDeathOT.ToString()
                                                   || assessmentType == DisciplineTasks.OASISCDeathPT.ToString()
                                                   || assessmentType == "OASISCDeath"))
                                        {
                                            var assessment = oasisAssessmentRepository.Get(assessmentId, assessmentType, Current.AgencyId);
                                            if (assessment != null)
                                            {
                                                var assessmentData = assessment.ToDictionary();
                                                var schedule = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, assessmentId);
                                                if (schedule != null)
                                                {
                                                    var date = DateTime.MinValue;
                                                    var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                                    if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate())
                                                    {
                                                        date = dateAssessment.ToDateTime();
                                                    }
                                                    var eventDateSchedule = schedule.EventDate;
                                                    if (eventDateSchedule.IsValid())
                                                    {
                                                        date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                    }
                                                    var visitDateSchedule = schedule.VisitDate;
                                                    if (visitDateSchedule.IsValid())
                                                    {
                                                        date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                    }
                                                    if (date.Date > DateTime.MinValue.Date)
                                                    {
                                                        var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to discharge OASIS.");
                                                        if (success.isSuccessful)
                                                        {
                                                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                                                            message = "Your Assessment was approved successfully, and the patient was discharged.";
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var schedule = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, assessmentId);
                                                if (schedule != null)
                                                {
                                                    var date = DateTime.MinValue;
                                                    var eventDateSchedule = schedule.EventDate;
                                                    if ( eventDateSchedule.IsValid())
                                                    {
                                                        date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                    }
                                                    var visitDateSchedule = schedule.VisitDate;
                                                    if (visitDateSchedule.IsValid())
                                                    {
                                                        date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                    }
                                                    if (date.Date > DateTime.MinValue.Date)
                                                    {
                                                        var success = patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to Discharge OASIS.");
                                                        if (success.isSuccessful)
                                                        {
                                                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                                                            message = "Your Assessment was approved successfully, and the patient was discharged.";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = message;
                                }
                                else
                                {
                                    viewData.errorMessage = message;
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = entityValidator.Message;
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = message;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = message;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = message;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisSignature(Guid Id, Guid PatientId, Guid EpisodeId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("NonOasisSignature", assessmentService.GetAssessmentWithScheduleType(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkExported(List<string> OasisSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The selected OASIS Assessments status could not be changed.";
            if (OasisSelected != null && OasisSelected.Count > 0)
            {
                if (StatusType.IsNotNullOrEmpty() && (StatusType.IsEqual("Exported") || StatusType.IsEqual("CompletedNotExported")))
                {
                    viewData = assessmentService.MarkAsExportedOrCompleted(OasisSelected, StatusType.IsEqual("Exported") ? (int)ScheduleStatus.OasisExported : (int)ScheduleStatus.OasisCompletedNotExported);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The selected OASIS Assessments have been  marked as " + (StatusType.IsEqual("Exported") ? "Exported." : "Completed ( Not Exported ).");
                    }
                    else
                    {
                        viewData.errorMessage = errorMessage;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected OASIS status change is not successfully . Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Select OASIS you want to change the status.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportView()
        {
            ViewData["SortColumn"] = "Index";
            ViewData["SortDirection"] = "ASC";
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("OasisExport", assessmentService.GetAssessmentByStatus(location != null ? location.Id : Guid.Empty, ScheduleStatus.OasisCompletedExportReady, new List<int> { 1 }));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Export(Guid BranchId, string paymentSources, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var paymentSourcesList = new List<int>();
            if (paymentSources.IsNotNullOrEmpty())
            {
                paymentSources.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ForEach(s => paymentSourcesList.Add(s.ToInteger()));
            }
            return PartialView("Content/OasisExport", paymentSourcesList != null && paymentSourcesList.Count > 0 ? assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedExportReady, paymentSourcesList) : new List<AssessmentExport>());

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportedView()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            
            return PartialView("Exported");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NotExportedView()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;

            return PartialView("NotExported");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Exported(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisExported, Status, StartDate, EndDate)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NotExported(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedNotExported, Status, StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = assessmentService.GetPatientAndAgencyInfo(episodeId, patientId, eventId);
            if (viewData == null) viewData = new PlanofCareViewData();
            return View("485/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                if (planofCare.Data.IsNotNullOrEmpty()) planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                var episodeRange = assessmentService.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                if (episodeRange != null)
                {
                    planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                    planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                    ViewData["IsLinkedToAssessment"] = episodeRange.IsLinkedToAssessment;
                }
                planofCare.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId);
                if (planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
                {
                    var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (oldPhysician != null && !oldPhysician.Id.IsEmpty()) planofCare.PhysicianId = oldPhysician.Id;
                }

                return View("485/Edit", planofCare);
            }
            return View("485/Edit", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanofCareContent(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var assessment = oasisAssessmentRepository.Get(planofCare.AssessmentId, planofCare.AssessmentType, Current.AgencyId);
                if (assessment != null)
                {
                    planofCare.Questions = assessmentService.Get485FromAssessment(assessment);
                }
            }
            return PartialView("485/LocatorQuestions", planofCare);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public JsonResult Save485(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The 485 Plan of Care could not be saved.";
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();
                if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                        return Json(viewData);
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                        return Json(viewData);
                    }
                }
                viewData = assessmentService.UpdatePlanofCare(formCollection);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                }
                else
                {
                    viewData.errorMessage = errorMessage;
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SavePlanofCareStandAlone(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved." };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();

                if (statusId == (int)ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            var assessment = assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId);
            return View("485/Print", assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<PlanOfCarePdf>(new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId)), "HCFA-485");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Plan of Care (485) found for this episode." };
            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduleEvent != null)
            {
                var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduleEvent.EventDate);
                if (assessment != null)
                {
                    var pocScheduleEvent = assessmentService.GetPlanofCareScheduleEvent(assessment.EpisodeId, patientId, assessment.Id, assessment.Type.ToString());
                    if (pocScheduleEvent != null)
                    {
                        viewData.isSuccessful = true;
                        viewData.url = "/Oasis/PlanOfCarePdf";
                        viewData.episodeId = pocScheduleEvent.EpisodeId;
                        viewData.patientId = pocScheduleEvent.PatientId;
                        viewData.eventId = pocScheduleEvent.EventId;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanOfCareMedication(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var medProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medProfile != null)
            {
                return View("485/Medication", medProfile);
            }
            return View("485/Medication", new MedicationProfile());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assessmentService.DeleteWoundCareAsset(episodeId, patientId, eventId, assessmentType, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");

            assessmentService.AddSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.UpdateSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.DeleteSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BlankMasterCalendar(Guid episodeId, Guid patientId, string assessmentType)
        {
            var episode = patientRepository.GetPatientEpisodeLean(Current.AgencyId, episodeId, patientId);
            var modelData = new EpisodeDateViewData();
            if (episode != null)
            {
                if (assessmentType.IsEqual("Recertification"))
                {
                    var nextEpisode = patientRepository.GetNextEpisodeByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
                    if (nextEpisode != null && episode.EndDate.AddDays(1).Date == nextEpisode.StartDate.Date)
                    {
                        modelData.StartDate = nextEpisode.StartDate;
                        modelData.EndDate = nextEpisode.EndDate;
                    }
                    else
                    {
                        modelData.StartDate = episode.EndDate.AddDays(1);
                        modelData.EndDate = modelData.StartDate.AddDays(59);
                    }
                }
                else
                {
                    modelData.EndDate = episode.EndDate;
                    modelData.StartDate = episode.StartDate;
                }
            }
            return PartialView("BlankMasterCalendar", modelData);
        }

        #endregion
    }
}
