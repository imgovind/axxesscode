﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public class PatientScheduleEventViewData
    {
        public PatientScheduleEventViewData()
        {
            this.ScheduleEvents = new List<ScheduleEvent>();
        }
        public Patient Patient { get; set; }
        public string DateFilterType { get; set; }
        public string DisciplineFilterType { get; set; }
        public DateTime FilterStartDate { get; set; }
        public DateTime FilterEndDate { get; set; }
        public List<ScheduleEvent> ScheduleEvents { get; set; }

        public string Range
        {
            get
            {
                if (FilterEndDate.Date > DateTime.MinValue && FilterStartDate.Date > DateTime.MinValue)
                {
                    return string.Format("{0}-{1}", FilterStartDate.ToString("MM/dd/yyyy"), FilterEndDate.ToString("MM/dd/yyyy"));
                }
                return "No Episodes Found";
            }
        }

        public string FilterStartDateFormatted { get { return FilterStartDate.Date > DateTime.MinValue ? FilterStartDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string FilterEndDateFormatted { get { return FilterEndDate.Date > DateTime.MinValue ? FilterEndDate.ToString("MM/dd/yyyy") : string.Empty; } }
    }
}
