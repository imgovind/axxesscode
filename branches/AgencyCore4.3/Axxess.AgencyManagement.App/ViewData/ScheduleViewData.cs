﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public class ScheduleViewData
    {
        public ScheduleViewData()
        {
            this.CalendarData = new CalendarViewData();
        }
        public Guid PatientId { get; set; }
        public bool IsDischarged { get; set; }
        public CalendarViewData CalendarData { get; set; }
        public List<PatientSelection> Patients { get; set; }
        public int Count { get; set; }
        public int PatientListStatus { get; set; }
       
    }
}
