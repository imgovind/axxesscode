﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Axxess.OasisC.Domain;
    using Axxess.Core.Infrastructure;
    using System.Web.Script.Serialization;

    public class OasisViewData : JsonViewData
    {
        public Guid assessmentId{get;set;}
        [ScriptIgnore]
        public IAssessment Assessment { get; set; }
        public bool IsHospitalizationLogRefresh { get; set; }
       
    }
}