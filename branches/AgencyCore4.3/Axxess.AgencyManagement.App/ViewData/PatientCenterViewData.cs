﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Domain;

    public class PatientCenterViewData
    {
        public List<PatientSelection> Patients { get; set; }
        public int PatientListStatus { get; set; }
        public int Count { get; set; }
        public Guid CurrentPatientId { get; set; }
        public PatientScheduleEventViewData PatientScheduleEvent { get; set; }
    }
}
