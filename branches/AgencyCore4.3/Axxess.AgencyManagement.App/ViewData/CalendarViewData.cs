﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;

    public class CalendarViewData
    {
        public CalendarViewData()
        {
            this.ScheduleEvents = new List<ScheduleEvent>();
        }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsEpisodeExist { get; set; }
        public List<ScheduleEvent> ScheduleEvents { get; set; }
        public bool IsDischarged { get; set; }
        public bool HasNext { get { return !this.NextEpisode.IsEmpty(); } }
        public Guid NextEpisode { get; set; }
        public bool HasPrevious { get { return !this.PreviousEpisode.IsEmpty(); } }
        public Guid PreviousEpisode { get; set; }
        public string DisplayName { get; set; }
        public string Discpline { get; set; }
        public string FrequencyList { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public Guid EpisodeAdmissionId { get; set; }
    }
}
