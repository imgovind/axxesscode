﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.App.ViewData
{
   [KnownType(typeof(ClaimSnapShotViewData))]
   public class ClaimSnapShotViewData
    {
        public Guid Id { get; set; }
        public long BatchId  { get; set; }
        public string Type { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool IsGenerated { get; set; }
        public bool IsVerified { get; set; }
        public DateTime Modified { get; set; }
        public string Remark { get; set; }
        public DateTime Created { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AdmissionSource { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string DiagnosisCode { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public string CBSA { get; set; }
        public string VerifiedVisit { get; set; }
        public bool AreOrdersComplete { get; set; }
        public int PrimaryInsuranceId { get; set; }
        [UIHint("Status")]
        public int Status { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public string ClaimDate { get; set; }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public string Comment { get; set; }
        public double SupplyTotal { get; set; }
        public string TypeName
        {
            get
            {
                return this.Type.ToUpperCase();
            }

        }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus), this.Status) ? ((BillingStatus)this.Status).GetDescription() : string.Empty;

            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
    }
}
