﻿namespace Axxess.AgencyManagement.App.ViewData {
    using Axxess.Core.Infrastructure;
    using System;
    public class OasisPlanOfCareJson : JsonViewData {
        public string url { get; set; }
        public Guid episodeId { get; set; }
        public Guid patientId { get; set; }
        public Guid eventId { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }
    }
}