﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    public class OasisMedicationProfileViewData
    {
        public Guid Id { get; set; }
        public string AssessmentType { get; set; }
        public MedicationProfile Profile { get; set; }
    }
}