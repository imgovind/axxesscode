﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class AgencyViewData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AgencyAdminUsername { get; set; }
        public string AgencyAdminPassword { get; set; }
        public string AgencyAdminFirstName { get; set; }
        public string AgencyAdminLastName { get; set; }
        public string TaxId { get; set; }
        public string TaxIdTypeId { get; set; }
        public string Payor { get; set; }
        public string NationalProviderNumber { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string MedicaidProviderNumber { get; set; }
        public string HomeHealthAgencyId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public string[] ContactPhoneArray { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressFull
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        public string Phone { get; set; }
        public string[] PhoneArray { get; set; }
    }
}
