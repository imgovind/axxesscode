﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class PayrollDetailsExport : BaseExporter
    {
       private List<PayrollDetail> payrollDetails;
       private DateTime startDate;
       private DateTime endDate;
        public PayrollDetailsExport(List<PayrollDetail> payrollDetails , DateTime startDate , DateTime endDate)
            : base()
        {
            this.payrollDetails = payrollDetails;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Details";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Payroll Details");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Details");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Schedule Date");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Patient");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Time");
            headerRow.CreateCell(5).SetCellValue("Total Min.");
            headerRow.CreateCell(6).SetCellValue("Associated Mileage");
            headerRow.CreateCell(7).SetCellValue("Status");

            if (this.payrollDetails != null && this.payrollDetails.Count > 0)
            {
                int i = 2;
                payrollDetails.ForEach(payrollDetail =>
                {
                    var totalTime = 0;
                    if (payrollDetail.Visits != null && payrollDetail.Visits.Count > 0)
                    {
                        var topRow = sheet.CreateRow(i);
                        topRow.CreateCell(0).SetCellValue(string.Format("Employee: {0}", payrollDetail.Name));
                        payrollDetail.Visits.ForEach(visit =>
                        {
                            i++;
                            var row = sheet.CreateRow(i);
                            DateTime scheduleDate =visit.ScheduleDate;
                            if (scheduleDate != DateTime.MinValue)
                            {
                                var createdDateCell = row.CreateCell(0);
                                createdDateCell.CellStyle = dateStyle;
                                createdDateCell.SetCellValue(scheduleDate);
                            }
                            else
                            {
                                row.CreateCell(0).SetCellValue(string.Empty);
                            }
                            DateTime visitDate = visit.VisitDate;
                            if (visitDate != DateTime.MinValue)
                            {
                                var createdDateCell = row.CreateCell(1);
                                createdDateCell.CellStyle = dateStyle;
                                createdDateCell.SetCellValue(visitDate);
                            }
                            else
                            {
                                row.CreateCell(1).SetCellValue(string.Empty);
                            }
                            row.CreateCell(2).SetCellValue(visit.PatientName);
                            row.CreateCell(3).SetCellValue(visit.TaskName);
                            row.CreateCell(4).SetCellValue(visit.TimeIn.IsNotNullOrEmpty() && visit.TimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", visit.TimeIn, visit.TimeOut) : string.Empty);
                            row.CreateCell(5).SetCellValue(visit.MinSpent);
                            row.CreateCell(6).SetCellValue(visit.AssociatedMileage);
                            row.CreateCell(7).SetCellValue(visit.StatusName);
                            totalTime += visit.MinSpent;
                        });
                        i += 1;
                        var totalRow = sheet.CreateRow(i);
                        totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Payroll Detail: {0}", payrollDetail.Visits.Count));
                        totalRow.CreateCell(1).SetCellValue(string.Format("Total Time for this employee: {0}", string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)));
                    }
                    workBook.FinishWritingToExcelSpreadsheet(8);
                    i += 2;

                });
                var finalRow = sheet.CreateRow(i + 2);
                finalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee : {0}", payrollDetails.Count));
            }
            else
            {
                workBook.FinishWritingToExcelSpreadsheet(8);
            }
        }
    }
}
