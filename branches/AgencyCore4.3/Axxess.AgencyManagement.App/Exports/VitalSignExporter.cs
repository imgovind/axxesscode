﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.App.Extensions;
using NPOI.SS.UserModel;
using NPOI.HPSF;

namespace Axxess.AgencyManagement.App.Exports
{
    public class VitalSignExporter : BaseExporter
    {
        private IList<VitalSign> vitalSigns;
        private DateTime StartDate;
        private DateTime EndDate;
        private string PatientName;
        public VitalSignExporter(IList<VitalSign> vitalSigns, DateTime StartDate, DateTime EndDate, string PatientName)
            : base()
        {
            this.vitalSigns = vitalSigns;
            this.StartDate=StartDate;
            this.EndDate=EndDate;
            this.PatientName = PatientName;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Vital Signs";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("VitalSigns");

            var headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            var headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Vital Signs");
            titleRow.CreateCell(2).SetCellValue("Patient: " + PatientName);
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}",string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"),EndDate.ToString("MM/dd/yyyy"))));
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Visit Date");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Employee");
            headerRow.CreateCell(3).SetCellValue("BP Lying");
            headerRow.CreateCell(4).SetCellValue("BP Sitting");
            headerRow.CreateCell(5).SetCellValue("BP Standing");
            headerRow.CreateCell(6).SetCellValue("Temp");
            headerRow.CreateCell(7).SetCellValue("Resp");
            headerRow.CreateCell(8).SetCellValue("Apical Pulse");
            headerRow.CreateCell(9).SetCellValue("Radial Pulse");
            headerRow.CreateCell(10).SetCellValue("BS");
            headerRow.CreateCell(11).SetCellValue("Weight");
            headerRow.CreateCell(12).SetCellValue("Pain Level");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.vitalSigns.Count > 0)
            {
                int i = 2;
                this.vitalSigns.ForEach(vitalSign =>
                {
                    var row = sheet.CreateRow(i);
                    DateTime visitDate = DateTime.Parse(vitalSign.VisitDate);
                    if (visitDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(0);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        row.CreateCell(0).SetCellValue(string.Empty);
                    }
                    row.CreateCell(1).SetCellValue(vitalSign.DisciplineTask);
                    row.CreateCell(2).SetCellValue(vitalSign.UserDisplayName);
                    row.CreateCell(3).SetCellValue(vitalSign.BPLying);
                    row.CreateCell(4).SetCellValue(vitalSign.BPSitting);
                    row.CreateCell(5).SetCellValue(vitalSign.BPStanding);
                    if (!string.IsNullOrEmpty(vitalSign.Temp) && vitalSign.Temp.IsDouble())
                    {
                        row.CreateCell(6).SetCellValue(vitalSign.Temp.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(vitalSign.Temp);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.Resp) && vitalSign.Resp.IsDouble())
                    {
                        row.CreateCell(7).SetCellValue(vitalSign.Resp.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(7).SetCellValue(vitalSign.Resp);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.ApicalPulse) && vitalSign.ApicalPulse.IsDouble())
                    {
                        row.CreateCell(8).SetCellValue(vitalSign.ApicalPulse.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(8).SetCellValue(vitalSign.ApicalPulse);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.RadialPulse) && vitalSign.RadialPulse.IsDouble())
                    {
                        row.CreateCell(9).SetCellValue(vitalSign.RadialPulse.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(9).SetCellValue(vitalSign.RadialPulse);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.BSMax) && vitalSign.BSMax.IsDouble())
                    {
                        row.CreateCell(10).SetCellValue(vitalSign.BSMax.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(10).SetCellValue(vitalSign.BSMax);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.Weight) && vitalSign.Weight.IsDouble())
                    {
                        row.CreateCell(11).SetCellValue(vitalSign.Weight.ToDouble());
                    }
                    else
                    {
                        row.CreateCell(11).SetCellValue(vitalSign.Weight);
                    }

                    if (!string.IsNullOrEmpty(vitalSign.PainLevel) && vitalSign.PainLevel.IsInteger())
                    {
                        row.CreateCell(12).SetCellValue(vitalSign.PainLevel.ToInteger());
                    }
                    else
                    {
                        row.CreateCell(12).SetCellValue(vitalSign.PainLevel);
                    }
                    
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Vital Signs: {0}", vitalSigns.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(13);
        }
    }
}
