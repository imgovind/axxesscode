﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "MissedVisit",
                "Visit/Miss",
                new { controller = this.Name, action = "MissedVisit", id = UrlParameter.Optional });

            routes.MapRoute(
                "MissedVisitPrint",
                "MissedVisit/View/{patientId}/{eventId}",
                new { controller = this.Name, action = "MissedVisitPrint", patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute("NotePrint", "Note/View/{episodeId}/{patientId}/{eventId}", new { controller = this.Name, action = "NotePrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute("ScheduleCenter", "Schedule/Center/{status}/{patientId}", new { controller = this.Name, action = "Center", status = 1, patientId = new IsGuid() });

        }
    }
}
