﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "NewContact",
               "Contact/New",
               new { controller = this.Name, action = "NewContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddContact",
               "Contact/Add",
               new { controller = this.Name, action = "AddContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditContact",
              "Contact/Edit",
              new { controller = this.Name, action = "EditContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateContact",
              "Contact/Update",
              new { controller = this.Name, action = "UpdateContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteContact",
              "Contact/Delete",
              new { controller = this.Name, action = "DeleteContact", id = new IsGuid() }
            );

            routes.MapRoute(
               "ContactGrid",
               "Contact/Grid",
               new { controller = this.Name, action = "Contacts", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ContactList",
               "Contact/List",
               new { controller = this.Name, action = "ContactList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewHospital",
               "Hospital/New",
               new { controller = this.Name, action = "NewHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddHospital",
               "Hospital/Add",
               new { controller = this.Name, action = "AddHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditHospital",
              "Hospital/Edit",
              new { controller = this.Name, action = "EditHospital", id = new IsGuid() }
            );

            routes.MapRoute(
              "UpdateHospital",
              "Hospital/Update",
              new { controller = this.Name, action = "UpdateHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteHospital",
              "Hospital/Delete",
              new { controller = this.Name, action = "DeleteHospital", id = new IsGuid() }
            );

            routes.MapRoute(
               "HospitalGrid",
               "Hospital/Grid",
               new { controller = this.Name, action = "Hospitals", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "HospitalList",
               "Hospital/List",
               new { controller = this.Name, action = "HospitalList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewLocation",
               "Location/New",
               new { controller = this.Name, action = "NewLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddLocation",
               "Location/Add",
               new { controller = this.Name, action = "AddLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditLocation",
              "Location/Edit",
              new { controller = this.Name, action = "EditLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateLocation",
              "Location/Update",
              new { controller = this.Name, action = "UpdateLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteLocation",
              "Location/Delete",
              new { controller = this.Name, action = "DeleteLocation", id = new IsGuid() }
            );

            routes.MapRoute(
               "LocationGrid",
               "Location/Grid",
               new { controller = this.Name, action = "Locations", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "LocationList",
               "Location/List",
               new { controller = this.Name, action = "LocationList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewInsurance",
               "Insurance/New",
               new { controller = this.Name, action = "NewInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddInsurance",
               "Insurance/Add",
               new { controller = this.Name, action = "AddInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditInsurance",
             "Insurance/Edit",
             new { controller = this.Name, action = "EditInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdateInsurance",
             "Insurance/Update",
             new { controller = this.Name, action = "UpdateInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteInsurance",
              "Insurance/Delete",
              new { controller = this.Name, action = "DeleteInsurance", id = new IsGuid() }
            );

            routes.MapRoute(
               "InsuranceGrid",
               "Insurance/Grid",
               new { controller = this.Name, action = "Insurances", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "InsuranceList",
               "Insurance/List",
               new { controller = this.Name, action = "InsuranceList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewPhysician",
               "Physician/New",
               new { controller = this.Name, action = "NewPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddPhysician",
               "Physician/Add",
               new { controller = this.Name, action = "AddPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditPhysician",
             "Physician/Edit",
             new { controller = this.Name, action = "EditPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdatePhysician",
             "Physician/Update",
             new { controller = this.Name, action = "UpdatePhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeletePhysician",
              "Physician/Delete",
              new { controller = this.Name, action = "DeletePhysician", id = new IsGuid() }
            );

            routes.MapRoute(
               "PhysicianGrid",
               "Physician/Grid",
               new { controller = this.Name, action = "Physicians", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "PhysicianList",
              "Physician/List",
              new { controller = this.Name, action = "PhysicianList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewIncident",
               "Incident/New",
               new { controller = this.Name, action = "NewIncident", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddIncident",
               "Incident/Add",
               new { controller = this.Name, action = "AddIncident", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditIncident",
             "Incident/Edit",
             new { controller = this.Name, action = "EditIncident", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdateIncident",
             "Incident/Update",
             new { controller = this.Name, action = "UpdateIncident", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "DeleteIncident",
                "IncidentReport/Delete",
                new { controller = this.Name, action = "DeleteIncident", id = UrlParameter.Optional }
                );

            routes.MapRoute(
             "IncidentReportPrint",
             "Incident/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "IncidentReportPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });


            routes.MapRoute(
               "IncidentGrid",
               "Incident/Grid",
               new { controller = this.Name, action = "IncidentGrid", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "IncidentList",
              "Incident/List",
              new { controller = this.Name, action = "IncidentList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewInfection",
               "Infection/New",
               new { controller = this.Name, action = "NewInfection", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddInfection",
               "Infection/Add",
               new { controller = this.Name, action = "AddInfection", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditInfection",
             "Infection/Edit",
             new { controller = this.Name, action = "EditInfection", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdateInfection",
             "Infection/Update",
             new { controller = this.Name, action = "UpdateInfection", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "InfectionReportPrint",
             "Infection/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "InfectionReportPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            //routes.MapRoute(
            //  "DeleteInfection",
            //  "Infection/Delete",
            //  new { controller = this.Name, action = "DeleteInfection", id = new IsGuid() }
            //);

            routes.MapRoute(
               "InfectionGrid",
               "Infection/Grid",
               new { controller = this.Name, action = "InfectionGrid", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "InfectionList",
              "Infection/List",
              new { controller = this.Name, action = "InfectionList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "InfectionDelete",
              "InfectionReport/Delete",
              new { controller = this.Name, action = "DeleteInfection", id = UrlParameter.Optional } 
            );


            routes.MapRoute(
               "NewTemplate",
               "Template/New",
               new { controller = this.Name, action = "NewTemplate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddTemplate",
               "Template/Add",
               new { controller = this.Name, action = "AddTemplate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditTemplate",
              "Template/Edit",
              new { controller = this.Name, action = "EditTemplate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateTemplate",
              "Template/Update",
              new { controller = this.Name, action = "UpdateTemplate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteTemplate",
              "Template/Delete",
              new { controller = this.Name, action = "DeleteTemplate", id = new IsGuid() }
            );

            routes.MapRoute(
             "GetTemplate",
             "Template/Get",
             new { controller = this.Name, action = "GetTemplate", id = new IsGuid() }
            );

            routes.MapRoute(
               "TemplateGrid",
               "Template/Grid",
               new { controller = this.Name, action = "Templates", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "TemplateList",
               "Template/List",
               new { controller = this.Name, action = "TemplateList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewAgencySupply",
               "Supply/New",
               new { controller = this.Name, action = "NewSupply", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddAgencySupply",
               "Supply/Add",
               new { controller = this.Name, action = "AddSupply", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditAgencySupply",
              "Supply/Edit",
              new { controller = this.Name, action = "EditSupply", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateAgencySupply",
              "Supply/Update",
              new { controller = this.Name, action = "UpdateSupply", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteAgencySupply",
              "Supply/Delete",
              new { controller = this.Name, action = "DeleteSupply", id = new IsGuid() }
            );

            routes.MapRoute(
             "GetAgencySupply",
             "Supply/Get",
             new { controller = this.Name, action = "GetSupply", id = new IsGuid() }
            );

            routes.MapRoute(
               "SupplyGrid",
               "Supply/Grid",
               new { controller = this.Name, action = "Supplies", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "SupplyList",
               "Supply/List",
               new { controller = this.Name, action = "SupplyList", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //   "MedicareEligibilityContent",
            //   "MedicareEligibility/Content",
            //   new { controller = this.Name, action = "MedicareEligibilityContent", id = UrlParameter.Optional }
            //);

            // routes.MapRoute(
            //   "MedicareEligibilityPrint",
            //   "MedicareEligibility/Print/{Id}",
            //   new { controller = this.Name, action = "MedicareEligibilityPrint", id = new IsGuid() }
            //);

        routes.MapRoute(
           "MissedVisitsList",
           "MissedVisits/List",
           new { controller = this.Name, action = "MissedVisitsList", id = UrlParameter.Optional }
        );

         routes.MapRoute(
            "MissedVisitsContent",
            "MissedVisits/Content",
            new { controller = this.Name, action = "MissedVisitsContent", id = UrlParameter.Optional }
         );


         routes.MapRoute(
            "NewAdjustmentCode",
            "AdjustmentCode/New",
            new { controller = this.Name, action = "NewAdjustmentCode", id = UrlParameter.Optional }
         );

         routes.MapRoute(
            "AddAdjustmentCode",
            "AdjustmentCode/Add",
            new { controller = this.Name, action = "AddAdjustmentCode", id = UrlParameter.Optional }
         );

         routes.MapRoute(
           "EditAdjustmentCode",
           "AdjustmentCode/Edit",
           new { controller = this.Name, action = "EditAdjustmentCode", id = UrlParameter.Optional }
         );

         routes.MapRoute(
           "UpdateAdjustmentCode",
           "AdjustmentCode/Update",
           new { controller = this.Name, action = "UpdateAdjustmentCode", id = UrlParameter.Optional }
         );

         routes.MapRoute(
           "DeleteAdjustmentCode",
           "AdjustmentCode/Delete",
           new { controller = this.Name, action = "DeleteAdjustmentCode", id = new IsGuid() }
         );

         routes.MapRoute(
            "AdjustmentCodeGrid",
            "AdjustmentCode/Grid",
            new { controller = this.Name, action = "AdjustmentCodes", id = UrlParameter.Optional }
         );

         routes.MapRoute(
            "AdjustmentCodeList",
            "AdjustmentCode/List",
            new { controller = this.Name, action = "AdjustmentCodeList", id = UrlParameter.Optional }
         );


        }
    }
}
