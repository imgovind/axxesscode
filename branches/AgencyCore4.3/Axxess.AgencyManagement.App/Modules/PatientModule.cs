﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class PatientModule : Module
    {
        public override string Name
        {
            get { return "Patient"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "NewPatient",
               "Patient/New/{referralId}",
               new { controller = this.Name, action = "New", referralId = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewOrder",
               "Order/New",
               new { controller = this.Name, action = "NewOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EditOrder",
               "Order/Edit",
               new { controller = this.Name, action = "EditOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "UpdateOrder",
               "Order/Update",
               new { controller = this.Name, action = "UpdateOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddOrder",
               "Order/Add",
               new { controller = this.Name, action = "AddOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "ViewBlankOrder",
              "Order/View/Blank",
              new { controller = this.Name, action = "PhysicianOrderBlank", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "ViewOrder",
              "Order/View/{episodeId}/{patientId}/{orderId}",
              new { controller = this.Name, action = "PhysicianOrderPrint", episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() }
            );

            routes.MapRoute(
              "NewFaceToFaceEncounter",
              "FaceToFaceEncounter/New",
              new { controller = this.Name, action = "NewFaceToFaceEncounter", id = UrlParameter.Optional }
           );
            routes.MapRoute(
              "AddFaceToFaceEncounter",
              "FaceToFaceEncounter/Add",
              new { controller = this.Name, action = "AddFaceToFaceEncounter", id = UrlParameter.Optional }
           );
            routes.MapRoute(
             "ViewFaceToFaceEncounter",
             "FaceToFaceEncounter/View/{patientId}/{eventId}",
             new { controller = this.Name, action = "PhysicianFaceToFaceEncounterPrint",  patientId = new IsGuid(), eventId = new IsGuid() }
           );

            routes.MapRoute(
              "PatientProfile",
              "Patient/Profile/{id}",
              new { controller = this.Name, action = "PatientProfilePrint", id = new IsGuid() }
            );

            routes.MapRoute(
              "PatientTriageClassification",
              "Patient/TriageClassification/{id}",
              new { controller = this.Name, action = "TriageClassification", id = new IsGuid() }
            );

            routes.MapRoute(
              "PatientPhoto",
              "Patient/NewPhoto/{patientId}",
              new { controller = this.Name, action = "NewPhoto", patientId = new IsGuid() }
            );

            routes.MapRoute(
              "AllergyProfilePrint",
              "AllergyProfile/View/{id}",
              new { controller = this.Name, action = "AllergyProfilePrint", id = new IsGuid() }
            );

            routes.MapRoute(
              "MedicationProfilePrint",
              "MedicationProfile/View/{patientId}",
              new { controller = this.Name, action = "MedicationProfilePrint", patientId = new IsGuid() }
            );

            routes.MapRoute(
              "NewCommunicationNote",
              "CommunicationNote/New",
              new { controller = this.Name, action = "NewCommunicationNote", patientId = UrlParameter.Optional }
           );

            routes.MapRoute(
               "AddCommunicationNote",
               "CommunicationNote/Add",
               new { controller = this.Name, action = "AddCommunicationNote", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditCommunicationNote",
              "CommunicationNote/Update",
              new { controller = this.Name, action = "UpdateCommunicationNote", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             "CommunicationNoteList",
             "CommunicationNote/List",
             new { controller = this.Name, action = "CommunicationNotesView", id = UrlParameter.Optional }
          );

            routes.MapRoute(
            "CommunicationNotePrint",
            "CommunicationNote/View/{patientId}/{eventId}",
            new { controller = this.Name, action = "CommunicationNotePrint", patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "NewAuthorization",
               "Authorization/New",
               new { controller = this.Name, action = "NewAuthorization", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EditAuthorization",
               "Authorization/Edit",
               new { controller = this.Name, action = "EditAuthorization", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "UpdateAuthorization",
               "Authorization/Update",
               new { controller = this.Name, action = "UpdateAuthorization", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddAuthorization",
               "Authorization/Add",
               new { controller = this.Name, action = "AddAuthorization", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AuthorizationList",
               "Authorization/List",
               new { controller = this.Name, action = "AuthorizationList", patientId = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AuthorizationGrid",
               "Authorization/Grid",
               new { controller = this.Name, action = "AuthorizationGrid", patientId = new IsGuid() }
            );
            routes.MapRoute(
              "PatientCenter",
              "Patient/Center/{status}/{patientId}",
              new { controller = this.Name, action = "Center", status = 1, patientId = new IsGuid() }
           );
        }
    }
}
