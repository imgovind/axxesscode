﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Timers;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.App.Domain;

    public class LoginMonitor
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly LoginMonitor instance = new LoginMonitor();
        }

        #endregion

        #region Private Members

        private Timer updateTimer;
        private SafeList<LoginAttempt> loginAttempts;
        
        #endregion

        #region Public Instance

        public static LoginMonitor Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private LoginMonitor()
        {
            this.loginAttempts = new SafeList<LoginAttempt>();
        }

        private void StartTimer()
        {
            this.updateTimer = new System.Timers.Timer(TimeSpan.FromMinutes(2).TotalMilliseconds);
            this.updateTimer.Enabled = true;
            this.updateTimer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
            this.updateTimer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var currentTime = DateTime.Now;
            var items = this.loginAttempts.FindAll(i => currentTime > i.LastLoginAttempt.AddMinutes(5));
            items.ForEach(i =>
            {
                this.loginAttempts.Remove(i);
            });
        }

        #endregion

        #region Public Methods

        public void Track(string userName, string ipAddress)
        {
            var loginAttempt = this.loginAttempts.Find(l => l.IpAddress == ipAddress && l.UserName.ToLower() == userName.ToLower());
            if (loginAttempt != null)
            {
                loginAttempt.Count++;
                loginAttempt.LastLoginAttempt = DateTime.Now;
            }
            else
            {
                loginAttempt = new LoginAttempt();
                loginAttempt.Count++;
                loginAttempt.UserName = userName;
                loginAttempt.IpAddress = ipAddress;
                loginAttempt.LastLoginAttempt = DateTime.Now;
                this.loginAttempts.Add(loginAttempt);
            }
        }

        public bool HasAttempts(string userName, string ipAddress)
        {
            var result = false;
            var loginAttempt = this.loginAttempts.Find(l => l.IpAddress == ipAddress && l.UserName.ToLower() == userName.ToLower());
            if (loginAttempt != null && loginAttempt.Count < 3)
            {
                return true;
            }
           
            return result;
        }

        public bool IsIpAddressLocked(string ipAddress)
        {
            var result = false;
            var loginAttempt = this.loginAttempts.Find(l => l.IpAddress == ipAddress);
            if (loginAttempt != null && loginAttempt.Count == 3)
            {
                return true;
            }

            return result;
        }


        #endregion
    }
}
