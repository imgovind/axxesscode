﻿namespace Axxess.Physician.App
{
    using System.Web.Mvc;

    using StructureMap;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC;
    using Modules;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
                x.AddRegistry<PhysicianApplicationRegistry>();
            });

            AreaRegistration.RegisterAllAreas();
        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Module.Register(new OrderModule());
            Axxess.Core.Infrastructure.Module.Register(new AccountModule());
            Axxess.Core.Infrastructure.Module.Register(new DefaultModule());
        }
    }
}
