﻿namespace Axxess.Physician.App
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Collections.Generic;

    using Axxess.LookUp;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public static class HtmlHelperExtensions
    {
        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static IPatientRepository patientRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, string listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case "CPTCode":
                    var cpoCodes = new Dictionary<string, string>();
                    Array cptCodesValues = Enum.GetValues(typeof(CptCodes));
                    foreach (CptCodes cptCode in cptCodesValues)
                    {
                        cpoCodes.Add(cptCode.GetDescription(), cptCode.ToString());
                    }
                    tempItems=from type in cpoCodes
                              select new SelectListItem
                              {
                                  Text = type.Key,
                                  Value = type.Value.ToString(),
                                  Selected = (type.Value.ToString().IsEqual(value))
                              };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select CPO Type --",
                        Value = "",
                    });
                    break;
                case "CPOActivity":
                    var cpoActivities = new Dictionary<string, int>();
                    Array cpoActivitiesValues = Enum.GetValues(typeof(CpoActivities));
                    foreach (CpoActivities cpoActivity in cpoActivitiesValues)
                    {
                        cpoActivities.Add(cpoActivity.GetDescription(), (int)cpoActivity);
                    }
                    tempItems = from type in cpoActivities
                                select new SelectListItem
                                {
                                    Text=type.Key,
                                    Value=type.Value.ToString(),
                                    Selected=(type.Value.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select CPO Activity --",
                        Value = "",
                    });
                    break;
                case "agency":
                    var agencies = agencyRepository.GetPhysicianAgenies(Current.LoginId);
                    tempItems = from agency in agencies
                                select new SelectListItem
                                {
                                    Text=agency.Name,
                                    Value=agency.Id.ToString(),
                                    Selected=(agency.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Agency --",
                        Value = "",
                    });
                    break;
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AgencyPhysicianPatient(this HtmlHelper html, string name, string value, Guid agencyId, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (!agencyId.IsEmpty())
            {
                var patients = patientRepository.GetPatientByAgencyPhysician(agencyId, Current.LoginId);
                if (patients != null && patients.Count > 0)
                {
                    items = patients.Select(p => new SelectListItem
                    {
                        Text=string.Format("{0} {1}", p.FirstName,p.LastName),
                        Value=p.Id.ToString(),
                        Selected=(p.Id.ToString().IsEqual(value))
                    }).ToList();
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Patient --",
                Value = Guid.Empty.ToString()
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
    }
}
