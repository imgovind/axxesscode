﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    using iTextSharp.text;
    class AxxessTitle : Paragraph {
        public AxxessTitle(String title, Font font) : base(title, font) {
            this.SetAlignment("Center");
        }
    }
}