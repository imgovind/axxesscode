﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AxxessScript2
    {
        private static string input = Path.Combine(App.Root, "Files\\SCOF.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\SCOF_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";
                                            var nameArray = dataRow.GetValue(1);
                                            var remainingArray = nameArray.Split(',')[1].Trim();
                                            patientData.LastName = nameArray.Split(',')[0];
                                            var firstMiddle = remainingArray.Split('-')[0].Trim();
                                            if (firstMiddle.Split(' ').Length > 1)
                                            {
                                                patientData.FirstName = firstMiddle.Split(' ')[0];
                                                patientData.MiddleInitial = firstMiddle.Split(' ')[1];
                                            }
                                            else
                                            {
                                                patientData.FirstName = firstMiddle;
                                            }
                                            patientData.PatientIdNumber = dataRow.GetValue(2);
                                            if (dataRow.GetValue(3) == "M")
                                                patientData.Gender = "Male";
                                            else
                                                patientData.Gender = "Female";
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                var ss = dataRow.GetValue(4);
                                                patientData.DOB=DateTime.FromOADate(double.Parse(dataRow.GetValue(4)));
                                               // patientData.DOB = dataRow.GetValue(4).ToDateTime();; //patientData.DOB = dataRow.GetValue(6).Trim().IsValidDate() ? dataRow.GetValue(6).Trim().ToDateTime() : DateTime.MinValue;
                                            }
                                            //patientData.Comments += string.Format("Status code:{0}.", dataRow.GetValue(5));
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            }
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                                patientData.Comments += string.Format("Current Cert:{0}.", dataRow.GetValue(6));
                                            patientData.PrimaryInsurance = "1";
                                            patientData.MedicareNumber = dataRow.GetValue(8);
                                            //patientData.PatientIdNumber = dataRow.GetValue(7);
                                            patientData.AddressLine1 = dataRow.GetValue(9);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(10);
                                            patientData.AddressStateCode = dataRow.GetValue(11);
                                            patientData.AddressZipCode = dataRow.GetValue(12);
                                            //patientData.Comments += string.Format("County:{0}.", dataRow.GetValue(12));
                                            patientData.PhoneHome = dataRow.GetValue(13).ToPhoneDB();
                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Case Manager:{0}.", dataRow.GetValue(14));
                                            }
                                            //patientData.Comments += string.Format("Date referred:{0}.", dataRow.GetValue(14));
                                            //if(dataRow.GetValue(15).IsNotNullOrEmpty())
                                            //    patientData.Comments += string.Format("Last admit date:{0}.", dataRow.GetValue(15));
                                            //if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                            //    patientData.Comments += string.Format("Last discharge date:{0}.", dataRow.GetValue(16));
                                            //patientData.MedicareNumber = dataRow.GetValue(17);
                                            //if (!dataRow.GetValue(18).Equals("NA"))
                                            //    patientData.MedicaidNumber = dataRow.GetValue(18);
                                            //if (dataRow.GetValue(4).Contains("/"))
                                            //{
                                            //    var numbers = dataRow.GetValue(4).Split('/');
                                            //    patientData.MedicareNumber = numbers[0];
                                            //    patientData.Comments += string.Format("{0}", numbers[1]);
                                            //}else                                                
                                            //    patientData.MedicareNumber = dataRow.GetValue(4);

                                            //if (dataRow.GetValue(5).IsNotNullOrEmpty() && dataRow.GetValue(5).IsDouble())
                                            //{
                                            //    patientData.DischargeDate = DateTime.FromOADate(dataRow.GetValue(5).ToDouble()); // patientData.DischargeDate = dataRow.GetValue(5).Trim().IsValidDate() ? dataRow.GetValue(5).Trim().ToDateTime() : DateTime.MinValue; 
                                            //}

                                            

                                            //if (dataRow.GetValue(7).IsNotNullOrEmpty() && dataRow.GetValue(7).IsDouble())
                                            //{
                                            //    patientData.StartofCareDate = DateTime.FromOADate(dataRow.GetValue(7).ToDouble()); //patientData.StartofCareDate = dataRow.GetValue(8).Trim().IsValidDate() ? dataRow.GetValue(8).Trim().ToDateTime() : DateTime.MinValue;
                                            //}
                                            //patientData.Comments += string.Format("Episode Start Date:{0}", dataRow.GetValue(8));
                                            //var martialStatus = dataRow.GetValue(3);
                                            //if (martialStatus.IsNotNullOrEmpty())
                                            //{
                                            //    if (martialStatus.StartsWith("M",StringComparison.InvariantCultureIgnoreCase))
                                            //    {
                                            //        patientData.MaritalStatus = "Married";
                                            //    }
                                            //    else if (martialStatus.StartsWith("S", StringComparison.InvariantCultureIgnoreCase))
                                            //    {
                                            //        patientData.MaritalStatus = "Single";
                                            //    }
                                            //    else if (martialStatus.StartsWith("W", StringComparison.InvariantCultureIgnoreCase))
                                            //    {
                                            //        patientData.MaritalStatus = "Widowed";
                                            //    }
                                            //    else if (martialStatus.StartsWith("D", StringComparison.InvariantCultureIgnoreCase))
                                            //    {
                                            //        patientData.MaritalStatus = "Divorced";
                                            //    }
                                            //    else
                                            //    {
                                            //        patientData.MaritalStatus = "Unknown";
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    patientData.MaritalStatus = "Unknown";
                                            //}
                                            patientData.MaritalStatus = "Unknown";
                                            
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(16);
                                                            var physician = Database.GetPhysician(npi, agencyId);
                                                            if (physician == null)
                                                            {
                                                                exists = false;
                                                                var info = Database.GetNpiData(npi);
                                                                if (info != null)
                                                                {
                                                                    physician = new AgencyPhysician
                                                                    {
                                                                        Id = Guid.NewGuid(),
                                                                        AgencyId = agencyId,
                                                                        NPI = npi,
                                                                        LoginId = Guid.Empty,
                                                                        //AddressLine1 = dataRow.GetValue(17),
                                                                        //AddressCity = dataRow.GetValue(18),
                                                                        //AddressStateCode = dataRow.GetValue(19),// dataRow.GetValue(20).Replace(".", ""),
                                                                        //AddressZipCode = dataRow.GetValue(20),
                                                                        //PhoneWork = dataRow.GetValue(21).ToPhoneDB(),
                                                                        //FaxNumber = dataRow.GetValue(22).ToPhoneDB(),
                                                                        //Credentials = dataRow.GetValue(16)
                                                                    };
                                                                    string names = "";
                                                                    if (dataRow.GetValue(15).StartsWith("DR"))
                                                                        names = dataRow.GetValue(15).Substring(4);
                                                                    else
                                                                        names = dataRow.GetValue(15);

                                                                    var physicianNameArray = names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                                    if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                    {
                                                                        physician.LastName = physicianNameArray[0].Trim();
                                                                        var FirstMiddleArray = physicianNameArray[1].Trim().Split(' ');
                                                                        if (FirstMiddleArray.Length > 1)
                                                                        {
                                                                            physician.FirstName = FirstMiddleArray[0];
                                                                            physician.MiddleName = FirstMiddleArray[1];
                                                                        }
                                                                        else
                                                                        {
                                                                            physician.FirstName = physicianNameArray[1];
                                                                        }
                                                                        
                                                                    }
                                                                }

                                                                Database.Add(physician);
                                                            }

                                                            if (physician != null)
                                                            {
                                                                var patientPhysician = new PatientPhysician
                                                                {
                                                                    IsPrimary = true,
                                                                    PatientId = patientData.Id,
                                                                    PhysicianId = physician.Id
                                                                };

                                                                if (Database.Add(patientPhysician))
                                                                {
                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
