﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class IgeaHomeHealthIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\PATIENTS.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PATIENTS_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int rowCounter = 1;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (rowCounter % 3 == 1)
                                        {
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;

                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            var nameDataArray = dataRow.GetValue(1).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameDataArray != null && nameDataArray.Length > 0)
                                            {
                                                var nameArray = nameDataArray[0].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    var firstNameArray = nameArray[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (firstNameArray != null && firstNameArray.Length > 0)
                                                    {
                                                        if (firstNameArray.Length == 2)
                                                        {
                                                            patientData.FirstName = firstNameArray[0].Trim();
                                                            patientData.MiddleInitial = firstNameArray[1].Trim();
                                                        }
                                                        else
                                                        {
                                                            patientData.FirstName = firstNameArray[0].Trim();
                                                        }
                                                    }
                                                    patientData.LastName = nameArray[0];
                                                }

                                                if (nameDataArray.Length > 1)
                                                {
                                                    patientData.MedicareNumber = nameDataArray[1].Trim();
                                                    patientData.PrimaryInsurance = "1";
                                                }
                                            }
                                            patientData.Comments += string.Format("County: {0}. ", dataRow.GetValue(5));
                                            patientData.PhoneHome = dataRow.GetValue(8).ToPhoneDB();
                                            if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Cert. Start Date: {0}. ", dataRow.GetValue(12).ToDateTime().ToShortDateString());
                                            }

                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Principal Diagnosis: {0}. ", dataRow.GetValue(14));
                                            }
                                            patientData.Gender = dataRow.GetValue(18).IsEqual("F") ? "Female" : "Male";
                                        }
                                        else
                                        {
                                            if (rowCounter % 3 == 0)
                                            {
                                                if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                                {
                                                    patientData.DOB = dataRow.GetValue(0).ToDateTime();
                                                }
                                                if (dataRow.GetValue(1).Trim().IsNotNullOrEmpty())
                                                {
                                                    //patientData.Comments += string.Format("Address: {0}. ", dataRow.GetValue(1));

                                                    var stateZip = dataRow.GetValue(1).Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (stateZip != null)
                                                    {
                                                        if (stateZip.Length == 1)
                                                        {
                                                            patientData.AddressZipCode = stateZip[0];
                                                        }
                                                        else if (stateZip.Length == 2)
                                                        {
                                                            patientData.AddressStateCode = stateZip[0];
                                                            patientData.AddressZipCode = stateZip[1];
                                                        }
                                                        else if (stateZip.Length == 3)
                                                        {
                                                            patientData.AddressCity = stateZip[0];
                                                            patientData.AddressStateCode = stateZip[1];
                                                            patientData.AddressZipCode = stateZip[2];
                                                        }
                                                    }
                                                }
                                                if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Case Manager: {0}. ", dataRow.GetValue(5));
                                                }
                                                if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician Fax: {0}. ", dataRow.GetValue(8));
                                                }
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty() && dataRow.GetValue(12).IsDate())
                                                {
                                                    patientData.StartofCareDate = dataRow.GetValue(12).ToDateTime();
                                                }
                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Eligibility Status: {0}. ", dataRow.GetValue(14));
                                                }

                                                if (dataRow.GetValue(20).IsEqual("on-hold"))
                                                {
                                                    patientData.Status = (int)PatientStatus.Pending;
                                                }

                                                patientData.Comments = patientData.Comments.Replace("'", "");

                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;

                                                var medicationProfile = new MedicationProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };
                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId.ToGuid(),
                                                        Created = DateTime.Now,
                                                        DischargedDate = DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };

                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            }
                                                        }
                                                    }
                                                }
                                                textWriter.Write(textWriter.NewLine);
                                                i++;
                                            }
                                            else
                                            {
                                                patientData.SSN = dataRow.GetValue(0).Replace("-", "");
                                                if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                                {
                                                    //patientData.Comments += string.Format("Address: {0}. ", dataRow.GetValue(1));

                                                    var addressDataArray = dataRow.GetValue(1).Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (addressDataArray != null)
                                                    {
                                                        if (addressDataArray.Length == 1)
                                                        {
                                                            patientData.AddressLine1 = dataRow.GetValue(1).Trim();
                                                        }
                                                        else if (addressDataArray.Length == 2)
                                                        {
                                                            patientData.AddressLine1 = addressDataArray[0].Trim();
                                                            patientData.AddressLine2 = string.Empty;
                                                            patientData.AddressCity = addressDataArray[1].Trim();
                                                        }
                                                        else if (addressDataArray.Length == 3)
                                                        {
                                                            patientData.AddressLine1 = addressDataArray[0].Trim();
                                                            patientData.AddressLine2 = string.Empty;
                                                            patientData.AddressCity = addressDataArray[1].Trim();
                                                            var stateZip = addressDataArray[2].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (stateZip != null)
                                                            {
                                                                if (stateZip.Length == 1)
                                                                {
                                                                    patientData.AddressStateCode = stateZip[0].Trim();
                                                                }
                                                                else if (stateZip.Length == 2)
                                                                {
                                                                    patientData.AddressStateCode = stateZip[0].Trim();
                                                                    patientData.AddressZipCode = stateZip[1].Trim();
                                                                }
                                                            }
                                                        }
                                                        else if (addressDataArray.Length == 4)
                                                        {
                                                            patientData.AddressLine1 = addressDataArray[0].Trim();
                                                            patientData.AddressLine2 = string.Empty;
                                                            patientData.AddressCity = addressDataArray[1].Trim();
                                                            patientData.AddressStateCode = addressDataArray[2].Trim();
                                                            patientData.AddressZipCode = addressDataArray[3].Trim();
                                                        }
                                                    }
                                                }
                                                if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(5));
                                                }
                                                if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician Phone: {0}. ", dataRow.GetValue(8));
                                                }
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Cert. End Date: {0}. ", dataRow.GetValue(12).ToDateTime().ToShortDateString());
                                                }
                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Disciplines: {0}. ", dataRow.GetValue(14));
                                                }
                                            }
                                        }
                                    }
                                    rowCounter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
