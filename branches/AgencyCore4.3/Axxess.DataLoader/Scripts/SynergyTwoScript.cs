﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class SynergyTwoScript
    {
        private static string input = Path.Combine(App.Root, "Files\\axxess.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\axxess_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 0;
                                    int dataCounter = 1;
                                    Patient patientData = null;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if (dataCounter == 1)
                                            {
                                                patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = agencyId;
                                                patientData.AgencyLocationId = locationId;
                                                patientData.Status = 1;
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                patientData.Status = 1;
                                                //patientData.PatientIdNumber = dataRow.GetValue(0);
                                                patientData.Gender = "";
                                                patientData.AddressLine1 = string.Empty;
                                                patientData.AddressCity = string.Empty;
                                                patientData.AddressStateCode = string.Empty;
                                                patientData.AddressZipCode = string.Empty;
                                                patientData.PhoneHome = string.Empty;
                                                patientData.PrimaryRelationship = "";
                                                patientData.SecondaryRelationship = "";
                                                patientData.TertiaryRelationship = "";

                                                if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                                {
                                                    var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (nameArray != null && nameArray.Length > 1)
                                                    {
                                                        patientData.FirstName = nameArray[1].Trim();
                                                        patientData.LastName = nameArray[0].Trim();
                                                    }
                                                }
                                                patientData.PatientIdNumber = dataRow.GetValue(4);
                                                patientData.MedicareNumber = dataRow.GetValue(5);
                                                if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                                {
                                                    var sss = dataRow.GetValue(7);
                                                    patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                                }
                                                if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(10)));
                                                }
                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    var status = dataRow.GetValue(14).ToLower();
                                                    if (status.Contains("admitted"))
                                                    {
                                                        patientData.Status = 1;
                                                    }
                                                    else if (status.Contains("discharged"))
                                                    {
                                                        patientData.Status = 2;
                                                    }
                                                    else
                                                    {
                                                        patientData.Status = 3;
                                                    }
                                                }
                                                if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Triage Level: {0}. ", dataRow.GetValue(17));
                                                }
                                                if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("DX Code: {0}. ", dataRow.GetValue(20));
                                                }
                                                var stringBuilder = new StringBuilder();
                                                if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                                {
                                                    stringBuilder.AppendFormat("Physician Name: {0}. ", dataRow.GetValue(25));
                                                }

                                                if (dataRow.GetValue(35).IsNotNullOrEmpty())
                                                {
                                                    stringBuilder.AppendFormat("Physician Phone: {0}. ", dataRow.GetValue(35).ToPhoneDB());
                                                }

                                                if (dataRow.GetValue(40).IsNotNullOrEmpty())
                                                {
                                                    stringBuilder.AppendFormat("Physician UPIN: {0}. ", dataRow.GetValue(40));
                                                }
                                                patientData.Comments += stringBuilder.ToString().Trim();
                                                dataCounter++;
                                            }
                                            else if (dataCounter == 2)
                                            {
                                                if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                                {
                                                    string ss = "   ";
                                                    if (dataRow.GetValue(1).Contains("#")) ss = "  ";//address who has apt# split by two space
                                                    var addressArray = dataRow.GetValue(1).Split(new string[] { ss }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (addressArray != null && addressArray.Length > 1)
                                                    {
                                                        patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                        patientData.AddressLine2 = "";

                                                        var locationArray = addressArray[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (locationArray != null && locationArray.Length == 2)
                                                        {
                                                            patientData.AddressCity = locationArray[0].Trim();

                                                            var zipStateArray = locationArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (zipStateArray != null && zipStateArray.Length == 2)
                                                            {
                                                                patientData.AddressStateCode = zipStateArray[0].Trim();
                                                                patientData.AddressZipCode = zipStateArray[1].Trim();
                                                            }
                                                        }
                                                    }
                                                }

                                                patientData.PhoneHome = dataRow.GetValue(14).ToPhoneDB();

                                                if (dataRow.GetValue(32).ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "1";
                                                }
                                                dataCounter = 1;
                                                i++;

                                                patientData.Comments = patientData.Comments.Replace("'", "");

                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;

                                                var medicationProfile = new MedicationProfile()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };

                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate()
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
										
