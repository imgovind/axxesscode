﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    

   public static class MedicationProfileHistoryPhysician
    {
       private static string output = Path.Combine(App.Root, string.Format("Files\\Medicationprofilehistories_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static readonly IPatientRepository patientRepository = new PatientRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        private static readonly IPhysicianRepository physicianRepository = new PhysicianRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        public static void Run()
        {
            var medicationProfileHistories = patientRepository.GetAllMedicationProfileHistory();
            if (medicationProfileHistories != null && medicationProfileHistories.Count > 0)
            {
                using (TextWriter textWriter = new StreamWriter(output, true))
                {
                    foreach (var profile in medicationProfileHistories)
                    {
                        if (!profile.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(profile.PhysicianId, profile.AgencyId);
                            if (physician != null)
                            {
                                var xmlData = new StringBuilder(physician.ToXml());
                                var data = xmlData.Replace("'", @"\'");

                                textWriter.WriteLine(string.Format("UPDATE `medicationprofilehistories` SET `PhysicianData` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;",data ,profile.Id, profile.AgencyId));
                                textWriter.Write(textWriter.NewLine);
                               
                            }
                        }
                    }
                }
            }
        }
    }
}
