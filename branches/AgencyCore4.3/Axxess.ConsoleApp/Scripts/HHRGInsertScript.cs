﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using Excel;
using System.Data;

namespace Axxess.ConsoleApp.Tests
{
    public static class HHRGInsertScript
    {
        private static string hhrgExcelFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), "Files\\2010HHRGCBSA1.xlsx");
        private static TextWriter tw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), "Files\\2010HHRGCBSA1.txt"), true);

        public static void Run()
        {
            FileStream fileStream = new FileStream(hhrgExcelFile, FileMode.Open, FileAccess.Read);
            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
            {
                if (excelReader != null && excelReader.IsValid)
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    DataTable dataTable = excelReader.AsDataSet().Tables[0];
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                       var data= dataTable.Rows[0];
                       var tempData = data[0].ToString();
                        foreach (DataRow row in dataTable.Rows)
                        {
                            if (row[0].ToString() != string.Empty && row[0].ToString() != tempData)
                            {
                                tempData = row[0].ToString();

                            }
                            row[0] = tempData;
                            tw.WriteLine("insert into HippsAndHhrgs (HHRG, HIPPS, HHRGWeight, SupplyAddOn) values ('{0}', '{1}', {2}, {3});", row[0].ToString(), row[1].ToString(), decimal.Round(decimal.Parse(row[2].ToString()), 4), decimal.Round(decimal.Parse(row[3].ToString()), 2));
                        }
                    }
                }
                
                tw.Close();
                excelReader.Close();
            }
        }
    }
}

