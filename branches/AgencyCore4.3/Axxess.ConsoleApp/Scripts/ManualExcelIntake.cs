﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class ManualExcelIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\twentyfourseven.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\twentyfourseven_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var patientData = new PatientData();
                                    patientData.AgencyId = "9b2b7515-a699-41c5-ac90-e9ec972eaded";
                                    patientData.AgencyLocationId = "f62283b7-3251-40eb-956e-75f3d95987cf";

                                    var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                    if (nameArray != null && nameArray.Length > 1)
                                    {
                                        patientData.LastName = nameArray[0].Trim();
                                        patientData.FirstName = nameArray[1].Trim();
                                    }
                                    patientData.PatientNumber = dataRow.GetValue(1);

                                    if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Policy / HIC Number: {0}. ", dataRow.GetValue(2));
                                    }
                                    if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                    {
                                        patientData.BirthDate = dataRow.GetValue(3).ToMySqlDate(0);
                                    }
                                    if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                    {
                                        patientData.StartofCareDate = dataRow.GetValue(4).ToMySqlDate(0);
                                    }
                                    if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                    {
                                        var status = dataRow.GetValue(5).ToLower();
                                        if (status.Contains("admit"))
                                        {
                                            patientData.PatientStatusId = "1";
                                        }
                                        else
                                        {
                                            patientData.PatientStatusId = "3";
                                        }
                                    }
                                    if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("DX Code: {0}. ", dataRow.GetValue(7));
                                    }
                                    if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician Name: {0}. ", dataRow.GetValue(8));
                                    }
                                    if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician Phone: {0}. ", dataRow.GetValue(9));
                                    }

                                    //patientData.PatientStatusId = "1";
                                    //patientData.PatientNumber = dataRow.GetValue(0);
                                    //patientData.FirstName = dataRow.GetValue(1);
                                    //patientData.MiddleInitial = "";
                                    //patientData.LastName = dataRow.GetValue(2);
                                    //patientData.MedicareNumber = "";
                                    //patientData.MedicaidNumber = "";
                                    //patientData.AddressLine1 = dataRow.GetValue(3);
                                    //patientData.AddressLine2 = "";
                                    //patientData.AddressCity = dataRow.GetValue(4);
                                    //patientData.AddressState = dataRow.GetValue(5);
                                    //patientData.AddressZipCode = dataRow.GetValue(6);
                                    //patientData.Gender = dataRow.GetValue(7).IsEqual("F") ? "Female" : "Male";
                                    //patientData.Phone = dataRow.GetValue(11).IsNotNullOrEmpty() && !dataRow.GetValue(11).IsEqual("NA") ? dataRow.GetValue(11).ToPhoneDB() : "";
                                    //patientData.MaritalStatus = "Unknown";
                                    //patientData.SSN = "";

                                    //if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                    //{
                                    //    patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(8))).ToString("yyyy-M-d");
                                    //}
                                    //if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                    //{
                                    //    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(10))).ToString("yyyy-M-d");
                                    //}

                                    //if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                    //{
                                    //    patientData.Comments += string.Format("Dr. {0}. ", dataRow.GetValue(9));
                                    //}

                                    //if (dataRow.GetValue(12).IsNotNullOrEmpty() && dataRow.GetValue(12).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Long Term Nursing. ";
                                    //}

                                    //if (dataRow.GetValue(13).IsNotNullOrEmpty() && dataRow.GetValue(13).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Skilled Nursing. ";
                                    //}

                                    //if (dataRow.GetValue(14).IsNotNullOrEmpty() && dataRow.GetValue(14).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Short Stay Acute. ";
                                    //}

                                    //if (dataRow.GetValue(15).IsNotNullOrEmpty() && dataRow.GetValue(15).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Long Term Care Hospital. ";
                                    //}

                                    //if (dataRow.GetValue(16).IsNotNullOrEmpty() && dataRow.GetValue(16).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "In-patient Rehab. ";
                                    //}

                                    //if (dataRow.GetValue(17).IsNotNullOrEmpty() && dataRow.GetValue(17).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Psychiatric Hospital. ";
                                    //}

                                    //if (dataRow.GetValue(18).IsNotNullOrEmpty() && dataRow.GetValue(18).IsEqual("1"))
                                    //{
                                    //    patientData.Comments += "Other Service Required. ";
                                    //}

                                    //if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(20).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "0;";
                                    //}

                                    //if (dataRow.GetValue(21).IsNotNullOrEmpty() && dataRow.GetValue(21).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "1;";
                                    //}

                                    //if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(22).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "2;";
                                    //}

                                    //if (dataRow.GetValue(23).IsNotNullOrEmpty() && dataRow.GetValue(23).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "3;";
                                    //}

                                    //if (dataRow.GetValue(24).IsNotNullOrEmpty() && dataRow.GetValue(24).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "4;";
                                    //}

                                    //if (dataRow.GetValue(25).IsNotNullOrEmpty() && dataRow.GetValue(25).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "5;";
                                    //}

                                    //if (dataRow.GetValue(26).IsNotNullOrEmpty() && dataRow.GetValue(26).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "6;";
                                    //}

                                    //if (dataRow.GetValue(27).IsNotNullOrEmpty() && dataRow.GetValue(27).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "7;";
                                    //}

                                    //if (dataRow.GetValue(28).IsNotNullOrEmpty() && dataRow.GetValue(28).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "8;";
                                    //}

                                    //if (dataRow.GetValue(29).IsNotNullOrEmpty() && dataRow.GetValue(29).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "9;";
                                    //}

                                    //if (dataRow.GetValue(30).IsNotNullOrEmpty() && dataRow.GetValue(30).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "10;";
                                    //}

                                    //if (dataRow.GetValue(31).IsNotNullOrEmpty() && dataRow.GetValue(31).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "12;";
                                    //}

                                    //if (dataRow.GetValue(32).IsNotNullOrEmpty() && dataRow.GetValue(32).IsEqual("1"))
                                    //{
                                    //    patientData.PaymentSource += "11;";
                                    //}

                                    //if (dataRow.GetValue(33).IsNotNullOrEmpty())
                                    //{
                                    //    patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(33));
                                    //}

                                    textWriter.WriteLine(new PatientScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                    textWriter.Write(textWriter.NewLine);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
