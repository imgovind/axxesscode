﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Enums;
    using Domain;

    public interface IPatientRepository
    {
        bool Add(Patient patient, out Patient patientOut);
        bool Edit(Patient patient, out Patient patientOut);
        bool PatientAdmissionEdit(Patient patient);
        bool PatientAdmissionAdd(Patient patient);
        bool Update(Patient patient, out Patient patientOut);
        bool Update(Patient patient);
        bool Delete(Guid agencyId, Guid id);
        bool DeprecatedPatient(Guid agencyId, Guid id);
        bool SetStatus(Guid agencyId, Guid patient, PatientStatus status);
        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        bool AddCPO(CarePlanOversight cpo);
        bool DeleteCPO(Guid id);
        bool UpdateCPO(CarePlanOversight cpo);
        Patient Get(Guid Id, Guid agencyId);
        Patient GetPatientOnly(Guid id, Guid agencyId);
        IList<Birthday> GetPatientBirthdays(Guid agencyId, Guid branchId, int month);
        List<AddressBookEntry> GetPatientAddressListing(Guid agencyId, Guid branchId, int status);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid admissionId);
        PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date);
        string GetPatientNameById(Guid patientId);
        string GetBranchName(Guid agencyId, Guid locationId);
        List<BirthdayWidget> GetCurrentPatientBirthdays(Guid agencyId);
        IList<PatientData> All(Guid agencyId, Guid branchId, int status);
        IList<PatientData> AllDeleted(Guid agencyId, Guid branchId);
        IList<Patient> All();
        List<PendingPatient> GetPendingByAgencyId(Guid agencyId);
        List<PatientHospitalizationData> GetHospitalizedPatients(Guid agencyId);
        IList<Patient> FindPatientOnly(int statusId, Guid agencyId);
        IList<Patient> Find(int statusId, Guid branchCode, Guid agencyId);
        IList<Patient> FindByUser(int statusId, Guid branchCode, Guid agencyId, Guid userId);
        IList<Patient> FindByCaseManager(Guid agencyId, Guid branchCode, int statusId, Guid caseManagerId);
        int GetPatientStatusCount(Guid agencyId, int statusId);
        List<PatientSelection> GetPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name);
        List<PatientSelection> GetPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name, Guid userId);
        List<PatientSelection> GetAuditorPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name, Guid userId);
        List<PatientSelection> GetPatientSelectionMedicare(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name);
        List<PatientSelection> GetPatientSelectionAllInsurance(Guid agencyId, Guid branchId, int statusId, string name, int insurnace);
        List<PatientEpisode> GetPatientActiveEpisodesLean(Guid agencyId, Guid patientId);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PatientEpisode> GetPatientActiveEpisodesLeanByDateRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeDataByBranch(Guid agencyId, Guid locationId, DateTime startDate, DateTime endDate);
        PatientEpisode GetPatientEpisodeLean(Guid agencyId, Guid episodeId, Guid patientId);
        List<Patient> GetPatientByAgencyPhysician(Guid agencyId, Guid loginId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactID);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid patientId);
        PatientEmergencyContact GetFirstEmergencyContactByPatient(Guid agencyId, Guid patientId);
        List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status);
        bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContacts(Guid patientId);
        bool DeleteEmergencyContact(Guid Id, Guid patientId);
        bool DeletePhysicianContact(Guid Id, Guid patientId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId);
        bool AddOrder(PhysicianOrder order);
        bool AddCommunicationNote(CommunicationNote communicationNote);
        CommunicationNote GetCommunicationNote(Guid Id, Guid patientId , Guid agencyId);
        bool EditCommunicationNote(CommunicationNote communicationNote);
        bool UpdateCommunicationNoteModal(CommunicationNote communicationNote);
        bool DeleteCommunicationNote(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated);
        List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId);
        List<CommunicationNote> GetCommunicationNoteByIds(Guid agencyId, string orderIds);
        List<CommunicationNote> GetAllCommunicationNotes();
        bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
       // string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId);
        //bool AddReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId, string reason, string user);
        //PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId);
        PatientEpisode GetCurrentEpisodeLean(Guid agencyId, Guid patientId);
        PatientEpisode GetCurrentEpisodeLeanNoPatientInfo(Guid agencyId, Guid patientId);
        PatientEpisode GetLastOrCurrentEpisode(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetPreviousEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate);
        PatientEpisode GetNextEpisodeByStartDate(Guid agencyId, Guid patientId, DateTime startDate);
        PatientEpisode GetLastEpisodeLean(Guid agencyId, Guid patientId);
        long GetNextOrderNumber();

        DateRange GetCurrentEpisodeDateRange(Guid agencyId, Guid patientId);
        DateRange GetNextEpisodeDateRange(Guid agencyId, Guid patientId);
        DateRange GetPreviousEpisodeDateRange(Guid agencyId, Guid patientId);
        PatientEpisode GetNextEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetPreviousEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date);
        
        //List<ScheduleEvent> GetScheduledEventsByEmployeeAssigned(Guid agencyId, Guid patientId, Guid employeeId, DateTime startDate, DateTime endDate);
        //PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId);
        //PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline);
        //PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId);
        //PatientEpisode GetEpisodeOnlyWithPreviousAndAfter(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisodeDateInBetween(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId);
        List<PatientEpisode> GetPatientAllEpisodes(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodes(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId);
        List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId);
        //List<RecertEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        //List<RecertEvent> GetPastDueRecertsWidgetLean(Guid agencyId);
        //List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId);
        //List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        //List<RecertEvent> GetUpcomingRecertsWidgetLean(Guid agencyId);

        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status);
        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetAllPhysicianOrders();

        //List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int status);
        List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate);
        PhysicianOrder GetOrder(Guid id, Guid agencyId);
        PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid agencyId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid patientId, Guid agencyId);
        bool MarkOrderAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        bool UpdateOrder(PhysicianOrder order);
        bool UpdateOrderModel(PhysicianOrder order);
        bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived);
        bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend);

        bool AddEpisode(PatientEpisode patientEpisode);

        //void AddNewUserEvent(Guid agencyId, Guid patientId, UserEvent newUserEvent);
        //bool AddNewScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, ScheduleEvent newScheduledEvent);

        //bool UpdateEpisode(Guid agencyId, ScheduleEvent editEvent);
        //bool UpdateEpisode(Guid agencyId, Guid episodeId, Guid patientId, List<ScheduleEvent> newEvents);
        //bool UpdateScheduleEventsForIsBillable(Guid agencyId, List<ScheduleEvent> scheduleEvents);
        //bool Reassign(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        //ScheduleEvent GetSchedule(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //ScheduleEvent GetScheduleOnly(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //bool DeleteScheduleEvent(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, int task);

        bool UpdateEpisode(Guid agencyId, PatientEpisode episode); 
        bool UpdateEpisode(PatientEpisode episode);
        List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate);
        bool UpdateEpisodesForDischarge(Guid agencyId, Guid patientId, PatientEpisode episodeContainDischargeDate, List<PatientEpisode> episodesAfterDischargeDate);
        bool Activate(Guid agencyId, Guid patientId);
        bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate);
        bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete);
        MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId);
        
        bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory);
        bool AddNewMedicationProfile(MedicationProfile medication);

        MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        bool UpdateMedication(MedicationProfile medicationProfile);
        bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
      
        bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate);
        MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication);
        bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfile);
        MedicationProfileHistory GetSignedMedicationAssocatiedToAssessment(Guid patientId, Guid assessmentId);
        MedicationProfileHistory GetSignedMedicationProfileForPatientByEpisode(Guid patientId, Guid agencyId, Guid episodeId);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId);
        IList<MedicationProfileHistory> GetAllMedicationProfileHistory();

        bool DeleteMedicationProfile(Guid Id, Guid agencyId);

        bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted);
        bool DeleteEpisode(Guid agencyId, Guid patientId, Guid episodeId);
        MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId);
        MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId);
        bool SaveMedicationProfile(MedicationProfile medicationProfile);

        bool MarkVisitNoteAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        bool ReassignNotesUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        bool UpdateVisitNote(PatientVisitNote patientVisitNote);
        bool AddMissedVisit(MissedVisit missedVisit);
        bool UpdateMissedVisit(MissedVisit missedVisit);
        MissedVisit GetMissedVisit(Guid agencyId, Guid id);
        List<MissedVisit> GetMissedVisitsOnly(Guid agencyId, DateTime startDate, DateTime endDate);
       // List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds);
        List<MissedVisit> GetMissedVisitsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status);

        bool AddVisitNote(PatientVisitNote patientVisitNote);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId);
        PatientVisitNote GetVisitNoteByType(Guid episodeId, Guid patientId, DisciplineTasks disciplineTask);
        List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds, DateTime startDate, DateTime endDate);
        List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate);

        bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId, out Patient patientOut);
        bool AdmitPatient(PendingPatient patient, out Patient patientOut);
        bool NonAdmitPatient(PendingPatient pending, out Patient patientOut);
        bool AddPatientAdmissionDate(PatientAdmissionDate managedDate);
        bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate);
        bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DeletePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeletePatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DischargePatient(Guid agencyId, Guid patientId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        //ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId);

        bool AddAuthorization(Authorization authorization);
        bool EditAuthorization(Authorization authorization);
        IList<Authorization> GetAuthorizations( Guid agencyId , Guid patientId);
        IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, string insuranceId, string status, DateTime startDate, DateTime endDate);
        Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id);
        Authorization GetAuthorization(Guid agencyId, Guid Id);
        bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id);

        bool IsPatientIdExist(Guid agencyId, string patientIdNumber);
        bool IsMedicareExist(Guid agencyId, string medicareNumber);
        bool IsMedicaidExist(Guid agencyId, string medicaidNumber);
        bool IsSSNExist(Guid agencyId, string ssn);
        bool IsPatientIdExistForEdit(Guid agencyId, Guid patientId, string patientIdNumber);
        bool IsMedicareExistForEdit(Guid agencyId, Guid patientId, string medicareNumber);
        bool IsMedicaidExistForEdit(Guid agencyId, Guid patientId, string medicaidNumber);
        bool IsSSNExistForEdit(Guid agencyId, Guid patientId, string ssn);
        bool IsEpisodeExist(Guid agencyId, Guid episodeId);
        bool IsPatientExist(Guid agencyId, Guid patientId);

        //List<PatientEpisodeData> GetEpisodeByBranch(Guid branchCode, Guid agencyId);
        List<PatientVisitNote> GetPreviousNotes(Guid patientId, Guid agencyId);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId);
        //List<PatientEpisodeData> GetAllPatientEpisodeData(Guid agencyId);
        List<PatientVisitNote> GetVisitNotesByDisciplineTask(Guid patientId, Guid agencyId, DisciplineTasks task);
        List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks);
        bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId);
        List<FaceToFaceEncounter> GetAllFaceToFaceEncounters();
        bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateRequested);
        bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        bool RemoveFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id);
        bool DeleteFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated);
        bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds);
        List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds);
        List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds);
        //List<ScheduleEvent> GetCommunicationNoteScheduleEvents(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        //List<ScheduleEvent> GetPhysicianOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int status);
        //List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status);
        //List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        //PatientEpisode GetPreviousEpisode(Guid agencyId, Guid patientId, DateTime startDate);
        //PatientEpisode GetPreviousEpisodeFluent(Guid agencyId, Guid patientId, DateTime startDate);

        PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId);

        //List<PatientEpisodeTherapyException> GetAllEpisodeAfterApril(Guid agencyId, Guid branchId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PatientSocCertPeriod> PatientSocCertPeriods(Guid agencyId, string patientIds, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId, int status);
        List<PatientRoster> GetPatientByResponsiableEmployee(Guid agencyId, Guid userId, Guid branchId, int status);
        List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid agencyId, Guid caseManagerId, Guid branchId, int status);
        List<PatientRoster> GetPatientByInsurance(Guid agencyId, Guid branchId, int insuranceId, int status);
        List<PatientRoster> GetPatientByAdmissionMonthYear(Guid agencyId, Guid branchId, int status, int month, int year);
        List<PatientRoster> GetPatientByAdmissionMonthYearUnduplicated(Guid agencyId, Guid branchId, int status, int year);
        List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByAdmissionYear(Guid agencyId, Guid branchId, int status, int year);
        List<DischargePatient> GetDischargePatients(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientRoster(Guid agencyId, Guid branchId, int statusId, int insuranceId);
        List<PatientRoster> GetPatientRosterByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate);
        //List<PatientEpisode> GetSurveyCensesPatientEpisodes(Guid agencyId, string patientIds);
        List<PatientWithPhysicanInfo> GetPatientPhysicianInfos(Guid agencyId, Guid branchId, int statusId);
        List<SurveyCensus> GetSurveyCensesByStatus(Guid agencyId, Guid branchId, int statusId, int insuranceId);
        List<AdmissionEpisode> PatientAdmissonPeriods(Guid agencyId, Guid patientId);

        bool AddDeletedItem(DeletedItem deletedItem);
        bool UpdateDeletedItem(DeletedItem deletedItem);
        List<DeletedItem> GetDeletedItems(Guid agencyId, Guid patientId);
        DeletedItem GetDeletedItem(Guid agencyId, Guid episodeId, Guid patientId);

        List<MedicareEligibility> GetMedicareEligibilities(Guid agencyId, Guid patientId);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid patientId, Guid id);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid EpisodeId, Guid patientId, Guid id);
        bool UpdateMedicareEligibility(MedicareEligibility medicareEligibility);

        AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId);
        AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId);
        bool UpdateAllergyProfile(AllergyProfile allergyProfile);
        bool AddAllergyProfile(AllergyProfile allergyProfile);

        string LastPatientId(Guid agencyId);

        List<PatientEpisode> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid userId, byte statusId);
        List<PatientSelection> GetPatientEpisodeData(Guid agencyId, Guid branchId, Guid userId, byte statusId);
        //List<PatientEpisode> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid branchId, int statusId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeDataByBranchAndPatient(Guid agencyId, Guid branchId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PatientSelection> GetPatientEpisodeData(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId);

        //List<PatientSelection> GetUserPatients(Guid agencyId, Guid userId, byte statusId);
        List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId);
        List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId);

        bool AddHospitalizationLog(HospitalizationLog transferLog);
        bool UpdateHospitalizationLog(HospitalizationLog transferLog);
        HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId);
        List<HospitalizationLog> GetHospitalizationLogs(Guid patientId, Guid agencyId);


        List<PhysicianOrder> GetPhysicianOrdersByPhysician(List<Guid> physicianIdentifiers, int status);
        List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(List<Guid> physicianIdentifiers, int status);
        List<PatientVisitNote> GetEvalOrdersByPhysician(List<Guid> physicianIdentifiers, int status);

        List<PatientAdmission> GetPatientAdmissionsByDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        Dictionary<string, int> GetPatientAdmits(Guid agencyId, Guid branchId);

        //List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisode> GetEpisodesBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //bool DeleteScheduleEvents(Guid agencyId, Guid episodeId, Guid patientId, List<Guid> eventsToBeDeleted, out List<ScheduleEvent> deletedEvents);

        bool DeleteReturnComments(Guid agencyId, int id);
        bool AddReturnComment(ReturnComment returnComment);
        bool UpdateReturnComment(ReturnComment returnComment);
        List<ReturnComment> GetReturnComments(Guid agencyId, Guid episodeId, Guid eventId);
        ReturnComment GetReturnComment(Guid agencyId, int id);
        List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds);
        List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, Guid episodeId);
        List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds);
        List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, int status, List<Guid> missedVisitIds);

        bool RemoveModel<T>(Guid Id) where T : class, new();
        bool UpdateEntityForReassingUser(Guid agencyId, Guid userId, string script);
        bool UpdateEntityForReassingUser(Guid agencyId, Guid patientId, Guid userId, string script);
        bool UpdateEntityForDelete(Guid agencyId, Guid patientId, Guid episodeId, string script);
    }
}
