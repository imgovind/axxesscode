﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IPhysicianRepository
    {
        bool Add(AgencyPhysician physician);
        bool Update(AgencyPhysician physician);
        bool Delete(Guid agencyId, Guid id);
        bool Link(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkAll(Guid patientId);
        bool Unlink(Guid patientId, Guid physicianId);
        AgencyPhysician Get(Guid physicianId, Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification(Guid agencyId);
        bool DoesPhysicianExist(Guid patientId, Guid physicianId);
        bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode);
        IList<AgencyPhysician> GetPatientPhysicians( Guid agencyId,Guid patientId );
        bool SetPrimary(Guid patientId, Guid physicianId);
        AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId, Guid agencyId);
        IList<AgencyPhysician> GetByLoginId(Guid loginId);
        List<CarePlanOversight> GetCPO(Guid physicianLoginId);
        CarePlanOversight GetCPOById(Guid id);
        IList<AgencyPhysician> GetAllPhysicians();
        IList<PhysicainLicense> GeAgencyPhysicianLicenses(Guid agencyId, Guid physicianId);
        AgencyPhysician GetPatientPrimaryPhysician(Guid agencyId, Guid patientId);
        AgencyPhysician GetPatientPrimaryOrFirstPhysician(Guid agencyId, Guid patientId);
        List<AgencyPhysician> GetPhysiciansByIdsLean(Guid agencyId, List<Guid> physicianIds);
    }
}
