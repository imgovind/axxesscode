﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IUserRepository
    {
        IEnumerable<User> All();
        User Get(Guid id, Guid agencyId);
        User GetUserOnly(Guid id, Guid agencyId);
        User GetByLoginId(Guid loginId);
        IList<User> GetUsersByLoginId(Guid loginId);
        IList<User> GetUsersByLoginId(Guid loginId, Guid agencyId);
        int GetActiveUserCount(Guid agencyId);
        bool Add(User user);
        bool Delete(Guid agencyId, Guid userId);
        bool Update(User user);
        bool UpdateModel(User user, bool IsCacheRefresh);
        bool UpdateProfile(User user);
        IList<User> GetAgencyUsers(Guid agencyId);
        IList<User> GetUsersOnly(Guid agencyId);
        IList<User> GetUsersOnly(Guid agencyId, int status);
        IList<User> GetUsersOnlyByBranch(Guid branchId, Guid agencyId);
        IList<User> GetRatedUserByBranch(Guid branchId, Guid agencyId);
        IList<User> GetUsersOnlyByBranch(Guid branchId, Guid agencyId, int status);
        IList<User> GetEmployeeRoster(Guid agencyId, Guid branchId, int status);
        IList<User> GetUsersByStatus(Guid agencyId, Guid branchId, int status);
        IList<User> GetUsersByStatusLean(Guid agencyId, Guid branchId, int status);
        IList<User> GetUsersByStatusAndDOB(Guid agencyId, Guid branchId, int status, int month);
        IList<User> GetUsersByStatusAndLicenses(Guid agencyId, Guid branchId, int status);
        IList<User> GetUsersByStatusAndPermissions(Guid agencyId, Guid branchId, int status);
        IList<User> GetClinicalUsers(Guid agencyId);
        IList<User> GetOTUsers(Guid agencyId);
        IList<User> GetPTUsers(Guid agencyId);
        IList<User> GetHHAUsers(Guid agencyId);
        IList<User> GetLVNUsers(Guid agencyId);
        IList<User> GetCaseManagerUsers(Guid agencyId);
        IList<User> GetAuditors(Guid agencyId);
        IList<User> GetAgencyUsers(string query, Guid agencyId);
        //bool Reassign(Guid agencyId, ScheduleEvent scheduleEvent, Guid userId);
        //UserEvent GetEvent(Guid agencyId, Guid userId, Guid patientId, Guid eventId);
        //bool DeleteScheduleEvent(Guid patientId, Guid eventId, Guid userId);
        //bool RemoveScheduleEvent(Guid agencyId ,Guid patientId, Guid eventId, Guid userId);
        int GetUserPatientCount(Guid agencyId, Guid userId, byte statusId);
        bool SetUserStatus(Guid agencyId, Guid userId, int status);
        IList<License> GetUserLicenses(Guid agencyId, Guid userId, bool isAssetNeeded);
        IList<AgencyLite> GetAgencies(Guid loginId);
        IList<User> GetAll();

        LicenseItem GetNonUserLicense(Guid licenseId, Guid agencyId);
        bool UpdateNonUserLicense(LicenseItem item);
        bool AddNonUserLicense(LicenseItem licenseItem);
        IList<LicenseItem> GetNonUserLicenses(Guid agencyId);
        IList<LicenseItem> GetSoftwareUserLicenses(Guid agencyId);
        LicenseItem GetUserLicenseItem(Guid licenseId, Guid userId, Guid agencyId);
        List<User> GetAllUsers(Guid agencyId, Guid branchId, int statusId);
        Dictionary<string, User> GetAllUsers(Guid agencyId);
        List<User> GetUsersByIds(Guid agencyId, List<Guid> userIds);
        List<User> GetUsersWithCredentialsByIds(Guid agencyId, List<Guid> userIds);
        IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter);
    }
}
