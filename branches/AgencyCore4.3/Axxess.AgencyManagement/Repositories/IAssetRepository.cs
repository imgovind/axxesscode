﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAssetRepository
    {
        bool Add(Asset asset);
        bool AddMany(List<Asset> assets);
        bool Delete(Guid id);
        bool DeleteMany(Guid agencyId, List<Guid> assetIds);
        Asset Get(Guid id, Guid agencyId);
    }
}
