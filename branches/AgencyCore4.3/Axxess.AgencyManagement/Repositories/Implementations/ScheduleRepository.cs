﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Common;
    using Axxess.AgencyManagement.Enums;

    public class ScheduleRepository : IScheduleRepository 
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ScheduleRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Add Schedule

        public bool AddScheduleEvent(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Add<ScheduleEvent>(scheduleEvent);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        #endregion

        #region Update Schedule

        public bool UpdateScheduleEventNew(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Update<ScheduleEvent>(scheduleEvent);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        #endregion

        #region Remove Schedule

        public bool RemoveScheduleEventNew(ScheduleEvent scheduleEvent)
        {
            var result = 0;
            var script = "DELETE FROM  scheduleevents WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId = @eventId ";
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", scheduleEvent.AgencyId)
                .AddGuid("patientid", scheduleEvent.PatientId)
                .AddGuid("episodeid", scheduleEvent.EpisodeId)
                .AddGuid("eventId", scheduleEvent.EventId).AsNonQuery();

            }
            return result > 0;
        }

        public bool RemoveScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            var result = 0;
            var script = "DELETE FROM  scheduleevents WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId = @eventId ";
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddGuid("eventId", eventId).AsNonQuery();

            }
            return result > 0;
        }

        #endregion

        #region Get Schedule

        public ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<ScheduleEvent>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EventId == eventId);
        }

        public ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            return database.Single<ScheduleEvent>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EpisodeId == episodeId && a.EventId == eventId);
        }

        public ScheduleEvent GetLastScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsDeprecated as IsDeprecated , 
                        IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        IsVisitPaid as IsVisitPaid ,
                        InPrintQueue as InPrintQueue  
                            FROM
                                scheduleevents 
                                    WHERE
                                        AgencyId = @agencyid AND 
                                        PatientId = @patientid AND
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND
                                        DisciplineTask > 0 AND
                                        IsMissedVisit = 0  AND
                                        DATE(EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                        DATE(EventDate) between DATE(@startdate) and DATE(@enddate) {0}  
                                            ORDER BY DATE(EventDate) DESC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = startDate,
                    EndDate = endDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetFirstScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsVisitPaid as IsVisitPaid  
                            FROM 
                                scheduleevents  
                                    WHERE 
                                        AgencyId = @agencyid AND 
                                        PatientId = @patientid AND
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND 
                                        DisciplineTask > 0 AND 
                                        IsMissedVisit = 0  AND 
                                        DATE(EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND
                                        DATE(EventDate) between DATE(@startdate) and DATE(@enddate) {0} 
                                            ORDER BY DATE(EventDate) ASC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    StartDate = episodeStartDate,
                    EndDate = episodeEndDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents
                                    WHERE 
                                        scheduleevents.AgencyId = @agencyid AND 
                                        scheduleevents.PatientId = @patientid  AND
                                        scheduleevents.EpisodeId = @episodeid AND 
                                        scheduleevents.IsDeprecated = 0 AND 
                                        scheduleevents.IsBillable = 1 AND  
                                        scheduleevents.IsMissedVisit = 0  AND
                                        DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                        scheduleevents.Status IN ( {0} ) 
                                            ORDER BY 
                                                 DATE(scheduleevents.VisitDate) ASC LIMIT 1", status.Join(","));

            var schedule = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                schedule = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsSingle();
            }
            return schedule;
        }

        #endregion

        #region Get Schedules

        //        public List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded)
        //        {
        //            var disciplineScript = string.Empty;
        //            if (discipline.IsEqual("all"))
        //            {

        //            }
        //            else if (discipline.IsEqual("Therapy"))
        //            {
        //                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
        //            }
        //            else
        //            {
        //                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
        //                {
        //                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
        //                }
        //                else
        //                {
        //                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
        //                }
        //            }
        //            var script = string.Format(@"SELECT 
        //                        scheduleevents.EventId as EventId ,
        //                        scheduleevents.PatientId as PatientId ,
        //                        scheduleevents.EpisodeId as EpisodeId ,
        //                        scheduleevents.UserId as UserId ,
        //                        scheduleevents.DisciplineTask as DisciplineTask , 
        //                        scheduleevents.EventDate as EventDate ,
        //                        scheduleevents.VisitDate as VisitDate ,
        //                        scheduleevents.Status as Status ,
        //                        scheduleevents.Discipline as Discipline ,
        //                        scheduleevents.IsMissedVisit as IsMissedVisit , 
        //                        scheduleevents.ReturnReason as ReturnReason ,
        //                        scheduleevents.Comments as Comments ,
        //                        scheduleevents.StartDate as StartDate ,
        //                        scheduleevents.EndDate as EndDate ,
        //                        scheduleevents.Asset as Asset ,
        //                        scheduleevents.Version  as Version ,
        //                        scheduleevents.UserName  as UserName ,
        //                        users.FirstName as FirstName , 
        //                        users.LastName as LastName ,
        //                        users.Suffix as Suffix , 
        //                        users.Credentials as Credentials ,
        //                        users.CredentialsOther as CredentialsOther ,
        //                        users.IsDeprecated as UserIsDeprecated
        //                            FROM 
        //                                scheduleevents 
        //                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
        //                                        WHERE 
        //                                            scheduleevents.AgencyId = @agencyid AND
        //                                            scheduleevents.PatientId = @patientid AND 
        //                                            scheduleevents.EpisodeId = @episodeid AND 
        //                                            scheduleevents.IsDeprecated = 0 AND
        //                                            scheduleevents.DisciplineTask > 0 {0}  
        //                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript);

        //            var list = new List<ScheduleEvent>();
        //            using (var cmd = new FluentCommand<ScheduleEvent>(script))
        //            {
        //                list = cmd.SetConnection("AgencyManagementConnectionString")
        //                .AddGuid("agencyid", agencyId)
        //                .AddGuid("patientid", patientId)
        //                .AddGuid("episodeid", episodeId)
        //                .AddString("discipline", discipline)
        //                .SetMap(reader => new ScheduleEvent
        //                {
        //                    EventId = reader.GetGuid("EventId"),
        //                    PatientId = reader.GetGuid("PatientId"),
        //                    EpisodeId = reader.GetGuid("EpisodeId"),
        //                    UserId = reader.GetGuid("UserId"),
        //                    DisciplineTask = reader.GetInt("DisciplineTask"),
        //                    EventDate = reader.GetDateTime("EventDate"),
        //                    VisitDate = reader.GetDateTime("VisitDate"),
        //                    Status = reader.GetIntOrZero("Status"),
        //                    Discipline = reader.GetStringNullable("Discipline"),
        //                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
        //                    ReturnReason = reader.GetStringNullable("ReturnReason"),
        //                    Comments = reader.GetStringNullable("Comments"),
        //                    StartDate = reader.GetDateTime("StartDate"),
        //                    EndDate = reader.GetDateTime("EndDate"),
        //                    Asset = reader.GetStringNullable("Asset"),
        //                    Version = reader.GetInt("Version"),
        //                    UserName = reader.GetStringNullable("UserName"),
        //                    UserFirstName = reader.GetStringNullable("FirstName"),
        //                    UserLastName = reader.GetStringNullable("LastName"),
        //                    UserSuffix = reader.GetStringNullable("Suffix"),
        //                    Credentials = reader.GetStringNullable("Credentials"),
        //                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
        //                })
        //                .AsList();
        //            }
        //            return list;
        //        }

        public List<ScheduleEvent> GetScheduledEventsOnlyWithId(Guid agencyId, Guid patientId,string eventIds, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        Surcharge as Surcharge ,
                        AssociatedMileage as AssociatedMileage ,
                        IsDeprecated as IsDeprecated , 
                        IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        IsVisitPaid as IsVisitPaid ,
                        InPrintQueue as InPrintQueue ,
                        StartDate as StartDate ,
                        EndDate as EndDate ,
                        Version  as Version 
                            FROM 
                                scheduleevents 
                                        WHERE 
                                            AgencyId = @agencyid AND
                                            PatientId = @patientid AND 
                                            EventId IN ( {0} ) AND
                                            IsDeprecated = 0 AND 
                                            DisciplineTask > 0 AND 
                                            DATE(EventDate) between DATE(@startdate) and DATE(@enddate) {1}
                                                ORDER BY DATE(EventDate) DESC ",eventIds, additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.IsDeprecated as IsDeprecated , 
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        scheduleevents.InPrintQueue as InPrintQueue ,
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate ,
                        scheduleevents.Version  as Version 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript); //AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }
       
        public List<ScheduleEvent> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript); //AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                })
                .AsList();
            }
            return list;
        }
       
        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EpisodeId == episodeId && e.IsDeprecated == false && e.DisciplineTask > 0).Where(e => e.VisitDate.Date >= startDate.Date && e.VisitDate.Date <= endDate.Date).OrderBy(s => s.VisitDate.Date).ToList();

        }
       
        public List<ScheduleEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };


            var script1 = string.Format(@"SELECT 
                    '{5}' as EventId ,
                    patientepisodes.PatientId as PatientId ,
                    patientepisodes.Id as EpisodeId ,
                    '{5}' as UserId ,
                    {7} as DisciplineTask , 
                    '{6}' as VisitDate ,
                    '{6}' as EventDate ,
                    {8} as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           Not Exists ( SELECT * from scheduleevents WHERE scheduleevents.PatientId = patients.Id AND scheduleevents.EpisodeId = patientepisodes.Id AND DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND scheduleevents.DisciplineTask > 0 AND scheduleevents.IsDeprecated = 0 AND  scheduleevents.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4}) )",
                                                                                   insurance,
                                                                                   !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                                                   string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   Guid.Empty.ToString(),
                                                                                   DateTime.MinValue.ToString("yyyy-MM-dd"),
                                                                                   (int)DisciplineTasks.OASISCRecertification,
                                                                                   0);
            var script2 = string.Format(@"SELECT 
                    scheduleevents.EventId as EventId ,
                    scheduleevents.PatientId as PatientId ,
                    scheduleevents.EpisodeId as EpisodeId ,
                    scheduleevents.UserId as UserId ,
                    scheduleevents.DisciplineTask as DisciplineTask , 
                    scheduleevents.EventDate as EventDate ,
                    scheduleevents.VisitDate as VisitDate ,
                    scheduleevents.Status as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                LEFT JOIN scheduleevents ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATE(patientepisodes.EndDate) < DATE(curdate()) AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                           scheduleevents.DisciplineTask > 0 AND
                                           scheduleevents.IsDeprecated = 0 AND 
                                           scheduleevents.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} OR {4} )",
                                                           insurance,
                                                           !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                           string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")), 
                                                           string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                           string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };



            var script1 = string.Format(@"SELECT 
                    '{5}' as EventId ,
                    patientepisodes.PatientId as PatientId ,
                    patientepisodes.Id as EpisodeId ,
                    '{5}' as UserId ,
                    {7} as DisciplineTask , 
                    '{6}' as EventDate ,
                    '{6}' as VisitDate ,
                    {8} as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           Not Exists ( SELECT * from scheduleevents WHERE scheduleevents.PatientId = patients.Id AND scheduleevents.EpisodeId = patientepisodes.Id AND DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND scheduleevents.DisciplineTask > 0 AND scheduleevents.IsDeprecated = 0 AND  scheduleevents.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4} ) )",
                                                                                     insurance,
                                                                                     !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                                                     string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     Guid.Empty.ToString(),
                                                                                     DateTime.MinValue.ToString("yyyy-MM-dd"),
                                                                                     (int)DisciplineTasks.OASISCRecertification,
                                                                                     0);
            var script2 = string.Format(@"SELECT 
                    scheduleevents.EventId as EventId ,
                    scheduleevents.PatientId as PatientId ,
                    scheduleevents.EpisodeId as EpisodeId ,
                    scheduleevents.UserId as UserId ,
                    scheduleevents.DisciplineTask as DisciplineTask , 
                    scheduleevents.EventDate as EventDate ,
                    scheduleevents.VisitDate as VisitDate ,
                    scheduleevents.Status as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                LEFT JOIN scheduleevents ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                           scheduleevents.DisciplineTask > 0 AND
                                           scheduleevents.IsDeprecated = 0 AND 
                                           scheduleevents.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} OR {4} ) ",
                                                            insurance,
                                                            !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                            string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                            string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                            string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND patients.AgencyLocationId = @branchId ";
            }

            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (status != null && status.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Status IN ( {0} )", status.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND
                                            patients.Status IN (1,2) {0} AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} ",branchScript, additionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        
        public List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, DateTime fromDate, DateTime toDate)
        {
            var disciplines = DisciplineFactory.TherapyDisciplines().Select(d => d.ToString()).ToArray();// new string[] { Disciplines.PT.ToString(), Disciplines.OT.ToString(), Disciplines.ST.ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        patientepisodes.EndDate as EndDate,
                        patientepisodes.StartDate as StartDate,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName, 
                        patients.PatientIdNumber as PatientIdNumber 
                                 FROM scheduleevents INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id INNER JOIN patients ON scheduleevents.PatientId = patients.Id    
                                      WHERE 
                                            scheduleevents.AgencyId = @agencyid {0} AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status = 1 AND
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0 AND 
                                            DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            scheduleevents.IsMissedVisit = 0 AND 
                                            scheduleevents.Discipline IN ( {1} )", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty, disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", fromDate)
                .AddDateTime("enddate", toDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetScheduleEventsByStatusDisciplineAndRange(Guid agencyId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks)
        {
            var scheduleList = new List<ScheduleEvent>();
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {

                var statusScript = string.Empty; ;
                if (patientStatus <= 0)
                {
                    statusScript = " AND  patients.Status IN (1,2)";
                }
                else
                {
                    statusScript = " AND patients.Status = @statusid";
                }
                string branchScript = string.Empty;
                if (!branchId.IsEmpty())
                {
                    branchScript = " AND patients.AgencyLocationId = @branchId ";
                }
                var additionalScript = string.Empty;
                if (disciplineTasks != null && disciplineTasks.Length > 0)
                {
                    additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
                }
                if (scheduleStatus != null && scheduleStatus.Length > 0)
                {
                    additionalScript += string.Format(" AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }

                var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode ,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName, 
                        patients.PatientIdNumber as PatientIdNumber,
                        patients.MiddleInitial as MiddleInitial 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 {0} {1} AND 
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {2} ",statusScript, branchScript, additionalScript);


                using (var cmd = new FluentCommand<ScheduleEvent>(script))
                {
                    scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusid", patientStatus)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new ScheduleEvent
                    {
                        EventId = reader.GetGuid("EventId"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                    })
                    .AsList();
                }
            }
            return scheduleList;
        }
        
        public List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().ToArray();//new int[] { (int)DisciplineTasks.FaceToFaceEncounter, (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485, (int)DisciplineTasks.PhysicianOrder, (int)DisciplineTasks.PTEvaluation, (int)DisciplineTasks.OTEvaluation, (int)DisciplineTasks.STEvaluation, (int)DisciplineTasks.PTReEvaluation, (int)DisciplineTasks.OTReEvaluation, (int)DisciplineTasks.STReEvaluation };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2) AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            scheduleevents.DisciplineTask IN ( {0} ) AND
                                            scheduleevents.Status IN ( {1} ) {2}", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
       
        public List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().Select(s => s.ToString()).ToArray();// new string[] { ((int)DisciplineTasks.FaceToFaceEncounter).ToString(), ((int)DisciplineTasks.HCFA485StandAlone).ToString(), ((int)DisciplineTasks.HCFA485).ToString(), ((int)DisciplineTasks.NonOasisHCFA485).ToString(), ((int)DisciplineTasks.PhysicianOrder).ToString(), ((int)DisciplineTasks.PTEvaluation).ToString(), ((int)DisciplineTasks.OTEvaluation).ToString(), ((int)DisciplineTasks.STEvaluation).ToString(), ((int)DisciplineTasks.PTReEvaluation).ToString(), ((int)DisciplineTasks.OTReEvaluation).ToString(), ((int)DisciplineTasks.STReEvaluation).ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        
        public List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().Select(s => s.ToString()).ToArray();// new string[] { ((int)DisciplineTasks.FaceToFaceEncounter).ToString(), ((int)DisciplineTasks.HCFA485).ToString(), ((int)DisciplineTasks.NonOasisHCFA485).ToString(), ((int)DisciplineTasks.PhysicianOrder).ToString(), ((int)DisciplineTasks.HCFA485StandAlone).ToString(), ((int)DisciplineTasks.PTEvaluation).ToString(), ((int)DisciplineTasks.OTEvaluation).ToString(), ((int)DisciplineTasks.STEvaluation).ToString(), ((int)DisciplineTasks.PTReEvaluation).ToString(), ((int)DisciplineTasks.OTReEvaluation).ToString(), ((int)DisciplineTasks.STReEvaluation).ToString() };
            var status = ScheduleStatusFactory.OrdersPendingPhysicianSignature().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString(), ((int)ScheduleStatus.OrderSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString() };

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2)  AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            scheduleevents.DisciplineTask IN ( {0} ) AND
                                            scheduleevents.Status IN ( {1} ) {2}", disciplineTasks.Join(","), status.Join(","), !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
       
        public List<ScheduleEvent> GetEpisodesForSurveyCensesAndPatientRoster(Guid agencyId, string patientIds)
        {
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments(true).ToArray();
            var list = new List<ScheduleEvent>();
            var script = string.Format(@"SELECT 
                                            agencymanagement.scheduleevents.EventId as EventId ,
                                            agencymanagement.patientepisodes.PatientId as PatientId,
                                            agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                                            agencymanagement.patientepisodes.StartDate as StartDate, 
                                            agencymanagement.patientepisodes.EndDate as EndDate,
                                            agencymanagement.scheduleevents.UserId as UserId ,
                                            agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                                            agencymanagement.scheduleevents.EventDate as EventDate ,
                                            agencymanagement.scheduleevents.VisitDate as VisitDate ,
                                            agencymanagement.scheduleevents.Status as Status ,
                                            agencymanagement.scheduleevents.Discipline as Discipline ,
                                            oasisc.assessments.OasisData as Note 
                                                    FROM  agencymanagement.patientepisodes 
                                                           LEFT JOIN agencymanagement.scheduleevents ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id 
                                                           LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.EpisodeId = agencymanagement.scheduleevents.EpisodeId AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId AND agencymanagement.scheduleevents.DisciplineTask IN ( {1} )
                                                                WHERE 
                                                                     agencymanagement.patientepisodes.AgencyId = @agencyid AND 
                                                                     agencymanagement.patientepisodes.PatientId IN ( {0} ) AND
                                                                     agencymanagement.patientepisodes.IsActive = 1 AND 
                                                                     agencymanagement.patientepisodes.IsDischarged = 0 AND 
                                                                     agencymanagement.scheduleevents.DisciplineTask > 0 AND
                                                                     agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                     agencymanagement.scheduleevents.IsMissedVisit = 0 AND
                                                                     DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND
                                                                     DATE(agencymanagement.patientepisodes.StartDate) < Date(@startdate)", patientIds, disciplineTasksOASISList.Select(d => d.ToString()).ToArray().Join(","));

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", DateTime.Now)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    Note = reader.GetStringNullable("Note")

                }).AsList();
            }
            return list;
        }
       
        public List<ScheduleEvent> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsDeprecated as IsDeprecated , 
                        IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        IsVisitPaid as IsVisitPaid ,
                        InPrintQueue as InPrintQueue ,
                        StartDate as StartDate ,
                        EndDate as EndDate  
                            FROM 
                                scheduleevents 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        PatientId = @patientid AND 
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND
                                        DisciplineTask > 0 
                                            ORDER BY DATE(EventDate) DESC ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return list;
        }
       
        public List<ScheduleEvent> GetPatientScheduledEventsOnlyLeanNew(Guid agencyId, Guid episodeId, Guid patientId, int[] scheduleStatus, string discipline, int[] disciplineTasks, bool isReportAndNotesIncluded)
        {
            var additionalScript = string.Empty;
          
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (discipline.IsEqual("all"))
            {
                additionalScript += string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                additionalScript += " AND (STRCMP(Discipline ,'PT') = 1 OR STRCMP(Discipline, 'OT') = 1 OR STRCMP(Discipline , 'ST') = 1) ";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                additionalScript += " AND (STRCMP(Discipline ,'Nursing') = 1 OR STRCMP(Discipline, 'ReportsAndNotes') = 1) ";
            }
            else
            {
                additionalScript += " AND Discipline = @discipline ";
            }
            //if (discipline.IsNotNullOrEmpty() && discipline.IsEqual("all"))
            //{
            //    additionalScript += string.Format(" AND Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            //}
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        StartDate as StartDate ,
                        EndDate as EndDate  
                            FROM 
                                scheduleevents 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        PatientId = @patientid AND 
                                        EpisodeId = @episodeid {0} AND
                                        IsDeprecated = 0 AND
                                        DisciplineTask > 0 
                                            ORDER BY DATE(EventDate) DESC ",additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return list;
        }
       
        //        public List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate)
        //        {
        //            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
        //            var script = string.Format(@"SELECT 
        //                        scheduleevents.EventId as EventId ,
        //                        scheduleevents.PatientId as PatientId ,
        //                        scheduleevents.EpisodeId as EpisodeId ,
        //                        scheduleevents.UserId as UserId ,
        //                        scheduleevents.DisciplineTask as DisciplineTask , 
        //                        scheduleevents.EventDate as EventDate ,
        //                        scheduleevents.VisitDate as VisitDate ,
        //                        scheduleevents.Status as Status ,
        //                        scheduleevents.Discipline as Discipline ,
        //                        scheduleevents.IsBillable as IsBillable ,
        //                        scheduleevents.IsMissedVisit as IsMissedVisit , 
        //                        scheduleevents.TimeIn as TimeIn , 
        //                        scheduleevents.TimeOut as TimeOut ,
        //                        scheduleevents.Surcharge as Surcharge ,
        //                        scheduleevents.AssociatedMileage as AssociatedMileage ,
        //                        scheduleevents.ReturnReason as ReturnReason ,
        //                        scheduleevents.Comments as Comments ,
        //                        scheduleevents.IsDeprecated as IsDeprecated , 
        //                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
        //                        scheduleevents.IsVisitPaid as IsVisitPaid ,
        //                        scheduleevents.InPrintQueue as InPrintQueue ,
        //                        scheduleevents.StartDate as StartDate ,
        //                        scheduleevents.EndDate as EndDate ,
        //                        scheduleevents.Asset as Asset ,
        //                        scheduleevents.Version  as Version ,
        //                        scheduleevents.UserName  as UserName ,
        //                        users.FirstName as FirstName , 
        //                        users.LastName as LastName ,
        //                        users.Suffix as Suffix , 
        //                        users.Credentials as Credentials ,
        //                        users.CredentialsOther as CredentialsOther ,
        //                        users.IsDeprecated as UserIsDeprecated 
        //                            FROM
        //                                scheduleevents 
        //                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
        //                                        WHERE 
        //                                            scheduleevents.AgencyId = @agencyid AND 
        //                                            scheduleevents.PatientId = @patientid AND 
        //                                            scheduleevents.EpisodeId = @episodeid AND 
        //                                            scheduleevents.IsDeprecated = 0 AND 
        //                                            scheduleevents.DisciplineTask > 0 {0} 
        //                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", dateRange);

        //            var list = new List<ScheduleEvent>();
        //            using (var cmd = new FluentCommand<ScheduleEvent>(script))
        //            {
        //                list = cmd.SetConnection("AgencyManagementConnectionString")
        //                .AddGuid("agencyid", agencyId)
        //                .AddGuid("patientid", patientId)
        //                .AddGuid("episodeid", episodeId)
        //                .AddDateTime("startdate", startDate)
        //                .AddDateTime("enddate", endDate)
        //                .SetMap(reader => new ScheduleEvent
        //                {
        //                    EventId = reader.GetGuid("EventId"),
        //                    PatientId = reader.GetGuid("PatientId"),
        //                    EpisodeId = reader.GetGuid("EpisodeId"),
        //                    UserId = reader.GetGuid("UserId"),
        //                    DisciplineTask = reader.GetInt("DisciplineTask"),
        //                    EventDate = reader.GetDateTime("EventDate"),
        //                    VisitDate = reader.GetDateTime("VisitDate"),
        //                    Status = reader.GetIntOrZero("Status"),
        //                    Discipline = reader.GetStringNullable("Discipline"),
        //                    IsBillable = reader.GetBoolean("IsBillable"),
        //                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
        //                    TimeIn = reader.GetStringNullable("TimeIn"),
        //                    TimeOut = reader.GetStringNullable("TimeOut"),
        //                    Surcharge = reader.GetStringNullable("Surcharge"),
        //                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
        //                    ReturnReason = reader.GetStringNullable("ReturnReason"),
        //                    Comments = reader.GetStringNullable("Comments"),
        //                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
        //                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
        //                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
        //                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
        //                    StartDate = reader.GetDateTime("StartDate"),
        //                    EndDate = reader.GetDateTime("EndDate"),
        //                    Asset = reader.GetStringNullable("Asset"),
        //                    Version = reader.GetInt("Version"),
        //                    UserName = reader.GetStringNullable("UserName"),
        //                    UserFirstName = reader.GetStringNullable("FirstName"),
        //                    UserLastName = reader.GetStringNullable("LastName"),
        //                    UserSuffix = reader.GetStringNullable("Suffix"),
        //                    Credentials = reader.GetStringNullable("Credentials"),
        //                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
        //                })
        //                .AsList();
        //            }
        //            return list;
        //        }
        //        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        //        {
        //            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
        //            var disciplineScript = string.Empty;
        //            if (discipline.IsEqual("all"))
        //            {

        //            }
        //            else if (discipline.IsEqual("Therapy"))
        //            {
        //                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
        //            }
        //            else
        //            {
        //                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
        //                {
        //                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
        //                }
        //                else
        //                {
        //                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
        //                }
        //            }
        //            var script = string.Format(@"SELECT 
        //                        scheduleevents.EventId as EventId ,
        //                        scheduleevents.PatientId as PatientId ,
        //                        scheduleevents.EpisodeId as EpisodeId ,
        //                        scheduleevents.UserId as UserId ,
        //                        scheduleevents.EventDate as EventDate ,
        //                        scheduleevents.VisitDate as VisitDate ,
        //                        scheduleevents.Discipline as Discipline ,
        //                        scheduleevents.DisciplineTask as DisciplineTask , 
        //                        scheduleevents.Status as Status ,
        //                        scheduleevents.IsBillable as IsBillable ,
        //                        scheduleevents.IsMissedVisit as IsMissedVisit , 
        //                        scheduleevents.ReturnReason as ReturnReason ,
        //                        scheduleevents.Comments as Comments ,
        //                        scheduleevents.Asset as Asset ,
        //                        scheduleevents.UserName  as UserName ,
        //                        users.FirstName as FirstName , 
        //                        users.LastName as LastName ,
        //                        users.Suffix as Suffix , 
        //                        users.Credentials as Credentials ,
        //                        users.CredentialsOther as CredentialsOther ,
        //                        users.IsDeprecated as UserIsDeprecated 
        //                            FROM 
        //                                scheduleevents
        //                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
        //                                        WHERE 
        //                                            scheduleevents.AgencyId = @agencyid AND 
        //                                            scheduleevents.PatientId = @patientid AND 
        //                                            scheduleevents.EpisodeId = @episodeid AND 
        //                                            scheduleevents.IsDeprecated = 0 AND 
        //                                            scheduleevents.DisciplineTask > 0 {0} {1} 
        //                                                ORDER BY 
        //                                                    DATE(scheduleevents.EventDate) DESC ", disciplineScript, dateRange);

        //            var list = new List<ScheduleEvent>();
        //            using (var cmd = new FluentCommand<ScheduleEvent>(script))
        //            {
        //                list = cmd.SetConnection("AgencyManagementConnectionString")
        //                .AddGuid("agencyid", agencyId)
        //                .AddGuid("patientid", patientId)
        //                .AddGuid("episodeid", episodeId)
        //                .AddString("discipline", discipline)
        //                .AddDateTime("startdate", startDate)
        //                .AddDateTime("enddate", endDate)
        //                .SetMap(reader => new ScheduleEvent
        //                {
        //                    EventId = reader.GetGuid("EventId"),
        //                    PatientId = reader.GetGuid("PatientId"),
        //                    EpisodeId = reader.GetGuid("EpisodeId"),
        //                    UserId = reader.GetGuid("UserId"),
        //                    EventDate = reader.GetDateTime("EventDate"),
        //                    VisitDate = reader.GetDateTime("VisitDate"),
        //                    Discipline = reader.GetStringNullable("Discipline"),
        //                    DisciplineTask = reader.GetInt("DisciplineTask"),
        //                    Status = reader.GetIntOrZero("Status"),
        //                    IsBillable = reader.GetBoolean("IsBillable"),
        //                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
        //                    ReturnReason = reader.GetStringNullable("ReturnReason"),
        //                    Comments = reader.GetStringNullable("Comments"),
        //                    Asset = reader.GetStringNullable("Asset"),
        //                    UserName = reader.GetStringNullable("UserName"),
        //                    UserFirstName = reader.GetStringNullable("FirstName"),
        //                    UserLastName = reader.GetStringNullable("LastName"),
        //                    UserSuffix = reader.GetStringNullable("Suffix"),
        //                    Credentials = reader.GetStringNullable("Credentials"),
        //                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
        //                })
        //                .AsList();
        //            }
        //            return list;
        //        }
       
        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid [] episodeIds, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var episodeScript = string.Empty;
            if (episodeIds != null && episodeIds.Length > 0)
            {
                if (episodeIds.Length == 1)
                {
                    episodeScript += string.Format(" AND scheduleevents.EpisodeId = '{0}' ", episodeIds.FirstOrDefault());
                }
                else
                {
                    episodeScript += string.Format(" AND scheduleevents.EpisodeId IN ( {0} )",episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ") );
                }
            }
            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {
                disciplineScript = string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND (STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP(scheduleevents.Discipline, 'OT') = 1 OR STRCMP(scheduleevents.Discipline , 'ST') = 1) ";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                disciplineScript = " AND (STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP(scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
            }
            else
            {
                disciplineScript = " AND scheduleevents.Discipline = @discipline ";
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName 
                            FROM 
                                scheduleevents 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid {2} AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0 {0} {1} 
                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript, dateRange, episodeScript);
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddString("discipline", discipline)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

//        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
//        {
//            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
//            var disciplineScript = string.Empty;
//            if (discipline.IsEqual("all"))
//            {

//            }
//            else if (discipline.IsEqual("Therapy"))
//            {
//                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
//            }
//            else
//            {
//                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
//                {
//                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
//                }
//                else
//                {
//                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
//                }
//            }
//            var script = string.Format(@"SELECT 
//                        scheduleevents.EventId as EventId ,
//                        scheduleevents.PatientId as PatientId ,
//                        scheduleevents.EpisodeId as EpisodeId ,
//                        scheduleevents.UserId as UserId ,
//                        scheduleevents.EventDate as EventDate ,
//                        scheduleevents.VisitDate as VisitDate ,
//                        scheduleevents.Discipline as Discipline ,
//                        scheduleevents.DisciplineTask as DisciplineTask , 
//                        scheduleevents.Status as Status ,
//                        scheduleevents.IsBillable as IsBillable ,
//                        scheduleevents.IsMissedVisit as IsMissedVisit , 
//                        scheduleevents.ReturnReason as ReturnReason ,
//                        scheduleevents.Comments as Comments ,
//                        scheduleevents.Asset as Asset ,
//                        scheduleevents.UserName  as UserName 
//                            FROM 
//                                scheduleevents
//                                        WHERE 
//                                            scheduleevents.AgencyId = @agencyid AND 
//                                            scheduleevents.PatientId = @patientid AND 
//                                            scheduleevents.EpisodeId = @episodeid AND 
//                                            scheduleevents.IsDeprecated = 0 AND 
//                                            scheduleevents.DisciplineTask > 0 {0} {1} 
//                                                ORDER BY 
//                                                    DATE(scheduleevents.EventDate) DESC ", disciplineScript, dateRange);

//            var list = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .AddGuid("episodeid", episodeId)
//                .AddString("discipline", discipline)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .SetMap(reader => new ScheduleEvent
//                {
//                    EventId = reader.GetGuid("EventId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    EventDate = reader.GetDateTime("EventDate"),
//                    VisitDate = reader.GetDateTime("VisitDate"),
//                    Discipline = reader.GetStringNullable("Discipline"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    Status = reader.GetInt("Status", 0),
//                    IsBillable = reader.GetBoolean("IsBillable"),
//                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
//                    ReturnReason = reader.GetStringNullable("ReturnReason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    Asset = reader.GetStringNullable("Asset")
//                })
//                .AsList();
//            }
//            return list;
//        }

        
        public IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid patientId, Guid employeeId)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }
        
        public IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid employeeId)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }
       
        public List<ScheduleEvent> GetDeletedItemsNew(Guid agencyId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName 
                            FROM
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 1 AND
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.EventId != @eventid  AND
                                            scheduleevents.IsMissedVisit = 0  AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            scheduleevents.DisciplineTask IN ({0}) AND
                                            scheduleevents.Status IN ({1}) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.EventId)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id  
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.EventId != @eventid  AND
                                            scheduleevents.IsMissedVisit = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  AND
                                            scheduleevents.DisciplineTask IN ({0}) AND
                                            scheduleevents.Status IN ({1}) AND
                                            scheduleevents.Discipline IN ({2}) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.EventId)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }
       
        public List<ScheduleEvent> GetCurrentAndPerviousOrders(Guid agencyId, PatientEpisode episode, int[] disciplineTasks)
        {
           // var episodeScript = " SELECT Id FROM patientepisodes WHERE PatientId = @patientid AND ( Id = @episodeid OR (Id != @episodeid AND DATE(EndDate) = DATE(@previousenddate))  AND IsActive = 1  AND IsDischarged = 0 ORDER BY DATE(StartDate) DESC LIMIT 2 ) ";
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.PatientId = patientepisodes.PatientId
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND
                                            scheduleevents.IsMissedVisit = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND 
                                            ( ( patientepisodes.Id = @episodeid AND scheduleevents.IsOrderForNextEpisode = 0 ) OR (patientepisodes.Id != @episodeid AND  DATE(patientepisodes.EndDate) = DATE(@previousenddate) AND scheduleevents.IsOrderForNextEpisode = 1 )) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            scheduleevents.DisciplineTask IN ( {0} ) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC  ",  disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", episode.PatientId)
                .AddGuid("episodeid", episode.Id)
                .AddDateTime("previousenddate", episode.StartDate.AddDays(-1))
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;

        }

        public List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty; ;
            if (patientStatus <= 0)
            {
                status = " AND  patients.Status IN (1,2)";
            }
            else
            {
                status = " AND patients.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND patients.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND scheduleevents.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.MissedVisitStatus as MissedVisitStatus , 
                        scheduleevents.IsQANote as IsQANote ,
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        patientepisodes.Details as Details ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0  {2} AND
                                            scheduleevents.IsDeprecated = 0  {3} AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0}  {1}", schedulleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    MissedVisitStatus = reader.GetInt("MissedVisitStatus"),
                    IsQANote = reader.GetBoolean("IsQANote"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    EpisodeNotes = reader.GetStringNullable("Details"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }
        
        public List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty; ;
            if (patientStatus <= 0)
            {
                status = " AND  patients.Status IN (1,2)";
            }
            else
            {
                status = " AND patients.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND patients.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND scheduleevents.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName ,
                        patients.PatientIdNumber as PatientIdNumber
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0  {2} AND
                                            scheduleevents.IsDeprecated = 0  {3} AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0}  {1}", schedulleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
       
        public List<ScheduleEvent> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid ";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId ";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
       
        public List<ScheduleEvent> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2)";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplineTasks.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName 
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            patients.IsDeprecated = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.IsMissedVisit = 1 AND 
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
       
        public List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0  AND 
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) <> DATE(scheduleevents.VisitDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} 
                                            ORDER BY patients.LastName ASC , patients.FirstName ASC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<ScheduleEvent> GetPrintQueueEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;
           
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName
                                FROM 
                                    scheduleevents 
                                        INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                        INNER JOIN patients ON scheduleevents.PatientId = patients.Id  
                                                WHERE
                                                        scheduleevents.AgencyId = @agencyid {0} AND 
                                                        scheduleevents.IsMissedVisit = 0 AND
                                                        scheduleevents.IsDeprecated = 0 AND
                                                        scheduleevents.InPrintQueue = 1 AND
                                                        patients.IsDeprecated = 0  AND 
                                                        patients.Status IN (1,2) AND 
                                                        scheduleevents.IsDeprecated = 0  AND 
                                                        patientepisodes.IsActive = 1 AND 
                                                        patientepisodes.IsDischarged = 0  AND 
                                                        DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                                        DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY scheduleevents.EventDate DESC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }
        
        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks)
        {
            var additionalScript1 = string.Empty;

            if (notesDisciplineTasks != null && notesDisciplineTasks.Length > 0)
            {
                additionalScript1 += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", notesDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (notesScheduleStatus != null && notesScheduleStatus.Length > 0)
            {
                additionalScript1 += string.Format("  AND scheduleevents.Status IN ( {0} ) ", notesScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script1 = string.Format(@"SELECT 
                        scheduleevents.EventId as Id ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        patientvisitnotes.Note as Note ,
                        scheduleevents.Status as Status ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate ,
                        scheduleevents.DisciplineTask as DisciplineTask  
                                        FROM patientvisitnotes 
                                            INNER JOIN scheduleevents ON patientvisitnotes.Id = scheduleevents.EventId 
                                            INNER JOIN patientepisodes ON  patientvisitnotes.EpisodeId = patientepisodes.Id  AND patientvisitnotes.EpisodeId = scheduleevents.EpisodeId
                                                        WHERE 
                                                              patientepisodes.AgencyId = @agencyid AND 
                                                              scheduleevents.AgencyId = @agencyid AND 
                                                              scheduleevents.PatientId = @patientid AND 
                                                              scheduleevents.IsMissedVisit = 0 AND 
                                                              scheduleevents.IsDeprecated = 0 AND 
                                                              patientepisodes.IsActive = 1 AND 
                                                              patientepisodes.IsDischarged = 0 AND 
                                                              DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                                              DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript1);

            var additionalScript2 = string.Empty;

            if (oasisDisciplineTasks != null && oasisDisciplineTasks.Length > 0)
            {
                additionalScript2 += string.Format(" AND agencymanagement.scheduleevents.DisciplineTask IN ( {0} ) ", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (oasisScheduleStatus != null && oasisScheduleStatus.Length > 0)
            {
                additionalScript2 += string.Format("  AND agencymanagement.scheduleevents.Status IN ( {0} ) ", oasisScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script2 = string.Format(@"SELECT 
                        agencymanagement.scheduleevents.EventId as Id ,
                        agencymanagement.scheduleevents.PatientId as PatientId ,
                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                        agencymanagement.scheduleevents.UserId as UserId ,
                        oasisc.assessments.OasisData as Note ,
                        agencymanagement.scheduleevents.Status as Status ,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                        agencymanagement.patientepisodes.StartDate as StartDate ,
                        agencymanagement.patientepisodes.EndDate as EndDate ,
                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask
                                            FROM oasisc.assessments 
                                                                INNER JOIN  agencymanagement.scheduleevents  ON oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                                                INNER JOIN agencymanagement.patientepisodes ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.EpisodeId = agencymanagement.scheduleevents.EpisodeId 
                                                                        WHERE  
                                                                          agencymanagement.patientepisodes.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.PatientId = @patientid AND 
                                                                          agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                                                          agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                          agencymanagement.patientepisodes.IsActive = 1 AND 
                                                                          agencymanagement.patientepisodes.IsDischarged = 0 AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}", additionalScript2);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1}", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Note = reader.GetStringNullable("Note"),
                    Status = reader.GetInt("Status",0),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByEpisode(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks)
        {
            var additionalScript1 = string.Empty;

            if (notesDisciplineTasks != null && notesDisciplineTasks.Length > 0)
            {
                additionalScript1 += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", notesDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (notesScheduleStatus != null && notesScheduleStatus.Length > 0)
            {
                additionalScript1 += string.Format("  AND scheduleevents.Status IN ( {0} ) ", notesScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script1 = string.Format(@"SELECT 
                        scheduleevents.EventId as Id ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        patientvisitnotes.Note as Note ,
                        scheduleevents.Status as Status ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.DisciplineTask as DisciplineTask  
                                        FROM patientvisitnotes 
                                            INNER JOIN scheduleevents ON patientvisitnotes.Id = scheduleevents.EventId 
                                                        WHERE 
                                                              scheduleevents.AgencyId = @agencyid AND 
                                                              scheduleevents.PatientId = @patientid AND 
                                                              scheduleevents.EpisodeId = @episodeid AND 
                                                              scheduleevents.IsMissedVisit = 0 AND 
                                                              scheduleevents.IsDeprecated = 0 AND 
                                                              DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript1);

            var additionalScript2 = string.Empty;

            if (oasisDisciplineTasks != null && oasisDisciplineTasks.Length > 0)
            {
                additionalScript2 += string.Format(" AND agencymanagement.scheduleevents.DisciplineTask IN ( {0} ) ", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (oasisScheduleStatus != null && oasisScheduleStatus.Length > 0)
            {
                additionalScript2 += string.Format("  AND agencymanagement.scheduleevents.Status IN ( {0} ) ", oasisScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script2 = string.Format(@"SELECT 
                        agencymanagement.scheduleevents.EventId as Id ,
                        agencymanagement.scheduleevents.PatientId as PatientId ,
                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                        agencymanagement.scheduleevents.UserId as UserId ,
                        oasisc.assessments.OasisData as Note ,
                        agencymanagement.scheduleevents.Status as Status ,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask
                                            FROM oasisc.assessments 
                                                                INNER JOIN  agencymanagement.scheduleevents  ON oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                                                        WHERE  
                                                                          agencymanagement.scheduleevents.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.PatientId = @patientid AND 
                                                                          agencymanagement.scheduleevents.EpisodeId = @episodeid AND 
                                                                          agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                                                          agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}", additionalScript2);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1}", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Note = reader.GetStringNullable("Note"),
                    Status = reader.GetInt("Status",0),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list;
        }
        
        public List<ScheduleEvent> GetScheduleEventsVeryLeanOptionalPar(Guid agencyId, Guid branchIdOptional, Guid patientIdOptional, Guid episodeIdOptional, int[] scheduleStatus, int[] disciplineTasks, int patientStatus, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var branchAdditionalScript = string.Empty;
            if (!branchIdOptional.IsEmpty())
            {
                branchAdditionalScript = " AND patients.AgencyLocationId = @branchId ";
            }
            var patientAdditionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                patientAdditionalScript += " AND scheduleevents.PatientId = @patientId ";
            }
            else
            {
                patientAdditionalScript = string.Format(" AND patients.IsDeprecated = 0 {0} ", patientStatus <= 0 ? " AND patients.Status IN (1,2) " : " AND patients.Status = " + patientStatus);
            }
            var episodeAdditionalScript = string.Empty;
            if (!episodeIdOptional.IsEmpty())
            {
                episodeAdditionalScript = " AND scheduleevents.EpisodeId = @episodeid ";
            }
            else
            {
                episodeAdditionalScript = " AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0  ";
            }
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (IsDateRange)
            {
                scheduleAdditionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents 
                                    INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    INNER JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyId {0} {1} {2} AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {3} ", branchAdditionalScript, patientAdditionalScript, episodeAdditionalScript, scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchIdOptional)
                .AddGuid("patientId", patientIdOptional)
                .AddGuid("episodeid", episodeIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetScheduleEventsVeryLeanNonOptionalPar(Guid agencyId,  Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format("  AND Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND IsMissedVisit = 0 ";
            }
            if (IsDateRange)
            {
                scheduleAdditionalScript += " AND DATE(EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        DisciplineTask as DisciplineTask , 
                        Status as Status ,
                        Discipline as Discipline  
                            FROM 
                                scheduleevents 
                                        WHERE
                                            AgencyId = @agencyId  AND 
                                            PatientId = @patientId  AND
                                            EpisodeId = @episodeid  AND 
                                            IsDeprecated = 0  {0} ", scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    EventId = reader.GetGuid("EventId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var additionalScript = string.Empty;
            if (IsDateRange)
            {
                additionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) ASC  ";
            }
            else
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) DESC  ";
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} {1} ", additionalScript, limitScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline")

                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var additionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                additionalScript = " AND patients.Id = @patientid ";
            }
            else
            {
                additionalScript = " AND patients.Status IN (1,2) ";
            }
            if (!userIdOptional.IsEmpty())
            {
                additionalScript += " AND  scheduleevents.UserId = @userid ";
            }
            if (IsDateRange)
            {
                additionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) ASC  ";
            }
            else
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) DESC  ";
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline,
                        patients.PatientIdNumber as PatientIdNumber, 
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName  
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                    LEFT JOIN patients ON  patients.Id = scheduleevents.PatientId
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} {1} ", additionalScript, limitScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientIdOptional)
                 .AddGuid("userid", userIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded)
        {
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {

            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
            }
            else
            {
                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
                {
                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
                }
                else
                {
                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
                }
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.EpisodeId = @episodeid AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0 {0}  
                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate)
        {
            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.IsDeprecated as IsDeprecated , 
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        scheduleevents.InPrintQueue as InPrintQueue ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName 
                            FROM
                                scheduleevents 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.EpisodeId = @episodeid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 {0} 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", dateRange);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleByUserId(Guid agencyId, Guid userId, DateTime start, DateTime end, bool isMissedVist, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                                    scheduleevents.EventId as EventId ,
                                    scheduleevents.PatientId as PatientId ,
                                    scheduleevents.EpisodeId as EpisodeId ,
                                    scheduleevents.UserId as UserId ,
                                    scheduleevents.DisciplineTask as DisciplineTask , 
                                    scheduleevents.EventDate as EventDate ,
                                    scheduleevents.VisitDate as VisitDate ,
                                    scheduleevents.Status as Status ,
                                    scheduleevents.IsMissedVisit as IsMissedVisit , 
                                    scheduleevents.ReturnReason as ReturnReason ,
                                    scheduleevents.Comments as Comments ,
                                    patientepisodes.StartDate as StartDate ,
                                    patientepisodes.EndDate as EndDate ,
                                    patients.FirstName as FirstName , 
                                    patients.LastName as LastName ,
                                    patientepisodes.Details as EpisodeNotes 
                                        FROM 
                                            scheduleevents 
                                                LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                                LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                                    WHERE 
                                                        scheduleevents.AgencyId = @agencyid AND 
                                                        scheduleevents.UserId = @userid AND 
                                                        scheduleevents.IsDeprecated = 0 AND 
                                                        scheduleevents.IsMissedVisit = {0} AND
                                                        scheduleevents.DisciplineTask > 0 AND 
                                                        patientepisodes.IsActive = 1  AND 
                                                        patientepisodes.IsDischarged = 0 AND 
                                                        patients.IsDeprecated = 0 AND
                                                        patients.Status IN (1,2) AND
                                                        DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                                        DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} 
                                                            ORDER BY DATE(scheduleevents.EventDate) DESC ", isMissedVist, additionalFilter);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", start)
                .AddDateTime("enddate", end)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("EndDate"),
                    Status = reader.GetInt("Status", 0),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleByUserIdLean(Guid agencyId, Guid patientIdOptional, Guid userId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            var leftJoinScript = string.Empty;
            var patientScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                patientScript = " AND scheduleevents.PatientId = @patientid ";
            }
            else
            {
                leftJoinScript = " LEFT JOIN patients ON scheduleevents.PatientId = patients.Id  ";
                patientScript = " AND patients.Status IN (1,2) AND patients.IsDeprecated = 0 AND  patients.Status IN (1,2) ";
            }
            if (IsDateRange)
            {
                additionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                                    scheduleevents.EventId as EventId ,
                                    scheduleevents.PatientId as PatientId ,
                                    scheduleevents.EpisodeId as EpisodeId ,
                                    scheduleevents.UserId as UserId ,
                                    scheduleevents.DisciplineTask as DisciplineTask , 
                                    scheduleevents.EventDate as EventDate ,
                                    scheduleevents.VisitDate as VisitDate ,
                                    scheduleevents.Status as Status ,
                                    patientepisodes.StartDate as StartDate ,
                                    patientepisodes.EndDate as EndDate 
                                        FROM 
                                            scheduleevents 
                                                LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                                {0}
                                                    WHERE 
                                                        scheduleevents.AgencyId = @agencyid AND 
                                                        scheduleevents.UserId = @userid AND 
                                                        scheduleevents.IsDeprecated = 0 AND 
                                                        scheduleevents.DisciplineTask > 0 AND 
                                                        patientepisodes.IsActive = 1  AND 
                                                        patientepisodes.IsDischarged = 0  
                                                        {1} AND
                                                        DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)  {2} "
                , leftJoinScript,patientScript, additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                 .AddGuid("patientid", patientIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = userId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("EndDate"),
                    Status = reader.GetInt("Status", 0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                })
                .AsList();
            }
            return list;
        }

        public bool ToggleScheduledEventsDelete(Guid agencyId, Guid patientId, Guid episodeId, List<Guid> eventIds, bool isDeprecated)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduleevents set IsDeprecated = {0} WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId IN ( {1})", isDeprecated ? 1 : 0, eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid", episodeId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool ToggleScheduledEventsPaid(Guid agencyId, List<Guid> eventIds, bool isVisitPaid)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduleevents set IsVisitPaid = {0} WHERE AgencyId = @agencyid AND  EventId IN ( {1})", isVisitPaid ? 1 : 0, eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

       
        #endregion


        public List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName  
                            FROM
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.UserId = @userid AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                ORDER BY patients.LastName ASC , patients.FirstName DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new UserVisit
                {
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Status = reader.GetInt("Status",0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new UserVisit
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Status = reader.GetInt("Status",0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid";
            }

            if (!userId.IsEmpty())
            {
                additionalScript += " AND scheduleevents.UserId = @userid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        patientepisodes.Details as Details,
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName ,
                        patients.PatientIdNumber as PatientIdNumber 
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND
                                            patients.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .AddGuid("userid", userId)
                .SetMap(reader => new UserVisit
                {
                    Id = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    VisitRate = "$0.00"
                })
                .AsList();
            }
            return list;
        }

        public List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        COUNT(*) as VisitCount  
                            FROM 
                                scheduleevents 
                                    INNER JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    INNER JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.UserId != '{1}' AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND
                                            patients.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                GROUP BY scheduleevents.UserId ", additionalScript, Guid.Empty.ToString());

            var list = new List<VisitSummary>();
            using (var cmd = new FluentCommand<VisitSummary>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new VisitSummary
                {
                    UserId = reader.GetGuid("UserId"),
                    VisitCount = reader.GetInt("VisitCount")
                })
                .AsList();
            }
            return list;
        }


        #region Process Schedule

        public bool UpdateScheduleEventsForReassign(Guid agencyId, Guid userId, string ids)
        {
            bool result = false;
            if (ids.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduleevents set UserId = @userid WHERE AgencyId = @agencyid AND EventId IN ( {0})", ids);
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduleevents set IsBillable = 1 WHERE AgencyId = @agencyid AND PatientId = @patientid AND EventId IN ( {0})", scheduleEvents.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AsNonQuery();
                    }
                    
                    //scheduleEvents.ForEach(scheduleEvent =>
                    //{
                    //    var evnt = database.Single<ScheduleEvent>(e => e.AgencyId == agencyId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId && e.EventId == scheduleEvent.EventId);
                    //    if (evnt != null)
                    //    {
                    //        evnt.IsBillable = scheduleEvent.IsBillable;
                    //        database.Update<ScheduleEvent>(evnt);
                    //    }
                    //});
                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduleevents set IsBillable = 1 WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId IN ( {0})", scheduleEvents.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid",episodeId)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool IsFirstBillableVisit(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT count(*)  
                                                FROM 
                                                    scheduleevents
                                                        WHERE 
                                                            scheduleevents.AgencyId = @agencyid AND 
                                                            scheduleevents.PatientId = @patientid  AND 
                                                            scheduleevents.EpisodeId = @episodeid AND 
                                                            scheduleevents.IsDeprecated = 0  AND
                                                            scheduleevents.IsBillable = 1 AND 
                                                            scheduleevents.IsMissedVisit = 0 AND 
                                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                                            scheduleevents.Status IN ( {0} ) 
                                                                ORDER BY 
                                                                    DATE(scheduleevents.VisitDate) ASC LIMIT 1", status.Join(","));

            var count = 0;
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AsScalar();
            }
            return count > 0;
        }

        #endregion

        #region Missed Visit

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Add<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Update<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate ,
                        patients.PatientIdNumber as PatientIdNumber,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName
                                FROM 
                                    scheduleevents 
                                        INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                        INNER JOIN patients ON scheduleevents.PatientId = patients.Id  
                                                WHERE
                                                        scheduleevents.AgencyId = @agencyid {0} AND 
                                                        scheduleevents.IsMissedVisit = 1 AND
                                                        scheduleevents.IsDeprecated = 0 AND
                                                        patients.IsDeprecated = 0  AND 
                                                        patients.Status IN (1,2) AND 
                                                        scheduleevents.IsDeprecated = 0  AND 
                                                        patientepisodes.IsActive = 1 AND 
                                                        patientepisodes.IsDischarged = 0  AND 
                                                        DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                                        DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY scheduleevents.EventDate DESC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Status = reader.GetInt("Status", 0),
                    IsMissedVisit = true,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }


        #endregion

    }
}
