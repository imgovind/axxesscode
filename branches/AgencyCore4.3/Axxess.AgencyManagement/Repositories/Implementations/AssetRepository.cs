﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }

        public bool AddMany(List<Asset> assets)
        {
            bool result = false;
            try
            {
                if (assets != null && assets.Count>0)
                {

                    database.AddMany<Asset>(assets);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool Delete(Guid id)
        {
            var asset = database.Single<Asset>(a => a.Id == id);
            if (asset != null)
            {
                asset.IsDeprecated = true;
                asset.Modified = DateTime.Now;
                database.Update<Asset>(asset);
                return true;
            }
            return false;
        }

        public bool DeleteMany(Guid agencyId, List<Guid> assetIds)
        {
            bool result = false;
            if (assetIds != null && assetIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE assets set IsDeprecated = 1 WHERE AgencyId = @agencyid AND Id IN ( {0})", assetIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public Asset Get(Guid id, Guid agencyId)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId && a.IsDeprecated == false);
        }


        #endregion
    }
}
