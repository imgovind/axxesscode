﻿namespace Axxess.AgencyManagement.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Enums;

    public static class DisciplineTaskFactory
    {

        public static List<int> SkilledNurseSharedFile()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.SkilledNurseVisit);
            list.Add((int)DisciplineTasks.SNInsulinAM);
            list.Add((int)DisciplineTasks.SNInsulinPM);
            list.Add((int)DisciplineTasks.SNInsulinHS);
            list.Add((int)DisciplineTasks.SNInsulinNoon);
            list.Add((int)DisciplineTasks.FoleyCathChange);
            list.Add((int)DisciplineTasks.SNB12INJ);
            list.Add((int)DisciplineTasks.SNBMP);
            list.Add((int)DisciplineTasks.SNCBC);
            list.Add((int)DisciplineTasks.SNHaldolInj);
            list.Add((int)DisciplineTasks.PICCMidlinePlacement);
            list.Add((int)DisciplineTasks.PRNFoleyChange);
            list.Add((int)DisciplineTasks.PRNSNV);
            list.Add((int)DisciplineTasks.PRNVPforCMP);
            list.Add((int)DisciplineTasks.PTWithINR);
            list.Add((int)DisciplineTasks.PTWithINRPRNSNV);
            list.Add((int)DisciplineTasks.SkilledNurseHomeInfusionSD);
            list.Add((int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional);
            list.Add((int)DisciplineTasks.SNDC);
            list.Add((int)DisciplineTasks.SNEvaluation);
            list.Add((int)DisciplineTasks.SNFoleyLabs);
            list.Add((int)DisciplineTasks.SNFoleyChange);
            list.Add((int)DisciplineTasks.SNInjection);
            list.Add((int)DisciplineTasks.SNInjectionLabs);
            list.Add((int)DisciplineTasks.SNLabsSN);
            list.Add((int)DisciplineTasks.SNVwithAideSupervision);
            list.Add((int)DisciplineTasks.SNVDCPlanning);
            list.Add((int)DisciplineTasks.SNVTeachingTraining);
            list.Add((int)DisciplineTasks.SNVManagementAndEvaluation);
            list.Add((int)DisciplineTasks.SNVObservationAndAssessment);

            return list;
        }

        public static List<int> SkilledNurseAssessment()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.SNAssessment);
            list.Add((int)DisciplineTasks.SNAssessmentRecert);
            return list;
        }

        public static List<int> ALLSkilledNurseDisciplineTasks(bool isAssessmentIncluded)
        {
            var list = new List<int>();
            list.AddRange(SkilledNurseSharedFile());
            list.Add((int)DisciplineTasks.SNDiabeticDailyVisit);
            list.Add((int)DisciplineTasks.SNVPsychNurse);
            list.Add((int)DisciplineTasks.SNPsychAssessment);
            list.Add((int)DisciplineTasks.SNPediatricVisit);
            list.Add((int)DisciplineTasks.SNPediatricAssessment);
            list.Add((int)DisciplineTasks.SNDiabeticDailyVisit);
            if (isAssessmentIncluded)
            {
                list.AddRange(SkilledNurseAssessment());
            }
            return list;
        }

        public static List<int> PTNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTAVisit);
            list.Add((int)DisciplineTasks.PTVisit);
            return list;
        }

        public static List<int> PTEvalDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTEvaluation);
            list.Add((int)DisciplineTasks.PTReEvaluation);
            list.Add((int)DisciplineTasks.PTMaintenance);
            return list;
        }

        public static List<int> PTDischargeDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTDischarge);
            return list;
        }

        public static List<int> OTNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OTVisit);
            list.Add((int)DisciplineTasks.COTAVisit);
            return list;
        }

        public static List<int> OTEvalDisciplineTasks(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OTEvaluation);
            list.Add((int)DisciplineTasks.OTReEvaluation);
            if (isDischargeInclueded)
            {
                list.Add((int)DisciplineTasks.OTDischarge);
            }
            list.Add((int)DisciplineTasks.OTMaintenance);
            return list;
        }

        public static List<int> STNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.STVisit);
            return list;
        }

        public static List<int> STEvalDisciplineTasks(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.STEvaluation);
            list.Add((int)DisciplineTasks.STReEvaluation);
            if (isDischargeInclueded)
            {
                list.Add((int)DisciplineTasks.STDischarge);
            }
            list.Add((int)DisciplineTasks.STMaintenance);
            return list;
        }

        public static List<int> AllTherapy(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.AddRange(PTNoteDisciplineTasks());
            list.AddRange(PTEvalDisciplineTasks());
            list.Add((int)DisciplineTasks.PTReassessment);

            list.AddRange(OTNoteDisciplineTasks());
            list.AddRange(OTEvalDisciplineTasks(isDischargeInclueded));
            list.Add((int)DisciplineTasks.OTReassessment);

            list.AddRange(STNoteDisciplineTasks());
            list.AddRange(STEvalDisciplineTasks(isDischargeInclueded));

            return list;
        }

        public static List<int> MSWProgressNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.MSWProgressNote);
            return list;
        }

        public static List<DisciplineTasks> PhysicianEvalNotes()
        {
            var list = new List<DisciplineTasks>();
            list.Add(DisciplineTasks.PTEvaluation);
            list.Add(DisciplineTasks.PTReEvaluation);
            list.Add(DisciplineTasks.OTEvaluation);
            list.Add(DisciplineTasks.OTReEvaluation);
            list.Add(DisciplineTasks.STEvaluation);
            list.Add(DisciplineTasks.STReEvaluation);
            return list;
        }

        public static List<int> AllPhysicianOrders()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.FaceToFaceEncounter);
            list.AddRange(PhysicianOrdersWithOutFaceToFace());
            return list;
        }

        public static List<int> PhysicianOrdersWithOutFaceToFace()
        {
            var list = new List<int>();
            list.AddRange(POC());
            list.Add((int)DisciplineTasks.PhysicianOrder);
            return list;
        }

        public static List<int> POC()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.HCFA485StandAlone);
            list.Add((int)DisciplineTasks.HCFA485);
            list.Add((int)DisciplineTasks.NonOasisHCFA485);
            return list;
        }

        public static List<int> NONOASISSOCDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISStartofCare);
            return list;
        }

        public static List<int> SOCDisciplineTasks(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCStartofCare);
            list.Add((int)DisciplineTasks.OASISCStartofCarePT);
            list.Add((int)DisciplineTasks.OASISCStartofCareOT);
            if (IsNonOasisIncluded)
            {
                list.AddRange(NONOASISSOCDisciplineTasks());
            }
            return list;
        }

        public static List<int> NONOASISRecertDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISRecertification);
            return list;
        }

        public static List<int> RecertDisciplineTasks(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCRecertification);
            list.Add((int)DisciplineTasks.OASISCRecertificationPT);
            list.Add((int)DisciplineTasks.OASISCRecertificationOT);
            if (IsNonOasisIncluded)
            {
                list.AddRange(NONOASISRecertDisciplineTasks());
            }
            return list;
        }

        public static List<int> ROCDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCResumptionofCare);
            list.Add((int)DisciplineTasks.OASISCResumptionofCareOT);
            list.Add((int)DisciplineTasks.OASISCResumptionofCarePT);
            return list;
        }

        public static List<int> EpisodeAllAssessments(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(IsNonOasisIncluded));
            list.AddRange(RecertDisciplineTasks(IsNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        public static List<int> LastFiveDayAssessments(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(RecertDisciplineTasks(IsNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        public static List<int> SOCANDROCDisciplineTasks(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(IsNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        public static List<int> DischargeOASISDisciplineTasks(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCDischarge);
            list.Add((int)DisciplineTasks.OASISCDischargeOT);
            list.Add((int)DisciplineTasks.OASISCDischargePT);
            if (IsNonOasisIncluded)
            {
                list.AddRange(NONOASISDischargeDisciplineTasks());
            }
            return list;
        }

        public static List<int> NONOASISDischargeDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISDischarge);
            return list;
        }

        public static List<int> DeathOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCDeath);
            list.Add((int)DisciplineTasks.OASISCDeathOT);
            list.Add((int)DisciplineTasks.OASISCDeathPT);
            return list;
        }

        public static List<int> TransferDischargeOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCTransferDischarge);
            return list;
        }

        public static List<int> TransferNotDischargeOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCTransfer);
            list.Add((int)DisciplineTasks.OASISCTransferOT);
            list.Add((int)DisciplineTasks.OASISCTransferPT);
            return list;
        }

        public static List<int> AllDischargingOASISDisciplineTasks(bool IsNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(DischargeOASISDisciplineTasks(IsNonOasisIncluded));
            list.AddRange(DeathOASISDisciplineTasks());
            list.AddRange(TransferDischargeOASISDisciplineTasks());
            return list;
        }

        public static List<int> FllowUpDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCFollowUp);
            list.Add((int)DisciplineTasks.OASISCFollowupPT);
            list.Add((int)DisciplineTasks.OASISCFollowupOT);
            return list;
        }

        public static List<int> MSWDischargeANDNoteOrders()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.MSWEvaluationAssessment);
            list.Add((int)DisciplineTasks.PTDischarge);
            list.Add((int)DisciplineTasks.SixtyDaySummary);
            return list;
        }

        public static List<int> EvalOrders()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTEvaluation);
            list.Add((int)DisciplineTasks.OTEvaluation);
            list.Add((int)DisciplineTasks.STEvaluation);
            list.Add((int)DisciplineTasks.PTReEvaluation);
            list.Add((int)DisciplineTasks.OTReEvaluation);
            list.Add((int)DisciplineTasks.STReEvaluation);
            return list;
        }

        public static List<int> AllOrders()
        {
            var list = new List<int>();
            list.AddRange(AllPhysicianOrders());
            list.AddRange(EvalOrders());
            list.AddRange(MSWDischargeANDNoteOrders());
            return list;
        }

        public static List<int> AllNOnOASIS()
        {
            var list = new List<int>();
            list.AddRange(NONOASISDischargeDisciplineTasks());
            list.AddRange(NONOASISRecertDisciplineTasks());
            list.AddRange(NONOASISSOCDisciplineTasks());
            return list;
        }

        public static List<int> CMSOASISOnly()
        {

            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(false));
            list.AddRange(RecertDisciplineTasks(false));
            list.AddRange(ROCDisciplineTasks());
            list.AddRange(FllowUpDisciplineTasks());

            list.AddRange(DischargeOASISDisciplineTasks(false));
            list.AddRange(TransferDischargeOASISDisciplineTasks());
            list.AddRange(TransferNotDischargeOASISDisciplineTasks());

            list.AddRange(DeathOASISDisciplineTasks());

            return list;

        }

        public static List<int> AllUAP()
        {

            var list = new List<int>();
            list.Add((int)DisciplineTasks.UAPInsulinPrepAdminVisit);
            list.Add((int)DisciplineTasks.UAPWoundCareVisit);
            return list;

            return list;

        }



    }
}


