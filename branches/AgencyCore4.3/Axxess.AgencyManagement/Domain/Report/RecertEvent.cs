﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;

    public class RecertEvent
    {
        public Guid Id { get; set; }
        public string Task { get; set; }
        public int Status { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string AssignedTo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime TargetDate { get; set; }
        public DateTime EventDate { get; set; }
        public string Schedule { get; set; }
        public string TargetDateFormatted { get {return this.TargetDate.ToString("MM/dd/yyyy") ;}}
        public int DateDifference { get; set; }
        public string StatusName
        {
            get
            {
                var status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)this.Status : ScheduleStatus.NoStatus;
                if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.EventDate.Date <= DateTime.Now.Date && this.EventDate.Date > DateTime.MinValue)
                {
                    return ScheduleStatus.CommonNotStarted.GetDescription();
                }
                else
                {
                    return "Not Scheduled";
                }

            }
        }
    }
}
