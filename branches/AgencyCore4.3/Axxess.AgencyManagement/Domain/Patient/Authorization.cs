﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class Authorization : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AssetId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string Number1 { get; set; }
        public string Number2 { get; set; }
        public string Number3 { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Insurance { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }

        public string SNVisit { get; set; }
        public string SNVisitCountType { get; set; }
        public string PTVisit { get; set; }
        public string PTVisitCountType { get; set; }
        public string OTVisit { get; set; }
        public string OTVisitCountType { get; set; }
        public string STVisit { get; set; }
        public string STVisitCountType { get; set; }
        public string MSWVisit { get; set; }
        public string MSWVisitCountType { get; set; }
        public string HHAVisit { get; set; }
        public string HHAVisitCountType { get; set; }
        public string DieticianVisit { get; set; }
        public string DieticianVisitCountType { get; set; }
        public string RNVisit { get; set; }
        public string RNVisitCountType { get; set; }
        public string LVNVisit { get; set; }
        public string LVNVisitCountType { get; set; }

        public bool IsDeprecated { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string Branch { get; set; }

        [SubSonicIgnore]
        public string InsuranceName { get; set; }

        [SubSonicIgnore]
        public string Url { get {return string.Format("<a href=\"javaScript:void(0);\" onclick=\"Patient.LoadEditAuthorization('{0}','{1}');\" >Edit</a> | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteAuthorization('{0}','{1}');\" >Delete</a>", this.PatientId, this.Id);} }
        
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string StartDateFormatted { get { return this.StartDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public string EndDateFormatted { get { return this.EndDate.ToShortDateString(); } }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => this.Insurance.IsEqual("0"), "Insurance is required."));
            AddValidationRule(new Validation(() => this.EndDate.Date < this.StartDate.Date, "End date is must be greater than start date."));
            AddValidationRule(new Validation(() => this.Number1.IsNullOrEmpty(), "Authorization number 1 is required."));
        }

        #endregion

    }
}
