﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using System.Web.Script.Serialization;

    public class PatientVisitNote
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public Guid UserId { get; set; }
        public string Note { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PhysicianId { get; set; }
        public string NoteType { get; set; }
        public bool IsBillable { get; set; }
        public bool IsWoundCare { get; set; }
        public string WoundNote { get; set; }
        public bool IsSupplyExist { get; set; }
        public string Supply { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }

        public long OrderNumber { get; set; }
        public DateTime SentDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string PhysicianData { get; set; }
        public int Version { get; set; }

        #region Domain

        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string PhysicianName { get; set; }
        [SubSonicIgnore]
        public DateTime OrderDate { get; set; }


        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime StartDate { get; set; }
        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime EndDate { get; set; }


        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime EventDate { get; set; }

        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime VisitDate { get; set; }


        #endregion

    }
}
