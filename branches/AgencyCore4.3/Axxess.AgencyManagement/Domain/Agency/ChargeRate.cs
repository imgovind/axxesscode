﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.AgencyManagement.Enums;
   
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [XmlRoot()]
    public class ChargeRate : EntityBase
    {
        [XmlElement]
        [UIHint("DisciplineTask")]
        public int Id { get; set; }
        [XmlElement]
        public string RateDiscipline { get; set; }
        [XmlElement]
        public string PreferredDescription { get; set; }
        [XmlElement]
        public string RevenueCode { get; set; }
        [XmlElement]
        public string Code { get; set; }
        [XmlElement]
        public double ExpectedRate { get; set; }
        [XmlElement]
        public double Charge { get; set; }
        [XmlElement]
        public string Modifier { get; set; }
        [XmlElement]
        public string Modifier2 { get; set; }
        [XmlElement]
        public string Modifier3 { get; set; }
        [XmlElement]
        public string Modifier4 { get; set; }
        [XmlElement]
        public string ChargeType { get; set; }
        [XmlElement]
        public int Unit { get; set; }
        [XmlElement]
        public double MedicareHMORate { get; set; }
        [XmlElement]
        public bool IsTimeLimit { get; set; }
        [XmlElement]
        public int TimeLimitHour { get; set; }
        [XmlElement]
        public int TimeLimitMin { get; set; }

        [XmlElement]
        public string SecondDescription { get; set; }
        [XmlElement]
        public string SecondRevenueCode { get; set; }
        [XmlElement]
        public string SecondCode { get; set; }
        [XmlElement]
        public bool IsSecondChargeDifferent { get; set; }
        [XmlElement]
        public double SecondCharge { get; set; }
        [XmlElement]
        public double SecondExpectedRate { get; set; }
        [XmlElement]
        public string SecondModifier { get; set; }
        [XmlElement]
        public string SecondModifier2 { get; set; }
        [XmlElement]
        public string SecondModifier3 { get; set; }
        [XmlElement]
        public string SecondModifier4 { get; set; }
        [XmlElement]
        public string SecondChargeType { get; set; }
        [XmlElement]
        public int SecondUnit { get; set; }
        [XmlElement]
        public bool IsUnitPerALineItem { get; set; }


        [XmlIgnore]
        public int InsuranceId { get; set; }

        [XmlIgnore]
        public bool IsMedicareHMO { get; set; }

        [XmlIgnore]
        public Guid LocationId { get; set; }

        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                return Enum.IsDefined(typeof(DisciplineTasks), this.Id) ? ((DisciplineTasks)this.Id).GetDescription() : string.Empty;
            }
        }

        [XmlIgnore]
        public string TimeLimitFormat
        {
            get
            {
                if (this.TimeLimitHour > 0 && this.TimeLimitMin > 0)
                {
                    return string.Format("{0:00}:{1:00}", this.TimeLimitHour, this.TimeLimitMin);
                }
                else if (this.TimeLimitHour > 0 && this.TimeLimitMin <= 0)
                {
                    return string.Format("{0:00}:00", this.TimeLimitHour);
                }
                else if (this.TimeLimitHour == 0 && this.TimeLimitMin > 0)
                {
                    return string.Format("00:{0:00}", this.TimeLimitMin);
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        public string ChargeTypeName
        {
            get
            {
                if (this.ChargeType.IsNotNullOrEmpty() && this.ChargeType.IsInteger() && Enum.IsDefined(typeof(BillUnitType), this.ChargeType.ToInteger()))
                {
                    return ((BillUnitType)this.ChargeType.ToInteger()).GetDescription();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        public string Modifiers
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", Modifier, Modifier2, Modifier3, Modifier4);
            }
        }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PreferredDescription), "Preferred Description is required."));
            AddValidationRule(new Validation(() => this.Id <= 0, "Task is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ChargeType) || (this.ChargeType.IsNotNullOrEmpty() && !this.ChargeType.IsInteger()), "Unit type is required."));
            if (this.ChargeType.IsNotNullOrEmpty() && this.ChargeType != ((int)BillUnitType.PerVisit).ToString())
            {
                if (this.IsTimeLimit)
                {
                    AddValidationRule(new Validation(() => this.TimeLimitHour <= 0, "Time limit hour is required."));
                    AddValidationRule(new Validation(() => this.TimeLimitHour <= 0 || this.TimeLimitHour > 12, "Time limit hour is not in the right range."));
                    AddValidationRule(new Validation(() => this.TimeLimitHour < 0 || this.TimeLimitHour > 60, "Time limit min is not in the right range."));
                }
            }
            AddValidationRule(new Validation(() => this.Charge < 0, "Rate is required."));
            //if (this.Charge.IsNotNullOrEmpty())
            //{
            //    AddValidationRule(new Validation(() => !this.Charge.IsDouble(), "Rate is not in the right format."));
            //}
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Code), "HCPCS Code is required."));
            // AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Modifier), "Modifier is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.RevenueCode), "Revenue Code is required."));
            //if (this.IsTimeLimit)
            //{

            //}

        }

        #endregion

    }
}
