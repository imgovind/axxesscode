﻿

namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

   public class RapLeanNotUsed
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public double ProspectivePay { get; set; }


        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName.ToUpperCase(), " ", this.LastName.ToUpperCase());
            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Concat(this.EpisodeStartDate.ToString("MM/dd/yyyy"), " - ", this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }
    }
}
