﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class VisitSummary
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int VisitCount { get; set; }
    }
}
