﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;
    using System.Xml.Serialization;
    using Axxess.AgencyManagement.Common;
    using Axxess.AgencyManagement.Enums;
    using System.Web.Script.Serialization;

    public class UserVisit
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public Guid UserId { get; set; }
        public string EpisodeNotes { get; set; }
        public string StatusComment { get; set; }
        public string VisitNotes { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public int DisciplineTask { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime VisitDate { get; set; }
        public string VisitRate { get; set; }
        public string MileageRate { get; set; }
        public string Surcharge { get; set; }
      
        public int Status{ get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string UserDisplayName { get; set; }
        public bool IsMissedVisit { get; set; }
        public bool IsVisitPaid { get; set; }
        public string AssociatedMileage { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string VisitPayment { get; set; }
        public string TotalPayment { get; set; }
        public double Total { get; set; }

        [XmlIgnore]
        public int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn))
                {
                    if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO;
                    }
                    else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO + 12 * 60;
                    }
                    else
                    {
                        if (timeOut >= timeIn)
                        {
                            return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);

                        }
                    }
                }
                return 0;
            }
        }

        [XmlIgnore]
        public bool IsMissedVisitReady { get { return this.ScheduleDate.IsValid() && this.ScheduleDate.Date <= DateTime.Now.Date ? true : false; } }

        [XmlIgnore]
        public string TaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)this.DisciplineTask).GetDescription(); } else { return string.Empty; };
            }
        }
        //need rewrite
        [XmlIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    if (this.IsMissedVisit)
                    {
                        return "Missed Visit";
                    }
                    else
                    {
                        var status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)this.Status : ScheduleStatus.NoStatus;
                        if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.ScheduleDate.Date <= DateTime.Now.Date)
                        {
                            return ScheduleStatus.CommonNotStarted.GetDescription();
                        }
                        else
                        {
                            return status.GetDescription();
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        public string ScheduleDateFormatted { get { return ScheduleDate.Date > DateTime.MinValue ? ScheduleDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [XmlIgnore]
        public string VisitDateFormatted { get { return VisitDate.Date <= DateTime.MinValue || ScheduleStatusFactory.AllNoteNotYetStarted().Exists(s => s == this.Status) ? string.Empty : VisitDate.ToString("MM/dd/yyyy"); } }
        [XmlIgnore]
        [ScriptIgnore]
        public string Details { get; set; }

    }
}
