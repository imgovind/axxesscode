﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Web.Script.Serialization;
    using Axxess.AgencyManagement.Enums;

    public class UserVisitWidget
    {
        public Guid PatientId { get; set; }
        public string TaskName { get; set; }
        public string EventDate { get; set; }
        public bool IsDischarged { get; set; }
        public string PatientName { get; set; }

        [ScriptIgnore]
        public int Status { get; set; }
        [ScriptIgnore]
        public Guid EventId { get; set; }
        [ScriptIgnore]
        public Guid EpisodeId { get; set; }
        [ScriptIgnore]
        public int DisciplineTask { get; set; }
    }
}
