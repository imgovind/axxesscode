﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum ClaimTypeSubCategory
    {
        [Description("RAP")]
        RAP,
        [Description("Final")]
        Final,
        [Description("Managed Care")]
        ManagedCare
    }
}
