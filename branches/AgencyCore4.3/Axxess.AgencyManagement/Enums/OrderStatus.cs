﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum OrderStatus
    {
        [Description("To Be Sent")]
        ToBeSent = 1,
        [Description("Sent To Physician")]
        SentToPhysician = 2,
        [Description("Returned With Signature")]
        ReturnedWithSignature = 3,
        [Description("Returned and Unsignature")]
        ReturnedUnSignature = 4
    }
}
