﻿
namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum CpoCode
    {
        [Description("")]
        NoCode=0,
        [Description("Cpo")]
        G0181 = 1,
        [Description("Certification")]
        G0180 = 2,
        [Description("Recertification")]
        G0179 = 3
    }
}
