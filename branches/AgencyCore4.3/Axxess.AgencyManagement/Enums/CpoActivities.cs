﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum CpoActivities
    {
        [Description("Development of Care")]
        DevelopmentOfCare = 1,
        [Description("Revision to Care Plan")]
        RevisionToCarePlan = 2,
        [Description("Review of Patient Records")]
        ReviewOfPtRecords = 3,
        [Description("Lab Reviews")]
        LabReviews = 4,
        [Description("Diagnostic Test Reviews")]
        DiagTestReviews = 5,
        [Description("Communication with other Health Professionals")]
        Communicate = 6,
        [Description("Integration of New Info-Treatment Plan")]
        Integration = 7,
        [Description("Adjustment/Med Therapy")]
        Adjustment = 8,
        [Description("Initial Orders")]
        InitialOrders = 9,
        [Description("Discharge/Review of Summary")]
        DischargeSummary = 10,
        [Description("Review of labs/medical reports")]
        ReviewReports = 11,
        [Description("Medical Decision Making")]
        DecisionMaking = 12,
        [Description("Any Physician Coordination")]
        PhysicianCoordination = 13,
        [Description("Other (specify in Comments)")]
        Other = 14
    }
}
