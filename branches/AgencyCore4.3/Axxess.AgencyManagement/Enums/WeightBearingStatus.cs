﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum WeightBearingStatus 
    {
        [Description("FWB (full weight bearing)")]
        FWB = 1,
        [Description("WBAT (weight bearing as tolerated)")]
        WBAT = 2,
        [Description("PWB (partial weight bearing)")]
        PWB = 3,
        [Description("TTWB (toe touch weight bearing)")]
        TTWB = 4,
        [Description("TDWB (touch down weight bearing)")]
        TDWB = 5,
        [Description("NWB (non-weight bearing)")]
        NWB = 6
    }
}
