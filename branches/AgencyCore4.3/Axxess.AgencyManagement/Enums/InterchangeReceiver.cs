﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Enums
{
    public enum InterchangeReceiver
    {
        [Description("Mutually Defined (ZZ)")]
        IR_ZZ = 1,
        [Description("Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)")]
        IR_27 = 2,
        [Description("Duns (Dun &amp; Bradstreet) (01)")]
        IR_01 = 3,
        [Description("Duns Plus Suffix (14)")]
        IR_14 = 4,
        [Description("Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)")]
        IR_28 = 5,
        [Description("Health Industry Number (HIN) (20)")]
        IR_20 = 6,
        [Description("Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)")]
        IR_29 = 7,
        [Description("Association of Insurance Commisioners Company Code (NAIC) (33)")]
        IR_33 = 8,
        [Description("U.S. Federal Tax Identification Number (30)")]
        IR_30 = 9
    }
}
