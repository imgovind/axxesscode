﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.OasisC.Enums;
    using Axxess.AgencyManagement.Common;

    public static class ScheduleEventExtensions
    {
        public static bool IsAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.GetCustomCategory().IsEqual("OASIS")).ToList();

                if (oasis != null && oasis.Exists(d => (int)d == scheduleEvent.DisciplineTask))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsStartofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOASISStartofCare
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT 
                    || scheduleEvent.DisciplineTask == (int) DisciplineTasks.OASISCStartofCareOT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISFollowUp(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCFollowUp
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCFollowupOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCFollowupPT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISDischarge(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDischarge)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISTransfer(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCTransfer
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISTransferDischarge(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischargePT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISSOC(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISSOCExcludingNON(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOASISDOD(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDeath
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsNONOASISSOC(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOASISStartofCare)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsNONOASISDischarge(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSkilledNurseNote(this ScheduleEvent scheduleEvent)
        {
            var result = false;

            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && Enum.IsDefined(typeof(DisciplineTasks),scheduleEvent.DisciplineTask))
            {
                if (IsSkilledNurseType((DisciplineTasks)scheduleEvent.DisciplineTask))
                {
                    result = true;
                }
            }

            return result;
        }

        private static bool IsSkilledNurseType(DisciplineTasks disciplineTasks)
        {
            var result = false;

            switch (disciplineTasks)
            {
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.SNInsulinHS:
                case DisciplineTasks.SNInsulinNoon:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNPsychAssessment:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static bool IsSkilledCare(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.Discipline == Disciplines.Nursing.ToString()
                    || scheduleEvent.Discipline == Disciplines.PT.ToString()
                    || scheduleEvent.Discipline == Disciplines.OT.ToString()
                    || scheduleEvent.Discipline == Disciplines.ST.ToString()
                    )
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTAVisit
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTVisit)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPediatricVisit(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.SNPediatricVisit)
                    return true;
            }
            return false;
        }

        public static bool IsPTEval(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTMaintenance)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPTDischarge(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTVisit
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.COTAVisit)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOTEval(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTDischarge
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTMaintenance)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.STVisit)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSTReassessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReassessment)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSTEval(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STDischarge
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STMaintenance)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsMSWProgressNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWProgressNote)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsMSW(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.Discipline == Disciplines.MSW.ToString())
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPlanofCare(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.DisciplineTask == (int) DisciplineTasks.HCFA485
                    || scheduleEvent.DisciplineTask == (int) DisciplineTasks.HCFA485StandAlone
                    || scheduleEvent.DisciplineTask == (int) DisciplineTasks.NonOasisHCFA485
                    || scheduleEvent.DisciplineTask == (int) DisciplineTasks.HHAideCarePlan
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PASCarePlan)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsHhaNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.Discipline == Disciplines.HHA.ToString())
                {
                    return true;
                }
            }
            return false;
        }

        public static AssessmentType GetAssessmentType(this ScheduleEvent scheduleEvent)
        {
            AssessmentType type = AssessmentType.None;
            if (scheduleEvent.IsOASISSOCExcludingNON())
                type = AssessmentType.StartOfCare;
            else if(scheduleEvent.IsOASISROC())
                type = AssessmentType.ResumptionOfCare;
            else if(scheduleEvent.IsOASISRecert())
                type = AssessmentType.Recertification;
            else if(scheduleEvent.IsOASISFollowUp())
                type = AssessmentType.FollowUp;
            else if(scheduleEvent.IsOASISTransfer())
                type = AssessmentType.TransferInPatientNotDischarged;
            else if(scheduleEvent.IsOASISTransferDischarge())
                type = AssessmentType.TransferInPatientDischarged;
            else if(scheduleEvent.IsOASISDOD())
                type = AssessmentType.DischargeFromAgencyDeath;
            else if(scheduleEvent.IsOASISDischarge())
                type = AssessmentType.DischargeFromAgency;
            else if(scheduleEvent.IsNONOASISSOC())
                type = AssessmentType.NonOasisStartOfCare;
            else if(scheduleEvent.IsNONOASISRecert())
                type = AssessmentType.NonOasisRecertification;
            else if(scheduleEvent.IsNONOASISDischarge())
                type = AssessmentType.NonOasisDischarge;

            return type;
        }

        public static bool IsRecertificationAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("recertification") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsOASISRecert(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsNONOASISRecert(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsResumptionofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("resumptionofcare") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public static bool IsOASISROC(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT)
                {
                    return true;
                }
            }
            return false;
        }

        //public static bool IsInLastFiveEpisodeDays(this ScheduleEvent scheduleEvent)
        //{
        //    if (scheduleEvent != null && scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsDate())
        //    {
        //        var eventDate = scheduleEvent.EventDate.ToDateTime();
        //        if (eventDate.Date >= scheduleEvent.EndDate.AddDays(-5).Date && eventDate.Date <= scheduleEvent.EndDate.Date)
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}

        //public static bool IsOasisOpen(this ScheduleEvent scheduleEvent)
        //{
        //    if (scheduleEvent != null && scheduleEvent.Status>0)
        //    {
        //        ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
        //        if (status == ScheduleStatus.OasisNotStarted || status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.OasisReopened || status == ScheduleStatus.OasisSaved || status == ScheduleStatus.OasisReturnedForClinicianReview)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool IsOrderAndStatus(this ScheduleEvent scheduleEvent, int statusId)
        //{
        //    if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty() && scheduleEvent.Status.IsNotNullOrEmpty())
        //    {
        //        Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), scheduleEvent.Discipline);
        //        ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
        //        if (discipline == Disciplines.Orders && statusId == (int)status)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool IsCompleteRecertAssessment(this ScheduleEvent scheduleEvent)
        //{
        //    if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
        //    {
        //        var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //        ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);

        //        if (disciplineTask.ToLowerInvariant().Contains("recert") && (status == ScheduleStatus.OasisCompletedPendingReview || status == ScheduleStatus.OasisCompletedExportReady))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool ContainsRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        //{
        //    var result = false;
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        scheduleEvents.ForEach(scheduleEvent =>
        //        {
        //            var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //            if (disciplineTask.IsNotNullOrEmpty() && disciplineTask.ToLowerInvariant().Contains("recert"))
        //            {
        //                result = true;
        //                return;
        //            }
        //        });
        //    }
        //    return result;
        //}


        //public static bool IsCompleteDischargeAssessment(this ScheduleEvent scheduleEvent)
        //{
        //    if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
        //    {
        //        var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //        ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);

        //        if (disciplineTask.ToLowerInvariant().Contains("discharge") && (status == ScheduleStatus.OasisCompletedPendingReview || status == ScheduleStatus.OasisCompletedExportReady || status==ScheduleStatus.OasisCompletedNotExported || status==ScheduleStatus.OasisExported))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool ContainsAnyDischargeAssessment(this List<ScheduleEvent> scheduleEvents)
        //{
        //    var result = false;
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        scheduleEvents.ForEach(scheduleEvent =>
        //        {
        //            var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //            if (disciplineTask.IsNotNullOrEmpty() && disciplineTask.ToLowerInvariant().Contains("discharge"))
        //            {
        //                    result = true;
        //                return;
        //            }
        //        });
        //    }
        //    return result;
        //}

        //public static bool ContainsAnyCompletedDischargeAssessment(this List<ScheduleEvent> scheduleEvents)
        //{
        //    var result = false;
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        scheduleEvents.ForEach(scheduleEvent =>
        //        {
        //            var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //            if (disciplineTask.IsNotNullOrEmpty() && disciplineTask.ToLowerInvariant().Contains("discharge"))
        //            {
        //                    result = IsCompleteDischargeAssessment(scheduleEvent);
        //                return;
        //            }
        //        });
        //    }
        //    return result;
        //}

        //private static bool ContainsAnyDischargeAssessmentAndOrIsCompleted(List<ScheduleEvent> scheduleEvents, bool checkIfAssessmentCompleted)
        //{
        //    var result = false;
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        scheduleEvents.ForEach(scheduleEvent =>
        //        {
        //            var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //            if (disciplineTask.IsNotNullOrEmpty() && disciplineTask.ToLowerInvariant().Contains("discharge"))
        //            {
        //                if (checkIfAssessmentCompleted)
        //                {
        //                    result = IsCompleteDischargeAssessment(scheduleEvent);
        //                }
        //                else 
        //                {
        //                    result = true;
        //                }
        //                return; 
        //            }
        //        });
        //    }
        //    return result;
        //}

        //public static ScheduleEvent GetRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        //{
        //    ScheduleEvent recert = null;
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        scheduleEvents.ForEach(scheduleEvent =>
        //        {
        //            var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
        //            if (disciplineTask.ToLowerInvariant().Contains("recert"))
        //            {
        //                recert = scheduleEvent;
        //                return;
        //            }
        //        });
        //    }
        //    return recert;
        //}

        public static bool IsCompleted(this ScheduleEvent scheduleEvent)
        {
            return ScheduleStatusFactory.OnAndAfterQAStatus(true).Contains(scheduleEvent.Status);
            //if (scheduleEvent != null && scheduleEvent.Status > 0)
            //{
            //    ScheduleStatus status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status) ? (ScheduleStatus)scheduleEvent.Status : ScheduleStatus.NoStatus;

            //    if (
            //           status == ScheduleStatus.OrderSubmittedPendingReview
            //        || status == ScheduleStatus.OrderSentToPhysician
            //        || status == ScheduleStatus.OrderToBeSentToPhysician
            //        || status == ScheduleStatus.OrderReturnedWPhysicianSignature
            //        || status == ScheduleStatus.OrderSentToPhysicianElectronically

            //        || status == ScheduleStatus.EvalSentToPhysician
            //        || status == ScheduleStatus.EvalToBeSentToPhysician
            //        || status == ScheduleStatus.EvalReturnedWPhysicianSignature
            //        || status == ScheduleStatus.EvalSentToPhysicianElectronically

            //        || status == ScheduleStatus.NoteCompleted

            //        || status == ScheduleStatus.NoteMissedVisit
            //        || status == ScheduleStatus.NoteMissedVisitComplete
            //        || status == ScheduleStatus.NoteSubmittedWithSignature

            //        || status == ScheduleStatus.OasisCompletedPendingReview
            //        || status == ScheduleStatus.OasisCompletedExportReady
            //        || status == ScheduleStatus.OasisExported
            //        || status == ScheduleStatus.OasisCompletedNotExported

            //        || status == ScheduleStatus.ReportAndNotesSubmittedWithSignature
            //        || status == ScheduleStatus.ReportAndNotesCompleted)
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        public static bool IsCompletelyFinished(this ScheduleEvent scheduleEvent)
        {
            return ScheduleStatusFactory.OnAndAfterQAStatus(false).Contains(scheduleEvent.Status);
            //if (scheduleEvent != null && scheduleEvent.Status>0)
            //{
            //    ScheduleStatus status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status) ? (ScheduleStatus)scheduleEvent.Status : ScheduleStatus.NoStatus;

            //    if (status == ScheduleStatus.NoteCompleted

            //        || status == ScheduleStatus.OrderSentToPhysician
            //        || status == ScheduleStatus.OrderToBeSentToPhysician
            //        || status == ScheduleStatus.OrderReturnedWPhysicianSignature
            //        || status == ScheduleStatus.OrderSentToPhysicianElectronically

            //        || status == ScheduleStatus.EvalSentToPhysician
            //        || status == ScheduleStatus.EvalToBeSentToPhysician
            //        || status == ScheduleStatus.EvalReturnedWPhysicianSignature
            //        || status == ScheduleStatus.EvalSentToPhysicianElectronically

            //        || status == ScheduleStatus.OasisCompletedExportReady
            //        || status == ScheduleStatus.OasisExported
            //        || status == ScheduleStatus.OasisCompletedNotExported

            //        || status == ScheduleStatus.NoteMissedVisitComplete)
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }



        //public static bool IsCompleted(this UserEvent userEvent)
        //{
        //    if (userEvent != null && userEvent.Status.IsNotNullOrEmpty())
        //    {
        //        ScheduleStatus status = userEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(userEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), userEvent.Status) : ScheduleStatus.NoStatus;

        //        if (status == ScheduleStatus.OrderReturnedWPhysicianSignature
        //            || status == ScheduleStatus.OrderSentToPhysician
        //            || status == ScheduleStatus.NoteCompleted
        //            || status == ScheduleStatus.NoteMissedVisit
        //            || status == ScheduleStatus.NoteSubmittedWithSignature
        //            || status == ScheduleStatus.OasisCompletedExportReady
        //            || status == ScheduleStatus.OasisCompletedPendingReview
        //            || status == ScheduleStatus.OasisExported
        //            || status == ScheduleStatus.ReportAndNotesCompleted
        //            || status == ScheduleStatus.ReportAndNotesSubmittedWithSignature
        //            || status == ScheduleStatus.OasisCompletedNotExported)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public static string[] Discipline(this List<ScheduleEvent> scheduleEvent)
        {
            var disciplines = new List<string>();
            if (scheduleEvent != null && scheduleEvent.Count > 0)
            {
                if (scheduleEvent.Exists(s => s.Discipline == "Nursing"))
                {
                    disciplines.Add("SN");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "PT"))
                {
                    disciplines.Add("PT");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "OT"))
                {
                    disciplines.Add("OT");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "ST"))
                {
                    disciplines.Add("ST");
                }

                if (scheduleEvent.Exists(s => s.Discipline == "HHA"))
                {
                    disciplines.Add("HHA");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "MSW"))
                {
                    disciplines.Add("MSW");
                }
            }
            return disciplines.ToArray();
        }

        //public static UserEvent ToUserEvent(this ScheduleEvent scheduleEvent)
        //{
        //    return new UserEvent
        //    {
        //        Status = scheduleEvent.Status,
        //        EventId = scheduleEvent.EventId,
        //        EpisodeId = scheduleEvent.EpisodeId,
        //        PatientId = scheduleEvent.PatientId,
        //        EventDate = scheduleEvent.EventDate,
        //        Discipline = scheduleEvent.Discipline,
        //        DisciplineTask = scheduleEvent.DisciplineTask,
        //        TimeIn = scheduleEvent.TimeIn,
        //        TimeOut = scheduleEvent.TimeOut,
        //        UserId = scheduleEvent.UserId,
        //        IsMissedVisit = scheduleEvent.IsMissedVisit,
        //        ReturnReason = scheduleEvent.ReturnReason
        //    };
        //}

        public static string GIdentify(this ScheduleEvent schedule)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
                switch (type)
                {
                    case "OASISCDeath":
                    case "OASISCDischarge":
                    case "NonOASISDischarge":
                    case "OASISCFollowUp":
                    case "OASISCRecertification":
                    case "NonOASISRecertification":
                    case "OASISCResumptionofCare":
                    case "OASISCStartofCare":
                    case "NonOASISStartofCare":
                    case "OASISCTransfer":
                    case "OASISCTransferDischarge":
                    case "SNAssessment":
                    case "SNAssessmentRecert":
                    case "SkilledNurseVisit":
                    case "SNInsulinAM":
                    case "SNInsulinPM":
                    case "SNInsulinHS":
                    case "SNInsulinNoon":
                    case "FoleyCathChange":
                    case "SNB12INJ":
                    case "SNBMP":
                    case "SNCBC":
                    case "SNHaldolInj":
                    case "PICCMidlinePlacement":
                    case "PRNFoleyChange":
                    case "PRNSNV":
                    case "PRNVPforCMP":
                    case "PTWithINR":
                    case "PTWithINRPRNSNV":
                    case "SkilledNurseHomeInfusionSD":
                    case "SkilledNurseHomeInfusionSDAdditional":
                    case "SNDC":
                    case "SNEvaluation":
                    case "SNFoleyLabs":
                    case "SNFoleyChange":
                    case "SNInjection":
                    case "SNInjectionLabs":
                    case "SNLabsSN":
                    case "SNVPsychNurse":
                    case "SNVwithAideSupervision":
                    case "SNVDCPlanning":
                    case "LVNSupervisoryVisit":
                    case "DieticianVisit":
                    case "DischargeSummary":
                    case "SixtyDaySummary":
                    case "TransferSummary":
                    case "CoordinationOfCare":
                    case "SNDiabeticDailyVisit":
                    case "SNPediatricVisit":
                    case "SNPsychAssessment":
                        return "SN";

                    case "SNVTeachingTraining":
                        return "SNT";

                    case "SNVManagementAndEvaluation":
                        return "SNM";

                    case "SNVObservationAndAssessment":
                        return "SNO";

                    case "OASISCStartofCarePT":
                    case "OASISCResumptionofCarePT":
                    case "OASISCRecertificationPT":
                    case "OASISCTransferDischargePT":
                    case "OASISCFollowupPT":
                    case "OASISCTransferPT":
                    case "OASISCDischargePT":
                    case "OASISCDeathPT":
                    case "PTEvaluation":
                    case "PTVisit":
                    case "PTDischarge":
                    case "PTReEvaluation":
                    case "PTSupervisoryVisit":
                        return "PT";

                    case "PTAVisit":
                        return "PTA";

                    case "PTMaintenance":
                        return "PTM";

                    case "OASISCStartofCareOT":
                    case "OASISCResumptionofCareOT":
                    case "OASISCRecertificationOT":
                    case "OASISCFollowupOT":
                    case "OASISCTransferOT":
                    case "OASISCDischargeOT":
                    case "OASISCDeathOT":
                    case "OTEvaluation":
                    case "OTReEvaluation":
                    case "OTDischarge":
                    case "OTVisit":
                    case "OTSupervisoryVisit":
                        return "OT";

                    case "OTMaintenance":
                        return "OTM";

                    case "STVisit":
                    case "STEvaluation":
                    case "STReEvaluation":
                    case "STDischarge":
                        return "ST";

                    case "STMaintenance":
                        return "STM";

                    case "MSWEvaluationAssessment":
                    case "MSWVisit":
                    case "MSWDischarge":
                    case "MSWAssessment":
                    case "MSWProgressNote":
                        return "MSW";

                    case "HHAideSupervisoryVisit":
                    case "HHAideVisit":
                    case "HHAideCarePlan":
                    case "HomeMakerNote":
                        return "HHA";

                    case "COTAVisit":
                        return "OTA";

                    case "PhysicianOrder":
                    case "HCFA485":
                    case "NonOasisHCFA485":
                    case "FaceToFaceEncounter":
                    case "IncidentAccidentReport":
                    case "InfectionReport":
                    case "CommunicationNote":
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        public static string TypeOfEvent(this ScheduleEvent schedule)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
                switch (type)
                {
                    case "OASISCDeath":
                    case "OASISCDeathOT":
                    case "OASISCDeathPT":
                    case "OASISCDischarge":
                    case "OASISCDischargeOT":
                    case "OASISCDischargePT":
                    case "NonOASISDischarge":
                    case "OASISCFollowUp":
                    case "OASISCFollowupPT":
                    case "OASISCFollowupOT":
                    case "OASISCRecertification":
                    case "OASISCRecertificationPT":
                    case "OASISCRecertificationOT":
                    case "NonOASISRecertification":
                    case "OASISCResumptionofCare":
                    case "OASISCResumptionofCarePT":
                    case "OASISCResumptionofCareOT":
                    case "OASISCStartofCare":
                    case "OASISCStartofCarePT":
                    case "OASISCStartofCareOT":
                    case "NonOASISStartofCare":
                    case "OASISCTransfer":
                    case "OASISCTransferPT":
                    case "OASISCTransferOT":
                    case "OASISCTransferDischarge":
                    case "OASISCTransferDischargePT":
                    case "SNAssessment":
                    case "SNAssessmentRecert":
                        return "OASIS";
                    case "SkilledNurseVisit":
                    case "Labs":
                    case "InitialSummaryOfCare":
                    case "SNInsulinAM":
                    case "SNInsulinPM":
                    case "SNInsulinHS":
                    case "SNInsulinNoon":
                    case "FoleyCathChange":
                    case "SNB12INJ":
                    case "SNBMP":
                    case "SNCBC":
                    case "SNHaldolInj":
                    case "PICCMidlinePlacement":
                    case "PRNFoleyChange":
                    case "PRNSNV":
                    case "PRNVPforCMP":
                    case "PTWithINR":
                    case "PTWithINRPRNSNV":
                    case "SkilledNurseHomeInfusionSD":
                    case "SkilledNurseHomeInfusionSDAdditional":
                    case "SNDC":
                    case "SNEvaluation":
                    case "SNFoleyLabs":
                    case "SNFoleyChange":
                    case "SNInjection":
                    case "SNInjectionLabs":
                    case "SNLabsSN":
                    case "SNVPsychNurse":
                    case "SNVwithAideSupervision":
                    case "SNVDCPlanning":
                    case "LVNSupervisoryVisit":
                    case "DieticianVisit":
                    case "DischargeSummary":
                    case "SixtyDaySummary":
                    case "TransferSummary":
                    case "SNVTeachingTraining":
                    case "SNVManagementAndEvaluation":
                    case "SNVObservationAndAssessment":
                    case "PTEvaluation":
                    case "PTVisit":
                    case "PTDischarge":
                    case "PTReEvaluation":
                    case "PTReassessment":
                    case "PTAVisit":
                    case "PTMaintenance":
                    case "OTEvaluation":
                    case "OTReEvaluation":
                    case "OTReassessment":
                    case "OTDischarge":
                    case "OTVisit":
                    case "OTMaintenance":
                    case "STVisit":
                    case "STEvaluation":
                    case "STReEvaluation":
                    case "STReassessment":
                    case "STDischarge":
                    case "STMaintenance":
                    case "MSWEvaluationAssessment":
                    case "MSWVisit":
                    case "MSWDischarge":
                    case "MSWAssessment":
                    case "MSWProgressNote":
                    case "HHAideSupervisoryVisit":
                    case "HHAideVisit":
                    case "HomeMakerNote":
                    case "HHAideCarePlan":
                    case "COTAVisit":
                    case "PASVisit":
                    case "PASTravel":
                    case "PASCarePlan":
                    case "SNDiabeticDailyVisit":
                    case "PTDischargeSummary":
                    case "OTDischargeSummary":
                    case "UAPWoundCareVisit":
                    case "UAPInsulinPrepAdminVisit":
                    case "CoordinationOfCare":
                    case "SNPediatricVisit":
                    case "SNPediatricAssessment":
                    case "PTSupervisoryVisit":
                    case "OTSupervisoryVisit":
                    case "DriverOrTransportationNote":
                    case "SNPsychAssessment":
                        return "Notes";
                    case "PhysicianOrder":
                        return "PhysicianOrder";
                    case "HCFA485":
                    case "NonOasisHCFA485":
                    case "HCFA485StandAlone" :
                        return "PlanOfCare";
                    case "FaceToFaceEncounter":
                        return "FaceToFaceEncounter";
                    case "IncidentAccidentReport":
                        return "IncidentAccident";
                    case "InfectionReport":
                        return "Infection";
                    case "CommunicationNote":
                        return "CommunicationNote";
                }
            }
            return string.Empty;
        }
    }
}
