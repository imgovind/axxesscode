﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Enums
{
    public enum CacheType
    {
        Agency = 1,
        Branch = 2,
        User = 3,
        Patient = 4,
        Physician=5,
    }
}
