﻿namespace Axxess.Core.Infrastructure
{
    using System;
    public class JsonViewData
    {
        public string url { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }

        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }

        public int PatientStatus { get; set; }

        public bool IsPatientListRefresh { get; set; }
        public bool IsDeletedPatientListRefresh { get; set; }
        public bool IsPendingPatientListRefresh { get; set; }
        public bool IsNonAdmitPatientListRefresh { get; set; }
        public bool IsReferralListRefresh { get; set; }

        public bool IsCenterRefresh { get; set; }
        public bool IsDataCentersRefresh { get; set; }
        public bool IsActivityRefresh { get; set; }
        public bool IsCaseManagementRefresh { get; set; }
        public bool IsMyScheduleTaskRefresh { get; set; }
        
        public bool IsExportOASISRefresh { get; set; }
        public bool IsExportedOASISRefresh { get; set; }
        public bool IsNotExportedOASISRefresh { get; set; }

        public bool IsOrdersToBeSentRefresh { get; set; }
        public bool IsOrdersHistoryRefresh { get; set; }
        public bool IsOrdersPendingRefresh { get; set; }

        public bool IsPhysicianOrderPOCRefresh { get; set; }
       
        public bool IsFaceToFaceEncounterRefresh { get; set; }

        public bool IsCommunicationNoteRefresh { get; set; }
       
        public bool IsIncidentAccidentRefresh { get; set; }
       
        public bool IsInfectionRefresh { get; set; }
       
      
    }
}
