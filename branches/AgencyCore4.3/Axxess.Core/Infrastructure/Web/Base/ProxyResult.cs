﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web.Mvc;

    using Axxess.Core.Extension;

    public class ProxyResult : ActionResult
    {
        private Uri targetUri { get; set; }
        public ProxyResult(string targetUrl)
        {
            this.targetUri = new Uri(targetUrl);
        }

        public ProxyResult(string baseUrl, string queryString)
        {
            this.targetUri = new Uri(baseUrl + "?" + queryString);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context.HttpContext.Request.UrlReferrer != null && context.HttpContext.Request.UrlReferrer.Host.ToLower().Contains("axxessweb.com"))
            {
                using (WebClient client = new WebClient())
                {
                    var buffer = client.DownloadData(targetUri);
                    if (buffer.Length > 0)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            memoryStream.Write(buffer, 0, buffer.Length);
                            memoryStream.Position = 0;
                            memoryStream.CopyTo(context.HttpContext.Response.OutputStream);
                        }
                    }
                }
            }
        }
    }
}
