﻿namespace Axxess.Core.Infrastructure
{
    public interface INotification
    {
        void Send(string fromAddress, string toAddress, string subject, string body);
        void SendAsync(string fromAddress, string toAddress, string subject, string body);
        void Send(string fromAddress, string toAddress, string ccAddress, string subject, string body);
    }
}
