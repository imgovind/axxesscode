﻿namespace Axxess.Core.Infrastructure
{
    using System.Configuration;
    using System.Collections.Generic;

    using MySql.Data.MySqlClient;

    public static class DatabaseConnectionFactory
    {
        private static Dictionary<string, DatabaseConnection> connections = new Dictionary<string, DatabaseConnection>();

        public static MySqlConnection CreateDefaultConnection(string connectionString)
        {
            var connection = new MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public static MySqlConnection GetConnection(string connectionStringName)
        {
            Check.Argument.IsNotEmpty(connectionStringName, "connectionStringName");

            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            if (connections != null)
            {
                if (connections.ContainsKey(connectionStringName))
                {
                    return connections[connectionStringName].Connection;
                }
                else
                {
                    var databaseConnection = new DatabaseConnection(connectionString);
                    connections.Add(connectionStringName, databaseConnection);
                    return databaseConnection.Connection;
                }
            }
            return CreateDefaultConnection(connectionString);
        }
    }
}
