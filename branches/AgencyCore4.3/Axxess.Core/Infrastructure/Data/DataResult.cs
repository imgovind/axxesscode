﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Collections.Generic;

    using Extension;

    public sealed class DataResult<T> where T : new()
    {
        private ResultMapDelegate mapper;
        private readonly DbDataReader reader;
        
        public delegate T ResultMapDelegate(DataReader reader);

        public DataResult(DbDataReader reader)
        {
            this.reader = reader;
        }

        public DataResult<T> SetMap(ResultMapDelegate resultMapDelegate)
        {
            mapper = resultMapDelegate;
            return this;
        }

        public IList<T> AsList()
        {
            List<T> returnList = new List<T>();

            var dataReader = new DataReader(reader);
            while (reader.Read())
            {
                T data = new T();
                if (mapper != null)
                {
                    data = mapper.Invoke(dataReader);
                }
                else
                {
                    reader.Load(data);
                }

                returnList.Add(data);
            }

            return returnList;
        }

        public IList<T> AsList(ResultMapDelegate map)
        {
            mapper = map;
            return AsList();
        }

        public T AsSingle()
        {
            var dataReader = new DataReader(reader);
            if (reader.Read())
            {
                T data = new T();
                if (mapper != null)
                {
                    data = mapper.Invoke(dataReader);
                }
                else
                {
                    reader.Load(data);
                }

                return data;
            }
            return default(T);
        }

        public T AsSingle(ResultMapDelegate map)
        {
            mapper = map;
            return AsSingle();
        }

    }
}
