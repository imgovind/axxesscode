﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="diagnosisList" class="abs window">
<div class="abs window_inner">
    <div class="window_top">
        <span class="float_left">Diagnosis List</span> <span class="float_right"><a href="javascript:void(0);"
            class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                href="javascript:void(0);" class="window_close"></a></span>
    </div>
    <div class="abs window_content general_form">
        <div class="submitoasisCont">
            
                <!--[if !IE]>start table_wrapper<![endif]-->
                <%= Html.Telerik().Grid<IDiagnosisCode>()
                        .Name("DiagnosisCodeGrid")
                        .Columns(columns =>
		                {
                            columns.Bound(d => d.Code).Title("Code").Width(100);
                            columns.Bound(d => d.LongDescription).Title("Description");
                        })
                        .DataBinding(dataBinding => dataBinding.Ajax().Select("DiagnosisCodes", "LookUp"))
                        .Pageable(paging => paging.PageSize(13))
                        .Sortable()
                        .Scrollable(scrolling => scrolling.Enabled(true).Height(369))
                %>
                <!--[if !IE]>end table_wrapper<![endif]-->
            </div>
    </div>
    <div class="abs window_bottom">
        List of Diagnosis Codes
    </div>
</div>
<span class="abs ui-resizable-handle ui-resizable-se"></span></div>
