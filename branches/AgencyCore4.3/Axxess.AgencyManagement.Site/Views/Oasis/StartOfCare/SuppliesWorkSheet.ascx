﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>

    <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareSuppliesWorksheetForm" }))%>
    <%  { %>
    <%= Html.Hidden("StartOfCare_Id", "")%>
    <%= Html.Hidden("StartOfCare_Action", "Edit")%>
    <%= Html.Hidden("StartOfCare_PatientGuid", "")%>
    <%= Html.Hidden("assessment", "StartOfCare")%>
    <div class="rowOasisButtons">
        <div class="row485">
            <table id="suppliesTable" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="4">
                            Current Supplies
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Supplies Description
                        </th>
                        <th>
                            Code
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <input type="hidden" name="StartOfCare_GenericSupply" id="StartOfCare_GenericSupply"
                value="" class="SupplyValue" />
            <input value="Add Row" onclick="Oasis.addTableRow('#suppliesTable');" type="button" />
        </div>
        <ul>
            <li style="float: left">
                <input type="button" value="Save/Continue" class="SaveContinue" onclick="Oasis.supplyInputFix('StartOfCare','#suppliesTable'); SOC.FormSubmit($(this));" /></li>
            <li style="float: left">
                <input type="button" value="Save/Exit" onclick="Oasis.supplyInputFix('StartOfCare','#suppliesTable'); SOC.FormSubmit($(this));" /></li>
        </ul>
    </div>
    <%} %>
