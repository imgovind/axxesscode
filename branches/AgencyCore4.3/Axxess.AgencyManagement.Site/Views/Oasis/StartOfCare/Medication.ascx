﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="row485">
    <table cellpadding="0" cellspacing="0">
        <%string[] newMedicationsLS = data.ContainsKey("485NewMedicationsLS") && data["485NewMedicationsLS"].Answer != "" ? data["485NewMedicationsLS"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="5">
                New Medications (locator #10)
            </th>
        </tr>
        <tr>
            <th>
                LS
            </th>
            <th>
                Start Date
            </th>
            <th>
                Medication/Dosage
            </th>
            <th>
                Classification
            </th>
            <th>
                Frequency/Route
            </th>
        </tr>
        <tr>
            <td>
                <input name="StartOfCare_485NewMedicationsLS" value=" " type="hidden" />
                <input name="StartOfCare_485NewMedicationsLS" value="1" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsStartDate1", data.ContainsKey("485NewMedicationsStartDate1") ? data["485NewMedicationsStartDate1"].Answer : "", new { @id = "StartOfCare_485NewMedicationsStartDate1", @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsDosage1", data.ContainsKey("485NewMedicationsDosage1") ? data["485NewMedicationsDosage1"].Answer : "", new { @id = "StartOfCare_485NewMedicationsDosage1",  @maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="StartOfCare_485NewMedicationsClassification1"
                    id="StartOfCare_485NewMedicationsClassification1">
                </select>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsFrequency1", data.ContainsKey("485NewMedicationsFrequency1") ? data["485NewMedicationsFrequency1"].Answer : "", new { @id = "StartOfCare_485NewMedicationsFrequency1", @maxlength="50",@style="width: 200px;" })%>
            </td>
        </tr>
        <tr>
            <td>
                <input name="StartOfCare_485NewMedicationsLS" value="2" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsStartDate2", data.ContainsKey("485NewMedicationsStartDate2") ? data["485NewMedicationsStartDate2"].Answer : "", new { @id = "StartOfCare_485NewMedicationsStartDate2", @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsDosage2", data.ContainsKey("485NewMedicationsDosage2") ? data["485NewMedicationsDosage2"].Answer : "", new { @id = "StartOfCare_485NewMedicationsDosage2",@maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="StartOfCare_485NewMedicationsClassification2"
                    id="StartOfCare_485NewMedicationsClassification2">
                </select>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsFrequency2", data.ContainsKey("485NewMedicationsFrequency2") ? data["485NewMedicationsFrequency2"].Answer : "", new { @id = "StartOfCare_485NewMedicationsFrequency2", @style = "width: 200px;", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td>
                <input name="StartOfCare_485NewMedicationsLS" value="3" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsStartDate3", data.ContainsKey("485NewMedicationsStartDate3") ? data["485NewMedicationsStartDate3"].Answer : "", new { @id = "StartOfCare_485NewMedicationsStartDate3",  @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsDosage3", data.ContainsKey("485NewMedicationsDosage3") ? data["485NewMedicationsDosage3"].Answer : "", new { @id = "StartOfCare_485NewMedicationsDosage3",  @maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="StartOfCare_485NewMedicationsClassification3"
                    id="StartOfCare_485NewMedicationsClassification3">
                </select>
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485NewMedicationsFrequency3", data.ContainsKey("485NewMedicationsFrequency3") ? data["485NewMedicationsFrequency3"].Answer : "", new { @id = "StartOfCare_485NewMedicationsFrequency3", @style = "width: 200px;", @maxlength = "50" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="100%">
                Medication Administration Record
            </th>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Time:</strong>&nbsp;
                <%=Html.TextBox("StartOfCare_GenericMedRecTime", data.ContainsKey("GenericMedRecTime") ? data["GenericMedRecTime"].Answer : "", new { @id = "StartOfCare_GenericMedRecTime", @size = "6" })%>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Medication</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecMedication", data.ContainsKey("GenericMedRecMedication") ? data["GenericMedRecMedication"].Answer : "", new { @id = "StartOfCare_GenericMedRecMedication", @size="30", @maxlength="50" })%>
            </td>
            <td>
                <strong>Dose</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecDose", data.ContainsKey("GenericMedRecDose") ? data["GenericMedRecDose"].Answer : "", new { @id = "StartOfCare_GenericMedRecDose", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>Route</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecRoute", data.ContainsKey("GenericMedRecRoute") ? data["GenericMedRecRoute"].Answer : "", new { @id = "StartOfCare_GenericMedRecRoute", @size = "30", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Frequency</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecFrequency", data.ContainsKey("GenericMedRecFrequency") ? data["GenericMedRecFrequency"].Answer : "", new { @id = "StartOfCare_GenericMedRecFrequency", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>PRN Reason</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecPRN", data.ContainsKey("GenericMedRecPRN") ? data["GenericMedRecPRN"].Answer : "", new { @id = "StartOfCare_GenericMedRecPRN", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>Location</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecLocation", data.ContainsKey("GenericMedRecLocation") ? data["GenericMedRecLocation"].Answer : "", new { @id = "StartOfCare_GenericMedRecLocation", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>Patient Response</strong><br />
                <%=Html.TextBox("StartOfCare_GenericMedRecResponse", data.ContainsKey("GenericMedRecResponse") ? data["GenericMedRecResponse"].Answer : "", new { @id = "StartOfCare_GenericMedRecResponse", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                <strong>Comment</strong><br />
                <%=Html.TextArea("StartOfCare_GenericMedRecComments", data.ContainsKey("GenericMedRecComments") ? data["GenericMedRecComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericMedRecComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2000) Drug Regimen Review: Does a complete drug regimen review indicate potential
                clinically significant medication issues, e.g., drug reactions, ineffective drug
                therapy, side effects, drug interactions, duplicate therapy, omissions, dosage errors,
                or noncompliance?
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("StartOfCare_M2000DrugRegimenReview", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2000DrugRegimenReview", "00", data.ContainsKey("M2000DrugRegimenReview") && data["M2000DrugRegimenReview"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Not assessed/reviewed [ Go to M2010 ]<br />
                <%=Html.RadioButton("StartOfCare_M2000DrugRegimenReview", "01", data.ContainsKey("M2000DrugRegimenReview") && data["M2000DrugRegimenReview"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - No problems found during review [ Go to M2010 ]<br />
                <%=Html.RadioButton("StartOfCare_M2000DrugRegimenReview", "02", data.ContainsKey("M2000DrugRegimenReview") && data["M2000DrugRegimenReview"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Problems found during review<br />
                <%=Html.RadioButton("StartOfCare_M2000DrugRegimenReview", "NA", data.ContainsKey("M2000DrugRegimenReview") && data["M2000DrugRegimenReview"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient is not taking any medications [ Go to M2040 ]
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>
                IV Access
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Does patient have IV access? </li>
                    <li>
                        <%=Html.Hidden("StartOfCare_GenericIVAccess", " ", new { @id = "" })%>
                        <%=Html.RadioButton("StartOfCare_GenericIVAccess", "1", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;Yes
                    </li>
                    <li>
                        <%=Html.RadioButton("StartOfCare_GenericIVAccess", "0", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;No
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Type:</li>
                    <li>
                        <%=Html.TextBox("StartOfCare_GenericIVAccessType", data.ContainsKey("GenericIVAccessType") ? data["GenericIVAccessType"].Answer : "", new { @id = "StartOfCare_GenericIVAccessType", @size = "50", @maxlength = "50" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Date of Insertion:</li>
                    <li>
                        <%=Html.TextBox("StartOfCare_GenericIVAccessDate", data.ContainsKey("GenericIVAccessDate") ? data["GenericIVAccessDate"].Answer : "", new { @id = "StartOfCare_GenericIVAccessDate", @size = "11", @maxlength = "11" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Date of Last Dressing Change:</li>
                    <li>
                        <%=Html.TextBox("StartOfCare_GenericIVAccessDressingChange", data.ContainsKey("GenericIVAccessDressingChange") ? data["GenericIVAccessDressingChange"].Answer : "", new { @id = "StartOfCare_GenericIVAccessDressingChange", @size = "11", @maxlength = "11" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideCol" id="soc_M2002">
        <div class="insiderow title">
            <div class="padding">
                (M2002) Medication Follow-up: Was a physician or the physician-designee contacted
                within one calendar day to resolve clinically significant medication issues, including
                reconciliation?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("StartOfCare_M2002MedicationFollowup", " ", new { @id = "" })%>
            <%=Html.RadioButton("StartOfCare_M2002MedicationFollowup", "0", data.ContainsKey("M2002MedicationFollowup") && data["M2002MedicationFollowup"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("StartOfCare_M2002MedicationFollowup", "1", data.ContainsKey("M2002MedicationFollowup") && data["M2002MedicationFollowup"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
    <div class="insideCol" id="soc_M2010">
        <div class="insiderow title">
            <div class="padding">
                (M2010) Patient/Caregiver High Risk Drug Education: Has the patient/caregiver received
                instruction on special precautions for all high-risk medications (such as hypoglycemics,
                anticoagulants, etc.) and how and when to report problems that may occur?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation", " ", new { @id = "" })%>
            <%=Html.RadioButton("StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation", "00", data.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && data["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation", "01", data.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && data["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
            <br />
            <%=Html.RadioButton("StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation", "NA", data.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && data["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient not taking any high risk drugs OR patient/caregiver fully knowledgeable
            about special precautions associated with all high-risk medications
        </div>
    </div>
</div>
<div class="rowOasis" id="soc_M2020">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2020) Management of Oral Medications: Patient's current ability to prepare and
                take all oral medications reliably and safely, including administration of the correct
                dosage at the appropriate times/intervals. Excludes injectable and IV medications.
                (NOTE: This refers to ability, not compliance or willingness.)
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("StartOfCare_M2020ManagementOfOralMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("StartOfCare_M2020ManagementOfOralMedications", "00", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct oral medication(s) and proper dosage(s)
            at the correct times.<br />
            <%=Html.RadioButton("StartOfCare_M2020ManagementOfOralMedications", "01", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual dosages are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("StartOfCare_M2020ManagementOfOralMedications", "02", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person at the appropriate times<br />
            <%=Html.RadioButton("StartOfCare_M2020ManagementOfOralMedications", "03", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take medication unless administered by another person.<br />
            <%=Html.RadioButton("StartOfCare_M2020ManagementOfOralMedications", "NA", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No oral medications prescribed.
        </div>
    </div>
</div>
<div class="rowOasis" id="soc_M2030">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2030) Management of Injectable Medications: Patient's current ability to prepare
                and take all prescribed injectable medications reliably and safely, including administration
                of correct dosage at the appropriate times/intervals. Excludes IV medications.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("StartOfCare_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("StartOfCare_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct medication(s) and proper dosage(s) at the
            correct times.<br />
            <%=Html.RadioButton("StartOfCare_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take injectable medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("StartOfCare_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person based on the frequency of the injection<br />
            <%=Html.RadioButton("StartOfCare_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take injectable medication unless administered by another person.<br />
            <%=Html.RadioButton("StartOfCare_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No injectable medications prescribed.
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M2040) Prior Medication Management: Indicate the patient’s usual ability with managing
                oral and injectable medications prior to this current illness, exacerbation, or
                injury. Check only one box in each row.
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <th>
                Functional Area
            </th>
            <th>
                Independent
            </th>
            <th>
                Needed Some Help
            </th>
            <th>
                Dependent
            </th>
            <th>
                Not Applicable
            </th>
        </tr>
        <tr>
            <td>
                a. Oral medications
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2040PriorMedicationOral", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationOral", "00", data.ContainsKey("M2040PriorMedicationOral") && data["M2040PriorMedicationOral"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationOral", "01", data.ContainsKey("M2040PriorMedicationOral") && data["M2040PriorMedicationOral"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationOral", "02", data.ContainsKey("M2040PriorMedicationOral") && data["M2040PriorMedicationOral"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationOral", "NA", data.ContainsKey("M2040PriorMedicationOral") && data["M2040PriorMedicationOral"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
        </tr>
        <tr>
            <td>
                b. Injectable medications
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2040PriorMedicationInject", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationInject", "00", data.ContainsKey("M2040PriorMedicationInject") && data["M2040PriorMedicationInject"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationInject", "01", data.ContainsKey("M2040PriorMedicationInject") && data["M2040PriorMedicationInject"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationInject", "02", data.ContainsKey("M2040PriorMedicationInject") && data["M2040PriorMedicationInject"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2040PriorMedicationInject", "NA", data.ContainsKey("M2040PriorMedicationInject") && data["M2040PriorMedicationInject"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] medicationInterventions = data.ContainsKey("485MedicationInterventions") && data["485MedicationInterventions"].Answer != "" ? data["485MedicationInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="StartOfCare_485MedicationInterventions" value=" " />
                <input name="StartOfCare_485MedicationInterventions" value="1" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient filling medication box to determine if patient is preparing
                correctly
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="2" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess caregiver filling medication box to determine if caregiver is preparing
                correctly
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="3" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to determine if the
                <%var determineFrequencEachMedPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DetermineFrequencEachMedPerson") && data["485DetermineFrequencEachMedPerson"].Answer != "" ? data["485DetermineFrequencEachMedPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485DetermineFrequencEachMedPerson", determineFrequencEachMedPerson)%>
                is able to identify the correct dose, route, and frequency of each medication
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="4" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess if the
                <%var assessIndicationEachMedPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessIndicationEachMedPerson") && data["485AssessIndicationEachMedPerson"].Answer != "" ? data["485AssessIndicationEachMedPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485AssessIndicationEachMedPerson", assessIndicationEachMedPerson)%>
                can verbalize an understanding of the indication for each medication
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="5" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to establish reminders to alert patient to take medications at correct times
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input type="checkbox" name="StartOfCare_485MedicationInterventions" value="6" '<% if( medicationInterventions!=null && medicationInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess the
                <%var assessOpenMedContainersPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessOpenMedContainersPerson") && data["485AssessOpenMedContainersPerson"].Answer != "" ? data["485AssessOpenMedContainersPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485AssessOpenMedContainersPerson", assessOpenMedContainersPerson)%>
                ability to open medication containers and determine the proper dose that should
                be administered
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="7" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedicationRegimen = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedicationRegimen") && data["485InstructMedicationRegimen"].Answer != "" ? data["485InstructMedicationRegimen"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructMedicationRegimen", instructMedicationRegimen)%>
                on medication regimen dose, indications, side effects, and interactions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="8" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to remove any duplicate or expired medications to prevent confusion with medication
                regimen
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="9" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("9")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to observe patient drawing up injectable medications to determine if patient
                is able to draw up the correct dose
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485AssessAdminInjectMeds" value="10" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("10")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess the
                <%var assessAdminInjectMedsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessAdminInjectMedsPerson") && data["485AssessAdminInjectMedsPerson"].Answer != "" ? data["485AssessAdminInjectMedsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485AssessAdminInjectMedsPerson", assessAdminInjectMedsPerson)%>
                administering injectable medications to determine if proper technique is utilized
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="11" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("11")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to report to physician if drug therapy appears to be ineffective
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="12" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("12")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <select name="StartOfCare_485InstructHighRiskMedsPerson" id="StartOfCare_485InstructHighRiskMedsPerson">
                    <option value="Patient/Caregiver">Patient/Caregiver</option>
                    <option value="Patient">Patient</option>
                    <option value="Caregiver">Caregiver</option>
                </select>
                on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets,
                sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle
                relaxants
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="13" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("13")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSignsSymptomsIneffectiveDrugPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructSignsSymptomsIneffectiveDrugPerson") && data["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer != "" ? data["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructSignsSymptomsIneffectiveDrugPerson", instructSignsSymptomsIneffectiveDrugPerson)%>
                on signs and symptoms of ineffective drug therapy to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="14" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("14")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedSideEffectsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedSideEffectsPerson") && data["485InstructMedSideEffectsPerson"].Answer != "" ? data["485InstructMedSideEffectsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructMedSideEffectsPerson", instructMedSideEffectsPerson)%>
                on medication side effects to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="15" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("15")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedReactionsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedReactionsPerson") && data["485InstructMedReactionsPerson"].Answer != "" ? data["485InstructMedReactionsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructMedReactionsPerson", instructMedReactionsPerson)%>
                on medication reactions to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="16" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("16")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer IV
                <%=Html.TextBox("StartOfCare_485AdministerIVType", data.ContainsKey("485AdministerIVType") ? data["485AdministerIVType"].Answer : "", new { @id = "StartOfCare_485AdministerIVType", @size = "15", @maxlength = "15" })%>
                at rate of
                <%=Html.TextBox("StartOfCare_485AdministerIVRate", data.ContainsKey("485AdministerIVRate") ? data["485AdministerIVRate"].Answer : "", new { @id = "StartOfCare_485AdministerIVRate", @size = "15", @maxlength = "15" })%>
                via
                <%=Html.TextBox("StartOfCare_485AdministerIVVia", data.ContainsKey("485AdministerIVVia") ? data["485AdministerIVVia"].Answer : "", new { @id = "StartOfCare_485AdministerIVVia", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485AdministerIVEvery", data.ContainsKey("485AdministerIVEvery") ? data["485AdministerIVEvery"].Answer : "", new { @id = "StartOfCare_485AdministerIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="17" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("17")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructAdministerIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructAdministerIVPerson") && data["485InstructAdministerIVPerson"].Answer != "" ? data["485InstructAdministerIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructAdministerIVPerson", instructAdministerIVPerson)%>
                to administer IV at rate of
                <%=Html.TextBox("StartOfCare_485InstructAdministerIVRate", data.ContainsKey("485InstructAdministerIVRate") ? data["485InstructAdministerIVRate"].Answer : "", new { @id = "StartOfCare_485InstructAdministerIVRate", @size = "15", @maxlength = "15" })%>
                via
                <%=Html.TextBox("StartOfCare_485InstructAdministerIVVia", data.ContainsKey("485InstructAdministerIVVia") ? data["485InstructAdministerIVVia"].Answer : "", new { @id = "StartOfCare_485InstructAdministerIVVia", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485InstructAdministerIVEvery", data.ContainsKey("485InstructAdministerIVEvery") ? data["485InstructAdministerIVEvery"].Answer : "", new { @id = "StartOfCare_485InstructAdministerIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="18" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("18")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change peripheral IV catheter every 72 hours with
                <%=Html.TextBox("StartOfCare_485ChangePeripheralIVGauge", data.ContainsKey("485ChangePeripheralIVGauge") ? data["485ChangePeripheralIVGauge"].Answer : "", new { @id = "StartOfCare_485ChangePeripheralIVGauge", @size = "15", @maxlength = "15" })%>
                gauge
                <%=Html.TextBox("StartOfCare_485ChangePeripheralIVWidth", data.ContainsKey("485ChangePeripheralIVWidth") ? data["485ChangePeripheralIVWidth"].Answer : "", new { @id = "StartOfCare_485ChangePeripheralIVWidth", @size = "15", @maxlength = "15" })%>
                inch angiocath
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="19" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("19")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to flush peripheral IV with
                <%=Html.TextBox("StartOfCare_485FlushPeripheralIVWith", data.ContainsKey("485FlushPeripheralIVWith") ? data["485FlushPeripheralIVWith"].Answer : "", new { @id = "StartOfCare_485FlushPeripheralIVWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("StartOfCare_485FlushPeripheralIVOf", data.ContainsKey("485FlushPeripheralIVOf") ? data["485FlushPeripheralIVOf"].Answer : "", new { @id = "StartOfCare_485FlushPeripheralIVOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485FlushPeripheralIVEvery", data.ContainsKey("485FlushPeripheralIVEvery") ? data["485FlushPeripheralIVEvery"].Answer : "", new { @id = "StartOfCare_485FlushPeripheralIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="20" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("20")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructFlushPerpheralIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructFlushPerpheralIVPerson") && data["485InstructFlushPerpheralIVPerson"].Answer != "" ? data["485InstructFlushPerpheralIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructFlushPerpheralIVPerson", instructFlushPerpheralIVPerson)%>
                to flush peripheral IV with
                <%=Html.TextBox("StartOfCare_485InstructFlushPerpheralIVWith", data.ContainsKey("485InstructFlushPerpheralIVWith") ? data["485InstructFlushPerpheralIVWith"].Answer : "", new { @id = "StartOfCare_485InstructFlushPerpheralIVWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("StartOfCare_485InstructFlushPerpheralIVOf", data.ContainsKey("485InstructFlushPerpheralIVOf") ? data["485InstructFlushPerpheralIVOf"].Answer : "", new { @id = "StartOfCare_485InstructFlushPerpheralIVOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485InstructFlushPerpheralIVEvery", data.ContainsKey("485InstructFlushPerpheralIVEvery") ? data["485InstructFlushPerpheralIVEvery"].Answer : "", new { @id = "StartOfCare_485InstructFlushPerpheralIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="21" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("21")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change central line dressing every
                <%=Html.TextBox("StartOfCare_485ChangeCentralLineEvery", data.ContainsKey("485ChangeCentralLineEvery") ? data["485ChangeCentralLineEvery"].Answer : "", new { @id = "StartOfCare_485ChangeCentralLineEvery", @size = "15", @maxlength = "15" })%>
                using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="22" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("22")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructChangeCentralLinePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructChangeCentralLinePerson") && data["485InstructChangeCentralLinePerson"].Answer != "" ? data["485InstructChangeCentralLinePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructChangeCentralLinePerson", instructChangeCentralLinePerson)%>
                to change central line dressing every
                <%=Html.TextBox("StartOfCare_485InstructChangeCentralLineEvery", data.ContainsKey("485InstructChangeCentralLineEvery") ? data["485InstructChangeCentralLineEvery"].Answer : "", new { @id = "StartOfCare_485InstructChangeCentralLineEvery", @size = "15", @maxlength = "15" })%>
                using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="23" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("23")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to flush central line with
                <%=Html.TextBox("StartOfCare_485FlushCentralLineWith", data.ContainsKey("485FlushCentralLineWith") ? data["485FlushCentralLineWith"].Answer : "", new { @id = "StartOfCare_485FlushCentralLineWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("StartOfCare_485FlushCentralLineOf", data.ContainsKey("485FlushCentralLineOf") ? data["485FlushCentralLineOf"].Answer : "", new { @id = "StartOfCare_485FlushCentralLineOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485FlushCentralLineEvery", data.ContainsKey("485FlushCentralLineEvery") ? data["485FlushCentralLineEvery"].Answer : "", new { @id = "StartOfCare_485FlushCentralLineEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="24" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("24")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructFlushCentralLinePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructFlushCentralLinePerson") && data["485InstructFlushCentralLinePerson"].Answer != "" ? data["485InstructFlushCentralLinePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructFlushCentralLinePerson", instructFlushCentralLinePerson)%>
                to flush central line with
                <%=Html.TextBox("StartOfCare_485InstructFlushCentralLineWith", data.ContainsKey("485InstructFlushCentralLineWith") ? data["485InstructFlushCentralLineWith"].Answer : "", new { @id = "StartOfCare_485InstructFlushCentralLineWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("StartOfCare_485InstructFlushCentralLineOf", data.ContainsKey("485InstructFlushCentralLineOf") ? data["485InstructFlushCentralLineOf"].Answer : "", new { @id = "StartOfCare_485InstructFlushCentralLineOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485InstructFlushCentralLineEvery", data.ContainsKey("485InstructFlushCentralLineEvery") ? data["485InstructFlushCentralLineEvery"].Answer : "", new { @id = "StartOfCare_485InstructFlushCentralLineEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="25" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("25")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to access
                <%=Html.TextBox("StartOfCare_485AccessPortType", data.ContainsKey("485AccessPortType") ? data["485AccessPortType"].Answer : "", new { @id = "StartOfCare_485AccessPortType", @size = "15", @maxlength = "15" })%>
                port every
                <%=Html.TextBox("StartOfCare_485AccessPortTypeEvery", data.ContainsKey("485AccessPortTypeEvery") ? data["485AccessPortTypeEvery"].Answer : "", new { @id = "StartOfCare_485AccessPortTypeEvery", @size = "15", @maxlength = "15" })%>
                and flush with
                <%=Html.TextBox("StartOfCare_485AccessPortTypeWith", data.ContainsKey("485AccessPortTypeWith") ? data["485AccessPortTypeWith"].Answer : "", new { @id = "StartOfCare_485AccessPortTypeWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("StartOfCare_485AccessPortTypeOf", data.ContainsKey("485AccessPortTypeOf") ? data["485AccessPortTypeOf"].Answer : "", new { @id = "StartOfCare_485AccessPortTypeOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("StartOfCare_485AccessPortTypeFrequency", data.ContainsKey("485AccessPortTypeFrequency") ? data["485AccessPortTypeFrequency"].Answer : "", new { @id = "StartOfCare_485AccessPortTypeFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="26" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("26")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change
                <%=Html.TextBox("StartOfCare_485ChangePortDressingType", data.ContainsKey("485ChangePortDressingType") ? data["485ChangePortDressingType"].Answer : "", new { @id = "StartOfCare_485ChangePortDressingType", @size = "15", @maxlength = "15" })%>
                port dressing using sterile technique every
                <%=Html.TextBox("StartOfCare_485ChangePortDressingEvery", data.ContainsKey("485ChangePortDressingEvery") ? data["485ChangePortDressingEvery"].Answer : "", new { @id = "StartOfCare_485ChangePortDressingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="27" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("27")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructPortDressingPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructPortDressingPerson") && data["485InstructPortDressingPerson"].Answer != "" ? data["485InstructPortDressingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructPortDressingPerson", instructPortDressingPerson)%>
                to change
                <%=Html.TextBox("StartOfCare_485InstructPortDressingType", data.ContainsKey("485InstructPortDressingType") ? data["485InstructPortDressingType"].Answer : "", new { @id = "StartOfCare_485InstructPortDressingType", @size = "15", @maxlength = "15" })%>
                port dressing using sterile technique every
                <%=Html.TextBox("StartOfCare_485InstructPortDressingEvery", data.ContainsKey("485InstructPortDressingEvery") ? data["485InstructPortDressingEvery"].Answer : "", new { @id = "StartOfCare_485InstructPortDressingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="28" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("28")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change IV tubing every
                <%=Html.TextBox("StartOfCare_485ChangeIVTubingEvery", data.ContainsKey("485ChangeIVTubingEvery") ? data["485ChangeIVTubingEvery"].Answer : "", new { @id = "StartOfCare_485ChangeIVTubingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationInterventions" value="29" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("29")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructInfectionSignsSymptomsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructInfectionSignsSymptomsPerson") && data["485InstructInfectionSignsSymptomsPerson"].Answer != "" ? data["485InstructInfectionSignsSymptomsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485InstructInfectionSignsSymptomsPerson", instructInfectionSignsSymptomsPerson)%>
                on signs and symptoms of infection and infiltration
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var medicationInterventionTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485MedicationInterventionTemplates") && data["485MedicationInterventionTemplates"].Answer != "" ? data["485MedicationInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485MedicationInterventionTemplates", medicationInterventionTemplates)%>
                <br />
                <%=Html.TextArea("StartOfCare_485MedicationInterventionComments", data.ContainsKey("485MedicationInterventionComments") ? data["485MedicationInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485MedicationInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] medicationGoals = data.ContainsKey("485MedicationGoals") && data["485MedicationGoals"].Answer != "" ? data["485MedicationGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="StartOfCare_485MedicationGoals" value=" " />
                <input name="StartOfCare_485MedicationGoals" value="1" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free of adverse medication reactions during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="2" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medManagementIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedManagementIndependentPerson") && data["485MedManagementIndependentPerson"].Answer != "" ? data["485MedManagementIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485MedManagementIndependentPerson", medManagementIndependentPerson)%>
                will be independent with medication management by:
                <%=Html.TextBox("StartOfCare_485MedManagementIndependentDate", data.ContainsKey("485MedManagementIndependentDate") ? data["485MedManagementIndependentDate"].Answer : "", new { @id = "StartOfCare_485MedManagementIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="3" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeMedRegimenUnderstandingPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeMedRegimenUnderstandingPerson") && data["485VerbalizeMedRegimenUnderstandingPerson"].Answer != "" ? data["485VerbalizeMedRegimenUnderstandingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485VerbalizeMedRegimenUnderstandingPerson", verbalizeMedRegimenUnderstandingPerson)%>
                will verbalize understanding of medication regimen, dose, route, frequency, indications,
                and side effects by:
                <%=Html.TextBox("StartOfCare_485VerbalizeMedRegimenUnderstandingDate", data.ContainsKey("485VerbalizeMedRegimenUnderstandingDate") ? data["485VerbalizeMedRegimenUnderstandingDate"].Answer : "", new { @id = "StartOfCare_485VerbalizeMedRegimenUnderstandingDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="4" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medAdminIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedAdminIndependentPerson") && data["485MedAdminIndependentPerson"].Answer != "" ? data["485MedAdminIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485MedAdminIndependentPerson", medAdminIndependentPerson)%>
                will be independent with
                <%=Html.TextBox("StartOfCare_485MedAdminIndependentWith", data.ContainsKey("485MedAdminIndependentWith") ? data["485MedAdminIndependentWith"].Answer : "", new { @id = "StartOfCare_485MedAdminIndependentWith", @size = "10", @maxlength = "10" })%>
                administration by:
                <%=Html.TextBox("StartOfCare_485MedAdminIndependentDate", data.ContainsKey("485MedAdminIndependentDate") ? data["485MedAdminIndependentDate"].Answer : "", new { @id = "StartOfCare_485MedAdminIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="5" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medSetupIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedSetupIndependentPerson") && data["485MedSetupIndependentPerson"].Answer != "" ? data["485MedSetupIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485MedSetupIndependentPerson", medSetupIndependentPerson)%>
                will be independent with setting up medication boxes by:
                <%=Html.TextBox("StartOfCare_485MedSetupIndependentDate", data.ContainsKey("485MedSetupIndependentDate") ? data["485MedSetupIndependentDate"].Answer : "", new { @id = "StartOfCare_485MedSetupIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="6" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeEachMedIndicationPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeEachMedIndicationPerson") && data["485VerbalizeEachMedIndicationPerson"].Answer != "" ? data["485VerbalizeEachMedIndicationPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485VerbalizeEachMedIndicationPerson", verbalizeEachMedIndicationPerson)%>
                will be able to verbalize an understanding of the indications for each medication
                by:
                <%=Html.TextBox("StartOfCare_485VerbalizeEachMedIndicationDate", data.ContainsKey("485VerbalizeEachMedIndicationDate") ? data["485VerbalizeEachMedIndicationDate"].Answer : "", new { @id = "StartOfCare_485VerbalizeEachMedIndicationDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="7" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var correctDoseIdentifyPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485CorrectDoseIdentifyPerson") && data["485CorrectDoseIdentifyPerson"].Answer != "" ? data["485CorrectDoseIdentifyPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485CorrectDoseIdentifyPerson", correctDoseIdentifyPerson)%>
                will be able to identify the correct dose, route, and frequency of each medication
                by:
                <%=Html.TextBox("StartOfCare_485CorrectDoseIdentifyDate", data.ContainsKey("485CorrectDoseIdentifyDate") ? data["485CorrectDoseIdentifyDate"].Answer : "", new { @id = "StartOfCare_485CorrectDoseIdentifyDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="8" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                IV will remain patent and free from signs and symptoms of infection
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="9" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("9")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateCentralLineFlushPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateCentralLineFlushPerson") && data["485DemonstrateCentralLineFlushPerson"].Answer != "" ? data["485DemonstrateCentralLineFlushPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485DemonstrateCentralLineFlushPerson", demonstrateCentralLineFlushPerson)%>
                will demonstrate understanding of flushing central line
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="10" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("10")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstratePeripheralIVLineFlushPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstratePeripheralIVLineFlushPerson") && data["485DemonstratePeripheralIVLineFlushPerson"].Answer != "" ? data["485DemonstratePeripheralIVLineFlushPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485DemonstratePeripheralIVLineFlushPerson", demonstratePeripheralIVLineFlushPerson)%>
                will demonstrate understanding of flushing peripheral IV line
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="11" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("11")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateSterileDressingTechniquePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateSterileDressingTechniquePerson") && data["485DemonstrateSterileDressingTechniquePerson"].Answer != "" ? data["485DemonstrateSterileDressingTechniquePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485DemonstrateSterileDressingTechniquePerson", demonstrateSterileDressingTechniquePerson)%>
                will demonstrate understanding of changing
                <%=Html.TextBox("StartOfCare_485DemonstrateSterileDressingTechniqueType", data.ContainsKey("485DemonstrateSterileDressingTechniqueType") ? data["485DemonstrateSterileDressingTechniqueType"].Answer : "", new { @id = "StartOfCare_485DemonstrateSterileDressingTechniqueType", @size = "10", @maxlength = "10" })%>
                dressing using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485MedicationGoals" value="12" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("12")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateAdministerIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateAdministerIVPerson") && data["485DemonstrateAdministerIVPerson"].Answer != "" ? data["485DemonstrateAdministerIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485DemonstrateAdministerIVPerson", demonstrateAdministerIVPerson)%>
                will demonstrate understanding of administering IV
                <%=Html.TextBox("StartOfCare_485DemonstrateAdministerIVType", data.ContainsKey("485DemonstrateAdministerIVType") ? data["485DemonstrateAdministerIVType"].Answer : "", new { @id = "StartOfCare_485DemonstrateAdministerIVType", @size = "10", @maxlength = "10" })%>
                at rate of
                <%=Html.TextBox("StartOfCare_485DemonstrateAdministerIVRate", data.ContainsKey("485DemonstrateAdministerIVRate") ? data["485DemonstrateAdministerIVRate"].Answer : "", new { @id = "StartOfCare_485DemonstrateAdministerIVRate", @size = "10", @maxlength = "10" })%>
                via
                <%=Html.TextBox("StartOfCare_485DemonstrateAdministerIVVia", data.ContainsKey("485DemonstrateAdministerIVVia") ? data["485DemonstrateAdministerIVVia"].Answer : "", new { @id = "StartOfCare_485DemonstrateAdministerIVVia", @size = "10", @maxlength = "10" })%>
                every
                <%=Html.TextBox("StartOfCare_485DemonstrateAdministerIVEvery", data.ContainsKey("485DemonstrateAdministerIVEvery") ? data["485DemonstrateAdministerIVEvery"].Answer : "", new { @id = "StartOfCare_485DemonstrateAdministerIVEvery", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var medicationGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485MedicationGoalTemplates") && data["485MedicationGoalTemplates"].Answer != "" ? data["485MedicationGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485MedicationGoalTemplates", medicationGoalTemplates)%>
                <br />
                <%=Html.TextArea("StartOfCare_485MedicationGoalComments", data.ContainsKey("485MedicationGoalComments") ? data["485MedicationGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485MedicationGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
