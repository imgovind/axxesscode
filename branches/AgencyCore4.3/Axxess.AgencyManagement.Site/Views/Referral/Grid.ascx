﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="submitoasisCont">
    <!--[if !IE]>start table_wrapper<![endif]-->
    <%= Html.Telerik().Grid<Referral>()
                        .Name("ExistingReferralGrid")
                        .ToolBar(commnds => commnds.Custom().HtmlAttributes(new { @id = "List_Referral_NewButton", @href = "javascript:void(0);", @onclick = "JQD.open_window('#newReferral');" }).Text("Add New Referral"))                                                                                                    
                        .Columns(columns =>
		                {
                            columns.Bound(r => r.Created).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width(100);
                            columns.Bound(r => r.DisplayName).Width(180);
                            columns.Bound(r => r.ReferralSource);
                            columns.Bound(r => r.Status).Width(80);           
                            columns.Bound(r =>r.Id)
                                   .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Referral.loadEditReferral('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Delete($(this),'<#=Id#>');\" class=\"deleteReferral\">Delete</a> | <button type='button' class='admitreferral' onclick='Referral.loadAdmit($(this).val());' value='<#=Id#>' >Admit</button>")
                                   .Title("Action").Width(180);
                        })
                        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Referral"))
                        .ClientEvents(events => events.OnDataBound("Referral.NewReferral"))
                        .Pageable(paging => paging.PageSize(10))
                        .Sortable()
                        .Scrollable(scrolling => scrolling.Enabled(true).Height(369))
    %>
</div>
