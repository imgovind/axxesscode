﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("NewEmergencyContact", "Patient", FormMethod.Post, new { @id = "newEmergencyContactForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model, new { @id = "txtNew_EmergencyContact_PatientID" })%>
<div id="newEmergencyContactValidaton" class="marginBreak" style="display: none">
</div>
<div class="marginBreak">
    <div class="rowBreak">
        <!--[if !IE]>start section content top<![endif]-->
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("FirstName", "", new { @id = "txtNew_EmergencyContact_FirstName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "33" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("LastName", "", new { @id = "txtNew_EmergencyContact_LastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "34" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneHome">
                            Primary Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                    margin: 0px;" name="PhonePrimaryArray" id="txtNew_EmergencyContact_PrimaryPhoneArray1"
                                    maxlength="3" size="3" tabindex="35" /></span> - <span class="input_wrappermultible">
                                        <input type="text" class="autotext digits required" style="width: 49px; padding: 0px;
                                            margin: 0px;" name="PhonePrimaryArray" id="txtNew_EmergencyContact_PrimaryPhoneArray2"
                                            maxlength="3" size="3" tabindex="36" /></span> - <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                    margin: 0px;" name="PhonePrimaryArray" id="txtNew_EmergencyContact_PrimaryPhoneArray3"
                                                    maxlength="4" size="5" tabindex="37" /></span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneMobile">
                            Alt Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                    name="PhoneAlternateArray" id="txtNew_EmergencyContact_AltPhoneArray1"
                                    maxlength="3" size="3" tabindex="38" /></span> - <span class="input_wrappermultible">
                                        <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                            name="PhoneAlternateArray" id="txtNew_EmergencyContact_AltPhoneArray2"
                                            maxlength="3" size="3" tabindex="39" /></span> - <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="PhoneAlternateArray" id="txtNew_EmergencyContact_AltPhoneArray3"
                                                    maxlength="4" size="5" tabindex="40" /></span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Email">
                            Email:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("EmailAddress", "", new { @id = "txtNew_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "41" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="Relationship">
                            Relationship:</label>
                        <div class="inputs">
                            <%=Html.TextBox("Relationship", "", new { @id = "txtNew_EmergencyContact_Relationship", @class = "text input_wrapper required", @tabindex = "42" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine1", "", new { @id = "txtNew_EmergencyContact_AddressLine1", @class = "text input_wrapper required", @tabindex = "43" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine2", "", new { @id = "txtNew_EmergencyContact_AddressLine2", @class = "text input_wrapper", tabindex = "44" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressCity", "", new { @id = "txtNew_EmergencyContact_AddressCity", @class = "text input_wrapper required", @tabindex = "45" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State, Zip Code :</label>
                        <div class="inputs">
                            <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "0", new { @id = "txtNew_EmergencyContact_AddressStateCode", @class = "AddressStateCode input_wrapper required selectDropDown", @tabindex = "", @style = "width: 119px;" })%>
                                &nbsp;
                            <%=Html.TextBox("AddressZipCode", "", new { @id = "txtNew_EmergencyContact_AddressZipCode", @class = "text digits isValidUSZip required", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SetPrimary">
                            Set Primary:</label>
                        <div class="inputs">
                            <%=Html.CheckBox("IsPrimary", new { @id = "txtNew_EmergencyContact_SetPrimary" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="buttons">
        <ul>
            <li>
                <input name="" type="submit" value="Add" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="reset" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
