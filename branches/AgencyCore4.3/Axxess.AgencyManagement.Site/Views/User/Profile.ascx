﻿
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="userprofile_window" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">User Profile</span> <span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <!--[if !IE]>start forms<![endif]-->
            <% using (Html.BeginForm("SaveDetails", "Employee", FormMethod.Post, new { @id = "userProfileForm" }))%>
            <%  { %>
            <div id="userProfileValidaton" class="marginBreak " style="display: none">
            </div>
            <div class="marginBreak">
                <b>My Address</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AddressLine1">
                                        Address Line 1:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressLine1", "", new { @id = "txtNew_UserProfile_AddressLine1", @class = "text required input_wrapper", @tabindex = "14" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressLine2">
                                        Address Line 2:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressLine2", "", new { @id = "txtNew_UserProfile_AddressLine2", @class = "text input_wrapper", tabindex = "15" })%>
                                    </div>
                                </div>
                                 <div class="row">
                                    <label for="AddressZipCode">
                                        Zip:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressZipCode", "", new { @id = "txtNew_UserProfile_AddressZipCode", @class = "text input_wrapper required digits isValidUSZip", @tabindex = "18", @style = "margin: 0px; padding: 0px; width: 54px;", @size = "5", @maxlength = "5" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressCity">
                                        City:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressCity", "", new { @id = "txtNew_UserProfile_AddressCity", @class = "text required input_wrapper", @tabindex = "16" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressStateCode">
                                        State:</label>
                                    <div class="inputs">
                                        <select style="width: 180px;" class="AddressStateCode required selectDropDown" tabindex="17" name="AddressStateCode"
                                            id="txtNew_UserProfile_AddressStateCode">
                                            <option value="0" selected="selected">** Select State **</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <label for="PhoneHome">
                                        Home Phone:</label>
                                    <div class="inputs">
                                        <input type="text" class="autotext required digits" style="width: 40px; padding: 0px;
                                            margin: 0px;" name="HomePhoneArray" id="txtNew_UserProfile_HomePhone1" maxlength="3"
                                            size="3" tabindex="19" />
                                        -
                                        <input type="text" class="autotext required digits" style="width: 40px; padding: 0px;
                                            margin: 0px;" name="HomePhoneArray" id="txtNew_UserProfile_HomePhone2" maxlength="3"
                                            size="3" tabindex="20" />
                                        -
                                        <input type="text" class="autotext required digits" style="width: 40px; padding: 0px;
                                            margin: 0px;" name="HomePhoneArray" id="txtNew_UserProfile_HomePhone3" maxlength="4"
                                            size="5" tabindex="21" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="PhoneMobile">
                                        Mobile Phone:</label>
                                    <div class="inputs">
                                        <input type="text" class="autotext digits" style="width: 40px; padding: 0px; margin: 0px;"
                                            name="MobilePhoneArray" id="txtNew_UserProfile_MobilePhone1" maxlength="3"
                                            size="3" tabindex="22" />
                                        -
                                        <input type="text" class="autotext digits" style="width: 40px; padding: 0px; margin: 0px;"
                                            name="MobilePhoneArray" id="txtNew_UserProfile_MobilePhone2" maxlength="3"
                                            size="3" tabindex="23" />
                                        -
                                        <input type="text" class="autotext digits" style="width: 40px; padding: 0px; margin: 0px;"
                                            name="MobilePhoneArray" id="txtNew_UserProfile_MobilePhone3" maxlength="4"
                                            size="5" tabindex="24" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li>
                            <input type="submit" value="Save" /></li>
                        <li>
                            <input type="button" value="Cancel" onclick="Employee.Close($(this));" /></li>
                        <li>
                            <input type="reset" value="Reset" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
            <!--[if !IE]>end forms<![endif]-->
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
