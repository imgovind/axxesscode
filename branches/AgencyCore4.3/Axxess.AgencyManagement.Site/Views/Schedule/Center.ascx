﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<div class="temp-container">
    <div id='ScheduleSplitter1' class="xSplitter splitterContainer">
        <div class="xsp-pane sidepanel">
            <div id="ScheduleLeftSide" class="side">
                <div class="sideTop">
                    <div id="schedule_tab" class="row">
                        <div id="pateintlistSche">
                            <div id="ScheduleFilterContainer">
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            View:
                                        </label>
                                    </div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="scheduleStatusDropDown" style="width: 170px;">
                                                <option value="0">All Patients</option>
                                                <option value="1">Active Patients</option>
                                                <option value="2">Discharged Patients</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Filter:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="schedulePaymentDropDown" style="width: 170px;">
                                                <option value="0">All</option>
                                                <option value="1">Medicare (traditional)</option>
                                                <option value="2">Medicare (HMO/managed care)</option>
                                                <option value="3">Medicaid (traditional)</option>
                                                <option value="4">Medicaid (HMO/managed care) </option>
                                                <option value="5">Workers' compensation</option>
                                                <option value="6">Title programs </option>
                                                <option value="7">Other government</option>
                                                <option value="8">Private</option>
                                                <option value="9">Private HMO/managed care</option>
                                                <option value="10">Self Pay</option>
                                                <option value="11">Unknown</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Find:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper">
                                            <input id="txtSearch_Schedule_Selection" class="text" name="" value="" type="text"
                                                size="18" style="width: 170px;" /></span>
                                    </div>
                                </div>
                                <div class="breakLine">
                                </div>
                            </div>
                            <hr class="xsp-dragbar-hframe" style="width: 100%;" />
                            <%Html.RenderPartial("PatientList", Model.Patients); %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="abs window_main xsp-pane" style="overflow: auto;">
            <div id="patientScheduleView" style="height: auto;">
                <div id="scheduleTop">
                    <%Html.RenderPartial("Calendar", Model.Episode); %>
                </div>
                <div class="row" style="min-width: 800px;">
                    <% Html.Telerik().TabStrip()
                                     .Name("ScheduleTabStrip")
                                     .ClientEvents(events => events
                                         .OnSelect("Schedule.OnSelect"))
                                .Items(tabstrip =>
                                     {
                                         tabstrip.Add()
                                             .Text("Nursing")
                                             .HtmlAttributes(new { id = "nursingTab" })
                                            .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="nursingScheduleTable" data="Nursing" class="scheduleTables" border="0"
                            cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#nursingScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                             .Text("PT")
                                              .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="PTScheduleTable" data="PT" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#PTScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                             .Text("OT")
                                             .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="OTScheduleTable" data="OT" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OTScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                             .Text("ST")
                                             .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="STScheduleTable" data="ST" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#STScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                             .Text("HHA")
                                             .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="HHAScheduleTable" data="HHA" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#HHAScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                          .Text("MSW")
                                          .Content(() =>
                                          {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="MSWScheduleTable" data="MSW" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#MSWScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});

                                         tabstrip.Add()
                                             .Text("Orders")
                                              .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="OrdersScheduleTable" data="Orders" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OrdersScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});   tabstrip.Add()
                                             .Text("Claim")
                                              .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("PatientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="ClaimScheduleTable" data="Claim" class="scheduleTables" border="0" cellpadding="0"
                            cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="Schedule.CloseNewEvent($(this));" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick="Schedule.ScheduleInputFix($(this),'Patient','#ClaimScheduleTable'); Schedule.FormSubmit($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                         tabstrip.Add()
                                             .Text("Add Multiple Schedule")
                                              .Content(() =>
                                             {%>
                    <% using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post, new { @id = " ", }))%>
                    <%  { %>
                    <%=Html.Hidden("patientId", "", new { @id = "" })%>
                    <div class="row485" style="float: none">
                        <table id="multipleScheduleTable" data="Multiple" class="scheduleTables" border="0"
                            cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>
                                        Discipline
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="">
                                        <select name="DisciplineTask" value="" class="MultipleDisciplineTask">
                                            <option value="0">Select Discipline</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="userId" class="Users">
                                        </select>
                                    </td>
                                    <td>
                                        <%=Html.Telerik().DatePicker().Name("StartDate").Value(DateTime.Today) %>
                                        &nbsp;&nbsp; To &nbsp;&nbsp;
                                        <%=Html.Telerik().DatePicker().Name("EndDate").Value(DateTime.Today)%>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right">
                                        <ul>
                                            <li style="float: right">
                                                <input type="button" value="Cancel" class="SaveContinue" onclick="" /></li>
                                            <li style="float: right">
                                                <input type="button" value="Save" onclick=" Schedule.FormSubmitMultiple($(this));" /></li>
                                            <li>
                                                <input type="hidden" name="episodeId" value="" class="scheduleValue" />
                                                <input type="hidden" name="Discipline" value="" /></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%} %>
                    <%});
                                     })
                                     .SelectedIndex(0)
                                     .Render();    
                    %>
                </div>
                <div id="scheduleBottomPanel" class='row' style="min-width: 800px; height: 100%;">
                    <%
                        Html.RenderPartial("Activities", new ScheduleActivityArgument { EpisodeId=Model.Episode.Id,PatientId=Model.Episode.PatientId,Discpline="Nursing"}); 
                    %>
                </div>
            </div>
        </div>
        <div class='abs xsp-dragbar xsp-dragbar-vframe '>
            &nbsp;</div>
    </div>
</div>
