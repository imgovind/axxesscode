﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Patient>>" %>
<div id="SchedulePatientSelectionGridContainer" class="sideBottom">
    <%Html.Telerik().Grid(Model)
                                                .Name("SchedulePatientSelectionGrid")
                                                .Columns(columns =>
                                                {

                                                    columns.Bound(p => p.LastName);
                                                    columns.Bound(p => p.FirstName);
                                                    columns.Bound(p => p.PatientIdNumber).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                    columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);

                                                })
                                                .RowAction(r =>
                                                {
                                                    if (r.Index == 0)
                                                    {
                                                        r.Selected = true;

                                                    }
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { statusId = 0, paymentSourceId = 0, name = string.Empty }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false)
                                                .ClientEvents(events => events.OnDataBound(
                                                    () =>
                                                    {%>
    function () { if($('#SchedulePatientSelectionGrid .t-grid-content tbody tr').length ==0){
    Schedule.NoPatientBind('<%=Guid.Empty %>'); } else { $('#SchedulePatientSelectionGrid .t-grid-content tbody tr:has(td):first').click(); } }
    <% }
                                                ).OnRowSelected("Schedule.OnPatientRowSelected")
                                                    .OnLoad("Schedule.CalculateGridHeight()")).Render();                                                
                                                                                                
    %>
</div>
