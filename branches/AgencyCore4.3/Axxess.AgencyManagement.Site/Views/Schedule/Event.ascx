﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="event">
    <div id="event_edit_container">
        <form>
        <input type="hidden" />
        <ul>
            <li><span>Date: </span><span id="date_holder"></span></li>
            <li>
                <label for="start">
                    Employee:
                </label>
                <select name="start" class="Employees">
                    <option value="0">Select Employee</option>
                </select>
            </li>
            <li>
                <label for="end">
                    Discipline:
                </label>
                <select name="end">
                    <option value="0">Select Discipline</option>
                    <option value="SN">SN</option>
                    <option value="HHA">HHA</option>
                    <option value="PT">PT</option>
                    <option value="ST">ST</option>
                    <option value="OT">OT</option>
                    <option value="MSW">MSW</option>
                </select>
            </li>
            <li>
                <label for="title">
                    Title:
                </label>
                <input type="text" name="title" />
            </li>
            <li>
                <label for="body">
                    Body:
                </label>
                <textarea name="body"></textarea>
            </li>
        </ul>
        </form>
    </div>
</div>
