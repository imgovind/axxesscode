﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="BillingPatientSelectionGridContainer" class="sideBottom">
    <%Html.Telerik().Grid<Patient>()
                                                .Name("BillingSelectionGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(p => p.LastName);
                                                    columns.Bound(p => p.FirstName);
                                                    columns.Bound(p => p.PatientIdNumber).Title("Patient #").HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                    columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { statusId = 0, paymentSourceId = 0, name = string.Empty }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false)
                                                .ClientEvents(events => events.OnDataBound(
                                                    () =>
                                                    {%>
    function () { if($('#BillingPatientSelectionGridContainer .t-grid-content tbody tr').length ==0){
    Billing.NoPatientBind('<%=Guid.Empty %>'); } else { $('#BillingPatientSelectionGridContainer .t-grid-content tbody tr:has(td):first').click(); } }
    <% }
                                                )
                                                    .OnRowSelected("Billing.OnPatientRowSelected").OnLoad("Billing.CalculateGridHeight()")).Render();                                                
                                                                                                
    %>
</div>