﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("InfoVerify", "Billing", FormMethod.Post, new { @id = "billingInfo" }))%>
<%  { %>
<fieldset style="height: 100%;">
    <div style="width: 48%; display: block; margin: 0px; padding: 0px;" class="window_aside">
        <div style="display: block; width: 100%;">
            <%=Html.Hidden("Id",Model.Id) %>
            <div style="vertical-align: middle;" class="row">
                <label for="FirstName">
                    Patient First Name:&nbsp;&nbsp;&nbsp;</label>
                <span class="input_wrapper">
                    <%=
                        Html.TextBox("FirstName", Model.FirstName, new { @class = "text {validate:{required:true}}", @maxlength = "20", @tabindex = "1" })
                    %>
                </span>
            </div>
            <div class="row">
                <label for="LastName">
                    Patient Last Name:&nbsp;&nbsp;&nbsp;</label>
                <span class="input_wrapper">
                    <%=Html.TextBox("LastName",Model.LastName, new { @class = "text required", @maxlength = "20", @tabindex = "3" })%>
                </span>
            </div>
            <div class="row">
                <label for="MedicareNumber">
                    Medicare #:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @class = "text required", @maxlength = "11", @tabindex = "12" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="PatientIdNumber">
                    Patient Record #:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text required", @maxlength = "11", @tabindex = "12" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="Gender">
                    Gender (M0069):</label>
                <div class="inputs">
                    <ul class="inline">
                        <li>
                            <%=Html.RadioButton("Gender", "Female",  Model.Gender == "Female" ? true : false, new { @id = "" })%>Female
                            <%=Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "" })%>Male
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <label for="DOB">
                    Date of Birth (M0066):</label>
                <div class="inputs">
                    <%= Html.Telerik().DatePicker().Name("DOB").Value(Model.DOB).HtmlAttributes(new { @class = "text required date required", @tabindex = "16" }) %>
                </div>
            </div>
            <div class="row">
                <label for="EpisodeStartDate">
                    Episode Start Date:</label>
                <div class="inputs">
                    <%= Html.Telerik().DatePicker().Name("EpisodeStartDate").Value(Model.EpisodeStartDate).HtmlAttributes(new { @class = "text required date required", @tabindex = "17" }) %>
                </div>
            </div>
            <div class="row">
                <label for="StartOfCareDate">
                    Admission Date:</label>
                <div class="inputs">
                    <%= Html.Telerik().DatePicker().Name("StartOfCareDate").Value(Model.StartofCareDate).HtmlAttributes(new {  @class = "text required date required", @tabindex = "17" }) %>
                </div>
            </div>
            <div class="row">
                <label for="AddressLine1">
                    Address Line 1:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("AddressLine1",Model.AddressLine1 , new { @class = "text required", @tabindex = "4" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="AddressLine2">
                    Address Line 2:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text", tabindex = "5" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="AddressCity">
                    City:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required",@tabindex="6" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="AddressStateCode">
                    State, Zip Code :</label>
                <div class="inputs">
                    <%=Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new {@style="width: 119px;",@class="input_wrapper",@tabindex="7" }) %>
                    ,
                    <%=Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip", @size = "5", @maxlength = "5", @style = "margin: 0px; padding: 0px; width: 54px;" })%>
                </div>
            </div>
        </div>
    </div>
    <div style="margin: 0px; width: 47%; float: left; padding-top: 10px; display: table;"
        class="window_main">
        <div style="display: block; width: 100%;">
            <div class="row">
                <label for="HippsCode">
                    HIPPS Code:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("HippsCode", Model.HippsCode, new { @class = "text required", @maxlength = "5", @tabindex = "5", @readonly = "readonly" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="ClaimKey">
                    Oasis Matching Key:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "text required", @maxlength = "18", @tabindex = "18", @readonly = "readonly" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <label for="FirstBillableVisitDate">
                    Date Of First Billable Visit:</label>
                <div class="inputs">
                <% if (Model.FirstBillableVisitDate != null && Model.FirstBillableVisitDate > DateTime.MaxValue)
                   {%>
                    <%= Html.Telerik().DatePicker().Name("FirstBillableVisitDate").Value(Model.FirstBillableVisitDate).HtmlAttributes(new { @class = "text required date", @tabindex = "17" })%>
                    <%}
                   else
                   { %>
                   <%= Html.Telerik().DatePicker().Name("FirstBillableVisitDate").HtmlAttributes(new { @class = "text required date", @tabindex = "17" })%>
                    <%} %>
                    
                </div>
            </div>
            <div class="row">
                <label for="PhysicianLastName">
                    Physician Last Name, F.I.:</label>
                <div class="inputs">
                    <span class="input_wrappermultible">
                        <%=Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20", @tabindex = "11", @size = "3", @style = "margin: 0px; padding: 0px; width: 115px;" })%></span>-<span
                            class="input_wrappermultible">
                            <%=Html.TextBox("PhysicianNPI", Model.PhysicianFirstName != null && Model.PhysicianFirstName != "" ? Model.PhysicianFirstName.Substring(0,1) : "", new { @class = "text required", @maxlength = "4", @tabindex = "11", @style = "margin: 0px; padding: 0px; width: 49px;" })%></span>
                </div>
            </div>
            <div class="row">
                <label for="PhysicianNPI">
                    Physician NPI #:</label>
                <div class="inputs">
                    <span class="input_wrapper">
                        <%=Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "10", @tabindex = "11" })%>
                    </span>
                </div>
            </div>
            <div class="row">
                <% var diganosis = XElement.Parse(Model.DiagonasisCode);              
                %>
                Diagonasis Codes:-
                <div style="width: 335px;">
                    <div class="row">
                        <div style="width: 33.33%; float: left;">
                            Primary
                            <%=Html.TextBox("Primary", diganosis!=null && diganosis.Element("code1")!=null?diganosis.Element("code1").Value:"" )%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Second
                            <%=Html.TextBox("Second", diganosis != null && diganosis.Element("code2") != null ? diganosis.Element("code2").Value : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Third
                            <%=Html.TextBox("Third", diganosis != null && diganosis.Element("code3") != null ? diganosis.Element("code3").Value : "")%>
                        </div>
                    </div>
                    <div class="row">
                        <div style="width: 33.33%; float: left;">
                            Fourth
                            <%=Html.TextBox("Fourth", diganosis != null && diganosis.Element("code4") != null ? diganosis.Element("code4").Value : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Fifth
                            <%=Html.TextBox("Fifth", diganosis != null && diganosis.Element("code5") != null ? diganosis.Element("code5").Value : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Sixth
                            <%=Html.TextBox("Sixth", diganosis != null && diganosis.Element("code6") != null ? diganosis.Element("code6").Value : "")%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                Remark:
                <div style="width: 335px;">
                    <%=Html.TextArea("FinalRemark", Model.Remark, new { @style = "width: 100%;" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div style="text-align: center; vertical-align: middle; width: 100%;" class="buttons">
            <ul style="margin: 0px; width: 100%; text-align: center;">
                <li><span class="button send_form_btn"><span><span>Verify and Next</span></span><input
                    type="submit" name="" id=" " onclick="Billing.Navigate(1,'#billingInfo');" /></span></li>
            </ul>
        </div>
    </div>
</fieldset>
<%} %>