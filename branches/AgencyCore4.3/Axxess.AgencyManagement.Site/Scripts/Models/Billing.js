﻿var Billing = {
    Init: function() {
        jQuery.event.add(window, "load", function() {
            Billing.CalculateGridHeight();
            Billing.CalculateActivityGrid();
        });
        jQuery.event.add(window, "resize", function() {
            Billing.CalculateGridHeight();
            Billing.CalculateActivityGrid();
        });
    },

    CalculateGridHeight: function() {
        setTimeout(function() {
            $('#billing_Splitter1').children().height($('#billing_Splitter1').height() + 4);
            var gridContentHeight = $('#billingLeftSide').height() - $('#billingFillterContainer').height() - 25 - 20 + 7;

            if ($.browser.msie) {
                $('#BillingPatientSelectionGridContainer').find(".t-grid-content").height(gridContentHeight);
            }
            else {
                $('#BillingPatientSelectionGridContainer').find(".t-grid-content").height(gridContentHeight);
            }

        }, 500);
    },
    CalculateActivityGrid: function() {
        setTimeout(function() {
            var activityGridRow = $('#scheduleBottomPanel').height();
            if ($.browser.msie) {
                $('#ScheduleActivityGrid').find(".t-grid-content").height(activityGridRow - 335);
            }
            else {
                $('#ScheduleActivityGrid').find(".t-grid-content").height(activityGridRow - 333);
            }
        }, 500);
    }
    ,
    loadBillingCenter: function() {
        $("#billingCenterResult").load('Billing/Center', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#billingCenterResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#Billing_Center');
            }
            else if (textStatus == "success") {
                JQD.open_window('#Billing_Center');

            }
        });
    },
    loadRap: function(EpisodeId, PatientId) {
        $("#rapResult").load('Billing/Rap', { episodeId: EpisodeId, patientId: PatientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#rapResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#rap');
            }
            else if (textStatus == "success") {
                JQD.open_window('#rap');
                $("#rapVerification").validate({
                    submitHandler: function(form) {
                        var options = {
                            dataType: 'json',
                            beforeSubmit: function(values, form, options) {
                            },
                            success: function(result) {

                                alert(result.errorMessage);
                            }
                        };
                        $(form).ajaxSubmit(options);
                        return false;
                    }
                });
            }
        });
    }
    ,
    loadFinal: function(EpisodeId, PatientId) {
        $("#finalResult").load('Billing/Final', { episodeId: EpisodeId, patientId: PatientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#finalResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#final');
            }
            else if (textStatus == "success") {
                JQD.open_window('#final');

            }
        });
    },
    loadBillingHistory: function(EpisodeId, PatientId) {
        $("#billingWindowResult").load('Billing/History', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#billingWindowResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#Billing_window');
            }
            else if (textStatus == "success") {
                JQD.open_window('#Billing_window');
                var s4;
                jQuery.event.add(window, "resize", win_onresize);
                var cw = xClientWidth();
                var w = $(".window").width() - 2;
                var h = $(".window").height() - 55.5;
                s4 = new xSplitter('billing_Splitter1', 0, 0, w, h, true, 6, 200, 200, h / 5, true, 2, null, 0);
                function win_onresize() {
                    var cw = xClientWidth();
                    var w = $(".window").width() - 2;
                    var h = $(".window").height() - 55.5;
                    s4.paint(w, h, 200, 200, w / 4);
                }
            }
        });
    }
    , Navigate: function(index, id) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            var tabstrip = $("#FinalTabStrip").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                        }
                        else {
                            alert("")
                        }
                    }
                    , error: function() {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

    }
    ,
    NavigateBack: function(index) {
        var tabstrip = $("#FinalTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    loadRap: function(EpisodeId, PatientId) {
        $("#rapResult").load('Billing/Rap', { episodeId: EpisodeId, patientId: PatientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#rapResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#rap');
            }
            else if (textStatus == "success") {
                JQD.open_window('#rap');
                $("#rapVerification").validate({
                    submitHandler: function(form) {
                        var options = {
                            dataType: 'json',
                            beforeSubmit: function(values, form, options) {
                            },
                            success: function(result) {

                                alert(result.errorMessage);
                            }
                        };
                        $(form).ajaxSubmit(options);
                        return false;
                    }
                });
            }
        });
    }
    ,
    loadGenerate: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        $("#cliamSummaryResult").load('Billing/ClaimSummary', fields, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#cliamSummaryResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#cliamSummary');
            }
            else if (textStatus == "success") {
                JQD.open_window('#cliamSummary');


            }
        });
    },
    loadPendingClaims: function() {
        $("#pendingClaimsResult").load('Billing/PendingClaims', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#pendingClaimsResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#pendingClaims');
            }
            else if (textStatus == "success") {
                JQD.open_window('#pendingClaims');


            }
        });
    },
    UpdateStatus: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.postUrl('Billing/UpdateStatus', fields, null, null);
    },
    OnPatientRowSelected: function(e) {
        var patientId = e.row.cells[3].innerHTML;

        Billing.RebindActivity(patientId);
    },
    RebindActivity: function(id) {
        var ActivityGrid = $('#BillingHistoryActivityGrid').data('tGrid');
        ActivityGrid.rebind({ patientId: id });
    },
    NoPatientBind: function(id) {
        Billing.RebindActivity(id);
    }
}