﻿namespace Axxess.Api
{
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class GrouperAgent : BaseAgent<IGrouperService>
    {
        #region Overrides

        public override string ToString()
        {
            return "GrouperService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Grouper Methods

        public Hipps GetHippsCode(string oasisDataString)
        {
            Hipps hipps = null;
            BaseAgent<IGrouperService>.Call(g => hipps = g.GetHippsCode(oasisDataString), this.ToString());
            return hipps;
        }

        #endregion
    }
}
