﻿namespace Axxess.Api.Server
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class GrouperService : MarshalByRefObject, IGrouperService
    {
        #region IGrouperService Members

        public Hipps GetHippsCode(string oasisDataString)
        {
            var hipps = new Hipps();

            try
            {
                string hippsCode = "00000";
                string claimKey = "000000000000000000";
                string version = "00000";
                string invflag = "0";

                Console.WriteLine("Calling Grouper");
                Grouper.GetHippsCode(ref hippsCode, ref oasisDataString, ref claimKey, ref version, ref invflag);

                hipps.Code = hippsCode;
                hipps.ClaimMatchingKey = claimKey;
                hipps.Version = version;
                hipps.InvFlag = invflag;

                Console.WriteLine(hipps.ToString());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return hipps;
        }

        #endregion

        #region IService Members

        public bool Ping()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}