﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface ICachedDataRepository
    {
        List<OasisGuide> GetOasisGuides();
        OasisGuide GetOasisGuide(string mooCode);
        List<SubmissionBodyFormat> GetSubmissionFormatInstructions();
        List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions();
        List<SubmissionFooterFormat> GetSubmissionFooterFormatInstructions();
        List<SubmissionInactiveBodyFormat> GetSubmissionInactiveBodyFormatInstructions();
    }
}
