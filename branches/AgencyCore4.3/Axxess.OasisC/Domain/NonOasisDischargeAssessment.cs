﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class NonOasisDischargeAssessment : Assessment
    {
        public NonOasisDischargeAssessment()
        {
            this.Type = AssessmentType.NonOasisDischarge.ToString();
        }
    }
}
