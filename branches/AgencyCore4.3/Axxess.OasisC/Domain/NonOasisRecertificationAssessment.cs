﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class NonOasisRecertificationAssessment : Assessment
    {
        public NonOasisRecertificationAssessment()
        {
            this.Type = AssessmentType.NonOasisRecertification.ToString();
        }
    }
}
