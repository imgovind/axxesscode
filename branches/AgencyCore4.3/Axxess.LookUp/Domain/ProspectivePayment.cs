﻿namespace Axxess.LookUp.Domain
{
    using System;

    public class ProspectivePayment
    {
        public string Hhrg { get; set; }
        public string Weight { get; set; }
        public string CbsaCode { get; set; }
        public string WageIndex { get; set; }
        public string LaborAmount { get; set; }
        public string NonLaborAmount { get; set; }
        public string NonRoutineSuppliesAmount { get; set; }
        public string TotalProspectiveAmount { get; set; }
        public string TotalAmountWithoutSupplies { get; set; }
        public string HippsCode { get; set; }
        public string OasisMatchingKey { get; set; }
        public string ClaimAmount { get; set; }
        public double TotalAmount { get; set; }
        public double CalculateRapClaimAmount(bool isStartofCare)
        {
            var claimAmount = 0.5 * TotalAmount;
            if (isStartofCare)
            {
                claimAmount =  0.6 * TotalAmount;
            }
            this.ClaimAmount = string.Format("${0:#0.00}", claimAmount);
            return Math.Round(claimAmount, 2);
        }

        public double CalculateFinalClaimAmount(bool isStartofCare)
        {
            var claimAmount = 0.5 * TotalAmount;
            if (isStartofCare)
            {
                claimAmount = 0.4 * TotalAmount;
            }
            this.ClaimAmount = string.Format("${0:#0.00}", claimAmount);
            return Math.Round(claimAmount, 2);
        }
    }
}
