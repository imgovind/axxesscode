﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class EthnicRace
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
