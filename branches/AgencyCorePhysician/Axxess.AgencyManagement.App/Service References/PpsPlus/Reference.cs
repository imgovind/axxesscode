﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5456
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Axxess.AgencyManagement.App.PpsPlus {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ppsplus.com/webservices", ConfigurationName="PpsPlus.PPSPlusServiceSoap")]
    public interface PPSPlusServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ppsplus.com/webservices/ValidatePECOS", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Axxess.AgencyManagement.App.PpsPlus.ValidatePECOSResult ValidatePECOS(string SerialNumber, string[] NPIs);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ppsplus.com/webservices/ValidatePECOSXML", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Axxess.AgencyManagement.App.PpsPlus.ValidatePECOSResultXML ValidatePECOSXML(string SerialNumber, string[] NPIs);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ppsplus.com/webservices/UpdateImportKey", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Axxess.AgencyManagement.App.PpsPlus.Result UpdateImportKey(string password, string myOldKey, string myNewKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ppsplus.com/webservices/SendAssessments", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Axxess.AgencyManagement.App.PpsPlus.Result SendAssessments(string myKey, string myData);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ppsplus.com/webservices/RetrieveAssessments", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Axxess.AgencyManagement.App.PpsPlus.RetrieveAssessmentResult RetrieveAssessments(string Password, string myKey);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ppsplus.com/webservices")]
    public partial class ValidatePECOSResult : Result {
        
        private System.Data.DataTable pECOSUnapprovedField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public System.Data.DataTable PECOSUnapproved {
            get {
                return this.pECOSUnapprovedField;
            }
            set {
                this.pECOSUnapprovedField = value;
                this.RaisePropertyChanged("PECOSUnapproved");
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RetrieveAssessmentResult))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidatePECOSResultXML))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidatePECOSResult))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ppsplus.com/webservices")]
    public partial class Result : object, System.ComponentModel.INotifyPropertyChanged {
        
        private ResultCodes resultCodeField;
        
        private string errorMessageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public ResultCodes ResultCode {
            get {
                return this.resultCodeField;
            }
            set {
                this.resultCodeField = value;
                this.RaisePropertyChanged("ResultCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string ErrorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                this.errorMessageField = value;
                this.RaisePropertyChanged("ErrorMessage");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ppsplus.com/webservices")]
    public enum ResultCodes {
        
        /// <remarks/>
        Success,
        
        /// <remarks/>
        Failure,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ppsplus.com/webservices")]
    public partial class RetrieveAssessmentResult : Result {
        
        private System.Data.DataSet[] assessmentDatasetsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        [System.Xml.Serialization.XmlArrayItemAttribute("DataSet")]
        public System.Data.DataSet[] AssessmentDatasets {
            get {
                return this.assessmentDatasetsField;
            }
            set {
                this.assessmentDatasetsField = value;
                this.RaisePropertyChanged("AssessmentDatasets");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ppsplus.com/webservices")]
    public partial class ValidatePECOSResultXML : Result {
        
        private string pECOSResultsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string PECOSResults {
            get {
                return this.pECOSResultsField;
            }
            set {
                this.pECOSResultsField = value;
                this.RaisePropertyChanged("PECOSResults");
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public interface PPSPlusServiceSoapChannel : Axxess.AgencyManagement.App.PpsPlus.PPSPlusServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public partial class PPSPlusServiceSoapClient : System.ServiceModel.ClientBase<Axxess.AgencyManagement.App.PpsPlus.PPSPlusServiceSoap>, Axxess.AgencyManagement.App.PpsPlus.PPSPlusServiceSoap {
        
        public PPSPlusServiceSoapClient() {
        }
        
        public PPSPlusServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PPSPlusServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PPSPlusServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PPSPlusServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Axxess.AgencyManagement.App.PpsPlus.ValidatePECOSResult ValidatePECOS(string SerialNumber, string[] NPIs) {
            return base.Channel.ValidatePECOS(SerialNumber, NPIs);
        }
        
        public Axxess.AgencyManagement.App.PpsPlus.ValidatePECOSResultXML ValidatePECOSXML(string SerialNumber, string[] NPIs) {
            return base.Channel.ValidatePECOSXML(SerialNumber, NPIs);
        }
        
        public Axxess.AgencyManagement.App.PpsPlus.Result UpdateImportKey(string password, string myOldKey, string myNewKey) {
            return base.Channel.UpdateImportKey(password, myOldKey, myNewKey);
        }
        
        public Axxess.AgencyManagement.App.PpsPlus.Result SendAssessments(string myKey, string myData) {
            return base.Channel.SendAssessments(myKey, myData);
        }
        
        public Axxess.AgencyManagement.App.PpsPlus.RetrieveAssessmentResult RetrieveAssessments(string Password, string myKey) {
            return base.Channel.RetrieveAssessments(Password, myKey);
        }
    }
}
