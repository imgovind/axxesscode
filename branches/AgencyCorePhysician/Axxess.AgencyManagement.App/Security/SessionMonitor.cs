﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    public sealed class SessionMonitor
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly SessionMonitor instance = new SessionMonitor();
        }

        #endregion

        #region Private Members

        private SafeDictionary<Guid, string> sessions;

        #endregion

        #region Public Instance

        public static SessionMonitor Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private SessionMonitor()
        {
            sessions = new SafeDictionary<Guid, string>();
        }

        #endregion

        #region Public Methods

        public void Set(Guid loginId, string sessionId)
        {
            if (this.sessions.ContainsKey(loginId))
            {
                this.sessions[loginId] = sessionId;
            }
            else
            {
                this.sessions.Add(loginId, sessionId);
            }
        }

        public void Expire(Guid loginId)
        {
            this.sessions.Remove(loginId);
        }

        public int Count
        {
            get
            {
                return sessions.Count;
            }
        }

        #endregion
    }
}
