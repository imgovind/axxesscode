﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web.Mvc;
    using System.Threading;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class AxxessAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers.Count > 0)
            {
                var userId = filterContext.HttpContext.Request.Headers.Get("UserId").ToGuid();
                var agencyId = filterContext.HttpContext.Request.Headers.Get("AgencyId").ToGuid();

                if (userId.IsNotEmpty() && agencyId.IsNotEmpty())
                {
                    IMembershipService membershipService = Container.Resolve<IMembershipService>();
                    AxxessPrincipal principal = membershipService.Get(userId, agencyId, true);
                    if (principal != null)
                    {
                        Thread.CurrentPrincipal = principal;
                        filterContext.HttpContext.User = principal;
                    }
                }
                base.OnAuthorization(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.End();
            }

            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
