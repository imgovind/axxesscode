﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.App.Domain;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class PayrollSummaryViewData
    {
       public List<VisitSummary> VisitSummary { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public string PayrollStatus { get; set; }
    }
}
