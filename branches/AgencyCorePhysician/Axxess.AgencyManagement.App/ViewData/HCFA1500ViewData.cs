﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public class HCFA1500ViewData : UBOFourViewData {
        public string PatientMaritalStatus { get; set; }
        public string PatientTelephoneNum { get; set; }
        public string PatientIdNumber { get; set; }
        public double AmountDue { get; set; }
        public double AmountPaid { get; set; }
        public List<ManagedClaimPayment> ManagedClaimPayments { get;set; }
    }
}
