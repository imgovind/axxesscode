﻿
namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Domain;

   public class NewClaimViewData
    {
       public List<ClaimEpisode> EpisodeData { get; set; }
       public string Type { get; set; }
       public Guid PatientId { get; set; }
    }
}
