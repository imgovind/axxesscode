﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App
{
    public class NewBillDataViewData
    {
        public Guid Id { get; set; }
        public bool IsMedicareHMO { get; set; }
        public string TypeOfClaim { get; set; }
    }
}
