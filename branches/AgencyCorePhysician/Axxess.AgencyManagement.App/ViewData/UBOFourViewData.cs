﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public class UBOFourViewData
    {
        public Agency Agency { get; set; }
        public AgencyLocation AgencyLocation { get; set; }
        public ClaimViewData Claim { get; set; }
        public bool PrintContentOnly { get; set; }
    }
}
