﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class FilterViewData
    {
       public List<SelectListItem> Insurances { get; set; }
       public List<SelectListItem> Branches { get; set; }
       public string SelecetdInsurance { get; set; }
       public Guid SelecetdBranch { get; set; }
    }
}
