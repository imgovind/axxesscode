﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Enums;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App
{
    public class ClaimRemittanceViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public DateTime RemitDate { get; set; }
        public string RemitId { get; set; }
        public double TotalAdjustmentAmount { get; set; }
        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillVisitDatas { get; set; }
    }
}
