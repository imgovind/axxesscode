﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;
    using System.Reflection;
    using Axxess.AgencyManagement;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    public class ReportHelper
    {
        public string GetInsuranceName(Guid agencyId, int insuranceId)
        {
            string result = default(string);
            if (insuranceId != null && insuranceId != default(int))
            {
                result = InsuranceEngine.Instance.Get(insuranceId, agencyId).Name;
            }
            else
            {
                result = "All Insurances";
            }
            return result;
        }

        public string GetLocationName(Guid agencyId, Guid locationId)
        {
            string result = default(string);
            if (locationId != null && locationId.IsNotEmpty())
            {
                result = LocationEngine.GetName(agencyId, locationId);
            }
            else
            {
                result = "All Branches";
            }
            return result;
        }

        public string GetUserName(Guid agencyId, Guid UserId)
        {
            string result = default(string);
            if (UserId != null && UserId.IsNotEmpty())
            {
                result = UserEngine.GetName(UserId,agencyId);
            }
            else
            {
                result = "";
            }
            return result;
        }

        public string GetShortDateFromDate(DateTime? Date)
        {
            string result = default(string);
            if (Date != null)
            {
                DateTime Temp = (DateTime)Date;
                result = Temp.ToShortDateString();
            }
            return result;
        }

        public string GetShortDateFromDate(DateTime Date)
        {
            string result = default(string);
            if (Date != null)
            {
                result = Date.ToShortDateString();
            }
            return result;
        }

        public string GetPhysicianName(Guid agencyId, Guid PhysicianId)
        {
            return PhysicianEngine.GetName(PhysicianId, agencyId);
        }



        public string GetPatientStatus(int StatusCode)
        {
            string result = default(string);
            PatientStatus patientStatus = (PatientStatus)StatusCode;
            switch (patientStatus)
            {
                case PatientStatus.Active:
                    result = "Active";
                    break;
                case PatientStatus.Deprecated:
                    result = "Deprecated";
                    break;
                case PatientStatus.Discharged:
                    result = "Discharged";
                    break;
                case PatientStatus.NonAdmission:
                    result = "NonAdmission";
                    break;
                case PatientStatus.Pending:
                    result = "Pending";
                    break;
                default:
                    result = "All";
                    break;
            }
            return result;
        }

        public string GetEffectiveDate()
        {
            return DateTime.Now.ToShortDateString();
        }

        public string GetMonthFromDate(int Month)
        {
            if (Month != default(int))
            {
                return string.Format(new DateTime(DateTime.Now.Year, Month, 1).ToString("MMMM"));
            }
            else
            {
                return string.Empty;
            }
        }

        public Dictionary<string, ExcelColumnModel> AddColumn(Dictionary<string, ExcelColumnModel> inDictionary, string Column)
        {
            if (inDictionary != null && Column.IsNotNullOrEmpty())
            {
                inDictionary.Add(
                    Column,
                    new ExcelColumnModel
                    {
                        Type = string.Empty.GetType(),
                        ColumnName = Column
                    }
                );
            }
            return inDictionary;
        }

        public Dictionary<string, ExcelColumnModel> AddColumn(Dictionary<string, ExcelColumnModel> inDictionary, string Column, string customColumnName)
        {
            if (inDictionary != null && Column.IsNotNullOrEmpty())
            {
                inDictionary.Add(
                    Column,
                    new ExcelColumnModel
                    {
                        Type = string.Empty.GetType(),
                        ColumnName = customColumnName.IsNotNullOrEmpty() ? customColumnName : Column
                    }
                );
            }
            return inDictionary;
        }

        public Dictionary<string, ExcelColumnModel> AddColumn(Dictionary<string, ExcelColumnModel> inDictionary, string Column, Type type)
        {
            if (inDictionary != null && Column.IsNotNullOrEmpty())
            {
                inDictionary.Add(
                    Column,
                    new ExcelColumnModel
                    {
                        Type = type,
                        ColumnName = Column
                    }
                );
            }
            return inDictionary;
        }

        public Dictionary<string, ExcelColumnModel> AddColumn(Dictionary<string, ExcelColumnModel> inDictionary, string Column, string customColumnName, Type type)
        {
            if (inDictionary != null && Column.IsNotNullOrEmpty())
            {
                inDictionary.Add(
                    Column,
                    new ExcelColumnModel
                    {
                        Type = type,
                        ColumnName = customColumnName.IsNotNullOrEmpty() ? customColumnName : Column
                    }
                );
            }
            return inDictionary;
        }
    }

    public class ReportHelper<T> : ReportHelper
    {
        private PropertyInfo[] Properties { get; set; }
        private Dictionary<string, PropertyInfo> PropertyDictionary { get; set; }
        private List<T> Data { get; set; }
        private bool CanConvertListToDictionary { get; set; }

        public ReportHelper(T data)
        {
            this.Properties = data.GetType().GetProperties();
            this.PropertyDictionary = new Dictionary<string, PropertyInfo>();
            foreach (var property in this.Properties)
            {
                this.PropertyDictionary.Add(property.Name, property);
            }
            this.CanConvertListToDictionary = false;
        }

        public ReportHelper(List<T> data)
        {
            this.Data = data;
            this.Properties = data.FirstOrDefault().GetType().GetProperties();
            this.PropertyDictionary = new Dictionary<string, PropertyInfo>();
            foreach (var property in this.Properties)
            {
                this.PropertyDictionary.Add(property.Name, property);
            }
            this.CanConvertListToDictionary = true;
        }

        public void AddExcelProperty<T>(Dictionary<string, ExcelColumnModel> inDictionary, Expression<Func<T>> expression)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            inDictionary.Add(
                body.Member.Name,
                new ExcelColumnModel
                {
                    Type = this.PropertyDictionary[body.Member.Name].PropertyType,
                    ColumnName = body.Member.Name,
                    Property = this.PropertyDictionary[body.Member.Name]
                }
            );
        }

        public Dictionary<string, ExcelColumnModel> AddExcelProperty<T>(Dictionary<string, ExcelColumnModel> inDictionary, Expression<Func<T>> expression, string CustomColumnName)
        {
            if (inDictionary != null && expression != null)
            {
                MemberExpression body = (MemberExpression)expression.Body;
                inDictionary.Add(
                    body.Member.Name,
                    new ExcelColumnModel
                    {
                        Type = this.PropertyDictionary[body.Member.Name].PropertyType,
                        ColumnName = CustomColumnName.IsNotNullOrEmpty() ? CustomColumnName : body.Member.Name,
                        Property = this.PropertyDictionary[body.Member.Name]
                    }
                );
            }
            return inDictionary;
        }

        public Dictionary<string, ExcelColumnModel> AddExcelProperty<T>(Dictionary<string, ExcelColumnModel> inDictionary, Expression<Func<T>> expression, Type customType)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            inDictionary.Add(
                body.Member.Name,
                new ExcelColumnModel
                {
                    Type = customType,
                    ColumnName = body.Member.Name,
                    Property = this.PropertyDictionary[body.Member.Name]
                }
            );
            return inDictionary;
        }

        public Dictionary<string, ExcelColumnModel> AddExcelProperty<T>(Dictionary<string, ExcelColumnModel> inDictionary, Expression<Func<T>> expression, string CustomColumnName, Type customType)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            inDictionary.Add(
                body.Member.Name,
                new ExcelColumnModel
                {
                    Type = customType,
                    ColumnName = CustomColumnName,
                    Property = this.PropertyDictionary[body.Member.Name]
                }
            );
            return inDictionary;
        }

        public string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            return body.Member.Name;
        }

        public List<Dictionary<string, string>> ConvertListToDictionary<T>(List<T> data)
        {
            List<Dictionary<string, string>> result = null;
            if (data != null && data.Count > 0)
            {
                result = new List<Dictionary<string, string>>();
                Dictionary<string, string> row = null;
                Type valueType = null;
                foreach (var item in data)
                {
                    row = new Dictionary<string, string>();
                    foreach (var property in this.Properties)
                    {
                        var value = property.GetValue(item, null);
                        valueType = value.GetType();
                        if (valueType == DateTime.MinValue.GetType())
                        {
                            if (!DateTime.Equals((DateTime)value, DateTime.MinValue))
                            {
                                DateTime tempDate = (DateTime)value;
                                row.Add(property.Name, tempDate.ToShortDateString());
                            }
                            else
                            {
                                row.Add(property.Name, string.Empty);
                            }
                        }
                        else if (valueType == default(int).GetType())
                        {
                            if (!Int32.Equals(value, default(int)))
                            {
                                int tempInt = (int)value;
                                row.Add(property.Name, tempInt.ToString());
                            }
                            else
                            {
                                row.Add(property.Name, string.Empty);
                            }
                        }
                        else if (valueType == default(bool).GetType())
                        {
                            if (!System.Boolean.Equals(value, default(bool)))
                            {
                                bool tempBool = (bool)value;
                                row.Add(property.Name, tempBool.ToString());
                            }
                            else
                            {
                                row.Add(property.Name, string.Empty);
                            }
                        }
                        else if (valueType == string.Empty.GetType())
                        {
                            if (!String.Equals(value, string.Empty))
                            {
                                row.Add(property.Name, value.ToString());
                            }
                            else
                            {
                                row.Add(property.Name, string.Empty);
                            }
                        }
                    }
                    result.Add(row);
                    row = null;
                }
            }
            return result;
        }

        public List<Dictionary<string, string>> ConvertListToDictionary()
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            if (this.CanConvertListToDictionary == true)
            {
                if (this.Data != null && this.Data.Count > 0)
                {
                    result = new List<Dictionary<string, string>>();
                    Dictionary<string, string> row = null;
                    Type valueType = null;
                    foreach (var item in this.Data)
                    {
                        row = new Dictionary<string, string>();
                        foreach (var property in this.Properties)
                        {
                            var value = property.GetValue(item, null);
                            valueType = value.GetType();
                            if (valueType == DateTime.MinValue.GetType())
                            {
                                if (!DateTime.Equals((DateTime)value, DateTime.MinValue))
                                {
                                    DateTime tempDate = (DateTime)value;
                                    row.Add(property.Name, tempDate.ToShortDateString());
                                }
                                else
                                {
                                    row.Add(property.Name, string.Empty);
                                }
                            }
                            else if (valueType == default(int).GetType())
                            {
                                if (!Int32.Equals(value, default(int)))
                                {
                                    int tempInt = (int)value;
                                    row.Add(property.Name, tempInt.ToString());
                                }
                                else
                                {
                                    row.Add(property.Name, string.Empty);
                                }
                            }
                            else if (valueType == default(bool).GetType())
                            {
                                if (!System.Boolean.Equals(value, default(bool)))
                                {
                                    bool tempBool = (bool)value;
                                    row.Add(property.Name, tempBool.ToString());
                                }
                                else
                                {
                                    row.Add(property.Name, string.Empty);
                                }
                            }
                            else if (valueType == string.Empty.GetType())
                            {
                                if (!String.Equals(value, string.Empty))
                                {
                                    row.Add(property.Name, value.ToString());
                                }
                                else
                                {
                                    row.Add(property.Name, string.Empty);
                                }
                            }
                        }
                        result.Add(row);
                        row = null;
                    }
                }
            }
            return result;
        }
    }
}
