﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    
    using Enums;
    using ViewData;
    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PhysicianController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPatientService patientService;

        public PhysicianController(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, IPhysicianService physicianService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.patientService = patientService;
        }

        #endregion

        #region PhysicianController Actions
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckPecos(string npi)
        {
            var viewData = new JsonViewData();
            if (npi.IsNotNullOrEmpty())
            {
                if (!lookupRepository.VerifyPecos(npi))
                {
                    viewData.isSuccessful = false;
                }
                else
                {
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetPrimary(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "PatientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your physician could not be set as primary." };

            bool result = physicianRepository.SetPrimary(patientId, id);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "This physician is now the primary physician";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your physician is not successfully set primary.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLogs(Guid physicianId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyPhysician, Current.AgencyId, physicianId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysicianPhone(Guid physicianId)
        {
            return Json(physicianService.GetPhysicianPhone(Current.AgencyId,physicianId));
        }

        [GridAction]
        public ActionResult LicenseList(Guid physicianId)
        {
            return View(new GridModel(physicianRepository.GeAgencyPhysicianLicenses(Current.AgencyId, physicianId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicense([Bind] PhysicainLicense license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (physicianService.AddLicense(license))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLicense(Guid Id, Guid physicianId, DateTime ExpirationDate)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(physicianId, "physicianId");

            if (physicianService.UpdateLicense(Id, physicianId, ExpirationDate))
            {
                return View(new GridModel(physicianRepository.GeAgencyPhysicianLicenses(Current.AgencyId, physicianId)));
            }
            return View(new GridModel(new List<PhysicainLicense>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicense(Guid Id, Guid physicianId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(physicianId, "physicianId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (physicianService.DeleteLicense(Id, physicianId))
            {
                return View(new GridModel(physicianRepository.GeAgencyPhysicianLicenses(Current.AgencyId, physicianId)));
            }
            return View(new GridModel(new List<PhysicainLicense>()));
        }

        #endregion
    }
}
