﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;

    using Telerik.Web.Mvc;

    using Enums;
    using Domain;
    using Common;
    using Services;
    using ViewData;
    using Security;
    using iTextExtension;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Extensions;

    
    using Axxess.Membership.Repositories;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Domain;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IPhysicianService physicianService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly ILookupRepository lookupRepository;


        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider coreDataProvider, IAssessmentService assessmentService, ILookUpDataProvider lookUpDataProvider, IAgencyService agencyService, IUserService userService, IPatientService patientService, IPhysicianService physicianService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "dataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.physicianService = physicianService;
            this.assessmentService = assessmentService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit()
        {
            return PartialView(agencyRepository.Get(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Signature()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSection(string category)
        {
            var cat = category.IsNotNullOrEmpty() ? category.Split('_').Last() : string.Empty;
            return PartialView("Edit/" + cat);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeInfo()
        {
            return PartialView("Edit/Upgrade");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BranchUpgradeInfo(Guid branchId)
        {
            var viewData = agencyService.GetAgencySubcriptionPlanDetails(branchId);
            return PartialView("Edit/UpgradeContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Information()
        {
            return PartialView("Edit/Information");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Branch(Guid branchId)
        {
            var viewData = agencyService.GetLocationWithAgencyInfo(branchId);
            return PartialView("Edit/Branch", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateAgency([Bind] Agency agency, [Bind(Prefix = "Location")] AgencyLocation location)
        {
            Check.Argument.IsNotNull(agency, "agency");
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();
            var existingLocation = agencyRepository.FindLocation(Current.AgencyId, location.Id);

            var agencyChange = new AgencyChange();
            var countAgencyUpates = 0;
            if (existingLocation != null)
            {
                if (existingLocation.IsMainOffice)
                {
                    agencyChange.AgencyId = Current.AgencyId;
                    agencyChange.FullName = Current.UserFullName;
                    agencyChange.LocationName = existingLocation.Name;
                    agencyChange.LocationAddressLine1 = existingLocation.AddressLine1;
                    agencyChange.LocationAddressLine2 = existingLocation.AddressLine2;
                    agencyChange.LocationAddressCity = existingLocation.AddressCity;
                    agencyChange.LocationAddressStateCode = existingLocation.AddressStateCode;
                    agencyChange.LocationAddressZipCode = existingLocation.AddressZipCode;
                    agencyChange.LocationPhoneWork = existingLocation.PhoneWork;
                    agencyChange.LocationFaxNumber = existingLocation.FaxNumber;

                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        location.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    if (existingLocation.Name != location.Name)
                    {
                        existingLocation.Name = location.Name;
                        countAgencyUpates++;
                    }
                    if (existingLocation.AddressLine1 != location.AddressLine1)
                    {
                        existingLocation.AddressLine1 = location.AddressLine1;
                        countAgencyUpates++;
                    }
                    if (existingLocation.AddressLine2 != location.AddressLine2)
                    {
                        existingLocation.AddressLine2 = location.AddressLine2;
                        countAgencyUpates++;
                    }
                    if (existingLocation.AddressCity != location.AddressCity)
                    {
                        existingLocation.AddressCity = location.AddressCity;
                        countAgencyUpates++;
                    }
                    if (existingLocation.AddressStateCode != location.AddressStateCode)
                    {
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        countAgencyUpates++;
                    }
                    if (existingLocation.AddressZipCode != location.AddressZipCode)
                    {
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        countAgencyUpates++;
                    }
                    if (existingLocation.PhoneWork != location.PhoneWork)
                    {
                        existingLocation.PhoneWork = location.PhoneWork;
                        countAgencyUpates++;
                    }
                    if (existingLocation.FaxNumber != location.FaxNumber)
                    {
                        existingLocation.FaxNumber = location.FaxNumber;
                        countAgencyUpates++;
                    }

                    var isAgencyUpdated = false;
                    var existingAgency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                    var previousAgencyName = existingAgency.Name;

                    if (existingAgency != null)
                    {
                        agencyChange.Name = existingAgency.Name;
                        agencyChange.TaxId = existingAgency.TaxId;
                        agencyChange.TaxIdType = existingAgency.TaxIdType;
                        agencyChange.ContactPersonEmail = existingAgency.ContactPersonEmail;
                        agencyChange.ContactPersonPhone = existingAgency.ContactPersonPhone;
                        agencyChange.CahpsVendor = existingAgency.CahpsVendor;
                        agencyChange.NationalProviderNumber = existingAgency.NationalProviderNumber;
                        agencyChange.MedicareProviderNumber = existingAgency.MedicareProviderNumber;
                        agencyChange.MedicaidProviderNumber = existingAgency.MedicaidProviderNumber;
                        agencyChange.HomeHealthAgencyId = existingAgency.HomeHealthAgencyId;
                        agencyChange.ContactPersonFirstName = existingAgency.ContactPersonFirstName;
                        agencyChange.ContactPersonLastName = existingAgency.ContactPersonLastName;

                        if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count > 0)
                        {
                            agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                        }
                        if (existingAgency.Name != agency.Name)
                        {
                            existingAgency.Name = agency.Name;
                            countAgencyUpates++;
                        }
                        if (existingAgency.TaxId != agency.TaxId)
                        {
                            existingAgency.TaxId = agency.TaxId;
                            countAgencyUpates++;
                        }
                        if (existingAgency.TaxIdType != agency.TaxIdType)
                        {
                            existingAgency.TaxIdType = agency.TaxIdType;
                            countAgencyUpates++;
                        }
                        if (existingAgency.ContactPersonEmail != agency.ContactPersonEmail)
                        {
                            existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                            countAgencyUpates++;
                        }
                        if (existingAgency.ContactPersonPhone != agency.ContactPersonPhone)
                        {
                            existingAgency.ContactPersonPhone = agency.ContactPersonPhone;
                            countAgencyUpates++;
                        }
                        if (existingAgency.CahpsVendor != agency.CahpsVendor)
                        {
                            existingAgency.CahpsVendor = agency.CahpsVendor;
                            countAgencyUpates++;
                        }
                        if (existingAgency.NationalProviderNumber != agency.NationalProviderNumber)
                        {
                            existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                            countAgencyUpates++;
                        }
                        if (existingAgency.MedicareProviderNumber != agency.MedicareProviderNumber)
                        {
                            existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                            countAgencyUpates++;
                        }
                        if (existingAgency.MedicaidProviderNumber != agency.MedicaidProviderNumber)
                        {
                            existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                            countAgencyUpates++;
                        }
                        if (existingAgency.HomeHealthAgencyId != agency.HomeHealthAgencyId)
                        {
                            existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            countAgencyUpates++;
                        }
                        if (existingAgency.ContactPersonFirstName != agency.ContactPersonFirstName)
                        {
                            existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                            countAgencyUpates++;
                        }
                        if (existingAgency.ContactPersonLastName != agency.ContactPersonLastName)
                        {
                            existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                            countAgencyUpates++;
                        }
                        isAgencyUpdated = true;
                        existingLocation.TaxId = agency.TaxId;
                        existingLocation.TaxIdType = agency.TaxIdType;
                        existingLocation.ContactPersonEmail = agency.ContactPersonEmail;
                        existingLocation.ContactPersonPhone = agency.ContactPersonPhone;
                        existingLocation.CahpsVendor = agency.CahpsVendor;
                        existingLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        existingLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                        existingLocation.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                        existingLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        existingLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                        existingLocation.ContactPersonLastName = agency.ContactPersonLastName;
                    }
                    if (!isAgencyUpdated || (isAgencyUpdated && agencyRepository.Update(existingAgency)))
                    {

                        if (agencyRepository.UpdateLocation(existingLocation))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                            viewData.isSuccessful = true;
                            if (isAgencyUpdated)
                            {
                                if (countAgencyUpates > 0)
                                {
                                    agencyRepository.AddChange(agencyChange);
                                    agencyService.Update(existingAgency, previousAgencyName);
                                }
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                                viewData.errorMessage += "Agency and Location was updated successfully.";
                            }
                            else
                            {
                                viewData.errorMessage = "Agency Location  was updated successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage += "Agency Location could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Agency could not be updated.";
                    }
                }
                else
                {
                    existingLocation.Name = location.Name;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }

                    if (existingLocation.IsLocationStandAlone)
                    {
                        if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count > 0)
                        {
                            existingLocation.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                        }
                        existingLocation.TaxId = agency.TaxId;
                        existingLocation.TaxIdType = agency.TaxIdType;
                        existingLocation.ContactPersonEmail = agency.ContactPersonEmail;
                        existingLocation.CahpsVendor = agency.CahpsVendor;
                        existingLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        existingLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                        existingLocation.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                        existingLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        existingLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                        existingLocation.ContactPersonLastName = agency.ContactPersonLastName;

                        if (agencyRepository.UpdateLocation(existingLocation))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Agency Location was updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage += "Agency Location could not be updated.";
                        }
                    }
                    else
                    {
                        if (agencyRepository.UpdateLocation(existingLocation))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Branch Location was updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage += "Branch Location could not be updated.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Location could not be found.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckSignature(string signature)
        {
            Check.Argument.IsNotEmpty(signature, "signature");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your signature does not match the one on file." };

            if (userService.IsSignatureCorrect(Current.UserId, signature))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Signature verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult ScheduleList()
        {
            return View(new GridModel(agencyService.GetSchedule()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitsList()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            ViewData["MissedVisitsGroupName"] = "EventDate";
            return PartialView("MissedVisits/List", patientService.GetMissedScheduledEvents(Current.AgencyId, Guid.Empty, DateTime.Now.AddDays(-89), DateTime.Today));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitsContent(Guid branchId, DateTime startDate, DateTime endDate, string groupName, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["MissedVisitsGroupName"] = groupName;
            return PartialView("MissedVisits/Content", patientService.GetMissedScheduledEvents(Current.AgencyId, branchId, startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsPastDueGrid()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Oasis/RecertsPastDue", agency != null ? agency.Payor : "0");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsUpcomingGrid()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Oasis/RecertsUpcoming", "0");
        }

        [GridAction]
        public ActionResult RecertsPastDue(Guid BranchId, int InsuranceId, DateTime StartDate)
        {
            return View(new GridModel(agencyService.GetRecertsPastDue(BranchId, InsuranceId, StartDate, DateTime.Now)));
        }

        public JsonResult RecertsPastDueWidget()
        {
            return Json(agencyService.GetRecertsPastDueWidget());
        }

        [GridAction]
        public ActionResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            return View(new GridModel(agencyService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24))));
        }

        public JsonResult RecertsUpcomingWidget()
        {
            return Json(agencyService.GetRecertsUpcomingWidget());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagement()
        {
            ViewData["GroupName"] = "EventDate";
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("QA/CaseManagement", agencyService.GetCaseManagerSchedule(location != null ? location.Id : Guid.Empty, 1, DateTime.Today.AddDays(-7), DateTime.Today));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagementContent(Guid BranchId, string groupName, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            ViewData["GroupName"] = groupName;
            // var location = agencyRepository.GetMainLocation(Current.AgencyId);
            // ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            List<PatientEpisodeEvent> events = agencyService.GetCaseManagerSchedule(BranchId, Status, StartDate, EndDate);
            //foreach (PatientEpisodeEvent e in events) e.RedNote = patientService.GetReturnComments(e.EventId, e.EpisodeId, e.PatientId);
            return PartialView("QA/CaseManagementContent", events);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public LargeJsonResult CaseManagementGrid(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return new LargeJsonResult
            {
                MaxJsonLength = int.MaxValue,
                Data = new GridModel(agencyService.GetCaseManagerSchedule(BranchId, Status, StartDate, EndDate))
            };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagementCenter()
        {
            return PartialView("QA/Center", new QaCenterViewData { });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagementData(Guid patientId)
        {
            var viewData = new QaCenterViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    patient.Physician = physician;
                }
                var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patientId);
                if (emergencyContact != null)
                {
                    patient.EmergencyContact = emergencyContact;
                }
                patientService.SetInsurance(patient);
                viewData.Patient = patient;
                viewData.Activities = agencyService.GetPatientCaseManagerSchedule(patientId);
            }

            return PartialView("QA/Data", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public LargeJsonResult CaseManagementActivity(Guid patientId)
        {
            return new LargeJsonResult
            {
                MaxJsonLength = int.MaxValue,
                Data = new GridModel(agencyService.GetPatientCaseManagerSchedule(patientId))
            };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintQueue()
        {
            ViewData["GroupName"] = "EventDate";
            ViewData["StartDate"] = DateTime.Now.AddDays(-7);
            ViewData["EndDate"] = DateTime.Now;
            ViewData["BranchId"] = Guid.Empty;
            return PartialView("PrintQueue/List");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AfterHoursSupport()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueContent(string groupName, Guid branchId, string startDate, string endDate)
        {
            ViewData["GroupName"] = groupName;
            ViewData["StartDate"] = Convert.ToDateTime(startDate);
            ViewData["Enddate"] = Convert.ToDateTime(endDate);
            ViewData["BranchId"] = branchId;
            return PartialView("PrintQueue/Content");
        }


        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueGrid(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(agencyService.GetPrintQueue(BranchId, StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersToBeSentView()
        {
            return PartialView("Order/ToBeSent");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersToBeSent(Guid BranchId, bool sendAutomatically, string StartDate, string EndDate)
        {
            return View(new GridModel(agencyService.GetOrdersToBeSent(BranchId, sendAutomatically, StartDate.ToDateTime(), EndDate.ToDateTime())));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersHistory()
        {
            return PartialView("Order/History");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryList(Guid BranchId, DateTime StartDate, DateTime EndDate, string orderBy)
        {
            
            var status = new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
            //return View(new GridModel(agencyService.GetProcessedOrders(BranchId, StartDate, EndDate, status)));
            var order=agencyService.GetProcessedOrders(BranchId, StartDate, EndDate, status);
            if (orderBy.IsNotNullOrEmpty())
            {
                string sortPram = orderBy.Split('-')[0].Trim();
                string sortOrder = orderBy.Split('-')[1].Trim();

                order = order.AsQueryable().OrderByDynamic(sortPram, sortOrder == "asc" ? SortDirection.Ascending : SortDirection.Descending).ToList();

            }
            else
            {
                order = order.OrderByDescending(o => o.CreatedDate).ToList();
            }
            return new LargeJsonResult
            {
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new GridModel<Order>
                {
                    Data = order??new List<Order>()
                }
            };
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkOrdersAsSent(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Orders could not be marked as sent to Physician" };

            if (formCollection.Count > 0)
            {
                if (agencyService.MarkOrdersAsSent(formCollection))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Orders have been marked as sent to Physician";
                }
            }
            else
            {
                viewData.errorMessage = "No Orders were selected";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkOrderAsReturned(Guid branchId, Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate, DateTime startDate, DateTime endDate)
        {
            agencyService.MarkOrderAsReturned(id, patientId, episodeId, type, receivedDate, physicianSignatureDate);
            return View(new GridModel(agencyService.GetOrdersPendingSignature(branchId, startDate, endDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderHistoryEdit(Guid id, Guid patientId, Guid episodeId, string type)
        {
            return PartialView("Order/HistoryEdit", agencyService.GetOrder(id, patientId, episodeId, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditOrders(FormCollection formCollection, OrderType Type)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order isn't updated. Try again." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var PatientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var EpisodeId = keys.Contains("EpisodeId") && formCollection["EpisodeId"].IsNotNullOrEmpty() ? formCollection["EpisodeId"].ToGuid() : Guid.Empty;
                //var Type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                var ReceivedDate = formCollection["ReceivedDate"].ToString();
                var SendDate = formCollection["SendDate"].ToString();
                var PhysicianSignatureDate = formCollection["PhysicianSignatureDate"].ToString();
                if (ReceivedDate.IsNullOrEmpty() || !ReceivedDate.IsValidDate())
                {
                    viewData.errorMessage = "Received date is not a right format.";
                }
                else if (SendDate.IsNullOrEmpty() || !SendDate.IsValidDate())
                {
                    viewData.errorMessage = "Send date is not a right format.";
                }
                else if (PhysicianSignatureDate.IsNullOrEmpty() || !PhysicianSignatureDate.IsValidDate())
                {
                    viewData.errorMessage = "Physician Signature date is not a right format.";
                }
                else if (!Id.IsEmpty() && !PatientId.IsEmpty() && !EpisodeId.IsEmpty())
                {
                    if (agencyService.UpdateOrderDates(Id, PatientId, EpisodeId, Type, ReceivedDate.ToDateTime(), SendDate.ToDateTime(), PhysicianSignatureDate.ToDateTime()))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Order successfully updated";
                    }

                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersPendingSignatureView()
        {
            return PartialView("Order/PendingSignature");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersPendingSignature(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(agencyService.GetOrdersPendingSignature(BranchId, StartDate, EndDate)));
        }

        [GridAction]
        public ActionResult Users()
        {
            return View(new GridModel(userRepository.GetAgencyUsers(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Blankforms()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upgrade(AgencyUpgrade agencyUpgrade, string signature)
        {
            Check.Argument.IsNotNull(agencyUpgrade, "agencyUpgrade");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your subscription plan could not be changed." };

            if (userService.IsSignatureCorrect(Current.UserId, signature))
            {
                var result = agencyService.Upgrade(agencyUpgrade);

                if (result.IsSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your subscription plan has been updated successfully.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = result.ErrorMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Signature could not be verified. Please try again.";
            }
            return Json(viewData);
        }

        #endregion

        #region Medicare Eligibility Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicareEligibilitySummary()
        {
            ViewData["SortColumn"] = "CreatedFormatted";
            ViewData["SortDirection"] = "ASC";
            return PartialView("MedicareEligibility/Summary",
                agencyRepository.GetMedicareEligibilitySummariesBetweenDates(Current.AgencyId, DateTime.Now.AddDays(-25), DateTime.Now.AddDays(25)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityContent(string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("MedicareEligibility/Content",
                agencyRepository.GetMedicareEligibilitySummariesBetweenDates(Current.AgencyId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicareEligibilityPrint(Guid id)
        {
            var report = agencyRepository.GetMedicareEligibilitySummary(Current.AgencyId, id);
            if (report == null)
            {
                report = new MedicareEligibilitySummary();
            }
            return View("MedicareEligibility/Print", report);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityPdf(Guid id)
        {
            var doc = new MedicareEligibilitySummaryPdf(agencyRepository.GetMedicareEligibilitySummary(Current.AgencyId, id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=MedicareEligibilitySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        #endregion

        #region Contact Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Contacts()
        {
            return PartialView("Contact/List");
        }

        [GridAction]
        public ActionResult ContactList()
        {
            return View(new GridModel(agencyRepository.GetContacts(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewContact()
        {
            return PartialView("Contact/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (!agencyService.CreateContact(contact))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the contact.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Contact was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditContact(Guid Id)
        {
            return PartialView("Contact/Edit", agencyRepository.FindContact(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (agencyRepository.FindContact(Current.AgencyId, contact.Id) != null)
                {
                    contact.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditContact(contact))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the contact.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Contact was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected contact don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteContact(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Contact could not be deleted. Please try again." };
            if (agencyRepository.DeleteContact(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyContact, LogAction.AgencyContactDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Contact has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Contact could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteIncident(Guid patientId, Guid episodeId, Guid id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Incident could not be deleted. Please try again." };

            if (agencyRepository.DeleteIncident(id) && patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, id, (int)DisciplineTasks.IncidentAccidentReport))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyIncident, LogAction.IncidentDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Incident has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Incident could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteInfection(Guid patientId, Guid episodeId, Guid id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Infection could not be deleted. Please try again." };
            bool deletedInfection = agencyRepository.DeleteInfection(id);
            if (deletedInfection && patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, id, (int)DisciplineTasks.InfectionReport))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyInfection, LogAction.InfectionDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Incident has been deleted.";
            }
            else
            {
                if (deletedInfection)
                {
                    var infection = agencyRepository.GetInfectionReport(Current.AgencyId, id);
                    infection.IsDeprecated = false;
                    agencyRepository.UpdateInfectionModal(infection);
                }
                viewData.isSuccessful = false;
                viewData.errorMessage = "Incident could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactLogs(Guid contactId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyContact, Current.AgencyId, contactId.ToString()));
        }

        #endregion

        #region Hospital Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewHospital()
        {
            return PartialView("Hospital/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                hospital.AgencyId = Current.AgencyId;
                hospital.Id = Guid.NewGuid();
                if (!agencyRepository.AddHospital(hospital))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the hospital.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Hospitals()
        {
            return PartialView("Hospital/List");
        }

        [GridAction]
        public ActionResult HospitalList()
        {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditHospital(Guid Id)
        {
            return PartialView("Hospital/Edit", agencyRepository.FindHospital(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                if (agencyRepository.FindHospital(Current.AgencyId, hospital.Id) != null)
                {
                    hospital.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditHospital(hospital))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the hospital.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Hospital was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Hospial don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteHospital(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (agencyRepository.FindHospital(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteHospital(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the hospital.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Hospital don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalLogs(Guid hospitalId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyHospital, Current.AgencyId, hospitalId.ToString()));
        }

        #endregion

        #region Pharmacy Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPharmacy()
        {
            return PartialView("Pharmacy/New");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPharmacy([Bind] AgencyPharmacy pharmacy)
        {
            Check.Argument.IsNotNull(pharmacy, "pharmacy");

            var viewData = new JsonViewData();

            if (pharmacy.IsValid)
            {
                pharmacy.AgencyId = Current.AgencyId;
                pharmacy.Id = Guid.NewGuid();
                if (!agencyRepository.AddPharmacy(pharmacy))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the pharmacy.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, pharmacy.Id.ToString(), LogType.AgencyPharmacy, LogAction.AgencyPharmacyAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Pharmacy was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = pharmacy.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Pharmacies()
        {
            return PartialView("Pharmacy/List");
        }

        [GridAction]
        public ActionResult PharmacyList()
        {
            return View(new GridModel(agencyRepository.GetPharmacies(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPharmacy(Guid Id)
        {
            return PartialView("Pharmacy/Edit", agencyRepository.FindPharmacy(Current.AgencyId, Id));
        }

        //need to change
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePharmacy([Bind] AgencyPharmacy pharmacy)
        {
            Check.Argument.IsNotNull(pharmacy, "pharmacy");

            var viewData = new JsonViewData();

            if (pharmacy.IsValid)
            {
                if (agencyRepository.FindPharmacy(Current.AgencyId, pharmacy.Id) != null)
                {
                    pharmacy.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditPharmacy(pharmacy))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the pharmacy.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, pharmacy.Id.ToString(), LogType.AgencyPharmacy, LogAction.AgencyPharmacyUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Pharmacy was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Pharmacy don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = pharmacy.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeletePharmacy(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this pharmacy. Please try again." };
            if (agencyRepository.FindPharmacy(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeletePharmacy(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyPharmacy, LogAction.AgencyPharmacyDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Pharmacy was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the pharmacy.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Pharmacy don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PharmacyLogs(Guid pharmacyId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyPharmacy, Current.AgencyId, pharmacyId.ToString()));
        }

        #endregion

        #region Physician Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysicians()
        {
            return Json(physicianService.GetAgencyPhysiciansWithPecosVerification().ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysician(Guid physicianId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            return Json(physicianRepository.Get(physicianId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Physicians()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Physician/List", physicianService.GetAgencyPhysiciansWithPecosVerification());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianListContent(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Physician/ListContent", physicianService.GetAgencyPhysiciansWithPecosVerification());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPhysician()
        {
            return PartialView("Physician/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPhysician([Bind] AgencyPhysician agencyPhysician)
        {
            //AgencyPhysician agencyPhysician = AgencyPhysician;
            Check.Argument.IsNotNull(agencyPhysician, "agencyPhysician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be added" };

            bool exist = physicianRepository.DoesPhysicianExistInAgency(Current.AgencyId, agencyPhysician.NPI, agencyPhysician.AddressZipCode);
            if (exist)
            {
                viewData.errorMessage = "The physician has already been added.";
                return Json(viewData);
            }

            if (agencyPhysician.PhysicianAccess && agencyPhysician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }

            if (agencyPhysician.IsValid)
            {
                agencyPhysician.AgencyId = Current.AgencyId;
                if (!physicianService.CreatePhysician(agencyPhysician))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully Saved";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agencyPhysician.ValidationMessage;
            }

            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPhysician(Guid Id)
        {
            var physician = physicianRepository.Get(Id, Current.AgencyId);
            physician.IsPecosVerified = lookupRepository.VerifyPecos(physician.NPI);
            return PartialView("Physician/Edit", physician);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePhysician([Bind] AgencyPhysician physician)
        {
            Check.Argument.IsNotNull(physician, "physician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be updated." };

            if (physician.PhysicianAccess && physician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }

            if (physician.IsValid)
            {
                if (physicianService.UpdatePhysician(physician))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Physician has been successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Physician data could not be saved.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = physician.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeletePhysician(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be deleted. Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewLocation()
        {
            return PartialView("Location/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Locations()
        {
            return PartialView("Location/List");
        }

        [GridAction]
        public ActionResult LocationList()
        {
            return View(new GridModel(agencyRepository.GetBranches(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid Id)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (agencyRepository.FindLocation(Current.AgencyId, location.Id) != null)
                {
                    location.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationLogs(Guid locationId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyLocation, Current.AgencyId, locationId.ToString()));
        }

        [GridAction]
        public ActionResult LocationBillDatas(Guid locationId)
        {
            var billDatas = new List<ChargeRate>();
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                billDatas = location.BillData.ToObject<List<ChargeRate>>();
            }
            return View(new GridModel(billDatas));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocationBillData(Guid LocationId, int Id)
        {
            var chargeRate = new ChargeRate();
            var location = agencyRepository.FindLocation(Current.AgencyId, LocationId);
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                chargeRate = location.BillData.ToObject<List<ChargeRate>>().FirstOrDefault(r => r.Id == Id);
                if (chargeRate != null)
                {
                    chargeRate.LocationId = location.Id;
                }
            }
            return PartialView("EditBillData", chargeRate);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocationBillData(ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this medicare insurance rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, chargeRate.LocationId);
            if (location != null && chargeRate != null)
            {
                chargeRate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                if (chargeRate.IsValid)
                {
                    if (location.BillData.IsNotNullOrEmpty())
                    {
                        var rates = location.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                rate.PreferredDescription = chargeRate.PreferredDescription;
                                rate.Code = chargeRate.Code;
                                rate.RevenueCode = chargeRate.RevenueCode;
                                rate.Charge = chargeRate.Charge;
                                rate.Modifier = chargeRate.Modifier;
                                rate.Modifier2 = chargeRate.Modifier2;
                                rate.Modifier3 = chargeRate.Modifier3;
                                rate.Modifier4 = chargeRate.Modifier4;
                                rate.ChargeType = chargeRate.ChargeType;
                                // rate.Unit = chargeRate.Unit;
                                location.BillData = rates.ToXml();
                                if (agencyRepository.UpdateLocation(location))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLocationBillData(Guid locationId)
        {
            return PartialView("NewBillData", locationId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveLocationBillData(Guid locationId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this medicare insurance rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (chargeRate != null)
            {
                chargeRate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                if (chargeRate != null && chargeRate.IsValid)
                {
                    if (location != null)
                    {
                        if (location.BillData.IsNotNullOrEmpty())
                        {
                            var rates = location.BillData.ToObject<List<ChargeRate>>();
                            if (rates != null && rates.Count > 0)
                            {
                                var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                                if (rate != null)
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Insurance rate already exist.";
                                }
                                else
                                {
                                    rates.Add(chargeRate);
                                    location.BillData = rates.ToXml();
                                    if (agencyRepository.UpdateLocation(location))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "Insurance rate saved successfully";
                                    }
                                }
                            }
                            else
                            {
                                rates = new List<ChargeRate>();
                                rates.Add(chargeRate);
                                location.BillData = rates.ToXml();
                                if (agencyRepository.UpdateLocation(location))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate saved successfully";
                                }
                            }
                        }
                        else
                        {
                            var rates = new List<ChargeRate>();
                            rates.Add(chargeRate);
                            location.BillData = rates.ToXml();
                            if (agencyRepository.UpdateLocation(location))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance rate saved successfully";
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLocationBillData(Guid LocationId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, LocationId);
            if (location != null)
            {
                if (location.BillData.IsNotNullOrEmpty())
                {
                    var rates = location.BillData.ToObject<List<ChargeRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id);
                        if (removed > 0)
                        {
                            location.BillData = rates.ToXml();
                            if (agencyRepository.UpdateLocation(location))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance visit rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Insurance Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewInsurance()
        {
            return PartialView("Insurance/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                if (disciplineList != null && disciplineList.Length > 0)
                {
                    disciplineList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Charge") && formCollection[l + "_Charge"].IsDouble())
                        {
                            visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                            rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                        }
                    });
                }

                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                var locators = new List<Locator>();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                        {
                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                        }
                    });
                }
                insurance.Ub04Locator81cca = locators.ToXml();
            }

            if (insurance.IsValid)
            {
                insurance.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddInsurance(insurance))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the insurance.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceAdded, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Insurance was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetInsurances()
        {
            return Json(agencyService.GetInsurances());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Insurances()
        {
            return PartialView("Insurance/List");
        }

        [GridAction]
        public ActionResult InsuranceList()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var insurances = new List<InsuranceLean>();
            if (agency != null)
            {
                int payor;
                if (int.TryParse(agency.Payor, out payor))
                {
                    var insurance = lookupRepository.GetInsurance(payor);
                    if (insurance != null)
                    {
                        insurances.Add(new InsuranceLean { Name = "Medicare", PayorType = 3, PayorId = "", InvoiceType = 1, IsTradtionalMedicare = true });
                    }
                }
                var data = agencyRepository.GetLeanInsurances(Current.AgencyId);
                if (data != null && data.Count > 0)
                {
                    insurances.AddRange(data.OrderBy(i => i.Name));
                }
            }

            return View(new GridModel(insurances));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInsurance(int Id)
        {
            return PartialView("Insurance/Edit", agencyRepository.FindInsurance(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge"))
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                    }
                });

                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            var Ub04Locator81cca = new List<Locator>();
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                        {
                            Ub04Locator81cca.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"].Trim(), Code2 = formCollection[l + "_Code2"].Trim(), Code3 = formCollection[l + "_Code3"].Trim() });
                        }
                    });
                }
                
            }
            if (keys.Contains("Ub04Locator81ccaPart2"))
            {
                var locatorList2 = formCollection["Ub04Locator81ccaPart2"].ToArray();
                if (locatorList2 != null && locatorList2.Length > 0)
                {
                    locatorList2.ForEach(l =>
                        {
                            if (keys.Contains(l + "_Customized") && formCollection[l + "_Customized"].Equals("1"))
                            {
                                if (l.Equals("Locator76") || formCollection[l + "_PhysicianType"].Equals("other") )
                                {
                                    Ub04Locator81cca.Add(new Locator { LocatorId = l, Customized=true, PhysicianType =keys.Contains(l+"_PhysicianType") ? formCollection[l + "_PhysicianType"].Trim():string.Empty, Code1 = formCollection[l + "_Code1"].Trim(), Code2 = formCollection[l + "_Code2"].Trim(), Code3 = formCollection[l + "_Code3"].Trim() });
                                }
                                else
                                {
                                    Ub04Locator81cca.Add(new Locator { LocatorId = l, Customized=true, PhysicianType = formCollection[l + "_PhysicianType"].Trim() });
                                }
                            }
                        });
                }
            }
            insurance.Ub04Locator81cca = Ub04Locator81cca.ToXml();
            if (keys.Contains("HCFALocators"))
            {
                var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "HCFALocators");
                insurance.HCFALocators = locators.ToXml();
            }

            if (insurance.IsValid)
            {
                if (agencyRepository.FindInsurance(Current.AgencyId, insurance.Id) != null)
                {
                    insurance.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditInsurance(insurance))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the insurance.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceUpdated, string.Empty);
                        InsuranceEngine.Instance.Refresh(Current.AgencyId);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteInsurance(int Id)
        {
            Check.Argument.IsNotNegativeOrZero(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance. Please try again." };
            if (agencyRepository.FindInsurance(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteInsurance(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceDeleted, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The insurance was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected insurance don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceLogs(int insuranceId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyInsurance, Current.AgencyId, insuranceId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewBillData(int insuranceId)
        {
            return PartialView("Insurance/NewBillData", agencyRepository.GetInsurance(insuranceId, Current.AgencyId) ?? new AgencyInsurance());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveBillData(int InsuranceId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (chargeRate.IsValid)
            {
                if (insurance != null)
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Insurance rate already exist.";
                            }
                            else
                            {
                                rates.Add(chargeRate);
                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate saved successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<ChargeRate>();
                            rates.Add(chargeRate);
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance rate saved successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<ChargeRate>();
                        rates.Add(chargeRate);
                        insurance.BillData = rates.ToXml();
                        if (agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance rate saved successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = chargeRate.ValidationMessage;
            }

            return Json(viewData);
        }

        [GridAction]
        public ActionResult BillDatas(int InsuranceId)
        {
            var billDatas = new List<ChargeRate>();
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                billDatas = insurance.BillData.ToObject<List<ChargeRate>>();
            }
            return View(new GridModel(billDatas));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditBillData(int InsuranceId, int Id)
        {
            var chargeRate = new ChargeRate();
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                chargeRate = insurance.BillData.ToObject<List<ChargeRate>>().FirstOrDefault(r => r.Id == Id);
                if (chargeRate != null)
                {
                    chargeRate.InsuranceId = insurance.Id;
                    chargeRate.IsMedicareHMO = insurance.PayorType == (int)PayerTypes.MedicareHMO;
                }
            }
            return PartialView("Insurance/EditBillData", chargeRate);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateBillData(ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(chargeRate.InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (chargeRate.IsValid)
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                rate.PreferredDescription = chargeRate.PreferredDescription;
                                rate.Code = chargeRate.Code;
                                rate.RevenueCode = chargeRate.RevenueCode;
                                rate.Charge = chargeRate.Charge;
                                rate.ExpectedRate = chargeRate.ExpectedRate;
                                rate.Modifier = chargeRate.Modifier;
                                rate.Modifier2 = chargeRate.Modifier2;
                                rate.Modifier3 = chargeRate.Modifier3;
                                rate.Modifier4 = chargeRate.Modifier4;
                                rate.ChargeType = chargeRate.ChargeType;
                                rate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                                if (chargeRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || chargeRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                                {
                                    if (chargeRate.IsTimeLimit)
                                    {

                                        rate.TimeLimitHour = chargeRate.TimeLimitHour;
                                        rate.TimeLimitMin = chargeRate.TimeLimitMin;
                                        rate.SecondDescription = chargeRate.SecondDescription;
                                        rate.SecondCode = chargeRate.SecondCode;
                                        rate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                        rate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                        if (chargeRate.IsSecondChargeDifferent)
                                        {
                                            rate.SecondCharge = chargeRate.SecondCharge;
                                        }
                                        else
                                        {
                                            rate.SecondCharge = 0;
                                        }
                                        rate.SecondModifier = chargeRate.SecondModifier;
                                        rate.SecondModifier2 = chargeRate.SecondModifier2;
                                        rate.SecondModifier3 = chargeRate.SecondModifier3;
                                        rate.SecondModifier4 = chargeRate.SecondModifier4;
                                        rate.SecondChargeType = chargeRate.SecondChargeType;
                                        if (chargeRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                        {
                                            rate.SecondUnit = chargeRate.SecondUnit;
                                        }
                                        else
                                        {
                                            rate.SecondUnit = 0;
                                        }
                                        rate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                    }
                                    rate.IsTimeLimit = chargeRate.IsTimeLimit;
                                }
                                else if (chargeRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                                {
                                    rate.Unit = chargeRate.Unit;
                                }
                                if (insurance.PayorType == (int)PayerTypes.MedicareHMO && (chargeRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || chargeRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                                {
                                    rate.MedicareHMORate = chargeRate.MedicareHMORate;
                                }
                                rate.IsTravelTimeCovered = chargeRate.IsTravelTimeCovered;
                                if (chargeRate.IsTravelTimeCovered)
                                {
                                    rate.TravelTimeCharge = chargeRate.TravelTimeCharge;
                                    rate.TravelTimeChargeType = chargeRate.TravelTimeChargeType;
                                    rate.TravelTimeDescription = chargeRate.TravelTimeDescription;
                                    rate.TravelTimeHcpcsCode = chargeRate.TravelTimeHcpcsCode;
                                    rate.TravelTimeModifier = chargeRate.TravelTimeModifier;
                                    rate.TravelTimeModifier2 = chargeRate.TravelTimeModifier2;
                                    rate.TravelTimeModifier3 = chargeRate.TravelTimeModifier3;
                                    rate.TravelTimeModifier4 = chargeRate.TravelTimeModifier4;
                                    rate.TravelTimeRevenueCode = chargeRate.TravelTimeRevenueCode;
                                    rate.TravelTimeUnit = chargeRate.TravelTimeUnit;
                                }

                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteBillData(int InsuranceId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (insurance.BillData.IsNotNullOrEmpty())
                {
                    var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id);
                        if (removed > 0)
                        {
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance visit rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceSelectList(Guid branchId)
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(i =>
                {
                    list.Add(new { Name = i.Name, Id = i.Id });
                });
            }
            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicareInsuranceSelectList(Guid branchId)
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
            }
            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicareWithHMOInsuranceSelectList(Guid branchId)
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                if (i.PayorType == 2)
                {
                    list.Add(new { Name = i.Name, Id = i.Id, Selected = false });
                }
            });
            return Json(list);
        }

        public JsonResult PatientInsurances(Guid branchId)
        {
            var selectList = new StringBuilder();
            selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>-- Select Insurance --</option>", "0", 0);
            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                        }
                    }
                }
            }

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance =>
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", insurance.Id.ToString(), 1, insurance.Name);
            });
            return Json(selectList.ToString());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReplaceInsuranceVisit(int Id, int replacedId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error in editing the insurance visit information." };
            var insurance = agencyRepository.FindInsurance(Current.AgencyId, Id);
            var selectedInsurance=new AgencyInsurance();
            if (insurance != null)
            {
                if (replacedId < 1000)
                {
                    var location = agencyRepository.GetMainLocation(Current.AgencyId);
                    selectedInsurance.BillData = location.BillData;
                }
                else
                {
                    selectedInsurance = agencyRepository.FindInsurance(Current.AgencyId, replacedId);
                }
                if (selectedInsurance != null)
                {
                    if (selectedInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = selectedInsurance.BillData;
                        if (!agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in editing the insurance visit information.";
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceVisitInfoOverWrite, string.Empty);
                            InsuranceEngine.Instance.Refresh(Current.AgencyId);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance was edited successfully";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Selected insurance bill information is empty.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The insurance to be updated don't exist.";
            }

            return Json(viewData);
        }

        #endregion

        #region Infection Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewInfection()
        {
            return PartialView("Infection/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewInfection(Guid patientId)
        {
            return PartialView("Infection/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInfection([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");

            var viewData = new JsonViewData();

            if (infection.IsValid)
            {
                infection.Id = Guid.NewGuid();
                infection.UserId = Current.UserId;
                infection.AgencyId = Current.AgencyId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection log.";
                        return Json(viewData);
                    }
                    else
                    {
                        infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }
                else
                {
                    infection.SignatureText = string.Empty;
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = infection.Id,
                    UserId = infection.UserId,
                    PatientId = infection.PatientId,
                    EpisodeId = infection.EpisodeId,
                    Status = infection.Status.ToString(),
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = infection.InfectionDate.ToShortDateString(),
                    VisitDate = infection.InfectionDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.InfectionReport
                };

                if (agencyRepository.AddInfection(infection))
                {
                    if (patientRepository.UpdateEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                    {
                        Auditor.Log(infection.EpisodeId, infection.PatientId, infection.Id, Actions.Add, DisciplineTasks.InfectionReport);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Infection log was added successfully";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The infection log could not be added. Please try again.";
                    }
                }
                else
                {

                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The infection log could not be added. Please try again.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult InfectionGrid()
        {
            return PartialView("Infection/List");
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult InfectionList()
        {
            return View(new GridModel(agencyService.GetInfections(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInfection(Guid Id)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, Id);
            if (infection != null)
            {
                var patient = patientRepository.GetPatientOnly(infection.PatientId, infection.AgencyId);
                if (patient != null) infection.PatientName = patient.DisplayName;
                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                        Assessment assessment = assessmentService.GetEpisodeAssessment(Current.AgencyId, episode);
                        infection.Diagnosis = new Dictionary<string, string>();
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                            infection.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "");
                            infection.Diagnosis.Add("ICD9M", oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "");
                            infection.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "");
                            infection.Diagnosis.Add("ICD9M2", oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "");
                        }
                    }
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, infection.EpisodeId, infection.PatientId, Id);
                    if (scheduledEvent != null) infection.StatusComment = scheduledEvent.StatusComment;
                }
            }
            else infection = new Infection();
            return PartialView("Infection/Edit", infection);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInfection([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid)
            {
                infection.AgencyId = Current.AgencyId;
                infection.UserId = infection.UserId.IsEmpty() ? Current.UserId : infection.UserId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection log.";
                        return Json(viewData);
                    }
                    else
                    {
                        infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) infection.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    }
                }
                else infection.SignatureText = string.Empty;
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null && scheduleEvents.Exists(e => e.EventId == infection.Id && e.PatientId == infection.PatientId))
                    {
                        var evnt = scheduleEvents.Single(e => e.EventId == infection.Id && e.PatientId == infection.PatientId);
                        if (evnt != null)
                        {
                            evnt.VisitDate = infection.InfectionDate.ToString("MM/dd/yyyy");
                            evnt.Status = infection.Status.ToString();
                            episode.Schedule = scheduleEvents.ToXml();
                            if (patientRepository.UpdateEpisode(episode))
                            {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                if (userEvent != null)
                                {
                                    userEvent.EventDate = evnt.EventDate;
                                    userEvent.VisitDate = evnt.VisitDate;
                                    userEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                                    userEvent.Status = evnt.Status;
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    var newUserEvent = new UserEvent
                                    {
                                        EventId = infection.Id,
                                        UserId = infection.UserId,
                                        PatientId = infection.PatientId,
                                        EpisodeId = infection.EpisodeId,
                                        Status = infection.Status.ToString(),
                                        Discipline = Disciplines.ReportsAndNotes.ToString(),
                                        EventDate = evnt.EventDate,
                                        VisitDate = evnt.VisitDate,
                                        DisciplineTask = (int)DisciplineTasks.InfectionReport
                                    };
                                    patientRepository.AddNewUserEvent(Current.AgencyId, infection.PatientId, newUserEvent);
                                }
                                if (evnt.Status.IsInteger()) Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.InfectionReport, string.Empty);
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "The infection log could not be updated. Please try again.";
                                return Json(viewData);
                            }
                        }
                        else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                    }
                    else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                }
                else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                if (!agencyRepository.UpdateInfection(infection))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The Infection log could not be updated.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Infection log was updated successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult InfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Infection/Print", agencyService.GetInfectionReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult InfectionReportPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new InfectionReportPdf(agencyService.GetInfectionReportPrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=InfectionLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessInfection(string button, Guid patientId, Guid eventId)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Infection Log could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (agencyService.ProcessInfections(button, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Log has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Log could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (agencyService.ProcessInfections(button, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Log has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Log could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Incident Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewIncident()
        {
            return PartialView("Incident/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewIncident(Guid patientId)
        {
            return PartialView("Incident/New", patientId);
        }

        public ActionResult AddIncident([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");

            var viewData = new JsonViewData();

            if (incident.IsValid)
            {
                incident.Id = Guid.NewGuid();
                incident.UserId = Current.UserId;
                incident.AgencyId = Current.AgencyId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident/accident log.";
                        return Json(viewData);
                    }
                    else
                    {
                        incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }
                else
                {
                    incident.SignatureText = string.Empty;
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = incident.Id,
                    UserId = incident.UserId,
                    PatientId = incident.PatientId,
                    EpisodeId = incident.EpisodeId,
                    Status = incident.Status.ToString(),
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = incident.IncidentDate.ToShortDateString(),
                    VisitDate = incident.IncidentDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
                };

                if (patientRepository.UpdateEpisode(Current.AgencyId, incident.EpisodeId, incident.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                {
                    Auditor.Log(incident.EpisodeId, incident.PatientId, incident.Id, Actions.Add, DisciplineTasks.IncidentAccidentReport);
                    if (!agencyRepository.AddIncident(incident))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the incident / accident.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Incident / Accident was saved successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the Incident / Accident.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IncidentGrid()
        {
            return PartialView("Incident/List");
        }

        [GridAction]
        public ActionResult IncidentList()
        {
            return View(new GridModel(agencyService.GetIncidents(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditIncident(Guid Id)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, Id);
            if (incident != null)
            {
                var patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);
                if (patient != null) incident.PatientName = patient.DisplayName;
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;
                    }
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, incident.EpisodeId, incident.PatientId, Id);
                    if (scheduledEvent != null) incident.StatusComment = scheduledEvent.StatusComment;
                }
            }
            else { incident = new Incident(); }
            return PartialView("Incident/Edit", incident);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateIncident([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");
            var viewData = new JsonViewData();
            if (incident.IsValid)
            {
                incident.AgencyId = Current.AgencyId;
                incident.UserId = incident.UserId.IsEmpty() ? Current.UserId : incident.UserId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident log.";
                        return Json(viewData);
                    }
                    else
                    {
                        incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) incident.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    }
                }
                else incident.SignatureText = string.Empty;
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null && scheduleEvents.Exists(e => e.EventId == incident.Id && e.PatientId == incident.PatientId))
                    {
                        var evnt = scheduleEvents.Single(e => e.EventId == incident.Id && e.PatientId == incident.PatientId);
                        if (evnt != null)
                        {
                            evnt.VisitDate = incident.IncidentDate.ToString("MM/dd/yyyy");
                            evnt.Status = incident.Status.ToString();
                            episode.Schedule = scheduleEvents.ToXml();
                            if (patientRepository.UpdateEpisode(episode))
                            {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                if (userEvent != null)
                                {
                                    userEvent.EventDate = evnt.EventDate;
                                    userEvent.VisitDate = evnt.VisitDate;
                                    userEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                                    userEvent.Status = evnt.Status;
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    var newUserEvent = new UserEvent
                                    {
                                        EventId = incident.Id,
                                        UserId = incident.UserId,
                                        PatientId = incident.PatientId,
                                        EpisodeId = incident.EpisodeId,
                                        Status = incident.Status.ToString(),
                                        Discipline = Disciplines.ReportsAndNotes.ToString(),
                                        EventDate = evnt.EventDate,
                                        VisitDate = evnt.VisitDate,
                                        DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
                                    };
                                    patientRepository.AddNewUserEvent(Current.AgencyId, incident.PatientId, newUserEvent);
                                }
                                if (evnt.Status.IsInteger()) Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.IncidentAccidentReport, string.Empty);
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Error in updating the data. Try again.";
                                return Json(viewData);
                            }
                        }
                        else patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                    }
                    else patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                }
                else patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                if (!agencyRepository.UpdateIncident(incident))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Incident could not be updated.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Incident was updated successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Incident/Print", agencyService.GetIncidentReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult IncidentReportPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new IncidentReportPdf(agencyService.GetIncidentReportPrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=IncidentLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessIncident(string button, Guid patientId, Guid eventId)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Incident/Accident Log could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (agencyService.ProcessIncidents(button, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Log has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Log could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (agencyService.ProcessIncidents(button, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Log has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Log could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Visit Rate Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitRates()
        {
            return PartialView("VisitRate", agencyRepository.GetMainLocation(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRateContent(Guid branchId)
        {
            var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (location != null)
            {
                if (!location.IsLocationStandAlone)
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        int payor;
                        if (int.TryParse(agency.Payor, out payor))
                        {
                            var medicareInsurance = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payor);
                            if (medicareInsurance != null)
                            {
                                location.MedicareInsurance = medicareInsurance;
                            }
                            var insurance = lookupRepository.GetInsurance(payor);
                            if (insurance != null)
                            {
                                location.InsuranceName = insurance.Name;
                                location.SubmitterId = agency.SubmitterId;
                                location.SubmitterName = agency.SubmitterName;
                                location.SubmitterPhone = agency.SubmitterPhone;
                                location.ContactPersonFirstName = agency.ContactPersonFirstName;
                                location.ContactPersonLastName = agency.ContactPersonLastName;
                                location.ContactPersonPhone = agency.ContactPersonPhone;
                            }
                        }
                    }
                }
                else
                {
                    int payor;
                    if (int.TryParse(location.Payor, out payor))
                    {
                        var medicareInsurance = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payor);
                        if (medicareInsurance != null)
                        {
                            location.MedicareInsurance = medicareInsurance;
                        }
                        var insurance = lookupRepository.GetInsurance(payor);
                        if (insurance != null)
                        {
                            location.InsuranceName = insurance.Name;
                            location.SubmitterId = location.SubmitterId;
                        }
                    }
                }
            }
            return PartialView("VisitRateContent", location);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditCost(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData();
            //var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0 && keys.Contains("AgencyLocationId") && formCollection["AgencyLocationId"].IsNotNullOrEmpty())
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, formCollection["AgencyLocationId"].ToGuid());
                if (agencyLocation != null)
                {
                    if (keys.Contains("Ub04Locator81"))
                    {
                        var locatorList = formCollection["Ub04Locator81"].ToArray();
                        var locators = new List<Locator>();
                        if (locatorList != null && locatorList.Length > 0)
                        {
                            locatorList.ForEach(l =>
                            {
                                if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                {
                                    locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                }
                            });
                        }
                        agencyLocation.Ub04Locator81cca = locators.ToXml();
                    }
                    int payor = agencyLocation.Payor.IsNotNullOrEmpty() && agencyLocation.Payor.IsInteger() ? agencyLocation.Payor.ToInteger() : -1;
                    var agencyLocationMedicare = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payor);
                    bool agencyLocationMedicareExists = agencyLocationMedicare != null;
                    if (!agencyLocationMedicareExists)
                    {
                        agencyLocationMedicare = new AgencyMedicareInsurance();
                    }
                    agencyLocationMedicare.AddressLine1 = keys.Contains("MedicareAddressLine1") ? formCollection["MedicareAddressLine1"] : string.Empty;
                    agencyLocationMedicare.AddressLine2 = keys.Contains("MedicareAddressLine2") ? formCollection["MedicareAddressLine2"] : string.Empty;
                    agencyLocationMedicare.AddressCity = keys.Contains("MedicareAddressCity") ? formCollection["MedicareAddressCity"] : string.Empty;
                    agencyLocationMedicare.AddressStateCode = keys.Contains("MedicareAddressStateCode") ? formCollection["MedicareAddressStateCode"] : string.Empty;
                    agencyLocationMedicare.AddressZipCode = keys.Contains("MedicareAddressZipCode") ? formCollection["MedicareAddressZipCode"] : string.Empty;
                    agencyLocationMedicare.AgencyId = Current.AgencyId;
                    agencyLocationMedicare.MedicareId = agencyLocation.Payor.IsNotNullOrEmpty() && agencyLocation.Payor.IsInteger() ? agencyLocation.Payor.ToInteger() : 0;
                    //if (keys.Contains("RateDiscipline"))
                    //{
                    //var disciplineList = formCollection["RateDiscipline"].ToArray();
                    //var visitRatesList = new List<CostRate>();
                    //disciplineList.ForEach(l =>
                    //{
                    //    if (keys.Contains(l + "_PerUnit"))
                    //    {
                    //        visitRatesList.Add(new CostRate { RateDiscipline = l, PerUnit = formCollection[l + "_PerUnit"] });
                    //        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerUnit"]) ? !formCollection[l + "_PerUnit"].IsDouble() : false, "Wrong entry"));
                    //    }
                    //});
                    //var entityValidator = new EntityValidator(rules.ToArray());
                    //entityValidator.Validate();
                    //if (entityValidator.IsValid)
                    //{
                    //    agencyLocation.Cost = visitRatesList.ToXml();
                    //}
                    //else
                    //{
                    //    viewData.isSuccessful = false;
                    //    viewData.errorMessage = "One of the cost rate is not in correct format";
                    //    return Json(viewData);
                    //}

                    if (agencyRepository.EditBranchCost(agencyLocation))
                    {
                        if (agencyLocationMedicareExists)
                        {
                            agencyRepository.UpdateAgencyLocationMedicare(agencyLocationMedicare);
                        }
                        else
                        {
                            agencyRepository.AddAgencyLocationMedicare(agencyLocationMedicare);
                        }
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Branch cost rates was successfully edited.";

                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Branch cost rates could not be updated.";
                    }
                    //}
                    //else
                    //{
                    //    viewData.isSuccessful = false;
                    //    viewData.errorMessage = "Could not update the branch cost rates.";
                    //}
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected branch don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return Json(viewData);
        }

        #endregion

        #region Non Visit Tasks
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewNonVisitTask()
        {
            return PartialView("NonVisitTask/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonVisitTask([Bind] AgencyNonVisit nonVisitTask)
        {
            Check.Argument.IsNotNull(nonVisitTask, "NonVisitTask");
            var viewData = new JsonViewData();
            if (nonVisitTask.IsValid)
            {
                nonVisitTask.AgencyId = Current.AgencyId;
                nonVisitTask.Id = Guid.NewGuid();
                if (!agencyRepository.AddNonVisitTask(nonVisitTask))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the Non-Visit Task";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, nonVisitTask.Id.ToString(), LogType.AgencyNonVisit, LogAction.AgencyNonVisitAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Non-Visit Task added Successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = nonVisitTask.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonVisitTasks()
        {
            return PartialView("NonVisitTask/List");
        }

        [GridAction]
        public ActionResult NonVisitTaskList()
        {
            return View(new GridModel(agencyRepository.GetNonVisitTasks(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNonVisitTask(Guid id)
        {
            return PartialView("NonVisitTask/Edit", agencyRepository.GetNonVisitTask(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonVisitTaskLogs(Guid nonvisittaskId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyNonVisit, Current.AgencyId, nonvisittaskId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateNonVisitTask([Bind] AgencyNonVisit nonVisitTask)
        {
            Check.Argument.IsNotNull(nonVisitTask, "nonvisittask");

            var viewData = new JsonViewData();

            if (nonVisitTask.IsValid)
            {
                var existingNonVisitTask = agencyRepository.GetNonVisitTask(Current.AgencyId, nonVisitTask.Id);
                if (existingNonVisitTask != null)
                {
                    existingNonVisitTask.Text = nonVisitTask.Text;
                    existingNonVisitTask.Title = nonVisitTask.Title;
                    if (!agencyRepository.UpdateNonVisitTask(existingNonVisitTask))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the Non-Visit Task.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, nonVisitTask.Id.ToString(), LogType.AgencyNonVisit, LogAction.AgencyNonVisitUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Non-Visit Task was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Non-Visit Task don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = nonVisitTask.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteNonVisitTask(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Template could not be deleted. Please try again." };
            if (agencyRepository.DeleteNonVisitTask(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyNonVisit, LogAction.AgencyNonVisitDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Non-Visit Task has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Non-Visit Task could not be deleted.";
            }
            return Json(viewData);
        }
        #endregion

        #region Non Visit Task Manager
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewUserNonVisitTask()
        {
            return PartialView("NonVisitTaskManager/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUserNonVisitTask(Guid id)
        {
            return PartialView("NonVisitTaskManager/EditModal", agencyRepository.GetUserNonVisitTask(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateUserNonVisitTask([Bind] UserNonVisitTask userNonVisitTask)
        {
            Check.Argument.IsNotNull(userNonVisitTask, "UserNonVisitTask");

            var viewData = new JsonViewData();

            if (userNonVisitTask.IsValid)
            {
                var existingNonVisitTask = agencyRepository.GetUserNonVisitTask(Current.AgencyId, userNonVisitTask.Id);
                if (existingNonVisitTask != null)
                {
                    existingNonVisitTask.UserId = userNonVisitTask.UserId;
                    existingNonVisitTask.TaskDate = userNonVisitTask.TaskDate;
                    existingNonVisitTask.TaskId = userNonVisitTask.TaskId;
                    
                    existingNonVisitTask.PaidStatus = userNonVisitTask.PaidStatus;
                    if (existingNonVisitTask.PaidStatus == true)
                    {
                        existingNonVisitTask.PaidDate = userNonVisitTask.PaidDate;
                    }
                    else 
                    {
                        existingNonVisitTask.PaidDate = DateTime.MinValue;
                    }
                    existingNonVisitTask.TimeIn = userNonVisitTask.TimeIn;
                    existingNonVisitTask.TimeOut = userNonVisitTask.TimeOut;
                    existingNonVisitTask.Comments = userNonVisitTask.Comments;
                    if (!agencyRepository.UpdateUserNonVisitTask(existingNonVisitTask))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the User Non-Visit Task.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userNonVisitTask.Id.ToString(), LogType.UserNonVisitTask, LogAction.UserNonVisitTaskUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User Non-Visit Task was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected User Non-Visit Task don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userNonVisitTask.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteUserNonVisitTask(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Non-Visit Task could not be deleted. Please try again." };
            if (agencyRepository.DeleteUserNonVisitTask(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.UserNonVisitTask, LogAction.UserNonVisitTaskDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User Non-Visit Task has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User Non-Visit Task could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserNonVisitTaskLogs(Guid usernonvisittaskId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.UserNonVisitTask, Current.AgencyId, usernonvisittaskId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddUserNonVisitTask(List<UserNonVisitTask> userNonVisitTaskList)
        {
            var viewData = new JsonViewData();
            if (userNonVisitTaskList.IsNotNullOrEmpty())
            {
                foreach (var userNonVisitTask in userNonVisitTaskList)
                {
                    if (userNonVisitTask.UserId.IsNotEmpty() && userNonVisitTask.TaskId.IsNotEmpty() && !userNonVisitTask.TaskDate.IsNull() && !userNonVisitTask.TimeIn.IsNull() && !userNonVisitTask.TimeOut.IsNull())
                    {
                        userNonVisitTask.Id = Guid.NewGuid();
                        userNonVisitTask.AgencyId = Current.AgencyId;
                        if (!agencyRepository.AddUserNonVisitTask(userNonVisitTask))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error saving the User Non-Visit Task";
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userNonVisitTask.Id.ToString(), LogType.UserNonVisitTask, LogAction.UserNonVisitTaskAdded, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User Non-Visit Task added Successfully";
                        }
                    }
                }
            }

            if (viewData.isSuccessful.IsNull() || viewData.errorMessage.IsNullOrEmpty())
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error saving the User Non-Visit Task";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserNonVisitTasks()
        {
            return PartialView("NonVisitTaskManager/List");
        }

        [GridAction]
        public ActionResult UserNonVisitTaskList()
        {
            return View(new GridModel(agencyService.GetUserNonVisitTasks(Current.AgencyId)));
        }
        #endregion

        #region Template Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Templates()
        {
            return PartialView("Template/List");
        }

        [GridAction]
        public ActionResult TemplateList()
        {
            return View(new GridModel(agencyRepository.GetTemplates(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewTemplate()
        {
            return PartialView("Template/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");

            var viewData = new JsonViewData();

            if (template.IsValid)
            {
                template.AgencyId = Current.AgencyId;
                template.Id = Guid.NewGuid();
                if (!agencyRepository.AddTemplate(template))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the template.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Template was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditTemplate(Guid id)
        {
            return PartialView("Template/Edit", agencyRepository.GetTemplate(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");

            var viewData = new JsonViewData();

            if (template.IsValid)
            {
                var existingTemplate = agencyRepository.GetTemplate(Current.AgencyId, template.Id);
                if (existingTemplate != null)
                {
                    existingTemplate.Text = template.Text;
                    existingTemplate.Title = template.Title;
                    if (!agencyRepository.UpdateTemplate(existingTemplate))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the template.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Template was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected template don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Template could not be deleted. Please try again." };
            if (agencyRepository.DeleteTemplate(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Template has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Template could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var template = agencyRepository.GetTemplate(Current.AgencyId, id);
            if (template != null)
            {
                return Json(template);
            }
            return Json(new AgencyTemplate());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateLogs(Guid templateId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyTemplate, Current.AgencyId, templateId.ToString()));
        }

        #endregion

        #region Supply Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SuppliesSearch(string term, int limit)
        {
            var supplies = agencyRepository.GetSupplies(Current.AgencyId, term, limit).Select(p => new { p.Description, p.Code, p.Id, p.RevenueCode, p.UnitCost }).ToList();
            return Json(supplies);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Supplies()
        {
            return PartialView("Supply/List");
        }

        [GridAction]
        public ActionResult SupplyList()
        {
            return View(new GridModel(agencyRepository.GetSupplies(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewSupply()
        {
            return PartialView("Supply/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddSupply([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");

            var viewData = new JsonViewData();

            if (supply.IsValid)
            {
                supply.Id = Guid.NewGuid();
                supply.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddSupply(supply))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the supply.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Supply was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid id)
        {
            return PartialView("Supply/Edit", agencyRepository.GetSupply(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSupply([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");

            var viewData = new JsonViewData();

            if (supply.IsValid)
            {
                var existingSupply = agencyRepository.GetSupply(Current.AgencyId, supply.Id);
                if (existingSupply != null)
                {
                    existingSupply.Code = supply.Code;
                    existingSupply.Description = supply.Description;
                    existingSupply.RevenueCode = supply.RevenueCode;
                    existingSupply.UnitCost = supply.UnitCost;
                    if (!agencyRepository.UpdateSupply(existingSupply))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the supply.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supply was updated successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected supply does not exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteSupply(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply could not be deleted. Please try again." };
            if (agencyRepository.DeleteSupply(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Supply has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Supply could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetSupply(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var supply = agencyRepository.GetSupply(Current.AgencyId, id);
            if (supply != null)
            {
                return Json(supply);
            }
            return Json(new AgencySupply());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyLogs(Guid supplyId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencySupply, Current.AgencyId, supplyId.ToString()));
        }

        #endregion

        #region Adjustment Code Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdjustmentCodes()
        {
            return PartialView("AdjustmentCode/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdjustmentCodeList()
        {
            return View(new GridModel(agencyRepository.GetAdjustmentCodes(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAdjustmentCode()
        {
            return PartialView("AdjustmentCode/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            Check.Argument.IsNotNull(adjustmentCode, "code");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the code." };
            if (adjustmentCode.IsValid)
            {
                adjustmentCode.AgencyId = Current.AgencyId;
                adjustmentCode.Id = Guid.NewGuid();
                if (agencyRepository.AddAdjustmentCode(adjustmentCode))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Code was saved successfully";
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, adjustmentCode.Id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeAdded, string.Empty);
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditAdjustmentCode(Guid id)
        {
            return PartialView("AdjustmentCode/Edit", agencyRepository.FindAdjustmentCode(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            Check.Argument.IsNotNull(adjustmentCode, "code");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the code." };

            if (adjustmentCode.IsValid)
            {
                var existingCode = agencyRepository.FindAdjustmentCode(Current.AgencyId, adjustmentCode.Id);
                if (existingCode != null)
                {
                    existingCode.Code = adjustmentCode.Code;
                    existingCode.Description = adjustmentCode.Description;
                    if (agencyRepository.UpdateAdjustmentCode(existingCode))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Adjustment Code was updated successfully";
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, adjustmentCode.Id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeUpdated, string.Empty);
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteAdjustmentCode(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Adjustment Code could not be deleted. Please try again." };

            if (agencyRepository.DeleteAdjustmentCode(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Adjustment Code was updated successfully";
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeDeleted, string.Empty);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdjustmentCodeLogs(Guid adjustmentCodeId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyAdjustmentCode, Current.AgencyId, adjustmentCodeId.ToString()));
        }

        #endregion

        #region UploadTypes

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UploadTypes()
        {
            return PartialView("UploadType/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadTypeGrid()
        {
            return View(new GridModel(agencyRepository.GetUploadType(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewUploadType()
        {
            return PartialView("UploadType/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddUploadType(UploadType uploadType)
        {
            Check.Argument.IsNotNull(uploadType, "uploadType");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the upload type." };
            if (uploadType.IsValid)
            {
                uploadType.AgencyId = Current.AgencyId;
                uploadType.Id = Guid.NewGuid();
                if (agencyRepository.AddUploadType(uploadType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Upload type was saved successfully";
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, uploadType.Id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeAdded, string.Empty);
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUploadType(Guid id)
        {
            return PartialView("UploadType/Edit", agencyRepository.FindUploadType(Current.AgencyId, id));
        }

        public JsonResult UpdateUploadType(UploadType uploadType)
        {
            Check.Argument.IsNotNull(uploadType, "uploadType");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the Upload Type." };

            if (uploadType.IsValid)
            {
                var existingUploadType = agencyRepository.FindUploadType(Current.AgencyId, uploadType.Id);
                if (existingUploadType != null)
                {
                    existingUploadType.Type = uploadType.Type;
                    if (agencyRepository.UpdateUploadType(existingUploadType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Upload Type updated successfully";
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, uploadType.Id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeUpdated, string.Empty);
                    }
                }
            }
            return Json(viewData);
        }

        public JsonResult DeleteUploadType(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Upload Type could not be deleted. Please try again." };

            if (agencyRepository.DeleteUploadType(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Upload Type was deleted successfully";
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeDeleted, string.Empty);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadTypeLogs(Guid uploadTypeId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyUploadType, Current.AgencyId, uploadTypeId.ToString()));
        }

        #endregion

        #region License
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LicenseManager()
        {
            return PartialView("License/Manager", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseList()
        {
            return PartialView("License/List", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LicenseNew()
        {
            return PartialView("License/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseAdd([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "license");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be added." };
            var entityValidator = new EntityValidator(
               new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
               new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
               new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
            );
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                if (userService.AddLicenseItem(licenseItem, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "License added successfully";
                }
            }
            else
            {
                viewData.errorMessage = entityValidator.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseEdit(Guid Id, Guid UserId)
        {
            var licenseItem = new LicenseItem();
            if (UserId.IsEmpty())
            {
                licenseItem = userRepository.GetNonUserLicense(Id, Current.AgencyId);
            }
            else
            {
                licenseItem = userRepository.GetUserLicenseItem(Id, UserId, Current.AgencyId);
            }
            return PartialView("License/Edit", licenseItem);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseUpdate([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "licenseItem");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The License could not be updated. Please try again." };
            if (licenseItem != null)
            {
                if (!licenseItem.UserId.IsEmpty())
                {
                    if (userService.UpdateLicense(licenseItem.Id, licenseItem.UserId, licenseItem.IssueDate, licenseItem.ExpireDate))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.UserId.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The License was updated successfully.";
                    }
                }
                else
                {
                    var entityValidator = new EntityValidator(
                       new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
                       new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
                       new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
                    );
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        licenseItem.AgencyId = Current.AgencyId;
                        if (userRepository.UpdateNonUserLicense(licenseItem))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The License was updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseDelete(Guid Id, Guid UserId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            string messageStart = UserId.IsEmpty() ? "Non-Software User license" : "User license";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("{0} cannot be deleted. Try Again.", messageStart) };
            if (userService.DeleteLicense(Id, UserId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = string.Format("{0} has been successfully deleted.", messageStart);
                return Json(viewData);
            }
            return Json(viewData);
        }
        #endregion

        #region Teams
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CareTeamList()
        {
            return PartialView("CareTeam/List", agencyRepository.GetTeamsWithUserToolTip(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CareTeamNew()
        {
            return PartialView("CareTeam/New", userRepository.GetAgencyUsers(Current.AgencyId).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamAdd([Bind] AgencyTeam team)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Team could not be saved." };
            if (team.IsValid)
            {
                if (team.UserArray != null && team.UserArray.Count > 0)
                {
                    team.Users = team.UserArray.ToXml();
                }
                team.AgencyId = Current.AgencyId;
                team.Id = Guid.NewGuid();
                if (agencyRepository.AddTeam(team))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Team saved successfully"; 
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CareTeamEditPatients(Guid id)
        {
            return PartialView("CareTeam/EditPatients", agencyRepository.GetTeam(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamPatientData(Guid id)
        {
            var patients = agencyRepository.GetTeamAccessPatientData(Current.AgencyId, id);
            return Json(patients);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamPatientMove(bool left, Guid id, List<Guid> movedItems)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Unable to update team access." };
            if (left) {
                if (agencyRepository.AddPatientsToTeam(id, movedItems))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Patient added successfully.";
                }
            }
            else
            {
                if (agencyRepository.RemovePatientsFromTeam(id, movedItems))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Patients removed successfully.";
                }

            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CareTeamEditUsers(Guid id)
        {
            return PartialView("CareTeam/EditUsers", agencyRepository.GetTeam(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamUserData(Guid id)
        {
            var patients = agencyRepository.GetTeamAccessUserData(Current.AgencyId, id, userRepository.GetAgencyUsers(Current.AgencyId).ToList());
            return Json(patients);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamUserMove(bool left, Guid id, List<Guid> movedItems)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Unable to update team access." };
            if (left)
            {
                if (agencyRepository.AddUsersToTeam(Current.AgencyId, id, movedItems))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Users were added successfully.";
                }
            }
            else
            {
                if (agencyRepository.RemoveUsersFromTeam(Current.AgencyId, id, movedItems))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Users were removed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CareTeamDelete(Guid id)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error deleting the team." };
            if (agencyRepository.DeleteTeam(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Team deleted successfully";
            }
            return Json(viewData);
        }
        #endregion
    }
}
