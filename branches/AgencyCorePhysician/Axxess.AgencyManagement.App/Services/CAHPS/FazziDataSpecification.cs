﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    internal class FazziDataSpecification : IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal FazziDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<Dictionary<string, string>> GetItems(List<int> paymentSources)
        {
            var result = new List<Dictionary<string, string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);

                List<EpisodeData> servedEpisodes = null;
                if (paymentSources != null && paymentSources.Count > 0)
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast, paymentSources);
                }
                else
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast);
                }
                if (servedEpisodes != null && servedEpisodes.Count > 0)
                {
                    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                    if (patientIds != null && patientIds.Count > 0)
                    {
                        var totalServed = patientIds.Count;
                        patientIds.ForEach(patientId =>
                        {
                            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                            if (episodes != null && episodes.Count > 0)
                            {
                                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);

                                var patientData = episodes.FirstOrDefault();
                                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                                if (assessment != null)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var collection = new Dictionary<string, string>();

                                    collection.Add("SampleMonth", sampleMonthFirst.ToString("MM"));
                                    collection.Add("SampleYear", sampleYear.ToString());
                                    collection.Add("LocationCode", agency.Name.IsNotNullOrEmpty() ? agency.Name : "M");//
                                    collection.Add("CMSProviderID", agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "M");
                                    collection.Add("NPINumberID", agency.NationalProviderNumber.IsNotNullOrEmpty() ? agency.NationalProviderNumber : "M");

                                    collection.Add("TotalServed", totalServed.ToString());

                                    collection.Add("FirstName", assessmentData.AnswerOrDefault("M0040FirstName", "M"));
                                    collection.Add("MiddleInitial", assessmentData.AnswerOrEmptyString("M0040MI"));
                                    collection.Add("LastName", assessmentData.AnswerOrDefault("M0040LastName", "M"));
                                    collection.Add("Birthday", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MMddyyyy") : "M");
                                    collection.Add("Gender", assessmentData.AnswerOrDefault("M0069Gender", "M"));

                                    collection.Add("Address1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : "M");
                                    collection.Add("Address2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : "M");
                                    collection.Add("City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : "M");
                                    collection.Add("State", assessmentData.AnswerOrDefault("M0050PatientState", "M"));
                                    collection.Add("ZipCode", assessmentData.AnswerOrDefault("M0060PatientZipCode", "M"));
                                    collection.Add("Telephone", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : "M");
                                    collection.Add("MedicalRecord", assessmentData.AnswerOrDefault("M0020PatientIdNumber", "M"));
                                    collection.Add("Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));

                                    collection.Add("SkilledVisits", skilledVisitCount);
                                    collection.Add("LookbackVisits", lookbackVisitCount);

                                    collection.Add("MedicarePayer", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "M");
                                    collection.Add("MedicaidPayer", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "M");
                                    collection.Add("HMOIndicator", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("PrivatePayer", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "M");
                                    collection.Add("OtherPayerSource", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "M");

                                    collection.Add("AdminSourceHospital", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") || assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdminSourceRehab", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdminSourceSkilled", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdminSourceNursing", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdminSourceInpatient", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdminSourceCommunity", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                                    collection.Add("CMSDualEligibility", CheckForDualElligibility(assessmentData));

                                    collection.Add("ICD9PrimaryDiagnosis", assessmentData.AnswerOrDefault("M1020ICD9M", "M").Replace(".", ""));
                                    collection.Add("ICD9OtherDiagnosis1", assessmentData.AnswerOrDefault("M1022ICD9M1", "M").Replace(".", ""));
                                    collection.Add("ICD9OtherDiagnosis2", assessmentData.AnswerOrDefault("M1022ICD9M2", "M").Replace(".", ""));
                                    collection.Add("ICD9OtherDiagnosis3", assessmentData.AnswerOrDefault("M1022ICD9M3", "M").Replace(".", ""));
                                    collection.Add("ICD9OtherDiagnosis4", assessmentData.AnswerOrDefault("M1022ICD9M4", "M").Replace(".", ""));
                                    collection.Add("ICD9OtherDiagnosis5", assessmentData.AnswerOrDefault("M1022ICD9M5", "M").Replace(".", ""));

                                    collection.Add("SurgicalDischarge", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "0");
                                    collection.Add("EndStageRenalDisease)", CheckForEndStageRenalDisiease(assessmentData));

                                    collection.Add("DressUpperADL", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : "M");
                                    collection.Add("DressLowerADL", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : "M");
                                    collection.Add("BathingADL", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : "M");
                                    collection.Add("ToiletingADL", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : "M");
                                    collection.Add("TransferringADL", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : "M");

                                    result.Add(collection);
                                }
                            }
                        });
                    }
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "EN";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "ES";
                }
                else if (language.ToLowerInvariant().Contains("chinese"))
                {
                    result = "ZH";
                }
                else if (language.ToLowerInvariant().Contains("russian"))
                {
                    result = "RU";
                }
                else if (language.ToLowerInvariant().Contains("vietnamese"))
                {
                    result = "VI";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "0";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        private string CheckForDualElligibility(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if ((data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1"))
                   && (data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1")))
                {
                    result = "1";
                }
            }

            return result;
        }

        #endregion
    }
}
