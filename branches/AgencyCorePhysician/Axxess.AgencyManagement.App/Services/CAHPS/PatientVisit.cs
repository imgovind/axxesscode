﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    public class PatientVisit
    {
        public PatientVisit() { AdmissionIds = new List<Guid>(); }
        public int Visits { get; set; }
        public int Patients { get; set; }
        public List<Guid> AdmissionIds { get; set; }
    }
}
