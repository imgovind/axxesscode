﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    public class AgeRange
    {
        public AgeRange() { List = new List<Guid>(); }
        public int Count { get; set; }
        public List<Guid> List { get; set; }
    }
}
