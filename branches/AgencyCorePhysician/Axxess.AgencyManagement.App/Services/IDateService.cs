﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.Core;

    public interface IDateService
    {
        DateRange GetDateRange(string rangeIdentifier, Guid patientId);
    }
}
