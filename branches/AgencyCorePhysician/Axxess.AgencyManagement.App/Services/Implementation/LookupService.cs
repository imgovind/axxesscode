﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.Core;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class LookupService : ILookupService
    {
        #region Constructor/Private Members

        private readonly ILookupRepository lookupRepository;

        public LookupService(ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region ILookupService Implementation

        public double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode, string agencyZipCode, string stateCode)
        {
            Double result = lookupRepository.GetProspectivePaymentAmount(hippsCode, time, zipCode);
            if (result == 0)
            {
                result = lookupRepository.GetProspectivePaymentAmount(hippsCode, time, agencyZipCode);
                if (result == 0)
                {
                    result = lookupRepository.GetRuralProspectivePaymentAmount(hippsCode, time, stateCode);
                }
            }
            return result;
        }

        public ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode, string agencyZipCode, string stateCode)
        {
            ProspectivePayment result = lookupRepository.GetProspectivePayment(hippsCode, time, zipCode);
            if (result == null)
            {
                result = lookupRepository.GetProspectivePayment(hippsCode, time, agencyZipCode);
                if (result == null)
                {
                    result = lookupRepository.GetRuralProspectivePayment(hippsCode, time, stateCode);
                }
            }
            return result;
        }

        public string GetInsuranceName(int id) 
        {
            string outResult = default(string);

            Insurance insure = new Insurance();

            insure = lookupRepository.GetInsurance(id);

            if (insure != null)
            {
                outResult = insure.Name;
            }
            return outResult;
        }

        #endregion
    }
}
