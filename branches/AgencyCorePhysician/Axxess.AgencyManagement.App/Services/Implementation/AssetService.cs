﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Security.Cryptography;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;

    public class AssetService : IAssetService
    {
        #region Constructor

        private readonly IAssetRepository assetRespository;

        public AssetService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assetRespository = agencyManagementDataProvider.AssetRepository;
        }

        #endregion

        #region IAssetService Members

        public bool AddAsset(string fileName, string contentType, byte[] bytes)
        {
            bool result = false;
            var asset = new Asset
            {
                Bytes = bytes,
                FileName = fileName,
                IsDeprecated = false,
                ContentType = contentType,
                AgencyId = Current.AgencyId,
                FileSize = bytes.Length.ToString()
            };
            if (CoreSettings.UseDBAssets)
            {
                if (assetRespository.Add(asset))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                if (HttpUpload(CoreSettings.AssetUploadURL, asset))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool AddAsset(HttpPostedFileBase file)
        {
            Check.Argument.IsNotNull(file, "file");

            bool result = false;
            Asset asset = null;
            file.InputStream.Position = 0;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                asset = new Asset
                {
                    IsDeprecated = false,
                    FileName = file.FileName,
                    AgencyId = Current.AgencyId,
                    ContentType = file.ContentType,
                    FileSize = file.ContentLength.ToString(),
                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                };
            }

            if (CoreSettings.UseDBAssets)
            {
                if (assetRespository.Add(asset))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                if (HttpUpload(CoreSettings.AssetUploadURL, asset))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public bool AddAsset(Asset asset)
        {
            bool result = false;
            if (CoreSettings.UseDBAssets)
            {
                if (assetRespository.Add(asset))
                {
                    result = true;
                }
            }
            else
            {
                if (HttpUpload(CoreSettings.AssetUploadURL, asset))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteAsset(Guid assetId)
        {
            return assetRespository.Delete(assetId);
        }

        public bool RemoveAsset(Guid assetId)
        {
            var result = false;
            if (assetRespository.Delete(assetId))
            {
                result = true;
                AssetHelper.HttpUploadOrDelete(Current.AgencyId, assetId,CoreSettings.AssetDeleteURL,new Byte[0], string.Empty,string.Empty);
            }
            return result;
        }

        public bool AddPatientDocument(Asset asset, PatientDocument patientDocument)
        {
            bool result = false;
            if (asset != null)
            {
                if (CoreSettings.UseDBAssets)
                {
                    if (assetRespository.Add(asset))
                    {
                        result = true;
                    }
                }
                else
                {
                    if (HttpUpload(CoreSettings.AssetUploadURL, asset))
                    {
                        result = true;
                    }
                }

            }
            assetRespository.AddPatientDocument(asset, patientDocument);
            return result;
        }

        #endregion

        #region Private Methods

        private bool HttpUpload(string url, Asset asset)
        {
           
            var result = false;
            byte[] temp = asset.Bytes;

            if (!CoreSettings.StoreAssetBytesInDB)
            {
                asset.Bytes = new Byte[1];
            }

            if (assetRespository.Add(asset))
            {
                return AssetHelper.HttpUploadOrDelete(asset.AgencyId, asset.Id, url, temp, asset.ContentType, asset.FileName);
            }
            return result;
        }

        #endregion
    }
}
