﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.LookUp.Domain;

    public interface ILookupService
    {
        double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode, string agencyZipCode, string stateCode);
        ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode, string agencyZipCode, string stateCode);
        string GetInsuranceName(int id);
    }
}
