﻿namespace Axxess.AgencyManagement.App.Enums
{
    public enum OrderType : int
    {
        PhysicianOrder = 1,
        HCFA485 = 2,
        HCFA486 = 3,
        HCFA485StandAlone = 4,
        FaceToFaceEncounter = 5,
        PtEvaluation = 6,
        PtReEvaluation = 7,
        OtEvaluation = 8,
        OtReEvaluation = 9,
        StEvaluation = 10,
        StReEvaluation = 11,
        MSWEvaluation = 12,
        PTDischarge=13,
        SixtyDaySummary=14,
        PtReassessment = 15,
        OtReassessment = 16,
        OtDischarge=17,
        MSWDischarge=18,
        StDischarge=19,
        PtPlanOfCare=20,
        OtPlanOfCare=21,
        StPlanOfCare=22,
        SnPsychAssessment=23,
        ThirtyDaySummary = 24,
        TenDaySummary = 25
    }
}
