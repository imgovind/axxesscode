﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;
    using Axxess.LookUp.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class PatientsAndVisitsByAgeExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid agencyId;
        private Guid branchId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();
        private static readonly ILookUpDataProvider lookupDataProvider = new LookUpDataProvider();
        private static readonly ILookupService lookupService = new LookupService(lookupDataProvider);
        private static readonly IAgencyManagementDataProvider agencyManagementDataProvider = new AgencyManagementDataProvider();
        private static readonly IAssessmentService assessmentService = new AssessmentService(oasisDataProvider, agencyManagementDataProvider, lookupDataProvider, lookupService, assetService);
        private static readonly IAssetService assetService = new AssetService(agencyManagementDataProvider);
        private static readonly Axxess.AgencyManagement.App.Services.IReportService reportServiceLocal = new Axxess.AgencyManagement.App.Services.ReportService(agencyManagementDataProvider, oasisDataProvider, lookupService, assessmentService);
        
        public PatientsAndVisitsByAgeExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)

            : base()
        {
            this.endDate = endDate;
            this.startDate = startDate;
            this.branchId = branchId;
            this.agencyId = agencyId;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "PatientsAndVisitsByAge.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients And Visits By Age";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PatientsAndVisitsByAge");

            var agency = agencyRepository.GetAgencyOnly(agencyId);
            var agencyName = agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty;
            //List<PatientsAndVisitsByAgeResult> data = reportAgent.PatientsAndVisitsByAge(agencyId, branchId, startDate, endDate);

            List<PatientsAndVisitsByAgeResult> data = reportServiceLocal.PatientsAndVisitsByAgeAndGender(agencyId, branchId, startDate, endDate);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 2;

                var titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(agencyName);
                titleRow.CreateCell(1).SetCellValue("Patients & Visits By Age Report");
                titleRow.CreateCell(2).SetCellValue(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

                var headerRow = sheet.CreateRow(1);
                headerRow.CreateCell(0).SetCellValue("Line No.");
                headerRow.CreateCell(1).SetCellValue("Age");
                headerRow.CreateCell(2).SetCellValue("Patients");
                headerRow.CreateCell(3).SetCellValue("Visits");

                data.ForEach(item =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    dataRow.CreateCell(0).SetCellValue(item.LineNumber);
                    dataRow.CreateCell(1).SetCellValue(item.Description);
                    dataRow.CreateCell(2).SetCellValue(item.Patients);
                    dataRow.CreateCell(3).SetCellValue(item.Visits);
                    rowIndex++;
                });

                workBook.FinishWritingToExcelSpreadsheet(4);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Age Information found!");
            }
        }

        #endregion

    }
}
