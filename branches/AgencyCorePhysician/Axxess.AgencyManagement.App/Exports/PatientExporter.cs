﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PatientExporter : BaseExporter
    {
        private IList<PatientData> patients;
        public PatientExporter(IList<PatientData> patients)
            : base()
        {
            this.patients = patients;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            ISheet sheet = base.workBook.CreateSheet("Patients");
           
            ICellStyle dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            IRow headerRow = sheet.CreateRow(1);
            //headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Insurance");
            //headerRow.CreateCell(2).SetCellValue("Policy #");
            //headerRow.CreateCell(3).SetCellValue("Last Name");
            //headerRow.CreateCell(4).SetCellValue("First Name");
            //headerRow.CreateCell(5).SetCellValue("Middle Initial");
            //headerRow.CreateCell(2).SetCellValue("Phone");
            //headerRow.CreateCell(7).SetCellValue("Mobile Phone");
            headerRow.CreateCell(2).SetCellValue("Address");
            //headerRow.CreateCell(9).SetCellValue("Address 2");
            //headerRow.CreateCell(10).SetCellValue("City");
            //headerRow.CreateCell(11).SetCellValue("State");
            //headerRow.CreateCell(12).SetCellValue("Zip Code");
            headerRow.CreateCell(3).SetCellValue("DOB");
            headerRow.CreateCell(4).SetCellValue("Gender");
            headerRow.CreateCell(5).SetCellValue("Email Address");
            headerRow.CreateCell(6).SetCellValue("DNR");
            int count = 2;
            if (this.patients.Count > 0)
            {
                
                this.patients.ForEach(p =>
                {
                    IRow dataRow = sheet.CreateRow(count);
                    string patientDisplay = string.Format("{0}\nMRN: {1}", p.DisplayName, p.PatientIdNumber);
                    dataRow.CreateCell(0).SetCellValue(patientDisplay);
                    string insurnace = string.Format("{0}\n{1}", p.InsuranceName, p.PolicyNumber);
                    dataRow.CreateCell(1).SetCellValue(insurnace);
                    string phone = p.Phone;
                    if (p.PhoneMobile.IsNotNullOrEmpty())
                    {
                        phone += (phone.IsNotNullOrEmpty() ? "\n" : string.Empty) + "Mobile:" + p.PhoneMobile;
                    }
                    dataRow.CreateCell(2).SetCellValue(p.AddressFullFormatted + (phone.IsNotNullOrEmpty() ?  "\n" + phone : string.Empty));
                    if (p.DateOfBirth != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(p.DateOfBirth);
                    }
                    else
                    {
                        dataRow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(4).SetCellValue(p.Gender);
                    dataRow.CreateCell(5).SetCellValue(p.EmailAddress);
                    dataRow.CreateCell(6).SetCellValue(p.IsDNR ? "Yes" : "No");
                    count++;
                });
                var totalRow = sheet.CreateRow(count + 2);
                var totalCell = totalRow.CreateCell(0);
                totalCell.SetCellValue(string.Format("Total Number Of Patients: {0}", patients.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(7);
        }
    }
}
