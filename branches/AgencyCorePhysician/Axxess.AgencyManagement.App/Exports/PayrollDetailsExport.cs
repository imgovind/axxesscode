﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Extension;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class PayrollDetailsExport : BaseExporter
    {
        private List<PayrollDetail> payrollDetails;
        private DateTime startDate;
        private DateTime endDate;
        public PayrollDetailsExport(List<PayrollDetail> payrollDetails, DateTime startDate, DateTime endDate)
            : base()
        {
            this.payrollDetails = payrollDetails;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Details";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Payroll Details");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Details");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Schedule Date");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Patient");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Visit Time");
            headerRow.CreateCell(5).SetCellValue("Total Visit Min.");
            headerRow.CreateCell(6).SetCellValue("Travel Time");
            headerRow.CreateCell(7).SetCellValue("Total Travel Min.");
            headerRow.CreateCell(8).SetCellValue("Pay Rate.");
            headerRow.CreateCell(9).SetCellValue("Visit Pay.");
            headerRow.CreateCell(10).SetCellValue("Mileage Rate.");
            headerRow.CreateCell(11).SetCellValue("Associated Mileage");
            headerRow.CreateCell(12).SetCellValue("Mileage Payment");
            headerRow.CreateCell(13).SetCellValue("Surcharge");
            headerRow.CreateCell(14).SetCellValue("Status");

            if (this.payrollDetails != null && this.payrollDetails.Count > 0)
            {
                int i = 2;
                double grandTotalVisitPayment = 0;
                double grandTotalMileagePayment = 0;
                double grandTotalSurchargePayment = 0;
                double grandTotalPayment = 0;
                var grandCategoties = new Dictionary<string, TaskSub>();
                var grandSingleCategory = new TaskSub();
                var tempCategories = new List<TaskSub>();
                payrollDetails.ForEach(payrollDetail =>
                {
                    var totalTime = 0;
                    double totalMileage = 0;
                    double totalPayment = 0;
                    double totalMileagePay = 0;
                    double totalSurcharge = 0;
                    if (payrollDetail.Visits != null && payrollDetail.Visits.Count > 0)
                    {
                        var topRow = sheet.CreateRow(i);
                        topRow.CreateCell(0).SetCellValue(string.Format("Employee: {0}", payrollDetail.Name));
                        var visits = payrollDetail.Visits;
                        var categoties = new Dictionary<string, TaskSub>();
                        var singleCategory = new TaskSub();
                        payrollDetail.Visits = payrollDetail.Visits.OrderBy(e => e.PatientName).ToList();
                        payrollDetail.Visits.ForEach(visit =>
                        {
                            i++;
                            var row = sheet.CreateRow(i);
                            DateTime scheduleDate = DateTime.Parse(visit.ScheduleDate);
                            if (scheduleDate != DateTime.MinValue)
                            {
                                var createdDateCell = row.CreateCell(0);
                                createdDateCell.CellStyle = dateStyle;
                                createdDateCell.SetCellValue(scheduleDate);
                            }
                            else
                            {
                                row.CreateCell(0).SetCellValue(string.Empty);
                            }
                            if (visit.VisitDate.IsValidDate())
                            {
                                DateTime visitDate = DateTime.Parse(visit.VisitDate);
                                if (visitDate != DateTime.MinValue)
                                {
                                    var createdDateCell = row.CreateCell(1);
                                    createdDateCell.CellStyle = dateStyle;
                                    createdDateCell.SetCellValue(visitDate);
                                }
                                else
                                {
                                    row.CreateCell(1).SetCellValue(string.Empty);
                                }
                            }
                            else
                            {
                                row.CreateCell(1).SetCellValue(string.Empty);
                            }
                            if (singleCategory.TaskName == visit.TaskName)
                            {
                                if (categoties.ContainsKey(visit.TaskName))
                                {
                                    categoties.Remove(visit.TaskName);
                                }
                                singleCategory.TaskVisit++;
                                singleCategory.TaskPayment += visit.VisitPayment.Substring(1).Trim().ToDouble();

                            }
                            else
                            {
                                singleCategory = new TaskSub();
                                singleCategory.TaskName = visit.TaskName;
                                singleCategory.TaskVisit = 1;
                                singleCategory.TaskPayment += visit.VisitPayment.Substring(1).Trim().ToDouble();
                            }
                            if (!categoties.ContainsKey(singleCategory.TaskName))
                            {
                                categoties.Add(singleCategory.TaskName, singleCategory);
                            }
                            tempCategories.Add(new TaskSub { TaskName = visit.TaskName, TaskVisit = 1, TaskPayment = visit.VisitPayment.Substring(1).Trim().ToDouble() });
                            row.CreateCell(2).SetCellValue(visit.PatientName);
                            row.CreateCell(3).SetCellValue(visit.TaskName);
                            row.CreateCell(4).SetCellValue(visit.TimeIn.IsNotNullOrEmpty() && visit.TimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", visit.TimeIn, visit.TimeOut) : string.Empty);
                            row.CreateCell(5).SetCellValue(visit.MinSpent);
                            row.CreateCell(6).SetCellValue(visit.TravelTimeIn.IsNotNullOrEmpty() && visit.TravelTimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", visit.TravelTimeIn, visit.TravelTimeOut) : string.Empty);
                            row.CreateCell(7).SetCellValue(visit.TravelMinSpent);
                            row.CreateCell(8).SetCellValue(visit.VisitRate);
                            row.CreateCell(9).SetCellValue(visit.VisitPayment.Substring(1).Trim().ToDouble());
                            row.CreateCell(10).SetCellValue(visit.MileageRate);
                            row.CreateCell(11).SetCellValue(visit.AssociatedMileage);
                            row.CreateCell(12).SetCellValue(visit.MileagePayment);
                            row.CreateCell(13).SetCellValue(visit.Surcharge);
                            row.CreateCell(14).SetCellValue(visit.StatusName);
                            totalTime += visit.MinSpent;
                            totalMileage += visit.AssociatedMileage.IsNotNullOrEmpty() ? visit.AssociatedMileage.ToDouble() : 0;
                            totalPayment += visit.VisitPayment.Substring(1).Trim().ToDouble();
                            totalMileagePay += visit.MileagePayment;
                            totalSurcharge += visit.Surcharge.Trim().StartsWith("$") ? visit.Surcharge.Substring(1).ToDouble() : visit.Surcharge.ToDouble();
                        });
                        i++;
                        var subTotalLine = sheet.CreateRow(i);
                        foreach (TaskSub c in categoties.Values)
                        {
                            var subLineRow = sheet.CreateRow(i);
                            subLineRow.CreateCell(0).SetCellValue(string.Format("{0}: {1} visits, pay ${2}.", c.TaskName, c.TaskVisit, c.TaskPayment));
                            i++;
                        };
                        var totalRow = sheet.CreateRow(i);
                        totalRow.CreateCell(0).SetCellValue(string.Format("Total Number visit of the employee: {0}", payrollDetail.Visits.Count));
                        totalRow.CreateCell(1).SetCellValue(string.Format("Total Visit Time: {0}", string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)));
                        totalRow.CreateCell(2).SetCellValue(string.Format("Total Visit Payment: ${0}", totalPayment.ToString()));
                        totalRow.CreateCell(3).SetCellValue(string.Format("Total Mileage: {0} miles", totalMileage));
                        totalRow.CreateCell(4).SetCellValue(string.Format("Total Mileage Payment:${0}", totalMileagePay));
                        totalRow.CreateCell(5).SetCellValue(string.Format("Total Surcharge:${0}", totalSurcharge));
                        totalRow.CreateCell(6).SetCellValue(string.Format("Grand Total:${0}", totalPayment + totalMileagePay + totalSurcharge));
                        i += 1;
                        sheet.CreateRow(i).CreateCell(0).SetCellValue(" ");
                    }
                    workBook.FinishWritingToExcelSpreadsheet(15);
                    i += 2;
                    grandTotalVisitPayment += totalPayment;
                    grandTotalMileagePayment += totalMileagePay;
                    grandTotalSurchargePayment += totalSurcharge;
                    grandTotalPayment += (totalPayment + totalMileagePay + totalSurcharge);
                });
                tempCategories.OrderBy(t => t.TaskName).ForEach(t =>
                {
                    if (grandSingleCategory.TaskName == t.TaskName)
                    {
                        if (grandCategoties.ContainsKey(t.TaskName))
                        {
                            grandCategoties.Remove(t.TaskName);
                        }
                        grandSingleCategory.TaskVisit++;
                        grandSingleCategory.TaskPayment += t.TaskPayment;
                    }
                    else
                    {
                        grandSingleCategory = new TaskSub();
                        grandSingleCategory.TaskName = t.TaskName;
                        grandSingleCategory.TaskVisit = 1;
                        grandSingleCategory.TaskPayment += t.TaskPayment;
                    }
                    grandCategoties.Add(t.TaskName, grandSingleCategory);
                });
                i += 1;
                var summaryRow = sheet.CreateRow(i);
                summaryRow.CreateCell(0).SetCellValue("Summary");
                foreach (TaskSub c in grandCategoties.Values)
                {
                    i++;
                    var subLineRow = sheet.CreateRow(i);
                    subLineRow.CreateCell(0).SetCellValue(string.Format("{0}: {1} visits, pay ${2}.", c.TaskName, c.TaskVisit, c.TaskPayment));
                };
                var finalRow = sheet.CreateRow(i + 1);
                finalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee : {0}", payrollDetails.Count));
                finalRow.CreateCell(1).SetCellValue(string.Format("Total Visit Payment:${0}", grandTotalVisitPayment));
                finalRow.CreateCell(2).SetCellValue(string.Format("Total Mileage Payment:${0}", grandTotalMileagePayment));
                finalRow.CreateCell(3).SetCellValue(string.Format("Total Surcharge:${0}", grandTotalSurchargePayment));
                finalRow.CreateCell(4).SetCellValue(string.Format("Total Payment:{0}", grandTotalPayment));
            }
            else
            {
                workBook.FinishWritingToExcelSpreadsheet(8);
            }
        }
    }
}

internal class TaskSub
{
    public string TaskName { get; set; }
    public int TaskVisit{get;set;}
    public double TaskPayment { get; set; }
}