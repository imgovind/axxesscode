﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using Axxess.Api;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.App.Exports
{
    public class UnbilledVisitsExporter : BaseExporter
    {

        #region Private Members and Constructor

        private Guid AgencyId;
        private string AgencyName;
        private Guid BranchId;
        private DateTime StartDate;
        private DateTime EndDate;
        private int Insurance;
        private int Status;

        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public UnbilledVisitsExporter(Guid agencyId, Guid branchId, int insurnaceId, int status, DateTime startDate, DateTime endDate, String agencyName)
            : base()
        {
            this.AgencyId = agencyId;
            this.AgencyName = agencyName;
            this.BranchId = branchId;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Insurance = insurnaceId;
            this.Status = status;

            this.FormatType = Axxess.AgencyManagement.App.Enums.ExportFormatType.XLS;
            this.FileName = "UnbilledVisits.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Unbilled Visits for Managed Claims";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("UnbilledVisits");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(AgencyName);
            titleRow.CreateCell(1).SetCellValue("Unbilled Visits for Managed Claims");
            titleRow.CreateCell(2).SetCellValue("Effective Date: " + DateTime.Today.ToZeroFilled());
            titleRow.CreateCell(3).SetCellValue("Date Range: " + StartDate.ToZeroFilled() + " - " + EndDate.ToZeroFilled());
            if (Insurance != 0)
            {
                var insurance = agencyRepository.GetInsurance(Insurance, AgencyId);
                titleRow.CreateCell(4).SetCellValue("Insurance: " + insurance.Name);
            }
            else
            {
                titleRow.CreateCell(4).SetCellValue("Insurance: All");
            }

            List<Dictionary<string, string>> data = reportAgent.UnbilledVisitsForManagedClaims(AgencyId, BranchId, Insurance, Status ,StartDate, EndDate);
            
            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            if (colIndex == 2)
                            {
                                if (value.IsNotNullOrEmpty() && value.IsInteger())
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue(((DisciplineTasks)value.ToInteger()).GetDescription());
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue("");
                                }
                            }
                            else if (colIndex == 3)
                            {
                                if (value.IsNotNullOrEmpty())
                                {
                                    string statusString = "";
                                    string eventDate = list.ElementAt(5).Value;
                                    int check = -1;
                                    ScheduleStatus status = ScheduleStatus.NoStatus;
                                    if (int.TryParse(value, out check)) { status = Enum.IsDefined(typeof(ScheduleStatus), check) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), value) : ScheduleStatus.NoStatus; }
                                    if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && eventDate.IsValidDate() && eventDate.ToDateTime().Date <= DateTime.Now.Date)
                                    {
                                        statusString = ScheduleStatus.CommonNotStarted.GetDescription();
                                    }
                                    else
                                    {
                                        statusString = status.GetDescription();
                                    }
                                    dataRow.CreateCell(colIndex).SetCellValue(statusString);
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue("");
                                }
                            }
                            else if (colIndex == 4)
                            {
                                if (value.IsNotNullOrEmpty() && value.IsGuid())
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue(UserEngine.GetName(value.ToGuid(), AgencyId));
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue("");
                                }
                            }
                            else if (colIndex == 5)
                            {
                                if (value.IsNotNullOrEmpty() && value.IsDate())
                                {
                                    var cell = dataRow.CreateCell(colIndex);
                                    cell.CellStyle = dateStyle;
                                    cell.SetCellValue(value.ToDateTime());
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue("");
                                }
                            }
                            else if (Insurance == 0 && colIndex > 6)
                            {
                                if (value.IsNotNullOrEmpty() && value.IsInteger())
                                {
                                    var insurnace = InsuranceEngine.Instance.Get(value.ToInteger(), AgencyId);
                                    dataRow.CreateCell(colIndex).SetCellValue(insurnace != null && insurnace.Name.IsNotNullOrEmpty() ? insurnace.Name : "");
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue("");
                                }
                                
                            }
                            else
                            {
                                dataRow.CreateCell(colIndex).SetCellValue(value);
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Visits Information found!");
            }
        }
        #endregion

    }
}
