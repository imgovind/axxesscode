﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using Enums;

    using NPOI.HSSF.UserModel;
    using NPOI.SS.UserModel;
    using NPOI.SS.Util;
    using NPOI.HSSF.Util;
    using NPOI.HPSF;
    using System.Reflection;
    using System.Linq.Expressions;

    #region Models
    public class ExcelColumnModel 
    {
        public Type Type { get; set; }
        public string ColumnName { get; set; }
        public PropertyInfo Property { get; set; }
    }

    public class ExcelDescriptionModel
    {
        public ExcelDescriptionModel() { }

        public ExcelDescriptionModel(string ReportName)
        {
            this.ReportName = ReportName;
            this.Author = "Axxess";
            this.Company = "Axxess Healthcare Consult";
            this.SheetName = ReportName;
        }

        public ExcelDescriptionModel(string ReportName, string SheetName)
        {
            this.ReportName = ReportName;
            this.Author = "Axxess";
            this.Company = "Axxess Healthcare Consult";
            this.SheetName = SheetName;
        }

        public ExcelDescriptionModel(string ReportName, string SheetName, string Company)
        {
            this.ReportName = ReportName;
            this.Author = "Axxess";
            this.Company = Company;
            this.SheetName = SheetName;
        }

        public ExcelDescriptionModel(string ReportName, string SheetName, string Company, string Author)
        {
            this.ReportName = ReportName;
            this.Author = Author;
            this.Company = Company;
            this.SheetName = SheetName;
        }

        public string Author { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string SheetName { get; set; }
        public string ReportName { get; set; }
    }

    public class ExcelStyleModel
    {
        public ExcelStyleModel()
        {
            isHeaderPresent = true;
            isFilterPresent = true;
            isFooterPresent = true;
            useDefaultContentStyle = true;
            useDefaultFilterStyle = true;
            useDefaultFooterStyle = true;
            useDefaultHeaderStyle = true;
        }

        #region Generic
        public bool isHeaderPresent { get; set; }
        public bool isFooterPresent { get; set; }
        public bool isFilterPresent { get; set; }
        public bool useDefaultHeaderStyle { get; set; }
        public bool useDefaultFilterStyle { get; set; }
        public bool useDefaultContentStyle { get; set; }
        public bool useDefaultFooterStyle { get; set; }

        #endregion

        #region Header
        public SectionModel HeaderStyle { get; set; }
        #endregion

        #region Filter
        public SectionModel FilterStyle { get; set; }
        public SectionModel FilterValueStyle { get; set; }
        #endregion

        #region Content
        public SectionModel ContentHeaderStyle { get; set; }
        public SectionModel ContentDataStyle { get; set; }
        #endregion

        #region Footer
        public SectionModel FooterStyle { get; set; }
        #endregion

        public SimpleStyle Style { get; set; }
    }

    public class SectionModel
    {
        public SectionModel() 
        {
            Style = new SimpleStyle();
            Row = new List<RowModel>();
        }

        public SimpleStyle Style { get; set; }
        public List<RowModel> Row { get; set; }
    }

    public class ExcelCustomColor
    {
        public short Color { get; set; }
        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }
    }

    public class RowModel
    {
        public RowModel()
        {
            Style = new SimpleStyle();
            Splits = new List<SplitModel>();
        }
        public SimpleStyle Style { get; set; }
        public List<SplitModel> Splits { get; set; }
    }

    public class SplitModel
    {
        public SplitModel()
        {
            From = default(int);
            To = default(int);
            Style = new SimpleStyle();
        }
        public int From { get; set; }
        public int To { get; set; }
        public SimpleStyle Style { get; set; }
    }

    public class ExportDataList<T>
    {
        public IDictionary<string, ExcelColumnModel> Columns { get; set; }
        public List<T> Data { get; set; }
        public Dictionary<string, string> ReportFilters { get; set; }
        public ExcelDescriptionModel Description { get; set; }
        public ExcelStyleModel Style { get; set; }
        public ExportFormatType FormatType { get; set; }
        public bool IdDictionaryList { get; set; }
    }

    public class ExportDataDictionary
    {
        public IDictionary<string, ExcelColumnModel> Columns { get; set; }
        public List<Dictionary<string,string>> Data { get; set; }
        public Dictionary<string, string> ReportFilters { get; set; }
        public ExcelDescriptionModel Description { get; set; }
        public ExcelStyleModel Style { get; set; }
        public ExportFormatType FormatType { get; set; }
        public bool IdDictionaryList { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
    }

    static class SimpleDefaultValues
    {
        public static short BorderColor { get { return HSSFColor.Black.Index; } }
        public static BorderStyle BorderStyle { get { return BorderStyle.Thin; } }
        public static short BackgroundColor { get { return HSSFColor.White.Index; } }
        public static FillPattern FillPattern { get { return FillPattern.SolidForeground; } }
        public static HorizontalAlignment HorizontalAlignment { get { return HorizontalAlignment.Left; } }
        public static VerticalAlignment VerticalAlignment { get { return VerticalAlignment.Center; } }
        public static string Font { get { return "Calibri"; } }
        public static short FontSize { get { return 12; } }
        public static short FontColor { get { return HSSFColor.Black.Index; } }
    }

    public class SimpleStyle
    {
        public SimpleStyle()
        {
            BackgroundColor = SimpleDefaultValues.BackgroundColor;
            FillPattern = SimpleDefaultValues.FillPattern;
            Font = SimpleDefaultValues.Font;
            FontSize = SimpleDefaultValues.FontSize;
            FontColor = SimpleDefaultValues.FontColor;
            CustomColors = new List<ExcelCustomColor>();
            HorizontalAlignment = SimpleDefaultValues.HorizontalAlignment;
            VerticalAlignment = SimpleDefaultValues.VerticalAlignment;
        }

        public bool IsAutoSizeColumn { get; set; }
        public bool IsMultiline { get; set; }
        public bool IsBorderPresent { get; set; }
        public short BorderColor { get; set; }
        public BorderStyle BorderStyle { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public short BackgroundColor { get; set; }
        public FillPattern FillPattern { get; set; }
        public string Font { get; set; }
        public short FontSize { get; set; }
        public short FontColor { get; set; }
        public HorizontalAlignment HorizontalAlignment { get; set; }
        public VerticalAlignment VerticalAlignment { get; set; }
        public bool UseCustomPalette { get; set; }
        public List<ExcelCustomColor> CustomColors { get; set; }
    }
    #endregion

    public class SimpleExcelExporter<T>
    {
        #region Members
        private HSSFWorkbook wb = new HSSFWorkbook();
        private ExcelStyleModel style { get; set; }
        private ExcelDescriptionModel description { get; set; }
        protected List<T> DataList { get; set; }
        private List<Dictionary<string,string>> DataDictionary { get; set; }
        private ExportFormatType formatType { get; set; }
        public bool useDictionary { get; set; }
        public bool useList { get; set; }
        private IDictionary<string, ExcelColumnModel> Columns { get; set; }
        private IDictionary<string, string> ReportFilters { get; set; }
        private MemoryStream outputStream = new MemoryStream();
        public string FileName { get; set; }
        public string MimeType { get; set; }
        #endregion

        #region Constructors

        public SimpleExcelExporter()
        {
        }
        public SimpleExcelExporter(ExportDataList<T> data)
        {
            this.DataList = data.Data;
            this.style = data.Style;
            this.description = data.Description;
            this.formatType = data.FormatType;
            this.ReportFilters = data.ReportFilters;
            this.Columns = data.Columns;
            this.useList = true;
            this.useDictionary = false;
        }

        public SimpleExcelExporter(ExportDataDictionary data)
        {
            this.DataDictionary = data.Data;
            this.style = data.Style;
            this.description = data.Description;
            this.formatType = data.FormatType;
            this.ReportFilters = data.ReportFilters;
            this.Columns = data.Columns;
            this.useList = false;
            this.useDictionary = true;
            this.FileName = data.FileName;
            this.MimeType = data.MimeType;
        }
        #endregion

        #region Public Methods
        public MemoryStream WriteToExcel()
        {
            HSSFPalette customPalette = wb.GetCustomPalette();
            if (this.formatType == ExportFormatType.XLS)
            {
                #region Description
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = this.description.Company;
                wb.DocumentSummaryInformation = dsi;
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Author = this.description.Author;
                wb.SummaryInformation = si;
                #endregion

                ISheet sheet = wb.CreateSheet(this.description.SheetName);
                int MaxColCount = default(int);
                int MasterRowCount = default(int);

                #region Header
                if (this.style.isHeaderPresent == true)
                {
                    if (this.style.useDefaultHeaderStyle == true)
                    {
                        this.style.HeaderStyle = DefaultHeaderStyle();
                    }
                    
                    #region HeaderStyle
                    if (this.style.HeaderStyle.Style.UseCustomPalette == true)
                    {
                        foreach (var customColor in this.style.HeaderStyle.Style.CustomColors)
                        {
                            customPalette.SetColorAtIndex(customColor.Color, customColor.Red, customColor.Green, customColor.Blue);
                        }
                    }

                    ICellStyle AgencyNameStyle = wb.CreateCellStyle();
                    IFont AgencyNameFont = wb.CreateFont();
                    
                    ICellStyle ReportNameStyle = wb.CreateCellStyle();
                    IFont ReportNameFont = wb.CreateFont();
                    #endregion

                    #region HeaderData
                    foreach (var row in this.style.HeaderStyle.Row)
                    {
                        IRow rows = sheet.CreateRow(MasterRowCount);
                        foreach (var split in row.Splits)
                        {
                            ICell cell = null;
                            for (int i = split.From; i < split.To; i++)
                            {
                                cell = rows.CreateCell(i);
                                if (i == 0)
                                {
                                    cell.SetCellValue(Current.AgencyName);
                                    cell.CellStyle = this.ApplyStyle(AgencyNameStyle, AgencyNameFont, split.Style);
                                }
                                if (i == 3)
                                {
                                    cell.SetCellValue(this.description.ReportName);
                                    cell.CellStyle = this.ApplyStyle(ReportNameStyle, ReportNameFont, split.Style);
                                }
                            }
                            CellRangeAddress region = new CellRangeAddress(0, 0, split.From, split.To);
                            sheet.AddMergedRegion(region);
                            if (this.style.HeaderStyle.Style.IsBorderPresent == true)
                            {
                                ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(region, BorderStyle.Thin, HSSFColor.Black.Index);
                            }
                        }
                        MasterRowCount++;
                    }
                    #endregion
                }
                #endregion

                ICellStyle EmptyCellStyle = wb.CreateCellStyle();
                IFont EmptyCellFont = wb.CreateFont();

                IRow row1 = sheet.CreateRow(MasterRowCount);
                for (int i = 0; i < this.Columns.Keys.Count; i++)
                {
                    ICell cell = row1.CreateCell(i);
                    cell.CellStyle = this.ApplyStyle(EmptyCellStyle, EmptyCellFont, this.EmptyCellStyler());
                }

                #region Filter
                if (this.style.isFilterPresent == true)
                {
                    if (this.style.useDefaultFilterStyle == true)
                    {
                        this.style.FilterStyle = DefaultFilterStyle();
                        this.style.FilterValueStyle = DefaultFilterValueStyle();
                    }

                    #region FilterStyle
                    ICellStyle FilterValueStyle = wb.CreateCellStyle();
                    IFont FilterValueFont = wb.CreateFont();

                    ICellStyle FilterStyle = wb.CreateCellStyle();
                    IFont FilterFont = wb.CreateFont();
                    #endregion

                    if (this.ReportFilters != null && this.ReportFilters.Count > 0)
                    {
                        MasterRowCount++;

                        foreach (var filter in this.ReportFilters)
                        {
                            IRow row = sheet.CreateRow(MasterRowCount);

                            ICell cellProperty = row.CreateCell(0);
                            cellProperty.CellStyle = this.ApplyStyle(FilterStyle, FilterFont, this.style.FilterStyle.Style);
                            cellProperty.SetCellValue(filter.Key);

                            ICell cellValue = row.CreateCell(1);
                            cellValue.CellStyle = this.ApplyStyle(FilterValueStyle, FilterValueFont, this.style.FilterValueStyle.Style);
                            cellValue.SetCellValue(filter.Value);

                            for (int i = 2; i < this.Columns.Keys.Count; i++)
                            {
                                ICell cell = row.CreateCell(i);
                                cell.CellStyle = this.ApplyStyle(EmptyCellStyle, EmptyCellFont, this.EmptyCellStyler());
                            }

                            MasterRowCount++;
                        }
                    }
                }
                #endregion

                IRow row2 = sheet.CreateRow(MasterRowCount);
                for (int i = 0; i < this.Columns.Keys.Count; i++)
                {
                    ICell cell = row2.CreateCell(i);
                    cell.CellStyle = this.ApplyStyle(EmptyCellStyle, EmptyCellFont, this.EmptyCellStyler());
                }
                MasterRowCount++;

                IRow row3 = sheet.CreateRow(MasterRowCount);
                for (int i = 0; i < this.Columns.Keys.Count; i++)
                {
                    ICell cell = row3.CreateCell(i);
                    cell.CellStyle = this.ApplyStyle(EmptyCellStyle, EmptyCellFont, this.EmptyCellStyler());
                }
                MasterRowCount++;

                #region Data
                if (this.style.useDefaultContentStyle == true)
                {
                    this.style.ContentHeaderStyle = DefaultContentHeaderStyle();
                    this.style.ContentDataStyle = DefaultContentDataStyle();
                }

                #region ContentHeaderStyle
                ICellStyle ContentHeaderStyle = wb.CreateCellStyle();
                IFont ContentHeaderFont = wb.CreateFont();
                #endregion

                #region ContentHeader
                int ContentDataColumnCount = default(int);
                IRow headerRow = sheet.CreateRow(MasterRowCount);
                foreach (var kvp in this.Columns)
                {
                    ICell headerCell = headerRow.CreateCell(ContentDataColumnCount);
                    headerCell.SetCellValue(kvp.Value.ColumnName);
                    headerCell.CellStyle = this.ApplyStyle(ContentHeaderStyle, ContentHeaderFont, this.style.ContentHeaderStyle.Style);
                    ContentDataColumnCount++;
                }
                #endregion

                MasterRowCount++;

                sheet.CreateFreezePane(0, MasterRowCount);

                #region ContentDataStyle
                ICellStyle ContentDataStyle = wb.CreateCellStyle();
                IFont ContentDataFont = wb.CreateFont();
                ContentDataStyle = this.ApplyStyle(ContentDataStyle, ContentDataFont, this.style.ContentDataStyle.Style);
                
                ICellStyle ContentDataNumericSyle = wb.CreateCellStyle();
                IFont ContentDataNumericFont = wb.CreateFont();
                ContentDataNumericSyle = this.ApplyStyle(ContentDataNumericSyle, ContentDataNumericFont, this.style.ContentDataStyle.Style, wb.CreateDataFormat().GetFormat("#,##0"));

                ICellStyle ContentDataDateSyle = wb.CreateCellStyle();
                IFont ContentDataDateFont = wb.CreateFont();
                ContentDataDateSyle = this.ApplyStyle(ContentDataDateSyle, ContentDataDateFont, this.style.ContentDataStyle.Style, wb.CreateDataFormat().GetFormat("MM/dd/yyyy"));
                #endregion

                if (this.useList)
                {
                    if (this.DataList != null && this.DataList.Count > 0)
                    {
                        #region ContentData
                        DateTime valueDateTime = DateTime.MinValue;
                        int valueInt = default(int);
                        bool valueBool = default(bool);
                        var properties = this.DataList.FirstOrDefault().GetType().GetProperties();
                        Type valueType = null;
                        foreach (var item in this.DataList)
                        {
                            IRow dataRow = sheet.CreateRow(MasterRowCount);
                            ContentDataColumnCount = default(int);
                            foreach (var kvp in this.Columns)
                            {
                                PropertyInfo property = kvp.Value.Property;
                                ICell dataCell = dataRow.CreateCell(ContentDataColumnCount);
                                #region SetCellValue
                                var value = property.GetValue(item, null);
                                if (value != null)
                                {
                                    valueType = value.GetType();
                                    if (this.Columns[property.Name].Type == DateTime.MinValue.GetType())
                                    {
                                        dataCell.CellStyle = ContentDataDateSyle;
                                        if (valueType == DateTime.MinValue.GetType())
                                        {
                                            if (!DateTime.Equals((DateTime)value, DateTime.MinValue))
                                            {
                                                dataCell.SetCellValue((DateTime)value);
                                            }
                                        }
                                        else if (valueType == String.Empty.GetType())
                                        {
                                            if (value.ToString().IsNotNullOrEmpty())
                                            {
                                                if (value.ToString().IsDate())
                                                {
                                                    if (DateTime.TryParse(value.ToString(), out valueDateTime))
                                                    {
                                                        if (!DateTime.Equals(valueDateTime, DateTime.MinValue))
                                                        {
                                                            dataCell.SetCellValue(valueDateTime);
                                                        }
                                                    }
                                                }
                                                else if (value.ToString().IsDouble())
                                                {
                                                    valueDateTime = DateTime.FromOADate(value.ToString().ToDouble());
                                                    if (!DateTime.Equals(valueDateTime, DateTime.MinValue))
                                                    {
                                                        dataCell.SetCellValue(valueDateTime);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[property.Name].Type == default(int).GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (valueType == default(int).GetType())
                                        {
                                            if (!Int32.Equals(value, default(int)))
                                            {
                                                dataCell.SetCellValue((int)value);
                                            }
                                        }
                                        else if (valueType == string.Empty.GetType())
                                        {
                                            if (value.ToString().IsNotNullOrEmpty())
                                            {
                                                if (value.ToString().IsInteger())
                                                {
                                                    if (int.TryParse(value.ToString(), out valueInt))
                                                    {
                                                        dataCell.SetCellValue(valueInt);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[property.Name].Type == default(bool).GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (valueType == default(bool).GetType())
                                        {
                                            if (!System.Boolean.Equals(value, default(bool)))
                                            {
                                                dataCell.SetCellValue((Boolean)value);
                                                dataCell.SetCellType(CellType.Boolean);
                                            }
                                        }
                                        else if (valueType == string.Empty.GetType())
                                        {
                                            if (value.ToString().IsNotNullOrEmpty())
                                            {
                                                if (value.ToString().IsBoolean())
                                                {
                                                    if (Boolean.TryParse(value.ToString(), out valueBool))
                                                    {
                                                        dataCell.SetCellValue(valueBool);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[property.Name].Type == string.Empty.GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (!String.Equals(value, string.Empty))
                                        {
                                            dataCell.SetCellType(CellType.String);
                                            dataCell.SetCellValue(value.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    dataCell.CellStyle = ContentDataStyle;
                                }
                                #endregion
                                ContentDataColumnCount++;
                            }
                            MasterRowCount++;
                        }
                        #endregion
                    }
                    else
                    {
                        IRow EmtpyRow = sheet.CreateRow(MasterRowCount);
                        ICell EmtpyDataCell = EmtpyRow.CreateCell(0);
                        EmtpyDataCell.SetCellValue("No Data Found");
                        EmtpyDataCell.CellStyle = this.ApplyStyle(ContentDataStyle, ContentDataFont, this.style.ContentDataStyle.Style);
                        CellRangeAddress region = new CellRangeAddress(MasterRowCount, MasterRowCount, 0, this.Columns.Keys.Count - 1);
                        sheet.AddMergedRegion(region);
                        ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(region, BorderStyle.Thin, HSSFColor.Black.Index);
                    }
                }
                else if(this.useDictionary)
                {
                    if (this.DataDictionary != null && this.DataDictionary.Count > 0)
                    {
                        #region ContentData
                        DateTime valueDateTime = DateTime.MinValue;
                        int valueInt = default(int);
                        bool valueBool = default(bool);
                        Type valueType = null;
                        foreach (var dictionary in this.DataDictionary)
                        {
                            IRow dataRow = sheet.CreateRow(MasterRowCount);
                            ContentDataColumnCount = default(int);
                            
                            foreach (var kvp in dictionary)
                            {
                                if (this.Columns.ContainsKey(kvp.Key))
                                {
                                    ICell dataCell = dataRow.CreateCell(ContentDataColumnCount);
                                    valueType = kvp.Value.GetType();
                                    #region SetCellValue
                                    if (this.Columns[kvp.Key].Type == DateTime.MinValue.GetType())
                                    {
                                        dataCell.CellStyle = ContentDataDateSyle;
                                        if (valueType == String.Empty.GetType())
                                        {
                                            if (kvp.Value.IsNotNullOrEmpty())
                                            {
                                                if (kvp.Value.IsDate())
                                                {
                                                    if (DateTime.TryParse(kvp.Value, out valueDateTime))
                                                    {
                                                        if (!DateTime.Equals(valueDateTime, DateTime.MinValue))
                                                        {
                                                            dataCell.SetCellValue(valueDateTime);
                                                        }
                                                    }
                                                }
                                                else if (kvp.Value.IsDouble())
                                                {
                                                    valueDateTime = DateTime.FromOADate(kvp.Value.ToDouble());
                                                    if (!DateTime.Equals(valueDateTime, DateTime.MinValue))
                                                    {
                                                        dataCell.SetCellValue(valueDateTime);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[kvp.Key].Type == default(int).GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (valueType == string.Empty.GetType())
                                        {
                                            if (kvp.Value.IsNotNullOrEmpty())
                                            {
                                                if (kvp.Value.IsInteger())
                                                {
                                                    if (int.TryParse(kvp.Value, out valueInt))
                                                    {
                                                        dataCell.SetCellValue(valueInt);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[kvp.Key].Type == default(bool).GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (valueType == string.Empty.GetType())
                                        {
                                            if (kvp.Value.IsNotNullOrEmpty())
                                            {
                                                if (kvp.Value.IsBoolean())
                                                {
                                                    if (Boolean.TryParse(kvp.Value, out valueBool))
                                                    {
                                                        dataCell.SetCellValue(valueBool);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (this.Columns[kvp.Key].Type == string.Empty.GetType())
                                    {
                                        dataCell.CellStyle = ContentDataStyle;
                                        if (!String.Equals(kvp.Value, string.Empty))
                                        {
                                            dataCell.SetCellType(CellType.String);
                                            dataCell.SetCellValue(kvp.Value.ToString());
                                        }
                                    }
                                    #endregion
                                    ContentDataColumnCount++;
                                }
                            }
                            MasterRowCount++;
                        }
                        #endregion
                    }
                    else
                    {
                        IRow EmtpyRow = sheet.CreateRow(MasterRowCount);
                        ICell EmtpyDataCell = EmtpyRow.CreateCell(0);
                        EmtpyDataCell.SetCellValue("No Data Found");
                        EmtpyDataCell.CellStyle = this.ApplyStyle(ContentDataStyle, ContentDataFont, this.style.ContentDataStyle.Style);
                        CellRangeAddress region = new CellRangeAddress(MasterRowCount, MasterRowCount, 0, this.Columns.Keys.Count - 1);
                        sheet.AddMergedRegion(region);
                        ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(region, BorderStyle.Thin, HSSFColor.Black.Index);
                    }
                }

                #region AutoSizeColumns
                if (this.style.ContentDataStyle.Style.IsAutoSizeColumn == true)
                {
                    for (int i = 0; i < this.Columns.Keys.Count; i++)
                    {
                        sheet.AutoSizeColumn(i, true);
                    }
                }
                #endregion

                #endregion

                #region Footer

                MasterRowCount++;

                if (this.style.isFooterPresent == true)
                {
                    if (this.style.useDefaultFooterStyle == true)
                    {
                        this.style.FooterStyle = DefaultFooterStyle();
                    }

                    if (this.style.FooterStyle.Style.UseCustomPalette == true)
                    {
                        foreach (var customColor in this.style.FooterStyle.Style.CustomColors)
                        {
                            customPalette.SetColorAtIndex(customColor.Color, customColor.Red, customColor.Green, customColor.Blue);
                        }
                    }

                    ICellStyle FooterStyle = wb.CreateCellStyle();
                    IFont FooterFont = wb.CreateFont();

                    ICellStyle FooterValueStyle = wb.CreateCellStyle();
                    IFont FooterValueFont = wb.CreateFont();
                    
                    int TotalDataCount = default(int);
                    if (this.useList)
                    {
                        TotalDataCount = this.DataList.Count;
                    }
                    else if (this.useDictionary)
                    {
                        TotalDataCount = this.DataDictionary.Count;
                    }

                    IRow row = sheet.CreateRow(MasterRowCount);

                    for (int i = 0; i < 3; i++)
                    {
                        ICell cell = row.CreateCell(i);
                        if (i == 0)
                        {
                            cell.SetCellValue("Total Records");
                            cell.CellStyle = this.ApplyStyle(FooterStyle, FooterFont, this.style.FooterStyle.Style); ;
                        }
                        if (i == 1)
                        {
                            cell.SetCellValue(TotalDataCount);
                            cell.CellStyle = this.ApplyStyle(FooterValueStyle, FooterValueFont, this.style.FooterStyle.Style); ;
                        }
                    }
                }
                #endregion

                wb.Write(outputStream);
            }
            return outputStream;
        }
        #endregion

        #region Private Methods
        private SectionModel DefaultHeaderStyle()
        {
            SectionModel result = new SectionModel();
            result.Style.IsBorderPresent = false;
            result.Style.UseCustomPalette = true;
            result.Style.CustomColors.Add(new ExcelCustomColor
            {
                Color = HSSFColor.Blue.Index,
                Red = (byte)25,
                Green = (byte)160,
                Blue = (byte)227
            });
            result.Style.CustomColors.Add(new ExcelCustomColor
            {
                Color = HSSFColor.Blue.Index,
                Red = (byte)50,
                Green = (byte)153,
                Blue = (byte)227
            });
            result.Style.CustomColors.Add(new ExcelCustomColor
            {
                Color = HSSFColor.Blue.Index,
                Red = (byte)51,
                Green = (byte)51,
                Blue = (byte)153
            });
            RowModel row = new RowModel();

            row.Splits = new List<SplitModel>();

            row.Splits.Add(new SplitModel
            {
                From = 0,
                To = 2,
                Style = new SimpleStyle
                {
                    FontSize = 16,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center
                }
            });

            int EndColumn = default(int);
            if (this.Columns != null && this.Columns.Count > 0)
            {
                EndColumn = this.Columns.Keys.Count - 1;
            }

            row.Splits.Add(new SplitModel
            {
                From = 3,
                To = EndColumn,
                Style = new SimpleStyle
                {
                    FontColor = HSSFColor.Blue.Index,
                    FontSize = 28,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    VerticalAlignment = VerticalAlignment.Center
                }
            });
            result.Row.Add(row);
            return result;
        }

        private SectionModel DefaultFilterStyle()
        {
            SectionModel result = new SectionModel
            {
                Style = new SimpleStyle
                {
                    FontSize = 11,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    IsBold = true
                }
            };
            return result;
        }

        private SimpleStyle EmptyCellStyler()
        {
            SimpleStyle style = new SimpleStyle();
            style.IsBorderPresent = true;
            style.BorderColor = HSSFColor.White.Index;
            style.FontColor = HSSFColor.White.Index;
            return style;
        }

        private SectionModel DefaultFilterValueStyle()
        {
            SectionModel result = new SectionModel
            {
                Style = new SimpleStyle
                {
                    FontSize = 11,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            return result;
        }

        private SectionModel DefaultContentHeaderStyle()
        {
            SectionModel result = new SectionModel
            {
                Style = new SimpleStyle
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    BackgroundColor = HSSFColor.Blue.Index,
                    IsBorderPresent = true,
                    BorderColor = HSSFColor.White.Index,
                    BorderStyle = BorderStyle.Thin,
                    IsBold = true,
                    FontColor = HSSFColor.White.Index
                }
            };
            return result;
        }

        private SectionModel DefaultContentDataStyle()
        {
            SectionModel result = new SectionModel
            {
                Style = new SimpleStyle
                {
                    IsAutoSizeColumn = true,
                    IsMultiline = true,
                    IsBorderPresent = true,
                    BorderStyle = BorderStyle.Thin,
                    BorderColor = HSSFColor.Black.Index
                }
            };
            return result;
        }

        private SectionModel DefaultFooterStyle()
        {
            SectionModel result = null;

            result = this.DefaultFilterStyle();
            result.Style.IsBorderPresent = true;
            result.Style.BorderStyle = BorderStyle.Thin;
            result.Style.BorderColor = HSSFColor.Grey50Percent.Index;
            result.Style.UseCustomPalette = true;
            result.Style.CustomColors = new List<ExcelCustomColor> ();
            result.Style.CustomColors.Add(new ExcelCustomColor 
            {
                Color = HSSFColor.Grey50Percent.Index,
                Red = (byte)212,
                Green = (byte)212,
                Blue = (byte)212
            });
            return result;
        }

        private ICellStyle ApplyStyle(ICellStyle CellStyle, IFont Font, SimpleStyle style)
        {
            if (style.IsMultiline == true)
            {
                CellStyle.WrapText = true;
            }
            if (style.IsBorderPresent == true)
            {
                CellStyle.BorderTop = style.BorderStyle;
                CellStyle.BorderRight = style.BorderStyle;
                CellStyle.BorderBottom = style.BorderStyle;
                CellStyle.BorderLeft = style.BorderStyle;

                CellStyle.TopBorderColor = style.BorderColor;
                CellStyle.RightBorderColor = style.BorderColor;
                CellStyle.BottomBorderColor = style.BorderColor;
                CellStyle.LeftBorderColor = style.BorderColor;
            }
            if (style.IsBold == true)
            {
                Font.Boldweight = (short)FontBoldWeight.Bold;
            }
            if (style.IsItalic == true)
            {
                Font.IsItalic = true;
            }
            CellStyle.FillPattern = style.FillPattern;
            CellStyle.FillForegroundColor = style.BackgroundColor;
            CellStyle.Alignment = style.HorizontalAlignment;
            CellStyle.VerticalAlignment = style.VerticalAlignment;
            Font.FontName = style.Font;
            Font.FontHeightInPoints = style.FontSize;
            Font.Color = style.FontColor;
            CellStyle.SetFont(Font);
            return CellStyle;
        }

        private ICellStyle ApplyStyle(ICellStyle CellStyle, IFont Font, SimpleStyle style, short dataFormat)
        {
            if (style.IsMultiline == true)
            {
                CellStyle.WrapText = true;
            }
            if (style.IsBorderPresent == true)
            {
                CellStyle.BorderTop = style.BorderStyle;
                CellStyle.BorderRight = style.BorderStyle;
                CellStyle.BorderBottom = style.BorderStyle;
                CellStyle.BorderLeft = style.BorderStyle;

                CellStyle.TopBorderColor = style.BorderColor;
                CellStyle.RightBorderColor = style.BorderColor;
                CellStyle.BottomBorderColor = style.BorderColor;
                CellStyle.LeftBorderColor = style.BorderColor;
            }
            if (style.IsBold == true)
            {
                Font.Boldweight = (short)FontBoldWeight.Bold;
            }
            if (style.IsItalic == true)
            {
                Font.IsItalic = true;
            }
            CellStyle.FillPattern = style.FillPattern;
            CellStyle.FillForegroundColor = style.BackgroundColor;
            CellStyle.Alignment = style.HorizontalAlignment;
            CellStyle.VerticalAlignment = style.VerticalAlignment;
            Font.FontName = style.Font;
            Font.FontHeightInPoints = style.FontSize;
            Font.Color = style.FontColor;
            CellStyle.SetFont(Font);
            CellStyle.DataFormat = dataFormat;
            return CellStyle;
        }
        #endregion
    }
}