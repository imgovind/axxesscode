﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class NonVisitTaskExporter : BaseExporter
    {
        private IList<AgencyNonVisit> agencyNonVisitTasks;

        public NonVisitTaskExporter(IList<AgencyNonVisit> agencyNonVisits)
            : base()    
        {
            this.agencyNonVisitTasks = agencyNonVisits;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Agency Non-Visit Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencyNonVisitTasks");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Non-Visit Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Description");
            headerRow.CreateCell(2).SetCellValue("Created Date");
            headerRow.CreateCell(3).SetCellValue("Modified Date");

            if (this.agencyNonVisitTasks.Count > 0)
            {
                int i = 2;
                this.agencyNonVisitTasks.ForEach(agencyNonVisitTask =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(agencyNonVisitTask.Title);
                    dataRow.CreateCell(1).SetCellValue(agencyNonVisitTask.Text);
                    if (agencyNonVisitTask.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(agencyNonVisitTask.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (agencyNonVisitTask.Modified != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(agencyNonVisitTask.Modified);
                    }
                    else
                    {
                        dataRow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency Non-Visits: {0}", agencyNonVisitTasks.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(4); 
        }
    }
}
