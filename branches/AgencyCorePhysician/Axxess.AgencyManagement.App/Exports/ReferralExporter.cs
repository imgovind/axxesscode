﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ReferralExporter : BaseExporter
    {
        private IList<Referral> referrals;
        public ReferralExporter(IList<Referral> referrals)
            : base()
        {
            this.referrals = referrals;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Referrals";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Referrals");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Referrals");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            headerRow.CreateCell(0).SetCellValue("Referral Date");
            headerRow.CreateCell(1).SetCellValue("Referral Source");
            headerRow.CreateCell(2).SetCellValue("Medicare Number");
            headerRow.CreateCell(3).SetCellValue("Last Name");
            headerRow.CreateCell(4).SetCellValue("First Name");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Address 1");
            headerRow.CreateCell(7).SetCellValue("Address 2");
            headerRow.CreateCell(8).SetCellValue("City");
            headerRow.CreateCell(9).SetCellValue("State");
            headerRow.CreateCell(10).SetCellValue("Zip Code");
            headerRow.CreateCell(11).SetCellValue("Date of Birth");
            headerRow.CreateCell(12).SetCellValue("Gender");
            headerRow.CreateCell(13).SetCellValue("Email Address");
            headerRow.CreateCell(14).SetCellValue("DNR");

            if (this.referrals.Count > 0)
            {
                int i = 2;
                this.referrals.ForEach(r =>
                {
                    var dataRow = sheet.CreateRow(i);

                    if (r.ReferralDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(0);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(r.ReferralDate);
                    }
                    else
                    {
                        dataRow.CreateCell(0).SetCellValue(string.Empty);
                    }       

                    dataRow.CreateCell(1).SetCellValue(r.AdmissionSource.ToString().GetAdmissionDescription());
                    dataRow.CreateCell(2).SetCellValue(r.MedicareNumber);
                    dataRow.CreateCell(3).SetCellValue(r.LastName.ToUpper());
                    dataRow.CreateCell(4).SetCellValue(r.FirstName.ToUpper());
                    dataRow.CreateCell(5).SetCellValue(r.PhoneHome.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(r.AddressLine1.ToTitleCase());
                    dataRow.CreateCell(7).SetCellValue(r.AddressLine2.ToTitleCase());
                    dataRow.CreateCell(8).SetCellValue(r.AddressCity.ToTitleCase());
                    dataRow.CreateCell(9).SetCellValue(r.AddressStateCode);
                    dataRow.CreateCell(10).SetCellValue(r.AddressZipCode);

                    if (r.DOB != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(11);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(r.DOB);
                    }
                    else
                    {
                        dataRow.CreateCell(11).SetCellValue(string.Empty);
                    }    

                    dataRow.CreateCell(12).SetCellValue(r.Gender);
                    dataRow.CreateCell(13).SetCellValue(r.EmailAddress);
                    dataRow.CreateCell(14).SetCellValue(r.IsDNR ? "Yes" : "No");
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Referrals: {0}", referrals.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(15);
        }
    }
}
