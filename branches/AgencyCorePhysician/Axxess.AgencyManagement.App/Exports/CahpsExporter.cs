﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    using Axxess.Membership.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.Log.Repositories;

    public class CahpsExporter : BaseExporter
    {
        #region Private Members and Constructor

        private int sampleYear;
        private int sampleMonth;
        private List<int> paymentSources;
        private Guid branchId;
        

        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly ILookUpDataProvider lookupDataProvider = new LookUpDataProvider();
        private static readonly ILookupService lookupService = new LookupService(lookupDataProvider);
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private static readonly IAgencyManagementDataProvider agencyManagementDataProvider = new AgencyManagementDataProvider();
        private static readonly ILogDataProvider logDataProvider = new LogDataProvider();
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();
        private static readonly IAssetService assetService = new AssetService(agencyManagementDataProvider);
        private static readonly IAssessmentService assessmentService = new AssessmentService(oasisDataProvider, agencyManagementDataProvider, lookupDataProvider, lookupService, assetService);
        private static readonly IDrugService drugService = new DrugService();
        private static readonly IPatientService patientService = new PatientService(agencyManagementDataProvider, lookupDataProvider, logDataProvider, oasisDataProvider, assessmentService, drugService, assetService);
        private static readonly IMembershipDataProvider membershipDataProvider = new MembershipDataProvider();
        private static readonly IPhysicianService physicianService = new PhysicianService(agencyManagementDataProvider, membershipDataProvider, lookupDataProvider);
        private static readonly IUserService userService = new UserService(agencyManagementDataProvider, membershipDataProvider, patientService, lookupDataProvider, assetService);
        private static readonly Axxess.AgencyManagement.App.Services.IReportService reportServiceLocal = new Axxess.AgencyManagement.App.Services.ReportService(agencyManagementDataProvider, oasisDataProvider, lookupService, assessmentService);
        public CahpsExporter(Guid branchId, int sampleMonth, int sampleYear, List<int> paymentSources)
            : base()
        {
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
            this.paymentSources = paymentSources;
            this.branchId = branchId;

            var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (agencyLocation != null)
            {
                if (!agencyLocation.IsLocationStandAlone)
                {
                    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                    if (agency != null)
                    {
                        agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                        agencyLocation.CahpsVendor = agency.CahpsVendor;
                    }
                }
            }
            if (agencyLocation != null)
            {
                switch (agencyLocation.CahpsVendor)
                {
                    case (int)CahpsVendors.DSSResearch: // DSS Research
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("{0}{1}{2}.csv", this.sampleYear, this.sampleMonth.ToString().PadLeft(2, '0'), agencyLocation.MedicareProviderNumber);
                        break;
                    case (int)CahpsVendors.Synovate: // Synovate
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Novaetus: // Novaetus
                        this.FormatType = ExportFormatType.XLS;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.xls", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Deyta: // Deyta
                    case (int)CahpsVendors.FieldsResearch: // Fields Research
                        this.FormatType = ExportFormatType.XLS;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.xls", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Pinnacle: // Pinnacle
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.PressGaney: // PressGaney
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agencyLocation.CahpsVendorId, this.sampleMonth.ToString().PadLeft(2, '0'));
                        break;
                    case (int)CahpsVendors.Ocs: // Ocs
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_Version1.2_{1}.csv", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Fazzi: // Fazzi
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Shp: // Strategic Healthcare
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agencyLocation.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Excel CAHPS

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - CAHPS Survey";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("CAHPS");

            var headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.Bold;
            headerFont.FontHeightInPoints = 13;

            var headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            List<Dictionary<string, string>> data = null;
            if (paymentSources != null && paymentSources.Count > 0)
            {
                //data = reportAgent.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear, paymentSources);
                data = reportServiceLocal.CahpsExportByPaymentSources(Current.AgencyId, branchId, sampleMonth, sampleYear, paymentSources);
            }
            else
            {
                //data = reportAgent.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear);
                data = reportServiceLocal.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear);
            }

            if (data != null && data.Count > 0)
            {
                var rowIndex = 0;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }
                headerRow.RowStyle = headerStyle;

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        dataRow.CreateCell(colIndex).SetCellValue(kvp.Value.Trim());
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                sheet.AutoResizeColumns(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No CAHPS Data found!");
                sheet.AutoResizeColumns(1);
            }
        }

        #endregion

        #region CSV CAHPS

        protected override void WriteCommaSeparatedFile()
        {
            List<Dictionary<string, string>> data = null;
            if (paymentSources != null && paymentSources.Count > 0)
            {
                //data = reportAgent.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear, paymentSources);
                data = reportServiceLocal.CahpsExportByPaymentSources(Current.AgencyId, branchId, sampleMonth, sampleYear, paymentSources);
            }
            else
            {
                //data = reportAgent.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear);
                data = reportServiceLocal.CahpsExport(Current.AgencyId, branchId, sampleMonth, sampleYear);
            }

            if (data != null && data.Count > 0)
            {
                var count = 0;
                var dictionary = data.FirstOrDefault();

                var headerRecord = new string[dictionary.Count];
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    if (kvp.Key.IsNotNullOrEmpty())
                    {
                        headerRecord[count] = kvp.Key;
                        count++;
                    }
                }
                this.csvWriter.WriteHeaderRecord(headerRecord);

                count = 0;

                data.ForEach(list =>
                {
                    var dataRecord = new string[list.Count];
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Key.IsNotNullOrEmpty())
                        {
                            dataRecord[count] = kvp.Value.Trim();
                            count++;
                        }
                    }
                    this.csvWriter.WriteDataRecord(dataRecord);
                    count = 0;
                });
            }
            else
            {
                this.csvWriter.WriteHeaderRecord("No CAHPS Data found!");
            }
        }

        #endregion
    }
}
