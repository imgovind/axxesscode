﻿namespace Axxess.Api
{
    using Axxess.Api.Contracts;

    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    public class ValidationAgent : BaseAgent<IValidationService>
    {
        #region Overrides

        public override string ToString()
        {
            return "OasisValidationService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Validation Methods

        public List<ValidationError> ValidateAssessment(string oasisDataString)
        {
            var validationErrors = new List<ValidationError>();
            BaseAgent<IValidationService>.Call(v => validationErrors = v.ValidateAssessment(oasisDataString), this.ToString());
            return validationErrors;
        }

        public List<LogicalError> LogicalInconsistencyCheck(string oasisDataString)
        {
            var logicalErrors = new List<LogicalError>();
            BaseAgent<IValidationService>.Call(v => logicalErrors = v.LogicalInconsistencyCheck(oasisDataString), this.ToString());
            return logicalErrors;
        }

        #endregion

    }
}
