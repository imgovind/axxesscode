﻿namespace Axxess.Api.Contracts
{
    using System;
    using Axxess.Api.Contracts.Enum;
    public static class ScheduleEventExtensions
    {
        public static bool IsSkilledCareForCAHPS(this ScheduleEvent scheduleEvent)
        {
            bool result = default(bool);
            DisciplineTasks disciplineTask = ((DisciplineTasks)scheduleEvent.DisciplineTask);
            switch (disciplineTask)
            {
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.OASISBFollowUp:
                case DisciplineTasks.OASISBRecertification:
                case DisciplineTasks.OASISBResumptionofCare:
                case DisciplineTasks.OASISBDischarge:
                case DisciplineTasks.OASISBStartofCare:
                case DisciplineTasks.OASISBDeathatHome:
                case DisciplineTasks.OASISCDeath:
                case DisciplineTasks.OASISCDischarge:
                case DisciplineTasks.OASISCFollowUp:
                case DisciplineTasks.OASISCRecertification:
                case DisciplineTasks.OASISCResumptionofCare:
                case DisciplineTasks.OASISCStartofCare:
                case DisciplineTasks.OASISCTransfer:
                case DisciplineTasks.OASISCTransferDischarge:
                case DisciplineTasks.SNVisitPM:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.LVNSupervisoryVisit:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.PTEvaluation:
                case DisciplineTasks.PTVisit:
                case DisciplineTasks.PTDischarge:
                case DisciplineTasks.OTEvaluation:
                case DisciplineTasks.OTReEvaluation:
                case DisciplineTasks.OTVisit:
                case DisciplineTasks.STVisit:
                case DisciplineTasks.STEvaluation:
                case DisciplineTasks.STDischarge:
                case DisciplineTasks.PTAVisit:
                case DisciplineTasks.PTReEvaluation:
                case DisciplineTasks.OASISCStartofCarePT:
                case DisciplineTasks.OASISCResumptionofCarePT:
                case DisciplineTasks.OASISCDeathPT:
                case DisciplineTasks.OASISCDischargePT:
                case DisciplineTasks.OASISCFollowupPT:
                case DisciplineTasks.OASISCRecertificationPT:
                case DisciplineTasks.OASISCTransferPT:
                case DisciplineTasks.COTAVisit:
                case DisciplineTasks.OASISCStartofCareOT:
                case DisciplineTasks.OASISCResumptionofCareOT:
                case DisciplineTasks.OASISCDeathOT:
                case DisciplineTasks.OASISCDischargeOT:
                case DisciplineTasks.OASISCFollowupOT:
                case DisciplineTasks.OASISCRecertificationOT:
                case DisciplineTasks.OASISCRecertificationST:
                case DisciplineTasks.OASISCTransferOT:
                case DisciplineTasks.NonOASISStartofCare:
                case DisciplineTasks.NonOASISRecertification:
                case DisciplineTasks.NonOASISDischarge:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNAssessmentRecert:
                case DisciplineTasks.OTDischarge:
                case DisciplineTasks.STReEvaluation:
                case DisciplineTasks.PTMaintenance:
                case DisciplineTasks.OTMaintenance:
                case DisciplineTasks.STMaintenance:
                case DisciplineTasks.SNDiabeticDailyVisit:
                case DisciplineTasks.OASISCTransferDischargePT:
                case DisciplineTasks.SNPediatricVisit:
                case DisciplineTasks.PTSupervisoryVisit:
                case DisciplineTasks.OTSupervisoryVisit:
                case DisciplineTasks.PTReassessment:
                case DisciplineTasks.OTReassessment:
                case DisciplineTasks.SNInsulinNoon:
                case DisciplineTasks.SNInsulinHS:
                case DisciplineTasks.SNPediatricAssessment:
                case DisciplineTasks.SNPsychAssessment:
                case DisciplineTasks.STReassessment:
                case DisciplineTasks.OASISCDischargeST:
                case DisciplineTasks.SNWoundCare:
                case DisciplineTasks.LVNVisit:
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static bool IsSkilledCare(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "Nursing"
                    || scheduleEvent.Discipline == "PT"
                    || scheduleEvent.Discipline == "OT"
                    || scheduleEvent.Discipline == "ST"
                    )
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompleted(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline) && !string.IsNullOrEmpty(scheduleEvent.Status))
            {
                if (scheduleEvent.Status == "115" || scheduleEvent.Status == "125" ||
                    scheduleEvent.Status == "130" || scheduleEvent.Status == "135" ||
                    scheduleEvent.Status == "145" || scheduleEvent.Status == "215" ||
                    scheduleEvent.Status == "220" || scheduleEvent.Status == "225" ||
                    scheduleEvent.Status == "240" || scheduleEvent.Status == "400" ||
                    scheduleEvent.Status == "420" || scheduleEvent.Status == "425" ||
                    scheduleEvent.Status == "445" || scheduleEvent.Status == "450" ||
                    scheduleEvent.Status == "455" || scheduleEvent.Status == "460" ||
                    scheduleEvent.Status == "520" || scheduleEvent.Status == "525")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "ST")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "OT")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "PT")
                {
                    return true;
                }
            }
            return false;
        }


        public static bool IsHhaNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "HHA")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsMSW(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "MSW" && scheduleEvent.DisciplineTask != 110)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsStartofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 13
                    || scheduleEvent.DisciplineTask == 89
                    || scheduleEvent.DisciplineTask == 61
                    || scheduleEvent.DisciplineTask == 112)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsRecertificationAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 8
                    || scheduleEvent.DisciplineTask == 73
                    || scheduleEvent.DisciplineTask == 66
                    || scheduleEvent.DisciplineTask == 90)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsResumptionofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 9
                    || scheduleEvent.DisciplineTask == 69
                    || scheduleEvent.DisciplineTask == 62)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetAssessmentType(this ScheduleEvent scheduleEvent)
        {
            var assessmentType = string.Empty;
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                switch (scheduleEvent.DisciplineTask)
                {
                    case 13:
                        assessmentType = "OASISCStartofCare";
                        break;
                    case 89:
                        assessmentType = "NonOASISStartofCare";
                        break;
                    case 61:
                        assessmentType = "OASISCStartofCarePT";
                        break;
                    case 112:
                        assessmentType = "OASISCStartofCareOT";
                        break;
                    case 8:
                        assessmentType = "OASISCRecertification";
                        break;
                    case 73:
                        assessmentType = "OASISCRecertificationOT";
                        break;
                    case 66:
                        assessmentType = "OASISCRecertificationPT";
                        break;
                    case 142:
                        assessmentType = "OASISCRecertificationST";
                        break;
                    case 90:
                        assessmentType = "NonOASISRecertification";
                        break;
                    case 9:
                        assessmentType = "OASISCResumptionofCare";
                        break;
                    case 69:
                        assessmentType = "OASISCResumptionofCareOT";
                        break;
                    case 62:
                        assessmentType = "OASISCResumptionofCarePT";
                        break;
                }
            }
            return assessmentType;
        }
    }
}
