﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/01/")]
    public class ProspectivePayment
    {
        [DataMember]
        public string Hhrg { get; set; }
        [DataMember]
        public string Weight { get; set; }
        [DataMember]
        public string CbsaCode { get; set; }
        [DataMember]
        public string WageIndex { get; set; }
        [DataMember]
        public string HippsCode { get; set; }
        [DataMember]
        public string OasisMatchingKey { get; set; }
        [DataMember]
        public double LaborAmount { get; set; }
        [DataMember]
        public double NonLaborAmount { get; set; }
        [DataMember]
        public double NonRoutineSuppliesAmount { get; set; }
        [DataMember]
        public double TotalAmountWithoutSupplies { get; set; }
        [DataMember]
        public double TotalAmount { get; set; }

        public double CalculateRapClaimAmount(bool isStartofCare)
        {
            var claimAmount = 0.5 * TotalAmount;
            if (isStartofCare)
            {
                claimAmount = 0.6 * TotalAmount;
            }
            return Math.Round(claimAmount, 2);
        }
    }
}
