﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Patient/2013/07/")]
    public class PatientList : Patient
    {
        [DataMember]
        public string ClinicianAssignedFirstName { get; set; }
        [DataMember]
        public string ClinicianAssignedLastName { get; set; }
        [DataMember]
        public string ClinicianAssignedMiddleName { get; set; }
        [DataMember]
        public string ClinicianAssignedName { get; set; }
        [DataMember]
        public string CaseManagerFirstName { get; set; }
        [DataMember]
        public string CaseManagerLastName { get; set; }
        [DataMember]
        public string CaseManagerMiddleName { get; set; }
        [DataMember]
        public string CaseManagerName { get; set; }
        [DataMember]
        public int DNR { get; set; }
        [DataMember]
        public string PrimaryInsuranceName { get; set; }
        [DataMember]
        public DateTime EpisodeStartDate { get; set; }
        [DataMember]
        public DateTime EpisodeEndDate { get; set; }
        [DataMember]
        public Guid PhysicianId { get; set; }
        [DataMember]
        public string PhysicianFirstName { get; set; }
        [DataMember]
        public string PhysicianLastName { get; set; }
        [DataMember]
        public string PhysicianMiddleName { get; set; }
        [DataMember]
        public string PhysicianNPI { get; set; }
        [DataMember]
        public string Payer { get; set; }
        [DataMember]
        public int PrimaryInsuranceNumber { get; set; }
        [DataMember]
        public int Ethnicity { get; set; }
    }
}
