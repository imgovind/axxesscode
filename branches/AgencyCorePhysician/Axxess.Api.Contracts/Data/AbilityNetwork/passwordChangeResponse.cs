﻿namespace Axxess.Api.Contracts
{
    using System.Xml.Serialization;

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class passwordChangeResponse : BaseApiEntity
    {
        public object[] Items { get; set; }
    }
}