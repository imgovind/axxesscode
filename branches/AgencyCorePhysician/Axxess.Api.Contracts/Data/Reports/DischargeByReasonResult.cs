﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/DischargeByReasonResult/2012/03/")]
    public class DischargeByReasonResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Discharges { get; set; }
    }
}
