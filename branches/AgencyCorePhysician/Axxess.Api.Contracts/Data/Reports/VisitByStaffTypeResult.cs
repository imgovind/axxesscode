﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/VisitByStaffTypeResult/2012/03/")]
    public class VisitByStaffTypeResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Visits { get; set; }
    }
}
