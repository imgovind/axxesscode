﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    [DataContract(Namespace = "http://api.axxessweb.com/logicalerror/2011/09/")]
    public class LogicalError
    {
        public LogicalError()
        {
            this.OasisItems = new List<string>();
        }
        [DataMember]
        public string Header { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public string Explanation { get; set; }
        [DataMember]
        public List<string> OasisItems { get; set; }

        public override string ToString()
        {
            var text = new StringBuilder()
                        .AppendFormat("Header: {0} ", this.Header)
                        .AppendFormat("Reason: {0} ", this.Reason);

            if (this.OasisItems.Count > 0)
            {
                this.OasisItems.ForEach(i =>
                {
                    text.AppendFormat("OASIS: {0} ", i);
                });
            }

            if (!string.IsNullOrEmpty(this.Action))
            {
                text.AppendFormat("Action: {0} ", this.Action);
            }

            if (!string.IsNullOrEmpty(this.Explanation))
            {
                text.AppendFormat("Explanation: {0} ", this.Explanation);
            }

            return text.ToString();
        }
    }
}
