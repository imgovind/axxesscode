﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class RequestValidation
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string yes_no_response_code { get; set; } // AAA01
        [DataMember]
        public string reject_reason_code { get; set; } //AAA03
        [DataMember]
        public string follow_up_action_code { get; set; } //AAA04
    }
}
