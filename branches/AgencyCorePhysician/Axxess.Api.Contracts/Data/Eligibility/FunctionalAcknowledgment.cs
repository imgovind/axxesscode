﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class FunctionalAcknowledgment
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string functional_identifier_code { get; set; }  //AK101
        [DataMember]
        public string transaction_set_identifier_code { get; set; } //AK201
        [DataMember]
        public string segment_id_code { get; set; } //AK301
        [DataMember]
        public string segment_syntax_error_code { get; set; } //AK304
        [DataMember]
        public string position_in_segment { get; set; } //AK401
        [DataMember]
        public string data_element_syntax_error_code { get; set; } //AK403
        [DataMember]
        public string copy_of_bad_data_element { get; set; } //AK404
        [DataMember]
        public string functional_group_acknowledge_code { get; set; } //AK901
    }
}
