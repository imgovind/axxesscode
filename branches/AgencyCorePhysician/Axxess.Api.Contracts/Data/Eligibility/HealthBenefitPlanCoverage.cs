﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class HealthBenefitPlanCoverage
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string payer { get; set; } //NM101
        [DataMember]
        public string name { get; set; } //NM103
        [DataMember]
        public string insurance_type { get; set; } //EB04
        [DataMember]
        public string reference_id_qualifier { get; set; } //REF01
        [DataMember]
        public string reference_id { get; set; } //REF02
        [DataMember]
        public string date { get; set; } //DTP03
        [DataMember]
        public string address1 { get; set; } //N301
        [DataMember]
        public string city { get; set; } //N401
        [DataMember]
        public string state { get; set; } //N402
        [DataMember]
        public string zip { get; set; } //N403
        [DataMember]
        public string phone { get; set; } //PER04
    }
}
