﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Fault/2010/11/", Name = "DefaultFault")]
    public class DefaultFault
    {
        #region Constructors

        public DefaultFault()
            : this(-1, string.Empty)
        {
        }

        public DefaultFault(int errorId, string errorMessage)
        {
            this.ErrorId = errorId;
            this.ErrorMessage = errorMessage;
        }

        #endregion Constructors

        #region Properties

        [DataMember(IsRequired = true, Name = "ErrorId", Order = 1)]
        public int ErrorId { get; set; }

        [DataMember(IsRequired = true, Name = "ErrorMessage", Order = 2)]
        public string ErrorMessage { get; set; }

        #endregion Properties
    }
}
