﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Globalization;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class LookupRepository : ILookupRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LookupRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILookupRepository Members

        public IList<AmericanState> States()
        {
            IList<AmericanState> states = null;
            if (!InProcCacher.TryGet(CacheKey.States.ToString(), out states))
            {
                states = database.All<AmericanState>().ToList();
                InProcCacher.Set(CacheKey.States.ToString(), states);
            }
            return states.OrderBy(s => s.Name).ToList();
        }

        public IList<Insurance> Insurances()
        {
            IList<Insurance> insurances = null;
            if (!Cacher.TryGet(CacheKey.Insurances.ToString(), out insurances))
            {
                insurances = database.All<Insurance>().ToList();
                Cacher.Set(CacheKey.Insurances.ToString(), insurances);
            }
            return insurances;
        }

        public Insurance GetInsurance(int insuranceId)
        {
            return Insurances().SingleOrDefault(i => i.Id == insuranceId);
        }

        public IList<EthnicRace> Races()
        {
            IList<EthnicRace> races = null;
            if (!Cacher.TryGet(CacheKey.Races.ToString(), out races))
            {
                races = database.All<EthnicRace>().ToList();
                Cacher.Set(CacheKey.Races.ToString(), races);
            }
            return races;
        }

        public IList<DrugClassification> DrugClassifications()
        {
            IList<DrugClassification> classifcations = null;
            if (!Cacher.TryGet(CacheKey.DrugClassifications.ToString(), out classifcations))
            {
                classifcations = database.All<DrugClassification>().ToList();
                Cacher.Set(CacheKey.DrugClassifications.ToString(), classifcations);
            }
            return classifcations;
        }

        public IList<PaymentSource> PaymentSources()
        {
            IList<PaymentSource> paymentSources = null;
            if (!Cacher.TryGet(CacheKey.PaymentSources.ToString(), out paymentSources))
            {
                paymentSources = database.All<PaymentSource>().ToList();
                Cacher.Set(CacheKey.PaymentSources.ToString(), paymentSources);
            }
            return paymentSources;
        }

        public IList<ReferralSource> ReferralSources()
        {
            IList<ReferralSource> referralSources = null;
            if (!Cacher.TryGet(CacheKey.ReferralSources.ToString(), out referralSources))
            {
                referralSources = database.All<ReferralSource>().ToList();
                Cacher.Set(CacheKey.ReferralSources.ToString(), referralSources);
            }
            return referralSources;
        }

        public IList<DiagnosisCode> DiagnosisCodes()
        {
            IList<DiagnosisCode> diagnosisCodes = null;
            if (!InProcCacher.TryGet(CacheKey.DiagnosisCodes.ToString(), out diagnosisCodes))
            {
                diagnosisCodes = database.All<DiagnosisCode>().ToList();
                InProcCacher.Set(CacheKey.DiagnosisCodes.ToString(), diagnosisCodes);
            }
            return diagnosisCodes;
        }

        public IList<ProcedureCode> ProcedureCodes()
        {
            IList<ProcedureCode> procedureCodes = null;
            if (!InProcCacher.TryGet(CacheKey.ProcedureCodes.ToString(), out procedureCodes))
            {
                procedureCodes = database.All<ProcedureCode>().ToList();
                InProcCacher.Set(CacheKey.ProcedureCodes.ToString(), procedureCodes);
            }
            return procedureCodes;
        }

        public IList<SupplyCategory> SupplyCategories()
        {
            IList<SupplyCategory> supplyCategories = null;
            if (!Cacher.TryGet(CacheKey.SupplyCategories.ToString(), out supplyCategories))
            {
                supplyCategories = database.All<SupplyCategory>().ToList();
                Cacher.Set(CacheKey.SupplyCategories.ToString(), supplyCategories);
            }
            return supplyCategories;
        }

        public IList<AdmissionSource> AdmissionSources()
        {
            IList<AdmissionSource> admissionSources = null;
            if (!Cacher.TryGet(CacheKey.AdmissionSources.ToString(), out admissionSources))
            {
                admissionSources = database.All<AdmissionSource>().ToList();
                Cacher.Set(CacheKey.AdmissionSources.ToString(), admissionSources);
            }
            return admissionSources;
        }

        public IList<Relationship> Relationships()
        {
            IList<Relationship> relationships = null;
            if (!Cacher.TryGet(CacheKey.Relationships.ToString(), out relationships))
            {
                relationships = database.All<Relationship>().ToList();
                Cacher.Set(CacheKey.Relationships.ToString(), relationships);
            }
            return relationships;
        }

        public Relationship GetRelationship(int Id)
        {
            return Relationships().SingleOrDefault(a => a.Id == Id);
        }

        public AdmissionSource GetAdmissionSource(int sourceId)
        {
            return AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
        }

        public string GetAdmissionSourceCode(int sourceId)
        {
            var addmissionCode = AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
            if (addmissionCode!=null)
            {
                return addmissionCode.Code;
            }
            return string.Empty;
        }

        public IList<Supply> Supplies()
        {
            IList<Supply> supplies = null;
            if (!Cacher.TryGet(CacheKey.Supplies.ToString(), out supplies))
            {
                supplies = database.All<Supply>().ToList();
                Cacher.Set(CacheKey.Supplies.ToString(), supplies);
            }
            return supplies;
        }

        public Supply GetSupply(int Id)
        {
            return database.Single<Supply>(s => s.Id == Id);
        }

        public Npi GetNpiData(string npiId)
        {
            return database.Single<Npi>(n => n.Id == npiId);
        }

        public IList<ZipCode> ZipCode()
        {
            IList<ZipCode> zipCodes = null;
            if (!InProcCacher.TryGet(CacheKey.ZipCodes.ToString(), out zipCodes))
            {
                zipCodes = database.All<ZipCode>().ToList();
                InProcCacher.Set(CacheKey.ZipCodes.ToString(), zipCodes);
            }
            return zipCodes;
        }

        public ZipCode GetZipCode(string zipCode)
        {
            return ZipCode().FirstOrDefault(z => z.Code == zipCode);
        }

        public Axxess.LookUp.Domain.IPAddress GetIPAddress(int ipAddress)
        {
            return database.Single<Axxess.LookUp.Domain.IPAddress>(i => i.Ip == ipAddress);
        }

        public string GetZipCodeFromIpAddress(int ipAddress)
        {
            var query = database.GetPaged<Axxess.LookUp.Domain.IPAddress>(i => i.Ip < ipAddress, "Ip desc", 0, 1);
            if (query.Count > 0)
            {
                return query[0].ZipCode;
            }
            return string.Empty;
        }

        public IList<Npi> GetNpis(string q, int limit)
        {
            var npies = new List<Npi>();
            var npiSQL = string.Format(@"SELECT 
                            `Id`, 
                            `ProviderLastName`, 
                            `ProviderFirstName`,
                            `ProviderMiddleName`, 
                            `ProviderCredentialText`, 
                            `ProviderFirstLineBusinessPracticeLocationAddress`,
                            `ProviderSecondLineBusinessPracticeLocationAddress`,
                            `ProviderBusinessPracticeLocationAddressCityName`,
                            `ProviderBusinessPracticeLocationAddressStateName`,
                            `ProviderBusinessPracticeLocationAddressPostalCode`,
                            `ProviderBusinessPracticeLocationAddressTelephoneNumber`,
                            `ProviderBusinessPracticeLocationAddressFaxNumber`
                                FROM
                                     `npis`
                                            WHERE 
                                                `EntityTypeCode` = 1 AND
                                                `Id` LIKE '{0}%' 
                                                 LIMIT 0, {1};", q, limit);

            using (var cmd = new FluentCommand<Npi>(npiSQL))
            {
                npies = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new Npi
                    {
                        Id = reader.GetStringNullable("Id"),
                        ProviderLastName = reader.GetStringNullable("ProviderLastName"),
                        ProviderFirstName = reader.GetStringNullable("ProviderFirstName"),
                        ProviderMiddleName = reader.GetStringNullable("ProviderMiddleName"),
                        ProviderCredentialText = reader.GetStringNullable("ProviderCredentialText"),
                        ProviderFirstLineBusinessPracticeLocationAddress = reader.GetStringNullable("ProviderFirstLineBusinessPracticeLocationAddress"),
                        ProviderSecondLineBusinessPracticeLocationAddress = reader.GetStringNullable("ProviderSecondLineBusinessPracticeLocationAddress"),
                        ProviderBusinessPracticeLocationAddressCityName = reader.GetStringNullable("ProviderBusinessPracticeLocationAddressCityName"),
                        ProviderBusinessPracticeLocationAddressStateName = reader.GetStringNullable("ProviderBusinessPracticeLocationAddressStateName"),
                        ProviderBusinessPracticeLocationAddressPostalCode = reader.GetStringNullable("ProviderBusinessPracticeLocationAddressPostalCode"),
                        ProviderBusinessPracticeLocationAddressTelephoneNumber = reader.GetStringNullable("ProviderBusinessPracticeLocationAddressTelephoneNumber"),
                        ProviderBusinessPracticeLocationAddressFaxNumber = reader.GetStringNullable("ProviderBusinessPracticeLocationAddressFaxNumber")

                    })
                    .AsList();
            }

            return npies;

            // return database.GetPaged<Npi>(n => n.Id.StartsWith(q) && n.EntityTypeCode == "1", 0, limit);
        }


        public bool VerifyPecos(string npi)
        {
            return database.Exists<PecosPhysician>(p => p.Id == npi);
        }

        public IList<DisciplineTask> DisciplineTasks(string Discipline)
        {
            IList<DisciplineTask> disciplineTasks = null;
            if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            {
                disciplineTasks = database.All<DisciplineTask>().ToList();
                Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            }
            if (Discipline.IsEqual("nursing"))
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == "Nursing" || e.Discipline == "ReportsAndNotes" && e.Task != "Medicare Eligibility Report").ToList();
            }
            else if (Discipline.IsEqual("therapy"))
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST").ToList();
            }
            else
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == Discipline).ToList();
            }
            return disciplineTasks.OrderBy(t => t.Task).ToList();
        }

        public IList<DisciplineTask> DisciplineTasks()
        {
            IList<DisciplineTask> disciplineTasks = null;
            //if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            //{
                disciplineTasks = database.All<DisciplineTask>().ToList();
                Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            //}
            return disciplineTasks.Cast<DisciplineTask>().OrderBy(d => d.Task).ToList();
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return DisciplineTasks().SingleOrDefault(t => t.Id == disciplineTaskId);
        }

        public IList<MedicationRoute> MedicationRoute(string q, int limit)
        {
            return database.Find<MedicationRoute>(m => m.LongName.Contains(q));
        }

        public IList<MedicationClassfication> MedicationClassification(string q, int limit)
        {
            return database.Find<MedicationClassfication>(m => m.Name.Contains(q));
        }

        public IList<MedicationDosage> MedicationDosage(string q, int limit)
        {
            return database.Find<MedicationDosage>(m => m.CommonName.StartsWith(q) || m.CommonName.Contains(q));
        }

        public IList<PPSStandard> PPSStandards()
        {
            IList<PPSStandard> ppsStandards = null;
            if (!Cacher.TryGet(CacheKey.PPSStandards.ToString(), out ppsStandards))
            {
                ppsStandards = database.All<PPSStandard>().ToList();
                Cacher.Set(CacheKey.PPSStandards.ToString(), ppsStandards);
            }
            return ppsStandards;
        }

        public IList<HippsAndHhrg> HippsAndHhrgs()
        {
            IList<HippsAndHhrg> hippsAndHhrgs = null;
            if (!Cacher.TryGet(CacheKey.HippsAndHhrg.ToString(), out hippsAndHhrgs))
            {
                hippsAndHhrgs = database.All<HippsAndHhrg>().ToList();
                Cacher.Set(CacheKey.HippsAndHhrg.ToString(), hippsAndHhrgs);
            }
            return hippsAndHhrgs;
        }

        public IList<CBSACode> CbsaCodes()
        {
            IList<CBSACode> cbsaCodes = null;
            if (!InProcCacher.TryGet(CacheKey.CBSACode.ToString(), out cbsaCodes))
            {
                cbsaCodes = database.All<CBSACode>().ToList();
                InProcCacher.Set(CacheKey.CBSACode.ToString(), cbsaCodes);
            }
            return cbsaCodes;
        }

        public IList<RuralCbsaCode> RuralCbsaCodes()
        {
            IList<RuralCbsaCode> ruralCbsaCodes = null;
            if (!InProcCacher.TryGet(CacheKey.RuralCbsaCode.ToString(), out ruralCbsaCodes))
            {
                ruralCbsaCodes = database.All<RuralCbsaCode>().ToList();
                InProcCacher.Set(CacheKey.RuralCbsaCode.ToString(), ruralCbsaCodes);
            }
            return ruralCbsaCodes;
        }

        public string CbsaCodeByZip(string zipCode)
        {
            var cbsa = string.Empty;
            if (zipCode.IsNotNullOrEmpty())
            {
                var cbsaCode = CbsaCodes().Where(c => c.Zip == zipCode).FirstOrDefault();
                if (cbsaCode != null)
                {
                    cbsa = cbsaCode.CBSA;
                }
            }
            return cbsa;
        }

        public CBSACode CbsaCode(string zipCode)
        {
            if (zipCode.IsNullOrEmpty())
            {
                return null;
            }
            return CbsaCodes().Where(c => c.Zip == zipCode).FirstOrDefault();
        }

        public RuralCbsaCode RuralCbsaCode(string stateCode)
        {
            if (stateCode.IsNullOrEmpty())
            {
                return null;
            }
            return RuralCbsaCodes().Where(c => c.StateCode == stateCode).FirstOrDefault();
        }

        public HippsAndHhrg GetHHRGByHIPPSCODE(string hippsCode)
        {
            return HippsAndHhrgs().Where(h => h.HIPPS == hippsCode).FirstOrDefault();
        }

        public HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            return HippsAndHhrgs().Where(h => h.HIPPS == hippsCode && h.Time.Year == year).FirstOrDefault();
        }

        public PPSStandard GetPPSStandardByYear(int year)
        {
            return PPSStandards().Where(s => s.Time.Year == year).FirstOrDefault();
        }

        public double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode)
        {
            Double prospectivePayment = 0;

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();
                var cbsaCode = CbsaCode(zipCode);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    var totalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment = totalAmount;

                }
            }
            return prospectivePayment;
        }

        public ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode)
        {
            ProspectivePayment prospectivePayment = null;

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                prospectivePayment = new ProspectivePayment();
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();
                var cbsaCode = CbsaCode(zipCode);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment.Hhrg = hhrg.HHRG;
                    prospectivePayment.CbsaCode = cbsaCode.CBSA;
                    prospectivePayment.WageIndex = wageIndex.ToString();
                    prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                    prospectivePayment.LaborAmount = string.Format("${0:#0.00}", laborAmount);
                    prospectivePayment.NonLaborAmount = string.Format("${0:#0.00}", nonLaborAmount);
                    prospectivePayment.NonRoutineSuppliesAmount = string.Format("${0:#0.00}", supplyAmount);
                    prospectivePayment.TotalAmountWithoutSupplies = string.Format("${0:#0.00}", totalAmountWithoutSupplies);
                    prospectivePayment.TotalProspectiveAmount = string.Format("${0:#0.00}", prospectivePayment.TotalAmount);
                }
            }
            return prospectivePayment;
        }

        public double GetRuralProspectivePaymentAmount(string hippsCode, DateTime time, string stateCode)
        {
            Double prospectivePayment = 0;

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && stateCode.IsNotNullOrEmpty())
            {
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();

                var cbsaCode = RuralCbsaCode(stateCode);
                var ppsStandard = GetPPSStandardByYear(time.Year);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = true;
                    var wageIndex = this.WageIndex(cbsaCode, time);
                    rate = ppsStandard.RuralRate;

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    var totalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment = totalAmount;
                }
            }
            return prospectivePayment;
        }

        public ProspectivePayment GetRuralProspectivePayment(string hippsCode, DateTime time, string stateCode)
        {
            ProspectivePayment prospectivePayment = null;

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && stateCode.IsNotNullOrEmpty())
            {
                prospectivePayment = new ProspectivePayment();
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();

                var cbsaCode = RuralCbsaCode(stateCode);
                var ppsStandard = GetPPSStandardByYear(time.Year);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = true;
                    var wageIndex = this.WageIndex(cbsaCode, time);
                    rate = ppsStandard.RuralRate;

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment.Hhrg = hhrg.HHRG;
                    prospectivePayment.CbsaCode = cbsaCode.CBSA;
                    prospectivePayment.WageIndex = wageIndex.ToString();
                    prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                    prospectivePayment.LaborAmount = string.Format("${0:#0.00}", laborAmount);
                    prospectivePayment.NonLaborAmount = string.Format("${0:#0.00}", nonLaborAmount);
                    prospectivePayment.NonRoutineSuppliesAmount = string.Format("${0:#0.00}", supplyAmount);
                    prospectivePayment.TotalAmountWithoutSupplies = string.Format("${0:#0.00}", totalAmountWithoutSupplies);
                    prospectivePayment.TotalProspectiveAmount = string.Format("${0:#0.00}", prospectivePayment.TotalAmount);
                }
            }
            return prospectivePayment;
        }

        public IList<MedicareRate> DefaultMedicareRates()
        {
            IList<MedicareRate> medicareRates = null;
            if (!Cacher.TryGet(CacheKey.MedicareRates.ToString(), out medicareRates))
            {
                medicareRates = database.All<MedicareRate>().ToList();
                Cacher.Set(CacheKey.MedicareRates.ToString(), medicareRates);
            }
            return medicareRates;
        }

        public MedicareProvider GetMedicareProvider(string providerNumber)
        {
            return database.Single<MedicareProvider>(n => n.ProviderNumber == providerNumber);
        }

        public List<string> NpisWithPecoVerified(string [] npis)
        {
            var list = new List<object>();
            var ids = npis.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            if (npis != null && npis.Length > 0)
            {
                var script = string.Format(@"SELECT  Id  FROM  pecosphysicians WHERE Id IN ({0}) ", ids);

                using (var cmd = new FluentCommand<object>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader =>
                        reader.GetStringNullable("Id")
                    ).AsList();
                }
            }
            return list.Select(p => p.ToString()).ToList();
        }

        #endregion

        #region Private Methods

        private double WageIndex(ICbsaCode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2014) wageIndex = cbsaCode.WITwoFourteen;
            else if (time.Year == 2013) wageIndex = cbsaCode.WITwoThirteen;
            else if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        #endregion
    }
}
