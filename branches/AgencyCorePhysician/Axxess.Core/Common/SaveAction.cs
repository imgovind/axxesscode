﻿namespace Axxess.Core
{
    public enum SaveAction
    {
        None,
        New,
        Edit,
        Delete
    }
}
