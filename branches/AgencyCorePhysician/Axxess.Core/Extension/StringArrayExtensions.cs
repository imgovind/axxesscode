﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Diagnostics;

    public static class StringArrayExtensions
    {
        [DebuggerStepThrough]
        public static string[] Reverse(this string[] elements)
        {
            Array.Reverse(elements);
            return elements;
        }

        [DebuggerStepThrough]
        public static string PhoneEncode(this string[] phoneNumberElements)
        {
            string PhoneNumber = string.Empty;
            foreach (string element in phoneNumberElements)
            {
                PhoneNumber += element;
            }
            return PhoneNumber;
        }

        [DebuggerStepThrough]
        public static string AddColons(this string[] elements)
        {
            int i = 0;
            string fixedString = string.Empty;

            foreach (string element in elements)
            {
                if (i < elements.Length)
                {
                    fixedString += element + ";";
                }
                else
                {
                    fixedString += element;
                }
                i++;
            }
            return fixedString;
        }
    }
}
