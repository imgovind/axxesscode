﻿namespace Axxess.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum CacheType
    {
        Agency = 1,
        Branch = 2,
        User = 3,
        Patient = 4,
        Physician = 5,
        Insurance = 6,
        UserSession = 7
    }
}
