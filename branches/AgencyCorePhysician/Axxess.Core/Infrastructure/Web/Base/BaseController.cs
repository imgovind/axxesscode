﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;

    [Audit]
    public abstract class BaseController : Controller
    {
        public static T Validate<T>(params Validation[] validations) where T : JsonViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();
            
            if (!entityValidator.IsValid)
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }

            else
            {
                return new T { isSuccessful = true };
            }
        }
    }
}
