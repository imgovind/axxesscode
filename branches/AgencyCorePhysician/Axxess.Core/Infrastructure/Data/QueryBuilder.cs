﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Text;

    public class QueryBuilder
    {
        private StringBuilder query;

        public QueryBuilder(string baseQuery)
        {
            Check.Argument.IsNotNull(baseQuery, "baseQuery");

            this.query = new StringBuilder(baseQuery.Trim());
        }

        public QueryBuilder Where(string whereClause)
        {
            if (this.query.ToString().Contains("WHERE"))
            {
                this.query.Append(whereClause.Trim());
            }
            else
            {
                this.query.AppendFormat(" WHERE {0}", whereClause.Trim());
            }
            return this;
        }

        public QueryBuilder And(string andStatement)
        {
            this.query.AppendFormat(" AND {0}", andStatement.Trim());
            return this;
        }

        public QueryBuilder Or(string orStatement)
        {
            this.query.AppendFormat(" OR {0}", orStatement.Trim());
            return this;
        }

        public QueryBuilder OrderBy(string column, bool isAscending)
        {
            if (isAscending)
            {
                this.query.AppendFormat(" ORDER BY {0} ASC", column.Trim());
            }
            else
            {
                this.query.AppendFormat(" ORDER BY {0} DESC", column.Trim());
            }
            return this;
        }


        public QueryBuilder LimitTo(int offset, int count)
        {
            this.query.AppendFormat(" LIMIT {0},{1}", offset.ToString(), count.ToString());
            return this;
        }

        public string Build()
        {
            return query.ToString();
        }
    }

}
