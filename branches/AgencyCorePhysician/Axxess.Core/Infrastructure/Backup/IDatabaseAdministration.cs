﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public interface IDatabaseAdministration
    {
        string DatabaseName { get; }
        string FilePath { get; set; }
        void Backup(bool enableEncryption);
        void Restore(bool enableEncryption);
    }
}
