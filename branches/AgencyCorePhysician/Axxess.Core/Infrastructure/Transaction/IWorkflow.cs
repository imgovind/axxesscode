﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public interface IWorkflow
    {
        void Process();
        string Message { get; }
        bool IsCommitted { get; }
    }
}
