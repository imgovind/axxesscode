﻿namespace Axxess.Core.Infrastructure
{
    using System;

    using Axxess.Core.Extension;

    public static class CoreSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static bool UseSSL
        {
            get
            {
                return configuration.AppSettings["UseSSL"].ToBoolean();
            }
        }

        public static bool CompressHtmlResponse
        {
            get
            {
                return configuration.AppSettings["CompressHtmlResponse"].ToBoolean();
            }
        }

        public static double CachingIntervalInMinutes
        {
            get
            {
                return configuration.AppSettings["CachingIntervalInMinutes"].ToDouble();
            }
        }

        public static double MembaseSessionTimeoutInMinutes
        {
            get
            {
                return configuration.AppSettings["MembaseSessionTimeoutInMinutes"].ToDouble();
            }
        }

        public static string[] MemcacheServerUriArray
        {
            get
            {
                return configuration.AppSettings["MemcacheServerUris"].Split(';');
            }
        }

        public static string MemcacheBucket
        {
            get
            {
                return configuration.AppSettings["MemcacheBucket"];
            }
        }


        public static string NoReplyEmail
        {
            get
            {
                return configuration.AppSettings["NoReplyEmail"];
            }
        }

        public static string NoReplyDisplayName
        {
            get
            {
                return configuration.AppSettings["NoReplyDisplayName"];
            }
        }

        public static bool EnableSSLMail
        {
            get
            {
                return configuration.AppSettings["EnableSSLMail"].ToBoolean();
            }
        }

        public static string MailTemplate
        {
            get
            {
                return configuration.AppSettings["TemplateDirectory"];
            }
        }

        public static string ExcelDirectory
        {
            get
            {
                return configuration.AppSettings["ExcelDirectory"];
            }
        }

        public static string CryptoPassPhrase
        {
            get
            {
                return configuration.AppSettings["CryptoPassPhrase"];
            }
        }

        public static string CryptoSaltValue
        {
            get
            {
                return configuration.AppSettings["CryptoSaltValue"];
            }
        }

        public static string CryptoHashAlgorithm
        {
            get
            {
                return configuration.AppSettings["CryptoHashAlgorithm"];
            }
        }

        public static string CryptoInitVector
        {
            get
            {
                return configuration.AppSettings["CryptoInitVector"];
            }
        }

        public static int CryptoPasswordIterations
        {
            get
            {
                return configuration.AppSettings["CryptoPasswordIterations"].ToInteger();
            }
        }

        public static int CryptoKeySize
        {
            get
            {
                return configuration.AppSettings["CryptoKeySize"].ToInteger();
            }
        }

        public static string[] NoEncryptPaths
        {
            get
            {
                return configuration.AppSettings["NoEncryptPaths"].Split(new string[] {";"}, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NonSecurePaths
        {
            get
            {
                return configuration.AppSettings["NonSecurePaths"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NoEncryptFiles
        {
            get
            {
                return configuration.AppSettings["NoEncryptFiles"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static bool MobileTestEnvironment
        {
            get
            {
                return configuration.AppSettings["MobileTestEnvironment"] == "true" ? true : false;
            }
        }

        public static bool MobileRedirect
        {
            get
            {
                return configuration.AppSettings["MobileRedirect"] == "true" ? true : false;
            }
        }

        public static int MaxFileSizeInBytes
        {
            get
            {
                return configuration.AppSettings["MaxFileSizeInBytes"].ToInteger();
            }
        }


        public static string AssetSecret { get { return configuration.AppSettings["AssetSecret"]; } }
        public static string AssetKey { get { return configuration.AppSettings["AssetKey"]; } }
        public static string AssetUploadURL { get { return configuration.AppSettings["AssetUploadURL"]; } }
        public static string AssetDeleteURL { get { return configuration.AppSettings["AssetDeleteURL"]; } }
        public static string AssetServeURL { get { return configuration.AppSettings["AssetServeURL"]; } }
        public static bool UseDBAssets { get { return configuration.AppSettings["UseDBAssets"].ToBoolean(); } }
        public static bool StoreAssetBytesInDB { get { return configuration.AppSettings["StoreAssetBytesInDB"].ToBoolean(); } }

    
    }
}
