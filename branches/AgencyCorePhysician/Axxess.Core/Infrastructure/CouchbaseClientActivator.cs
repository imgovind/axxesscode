﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Net;

    using Couchbase;
    using Couchbase.Configuration;

    using Enyim.Caching.Memcached;

    public static class CouchbaseClientActivator
    {
        #region Members

        private static CouchbaseClient couchbaseClient;

        public static CouchbaseClient Cache
        {
            get
            {
                if (couchbaseClient == null)
                {
                    LoadConfig();
                }
                return couchbaseClient;
            }
        }

        private static void LoadConfig()
        {
            var couchbaseClientConfiguration = new CouchbaseClientConfiguration();
            couchbaseClientConfiguration.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);
            couchbaseClientConfiguration.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 2);
            couchbaseClientConfiguration.Bucket = CoreSettings.MemcacheBucket;
            couchbaseClientConfiguration.BucketPassword = "";

            //Quick test of Store/Get operations
            if (CoreSettings.MemcacheServerUriArray != null && CoreSettings.MemcacheServerUriArray.Length > 0)
            {
                foreach (var serverUri in CoreSettings.MemcacheServerUriArray)
                {
                    couchbaseClientConfiguration.Urls.Add(new Uri(serverUri));
                }
                couchbaseClient = new CouchbaseClient(couchbaseClientConfiguration);
            }
        }

        #endregion
    }
}
