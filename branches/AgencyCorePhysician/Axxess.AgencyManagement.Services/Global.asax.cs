﻿namespace Axxess.AgencyManagement.Services
{
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.AgencyManagement.App;

    public class MvcApplication : System.Web.HttpApplication
    {
        public override void Init()
        {
            base.Init();
            this.AcquireRequestState += new System.EventHandler(MvcApplication_AcquireRequestState);
        }

        void MvcApplication_AcquireRequestState(object sender, System.EventArgs e)
        {
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            Bootstrapper.RunApi();

            AreaRegistration.RegisterAllAreas();
            RegisterRoutes(RouteTable.Routes);
        }
    }
}