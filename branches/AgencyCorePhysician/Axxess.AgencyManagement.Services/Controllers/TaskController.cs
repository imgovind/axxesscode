﻿namespace Axxess.AgencyManagement.Services.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.App;
    using Axxess.AgencyManagement.App.Security;

    [HandleError]
    [AxxessAuthorize]
    public class TaskController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Message"] = string.Format("Welcome {0}!", Current.UserFullName);

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
