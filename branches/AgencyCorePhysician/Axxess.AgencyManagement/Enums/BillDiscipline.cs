﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Enums
{
   public enum BillDiscipline
    {
        [CustomDescription("Skilled Nursing", "SNV")]
        Nursing,
        [CustomDescription("Physical Therapy", "PT")]
        PT,
        [CustomDescription("Occupational Therapy", "OT")]
        OT,
        [CustomDescription("Speech Therapy", "ST")]
        ST,
        [CustomDescription("Dietitian", "DV")]
        Dietician,
        [CustomDescription("HHA", "HHA")]
        HHA,
        [CustomDescription("Social Worker", "MSW")]
        MSW
    }
}
