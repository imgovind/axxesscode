﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    using SubSonic.SqlGeneration.Schema;

    public class Birthday
    {
        public Guid Id { get;  set; }
        public string Name { get;  set; }
        public string IdNumber { get; set; }
        public DateTime Date { get;  set; }
        public string BirthDay
        {
            get
            {
                return string.Format("{0:MMMM d}", this.Date);
            }
        }
        public string Age
        {
            get
            {
                int age = DateTime.Now.Year - this.Date.Year;
                if (DateTime.Now.Month < this.Date.Month || (DateTime.Now.Month == this.Date.Month && DateTime.Now.Day < this.Date.Day))
                    age--;
                return (age).ToString();
            }
        }
        public string DbAge { get; set; }
        public string Address { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.IsNotNullOrEmpty() ? this.AddressLine1.Trim().ToTitleCase() : string.Empty, this.AddressLine2.IsNotNullOrEmpty() ? this.AddressLine2.Trim().ToTitleCase() : string.Empty);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim().ToTitleCase();
                }
                return string.Empty;
            }
        }
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.IsNotNullOrEmpty()? this.AddressCity.Trim().ToTitleCase():string.Empty, this.AddressStateCode.IsNotNullOrEmpty()? this.AddressStateCode.Trim().ToTitleCase():string.Empty, this.AddressZipCode.IsNotNullOrEmpty()? this.AddressZipCode.Trim().ToTitleCase():string.Empty);
            }
        }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        public string PhoneMobile { get; set; }
        public string PhoneMobileFormatted { get { return this.PhoneMobile.ToPhone(); } }
        public string EmailAddress { get; set; }
    }
}
