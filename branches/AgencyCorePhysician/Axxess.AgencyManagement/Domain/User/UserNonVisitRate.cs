﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public class UserNonVisitRate : EntityBase
    {

        #region Members
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public string Rate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        #endregion

        #region Validation Rules
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyId.ToString()), "Task is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.UserId.ToString()), "User is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Rate.ToString()), "Rate is required. <br />"));
        }
        #endregion

    }
}
