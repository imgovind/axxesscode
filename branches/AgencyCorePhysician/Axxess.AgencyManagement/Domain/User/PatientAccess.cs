﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class PatientAccess
    {
        public List<SelectedPatient> Granted { get; set; }
        public List<SelectedPatient> Denied { get; set; }
    }
}
