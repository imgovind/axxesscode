﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core;
    using Axxess.Core.Extension;
    public class PatientEvacuation
    {
        public Guid Id { get; set; }
        public string PatientName { get; set; }
        public int Triage { get; set; }
        public string PatientIdNumber { get; set; }
        public string ContactName { get; set; }
        public string ContactRelation { get; set; }
        public string ContactPhoneHome { get; set; }
        public string ContactEmailAddress { get; set; }
        public string EvacuationAddress { get; set; }
        public string EvacuationAddressLine1 { get; set; }
        public string EvacuationAddressLine2 { get; set; }
        public string EvacuationAddressCity { get; set; }
        public string EvacuationAddressStateCode { get; set; }
        public string EvacuationAddressZipCode { get; set; }
        public string EvacuationPhoneHome { get; set; }
        public string EvacuationZone { get; set; }
        public string EvacuationPhoneMobile { get; set; }
        public string EvacuationPhoneHomeFormatted
        {
            get
            {
                return this.EvacuationPhoneHome.ToPhone();
            }
        }
        public string EvacuationPhoneMobileFormatted
        {
            get
            {
                return this.EvacuationPhoneMobile.ToPhone();
            }
        }
        public string Address
        {
            get
            {
                if ((this.EvacuationAddressLine1 + this.EvacuationAddressLine2).Trim().Length != 0)
                {
                    return this.EvacuationAddressLine1 + " " + this.EvacuationAddressLine2 + ", " + this.EvacuationAddressCity + " " + this.EvacuationAddressStateCode + " " + this.EvacuationAddressZipCode;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
