﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    public class SelectedUser
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Credentials { get; set; }
        public string Suffix { get; set; }
        public string CredentialsOther { get; set; }
        public Guid PatientUserId { get; set; }
        public bool Selected { get; set; }
        public string Roles { get; set; }

        public string DisplayName
        {
            get
            {
                var nameBuilder = new StringBuilder();

                if (Credentials.IsEqual("None"))
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Suffix);
                    return nameBuilder.ToString().TrimEnd();
                }

                if (CredentialsOther.IsNotNullOrEmpty())
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                    if (this.CredentialsOther.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.CredentialsOther);
                    return nameBuilder.ToString().TrimEnd();
                }

                if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                if (this.Credentials.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Credentials);
                return nameBuilder.ToString().TrimEnd();
            }
        }
    }
}
