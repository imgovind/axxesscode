﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Enums;

    using Axxess.Core.Extension;

    public class PatientEpisodeData
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public string EndDate { get; set; }
        public string Details { get; set; }
        public string Schedule { get; set; }
        public string StartDate { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public Nullable<DateTime> PatientDischargeDate { get; set; }
        public bool IsDischarged { get { return this.Status == (int)PatientStatus.Discharged; } }
        public bool IsEpisodeDischarged { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DOB { get; set; }
        public string MiddleInitial { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
    }
}
