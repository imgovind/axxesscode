﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class ManagedClaimEpisodeData
    {
        public string ClaimKey { get; set; }
        public string HippsCode { get; set; }
        public double ProspectivePay { get; set; }
        public string AssessmentType { get; set; }
        public string DiagnosisCode1 { get; set; }
        public string DiagnosisCode2 { get; set; }
        public string DiagnosisCode3 { get; set; }
        public string DiagnosisCode4 { get; set; }
        public string DiagnosisCode5 { get; set; }
        public string DiagnosisCode6 { get; set; }
    }
}
