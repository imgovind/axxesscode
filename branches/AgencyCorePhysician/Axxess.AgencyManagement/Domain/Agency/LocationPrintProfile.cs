﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class LocationPrintProfile
    {
        /// <summary>
        /// Agency name
        /// </summary>
        public string Name { get; set; }
        public string OfficeName { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string ContactPersonEmail { get; set; }
        /// <summary>
        /// Location informations
        /// </summary>
        /// 

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneWorkFormatted { get; set; }
        public string FaxNumberFormatted { get; set; }

        public bool IsLocationStandAlone { get; set; }
    }
}
