﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    public class AgencyCluster
    {
        //physcianId
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int ClusterId { get; set; }

    }
}
