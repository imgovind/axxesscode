﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class InsuranceCache
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int InvoiceType { get; set; }
        public int PayorType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public bool IsAxxessTheBiller { get; set; }
        public string PayorId { get; set; }
        #region Domain
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.IsNotNullOrEmpty() ? this.AddressCity.Trim() : string.Empty, this.AddressStateCode.IsNotNullOrEmpty() ? this.AddressStateCode.Trim() : string.Empty, this.AddressZipCode.IsNotNullOrEmpty() ? this.AddressZipCode.Trim() : string.Empty);
                }
                return string.Empty;
            }
        }
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2} ,{3}  {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} , {1} ,{2}  {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }
        public string InvoiceTypeName
        {
            get
            {
                return ((InvoiceType)Enum.ToObject(typeof(InvoiceType), this.InvoiceType)).GetDescription();
            }
        }
        public string PayerTypeName
        {
            get
            {
                return ((PayerTypes)Enum.ToObject(typeof(PayerTypes), this.PayorType)).GetDescription();
            }
        }
        #endregion
    }
}
