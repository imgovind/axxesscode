﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public class UploadType : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Type { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string CreatedDateString { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public string ModifiedDateString { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Type), "Upload Type is required.<br/>"));
        }
    }
}
