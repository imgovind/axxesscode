﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public static class LocationEngine
    {
        #region Private Members
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
        private static Guid mainLocationId = Guid.Empty;
        #endregion

        #region Public Methods

               
        
        //public static List<AgencyLocationLean> GetLocations(Guid agencyId, List<Guid> locationIds)
        //{
        //    var locations = new List<AgencyLocationLean>();
        //    var locationIdsNotInCache = new List<Guid>();
        //    if (!agencyId.IsEmpty() && locationIds != null && locationIds.Count > 0)
        //    {
        //        var keys = locationIds.Select(id => Key(agencyId, id));
        //        var results = Cacher.Get<string>(keys);
        //        if (results != null && results.Count > 0)
        //        {
        //            results.ForEach((key, value) =>
        //            {
        //                if (key.IsNotNullOrEmpty() && value != null)
        //                {
        //                    string locationXml = value.ToString();
        //                    if (locationXml.IsNotNullOrEmpty())
        //                    {
        //                        locations.Add(locationXml.ToObject<AgencyLocationLean>());
        //                    }
        //                }

        //            });
        //        }
        //        locationIdsNotInCache = locationIds.Where(id => !locations.Exists(u => u.Id == id)).ToList();
        //        if (locationIdsNotInCache != null && locationIdsNotInCache.Count > 0)
        //        {
        //            var locationsNotInCache = dataProvider.AgencyRepository.GetLocationsLean(agencyId, locationIdsNotInCache);
        //            if (locationIdsNotInCache != null && locationIdsNotInCache.Count > 0)
        //            {
        //                locationsNotInCache.ForEach(u =>
        //                {
        //                    var key = Key(agencyId, u.Id);
        //                    Cacher.Set<string>(key, u.ToXml());
        //                    locations.Add(u);

        //                });
        //            }
        //        }
        //    }
        //    return locations;
        //}

        public static void AddOrUpdate(Guid agencyId, AgencyLocationLean location)
        {
            if (!agencyId.IsEmpty() && location != null && !location.Id.IsEmpty())
            {
                var locationXml = location.ToXml();
                if (locationXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId, location.Id);
                    Cacher.Set<string>(key, locationXml);
                }
            }
        }

        #endregion

        #region Private Methods


        private static AgencyLocation AddLocation(Guid agencyId, Guid locationId, string key, int clusterId)
        {
            var location = dataProvider.AgencyRepository.FindLocation(agencyId, clusterId, locationId);
            if (location != null)
            {
                Cacher.Set<string>(key, location.ToXml());
            }
            return location;
        }

        private static string Key(Guid agencyId, Guid locationId)
        {
            return string.Format("{0}_{1}_{2}", agencyId, (int)CacheType.Branch, locationId);
        }

        #endregion
    }
}
