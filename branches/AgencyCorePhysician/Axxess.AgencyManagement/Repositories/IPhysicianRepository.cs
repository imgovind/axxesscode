﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.Core;

    public interface IPhysicianRepository
    {
        AgencyPhysician Get(Guid agencyId, int clusterId, Guid physicianId);
        IDictionary<string, AgencyCluster> GetAgencyPhysiciansByLoginIdLean(Guid LoginId);
        List<CarePlanOversight> GetCPO(Guid agencyId, int clusterId, Guid physicianLoginId, DateTime startDate, DateTime endDate);
        CarePlanOversight GetCPOById(Guid agencyId, int clusterId, Guid id );
    }
}
