﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Enums;
    using Domain;

    public interface IPatientRepository
    {
        #region New Methods
        Patient Get(Guid patientId, Guid agencyId, int clusterId);
        bool AddCPO(CarePlanOversight cpo, int clusterId);
        bool DeleteCPO(Guid agencyId, int clusterId, Guid id);
        Patient GetPatientOnly(Guid Id, Guid agencyId, int clusterId);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, int clusterId, Guid admissionId);
        List<Patient> GetPatientByAgencyPhysician(Guid agencyId, int clusterId, Guid loginId);
        PatientEpisode GetEpisodeById(Guid agencyId, int clusterId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, int clusterId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisode(Guid agencyId, int clusterId, Guid episodeId, Guid patientId);
        PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId, int clusterId);
        //PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId, int clusterId);
        #endregion

        #region SecondRound
        bool UpdateCPO(CarePlanOversight cpo, int clusterId);
        string GetPatientNameById(Guid patientId, Guid agencyId, int clusterId);
        string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId, int clusterId);
        bool UpdateOrderModel(int clusterId, PhysicianOrder order);
        PatientVisitNote GetVisitNote(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId);
        bool UpdateVisitNote(int clusterId, PatientVisitNote patientVisitNote);
        bool UpdateFaceToFaceEncounter(int clusterId, FaceToFaceEncounter faceToFaceEncounter);
        AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId, int clusterId);
        List<PhysicianOrder> GetPhysicianOrdersByPhysicianAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate);
        List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysicianAndDate(Guid agencyId, int clusterId, Guid physicianId, int[] status, DateTime startDate, DateTime endDate);
        List<OrderLean> GetEvalOrdersByPhysician(Guid agencyId, int clusterId, Guid physicianId, DateTime startDate, DateTime endDate, int[] status);
        bool AddReturnComment(ReturnComment returnComment, int clusterId);
        List<ReturnComment> GetReturnComments(Guid agencyId, int clusterId, Guid episodeId, Guid eventId);
        //bool UpdateEpisode(Guid agencyId, int clusterId, ScheduleEvent scheduleEvent);

        bool UpdateEpisode(PatientEpisode episode, int clusterId);
        //ScheduleEvent GetSchedule(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId);
        #endregion
    }
}
