﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IUserRepository
    {
        User Get(Guid agencyId, int clusterId, Guid id);
        void AddUserEvent(Guid agencyId, int clusterId, Guid patientId, Guid userId, UserEvent userEvent);
        UserEvent GetEvent(Guid agencyId, int clusterId, Guid userId, Guid patientId, Guid eventId);
        bool UpdateEvent(Guid agencyId, int clusterId, UserEvent userEvent);
        UserSchedule GetUserSchedule(Guid agencyId, int clusterId, Guid userId, Guid patientId);
    }
}
