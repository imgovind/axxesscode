﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;
    using Extensions;

    using SubSonic.Repository;
    using System.Text;
    using SubSonic.DataProviders;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

       
        #region Agency Methods

        public Agency Get(Guid id, int clusterId)
        {
            //var agency = database.Single<Agency>(a => a.Id == id);
            var agency = new Agency();
            var script = @"SELECT Id, Name, ContactPersonFirstName, ContactPersonLastName, ContactPersonEmail, ContactPersonPhone, IsDeprecated, ClusterId " +
                "FROM agencies WHERE Id=@id";
            using (var cmd = new FluentCommand<Agency>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("id", id)
                    .SetMap(reader => new Agency
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetStringNullable("Name"),
                        ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                        ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                        ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                        ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                        IsDeprecated = reader.GetInt("IsDeprecated") == 0 ? true : false,
                        ClusterId = reader.GetInt("ClusterId")
                    }).AsSingle();
            }
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id, clusterId);
            }

            return agency;
        }

        public Agency GetWithBranches(Guid agencyId, int clusterId)
        {
            var agency = Get(agencyId, clusterId);
            if (agency != null)
            {

                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    agency.Branches = database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
                }
            }

            return agency;
        }


        public AgencyLocation GetMainLocation(Guid agencyId, int clusterId)
        {
            AgencyLocation location = new AgencyLocation();
            var script = @"SELECT Id, AddressLine1, AddressLine2, AddressCity, AddressStateCode, AddressZipCode, AddressZipCodeFour, " +
                "PhoneWork, FaxNumber, Payor FROM agencylocations WHERE AgencyId=@agencyid AND IsMainOffice=1 AND IsDeprecated=0";
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyLocation
                    {
                        Id = reader.GetGuid("Id"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        AddressZipCodeFour = reader.GetStringNullable("AddressZipCodeFour"),
                        PhoneWork = reader.GetStringNullable("PhoneWork"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        Payor=reader.GetStringNullable("Payor")
                    }).AsSingle();
            }
            return location;
        }

        public LocationPrintProfile AgencyNameWithLocationAddress(Guid agencyId, Guid locationId, int clusterId)
        {
            var location = new LocationPrintProfile();
            var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencies.ContactPersonEmail as ContactPersonEmail,
                        agencies.MedicareProviderNumber as MedicareProviderNumber,
                        agencylocations.Name as OfficeName,
                        agencylocations.AddressLine1 as AddressLine1,
                        agencylocations.AddressLine2 as AddressLine2,
                        agencylocations.AddressCity as AddressCity,
                        agencylocations.AddressStateCode as AddressStateCode,
                        agencylocations.AddressZipCode as AddressZipCode,
                        agencylocations.PhoneWork as PhoneWork,
                        agencylocations.FaxNumber as FaxNumber,
                        agencylocations.MedicareProviderNumber as LocationMedicareProviderNumber,
                        agencylocations.IsLocationStandAlone as IsLocationStandAlone

                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.Id = @locationid LIMIT 0,1");

            using (var cmd = new FluentCommand<LocationPrintProfile>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("locationid", locationId)
                 .SetMap(reader => new LocationPrintProfile
                 {
                     //Id = reader.GetGuid("Id"),
                     Name = reader.GetStringNullable("Name"),
                     OfficeName = reader.GetStringNullable("OfficeName"),
                     AddressLine1 = reader.GetStringNullable("AddressLine1"),
                     AddressLine2 = reader.GetStringNullable("AddressLine2"),
                     AddressCity = reader.GetStringNullable("AddressCity"),
                     AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                     PhoneWorkFormatted = reader.GetStringNullable("PhoneWork").ToPhone(),
                     FaxNumberFormatted = reader.GetStringNullable("FaxNumber").ToPhone(),
                     ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                     IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                     MedicareProviderNumber = !reader.GetBoolean("IsLocationStandAlone") ? reader.GetStringNullable("MedicareProviderNumber") : reader.GetStringNullable("LocationMedicareProviderNumber")

                 }).AsSingle();
            }
            return location;
        }

        public LocationPrintProfile AgencyNameWithMainLocationAddress(Guid agencyId, int clusterId)
        {
            var location = new LocationPrintProfile();
            var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencies.ContactPersonEmail as ContactPersonEmail,
                        agencies.MedicareProviderNumber as MedicareProviderNumber,
                        agencylocations.Name as OfficeName,
                        agencylocations.AddressLine1 as AddressLine1,
                        agencylocations.AddressLine2 as AddressLine2,
                        agencylocations.AddressCity as AddressCity,
                        agencylocations.AddressStateCode as AddressStateCode,
                        agencylocations.AddressZipCode as AddressZipCode,
                        agencylocations.PhoneWork as PhoneWork,
                        agencylocations.FaxNumber as FaxNumber,
                        agencylocations.MedicareProviderNumber as LocationMedicareProviderNumber,
                        agencylocations.IsLocationStandAlone as IsLocationStandAlone
                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.IsMainOffice = 1 LIMIT 0,1");

            using (var cmd = new FluentCommand<LocationPrintProfile>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new LocationPrintProfile
                 {
                     //Id = reader.GetGuid("Id"),
                     Name = reader.GetStringNullable("Name"),
                     OfficeName = reader.GetStringNullable("OfficeName"),
                     AddressLine1 = reader.GetStringNullable("AddressLine1"),
                     AddressLine2 = reader.GetStringNullable("AddressLine2"),
                     AddressCity = reader.GetStringNullable("AddressCity"),
                     AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                     PhoneWorkFormatted = reader.GetStringNullable("PhoneWork").ToPhone(),
                     FaxNumberFormatted = reader.GetStringNullable("FaxNumber").ToPhone(),
                     ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                     IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                     MedicareProviderNumber = !reader.GetBoolean("IsLocationStandAlone") ? reader.GetStringNullable("MedicareProviderNumber") : reader.GetStringNullable("LocationMedicareProviderNumber")

                 }).AsSingle();
            }

            return location;
        }


        public AgencyLocation FindLocation(Guid agencyId, int clusterId, Guid Id)
        {
            //return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
            AgencyLocation location = new AgencyLocation();
            var script = @"SELECT Id, AddressLine1, AddressLine2, AddressCity, AddressStateCode, AddressZipCode, AddressZipCodeFour, " +
                "PhoneWork, FaxNumber, Payor FROM agencylocations WHERE Id=@id AND AgencyId=@agencyid";
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("id", Id)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyLocation
                    {
                        Id=reader.GetGuid("Id"),
                        AddressLine1=reader.GetStringNullable("AddressLine1"),
                        AddressLine2=reader.GetStringNullable("AddressLine2"),
                        AddressCity=reader.GetStringNullable("AddressCity"),
                        AddressStateCode=reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode=reader.GetStringNullable("AddressZipCode"),
                        AddressZipCodeFour=reader.GetStringNullable("AddressZipCodeFour"),
                        PhoneWork=reader.GetStringNullable("PhoneWork"),
                        FaxNumber=reader.GetStringNullable("FaxNumber"),
                        Payor=reader.GetStringNullable("Payor")
                    }).AsSingle();
            }
            return location;            
        }

        #endregion

     



   
      

       

      

       

        

       

   

       
     

   

  

     

       

      

       
    }
}
