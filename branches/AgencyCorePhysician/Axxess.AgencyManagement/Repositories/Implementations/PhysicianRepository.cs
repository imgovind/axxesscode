﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Domain;
    using Axxess.Core.Infrastructure;
    using SubSonic.DataProviders;

    public class PhysicianRepository : IPhysicianRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PhysicianRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }
        #endregion

        #region IPhysicianRepository Members

        public IDictionary<string, AgencyCluster> GetAgencyPhysiciansByLoginIdLean(Guid LoginId)
        {
            IDictionary<string, AgencyCluster> list = new Dictionary<string, AgencyCluster>();

            var script = @"SELECT 
                    ap.`Id`, 
                    ap.`AgencyId` ,
                    ag.Name,
                    ag.ClusterId
                        FROM
                           agencysnapshots ag INNER JOIN
                            physiciansnapshots ap 
                                ON ag.Id = ap.AgencyId
                                    WHERE
                                        ap.LoginId = @loginid AND
                                        ap.HasPhysicianAccess = 1 AND
                                        ap.IsDeprecated = 0";

            using (var cmd = new FluentCommand<AgencyCluster>(script))
            {
                list = cmd.SetConnection("AxxessMembershipConnectionString")
                .AddGuid("loginid", LoginId)
                .SetDictonaryId("AgencyId")
                .SetMap(reader => new AgencyCluster
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetStringNullable("Name"),
                    ClusterId = reader.GetInt("ClusterId")
                }).AsDictionary();
            };
            return list;
        }

        #endregion

        #region patientListExporterHelperMethods


        #endregion


        #region NewMethods


        public List<CarePlanOversight> GetCPO(Guid agencyId, int clusterId, Guid physicianLoginId, DateTime startDate, DateTime endDate)
        {
            var cpoList = new List<CarePlanOversight>();
            var script = @"SELECT
                            `Id`, 
                            `AgencyId`, 
                            `PatientId`, 
                            `PhysicianLoginId`, 
                            `CptCode`, 
                            `Duration`, 
                            `Activity`, 
                            `Comments`, 
                            `LogDate`, 
                            `Status`, 
                            `Created`, 
                            `Modified`, 
                            `IsDeprecated`
                                FROM careplanoversights
                                    WHERE AgencyId = @agencyid
                                        AND PhysicianLoginId = @physicianloginid
                                        AND (careplanoversights.LogDate between @startDate and @endDate) 
                                        AND careplanoversights.IsDeprecated = 0 ";

            using (var cmd = new FluentCommand<CarePlanOversight>(script))
            {
                cpoList = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("physicianloginid", physicianLoginId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new CarePlanOversight
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianLoginId = reader.GetGuid("PhysicianLoginId"),
                    CptCode = reader.GetStringNullable("CptCode"),
                    Duration = reader.GetInt("Duration"),
                    Activity = reader.GetInt("Activity"),
                    Comments = reader.GetStringNullable("Comments"),
                    LogDate = reader.GetDateTime("LogDate"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsList();
            }

            return cpoList.OrderBy(p => p.LogDate).ToList(); 
        }


        public CarePlanOversight GetCPOById(Guid agencyId, int clusterId, Guid id)
        {
            //return database.Find<CarePlanOversight>(c => c.Id == id).FirstOrDefault();
            CarePlanOversight cpo=null;
            var script = @"SELECT
                            `Id`, 
                            `AgencyId`, 
                            `PatientId`, 
                            `PhysicianLoginId`, 
                            `CptCode`, 
                            `Duration`, 
                            `Activity`, 
                            `Comments`, 
                            `LogDate`, 
                            `Status`, 
                            `Created`, 
                            `Modified`, 
                            `IsDeprecated`
                                FROM careplanoversights
                                    WHERE Id = @id AND AgencyId = @agencyid LIMIT 0, 1";

            using (var cmd = new FluentCommand<CarePlanOversight>(script))
            {
                cpo = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("id", id)
                .AddGuid("agencyid",agencyId)
                .SetMap(reader => new CarePlanOversight
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianLoginId = reader.GetGuid("PhysicianLoginId"),
                    CptCode = reader.GetStringNullable("CptCode"),
                    Duration = reader.GetInt("Duration"),
                    Activity = reader.GetInt("Activity"),
                    Comments = reader.GetStringNullable("Comments"),
                    LogDate = reader.GetDateTime("LogDate"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsSingle();
            }

            return cpo; 

        }

        public AgencyPhysician Get(Guid agencyId, int clusterId, Guid physicianId)
        {
            AgencyPhysician result = null;
            var script = @"SELECT
                            `Id`, 
                            `AgencyId`, 
                            `LoginId`, 
                            `NPI`, 
                            `UPIN`, 
                            `FirstName`, 
                            `LastName`, 
                            `MiddleName`, 
                            `Gender`, 
                            `Credentials`, 
                            `AddressLine1`, 
                            `AddressLine2`, 
                            `AddressCity`, 
                            `AddressStateCode`, 
                            `AddressZipCode`, 
                            `PhoneWork`, 
                            `PhoneAlternate`, 
                            `FaxNumber`, 
                            `EmailAddress`, 
                            `LicenseNumber`, 
                            `LicenseStateCode`, 
                            `Comments`, 
                            `Created`, 
                            `Modified`, 
                            `IsDeprecated`, 
                            `PhysicianAccess`, 
                            `Licenses`
                                FROM agencyphysicians
                                    WHERE AgencyId = @agencyid
                                        AND Id = @physicianid";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("physicianid", physicianId)
                .SetMap(reader => new AgencyPhysician{
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LoginId = reader.GetGuid("LoginId"),
                    NPI = reader.GetStringNullable("NPI"),
                    UPIN = reader.GetStringNullable("UPIN"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    MiddleName = reader.GetStringNullable("MiddleName"),
                    Gender = reader.GetStringNullable("Gender"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    LicenseNumber = reader.GetStringNullable("LicenseNumber"),
                    LicenseStateCode = reader.GetStringNullable("LicenseStateCode"),
                    Comments = reader.GetStringNullable("Comments"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    PhysicianAccess = reader.GetBoolean("PhysicianAccess"),
                    Licenses = reader.GetStringNullable("Licenses")
                })
                .AsSingle();
            }

            return result;
        }
        #endregion
    }
}
