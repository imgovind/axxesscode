﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using SubSonic.Repository;

    public class UserRepository : IUserRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public UserRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region NewMethod

        public void AddUserEvent(Guid agencyId, int clusterId, Guid patientId, Guid userId, UserEvent userEvent)
        {

            if (!agencyId.IsEmpty() && !patientId.IsEmpty() && !userId.IsEmpty() && userEvent != null)
            {
                var userEpisode = GetUserSchedule(agencyId, clusterId, userId, patientId);
                try
                {
                    if (userEpisode != null)
                    {
                        var events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
                        events.Add(userEvent);
                        userEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                        userEpisode.Modified = DateTime.Now;

                        var script = string.Format
                           (
                               "UPDATE agencymanagement.userschedules as us " +
                               "SET us.Visits = '{0}' " +
                               "AND us.Modified = '{1}' " +
                               "WHERE us.Id = '{2}' " +
                               "AND us.PatientId = '{3}' "
                               , userEpisode.Visits, userEpisode.Modified, userEpisode.Id, userEpisode.AgencyId
                            );
                        using (var cmd = new FluentCommand<UserSchedule>(script))
                        {
                            cmd.SetConnection("AgencyManagementConnectionString"+ clusterId).AsNonQuery(); 
                        }
                        //database.Update<UserSchedule>(userEpisode);
                    }
                    else if (userEpisode == null)
                    {
                        var userSchedule = new UserSchedule();

                        try
                        {
                            userSchedule.Id = Guid.NewGuid();
                            userSchedule.PatientId = patientId;
                            userSchedule.UserId = userId;
                            userSchedule.AgencyId = agencyId;
                            List<UserEvent> events = new List<UserEvent>();
                            events.Add(userEvent);
                            userSchedule.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                            userSchedule.Created = DateTime.Now;
                            userSchedule.Modified = DateTime.Now;
                            //database.Add<UserSchedule>(userSchedule);
                            string insertScript = string.Format("INSERT INTO `userschedules`(`Id`,`PatientId`,'UserId','AgencyId','Visits','Created','Modified') VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}');", userSchedule.Id, userSchedule.PatientId, userSchedule.UserId = userId, userSchedule.AgencyId, userSchedule.Visits, userSchedule.Created, userSchedule.Modified);
                            using (var cmd = new FluentCommand<UserSchedule>(insertScript))
                            {
                                cmd.SetConnection("AgencyManagementConnectionString" + clusterId).AsNonQuery();
                            }

                        }
                        catch (Exception e)
                        {
                            //TODO: Log Exception
                        }
                    }
                }
                catch (Exception e)
                {
                    //TODO Log Exception
                }
            }
        }

        public UserEvent GetEvent(Guid agencyId, int clusterId, Guid userId, Guid patientId, Guid eventId)
        {
            UserEvent evnt = null;
            if (!agencyId.IsEmpty() && !userId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                //var episode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
                var episode = GetUserSchedule( agencyId, clusterId, userId, patientId);
                if (episode != null && episode.Visits.IsNotNullOrEmpty())
                {
                    evnt = episode.Visits.ToObject<List<UserEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
                }
            }
            return evnt;
        }

        public UserSchedule GetUserSchedule(Guid agencyId, int clusterId, Guid userId, Guid patientId)
        {
            UserSchedule userSchedule = null;
            var script = @"SELECT
                            `Id`, 
                            `AgencyId`, 
                            `UserId`, 
                            `PatientId`, 
                            `Visits`, 
                            `Created`, 
                            `Modified` 
                                FROM userschedules
                                    WHERE AgencyId = @agencyId 
                                    AND UserId= @userid AND PatientId = @patientId";

            using (var cmd = new FluentCommand<UserSchedule>(script))
            {
                userSchedule = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new UserSchedule
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    UserId = reader.GetGuid("UserId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Visits = reader.GetStringNullable("Visits"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified")
                })
                .AsSingle();
            }
            return userSchedule;
        }



        public bool UpdateEvent(Guid agencyId, int clusterId, UserEvent userEvent)
        {
            bool result = false;

            if (userEvent != null)
            {
                var userEpisode = GetUserSchedule(agencyId, clusterId, userEvent.UserId, userEvent.PatientId);
                if (userEpisode != null && !string.IsNullOrEmpty(userEpisode.Visits))
                {
                    var events = userEpisode.Visits.ToObject<List<UserEvent>>();
                    events.ForEach(e =>
                    {
                        if (e.EventId == userEvent.EventId)
                        {
                            e.UserId = userEvent.UserId;
                            e.Discipline = userEvent.Discipline;
                            e.PatientId = userEvent.PatientId;
                            e.EventDate = userEvent.EventDate;
                            e.VisitDate = userEvent.VisitDate;
                            e.Status = userEvent.Status;
                            e.EpisodeId = userEvent.EpisodeId;
                            e.DisciplineTask = userEvent.DisciplineTask;
                            e.Discipline = userEvent.Discipline;
                            e.IsMissedVisit = userEvent.IsMissedVisit;
                            e.IsDeprecated = userEvent.IsDeprecated;

                            return;
                        }

                    });
                    userEpisode.Modified = DateTime.Now;
                    userEpisode.Visits = events.ToXml();
                    //database.Update<UserSchedule>(userEpisode);
                    var script = string.Format
                          (
                              "UPDATE agencymanagement.userschedules as us " +
                              "SET us.Visits = '{0}' " +
                              "AND us.Modified = '{1}' " +
                              "WHERE us.Id = '{2}' " +
                              "AND us.PatientId = '{3}' "
                              , userEpisode.Visits, userEpisode.Modified, userEpisode.Id, userEpisode.AgencyId
                           );
                    using (var cmd = new FluentCommand<UserSchedule>(script))
                    {
                        cmd.SetConnection("AgencyManagementConnectionString" + clusterId).AsNonQuery();
                    }
                    result = true;
                }
            }
            return result;
        }



        public User Get(Guid agencyId, int clusterId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(id, "id");
            User user = null;
            var script =  @"SELECT
                                `Id`, 
                                `FirstName`, 
                                `LastName`, 
                                `MiddleName`, 
                               FROM users
                                    WHERE AgencyId = @agencyid
                                        AND Id = @id";

                using (var cmd = new FluentCommand<User>(script))
                {
                    user = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", id)
                    .SetMap(reader => new User
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleName = reader.GetStringNullable("MiddleName"),

                    })
                    .AsSingle();
                }

                return user;
        }

        #endregion
    }
}
