﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Transactions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using AutoMapper;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class PatientRepository : IPatientRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region NewMethods
        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId, int clusterId)
        {
            FaceToFaceEncounter result = new FaceToFaceEncounter();
            var script = @"
                            SELECT
                                f2f.`Id`, 
                                f2f.`OrderNumber`, 
                                f2f.`AgencyId`, 
                                f2f.`PatientId`, 
                                f2f.`PhysicianId`, 
                                f2f.`UserId`, 
                                f2f.`EpisodeId`, 
                                f2f.`Status`, 
                                f2f.`Certification`, 
                                f2f.`EncounterDate`, 
                                f2f.`ReceivedDate`, 
                                f2f.`RequestDate`, 
                                f2f.`SentDate`, 
                                f2f.`ClinicalFinding`, 
                                f2f.`Services`, 
                                f2f.`ServicesOther`, 
                                f2f.`SignatureText`, 
                                f2f.`SignatureDate`, 
                                f2f.`IsDeprecated`, 
                                f2f.`Modified`, 
                                f2f.`Created`, 
                                f2f.`MedicalReason`, 
                                f2f.`PhysicianData`
                            FROM facetofaceencounters as f2f
                            WHERE f2f.AgencyId = @agencyid
                            AND f2f.PatientId = @patientid
                            AND f2f.Id = @facetofaceencounterid
                            ";
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("facetofaceencounterid", Id)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = reader.GetInt("OrderNumber"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    UserId = reader.GetGuid("UserId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Certification = reader.GetStringNullable("Certification"),
                    EncounterDate = reader.GetDateTime("EncounterDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    ClinicalFinding = reader.GetStringNullable("ClinicalFinding"),
                    Services = reader.GetStringNullable("Services"),
                    ServicesOther = reader.GetStringNullable("ServicesOther"),
                    SignatureText = reader.GetStringNullable("SignatureText"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Modified = reader.GetDateTime("Modified"),
                    Created = reader.GetDateTime("Created"),
                    MedicalReason = reader.GetStringNullable("MedicalReason"),
                    PhysicianData = reader.GetStringNullable("PhysicianData")
                })
                .AsSingle();
            }
            return result;
        }



        private PhysicianOrder GetOrderFull(Guid agencyId, int clusterId, Guid patientId, Guid Id)
        {
            PhysicianOrder result = new PhysicianOrder();
            var script = @"
                            SELECT
                                `Id`, 
                                `AgencyId`, 
                                `OrderNumber`, 
                                `PatientId`, 
                                `EpisodeId`, 
                                `PhysicianId`, 
                                `UserId`, 
                                `Summary`, 
                                `Text`, 
                                `OrderDate`, 
                                `Created`, 
                                `Modified`, 
                                `SignatureText`, 
                                `SignatureDate`, 
                                `SentDate`, 
                                `PhysicianSignatureText`, 
                                `ReceivedDate`, 
                                `PhysicianSignatureDate`, 
                                `Status`, 
                                `IsDeprecated`, 
                                `IsOrderForNextEpisode`, 
                                `IsOrderReadAndVerified`, 
                                `PhysicianData`
                            FROM physicianorders as po
                            WHERE po.AgencyId = @agencyid
                            AND po.PatientId = @patientid
                            AND po.Id = @physicianorderid
                            AND po.IsDeprecated = 0
                            ";
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("physicianorderid", Id)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    OrderNumber = reader.GetInt("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    UserId = reader.GetGuid("UserId"),
                    Summary = reader.GetStringNullable("Summary"),
                    Text = reader.GetStringNullable("Text"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    SignatureText = reader.GetStringNullable("SignatureText"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianSignatureText = reader.GetStringNullable("PhysicianSignatureText"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate"),
                    Status = reader.GetInt("Status"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsOrderReadAndVerified = reader.GetBoolean("IsOrderReadAndVerified"),
                    PhysicianData = reader.GetStringNullable("PhysicianData")
                })
                .AsSingle();
            }
            return result;
        }

        public PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId, int clusterId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return this.GetOrderFull(agencyId, clusterId, patientId, Id);
        }


        public PatientEpisode GetEpisode(Guid agencyId, int clusterId, Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                }
            }
            return patientEpisode;
        }
        public Patient Get(Guid patientId, Guid agencyId, int clusterId)
        {
            Patient result = new Patient();
            using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                result = database.Single<Patient>(e => e.AgencyId == agencyId && e.Id == patientId);
            }
            return result;
        }
        public bool AddCPO(CarePlanOversight cpo, int clusterId)
        {
            var result = false;
            if (cpo != null)
            {
                cpo.Id = Guid.NewGuid();
                cpo.Created = DateTime.Now;
                cpo.Modified = DateTime.Now;
                try
                {
                    using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                    {
                        database.Add<CarePlanOversight>(cpo);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteCPO(Guid agencyId, int clusterId, Guid id)
        {
            // var cpo = database.Single<CarePlanOversight>(c => c.Id == id);
            var script = @"UPDATE careplanoversights SET Modified=curdate(), IsDeprecated=1 WHERE AgencyId=@agencyid and Id=@id;";
            using (var cmd = new FluentCommand<int>(script))
            {
                try
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                                .AddGuid("id", id)
                                .AddGuid("agencyid", agencyId)
                                .AsNonQuery() > 0 ? true : false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public Patient GetPatientOnly(Guid Id, Guid agencyId, int clusterId)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return this.Get(Id, agencyId, clusterId);
        }
        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, int clusterId, Guid admissionId)
        {
            PatientAdmissionDate result = new PatientAdmissionDate();
            var script = @"
                            SELECT
                                `Id`, 
                                `AgencyId`, 
                                `PatientId`, 
                                `StartOfCareDate`, 
                                `DischargedDate`, 
                                `PatientData`,  
                                `Status`, 
                                `IsActive`, 
                                `IsDeprecated`, 
                                `DischargeReasonId`, 
                                `Created`, 
                                `Modified`, 
                                `Reason`
                            FROM patientadmissiondates as pad
                            WHERE pad.AgencyId = @agencyid
                            AND pad.Id = @patientadmissiondateid
                            ";
            using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientadmissiondateid", admissionId)
                .SetMap(reader => new PatientAdmissionDate
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                    DischargedDate = reader.GetDateTime("DischargedDate"),
                    PatientData = reader.GetStringNullable("PatientData"),
                    Status = reader.GetInt("Status"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    DischargeReasonId = reader.GetInt("DischargeReasonId"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    Reason = reader.GetStringNullable("Reason")
                })
                .AsSingle();
            }
            return result;
        }
        public List<Patient> GetPatientByAgencyPhysician(Guid agencyId, int clusterId, Guid loginId)
        {
            var patients = new List<Patient>();
            var patientIds = new List<Guid>();
            var script = @"SELECT patientphysicians.PatientId FROM patientphysicians INNER JOIN agencyphysicians ON agencyphysicians.Id=patientphysicians.PhysicianId WHERE agencyphysicians.AgencyId=@agencyid AND agencyphysicians.LoginId=@loginid AND agencyphysicians.IsDeprecated=0";
            using (var cmd = new FluentCommand<Guid>(script))
            {
                patientIds = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("loginid", loginId)
                    .SetMap(reader => new Guid(reader.GetString("PatientId")))
                    .AsList();
            }
            if (patientIds != null && patientIds.Count > 0)
            {
                var ids = patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var query = string.Format(@"SELECT Id, FirstName, LastName FROM patients WHERE Id in ({0}) AND IsDeprecated=0", ids);
                using (var cmd = new FluentCommand<Patient>(query))
                {
                    patients = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                        .SetMap(reader => new Patient
                        {
                            Id = reader.GetGuid("Id"),
                            FirstName = reader.GetStringNullable("FirstName"),
                            LastName = reader.GetStringNullable("LastName")
                        })
                        .AsList();
                }
            }
            return patients;
        }
        public PatientEpisode GetEpisodeById(Guid agencyId, int clusterId, Guid episodeId, Guid patientId)
        {
            using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
            }
        }
        public PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, int clusterId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.Id as Id , patientepisodes.PatientId as PatientId ,  patientepisodes.Details as Details , patientepisodes.StartDate as StartDate , patientepisodes.EndDate as EndDate , patientepisodes.Schedule as Schedule , " +
                " patientepisodes.IsActive as IsActive , patientepisodes.IsRecertCompleted as IsRecertCompleted , patientepisodes.IsDischarged as IsDischarged , patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge ,patientepisodes.AssessmentId as AssessmentId , patientepisodes.AssessmentType as AssessmentType , patientepisodes.Created as Created , patientepisodes.Modified as Modified , patientepisodes.AdmissionId as AdmissionId , patientadmissiondates.StartOfCareDate as StartOfCareDate " +
                "FROM patientepisodes INNER JOIN patientadmissiondates ON patientepisodes.AdmissionId = patientadmissiondates.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patientadmissiondates.AgencyId = @agencyid AND patientepisodes.Id = @episodeid AND patientepisodes.PatientId = @patientid LIMIT 1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    Details = reader.GetStringNullable("Details"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsRecertCompleted = reader.GetBoolean("IsRecertCompleted"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                    AssessmentId = reader.GetGuid("AssessmentId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }
        #endregion

        #region SecondRound
        public bool UpdateCPO(CarePlanOversight cpo, int clusterId)
        {
            var result = false;
            if (cpo != null)
            {
                cpo.Modified = DateTime.Now;
                try
                {
                    using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                    {
                        database.Update<CarePlanOversight>(cpo);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }
        public string GetPatientNameById(Guid patientId, Guid agencyId, int clusterId)
        {
            string name = string.Empty;
            var script = @"SELECT FirstName, LastName, MiddleInitial FROM patients WHERE Id=@patientid AND AgencyId=@agencyid limit 1";
            using (var cmd = new FluentCommand<Patient>(script))
            {
                var patient = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new Patient
                    {
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial")
                    }).AsSingle();
                if (patient != null)
                {
                    name = string.Concat(patient.LastName.ToUpperCase(), ", ", patient.FirstName.ToUpperCase(), (patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial.ToUpperCase() + "." : string.Empty));
                }
            }
            return name;
        }

        public string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId, int clusterId)
        {
            var episode = this.GetEpisode(agencyId, clusterId, episodeId, patientId);
            if (episode != null)
            {
                var scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduleEvent != null)
                {
                    return scheduleEvent.ReturnReason.IsNotNullOrEmpty() ? scheduleEvent.ReturnReason : string.Empty;
                }
            }
            return  string.Empty;
        }
        //public bool UpdateOrder(int clusterId, PhysicianOrder order)
        //{
        //    Check.Argument.IsNotNull(order, "order");
        //    Check.Argument.IsNotEmpty(order.Id, "order.Id");
        //    bool result = false;
        //    try
        //    {
        //        var editOrder = database.Single<PhysicianOrder>(o => o.Id == order.Id && o.PatientId == order.PatientId);
        //        if (order != null && editOrder != null)
        //        {
        //            editOrder.Modified = DateTime.Now;
        //            editOrder.OrderDate = order.OrderDate;
        //            editOrder.Summary = order.Summary;
        //            editOrder.Text = order.Text;
        //            editOrder.PhysicianId = order.PhysicianId;
        //            editOrder.Status = order.Status;
        //            editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
        //            editOrder.IsOrderReadAndVerified = order.IsOrderReadAndVerified;
        //            editOrder.SignatureDate = order.SignatureDate;
        //            editOrder.SignatureText = order.SignatureText;
        //            editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
        //            if (order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
        //            {
        //                editOrder.PhysicianSignatureText = order.PhysicianSignatureText;
        //                editOrder.PhysicianSignatureDate = order.PhysicianSignatureDate;
        //                editOrder.ReceivedDate = order.ReceivedDate;
        //            }
        //            editOrder.PhysicianData = order.PhysicianData;
        //            using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
        //            {
        //                database.Update<PhysicianOrder>(editOrder);
        //                result = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    return result;
        //}

        public bool UpdateOrderModel(int clusterId, PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                    using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                    {
                        database.Update<PhysicianOrder>(order);
                        result = true;
                    }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId)
        {
            using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
            }
        }
        public bool UpdateVisitNote(int clusterId, PatientVisitNote patientVisitNote)
        {
            var result = false;
            try
            {
                if (patientVisitNote != null)
                {
                    if (patientVisitNote.Note.IsNotNullOrEmpty())
                    {
                        patientVisitNote.Modified = DateTime.Now;
                        using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                        {
                            database.Update<PatientVisitNote>(patientVisitNote);
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }
        public bool UpdateFaceToFaceEncounter(int clusterId, FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                if (faceToFaceEncounter != null)
                {
                    try
                    {
                        faceToFaceEncounter.Modified = DateTime.Now;
                        using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                        {
                            database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }
        public AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId, int clusterId)
        {
            var profile = new AllergyProfile();
            var script = @"SELECT Id, PatientId,  Allergies FROM allergyprofiles WHERE AgencyId=@agencyid AND PatientId=@patientid";
            using (var cmd = new FluentCommand<AllergyProfile>(script))
            {
                profile = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new AllergyProfile
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                        AgencyId = agencyId,
                        Allergies = reader.GetStringNullable("Allergies")
                    }).AsSingle();
            }
            if (profile == null)
            {
                profile = new AllergyProfile
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    PatientId = patientId,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Allergies = new List<Allergy>().ToXml()
                };
                using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    database.Add<AllergyProfile>(profile);
                }
            }

            return profile;
        }

        public List<PhysicianOrder> GetPhysicianOrdersByPhysicianAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate)
        {
            var list = new List<PhysicianOrder>();
            var query = string.Format(@"select
                                        po.Id,
                                        po.Status,
                                        po.AgencyId, 
                                        po.OrderNumber,
                                        po.PatientId,
                                        po.OrderDate,
                                        po.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from  patients pa
                                                 INNER JOIN physicianorders  po ON pa.Id = po.PatientId
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    po.AgencyId = @agencyid AND
                                                    po.PhysicianId = @physicianid AND
                                                    po.Status = {0} AND 
                                                    DATE(po.OrderDate) between DATE(@startdate) and DATE(@enddate) AND
                                                    po.IsDeprecated = 0 ", status);

            using (var cmd = new FluentCommand<PhysicianOrder>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new PhysicianOrder
                    {
                        Id = reader.GetGuid("Id"),
                        Status = reader.GetInt("Status"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        OrderNumber = reader.GetInt("OrderNumber"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        OrderDate = reader.GetDateTime("OrderDate"),
                        PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
                    })
                    .AsList();
            }


            return list;
        }
        public List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysicianAndDate(Guid agencyId, int clusterId, Guid physicianId, int[] status, DateTime startDate, DateTime endDate)
        {
            var list = new List<FaceToFaceEncounter>();
            var allStatus = status.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

            var query = string.Format(@"select
                                        ftf.Id,
                                        ftf.Status,
                                        ftf.AgencyId, 
                                        ftf.OrderNumber,
                                        ftf.PatientId,
                                        ftf.RequestDate,
                                        ftf.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from patients pa 
                                                 INNER JOIN facetofaceencounters ftf ON ftf.PatientId= pa.Id
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    ftf.AgencyId = @agencyid AND
                                                    ftf.PhysicianId = @physicianid AND
                                                    ftf.Status in ({0}) AND 
                                                    DATE(ftf.RequestDate) BETWEEN DATE(@startDate) AND DATE(@endDate) AND
                                                    ftf.IsDeprecated = 0 ", allStatus);


            using (var cmd = new FluentCommand<FaceToFaceEncounter>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startDate", startDate)
                    .AddDateTime("endDate", endDate)
                    .SetMap(reader => new FaceToFaceEncounter
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        OrderNumber = reader.GetInt("OrderNumber"),
                        Status = reader.GetInt("Status"),
                        RequestDate = reader.GetDateTime("RequestDate"),
                        PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
                    })
                    .AsList();
            }

            return list;
        }
        public List<OrderLean> GetEvalOrdersByPhysician(Guid agencyId, int clusterId, Guid physicianId, DateTime startDate, DateTime endDate, int[] status)
        {
            var list = new List<OrderLean>();
            var allStatus = status.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

            var query = string.Format(@"select
                                        pn.Id,
                                        pn.NoteType,
                                        pn.OrderNumber,
                                        pn.Status, 
                                        pn.AgencyId, 
                                        pn.PatientId,
                                        pn.EpisodeId, 
                                        pn.SentDate ,
                                        pa.LastName,
                                        pa.FirstName,
                                        pe.Schedule 
                                            from patients pa  
                                                 INNER JOIN patientepisodes pe ON pe.PatientId= pa.Id
                                                 INNER JOIN  patientvisitnotes pn ON pe.Id = pn.EpisodeId 
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    pe.AgencyId = @agencyid AND
                                                    pn.AgencyId = @agencyid AND
                                                    DATE(pe.StartDate) <=DATE(@enddate) AND DATE(pe.EndDate)>= DATE(@startdate) AND
                                                    pn.PhysicianId = @physicianid AND
                                                    pn.Status in ({0}) AND 
                                                    pn.IsDeprecated = 0 ", allStatus);

            using (var cmd = new FluentCommand<OrderLean>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                         .SetMap(reader => new OrderLean
                         {
                             Id = reader.GetGuid("Id"),
                             NoteType = reader.GetStringNullable("NoteType"),
                             OrderNumber = reader.GetInt("OrderNumber"),
                             Status = reader.GetInt("Status"),
                             AgencyId = reader.GetGuid("AgencyId"),
                             PatientId = reader.GetGuid("PatientId"),
                             EpisodeId = reader.GetGuid("EpisodeId"),
                             SentDate = reader.GetDateTime("SentDate"),
                             PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName"),
                             Schedule = reader.GetStringNullable("Schedule")
                         })
                    .AsList();
            }

            return list;
        }
        public bool AddReturnComment(ReturnComment returnComment, int clusterId)
        {
            bool result = false;

            if (returnComment != null)
            {
                returnComment.Created = DateTime.Now;
                returnComment.Modified = DateTime.Now;
                using (var scope = new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    database.Add<ReturnComment>(returnComment);
                    result = true;
                }
            }

            return result;
        }
        public List<ReturnComment> GetReturnComments(Guid agencyId, int clusterId, Guid episodeId, Guid eventId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.EpisodeId == episodeId && a.EventId == eventId && a.IsDeprecated == false).ToList();
            }
        }
        //public bool UpdateEpisode(Guid agencyId, int clusterId, ScheduleEvent scheduleEvent)
        //{
        //    bool result = false;
        //    try
        //    {
        //        if (scheduleEvent != null)
        //        {
        //            var patientEpisode = this.GetEpisode(agencyId, clusterId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
        //            if (patientEpisode != null && !string.IsNullOrEmpty(patientEpisode.Schedule))
        //            {
        //                var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
        //                SetSchedule(scheduleEvent, events);
        //                patientEpisode.Modified = DateTime.Now;
        //                patientEpisode.Schedule = events.ToXml();
        //                var script = @"UPDATE patientepisodes SET Schedule=@schedule AND Modified=curdate() WHERE Id=@id AND AgencyId=@agencyid";
        //                using (var cmd = new FluentCommand<int>(script))
        //                {
        //                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
        //                        .AddString("schedule", events.ToXml())
        //                        .AddGuid("id", scheduleEvent.EpisodeId)
        //                        .AddGuid("agencyid", scheduleEvent.AgencyId)
        //                        .AsNonQuery() > 0 ? true : false;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    return result;
        //}

        public bool UpdateEpisode(PatientEpisode episode, int clusterId)
        {

            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Update<PatientEpisode>(episode) > 0;
            }
            //try
            //{
            //    var script = @"UPDATE patientepisodes SET Schedule=@schedule AND Modified=curdate() WHERE  AgencyId=@agencyid AND Id=@id AND PatientId = @patientid ";
            //    using (var cmd = new FluentCommand<int>(script))
            //    {
            //        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
            //            .AddString("schedule", scheduleXml)
            //            .AddGuid("id", episodeId)
            //            .AddGuid("agencyid", agencyId)
            //            .AddGuid("patientid", patientId)
            //            .AsNonQuery() > 0 ? true : false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}
        }


      
        //public ScheduleEvent GetSchedule(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    ScheduleEvent scheduleEvent = null;
        //    if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
        //    {
        //        var episode = this.GetEpisode(agencyId, clusterId, episodeId, patientId);
        //        if (episode != null)
        //        {
        //            scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
        //            if (scheduleEvent != null)
        //            {

        //                scheduleEvent.StartDate = episode.StartDate;
        //                scheduleEvent.EndDate = episode.EndDate;
        //                scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
        //            }
        //        }
        //    }
        //    return scheduleEvent;
        //}

        #endregion
    }
}
