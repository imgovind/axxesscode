﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAgencyRepository
    {
        Agency Get(Guid id, int clusterId);
        AgencyLocation GetMainLocation(Guid agencyId, int clusterId);
        AgencyLocation FindLocation(Guid agencyId, int clusterId, Guid Id);
        LocationPrintProfile AgencyNameWithLocationAddress(Guid agencyId, Guid locationId, int clusterId);
        LocationPrintProfile AgencyNameWithMainLocationAddress(Guid agencyId, int clusterId);
    }
}
