﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Api;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Repositories;

    public static class PhysicianEngine
    {
        #region Private Members

        private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        public static void Refresh(Guid agencyId)
        {
            //cacheAgent.RefreshPhysicians(agencyId);
        }

        public static IList<AgencyPhysician> AsList(Guid agencyId)
        {
            IList<AgencyPhysician> physicianList = new List<AgencyPhysician>();

            //var physicianInfoList = cacheAgent.GetPhysicians(agencyId);
            //if (physicianInfoList == null || physicianInfoList.Count == 0)
            //{
            //physicianList = dataProvider.PhysicianRepository.GetAgencyPhysicians(agencyId);
            //}
            //else
            //{
            //    physicianInfoList.ForEach(p =>
            //    {
            //        if (p.IsNotNullOrEmpty())
            //        {
            //            physicianList.Add(p.ToObject<AgencyPhysician>());
            //        }
            //    });
            //}

            return physicianList;
        }

        #endregion
    }
}
