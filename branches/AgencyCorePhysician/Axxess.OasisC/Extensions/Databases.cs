﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using SubSonic.DataProviders;
    

    public static class Databases
    {
        public static Assessment FindAny(this SimpleRepository repository, string assessmentType, Guid assessmentId, Guid agencyId, int clusterId)
        {
            Assessment assessment = null;
            var type = new AssessmentType();
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<StartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.StartOfCare;
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<ResumptionofCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.ResumptionOfCare;
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<FollowUpAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.FollowUp;
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationST":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<RecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.Recertification;
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<TransferNotDischargedAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.TransferInPatientNotDischarged;
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                case "OASISCTransferDischargePT":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<TransferDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.TransferInPatientDischarged;
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<DeathAtHomeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.DischargeFromAgencyDeath;
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "OASISCDischargeST":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<DischargeFromAgencyAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.DischargeFromAgency;
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<NonOasisDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.NonOasisDischarge;
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<NonOasisStartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.NonOasisStartOfCare;
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                    {
                        assessment = repository.Single<NonOasisRecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    }
                    type = AssessmentType.NonOasisRecertification;
                    break;
                default:
                    break;
            }
            if (assessment != null)
            {
                assessment.Type = type;
            }
            return assessment;
        }







    }
}
