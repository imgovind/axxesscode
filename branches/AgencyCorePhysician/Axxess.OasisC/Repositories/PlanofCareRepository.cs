﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class PlanofCareRepository : IPlanofCareRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PlanofCareRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IPlanofCareRepository Member

        
        public bool Update(int clusterId, PlanofCare planofCare)
        {
            bool result = false;

            if (planofCare != null)
            {
                planofCare.Modified = DateTime.Now;
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                {
                    database.Update<PlanofCare>(planofCare);
                }
                result = true;
            }
            return result;
        }

        public bool UpdateStandAlone(int clusterId, PlanofCareStandAlone planofCare)
        {
            bool result = false;

            if (planofCare != null)
            {
                planofCare.Modified = DateTime.Now;
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
                {
                    database.Update<PlanofCareStandAlone>(planofCare);
                }
                result = true;
            }
            return result;
        }

        public PlanofCare Get(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
            {
                return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
            }
        }

        public PlanofCareStandAlone GetStandAlone(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("OasisCConnectionString" + clusterId)))
            {
                return database.Single<PlanofCareStandAlone>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
            }
        }
 
        public List<PlanofCare> GetByPhysicianIdAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate)
        {
            var query = string.Format(@"select
                                        pc.Id,
                                        pc.Status,
                                        pc.AgencyId, 
                                        pc.OrderNumber,
                                        pc.PatientId,
                                        pc.Created,
                                        pc.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from agencymanagement.patients pa 
                                                 INNER JOIN oasisc.planofcares pc ON pc.PatientId = pa.Id
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    pc.AgencyId = @agencyid AND
                                                    pc.PhysicianId = @physicianid AND
                                                    pc.Status = {0} AND 
                                                    DATE(pc.Created) BETWEEN DATE(@startDate) AND DATE(@endDate) AND
                                                    pc.IsDeprecated = 0 ", status);

                

            return new FluentCommand<PlanofCare>(query)
                .SetConnection("OasisCConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("physicianid", physicianId)
                .AddDateTime("startDate",startDate)
                .AddDateTime("endDate",endDate)
                .SetMap(reader => new PlanofCare
                 {
                     Id = reader.GetGuid("Id"),
                     OrderNumber = reader.GetInt("OrderNumber"),
                     Status = reader.GetInt("Status"),
                     AgencyId = reader.GetGuid("AgencyId"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     Created = reader.GetDateTime("Created"),
                     PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
                 })
                .AsList();

        }

        public List<PlanofCareStandAlone> GetStandAloneByPhysicianIdAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate)
        {

            var query = string.Format(@"select
                                        pc.Id,
                                        pc.Status,
                                        pc.AgencyId, 
                                        pc.OrderNumber,
                                        pc.PatientId,
                                        pc.Created,
                                        pc.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from agencymanagement.patients pa 
                                                 INNER JOIN oasisc.planofcarestandalones pc ON pc.PatientId= pa.Id
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    pc.AgencyId = @agencyid AND
                                                    pc.PhysicianId = @physicianid AND
                                                    pc.Status = {0} AND 
                                                    DATE(pc.Created) BETWEEN DATE(@startDate) AND DATE(@endDate) AND
                                                    pc.IsDeprecated = 0 ", status);

            return new FluentCommand<PlanofCareStandAlone>(query)
                .SetConnection("OasisCConnectionString" + clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("physicianid", physicianId)
                .AddDateTime("startDate",startDate)
                .AddDateTime("endDate",endDate)
                .SetMap(reader => new PlanofCareStandAlone
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = reader.GetInt("OrderNumber"),
                    Status = reader.GetInt("Status"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Created = reader.GetDateTime("Created"),
                    PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
                })
                .AsList();

        }


        #endregion

    }
}
