﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IPlanofCareRepository
    {
        
        bool Update(int clusterId, PlanofCare planofCare);
        PlanofCare Get(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId);

        PlanofCareStandAlone GetStandAlone(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid eventId);
        bool UpdateStandAlone(int clusterId, PlanofCareStandAlone planOfCare);

        List<PlanofCare> GetByPhysicianIdAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate);
        List<PlanofCareStandAlone> GetStandAloneByPhysicianIdAndDate(Guid agencyId, int clusterId, Guid physicianId, int status, DateTime startDate, DateTime endDate);

    }
}
