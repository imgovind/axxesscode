﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class OasisCDataProvider : IOasisCDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public OasisCDataProvider()
        {
            this.database = new SimpleRepository();
        }

        #endregion

        #region IOasisCDataProvider Members

        private IAssessmentRepository assessmentRepository;
        public IAssessmentRepository AssessmentRepository
        {
            get
            {
                if (assessmentRepository == null)
                {
                    assessmentRepository = new AssessmentRepository(this.database);
                }
                return assessmentRepository;
            }
        }

        private IPlanofCareRepository planofCareRepository;
        public IPlanofCareRepository PlanofCareRepository
        {
            get
            {
                if (planofCareRepository == null)
                {
                    planofCareRepository = new PlanofCareRepository(this.database);
                }
                return planofCareRepository;
            }
        }

        private ICachedDataRepository cachedDataRepository;
        public ICachedDataRepository CachedDataRepository
        {
            get
            {
                if (cachedDataRepository == null)
                {
                    cachedDataRepository = new CachedDataRepository(this.database);
                }
                return cachedDataRepository;
            }
        }
        #endregion
    }
}

