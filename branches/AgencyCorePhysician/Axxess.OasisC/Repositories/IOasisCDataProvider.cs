﻿namespace Axxess.OasisC.Repositories
{
    using System;

    public interface IOasisCDataProvider
    {
        IPlanofCareRepository PlanofCareRepository
        {
            get;
        }

        IAssessmentRepository AssessmentRepository
        {
            get;
        }

        ICachedDataRepository CachedDataRepository
        {
            get;
        }
    }
}
