﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;
    using SubSonic.DataProviders;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

       
        public Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId, int clusterId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentType, assessmentId, agencyId, clusterId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
                
            }
            return assessment;
        }
      

        #endregion
    }
}
