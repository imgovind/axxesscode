﻿namespace Axxess.OasisC.Enums
{
    using System.ComponentModel;

    public enum AssessmentType
    {
        None = 0,
        [Description("OASIS-C Start Of Care")]
        StartOfCare = 1,
        [Description("OASIS-C Resumption Of Care")]
        ResumptionOfCare = 3,
        [Description("OASIS-C Recertification")]
        Recertification = 4,
        [Description("OASIS-C Other Follow-up")]
        FollowUp = 5,
        [Description("OASIS-C Transfer to inpatient facility - Not Discharged")]
        TransferInPatientNotDischarged = 6,
        [Description("OASIS-C Transfer to inpatient facility - Discharged")]
        TransferInPatientDischarged = 7,
        [Description("OASIS-C Death at Home")]
        DischargeFromAgencyDeath = 8,
        [Description("OASIS-C Discharge From Agency")]
        DischargeFromAgency = 9,
        [Description("OASIS C 485 (Plan of Care)")]
        PlanOfCare485 = 10,
        [Description("Non-OASIS Start Of Care")]
        NonOasisStartOfCare = 11,
        [Description("Non-OASIS Recertification")]
        NonOasisRecertification = 14,
        [Description("Non-OASIS Discharge")]
        NonOasisDischarge = 19
    }
}
