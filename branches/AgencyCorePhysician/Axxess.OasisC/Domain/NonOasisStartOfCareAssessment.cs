﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class NonOasisStartOfCareAssessment : Assessment
    {
        public NonOasisStartOfCareAssessment()
        {
            this.Type = AssessmentType.NonOasisStartOfCare;
        }
    }
}
