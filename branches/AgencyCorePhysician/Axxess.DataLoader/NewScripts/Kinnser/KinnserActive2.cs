﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using Excel;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;
    using Axxess.DataLoader.Columns;
    public static class KinnserActive2
    {
        private static KinnserActiveColumn columnvalue = new KinnserActiveColumn();
        private static string inputFile = Path.Combine(App.Root, string.Format("Files\\columbus-active.xls"));
        private static string eventLog = Path.Combine(App.Root, string.Format("Files\\columbus-active-EventLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string CommentsDelimiter = "; ";

        public static void Run(Guid agencyId, Guid locationId)
        {
            columnvalue.PatientStatus = 1;
            columnvalue.ServiceLocation = "Q5001";
            columnvalue.DefaultPayor = "1";
            columnvalue.MDSplitChar = ',';
            columnvalue.MDSUBFirst = 0;
            columnvalue.MDSUBLast = 2;
            columnvalue.MDSUBMiddle = 1;
            columnvalue.agencyId = agencyId;
            columnvalue.locationId = locationId;
            #region columns
            columnvalue.FirstName = 1;
            columnvalue.LastName = 0;
            columnvalue.MRN = 13;
            columnvalue.Gender = 18;
            columnvalue.DOB = 14;
            columnvalue.SOC = 12;
            columnvalue.Insurance = 16;
            columnvalue.HIC = 15;
            columnvalue.PrimaryDiagnosis = 7;
            columnvalue.Address1 = 2;
            columnvalue.City = 3;
            columnvalue.State = 4;
            columnvalue.Zip = 5;
            columnvalue.Phone = 6;

            //columnvalue.MedicaidNumber = 5;
            columnvalue.EmergencyContactName = 9;
            columnvalue.EmergencyContactRelationship = 22;
            columnvalue.EmergencyContactPhone = 10;
            //columnvalue.EmergencyContactAddress = 15;
            //columnvalue.Pharmacy = 16;
            //columnvalue.PharmacyPhone = 17;
            //columnvalue.EpisodeSD = 19;
            //columnvalue.EpisodeED = 20;
            //columnvalue.SecondaryDiagnosis = 8;
            //columnvalue.Disciplines = 23;
            //columnvalue.DisciplineFrequency = 24;
            //columnvalue.Triage = 25;
            columnvalue.PhysicianName = 21;
            //columnvalue.PhysicianNPI = 27;
            //columnvalue.PhysicianPhone = 28;
            //columnvalue.PhysicianFax = 29;
            //columnvalue.PrimaryClinician = 30;
            //columnvalue.Therapist = 36;
            //columnvalue.ExternalReferral = 37;
            columnvalue.CaseManager = 19;
            #endregion
            #region FirstColumn
            foreach (var item in typeof(KinnserActiveColumn).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }
            #endregion
            Process();
        }

        private static void Process()
        {
            using (TextWriter textWriter = new StreamWriter(eventLog, true))
            {
                try
                {
                    var totalpatients = default(int);
                    var totalCounter = default(int);
                    using (FileStream fileStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        totalpatients = dataTable.Rows.Count;
                                        totalCounter++;
                                        if (!dataRow.IsEmpty())
                                        {
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = columnvalue.agencyId;
                                            patientData.AgencyLocationId = columnvalue.locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = columnvalue.PatientStatus;
                                            patientData.ServiceLocation = columnvalue.ServiceLocation;

                                            #region Name
                                            if (columnvalue.FirstName != default(int) || (columnvalue.FirstColumn.Equals("FirstName") && columnvalue.FirstName != null))
                                            {
                                                patientData.FirstName = dataRow.GetValue(columnvalue.FirstName);
                                            }
                                            if (columnvalue.LastName != default(int) || (columnvalue.FirstColumn.Equals("LastName") && columnvalue.LastName != null))
                                            {
                                                patientData.LastName = dataRow.GetValue(columnvalue.LastName);
                                            }
                                            #endregion
                                            #region Gender
                                            if (columnvalue.Gender != default(int) || (columnvalue.FirstColumn.Equals("Gender") && columnvalue.Gender != null))
                                            {
                                                var genderStaging = dataRow.GetValue(columnvalue.Gender);
                                                if (genderStaging.IsNotNullOrEmpty() && genderStaging.Length >= 1)
                                                {
                                                    patientData.Gender = genderStaging.ToUpper().StartsWith("F") ? "Female" : genderStaging.ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                patientData.Gender = "";
                                            }
                                            #endregion
                                            #region MRN
                                            if (columnvalue.MRN != null || (columnvalue.FirstColumn.Equals("MRN") && columnvalue.MRN != null))
                                            {
                                                patientData.PatientIdNumber = dataRow.GetValue(columnvalue.MRN);
                                            }
                                            #endregion
                                            #region Medicare
                                            bool primaryInsurancePayor = default(bool);
                                            bool IsMedicaidpatient = default(bool);
                                            var insuranceStagingNomer = dataRow.GetValue(columnvalue.Insurance);
                                            if (insuranceStagingNomer.ToUpper().Contains("MEDICARE") || insuranceStagingNomer.ToUpper().Contains("PALMETTO"))
                                            {
                                                primaryInsurancePayor = true;
                                            }
                                            else if (insuranceStagingNomer.ToUpper().Contains("MEDICAID"))
                                            {
                                                IsMedicaidpatient = true;
                                            }

                                            if (columnvalue.HIC != default(int) || (columnvalue.FirstColumn.Equals("HIC") && columnvalue.HIC != null))
                                            {
                                                if (IsMedicaidpatient == true)
                                                { 
                                                    var medicaidNumberStaging = dataRow.GetValue(columnvalue.HIC);
                                                    if(medicaidNumberStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MedicaidNumber = medicaidNumberStaging;
                                                        primaryInsurancePayor = true;
                                                    }
                                                }

                                                var medicareNumberStaging = dataRow.GetValue(columnvalue.HIC);
                                                if (medicareNumberStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.MedicareNumber = medicareNumberStaging;
                                                    primaryInsurancePayor = true;
                                                }
                                            }
                                            #endregion
                                            #region SOC
                                            if (columnvalue.SOC != default(int) || (columnvalue.FirstColumn.Equals("SOC") && columnvalue.SOC != null))
                                            {
                                                var SOCStaging = dataRow.GetValue(columnvalue.SOC);
                                                if (SOCStaging.IsNotNullOrEmpty())
                                                {
                                                    if (SOCStaging.IsDouble())
                                                    {
                                                        patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.StartofCareDate = DateTime.Parse(SOCStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.StartofCareDate = DateTime.MinValue;
                                                }
                                            }
                                            #endregion
                                            #region EpisodeSD
                                            if (columnvalue.EpisodeSD != default(int) || (columnvalue.FirstColumn.Equals("EpisodeSD") && columnvalue.EpisodeSD != null))
                                            {
                                                var episodeStartDateStaging = dataRow.GetValue(columnvalue.EpisodeSD);
                                                if (episodeStartDateStaging.IsNotNullOrEmpty())
                                                {
                                                    if (episodeStartDateStaging.IsDouble())
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeStartDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.Parse(episodeStartDateStaging).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region Address
                                            #region Address1
                                            if (columnvalue.Address1 != default(int) || (columnvalue.FirstColumn.Equals("Address1") && columnvalue.Address1 != null))
                                            {
                                                var addressLineStaging = dataRow.GetValue(columnvalue.Address1);
                                                if (addressLineStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressLine1 = addressLineStaging;
                                                }
                                            }
                                            else
                                            {
                                                patientData.AddressLine1 = "";
                                            }
                                            #endregion
                                            #region City
                                            if (columnvalue.City != default(int) || (columnvalue.FirstColumn.Equals("City") && columnvalue.City != null))
                                            {
                                                var addressCityStaging = dataRow.GetValue(columnvalue.City);
                                                if (addressCityStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressCity = addressCityStaging;
                                                }
                                            }
                                            #endregion
                                            #region State
                                            if (columnvalue.State != default(int) || (columnvalue.FirstColumn.Equals("State") && columnvalue.State != null))
                                            {
                                                var addressStateCodeStaging = dataRow.GetValue(columnvalue.State).ToUpper().Replace(".", "");
                                                if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressStateCode = addressStateCodeStaging;
                                                }
                                            }
                                            #endregion
                                            #region Zip
                                            if (columnvalue.Zip != default(int) || (columnvalue.FirstColumn.Equals("Zip") && columnvalue.Zip != null))
                                            {
                                                var addressZipCodeStaging = dataRow.GetValue(columnvalue.Zip);
                                                if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressZipCode = addressZipCodeStaging;
                                                }
                                            }
                                            #endregion
                                            #endregion
                                            #region Phone
                                            if (columnvalue.Phone != default(int) || (columnvalue.FirstColumn.Equals("Phone") && columnvalue.Phone != null))
                                            {
                                                var phoneHomeStaging = dataRow.GetValue(columnvalue.Phone);
                                                patientData.PhoneHome = phoneHomeStaging.IsNotNullOrEmpty() ? phoneHomeStaging.ToPhoneDB() : string.Empty;
                                            }
                                            #endregion
                                            #region Medicaid
                                            if (columnvalue.MedicaidNumber != default(int) || (columnvalue.FirstColumn.Equals("MedicaidNumber") && columnvalue.MedicaidNumber != null))
                                            {
                                                var medicaidNumberStaging = dataRow.GetValue(columnvalue.MedicaidNumber);
                                                if (medicaidNumberStaging.IsNotNullOrEmpty())
                                                {

                                                    patientData.MedicaidNumber = medicaidNumberStaging;
                                                    primaryInsurancePayor = true;
                                                }
                                            }
                                            #endregion
                                            #region DOB
                                            if (columnvalue.DOB != default(int) || (columnvalue.FirstColumn.Equals("dob_col") && columnvalue.DOB != null))
                                            {
                                                var DOBStaging = dataRow.GetValue(columnvalue.DOB);
                                                if (DOBStaging.IsNotNullOrEmpty())
                                                {
                                                    if (DOBStaging.IsDouble())
                                                    {
                                                        patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.Parse(DOBStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }

                                                //patientData.DOB = DateTime.MinValue;
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }
                                            #endregion
                                            #region EmergencyContact
                                            #region ECName
                                            if (columnvalue.EmergencyContactName != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactName") && columnvalue.EmergencyContactName != null))
                                            {
                                                var emergencyContactNameStaging = dataRow.GetValue(columnvalue.EmergencyContactName);
                                                if (emergencyContactNameStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "[Emergency Contact Name: " + emergencyContactNameStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region ECRelationship
                                            if (columnvalue.EmergencyContactRelationship != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactRelationship") && columnvalue.EmergencyContactRelationship != null))
                                            {
                                                var emergencyContactRelationshipStaging = dataRow.GetValue(columnvalue.EmergencyContactRelationship);
                                                if (emergencyContactRelationshipStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Relationship: " + emergencyContactRelationshipStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region ECPhone
                                            if (columnvalue.EmergencyContactPhone != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactPhone") && columnvalue.EmergencyContactPhone != null))
                                            {
                                                var emergencyContactPhoneStaging = dataRow.GetValue(columnvalue.EmergencyContactPhone);
                                                if (emergencyContactPhoneStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Phone: " + emergencyContactPhoneStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region ECAddress
                                            if (columnvalue.EmergencyContactAddress != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactAddress") && columnvalue.EmergencyContactAddress != null))
                                            {
                                                var emergencyContactAddressStaging = dataRow.GetValue(columnvalue.EmergencyContactAddress);
                                                if (emergencyContactAddressStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Address: " + emergencyContactAddressStaging + "]" + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #endregion
                                            #region Pharmacy
                                            #region PharmacyName
                                            if (columnvalue.Pharmacy != default(int) || (columnvalue.FirstColumn.Equals("PharmacyName") && columnvalue.Pharmacy != null))
                                            {
                                                var pharmacyStaging = dataRow.GetValue(columnvalue.Pharmacy);
                                                if (pharmacyStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.PharmacyName = pharmacyStaging;
                                                }
                                            }
                                            #endregion
                                            #region PharmacyPhone
                                            if (columnvalue.PharmacyPhone != default(int) || (columnvalue.FirstColumn.Equals("PharmacyPhone") && columnvalue.PharmacyPhone != null))
                                            {
                                                var pharmacyPhoneStaging = dataRow.GetValue(columnvalue.PharmacyPhone);
                                                if (pharmacyPhoneStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.PharmacyPhone = pharmacyPhoneStaging.ToPhoneDB();
                                                }
                                            }
                                            #endregion
                                            #endregion
                                            #region EpisodeED
                                            if (columnvalue.EpisodeED != default(int) || (columnvalue.FirstColumn.Equals("EpisodeED") && columnvalue.EpisodeED != null))
                                            {
                                                var episodeEndDateStaging = dataRow.GetValue(columnvalue.EpisodeED);
                                                if (episodeEndDateStaging.IsNotNullOrEmpty())
                                                {
                                                    if (episodeEndDateStaging.IsDouble())
                                                    {
                                                        patientData.Comments += string.Format("Episode End Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeEndDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Episode End Date:{0} {1}", DateTime.Parse(episodeEndDateStaging).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region PrimaryInsurance
                                            if (primaryInsurancePayor == true)
                                            {
                                                patientData.PrimaryInsurance = columnvalue.DefaultPayor;
                                            }
                                            else if (primaryInsurancePayor == false)
                                            {
                                                patientData.PrimaryInsurance = "";
                                            }
                                            #endregion
                                            #region PrimaryDiagnosis
                                            if (columnvalue.PrimaryDiagnosis != default(int))
                                            {
                                                var primaryDiagStaging = dataRow.GetValue(columnvalue.PrimaryDiagnosis);
                                                if (primaryDiagStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + primaryDiagStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region SecondaryDiagnosis
                                            if (columnvalue.SecondaryDiagnosis != default(int))
                                            {
                                                var secondaryDiagStaging = dataRow.GetValue(columnvalue.SecondaryDiagnosis);
                                                if (secondaryDiagStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Secondary Diagnosis: " + secondaryDiagStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region Disciplines
                                            if (columnvalue.Disciplines != default(int) || (columnvalue.FirstColumn.Equals("Disciplines") && columnvalue.Disciplines != null))
                                            {
                                                var DisciplinesStaging = dataRow.GetValue(columnvalue.Disciplines);
                                                if (DisciplinesStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Disciplines: " + DisciplinesStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region DisciplineFrequency
                                            if (columnvalue.DisciplineFrequency != default(int) || (columnvalue.FirstColumn.Equals("DisciplineFrequency") && columnvalue.DisciplineFrequency != null))
                                            {
                                                var DisciplineFrequenciesStaging = dataRow.GetValue(columnvalue.DisciplineFrequency);
                                                if (DisciplineFrequenciesStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Discipline Frequencies: " + DisciplineFrequenciesStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region Triage
                                            if (columnvalue.Triage != default(int) || (columnvalue.FirstColumn.Equals("Triage") && columnvalue.Triage != null))
                                            {
                                                var triageStaging = dataRow.GetValue(columnvalue.Triage);
                                                if (triageStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Triage = Int32.Parse(triageStaging);
                                                }
                                                sb.Append("Triage"); sb.Append(": "); sb.Append(patientData.Triage); sb.Append("\t\t");
                                            }
                                            #endregion
                                            #region Therapist
                                            if (columnvalue.Therapist != default(int) || (columnvalue.FirstColumn.Equals("Therapist") && columnvalue.Therapist != null))
                                            {
                                                var therapistStaging = dataRow.GetValue(columnvalue.Therapist);
                                                if (therapistStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Therapist: " + therapistStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region CaseManager
                                            if (columnvalue.CaseManager != default(int) || (columnvalue.FirstColumn.Equals("CaseManager") && columnvalue.CaseManager != null))
                                            {
                                                var caseManagerStaging = dataRow.GetValue(columnvalue.CaseManager);
                                                if (caseManagerStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Case Manager: " + caseManagerStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region ExternalReferral
                                            if (columnvalue.ExternalReferral != default(int) || (columnvalue.FirstColumn.Equals("externalReferral_col") && columnvalue.ExternalReferral != null))
                                            {
                                                var externalReferralStaging = dataRow.GetValue(columnvalue.ExternalReferral);
                                                if (externalReferralStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "External Referral: " + externalReferralStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region InsuranceComment
                                            var insuranceStaging = dataRow.GetValue(columnvalue.Insurance);
                                            if (insuranceStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;
                                                var insuranceNumber = dataRow.GetValue(columnvalue.HIC);
                                                if (insuranceNumber.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Insurance Number: " + insuranceNumber + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region Comments
                                            string CommentName = string.Empty; int CommentNum = default(int);
                                            CommentName = "Primary Diagnosis Description:"; CommentNum = 8; patientData.Comments += (dataRow.GetValue(CommentNum).IsNotNullOrEmpty() ? CommentName + dataRow.GetValue(CommentNum) : string.Empty);
                                            CommentName = "Cert Period:"; CommentNum = 20; patientData.Comments += (dataRow.GetValue(CommentNum).IsNotNullOrEmpty() ? CommentName + dataRow.GetValue(CommentNum) : string.Empty);
                                            //patientData.Comments += (dataRow.GetValue(8).IsNotNullOrEmpty() ? " " + dataRow.GetValue(8) : string.Empty);
                                            //patientData.Comments += (dataRow.GetValue(20).IsNotNullOrEmpty() ? " " + dataRow.GetValue(20) : string.Empty);
                                            #endregion
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;
                                            bool checkExists = default(bool);
                                            Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, columnvalue.agencyId);
                                            if (patientExisting != null)
                                            {
                                                checkExists = true;
                                            }
                                            if (checkExists != true)
                                            {
                                                #region DBWrite
                                                var medicationProfile = new MedicationProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = columnvalue.agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = columnvalue.agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };

                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = columnvalue.agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, columnvalue.agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                var exists = true;
                                                                //#region physicianDBWrite
                                                                //var npi = dataRow.GetValue(columnvalue.PhysicianNPI);
                                                                //if (npi.IsNotNullOrEmpty())
                                                                //{
                                                                //    var physician = Database.GetPhysician(npi, columnvalue.agencyId);
                                                                //    if (physician == null)
                                                                //    {
                                                                //        exists = false;
                                                                //        var info = Database.GetNpiData(npi);
                                                                //        if (info != null)
                                                                //        {
                                                                //            physician = new AgencyPhysician
                                                                //            {
                                                                //                Id = Guid.NewGuid(),
                                                                //                AgencyId = columnvalue.agencyId,
                                                                //                NPI = npi,
                                                                //                LoginId = Guid.Empty,
                                                                //                AddressLine1 = string.Empty,
                                                                //                AddressCity = string.Empty,
                                                                //                AddressStateCode = string.Empty,
                                                                //                AddressZipCode = string.Empty,
                                                                //                PhoneWork = string.Empty,
                                                                //                FaxNumber = string.Empty,
                                                                //                Credentials = string.Empty
                                                                //            };
                                                                //            #region Address1
                                                                //            if (columnvalue.PhysicianAddress1 != default(int))
                                                                //            {
                                                                //                var mdAddr1Staging = dataRow.GetValue(columnvalue.PhysicianAddress1);
                                                                //                if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.AddressLine1 = mdAddr1Staging;
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region City
                                                                //            if (columnvalue.PhysicianCity != default(int))
                                                                //            {
                                                                //                var mdCityStaging = dataRow.GetValue(columnvalue.PhysicianCity);
                                                                //                if (mdCityStaging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.AddressCity = mdCityStaging;
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region State
                                                                //            if (columnvalue.PhysicianState != default(int))
                                                                //            {
                                                                //                var mdStateStaging = dataRow.GetValue(columnvalue.PhysicianState);
                                                                //                if (mdStateStaging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.AddressStateCode = mdStateStaging;
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region Zip
                                                                //            if (columnvalue.PhysicianZip != default(int))
                                                                //            {
                                                                //                var mdZipStaging = dataRow.GetValue(columnvalue.PhysicianZip);
                                                                //                if (mdZipStaging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.AddressZipCode = mdZipStaging;
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region Phone
                                                                //            if (columnvalue.PhysicianPhone != default(int))
                                                                //            {
                                                                //                var mdPhoneStaging = dataRow.GetValue(columnvalue.PhysicianPhone);
                                                                //                if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.PhoneWork = mdPhoneStaging.ToPhoneDB();
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region Fax
                                                                //            if (columnvalue.PhysicianFax != default(int))
                                                                //            {
                                                                //                var mdFaxStaging = dataRow.GetValue(columnvalue.PhysicianFax);
                                                                //                if (mdFaxStaging.IsNotNullOrEmpty())
                                                                //                {
                                                                //                    physician.FaxNumber = mdFaxStaging.ToPhoneDB();
                                                                //                }
                                                                //            }
                                                                //            #endregion
                                                                //            #region Name
                                                                //            var physicianName = dataRow.GetValue(columnvalue.PhysicianName);
                                                                //            if (physicianName.IsNotNullOrEmpty())
                                                                //            {
                                                                //                var physicianNameArray = physicianName.Trim().Split(new char[] { columnvalue.MDSplitChar }, StringSplitOptions.RemoveEmptyEntries);
                                                                //                if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                //                {
                                                                //                    if (physicianNameArray.Length == 3)
                                                                //                    {
                                                                //                        physician.FirstName = physicianNameArray[columnvalue.MDSUBFirst].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBFirst].Trim() : info.ProviderFirstName;
                                                                //                        physician.MiddleName = physicianNameArray[columnvalue.MDSUBMiddle].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBMiddle].Trim() : info.ProviderMiddleName;
                                                                //                        physician.LastName = physicianNameArray[columnvalue.MDSUBLast].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBLast].Trim() : info.ProviderLastName;
                                                                //                    }
                                                                //                    else
                                                                //                    {
                                                                //                        if (columnvalue.MDSUBFirst > columnvalue.MDSUBLast) { columnvalue.MDSUBFirst = columnvalue.MDSUBFirst - 1; }
                                                                //                        else if (columnvalue.MDSUBLast > columnvalue.MDSUBFirst) { columnvalue.MDSUBLast = columnvalue.MDSUBLast - 1; }
                                                                //                        physician.FirstName = physicianNameArray[columnvalue.MDSUBFirst].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBFirst].Trim() : info.ProviderFirstName;
                                                                //                        physician.LastName = physicianNameArray[columnvalue.MDSUBLast].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBLast].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                //                    }
                                                                //                }
                                                                //                else
                                                                //                {
                                                                //                    physician.LastName = info.ProviderLastName;
                                                                //                    physician.FirstName = info.ProviderFirstName;
                                                                //                }
                                                                //            }
                                                                //            else
                                                                //            {
                                                                //                physician.LastName = info.ProviderLastName;
                                                                //                physician.FirstName = info.ProviderFirstName;
                                                                //            }
                                                                //            #endregion
                                                                //        }
                                                                //        Database.Add(physician);
                                                                //    }
                                                                //    if (physician != null)
                                                                //    {
                                                                //        var patientPhysician = new PatientPhysician
                                                                //        {
                                                                //            IsPrimary = true,
                                                                //            PatientId = patientData.Id,
                                                                //            PhysicianId = physician.Id
                                                                //        };
                                                                //        if (Database.Add(patientPhysician))
                                                                //        {
                                                                //            Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "PHYSICIAN ALREADY EXISTS" : "");
                                                                //        }
                                                                //    }
                                                                //}
                                                                //#endregion
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("\nTotal Patients:" + totalpatients);
                    Console.WriteLine("\nTotal Counted:" + totalCounter);
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}