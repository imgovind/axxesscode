﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public class PromptColumnPhysician
    {
        public int PhysicianCode { get; set; }
        public int FirstName { get; set; }
        public int LastName { get; set; }
        public int MiddleInitial { get; set; }
        public int Office { get; set; }
        public int Address1 { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
        public int Phone { get; set; }
        public int Title { get; set; }
        public int DegreeDesignation { get; set; }
        public int StateLicense { get; set; }
        public int MedicaidLicense { get; set; }
        public int Fax { get; set; }
        public int NPI { get; set; }
        public int PecosCertified { get; set; }
        public int PecosCertDate { get; set; }

        public bool DBWritePermission { get; set; }

        public Guid AgencyId { get; set; }

        public string FirstColumn { get; set; }
    }

    public static class PromptPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\vida-doctor.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\vida-doctor-event-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\vida-doctor-data-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            PromptColumnPhysician columnvalue = new PromptColumnPhysician
            {
                LastName = 1,
                FirstName = 2,
                MiddleInitial = 3,
                Office = 4,
                Address1 = 5,
                City = 6,
                State = 7,
                Zip = 8,
                Phone = 9,
                StateLicense = 12,
                MedicaidLicense = 13,
                Fax = 15,
                NPI = 18,
                DBWritePermission = true,
                AgencyId = agencyId
            };

            foreach (var item in typeof(PromptColumnPhysician).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }

            Process(columnvalue);
        }

        public static void Process(PromptColumnPhysician column)
        {
            int addedCounter = default(int), cannotAddCounter = default(int), skippedCounter = default(int), noNPICounter = default(int);
            using (TextWriter errorWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                            {
                                if (excelReader != null && excelReader.IsValid)
                                {
                                    excelReader.IsFirstRowAsColumnNames = true;
                                    DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                    if (dataTable != null && dataTable.Rows.Count > 0)
                                    {
                                        var i = 1;
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!dataRow.IsEmpty())
                                            {
                                                StringBuilder logOutput = new StringBuilder();
                                                StringBuilder commentOutput = new StringBuilder();
                                                var npi = dataRow.GetValue(column.NPI);
                                                var physician = Database.GetPhysician(npi, column.AgencyId);
                                                if (physician == null)
                                                {
                                                    var info = Database.GetNpiData(npi);
                                                    if (info != null)
                                                    {
                                                        physician = new AgencyPhysician
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            NPI = npi,
                                                            LoginId = Guid.Empty
                                                        };

                                                        #region Name
                                                        if (column.FirstName != default(int) || (column.FirstColumn.Equals("FirstName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.FirstName);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.FirstName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("FN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.MiddleInitial != default(int) || (column.FirstColumn.Equals("MiddleName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.MiddleInitial);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.MiddleName = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("MN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.LastName != default(int) || (column.FirstColumn.Equals("LastName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.LastName);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.LastName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("LN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        #endregion
                                                        #region Address
                                                        if (column.Address1 != default(int) || (column.FirstColumn.Equals("Address1")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Address1);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressLine1 = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("Address1:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.City != default(int) || (column.FirstColumn.Equals("City")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.City);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressCity = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("City:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.State != default(int) || (column.FirstColumn.Equals("State")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.State);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressStateCode = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("State:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.Zip != default(int) || (column.FirstColumn.Equals("Zip")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Zip);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressZipCode = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("Zip:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        #endregion
                                                        #region PhoneFax
                                                        if (column.Phone != default(int) || (column.FirstColumn.Equals("Phone")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Phone);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                Staging = Staging.ToPhoneDB();
                                                                physician.PhoneWork = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("Phone:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.Fax != default(int) || (column.FirstColumn.Equals("Fax")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Fax);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                Staging = Staging.ToPhoneDB();
                                                                physician.FaxNumber = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                                logOutput.Append("Fax:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        #endregion
                                                        #region Others
                                                        if (column.PecosCertified != default(int) || (column.FirstColumn.Equals("PecosStatus")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PecosCertified);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                                commentOutput.Append("Pecos Certified:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                                logOutput.Append("Pecos Certified:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                            
                                                        }
                                                        if (column.PecosCertDate != default(int) || (column.FirstColumn.Equals("PecosLastChecked")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PecosCertDate);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                                commentOutput.Append("Pecos Cert Date:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                                logOutput.Append("Pecos Cert Date:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.MedicaidLicense != default(int) || (column.FirstColumn.Equals("LicenseNumber")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.MedicaidLicense);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                                commentOutput.Append("Medicaid Number:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                                logOutput.Append("Medicaid Number:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.StateLicense != default(int) || (column.FirstColumn.Equals("LicenseState")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.StateLicense);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                                commentOutput.Append("License State:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                                logOutput.Append("License State:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        if (column.PhysicianCode != default(int) || (column.FirstColumn.Equals("PhysicianCode")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PhysicianCode);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                                commentOutput.Append("Physician Code:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                                logOutput.Append("Physician Code:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                            }
                                                        }
                                                        physician.Comments = commentOutput.ToString();
                                                        #endregion
                                                        #region specialProcess
                                                        if (physician.FirstName.IsNullOrEmpty()) { physician.FirstName = ""; }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Physician Not Added - No NPI Info");
                                                        noNPICounter++;
                                                    }
                                                    if (column.DBWritePermission == true)
                                                    {
                                                        if (Database.Add(physician))
                                                        {
                                                            Console.WriteLine("(" + i.ToString() + ")" + "Physician Added: " + physician.LastName + ", " + physician.FirstName);
                                                            addedCounter++;
                                                            logWriter.WriteLine(logOutput.ToString());
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("(" + i.ToString() + ")" + "Physician NOT Added: ");
                                                            cannotAddCounter++;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("(" + i.ToString() + ")" + "Physician PRESENT: " + physician.LastName + ", " + physician.FirstName);
                                                    skippedCounter++;
                                                }
                                                logOutput.Length = 0;
                                                commentOutput.Length = 0;
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorWriter.WriteLine(ex);
                }
            }
        }
    }
}
