﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public class PromptColumnActive
    {
        public int IDNumber { get; set; }

        public int FirstName { get; set; }
        public int LastName { get; set; }
        public int MiddleInitial { get; set; }
        public int AdmissionDate { get; set; }
        public int Gender { get; set; }
        public int DOB { get; set; }
        public int MaritalStatus { get; set; }
        public int Address1 { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
        public int ServiceLocation { get; set; }
        public int SSN { get; set; }
        public int Phone { get; set; }
        public int PhysicianName1 { get; set; }
        public int PhysicianName2 { get; set; }
        public int PayorName1 { get; set; }
        public int InsuranceNumber1 { get; set; }
        public int PayorName2 { get; set; }
        public int InsuranceNumber2 { get; set; }
        public int HICNumber { get; set; }
        public int MedicaidNumber { get; set; }

        public bool DBWritePermission { get; set; }
        public bool AssociatePhysician { get; set; }

        public Guid AgencyId { get; set; }
        public Guid LocationId { get; set; }

        public int PatientStatus { get; set; }
        

        public string DefaultInsurance { get; set; }
        public string DefaultPayor { get; set; }
        public bool useDefaultInsuranceValues { get; set; }

        public string FirstColumn { get; set; }
    }

    public static class PromptActive
    {
        private static string input = Path.Combine(App.Root, "Files\\area-active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\area-active-event-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\area-active-data-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            PromptColumnActive columnvalue = new PromptColumnActive 
            {
                LastName = 1,
                FirstName = 2,
                MiddleInitial = 3,
                AdmissionDate = 4,
                Gender = 5,
                DOB = 6,
                MaritalStatus = 7,
                Address1 = 8,
                City = 10,
                State = 11,
                Zip = 12,
                ServiceLocation = 13,
                SSN = 14,
                Phone = 15,
                PhysicianName1 = 16,
                PhysicianName2 = 17,
                PayorName1 = 18,
                InsuranceNumber1 = 19,
                PayorName2 = 20,
                InsuranceNumber2 = 21,
                HICNumber = 22,
                MedicaidNumber = 23,
                AgencyId = agencyId,
                LocationId = locationId,
                PatientStatus = 1,
                DefaultInsurance = "MEDICARE",
                DefaultPayor = "1",
                DBWritePermission = true,
                useDefaultInsuranceValues = false,
                AssociatePhysician = false
            };

            foreach (var item in typeof(PromptColumnActive).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }

            Process(columnvalue);
        }

        public static void Process(PromptColumnActive column)
        {
            using (TextWriter errorWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                            {
                                if (excelReader != null && excelReader.IsValid)
                                {
                                    excelReader.IsFirstRowAsColumnNames = true;
                                    DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                    if (dataTable != null && dataTable.Rows.Count > 0)
                                    {
                                        var i = 1;
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!dataRow.IsEmpty())
                                            {
                                                StringBuilder logOutput = new StringBuilder();
                                                StringBuilder commentOutput = new StringBuilder();
                                                var patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = column.AgencyId;
                                                patientData.AgencyLocationId = column.LocationId;
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                patientData.Status = column.PatientStatus;
                                                patientData.ServiceLocation = "Q5001";
                                                patientData.Gender = "";

                                                #region IDNumber
                                                Random random = new Random();
                                                patientData.PatientIdNumber = random.Next(100000).ToString();
                                                #endregion
                                                #region Name
                                                if (column.FirstName != default(int) || (column.FirstColumn.Equals("FirstName")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.FirstName);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.FirstName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                        ForComment = Staging.Trim();
                                                        logOutput.Append("FN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                if (column.MiddleInitial != default(int) || (column.FirstColumn.Equals("MiddleName")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.MiddleInitial);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MiddleInitial = Staging.Trim();
                                                        ForComment = Staging.Trim();
                                                        logOutput.Append("MN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                if (column.LastName != default(int) || (column.FirstColumn.Equals("LastName")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.LastName);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.LastName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                        ForComment = Staging.Trim();
                                                        logOutput.Append("LN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region AdmissionDate
                                                if (column.AdmissionDate != default(int) || (column.FirstColumn.Equals("AdmissionDate")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.AdmissionDate);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.IsDouble())
                                                        {
                                                            ForComment = DateTime.FromOADate(double.Parse(Staging)).ToShortDateString();
                                                        }
                                                        else
                                                        {
                                                            ForComment = DateTime.Parse(Staging).ToShortDateString();
                                                        }
                                                        logOutput.Append("Admission Date"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        commentOutput.Append("Admission Date"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region Gender
                                                if (column.Gender != default(int) || (column.FirstColumn.Equals("Gender")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.Gender);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.ToUpper().Contains("M")) 
                                                        {
                                                            patientData.Gender = "Male";
                                                        } 
                                                        else if (Staging.ToUpper().Contains("F")) 
                                                        { 
                                                            patientData.Gender = "Female";
                                                        }
                                                        ForComment = patientData.Gender.Trim();
                                                        logOutput.Append("LN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region DOB
                                                if (column.DOB != default(int) || (column.FirstColumn.Equals("DOB")))
                                                {
                                                    var Staging = dataRow.GetValue(column.DOB);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.IsDouble())
                                                        {
                                                            patientData.DOB = DateTime.FromOADate(double.Parse(Staging));
                                                        }
                                                        else
                                                        {
                                                            patientData.DOB = DateTime.Parse(Staging);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.MinValue;
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }
                                                logOutput.Append("DOB"); logOutput.Append(": "); logOutput.Append(patientData.DOB); logOutput.Append("\t\t");
                                                #endregion
                                                #region Marital
                                                if (column.MaritalStatus != default(int) || (column.FirstColumn.Equals("MaritalStatus")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.MaritalStatus);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        Staging = Staging.ToUpperCase();
                                                        if (Staging.StartsWith("M"))
                                                        {
                                                            patientData.MaritalStatus = "Married";
                                                        }
                                                        else if (Staging.StartsWith("S"))
                                                        {
                                                            patientData.MaritalStatus = "Single";
                                                        }
                                                        else if (Staging.StartsWith("W"))
                                                        {
                                                            patientData.MaritalStatus = "Widowed";
                                                        }
                                                        else if (Staging.StartsWith("D"))
                                                        {
                                                            patientData.MaritalStatus = "Divorced";
                                                        }
                                                        else
                                                        {
                                                            patientData.MaritalStatus = "Unknown";
                                                        }
                                                        ForComment = patientData.MaritalStatus.Trim();
                                                        logOutput.Append("Marital Status:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region Address
                                                if (column.Address1 != default(int) || (column.FirstColumn.Equals("Address1")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.Address1);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.AddressLine1 = Staging.Trim();
                                                        ForComment = Staging.Trim();
                                                    }
                                                    logOutput.Append("Address1:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                }
                                                if (column.City != default(int) || (column.FirstColumn.Equals("City")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.City);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.AddressCity = Staging.Trim();
                                                        ForComment = Staging.Trim();
                                                    }
                                                    logOutput.Append("City:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                }
                                                if (column.State != default(int) || (column.FirstColumn.Equals("State")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.State);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.AddressStateCode = Staging.Trim();
                                                        ForComment = Staging.Trim();
                                                    }
                                                    logOutput.Append("State:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                }
                                                if (column.Zip != default(int) || (column.FirstColumn.Equals("Zip")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.Zip);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.AddressZipCode = Staging.Trim();
                                                        ForComment = Staging.Trim();
                                                    }
                                                    logOutput.Append("Zip:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                }
                                                #endregion
                                                #region ServiceLocation
                                                if (column.ServiceLocation != default(int) || (column.FirstColumn.Equals("ServiceLocation")))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.ServiceLocation);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.ToUpper().Contains("HOME")) 
                                                        {
                                                            patientData.ServiceLocation = "Q5001";
                                                        }
                                                        else if (Staging.ToUpper().Contains("ASSISTIVE"))
                                                        {
                                                            patientData.ServiceLocation = "Q5002";
                                                        }
                                                        ForComment = patientData.ServiceLocation.Trim();
                                                        logOutput.Append("MN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region SSN
                                                if (column.SSN != default(int) || (column.FirstColumn.Equals("SSN")))
                                                {
                                                    var Staging = dataRow.GetValue(column.SSN).Replace("-", "").Trim();
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.SSN = Staging;
                                                    }
                                                    logOutput.Append("SSN"); logOutput.Append(": "); logOutput.Append(patientData.SSN); logOutput.Append("\t\t");
                                                }
                                                #endregion
                                                #region Phone
                                                if (column.Phone != default(int) || (column.FirstColumn.Equals("Phone")))
                                                {
                                                    var Staging = dataRow.GetValue(column.Phone);
                                                    patientData.PhoneHome = Staging.IsNotNullOrEmpty() ? Staging.ToPhoneDB() : string.Empty;
                                                    logOutput.Append("PhoneHome"); logOutput.Append(": "); logOutput.Append(patientData.PhoneHome); logOutput.Append("\t\t");
                                                }
                                                #endregion
                                                #region Physician
                                                if (column.PhysicianName1 != default(int) || (column.FirstColumn.Equals("PhysicianName1")))
                                                {
                                                    var Staging = dataRow.GetValue(column.PhysicianName1);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        commentOutput.Append("Physician Name 1:"); commentOutput.Append(": "); commentOutput.Append(Staging); commentOutput.Append("\t\t");
                                                        logOutput.Append("Physician Name 1:"); logOutput.Append(": "); logOutput.Append(Staging); logOutput.Append("\t\t");
                                                    }
                                                }
                                                if (column.PhysicianName2 != default(int) || (column.FirstColumn.Equals("PhysicianName2")))
                                                {
                                                    var Staging = dataRow.GetValue(column.PhysicianName2);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        commentOutput.Append("Physician Name 2:"); commentOutput.Append(": "); commentOutput.Append(Staging); commentOutput.Append("\t\t");
                                                        logOutput.Append("Physician Name 2:"); logOutput.Append(": "); logOutput.Append(Staging); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region PrimaryInsurance
                                                if (column.PayorName1 != default(int) || (column.FirstColumn.Equals("PayorName1")))
                                                {
                                                    var PrimaryInsuranceForComment = default(string);
                                                    var PayorForComment = default(string);
                                                    var PrimaryInsuranceStaging = dataRow.GetValue(column.PayorName1);
                                                    if (PrimaryInsuranceStaging.IsNotNullOrEmpty())
                                                    {
                                                        if (PrimaryInsuranceStaging.ToUpper().Contains("MEDICARE"))
                                                        {
                                                            patientData.PrimaryInsurance = column.DefaultPayor;
                                                            patientData.MedicareNumber = dataRow.GetValue(column.InsuranceNumber1).Replace("-","").Trim();
                                                        }
                                                        else
                                                        {
                                                            if (column.useDefaultInsuranceValues == default(bool))
                                                            {
                                                                PrimaryInsuranceForComment = PrimaryInsuranceStaging;
                                                                PayorForComment = column.DefaultPayor;
                                                            }
                                                            else
                                                            {
                                                                PrimaryInsuranceForComment = column.DefaultInsurance;
                                                                PayorForComment = column.DefaultPayor;
                                                            }
                                                        }
                                                        commentOutput.Append("Primary Insurance"); commentOutput.Append(": "); commentOutput.Append(PrimaryInsuranceForComment); commentOutput.Append("\t\t");
                                                        commentOutput.Append("Agency Payor"); commentOutput.Append(": "); commentOutput.Append(PayorForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("Primary Insurance"); logOutput.Append(": "); logOutput.Append(PrimaryInsuranceForComment); logOutput.Append("\t\t");
                                                        logOutput.Append("Agency Payor"); logOutput.Append(": "); logOutput.Append(PayorForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                if (column.PayorName2 != default(int) || (column.FirstColumn.Equals("PayorName2")))
                                                {
                                                    var SecondaryInsuranceForComment = default(string);
                                                    var SecondaryPayorForComment = default(string);
                                                    var SecondaryInsuranceStaging = dataRow.GetValue(column.PayorName2);
                                                    if (SecondaryInsuranceStaging.IsNotNullOrEmpty())
                                                    {
                                                        if (SecondaryInsuranceStaging.ToUpper().Contains("MEDICARE"))
                                                        {
                                                            patientData.SecondaryInsurance = column.DefaultPayor;
                                                            if (patientData.MedicareNumber.IsNullOrEmpty()) { patientData.MedicareNumber = dataRow.GetValue(column.InsuranceNumber2).Replace("-", "").Trim(); }
                                                        }
                                                        else
                                                        {
                                                            if (column.useDefaultInsuranceValues == default(bool))
                                                            {
                                                                SecondaryInsuranceForComment = SecondaryInsuranceStaging;
                                                                SecondaryPayorForComment = column.DefaultPayor;
                                                            }
                                                            else
                                                            {
                                                                SecondaryInsuranceForComment = column.DefaultInsurance;
                                                                SecondaryPayorForComment = column.DefaultPayor;
                                                            }
                                                        }
                                                        commentOutput.Append("Secondary Insurance"); commentOutput.Append(": "); commentOutput.Append(SecondaryInsuranceForComment); commentOutput.Append("\t\t");
                                                        commentOutput.Append("Agency Payor"); commentOutput.Append(": "); commentOutput.Append(SecondaryPayorForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("Secondary Insurance"); logOutput.Append(": "); logOutput.Append(SecondaryInsuranceForComment); logOutput.Append("\t\t");
                                                        logOutput.Append("Agency Payor"); logOutput.Append(": "); logOutput.Append(SecondaryPayorForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region HICNumber
                                                if (column.HICNumber != default(int) || (column.FirstColumn.Equals("HICNumber")))
                                                {
                                                    var Staging = dataRow.GetValue(column.HICNumber);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        commentOutput.Append("HICNumber:"); commentOutput.Append(": "); commentOutput.Append(Staging); commentOutput.Append("\t\t");
                                                        logOutput.Append("HICNumber:"); logOutput.Append(": "); logOutput.Append(Staging); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region MedicaidNumber
                                                if (column.MedicaidNumber != default(int) || (column.FirstColumn.Equals("MedicaidNumber")))
                                                {
                                                    var Staging = dataRow.GetValue(column.MedicaidNumber);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MedicaidNumber = Staging;
                                                        commentOutput.Append("Medicaid:"); commentOutput.Append(": "); commentOutput.Append(Staging); commentOutput.Append("\t\t");
                                                        logOutput.Append("Medicaid:"); logOutput.Append(": "); logOutput.Append(Staging); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion

                                                patientData.Comments = commentOutput.ToString();

                                                #region DBWrite
                                                if (column.DBWritePermission == true)
                                                {
                                                    bool checkExists = default(bool);
                                                    Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, column.AgencyId);

                                                    if (patientExisting != null)
                                                    {
                                                        checkExists = true;
                                                    }

                                                    if (checkExists != true)
                                                    {
                                                        var medicationProfile = new MedicationProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Medication = "<ArrayOfMedication />"
                                                        };

                                                        var allergyProfile = new AllergyProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Allergies = "<ArrayOfAllergy />"
                                                        };

                                                        if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                        {
                                                            var admissionPeriod = new PatientAdmissionDate
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = column.AgencyId,
                                                                Created = DateTime.Now,
                                                                DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                                IsActive = true,
                                                                IsDeprecated = false,
                                                                Modified = DateTime.Now,
                                                                PatientData = patientData.ToXml().Replace("'", ""),
                                                                PatientId = patientData.Id,
                                                                Reason = string.Empty,
                                                                StartOfCareDate = patientData.StartofCareDate,
                                                                Status = patientData.Status
                                                            };
                                                            if (Database.Add(admissionPeriod))
                                                            {
                                                                var patient = Database.GetPatient(patientData.Id, column.AgencyId);
                                                                if (patient != null)
                                                                {
                                                                    patient.AdmissionId = admissionPeriod.Id;
                                                                    if (Database.Update(patient))
                                                                    {
                                                                        Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                        var exists = true;
                                                                        #region AssociatePhysician
                                                                        //if (column.AssociatePhysician == true)
                                                                        //{
                                                                        //    var npi = dataRow.GetValue(mdNPI_col);
                                                                        //    if (npi.IsNotNullOrEmpty())
                                                                        //    {
                                                                        //        var physician = Database.GetPhysician(npi, agencyId);
                                                                        //        if (physician == null)
                                                                        //        {
                                                                        //            exists = false;
                                                                        //            var info = Database.GetNpiData(npi);
                                                                        //            if (info != null)
                                                                        //            {
                                                                        //                physician = new AgencyPhysician
                                                                        //                {
                                                                        //                    Id = Guid.NewGuid(),
                                                                        //                    AgencyId = column.agencyId,
                                                                        //                    NPI = npi,
                                                                        //                    LoginId = Guid.Empty,
                                                                        //                    AddressLine1 = string.Empty,
                                                                        //                    AddressCity = string.Empty,
                                                                        //                    AddressStateCode = string.Empty,
                                                                        //                    AddressZipCode = string.Empty,
                                                                        //                    PhoneWork = string.Empty,
                                                                        //                    FaxNumber = string.Empty,
                                                                        //                    Credentials = string.Empty,
                                                                        //                    FirstName = dataRow.GetValue(md_fn_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_fn_col) : string.Empty,
                                                                        //                    LastName = dataRow.GetValue(md_ln_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_ln_col) : string.Empty
                                                                        //                };
                                                                        //                #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                        //                if (mdAddr1_col != default(int))
                                                                        //                {
                                                                        //                    var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                        //                    if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressLine1 = mdAddr1Staging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdCity_col != default(int))
                                                                        //                {
                                                                        //                    var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                        //                    if (mdCityStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressCity = mdCityStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdState_col != default(int))
                                                                        //                {
                                                                        //                    var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                        //                    if (mdStateStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressStateCode = mdStateStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdZip_col != default(int))
                                                                        //                {
                                                                        //                    var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                        //                    if (mdZipStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressZipCode = mdZipStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdPhone_col != default(int))
                                                                        //                {
                                                                        //                    var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                        //                    if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.PhoneWork = mdPhoneStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdFax_col != default(int))
                                                                        //                {
                                                                        //                    var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                        //                    if (mdFaxStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.FaxNumber = mdFaxStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdCred_col != default(int))
                                                                        //                {
                                                                        //                    var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                        //                    if (mdCredStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.Credentials = mdCredStaging;
                                                                        //                    }
                                                                        //                }
                                                                        //                #endregion
                                                                        //                var physicianName = dataRow.GetValue(mdName_col);
                                                                        //                if (physicianName.IsNotNullOrEmpty())
                                                                        //                {
                                                                        //                    var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                        //                    if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                        //                    {
                                                                        //                        if (physicianNameArray.Length == 3)
                                                                        //                        {
                                                                        //                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                        //                            physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                        //                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                        //                        }
                                                                        //                        else
                                                                        //                        {
                                                                        //                            physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                        //                            physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                        //                        }
                                                                        //                    }
                                                                        //                    else
                                                                        //                    {
                                                                        //                        physician.LastName = info.ProviderLastName;
                                                                        //                        physician.FirstName = info.ProviderFirstName;
                                                                        //                    }
                                                                        //                }
                                                                        //                else
                                                                        //                {
                                                                        //                    physician.LastName = info.ProviderLastName;
                                                                        //                    physician.FirstName = info.ProviderFirstName;
                                                                        //                }
                                                                        //            }
                                                                        //            Database.Add(physician);
                                                                        //        }
                                                                        //        if (physician != null)
                                                                        //        {
                                                                        //            var patientPhysician = new PatientPhysician
                                                                        //            {
                                                                        //                IsPrimary = true,
                                                                        //                PatientId = patientData.Id,
                                                                        //                PhysicianId = physician.Id
                                                                        //            };
                                                                        //            if (Database.Add(patientPhysician))
                                                                        //            {
                                                                        //                Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                        //            }
                                                                        //        }
                                                                        //    }
                                                                        //}
                                                                        #endregion
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                                    }
                                                }
                                                #endregion

                                                logWriter.WriteLine(logOutput.ToString());
                                                logOutput.Length = 0;
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorWriter.WriteLine(ex);
                }
            }
        }

    }
}
