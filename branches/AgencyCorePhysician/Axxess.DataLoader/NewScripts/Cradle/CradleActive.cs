﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using Excel;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;
    using Axxess.DataLoader.Columns;
    public static class CradleActive
    {
        private static CradleActiveColumn columnvalue = new CradleActiveColumn();
        private static string inputFile = Path.Combine(App.Root, string.Format("Files\\glow-active.xls"));
        private static string eventLog = Path.Combine(App.Root, string.Format("Files\\glow-active-EventLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string CommentsDelimiter = "; ";

        public static void Run(Guid agencyId, Guid locationId)
        {
            columnvalue.ServiceLocation = "Q5001";
            columnvalue.DefaultPayor = "1";
            columnvalue.MDSplitChar = ',';
            columnvalue.MDSUBFirst = 0;
            columnvalue.MDSUBLast = 2;
            columnvalue.MDSUBMiddle = 1;
            columnvalue.agencyId = agencyId;
            columnvalue.locationId = locationId;
            columnvalue.PatientStatus = 1;
            #region columns
            columnvalue.PatientHICNumber = 0;
            columnvalue.SOCDate = 1;
            columnvalue.EpisodeFrom = 2;
            columnvalue.EpisodeTo = 3;
            columnvalue.PatientMRN = 4;
            columnvalue.PatientLastName = 5;
            columnvalue.PatientFirstName = 6;
            columnvalue.PatientAddress = 7;
            columnvalue.PatientCity = 8;
            columnvalue.PatientState = 9;
            columnvalue.PatientZip = 10;
            columnvalue.PatientPhone = 11;
            columnvalue.PatientDOB = 12;
            columnvalue.EmergencyLevel = 14;
            columnvalue.EpisodeFreq = 15;
            columnvalue.PhysicianNPI = 16;
            columnvalue.PhysicianLName = 18;
            columnvalue.PhysicianFName = 19;
            columnvalue.PhysicianAddr = 20;
            columnvalue.PhysicianCity = 21;
            columnvalue.PhysicianState = 22;
            columnvalue.PhysicianZip = 23;
            columnvalue.PhysicianPhone = 24;
            columnvalue.PhysicianFax = 25;
            columnvalue.PrimDiagCode = 28;
            columnvalue.PrimDiagDesc = 29;
            columnvalue.ActiveFlag = 30;
            columnvalue.DischargeType = 31;
            columnvalue.DischargeDate = 32;
            columnvalue.DischargeReason = 33;
            columnvalue.PayerName = 36;
            columnvalue.RFD = 39;
            columnvalue.DefaultInsurance = "Medicare";
            #endregion
            #region FirstColumn
            foreach (var item in typeof(CradleActiveColumn).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }
            #endregion
            Process();
        }

        private static void Process()
        {
            using (TextWriter textWriter = new StreamWriter(eventLog, true))
            {
                try
                {
                    var totalpatients = default(int);
                    var totalCounter = default(int);
                    using (FileStream fileStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        totalpatients = dataTable.Rows.Count;
                                        totalCounter++;
                                        if (!dataRow.IsEmpty())
                                        {
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = columnvalue.agencyId;
                                            patientData.AgencyLocationId = columnvalue.locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.ServiceLocation = columnvalue.ServiceLocation;
                                            patientData.Status = columnvalue.PatientStatus; 
                                            
                                            #region Medicare
                                            bool primaryInsurancePayor = default(bool);
                                            bool IsMedicaidpatient = default(bool);
                                            var insuranceStagingNomer = columnvalue.DefaultInsurance.IsNullOrEmpty() ? dataRow.GetValue(columnvalue.PayerName) : columnvalue.DefaultInsurance;
                                            if (insuranceStagingNomer.ToUpper().Contains("MEDICARE") || insuranceStagingNomer.ToUpper().Contains("PALMETTO"))
                                            {
                                                primaryInsurancePayor = true;
                                            }
                                            else if (insuranceStagingNomer.ToUpper().Contains("MEDICAID"))
                                            {
                                                IsMedicaidpatient = true;
                                            }

                                            if (columnvalue.PatientHICNumber != default(int) || (columnvalue.FirstColumn.Equals("PatientHICNumber") && columnvalue.PatientHICNumber != null))
                                            {
                                                if (IsMedicaidpatient == true)
                                                {
                                                    var medicaidNumberStaging = dataRow.GetValue(columnvalue.PatientHICNumber);
                                                    if (medicaidNumberStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MedicaidNumber = medicaidNumberStaging;
                                                    }
                                                }

                                                var medicareNumberStaging = dataRow.GetValue(columnvalue.PatientHICNumber);
                                                if (medicareNumberStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.MedicareNumber = medicareNumberStaging;
                                                    primaryInsurancePayor = true;
                                                }
                                            }
                                            #endregion
                                            #region SOC
                                            if (columnvalue.SOCDate != default(int) || (columnvalue.FirstColumn.Equals("SOCDate") && columnvalue.SOCDate != null))
                                            {
                                                var SOCStaging = dataRow.GetValue(columnvalue.SOCDate);
                                                if (SOCStaging.IsNotNullOrEmpty())
                                                {
                                                    if (SOCStaging.IsDouble())
                                                    {
                                                        patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.StartofCareDate = DateTime.Parse(SOCStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.StartofCareDate = DateTime.MinValue;
                                                }
                                            }
                                            #endregion
                                            #region EpisodeSD
                                            if (columnvalue.EpisodeFrom != default(int) || (columnvalue.FirstColumn.Equals("EpisodeFrom") && columnvalue.EpisodeFrom != null))
                                            {
                                                var episodeStartDateStaging = dataRow.GetValue(columnvalue.EpisodeFrom);
                                                if (episodeStartDateStaging.IsNotNullOrEmpty())
                                                {
                                                    if (episodeStartDateStaging.IsDouble())
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeStartDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.Parse(episodeStartDateStaging).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region EpisodeED
                                            if (columnvalue.EpisodeTo != default(int) || (columnvalue.FirstColumn.Equals("EpisodeTo") && columnvalue.EpisodeTo != null))
                                            {
                                                var episodeEndDateStaging = dataRow.GetValue(columnvalue.EpisodeTo);
                                                if (episodeEndDateStaging.IsNotNullOrEmpty())
                                                {
                                                    if (episodeEndDateStaging.IsDouble())
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeEndDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.Parse(episodeEndDateStaging).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region MRN
                                            if (columnvalue.PatientMRN != null || (columnvalue.FirstColumn.Equals("PatientMRN") && columnvalue.PatientMRN != null))
                                            {
                                                patientData.PatientIdNumber = dataRow.GetValue(columnvalue.PatientMRN);
                                            }
                                            #endregion
                                            #region Name
                                            if (columnvalue.PatientFirstName != default(int) || (columnvalue.FirstColumn.Equals("PatientFirstName") && columnvalue.PatientFirstName != null))
                                            {
                                                patientData.FirstName = dataRow.GetValue(columnvalue.PatientFirstName);
                                            }
                                            if (columnvalue.PatientLastName != default(int) || (columnvalue.FirstColumn.Equals("PatientLastName") && columnvalue.PatientLastName != null))
                                            {
                                                patientData.LastName = dataRow.GetValue(columnvalue.PatientLastName);
                                            }
                                            #endregion
                                            #region Address
                                            #region Address1
                                            if (columnvalue.PatientAddress != default(int) || (columnvalue.FirstColumn.Equals("PatientAddress") && columnvalue.PatientAddress != null))
                                            {
                                                var addressLineStaging = dataRow.GetValue(columnvalue.PatientAddress);
                                                if (addressLineStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressLine1 = addressLineStaging;
                                                }
                                                else
                                                {
                                                    patientData.AddressLine1 = "";
                                                }
                                            }
                                            else
                                            {
                                                patientData.AddressLine1 = "";
                                            }
                                            #endregion
                                            #region City
                                            if (columnvalue.PatientCity != default(int) || (columnvalue.FirstColumn.Equals("PatientCity") && columnvalue.PatientCity != null))
                                            {
                                                var addressCityStaging = dataRow.GetValue(columnvalue.PatientCity);
                                                if (addressCityStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressCity = addressCityStaging;
                                                }
                                            }
                                            #endregion
                                            #region State
                                            if (columnvalue.PatientState != default(int) || (columnvalue.FirstColumn.Equals("PatientState") && columnvalue.PatientState != null))
                                            {
                                                var addressStateCodeStaging = dataRow.GetValue(columnvalue.PatientState).ToUpper().Replace(".", "");
                                                if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressStateCode = addressStateCodeStaging;
                                                }
                                            }
                                            #endregion
                                            #region Zip
                                            if (columnvalue.PatientZip != default(int) || (columnvalue.FirstColumn.Equals("PatientZip") && columnvalue.PatientZip != null))
                                            {
                                                var addressZipCodeStaging = dataRow.GetValue(columnvalue.PatientZip);
                                                if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressZipCode = addressZipCodeStaging;
                                                }
                                            }
                                            #endregion
                                            #endregion
                                            #region Phone
                                            if (columnvalue.PatientPhone != default(int) || (columnvalue.FirstColumn.Equals("PatientPhone") && columnvalue.PatientPhone != null))
                                            {
                                                var phoneHomeStaging = dataRow.GetValue(columnvalue.PatientPhone);
                                                patientData.PhoneHome = phoneHomeStaging.IsNotNullOrEmpty() ? phoneHomeStaging.ToPhoneDB() : string.Empty;
                                            }
                                            #endregion
                                            #region DOB
                                            if (columnvalue.PatientDOB != default(int) || (columnvalue.FirstColumn.Equals("PatientDOB") && columnvalue.PatientDOB != null))
                                            {
                                                var DOBStaging = dataRow.GetValue(columnvalue.PatientDOB);
                                                if (DOBStaging.IsNotNullOrEmpty())
                                                {
                                                    if (DOBStaging.IsDouble())
                                                    {
                                                        patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.Parse(DOBStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }

                                                //patientData.DOB = DateTime.MinValue;
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }
                                            #endregion
                                            #region EmergencyLevel
                                            columnvalue.CommentName = "Emergency Level"; columnvalue.CommentNum = columnvalue.EmergencyLevel; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            #endregion
                                            #region DisciplineFrequency
                                            if (columnvalue.EpisodeFreq != default(int) || (columnvalue.FirstColumn.Equals("EpisodeFreq") && columnvalue.EpisodeFreq != null))
                                            {
                                                var DisciplineFrequencyStaging = dataRow.GetValue(columnvalue.EpisodeFreq);
                                                if (DisciplineFrequencyStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Discipline Frequencies: " + DisciplineFrequencyStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region PrimaryDiagnosis
                                            if (columnvalue.PrimDiagCode != default(int))
                                            {
                                                var primaryDiagStaging = dataRow.GetValue(columnvalue.PrimDiagCode);
                                                if (primaryDiagStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + primaryDiagStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region PrimaryDiagnosisDescription
                                            if (columnvalue.PrimDiagDesc != default(int))
                                            {
                                                var primaryDiagDescStaging = dataRow.GetValue(columnvalue.PrimDiagDesc);
                                                if (primaryDiagDescStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis Description: " + primaryDiagDescStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region DischargeType
                                            columnvalue.CommentName = "Discharge Type:"; columnvalue.CommentNum = columnvalue.DischargeType; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            #endregion
                                            #region DischargeDate
                                            columnvalue.CommentName = "Discharge Date:"; columnvalue.CommentNum = columnvalue.DischargeDate; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            #endregion
                                            #region DischargeReason
                                            columnvalue.CommentName = "Discharge Reason:"; columnvalue.CommentNum = columnvalue.DischargeReason; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            #endregion
                                            #region RFD
                                            columnvalue.CommentName = "RFD:"; columnvalue.CommentNum = columnvalue.RFD; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            #endregion
                                            #region PrimaryInsurance
                                            if (primaryInsurancePayor == true)
                                            {
                                                patientData.PrimaryInsurance = columnvalue.DefaultPayor;
                                            }
                                            else if (primaryInsurancePayor == false)
                                            {
                                                patientData.PrimaryInsurance = "";
                                            }
                                            #endregion
                                            #region PatientStatus
                                            if (columnvalue.ActiveFlag != default(int))
                                            {
                                                var activeFlagStaging = dataRow.GetValue(columnvalue.ActiveFlag);
                                                if (activeFlagStaging.IsNotNullOrEmpty())
                                                {
                                                    if (activeFlagStaging.ToInteger() == 1)
                                                    {
                                                        patientData.Status = 1;
                                                    }
                                                    else
                                                    {
                                                        patientData.Status = 2;
                                                    }
                                                }
                                            }
                                            //if (columnvalue.PatientStatusFromXLS != default(int) || (columnvalue.FirstColumn.Equals("PatientStatusFromXLS") && columnvalue.PatientStatusFromXLS != null))
                                            //{
                                            //    var statusStaging = dataRow.GetValue(columnvalue.PatientStatusFromXLS);
                                            //    if (statusStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        if (statusStaging.Trim().ToLower().Equals("discharged"))
                                            //        {
                                            //            patientData.Status = 2;
                                            //        }
                                            //        else if (statusStaging.Trim().ToLower().Equals("admit"))
                                            //        {
                                            //            patientData.Status = 1;
                                            //        }
                                            //        else if (statusStaging.Trim().ToLower().Equals("pre-admit"))
                                            //        {
                                            //            patientData.Status = 4;
                                            //        }
                                            //        else
                                            //        {
                                            //            patientData.Status = 3;
                                            //        }
                                            //    }
                                            //}
                                            #endregion
                                            #region InsuranceComment
                                            //var insuranceStaging = dataRow.GetValue(columnvalue.PayerName);
                                            //if (insuranceStaging.IsNotNullOrEmpty())
                                            //{
                                            //    patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;
                                            //    var insuranceNumber = dataRow.GetValue(columnvalue.PatientHICNumber);
                                            //    if (insuranceNumber.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Insurance Number: " + insuranceNumber + CommentsDelimiter;
                                            //    }
                                            //}
                                            columnvalue.CommentName = "Insurance Name:"; columnvalue.CommentNum = columnvalue.PayerName; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            columnvalue.CommentName = "Insurance Number:"; columnvalue.CommentNum = columnvalue.PatientHICNumber; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            columnvalue.CommentName = "Physician NPI:"; columnvalue.CommentNum = columnvalue.PhysicianNPI; patientData.Comments += (dataRow.GetValue(columnvalue.CommentNum).IsNotNullOrEmpty() ? columnvalue.CommentName + dataRow.GetValue(columnvalue.CommentNum) : string.Empty);
                                            columnvalue.CommentName = "Physician Name"; columnvalue.CommentNum = columnvalue.PhysicianFName; patientData.Comments += columnvalue.CommentName + (dataRow.GetValue(columnvalue.PhysicianFName).IsNotNullOrEmpty() ? dataRow.GetValue(columnvalue.PhysicianFName) : string.Empty) + (dataRow.GetValue(columnvalue.PhysicianLName).IsNotNullOrEmpty() ? dataRow.GetValue(columnvalue.PhysicianLName) : string.Empty);
                                            columnvalue.CommentName = "Physician Address:"; columnvalue.CommentNum = columnvalue.PhysicianNPI; patientData.Comments += columnvalue.CommentName + (dataRow.GetValue(columnvalue.PhysicianAddr).IsNotNullOrEmpty() ? dataRow.GetValue(columnvalue.PhysicianAddr) : string.Empty) + (dataRow.GetValue(columnvalue.PhysicianCity).IsNotNullOrEmpty() ? dataRow.GetValue(columnvalue.PhysicianCity) : string.Empty) + (dataRow.GetValue(columnvalue.PhysicianState).IsNotNullOrEmpty() ? dataRow.GetValue(columnvalue.PhysicianState) : string.Empty) + (dataRow.GetValue(columnvalue.PhysicianZip).IsNotNullOrEmpty() && dataRow.GetValue(columnvalue.PhysicianZip).Length >= 5 ? dataRow.GetValue(columnvalue.PhysicianZip).Substring(0, 5) : string.Empty);
                                            #endregion

                                            #region Comment
                                            #region SSN
                                            //if (columnvalue.SSN != default(int) || (columnvalue.FirstColumn.Equals("SSN") && columnvalue.SSN != null))
                                            //{
                                            //    var SSNStaging = dataRow.GetValue(columnvalue.SSN);
                                            //    if (SSNStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.SSN = SSNStaging;
                                            //    }
                                            //}
                                            #endregion
                                            #region Gender
                                            //if (columnvalue.Gender != default(int) || (columnvalue.FirstColumn.Equals("Gender") && columnvalue.Gender != null))
                                            //{
                                            //    var genderStaging = dataRow.GetValue(columnvalue.Gender);
                                            //    if (genderStaging.IsNotNullOrEmpty() && genderStaging.Length >= 1)
                                            //    {
                                            //        patientData.Gender = genderStaging.ToUpper().StartsWith("F") ? "Female" : genderStaging.ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    patientData.Gender = "";
                                            //}
                                            #endregion
                                            #region Medicaid
                                            //if (columnvalue.MedicaidNumber != default(int) || (columnvalue.FirstColumn.Equals("MedicaidNumber") && columnvalue.MedicaidNumber != null))
                                            //{
                                            //    var medicaidNumberStaging = dataRow.GetValue(columnvalue.MedicaidNumber);
                                            //    if (medicaidNumberStaging.IsNotNullOrEmpty())
                                            //    {

                                            //        patientData.MedicaidNumber = medicaidNumberStaging;
                                            //        primaryInsurancePayor = true;
                                            //    }
                                            //}
                                            #endregion
                                            #region MaritalStatus
                                            //if (columnvalue.MaritalStatus != default(int) || (columnvalue.FirstColumn.Equals("MaritalStatus") && columnvalue.MaritalStatus != null))
                                            //{
                                            //    var maritalStatus = dataRow.GetValue(columnvalue.MaritalStatus);
                                            //    if (maritalStatus.IsNotNullOrEmpty())
                                            //    {
                                            //        maritalStatus = maritalStatus.ToUpperCase();
                                            //        if (maritalStatus.StartsWith("M"))
                                            //        {
                                            //            patientData.MaritalStatus = "Married";
                                            //        }
                                            //        else if (maritalStatus.StartsWith("S"))
                                            //        {
                                            //            patientData.MaritalStatus = "Single";
                                            //        }
                                            //        else if (maritalStatus.StartsWith("W"))
                                            //        {
                                            //            patientData.MaritalStatus = "Widowed";
                                            //        }
                                            //        else if (maritalStatus.StartsWith("D"))
                                            //        {
                                            //            patientData.MaritalStatus = "Divorced";
                                            //        }
                                            //        else
                                            //        {
                                            //            patientData.MaritalStatus = "Unknown";
                                            //        }
                                            //    }
                                            //    else
                                            //    {
                                            //        patientData.MaritalStatus = "Unknown";
                                            //    }
                                            //    sb.Append("MaritalStatus"); sb.Append(": "); sb.Append(patientData.MaritalStatus); sb.Append("\t\t");
                                            //}
                                            #endregion
                                            #region EmergencyContact
                                            #region ECName
                                            //if (columnvalue.EmergencyContactName != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactName") && columnvalue.EmergencyContactName != null))
                                            //{
                                            //    var emergencyContactNameStaging = dataRow.GetValue(columnvalue.EmergencyContactName);
                                            //    if (emergencyContactNameStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "[Emergency Contact Name: " + emergencyContactNameStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region ECRelationship
                                            //if (columnvalue.EmergencyContactRelationship != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactRelationship") && columnvalue.EmergencyContactRelationship != null))
                                            //{
                                            //    var emergencyContactRelationshipStaging = dataRow.GetValue(columnvalue.EmergencyContactRelationship);
                                            //    if (emergencyContactRelationshipStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Relationship: " + emergencyContactRelationshipStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region ECPhone
                                            //if (columnvalue.EmergencyContactPhone != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactPhone") && columnvalue.EmergencyContactPhone != null))
                                            //{
                                            //    var emergencyContactPhoneStaging = dataRow.GetValue(columnvalue.EmergencyContactPhone);
                                            //    if (emergencyContactPhoneStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Phone: " + emergencyContactPhoneStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region ECAddress
                                            //if (columnvalue.EmergencyContactAddress != default(int) || (columnvalue.FirstColumn.Equals("EmergencyContactAddress") && columnvalue.EmergencyContactAddress != null))
                                            //{
                                            //    var emergencyContactAddressStaging = dataRow.GetValue(columnvalue.EmergencyContactAddress);
                                            //    if (emergencyContactAddressStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Address: " + emergencyContactAddressStaging + "]" + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #endregion
                                            #region Pharmacy
                                            #region PharmacyName
                                            //if (columnvalue.Pharmacy != default(int) || (columnvalue.FirstColumn.Equals("PharmacyName") && columnvalue.Pharmacy != null))
                                            //{
                                            //    var pharmacyStaging = dataRow.GetValue(columnvalue.Pharmacy);
                                            //    if (pharmacyStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.PharmacyName = pharmacyStaging;
                                            //    }
                                            //}
                                            #endregion
                                            #region PharmacyPhone
                                            //if (columnvalue.PharmacyPhone != default(int) || (columnvalue.FirstColumn.Equals("PharmacyPhone") && columnvalue.PharmacyPhone != null))
                                            //{
                                            //    var pharmacyPhoneStaging = dataRow.GetValue(columnvalue.PharmacyPhone);
                                            //    if (pharmacyPhoneStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.PharmacyPhone = pharmacyPhoneStaging.ToPhoneDB();
                                            //    }
                                            //}
                                            #endregion
                                            #endregion
                                            #region SecondaryDiagnosis
                                            //if (columnvalue.SecondaryDiagnosis != default(int))
                                            //{
                                            //    var secondaryDiagStaging = dataRow.GetValue(columnvalue.SecondaryDiagnosis);
                                            //    if (secondaryDiagStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Secondary Diagnosis: " + secondaryDiagStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region Triage
                                            //if (columnvalue.Triage != default(int) || (columnvalue.FirstColumn.Equals("Triage") && columnvalue.Triage != null))
                                            //{
                                            //    var triageStaging = dataRow.GetValue(columnvalue.Triage);
                                            //    if (triageStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Triage = Int32.Parse(triageStaging);
                                            //    }
                                            //    sb.Append("Triage"); sb.Append(": "); sb.Append(patientData.Triage); sb.Append("\t\t");
                                            //}
                                            #endregion
                                            #region Therapist
                                            //if (columnvalue.Therapist != default(int) || (columnvalue.FirstColumn.Equals("Therapist") && columnvalue.Therapist != null))
                                            //{
                                            //    var therapistStaging = dataRow.GetValue(columnvalue.Therapist);
                                            //    if (therapistStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Therapist: " + therapistStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region CaseManager
                                            //if (columnvalue.CaseManager != default(int) || (columnvalue.FirstColumn.Equals("CaseManager") && columnvalue.CaseManager != null))
                                            //{
                                            //    var caseManagerStaging = dataRow.GetValue(columnvalue.CaseManager);
                                            //    if (caseManagerStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "Case Manager: " + caseManagerStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region ExternalReferral
                                            //if (columnvalue.ExternalReferral != default(int) || (columnvalue.FirstColumn.Equals("externalReferral_col") && columnvalue.ExternalReferral != null))
                                            //{
                                            //    var externalReferralStaging = dataRow.GetValue(columnvalue.ExternalReferral);
                                            //    if (externalReferralStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        patientData.Comments += "External Referral: " + externalReferralStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #region Disciplines
                                            //if (columnvalue.EpisodeFreq != default(int) || (columnvalue.FirstColumn.Equals("EpisodeFreq") && columnvalue.EpisodeFreq != null))
                                            //{
                                            //    StringBuilder disciplineSB = new StringBuilder();
                                            //    var DisciplinesStaging = dataRow.GetValue(columnvalue.EpisodeFreq);
                                            //    if (DisciplinesStaging.IsNotNullOrEmpty())
                                            //    {
                                            //        var DisciplinesArray = DisciplinesStaging.Split(',');
                                            //        if (DisciplinesArray != null && DisciplinesArray.Length > 0)
                                            //        {
                                            //            foreach (var discipline in DisciplinesArray)
                                            //            { 
                                            //                if(discipline.IsNotNullOrEmpty())
                                            //                {
                                            //                    disciplineSB.Append(discipline.Trim().Length >=3 ? discipline.Trim().Substring(0, 3).Trim() : (discipline.Trim().Length >= 2 ? discipline.Trim().Substring(0, 2) : string.Empty));
                                            //                }
                                            //            }
                                            //        }
                                            //        patientData.Comments += "Disciplines: " + DisciplinesStaging + CommentsDelimiter;
                                            //    }
                                            //}
                                            #endregion
                                            #endregion
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;
                                            bool checkExists = default(bool);
                                            Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, columnvalue.agencyId);
                                            if (patientExisting != null)
                                            {
                                                checkExists = true;
                                            }
                                            if (checkExists != true)
                                            {
                                                #region DBWrite
                                                var medicationProfile = new MedicationProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = columnvalue.agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = columnvalue.agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };

                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = columnvalue.agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, columnvalue.agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                var exists = true;
                                                                #region physicianDBWrite
                                                                var npi = dataRow.GetValue(columnvalue.PhysicianNPI);
                                                                if (npi.IsNotNullOrEmpty())
                                                                {
                                                                    var physician = Database.GetPhysician(npi, columnvalue.agencyId);
                                                                    if (physician == null)
                                                                    {
                                                                        exists = false;
                                                                        var info = Database.GetNpiData(npi);
                                                                        if (info != null)
                                                                        {
                                                                            physician = new AgencyPhysician
                                                                            {
                                                                                Id = Guid.NewGuid(),
                                                                                AgencyId = columnvalue.agencyId,
                                                                                NPI = npi,
                                                                                LoginId = Guid.Empty,
                                                                                AddressLine1 = string.Empty,
                                                                                AddressCity = string.Empty,
                                                                                AddressStateCode = string.Empty,
                                                                                AddressZipCode = string.Empty,
                                                                                PhoneWork = string.Empty,
                                                                                FaxNumber = string.Empty,
                                                                                Credentials = string.Empty
                                                                            };
                                                                            #region Address1
                                                                            if (columnvalue.PhysicianAddr != default(int))
                                                                            {
                                                                                var mdAddr1Staging = dataRow.GetValue(columnvalue.PhysicianAddr);
                                                                                if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.AddressLine1 = mdAddr1Staging;
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region City
                                                                            if (columnvalue.PhysicianCity != default(int))
                                                                            {
                                                                                var mdCityStaging = dataRow.GetValue(columnvalue.PhysicianCity);
                                                                                if (mdCityStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.AddressCity = mdCityStaging;
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region State
                                                                            if (columnvalue.PhysicianState != default(int))
                                                                            {
                                                                                var mdStateStaging = dataRow.GetValue(columnvalue.PhysicianState);
                                                                                if (mdStateStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.AddressStateCode = mdStateStaging;
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region Zip
                                                                            if (columnvalue.PhysicianZip != default(int))
                                                                            {
                                                                                var mdZipStaging = dataRow.GetValue(columnvalue.PhysicianZip);
                                                                                if (mdZipStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.AddressZipCode = mdZipStaging.Length >= 5 ? mdZipStaging.Trim().Substring(0,5) : "";
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region Phone
                                                                            if (columnvalue.PhysicianPhone != default(int))
                                                                            {
                                                                                var mdPhoneStaging = dataRow.GetValue(columnvalue.PhysicianPhone);
                                                                                if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.PhoneWork = mdPhoneStaging.ToPhoneDB();
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region Fax
                                                                            if (columnvalue.PhysicianFax != default(int))
                                                                            {
                                                                                var mdFaxStaging = dataRow.GetValue(columnvalue.PhysicianFax);
                                                                                if (mdFaxStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.FaxNumber = mdFaxStaging.ToPhoneDB();
                                                                                }
                                                                            }
                                                                            #endregion
                                                                            #region Name
                                                                            if (columnvalue.PhysicianLName != default(int))
                                                                            {
                                                                                var physicianLNameStaging = dataRow.GetValue(columnvalue.PhysicianLName);
                                                                                if (physicianLNameStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.LastName = physicianLNameStaging;
                                                                                }
                                                                            }
                                                                            if (columnvalue.PhysicianFName != default(int))
                                                                            {
                                                                                var physicianFNameStaging = dataRow.GetValue(columnvalue.PhysicianFName);
                                                                                if (physicianFNameStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physician.FirstName = physicianFNameStaging;
                                                                                }
                                                                            }
                                                                            //var physicianName = dataRow.GetValue(columnvalue.PhysicianName);
                                                                            //if (physicianName.IsNotNullOrEmpty())
                                                                            //{
                                                                            //    var physicianNameArray = physicianName.Trim().Split(new char[] { columnvalue.MDSplitChar }, StringSplitOptions.RemoveEmptyEntries);
                                                                            //    if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                            //    {
                                                                            //        if (physicianNameArray.Length == 3)
                                                                            //        {
                                                                            //            physician.FirstName = physicianNameArray[columnvalue.MDSUBFirst].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBFirst].Trim() : info.ProviderFirstName;
                                                                            //            physician.MiddleName = physicianNameArray[columnvalue.MDSUBMiddle].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBMiddle].Trim() : info.ProviderMiddleName;
                                                                            //            physician.LastName = physicianNameArray[columnvalue.MDSUBLast].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBLast].Trim() : info.ProviderLastName;
                                                                            //        }
                                                                            //        else
                                                                            //        {
                                                                            //            if (columnvalue.MDSUBFirst > columnvalue.MDSUBLast) { columnvalue.MDSUBFirst = columnvalue.MDSUBFirst - 1; }
                                                                            //            else if (columnvalue.MDSUBLast > columnvalue.MDSUBFirst) { columnvalue.MDSUBLast = columnvalue.MDSUBLast - 1; }
                                                                            //            physician.FirstName = physicianNameArray[columnvalue.MDSUBFirst].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBFirst].Trim() : info.ProviderFirstName;
                                                                            //            physician.LastName = physicianNameArray[columnvalue.MDSUBLast].IsNotNullOrEmpty() ? physicianNameArray[columnvalue.MDSUBLast].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                            //        }
                                                                            //    }
                                                                            //    else
                                                                            //    {
                                                                            //        physician.LastName = info.ProviderLastName;
                                                                            //        physician.FirstName = info.ProviderFirstName;
                                                                            //    }
                                                                            //}
                                                                            //else
                                                                            //{
                                                                            //    physician.LastName = info.ProviderLastName;
                                                                            //    physician.FirstName = info.ProviderFirstName;
                                                                            //}
                                                                            #endregion
                                                                        }
                                                                        Database.Add(physician);
                                                                    }
                                                                    if (physician != null)
                                                                    {
                                                                        var patientPhysician = new PatientPhysician
                                                                        {
                                                                            IsPrimary = true,
                                                                            PatientId = patientData.Id,
                                                                            PhysicianId = physician.Id
                                                                        };
                                                                        if (Database.Add(patientPhysician))
                                                                        {
                                                                            Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "PHYSICIAN ALREADY EXISTS" : "");
                                                                        }
                                                                    }
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("\nTotal Patients:" + totalpatients);
                    Console.WriteLine("\nTotal Counted:" + totalCounter);
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}