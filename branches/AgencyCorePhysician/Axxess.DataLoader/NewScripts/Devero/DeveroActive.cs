﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public class DeveroColumnActive
    {
        
        public int Name { get; set; }
        public int IDNumber { get; set; }
        public int DOB { get; set; }
        public int SSN { get; set; }
        public int Address { get; set; }
        public int Phone { get; set; }
        public int AdmissionDate { get; set; }
        public int PhysicianAndPhone { get; set; }
        public int PrimaryInsurance { get; set; }
        public int DischargeDate { get; set; }
        public int COTA { get; set; }
        public int HHA { get; set; }
        public int LVN { get; set; }
        public int MSW { get; set; }
        public int RN { get; set; }
        public int OT { get; set; }
        public int PT { get; set; }
        public int PTA { get; set; }
        public int ST { get; set; }

        public bool DBWritePermission { get; set; }
        public bool AssociatePhysician { get; set; }

        public Guid AgencyId { get; set; }
        public Guid LocationId { get; set; }

        public int patientStatus { get; set; }
        public string ServiceLocation { get; set; }

        public string defaultInsurance { get; set; }
        public string defaultPayor { get; set; }
        public bool useDefaultInsuranceValues { get; set; }

        public string FirstColumn { get; set; }
    }

    public static class DeveroActive
    {
        private static string input = Path.Combine(App.Root, "Files\\elsmar-active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\elsmar-active-event-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\elsmar-active-data-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            DeveroColumnActive columnvalue = new DeveroColumnActive 
            {
                Name = 0,
                IDNumber = 1,
                DOB = 2,
                SSN = 3,
                Address = 4,
                Phone = 5,
                AdmissionDate = 6,
                PhysicianAndPhone = 7,
                PrimaryInsurance = 8,
                DischargeDate = 9,
                COTA = 10,
                HHA = 11,
                LVN = 12,
                MSW = 13,
                RN = 14,
                OT = 15,
                PT = 16,
                PTA = 17,
                ST = 18,
                DBWritePermission = true,
                AssociatePhysician = false,
                AgencyId = agencyId,
                LocationId = locationId,
                patientStatus = 1,
                ServiceLocation = "Q5001",
                defaultInsurance = "MEDICARE",
                defaultPayor = "5"
            };

            foreach (var item in typeof(PromptColumnPhysician).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }

            Process(columnvalue);
        }

        public static void Process(DeveroColumnActive column)
        {
            using (TextWriter errorWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                            {
                                if (excelReader != null && excelReader.IsValid)
                                {
                                    excelReader.IsFirstRowAsColumnNames = true;
                                    DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                    if (dataTable != null && dataTable.Rows.Count > 0)
                                    {
                                        var i = 1;
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!dataRow.IsEmpty())
                                            {
                                                StringBuilder logOutput = new StringBuilder();
                                                StringBuilder commentOutput = new StringBuilder();
                                                var patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = column.AgencyId;
                                                patientData.AgencyLocationId = column.LocationId;
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                patientData.Status = column.patientStatus;
                                                patientData.ServiceLocation = column.ServiceLocation;
                                                patientData.Gender = "";

                                                #region Name
                                                if (column.Name != default(int) || (column.FirstColumn.Equals("Name") && column.Name != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.Name);
                                                    var Array = Staging.Split(',');
                                                    if (Array != null && Array.Length > 0)
                                                    {
                                                        if (Array.Length > 1)
                                                        {
                                                            var naamArray1 = Array[1].TrimStart(' ').Split(' ');
                                                            if (naamArray1.Length > 1)
                                                            {
                                                                patientData.MiddleInitial = naamArray1[1];
                                                                patientData.FirstName = naamArray1[0];
                                                                logOutput.Append("MiddleInitial"); logOutput.Append(": "); logOutput.Append(patientData.MiddleInitial); logOutput.Append("\t\t");
                                                            }
                                                            else
                                                            {
                                                                patientData.FirstName = Array[1];
                                                            }
                                                            patientData.LastName = Array[0];
                                                        }
                                                    }
                                                    logOutput.Append("FirstName"); logOutput.Append(": "); logOutput.Append(patientData.FirstName); logOutput.Append("\t\t");
                                                    logOutput.Append("LastName"); logOutput.Append(": "); logOutput.Append(patientData.LastName); logOutput.Append("\t\t");
                                                }
                                                #endregion
                                                #region IDNumber
                                                if (column.IDNumber != default(int) || (column.FirstColumn.Equals("IDNumber") && column.IDNumber != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.IDNumber);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.PatientIdNumber = Staging;
                                                        logOutput.Append("PatientIdNumber"); logOutput.Append(": "); logOutput.Append(patientData.PatientIdNumber); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region DOB
                                                if (column.DOB != default(int) || (column.FirstColumn.Equals("DOB") && column.DOB != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.DOB);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.IsDouble())
                                                        {
                                                            patientData.DOB = DateTime.FromOADate(double.Parse(Staging));
                                                        }
                                                        else
                                                        {
                                                            patientData.DOB = DateTime.Parse(Staging);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.MinValue;
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }
                                                logOutput.Append("DOB"); logOutput.Append(": "); logOutput.Append(patientData.DOB); logOutput.Append("\t\t");
                                                #endregion
                                                #region SSN
                                                if (column.SSN != default(int) || (column.FirstColumn.Equals("ssn_col") && column.SSN != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.SSN).Replace(" ", "");
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.SSN = Staging;
                                                        logOutput.Append("SSN"); logOutput.Append(": "); logOutput.Append(patientData.SSN); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region Address
                                                if (column.Address != default(int) || (column.FirstColumn.Equals("Address") && column.Address != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.Address);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        var addressArray = Staging.Split(',');
                                                        if (addressArray.Length >= 2)
                                                        {
                                                            var statecode = addressArray[1].Trim();
                                                            patientData.AddressStateCode = (statecode.Split(' '))[0].Trim();
                                                            patientData.AddressZipCode = (statecode.Split(' '))[1].Trim();
                                                            int end = addressArray[0].LastIndexOf(' ') + 1;
                                                            patientData.AddressCity = addressArray[0].Substring(end);
                                                            patientData.AddressLine1 = addressArray[0].Substring(0, end - 1).Trim();
                                                        }
                                                    }
                                                }
                                                #endregion
                                                #region Phone
                                                if (column.Phone != default(int) || (column.FirstColumn.Equals("Phone") && column.Phone != null))
                                                {
                                                    var Staging = dataRow.GetValue(column.Phone);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.PhoneHome = Staging.IsNotNullOrEmpty() ? Staging.ToPhoneDB() : string.Empty;
                                                        logOutput.Append("PhoneHome"); logOutput.Append(": "); logOutput.Append(patientData.PhoneHome); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region AdmissionDate
                                                if (column.AdmissionDate != default(int) || (column.FirstColumn.Equals("AdmissionDate") && column.AdmissionDate != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.AdmissionDate);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.IsDouble())
                                                        {
                                                            ForComment = DateTime.FromOADate(double.Parse(Staging)).ToShortDateString();
                                                        }
                                                        else
                                                        {
                                                            ForComment = DateTime.Parse(Staging).ToShortDateString();
                                                        }
                                                        logOutput.Append("Admission Date"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        commentOutput.Append("Admission Date"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region PhysicianNPhone
                                                if (column.PhysicianAndPhone != default(int) || (column.FirstColumn.Equals("PhysicianAndPhone") && column.PhysicianAndPhone != null))
                                                {
                                                    var PhysicianPhone = default(string);
                                                    var PhysicianName = default(string);
                                                    var Staging = dataRow.GetValue(column.PhysicianAndPhone);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        var Array = Staging.Split(':');
                                                        if (Array.Length >= 2)
                                                        {
                                                            PhysicianPhone = Array[1].TrimStart().TrimEnd().Trim().IsNotNullOrEmpty() ? Array[1].TrimStart().TrimEnd().Trim() : string.Empty;
                                                            PhysicianName = Array[0].TrimStart().TrimEnd().Trim().IsNotNullOrEmpty() ? Array[1].TrimStart().TrimEnd().Trim() : string.Empty;
                                                            PhysicianName.Replace("(","");
                                                            PhysicianName.Replace("MD","");
                                                            PhysicianName.Replace(")","");
                                                        }
                                                        commentOutput.Append("Physician Name"); commentOutput.Append(": "); commentOutput.Append(PhysicianName); commentOutput.Append("\t\t");
                                                        commentOutput.Append("Physician Phone"); commentOutput.Append(": "); commentOutput.Append(PhysicianPhone); commentOutput.Append("\t\t");
                                                        logOutput.Append("Physician Name"); logOutput.Append(": "); logOutput.Append(PhysicianName); logOutput.Append("\t\t");
                                                        logOutput.Append("Physician Phone"); logOutput.Append(": "); logOutput.Append(PhysicianPhone); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region PrimaryInsurance
                                                if (column.PrimaryInsurance != default(int) || (column.FirstColumn.Equals("PrimaryInsurance") && column.PrimaryInsurance != null))
                                                {
                                                    var PrimaryInsuranceForComment = default(string);
                                                    var PayorForComment = default(string);
                                                    var PrimaryInsuranceStaging = dataRow.GetValue(column.PrimaryInsurance);
                                                    if (PrimaryInsuranceStaging.IsNotNullOrEmpty())
                                                    {
                                                        if (PrimaryInsuranceStaging.ToUpper().Contains("MEDICARE"))
                                                        {
                                                            patientData.PrimaryInsurance = column.defaultPayor;
                                                        }
                                                        else
                                                        {
                                                            if (column.useDefaultInsuranceValues == default(bool))
                                                            {
                                                                PrimaryInsuranceForComment = PrimaryInsuranceStaging;
                                                                PayorForComment = column.defaultPayor;
                                                            }
                                                            else
                                                            {
                                                                PrimaryInsuranceForComment = column.defaultInsurance;
                                                                PayorForComment = column.defaultPayor;
                                                            }
                                                        }
                                                        commentOutput.Append("Primary Insurance"); commentOutput.Append(": "); commentOutput.Append(PrimaryInsuranceForComment); commentOutput.Append("\t\t");
                                                        commentOutput.Append("Agency Payor"); commentOutput.Append(": "); commentOutput.Append(PayorForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("Primary Insurance"); logOutput.Append(": "); logOutput.Append(PrimaryInsuranceForComment); logOutput.Append("\t\t");
                                                        logOutput.Append("Agency Payor"); logOutput.Append(": "); logOutput.Append(PayorForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region DischargeDate
                                                if (column.DischargeDate != default(int) || (column.FirstColumn.Equals("DischargeDate") && column.DischargeDate != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.DischargeDate);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        if (Staging.IsDouble())
                                                        {
                                                            ForComment = DateTime.FromOADate(double.Parse(Staging)).ToShortDateString();
                                                            patientData.DischargeDate = DateTime.FromOADate(double.Parse(Staging));
                                                        }
                                                        else
                                                        {
                                                            ForComment = DateTime.Parse(Staging).ToShortDateString();
                                                            patientData.DischargeDate = DateTime.Parse(Staging);
                                                        }
                                                        logOutput.Append("Discharge Date"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region COTA
                                                if (column.COTA != default(int) || (column.FirstColumn.Equals("COTA") && column.COTA != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.COTA);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("COTA"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("COTA"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region HHA
                                                if (column.HHA != default(int) || (column.FirstColumn.Equals("HHA") && column.HHA != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.HHA);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("HHA"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("HHA"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region LVN
                                                if (column.LVN != default(int) || (column.FirstColumn.Equals("LVN") && column.LVN != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.LVN);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("LVN"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("LVN"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region MSW
                                                if (column.MSW != default(int) || (column.FirstColumn.Equals("MSW") && column.MSW != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.MSW);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("MSW"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("MSW"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region RN
                                                if (column.RN != default(int) || (column.FirstColumn.Equals("RN") && column.RN != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.RN);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("RN"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("RN"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region OT
                                                if (column.OT != default(int) || (column.FirstColumn.Equals("OT") && column.OT != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.OT);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("OT"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("OT"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region PT
                                                if (column.PT != default(int) || (column.FirstColumn.Equals("PT") && column.PT != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.PT);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("PT"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("PT"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region PTA
                                                if (column.PTA != default(int) || (column.FirstColumn.Equals("PTA") && column.PTA != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.PTA);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("PTA"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("PTA"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion
                                                #region ST
                                                if (column.ST != default(int) || (column.FirstColumn.Equals("ST") && column.ST != null))
                                                {
                                                    var ForComment = default(string);
                                                    var Staging = dataRow.GetValue(column.ST);
                                                    if (Staging.IsNotNullOrEmpty())
                                                    {
                                                        ForComment = Staging.Trim();
                                                        commentOutput.Append("ST"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                        logOutput.Append("ST"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                    }
                                                }
                                                #endregion

                                                patientData.Comments = commentOutput.ToString();

                                                #region DBWrite
                                                if (column.DBWritePermission == true)
                                                {
                                                    bool checkExists = default(bool);
                                                    Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, column.AgencyId);

                                                    if (patientExisting != null)
                                                    {
                                                        checkExists = true;
                                                    }

                                                    if (checkExists != true)
                                                    {
                                                        var medicationProfile = new MedicationProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Medication = "<ArrayOfMedication />"
                                                        };

                                                        var allergyProfile = new AllergyProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Allergies = "<ArrayOfAllergy />"
                                                        };

                                                        if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                        {
                                                            var admissionPeriod = new PatientAdmissionDate
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = column.AgencyId,
                                                                Created = DateTime.Now,
                                                                DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                                IsActive = true,
                                                                IsDeprecated = false,
                                                                Modified = DateTime.Now,
                                                                PatientData = patientData.ToXml().Replace("'", ""),
                                                                PatientId = patientData.Id,
                                                                Reason = string.Empty,
                                                                StartOfCareDate = patientData.StartofCareDate,
                                                                Status = patientData.Status
                                                            };
                                                            if (Database.Add(admissionPeriod))
                                                            {
                                                                var patient = Database.GetPatient(patientData.Id, column.AgencyId);
                                                                if (patient != null)
                                                                {
                                                                    patient.AdmissionId = admissionPeriod.Id;
                                                                    if (Database.Update(patient))
                                                                    {
                                                                        Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                        var exists = true;
                                                                        #region AssociatePhysician
                                                                        //if (column.AssociatePhysician == true)
                                                                        //{
                                                                        //    var npi = dataRow.GetValue(mdNPI_col);
                                                                        //    if (npi.IsNotNullOrEmpty())
                                                                        //    {
                                                                        //        var physician = Database.GetPhysician(npi, agencyId);
                                                                        //        if (physician == null)
                                                                        //        {
                                                                        //            exists = false;
                                                                        //            var info = Database.GetNpiData(npi);
                                                                        //            if (info != null)
                                                                        //            {
                                                                        //                physician = new AgencyPhysician
                                                                        //                {
                                                                        //                    Id = Guid.NewGuid(),
                                                                        //                    AgencyId = column.agencyId,
                                                                        //                    NPI = npi,
                                                                        //                    LoginId = Guid.Empty,
                                                                        //                    AddressLine1 = string.Empty,
                                                                        //                    AddressCity = string.Empty,
                                                                        //                    AddressStateCode = string.Empty,
                                                                        //                    AddressZipCode = string.Empty,
                                                                        //                    PhoneWork = string.Empty,
                                                                        //                    FaxNumber = string.Empty,
                                                                        //                    Credentials = string.Empty,
                                                                        //                    FirstName = dataRow.GetValue(md_fn_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_fn_col) : string.Empty,
                                                                        //                    LastName = dataRow.GetValue(md_ln_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_ln_col) : string.Empty
                                                                        //                };
                                                                        //                #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                        //                if (mdAddr1_col != default(int))
                                                                        //                {
                                                                        //                    var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                        //                    if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressLine1 = mdAddr1Staging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdCity_col != default(int))
                                                                        //                {
                                                                        //                    var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                        //                    if (mdCityStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressCity = mdCityStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdState_col != default(int))
                                                                        //                {
                                                                        //                    var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                        //                    if (mdStateStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressStateCode = mdStateStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdZip_col != default(int))
                                                                        //                {
                                                                        //                    var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                        //                    if (mdZipStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.AddressZipCode = mdZipStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdPhone_col != default(int))
                                                                        //                {
                                                                        //                    var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                        //                    if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.PhoneWork = mdPhoneStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdFax_col != default(int))
                                                                        //                {
                                                                        //                    var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                        //                    if (mdFaxStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.FaxNumber = mdFaxStaging;
                                                                        //                    }
                                                                        //                }

                                                                        //                if (mdCred_col != default(int))
                                                                        //                {
                                                                        //                    var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                        //                    if (mdCredStaging.IsNotNullOrEmpty())
                                                                        //                    {
                                                                        //                        physician.Credentials = mdCredStaging;
                                                                        //                    }
                                                                        //                }
                                                                        //                #endregion
                                                                        //                var physicianName = dataRow.GetValue(mdName_col);
                                                                        //                if (physicianName.IsNotNullOrEmpty())
                                                                        //                {
                                                                        //                    var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                        //                    if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                        //                    {
                                                                        //                        if (physicianNameArray.Length == 3)
                                                                        //                        {
                                                                        //                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                        //                            physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                        //                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                        //                        }
                                                                        //                        else
                                                                        //                        {
                                                                        //                            physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                        //                            physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                        //                        }
                                                                        //                    }
                                                                        //                    else
                                                                        //                    {
                                                                        //                        physician.LastName = info.ProviderLastName;
                                                                        //                        physician.FirstName = info.ProviderFirstName;
                                                                        //                    }
                                                                        //                }
                                                                        //                else
                                                                        //                {
                                                                        //                    physician.LastName = info.ProviderLastName;
                                                                        //                    physician.FirstName = info.ProviderFirstName;
                                                                        //                }
                                                                        //            }
                                                                        //            Database.Add(physician);
                                                                        //        }
                                                                        //        if (physician != null)
                                                                        //        {
                                                                        //            var patientPhysician = new PatientPhysician
                                                                        //            {
                                                                        //                IsPrimary = true,
                                                                        //                PatientId = patientData.Id,
                                                                        //                PhysicianId = physician.Id
                                                                        //            };
                                                                        //            if (Database.Add(patientPhysician))
                                                                        //            {
                                                                        //                Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                        //            }
                                                                        //        }
                                                                        //    }
                                                                        //}
                                                                        #endregion
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                                    }
                                                }
                                                #endregion

                                                logWriter.WriteLine(logOutput.ToString());
                                                logOutput.Length = 0;
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorWriter.WriteLine(ex.StackTrace);
                }
            }
        }

    }
}
