﻿namespace Axxess.DataLoader.Columns
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class CradleActiveColumn
    {
        public int PatientHICNumber { get; set; }//
        public int SOCDate { get; set; }//
        public int EpisodeFrom { get; set; }//
        public int EpisodeTo { get; set; }//
        public int PatientMRN { get; set; }//
        public int PatientLastName { get; set; }//
        public int PatientFirstName { get; set; }//
        public int PatientAddress { get; set; }//
        public int PatientCity { get; set; }//
        public int PatientState { get; set; }//
        public int PatientZip { get; set; }//
        public int PatientPhone { get; set; }//
        public int PatientDOB { get; set; }//
        public int EpisodeId { get; set; }//Nan
        public int EmergencyLevel { get; set; }
        public int EpisodeFreq { get; set; }
        public int PhysicianNPI { get; set; }
        public int PhysicianTitle { get; set; }//Nan
        public int PhysicianLName { get; set; }
        public int PhysicianFName { get; set; }
        public int PhysicianAddr { get; set; }
        public int PhysicianCity { get; set; }
        public int PhysicianState { get; set; }
        public int PhysicianZip { get; set; }
        public int PhysicianPhone { get; set; }
        public int PhysicianFax { get; set; }
        public int AssessorName { get; set; }
        public int AssessorSkill { get; set; }
        public int PrimDiagCode { get; set; }//
        public int PrimDiagDesc { get; set; }//
        public int ActiveFlag { get; set; }
        public int DischargeType { get; set; }
        public int DischargeDate { get; set; }
        public int DischargeReason { get; set; }
        public int PatientId { get; set; }
        public int PayerId { get; set; }
        public int PayerName { get; set; }
        public int PayerType { get; set; }
        public int ParamLocationId { get; set; }
        public int RFD { get; set; }
        public int AgencyName { get; set; }
        public int PrintedOn { get; set; }
        public int AgencyAddress { get; set; }
        public int AgencyPhone { get; set; }
        public int ReportHeader { get; set; }
        public int DateFrom { get; set; }
        public int DateTo { get; set; }
        public int Insurance { get; set; }

        public string CommentName { get; set; }
        public int CommentNum { get; set; }
        public Guid agencyId { get; set; }
        public Guid locationId { get; set; }
        public int MDSUBFirst { get; set; }
        public int MDSUBLast { get; set; }
        public int MDSUBMiddle { get; set; }
        public char MDSplitChar { get; set; }
        public string DefaultPayor { get; set; }
        public string FirstColumn { get; set; }
        public int PatientStatus { get; set; }
        public string ServiceLocation { get; set; }
        public string DefaultInsurance { get; set; }
    }
}
