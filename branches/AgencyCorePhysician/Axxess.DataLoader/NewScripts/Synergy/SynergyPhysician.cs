﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class SynergyPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\vnhomehealth-doctor.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\vnhomehealth-doctor-Eventlog-{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run(Guid agencyId)
        {           
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    int addedCounter = default(int), cannotAddCounter = default(int), skippedCounter = default(int), noNPICounter = default(int);
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if (dataRow.GetValue(14).IsNotNullOrEmpty() && dataRow.GetValue(14).IsInteger()) 
                                            {
                                                #region Process
                                                var npi = dataRow.GetValue(14);
                                                var physician = Database.GetPhysician(npi, agencyId);
                                                if (physician == null)
                                                {
                                                    var info = Database.GetNpiData(npi);
                                                    if (info != null)
                                                    {
                                                        physician = new AgencyPhysician
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            NPI = npi,
                                                            LoginId = Guid.Empty
                                                        };

                                                        #region Name
                                                        var oneNameStaging = dataRow.GetValue(1);
                                                        var nameArray = oneNameStaging.Split(',');
                                                        if (nameArray != null && nameArray.Length > 0)
                                                        {
                                                            if (nameArray.Length > 1)
                                                            {
                                                                var naamArray1 = nameArray[1].TrimStart(' ').Split(' ');
                                                                if (naamArray1.Length > 1)
                                                                {
                                                                    if (naamArray1[1].Trim().IsNullOrEmpty())
                                                                    {
                                                                        if (naamArray1.Length > 2)
                                                                        {
                                                                            if (naamArray1[2].Trim().IsNullOrEmpty())
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                physician.MiddleName = naamArray1[2];
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        physician.MiddleName = naamArray1[1];
                                                                    }
                                                                    physician.FirstName = naamArray1[0];
                                                                }
                                                                else
                                                                {
                                                                    physician.FirstName = nameArray[1];
                                                                }
                                                                physician.LastName = nameArray[0];
                                                            }
                                                        }
                                                        #endregion

                                                        #region Phone + Fax
                                                        var phoneWorkStaging = dataRow.GetValue(21);
                                                        if (phoneWorkStaging.Contains("H")) { phoneWorkStaging = phoneWorkStaging.Replace("H", "").TrimStart().TrimEnd(); }
                                                        if (phoneWorkStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.PhoneWork = phoneWorkStaging.ToPhoneDB();
                                                        }
                                                        #endregion

                                                        #region UPIN
                                                        if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                                        {
                                                            physician.UPIN = dataRow.GetValue(11);
                                                        }
                                                        #endregion

                                                        #region COMMENTS
                                                        physician.Comments += dataRow.GetValue(7).IsNotNullOrEmpty() ? " CODE: " + dataRow.GetValue(7).ToString() : string.Empty + ";";
                                                        physician.Comments += dataRow.GetValue(8).IsNotNullOrEmpty() ? " TITLE: " + dataRow.GetValue(8).ToString() : string.Empty + ";";
                                                        physician.Comments += dataRow.GetValue(11).IsNotNullOrEmpty() ? " UPIN: " + dataRow.GetValue(11).ToString() : string.Empty + ";";
                                                        physician.Comments += dataRow.GetValue(15).IsNotNullOrEmpty() ? " LICENSE: " + dataRow.GetValue(15).ToString() : string.Empty + ";";
                                                        physician.Comments += dataRow.GetValue(17).IsNotNullOrEmpty() ? " CLASSIFICATION: " + dataRow.GetValue(17).ToString() : string.Empty + ";";
                                                        physician.Comments += dataRow.GetValue(26).IsNotNullOrEmpty() ? " SEX: " + dataRow.GetValue(26).ToString() : string.Empty + ";";
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Physician Not Added - No NPI Info");
                                                        noNPICounter++;
                                                    }
                                                    if (Database.Add(physician))
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician Added: " + physician.LastName + ", " + physician.FirstName);
                                                        addedCounter++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician NOT Added: ");
                                                        cannotAddCounter++;
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("(" + i.ToString() + ")" + "Physician PRESENT: " + physician.LastName + ", " + physician.FirstName);
                                                    skippedCounter++;
                                                }
                                                #endregion
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("Finished Writing to Log File");
                    Console.WriteLine(string.Format("{0} : {1}","ADDED:",addedCounter));
                    Console.WriteLine(string.Format("{0} : {1}:","NOT ADDED:",cannotAddCounter));
                    Console.WriteLine(string.Format("{0} : {1}:","NO NPI:",noNPICounter));
                    Console.WriteLine(string.Format("{0} : {1}:", "SKIPPED:", skippedCounter));
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex);
                }
            }
        }
    }
}
