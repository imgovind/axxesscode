﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public static class UpdatePaymentsScript
    {
        public static void Run()
        {
            var agencies = Database.GetAgencies();
            var payments = Database.GetManagedClaimPayments();
            var paymentsToUpdate = new List<ManagedClaimPayment>();
            var paymentsToAdd = new List<ManagedClaimPayment>();
            Console.WriteLine("About to start Updating Payments. Press any key to continue.");
            Console.ReadLine();
            agencies.ForEach(agency =>
            {
                var claims = Database.GetPaidManagedClaims(agency.Id);
                if (claims != null && claims.Count > 0)
                {
                    claims.ForEach(claim =>
                    {
                        var claimPayments = payments.Where(p => p.ClaimId == claim.Id).ToList();
                        var oldPayments = claimPayments.Where(p => p.AgencyId == Guid.Empty && p.Payment > 0).ToList();
                        oldPayments.ForEach(p =>
                        {
                            p.AgencyId = claim.AgencyId;
                            p.Payor = claim.PrimaryInsuranceId;
                            paymentsToUpdate.Add(p);
                        });

                        ManagedClaimPayment payment = new ManagedClaimPayment()
                        {
                            Id = Guid.NewGuid(),
                            AgencyId = claim.AgencyId,
                            ClaimId = claim.Id,
                            PatientId = claim.PatientId,
                            Payment = claim.Payment,
                            PaymentDate = claim.PaymentDate,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,
                            Payor = claim.PrimaryInsuranceId,
                            IsDeprecated = false
                        };

                        if (claimPayments.Count > 0)
                        {
                            double paymentSum = claimPayments.Sum(p => p.Payment);
                            double paymentDifference = claim.Payment - paymentSum;
                            if (paymentDifference > 0)
                            {
                                payment.Payment = paymentDifference;
                                paymentsToAdd.Add(payment);
                            }
                        }
                        else
                        {
                            paymentsToAdd.Add(payment);
                        }
                        
                    });
                }
                if (paymentsToAdd.Count > 0)
                {
                    Database.AddMany<ManagedClaimPayment>(paymentsToAdd);
                    Console.WriteLine("Finished adding payments for " + agency.Name);
                    paymentsToAdd.Clear();
                }
            });
            if (paymentsToUpdate.Count > 0)
            {
                Console.WriteLine("Updating Payments.");
                Database.UpdateMany<ManagedClaimPayment>(paymentsToUpdate);
            }
        }
    }
}
