﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class DataSoftLogic
    {
        private static string input = Path.Combine(App.Root, "Files\\Discharge.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Discharge_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";
                                            patientData.Gender = "";


                                            var nameRow = dataRow.GetValue(0);
                                            var name = nameRow.Split('\n');
                                            if (name.Length > 1)
                                            {
                                                patientData.PatientIdNumber = name[1];
                                            }
                                            name = name[0].Split(',');
                                            var firstName = name[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (firstName.Length > 1)
                                            {
                                                patientData.FirstName = firstName[0];
                                                patientData.MiddleInitial = firstName[1];
                                            }
                                            else
                                            {
                                                patientData.FirstName = firstName[0];
                                            }
                                            patientData.LastName = name[0];

                                            if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Age: {0}. ", dataRow.GetValue(1));
                                            }

                                            var phones = dataRow.GetValue(2).Split('\n');
                                            if (phones.Length > 1)
                                            {
                                                patientData.PhoneHome = phones[0];
                                                patientData.PhoneMobile = phones[1];
                                            }
                                            else
                                            {
                                                patientData.PhoneHome = phones[0];
                                            }

                                            var address = dataRow.GetValue(3).Split('\n');
                                            patientData.AddressLine1 = address[0];
                                            var cityStateZip = address[1].Split(',');
                                            patientData.AddressCity = cityStateZip[0];
                                            var stateZip = cityStateZip[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            patientData.AddressStateCode = stateZip[0];
                                            patientData.AddressZipCode = stateZip[1];

                                            var ins = dataRow.GetValue(4).Split(',');
                                            if (ins[0].ToLower().Contains("medicaid"))
                                            {
                                                patientData.MedicaidNumber = ins[1];
                                            }
                                            else if (ins[0].ToLower().Contains("medicare"))
                                            {
                                                patientData.MedicareNumber = ins[1];
                                                patientData.PrimaryInsurance = "1";
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("Insurance: {0} - {1}. ", ins[0], ins[1]);
                                            }

                                            var socStatus = dataRow.GetValue(5).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            patientData.StartofCareDate = socStatus[0].ToDateTime();
                                            if (socStatus[1].ToLower().Contains("discharged"))
                                            {
                                                patientData.Status = 2;
                                            }
                                            else if (socStatus[1].ToLower().Contains("admit"))
                                            {
                                                patientData.Status = 1;
                                            }
                                            else
                                            {
                                                patientData.Status = 3;
                                            }

                                            var diag = dataRow.GetValue(6).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (diag.Length == 3)
                                            {
                                                patientData.Comments += string.Format("Acuity level: {0}. ", diag[0]);
                                                patientData.Comments += string.Format("Current Cert: {0}. ", diag[1]);
                                                patientData.Comments += string.Format("Primary Diag: {0}. ", diag[2]);
                                            }
                                            else if (diag.Length == 2)
                                            {
                                                patientData.Comments += string.Format("Current Cert: {0}. ", diag[0]);
                                                patientData.Comments += string.Format("Primary Diag: {0}. ", diag[1]);
                                            }

                                            var phys = dataRow.GetValue(7).Split('\n');
                                            if (phys.Length > 1)
                                            {
                                                patientData.Comments += string.Format("Physician: {0} - {1}. ", phys[0], phys[1]);
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("Physician: {0}. ", phys[0]);
                                            }

                                            var caseMan = dataRow.GetValue(8).Split('\n');
                                            if (caseMan.Length >= 1)
                                            {
                                                patientData.Comments += string.Format("Case Manager: {0}. ", caseMan[0]);
                                            }


                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            if (Database.GetPatientByMR(patientData.PatientIdNumber,new Guid(agencyId))==null)
                                            {
                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
