﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class SansioPhysicansScript
    {
        private static string input = Path.Combine(App.Root, "Files\\Physician.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Physician_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 2;
                                int rowCounter = 1;
                                AgencyPhysician physicianData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (rowCounter % numberOfRows == 1)
                                        {
                                            physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId.ToGuid();
                                            physicianData.IsDeprecated = false;
                                            var name = dataRow.GetValue(0).Split(',');
                                            if (name.Length < 2)
                                            {
                                                continue;
                                            }
                                            physicianData.LastName = name[0];
                                            physicianData.FirstName = name[1];

                                            var address = dataRow.GetValue(6).Split('\n');
                                            physicianData.AddressLine1 = address[0];
                                            if(address.Length > 1)
                                            {
                                                var cityStateZip = address[1].Split(',');
                                                if (cityStateZip.Length == 1)
                                                {
                                                    var stateZip = cityStateZip[0].Split(' ');
                                                    physicianData.AddressStateCode = stateZip[0];
                                                    if (stateZip.Length > 1)
                                                    {
                                                        var zip = stateZip[1];
                                                        if (!zip.IsDigitsOnly())
                                                        {
                                                            zip = zip.Split('-')[0];
                                                        }
                                                        physicianData.AddressZipCode = zip;
                                                    }
                                                }
                                                else
                                                {
                                                    physicianData.AddressCity = cityStateZip[0];
                                                    var stateZip = cityStateZip[1].Split(' ');
                                                    physicianData.AddressStateCode = stateZip[1];
                                                    var zip = stateZip[2];
                                                    if (!zip.IsDigitsOnly())
                                                    {
                                                        zip = zip.Split('-')[0];
                                                    }
                                                    physicianData.AddressZipCode = zip;
                                                }

                                            }

                                            var phone = dataRow.GetValue(13).ToPhoneDB();
                                            var fax = dataRow.GetValue(16).ToPhoneDB();
                                            physicianData.PhoneWork = phone;
                                            physicianData.FaxNumber = fax;

                                            if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                            {
                                                physicianData.LicenseNumber = dataRow.GetValue(20);
                                            }
                                            if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                            {
                                                physicianData.Comments += string.Format("License Expiration: {0}. ", dataRow.GetValue(23));
                                            }

                                        }
                                        else if (rowCounter % numberOfRows == 0)
                                        {
                                            var upin = dataRow.GetValue(0);
                                            physicianData.UPIN = upin.Split(':')[1].Trim();
                                            var npi = dataRow.GetValue(2);
                                            physicianData.NPI = npi.Split('\n')[0].Split(':')[1].Trim();

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId.ToGuid());
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALREADY EXISTS", i, physicianData.DisplayName);
                                            }
                                            textWriter.Write(textWriter.NewLine);
                                            i++;
                                        }
                                        rowCounter++;
                                    }
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }
    }
}
