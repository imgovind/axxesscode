﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class AxxessScript3
    {
        private static string input = Path.Combine(App.Root, "Files\\AXXESS.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AXXESS{0}.txt", DateTime.Now.Ticks.ToString()));

        private static string log = Path.Combine(App.Root, string.Format("Files\\LOG{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            TextWriter txtWrtr = new StreamWriter(log, true);
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.ServiceLocation = "Q5001";

                                            #region patientName
                                            //FN - 2
                                            patientData.FirstName = dataRow.GetValue(2);
                                            sb.Append("FirstName"); sb.Append(": "); sb.Append(patientData.FirstName); sb.Append("\t\t");

                                            //LN - 1
                                            patientData.LastName = dataRow.GetValue(1);
                                            sb.Append("LastName"); sb.Append(": "); sb.Append(patientData.LastName); sb.Append("\t\t");

                                            //if (dataRow.GetValue(0).Split(' ').Length > 1)
                                            //{
                                            //    patientData.FirstName = dataRow.GetValue(0).Split(' ')[0];
                                            //    patientData.MiddleInitial = dataRow.GetValue(0).Split(' ')[1];
                                            //}
                                            //else
                                            //{
                                            //    patientData.FirstName = dataRow.GetValue(0);
                                            //}
                                            #endregion

                                            #region patientIDGender
                                            //GENDER - 3
                                            var genderStaging = dataRow.GetValue(3);
                                            if (genderStaging.IsNotNullOrEmpty() && genderStaging.Length >= 1)
                                            {
                                                patientData.Gender = genderStaging.ToUpper().StartsWith("F") ? "Female" : genderStaging.ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                            }
                                            sb.Append("Gender"); sb.Append(": "); sb.Append(patientData.Gender); sb.Append("\t\t"); 

                                            //PATIENTIDNUMBER - 0
                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            sb.Append("PatientIdNumber"); sb.Append(": "); sb.Append(patientData.PatientIdNumber); sb.Append("\t\t");

                                            #endregion

                                            #region patientMedicareMedicaid

                                            bool primaryInsurancePayor = default(bool);

                                            //MEDICARE - 4
                                            var medicareNumberStaging = dataRow.GetValue(4);
                                            if (medicareNumberStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.MedicareNumber = medicareNumberStaging;
                                                primaryInsurancePayor = true;
                                            }
                                            sb.Append("MedicareNumber"); sb.Append(": "); sb.Append(patientData.MedicareNumber); sb.Append("\t\t");

                                            //MEDICAID - 5
                                            var medicaidNumberStaging = dataRow.GetValue(5);
                                            if (medicaidNumberStaging.IsNotNullOrEmpty())
                                            {

                                                patientData.MedicaidNumber = medicaidNumberStaging;
                                                primaryInsurancePayor = true;
                                            }
                                            sb.Append("MedicaidNumber"); sb.Append(": "); sb.Append(patientData.MedicaidNumber); sb.Append("\t\t");
                                            #endregion

                                            #region patientDOB

                                            //DOB - 6
                                            var DOBStaging = dataRow.GetValue(6);
                                            if (DOBStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }

                                            //patientData.DOB = DateTime.MinValue;
                                            sb.Append("DOB"); sb.Append(": "); sb.Append(patientData.DOB); sb.Append("\t\t");

                                            #endregion

                                            #region patientMaritalSSN

                                            //SSN
                                            //var ssnStaging = dataRow.GetValue(5).Replace(" ", "");
                                            //if (ssnStaging.IsNotNullOrEmpty())
                                            //{
                                            //    patientData.SSN = ssnStaging;
                                            //}
                                            //sb.Append("SSN"); sb.Append(": "); sb.Append(patientData.SSN); sb.Append("\t\t");

                                            //MARITAL STATUS
                                            //var maritalStatus = dataRow.GetValue(8);
                                            //if (maritalStatus.IsNotNullOrEmpty())
                                            //{
                                            //    maritalStatus = maritalStatus.ToUpperCase();
                                            //    if (maritalStatus.StartsWith("M"))
                                            //    {
                                            //        patientData.MaritalStatus = "Married";
                                            //    }
                                            //    else if (maritalStatus.StartsWith("S"))
                                            //    {
                                            //        patientData.MaritalStatus = "Single";
                                            //    }
                                            //    else if (maritalStatus.StartsWith("W"))
                                            //    {
                                            //        patientData.MaritalStatus = "Widowed";
                                            //    }
                                            //    else if (maritalStatus.StartsWith("D"))
                                            //    {
                                            //        patientData.MaritalStatus = "Divorced";
                                            //    }
                                            //    else
                                            //    {
                                            //        patientData.MaritalStatus = "Unknown";
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    patientData.MaritalStatus = "Unknown";
                                            //}
                                            //sb.Append("MaritalStatus"); sb.Append(": "); sb.Append(patientData.MaritalStatus); sb.Append("\t\t");

                                            #endregion

                                            #region patientPhoneAddress
                                            
                                            //PHONE HOME - 7
                                            var phoneHomeStaging = dataRow.GetValue(7);
                                            patientData.PhoneHome = phoneHomeStaging.IsNotNullOrEmpty() ? phoneHomeStaging.ToPhoneDB() : string.Empty;
                                            sb.Append("PhoneHome"); sb.Append(": "); sb.Append(patientData.PhoneHome); sb.Append("\t\t");
                                            
                                            //ADDRESS LINE 1 - 8
                                            var addressLineStaging = dataRow.GetValue(8);
                                            if (addressLineStaging.IsNotNullOrEmpty()) 
                                            {
                                                patientData.AddressLine1 = addressLineStaging;
                                            }
                                            
                                            sb.Append("AddressLine1"); sb.Append(": "); sb.Append(patientData.AddressLine1); sb.Append("\t\t"); 
                                            
                                            //ADDRESS CITY - 9
                                            var addressCityStaging = dataRow.GetValue(9);
                                            if (addressCityStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.AddressCity = addressCityStaging;
                                            }
                                            sb.Append("AddressCity"); sb.Append(": "); sb.Append(patientData.AddressCity); sb.Append("\t\t");
                                            
                                            //ADDRESS STATE - 10
                                            var addressStateCodeStaging = dataRow.GetValue(10).ToUpper().Replace(".", "");
                                            if (addressStateCodeStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.AddressStateCode = addressStateCodeStaging;
                                            }
                                            sb.Append("AddressStateCode"); sb.Append(": "); sb.Append(patientData.AddressStateCode); sb.Append("\t\t");
                                            
                                            //ADDRESSS ZIP - 11
                                            var addressZipCodeStaging = dataRow.GetValue(11);
                                            if (addressZipCodeStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.AddressZipCode = addressZipCodeStaging;
                                            }
                                            sb.Append("AddressZipCode"); sb.Append(": "); sb.Append(patientData.AddressZipCode); sb.Append("\t\t");
                                            #endregion

                                            #region patientSOC

                                            //SOC - 13
                                            var SOCStaging = dataRow.GetValue(19);
                                            if (SOCStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                            }
                                            else 
                                            {
                                                patientData.StartofCareDate = DateTime.MinValue;
                                            }
                                            sb.Append("StartofCareDate"); sb.Append(": "); sb.Append(patientData.StartofCareDate); sb.Append("\t\t");
                                            
                                            #endregion

                                            //Insurance - 27
                                            var insuranceStagingJimple = dataRow.GetValue(33);

                                            #region primaryInsurnace
                                            //PRIMARY INSURANCE - Get From Payor in agency record from agencies table
                                            if (primaryInsurancePayor == true)
                                            {
                                                if (insuranceStagingJimple.IsNotNullOrEmpty())
                                                {
                                                    if (insuranceStagingJimple.Contains("GBA"))
                                                    {
                                                        patientData.PrimaryInsurance = "1";
                                                    }
                                                    else 
                                                    {
                                                        patientData.PrimaryInsurance = "";
                                                    }
                                                }
                                            }
                                            else if (primaryInsurancePayor == false)
                                            {
                                                patientData.PrimaryInsurance = "";
                                            }
                                            sb.Append("PrimaryInsurance"); sb.Append(": "); sb.Append(patientData.PrimaryInsurance); sb.Append("\t\t");
                                            #endregion

                                            #region anamathu

                                            //Discipline Frequencies - 19
                                            var triageStaging = dataRow.GetValue(26);
                                            if (triageStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Triage = Int32.Parse(triageStaging);
                                            }
                                            sb.Append("Triage"); sb.Append(": "); sb.Append(patientData.Triage); sb.Append("\t\t");
                                            #endregion

                                            #region CommentsSection
                                            //COMMENTS SECTION
                                            string CommentsDelimiter = "; ";

                                            #region 13-18
                                            //Emergency Contact Name
                                            var emergencyContactNameStaging = dataRow.GetValue(13);
                                            if (emergencyContactNameStaging.IsNotNullOrEmpty())
                                            {
                                                //var eCNameArray = emergencyContactNameStaging.Split(' ');
                                                //if (eCNameArray.Length > 0) 
                                                //{
                                                //    if (eCNameArray.Length > 1) 
                                                //    {
                                                //        patientData.EmergencyContact.FirstName = eCNameArray[0];
                                                //        patientData.EmergencyContact.LastName = eCNameArray[1];
                                                //    } 
                                                //    else 
                                                //    {
                                                //        patientData.EmergencyContact.FirstName = eCNameArray[0];
                                                //    }
                                                //}
                                                patientData.Comments += "[Emergency Contact Name: " + emergencyContactNameStaging + CommentsDelimiter;
                                            }

                                            //Emergency Contact
                                            var emergencyContactRelationshipStaging = dataRow.GetValue(14);
                                            if (emergencyContactRelationshipStaging.IsNotNullOrEmpty())
                                            {
                                                //patientData.EmergencyContact.Relationship = emergencyContactRelationshipStaging;
                                                patientData.Comments += "Relationship: " + emergencyContactRelationshipStaging + CommentsDelimiter;
                                            }

                                            //EmergencyPhone
                                            var emergencyContactPhoneStaging = dataRow.GetValue(15);
                                            if (emergencyContactPhoneStaging.IsNotNullOrEmpty())
                                            {
                                                //patientData.EmergencyContact.PrimaryPhone = emergencyContactPhoneStaging.ToPhoneDB();
                                                patientData.Comments += "Phone: " + emergencyContactPhoneStaging + CommentsDelimiter;
                                            }

                                            //Emergency Address
                                            var emergencyContactAddressStaging = dataRow.GetValue(16);
                                            if (emergencyContactAddressStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Address: " + emergencyContactAddressStaging + "]" + CommentsDelimiter;
                                            }

                                            //Pharmacy
                                            var pharmacyStaging = dataRow.GetValue(17);
                                            if (pharmacyStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.PharmacyName = pharmacyStaging;
                                                patientData.Comments += "[Pharmacy: " + pharmacyStaging  + CommentsDelimiter;
                                            }

                                            //Pharmacy Phone
                                            var pharmacyPhoneStaging = dataRow.GetValue(18);
                                            if (pharmacyPhoneStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.PharmacyPhone = pharmacyPhoneStaging.ToPhoneDB();
                                                patientData.Comments += "Pharmacy Phone: " + emergencyContactPhoneStaging + "]" + CommentsDelimiter;
                                            }
                                            #endregion

                                            #region 20-25

                                            //Episode Start Date - 14
                                            var episodeStartDateStaging = dataRow.GetValue(20);
                                            if (episodeStartDateStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeStartDateStaging)).ToShortDateString(), CommentsDelimiter);
                                            }

                                            //Episode End Date - 15
                                            var episodeEndDateStaging = dataRow.GetValue(21);
                                            if (episodeEndDateStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Episode End Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeEndDateStaging)).ToShortDateString(), CommentsDelimiter);
                                            }

                                            //Primary Diagnosis - 16
                                            var primaryDiagStaging = dataRow.GetValue(22);
                                            if (primaryDiagStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Primary Diagnosis: " + primaryDiagStaging + CommentsDelimiter;
                                            }

                                            //Secondary Diagnosis - 17
                                            var secondaryDiagStaging = dataRow.GetValue(23);
                                            if (secondaryDiagStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Secondary Diagnosis: " + secondaryDiagStaging + CommentsDelimiter;
                                            }

                                            //Disciplines - 18
                                            var DisciplinesStaging = dataRow.GetValue(24);
                                            if (DisciplinesStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Disciplines: " + DisciplinesStaging + CommentsDelimiter;
                                            }

                                            //Discipline Frequencies - 19
                                            var DisciplineFrequenciesStaging = dataRow.GetValue(25);
                                            if (DisciplineFrequenciesStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Discipline Frequencies: " + DisciplineFrequenciesStaging + CommentsDelimiter;
                                            }
                                            #endregion

                                            var primaryClinicianStaging = dataRow.GetValue(31);
                                            if (primaryClinicianStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Primary Clinician: " + primaryClinicianStaging + CommentsDelimiter;
                                            }

                                            #region 36-40
                                            //Referral Source - 30
                                            var ReferralSourceStaging = dataRow.GetValue(36);
                                            if (ReferralSourceStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Referral Source: " + ReferralSourceStaging + CommentsDelimiter;
                                            }

                                            var therapistStaging = dataRow.GetValue(37);
                                            if (therapistStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Therapist: " + therapistStaging + CommentsDelimiter;
                                            }

                                            var externalReferralStaging = dataRow.GetValue(38);
                                            if (externalReferralStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "External Referral: " + externalReferralStaging + CommentsDelimiter;
                                            }

                                            var caseManagerStaging = dataRow.GetValue(39);
                                            if (caseManagerStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Case Manager: " + caseManagerStaging + CommentsDelimiter;
                                            }

                                            var patientFlagStaging = dataRow.GetValue(40);
                                            if (patientFlagStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Patient Flags: " + patientFlagStaging + CommentsDelimiter;
                                            }

                                            #endregion

                                            //Insurance - 27
                                            var insuranceStaging = dataRow.GetValue(33);
                                            if (insuranceStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;

                                                //if (insuranceStaging.Contains("MEDICAID") || insuranceStaging.Contains("HUMANA") || insuranceStaging.Contains("DAYTON") || insuranceStaging.Contains("UNITED"))
                                                //{
                                                //    patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;
                                                //}
                                                //else
                                                //{
                                                //    patientData.Comments += "Insurance: " + insuranceStaging + " (MEDICARE) " + CommentsDelimiter;
                                                //}
                                            }
                                            
                                            //Secondary Insurance - 28
                                            //var insuranceSecondaryStaging = dataRow.GetValue(28);
                                            //if (insuranceSecondaryStaging.IsNotNullOrEmpty()) 
                                            //{
                                            //    patientData.Comments += "Secondary Insurance: " + insuranceSecondaryStaging + CommentsDelimiter;
                                            //}

                                            patientData.Created = DateTime.Now;
                                            sb.Append("Created"); sb.Append(": "); sb.Append(patientData.Created); sb.Append("\t\t");
                                            patientData.Modified = DateTime.Now;
                                            sb.Append("Modified"); sb.Append(": "); sb.Append(patientData.Modified); sb.Append("\t\t");

                                            //#region PhysicianCheck

                                            //var npi = dataRow.GetValue(28);
                                            //if (npi.IsNotNullOrEmpty())
                                            //{
                                            //    var physician = Database.GetPhysician(npi, agencyId);
                                            //    if (physician == null)
                                            //    {
                                            //        var info = Database.GetNpiData(npi);
                                            //        if (info != null)
                                            //        {
                                            //            physician = new AgencyPhysician
                                            //            {
                                            //                Id = Guid.NewGuid(),
                                            //                AgencyId = agencyId,
                                            //                NPI = npi,
                                            //                LoginId = Guid.Empty,
                                            //                //AddressLine1 = dataRow.GetValue(15),
                                            //                //AddressCity = dataRow.GetValue(16),
                                            //                //AddressStateCode = dataRow.GetValue(17).Replace(".", ""),
                                            //                //AddressZipCode = dataRow.GetValue(21),
                                            //                PhoneWork = dataRow.GetValue(29).ToPhoneDB(),
                                            //                FaxNumber = dataRow.GetValue(30).ToPhoneDB(),
                                            //                //Credentials = dataRow.GetValue(16).IsNotNullOrEmpty()?dataRow.GetValue(16):string.Empty
                                            //            };
                                            //            var physicianName = dataRow.GetValue(27);
                                            //            if (physicianName.IsNotNullOrEmpty())
                                            //            {
                                            //                var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            //                if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                            //                {
                                            //                    if (physicianNameArray.Length == 3)
                                            //                    {
                                            //                        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                            //                        physician.MiddleName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderMiddleName;
                                            //                        physician.LastName = physicianNameArray[2].IsNotNullOrEmpty() ? physicianNameArray[2].Trim() : info.ProviderLastName;
                                            //                    }
                                            //                    else
                                            //                    {
                                            //                        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                            //                        physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                            //                    }
                                            //                }
                                            //                else
                                            //                {
                                            //                    physician.LastName = info.ProviderLastName;
                                            //                    physician.FirstName = info.ProviderFirstName;
                                            //                }
                                            //            }
                                            //            else
                                            //            {
                                            //                physician.LastName = info.ProviderLastName;
                                            //                physician.FirstName = info.ProviderFirstName;
                                            //            }
                                            //        }
                                            //    }
                                            //    if (physician != null)
                                            //    {
                                            //        StringBuilder sb2 = new StringBuilder();
                                            //        sb2.Append("Phys FN: "); sb2.Append(": "); sb2.Append(physician.FirstName);
                                            //        sb2.Append("Phys LN: "); sb2.Append(": "); sb2.Append(physician.LastName);
                                            //        sb2.Append("Phys NPI: "); sb2.Append(": "); sb2.Append(physician.NPI);
                                            //        sb2.Append("Phys Phone: "); sb2.Append(": "); sb2.Append(physician.PhoneWork);
                                            //        sb2.Append("Phys Fax: "); sb2.Append(": "); sb2.Append(physician.FaxNumber);
                                            //        Console.WriteLine(sb2.ToString());
                                            //        sb.Append("Phys FN: "); sb.Append(": "); sb.Append(physician.FirstName);
                                            //        sb.Append("Phys LN: "); sb.Append(": "); sb.Append(physician.LastName);
                                            //        sb.Append("Phys NPI: "); sb.Append(": "); sb.Append(physician.NPI);
                                            //        sb.Append("Phys Phone: "); sb.Append(": "); sb.Append(physician.PhoneWork);
                                            //        sb.Append("Phys Fax: "); sb.Append(": "); sb.Append(physician.FaxNumber);
                                            //    }
                                            //}

                                            //#endregion

                                            sb.Append("Comments"); sb.Append(": "); sb.Append(patientData.Comments); sb.Append("\t\t");
                                            txtWrtr.WriteLine(sb.ToString());

                                            sb.Length = 0;
                                            txtWrtr.Close();
                                            #endregion

                                            //Check if writing to Dev Database.
                                            //Check the Program.cs for AgencyId, AgencyLocationId
                                            #region DBWrite
                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(28);
                                                            if (npi.IsNotNullOrEmpty())
                                                            {
                                                                var physician = Database.GetPhysician(npi, agencyId);
                                                                if (physician == null)
                                                                {
                                                                    exists = false;
                                                                    var info = Database.GetNpiData(npi);
                                                                    if (info != null)
                                                                    {
                                                                        physician = new AgencyPhysician
                                                                        {
                                                                            Id = Guid.NewGuid(),
                                                                            AgencyId = agencyId,
                                                                            NPI = npi,
                                                                            LoginId = Guid.Empty,
                                                                            //AddressLine1 = dataRow.GetValue(15),
                                                                            //AddressCity = dataRow.GetValue(16),
                                                                            //AddressStateCode = dataRow.GetValue(17).Replace(".", ""),
                                                                            //AddressZipCode = dataRow.GetValue(21),
                                                                            PhoneWork = dataRow.GetValue(29).ToPhoneDB(),
                                                                            FaxNumber = dataRow.GetValue(30).ToPhoneDB(),
                                                                            //Credentials = dataRow.GetValue(16).IsNotNullOrEmpty()?dataRow.GetValue(16):string.Empty
                                                                        };
                                                                        var physicianName = dataRow.GetValue(27);
                                                                        if (physicianName.IsNotNullOrEmpty())
                                                                        {
                                                                            var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                            if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                            {
                                                                                if (physicianNameArray.Length == 3)
                                                                                {
                                                                                    physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                                    physician.MiddleName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderMiddleName;
                                                                                    physician.LastName = physicianNameArray[2].IsNotNullOrEmpty() ? physicianNameArray[2].Trim() : info.ProviderLastName;
                                                                                }
                                                                                else
                                                                                {
                                                                                    physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                                    physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                physician.LastName = info.ProviderLastName;
                                                                                physician.FirstName = info.ProviderFirstName;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            physician.LastName = info.ProviderLastName;
                                                                            physician.FirstName = info.ProviderFirstName;

                                                                        }

                                                                    }

                                                                    Database.Add(physician);

                                                                }


                                                                if (physician != null)
                                                                {
                                                                    var patientPhysician = new PatientPhysician
                                                                    {
                                                                        IsPrimary = true,
                                                                        PatientId = patientData.Id,
                                                                        PhysicianId = physician.Id
                                                                    };

                                                                    if (Database.Add(patientPhysician))
                                                                    {
                                                                        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}