﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class SynergyScript
    {
        private static string input = Path.Combine(App.Root, "Files\\5star.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\5star_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            patientData.Gender = "";
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;
                                            patientData.PhoneHome = string.Empty;
                                            patientData.FirstName = dataRow.GetValue(0);
                                            patientData.LastName = dataRow.GetValue(1);
                                            patientData.Gender=dataRow.GetValue(2).Equals("M")?"Male":"Female";
                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            patientData.MedicareNumber = dataRow.GetValue(4);
                                            patientData.PrimaryInsurance = "2";
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            }
                                            patientData.SSN = dataRow.GetValue(6);
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            }
                                            
                                            patientData.AddressLine1 = dataRow.GetValue(11);
                                            patientData.AddressCity = dataRow.GetValue(12);
                                            patientData.AddressStateCode = dataRow.GetValue(13);
                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                            {
                                                patientData.AddressZipCode = dataRow.GetValue(14);
                                            }
                                            if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                            {
                                                patientData.PhoneHome = dataRow.GetValue(15).ToPhoneDB();
                                            }
                                            
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (!Database.ExistPatient(agencyId, patientData.PatientIdNumber))
                                            {
                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                var exists = true;
                                                                var npi = dataRow.GetValue(24);
                                                                var physician = Database.GetPhysician(npi, agencyId);
                                                                if (physician == null)
                                                                {
                                                                    exists = false;
                                                                    var info = Database.GetNpiData(npi);
                                                                    if (info != null)
                                                                    {
                                                                        physician = new AgencyPhysician
                                                                        {
                                                                            Id = Guid.NewGuid(),
                                                                            AgencyId = agencyId,
                                                                            NPI = npi,
                                                                            LoginId = Guid.Empty,
                                                                            AddressLine1 = dataRow.GetValue(18),
                                                                            AddressCity = dataRow.GetValue(19),
                                                                            AddressStateCode = dataRow.GetValue(20),// dataRow.GetValue(20).Replace(".", ""),
                                                                            AddressZipCode = dataRow.GetValue(21),
                                                                            PhoneWork = dataRow.GetValue(22).ToPhoneDB(),
                                                                            FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                                            //Credentials = dataRow.GetValue(16)
                                                                        };
                                                                        string names = "";
                                                                        if (dataRow.GetValue(16).StartsWith("DR"))
                                                                            names = dataRow.GetValue(16).Substring(4);
                                                                        else
                                                                            names = dataRow.GetValue(16);

                                                                        var physicianNameArray = names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                                        if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                        {
                                                                            physician.LastName = physicianNameArray[0].Trim();
                                                                            var FirstMiddleArray = physicianNameArray[1].Trim().Split(' ');
                                                                            if (FirstMiddleArray.Length > 1)
                                                                            {
                                                                                physician.FirstName = FirstMiddleArray[0];
                                                                                physician.MiddleName = FirstMiddleArray[1];
                                                                            }
                                                                            else
                                                                            {
                                                                                physician.FirstName = physicianNameArray[1];
                                                                            }

                                                                        }
                                                                    }

                                                                    Database.Add(physician);
                                                                }

                                                                if (physician != null)
                                                                {
                                                                    var patientPhysician = new PatientPhysician
                                                                    {
                                                                        IsPrimary = true,
                                                                        PatientId = patientData.Id,
                                                                        PhysicianId = physician.Id
                                                                    };

                                                                    if (Database.Add(patientPhysician))
                                                                    {
                                                                        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
