﻿
namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    public static class Genie
    {
        private static string input = Path.Combine(App.Root, "Files\\Conversion.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Conversion_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Gender = "";
                                            

                                            patientData.FirstName = dataRow.GetValue(1);
                                            patientData.LastName = dataRow.GetValue(0);
                                            if(dataRow.GetValue(2).IsNotNullOrEmpty())
                                                patientData.MiddleInitial = dataRow.GetValue(2);
                                            patientData.Comments += string.Format("M/S:{0}.", dataRow.GetValue(3));
                                            //patientData.Gender = dataRow.GetValue(2).IsEqual("f") ? "Female" : "Male";
                                            //patientData.PatientIdNumber = dataRow.GetValue(3);
                                            if (dataRow.GetValue(8).IsNotNullOrEmpty() && dataRow.GetValue(8).Contains("MEDICARE"))
                                            {
                                                patientData.PrimaryInsurance = "1";
                                                patientData.MedicareNumber = dataRow.GetValue(12);
                                            }
                                            else if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Insurance:{0}.", dataRow.GetValue(8));
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty() && !dataRow.GetValue(12).Equals("NA"))
                                                {
                                                    patientData.Comments += string.Format("Insurance No.{0}.", dataRow.GetValue(12));
                                                }
                                            }

                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.Parse(dataRow.GetValue(4));
                                            }

                                            //if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            //{
                                            //    patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            //}
                                            //if(!dataRow.GetValue(6).Equals("NA"))
                                            //patientData.SSN = dataRow.GetValue(6).Replace(" ", "");
                                            //if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            //{
                                            //    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            //}
                                            if (dataRow.GetValue(9).IsNotNullOrEmpty() )
                                            {
                                                patientData.StartofCareDate = DateTime.Parse(dataRow.GetValue(9));
                                            }
                                            //patientData.Comments += string.Format("date of care:{0}", DateTime.FromOADate(double.Parse(dataRow.GetValue(8))).ToShortDateString());
                                            //if (dataRow.GetValue(9).ToLower().Contains("m"))
                                            //{
                                            //    patientData.MaritalStatus = "Married";
                                            //}
                                            //else if (dataRow.GetValue(9).ToLower().Contains("s"))
                                            //{
                                            //    patientData.MaritalStatus = "Single";
                                            //}
                                            //else if (dataRow.GetValue(9).ToLower().Contains("w"))
                                            //{
                                            //    patientData.MaritalStatus = "Widowed";
                                            //}
                                            //else if (dataRow.GetValue(9).ToLower().Contains("d"))
                                            //{
                                            //    patientData.MaritalStatus = "Divorced";
                                            //}
                                            //else
                                            //{
                                            //    patientData.MaritalStatus = "Unknown";
                                            //}

                                            var address = dataRow.GetValue(5).Split(',')[0].Trim();
                                            if (address.ToLowerCase().Contains("new lebanon"))
                                            {
                                                int end=address.ToLowerCase().IndexOf("new lebanon");
                                                patientData.AddressLine1 = address.Substring(0, end).Trim();
                                                patientData.AddressCity = "New Lebanon";
                                            }
                                            else if (address.ToLowerCase().Contains("west chester"))
                                            {
                                                int end = address.ToLowerCase().IndexOf("west chester");
                                                patientData.AddressLine1 = address.Substring(0, end).Trim();
                                                patientData.AddressCity = "West Chester";
                                            }
                                            else
                                            {
                                                int end = address.LastIndexOf(" ");
                                                patientData.AddressLine1 = address.Substring(0, end).Trim();
                                                patientData.AddressCity = address.Substring(end).Trim();
                                            }
                                            patientData.AddressLine2 = "";
                                            patientData.AddressStateCode = "OH";
                                            patientData.AddressZipCode = dataRow.GetValue(6);
                                            patientData.PhoneHome = dataRow.GetValue(7).ToPhoneDB();
                                            patientData.Comments += string.Format("PCP PHONE:{0}.", dataRow.GetValue(11));
                                            patientData.Comments += string.Format("PCP FAX:{0}.", dataRow.GetValue(14));
                                            if(dataRow.GetValue(13).IsNotNullOrEmpty() && !dataRow.GetValue(13).Equals("NA"))
                                                patientData.Comments += string.Format("MCD:{0}.", dataRow.GetValue(13));

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            //var exists = true;
                                                            //var npi = dataRow.GetValue(25);
                                                            //var physician = Database.GetPhysician(npi, agencyId.ToGuid());
                                                            //if (physician == null)
                                                            //{
                                                            //    exists = false;
                                                            //    var info = Database.GetNpiData(npi);
                                                            //    physician = new AgencyPhysician
                                                            //    {
                                                            //        Id = Guid.NewGuid(),
                                                            //        AgencyId = agencyId.ToGuid(),
                                                            //        NPI = npi,
                                                            //        LoginId = Guid.Empty,
                                                            //        AddressLine1 = dataRow.GetValue(18)+" "+dataRow.GetValue(19),
                                                            //        AddressCity = dataRow.GetValue(20),
                                                            //        AddressStateCode = dataRow.GetValue(21),
                                                            //        AddressZipCode = dataRow.GetValue(22),
                                                            //        PhoneWork = dataRow.GetValue(23).ToPhoneDB(),
                                                            //        FaxNumber = dataRow.GetValue(24).ToPhoneDB(),
                                                            //        FirstName = info != null ? info.ProviderFirstName : string.Empty,
                                                            //        LastName = info != null ? info.ProviderLastName : string.Empty,
                                                            //        Credentials = dataRow.GetValue(24)
                                                            //    };

                                                            //    physician.LastName = dataRow.GetValue(15);
                                                            //    physician.FirstName = dataRow.GetValue(16);   
                                                                
                                                                

                                                            //    Database.Add(physician);
                                                            //}

                                                            //if (physician != null)
                                                            //{
                                                            //    var patientPhysician = new PatientPhysician
                                                            //    {
                                                            //        IsPrimary = true,
                                                            //        PatientId = patientData.Id,
                                                            //        PhysicianId = physician.Id
                                                            //    };

                                                            //    if (Database.Add(patientPhysician))
                                                            //    {
                                                            //        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                            //    }
                                                            //}
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}

