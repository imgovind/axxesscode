﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.OasisC.Domain;

    public class AddPermission
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\AddPermission_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                var users = Database.GetUsers();
                if (users != null && users.Count > 0)
                {
                    int count = 0;
                    users.ForEach(u =>
                    {
                        if (u.Permissions.IsNotNullOrEmpty())
                        {
                            u.PermissionsArray = u.Permissions.ToObject<List<string>>();
                            if (u.PermissionsArray.Contains(((ulong)Permissions.AccessBillingCenter).ToString()) &&
                                !u.PermissionsArray.Contains(((ulong)Permissions.RemittanceAdvice).ToString()))
                            {
                                count++;
                                u.PermissionsArray.Add(((ulong)Permissions.RemittanceAdvice).ToString());
                                u.Permissions = u.PermissionsArray.ToXml();
                                u.Modified = DateTime.Now;

                                try
                                {
                                    if (Database.Update<User>(u))
                                    {
                                        Console.WriteLine("{1}.Modified user:{0} successfully", u.DisplayName, count.ToString());
                                    }
                                    else
                                    {
                                        textWriter.Write("{1}.Modified user:{0} failed", u.DisplayName, count.ToString());
                                        Console.WriteLine("{1}.Modified user:{0} failed", u.DisplayName, count.ToString());

                                    }
                                }
                                catch (Exception ex)
                                {
                                    textWriter.Write(ex.ToString());
                                }
                            }
                        }
                    });
                    Console.WriteLine("Totally {0} users has been modified.", count.ToString());
                    Console.WriteLine("--------------------DONE-----------------------------");
                }
            }
        }
    }
}
