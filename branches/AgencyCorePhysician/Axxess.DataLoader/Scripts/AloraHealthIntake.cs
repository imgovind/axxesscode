﻿

namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public static class AloraHealthIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\FamilyHome.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\FamilyHome_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int rowCounter = 0;
                                int dataCounter = 0;
                                int count = 0;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        switch (dataCounter)
                                        {
                                            case 0:
                                                patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = agencyId;
                                                patientData.AgencyLocationId = locationId;
                                                patientData.Status = 1;
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                var nameArray = dataRow.GetValue(2).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    patientData.LastName = nameArray[0].Trim();
                                                    patientData.FirstName = nameArray[1].Trim();
                                                }
                                                if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = DateTime.FromOADate(dataRow.GetValue(9).ToDouble());//double.Parse(dataRow.GetValue(9))).ToString("yyyy-M-d");
                                                }
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                                {
                                                    patientData.Status = 2;
                                                    patientData.DischargeDate = DateTime.FromOADate(dataRow.GetValue(12).ToDouble());
                                                }
                                                patientData.PatientIdNumber = dataRow.GetValue(16);
                                                if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                                {
                                                    patientData.DOB = DateTime.FromOADate(dataRow.GetValue(20).ToDouble());//double.Parse(dataRow.GetValue(20))).ToString("yyyy-M-d");
                                                }
                                                patientData.Gender = dataRow.GetValue(24).IsEqual("f") ? "Female" : "Male";
                                                if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Case Manager/Primary RN:{0}.", dataRow.GetValue(26));
                                                }
                                                if (dataRow.GetValue(33).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Current Certification Period: {0}. ", dataRow.GetValue(33));
                                                }
                                                if (dataRow.GetValue(39).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(39));
                                                }
                                                break;
                                            case 1:
                                                patientData.PhoneHome = dataRow.GetValue(2).IsNotNullOrEmpty() ? dataRow.GetValue(2).ToPhoneDB() : string.Empty;
                                                if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(6));
                                                }
                                                if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Evacuation {0}. ", dataRow.GetValue(23));
                                                }
                                                if (dataRow.GetValue(29).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Disaster/Triage code:{0}.", dataRow.GetValue(29));
                                                }
                                                break;
                                            case 2:
                                                if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                                {
                                                    var addressArray = dataRow.GetValue(2).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (addressArray != null)
                                                    {
                                                        if (addressArray.Length == 3)
                                                        {
                                                            patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = "";
                                                            patientData.AddressCity = addressArray[1].Trim();

                                                            var locationArray = addressArray[2].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (locationArray != null && locationArray.Length == 2)
                                                            {
                                                                patientData.AddressStateCode = locationArray[0];
                                                                patientData.AddressZipCode = locationArray[1];
                                                            }
                                                        }

                                                        if (addressArray.Length == 4)
                                                        {
                                                            patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = addressArray[1].Replace("'", "");
                                                            patientData.AddressCity = addressArray[2].Trim();

                                                            var locationArray = addressArray[3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (locationArray != null && locationArray.Length == 2)
                                                            {
                                                                patientData.AddressStateCode = locationArray[0];
                                                                patientData.AddressZipCode = locationArray[1];
                                                            }
                                                        }
                                                    }
                                                }

                                                if (dataRow.GetValue(23).IsNotNullOrEmpty() && dataRow.GetValue(23).ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "3";
                                                    var medicareNumberArray = dataRow.GetValue(23).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (medicareNumberArray != null)
                                                    {
                                                        patientData.MedicareNumber = medicareNumberArray[2].Replace(")", "");
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.Comments += string.Format("Payer & Insurance ID:{0}.", dataRow.GetValue(23));
                                                }
                                                if (dataRow.GetValue(37).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Referral Source:{0}.", dataRow.GetValue(37));
                                                }
                                                //textWriter.WriteLine(new PatientScript(patientData).ToString());
                                                //textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());

                                                //textWriter.Write(textWriter.NewLine);

                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;

                                                var medicationProfile = new MedicationProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };

                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                count++;
                                                                Console.WriteLine("{0}) {1}", count, patientData.DisplayName);
                                                            }
                                                        }
                                                    }
                                                }

                                                break;
                                        }
                                        //for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                                        //{
                                        //    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                                        //    {
                                        //        Console.WriteLine("{0}) Data: {1}", dataCounter, dataRow.GetValue(columnCounter));
                                        //        Console.WriteLine("Row x Column: [{0} x {1}]", rowCounter, columnCounter);
                                        //        Console.WriteLine();
                                        //    }
                                        //}
                                        dataCounter++;
                                        
                                    }
                                    else
                                    {
                                        dataCounter = 0;
                                    }
                                    rowCounter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
