﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class HHCenterScript
    {
        private static string input = Path.Combine(App.Root, "Files\\hhcactive.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\hhcactive_insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Gender = "";

                                            var nameArray = dataRow.GetValue(0).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray.Length > 1)
                                            {
                                                patientData.LastName = nameArray[0].Trim();
                                                patientData.FirstName = nameArray[1].Trim();
                                            }

                                            patientData.PhoneHome = dataRow.GetValue(2).ToPhoneDB();
                                            if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(3).Replace("\n", " ").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null)
                                                {
                                                    if (addressArray.Length == 2)
                                                    {
                                                        var twoWordCityArray = addressArray[0].Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (twoWordCityArray != null && twoWordCityArray.Length == 2)
                                                        {
                                                            patientData.AddressLine1 = twoWordCityArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = "";
                                                            patientData.AddressCity = twoWordCityArray[1].Replace("'", "");
                                                        }
                                                        else
                                                        {
                                                            var oneWordCityArray = addressArray[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (oneWordCityArray != null && oneWordCityArray.Length > 1)
                                                            {
                                                                patientData.AddressCity = oneWordCityArray.Reverse()[0].Replace("'", "");

                                                                oneWordCityArray = oneWordCityArray.Reverse();
                                                                int addressLine1Count = 0;
                                                                var addressLine1Array = new StringBuilder();
                                                                do
                                                                {
                                                                    addressLine1Array.AppendFormat("{0} ", oneWordCityArray[addressLine1Count]);
                                                                    addressLine1Count++;
                                                                } while (addressLine1Count < oneWordCityArray.Length - 1);

                                                                patientData.AddressLine1 = addressLine1Array.ToString().TrimEnd();
                                                                patientData.AddressLine2 = "";
                                                            }
                                                        }
                                                        var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (locationArray != null && locationArray.Length == 2)
                                                        {
                                                            patientData.AddressStateCode = locationArray[0];
                                                            patientData.AddressZipCode = locationArray[1].Substring(0, 5);
                                                        }
                                                    }
                                                }
                                            }
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                var insuranceArray = dataRow.GetValue(4).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                if (insuranceArray != null && insuranceArray.Length == 2)
                                                {
                                                    if (insuranceArray[0].ToLower().StartsWith("medicare"))
                                                    {
                                                        patientData.PrimaryInsurance = "1";
                                                        patientData.MedicareNumber = insuranceArray[1];
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(4));
                                                    }
                                                }
                                            }

                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                var socArray = dataRow.GetValue(5).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                if (socArray != null && socArray.Length == 2)
                                                {
                                                    patientData.StartofCareDate = socArray[0].ToDateTime();
                                                }
                                            }

                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Acuity Level/Current Cert/Primary Diagnosis: {0}. ", dataRow.GetValue(6));
                                            }

                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician/Phone: {0}.", dataRow.GetValue(7));
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
