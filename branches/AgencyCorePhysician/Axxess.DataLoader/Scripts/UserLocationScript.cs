﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class UserLocationScript
    {
        public static void Run()
        {
            var agencies = Database.GetAgencies();
            if (agencies != null && agencies.Count > 0)
            {
                Console.WriteLine("Agency List Count: {0}", agencies.Count);
                agencies.ForEach(agency =>
                {
                    if (!agency.IsDeprecated)
                    {
                        Console.WriteLine("Agency Name: {0}", agency.Name);
                        var agencyUsers = Database.GetUsers(agency.Id);
                        var agencyLocations = Database.GetAgencyLocations(agency.Id);
                        if (agencyUsers != null && agencyLocations != null)
                        {
                            Console.WriteLine("Agency User Count: {0}", agencyUsers.Count);
                            Console.WriteLine("Agency Location Count: {0}", agencyLocations.Count);
                            agencyUsers.ForEach(user =>
                            {
                                if (!user.IsDeprecated)
                                {
                                    Console.WriteLine("User Name: {0}", user.DisplayName);

                                    var userLocations = Database.GetUserLocations(user.AgencyId, user.Id);
                                    var userLocationIds = userLocations != null ? userLocations.Select(l => l.AgencyLocationId).ToList() : new List<Guid>();
                                    agencyLocations.ForEach(location =>
                                    {
                                        if (!location.IsDeprecated)
                                        {
                                            Console.WriteLine("Agency Location Name: {0}", location.Name);
                                            if (!userLocationIds.Contains(location.Id))
                                            {
                                                Console.WriteLine("Location {0} for User: {1}", location.Name, user.DisplayName);
                                                Database.Add<UserLocation>(new UserLocation
                                                {
                                                    UserId = user.Id,
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = user.AgencyId,
                                                    AgencyLocationId = location.Id
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    }
}
