﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Linq;
    using System.Threading;
    using System.Diagnostics;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Log.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Excel;
    using Kent.Boogaart.KBCsv;
    using SubSonic.Repository;
    using SubSonic.Query;


    public static class ScheduleEventExportToTable
    {
        private static string error = Path.Combine(App.Root, string.Format("Files\\ScheduleEventExport-ErrorLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string output = Path.Combine(App.Root, string.Format("Files\\ScheduleEventExport-EventLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\ScheduleEventExport-Datalog-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            DateTime startTimeForRun = DateTime.Now;
            using (TextWriter errorWriter = new StreamWriter(error, true))
            {
                using (TextWriter eventWriter = new StreamWriter(output, true))
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        try
                        {
                            logWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            var agencyIds = Database.GetAgencyIdsOnly();
                            if (agencyIds != null && agencyIds.Count > 0)
                            {
                                IList<Guid> patientEpisodeIds = null;
                                PatientEpisode patientEpisode = null;
                                List<ScheduleEvent> scheduleEvents = null;
                                foreach(Guid agencyId in agencyIds)
                                {
                                    if (agencyId != null && agencyId.IsNotEmpty())
                                    {
                                        Console.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                        logWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                        errorWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                        patientEpisodeIds = Database.GetPatientEpisodeIdsOnly(agencyId);
                                        if (patientEpisodeIds != null && patientEpisodeIds.Count > 0)
                                        {
                                            foreach (var patientEpisodeId in patientEpisodeIds)
                                            {
                                                if (patientEpisodeId.IsNotEmpty())
                                                {
                                                    patientEpisode = Database.GetPatientEpisode(patientEpisodeId, agencyId);
                                                    if (patientEpisode != null)
                                                    {
                                                        patientEpisode.AgencyId = agencyId;
                                                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                                                        {
                                                            scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                                            scheduleEvents = scheduleEvents.Select(x => 
                                                                { 
                                                                    x.IsActive = patientEpisode.IsActive; 
                                                                    x.IsDischarged = patientEpisode.IsDischarged; 
                                                                    return x; 
                                                                }
                                                            ).ToList();
                                                            if (Database.AddManyScheduleEvents(scheduleEvents))
                                                            {
                                                                Console.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                logWriter.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                errorWriter.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            logWriter.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            TimeSpan endTimerForRun = DateTime.Now - startTimeForRun;
                            logWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            logWriter.WriteLine("Total Time (Mins): " + endTimerForRun.TotalMinutes);
                            errorWriter.WriteLine("Total Time (Mind): " + endTimerForRun.TotalMinutes);
                        }
                        catch (Exception ex)
                        {
                            eventWriter.WriteLine("Time: " + DateTime.Now.ToString());
                            eventWriter.WriteLine(ex.StackTrace);
                        }
                    }
                }
            }
        }
        public static void Run(Guid agencyId)
        {
            DateTime startTimeForRun = DateTime.Now;
            using (TextWriter errorWriter = new StreamWriter(error, true))
            {
                using (TextWriter eventWriter = new StreamWriter(output, true))
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        try
                        {
                            logWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            if (agencyId != null && agencyId.IsNotEmpty())
                            {
                                IList<Guid> patientEpisodeIds = null;
                                PatientEpisode patientEpisode = null;
                                List<ScheduleEvent> scheduleEvents = null;
                                Console.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                logWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                errorWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                patientEpisodeIds = Database.GetPatientEpisodeIdsOnly(agencyId);
                                if (patientEpisodeIds != null && patientEpisodeIds.Count > 0)
                                {
                                    foreach (var patientEpisodeId in patientEpisodeIds)
                                    {
                                        if (patientEpisodeId.IsNotEmpty())
                                        {
                                            patientEpisode = Database.GetPatientEpisode(patientEpisodeId, agencyId);
                                            if (patientEpisode != null)
                                            {
                                                patientEpisode.AgencyId = agencyId;
                                                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                                                {
                                                    scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                                                    {
                                                        scheduleEvents = scheduleEvents.Where(x => x != null && x.EventId != null && x.EventId.IsNotEmpty()).ToList();
                                                        scheduleEvents = scheduleEvents.Select(x => { x.Id = x.EventId; x.StartDate = patientEpisode.StartDate; x.EndDate = patientEpisode.EndDate; return x; }).ToList();

                                                        if (Database.AddManyScheduleEvents(scheduleEvents))
                                                        {
                                                            Console.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            logWriter.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            errorWriter.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                        logWriter.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                    logWriter.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("PATIENTEPISODE DOES NOT EXIST => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                logWriter.WriteLine("PATIENTEPISODE DOES NOT EXIST => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                            }
                                        }
                                    }
                                }
                            }
                            TimeSpan endTimerForRun = DateTime.Now - startTimeForRun;
                            logWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            logWriter.WriteLine("Total Time (Mins): " + endTimerForRun.TotalMinutes);
                            errorWriter.WriteLine("Total Time (Mind): " + endTimerForRun.TotalMinutes);
                        }
                        catch (Exception ex)
                        {
                            eventWriter.WriteLine("Time: " + DateTime.Now.ToString());
                            eventWriter.WriteLine(ex);
                        }
                    }
                }
            }
        }
        public static void RunAtOnce(Guid agencyId)
        {
            DateTime startTimeForRun = DateTime.Now;
            using (TextWriter errorWriter = new StreamWriter(error, true))
            {
                using (TextWriter eventWriter = new StreamWriter(output, true))
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        try
                        {
                            logWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            if (agencyId != null && agencyId.IsNotEmpty())
                            {
                                IList<PatientEpisode> patientEpisodes = null;
                                List<ScheduleEvent> scheduleEvents = null;
                                Console.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                logWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                errorWriter.WriteLine("STARTED : Agency Update For => [" + agencyId + "]");
                                patientEpisodes = Database.GetPatientEpisodes(agencyId);
                                if (patientEpisodes != null && patientEpisodes.Count > 0)
                                {
                                    scheduleEvents = patientEpisodes.SelectMany(episode => episode.ScheduleEvents.IsNotNullOrEmpty() ? (episode.ScheduleEvents.Select(x => { if (x != null) { x.Id = x.EventId; x.StartDate = episode.StartDate; x.EndDate = episode.EndDate; } return x; })) : new List<ScheduleEvent>()).ToList();
                                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                                    {
                                        if (Database.AddManyScheduleEvents(scheduleEvents))
                                        {
                                            Console.WriteLine("SUCCEEDED [" + agencyId + "]");
                                            logWriter.WriteLine("SUCCEEDED [" + agencyId + "]");
                                        }
                                        else
                                        {
                                            Console.WriteLine("FAILED [" + agencyId + "]");
                                            errorWriter.WriteLine("FAILED [" + agencyId + "]");
                                        }
                                    }
                                }
                            }
                            TimeSpan endTimerForRun = DateTime.Now - startTimeForRun;
                            logWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            logWriter.WriteLine("Total Time (Mins): " + endTimerForRun.TotalMinutes);
                            errorWriter.WriteLine("Total Time (Mind): " + endTimerForRun.TotalMinutes);
                        }
                        catch (Exception ex)
                        {
                            eventWriter.WriteLine("Time: " + DateTime.Now.ToString());
                            eventWriter.WriteLine(ex);
                        }
                    }
                }
            }
        }
    }
}
