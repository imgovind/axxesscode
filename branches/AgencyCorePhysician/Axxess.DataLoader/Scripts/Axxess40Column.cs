﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class Axxess40Column
    {
        private static string input = Path.Combine(App.Root, "Files\\AXXESS.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AXXESS{0}.txt", DateTime.Now.Ticks.ToString()));

        private static string log = Path.Combine(App.Root, string.Format("Files\\LOG{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    var totalpatients = default(int);
                    var totalCounter = default(int);
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        totalpatients = dataTable.Rows.Count;
                                        totalCounter++;
                                        if (!dataRow.IsEmpty())
                                        {
                                            TextWriter txtWrtr = new StreamWriter(log, true);
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.ServiceLocation = "Q5001";

                                            #region DataPresent
                                            bool isPhysicianDataPresent = default(bool);
                                            bool isDBWritePermission = default(bool);
                                            bool isPhysicianDataChecked = default(bool);

                                            
                                            #endregion

                                            #region VARS
                                            int firstName_col = default(int);
                                            int lastName_col = default(int);
                                            int gender_col = default(int);
                                            int id_col = default(int);
                                            int medicare_col = default(int);
                                            int medicaid_col = default(int);
                                            int dob_col = default(int);
                                            int ssn_col = default(int);
                                            int soc_col = default(int);
                                            int episodeSD_col = default(int);
                                            int episodeED_col = default(int);
                                            int marital_col = default(int);
                                            int addr1_col = default(int);
                                            int city_col = default(int);
                                            int state_col = default(int);
                                            int zip_col = default(int);
                                            int phonehome_col = default(int);
                                            int mdName_col = default(int);
                                            int mdAddr1_col = default(int);
                                            int mdCity_col = default(int);
                                            int mdState_col = default(int);
                                            int mdZip_col = default(int);
                                            int mdPhone_col = default(int);
                                            int mdFax_col = default(int);
                                            int mdCred_col = default(int);
                                            int mdNPI_col = default(int);
                                            
                                            
                                            int triage_col = default(int);
                                            int discipline_col = default(int);
                                            int disciplineFreq_col = default(int);


                                            int primaryDiag_col = default(int);
                                            int primaryDiagDesc_col = default(int);
                                            int secondaryDiag_col = default(int);
                                            int primaryClinician_col = default(int);
                                            int referralSource_col = default(int);
                                            int externalReferral_col = default(int);
                                            int caseManager_col = default(int);
                                            int type_col = default(int);
                                            int county_col = default(int);
                                            int CBSA_col = default(int);
                                            int ethnicity_col = default(int);
                                            int insurance_col = default(int);
                                            int primaryPhysician_col = default(int);
                                            string firstColumn = "firstName_col";

                                            firstName_col = 2;      
                                            lastName_col = 1;       
                                            gender_col = 3;         
                                            id_col = 0;
                                            medicare_col = 4;
                                            //medicaid_col = 4;
                                            dob_col = 6;
                                            //ssn_col = 7;            
                                            soc_col = 13;            
                                            episodeSD_col = 14;
                                            episodeED_col = 15;
                                            //marital_col = 10;        
                                            addr1_col = 8;          
                                            city_col = 9;          
                                            state_col = 10;
                                            zip_col = 11;
                                            phonehome_col = 7;
                                            //mdName_col = 21;
                                            //mdAddr1_col = 15;
                                            //mdCity_col = 16;
                                            //mdState_col = 17;
                                            //mdZip_col = 23;
                                            //mdPhone_col = 23;
                                            //mdFax_col = 24;
                                            //mdCred_col = 20;
                                            mdNPI_col = 22;
                                            primaryPhysician_col = 21;
                                            
                                            
                                            primaryDiag_col = 16;
                                            //primaryDiagDesc_col = 24;
                                            secondaryDiag_col = 17;
                                            triage_col = 20;
                                            discipline_col = 18;
                                            //disciplineFreq_col = 19;

                                            insurance_col = 25;
                                            //primaryClinician_col = 25;
                                            referralSource_col = 27;
                                            externalReferral_col = 29;
                                            caseManager_col = 32;
                                            type_col = 39;
                                            //county_col = 35;
                                            CBSA_col = 34;
                                            ethnicity_col = 35;

                                            string CommentsDelimiter = "; ";
                                            #endregion

                                            #region physicianVARS
                                            int mdFirstName_SubCol = default(int);
                                            int mdLastName_SubCol = default(int);
                                            int mdMiddleName_SubCol = default(int);
                                            char mdSplitChar = default(char);

                                            mdSplitChar = ',';
                                            mdFirstName_SubCol = 1;
                                            mdLastName_SubCol = 0;
                                            mdMiddleName_SubCol = 2;
                                            #endregion

                                            isPhysicianDataChecked = false;
                                            isPhysicianDataPresent = false;
                                            #region physicianCheckProcess
                                            //Set this to true if you dont want to write to DB
                                            if (isPhysicianDataChecked == true)
                                            {
                                                isDBWritePermission = false;
                                            }
                                            else if (isPhysicianDataChecked == false)
                                            {
                                                isDBWritePermission = true;
                                            }
                                 
                                            #endregion

                                            //GOD MODE OVERRIDE
                                            isDBWritePermission = true;

                                            #region PROCESS

                                            #region patientName
                                            //FN - 0
                                            if (firstName_col != default(int))
                                            {
                                                patientData.FirstName = dataRow.GetValue(firstName_col);
                                                sb.Append("FirstName"); sb.Append(": "); sb.Append(patientData.FirstName); sb.Append("\t\t");
                                            }
                                            //
                                            
                                            //LN - 0
                                            if (lastName_col != default(int))
                                            {
                                                patientData.LastName = dataRow.GetValue(lastName_col);
                                                sb.Append("LastName"); sb.Append(": "); sb.Append(patientData.LastName); sb.Append("\t\t");
                                            }
                                            //
                                            #endregion

                                            #region ID+Gender
                                            //GENDER
                                            if (gender_col != default(int))
                                            {
                                                var genderStaging = dataRow.GetValue(gender_col);
                                                if (genderStaging.IsNotNullOrEmpty() && genderStaging.Length >= 1)
                                                {
                                                    patientData.Gender = genderStaging.ToUpper().StartsWith("F") ? "Female" : genderStaging.ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                                }
                                                sb.Append("Gender"); sb.Append(": "); sb.Append(patientData.Gender); sb.Append("\t\t");
                                            }
                                            else 
                                            {
                                                patientData.Gender = "";
                                                sb.Append("Gender"); sb.Append(": "); sb.Append(patientData.Gender); sb.Append("\t\t");
                                            }
                                            //

                                            //ID - MRN
                                            if (id_col != null)
                                            {
                                                patientData.PatientIdNumber = dataRow.GetValue(id_col);
                                                sb.Append("PatientIdNumber"); sb.Append(": "); sb.Append(patientData.PatientIdNumber); sb.Append("\t\t");
                                            }
                                            //
                                            #endregion

                                            #region Medicare+Medicaid
                                            bool primaryInsurancePayor = default(bool);
                                            var insuranceStagingNomer = dataRow.GetValue(insurance_col);
                                            if (insuranceStagingNomer.ToUpper().Contains("MEDICARE"))
                                            {
                                                //MEDICARE - 4
                                                if (medicare_col != default(int))
                                                {
                                                    var medicareNumberStaging = dataRow.GetValue(medicare_col);
                                                    if (medicareNumberStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MedicareNumber = medicareNumberStaging;
                                                        primaryInsurancePayor = true;
                                                    }
                                                    sb.Append("MedicareNumber"); sb.Append(": "); sb.Append(patientData.MedicareNumber); sb.Append("\t\t");
                                                }
                                                //

                                                //MEDICAID - 5
                                                if (medicaid_col != default(int))
                                                {
                                                    var medicaidNumberStaging = dataRow.GetValue(medicaid_col);
                                                    if (medicaidNumberStaging.IsNotNullOrEmpty())
                                                    {

                                                        patientData.MedicaidNumber = medicaidNumberStaging;
                                                        primaryInsurancePayor = true;
                                                    }
                                                    sb.Append("MedicaidNumber"); sb.Append(": "); sb.Append(patientData.MedicaidNumber); sb.Append("\t\t");
                                                }
                                                //    
                                            }
                                            
                                            #endregion

                                            #region PrimaryInsurance - Set it Right - Manual

                                            //var insuranceStagingJimple = dataRow.GetValue(33);

                                            //PRIMARY INSURANCE - Get From Payor in agency record from agencies table
                                            if (primaryInsurancePayor == true)
                                            {
                                                patientData.PrimaryInsurance = "1";
                                            }
                                            else if (primaryInsurancePayor == false)
                                            {
                                                patientData.PrimaryInsurance = "";
                                            }
                                            sb.Append("PrimaryInsurance"); sb.Append(": "); sb.Append(patientData.PrimaryInsurance); sb.Append("\t\t");
                                            #endregion

                                            #region SSN+Marital
                                            //SSN
                                            if (ssn_col != default(int))
                                            {
                                                var ssnStaging = dataRow.GetValue(ssn_col).Replace(" ", "");
                                                if (ssnStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.SSN = ssnStaging;
                                                }
                                                sb.Append("SSN"); sb.Append(": "); sb.Append(patientData.SSN); sb.Append("\t\t");
                                            }
                                            //

                                            //MARITAL STATUS
                                            if (marital_col != default(int))
                                            {
                                                var maritalStatus = dataRow.GetValue(marital_col);
                                                if (maritalStatus.IsNotNullOrEmpty())
                                                {
                                                    maritalStatus = maritalStatus.ToUpperCase();
                                                    if (maritalStatus.StartsWith("M"))
                                                    {
                                                        patientData.MaritalStatus = "Married";
                                                    }
                                                    else if (maritalStatus.StartsWith("S"))
                                                    {
                                                        patientData.MaritalStatus = "Single";
                                                    }
                                                    else if (maritalStatus.StartsWith("W"))
                                                    {
                                                        patientData.MaritalStatus = "Widowed";
                                                    }
                                                    else if (maritalStatus.StartsWith("D"))
                                                    {
                                                        patientData.MaritalStatus = "Divorced";
                                                    }
                                                    else
                                                    {
                                                        patientData.MaritalStatus = "Unknown";
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.MaritalStatus = "Unknown";
                                                }
                                                sb.Append("MaritalStatus"); sb.Append(": "); sb.Append(patientData.MaritalStatus); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region patientDOB
                                            //DOB - 6
                                            if (dob_col != default(int))
                                            {
                                                var DOBStaging = dataRow.GetValue(dob_col);
                                                if (DOBStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }

                                                //patientData.DOB = DateTime.MinValue;
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }
                                            sb.Append("DOB"); sb.Append(": "); sb.Append(patientData.DOB); sb.Append("\t\t");
                                            #endregion

                                            #region Phone+Addr1
                                            //PHONE HOME - 7
                                            if (phonehome_col != default(int))
                                            {
                                                var phoneHomeStaging = dataRow.GetValue(phonehome_col);
                                                patientData.PhoneHome = phoneHomeStaging.IsNotNullOrEmpty() ? phoneHomeStaging.ToPhoneDB() : string.Empty;
                                                sb.Append("PhoneHome"); sb.Append(": "); sb.Append(patientData.PhoneHome); sb.Append("\t\t");
                                            }
                                            //ADDRESS LINE 1 - 8
                                            if (addr1_col != default(int))
                                            {
                                                var addressLineStaging = dataRow.GetValue(addr1_col);
                                                if (addressLineStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressLine1 = addressLineStaging;
                                                }
                                                sb.Append("AddressLine1"); sb.Append(": "); sb.Append(patientData.AddressLine1); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region City+State+Zip
                                            //ADDRESS CITY - 9
                                            if (city_col != default(int))
                                            {
                                                var addressCityStaging = dataRow.GetValue(city_col);
                                                if (addressCityStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressCity = addressCityStaging;
                                                }
                                                sb.Append("AddressCity"); sb.Append(": "); sb.Append(patientData.AddressCity); sb.Append("\t\t");
                                            }
                                            if (state_col != default(int))
                                            {
                                                //ADDRESS STATE - 10
                                                var addressStateCodeStaging = dataRow.GetValue(state_col).ToUpper().Replace(".", "");
                                                if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressStateCode = addressStateCodeStaging;
                                                }
                                                sb.Append("AddressStateCode"); sb.Append(": "); sb.Append(patientData.AddressStateCode); sb.Append("\t\t");
                                            }
                                            //ADDRESSS ZIP - 11
                                            if (zip_col != default(int))
                                            {
                                                var addressZipCodeStaging = dataRow.GetValue(zip_col);
                                                if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressZipCode = addressZipCodeStaging;
                                                }
                                                sb.Append("AddressZipCode"); sb.Append(": "); sb.Append(patientData.AddressZipCode); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region SOC
                                            //SOC - 13
                                            if (soc_col != default(int))
                                            {
                                                var SOCStaging = dataRow.GetValue(soc_col);
                                                if (SOCStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                                }
                                                else
                                                {
                                                    patientData.StartofCareDate = DateTime.MinValue;
                                                }
                                                sb.Append("StartofCareDate"); sb.Append(": "); sb.Append(patientData.StartofCareDate); sb.Append("\t\t");
                                            }
                                            //
                                            #endregion

                                            #region Triage
                                            if (triage_col != default(int)) 
                                            {
                                                var triageStaging = dataRow.GetValue(triage_col);
                                                if (triageStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Triage = Int32.Parse(triageStaging);
                                                }
                                                sb.Append("Triage"); sb.Append(": "); sb.Append(patientData.Triage); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region Ethnicity
                                            //Ethnicity
                                            if (ethnicity_col != default(int)) 
                                            {
                                                var ethnicityStaging = dataRow.GetValue(ethnicity_col);
                                                if (ethnicityStaging.IsNotNullOrEmpty())
                                                {
                                                    if (ethnicityStaging.ToUpper().Contains("WHITE"))
                                                    {
                                                        //5
                                                        patientData.Ethnicities = "5";
                                                    }
                                                    else if (ethnicityStaging.ToUpper().Contains("BLACK"))
                                                    {
                                                        //2
                                                        patientData.Ethnicities = "2";
                                                    }
                                                    else if (ethnicityStaging.ToUpper().Contains("HISPANIC"))
                                                    {
                                                        //3
                                                        patientData.Ethnicities = "3";
                                                    }
                                                }
                                                sb.Append("Ethnicities"); sb.Append(": "); sb.Append(ethnicityStaging); sb.Append(":"); sb.Append(patientData.Ethnicities); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #endregion

                                            #region COMMENTS

                                            #region PrimaryDiagnosis + rySecondaryDiagnosis

                                            //Primary

                                            if (primaryDiag_col != default(int)) 
                                            {
                                                var primaryDiagStaging = dataRow.GetValue(primaryDiag_col);
                                                if (primaryDiagStaging.IsNotNullOrEmpty()) 
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + primaryDiagStaging + CommentsDelimiter;
                                                }
                                            }

                                            //Secondary
                                            if (secondaryDiag_col != default(int)) 
                                            {
                                                var secondaryDiagStaging = dataRow.GetValue(secondaryDiag_col);
                                                if (secondaryDiagStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Secondary Diagnosis: " + secondaryDiagStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion

                                            #region Discipline + Discipline Frequencies
                                            //Disciplines
                                            if (discipline_col != default(int))
                                            {
                                                var DisciplinesStaging = dataRow.GetValue(discipline_col);
                                                if (DisciplinesStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Disciplines: " + DisciplinesStaging + CommentsDelimiter;
                                                }
                                            }
                                            //Discipline Frequencies
                                            if (disciplineFreq_col != default(int))
                                            {
                                                var DisciplineFrequenciesStaging = dataRow.GetValue(disciplineFreq_col);
                                                if (DisciplineFrequenciesStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Discipline Frequencies: " + DisciplineFrequenciesStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion

                                            #region EPISODE SD + EPISODE ED
                                            //Episode Start Date
                                            if(episodeSD_col!=default(int))
                                            {
                                                var episodeStartDateStaging = dataRow.GetValue(episodeSD_col);
                                                if (episodeStartDateStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Episode Start Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeStartDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                }
                                            }
                                            //

                                            //Episode Start Date
                                            if (episodeED_col != default(int))
                                            {
                                                var episodeEndDateStaging = dataRow.GetValue(episodeED_col);
                                                if (episodeEndDateStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Episode End Date:{0} {1}", DateTime.FromOADate(double.Parse(episodeEndDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                }
                                            }
                                            //
                                            #endregion

                                            #region CaseManager + ReferralSource + ExternalReferral + Insurance
                                            //Case Manager
                                            if (caseManager_col != default(int))
                                            {
                                                var caseManagerStaging = dataRow.GetValue(caseManager_col);
                                                if (caseManagerStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Case Manager: " + caseManagerStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //Referral Source
                                            if (referralSource_col != default(int))
                                            {
                                                var referralSourceStaging = dataRow.GetValue(referralSource_col);
                                                if (referralSourceStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Referral Source: " + referralSourceStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //External Referral
                                            if (externalReferral_col != default(int)) 
                                            {
                                                var externalReferralStaging = dataRow.GetValue(externalReferral_col);
                                                if (externalReferralStaging.IsNotNullOrEmpty()) 
                                                {
                                                    patientData.Comments += "External Referral: " + externalReferralStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //Insurance Information - MANUAL
                                            var insuranceStaging = dataRow.GetValue(insurance_col);
                                            if (insuranceStaging.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;
                                                var insuranceNumber = dataRow.GetValue(4);
                                                if (insuranceNumber.IsNotNullOrEmpty()) 
                                                {
                                                    patientData.Comments += "Insurance Number: " + insuranceNumber + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            #endregion

                                            #region primary Clinician + County + CBSA + Type
                                            //Primary Clinician
                                            if (primaryClinician_col != default(int))
                                            {
                                                var primaryClinicianStaging = dataRow.GetValue(primaryClinician_col);
                                                if (primaryClinicianStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary clinician: " + primaryClinicianStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //Primary physician
                                            if (primaryPhysician_col != default(int))
                                            {
                                                var primaryPhysicianStaging = dataRow.GetValue(primaryPhysician_col);
                                                if (primaryPhysicianStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Physician: " + primaryPhysicianStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //Type
                                            if (type_col != default(int))
                                            {
                                                var typeStaging = dataRow.GetValue(type_col);
                                                if (typeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Type: " + typeStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //County
                                            if (county_col != default(int))
                                            {
                                                var countyStaging = dataRow.GetValue(county_col);
                                                if (countyStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "County: " + countyStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //CBSA
                                            if (CBSA_col != default(int))
                                            {
                                                var CBSAStaging = dataRow.GetValue(CBSA_col);
                                                if (CBSAStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "CBSA: " + CBSAStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            #endregion

                                            patientData.Created = DateTime.Now;
                                            sb.Append("Created"); sb.Append(": "); sb.Append(patientData.Created); sb.Append("\t\t");
                                            patientData.Modified = DateTime.Now;
                                            sb.Append("Modified"); sb.Append(": "); sb.Append(patientData.Modified); sb.Append("\t\t");

                                            if (isPhysicianDataPresent == true)
                                            {
                                                if (isPhysicianDataChecked == true)
                                                {
                                                    #region PhysicianCheck
                                                    var npi = dataRow.GetValue(mdNPI_col);
                                                    if (npi.IsNotNullOrEmpty())
                                                    {
                                                        var physician = Database.GetPhysician(npi, agencyId);
                                                        if (physician == null)
                                                        {
                                                            var info = Database.GetNpiData(npi);
                                                            if (info != null)
                                                            {
                                                                physician = new AgencyPhysician
                                                                {
                                                                    Id = Guid.NewGuid(),
                                                                    AgencyId = agencyId,
                                                                    NPI = npi,
                                                                    LoginId = Guid.Empty,
                                                                    AddressLine1 = string.Empty,
                                                                    AddressCity = string.Empty,
                                                                    AddressStateCode = string.Empty,
                                                                    AddressZipCode = string.Empty,
                                                                    PhoneWork = string.Empty,
                                                                    FaxNumber = string.Empty,
                                                                    Credentials = string.Empty
                                                                };

                                                                #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                if (mdAddr1_col != default(int))
                                                                {
                                                                    var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                    if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressLine1 = mdAddr1Staging;
                                                                    }
                                                                }

                                                                if (mdCity_col != default(int))
                                                                {
                                                                    var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                    if (mdCityStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressCity = mdCityStaging;
                                                                    }
                                                                }

                                                                if (mdState_col != default(int))
                                                                {
                                                                    var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                    if (mdStateStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressStateCode = mdStateStaging;
                                                                    }
                                                                }

                                                                if (mdZip_col != default(int))
                                                                {
                                                                    var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                    if (mdZipStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressZipCode = mdZipStaging;
                                                                    }
                                                                }

                                                                if (mdPhone_col != default(int))
                                                                {
                                                                    var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                    if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.PhoneWork = mdPhoneStaging;
                                                                    }
                                                                }

                                                                if (mdFax_col != default(int))
                                                                {
                                                                    var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                    if (mdFaxStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.FaxNumber = mdFaxStaging;
                                                                    }
                                                                }

                                                                if (mdCred_col != default(int))
                                                                {
                                                                    var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                    if (mdCredStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.Credentials = mdCredStaging;
                                                                    }
                                                                }
                                                                #endregion

                                                                var physicianName = dataRow.GetValue(mdName_col);
                                                                if (physicianName.IsNotNullOrEmpty())
                                                                {
                                                                    var physicianNameArray = physicianName.Trim().Split(new char[] { mdSplitChar }, StringSplitOptions.RemoveEmptyEntries);
                                                                    if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                    {
                                                                        if (physicianNameArray.Length == 3)
                                                                        {
                                                                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                            physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                        }
                                                                        else
                                                                        {
                                                                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        physician.LastName = info.ProviderLastName;
                                                                        physician.FirstName = info.ProviderFirstName;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    physician.LastName = info.ProviderLastName;
                                                                    physician.FirstName = info.ProviderFirstName;
                                                                }
                                                            }
                                                        }
                                                        if (physician != null)
                                                        {
                                                            StringBuilder sb2 = new StringBuilder();
                                                            sb2.Append("<----------------------------->");
                                                            sb2.Append("Phys FN: "); sb2.Append(physician.FirstName); sb2.AppendLine();
                                                            sb2.Append("Phys LN: "); sb2.Append(physician.LastName); sb2.AppendLine();
                                                            sb2.Append("Phys NPI: "); sb2.Append(physician.NPI); sb2.AppendLine();
                                                            sb2.Append("Phys Phone: "); sb2.Append(physician.PhoneWork); sb2.AppendLine();
                                                            sb2.Append("Phys Fax: "); sb2.Append(physician.FaxNumber); sb2.AppendLine();
                                                            sb2.Append("<----------------------------->");
                                                            Console.WriteLine(sb2.ToString());
                                                            sb.Append("Phys FN: "); sb.Append(physician.FirstName); sb.Append("\t\t");
                                                            sb.Append("Phys LN: "); sb.Append(physician.LastName); sb.Append("\t\t");
                                                            sb.Append("Phys NPI: "); sb.Append(physician.NPI); sb.Append("\t\t");
                                                            sb.Append("Phys Phone: "); sb.Append(physician.PhoneWork); sb.Append("\t\t");
                                                            sb.Append("Phys Fax: "); sb.Append(physician.FaxNumber); sb.Append("\t\t");
                                                        }
                                                    }

                                                    #endregion
                                                }
                                            }

                                            if (patientData.LastName.ToUpper().Equals("MAYS") && patientData.FirstName.ToUpper().Equals("RAGEL")) 
                                            {
                                                patientData.Comments += "\n\n";
                                                patientData.Comments += "Additional Information";
                                                patientData.Comments += "Episode Start Date: 08/30/2013; SOC Date: 08/30/2013; Episode End Date: 10/28/2013; Physician : Arvind Nana; NPI: 1679515340; Phone: 8177350474; Fax 8178829730; Referral Source: JPS;";
                                            }

                                            sb.Append("Comments"); sb.Append(": "); sb.Append(patientData.Comments); sb.Append("\t\t");
                                            txtWrtr.WriteLine(sb.ToString());

                                            sb.Length = 0;
                                            txtWrtr.Close();
                                            #endregion

                                            //Check if writing to Dev Database.
                                            //Check the Program.cs for AgencyId, AgencyLocationId
                                            if (isDBWritePermission == true)
                                            {
                                                bool checkExists = default(bool);
                                                Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, agencyId);

                                                if (patientExisting != null)
                                                {
                                                    checkExists = true;
                                                }

                                                if (checkExists != true)
                                                {
                                                    #region DBWrite
                                                    var medicationProfile = new MedicationProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Medication = "<ArrayOfMedication />"
                                                    };

                                                    var allergyProfile = new AllergyProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Allergies = "<ArrayOfAllergy />"
                                                    };

                                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                    {
                                                        var admissionPeriod = new PatientAdmissionDate
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            Created = DateTime.Now,
                                                            DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                            IsActive = true,
                                                            IsDeprecated = false,
                                                            Modified = DateTime.Now,
                                                            PatientData = patientData.ToXml().Replace("'", ""),
                                                            PatientId = patientData.Id,
                                                            Reason = string.Empty,
                                                            StartOfCareDate = patientData.StartofCareDate,
                                                            Status = patientData.Status
                                                        };
                                                        if (Database.Add(admissionPeriod))
                                                        {
                                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                                            if (patient != null)
                                                            {
                                                                patient.AdmissionId = admissionPeriod.Id;
                                                                if (Database.Update(patient))
                                                                {
                                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                    var exists = true;
                                                                    if (isPhysicianDataPresent == true)
                                                                    {
                                                                        #region physicianDBWrite
                                                                        var npi = dataRow.GetValue(mdNPI_col);
                                                                        if (npi.IsNotNullOrEmpty())
                                                                        {
                                                                            var physician = Database.GetPhysician(npi, agencyId);
                                                                            if (physician == null)
                                                                            {
                                                                                exists = false;
                                                                                var info = Database.GetNpiData(npi);
                                                                                if (info != null)
                                                                                {
                                                                                    physician = new AgencyPhysician
                                                                                    {
                                                                                        Id = Guid.NewGuid(),
                                                                                        AgencyId = agencyId,
                                                                                        NPI = npi,
                                                                                        LoginId = Guid.Empty,
                                                                                        AddressLine1 = string.Empty,
                                                                                        AddressCity = string.Empty,
                                                                                        AddressStateCode = string.Empty,
                                                                                        AddressZipCode = string.Empty,
                                                                                        PhoneWork = string.Empty,
                                                                                        FaxNumber = string.Empty,
                                                                                        Credentials = string.Empty
                                                                                    };
                                                                                    #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                                    if (mdAddr1_col != default(int))
                                                                                    {
                                                                                        var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                                        if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressLine1 = mdAddr1Staging;
                                                                                        }
                                                                                    }

                                                                                    if (mdCity_col != default(int))
                                                                                    {
                                                                                        var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                                        if (mdCityStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressCity = mdCityStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdState_col != default(int))
                                                                                    {
                                                                                        var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                                        if (mdStateStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressStateCode = mdStateStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdZip_col != default(int))
                                                                                    {
                                                                                        var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                                        if (mdZipStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressZipCode = mdZipStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdPhone_col != default(int))
                                                                                    {
                                                                                        var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                                        if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.PhoneWork = mdPhoneStaging.ToPhoneDB();
                                                                                        }
                                                                                    }

                                                                                    if (mdFax_col != default(int))
                                                                                    {
                                                                                        var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                                        if (mdFaxStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.FaxNumber = mdFaxStaging.ToPhoneDB();
                                                                                        }
                                                                                    }

                                                                                    if (mdCred_col != default(int))
                                                                                    {
                                                                                        var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                                        if (mdCredStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.Credentials = mdCredStaging;
                                                                                        }
                                                                                    }
                                                                                    #endregion
                                                                                    var physicianName = dataRow.GetValue(mdName_col);
                                                                                    if (physicianName.IsNotNullOrEmpty())
                                                                                    {
                                                                                        var physicianNameArray = physicianName.Trim().Split(new char[] { mdSplitChar }, StringSplitOptions.RemoveEmptyEntries);
                                                                                        if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                                        {
                                                                                            if (physicianNameArray.Length == 3)
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                                                physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                                                physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                                                physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                                            }

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            physician.LastName = info.ProviderLastName;
                                                                                            physician.FirstName = info.ProviderFirstName;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        physician.LastName = info.ProviderLastName;
                                                                                        physician.FirstName = info.ProviderFirstName;

                                                                                    }

                                                                                }

                                                                                Database.Add(physician);

                                                                            }


                                                                            if (physician != null)
                                                                            {
                                                                                var patientPhysician = new PatientPhysician
                                                                                {
                                                                                    IsPrimary = true,
                                                                                    PatientId = patientData.Id,
                                                                                    PhysicianId = physician.Id
                                                                                };

                                                                                if (Database.Add(patientPhysician))
                                                                                {
                                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                                    
                                                }
                     
                                            }
                                            i++;
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("\nTotal Patients:" + totalpatients);
                    Console.WriteLine("\nTotal Counted:" + totalCounter);
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}