﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class AxxessScript4
    {
        private static string input = Path.Combine(App.Root, "Files\\AXXESS.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AXXESS{0}.txt", DateTime.Now.Ticks.ToString()));

        private static string log = Path.Combine(App.Root, string.Format("Files\\Log{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.Gender = "";

                                            TextWriter txtWrtr = new StreamWriter(log, true);
                                            StringBuilder sb = new StringBuilder();
                                            patientData.FirstName = dataRow.GetValue(0);
                                            sb.Append(patientData.FirstName); sb.Append(" | ");

                                            patientData.LastName = dataRow.GetValue(1);
                                            sb.Append(patientData.LastName); sb.Append(" | "); 

                                            patientData.MiddleInitial = dataRow.GetValue(2);
                                            sb.Append(patientData.MiddleInitial); sb.Append(" | "); 

                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            sb.Append(patientData.PatientIdNumber); sb.Append(" | "); 

                                            patientData.MedicareNumber = dataRow.GetValue(4);
                                            sb.Append(patientData.MedicareNumber); sb.Append(" | "); 

                                            DateTime doubtdobt = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            if (doubtdobt != null) 
                                            {
                                                patientData.DOB = doubtdobt;
                                            }
                                            sb.Append(patientData.DOB); sb.Append(" | ");

                                            doubtdobt = new DateTime();
                                            doubtdobt = DateTime.FromOADate(double.Parse(dataRow.GetValue(6)));
                                            if (doubtdobt != null)
                                            {
                                                patientData.StartofCareDate = doubtdobt;
                                            }
                                            sb.Append(patientData.StartofCareDate); sb.Append(" | "); 

                                            patientData.Comments += "Primary Diagnosis: " + dataRow.GetValue(7) + "; ";
                                            sb.Append(patientData.Comments); sb.Append(" | "); 

                                            patientData.AddressLine1 = dataRow.GetValue(8);
                                            sb.Append(patientData.AddressLine1); sb.Append(" | "); 

                                            patientData.AddressCity = dataRow.GetValue(9);
                                            sb.Append(patientData.AddressCity); sb.Append(" | "); 

                                            patientData.AddressStateCode = dataRow.GetValue(10);
                                            sb.Append(patientData.AddressStateCode); sb.Append(" | "); 

                                            patientData.AddressZipCode = dataRow.GetValue(11);
                                            sb.Append(patientData.AddressZipCode); sb.Append(" | ");

                                            patientData.Comments += "Cert Period: " + dataRow.GetValue(16) + "; ";
                                            sb.Append(patientData.Comments); sb.Append(" | "); 

                                            patientData.Comments += "Natural Disaster Code: " + dataRow.GetValue(17) + "; ";
                                            sb.Append(patientData.Comments); sb.Append(" | "); 

                                            patientData.Comments += "Discipline: " + dataRow.GetValue(15) + "; ";
                                            sb.Append(patientData.Comments); sb.Append(" | "); 

                                            patientData.PrimaryInsurance = "1";
                                            sb.Append(patientData.PrimaryInsurance); sb.Append(" | "); 

                                            patientData.Created = DateTime.Now;
                                            sb.Append(patientData.Created); sb.Append(" | "); 

                                            patientData.Modified = DateTime.Now;
                                            sb.Append(patientData.Modified); sb.Append(" | ");

                                            sb.Append("EOR");
                                            sb.AppendLine();
                                            txtWrtr.WriteLine(sb.ToString());
                                            txtWrtr.Close();
                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(13);
                                                            if (npi.IsNotNullOrEmpty())
                                                            {
                                                                var physician = Database.GetPhysician(npi, agencyId);
                                                                if (physician == null)
                                                                {
                                                                    exists = false;
                                                                    var info = Database.GetNpiData(npi);
                                                                    if (info != null)
                                                                    {
                                                                        physician = new AgencyPhysician
                                                                        {
                                                                            Id = Guid.NewGuid(),
                                                                            AgencyId = agencyId,
                                                                            NPI = npi,
                                                                            LoginId = Guid.Empty,
                                                                            //AddressLine1 = dataRow.GetValue(18),
                                                                            //AddressCity = dataRow.GetValue(19),
                                                                            //AddressStateCode = dataRow.GetValue(20).Replace(".", ""),
                                                                            //AddressZipCode = dataRow.GetValue(21),
                                                                            PhoneWork = dataRow.GetValue(14).ToPhoneDB()
                                                                            //FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                                            //Credentials = dataRow.GetValue(16).IsNotNullOrEmpty()?dataRow.GetValue(16):string.Empty
                                                                        };
                                                                        var physicianName = dataRow.GetValue(12);
                                                                        if (physicianName.IsNotNullOrEmpty())
                                                                        {
                                                                            var physicianNameArray = physicianName.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                                            if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                            {
                                                                                if (physicianNameArray.Length == 3)
                                                                                {
                                                                                    physician.LastName = physicianNameArray[2].IsNotNullOrEmpty() ? physicianNameArray[2].Trim() : info.ProviderLastName;
                                                                                    physician.MiddleName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderMiddleName;
                                                                                    physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                                }
                                                                                else
                                                                                {
                                                                                    physician.FirstName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderFirstName;
                                                                                    physician.LastName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                physician.LastName = info.ProviderLastName;
                                                                                physician.FirstName = info.ProviderFirstName;

                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            physician.LastName = info.ProviderLastName;
                                                                            physician.FirstName = info.ProviderFirstName;

                                                                        }
                                                                    }

                                                                    Database.Add(physician);
                                                                }


                                                                if (physician != null)
                                                                {
                                                                    var patientPhysician = new PatientPhysician
                                                                    {
                                                                        IsPrimary = true,
                                                                        PatientId = patientData.Id,
                                                                        PhysicianId = physician.Id
                                                                    };

                                                                    if (Database.Add(patientPhysician))
                                                                    {
                                                                        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}