﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class WizardPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\Physician.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Physician{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty() && dataRow.GetValue(3).IsNotNullOrEmpty())
                                        {
                                            var physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;
                                            if(dataRow.GetValue(13).Trim().IsNotNullOrEmpty())
                                                physicianData.UPIN = dataRow.GetValue(5);
                                            physicianData.NPI = dataRow.GetValue(14);

                                            physicianData.FirstName = dataRow.GetValue(2).Trim();
                                            physicianData.LastName = dataRow.GetValue(3).Trim();
                                            physicianData.Credentials = dataRow.GetValue(1);
                                            if(dataRow.GetValue(4).IsNotNullOrEmpty())
                                                physicianData.MiddleName = dataRow.GetValue(4);

                                            
                                                physicianData.AddressLine1 = dataRow.GetValue(5);
                                            if(dataRow.GetValue(6).IsNotNullOrEmpty())
                                                physicianData.AddressLine2 = dataRow.GetValue(6);
                                                
                                            
                                            physicianData.AddressCity = dataRow.GetValue(7);
                                            physicianData.AddressStateCode = dataRow.GetValue(8);
                                            physicianData.AddressZipCode = dataRow.GetValue(9).Trim();
                                            physicianData.PhoneWork = dataRow.GetValue(11);
                                            physicianData.FaxNumber = dataRow.GetValue(12);
                                            if(dataRow.GetValue(10).Trim().IsNotNullOrEmpty())
                                                physicianData.FaxNumber = dataRow.GetValue(10).ToPhoneDB();
                                            
                                            var stringBuilder = new StringBuilder();
                                            if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                            {
                                                stringBuilder.AppendFormat("Organization: {0}", dataRow.GetValue(22));
                                            }
                                            //if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            //{
                                            //    stringBuilder.AppendFormat("License: {0}. ", dataRow.GetValue(11));
                                            //}
                                            //if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            //{
                                            //    stringBuilder.AppendFormat("Specialty: {0}. ", dataRow.GetValue(12));
                                            //}
                                            //if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                            //{
                                            //    stringBuilder.AppendFormat("License Expiration: {0}.", dataRow.GetValue(15));
                                            //}
                                            physicianData.Comments = stringBuilder.ToString().Trim();

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
