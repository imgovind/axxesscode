﻿namespace Axxess.Membership.Domain
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class DdeCredential : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string CreatedFormatted
        {
            get
            {
                return this.Created.ToShortDateString().ToZeroFilled();
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.UserId.IsNullOrEmpty(), "UserId is required."));
            AddValidationRule(new Validation(() => this.Password.IsNullOrEmpty(), "Password is required."));
        }

        #endregion
    }
}
