﻿namespace Axxess.Membership.Logging
{
    using System;
    using System.Runtime.CompilerServices;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public static class Logger
    {
        private static ILog InternalLog
        {
            get
            {
                return Container.Resolve<ILog>();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Info(string message)
        {
            Check.Argument.IsNotEmpty(message, "message");

            InternalLog.Info(message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Warning(string message)
        {
            Check.Argument.IsNotEmpty(message, "message");

            InternalLog.Warning(message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Error(string message)
        {
            Check.Argument.IsNotEmpty(message, "message");

            InternalLog.Error(message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Exception(Exception exception)
        {
            Check.Argument.IsNotNull(exception, "exception");

            InternalLog.Exception(exception);
        }
    }
}
