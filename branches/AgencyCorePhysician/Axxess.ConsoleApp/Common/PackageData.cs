﻿namespace Axxess.ConsoleApp
{
    using System;

    public class PackageData
    {
        #region Members

        public string SequenceNumber { get; set; }
        public string Code { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", this.SequenceNumber, this.Code, this.Size, this.Type);
        }
        #endregion
    }
}
