﻿using System;
using System.IO;

using iTextSharp;
using iTextSharp.text.pdf;

namespace Axxess.ConsoleApp.Tests
{
    public static class Ub04Document
    {
        private static string template = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), "Templates\\ub04.pdf");
        private static string output = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), string.Format("Files\\ub04_{0}.pdf", DateTime.Now.Ticks.ToString()));

        public static void Create()
        {
            var pdfReader = new PdfReader(template);
            using (FileStream fileStream = new FileStream(output, FileMode.Create))
            {
                var pdfStamper = new PdfStamper(pdfReader, fileStream);
                AcroFields fields = pdfStamper.AcroFields;

                fields.SetField("AgencyName", "Andrew's Home Health Agency");
                fields.SetField("AgencyAddress", "9535 Forest Lane Suite 235");
                fields.SetField("AgencyCityStateZip", "Dallas, TX 75243");

                pdfStamper.FormFlattening = true;
                pdfStamper.Close();
            }
        }
    }
}
