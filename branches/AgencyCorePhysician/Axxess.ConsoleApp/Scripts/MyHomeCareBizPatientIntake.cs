﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;
    using Kent.Boogaart.KBCsv;
    using Axxess.Core.Extension;

    public static class MyHomeCareBizPatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\1stop.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\1stop_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (TextWriter textWriter = new StreamWriter(output, true))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "76961263-e928-4df4-8752-613ff49bd002";
                                patientData.AgencyLocationId = "3ba6d615-968b-48a6-b825-08c4de0f25f0";

                                patientData.PatientNumber = record.GetValue(6);
                                patientData.FirstName = record.GetValue(7).ToTitleCase();
                                patientData.MiddleInitial = record.GetValue(8).ToUpper();
                                patientData.LastName = record.GetValue(9).ToTitleCase();
                                patientData.AddressLine1 = record.GetValue(11);
                                patientData.AddressLine2 = record.GetValue(12);
                                patientData.AddressCity = record.GetValue(13).ToTitleCase();
                                patientData.AddressState = record.GetValue(14);
                                patientData.AddressZipCode = record.GetValue(15);
                                patientData.Phone = record.GetValue(16).ToPhoneDB();
                                patientData.MedicareNumber = record.GetValue(17);
                                patientData.MedicaidNumber = record.GetValue(18);
                                patientData.Gender = record.GetValue(19).IsEqual("2") ? "Female" : "Male";
                                patientData.BirthDate = record.GetValue(20).ToMySqlDateString();
                                patientData.PatientStatusId = "1";

                                if (record.GetValue(21).IsNotNullOrEmpty())
                                {
                                    patientData.PatientStatusId = "2";
                                    patientData.DischargeDate = record.GetValue(21).ToMySqlDateString();
                                }

                                if (record.GetValue(47).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Primary Diagnosis Code: {0}. ", record.GetValue(47));
                                }
                                if (record.GetValue(48).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Other Diagnosis Code 1: {0}. ", record.GetValue(48));
                                }
                                if (record.GetValue(49).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Other Diagnosis Code 2: {0}. ", record.GetValue(49));
                                }
                                if (record.GetValue(50).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Other Diagnosis Code 3: {0}. ", record.GetValue(50));
                                }
                                if (record.GetValue(51).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Other Diagnosis Code 4: {0}. ", record.GetValue(51));
                                }
                                if (record.GetValue(52).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Other Diagnosis Code 5: {0}. ", record.GetValue(52));
                                }

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
