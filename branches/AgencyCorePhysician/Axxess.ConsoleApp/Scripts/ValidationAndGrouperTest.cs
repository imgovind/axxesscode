﻿using System;

using Axxess.Api;

namespace Axxess.ConsoleApp.Tests
{
    public static class ValidationAndGrouperTest
    {
        private static void ValidationAndGrouper(string oasisDataString)
        {
            Console.WriteLine("OASIS Data String: {0}", oasisDataString);
            Console.WriteLine("OASIS Data String Length: {0}", oasisDataString.Length);

            try
            {
                Console.WriteLine("Calling Validation");
                var validationAgent = new ValidationAgent();
                var result = validationAgent.ValidateAssessment(oasisDataString);

                Console.WriteLine("found {0}", result.Count);

                Console.WriteLine("Calling Grouper Logic");
                var grouperAgent = new GrouperAgent();
                Axxess.Api.Contracts.Hipps hipps = grouperAgent.GetHippsCode(oasisDataString);
                Console.WriteLine("hipps code: {0}", hipps.Code);
                Console.WriteLine("hipps version: {0}", hipps.Version);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void Run()
        {
            ValidationAndGrouper("B1          00        C-072009    02.003646497171.0  7003606                                                               679670                 N         CHHS-2098           20110205201108030ANTHONY      WARREN               TX75217      455272098A  04552720980              119620830 111544344390012011080304       010000000000                                                                          401.9 02 250.9102 724.2 02 715.0002 530.8102 564.00020001                                                                   01         00       NA              01      01  0200                                   01  01                                                                                                                                                   174029981702                                                                                    0000                                                                                                                         03   0                                                00    00  0                0100  00013BFKS     V3210                     NA                                                                                                                                                                                                                                                                                                                                       %");
            ValidationAndGrouper("A1678225                   TXC000224       KELLYS HEALTH CARE INC.      2007 N CONWAY                                               MISSION             TX78572      CHAPA, JAYNIE                 9565198118                                                                                                                                                                                      364649717AXXESS AGENCYCORE             9535 FOREST LANE              SUITE 235                     DALLAS              TX75243      NIYI OLAJIDE                  2145757711     2011101111124045984                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             %");
        }
    }
}
