﻿namespace Axxess.Physician.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;

    using Enums;
    using Services;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;
    using Axxess.AgencyManagement;
    using Axxess.OasisC.Domain;
    using Axxess.Physician.App.ViewData;
    using Axxess.Physician.App.Common;
    using Axxess.AgencyManagement.App.iTextExtension;
    using Axxess.Physician.App.Domain;
    using System.Web.UI.WebControls;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrderController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly IAssessmentService assessmentService;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        public OrderController(IAgencyManagementDataProvider agencyManagementDataProvider, IPhysicianService physicianService, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.assessmentService = assessmentService;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Order Actions


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ListHeader()
        {
            ViewData["GroupName"] = "EventDate";

            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListView(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView();
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListGrid(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var agencyid = agencyId.IsEmpty() ? Current.DefaultAgencyId : agencyId;
            return View(new GridModel(physicianService.GetOrders(agencyid,startDate,endDate)));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewCPO()
        {
            return PartialView("CPO/New", null);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddCPO(CarePlanOversight cpo)
        {
            Check.Argument.IsNotNull(cpo, "cpo");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New care plan oversight could not be saved." };
            if (cpo.IsValid)
            {
                cpo.PhysicianLoginId = Current.LoginId;
                var clusterId = Current.Agencies.GetOrDefault(cpo.AgencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    if (patientRepository.AddCPO(cpo, clusterId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Care plan oversight has been saved successfully.";
                    }
                }
            }
            
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgencyPatientList(Guid agencyId)
        {
            var viewData = new List<AgencyPatientViewData>();
            var patients= new List<Patient>();
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId != -1)
            {
                patients = patientRepository.GetPatientByAgencyPhysician(agencyId, clusterId, Current.LoginId);
                if (patients != null && patients.Count > 0)
                {
                    viewData = patients.Select(p => new AgencyPatientViewData { PatientId = p.Id, Name = string.Format("{0} {1}", p.FirstName, p.LastName) }).ToList();
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateCPO([Bind] CarePlanOversight cpo)
        {
            Check.Argument.IsNotNull(cpo, "cpo");

            var viewData = new JsonViewData { errorMessage = "Care plan oversight could not be updated. Please try again.", isSuccessful = false };

            if (cpo.IsValid)
            {
                cpo.PhysicianLoginId = Current.LoginId;
                var clusterId = Current.Agencies.GetOrDefault(cpo.AgencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    if (physicianService.SaveCPO(clusterId, cpo))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Care plan oversight log has been updated successfully.";
                    }
                }
            }
            else
            {
                viewData.errorMessage = cpo.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteCPO(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this oversight. Please try again." };
            if (clusterId != -1)
            {
                if (patientRepository.DeleteCPO(agencyId, clusterId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Care plan oversight was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error happened in deleting the oversight.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = GetPhysicianOrderForPrint(agencyId, patientId, orderId);
            if (order != null)
            {
                return View("Print/Physician", order);
            }
            return View("Print/Physician", new PhysicianOrder());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PhysicianPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var order = GetPhysicianOrderForPrint(agencyId, patientId, orderId);
            var doc = new PhysicianOrderPdf(order);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Order_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        private PhysicianOrder GetPhysicianOrderForPrint(Guid agencyId, Guid patientId, Guid orderId)
        {
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            var order = new PhysicianOrder();
            if (clusterId != -1)
            {
                order = patientRepository.GetOrder(orderId, patientId, agencyId, clusterId);
                if (order != null)
                {
                    order.Patient = patientRepository.Get(order.PatientId, order.AgencyId, clusterId);
                    if (order.Patient != null && order.Patient.AgencyLocationId.IsNotEmpty())
                    {
                        order.Agency = agencyRepository.AgencyNameWithLocationAddress(agencyId, order.Patient.AgencyLocationId, clusterId);
                    }
                    else
                    {
                        order.Agency = agencyRepository.AgencyNameWithMainLocationAddress(agencyId, clusterId);
                    }
                    order.Physician = physicianRepository.Get(order.AgencyId, clusterId, order.PhysicianId);
                    var episode = patientRepository.GetEpisodeById(order.AgencyId, clusterId, order.EpisodeId, order.PatientId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                        order.Allergies = assessmentService.GetAllergies(patientId, agencyId);
                        var diagnosis = assessmentService.GetDiagnoses(episode.AssessmentId, clusterId, episode.AssessmentType, order.AgencyId);
                        if (diagnosis != null && diagnosis.Count > 0)
                        {
                            if (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) order.PrimaryDiagnosisText = diagnosis["M1020PrimaryDiagnosis"].Answer;
                            if (diagnosis.ContainsKey("M1020ICD9M") && diagnosis["M1020ICD9M"].Answer.IsNotNullOrEmpty()) order.PrimaryDiagnosisCode = diagnosis["M1020ICD9M"].Answer;
                            if (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) order.SecondaryDiagnosisText = diagnosis["M1022PrimaryDiagnosis1"].Answer;
                            if (diagnosis.ContainsKey("M1022ICD9M1") && diagnosis["M1022ICD9M1"].Answer.IsNotNullOrEmpty()) order.SecondaryDiagnosisCode = diagnosis["M1022ICD9M1"].Answer;
                        }
                    }
                }
            }
            return order;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanofCareOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var viewData = GetPlanOfCareForPrint(agencyId, episodeId, patientId, orderId);
            if (viewData.PlanOfCare != null)
            {
                return View("Print/485", viewData);

            }
            viewData.PlanOfCare = new PlanofCare();
            return View("Print/485", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PlanofCarePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var viewData = GetPlanOfCareForPrint(agencyId, episodeId, patientId, orderId);
            var doc = new PlanOfCarePdf(viewData.PlanOfCare, viewData.Location, viewData.Patient, viewData.Physician);
            var stream = doc.GetPlanOfCareStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=HCFA-485_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        private PlanOfCarePrintViewData GetPlanOfCareForPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var viewData = new PlanOfCarePrintViewData();
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            PlanofCare planofCare = new PlanofCare();
            if (clusterId != -1)
            {
                planofCare = assessmentService.GetPlanofCare(agencyId, clusterId, episodeId, patientId, orderId);
                if (planofCare != null && planofCare.Data.IsNotNullOrEmpty())
                {
                    planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                    var episode = patientRepository.GetEpisode(planofCare.AgencyId, clusterId, planofCare.EpisodeId, planofCare.PatientId);
                    if (episode != null)
                    {
                        if (planofCare.AssessmentType.IsNotNullOrEmpty() && planofCare.AssessmentType.ToLower().Contains("recert"))
                        {
                            planofCare.EpisodeEnd = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(60).ToShortDateString().ToZeroFilled() : string.Empty;
                            planofCare.EpisodeStart = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(1).ToShortDateString().ToZeroFilled() : string.Empty;
                        }
                        else
                        {
                            planofCare.EpisodeEnd = episode.EndDateFormatted;
                            planofCare.EpisodeStart = episode.StartDateFormatted;
                        }

                        var admission = patientRepository.GetPatientAdmissionDate(agencyId, clusterId, episode.AdmissionId);
                        if (admission != null && admission.PatientData.IsNotNullOrEmpty() && admission.StartOfCareDate > DateTime.MinValue)
                        {
                            viewData.Patient = admission.PatientData.ToObject<Patient>();
                            if (viewData.Patient != null)
                            {
                                viewData.Patient.StartofCareDate = admission.StartOfCareDate;
                            }
                        }
                    }
                    if (viewData.Patient == null)
                    {
                        viewData.Patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId, clusterId);
                    }
                    if (viewData.Patient != null && viewData.Patient.AgencyLocationId.IsNotEmpty() && clusterId != -1)
                    {
                        viewData.Location = agencyRepository.AgencyNameWithLocationAddress(agencyId, viewData.Patient.AgencyLocationId, clusterId);
                    }

                    if (!planofCare.PhysicianId.IsEmpty())
                    {
                        viewData.Physician = physicianRepository.Get(planofCare.AgencyId, clusterId, planofCare.PhysicianId);
                    }
                    else
                    {
                        if (planofCare.PhysicianData.IsNotNullOrEmpty())
                        {
                            var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                            {
                                viewData.Physician = physicianRepository.Get(planofCare.AgencyId, clusterId, oldPhysician.Id);
                            }
                        }
                    }
                    if (viewData.Patient == null || viewData.Patient.AgencyLocationId.IsEmpty())
                    {
                        viewData.Location = agencyRepository.AgencyNameWithMainLocationAddress(agencyId, clusterId);
                    }
                }
            }
            viewData.PlanOfCare = planofCare;
            return viewData;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CarePlanOversights(Guid agencyId, Guid id)
        {
            var cpo= new CarePlanOversight();
            var agencies = Current.Agencies;
            if (agencies.ContainsKey(agencyId.ToString()))
            {
                var agency = agencies[agencyId.ToString()];
                if (agency != null)
                {
                    cpo = physicianRepository.GetCPOById(agencyId, agency.ClusterId, id);
                    if (cpo == null)
                    {
                        cpo = new CarePlanOversight();
                    }
                }
            }
            return View("CPO/Edit", cpo);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid id)
        {
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            var order = new FaceToFaceEncounter();
            if (clusterId != -1)
            {
                order = patientRepository.GetFaceToFaceEncounter(id, patientId, agencyId, clusterId);
                if (order != null)
                {
                    //order.Agency = agencyRepository.Get(agencyId, clusterId);
                    order.Patient = patientRepository.Get(order.PatientId, agencyId, clusterId);
                    order.Physician = physicianRepository.Get(agencyId, clusterId, order.PhysicianId);
                    var episode = patientRepository.GetEpisodeById(agencyId, clusterId, order.EpisodeId, order.PatientId);
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
                else
                {
                    order = new FaceToFaceEncounter();
                    //order.Agency = agencyRepository.Get(agencyId, clusterId);
                    order.Patient = patientRepository.Get(patientId, agencyId, clusterId);
                }
            }
            return View("FaceToFace/Edit", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SendCpoBill(string list)
        {
            Check.Argument.IsNotNull(list, "list");
            var viewData = new JsonViewData { errorMessage = "The cpo bill could not be sent. Please try again.", isSuccessful = false };
            
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateFaceToFace([Bind] FaceToFaceEncounter faceToFace)
        {
          
            var viewData = new JsonViewData { errorMessage = "Face-To-Face Encounter could not be updated. Please try again.", isSuccessful = false };
            if (faceToFace.IsValid)
            {
                if (physicianService.UpdateFaceToFaceEncounter(faceToFace))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Face-To-Face Encounter has been updated successfully.";
                }
            }
            else
            {
                viewData.errorMessage = faceToFace.ValidationMessage;
            }
        
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddReturnReason(Guid agencyId, Guid orderId, Guid episodeId, Guid patientId, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };

            if (physicianService.AddReturnComments(orderId, episodeId, comment, agencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReturnReason(Guid agencyId, Guid orderId, Guid episodeId, Guid patientId)
        {
            var viewData = new JsonViewData();
            var comments = physicianService.GetReturnComments(agencyId, orderId, episodeId, patientId);
            if (comments.IsNotNullOrEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = comments;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CpoListHeader()
        {
            ViewData["GroupName"] = "LogDate";
            return PartialView("CPO/ListHeader");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CpoListView(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView("CPO/ListView");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CPOGrid(Guid agencyId,DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(physicianService.GetPhysicianCPO(agencyId, startDate, endDate)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceList()
        {
            return PartialView("FaceToFace/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceGrid(Guid agencyId, DateTime startDate, DateTime endDate, string orderBy)
        {
            var agencyid = agencyId.IsEmpty() ? Current.DefaultAgencyId : agencyId;
            //return View(new GridModel(physicianService.GetFaceToFaceEncounters(agencyid, startDate, endDate)));
            var faceTofaceEncounters = physicianService.GetFaceToFaceEncounters(agencyid, startDate, endDate);
            if (orderBy.IsNotNullOrEmpty())
            {
                string sortPram = orderBy.Split('-')[0].Trim();
                string sortOrder = orderBy.Split('-')[1].Trim();
                faceTofaceEncounters = faceTofaceEncounters.AsQueryable().OrderByDynamic(sortPram, sortOrder == "asc" ? SortDirection.Ascending : SortDirection.Descending).ToList();
            }
            else
            {
                faceTofaceEncounters = faceTofaceEncounters.OrderByDescending(e => e.RequestDateFormatted).ToList();
            }
            return new LargeJsonResult
            {
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new GridModel<FaceToFaceEncounter>
                {
                    Data = faceTofaceEncounters ?? new List<FaceToFaceEncounter>()
                }
            };
            
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceEncounterPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = GetFaceToFaceForPrint(agencyId, episodeId, patientId, orderId);
            var xml = new PhysFaceToFaceXml(order);
            order.PrintViewJson = xml.GetJson();
            return View("Print/FaceToFace", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult FaceToFacePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var doc = new PhysFaceToFacePdf(GetFaceToFaceForPrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=FaceToFace_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        private FaceToFaceEncounter GetFaceToFaceForPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = new FaceToFaceEncounter();
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId != -1)
            {
                order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId, clusterId);
                if (order != null)
                {
                    order.Patient = patientRepository.Get(order.PatientId, agencyId, clusterId);
                    if (order.Patient != null && order.Patient.AgencyLocationId.IsNotEmpty())
                    {
                        order.Agency = agencyRepository.AgencyNameWithLocationAddress(agencyId, order.Patient.AgencyLocationId, clusterId);
                    }
                    else
                    {
                        order.Agency = agencyRepository.AgencyNameWithMainLocationAddress(agencyId, clusterId);
                    }
                    order.Physician = physicianRepository.Get(agencyId, clusterId, order.PhysicianId);
                    var episode = patientRepository.GetEpisodeByIdWithSOC(agencyId, clusterId, order.EpisodeId, order.PatientId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                        if (order.Patient != null)
                        {
                            order.Patient.StartofCareDate = episode.StartOfCareDate;
                        }
                    }
                    if (order.Patient != null)
                    {
                        order.Location = agencyRepository.FindLocation(agencyId, clusterId, order.Patient.AgencyLocationId);
                    }
                }
                else
                {
                    order = new FaceToFaceEncounter();
                    order.Agency = agencyRepository.AgencyNameWithMainLocationAddress(agencyId, clusterId);
                    order.Patient = patientRepository.Get(patientId, agencyId, clusterId);
                }
            }
            return order;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWEval(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            return View("Print/MSWEval", assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTEval(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var OTVisitNoteViewData=assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView(string.Format("Print/OTEval{0}",OTVisitNoteViewData!=null && OTVisitNoteViewData.Version>0?OTVisitNoteViewData.Version:1), OTVisitNoteViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTDischarge(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var OTDischargeNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView(string.Format("Print/OTDischarge{0}", OTDischargeNote.Version > 1 ? OTDischargeNote.Version.ToString() : string.Empty), OTDischargeNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTDischargePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischarge(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var PTDischargeNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView(string.Format("Print/PTDischarge{0}",PTDischargeNote.Version>1?PTDischargeNote.Version.ToString():string.Empty), PTDischargeNote);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTReassessmentPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var PTReassessmentPrint = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView("Print/PTReassessment", PTReassessmentPrint);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTReassessmentPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTDischargePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTReassessmentPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var OTReassessmentPrint = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView("Print/OTReassessment", OTReassessmentPrint);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTReassessmentPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummary(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var note = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            var xml = new VisitNoteXml(note, PdfDocs.SixtyDaySummary);
            note.PrintViewJson = xml.GetJson();
            return View("Print/SixtyDaySummaryPrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SixtyDaySummaryPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            VisitNoteViewData viewdata = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            var doc = new SixtyDaySummaryPdf(viewdata);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTEval(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var PTEvaluationNote=assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            
            return PartialView(string.Format("Print/PTEval{0}",PTEvaluationNote!=null&&PTEvaluationNote.Version>0?PTEvaluationNote.Version:1), PTEvaluationNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PsychAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var PsychAssessmentNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);

            return PartialView(string.Format("Print/PsychAssessment{0}", PsychAssessmentNote != null && PsychAssessmentNote.Version > 0 ? PsychAssessmentNote.Version.ToString() : string.Empty), PsychAssessmentNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PsychAssessmentPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new PsychAssessmentPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId),PdfDocs.PsychAssessment2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PsychAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTPOC(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var PTPOCNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);

            return PartialView(string.Format("Print/PTPOC{0}", PTPOCNote != null && PTPOCNote.Version > 1 ? PTPOCNote.Version.ToString() : string.Empty), PTPOCNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTPOCPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTPOC_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTPOC(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var OTPOCNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);

            return PartialView(string.Format("Print/OTPOC{0}", OTPOCNote != null && OTPOCNote.Version > 1 ? OTPOCNote.Version.ToString() : string.Empty), OTPOCNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTPOCPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTPOC_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STPOC(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var STPOCNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);

            return PartialView(string.Format("Print/STPOC{0}", STPOCNote != null && STPOCNote.Version > 1 ? STPOCNote.Version.ToString() : string.Empty), STPOCNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STPOCPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STPOC_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STDischarge(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var STDischargeNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);

            return PartialView(string.Format("Print/STDischarge{0}", STDischargeNote != null && STDischargeNote.Version > 1 ? STDischargeNote.Version : 1), STDischargeNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STDischargePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STEval(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var STEvaluationNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId);
            return PartialView(string.Format("Print/STEval{0}", STEvaluationNote != null && STEvaluationNote.Version > 0 ? STEvaluationNote.Version : 1), STEvaluationNote);
            //return PartialView("Print/STEval", assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(string action, Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, int orderType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This order could not been updated." };
            if (orderType == (int)OrderType.PhysicianOrder)
            {
                if (physicianService.UpdatePhysicianOrder(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.HCFA485 || orderType == (int)OrderType.HCFA485StandAlone)
            {
                if (assessmentService.UpdatePlanofCare(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The plan of care has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.FaceToFaceEncounter)
            {
                if (physicianService.UpdateFaceToFaceEncounter(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Face-to-Face encounter has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.PtEvaluation
                || orderType == (int)OrderType.PtReEvaluation
                || orderType == (int)OrderType.PTReassessment
                || orderType == (int)OrderType.StEvaluation
                || orderType == (int)OrderType.StReEvaluation
                || orderType == (int)OrderType.OtEvaluation
                || orderType == (int)OrderType.OtReEvaluation
                || orderType == (int)OrderType.OTReassessment
                || orderType == (int)OrderType.OTDischarge
                || orderType == (int)OrderType.PTDischarge
                || orderType == (int)OrderType.SixtyDaySummary
                || orderType == (int)OrderType.MSWEvaluation
                || orderType == (int)OrderType.MSWDischarge
                || orderType == (int)OrderType.STDischarge
                || orderType == (int)OrderType.SNPsychAssessment
                || orderType == (int)OrderType.PTPlanOfCare
                || orderType == (int)OrderType.OTPlanOfCare
                || orderType == (int)OrderType.STPlanOfCare
                )
            {
                if (physicianService.UpdateEvalOrder(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Eval has been updated successfully.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CompleteList()
        {
            return PartialView("List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompleteGrid(Guid agencyId, DateTime startDate, DateTime endDate, string orderBy)
        {
            //return View(new GridModel(physicianService.GetOrdersCompleted(agencyId, startDate, endDate)));
            var completedOrders = physicianService.GetOrdersCompleted(agencyId, startDate, endDate);
            if (orderBy.IsNotNullOrEmpty())
            {
                string sortPram = orderBy.Split('-')[0].Trim();
                string sortOrder = orderBy.Split('-')[1].Trim();
                completedOrders = completedOrders.AsQueryable().OrderByDynamic(sortPram, sortOrder == "asc" ? SortDirection.Ascending : SortDirection.Descending).ToList();
            }
            else
            {
                completedOrders = completedOrders.OrderByDescending(e =>e.OrderDate).ToList();
            }
            return new LargeJsonResult
            {
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new GridModel<Order>
                {
                    Data = completedOrders ?? new List<Order>()
                }
            };
        }

        #endregion
    }
}
