﻿

namespace Axxess.Physician.App.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public class PrintHelper
    {
        public static string AgencyAddress(LocationPrintProfile location)
        {
            return (
                            location != null ?
                                (location.Name.IsNotNullOrEmpty() ? location.Name + "\n" : String.Empty) +
                                (
                                    (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                                    (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                                    (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                                    (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                                    (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                                    (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                                    (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                                )
                            : String.Empty);
        }

        public static string AgencyAddressWithContact(LocationPrintProfile location)
        {
            return (
                            location != null ?
                                (location.Name.IsNotNullOrEmpty() ? location.Name + "\n" : String.Empty) +
                                (
                                    (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                                    (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                                    (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                                    (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                                    (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                                    (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                                    (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty) +
                                    (location.ContactPersonEmail.IsNotNullOrEmpty() ? "\nEmail: " + location.ContactPersonEmail : string.Empty)
                                )
                            : String.Empty);
        }

        public static string PatientName(Patient patient)
        {
            return (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() + "\n" : "\n");
        }

        public static string PhysicianAddress(AgencyPhysician physician)
        {
            return (
                            physician != null ?
                                (physician.AddressLine1.IsNotNullOrEmpty() ? physician.AddressLine1.ToTitleCase() : string.Empty) +
                                (physician.AddressLine2.IsNotNullOrEmpty() ? physician.AddressLine2.ToTitleCase() + "\n" : "\n") +
                                (physician.AddressCity.IsNotNullOrEmpty() ? physician.AddressCity.ToTitleCase() + ", " : string.Empty) +
                                (physician.AddressStateCode.IsNotNullOrEmpty() ? physician.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                                (physician.AddressZipCode.IsNotNullOrEmpty() ? physician.AddressZipCode : string.Empty)
                            : string.Empty);
        }

        public static string PhysicianAddressWithNPI(AgencyPhysician physician)
        {
            return (physician != null ?
                (physician.AddressLine1.IsNotNullOrEmpty() ? physician.AddressLine1.ToTitleCase() : "") + (physician.AddressLine2.IsNotNullOrEmpty() ? ", " + physician.AddressLine2.ToTitleCase() + "\n" : "\n") + (physician.AddressCity.IsNotNullOrEmpty() ? physician.AddressCity.ToTitleCase() + ", " : "") + (physician.AddressStateCode.IsNotNullOrEmpty() ? physician.AddressStateCode.ToTitleCase() + "  " : "") + (physician.AddressZipCode.IsNotNullOrEmpty() ? physician.AddressZipCode + "\n" : "") + (physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + physician.PhoneWork.ToPhone() : "") + (physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + physician.FaxNumber.ToPhone() + "\n" : "\n") + (physician.NPI.IsNotNullOrEmpty() ? "NPI: " + physician.NPI : "")
                : "");
        }

        public static string PatientAddress(Patient patient)
        {
            return (patient != null ?
                    (patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : string.Empty) +
                    (patient.AddressLine2.IsNotNullOrEmpty() ? patient.AddressLine2.ToTitleCase() + "\n" : "\n") +
                    (patient.AddressCity.IsNotNullOrEmpty() ? patient.AddressCity.ToTitleCase() + ", " : string.Empty) +
                    (patient.AddressStateCode.IsNotNullOrEmpty() ? patient.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty) +
                    (patient.PhoneHome.IsNotNullOrEmpty() ? "\nPhone: " + patient.PhoneHome.ToPhone() : string.Empty)
                : string.Empty);
        }
    }
}
