﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.Physician.App.Helpers
{
    public static class  ScheduleHelper
    {

        public static void SetSchedule( List<ScheduleEvent> events, ScheduleEvent scheduleEventInput)
        {
            if (events != null && events.Count > 0)
            {
                events.ForEach(e =>
                {
                    if (e.EventId == scheduleEventInput.EventId)
                    {
                        e.Assets = scheduleEventInput.Assets;
                        e.TimeIn = scheduleEventInput.TimeIn;
                        e.Status = scheduleEventInput.Status;
                        e.UserId = scheduleEventInput.UserId;
                        e.TimeOut = scheduleEventInput.TimeOut;
                        e.Comments = scheduleEventInput.Comments;
                        e.IsPayable = scheduleEventInput.IsPayable;
                        e.EventDate = scheduleEventInput.EventDate;
                        e.Surcharge = scheduleEventInput.Surcharge;
                        e.VisitDate = scheduleEventInput.VisitDate;
                        e.Discipline = scheduleEventInput.Discipline;
                        e.IsBillable = scheduleEventInput.IsBillable;
                        e.IsPayable = scheduleEventInput.IsPayable;
                        e.GCode = scheduleEventInput.GCode;
                        e.IsVisitPaid = scheduleEventInput.IsVisitPaid;
                        e.SendAsOrder = scheduleEventInput.SendAsOrder;
                        e.ReturnReason = scheduleEventInput.ReturnReason;
                        e.TravelTimeIn = scheduleEventInput.TravelTimeIn;
                        e.IsDeprecated = scheduleEventInput.IsDeprecated;
                        e.InPrintQueue = scheduleEventInput.InPrintQueue;
                        e.IsMissedVisit = scheduleEventInput.IsMissedVisit;
                        e.TravelTimeOut = scheduleEventInput.TravelTimeOut;
                        e.DisciplineTask = scheduleEventInput.DisciplineTask;
                        e.ServiceLocation = scheduleEventInput.ServiceLocation;
                        e.PersonalComments = scheduleEventInput.PersonalComments;
                        e.AssociatedMileage = scheduleEventInput.AssociatedMileage;
                        e.MissedVisitFormReturnReason = scheduleEventInput.MissedVisitFormReturnReason;
                    }
                });
            }
        }
    }
}
