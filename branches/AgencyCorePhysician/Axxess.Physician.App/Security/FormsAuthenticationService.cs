﻿namespace Axxess.Physician.App.Security
{
    using System;
    using System.Web;
    using System.Web.Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        #region IFormsAuthenticationService Members

        public string LoginUrl
        {
            get
            {
                return FormsAuthentication.LoginUrl;
            }
        }

        public void SignIn(string userName, bool rememberMe)
        {
            FormsAuthentication.Initialize();
            FormsAuthentication.SetAuthCookie(userName, false);

            if (rememberMe)
            {
                var cookie = HttpContext.Current.Request.Cookies[PhysicianAppSettings.RememberMeCookie];
                if (cookie != null && userName.IsEqual(Crypto.Decrypt(cookie.Value)))
                {
                    cookie.Expires = DateTime.Now.AddDays(PhysicianAppSettings.RememberMeForTheseDays);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
                else
                {
                    HttpCookie newCookie = new HttpCookie(PhysicianAppSettings.RememberMeCookie);
                    newCookie.Expires = DateTime.Now.AddDays(PhysicianAppSettings.RememberMeForTheseDays);
                    newCookie.Value = Crypto.Encrypt(userName);
                    HttpContext.Current.Response.Cookies.Add(newCookie);
                }
            }
            else
            {
                var cookie = HttpContext.Current.Request.Cookies[PhysicianAppSettings.RememberMeCookie];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
            }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        #endregion
    }
}
