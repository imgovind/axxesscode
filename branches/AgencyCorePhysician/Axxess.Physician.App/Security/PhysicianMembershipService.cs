﻿namespace Axxess.Physician.App.Security
{
    using System;
    using System.Web;
    using System.Text;
    using System.Threading;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    public class PhysicianMembershipService : IPhysicianMembershipService
    {
        #region Private Members

        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ISupportRepository supportRepository;
        private readonly IPhysicianRepository physicianRepository;

        #endregion

        #region Constructor

        public PhysicianMembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.physicianRepository = agencyManagementProvider.PhysicianRepository;
        }

        #endregion

        #region IMembershipService Members

        public bool Activate(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string passwordSalt = string.Empty;
                string passwordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                login.PasswordSalt = passwordSalt;
                login.SignatureSalt = passwordSalt;
                login.PasswordHash = passwordHash;
                login.SignatureHash = passwordHash;
                login.LastLoginDate = DateTime.Now;
                if (loginRepository.Update(login))
                {
                    AxxessPhysicianPrincipal principal = Get(login.EmailAddress);
                    if (principal != null)
                    {
                        account.UserName = login.EmailAddress;
                        return true;
                    }
                }
            }

            return false;
        }

        public AxxessPhysicianPrincipal Get(string userName)
        {
            ImpersonationLink link = null;
            var principal = Cacher.Get<AxxessPhysicianPrincipal>(userName);
            if (principal == null)
            {
                string[] userNameArray = userName.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                if (userNameArray != null && userNameArray.Length > 1)
                {
                    userName = userNameArray[0];
                    link = supportRepository.GetImpersonationLink(userNameArray[1].ToGuid());
                }

                var login = loginRepository.Find(userName);
                if (login != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.Physician);
                    if (role == Roles.Physician)
                    {
                        var physicianList = physicianRepository.GetAgencyPhysiciansByLoginIdLean(login.Id);
                        if (physicianList != null && physicianList.Count > 0)
                        {
                            var identity = new AxxessPhysicianIdentity(login.Id, link != null ? string.Format("{0}:{1}", login.EmailAddress, link.Id) : login.EmailAddress);
                            
                            var session= new PhysicianSession
                                {
                                    LoginId = login.Id,
                                    DisplayName = login.DisplayName,
                                    Agencies = physicianList
                                    //AgencyPhysicianIdentifiers = GetPhysicianIdentifiers(physicianList)
                                };
                            if (session.Agencies.Count > 0)
                            {
                                session.DefaultAgencyId = session.Agencies.FirstOrDefault().Key.ToGuid();
                            }
                            identity.Session = session;

                            if (link != null)
                            {
                                identity.IsImpersonated = true;
                                identity.Session.ImpersonatorName = link.RepName;
                                userName = string.Format("{0}:{1}", login.EmailAddress, link.Id);
                            }
                            principal = new AxxessPhysicianPrincipal(identity);
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessPhysicianPrincipal>(userName, principal);
                        }
                    }
                }
            }

            return principal;
        }

        public LoginAttemptType Validate(string userName, string password)
        {
            var loginAttempt = LoginAttemptType.Failed;
            var login = loginRepository.Find(userName);

            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(password.Trim(), login.PasswordHash, login.PasswordSalt))
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.Physician);
                    if (role == Roles.Physician)
                    {
                        var physicianList = physicianRepository.GetAgencyPhysiciansByLoginIdLean(login.Id);
                        if (physicianList != null && physicianList.Count > 0)
                        {
                            var identity = new AxxessPhysicianIdentity(login.Id, login.EmailAddress);
                             var session = new PhysicianSession
                                {
                                    LoginId = login.Id,
                                    DisplayName = login.DisplayName,
                                    Agencies=physicianList
                                };

                            //var agencies = agencyRepository.AgencyWithNameOnly(physicianList.Where(p => !p.AgencyId.IsEmpty()).Select(p => p.AgencyId).ToList());
                            //session.DefaultAgencyId = agencies.FirstOrDefault().Id;
                            //session.Agencies = agencies.GroupBy(p => p.Id).ToDictionary(p => p.Key, v => v.FirstOrDefault().Name);
                            if (session.Agencies.Count > 0)
                            {
                                session.DefaultAgencyId = session.Agencies.FirstOrDefault().Key.ToGuid();
                            }
                            identity.Session = session;

                            login.LastLoginDate = DateTime.Now;
                            if (loginRepository.Update(login))
                            {
                                loginAttempt = LoginAttemptType.Success;
                            }

                            var principal = new AxxessPhysicianPrincipal(identity);
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessPhysicianPrincipal>(userName, principal);
                        }
                    }
                }
            }

            return loginAttempt;
        }

        public void LogOff(string emailAddress)
        {
            SessionStore.Abandon();
            Cacher.Remove(emailAddress);
        }

        public ResetAttemptType Validate(string userName)
        {
            var resetAttempt = ResetAttemptType.Failed;

            var login = loginRepository.Find(userName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    resetAttempt = ResetAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        resetAttempt = ResetAttemptType.Success;
                    }
                    else
                    {
                        resetAttempt = ResetAttemptType.Deactivated;
                    }
                }
            }

            return resetAttempt;
        }

        public bool ResetPassword(string userName)
        {
            var login = loginRepository.Find(userName);
            if (login != null && login.IsAxxessAdmin == false && login.IsAxxessSupport == false)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "PasswordResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, userName, "Reset Password - Axxess Home Health Management Software", bodyText);
                return true;
            }
            return false;
        }

        public bool UpdatePassword(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newPasswordSalt = string.Empty;
                string newPasswordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out newPasswordHash, out newPasswordSalt);
                login.PasswordSalt = newPasswordSalt;
                login.PasswordHash = newPasswordHash;
                if (loginRepository.Update(login))
                {
                    return true;
                }
            }

            return false;
        }

        public bool Impersonate(Guid linkId)
        {
            var result = false;

            var link = supportRepository.GetImpersonationLink(linkId);
            if (link != null && link.IsUsed == false && DateTime.Now < link.Created.AddMinutes(1))
            {
                var login = loginRepository.Find(link.LoginId);
                if (login != null && login.Role.IsEqual(Roles.Physician.ToString()))
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.Physician);
                    if (role == Roles.Physician)
                    {
                        var physicianList = physicianRepository.GetAgencyPhysiciansByLoginIdLean(login.Id);
                        if (physicianList != null && physicianList.Count > 0)
                        {
                            var identity = new AxxessPhysicianIdentity(login.Id, string.Format("{0}:{1}", login.EmailAddress, link.Id));
                            identity.IsImpersonated = true;
                            var session = new PhysicianSession
                            {
                                LoginId = login.Id,
                                DisplayName = login.DisplayName,
                                Agencies=physicianList
                                //AgencyPhysicianIdentifiers = GetPhysicianIdentifiers(physicianList),
                            };
                            //var agencies = agencyRepository.AgencyWithNameOnly(physicianList.Where(p => !p.AgencyId.IsEmpty()).Select(p => p.AgencyId).ToList());
                            //session.DefaultAgencyId = agencies.FirstOrDefault().Id;
                            //session.Agencies = agencies.GroupBy(p=>p.Id).ToDictionary(p => p.Key,v=>v.FirstOrDefault().Name);
                            if (session.Agencies.Count > 0)
                            {
                                session.DefaultAgencyId = session.Agencies.FirstOrDefault().Key.ToGuid();
                            }
                            identity.Session = session;

                            var principal = new AxxessPhysicianPrincipal(identity);
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessPhysicianPrincipal>(string.Format("{0}:{1}", login.EmailAddress, link.Id), principal);

                            link.IsUsed = true;
                            if (supportRepository.UpdateImpersonationLink(link))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        #endregion

        #region Private Methods

        private List<Guid> GetPhysicianIdentifiers(IList<AgencyPhysician> physicianList)
        {
            var idList = new List<Guid>();

            physicianList.ForEach(p =>
            {
                idList.Add(p.Id);
            });

            return idList;
        }

        #endregion

    }
}
