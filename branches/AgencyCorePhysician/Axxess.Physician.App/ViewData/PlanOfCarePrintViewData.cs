﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.OasisC.Domain;
using Axxess.AgencyManagement.Domain;

namespace Axxess.Physician.App
{
    public class PlanOfCarePrintViewData
    {
        public PlanofCare PlanOfCare { get; set; }
        public LocationPrintProfile Location { get; set; }
        public AgencyPhysician Physician { get; set; }
        public Patient Patient { get; set; }
    }
}
