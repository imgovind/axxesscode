﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Physician.App.ViewData;
    using Axxess.Physician.App.Extensions;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;
    using Axxess.Physician.App.Helpers;

    public class AssessmentService : IAssessmentService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IOasisCDataProvider oasisDataProvider;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(oasisDataProvider, "dataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.oasisDataProvider = oasisDataProvider;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region IAssessmentService Members

        public VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var note = new VisitNoteViewData();
            var agency = new Agency();
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, clusterId, episodeId, patientId, orderId);
            if (patientvisitNote != null)
            {
                note.SignatureText = patientvisitNote.SignatureText;
                note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                note.PhysicianSignatureText = patientvisitNote.PhysicianSignatureText;
                note.PhysicianSignatureDate = patientvisitNote.PhysicianSignatureDate;
                note.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(agencyId, clusterId, episodeId, patientId);
                if (episode != null)
                {
                    var scheduledEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == orderId);//patientRepository.GetSchedule(agencyId, clusterId, episodeId, patientId, orderId);
                    if (scheduledEvent != null)
                    {
                        note.DisciplineTask = scheduledEvent.DisciplineTask;
                    }
                    var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                    if (!selectedEpisodeId.IsEmpty())
                    {
                        var selectedEpisode = patientRepository.GetEpisodeById(agencyId, clusterId, selectedEpisodeId, patientId);
                        if (episode != null)
                        {
                            note.EndDate = selectedEpisode.EndDate;
                            note.StartDate = selectedEpisode.StartDate;
                        }
                    }
                    else
                    {

                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                        note.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                    }
                    note.PatientId = patientvisitNote.PatientId;
                    note.EpisodeId = patientvisitNote.EpisodeId;
                    note.EventId = patientvisitNote.Id;
                    note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    note.Version = patientvisitNote.Version;
                }
            }
            else note.Questions = new Dictionary<string, NotesQuestion>();
            var patient = patientRepository.Get(patientId, agencyId, clusterId);
            note.Patient = patient;
            if (patient != null)
            {
                if (patient.AgencyLocationId.IsNotEmpty() && clusterId != -1)
                {
                    note.Agency = agencyRepository.AgencyNameWithLocationAddress(agencyId, patient.AgencyLocationId, clusterId);
                }
                //if (note.Questions != null && note.Questions.ContainsKey("Physician") && note.Questions["Physician"].Answer.IsNotNullOrEmpty() && note.Questions["Physician"].Answer.IsGuid()) {
                //    var physician = physicianRepository.Get(note.Questions["Physician"].Answer.ToGuid(), agencyId);
                //    if (physician != null) {
                //        note.PhysicianId = physician.Id;
                //        note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                //    }
                //} else {
                //    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0) {
                //        var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                //        if (physician != null) {
                //            note.PhysicianId = physician.Id;
                //            note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                //        }
                //    }
                //}
                AgencyPhysician physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    note.PhysicianId = physician.Id;
                    note.PhysicianDisplayName = physician.DisplayName;
                }
                else if (!patientvisitNote.PhysicianId.IsEmpty())
                {
                    physician = physicianRepository.Get(agencyId, clusterId, patientvisitNote.PhysicianId);
                    if (physician != null)
                    {
                        note.PhysicianDisplayName = physician.DisplayName;
                    }
                }
                if (note.Type != null && (note.Type == DisciplineTasks.PTPlanOfCare.ToString() || note.Type == DisciplineTasks.OTPlanOfCare.ToString() || note.Type == DisciplineTasks.STPlanOfCare.ToString()
                    || note.Type == DisciplineTasks.SNPsychAssessment.ToString()))
                {
                    note.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
                }
            }
            if(clusterId != -1 && (patient == null || patient.AgencyLocationId.IsEmpty()))
            {
                note.Agency = agencyRepository.AgencyNameWithMainLocationAddress(agencyId, clusterId);
            }
            return note;
        }

        
        private Assessment GetAssessment(Guid assessmentId, int clusterId, string assessmentType, Guid agencyId)
        {
            if (clusterId>0)
            {
                return oasisDataProvider.AssessmentRepository.Get(assessmentId, assessmentType, agencyId, clusterId);
            }
            else
            {
                return new Assessment();
            }
        }

        public PlanofCare GetPlanofCare(Guid agencyId, int clusterId, Guid episodeId, Guid patientId, Guid planofcareId)
        {
            var standAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, clusterId, episodeId, patientId, planofcareId);
            if(standAlone != null)
            {
                return standAlone.ToPlanofCare();
            }
            else
            {
                return oasisDataProvider.PlanofCareRepository.Get(agencyId, clusterId, episodeId, patientId, planofcareId);
            }
        }
        
        public string GetAllergies(Guid patientId, Guid agencyId)
        {
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId != -1)
            {
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId, clusterId);
                if (allergyProfile != null)
                {
                    return allergyProfile.ToString();
                }
            }
            return string.Empty;
        }

        public IDictionary<string, Question> GetDiagnoses(Guid assessmentId, int clusterId, string AssessmentType, Guid agencyId)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId,clusterId, AssessmentType, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;


            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    var episode = patientRepository.GetEpisode(agencyId, clusterId, episodeId, patientId);
                    if (episode != null && !string.IsNullOrEmpty(episode.Schedule))
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var scheduleEvent = events.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();// patientRepository.GetSchedule(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                        if (scheduleEvent != null)
                        {
                            //scheduleEvent = patientRepository.GetSchedule(agencyId, clusterId, episodeId, patientId, eventId);
                            //if (scheduleEvent != null)
                            //{
                            //    userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, eventId);
                            //}

                            var userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, eventId);

                            var planofCareStandAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, clusterId, episodeId, patientId, eventId);
                            if (planofCareStandAlone != null)
                            {
                                if (actionType == "Approve")
                                {
                                    planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                    planofCareStandAlone.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                    planofCareStandAlone.PhysicianSignatureDate = DateTime.Now;
                                    planofCareStandAlone.ReceivedDate = DateTime.Now;
                                    if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(clusterId, planofCareStandAlone))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                            scheduleEvent.ReturnReason = string.Empty;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                userEvent.ReturnReason = string.Empty;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (actionType == "Return")
                                {
                                    planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                    if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(clusterId, planofCareStandAlone))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                            scheduleEvent.ReturnReason = reason;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                userEvent.ReturnReason = reason;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, clusterId, episodeId, patientId, eventId);
                                if (planofCare != null)
                                {
                                    if (actionType == "Approve")
                                    {
                                        planofCare.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                        planofCare.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                        planofCare.PhysicianSignatureDate = DateTime.Now;
                                        planofCare.ReceivedDate = DateTime.Now;
                                        if (oasisDataProvider.PlanofCareRepository.Update(clusterId, planofCare))
                                        {
                                            if (scheduleEvent != null)
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                scheduleEvent.ReturnReason = string.Empty;
                                                if (userEvent != null)
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                    userEvent.ReturnReason = string.Empty;
                                                }
                                                shouldUpdateEpisode = true;
                                            }
                                        }
                                    }
                                    else if (actionType == "Return")
                                    {
                                        planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                        if (oasisDataProvider.PlanofCareRepository.Update(clusterId, planofCare))
                                        {
                                            if (scheduleEvent != null)
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                scheduleEvent.ReturnReason = reason;
                                                if (userEvent != null)
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                    userEvent.ReturnReason = reason;
                                                }
                                                shouldUpdateEpisode = true;
                                            }
                                        }
                                    }
                                }
                            }

                            if (shouldUpdateEpisode)
                            {
                                ScheduleHelper.SetSchedule(events, scheduleEvent);
                                var xml = events.ToXml();
                                episode.Schedule = xml;
                                if (patientRepository.UpdateEpisode(episode, clusterId))
                                {
                                    if (userEvent != null)
                                    {
                                        if (userRepository.UpdateEvent(agencyId, clusterId, userEvent))
                                        {
                                            result = true;
                                        }
                                    }
                                    else
                                    {
                                        userRepository.AddUserEvent(agencyId, clusterId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                        result = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
