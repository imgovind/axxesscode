﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement;
    using Axxess.Physician.App.Helpers;

    public class PhysicianService : IPhysicianService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagmentDataProvider, IOasisCDataProvider oasisCDataProvider)
        {
            Check.Argument.IsNotNull(oasisCDataProvider, "oasisCDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.planofCareRepository = oasisCDataProvider.PlanofCareRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.physicianRepository = agencyManagmentDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
        }

        #endregion

        #region IPhysicianService Members

        public List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var faceToFaceEncounter = new List<FaceToFaceEncounter>();
            var agencies = Current.Agencies;
            if (agencies.ContainsKey(agencyId.ToString()))
            {
                var agency = agencies[agencyId.ToString()];
                if (agency != null)
                {
                    var physicianId = agency.Id;
                    faceToFaceEncounter = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, agency.ClusterId, physicianId, new int[] { (int)ScheduleStatus.OrderSavedByPhysician, (int)ScheduleStatus.OrderSentToPhysicianElectronically }, startDate, endDate);
                    faceToFaceEncounter.ForEach(ffe =>
                    {
                        ffe.AgencyDisplayName = agency.Name;
                        ffe.PrintUrl = Url.Print(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true);
                    });
                }
            }
            return faceToFaceEncounter;
        }

        public List<CarePlanOversight> GetPhysicianCPO(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var cpos = new List<CarePlanOversight>();
            var agencies = Current.Agencies;
            var agency = agencies.Get(agencyId.ToString());
            if (agency != null)
            {
                cpos = physicianRepository.GetCPO(agencyId, agency.ClusterId, Current.LoginId, startDate, endDate);
                if (cpos != null)
                {
                    cpos.OrderBy(c => c.LogDateFormatted)
                        .ForEach(cpo =>
                        {
                            cpo.AgencyName = agency.Name;//agencyRepository.GetAgencyOnly(cpo.AgencyId).Name;
                            cpo.PatientName = patientRepository.GetPatientNameById(cpo.PatientId, cpo.AgencyId, agency.ClusterId);
                        });
                }
            }
            return cpos;
        }

        public bool SaveCPO(int clusterId, CarePlanOversight cpo)
        {
            if (patientRepository.UpdateCPO(cpo, clusterId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Order> GetOrders(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var agencies = Current.Agencies;
            if (agencies.ContainsKey(agencyId.ToString()))
            {
                var agency = agencies[agencyId.ToString()];
                if (agency.ClusterId > 0)
                {
                    if (agency != null)
                    {
                        var physicianId = agency.Id;
                        var evalOrders = patientRepository.GetEvalOrdersByPhysician(agencyId, agency.ClusterId, physicianId, startDate, endDate, new int[] { (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.OrderSentToPhysicianElectronically });
                        if (evalOrders.IsNotNullOrEmpty())
                        {
                            evalOrders.ForEach(eval =>
                            {
                                var orderDate = GetNoteVisitDateFromSchedule(eval.Schedule, eval.Id);
                                if (orderDate >= startDate && orderDate <= endDate)
                                {
                                    orders.Add(new Order
                                    {
                                        Id = eval.Id,
                                        Type = GetOrderType(eval.NoteType),
                                        Number = eval.OrderNumber,
                                        Status = eval.Status,
                                        AgencyName = agency.Name,
                                        PrintUrl = Url.Print(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), false),
                                        PatientName = eval.PatientName,
                                        CreatedDate = orderDate.ToString("MM/dd/yyyy")
                                    });
                                }
                            });
                        }

                        var physicianOrders = patientRepository.GetPhysicianOrdersByPhysicianAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                        if (physicianOrders.IsNotNullOrEmpty())
                        {
                            physicianOrders.ForEach(po =>
                            {
                                orders.Add(new Order
                                {
                                    Id = po.Id,
                                    Status = po.Status,
                                    Number = po.OrderNumber,
                                    Type = OrderType.PhysicianOrder,
                                    PrintUrl = Url.Print(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, false),
                                    PatientName = po.PatientName,
                                    AgencyName = agency.Name,
                                    CreatedDate = po.OrderDateFormatted
                                });
                            });
                        }

                        var planofCareOrders = planofCareRepository.GetByPhysicianIdAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                        if (planofCareOrders.IsNotNullOrEmpty())
                        {
                            planofCareOrders.ForEach(poc =>
                            {
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Number = poc.OrderNumber,
                                    Status = poc.Status,
                                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, false),
                                    AgencyName = agency.Name,
                                    PatientName = poc.PatientName,
                                    CreatedDate = poc.OrderDateFormatted
                                });
                            });
                        }

                        var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianIdAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                        if (planofCareStandAloneOrders.IsNotNullOrEmpty())
                        {
                            planofCareStandAloneOrders.ForEach(poc =>
                            {
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Number = poc.OrderNumber,
                                    Status = poc.Status,
                                    AgencyName = agency.Name,
                                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, false),
                                    PatientName = poc.PatientName,
                                    CreatedDate = poc.OrderDateFormatted
                                });
                            });
                        }



                        List<FaceToFaceEncounter> facetofaceOrder = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, agency.ClusterId, physicianId, new int[] { (int)ScheduleStatus.OrderSavedByPhysician, (int)ScheduleStatus.OrderSentToPhysicianElectronically }, startDate, endDate);
                        if (facetofaceOrder.IsNotNullOrEmpty())
                        {
                            facetofaceOrder.ForEach(ftf =>
                            {
                                orders.Add(new Order
                                {
                                    Id = ftf.Id,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Number = ftf.OrderNumber,
                                    Status = ftf.Status,
                                    AgencyName = agency.Name,
                                    PrintUrl = Url.Print(ftf.Id, ftf.PatientId, ftf.EpisodeId, ftf.AgencyId, OrderType.FaceToFaceEncounter, false),
                                    PatientName = ftf.PatientName,
                                    CreatedDate = ftf.RequestDate.ToString("MM/dd/yyyy")
                                });
                            });
                        }

                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }
        private DateTime GetNoteVisitDate(string note)
        {
            if (note.IsNotNullOrEmpty())
            {
                XElement rootElement = XElement.Parse(note);
                IDictionary<string, NotesQuestion> dict = new Dictionary<string, NotesQuestion>();
                foreach (XElement el in rootElement.Elements())
                {
                    if (el.Attribute("Name") != null && el.Attribute("Answer") != null && el.Attribute("Type") != null)
                    {
                        dict.Add(el.Attribute("Name").Value, new NotesQuestion { Name = el.Attribute("Name").Value, Answer = el.Attribute("Answer").Value, Type = el.Attribute("Type").Value });
                    }
                }
                if (dict != null && dict.ContainsKey("VisitDate"))
                {
                    return dict["VisitDate"].Answer.ToDateTime();
                }
                else
                    return DateTime.MinValue;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private DateTime GetNoteVisitDateFromSchedule(string schedule, Guid eventId)
        {
            if (schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = schedule.ToObject<List<ScheduleEvent>>();
                if (scheduleEvents.IsNotNullOrEmpty())
                {
                    var scheduleEvent = scheduleEvents.FirstOrDefault(s => s.EventId == eventId);
                    if (scheduleEvent != null)
                    {
                        return scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate.ToDateTime() : (scheduleEvent.EventDate.IsValidDate() ? scheduleEvent.EventDate.ToDateTime() : DateTime.MinValue);
                    }
                }
            }
            return DateTime.MinValue;
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTEvaluation":
                        orderType = OrderType.PtEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "PTReassessment":
                        orderType = OrderType.PTReassessment;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "OTReassessment":
                        orderType = OrderType.OTReassessment;
                        break;
                    case "OTDischarge":
                        orderType = OrderType.OTDischarge;
                        break;
                    case "STDischarge":
                        orderType = OrderType.STDischarge;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "MSWDischarge":
                        orderType = OrderType.MSWDischarge;
                        break;
                    case "PTPlanOfCare":
                        orderType = OrderType.PTPlanOfCare;
                        break;
                    case "OTPlanOfCare":
                        orderType = OrderType.OTPlanOfCare;
                        break;
                    case "STPlanOfCare":
                        orderType = OrderType.STPlanOfCare;
                        break;
                    case "SNPsychAssessment":
                        orderType = OrderType.SNPsychAssessment;
                        break;

                }
            }
            return orderType;
        }

        public string GetReturnComments(Guid agencyId, Guid eventId, Guid episodeId, Guid patientId)
        {
            string CommentString = string.Empty;
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId != -1)
            {
                CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, agencyId, clusterId);
                List<ReturnComment> NewComments = patientRepository.GetReturnComments(agencyId, clusterId, episodeId, eventId);
                foreach (ReturnComment comment in NewComments)
                {
                    if (comment.IsDeprecated) continue;
                    if (CommentString.IsNotNullOrEmpty()) CommentString += "<hr/>";
                    if (comment.UserId == Current.User.Id) CommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                    CommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userRepository.Get(comment.UserId, clusterId, agencyId).DisplayName, comment.Modified.ToString("g"), comment.Comments.Clean());
                }
            }
            return CommentString;
        }

        public bool AddReturnComments(Guid eventId, Guid episodeId, string comment, Guid agencyId)
        {
            var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId != -1)
            {
                return patientRepository.AddReturnComment(new ReturnComment
                {
                    AgencyId = agencyId,
                    Comments = comment,
                    Created = DateTime.Now,
                    EpisodeId = episodeId,
                    EventId = eventId,
                    Modified = DateTime.Now,
                    UserId = Current.User.Id
                }, clusterId);
            }
            else
            {
                return false;
            }
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetOrdersCompleted(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var agencies = Current.Agencies;
            if (agencies.ContainsKey(agencyId.ToString()))
            {
                var agency = agencies[agencyId.ToString()];
                if (agency != null)
                {
                    var physicianId = agency.Id;

                    var evalOrders = patientRepository.GetEvalOrdersByPhysician(agencyId, agency.ClusterId, physicianId, startDate, endDate, new int[] { (int)ScheduleStatus.EvalReturnedWPhysicianSignature, (int)ScheduleStatus.OrderReturnedWPhysicianSignature });
                    if (evalOrders.IsNotNullOrEmpty())
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var orderDate = GetNoteVisitDateFromSchedule(eval.Schedule, eval.Id);
                            if (orderDate >= startDate && orderDate <= endDate)
                            {
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    Type = GetOrderType(eval.NoteType),
                                    Number = eval.OrderNumber,
                                    AgencyName = agency.Name,
                                    PrintUrl = Url.View(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), true),
                                    PatientName = eval.PatientName,
                                    CreatedDate = orderDate.ToString("MM/dd/yyyy")
                                });
                            }
                        });
                    }

                    var physicianOrders = patientRepository.GetPhysicianOrdersByPhysicianAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                    if (physicianOrders.IsNotNullOrEmpty())
                    {
                        physicianOrders.ForEach(po =>
                        {
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                Number = po.OrderNumber,
                                Type = OrderType.PhysicianOrder,
                                PrintUrl = Url.View(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, true),
                                PatientName = po.PatientName,
                                AgencyName = agency.Name,
                                CreatedDate = po.OrderDateFormatted
                            });
                        });
                    }

                    var planofCareOrders = planofCareRepository.GetByPhysicianIdAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                    if (planofCareOrders.IsNotNullOrEmpty())
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                Type = OrderType.HCFA485,
                                Number = poc.OrderNumber,
                                PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, true),
                                AgencyName = agency.Name,
                                PatientName = poc.PatientName,
                                CreatedDate = poc.OrderDateFormatted
                            });
                        });
                    }

                    var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianIdAndDate(agencyId, agency.ClusterId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                    if (planofCareStandAloneOrders.IsNotNullOrEmpty())
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                Type = OrderType.HCFA485StandAlone,
                                Number = poc.OrderNumber,
                                AgencyName = agency.Name,
                                PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, true),
                                PatientName = poc.PatientName,
                                CreatedDate = poc.OrderDateFormatted
                            });
                        });
                    }

                    var facetofaceOrders = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, agency.ClusterId, physicianId, new int[] { (int)ScheduleStatus.OrderReturnedWPhysicianSignature }, startDate, endDate);
                    if (facetofaceOrders.IsNotNullOrEmpty())
                    {
                        facetofaceOrders.ForEach(ffe =>
                        {
                            orders.Add(new Order
                            {
                                Id = ffe.Id,
                                Type = OrderType.FaceToFaceEncounter,
                                Number = ffe.OrderNumber,
                                PatientName = ffe.PatientName,
                                AgencyName = agency.Name,
                                PrintUrl = Url.View(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true),
                                CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                            });
                        });
                    }

                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }



        public bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;


            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    var episode = patientRepository.GetEpisode(agencyId, clusterId, episodeId, patientId);
                    if (episode != null && !string.IsNullOrEmpty(episode.Schedule))
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var scheduleEvent = events.Where(e => e.EventId == orderId && e.IsDeprecated == false).FirstOrDefault();// patientRepository.GetSchedule(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                        if (scheduleEvent != null)
                        {
                            //scheduleEvent = patientRepository.GetSchedule(agencyId, clusterId, episodeId, patientId, orderId);
                            //if (scheduleEvent != null)
                            //{
                            //    
                            //}
                            var userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, orderId);
                            var physicianOrder = patientRepository.GetOrder(orderId, patientId, agencyId, clusterId);
                            if (physicianOrder != null)
                            {
                                if (actionType == "Approve")
                                {
                                    physicianOrder.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                                    physicianOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                    physicianOrder.PhysicianSignatureDate = DateTime.Now;
                                    physicianOrder.ReceivedDate = DateTime.Now;
                                    if (patientRepository.UpdateOrderModel(clusterId, physicianOrder))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                            scheduleEvent.ReturnReason = string.Empty;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                userEvent.ReturnReason = string.Empty;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (actionType == "Return")
                                {
                                    physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                    physicianOrder.Modified = DateTime.Now;
                                    if (patientRepository.UpdateOrderModel(clusterId, physicianOrder))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                            scheduleEvent.ReturnReason = reason;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                userEvent.ReturnReason = reason;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                if (shouldUpdateEpisode)
                                {
                                    ScheduleHelper.SetSchedule(events, scheduleEvent);
                                    var xml = events.ToXml();
                                    episode.Schedule = xml;
                                    if (patientRepository.UpdateEpisode(episode, clusterId))
                                    {
                                        if (userEvent != null)
                                        {
                                            if (userRepository.UpdateEvent(agencyId, clusterId, userEvent))
                                            {
                                                result = true;
                                            }
                                        }
                                        else
                                        {
                                            userRepository.AddUserEvent(agencyId, clusterId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;


            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    var episode = patientRepository.GetEpisode(agencyId, clusterId, episodeId, patientId);
                    if (episode != null && !string.IsNullOrEmpty(episode.Schedule))
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var scheduleEvent = events.Where(e => e.EventId == orderId && e.IsDeprecated == false).FirstOrDefault();// patientRepository.GetSchedule(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                        if (scheduleEvent != null)
                        {
                            //scheduleEvent = patientRepository.GetSchedule(agencyId, clusterId, episodeId, patientId, orderId);
                            //if (scheduleEvent != null)
                            //{
                            //   
                            //}
                            var userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, orderId);
                            var facetoFace = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId, clusterId);
                            if (facetoFace != null)
                            {
                                if (actionType == "Approve")
                                {
                                    facetoFace.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                                    facetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                    facetoFace.SignatureDate = DateTime.Now;
                                    facetoFace.ReceivedDate = DateTime.Now;
                                    if (patientRepository.UpdateFaceToFaceEncounter(clusterId, facetoFace))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                            scheduleEvent.ReturnReason = string.Empty;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                userEvent.ReturnReason = string.Empty;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (actionType == "Return")
                                {
                                    facetoFace.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                    facetoFace.Modified = DateTime.Now;
                                    if (patientRepository.UpdateFaceToFaceEncounter(clusterId, facetoFace))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                            scheduleEvent.ReturnReason = reason;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                userEvent.ReturnReason = reason;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                if (shouldUpdateEpisode)
                                {
                                    ScheduleHelper.SetSchedule(events, scheduleEvent);
                                    var xml = events.ToXml();
                                    episode.Schedule = xml;
                                    if (patientRepository.UpdateEpisode(episode, clusterId))
                                    {
                                        if (userEvent != null)
                                        {
                                            if (userRepository.UpdateEvent(agencyId, clusterId, userEvent))
                                            {
                                                result = true;
                                            }
                                        }
                                        else
                                        {
                                            userRepository.AddUserEvent(agencyId, clusterId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter order)
        {
            Check.Argument.IsNotNull(order, "order");
            var result = false;
            var existingFacetoFace = new FaceToFaceEncounter();
            var clusterId = Current.Agencies.GetOrDefault(order.AgencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
            if (clusterId > 0)
            {
                existingFacetoFace = patientRepository.GetFaceToFaceEncounter(order.Id, order.PatientId, order.AgencyId, clusterId);
                if (existingFacetoFace != null)
                {
                    existingFacetoFace.Status = order.Status;
                    existingFacetoFace.Modified = DateTime.Now;
                    existingFacetoFace.Services = order.Services;
                    existingFacetoFace.ServicesOther = order.ServicesOther;
                    existingFacetoFace.Certification = order.Certification;
                    existingFacetoFace.EncounterDate = order.EncounterDate;
                    existingFacetoFace.MedicalReason = order.MedicalReason;
                    existingFacetoFace.ClinicalFinding = order.ClinicalFinding;

                    if (order.ServicesArray != null && order.ServicesArray.Count > 0)
                    {
                        existingFacetoFace.Services = order.ServicesArray.ToArray().AddColons();
                    }

                    if (existingFacetoFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                    {
                        existingFacetoFace.ReceivedDate = DateTime.Now;
                        existingFacetoFace.SignatureDate = DateTime.Now;
                        existingFacetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                    }
                    else
                    {
                        existingFacetoFace.SignatureText = string.Empty;
                    }
                    var episode = patientRepository.GetEpisode(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId);
                    if (episode != null && !string.IsNullOrEmpty(episode.Schedule))
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var scheduleEvent = events.Where(e => e.EventId == existingFacetoFace.Id && e.IsDeprecated == false).FirstOrDefault();// patientRepository.GetSchedule(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                        if (scheduleEvent != null)
                        {
                            if (patientRepository.UpdateFaceToFaceEncounter(clusterId, existingFacetoFace))
                            {
                                scheduleEvent.Status = existingFacetoFace.Status.ToString();
                                ScheduleHelper.SetSchedule(events, scheduleEvent);
                                var xml = events.ToXml();
                                episode.Schedule = xml;
                                if (patientRepository.UpdateEpisode(episode, clusterId))
                                {
                                    var userEvent = userRepository.GetEvent(existingFacetoFace.AgencyId, clusterId, scheduleEvent.UserId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = scheduleEvent.Status;
                                        userEvent.ReturnReason = scheduleEvent.ReturnReason;
                                        userRepository.UpdateEvent(existingFacetoFace.AgencyId, clusterId, userEvent);
                                        result = true;
                                    }
                                    else
                                    {
                                        userRepository.AddUserEvent(existingFacetoFace.AgencyId, clusterId, scheduleEvent.PatientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                        result = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;


            if (!evalId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var clusterId = Current.Agencies.GetOrDefault(agencyId.ToString(), new AgencyCluster { ClusterId = -1 }).ClusterId;
                if (clusterId != -1)
                {
                    var episode = patientRepository.GetEpisode(agencyId, clusterId, episodeId, patientId);
                    if (episode != null && !string.IsNullOrEmpty(episode.Schedule))
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var scheduleEvent = events.Where(e => e.EventId == evalId && e.IsDeprecated == false).FirstOrDefault();// patientRepository.GetSchedule(existingFacetoFace.AgencyId, clusterId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                        if (scheduleEvent != null)
                        {

                            //scheduleEvent = patientRepository.GetSchedule(agencyId, clusterId, episodeId, patientId, evalId);
                            //if (scheduleEvent != null)
                            //{
                            //    userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, evalId);
                            //}
                            var userEvent = userRepository.GetEvent(agencyId, clusterId, scheduleEvent.UserId, patientId, evalId);
                            bool poc = false;
                            if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STPlanOfCare)
                            {
                                poc = true;
                            }
                            var evalOrder = patientRepository.GetVisitNote(agencyId, clusterId, episodeId, patientId, evalId);
                            if (evalOrder != null)
                            {
                                if (actionType == "Approve")
                                {
                                    evalOrder.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                                    evalOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                    evalOrder.PhysicianSignatureDate = DateTime.Now;
                                    evalOrder.ReceivedDate = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(clusterId, evalOrder))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            if (poc)
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                            }
                                            else
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                            }
                                            scheduleEvent.ReturnReason = string.Empty;
                                            if (userEvent != null)
                                            {
                                                if (poc)
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                                }
                                                else
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                                }
                                                userEvent.ReturnReason = string.Empty;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (actionType == "Return")
                                {
                                    evalOrder.Status = ((int)ScheduleStatus.EvalReturnedByPhysician);
                                    evalOrder.Modified = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(clusterId, evalOrder))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            if (poc)
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                            }
                                            else
                                            {
                                                scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                            }
                                            scheduleEvent.ReturnReason = reason;
                                            if (userEvent != null)
                                            {
                                                if (poc)
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                                }
                                                else
                                                {
                                                    userEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                                }
                                                userEvent.ReturnReason = reason;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                if (shouldUpdateEpisode)
                                {
                                    ScheduleHelper.SetSchedule(events, scheduleEvent);
                                    var xml = events.ToXml();
                                    episode.Schedule = xml;
                                    if (patientRepository.UpdateEpisode(episode, clusterId))
                                    {
                                        if (userEvent != null)
                                        {
                                            if (userRepository.UpdateEvent(agencyId, clusterId, userEvent))
                                            {
                                                result = true;
                                            }
                                        }
                                        else
                                        {
                                            userRepository.AddUserEvent(agencyId, clusterId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
