﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Collections.Generic;
    using Axxess.Physician.App.ViewData;

    using Axxess.OasisC.Domain;
    
    public interface IAssessmentService
    {
        PlanofCare GetPlanofCare(Guid agencyId,int clusterId, Guid episodeId, Guid patientId, Guid eventId);
        string GetAllergies(Guid patientId, Guid agencyId);
        IDictionary<string, Question> GetDiagnoses(Guid assessmentId, int clusterId, string AssessmentType, Guid agencyId);
        bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason);
        VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
    }
}
