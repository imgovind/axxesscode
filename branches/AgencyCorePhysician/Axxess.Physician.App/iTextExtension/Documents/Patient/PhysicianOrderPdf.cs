﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.Physician.App.Helpers;
    class PhysicianOrderPdf : AxxessPdf {
        public PhysicianOrderPdf(PhysicianOrder data) {
            this.SetType(PdfDocs.PhysicianOrder);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Text.IsNotNullOrEmpty() ? data.Text : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 225, 35, 125, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", data != null && data.Agency != null ? PrintHelper.AgencyAddress(data.Agency) : String.Empty);
            fieldmap[0].Add("patient", (data != null && data.Patient != null ? (data.Patient.AddressLine1.IsNotNullOrEmpty() ? data.Patient.AddressLine1.ToTitleCase() + "\n" : "") + (data.Patient.AddressLine2.IsNotNullOrEmpty() ? data.Patient.AddressLine2.ToTitleCase() + "\n" : "") + (data.Patient.AddressCity.IsNotNullOrEmpty() ? data.Patient.AddressCity.ToTitleCase() + ", " : "") + (data.Patient.AddressStateCode.IsNotNullOrEmpty() ? data.Patient.AddressStateCode.ToTitleCase() + "  " : "") + (data.Patient.AddressZipCode.IsNotNullOrEmpty() ? data.Patient.AddressZipCode + "\n" : "") + (data.Patient.PhoneHome.IsNotNullOrEmpty() ? data.Patient.PhoneHome.ToPhone() + "\n" : "") + (data.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + data.Patient.MedicareNumber : "") : ""));
            fieldmap[0].Add("physician", (data != null && data.Physician != null ? (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1.ToTitleCase() : "") + (data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + data.Physician.AddressLine2.ToTitleCase() + "\n" : "\n") + (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity.ToTitleCase() + ", " : "") + (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode + "\n" : "") + (data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + data.Physician.PhoneWork.ToPhone() : "") + (data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + data.Physician.FaxNumber.ToPhone() + "\n" : "\n") + (data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + data.Physician.NPI : "") : ""));
            fieldmap[0].Add("orderdate", data != null && data.OrderDate.IsValid() ? data.OrderDate.ToShortDateString() : "");
            fieldmap[0].Add("ordernum", data != null && data.OrderNumber.ToString().IsNotNullOrEmpty() && data.OrderNumber.ToString().ToInteger() != 0 ? data.OrderNumber.ToString() : "");
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValidDate() ? data.EpisodeStartDate + "—" : string.Empty) + (data != null && data.EpisodeEndDate.IsValidDate() ? data.EpisodeEndDate : string.Empty));
            fieldmap[0].Add("dob", data != null && data.Patient != null && data.Patient.DOB.IsValid() ? data.Patient.DOB.ToShortDateString() : string.Empty);
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : "");
            fieldmap[0].Add("summary", data != null && data.Summary.IsNotNullOrEmpty() ? data.Summary : "");
            fieldmap[0].Add("IsOrderReadAndVerified", data != null && data.IsOrderReadAndVerified ? "X" : "");
            fieldmap[0].Add("clinsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("clinsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[0].Add("physsign", data != null && data.PhysicianSignatureText.IsNotNullOrEmpty() ? data.PhysicianSignatureText : "");
            fieldmap[0].Add("physsigndate", data != null && data.PhysicianSignatureDate.IsValid() ? data.PhysicianSignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}