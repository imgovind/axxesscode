﻿namespace Axxess.Physician.App.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.Physician.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Physician.App.Helpers;
    class MSWPdf : VisitNotePdf {
        public MSWPdf(VisitNoteViewData data) : base(data) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 120, 28.3F, data.Type.Contains("Assessment") ? 93 : 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", data != null && data.Agency != null ? PrintHelper.AgencyAddress(data.Agency) : string.Empty);
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : "");
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : "");
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            switch (data.Type) {
                case "MSWAssessment": fieldmap[1].Add("doctype", "MSW ASSESSMENT"); break;
                case "MSWDischarge": fieldmap[1].Add("doctype", "MSW DISCHARGE"); break;
                case "MSWEvaluationAssessment": fieldmap[1].Add("doctype", "MSW EVALUATION"); break;
                case "MSWProgressNote": fieldmap[1].Add("doctype", "MSW PROGRESS NOTE"); break;
                case "MSWVisit": fieldmap[1].Add("doctype", "MSW VISIT"); break;
            }
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? PrintHelper.PatientName(data.Patient) : string.Empty);
            return fieldmap;
        }
    }
}