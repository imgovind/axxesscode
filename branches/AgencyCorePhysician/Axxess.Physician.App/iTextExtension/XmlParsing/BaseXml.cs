﻿namespace Axxess.Physician.App.iTextExtension.XmlParsing {
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    abstract class BaseXml {
        protected String Type = string.Empty;
        protected List<XmlPrintSection> Layout = null;
        private XElement XmlFile;
        public static String BlankChar = "_";
        public int SectionCount() {
            return this.Layout.Count();
        }
        public String GetType() {
            return this.Type != null ? this.Type : string.Empty;
        }
        public List<XmlPrintSection> GetLayout() {
            return this.Layout;
        }
        protected BaseXml(PdfDoc doc) {
            this.XmlFile = XElement.Load(doc.GetXmlFile());
        }
        protected void Init() {
            this.Layout = this.ExtractSections(this.XmlFile);
        }
        protected void NotaFilter() {
            for (int tabI = 0; tabI < this.Layout.Count(); tabI++) {
                for (int fieldsetI = 0; fieldsetI < this.Layout[tabI].Subsection.Count(); fieldsetI++) {
                    for (int questionI = 0; questionI < this.Layout[tabI].Subsection[fieldsetI].Question.Count(); questionI++) {
                        if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Type == "checkgroup") {
                            for (int optionI = 0; optionI < this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option.Count(); optionI++) if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion != null && !(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Data.Split(',').Contains(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Value) || this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Data.Equals(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Value))) this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion.RemoveRange(0, this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion.Count());
                        } else if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Type == "multiple") {
                            for (int subquestionI = 0; subquestionI < this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count(); subquestionI++) {
                                if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Type == "notacheck" && this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Data.Equals(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Value)) {
                                    this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.RemoveRange(0, subquestionI);
                                    if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count() > 0) this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.RemoveRange(1, this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count() - 1);
                                    subquestionI = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
        public virtual String GetData(String Index) {
            return Index;
        }
        private List<XmlPrintSection> ExtractSections(XElement Node) {
            return (from section in Node.Elements("section")
                where (section.HasAttributes || section.HasElements) && (this.Type == null || section.Attribute("membership") == null || section.Attribute("membership").Value.Split(',').Contains(this.Type))
                select new XmlPrintSection(this, section)).ToList();
        }
        public String GetJson() {
            StringBuilder Json = new StringBuilder();
            Json.Append("[");
            foreach (XmlPrintSection section in this.Layout) Json.Append(section.GetJson());
            Json.Append("]");
            Json.Replace("[],","");
            Json.Replace("}{","},{");
            return Json.ToString();
        }
    }
}