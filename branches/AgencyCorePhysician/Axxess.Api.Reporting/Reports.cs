﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;
    using System.Text;
    

    internal static class Reports
    {
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();

        #region Static Helper Methods

        internal static AgencyData GetAgency(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator`  FROM `agencies` WHERE `Id` = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static AgencyData GetAgencyLocation(Guid agencyId, Guid agencyLocationId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `AgencyId`, `Name`, `Payor`, `NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator` , `IsLocationStandAlone`, `AddressZipCode` FROM `agencylocations` WHERE `Id` = @agencylocationid  AND `AgencyId` = @agencyid ;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("AgencyId"),
                    LocationId = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    Payor = reader.GetInt("Payor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    AddressZipCode = reader.GetString("AddressZipCode")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static string NumberEligible(Guid agencyId, Guid agencyLocationId, DateTime sampleMonthLast)
        {
            int count = 0;
            var script = @"SELECT Count(`Id`) FROM `patients` WHERE `AgencyId` = @agencyid AND `AgencyLocationId` = @agencylocationid  AND `IsDeprecated` = 0 AND `Status` in (1, 2) " +
                "AND `PrimaryInsurance` in (1,2,3,4) AND (`Created` <= @month || `StartofCareDate` <= @month)";

            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("month", sampleMonthLast)
                .AsScalar();
            }

            return count.ToString();
        }

        internal static string SkilledVisitCount(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();

            episodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (schedule != null && schedule.Count > 0)
                    {
                        var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCareForCAHPS() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            list.AddRange(scheduledEvents);
                        }
                    }
                }
            });

            return list.Count.ToString();
        }

        internal static string SkilledVisitCount(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var episodes = GetEpisodesBetweenByPatientId(agencyId, patientId, startDate, endDate);
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCareForCAHPS() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                            if (scheduledEvents != null && scheduledEvents.Count > 0)
                            {
                                list.AddRange(scheduledEvents);
                            }
                        }
                    }
                });
            }

            return list.Count.ToString();
        }

        internal static List<ScheduleEvent> AllSkilledVisits(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var patientEpisodes = episodes.Where(e =>
                 (e.StartDate.Date >= startDate.Date && e.StartDate.Date <= endDate.Date)
                || (e.EndDate.Date >= startDate.Date && e.EndDate.Date <= endDate.Date)
                || (startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date)
                || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date));
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW())
                               && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                    if (schedule != null && schedule.Count > 0)
                    {
                        list.AddRange(schedule);
                    }
                }
            });

            return list;
        }

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule, patientepisodes.Details as Details  " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid  " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(patients.DOB as DATETIME)))) >= 18 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesBetweenByPatientId(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate , patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesByAdmissionPeriodId(List<Guid> admissionPeriodIds)
        {
            var list = new List<EpisodeData>();
            var script = string.Format("select `Id`, `EndDate`, `StartDate`, `Schedule` from `patientepisodes` where `AdmissionId` in ({0});", admissionPeriodIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, List<int> paymentSources)
        {
            var items = new List<EpisodeData>();
            var results = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.PaymentSource as PaymentSource, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                items = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PaymentSources = reader.GetStringNullable("PaymentSource")
                })
                .AsList();
            }

            if (items != null && items.Count > 0)
            {
                items.ForEach(i =>
                {
                    if (i.PaymentSources.IsNotNullOrEmpty())
                    {
                        paymentSources.ForEach(p =>
                        {
                            if (i.PaymentSources.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Contains(p.ToString()))
                            {
                                results.Add(i);
                                return;
                            }
                        });
                    }
                });
            }
            return results;
        }

        internal static List<EpisodeData> GetEpisodesBetweenForPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate , patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static Assessment GetEpisodeAssessment(EpisodeData episode)
        {
            Assessment assessment = null;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var sampleMonthEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && !e.IsMissedVisit && !e.IsDeprecated
                            && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date
                            && (e.IsStartofCareAssessment()))
                    .OrderBy(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                if (sampleMonthEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(sampleMonthEvent.EventId, sampleMonthEvent.GetAssessmentType(), episode.AgencyId);
                }
                else
                {
                    var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.StartDate);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var samplePreviousEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && !e.IsMissedVisit && !e.IsDeprecated 
                                && e.EventDate.ToDateTime().Date >= episode.StartDate.AddDays(-6).Date
                                && e.EventDate.ToDateTime().Date <= episode.StartDate.AddDays(-1).Date
                                && previousEpisode.EndDate.AddDays(1).Date == episode.StartDate.Date
                                && (e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                            .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                        if (samplePreviousEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(samplePreviousEvent.EventId, samplePreviousEvent.GetAssessmentType(), episode.AgencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        internal static Assessment GetEpisodeAssessment(EpisodeData episode, DateTime end)
        {
            Assessment assessment = null;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var sampleMonthEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                    .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                if (sampleMonthEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(sampleMonthEvent.EventId, sampleMonthEvent.GetAssessmentType(), episode.AgencyId);
                }
                else
                {
                    var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.Id);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var samplePreviousEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                            .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                        if (samplePreviousEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(samplePreviousEvent.EventId, samplePreviousEvent.GetAssessmentType(), episode.AgencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var episodes = GetPatientEpisodes(agencyId, patientId);
            if (episodes != null && episodes.Count > 0)
            {
                var episode = episodes.Where(e => e.Id == episodeId).FirstOrDefault();
                if (episode != null)
                {
                    return episodes.Where(e => e.EndDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).FirstOrDefault();
                }
            }
            return null;
        }

        internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT patientepisodes.Id as Id, patientepisodes.PatientId as PatientId, patientepisodes.AdmissionId as AdmissionId, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate, patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
                "AND DATE(patientepisodes.EndDate) < DATE(@date) AND patientepisodes.isactive = 1 AND patientepisodes.IsDischarged = 0 " +
                "ORDER BY patientepisodes.StartDate DESC LIMIT 1;";

            var patientEpisode = new EpisodeData();
            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("Id"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        internal static List<EpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            var list = new List<EpisodeData>();
            var script = @"SELECT patientepisodes.Id, patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
                "AND patientepisodes.IsActive = 1";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static List<EpisodeData> GetEpisodesExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.DOB as DOB, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, patientepisodes.AdmissionId as AdmissionId, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule, patientepisodes.Details as Details, patients.PatientIdNumber, patients.StartofCareDate " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 " +
                "AND patients.AgencyLocationId = @agencylocationid  AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (Date(patientepisodes.StartDate) <= Date(@enddate) and Date(patientepisodes.EndDate) >= Date(@startdate) ) ORDER BY patients.LastName ASC, patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Status = reader.GetInt("Status"),
                    Id = reader.GetGuid("EpisodeId"),
                    AdmissionId = reader.GetGuidIncludeEmpty("AdmissionId"),
                    PatientId = reader.GetGuid("PatientId"),
                    DOB = reader.GetDateTimeWithMin("DOB"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartofCareDate = reader.GetDateTimeWithMin("StartofCareDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    DischargeDate = reader.GetDateTimeWithMin("DischargeDate"),
                    MRN = reader.GetStringNullable("PatientIdNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesStartDateExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.DOB as DOB, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, patientepisodes.AdmissionId as AdmissionId, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule, patientepisodes.Details as Details, patients.PatientIdNumber, patients.StartofCareDate " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 " +
                "AND patients.AgencyLocationId = @agencylocationid  AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate)) ORDER BY patients.LastName ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Status = reader.GetInt("Status"),
                    Id = reader.GetGuid("EpisodeId"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    PatientId = reader.GetGuid("PatientId"),
                    DOB = reader.GetDateTime("DOB"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    MRN = reader.GetStringNullable("PatientIdNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode")
                })
                .AsList();
            }
            return list.ToList();
        }


        internal static CbsaCode GetCbsaCode(string zipCode)
        {
            var cbsaCode = new CbsaCode();
            var script = string.Format("SELECT * FROM `cbsacodes` WHERE `zip` = '{0}' limit 0, 1;", zipCode.ToZipCodeFive());

            using (var cmd = new FluentCommand<CbsaCode>(script))
            {
                cbsaCode = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new CbsaCode
                    {
                        CBSA = reader.GetStringNullable("CBSA"),
                        WITwoTen = reader.GetDouble("WITwoTen"),
                        WITwoEleven = reader.GetDouble("WITwoEleven"),
                        WITwoTwelve = reader.GetDouble("WITwoTwelve"),
                        WITwoThirteen=reader.GetDouble("WITwoThirteen")
                    }).AsSingle();
            }

            return cbsaCode;
        }

        internal static HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            var result = new HippsAndHhrg();
            var list = new List<HippsAndHhrg>();
            var script = string.Format("SELECT * FROM `hippsandhhrgs` WHERE `HIPPS` = '{0}';", hippsCode);

            using (var cmd = new FluentCommand<HippsAndHhrg>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new HippsAndHhrg
                    {
                        HHRG = reader.GetStringNullable("HHRG"),
                        HIPPS = reader.GetStringNullable("HIPPS"),
                        HHRGWeight = reader.GetDouble("HHRGWeight"),
                        Time = reader.GetDateTime("Time")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }
            return result;
        }

        internal static PPSStandard GetPPSStandardByYear(int year)
        {
            var result = new PPSStandard();
            var list = new List<PPSStandard>();

            var script = @"SELECT * FROM `ppsstandards`;";
            using (var cmd = new FluentCommand<PPSStandard>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new PPSStandard
                    {
                        Time = reader.GetDateTime("Time"),
                        UrbanRate = reader.GetDouble("UrbanRate"),
                        RuralRate = reader.GetDouble("RuralRate"),
                        Labor = reader.GetDouble("Labor"),
                        NonLabor = reader.GetDouble("NonLabor"),
                        S = reader.GetDouble("S"),
                        RuralS = reader.GetDouble("RuralS"),
                        T = reader.GetDouble("T"),
                        RuralT = reader.GetDouble("RuralT"),
                        U = reader.GetDouble("U"),
                        RuralU = reader.GetDouble("RuralU"),
                        V = reader.GetDouble("V"),
                        RuralV = reader.GetDouble("RuralV"),
                        W = reader.GetDouble("W"),
                        RuralW = reader.GetDouble("RuralW"),
                        X = reader.GetDouble("X"),
                        RuralX = reader.GetDouble("RuralX")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }

            return result;
        }

        internal static Claim GetRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var rap = new Claim();
            var script = string.Format("SELECT * FROM `raps` WHERE `agencyid` = '{0}' AND `patientid` = '{1}' AND `episodeid` = '{2}' limit 0, 1;", agencyId, patientId, episodeId);

            using (var cmd = new FluentCommand<Claim>(script))
            {
                rap = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsSingle();
            }

            return rap;
        }

        internal static Claim GetFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var final = new Claim();
            var script = string.Format("SELECT * FROM `finals` WHERE `agencyid` = '{0}' AND `patientid` = '{1}' AND `episodeid` = '{2}' limit 0, 1;", agencyId, patientId, episodeId);

            using (var cmd = new FluentCommand<Claim>(script))
            {
                final = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsSingle();
            }

            return final;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var list = new List<AdmissionPeriod>();
            var script = @"select * from patientadmissiondates WHERE `AgencyId` = @agencyid and `StartOfCareDate` between @start and @end";

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("start", startDate)
                .AddDateTime("end", endDate)
                .AsList();
            }

            return list;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissionsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status)
        {
            var list = new List<AdmissionPeriod>();
            var script = string.Format("select * from patientadmissiondates WHERE `AgencyId` = @agencyid and `StartOfCareDate` between @start and @end and status = {0};", status);

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("start", startDate)
                .AddDateTime("end", endDate)
                .AsList();
            }

            return list;
        }

        internal static List<AdmissionPeriod> GetDischargePatients(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var dischargePatients = new List<AdmissionPeriod>();
            var script = string.Format(@"SELECT p.DischargeReasonId, pa.PatientData" +
                " FROM patients p " +
                " JOIN patientadmissiondates pa on p.Id = pa.PatientId " +
                " WHERE pa.AgencyId = @agencyId AND (pa.Status = 2) AND p.IsDeprecated = 0 AND pa.IsDeprecated = 0 " +
                " AND DATE(pa.DischargedDate) between DATE(@startdate) and DATE(@enddate) {0} ", !branchId.IsEmpty() ? "AND p.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                dischargePatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new AdmissionPeriod
                    {
                        PatientData = reader.GetStringNullable("PatientData"),
                        DischargeReasonId = reader.GetInt("DischargeReasonId")
                    }).AsList();
            }

            foreach (var patient in dischargePatients)
            {
                if (patient.DischargeReasonId > 0)
                {
                    
                }
                else
                {
                    var patientData = patient.PatientData.ToObject<Patient>();
                    if (patientData != null)
                    {
                        if (patientData.DischargeReasonId > 0)
                        {
                            patient.DischargeReasonId = patientData.DischargeReasonId;
                        }
                    }
                    else
                    {
                        patient.DischargeReasonId = 15;
                    }
                }
            }
            return dischargePatients;
        }

        internal static List<InsuranceData> GetAgencyInsurances(Guid agencyId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }

        internal static List<InsuranceData> GetAgencyInsurancesByBranch(Guid agencyId, Guid branchId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }


        internal static InsuranceData GetMedicareInsurance(int id)
        {
            var script = string.Format("SELECT * FROM `insurances` WHERE `Id` = '{0}';", id);
            var insurance = new InsuranceData();
            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                insurance = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name")
                    }).AsSingle();
            }
            return insurance;
        }

        internal static List<EpisodeData> GetEpisodesBetweenByInsurance(Guid agencyId, Guid agencyLocationId, int insurance ,DateTime startDate, DateTime endDate)
        {

            var list = new List<EpisodeData>();
            string insuranceScript = "";
            if (insurance > 0)
            {
                insuranceScript = " AND (patients.PrimaryInsurance = @insurance OR patients.SecondaryInsurance = @insurance OR patients.TertiaryInsurance = @insurance)";
            }
            var script =
                string.Format(@"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.PatientIdNumber as PatientIdNumber, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule, patientepisodes.Details as Details  " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid  " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(patients.DOB as DATETIME)))) >= 18 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) {0}" +
                "ORDER BY patientepisodes.StartDate ASC", insuranceScript);

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddInt("insurance", insurance)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    MRN = reader.GetStringNullable("PatientIdNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesBetweenByNonMedicareInsurance(Guid agencyId, Guid agencyLocationId, int insurance, DateTime startDate, DateTime endDate)
        {

            var list = new List<EpisodeData>();
            string insuranceScript = "";
            if (insurance > 0)
            {
                insuranceScript = " AND (patients.PrimaryInsurance = @insurance OR patients.SecondaryInsurance = @insurance OR patients.TertiaryInsurance = @insurance)";
            }
            else
            {
                insuranceScript = " AND (patients.PrimaryInsurance > 1000 OR patients.SecondaryInsurance > 1000 OR patients.TertiaryInsurance > 1000)";
            }
            var script =
                string.Format(@"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.PatientIdNumber as PatientIdNumber, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patients.PrimaryInsurance as PrimaryInsurance, patients.SecondaryInsurance as SecondaryInsurance, patients.TertiaryInsurance as TertiaryInsurance, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule, patientepisodes.Details as Details  " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid  " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(patients.DOB as DATETIME)))) >= 18 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) {0}" +
                "ORDER BY patientepisodes.StartDate ASC", insuranceScript);

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddInt("insurance", insurance)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    MRN = reader.GetStringNullable("PatientIdNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                    SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                    TertiaryInsurance = reader.GetStringNullable("TertiaryInsurance")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ManagedClaimData> GetManagedClaimsByInsurance(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            string insuranceScript = "";
            if (insurance > 0)
            {
                insuranceScript = " AND (patients.PrimaryInsurance = @insurance OR patients.SecondaryInsurance = @insurance OR patients.TertiaryInsurance = @insurance)";
            }
            else
            {
                insuranceScript = " AND (patients.PrimaryInsurance > 1000 OR patients.SecondaryInsurance > 1000 OR patients.TertiaryInsurance > 1000)";
            }
            var script = string.Format(@"SELECT 
                            managedclaims.Id as Id,
                            managedclaims.PatientId as PatientId,
                            managedclaims.EpisodeStartDate as StartDate,
                            managedclaims.EpisodeEndDate as EndDate
                                FROM 
                                    managedclaims 
                                        INNER JOIN patients ON managedclaims.PatientId = patients.Id 
                                            WHERE 
                                                managedclaims.AgencyId = @agencyid {0} AND
                                                patients.Status IN (1,2) AND
                                                patients.IsDeprecated = 0 
                                                {1} AND (DATE(managedclaims.EpisodeEndDate) between DATE(@startdate) and DATE(@enddate) 
                                                OR DATE(managedclaims.EpisodeStartDate) between DATE(@startdate) and DATE(@enddate)) 
                                                ORDER BY  managedclaims.EpisodeStartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty, insuranceScript);
            var list = new List<ManagedClaimData>();
            using (var cmd = new FluentCommand<ManagedClaimData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insurance", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ManagedClaimData
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                 })
                 .AsList();
            }
            return list;
        }

        internal static List<Claim> GetFinals(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var finals = new List<Claim>();
            var script = string.Format("SELECT * " +
                                        "FROM `finals` " + 
                                        "WHERE `agencyid` = '{0}' AND " + 
                                        "EpisodeStartDate BETWEEN @startDate AND @endDate " +
                                        "", agencyId);

            using (var cmd = new FluentCommand<Claim>(script))
            {
                finals = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddDateTime("startDate", startDate)
                    .AddDateTime("endDate", endDate)
                    .SetMap(reader => new Claim
                    {
                        //PatientId = reader.GetGuid("PatientId"),
                        //EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                        //EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }
            return finals;
        }

        internal static List<Patient> GetPatients(Guid agencyId, Guid branchId)
        {
            var patients = new List<Patient>();
            var script = string.Format("SELECT * " +
                                        "FROM `patients` " +
                                        "WHERE `agencyid` = '{0}' AND " +
                                        "`branchId` = @branchId AND " +
                                        "IsDeprecated = 0", agencyId);

            using (var cmd = new FluentCommand<Patient>(script))
            {
                patients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new Patient
                    {
                        Id = reader.GetGuid("Id"),
                        InternalReferral = reader.GetGuidIncludeEmpty("InternalReferral"),
                        FirstName = reader.GetString("FirstName"),
                        LastName = reader.GetString("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial")
                    })
                    .AsList();
            }
            return patients;
        }

        internal static Dictionary<string, object> GetPatientGenders(Guid agencyId, List<Guid> patientIds)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            string patientIdString = patientIds.Select(x => string.Format("'{0}'", x)).ToArray().Join(", ");
            var script = string.Format(@"
                            SELECT p.Id as patientId, p.Gender as patientGender 
                            FROM patients AS p
                            WHERE p.AgencyId = @agencyid
                            AND p.Id IN ({0}) 
                            ",patientIdString);
            using(var cmd = new FluentCommand<object>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid",agencyId)
                    .SetDictonaryId("patientId")
                    .SetMap(reader => reader.GetStringNullable("patientGender").ToString()).AsDictionary();
            }
            return result;
        }

        internal static List<PatientList> GetPatientsList(Guid agencyId, Guid branchId, int StatusId)
        {
            var patientList = new List<PatientList>();
            var script = string.Format
                (
                    "SELECT patients.Id As Id, patients.UserId as ClinicianId, patients.PatientIdNumber as patientMRN, patients.FirstName as patientFirstName, patients.LastName as patientLastName, patients.MiddleInitial as patientMiddleInitial, patients.DOB as patientDOB,patients.MaritalStatus as patientMaritalStatus, patients.CaseManagerId as patientCaseManagerId, patients.MedicareNumber as patientMedicareNumber,patients.MedicaidNumber as patientMedicaidNumber, patients.SSN as patientSSN, patients.StartofCareDate as patientSOCDate, patients.Ethnicities as patientEthinicty, patients.PaymentSource as patientPaymentSource, patients.AddressLine1 as patientAddressLine1,patients.AddressLine2 as patientAddressLine2, patients.AddressCity as patientAddressCity, patients.AddressStateCode as patientAddressStateCode,patients.AddressZipCode as patientAddressZipCode, patients.PhoneHome as patientPhoneHome, patients.PhoneMobile as patientPhoneMobile,patients.EmailAddress as patientEmailAddress, patients.PrimaryInsurance as patientPrimaryInsurance, patients.IsDNR as patientDNR,patients.PrimaryHealthPlanId as patientPrimaryHealthPlanId, patients.Payer as patientPayor, patients.ReferrerPhysician as patientReferrerPhysician, patients.OtherReferralSource as patientOtherReferralSource, patients.ServicesRequired as patientServicesRequired, patients.Comments as patientComments, patients.DME as patientDME,patients.InternalReferral as patientInternalReferral, agencyinsurances.Name as patientPrimaryInsuranceName ,caseManager.FirstName as patientCaseManagerFirstName, caseManager.LastName as patientCaseManagerLastName, caseManager.MiddleName as patientCaseManagerMiddleName, clinicianAssigned.FirstName as patientClinicianAssignedFirstName, clinicianAssigned.LastName as patientClinicianAssignedLastName, clinicianAssigned.MiddleName as patientClinicianAssignedMiddleName " +
                    "FROM patients " +
                    "LEFT JOIN users AS caseManager ON caseManager.Id = patients.CaseManagerId " +
                    "LEFT JOIN users As clinicianAssigned ON clinicianAssigned.Id = patients.UserId " +
                    "LEFT JOIN agencyinsurances ON agencyinsurances.Id = patients.PrimaryInsurance " +
                    "WHERE patients.AgencyId = '{0}' " +
                    "AND patients.`Status` = '{1}'  " +
                    "AND patients.IsDeprecated = '0'",
                    agencyId, StatusId
                );
            if (branchId.IsNotEmpty()) 
            {
                script = script + string.Format(" AND patients.AgencyLocationId = '{0}'", branchId);
            }
            try
            {
                using (var cmd = new FluentCommand<PatientList>(script))
                {
                    patientList = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("branchId", branchId)
                        .SetMap(reader => new PatientList
                        {
                            Id = reader.GetGuid("Id"),
                            UserId = reader.GetGuid("ClinicianId"),
                            PatientIdNumber = reader.GetStringNullable("patientMRN"),
                            FirstName = reader.GetStringNullable("patientFirstName"),
                            LastName = reader.GetStringNullable("patientLastName"),
                            MiddleInitial = reader.GetStringNullable("patientMiddleInitial"),
                            DOB = reader.GetDateTime("patientDOB"),
                            MaritalStatus = reader.GetStringNullable("patientMaritalStatus"),
                            CaseManagerId = reader.GetGuidIncludeEmpty("patientCaseManagerId"),
                            CaseManagerFirstName = reader.GetStringNullable("patientCaseManagerFirstName"),
                            CaseManagerMiddleName = reader.GetStringNullable("patientCaseManagerMiddleName"),
                            CaseManagerLastName = reader.GetStringNullable("patientCaseManagerLastName"),
                            ClinicianAssignedFirstName = reader.GetStringNullable("patientClinicianAssignedFirstName"),
                            ClinicianAssignedLastName = reader.GetStringNullable("patientClinicianAssignedLastName"),
                            ClinicianAssignedMiddleName = reader.GetStringNullable("patientClinicianAssignedMiddleName"),
                            MedicareNumber = reader.GetStringNullable("patientMedicareNumber"),
                            MedicaidNumber = reader.GetStringNullable("patientMedicaidNumber"),
                            SSN = reader.GetStringNullable("patientSSN"),
                            StartofCareDate = reader.GetDateTime("patientSOCDate"),
                            DNR = reader.GetInt("patientDNR"),
                            Ethnicities = reader.GetStringNullable("patientEthinicty"),
                            PaymentSource = reader.GetStringNullable("patientPaymentSource"),
                            AddressLine1 = reader.GetStringNullable("patientAddressLine1"),
                            AddressLine2 = reader.GetStringNullable("patientAddressLine2"),
                            AddressCity = reader.GetStringNullable("patientAddressCity"),
                            AddressStateCode = reader.GetStringNullable("patientAddressStateCode"),
                            AddressZipCode = reader.GetStringNullable("patientAddressZipCode"),
                            PhoneHome = reader.GetStringNullable("patientPhoneHome"),
                            PhoneMobile = reader.GetStringNullable("patientPhoneMobile"),
                            EmailAddress = reader.GetStringNullable("patientEmailAddress"),
                            PrimaryInsuranceNumber = reader.GetInt("patientPrimaryInsurance"),
                            PrimaryInsuranceName = reader.GetStringNullable("patientPrimaryInsuranceName"),
                            PrimaryHealthPlanId = reader.GetStringNullable("patientPrimaryHealthPlanId"),
                            Payer = reader.GetStringNullable("patientPayor"),
                            ReferrerPhysician = reader.GetGuidIncludeEmpty("patientReferrerPhysician"),
                            OtherReferralSource = reader.GetStringNullable("patientOtherReferralSource"),
                            ServicesRequired = reader.GetStringNullable("patientServicesRequired"),//Process This
                            Comments = reader.GetStringNullable("patientComments"),
                            DME = reader.GetStringNullable("patientDME"),
                            InternalReferral = reader.GetGuidIncludeEmpty("patientInternalReferral"),
                            EpisodeStartDate = DateTime.Now,
                            EpisodeEndDate = DateTime.Now,
                            PhysicianId = Guid.Empty,
                            PhysicianFirstName = string.Empty,
                            PhysicianLastName = string.Empty,
                            PhysicianMiddleName = string.Empty,
                            PhysicianNPI = string.Empty
                        })
                        .AsList();
                }
            }
            catch (Exception ex) 
            {
                Windows.EventLog.WriteEntry("THIS HAPPENED IN REPORTS/GetPatientList \n"+ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            return patientList;
        }

        


        //internal static IList<EmployeeList> GetUser(Guid agencyId, Guid branchId, int status)
        //{
        //    var users = new List<EmployeeList>();
        //    //var script = string.Format(@"SELECT FirstName , LastName , MiddleName , ProfileData , Credentials, CredentialsOther, TitleType, TitleTypeOther, EmploymentType ,DOB, SSN, CustomId , Roles ,Licenses,Permissions " +
        //    //  "  FROM users " +
        //    //  " WHERE  AgencyId = @agencyId " +
        //    //  " {0} {1} AND IsDeprecated = 0", status == 0 ? string.Empty : " AND Status = " + status, !branchId.IsEmpty() ? "AND AgencyLocationId = @branchId" : string.Empty);

        //    var script = string.Format(@"SELECT agencymanagement.users.FirstName AS userFirstName, agencymanagement.users.LastName AS userLastName, agencymanagement.users.MiddleName AS userMiddleName, agencymanagement.users.ProfileData AS userProfileData ,agencymanagement.users.Credentials AS userCredentials,agencymanagement.users.CredentialsOther AS userCredentialsOther,agencymanagement.users.TitleType AS userTitleType,agencymanagement.users.TitleTypeOther AS userTitleTypeOther,agencymanagement.users.EmploymentType AS userEmploymentType,agencymanagement.users.DOB AS userDOB,agencymanagement.users.SSN AS userSSN,agencymanagement.users.CustomId AS userCustomId,agencymanagement.users.Roles AS userRoles,agencymanagement.users.Licenses AS userLicenses,agencymanagement.users.Permissions AS userPermissions," +
        //      "FROM agencymanagement.users " +
        //      "LEFT JOIN agencymanagement.userlocations on agencymanagement.userlocations.UserId = agencymanagement.users.Id " +
        //      "WHERE agencymanagement.users.AgencyId = @agencyId " +
        //      "{0} {1} AND agencymanagement.users.IsDeprecated = 0", status == 0 ? string.Empty : "AND agencymanagement.users.`Status` = " + status, !branchId.IsEmpty() ? "AND agencymanagement.userlocations.AgencyLocationId = @branchId " : string.Empty);
            
        //    using (var cmd = new FluentCommand<EmployeeList>(script))
        //    {
        //        users = cmd.SetConnection("AgencyManagementConnectionString")
        //            .AddGuid("agencyId", agencyId)
        //            .AddGuid("branchId", branchId)
        //            .SetMap(reader => new EmployeeList
        //            {
        //                FirstName = reader.GetStringNullable("LastName").ToUpperCase(),
        //                LastName = reader.GetStringNullable("FirstName").ToUpperCase(),
        //                MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
        //                Credentials = reader.GetStringNullable("Credentials"),
        //                CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                TitleType = reader.GetStringNullable("TitleType"),
        //                TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
        //                EmploymentType = reader.GetStringNullable("EmploymentType"),
        //                DOB = reader.GetDateTime("DOB"),
        //                SSN = reader.GetStringNullable("SSN"),
        //                CustomId = reader.GetStringNullable("CustomId"),
        //                Roles = reader.GetStringNullable("Roles"),
        //                Licenses = reader.GetStringNullable("Licenses"),
        //                Permissions = reader.GetStringNullable("Roles"),
        //                Profile = reader.GetStringNullable("ProfileData")
        //                //Profile = reader.GetStringNullable("ProfileData").IsNotNullOrEmpty() ? reader.GetStringNullable("ProfileData").ToObject<EmployeeProfile>() : new EmployeeProfile()
        //            })
        //            .AsList();
        //    }


        //    return users;
        //}

        internal static IList<EmployeeList> GetUsers(Guid agencyId, Guid branchId, int status)
        {
            var users = new List<EmployeeList>();

            var script = string.Format(@"SELECT agencymanagement.users.FirstName AS userFirstName, agencymanagement.users.LastName AS userLastName, agencymanagement.users.MiddleName AS userMiddleName, agencymanagement.users.ProfileData AS userProfileData ,agencymanagement.users.Credentials AS userCredentials,agencymanagement.users.CredentialsOther AS userCredentialsOther,agencymanagement.users.TitleType AS userTitleType,agencymanagement.users.TitleTypeOther AS userTitleTypeOther,agencymanagement.users.EmploymentType AS userEmploymentType,agencymanagement.users.DOB AS userDOB,agencymanagement.users.SSN AS userSSN,agencymanagement.users.CustomId AS userCustomId,agencymanagement.users.Roles AS userRoles,agencymanagement.users.Licenses AS userLicenses,agencymanagement.users.Permissions AS userPermissions " +
              "FROM agencymanagement.users " +
              "LEFT JOIN agencymanagement.userlocations on agencymanagement.userlocations.UserId = agencymanagement.users.Id " +
              "WHERE agencymanagement.users.AgencyId = @agencyId " +
              "{0} {1} AND agencymanagement.users.IsDeprecated = 0 ", status == 0 ? string.Empty : "AND agencymanagement.users.`Status` = " + status, !branchId.IsEmpty() ? "AND agencymanagement.userlocations.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<EmployeeList>(script))
            {
                users = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new EmployeeList
                    {
                        FirstName = reader.GetStringNullable("userFirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("userLastName").ToUpperCase(),
                        MiddleName = reader.GetStringNullable("userMiddleName").ToUpperCase(),
                        Credentials = reader.GetStringNullable("userCredentials"),
                        CredentialsOther = reader.GetStringNullable("userCredentialsOther"),
                        TitleType = reader.GetStringNullable("userTitleType"),
                        TitleTypeOther = reader.GetStringNullable("userTitleTypeOther"),
                        EmploymentType = reader.GetStringNullable("userEmploymentType"),
                        DOB = reader.GetDateTime("userDOB"),
                        SSN = reader.GetStringNullable("userSSN"),
                        CustomId = reader.GetStringNullable("userCustomId"),
                        Roles = reader.GetStringNullable("userRoles"),
                        Licenses = reader.GetStringNullable("userLicenses"),
                        Permissions = reader.GetStringNullable("userRoles"),
                        Profile = reader.GetStringNullable("userProfileData")
                    })
                    .AsList();
            }

            return users.OrderBy(u => u.FirstName).ToList();
        }

        #endregion

        #region Helper Functions

        internal static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month, 1, 0, 0, 0, 0);
        }

        internal static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month,
               DateTime.DaysInMonth(Year, (int)Month), 23, 59, 59, 999);
        }

        internal static DateTime GetStartOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetStartOfMonth(12, Year - 1);
            else
                return GetStartOfMonth(Month - 1, Year);
        }

        internal static DateTime GetEndOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetEndOfMonth(12, Year - 1);
            else
                return GetEndOfMonth(Month - 1, Year);
        }

        #endregion
    }
}
