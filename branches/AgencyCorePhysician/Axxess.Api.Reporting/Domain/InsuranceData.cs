﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Collections.Generic;

    public class InsuranceData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PayorType { get; set; }
    }
}
