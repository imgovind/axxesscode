﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net;

namespace Axxess.Api.Eligibility
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new EligibilityWindowsService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
