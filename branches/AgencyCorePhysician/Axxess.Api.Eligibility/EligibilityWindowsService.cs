﻿namespace Axxess.Api.Eligibility
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    public partial class EligibilityWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public EligibilityWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "EligibilityService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(EligibilityService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("Eligibility Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("Eligibility Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
