﻿namespace Axxess.Api.Eligibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using OopFactory.X12.Parsing.Model;

    public interface IEligibility
    {
        string ResponseStatus { get; }
        string CreateEDI270(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
        string Submit(string edi270);
        Interchange ParseEDI271(string edi271);
        Container GetContainer(Container holdingContainer, Type returnContainerType, int elementNumber, string value);
        string GetElementSpecificationDescription(Interchange interchange, string versionCode, string segmentId, string elementName, string elementValue);
    }
}
