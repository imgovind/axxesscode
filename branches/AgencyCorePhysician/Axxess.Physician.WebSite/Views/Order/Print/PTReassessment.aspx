﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency; %>
<%  string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
<%  string[] genericGaitStepStairRail1 = data.AnswerArray("GenericGaitStepStairRail1"); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.8.3.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Reassessment" +
        "<%= Model.Type == "PTEvaluation" ? "Evaluation" : ""%><%= Model.Type == "PTReEvaluation" ? "Re-Evaluation" : ""%><%= Model.Type == "PTMaintenance" ? "Maintenance Visit" : ""%>" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : ""%>Evaluation%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<%if(data.ContainsKey("IsVitalSignsApply") && data.AnswerOrEmptyString("IsVitalSignsApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Vital Signs");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(7,
            printview.span("SBP",true) +
            printview.span("DBP",true) +
            printview.span("HR",true) +
            printview.span("Resp",true) +
            printview.span("Temp",true) +
            printview.span("Weight",true) +
            printview.span("O<sub>2</sub> Sat",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressure").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodPressurePer").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericPulse").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericResp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTemp").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWeight").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("GenericO2Sat").Clean() %>")),
        "Vital Signs");
</script>

<%} %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Orientation:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentOrientation").Clean() %>",0,1) +
            printview.span("Level of Consciousness::",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentLOC").Clean() %>",0,1))+
            printview.span("Comment:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentComment").Clean() %>",0,1),
            "Mental Assessment");
   
</script>
<%if (data.AnswerOrEmptyString("DisciplineTask")=="44")
              {if(data.AnswerOrEmptyString("IsMedicalApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
                printview.checkbox("N/A", true),
                "Medical Diagnosis");
</script>
              <%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("MedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("PT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("TherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  }
  else if (data.AnswerOrEmptyString("DisciplineTask") == "47")
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("MedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("TherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%} %>
<% if(data.AnswerOrEmptyString("IsPainApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Pain Assessment");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Pain Location",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentLocation").Clean() %>",0,1) +
            printview.span("Pain Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainLevel").Clean() %>",0,1) +
            printview.span("Increased by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy").Clean() %>",0,1) +
            printview.span("Relieved by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy").Clean() %>",0,1)),
        "Pain Assessment");
</script>
<%} %>
<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<% if(data.AnswerOrEmptyString("IsLivingApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Living Situation");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(5,
        printview.span("Dwelling Level:",true) +
        printview.span("&#160;") +
        printview.checkbox("One",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Multiple",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0").ToString().ToLower() %>) +
        printview.span("&#160;") +
        printview.span("Stairs",true) +
        printview.span("&#160;") +
        printview.span("#Steps<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairsNumber").Clean() %>",0,1) +
        printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0").ToString().ToLower() %>) +
        printview.span("Lives with:",true) +
        printview.checkbox("Alone",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2").ToString().ToLower() %>) +
        printview.checkbox("Family",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Friends",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0").ToString().ToLower() %>) +
        printview.checkbox("Significant Other",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("3").ToString().ToLower() %>)) +
        printview.span("Support:",true) +
        printview.col(3,
        printview.checkbox("Willing caregiver available",<%= genericLivingSituationSupport.Contains("1").ToString().ToLower() %>) +
        printview.checkbox("Limited caregiver support",<%= genericLivingSituationSupport.Contains("2").ToString().ToLower() %>) +
        printview.checkbox("No caregiver available",<%= genericLivingSituationSupport.Contains("3").ToString().ToLower() %>)) +
        printview.col(2,
        printview.span("Home Safety Barriers:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericHomeSafetyBarriers").Clean() %>",0,1)),
        "Living Situation");
</script>

<%} %>          
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("Speech:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech").Clean() %>",0,1) +
            printview.span("Vision:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentVision").Clean() %>",0,1) +
            printview.span("Hearing:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing").Clean() %>",0,1) +
            printview.span("Skin:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin").Clean() %>",0,1) +
            printview.span("Edema:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema").Clean() %>",0,1) +
            printview.span("Muscle Tone:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone").Clean() %>",0,1) +
            printview.span("Coordination:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination").Clean() %>",0,1) +
            printview.span("Sensation:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation").Clean() %>",0,1) +
            printview.span("Endurance:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance").Clean() %>",0,1))+
            printview.col(3,
            printview.span("Safety Awareness:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness").Clean() %>",0,1)+
            printview.span("&#160;")),
        "Physical Assessment");
        
</script>
<% if (data.ContainsKey("IsPhysicalAssessmentApply") && data.AnswerOrEmptyString("IsPhysicalAssessmentApply").Equals("1"))
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Physical Assessment");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentComment").Clean() %>",0,1) +
        printview.col(3,
            printview.span("") +
            printview.span("%3Cspan class=%22align-center fill%22%3EROM%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3EStrength%3C/span%3E",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthLeft").Clean() %>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Hip",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotStrengthLeft").Clean() %>",0,1) +
            printview.span("Knee",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Ankle",true) +
            printview.span("Plantarflexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Dorsiflexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Lat Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Long Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthLeft").Clean() %>",0,1)),
        "Physical Assessment");
</script>
<%} %>
<% if(data.ContainsKey("IsLOFApply")&&data.AnswerOrEmptyString("IsLOFApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Prior And Current Level Of Funtion");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Prior Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorFunctionalStatus").Clean() %>",0,1) +
        
        printview.span("Current Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericCurrentFunctionalStatus").Clean() %>",0,1),
       
        "Prior And Current Level Of Funtion");
</script>

<%} %>
<% string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
<% if(data.ContainsKey("IsHomeboundApply")&&data.AnswerOrEmptyString("IsHomeboundApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Homebound Reason");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Requires considerable and taxing effort.",<%= genericHomeBoundStatus.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Medical restriction.",<%= genericHomeBoundStatus.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with transfer.",<%= genericHomeBoundStatus.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with ambulation.",<%= genericHomeBoundStatus.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Needs assist leaving the home.",<%= genericHomeBoundStatus.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Unable to be up for long period.",<%= genericHomeBoundStatus.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Severe SOB upon exertion.",<%= genericHomeBoundStatus.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Unsafe to go out of home alone.",<%= genericHomeBoundStatus.Contains("6").ToString().ToLower() %>)),
        "Homebound Reason");
</script>  
<%} %>  
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorTherapyReceived").Clean()%>",0,3),
        "Prior Therapy Received");

    printview.addsection(
        printview.span(""),
        "Compare functional status");
    
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Rolling to Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Rolling to Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityComment").Clean() %>",0,1),
        "Bed Mobility-Prior",2);
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Rolling to Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Rolling to Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityComment1").Clean() %>",0,1),
        "Bed Mobility-Current");
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),
        "Transfer-Prior",2);
        
        printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistance1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic1").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist1").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceStatic1").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic1").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),
        "Transfer-Current");
        
        printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet").Clean() %> feet")+
            printview.span("")+
            printview.col(3,
            printview.checkbox("No Rail",<%= genericGaitStepStairRail.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("1 Rail",<%= genericGaitStepStairRail.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("2 Rails",<%= genericGaitStepStairRail.Contains("3").ToString().ToLower() %>)) +
            printview.span("Assistive Device",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Gait Quality/Deviation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment").Clean() %>",0,1),
        "Gait Analysis-Prior",2);
        
        printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet1").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet1").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist1").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet1").Clean() %> feet")+
            printview.span("")+
            printview.col(3,
            printview.checkbox("No Rail",<%= genericGaitStepStairRail1.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("1 Rail",<%= genericGaitStepStairRail1.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("2 Rails",<%= genericGaitStepStairRail1.Contains("3").ToString().ToLower() %>)) +
            printview.span("Assistive Device",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice1").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Gait Quality/Deviation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment1").Clean() %>",0,1),
        "Gait Analysis-Current");

    printview.addsubsection(
        printview.col(2,
            printview.span("<b>Status:</b><%= data.AnswerOrEmptyString("GenericWBStatus").StringIntToEnumDescriptionFactory("WeightBearingStatus").Clean() %>",0,1) +
            printview.span("<b>Other:</b><%= data.AnswerOrEmptyString("GenericWBStatusOther").Clean()%>",0,1)) +
            printview.span("Comment", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSComment").Clean()%>",0,1),
        "WB-Prior",2);
        
    printview.addsubsection(
        printview.col(2,
            printview.span("<b>Status:</b><%= data.AnswerOrEmptyString("GenericWBStatus1").StringIntToEnumDescriptionFactory("WeightBearingStatus").Clean() %>",0,1) +
            printview.span("<b>Other:</b><%= data.AnswerOrEmptyString("GenericWBStatusOther1").Clean()%>",0,1)) +
            printview.span("Comment", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSComment1").Clean()%>",0,1),
        "WB-Current");
        
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA").Clean()%>",0,1)+
            printview.span("Timed Get Up & Go Test",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp").Clean()%>",0,1)+
            printview.span("Functional Reach",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach").Clean()%>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther").Clean()%>",0,2)),
            "Standardized test-Prior",2);
    
    printview.addsubsection(
        printview.col(2,
            printview.span("Tinetti POMA",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTinettiPOMA1").Clean()%>",0,1)+
            printview.span("Timed Get Up & Go Test",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTimedGetUp1").Clean()%>",0,1)+
            printview.span("Functional Reach",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalReach1").Clean()%>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandardizedTestOther1").Clean()%>",0,2)),
            "Standardized test-Prior");

    printview.addsection(
        printview.span("Indicate all factors influencing the patient's progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericFactors").Clean() %>",0,2)+
        printview.span("Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician's professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericExpectations").Clean() %>",0,2)+
        printview.span("Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericRecommendations").Clean() %>",0,2),
        "");
        
   printview.addsection(
    printview.col(2,
        printview.checkbox("POC UNCHANGED (PHYSICIAN SIGNATURE NOT REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("1").ToString().ToLower() %>) +
        printview.checkbox("POC CHANGED (PHYSICIAN SIGNATURE REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("0").ToString().ToLower() %>) ),
        "");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericCareCoordination").Clean() %>",0,3),"Care Coordination");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericSkilledCareProvided").Clean() %>",0,3),"Skilled Care Provided This Visit");
    
</script>
</body>
</html>
