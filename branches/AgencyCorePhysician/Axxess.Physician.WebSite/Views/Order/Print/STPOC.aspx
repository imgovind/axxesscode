﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.STEvaluation.ToString(), "Speech Therapy Evaluation" },
    { DisciplineTasks.STDischarge.ToString(), "Speech Therapy Discharge" },
    { DisciplineTasks.STReEvaluation.ToString(), "Speech Therapy ReEvaluation" },
    { DisciplineTasks.STMaintenance.ToString(), "Speech Therapy Maintenance Visit" }
}; %>
<%  var location = Model.Agency; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %><% =dictonary.ContainsKey(Model.Type)? dictonary[Model.Type]:"" %><%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    <% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>    
<script type="text/javascript">
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EST Plan Of Care" +
            "<%= dictonary.ContainsKey(Model.Type)? dictonary[Model.Type]:"" %>" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EOrder Number:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("OrderNumber") %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EST Plan Of Care%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        
    
    
</script>
<% if (data.AnswerOrEmptyString("POCIsDMEApply").Equals("1"))
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "DME");
</script>   
<%}
   else
   { %>
<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>
<%} %>

<%if (Model.DisciplineTask==44 || Model.DisciplineTask==137)
              {if(data.AnswerOrEmptyString("IsMedicalApply").Equals("1")){ %>
<script type="text/javascript">
              printview.addsection(
                printview.checkbox("N/A",true),
                "Medical Diagnosis");
</script>
              <%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("PT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  }
  else if (Model.DisciplineTask==47 || Model.DisciplineTask==135)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Diagnosis");
</script>
<%}
  else if (Model.DisciplineTask == 51 || Model.DisciplineTask == 136)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("ST Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%} %>

<script type="text/javascript">          
        printview.addsection(
            printview.col(2,
                printview.checkbox("Evaluation (C1)",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Establish Rehab Program",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Given to Patient",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Attached to Chart",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Patient/Family Education",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Voice Disorders",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Speech Articulation Disorders",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Dysphagia Treatments",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Language Disorders",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Aural Rehabilitation (C6)",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Non-Oral Communication (C8)",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                printview.checkbox("Alaryngeal Speech Skills",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("12") ? "true" : "false"%>) +
                printview.checkbox("Language Processing",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("15") ? "true" : "false"%>) +
                printview.checkbox("Food Texture Recommendations",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("16") ? "true" : "false"%>) +
                printview.checkbox("Safe Swallowing Evaluation",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("17") ? "true" : "false"%>) +
                printview.checkbox("Pain Management",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("20") ? "true" : "false"%>) +
                
                printview.checkbox("Therapy to Increase Articulation, Proficiency, Verbal Expression",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("18") ? "true" : "false"%>) +
                printview.checkbox("Lip, Tongue, Facial Exercises to Improve Swallowing/Vocal Skills",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("19") ? "true" : "false"%>) +
                printview.checkbox("Speech Dysphagia Instruction Program",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("21") ? "true" : "false"%>) +
                printview.checkbox("Care of Voice Prosthesis — Removal, Cleaning, Site Maint",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("22") ? "true" : "false"%>) +
                printview.checkbox("Teach/Develop Comm. System",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("23") ? "true" : "false"%>) +
                printview.checkbox("Trach Inst. and Care",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("24") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("POCGenericPlanOfCare") && data["POCGenericPlanOfCare"].Answer.Split(',').Contains("25") ? "true" : "false"%>)+
                printview.span("")),
            "Treatment Plan");
        printview.addsection(
            printview.col(2,
                printview.span("Frequency and Duration",true) +
                printview.span("<%= data != null && data.ContainsKey("POCGenericFrequencyAndDuration") && data["POCGenericFrequencyAndDuration"].Answer.IsNotNullOrEmpty() ? data["POCGenericFrequencyAndDuration"].Answer.Clean() : string.Empty %>",0,1)));
               
        printview.addsection(
            printview.span("Equipment Recommendations",true) +
            printview.span("<%= data != null && data.ContainsKey("POCGenericEquipmentRecommendations") && data["POCGenericEquipmentRecommendations"].Answer.IsNotNullOrEmpty() ? data["POCGenericEquipmentRecommendations"].Answer.Clean() : string.Empty %>",0,2));
        
        printview.addsection(
            printview.span("Comments/ Additional Information",true) +
            printview.span("<%= data != null && data.ContainsKey("POCGenericAdditionalInformation") && data["POCGenericAdditionalInformation"].Answer.IsNotNullOrEmpty() ? data["POCGenericAdditionalInformation"].Answer.Clean() : string.Empty %>",0,2));
        
        printview.addsection(
            
            printview.span("Plan for Next Visit ",true) +
            printview.span("<%= data != null && data.ContainsKey("POCGenericPlanForNextVisit") && data["POCGenericPlanForNextVisit"].Answer.IsNotNullOrEmpty() ? data["POCGenericPlanForNextVisit"].Answer.Clean() : string.Empty%>",0,2));
        printview.addsection(
            printview.span("Assessment",true) +
            printview.span("<%= data != null && data.ContainsKey("POCGenericPatientDesiredOutcomes") && data["POCGenericPatientDesiredOutcomes"].Answer.IsNotNullOrEmpty() ? data["POCGenericPatientDesiredOutcomes"].Answer.Clean() : string.Empty %>",0,2) +
            
            "");     
 </script>            
<% if (data.AnswerOrEmptyString("POCIsModalitiesApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Modalities");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericModalitiesComment").Clean() %>",0,3),"Modalities");
  </script>
  <%} %>     
  
<% if (data.ContainsKey("IsGoalsApply") && data.AnswerOrEmptyString("POCIsGoalsApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "ST Goals");
</script>    
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("ST Short Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericShortTermGoalsComment").Clean() %>",0,2)+
        printview.span("ST Long Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericLongTermGoalsComment").Clean() %>",0,2)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSTGoalsDesired").Contains("0").ToString().ToLower() %>) +
            
            printview.checkbox("Caregiver  desired outcomes:<%=data.AnswerOrEmptyString("POCSTGoalsDesiredOutcomes").Clean() %>",<%= data.AnswerOrEmptyString("POCSTGoalsDesired").Contains("1").ToString().ToLower() %>)),
        "ST Goals");
</script>
<%} %>    
<% if (data.AnswerOrEmptyString("POCIsRecommendationApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Other Discipline Recommendation");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("OT", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("MSW", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("PT", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Podiatrist", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther").Clean() %>",0,1)) +
            printview.span("Reason", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason").Clean() %>",0,1) ,
        "Other Discipline Recommendation");
 </script>       
 <%} %>
 <% if (data.AnswerOrEmptyString("POCIsRehabApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Rehab");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Rehab Diagnosis:<%=data.AnswerOrEmptyString("").Clean() %>",true)+
        printview.span("Rehab Potential:",true)+
        printview.col(3,
            printview.checkbox("Good",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("0").ToString().ToLower() %>)+
            printview.checkbox("Fair",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Poor",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("2").ToString().ToLower() %>))+
        printview.span("<%=data.AnswerOrEmptyString("POCOtherRehabPotential").Clean() %>",0,1),
        "Rehab");
            
</script>
 <%} %>
 <% if (data.AnswerOrEmptyString("POCIsDCPlanApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Discharge Plan");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(
        printview.span("Patient to be discharged to the care of:",true)+
        printview.col(3,
            printview.checkbox("Physician",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToString().ToLower() %>)+
            printview.checkbox("Selfcare",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToString().ToLower() %>))+
        printview.span("Discharge Plans:",true)+
        printview.col(2,
            printview.checkbox("Discharge when caregiver willing and able to manage all aspects of patient’s care",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Discharge when goals met.",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToString().ToLower() %>))+
        printview.span("<%= data.AnswerOrEmptyString("POCDCPlanAdditional").Clean() %>",0,3),"Discharge Plan");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("POCIsSkilledCareApply").Equals("1"))
    { %>
 <script type="text/javascript">
     printview.addsection(
        printview.checkbox("N/A", true),
        "Skilled Care Provided");
  </script>
  <%}
    else
    { %>
    <script type="text/javascript">
    printview.addsection(
        printview.span("Training Topics:<%= data.AnswerOrEmptyString("POCSkilledCareTrainingTopics").Clean() %>",0,3)+
        printview.span("Trained:",true)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToString().ToLower() %>)) +
        printview.span("Treatment Performed: <%= data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed").Clean() %>",0,3)+
        printview.span("Patient Response: <%= data.AnswerOrEmptyString("POCSkilledCarePatientResponse").Clean() %>",0,3),
    "Skilled Care Provided");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("POCIsCareApply").Equals("1"))
    {%>
  <script type="text/javascript">
      printview.addsubsection(
        printview.checkbox("N/A", true),
        "Care Coordination", 2);
  </script>
 <%}
    else
    { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericCareCoordination").Clean() %>",0,3),"Care Coordination");
  </script>
  <%} %>
  <% if (data.AnswerOrEmptyString("POCIsSafetyIssueApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsubsection(
        printview.checkbox("N/A", true),
        "Safety Issues/Instruction/Education");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericSafetyIssue").Clean() %>",0,3),"Safety Issues/Instruction/Education");
  </script>
  <%} %>
  <script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Patient ",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("1").ToString().ToLower() %>)) +
        printview.span("understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)",true)+
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("0").ToString().ToLower() %>))+
        printview.checkbox("Physician notified and agrees with POC, frequency and duration. Comments (if any):<%=data.AnswerOrEmptyString("POCNotificationPhysicianComment").Clean() %>",<%= data.AnswerOrEmptyString("POCNotificationPhysician").Contains("0").ToString().ToLower() %>),
    "Notification");
</script>
<script type="text/javascript">                 
            printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
    
    </script>
 </body>
</html>
