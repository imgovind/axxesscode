﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (data.AnswerOrEmptyString("POCIsModalitiesApply").Equals("1"))
   {%>

<script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "Modalities");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericModalitiesComment").Clean() %>",0,3),"Modalities");
</script>

<%} %>