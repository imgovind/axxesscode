﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
<div>
    <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="float-left">Level</label>
    <div class="float-right">
        <%= Html.TherapyAssistance(Model.Type + "_GenericGaitLevelAssist", data.AnswerOrEmptyString("GenericGaitLevelAssist"), new { @id = Model.Type + "_GenericGaitLevelAssist", @class = "" })%>
        X
        <%= Html.TextBox(Model.Type+"_GenericGaitLevelFeet", data.AnswerOrEmptyString("GenericGaitLevelFeet"), new { @class = "sn", @id = Model.Type+"_GenericGaitLevelFeet" })%>
        Feet
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="float-left">Unlevel</label>
    <div class="float-right">
        <%= Html.TherapyAssistance(Model.Type + "_GenericGaitUnLevelAssist", data.AnswerOrEmptyString("GenericGaitUnLevelAssist"), new { @id = Model.Type + "_GenericGaitUnLevelAssist", @class = "" })%>
        X
        <%= Html.TextBox(Model.Type+"_GenericGaitUnLevelFeet", data.AnswerOrEmptyString("GenericGaitUnLevelFeet"), new { @class = "sn", @id = Model.Type+"_GenericGaitUnLevelFeet" })%>
        Feet
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericGaitStepStairAssist" class="float-left">Step/ Stair</label>
    <div class="float-right">
        <%= string.Format("<input id='{1}_GenericGaitStepStairRail1' class='radio' name='{1}_GenericGaitStepStairRail' value='1' type='checkbox' {0} />", genericGaitStepStairRail.Contains("1").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_GenericGaitStepStairRail1">No Rail</label>
        <%= string.Format("<input id='{1}_GenericGaitStepStairRail2' class='radio' name='{1}_GenericGaitStepStairRail' value='2' type='checkbox' {0} />", genericGaitStepStairRail.Contains("2").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_GenericGaitStepStairRail2">1 Rail</label>
        <%= string.Format("<input id='{1}_GenericGaitStepStairRail3' class='radio' name='{1}_GenericGaitStepStairRail' value='3' type='checkbox' {0} />", genericGaitStepStairRail.Contains("3").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_GenericGaitStepStairRail3">2 Rails</label>
        <%= Html.TherapyAssistance(Model.Type + "_GenericGaitStepStairAssist", data.AnswerOrEmptyString("GenericGaitStepStairAssist"), new { @id = Model.Type + "_GenericGaitStepStairAssist", @class = "" })%>
        X
        <%= Html.TextBox(Model.Type+"_GenericGaitStepStairFeet", data.AnswerOrEmptyString("GenericGaitStepStairFeet"), new { @class = "sn", @id = Model.Type+"_GenericGaitStepStairFeet" })%>
        Steps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label class="float-left">Assistive Device</label>
    <div class="float-right"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericGaitAnalysisAssistiveDevice", data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice"), new { @id = Model.Type + "_GenericGaitAnalysisAssistiveDevice", @class = "" })%>
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericGaitComment" class="strong float-left">Gait Quality/Deviation</label>
    <%= Html.Templates(Model.Type + "_GaitCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericGaitComment" })%>
    <%= Html.TextArea(Model.Type + "_GenericGaitComment", data.ContainsKey("GenericGaitComment") ? data["GenericGaitComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment", @class = "fill" }) %>
</div>
<div class="clear"></div>
<div>
    <label for="<%= Model.Type %>_WBStatus" class="strong float-left">WB Status</label>
    <%  var wbstatus = new SelectList(new[] {
                                    new SelectListItem { Text = "--Select Status--", Value = "" },
                                    new SelectListItem { Text = "Non Weight Bearing ", Value = "Non Weight Bearing" },
                                    new SelectListItem { Text = "Partial Weight Bearing ", Value = "Partial Weight Bearing " },
                                    new SelectListItem { Text = "Toe Touch Weight Bearing", Value = "Toe Touch Weight Bearing" },
                                    new SelectListItem { Text = "Weight Bearing as Tolerated", Value = "Weight Bearing as Tolerated" },
                                    new SelectListItem { Text = "Full Weight Bearing", Value = "Full Weight Bearing" }
                                }, "Value", "Text", data.AnswerOrDefault("WBStatus", ""));%>
                            <%= Html.DropDownList(Model.TypeName + "_WBStatus", wbstatus, new { @id = Model.TypeName + "_WBStatus" })%>
</div>
<div class="clear" />
<div>
    <label for="<%= Model.Type %>_GenericGaitFinalComment" class="strong float-left">Comment:</label>
    <%= Html.Templates(Model.Type + "_GaitFinalCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericGaitFinalComment" })%>
    <%= Html.TextArea(Model.Type + "_GenericGaitFinalComment", data.ContainsKey("GenericGaitFinalComment") ? data["GenericGaitFinalComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitFinalComment", @class = "fill" })%>
</div>