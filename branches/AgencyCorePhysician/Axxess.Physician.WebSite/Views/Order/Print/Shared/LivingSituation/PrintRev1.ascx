﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<% if(data.AnswerOrEmptyString("IsLivingApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Living Situation");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(5,
        printview.span("Dwelling Level:",true) +
        printview.span("&#160;") +
        printview.checkbox("One",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Multiple",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0").ToString().ToLower() %>) +
        printview.span("&#160;") +
        printview.span("Stairs",true) +
        printview.span("&#160;") +
        printview.span("#Steps<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairsNumber").Clean() %>",0,1) +
        printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0").ToString().ToLower() %>) +
        printview.span("Lives with:",true) +
        printview.checkbox("Alone",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2").ToString().ToLower() %>) +
        printview.checkbox("Family",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("Friends",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0").ToString().ToLower() %>) +
        printview.checkbox("Significant Other",<%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("3").ToString().ToLower() %>)) +
        printview.span("Support:",true) +
        printview.col(3,
        printview.checkbox("Willing caregiver available",<%= genericLivingSituationSupport.Contains("1").ToString().ToLower() %>) +
        printview.checkbox("Limited caregiver support",<%= genericLivingSituationSupport.Contains("2").ToString().ToLower() %>) +
        printview.checkbox("No caregiver available",<%= genericLivingSituationSupport.Contains("3").ToString().ToLower() %>)) +
        printview.col(2,
        printview.span("Home Safety Barriers:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericHomeSafetyBarriers").Clean() %>",0,1)),
        "Living Situation");
</script>

<%} %>