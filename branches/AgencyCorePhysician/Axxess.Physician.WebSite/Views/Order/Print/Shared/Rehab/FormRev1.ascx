﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="float-left">
    <label for="<%= Model.Type %>_POCGenericDisciplineRehabDiagnosis" class="strong">
        Rehab Diagnosis:</lable><%= Html.TextBox(Model.Type + "_POCGenericDisciplineRehabDiagnosis", data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis"), new { @class = "", @id = Model.Type + "_POCGenericDisciplineRehabDiagnosis" })%></div>
<div class="clear" />
<div class="float-left">
    <label for="<%= Model.Type %>_POCGenericRehabPotential" class="strong">
        Rehab Potential</label>
    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "0", data.AnswerOrEmptyString("POCRehabPotential").Equals("0"), new { @id = Model.Type + "_POCRehabPotential0", @class = "radio" })%>
    <label for="<%= Model.Type %>_POCRehabPotential0" class="inline-radio">
        Good</label>
    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "1", data.AnswerOrEmptyString("POCRehabPotential").Equals("1"), new { @id = Model.Type + "_POCRehabPotential1", @class = "radio" })%>
    <label for="<%= Model.Type %>_POCRehabPotential1" class="inline-radio">
        Fair</label>
    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "2", data.AnswerOrEmptyString("POCRehabPotential").Equals("2"), new { @id = Model.Type + "_POCRehabPotential2", @class = "radio" })%>
    <label for="<%= Model.Type %>_POCRehabPotential2" class="inline-radio">
        Poor</label>
</div>
<div class="clear" />
<%= Html.Templates(Model.Type + "_POCOtherRehabPotentialTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCOtherRehabPotential" })%>
<%= Html.TextArea(Model.Type + "_POCOtherRehabPotential", data.ContainsKey("POCOtherRehabPotential") ? data["POCOtherRehabPotential"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCOtherRehabPotential", @class = "fill" })%>
