﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>DCPlanContainer">
    <label class="strong float-left">
        Patient to be discharged to the care of:</label>
    <div class="margin">
        <%= string.Format("<input id='{1}_POCDCPlanCareOf1' class='radio' name='{1}_POCDCPlanCareOf' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_POCDCPlanCareOf1">
            Physician</label>
        <%= string.Format("<input id='{1}_POCDCPlanCareOf2' class='radio' name='{1}_POCDCPlanCareOf' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_POCDCPlanCareOf1">
            Caregiver</label>
        <%= string.Format("<input id='{1}_POCDCPlanCareOf3' class='radio' name='{1}_POCDCPlanCareOf' value='3' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_POCDCPlanCareOf1">
            Self care</label>
    </div>
    <div class="clear" />
    <label class="strong float-left">
        Discharge Plans:</label>
    <div class="margin">
        <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_POCDCPlanPlans1">
            Discharge when caregiver willing and able to manage all aspects of patient’s care</label>
        <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToChecked(), Model.Type)%>
        <label for="<%= Model.Type %>_POCDCPlanPlans1">
            Discharge when goals met.</label>
    </div>
    <div class="clear" />
    <%= Html.Templates(Model.Type + "_POCDCPlanAdditionalTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCDCPlanAdditional" })%>
    <%= Html.TextArea(Model.Type + "_POCDCPlanAdditional", data.ContainsKey("POCDCPlanAdditional") ? data["POCDCPlanAdditional"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCDCPlanAdditional", @class = "fill" })%>
</div>
