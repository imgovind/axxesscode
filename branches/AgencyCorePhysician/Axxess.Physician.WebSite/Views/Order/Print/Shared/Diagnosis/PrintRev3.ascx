﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%if (Model.DisciplineTask==44 || Model.DisciplineTask==137)
              {if(data.AnswerOrEmptyString("IsMedicalApply").Equals("1")){ %>
<script type="text/javascript">
              printview.addsection(
                printview.checkbox("N/A",true),
                "Medical Diagnosis");
</script>
              <%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("PT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  }
  else if (Model.DisciplineTask==47 || Model.DisciplineTask==135)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Diagnosis");
</script>
<%}
  else if (Model.DisciplineTask == 51 || Model.DisciplineTask == 136)
  {
      if (data.AnswerOrEmptyString("IsMedicalApply").Equals("1"))
      { %>
<script type="text/javascript">
    printview.addsection(
                printview.checkbox("N/A", true),
                "Medical Diagnosis");
</script>
              <%}
      else
      { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("ST Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  } %>
