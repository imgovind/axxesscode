﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericOTGoals = data.AnswerArray("POCGenericOTGoals"); %>
<% var noteDiscipline = data.AnswerOrEmptyString("POCDisciplineTask"); %>
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals1' class='radio' name='{1}_POCGenericOTGoals' value='1' type='checkbox' {0} />", genericOTGoals.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals1">Patient will be able to perform upper body dressing with </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals1With", data.AnswerOrEmptyString("POCGenericOTGoals1With"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals1With", @maxlength = "30" })%>
                    assist  utilizing
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals1Utilize", data.AnswerOrEmptyString("POCGenericOTGoals1Utilize"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals1Utilize", @maxlength = "30" })%>
                    device within  
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals1Weeks", data.AnswerOrEmptyString("POCGenericOTGoals1Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals1Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals1Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals2' class='radio' name='{1}_POCGenericOTGoals' value='2' type='checkbox' {0} />", genericOTGoals.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals2">Patient will be able to perform lower body dressing with </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals2With", data.AnswerOrEmptyString("POCGenericOTGoals2With"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals2With", @maxlength = "30" })%> 
                    assist utilizing 
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals2Utilize", data.AnswerOrEmptyString("POCGenericOTGoals2Utilize"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals2Utilize", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals2Utilize">device within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals2Weeks", data.AnswerOrEmptyString("POCGenericOTGoals2Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals2Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals2Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals3' class='radio' name='{1}_POCGenericOTGoals' value='3' type='checkbox' {0} />", genericOTGoals.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals3">Patient will be able to perform toilet hygiene with </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals3Assist", data.AnswerOrEmptyString("POCGenericOTGoals3Assist"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals3Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals3Assist">assist within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals3Weeks", data.AnswerOrEmptyString("POCGenericOTGoals3Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals3Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals3Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals4' class='radio' name='{1}_POCGenericOTGoals' value='4' type='checkbox' {0} />", genericOTGoals.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals4">Patient will be able to perform grooming with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals4Assist", data.AnswerOrEmptyString("POCGenericOTGoals4Assist"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals4Assist", @maxlength = "30" })%> 
                    assist within 
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals4Weeks", data.AnswerOrEmptyString("POCGenericOTGoals4Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals4Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals4Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals5' class='radio' name='{1}_POCGenericOTGoals' value='5' type='checkbox' {0} />", genericOTGoals.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals5">Patient will be able to perform </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals5With", data.AnswerOrEmptyString("POCGenericOTGoals5With"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals5With", @maxlength = "30" })%> 
                    (task)  with 
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals5Assist", data.AnswerOrEmptyString("POCGenericOTGoals5Assist"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals5Assist", @maxlength = "30" })%> 
                     <label for="<%= Model.Type %>_POCGenericOTGoals5Assist">assist using </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals5Within", data.AnswerOrEmptyString("POCGenericOTGoals5Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals5Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals5Within">device within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals5Weeks", data.AnswerOrEmptyString("POCGenericOTGoals5Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals5Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals5Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals6' class='radio' name='{1}_POCGenericOTGoals' value='6' type='checkbox' {0} />", genericOTGoals.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals6">Improve strength of</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals6Of", data.AnswerOrEmptyString("POCGenericOTGoals6Of"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals6Of", @maxlength = "30" })%> 
                    to
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals6To", data.AnswerOrEmptyString("POCGenericOTGoals6To"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals6To", @maxlength = "30" })%> 
                    grade to improve
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals6Within", data.AnswerOrEmptyString("POCGenericOTGoals6Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals6Within", @maxlength = "30" })%> 
                    within
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals6Weeks", data.AnswerOrEmptyString("POCGenericOTGoals6Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals6Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals6Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals7' class='radio' name='{1}_POCGenericOTGoals' value='7' type='checkbox' {0} />", genericOTGoals.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals7">Increase muscle strength of </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals7Skill", data.AnswerOrEmptyString("POCGenericOTGoals7Skill"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals7Skill", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals7Skill"> to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals7Assist", data.AnswerOrEmptyString("POCGenericOTGoals7Assist"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals7Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals7Assist">grade to improve </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals7Within", data.AnswerOrEmptyString("POCGenericOTGoals7Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals7Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals7Within">(task) within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals7Weeks", data.AnswerOrEmptyString("POCGenericOTGoals7Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals7Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals7Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals8' class='radio' name='{1}_POCGenericOTGoals' value='8' type='checkbox' {0} />", genericOTGoals.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals8">Increase trunk muscle strength to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals8To", data.AnswerOrEmptyString("POCGenericOTGoals8To"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals8To", @maxlength = "30" })%> 
                    to improve postural control and balance necessary to perform
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals8Within", data.AnswerOrEmptyString("POCGenericOTGoals8Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals8Within", @maxlength = "30" })%> 
                    (task) within
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals8Weeks", data.AnswerOrEmptyString("POCGenericOTGoals8Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals8Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals8Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals9' class='radio' name='{1}_GenericOTGoals' value='9' type='checkbox' {0} />", genericOTGoals.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals9">Increase trunk muscle strength to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals9Within", data.AnswerOrEmptyString("GenericOTGoals9Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals9Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals9Within">to improve postural control during bed mobility and transfer within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals9Weeks", data.AnswerOrEmptyString("GenericOTGoals9Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals9Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals9Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals10' class='radio' name='{1}_POCGenericOTGoals' value='10' type='checkbox' {0} />", genericOTGoals.Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals10">Patient will increase ROM of </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals10Device", data.AnswerOrEmptyString("POCGenericOTGoals10Device"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals10Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals10Device">joint to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals10Feet", data.AnswerOrEmptyString("POCGenericOTGoals10Feet"), new { @class = "sn", @id = Model.Type + "_POCGenericOTGoals10Feet", @maxlength = "10" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals10Feet">degree of</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals10Within", data.AnswerOrEmptyString("POCGenericOTGoals10Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals10Within", @maxlength = "30" })%> 
                    within
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals10Weeks", data.AnswerOrEmptyString("POCGenericOTGoals10Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals10Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals10Weeks">weeks to improve </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals10DegreeOf", data.AnswerOrEmptyString("POCGenericOTGoals10DegreeOf"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals10DegreeOf", @maxlength = "30" })%> 
                    (task).
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals11' class='radio' name='{1}_POCGenericOTGoals' value='11' type='checkbox' {0} />", genericOTGoals.Contains("11").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals11">Patient/caregiver will be able to perform HEP safely and effectively within  </label>                    
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals11Weeks", data.AnswerOrEmptyString("POCGenericOTGoals11Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals11Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals11Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals12' class='radio' name='{1}_POCGenericOTGoals' value='12' type='checkbox' {0} />", genericOTGoals.Contains("12").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals12">Patient will learn</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals12Device", data.AnswerOrEmptyString("POCGenericOTGoals12Device"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals12Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals12Device">techniques to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals12Within", data.AnswerOrEmptyString("POCGenericOTGoals12Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals12Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals12Within">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals12Weeks", data.AnswerOrEmptyString("POCGenericOTGoals12Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals12Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals12Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals13' class='radio' name='{1}_POCGenericOTGoals' value='13' type='checkbox' {0} />", genericOTGoals.Contains("13").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals13">Patient will improve</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals13Of", data.AnswerOrEmptyString("POCGenericOTGoals13Of"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals13Of", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals13Of">standardized test score to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals13To", data.AnswerOrEmptyString("POCGenericOTGoals13To"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals13To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals13To">to improve</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals13Within", data.AnswerOrEmptyString("POCGenericOTGoals13Within"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals13Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals13Within">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals13Weeks", data.AnswerOrEmptyString("POCGenericOTGoals13Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals13Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals13Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals14' class='radio' name='{1}_POCGenericOTGoals' value='14' type='checkbox' {0} />", genericOTGoals.Contains("14").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals14">Patient/caregiver will demonstrate proper use of brace/splint within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals14Weeks", data.AnswerOrEmptyString("POCGenericOTGoals14Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals14Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals14Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals15' class='radio' name='{1}_POCGenericOTGoals' value='15' type='checkbox' {0} />", genericOTGoals.Contains("15").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals15">Patient/caregiver will demonstrate proper use of wheelchair with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals15To", data.AnswerOrEmptyString("POCGenericOTGoals15To"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals15To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals15Feet">assist within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals15Weeks", data.AnswerOrEmptyString("POCGenericOTGoals15Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals15Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals15Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals16' class='radio' name='{1}_POCGenericOTGoals' value='16' type='checkbox' {0} />", genericOTGoals.Contains("16").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals16">Patient will be able to perform upper body bathing with  </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals16With", data.AnswerOrEmptyString("POCGenericOTGoals16With"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals16With", @maxlength = "30" })%> 
                    assist utilizing
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals16To", data.AnswerOrEmptyString("POCGenericOTGoals16To"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals16To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals16Feet">device within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals16Weeks", data.AnswerOrEmptyString("POCGenericOTGoals16Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals16Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals16Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericOTGoals17' class='radio' name='{1}_POCGenericOTGoals' value='17' type='checkbox' {0} />", genericOTGoals.Contains("17").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericOTGoals17">Patient will be able to perform lower body bathing with </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals17Joint", data.AnswerOrEmptyString("POCGenericOTGoals17Joint"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals17Joint", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals17Joint">assist utilizing  </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals17Degree", data.AnswerOrEmptyString("POCGenericOTGoals17Degree"), new { @class = "", @id = Model.Type + "_POCGenericOTGoals17Degree", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals17Degree">device within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericOTGoals17Weeks", data.AnswerOrEmptyString("POCGenericOTGoals17Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericOTGoals17Weeks", @maxlength = "3" })%> 
                    <label for="<%= Model.Type %>_POCGenericOTGoals17Weeks">weeks</label>
               </div>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericOTGoalsComments">Additional goals</label><br />
                <%= Html.TextArea(Model.Type + "_POCGenericOTGoalsComments", data.AnswerOrEmptyString("POCGenericOTGoalsComments"), new { @id = Model.Type + "_POCGenericOTGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GPOCenericOTGoalsComments">OT Short Term Goals</label><br />
                <%= Html.Templates(Model.Type + "_POCGenericOTShortTermGoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericOTShortTermGoalsComments" })%>
                <%= Html.TextArea(Model.Type + "_POCGenericOTShortTermGoalsComments", data.AnswerOrEmptyString("POCGenericOTShortTermGoalsComments"), new { @id = Model.Type + "_POCGenericOTShortTermGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericOTLongTermGoalsComments">OT Long Term Goals</label><br />
                <%= Html.Templates(Model.Type + "_POCGenericOTLongTermGoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericOTLongTermGoalsComments" })%>
                <%= Html.TextArea(Model.Type + "_POCGenericOTLongTermGoalsComments", data.AnswerOrEmptyString("POCGenericOTLongTermGoalsComments"), new { @id = Model.Type + "_POCGenericOTLongTermGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input class='radio' id='{0}OTGoalsDesired0' name='{0}_OTGoalsDesired' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("0").ToChecked())%>Patient
                <%= string.Format("<input class='radio' id='{0}OTGoalsDesired1' name='{0}_OTGoalsDesired' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("1").ToChecked())%>Caregiver
                <span> desired outcomes:</span>
                <%= Html.TextBox(Model.Type + "_POCOTGoalsDesiredOutcomes", data.AnswerOrEmptyString("POCOTGoalsDesiredOutcomes"), new { @class = "", @id = Model.Type + "_POCOTGoalsDesiredOutcomes" })%> 
            </td>
        </tr>
    </tbody>
</table>