﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Face-To-Face Encounters</span>
<fieldset class="orders-filter align-center">
    <div class="buttons float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="Order.GetFaceToFaceList();">Generate</a></li>
        </ul>
    </div>
    <div>
    <label class="float-left">Agency:</label>
    <%=Html.LookupSelectList("agencies", "FacetoFaceList_AgencyId", Current.DefaultAgencyId.ToString(), new { @id = "FacetoFaceList_AgencyId", @class = "" })%>
    </div>
    <div class="clear" />
    <label class="float-left">Date Range:</label>
    <input type="text" name="StartDate" class="date-picker shortdate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="FacetoFace_StartDate" />
    <input type="text" name="EndDate" class="date-picker shortdate" value="<%= DateTime.Now.ToShortDateString() %>" id="FacetoFace_EndDate" />
    <div class="clear" />
</fieldset>

<div class="wrapper">
    <%= Html.Telerik().Grid<FaceToFaceEncounter>().Name("List_FaceToFace").HtmlAttributes(new { style = "top:76px;" }).Columns(columns =>
{
        columns.Bound(f => f.AgencyDisplayName).Title("Agency").Sortable(true);
        columns.Bound(f => f.PatientName).Title("Patient").Sortable(true);
        columns.Bound(f => f.StatusName).Title("Status").Sortable(true);
        columns.Bound(f => f.RequestDateFormatted).Title("Request Date").Sortable(true).Width(100);
        columns.Bound(f => f.PrintUrl).Title(" ").Width(35);
        columns.Bound(f => f.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditFaceToFaceEncounter('<#=AgencyId#>','<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Edit</a>").Title("Action").Width(150);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("FaceToFaceGrid", "Order", new { agencyId = Current.DefaultAgencyId, startDate = DateTime.Now.AddDays(-60).ToShortDateString(), endDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>