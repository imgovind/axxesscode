﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<fieldset class="ac orders-filter">
    <div class="buttons float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="Order.GetOrdersList();">Generate</a></li>
        </ul>
    </div>
    <div>
        <label class="float-left">Agency:</label>
        <%=Html.LookupSelectList("agencies", "AgencyId", Current.DefaultAgencyId.ToString(), new { @id = "List_AgencyId", @class = "" })%>
    </div>
    <div class="clear" />
    <label class="float-left">Date Range:</label>
    <input type="text" name="StartDate" class="date-picker shortdate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="Order_StartDate" />
    <input type="text" name="EndDate" class="date-picker shortdate" value="<%= DateTime.Now.ToShortDateString() %>" id="Order_EndDate" />
    <div class="buttons">
        <ul>
            <%--<li><a class="groupLink" onclick="Order.Load('AgencyName');">Group By Agency</a></li>--%>
            <li><a class="groupLink" onclick="Order.Load('Patient Name');">Group By Patient</a></li>
            <li><a class="groupLink" onclick="Order.Load('Order Type');">Group By Type</a></li>
            <li><a class="groupLink" onclick="Order.Load('Order Date');">Group By Date</a></li>
        </ul>
    </div>
</fieldset>
<div id="orderListContent"><% Html.RenderPartial("~/Views/Order/ListView.ascx"); %></div>
