﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Activate - Axxess MD</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<%= Html.Telerik()
			.StyleSheetRegistrar()
			.DefaultGroup(group => group
			.Add("account.css")
			.Combined(true)
			.Compress(true)
			.CacheDurationInDays(1)
			.Version(Current.AssemblyVersion))
		%>
		<link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
		<link href="/Images/favicon.ico" rel="shortcut icon" />
		<link href="/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	</head>
	<body>
		<div class="centered-window">
			<img src="/Images/Login/logo.png" class="logo">
			<h3>To activate your account, follow the steps below.</h3>
			
			<h4 class="hint">Note: Your Electronic Signature will be the same as your new password.</h4>
			<div class="form-wrapper">
				<div id="messages" <%=Model.LoginId == Guid.Empty ? "class='notification error' style='margin-bottom:1em;'" : string.Empty %>>
				<% if (Model.LoginId == Guid.Empty) { %>
					<span class="img icon32 error"></span>
					<span>You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.</span>
				<% } %>
				</div>
				<% if (Model.LoginId != Guid.Empty) { %>
				<% using (Html.BeginForm("Activate", "Account", FormMethod.Post, new { @id = "activateAccountForm" })) { %>
				<%= Html.Hidden("UserId", Model.UserId, new { @id = "Activate_User_Id" })%>
				<%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Activate_User_AgencyId" })%>
				<%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "Activate_User_EmailAddress" })%>
				<div class="padded">
					<table>
						<tbody>
							<tr>
								<td colspan="2">
									<h2 style="margin-top: 0">Step 1 - Verify your information below.</h2>
								</td>
							</tr>
							<tr>
								<td class="label">
									<label>Name</label>
								</td>
								<td>
									<%= Html.TextBoxFor(a => a.Name, new { disabled = "disabled" })%>
								</td>
							</tr>
							<tr>
								<td class="label">
									<label>Email</label>
								</td>
								<td>
									<%= Html.TextBoxFor(a => a.EmailAddress, new { disabled = "disabled" })%>
								</td>
							</tr>
							<tr>
								<td class="label">
									<label>Agency Name</label>
								</td>
								<td>
									<%= Html.TextBoxFor(a => a.AgencyName, new { disabled = "disabled" })%>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<h2>Step 2 - Enter a new password.</h2>
								</td>
							</tr>
							<tr>
								<td class="label">
									<label for="Password" class="fl">Password</label>
								</td>
								<td>
									<%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength = "20" })%>
								</td>
							</tr>
							<tr>
								<td class="label">
									<label for="ConfirmPassword" class="fl">Confirm Password</label>
								</td>
								<td>
									<%= Html.Password("ConfirmPassword", string.Empty, new { @class = "", @maxlength = "20" })%>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<input type="submit" value="Activate Account" class="button" />
				<% } %>
			<% } %>
			</div>
			<div class="login-loading">
				<i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i>
			</div>
		</div>
		<% Html.Telerik().ScriptRegistrar().jQuery(false)
			 .DefaultGroup(group => group
				 .Add("jquery-1.8.3.min.js")
				 .Add("Plugins/Other/form.min.js")
				 .Add("Plugins/Other/validate.min.js")
				 .Add("Plugins/Other/jgrowl.min.js")
				 .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
				 .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
				 .Compress(true).Combined(true)
				 .CacheDurationInDays(1)
				 .Version(Current.AssemblyVersion))
			.OnDocumentReady(() =>{ %>
		Activate.Init();
		<% }).Render(); %>
	</body>
</html>
