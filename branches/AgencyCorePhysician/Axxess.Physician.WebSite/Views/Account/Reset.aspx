﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Change Account - Axxess MD</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<%= Html.Telerik()
			.StyleSheetRegistrar()
			.DefaultGroup(group => group
			.Add("account.css")
			.Combined(true)
			.Compress(true)
			.CacheDurationInDays(1)
			.Version(Current.AssemblyVersion))
		%>
		<link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
		<link href="/Images/favicon.ico" rel="shortcut icon" />
		<link href="/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	</head>
    <body>
      	<div class="centered-window">
			<img src="/Images/Login/logo.png" class="logo">
            <h3>To change your password/signature, follow the steps below.</h3>
			<div class="form-wrapper">
				<div id="messages"></div>
				<%  using (Html.BeginForm("Reset", "Account", FormMethod.Post, new { @id = "changePasswordForm" })) { %>
					<%= Html.Hidden("LoginId", Model.LoginId, new { @id = "ChangePassword_LoginId" })%>
					<%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "ChangePassword_EmailAddress" })%>
					<%= Html.Hidden("ResetType", Model.ResetType.ToString(), new { @id = "ChangePassword_ResetType" })%>
					<div class="padded">
						<table>
							<tbody>
								<tr>
									<td colspan="2"><h2 style="margin-top: 0;">Step 1 - Verify your information below.</h2></td>
								</tr>
								<tr>
									<td class="label"><label>Name</label></td>
									<td><%= Html.TextBoxFor(a => a.Name, new { disabled = "disabled" })%></td>
								</tr>
								<tr>
									<td class="label"><label>Email</label></td>
									<td><%= Html.TextBoxFor(a => a.EmailAddress, new { disabled = "disabled" })%></td>
								</tr>
								<tr>
									<td colspan="2"><h2>Step 2 - Enter a new <%= Model.ResetType.ToString().ToLower() %>.</h2></td>
								</tr>
								<tr>
									<td class="label"><label>New <%= Model.ResetType.ToString() %></label></td>
									<td><%= Html.Password(Model.ResetType.ToString(), Model.ResetType == Change.Password ? Model.Password : Model.Signature, new { @class = "required", @maxlength = "20" })%></td>
								</tr>
							</tbody>
						</table>
					</div>
                    <div style="padding-bottom: .1em;" class="clr"></div>
					<input type="submit" value="Change Password" class="button" />
				<%  } %>
            </div>
            <div class="login-loading">
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
			</div>
        </div>
        <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
                .Add("jquery-1.8.3.min.js")
                .Add("Plugins/Other/form.min.js")
                .Add("Plugins/Other/validate.min.js")
				.Add("Plugins/Other/jgrowl.min.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
                .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).OnDocumentReady(() => { %>
            ChangePassword.Init();
        <%  }).Render(); %>
    </body>
</html>