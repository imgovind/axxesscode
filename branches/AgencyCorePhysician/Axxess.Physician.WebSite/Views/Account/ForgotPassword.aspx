﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Forgot Password - Axxess MD</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<%= Html.Telerik()
			.StyleSheetRegistrar()
			.DefaultGroup(group => group
			.Add("account.css")
			.Combined(true)
			.Compress(true)
			.CacheDurationInDays(1)
			.Version(Current.AssemblyVersion))
		%>
		<link href="/Images/favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
        <link href="/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
	</head>
	<body>
		<div class="centered-window">
			<img src="/Images/Login/logo.png" class="logo">
            <h3>Fill in the fields below to reset your password.</h3>
			<div class="form-wrapper">
					<div id="messages"></div>
					<% using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "forgotPasswordForm" })) { %>
						<div class="padded">
							<table>
								<tbody>
									<tr>
										<td class="label"><label for="EmailAddress">Email</label></td>
										<td><%= Html.TextBoxFor(a => a.EmailAddress, new { @class = "required" })%></td>
									</tr>
									<tr>
										<td colspan="2" class="label">
											<hr/>
											<label class="fl" for="">Security Check</label><br/>
											<label class="hint fr">Enter <em class="bold">both</em> words below, separated by a <em class="bold">space</em>.</label>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<div>
												<script type="text/javascript">var RecaptchaOptions = { theme: "clean", tabindex : 0 }</script>
												<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LemQrsSAAAAAM5b7-XCi9VUDVIu2dMG49YlEo4-"></script>
												<noscript>
													<iframe src="https://www.google.com/recaptcha/api/noscript?k=6LemQrsSAAAAAM5b7-XCi9VUDVIu2dMG49YlEo4-" width="500" height="300" frameborder="0"></iframe>
													<br />
													<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
													<input name="recaptcha_response_field" value="manual_challenge" type="hidden" />
												</noscript>
											</div> 
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<input type="submit" value="Send" class="button"/>
					<% } %>
			</div>
			<div class="login-loading">
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-circle"></i>
			</div>
		</div>
	    
		<% Html.Telerik().ScriptRegistrar().jQuery(false)
			 .DefaultGroup(group => group
				 .Add("jquery-1.8.3.min.js")
				 .Add("Plugins/Other/jgrowl.min.js")
				 .Add("Plugins/Other/form.min.js")
				 .Add("Plugins/Other/validate.min.js")
				 .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
                 .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
				 .Version(Current.AssemblyVersion))
			.OnDocumentReady(() => { %> ResetPassword.Init(); <% }).Render(); %>
	</body>
</html>
