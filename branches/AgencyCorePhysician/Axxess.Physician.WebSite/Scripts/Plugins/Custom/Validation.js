$.validator.messages.required = "* Required";
$.validator.messages.email = $.validator.messages.invalid = "* Invalid";
$.validator.addMethod("notzero", function(value, element) { return value.trim().length && !/^0$/.test(value) && !/^0{8}-0{4}-0{4}-0{4}-0{12}$/.test(value) }, $.validator.messages.required);
$.validator.addMethod("physician-picker", function(value, element) { return !value.length || /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test($(element).siblings("input[type=hidden]").val().trim()) }, $.validator.messages.invalid);
$.validator.addMethod("phone-short", function(value, element) { return !value.length || /^\d{3}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("phone-long", function(value, element) { return !value.length || /^\d{4}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("zip", function(value, element) { return !value.length || /^\d{5}(\d{4})?$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("ssn", function(value, element) { return !value.length || /^\d{9}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("email", function(value, element) { return !value.length || /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(value) }, $.validator.messages.invalid);
(function($) {
    $.extend($.fn, {
        Decimal: function(Options) {
            return this.each(function() {
                if (Options == undefined) Options = {};
                if (Options.DecimalLength == undefined) Options.DecimalLength = 2;
                if (Options.ZeroFill == undefined) Options.ZeroFill = false;
                $(this).keyup(function() {
                    $(this).val($(this).val().replace(/[^0-9\.]*/g, ""))
                });
                $(this).blur(function() {
                    var Value = $(this).val();
                    Value = Value.replace(/[^0-9\.]*/g, "");
                    var ValSplit = Value.split(/\./);
                    if (ValSplit.length > 1) {
                        Value = ValSplit[0] + ".";
                        for (i = 1; i < ValSplit.length; i++) Value = Value + ValSplit[i];
                        if (Options.ZeroFill) for (i = 0; i < Options.DecimalLength; i++) Value = Value + "0";
                        if (Value.length > Value.indexOf(".") + Options.DecimalLength) Value = Value.substring(0, Value.indexOf(".") + Options.DecimalLength + 1);
                    } else if (Options.ZeroFill) Value = Value + ".00";
                    $(this).val(Value);
                });
            })
        },
        Code: function(Options) {
            return this.each(function() {
                if (Options == undefined) Options = {};
                if (Options.DecimalLength == undefined) Options.DecimalLength = 2;
                if (Options.ZeroFill == undefined) Options.ZeroFill = false;
                $(this).keyup(function() {
                    var firstCharacter = $(this).val().substring(0, 1);
                    var firstCharacterLowered = firstCharacter.toLowerCase();
                    var replacement = $(this).val().replace(/[^0-9\.]*/g, "");
                    if (firstCharacterLowered == "v" || firstCharacterLowered == "e") {
                        $(this).val(firstCharacter + replacement)
                    } else {
                        $(this).val(replacement);
                    }
                });
                $(this).blur(function() {
                    var Value = $(this).val();
                    var firstCharacter = Value.substring(0, 1);
                    var firstCharacterLowered = firstCharacter.toLowerCase();
                    var replacement = $(this).val().replace(/[^0-9\.]*/g, "");
                    if (firstCharacterLowered == "v" || firstCharacterLowered == "e") {
                        Value = firstCharacter + replacement;
                    } else {
                        Value = replacement;
                    }
                    var ValSplit = Value.split(/\./);
                    if (ValSplit.length > 1) {
                        Value = ValSplit[0] + ".";
                        for (i = 1; i < ValSplit.length; i++) Value = Value + ValSplit[i];
                        if (Options.ZeroFill) for (i = 0; i < Options.DecimalLength; i++) Value = Value + "0";
                        if (Value.length > Value.indexOf(".") + Options.DecimalLength) Value = Value.substring(0, Value.indexOf(".") + Options.DecimalLength + 1);
                    } else if (Options.ZeroFill) Value = Value + ".00";
                    $(this).val(Value);
                });
            })
        },
        Currency: function(Options) {
            return this.each(function() {
                $(this).Decimal({ ZeroFilled: true })
            })
        },
        Validate: function(Options) {
            return this.each(function() {
                if (Options == undefined) Options = new Object();
                var form = this,
                    AfterSubmit = function(Result) {
                        if (Result.responseText && Result.status == 200) Result = JSON.parse(Result.responseText.replace(/<[^>]*>/g, ""));
                        var Message = Result.errorMessage;
                        if (Result.isSuccessful) {
                            $(form).trigger("SubmitSuccess");
                            if (!Message) Message = "Your information was successfully saved.";
                            if (Options.Success != undefined) try {
                                if (typeof Options.Success == "function") Options.Success(Result, form);
                                else if (typeof Options.Success == "string") eval(Options.Success);
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " success function: " + e.toString());
                            }
                            U.Growl(Message, "success");
                        } else {
                            $(form).trigger("SubmitFailure");
                            if (!Message) Message = "There was an error while trying to save your information.";
                            if (Options.Success != undefined) try {
                                if (typeof Options.Fail == "function") Options.Fail(Result, form);
                                else if (typeof Options.Fail == "string") eval(Options.Fail);
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " fail function: " + e.toString());
                            }
                            U.Growl(Message, "error");
                        }
                    };
                $(form).bind("SubmitSuccess", function() { }).bind("SubmitFailure", function() { }).validate(
                    $.extend({
                        invalidHandler: function() {
                            U.Growl("There was a problem validating your information.", "error")
                            $(".phone-short.required", form).each(function() { if ($(this).val() == "") $(this).parent().find("input").addClass("error") });
                            $(form).closest(".window-content").scrollTop(0);
                        },
                        submitHandler: function() {
                            $(form).ajaxSubmit({
                                beforeSubmit: typeof Options.BeforeSubmit == "function" ? Options.BeforeSubmit : function() { },
                                dataType: "json",
                                success: AfterSubmit,
                                error: AfterSubmit
                            })
                            return false
                        }
                    }, Options)
                )
            })
        }
    })
})(jQuery);