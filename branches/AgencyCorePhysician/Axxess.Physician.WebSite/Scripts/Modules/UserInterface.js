﻿var UserInterface = {
    CloseWindow: function(window) {
        $("#" + window).Close();
    },
    ShowEditFaceToFaceEncounter: function(agencyId, episodeId, patientId, orderId) {
        Acore.Open("faceToFace", 'Order/FaceToFaceEncounter', Order.InitFaceToFaceEncounter, { agencyId: agencyId, episodeId: episodeId, patientId: patientId, id: orderId })
    },
    ShowEditCPO: function(agencyId, cpoId) {
    Acore.Open("editcpo", 'Order/CarePlanOversights', '', { agencyId: agencyId , id: cpoId })
    }
}
