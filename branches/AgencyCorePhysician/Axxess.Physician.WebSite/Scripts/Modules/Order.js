﻿var Order = {
    Load: function(groupby) {
        var grid = $("#orderGrid").data("tGrid");
        if (grid) {
            grid.groups = [];
            $("#orderGrid").data("tGrid").group(groupby, "asc");
        }
        //        $("#orderListContent").empty().addClass("loading").load('Order/ListView', { groupName: groupby }, function(responseText, textStatus, XMLHttpRequest) {
        //            if (textStatus == 'error') U.Growl('Pending Orders List could not be grouped. Please close this window and try again.', 'error');
        //            else if (textStatus == "success") $("#orderListContent").removeClass("loading");
        //        });
    },
    Rebind: function() {
        U.RebindTGrid($('#orderGrid'));
    },
    RebindFaceToFace: function() {
        U.RebindTGrid($('#List_FaceToFace'));
    },
    Update: function(action, agencyId, episodeId, patientId, orderId, orderType) {
        var reason = "";
        if (action == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        $("#window_Dialog").Close();
                        U.Growl(result.errorMessage, "success");
                        Order.Rebind();
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    $("#window_Dialog").Close();
                    $(this).closest(".window").Close();
                    U.Growl(result.errorMessage, "success");
                    Order.Rebind();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    GetCompletedList: function() {
        var grid = $('#List_CompleteOrders').data('tGrid');
        if (grid != null) {
            grid.rebind({ agencyId: $('#CompleteList_AgencyId').val(), startDate: $('#CompleteOrder_StartDate').val(), endDate: $('#CompleteOrder_EndDate').val() });
        }
    },
    GetOrdersList: function() {
        var grid = $('#orderGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ agencyId: $('#List_AgencyId').val(), startDate: $('#Order_StartDate').val(), endDate: $('#Order_EndDate').val() });
        }
    },
    GetFaceToFaceList: function() {
        var grid = $('#List_FaceToFace').data('tGrid');
        if (grid != null) {
            grid.rebind({ agencyId: $('#FacetoFaceList_AgencyId').val(), startDate: $('#FacetoFace_StartDate').val(), endDate: $('#FacetoFace_EndDate').val() });
        }
    },
    InitFaceToFaceEncounter: function(responseText, textStatus, XMLHttpRequest, Element) {
        U.ShowIfChecked($("#FaceToFace_Services7"), $("#FaceToFace_ServicesOther"));
        U.ShowIfOtherChecked($("#FaceToFace_Services7"), $("#FaceToFace_ServicesOther"));
        $(".complete", Element).click(function() {
            $("#Edit_FaceToFace_Status").val("135");
            $(this).closest("form").submit();
        });
        $("#editFaceToFaceForm").Validate({
            Success: function(result) {
                if (result.isSuccessful) {
                    Order.RebindFaceToFace();
                    U.Growl(result.errorMessage, "success");
                    UserInterface.CloseWindow("faceToFace");
                } else U.Growl(result.errorMessage, "error");
            }
        })
    }
}

var CarePlanOversight = {
    Load: function(groupby) {
        $("#cpoListContent").empty().addClass("loading").load('Order/CPO/ListView', { groupName: groupby }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Care Plan Oversight List could not be grouped. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#cpoListContent").removeClass("loading");
        });
    },
    Submit: function(control) {
        $('#CarePlanOversight_Button').val(control.html());
        control.closest("form").validate();
        var windowId = control.closest(".window").attr("id");
        if (control.closest("form").valid()) {
            CarePlanOversight.HandleSubmit(windowId, control.closest("form"), control);
            if (Acore.Windows["listcpo"].IsOpen) {
                CarePlanOversight.RebindList();
            }
        }
        else U.ValidationError(control);
    },
    HandleSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl("The Care Plan Oversight Log has been saved.", "success");
                    } else if (control.html() == "Complete") {
                        U.Growl("The Care Plan Oversight Log has been saved and completed.", "success");
                        CarePlanOversight.RebindList();
                        $("#" + page).Close();
                    }
                } else U.Growl(result.errorMessage, "error");
            },
            error: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl("The Care Plan Oversight Log has been saved.", "success");
                    } else if (control.html() == "Complete") {
                        U.Growl("The Care Plan Oversight Log has been saved and completed.", "success");
                        CarePlanOversight.RebindList();
                        $("#" + page).Close();
                    }
                } else U.Growl(result.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    GetCPOList: function() {
        var grid = $('#List_CPO').data('tGrid');
        if (grid != null) {
            grid.rebind({ agencyId: $('#CPO_AgencyId').val(), startDate: $('#CPO_StartDate').val(), endDate: $('#CPO_EndDate').val()});
        }
    },
    Delete: function(agencyId,CPOid) {
    if (confirm("Are you sure you want to delete this CPO log?")) {
            U.PostUrl("/Order/DeleteCPO", { agencyId: agencyId, id: CPOid }, function(result) {
                if (result.isSuccessful) {
                    CarePlanOversight.RebindList();
                    U.Growl(result.errorMessage, "success");
                } else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    SubmitCpoBill: function(idList) {
        U.PostUrl("/Order/SendCpoBill", { List: idList }, function(result) {
            if (result.isSuccessful) {
                CarePlanOversight.RebindList();
                U.Growl(result.errorMessage, "success");
            } else {
                U.Growl(result.errorMessage, "error");
            }
        });
    },
    RebindList: function() {
        $('#cpoListContent').load('/Order/CPO/List');
    },
    AddCheckListOnRowDataBound: function(e) {
        var billchecked = $(".BillChecked");
        var cpoBill = $('#CPO_BillList');
        billchecked.change(function() {
            if ($(this).is(':checked')) {
                var idList = cpoBill.val() + $(this).attr('value') + ",";
                cpoBill.val(idList);
            } else {
                var temp = cpoBill.val().replace($(this).attr('value') + ',', '');
                cpoBill.val(temp);
            }
            //alert($('#CPO_BillList').val());
        });
    },
    
    LoadPatientsDropDown: function(dropDown, control) {
        U.PostUrl("/Order/AgencyPatientList", { agencyId: $(control).val() }, function(data) {
            var s = $("#" + dropDown);
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.PatientId, false, false);
            });
        });
    }
}