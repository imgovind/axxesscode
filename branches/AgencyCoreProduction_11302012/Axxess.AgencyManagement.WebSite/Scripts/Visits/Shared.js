﻿var Visit = {
    Shared: {
        Init: function(Type, InitFunction, ContentUrl) {
            var Prefix = "#" + Type + "_";
            if (ContentUrl == undefined) ContentUrl = "Schedule/" + Type + "Content";
            Template.OnChangeInit();

            $("#" + Type + "_PreviousNotes").change(function() {
                $(Prefix + "Content").Load(ContentUrl, {
                    patientId: $("#" + Type + "_PatientId").val(),
                    noteId: $("#" + Type + "_PreviousNotes").val(),
                    type: Type
                }, InitFunction);
            })
        },
        Submit: function(Button, Completing, Type) {
            if (Completing) $(".complete-required", "#window_" + Type).addClass("required");
            else $(".complete-required", "#window_" + Type).removeClass("required");
            $("#" + Type + "_Button").val(Button.text());
            Button.closest("form").validate();
            if (Button.closest("form").valid()) {
                Button.closest("form").ajaxSubmit({
                    dataType: "json",
                    beforeSubmit: Visit.Shared.BeforeSubmit,
                    success: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type) },
                    error: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type) }
                })
            } else U.ValidationError(Button);
        },
        BeforeSubmit: function() { },
        AfterSubmit: function(Result, Command, Type) {
            if (Result.isSuccessful) switch (Command) {
                case "Save":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.Refresh();
                    break;
                case "Complete":
                case "Approve":
                case "Return":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.CloseAndRefresh(Type);
                    break;
            } else {
                if (Result.errorMessage) {
                    U.Growl(Result.errorMessage, "error");
                }
                else {
                    U.Growl("The note contains potentially dangerous values. Ensure that special characters such as #,$,<,&...etc are not next to each other.", "error");
                }

            };
        },
        Print: function(EpisodeId, PatientId, EventId, QA, Type, Load) {
            if (Load == undefined) Load = function() {
                eval("Visit." + Type + ".Load('" + EpisodeId + "','" + PatientId + "','" + EventId + "')");
                UserInterface.CloseModal();
            };
            var Arguments = {
                Url: Type + "/View/" + EpisodeId + "/" + PatientId + "/" + EventId,
                PdfUrl: "Schedule/" + Type + "Pdf",
                PdfData: {
                    episodeId: EpisodeId,
                    patientId: PatientId,
                    eventId: EventId
                }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Schedule.ProcessNote("Return", EpisodeId, PatientId, EventId) },
                Buttons: [
                    {
                        Text: "Edit",
                        Click: function() {
                            if (typeof Load === "function") {
                                try {
                                    Load();
                                } catch (e) {
                                    U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                    console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                    $.error(e)
                                }
                            }
                        }
                    }, {
                        Text: "Approve",
                        Click: function() {
                            Schedule.ProcessNote("Approve", EpisodeId, PatientId, EventId)
                        }
                    }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
};