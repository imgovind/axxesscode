﻿var Schedule = {
    _scheduleWindow: "",
    _patientId: "",
    _patientStatus: "",
    _EpisodeId: "",
    _patientName: "",
    _EpisodeStartDate: "",
    _EpisodeEndDate: "",
    _tableId: "NursingScheduleTable",
    _Discipline: "all",
    _DisciplineIndex: 0,
    _isRebind: false,
    GetTableId: function() { return Schedule._tableId; },
    SetTableId: function(tableId) { Schedule._tableId = tableId; },
    GetId: function() { return Schedule._patientId; },
    SetId: function(patientId) { Schedule._patientId = patientId; },
    SetName: function(patientName) { Schedule._patientName = patientName; },
    SetPatientRowIndex: function(patientRowIndex) { Schedule._patientRowIndex = patientRowIndex; },
    GetEpisodeId: function() { return Schedule._EpisodeId; },
    SetEpisodeId: function(episodeId) { Schedule._EpisodeId = episodeId; },
    SetStartDate: function(episodeStartDate) { Schedule._EpisodeStartDate = episodeStartDate; },
    SetEndDate: function(episodeEndDate) { Schedule._EpisodeEndDate = episodeEndDate; },
    SetDiscipline: function(discipline) { Schedule._Discipline = discipline; },
    SetDisciplineIndex: function(disciplineIndex) { Schedule._DisciplineIndex = disciplineIndex; },
    Filter: function(text) {
        search = text.split(" ");
        $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#ScheduleSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#ScheduleSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
    },
    InitCenter: function() {
        Lookup.LoadMultipleDisciplines("#multiple-schedule-table");
        $('#window_schedulecenter .layout').layout();
        $('#window_schedulecenter .t-grid-content').css({ height: 'auto' });
        Schedule.positionBottom();
        $("#window_schedulecenter .top input").keyup(function() {
            if ($(this).val()) {
                Schedule.Filter($(this).val())
                if ($("tr.match:even", "#ScheduleSelectionGrid .t-grid-content").length) $("tr.match:first", "#ScheduleSelectionGrid .t-grid-content").click();
                else $("#ScheduleMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3" +
                    "Cdiv class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javas" +
                    "cript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            } else {
                $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match t-alt").show();
                $("tr:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
                $("tr:first", "#ScheduleSelectionGrid .t-grid-content").click();
            }
        });
        var status = Schedule._patientStatus;
        Schedule._patientId = "";
        $("#window_schedulecenter .top select").change(function() { Schedule._patientId = ""; Schedule._patientStatus = ""; U.FilterResults("Schedule"); });
        if (Schedule._patientStatus == "true") {
            $("select.ScheduleStatusDropDown").val(2);
            $("select.ScheduleStatusDropDown").change();
        }
        Schedule._patientStatus = status;
    },
    Add: function(date) {
        if ($(".schedule-tables.purgable tbody tr").length > 0) Schedule.CurrentTableAdd(date);
        else Schedule.AllTableAdd(date);
    },
    CurrentTableAdd: function(currentday) {
        if ($('#schedule-tab-strip').is(':hidden')) Schedule.ShowScheduler();
        if ($('tbody  tr', $("#" + Schedule.GetTableId())).length < 4) Schedule.addTableRow("#" + Schedule.GetTableId(), currentday, Schedule._Discipline);
    },
    AllTableAdd: function(date) {
        if (date == undefined) date = "";
        Schedule.addTableRow("#NursingScheduleTable", date, "Nursing");
        Schedule.addTableRow("#TherapyScheduleTable", date, "Therapy");
        Schedule.addTableRow("#HHAScheduleTable", date, "HHA");
        Schedule.addTableRow("#MSWScheduleTable", date, "MSW");
        Schedule.addTableRow("#OrdersScheduleTable", date, "Orders");
        Schedule.ShowScheduler();
    },
    RebindActivity: function() {
        var grid = $('#ScheduleActivityGrid').data('tGrid');
        if (grid != null) grid.rebind({ episodeId: Schedule._EpisodeId, patientId: Schedule._patientId, discipline: Schedule._Discipline });
    },
    RebindCalendar: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Schedule/GetEpisode",
            data: "patientId=" + Schedule._patientId + "&episodeId=" + Schedule._EpisodeId + "&discipline=" + Schedule._Discipline,
            success: function(result) {
                var data = eval(result);
                $("#EpisodeStartDate").text((data.StartDateFormatted !== null ? data.StartDateFormatted : "________"));
                $("#EpisodeEndDate").text((data.EndDateFormatted !== null && data.EndDateFormatted !== undefined ? data.EndDateFormatted : "________"));
                if (data.HasNext) {
                    if (data.NextEpisode != null) {
                        $("#nextEpisode").val(data.NextEpisode.Id);
                        $("#nextEpisode").show();
                    } else $("#nextEpisode").hide();
                } else $("#nextEpisode").hide();
                if (data.HasPrevious) {
                    if (data.PreviousEpisode != null) {
                        $("#previousEpisode").val(data.PreviousEpisode.Id);
                        $("#previousEpisode").show();
                    } else $("#previousEpisode").hide();
                } else $("#previousEpisode").hide();
            }
        });
    },
    MissedVisitPopup: function(e, missedVisitId) {
        Acore.Modal({
            Name: "Missed Visit",
            Url: "Schedule/MissedVisitInfo",
            Input: { id: missedVisitId },
            Width: "500px",
            Height: "300px"
        })
    },
    ShowAll: function(patientId) {
        Schedule.SetDiscipline('all');
        if ($("#scheduleTop").html() != null) {
            Schedule.loadCalendar(patientId, Schedule._Discipline);

        } else {
            Schedule.loadCalendarAndActivities(patientId);
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            if (!Schedule._isRebind) {
                Schedule._EpisodeId = "";
            }
            Schedule._Discipline = "all";
            Schedule._DisciplineIndex = 0;
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Schedule.loadCalendarAndActivities(patientId, Schedule._EpisodeId);
            Lookup.LoadMultipleDisciplines("#multiple-schedule-table");
        }
    },
    loadCalendarAndActivities: function(patientId, episodeId) {
        if (episodeId != null && episodeId != "") {
            $('#ScheduleMainResult').empty().addClass('loading').load('Schedule/RefreshData', { patientId: patientId, episodeId: episodeId }, function(responseText, textStatus, XMLHttpRequest) {
                $('#ScheduleMainResult').removeClass('loading');
                Schedule.SetId(patientId);
                if (textStatus == 'error') $('#ScheduleMainResult').html(U.AjaxError);
                $('#window_schedulecenter_content .t-grid-content').css({ height: 'auto' });
            });
        }
        else {
            $('#ScheduleMainResult').empty().addClass('loading').load('Schedule/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
                $('#ScheduleMainResult').removeClass('loading');
                Schedule.SetId(patientId);
                if (textStatus == 'error') $('#ScheduleMainResult').html(U.AjaxError);
                $('#window_schedulecenter_content .t-grid-content').css({ height: 'auto' });
            });
        }
        Schedule._isRebind = false;
    },
    RefreshCurrentEpisode: function(patientId, episodeId) {
        $('#ScheduleMainResult').empty().addClass('loading').load('Schedule/RefreshData', { patientId: patientId, episodeId: episodeId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#ScheduleMainResult').removeClass('loading');
            Schedule.SetId(patientId);
            if (textStatus == 'error') $('#ScheduleMainResult').html(U.AjaxError);
            $('#window_schedulecenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    NoPatientBind: function(id) {
        Schedule.loadCalendar(id, Schedule._Discipline);
        Schedule.loadFirstActivity(id);
    },
    addTableRow: function(table, currentday, disp) {
        if ($(table + " tbody tr").length && $(table + " tbody tr:first input.current-date").val() == "") $(table + " tbody tr:first input.current-date").val(currentday);
        else {
            var row = "%3Ctr%3E%3Ctd%3E%3Cselect class=%22DisciplineTask requireddropdown%22%3E%3Coption value=%220%22%3E-- Select Discipline --%3C/option%3E%3C/select%3E%3C/td%3" +
                "E%3Ctd%3E%3Cselect class=%22supplies Code Users requireddropdown%22%3E%3Coption value=%2200000000-0000-0000-0000-000000000000%22%3ESelect Employee%3C/option%3E%3" +
                "C/select%3E%3C/td%3E%3Ctd%3E%3Cinput onclick=%22javascript:void(0);%22 type=%22text%22 class=%22current-date%22 value=%22" + currentday + "%22 re" +
                "adonly=%22readonly%22 /%3E%3C/td%3E%3Ctd%3E%3Ca href=%22javascript:void(0);%22 class=%22action%22 onclick=%22Oasis.DeleteRow($(this));%22%3EDele" +
                "te%3C/a%3E%3C/td%3E%3C/tr%3E";
            $(table).find('tbody').append(unescape(row));
            Lookup.LoadDiscipline(table, disp);
            Lookup.LoadUsers(table);
            Schedule.positionBottom();
        }
    },
    ScheduleInputFix: function(control, Type, tableName) {
        var submit = true;
        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 1;
        var jsonData = '[';
        $(tableTr).each(function() {
            if ($(this).find("select.DisciplineTask").val() != "0" && $(this).find("select.Users").val() != "00000000-0000-0000-0000-000000000000" && $(this).find("input.current-date").val() != "") {
                if (len + 1 > i) jsonData += '{"DisciplineTask":"' + $(this).find('.DisciplineTask').val() + '","UserId":"' + $(this).find('.Users').val() + '","EventDate":"' + $(this).find('.current-date').val() + '","Discipline":"' + $(this).find('.DisciplineTask').find(":selected").attr("data") + '","IsBillable":"' + $(this).find('.DisciplineTask').find(":selected").attr("isbillable") + ' "}';
                if (len > i) jsonData += ',';
                i++
            } else {
                U.Growl("Unable to schedule this task. Make sure all required fields are entered in row " + ($(this).parent().children().index($(this)) + 1) + ". ", "error");
                submit = false;
            }
        });
        jsonData += ']';
        control.closest('form').find('input[name= ' + Type + '_Schedule][type=hidden]').val(jsonData.toString());
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        if (submit) Schedule.FormSubmit(control);
    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val()
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    control.closest("table").find('tbody').empty();
                    Schedule.CurrentTableAdd('');
                    Schedule.CloseNewEvent(control);
                    Patient.CustomDateRange();
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(resultObject.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandlerHelperMultiple: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var tabstrip = $("#schedule-tab-strip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[Schedule._DisciplineIndex];
                    tabstrip.select(item);
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Schedule.CloseNewEvent(control);
                    Patient.CustomDateRange();
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(resultObject.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelper(form, control);
    },
    FormSubmitMultiple: function(control) {
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        control.closest('form').find('input[name=Discipline][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("data"));
        control.closest('form').find('input[name=IsBillable][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("IsBillable"));

        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelperMultiple(form, control);
    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    if ($('#ScheduleActivityGrid').length) $('#ScheduleActivityGrid').data('tGrid').rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    if ($('#PatientActivityGrid').length) $('#PatientActivityGrid').data('tGrid').rebind({ patientId: Patient._patientId, discipline: $("select.patient-activity-drop-down").val(), dateRangeId: $("select.patient-activity-date-drop-down").val() });
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            },
            error: function() { U.Growl("Unable to reassign task to this user", "error") }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    SubmitReassign: function(control, patientId, episodeId) {
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    },
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    },
    OnSelect: function(e) {
        var content = $(e.contentElement);
        var tableControl = $('table', content);
        if ($(tableControl).attr('id') == "multiple-schedule-table") return true;
        Schedule.SetDisciplineIndex($(e.item).index());
        Schedule.SetTableId($(tableControl).attr('id'));
        Schedule.SetDiscipline($(tableControl).attr('data'));
        if (Schedule._Discipline == "Multiple") return;
        else {
            var patientId = $("#SchedulePatientID").val();
            var episodeId = $("#ScheduleEpisodeID").val()
            Schedule.loadCalendarNavigation(episodeId, patientId);
            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
            var name = "#" + Schedule.GetTableId();
            var $table = $(name);
        }
    },
    Delete: function(patientId, episodeId, eventId, employeeId, task) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId + "&task=" + task;
            U.PostUrl("/Schedule/Delete", input, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Agency.RebindMissedVisitList();
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Patient.CustomDateRange();
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    Restore: function(episodeId, patientId, eventId) {
        if (confirm("Are you sure you want to restore this task?")) {
            var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
            U.PostUrl("/Schedule/Restore", input, function(result) {
                if (result.isSuccessful) {
                    Schedule.Rebind();
                    Patient.Rebind();
                    Agency.RebindMissedVisitList();
                    var deletedGrid = $('#List_Patient_DeletedTasks').data('tGrid');
                    if (deletedGrid != null) {
                        deletedGrid.rebind({ patientId: patientId });
                    }
                    U.Growl(result.errorMessage, "success");
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    ReAssign: function(control, episodeId, patientId, id, oldEmployeeId) {
        control.hide();
        control.parent().append(unescape("%3Cform action=%22/Schedule/ReAssign%22 method=%22post%22%3E%3Cinput name=%22episodeId%22 type=%22hidden%22 value=%22" +
            episodeId + "%22 /%3E%3Cinput name=%22oldUserId%22 type=%22hidden%22 value=%22" + oldEmployeeId + "%22 /%3E%3Cinput name=%22patientId%22 type=%22hidd" +
            "en%22 value=%22" + patientId + "%22 /%3E%3Cinput name=%22eventId%22 type=%22hidden%22 value=%22" + id + "%22 /%3E%3Cselect class=%22Users%22 name=%2" +
            "2userId%22 style=%22width:130px;%22 class=%22Users%22%3E%3C/select%3E%3Cinput type=%22button%22 value=%22Save%22 onclick=%22Schedule.SubmitReassign(" +
            "$(this),'" + patientId + "','" + episodeId + "');%22/%3E %3Cinput type=%22button%22 value=%22Cancel%22 onclick=%22Schedule.CancelReassign($(this));%" +
            "22 /%3E%3C/form%3E"));
        Lookup.LoadUsers("#" + control.closest(".t-grid").attr("id"));
    },
    ReOpen: function(episodeId, patientId, eventId) {
        var input = "patientId=" + patientId + "&eventId=" + eventId + "&episodeId=" + episodeId;
        U.PostUrl("/Schedule/Reopen", input, function(result) {
            if (result.isSuccessful) {
                var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                if (scheduleActivityGrid != null) {
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                }
                Agency.RebindMissedVisitList();
                Schedule.loadCalendarNavigation(episodeId, patientId);
                Patient.CustomDateRange();
            } else U.Growl(result.errorMessage, "error")
        })
    },
    CancelReassign: function(control) {
        var reassignLink = control.parent().parent().find('a.reassign').show();
        control.parent().remove();
    },
    CloseNewEvent: function(control) {
        $('.schedule-tables.purgable').each(function() { $(this).find('tbody').empty(); })
        Schedule.ShowScheduler();
    },
    NavigateEpisode: function(episodeId, patientId) {
        Schedule.SetEpisodeId(episodeId);
        $("#nursingTab").click();
        Schedule.loadCalendarNavigation(episodeId, patientId);
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
        $('#schedule-tab-strip').find('table.schedule-tables tbody').empty();
        $('#schedule-tab-strip').hide();
        Schedule.positionBottom();
    },
    loadCalendarNavigation: function(EpisodeId, PatientId) {
        $('#scheduleTop').load('Schedule/CalendarNav', { patientId: PatientId, episodeId: EpisodeId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleTop').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere was an er" +
                    "ror loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further ass" +
                    "istance.%3C/div%3E"));
            }
            Schedule.positionBottom();
        });
    },
    loadMasterCalendar: function(_patientId, _EpisodeId) {
        Acore.Open("masterCalendarMain", 'Schedule/MasterCalendarMain', function() {
            $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                $(this).css({
                    position: 'relative',
                    left: -100,
                    top: 0
                });
            });
        }, { patientId: _patientId, episodeId: _EpisodeId });
    },
    loadMasterCalendarNavigation: function(EpisodeId, PatientId) {
        $('#window_masterCalendarMain_content').load('/Schedule/MasterCalendar', { patientId: PatientId, episodeId: EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere " +
                    "was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fu" +
                    "rther assistance.%3C/div%3E"));
            }

        });
    },
    loadCalendar: function(patientId, discipline) {
        $('#scheduleTop').load('Schedule/Calendar', { patientId: patientId, discipline: discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#ScheduleMainResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere wa" +
                    "s an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for furt" +
                    "her assistance.%3C/div%3E"));
            }
            else if (textStatus == "success") {
                Schedule.loadActivity(patientId);
            }
        });
    },
    loadActivity: function(patientId) {
        $('#scheduleBottomPanel').load('Schedule/ActivityFirstTime', { patientId: patientId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere w" +
                    "as an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fur" +
                    "ther assistance.%3C/div%3E"));
            }
        });
    },
    loadFirstActivity: function(PatientId) {
        $('#scheduleBottomPanel').load('Schedule/ActivityFirstTime', { patientId: PatientId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere w" +
                    "as an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fur" +
                    "ther assistance.%3C/div%3E"));
            }
        });
    },
    InitEpisode: function(action) {
        $("#Edit_Episode_StartDate").blur(function() { setTimeout(Schedule.editEpisodeStartDateOnChange, 200) });
        $("#editEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Schedule.ReLoadInactiveEpisodes($("#editEpisodeForm").find("#Edit_Episode_PatientId").val());
                            if (action != null && action != undefined && typeof (action) == "function") {
                                action();
                            }
                            UserInterface.CloseModal();
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    gatherMultiSchedulerDates: function() {
        var visitDates = '';
        $('.multiDayScheduleTable td.selectdate').each(function() {
            visitDates += $(this).attr("date") + ",";
        });
        $("#multiDayScheduleVisitDates").val(visitDates);
    },
    InitMultiDayScheduler: function() {
        $("#multiDayScheduleForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            var patientId = $("#SchedulePatientID").val();
                            var episodeId = $("#ScheduleEpisodeID").val()
                            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                            Schedule.loadCalendarNavigation(episodeId, patientId);
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewEpisode: function() {
        $("#New_Episode_StartDate").blur(function() { setTimeout(Schedule.newEpisodeStartDateOnChange, 200) });
        $('#New_Episode_PatientId').change(function() {
            $("#New_Episode_PrimaryPhysician").PhyscianInput();
            var patientId = $(this).val();
            U.PostUrl("/Patient/GetPatientForEpisode", "patientId=" + patientId, function(data) {
                if (data != null) {
                    $("#newEpisodeTargetDateDiv").show();
                    $("#newEpisodeTargetDate").html(data.StartOfCareDateFormatted);
                    $("#newEpisodeTip").html("<label class=\"bold\">Tip:</label><em> Last Episode end date is: " + data.EndDateFormatted + "</em>");
                } else {
                    $("#newEpisodeTip").html("");
                    $("#newEpisodeTargetDate").html("");
                    $("#newEpisodeTargetDateDiv").hide();
                }
            });
        });
        $("#newEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow("newepisode");
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitTopMenuNewEpisode: function() {
        $('#TopMenuNew_Episode_PatientId').change(function() {
            var patientId = $(this).val();
            $('#topMenuNewEpisodeContent').Load('Schedule/NewPatientEpisodeContent', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
                if (textStatus == 'error') {
                    $('#topMenuNewEpisodeContent').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere w" +
                    "as an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fur" +
                    "ther assistance.%3C/div%3E"));
                }
            });
        });
        $("#topMenuNewEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow("newepisode");
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetEpisode: function(episodeId, patientId) { Acore.Open("editepisode", 'Schedule/EditEpisode', function() { Schedule.InitEpisode(); }, { episodeId: episodeId, patientId: patientId }); },
    InitTaskDetails: function() {
        $("#Schedule_DetailForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Task details updated successfully.", "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow('scheduledetails');
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Task details updated successfully.", "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow('scheduledetails');
                        }
                        else { U.Growl($.trim(result.responseText), "error"); }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetTaskDetails: function(episodeId, patientId, eventId) {
        Acore.Open("scheduledetails", 'Schedule/EditDetails', function() {
            Schedule.InitTaskDetails();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    GetTaskAttachments: function(episodeId, patientId, eventId) {
        Acore.Modal({ Url: 'Schedule/Attachments', Input: { episodeId: episodeId, patientId: patientId, eventId: eventId }, Width: 500, Height: 225, WindowFrame: false });
    },
    EventMouseOver: function(control) {
        var currentControl = $('.events', $(control));
        currentControl.show();
    },
    EventMouseOut: function(control) {
        $('.events', $(control)).hide();
    },
    positionBottom: function() {
        $('#window_schedulecenter .layout-main .bottom').css({ top: $('#window_schedulecenter .layout-main .top').height() + "px" });
    },
    loadHHASVisit: function(episodeId, patientId, eventId) {
        Acore.Open("hhasVisit", 'Schedule/HHASVisit', function() {
            Schedule.hhaInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadLVNSVisit: function(episodeId, patientId, eventId) {
        Acore.Open("lvnsVisit", 'Schedule/LVNSVisit', function() {
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadDischargeSummary: function(episodeId, patientId, eventId) {
        Acore.Open("dischargeSummary", 'Schedule/DischargeSummary', function() {
            Schedule.dischargeSummaryInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadHHACarePlan: function(episodeId, patientId, eventId) {
        Acore.Open("hhaCarePlan", 'Schedule/HHACarePlan', function() {
            Schedule.hhaCarePlanInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSixtyDaySummary: function(episodeId, patientId, eventId) {
        Acore.Open("sixtyDaySummary", 'Schedule/SixtyDaySummary', function() {
            Schedule.sixtyDaySummaryInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadTransferSummary: function(episodeId, patientId, eventId, type) {
        Acore.Open("transferSummary", 'Schedule/TransferSummary', function() {
            Schedule.transferSummaryInit(type, "transferSummary");
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadCoordinationOfCare: function(episodeId, patientId, eventId, type) {
        Acore.Open("coordinationofcare", 'Schedule/TransferSummary', function() {
            Schedule.transferSummaryInit(type, "coordinationofcare");
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSnVisit: function(episodeId, patientId, eventId) {
        Acore.Open("snVisit", 'Schedule/SNVisit', function() { Schedule.snVisitInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadWoundCare: function(episodeId, patientId, eventId) {
        Acore.Open("woundcare", 'Schedule/WoundCare', function() { Schedule.WoundCareInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadNoteSupplyWorkSheet: function(episodeId, patientId, eventId) {
        Acore.Open("notessupplyworksheet", 'Schedule/SupplyWorksheet', function() { }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadPTVisit: function(episodeId, patientId, eventId, type) {
        Acore.Open("PTVisit", 'Schedule/PTVisit', function() { Schedule.ptVisitsInit(type, "ptVisit"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadPTAVisit: function(episodeId, patientId, eventId, type) {
        Acore.Open("PTAVisit", 'Schedule/PTVisit', function() { Schedule.ptVisitsInit(type, "ptaVisit"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSTEvaluation: function(episodeId, patientId, eventId, type) {
        Acore.Open("stEvaluation", 'Schedule/STEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "stEvaluation"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSTReEvaluation: function(episodeId, patientId, eventId, type) {
        Acore.Open("stReEvaluation", 'Schedule/STEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "stReEvaluation"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSTMaintenance: function(episodeId, patientId, eventId, type) {
        Acore.Open("stMaintenance", 'Schedule/STEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "stMaintenance"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSTDischarge: function(episodeId, patientId, eventId, type) {
        Acore.Open("stDischarge", 'Schedule/STEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "stDischarge"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSTVisit: function(episodeId, patientId, eventId, type) {
        Acore.Open("stVisit", 'Schedule/STVisit', function() { Schedule.stVisitInit(type, "stVisit"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadMSWEvaluation: function(episodeId, patientId, eventId, type) {
        Acore.Open("mswEvaluation", 'Schedule/MSWEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "mswEvaluation"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadMSWAssessment: function(episodeId, patientId, eventId, type) {
        Acore.Open("mswAssessment", 'Schedule/MSWEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "mswAssessment"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadMSWProgressNote: function(episodeId, patientId, eventId, type) {
        Acore.Open("mswProgressNote", 'Schedule/MSWProgressNote', function() { Schedule.stEvaluationAndDischargeInit(type, "mswProgressNote"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadMSWDischarge: function(episodeId, patientId, eventId, type) {
        Acore.Open("mswDischarge", 'Schedule/MSWEvaluation', function() { Schedule.stEvaluationAndDischargeInit(type, "mswDischarge"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadMSWVisit: function(episodeId, patientId, eventId, type) {
        Acore.Open("mswVisit", 'Schedule/MSWVisit', function() { Schedule.stEvaluationAndDischargeInit(type, "mswVisit"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadTransportationNote: function(episodeId, patientId, eventId) { Acore.Open("transportationnote", 'Schedule/TransportationNote', function() { Schedule.transportationNoteInit("TransportationNote", "transportationnote"); }, { episodeId: episodeId, patientId: patientId, eventId: eventId }); },
    transportationNoteInit: function(type, page) {
        $("#" + type + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            if ($("#" + type + "_Button").val() == "Complete") {
                                UserInterface.CloseAndRefresh(page);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    dischargeSummaryInit: function() {
        $("#DischargeSummary_PhysicianId").PhysicianInput();
        $("#DischargeSummary_NotificationDate").change(function() {
            if ($("#DischargeSummary_NotificationDate").val() == "3") {
                $("#DischargeSummary_NotificationDateOther").show();
            }
            else {
                $("#DischargeSummary_NotificationDateOther").hide();
            }
        });
        if ($("#DischargeSummary_NotificationDate").val() == "3") {
            $("#DischargeSummary_NotificationDateOther").show();
        }
        else {
            $("#DischargeSummary_NotificationDateOther").hide();
        }
        $("#DischargeSummary_ReasonForDC").change(function() {
            if ($("#DischargeSummary_ReasonForDC").val() == "9") {
                $("#DischargeSummary_ReasonForDCOther").show();
            }
            else {
                $("#DischargeSummary_ReasonForDCOther").hide();
            }
        });
        if ($("#DischargeSummary_ReasonForDC").val() == "9") {
            $("#DischargeSummary_ReasonForDCOther").show();
        }
        else {
            $("#DischargeSummary_ReasonForDCOther").hide();
        }
    },
    ptDischargeSummaryInit: function() {
        $("#PTDischargeSummary_PhysicianId").PhysicianInput();
        $("#PTDischargeSummary_NotificationDate").change(function() {
            if ($("#PTDischargeSummary_NotificationDate").val() == "3") {
                $("#PTDischargeSummary_NotificationDateOther").show();
            }
            else {
                $("#PTDischargeSummary_NotificationDateOther").hide();
            }
        });
        if ($("#PTDischargeSummary_NotificationDate").val() == "3") {
            $("#PTDischargeSummary_NotificationDateOther").show();
        }
        else {
            $("#PTDischargeSummary_NotificationDateOther").hide();
        }
        $("#PTDischargeSummary_ReasonForDC").change(function() {
            if ($("#PTDischargeSummary_ReasonForDC").val() == "9") {
                $("#PTDischargeSummary_ReasonForDCOther").show();
            }
            else {
                $("#PTDischargeSummary_ReasonForDCOther").hide();
            }
        });
        if ($("#PTDischargeSummary_ReasonForDC").val() == "9") {
            $("#PTDischargeSummary_ReasonForDCOther").show();
        }
        else {
            $("#PTDischargeSummary_ReasonForDCOther").hide();
        }
    },
    otDischargeSummaryInit: function() {
        $("#OTDischargeSummary_PhysicianId").PhysicianInput();
        $("#OTDischargeSummary_NotificationDate").change(function() {
            if ($("#OTDischargeSummary_NotificationDate").val() == "3") {
                $("#OTDischargeSummary_NotificationDateOther").show();
            }
            else {
                $("#OTDischargeSummary_NotificationDateOther").hide();
            }
        });
        if ($("#OTDischargeSummary_NotificationDate").val() == "3") {
            $("#OTDischargeSummary_NotificationDateOther").show();
        }
        else {
            $("#OTDischargeSummary_NotificationDateOther").hide();
        }
        $("#OTDischargeSummary_ReasonForDC").change(function() {
            if ($("#OTDischargeSummary_ReasonForDC").val() == "9") {
                $("#OTDischargeSummary_ReasonForDCOther").show();
            }
            else {
                $("#OTDischargeSummary_ReasonForDCOther").hide();
            }
        });
        if ($("#OTDischargeSummary_ReasonForDC").val() == "9") {
            $("#OTDischargeSummary_ReasonForDCOther").show();
        }
        else {
            $("#OTDischargeSummary_ReasonForDCOther").hide();
        }
    },
    sixtyDaySummaryInit: function() {
        $("#SixtyDaySummary_Physician").PhysicianInput();
        $("#SixtyDaySummary_HomeboundStatus").change(function() {
            if ($("#SixtyDaySummary_HomeboundStatus").val() == "8") {
                $("#SixtyDaySummary_HomeboundStatusOther").show();
            }
            else {
                $("#SixtyDaySummary_HomeboundStatusOther").hide();
            }
        });
        if ($("#SixtyDaySummary_HomeboundStatus").val() == "8") {
            $("#SixtyDaySummary_HomeboundStatusOther").show();
        }
        else {
            $("#SixtyDaySummary_HomeboundStatusOther").hide();
        }
    },
    transferSummaryInit: function(type, page) {
        $("#" + type + "_HomeboundStatus").change(function() {
            if ($("#" + type + "_HomeboundStatus").val() == "8") {
                $("#" + type + "_HomeboundStatusOther").show();
            }
            else {
                $("#" + type + "_HomeboundStatusOther").hide();
            }
        });
        if ($("#" + type + "_HomeboundStatus").val() == "8") {
            $("#" + type + "_HomeboundStatusOther").show();
        }
        else {
            $("#" + type + "_HomeboundStatusOther").hide();
        }

        $("#" + type + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            if ($("#" + type + "_Button").val() == "Complete") {
                                UserInterface.CloseAndRefresh(page);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

    },
    ProcessNote: function(button, episodeId, patientId, eventId) {
        U.PostUrl("Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                UserInterface.CloseModal();
                Agency.RebindCaseManagement();
                Patient.LoadInfoAndActivity(patientId);
                Patient.Rebind();
                Schedule.Rebind();
                User.RebindScheduleList();
            }
        })
    },
    ptVisitsInit: function(type, page) {
        $("#" + type + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            if ($("#" + type + "_Button").val() == "Complete") {
                                UserInterface.CloseAndRefresh(page);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    stEvaluationAndDischargeInit: function(type, page) {
        $(".Physicians").PhysicianInput();
        $("#" + type + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            if ($("#" + type + "_Button").val() == "Complete") {
                                UserInterface.CloseAndRefresh(page);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    stVisitInit: function(type, page) {
        $("#STVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            if ($("#" + type + "_Button").val() == "Complete") {
                                UserInterface.CloseAndRefresh(page);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    ToolTip: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    ActivityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
            if (dataItem.StatusName == "Missed Visit(complete)") {
                $(e.row).removeClass('darkgreen');
                $(e.row).addClass('darkred');
            }
        }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append($("<li/>", { "text": "Reopen Task" }).click(function() {
                $(e.row).find("a:contains('Reopen Task')").click();
            }));
            else if (!dataItem.IsOrphaned) Menu.append($("<li/>", { "text": "Edit Note" }).click(function() {
                $(e.row).find("a:first").click();
            }));
            Menu.append($("<li/>", { "text": "Details" }).click(function() {
                $(e.row).find("a:contains('Details')").click();
            })).append($("<li/>", { "text": "Delete" }).click(function() {
                $(e.row).find("a:contains('Delete')").click();
            })).append($("<li/>", { "text": "Print" }).click(function() {
                $(e.row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        });
    },
    WoundCareInit: function() {
        $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
        $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#WoundCare_EpisodeId").val() + "&patientId=" + $("#WoundCare_PatientId").val() + "&eventId=" + $("#WoundCare_EventId").val() + "&name=" + name + "&assetId=" + assetId;
            U.PostUrl("/Schedule/DeleteWoundCareAsset", input, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"float-left uploadWidth\" />");
                }
            });
        }
    },
    DeleteScheduleEventAsset: function(control, patientId, episodeId, eventId, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            U.PostUrl("/Schedule/DeleteScheduleEventAsset", { patientId: patientId, episodeId: episodeId, eventId: eventId, assetId: assetId }, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('span').remove();
                    $("#scheduleEvent_Assest_Count").html($("#scheduleEvent_Assest_Count").html() - 1);
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                }
            });
        }
    },
    Rebind: function() {
        Schedule._isRebind = true;
        Schedule.SetDiscipline("all");
        Schedule.SetDisciplineIndex(0);
        U.RebindTGrid($('#ScheduleSelectionGrid'));
        U.RebindTGrid($('#caseManagementGrid'));
    },
    AddSupply: function(control, episodeId, patientId, eventId, type, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&supplyId=" + supplyId + "&quantity=" + quantity + "&date=" + date;
        U.PostUrl("/Schedule/AddNoteSupply", input, function(result) {
            var gridfilter = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + type + "_GenericSupplyDescription").val('');
                $("#" + type + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + type + "_SupplyGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId });
            }
        });
    },
    ShowScheduler: function() {
        if ($('#schedule-tab-strip').is(':visible')) {
            $('#schedule-collapsed').find(".show-scheduler").html("Show Scheduler");
            $('#schedule-tab-strip').hide();
            Schedule.showAll();
        } else {
            if ($("#Nursing_Tab").hasClass("t-state-active")) {
                Schedule.SetDiscipline('Nursing');
                var patientId = $("#SchedulePatientID").val();
                var episodeId = $("#ScheduleEpisodeID").val()
                Schedule.loadCalendarNavigation(episodeId, patientId);
                var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
            } else ($("#Nursing_Tab").click());
            if ($(".schedule-tables.purgable tbody tr").length == 0) Schedule.AllTableAdd();
            else {
                $('#schedule-tab-strip').show();
                $('#schedule-collapsed').find(".show-scheduler").html("Hide Scheduler");
            }
        }
        Schedule.positionBottom();
    },
    newEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("New")
    },
    editEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("Edit")
    },
    EpisodeStartDateChange: function(prefix) {
        var startDate = $("#" + prefix + "_Episode_StartDate"), endDate = $("#" + prefix + "_Episode_EndDate");
        if (startDate.val()) {
            var newStartDate = new Date(startDate.val());
            var newEndDate = new Date(startDate.val());
            newEndDate.setDate(newStartDate.getDate() + 59);
            var month = newEndDate.getMonth() + 1, day = newEndDate.getDate(), year = newEndDate.getFullYear();
            endDate.val(month + "/" + day + "/" + year);
        }
    },
    showAll: function() {
        Schedule.SetDiscipline('all');
        var patientId = $("#SchedulePatientID").val();
        var episodeId = $("#ScheduleEpisodeID").val()
        Schedule.loadCalendarNavigation(episodeId, patientId);
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
    },
    PatientListDataBound: function() {
        Schedule.Filter($("#txtSearch_Schedule_Selection").val());
        if ($("#ScheduleSelectionGrid .t-grid-content tr").length) {
            if (Schedule._patientId.length) $("td:contains(" + Schedule._patientId + ")", $("#ScheduleSelectionGrid")).closest("tr").click();
            else $("#ScheduleSelectionGrid .t-grid-content tr" + ($("#txtSearch_Schedule_Selection").val().length ? ".match" : "")).eq(0).click();
        } else $("#ScheduleMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#ScheduleSelectionGrid .t-state-selected").length) $("#ScheduleSelectionGrid .t-grid-content").scrollTop($("#ScheduleSelectionGrid .t-state-selected").position().top - 50);
    },
    PhlebotomyLab: function(selector) {
        $(selector).Autocomplete({ source: ["CBC", "Chem 7", "BMP", "PT/INR", "PT", "Chem 8", "K+", "Urinalysis", "TSH", "CBC W/ diff", "Hemoglobin A1c", "Lipid Panel", "Comp Metabolic Panel  (CMP 14)", "TSH", "ALT/SGOT", "ALT/SGBOT", "Iron (Fe)"] })
    },
    reassignScheduleInit: function(identifier) {
        $("#reassign" + identifier + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Schedule.Rebind();
                            Patient.Rebind();
                            User.RebindScheduleList();
                            if (identifier == "All") { UserInterface.CloseWindow('schedulereassign'); } else { UserInterface.CloseModal(); }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    loadEpisodeDropDown: function(dropDown, control) {
        U.PostUrl("/Schedule/EpisodeRangeList", { patientId: $(control).val() }, function(data) {
            var s = $("select#" + dropDown);
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Episode --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Range, itemData.Id, false, false);
            });
        });
    },
    BulkUpdate: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.PostUrl("Schedule/BulkUpdate", fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.LoadCaseManagement($("#CaseManagement_GroupName").val());
                Agency.RebindPrintQueue();

            } else U.Growl(data.errorMessage, "error");
        });
    },
    LoadLog: function(patientId, eventId, task) {
        Acore.Open("schdeuleeventlogs", 'Schedule/ScheduleLogs', function() { }, { patientId: patientId, eventId: eventId, task: task });
    },
    GetPlanofCareUrl: function(episodeId, patientId, eventId) {
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
        U.PostUrl("/Oasis/GetPlanofCareUrl", input, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { 'episodeId': result.episodeId, 'patientId': result.patientId, 'eventId': result.eventId });
            else { U.Growl(result.errorMessage, "error"); }
        });
    },
    GetHHACarePlanUrl: function(episodeId, patientId, eventId) {
        U.GetAttachment("/Schedule/HHACarePlanPdf", { 'episodeId': episodeId, 'patientId': patientId, 'eventId': eventId });
    },
    GetHHACarePlanUrlFromHHAVisit: function(episodeId, patientId, eventId) {
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
        U.PostUrl("/Schedule/HHACarePlanPdfFromHHAVisit", input, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { 'episodeId': result.episodeId, 'patientId': result.patientId, 'eventId': result.eventId });
            else { U.Growl(result.errorMessage, "error"); }
        });
    },
    loadInactiveEpisodes: function(patientId) { Acore.Open("inactiveepisode", 'Schedule/Inactive', function() { }, { patientId: patientId }); },
    ActivateEpisode: function(episodeId, patientId) {
        U.PostUrl('Schedule/ActivateEpisode', { 'episodeId': episodeId, 'patientId': patientId }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                User.RebindScheduleList();
                Schedule.ReLoadInactiveEpisodes(patientId);
            } else U.Growl(data.errorMessage, "error");
        });
    },
    ReLoadInactiveEpisodes: function(patientId) {
        $("#InactiveEpisodesContent ol").addClass('loading');
        $("#InactiveEpisodesContent").load('Schedule/InactiveGrid', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#InactiveEpisodesContent ol").removeClass("loading");
            if (textStatus == 'error') $('#InactiveEpisodesContent').html(U.AjaxError);
        });
    },
    LoadEpisodeLog: function(episodeId, patientId) { Acore.Open("episodelogs", 'Schedule/EpisodeLogs', function() { }, { episodeId: episodeId, patientId: patientId }); },
    ConvertToTime: function(time) {
        if (time != undefined) {
            var hours = +time.substring(0, 2);
            var minutes = +time.substring(3, 5);
            var pm = time.indexOf("P") != -1;
            if (pm && hours !== 12) {
                hours += 12;
            }
            var date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);
            return date;
        }
        else
            return new Date();
    },
    CheckTimeInOut: function(prefix, showMessage) {
        var tempTimeIn = $("#" + prefix + "_TimeIn").val();
        var tempTimeOut = $("#" + prefix + "_TimeOut").val();
        var timeIn = Schedule.ConvertToTime(tempTimeIn);
        var timeOut = Schedule.ConvertToTime(tempTimeOut);
        var timeOutNextDay = Schedule.ConvertToTime(tempTimeOut);
        timeOutNextDay.setDate(timeOutNextDay.getDate() + 1);
        if (timeIn && timeOut) {
            var timeDiff = (timeOut - timeIn) / 60 / 60 / 1000;
            var timeDiffNextDay = (timeOutNextDay - timeIn) / 60 / 60 / 1000;
            var over3Hours = timeDiff > 3 || timeDiff < 0;
            var over3HoursNextDay = timeDiffNextDay > 3 || timeDiffNextDay < 0;
            if (over3Hours && !over3HoursNextDay)
                over3Hours = false;
            var defaultClick = "$('#" + prefix + "Form').focus();";
            if (over3Hours == true) {
                if (showMessage == true) {
                    var growl = $(".jGrowl-notification").text();
                    if (growl == "") {
                        U.Growl("WARNING: The Time In and Out are over 3 hours apart.", "warning");
                    }
                }
                else {
                    var answer = confirm('The time out is 3 hours greater than the time in. Are you sure you want to continue?');
                    return !answer;
                }
            } else {
                return false;
            }
        }
    },
    WarnTimeInOut: function(prefix, type) {
        Schedule.CheckTimeInOut(prefix, true);
        if (type === "Details") {
            var saveBtn = $("form#" + prefix + "Form a:contains('Save')");
            var saveBtnClick = saveBtn.attr('onClick');
            saveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + saveBtnClick + "}else{return false}");
        }
        else if (type === "Oasis") {
            var finishBtn = $("div#print-controls a:contains('Finish')");
            var finishBtnClick = finishBtn.attr('onClick');
            finishBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + finishBtnClick + "}else{return false}");
        }
        else {
            var saveBtn = $("form#" + prefix + "Form a:contains('Save')");
            var completeBtn = $("form#" + prefix + "Form a:contains('Complete')");
            var approveBtn = $("form#" + prefix + "Form a:contains('Approve')");
            var returnBtn = $("form#" + prefix + "Form a:contains('Return')");
            var saveBtnClick = saveBtn.attr('onClick');
            var completeBtnClick = completeBtn.attr('onClick');
            var approveBtnClick = approveBtn.attr('onClick');
            var returnBtnClick = returnBtn.attr('onClick');
            saveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + saveBtnClick + "}");
            completeBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + completeBtnClick + "}");
            approveBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + approveBtnClick + "}");
            returnBtn.attr('onClick', "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + returnBtnClick + "}");
        }
    },
    loadDiagnosisData: function(patientId, episodeId) {
        U.PostUrl("Schedule/GetDiagnosisData", { patientId: patientId, episodeId: episodeId }, function(data) {
            $("#PrimaryDiagnosis").text(data.diag1);
            $("#SecondaryDiagnosis").text(data.diag2);
            if (data.code1 === '') {
                $("#ICD9M").remove();
            } else {
                $("#PrimaryDiagnosis").append("<a id='ICD9M' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code1 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
            }
            if (data.code2 === '') {
                $("#ICD9M1").remove();
            } else {
                $("#SecondaryDiagnosis").append("<a id='ICD9M1' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code2 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
            }
        });
    },
    WarnEpisodeGap: function(prefix) {
        $("#" + prefix + "_StartDate").bind("ValueSet", function(event) {
            Schedule.CheckEpisodeStartDate(prefix, true)
        });
        var formPrefix = "";
        if (prefix == "Edit_Episode")
            formPrefix = "editEpisode";
        else if (prefix == "New_Episode")
            formPrefix = "newEpisode";
        else if (prefix == "TopMenuNew_Episode")
            formPrefix = "topMenuNewEpisode";
        $("#" + formPrefix + "Form a:contains('Save')").unbind('click');
        $("#" + formPrefix + "Form a:contains('Save')").click(function() {
            if (Schedule.CheckEpisodeStartDate(prefix, false)) {
                var answer = confirm("There is a gap between the start date and the last episode's end date. Are you sure you want to save?");
                if (answer) $(this).closest('form').submit()
            }
            else {
                $(this).closest('form').submit()
            }
        });
    },
    CheckEpisodeStartDate: function(prefix, showGrowl) {
        var d1 = $("#" + prefix + "_StartDate").val();
        var d2 = $("#" + prefix + "_PreviousEndDate").val();
        if (d1 != "" && d2 != "") {
            var startDate = $.datepicker.parseDate("mm/dd/yy", d1);
            var previousEndDate = $.datepicker.parseDate("mm/dd/yy", d2);
            previousEndDate.setDate(previousEndDate.getDate() + 1)
            if (startDate.getTime() != previousEndDate.getTime()) {
                if (showGrowl) U.Growl("WARNING: There is a gap between the start date and the last episode's end date.", "warning");
                return true;
            }
        }
        return false;
    },
    RestoreMissedVisit: function(episodeId, patientId, eventId) {
        var answer = confirm("Are you sure you want to restore this missed visit?");
        if (answer) {
            U.PostUrl("Schedule/MissedVisitRestore", {
                episodeId: episodeId,
                patientId: patientId,
                eventId: eventId
            }, function(data) {
                if (data.isSuccessful) {
                    Agency.RebindMissedVisitList();
                    Agency.RebindOrders();
                    Patient.Rebind();
                    Schedule.Rebind();
                } else U.Growl(data.errorMessage, 'error');
            })
        }
    },
    SelectPatient: function(patientId, status) {
        var selectionFilter = $("#txtSearch_Schedule_Selection");
        if (selectionFilter.val() != "") {
            $("#txtSearch_Schedule_Selection").val("");
            $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
        }

        var statusNumber = !isNaN(status) ? status : (status.toLowerCase() == "false" ? "1" : "2");
        var statusDropDown = $("select.ScheduleStatusDropDown");
        if (statusNumber != statusDropDown.val()) {
            statusDropDown.val(statusNumber);
            Schedule.SetId(patientId);
            U.RebindTGrid($('#ScheduleSelectionGrid'), { branchId: $("select.ScheduleBranchCode").val(), statusId: $("select.ScheduleStatusDropDown").val(), paymentSourceId: $("select.SchedulePaymentDropDown").val() });
        }
        else {
            var row = $("#ScheduleSelectionGrid .t-grid-content tr td.t-last:contains('" + patientId + "')").parent();
            $("#ScheduleSelectionGrid tr.t-state-selected").removeClass("t-state-selected");
            row.addClass("t-state-selected");
            var scroll = $(row).position().top + $(row).closest(".t-grid-content").scrollTop() - 24;
            $(row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            Schedule.loadCalendarAndActivities(patientId, null);
        }
    },
    DeleteAction: function() {
            var data = $("#DeleteMultipleActivityGrid .t-grid-content tr input:checked").serializeArray();
            if (!data.length) {
                U.Growl("You must select at least one task to delete.", 'error');
            }
            else {
                if (confirm("Are you sure you want to delete these event(s)?")) {
                    data.push({ name: "PatientId", value: Schedule.GetId() });
                    data.push({ name: "EpisodeId", value: Schedule.GetEpisodeId() });
                    U.PostUrl("Schedule/DeleteMultiple", data,
                    function(data) {
                        if (data.isSuccessful) {
                            U.Growl(data.errorMessage, 'success');
                            Agency.RebindMissedVisitList();
                            Agency.RebindOrders();
                            Patient.Rebind();
                            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                            if (scheduleActivityGrid != null) {
                                scheduleActivityGrid.rebind({ episodeId: Schedule.GetEpisodeId(), patientId: Schedule.GetId(), discipline: Schedule._Discipline });
                            }
                            Schedule.loadCalendarNavigation(Schedule.GetEpisodeId(), Schedule.GetId());
                            UserInterface.CloseWindow('scheduledelete');
                        } else {
                            U.Growl(data.errorMessage, 'error');
                        }
                    }, null);
                } 
            }
        
    },
    DeleteActivityRowSelected: function(e) {
       var checkbox = $(e.row).find("input");
       checkbox.prop("checked", !checkbox.prop("checked"));
    }
}

