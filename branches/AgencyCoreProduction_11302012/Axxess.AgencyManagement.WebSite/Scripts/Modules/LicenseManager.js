﻿var LicenseManager = {
    Add: function() {
        Acore.Modal({
            "Name": "Add New Non-Software User License",
            "Url": "User/NewLicenseItem",
            "Input": {},
            "OnLoad": function() { LicenseManager.InitNew() },
            "Width": "650px",
            "Height": "350px",
            "WindowFrame": false
        })
    },
    Delete: function(licenseId, userId) {
        if (confirm("Are you sure you want to delete this license?")) {
            U.PostUrl('User/DeleteLicense', { Id: licenseId, userId: userId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    LicenseManager.Refresh();
                } else U.Growl(result.errorMessage, "error");
            })
        }
    },
    Edit: function(licenseId, userId) {
        Acore.Modal({
            "Name": "Edit License",
            "Url": "User/EditLicenseItem",
            "Input": { licenseId: licenseId, userId: userId },
            "OnLoad": function() { LicenseManager.InitEdit() },
            "Width": "650px",
            "Height": "300px",
            "WindowFrame": false
        })
    },
    InitNew: function() {
        $("#newLicenseItemForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            LicenseManager.Refresh();
                            $("#window_ModalWindow").Close();
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            LicenseManager.Refresh();
                            $("#window_ModalWindow").Close();
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEdit: function() {
        $("#editLicenseItemForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            LicenseManager.Refresh();
                            U.Growl(result.errorMessage, "success");
                            $("#window_ModalWindow").Close();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Refresh: function() {
        $("#LicenseManager_List ol").addClass("loading");
        $("#LicenseManager_List").load("User/LicenseManagerList", {},
            function(responseText, textStatus, XMLHttpRequest) {
                $('#LicenseManager_List ol').removeClass("loading");
                if (textStatus == 'error') $('#LicenseManager_List').html(U.AjaxError);
            }
        );
    }
}