﻿var ChangePassword = {
    Init: function() {
        $("#changePasswordForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.Block(); },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                        U.UnBlock();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Logon = {
    InitMobile: function() {
        this.Validate(true);
        if (document.referrer.split('/')[2] != document.domain) {
            $("#top_wrapper").slideDown(500, function() {
                $(".logo", "#top_wrapper").animate({ width: "toggle" }, 500);
                $(".mobile", "#top_wrapper").animate({ width: "toggle" }, 500);
                $("#login_wrapper").fadeIn(500)
            })
        } else {
            $("#top_wrapper").show().find(".logo,.mobile").show();
            $("#login_wrapper").fadeIn(500)
        }
        $("#Login_Forgot a").click(function() {
            $("#login_wrapper").fadeOut(500, function() {
                $(location).attr("href", "/Forgot")
            })
        });
        $("#Login_Button").click(function(e) {
            if ($("#Login_UserName").val() && $("#Login_Password").val()) {
                e.preventDefault();
                $("#login_wrapper").fadeOut(500, function() { $("#login_wrapper").find("form").submit() })
            }
        });
    },
    Init: function() {
        $("#login-window").show();
        $("#browser-window").show();
        this.Validate();
    },
    Validate: function(Mobile) {
        $("#loginForm").validate({
            messages: { UserName: "", Password: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { U.Block() },
                    success: function(result) {
                        if (result.isSuccessful)
                        // Multiple Agencies
                            if (result.hasMultiple) $("#Agency_Selection_Container").load("/Account/Agencies", { loginId: result.id }, function(responseText, textStatus, XMLHttpRequest) {
                                if (textStatus == "success") U.ShowDialog("#Agency_Selection_Container", function() { })
                            });
                        // Complete, Successful Login
                        else {
                            if (confirm('This system and all its components and contents (collectively, the "System") are intended for authorized business use only. All data within is considered confidential and proprietary. Unauthorized access, use, modification, destruction, disclosure or copy of this system is prohibited and will result in prosecution. Click ok to continue.')) {
                                $(location).attr("href", result.redirectUrl);
                            } else {
                                $(location).attr("href", "/logout");
                            }
                        }
                        else {
                            U.UnBlock();
                            // Alredy logged in elsewhere
                            if (result.isAccountInUse) U.ShowDialog("#Account_Modal_Container", function() {
                                $("#Login_Cancel_Button").click(function() {
                                    U.CloseDialog();
                                    if (Mobile) $("#login_wrapper").fadeIn(500);
                                });
                                $("#Login_Continue_Button").click(function() {
                                    U.PostUrl("/Account/Kick", "loginId=" + result.id + "&emailAddress=" + result.email, function(result) {
                                        if (result.isSuccessful) $(location).attr("href", result.redirectUrl);
                                        else {
                                            U.Growl(result.errorMessage, "error");
                                            if (Mobile) $("#login_wrapper").fadeIn(500);
                                        }
                                    })
                                })
                            })
                            // Failed login
                            else {
                                if (Mobile) {
                                    if (result.isLocked) $("#login_wrapper").html($("<p/>").html(result.errorMessage));
                                    else U.Growl(result.errorMessage, "error");
                                    $("#login_wrapper").fadeIn(500)
                                } else {
                                    U.Growl(result.errorMessage, "error");
                                    if (result.isLocked) {
                                        $("#Login_UserName,#Login_Password").attr("disabled", "disabled");
                                        $("#Login_RememberMe,#Login_Forgot,#Login_Button").hide();
                                    }
                                }
                            }
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Select: function(agencyId, userId) {
        U.PostUrl("/Account/Agency", "agencyId=" + agencyId + "&userId=" + userId, function(result) {
            if (result.isSuccessful) {
                if (confirm('This system and all its components and contents (collectively, the "System") are intended for authorized business use only. All data within is considered confidential and proprietary. Unauthorized access, use, modification, destruction, disclosure or copy of this system is prohibited and will result in prosecution. Click ok to continue.')) {
                    U.Growl("Login successful", "success");
                    window.location.replace(result.redirectUrl);
                } else {
                    $(location).attr("href", "/logout");
                }
            } else {
                U.UnBlock();
                if (result.isAccountInUse) {
                    U.ShowDialog("#Account_Modal_Container", function() {
                        $("#Login_Cancel_Button").click(function() {
                            U.CloseDialog();
                        });
                        $("#Login_Continue_Button").click(function() {
                            U.PostUrl("/Account/Kick", "loginId=" + result.id + "&emailAddress=" + result.email, function(result) {
                                if (result.isSuccessful) window.location.replace(result.redirectUrl);
                                else U.Growl(result.errorMessage, "error");
                            });
                        });
                    });
                } else {
                    U.Growl(result.errorMessage, "error");
                    if (result.isLocked) {
                        $("#Login_UserName,#Login_Password").attr("disabled", "disabled");
                        $("#Login_RememberMe,#Login_Forgot,#Login_Button").hide();
                    }
                }
            }
        });
    }
}
var ResetPassword = {
    InitMobile: function() {
        this.Init("Mobile");
        $("input[type=button]", "#login_wrapper").click(function() {
            $("#login_wrapper").fadeOut(500, function() {
                $(location).attr("href", "/Login")
            })
        });
        $("#login_wrapper").fadeIn(500)
    },
    Init: function(Mobile) {
        $("#forgotPasswordForm").validate({
            messages: { UserName: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        if (Mobile) $("#login_wrapper").fadeOut(500, function() { U.Block() });
                        else U.Block();
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $("#forgotPasswordFormContainer").hide();
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        } else $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        U.UnBlock();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Activate = {
    Init: function() {
        $("#activateAccountForm").validate({
            rules: {
                Password: { required: true, minlength: 8 }
            },
            messages: {
                Password: { required: "* required", minlength: "8 characters minimum" }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.Block(); },
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                            U.UnBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Link = {
    Init: function() {
        $("#linkAccountForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.Block(); },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.UnBlock();
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                            $("#Agency_Selection_Container").load("/Account/Agencies", { loginId: $("#Link_User_LoginId").val() }, function(responseText, textStatus, XMLHttpRequest) {
                                if (textStatus == 'error') {
                                }
                                else if (textStatus == "success") {
                                    U.ShowDialog("#Agency_Selection_Container", function() { });
                                }
                            });
                        }
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                            U.UnBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}