﻿var Contact = {
    Delete: function(Id) { U.DeleteTemplate("Contact", Id); },
    InitEdit: function() {
        U.ShowIfOtherSelected($("#Edit_Contact_Type"), $("#Edit_Contact_OtherContactType"));
        U.PhoneAutoTab("Edit_Contact_PhonePrimary");
        U.PhoneAutoTab("Edit_Contact_PhoneAlternate");
        U.PhoneAutoTab("Edit_Contact_Fax");
        U.InitEditTemplate("Contact");
    },
    InitNew: function() {
        U.ShowIfOtherSelected($("#New_Contact_Type"), $("#New_Contact_OtherContactType"));
        U.PhoneAutoTab("New_Contact_PhonePrimary");
        U.PhoneAutoTab("New_Contact_PhoneAlternate");
        U.PhoneAutoTab("New_Contact_Fax");
        U.InitNewTemplate("Contact");
    },
    RebindList: function() { U.RebindTGrid($('#List_Contact')); },
    ListRowDataBound: function(e) {
        $("a.tooltip", e.row).each(function() {
            if ($(this).attr("tooltip")) $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), "") }).tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: -15,
                bodyHandler: function() {
                    return $(this).attr("tooltip");
                }
            });
            else $(this).remove();
        })
    }
}