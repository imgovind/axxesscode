var Oasis = {
    DeleteAsset: function(control, assessmentType, name, assetId) {
        U.Delete("Asset", "/Oasis/DeleteWoundCareAsset", {
            episodeId: $("#" + assessmentType + "_EpisodeId").val(),
            patientId: $("#" + assessmentType + "_PatientGuid").val(),
            eventId: $("#" + assessmentType + "_Id").val(),
            assessmentType: assessmentType,
            name: name,
            assetId: assetId
        }, function() {
            $(control).closest('td').empty().append(
                $("<input/>", {
                    "type": "file",
                    "name": assessmentType + "_" + name,
                    "value": "Upload",
                    "size": "1",
                    "class": "float-left uploadWidth"
                })
            )
        })
    },
    Load: function(Id, PatientId, AssessmentType) {
        Acore.Open(AssessmentType, "Oasis/" + AssessmentType, function() {
            Oasis.InitTabs(Id, PatientId, AssessmentType)
        }, {
            Id: Id,
            PatientId: PatientId,
            AssessmentType: AssessmentType
        })
    },
    InitTabs: function(Id, PatientId, AssessmentType) {
        U.ToolTip($(".vertical-tab-list a", "#" + AssessmentType + "_Tabs"), "calday");
        $("#" + AssessmentType + "_Tabs").addClass("ui-tabs-vertical ui-helper-clearfix").tabs().bind("tabsselect", {
            Id: Id,
            PatientId: PatientId,
            AssessmentType: AssessmentType
        }, function(event, ui) {
            Oasis.LoadParts(AssessmentType, event, ui)
        }).find("li").removeClass("ui-corner-top").addClass("ui-corner-left")
    },
    LoadParts: function(AssessmentType, event, ui) {
        var category = $(ui.tab).attr("href");
        $(".general .main", "#" + AssessmentType + "_Tabs").remove();
        $(category).addClass("loading").Load("Oasis/Category", {
            Id: event.data.Id,
            PatientId: event.data.PatientId,
            AssessmentType: event.data.AssessmentType,
            Category: category
        }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "error") $(category).html(U.AjaxError);
            else if (textStatus == "success") {
                $(category).removeClass("loading");
                Oasis.Init();
            }
        })
    },
    NonOasisSignature: function(id, patientId, episodeId, assessmentType) {
        Acore.Modal({
            Name: "Non-OASIS Signature",
            Url: "Oasis/NonOasisSignature",
            Input: {
                Id: id,
                PatientId: patientId,
                EpisodeId: episodeId,
                AssessmentType: assessmentType
            },
            OnLoad: function() { Oasis.NonOasisSignatureSubmit($("#NonOasisSignatureForm")) },
            Width: "800px",
            Height: "200px",
            WindowFrame: false
        })
    },
    FallAssessment: function(AssessmentType) {
        $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        $("#" + AssessmentType + "_FallAssessment .option input").change(function() {
            $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        })
    },
    NonOasisSignatureSubmit: function(form) {
        form.validate({
            submitHandler: function(form) {
                var completeBtn = $("#NonOasisSignatureForm a:contains('Complete')");
                completeBtn.attr("old-onclick", completeBtn.attr("onclick"));
                completeBtn.removeAttr("onclick");
                completeBtn.attr("style", "box-shadow: 0 0 0.5em #000000; border-color: #333333 #AAAAAA #AAAAAA #333333; color: #DDDDDD !important;padding: 0 26px 2px 24px; background: url('/Images/sprite.png') no-repeat scroll center -157px transparent;");
                completeBtn.text("Please Wait...");
                var options = {
                    dataTpe: "json",
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            var window = $("input[name=OasisValidationType]").val();
                            U.Growl(resultObject.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindCaseManagement();
                            UserInterface.CloseModal();
                            UserInterface.CloseWindow(window);
                        } else {
                            U.Growl(resultObject.errorMessage, "error");
                            completeBtn.attr("onclick", completeBtn.attr("old-onclick"));
                            completeBtn.removeAttr("old-onclick");
                            completeBtn.removeAttr("style");
                            completeBtn.text("Complete");
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Init: function() {
        $(".icd").each(function() {
            var icd9Code = $(this).val();
            if (icd9Code.length > 0) {
                $(this).parent().append(
                    $("<a/>", {
                        "class": "teachingguide",
                        "title": "Click Here for Teaching/Reference Materials",
                        "href": "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + icd9Code + "&informationRecipient.languageCode.c=en",
                        "target": "_blank",
                        "text": "Teaching Guide"
                    })
                )
            }
        });
        $(".numeric").numeric();
        $(".floatnum").floatnum();
        $(".tabs").addClass("ui-tabs-vertical ui-helper-clearfix").tabs().find("li").removeClass("ui-corner-top").addClass("ui-corner-left");
        $.ajaxSetup({ type: "POST" });
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis,.ICDM1024,.diagnosisM1024").IcdInput();
        Template.OnChangeInit();
    },
    stripDecimal: function(data) {
        var newData = "";
        for (i = 0; i < data.length; i++)
            if (i != data.indexOf(".") && data.charAt(i) != " ") newData += data.charAt(i);
        return newData;
    },
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "Patient/GetPhysicianContact",
            data: { "PhysicianContactId": id },
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text(
                    (resultObject.FirstName !== null ? resultObject.FirstName : "") + " " +
                    (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (
                    resultObject.PhoneWork.substring(0, 3) + "-" +
                    resultObject.PhoneWork.substring(3, 6) + "-" +
                    resultObject.PhoneWork.substring(6)) : "")
            }
        })
    },
    NextTab: function(id) {
        $(id).tabs().tabs("select", $(".vertical-tab-list", id).children("li").index($(".ui-tabs-selected", id)) + 1)
    },
    Refresh: function(id) {
        $(id).tabs().tabs("select", 0).find(id + " .general").each(function() {
            $(this).scrollTop(0)
        })
    },
    Delete: function(cont, id) {
        U.Delete("Assessment", "Oasis/Delete", { Id: id }, function() {
            cont.parents("tr:first").remove()
        })
    },
    supplyInputFix: function(assessmentType, tableName) {
        var tableTr = $("tbody tr", $(tableName)), len = tableTr.length, i = 0, jsonData = '{ "Supply": [';
        $(tableTr).each(function() {
            jsonData += '{' +
                '"suppliesDescription":"' + $(this).find(".suppliesDescription").val() + '",' +
                '"suppliesCode":"' + $(this).find('.suppliesCode').val() + '",' +
                '"supplyQuantity":"' + $(this).find('.supplyQuantity').val() + '"}';
            if (len > ++i) jsonData += ',';
        });
        jsonData += '] }';
        $("#" + assessmentType + "_GenericSupply").val(jsonData);
    },
    ClearRows: function(table) {
        $("tbody tr", table).remove()
    },
    DeleteRow: function(control) {
        if ($(control).closest("tbody").find("tr").length == 1) {
            $(".schedule-tables.purgable").each(function() { $(this).find("tbody").empty() });
            Schedule.ShowScheduler();
        } else $(control).closest("tr").remove();
        Schedule.positionBottom();
    },
    BradenScale: function(AssessmentType) {
        Oasis.BradenScaleCalc(AssessmentType);
        $("#" + AssessmentType + "_BradenScale .active input").prop("checked", true);
        $("#" + AssessmentType + "_BradenScale td").click(function() {
            $(this).closest("tr").find("td").removeClass("active").find("input").prop("checked", false);
            $(this).addClass("active").find("input").prop("checked", true).closest("tr").find("input[type=hidden]").val($(this).attr("value"));
            Oasis.BradenScaleCalc(AssessmentType);
        });
    },
    BradenScaleCalc: function(AssessmentType) {
        var sensory = $("#" + AssessmentType + "_485BradenScaleSensoryHidden").val();
        var moisture = $("#" + AssessmentType + "_485BradenScaleMoistureHidden").val();
        var activity = $("#" + AssessmentType + "_485BradenScaleActivityHidden").val();
        var mobility = $("#" + AssessmentType + "_485BradenScaleMobilityHidden").val();
        var nutrition = $("#" + AssessmentType + "_485BradenScaleNutritionHidden").val();
        var friction = $("#" + AssessmentType + "_485BradenScaleFrictionHidden").val();
        var total = (sensory.length ? parseInt(sensory) : 0) +
                    (moisture.length ? parseInt(moisture) : 0) +
                    (activity.length ? parseInt(activity) : 0) +
                    (mobility.length ? parseInt(mobility) : 0) +
                    (nutrition.length ? parseInt(nutrition) : 0) +
                    (friction.length ? parseInt(friction) : 0);
        $("#" + AssessmentType + "_BradenScale").find("input[type=text]").val(total);
        if (total == 0) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong");
        else if (total >= 19) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(0).addClass("strong");
        else if (total >= 15 && total <= 18) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(1).addClass("strong");
        else if (total >= 13 && total <= 14) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(2).addClass("strong");
        else if (total >= 10 && total <= 12) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(3).addClass("strong");
        else if (total <= 9) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(4).addClass("strong");
    },
    CalculateNutritionScore: function(assessmentType) {
        var score = 0;
        $("input[name=" + assessmentType + "_GenericNutritionalHealth][type=checkbox]:checked").each(function() {
            score += parseInt($(this).parent('.option').find('label:first').text());
        });
        if (score <= 25) {
            $("#" + assessmentType + "_GoodNutritionalStatus").addClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 55) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").addClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 100) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").addClass("strong");
        }
        $("#" + assessmentType + "_GenericGoodNutritionScore").val(score);
    },
    TinettiAssessment: function(assessmentType) {
        Oasis.CalculateTinettiScore(assessmentType);
        $("#" + assessmentType + "_Tinetti input").change(function() { Oasis.CalculateTinettiScore(assessmentType) });
    },
    CalculateTinettiScore: function(assessmentType) {
        var balance = 0, gait = 0, total = 0;
        $("#" + assessmentType + "_Tinetti :checked").each(function() {
            if ($(this).hasClass("TinettiBalance")) balance += parseInt($(this).val());
            if ($(this).hasClass("TinettiGait")) gait += parseInt($(this).val());
            total += parseInt($(this).val());
        });
        $("input[name=" + assessmentType + "_TinettiBalanceTotal]").val(String(balance));
        $("input[name=" + assessmentType + "_TinettiGaitTotal]").val(String(gait));
        $("input[name=" + assessmentType + "_TinettiTotal]").val(String(total));
        $("#" + assessmentType + "_Tinetti li").removeClass("strong");
        if (total < 19) $("#" + assessmentType + "_Tinetti li:eq(1)").addClass("strong");
        else if (total < 25) $("#" + assessmentType + "_Tinetti li:eq(2)").addClass("strong");
        else $("#" + assessmentType + "_Tinetti li:eq(3)").addClass("strong");
    },
    ToolTip: function(mooCode) {
        U.PostUrl("/Oasis/Guide", 'mooCode=' + mooCode, function(data) {
            $('.tooltip-box').remove()
            if (data.ItemIntent == null) return false;
            $('#desktop').append(unescape("%3Cdiv class=%22tooltip-box%22%3E%3Cdiv class=%22closer%22 onclick=%22$(this).parent().remove();%22%3EX%3C/div%3E%3Ch3%" +
                "3EItem Intent%3C/h3%3E%3Cdiv class=%22content intent%22%3E" + data.ItemIntent + "%3C/div%3E%3Ch3%3EResponse%3C/h3%3E%3Cdiv class=%22content resp" +
                "onse%22%3E" + data.Response + "%3C/div%3E%3Ch3%3EData Sources%3C/h3%3E%3Cdiv class=%22content sources%22%3E" + data.DataSources + "%3C/div%3E%3C" +
                "/div%3E"));
        });
    },
    OasisStatusAction: function(id, patientId, episodeId, assessmentType, actionType, pageName) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        Agency.RebindOasisToExport("OasisExport");
                        Oasis.RebindExported();
                        UserInterface.CloseWindow(pageName);
                        U.Growl(data.errorMessage, "success");
                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    Agency.RebindOasisToExport("OasisExport");
                    Oasis.RebindExported();
                    UserInterface.CloseWindow(pageName);
                    U.Growl(data.errorMessage, "success");
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    OasisSubmitOnlyAction: function(id, patientId, episodeId, assessmentType, pageName, signature, date) {
        U.PostUrl('Oasis/SubmitOnly', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, signature: signature, date: date }, function(data) {
            if (data.isSuccessful) {
                UserInterface.CloseModal();
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindOasisToExport("OasisExport");
                Oasis.RebindExported();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow(pageName);
                U.Growl(data.errorMessage, "success");
            } else U.Growl(data.errorMessage, "error");
        });
    },
    MarkAsExported: function(control, type) {
        var fields = $(":input", $(control)).serializeArray();
        if (fields != null && fields != undefined && fields.length > 0) {
            fields[fields.length] = { name: "StatusType", value: type };
            U.PostUrl('Oasis/MarkExported', fields, function(data) {
                if (data.isSuccessful) {
                    U.Growl(data.errorMessage, "success");
                    Patient.Rebind();
                    Schedule.Rebind();
                    Agency.RebindOasisToExport("OasisExport");
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    MarkAsSubmited: function(control, type) {
        var flag = (type === 'Exported');
        if ($("input[name=OasisSelected]:checked", $(control)).length > 0) {
            Acore.Modal({
                Name: "OASIS Mark as " + (flag ? "Exported" : "Completed ( Not Exported )"),
                Content: $("<div/>", { "id": "oasisMarkAsExportDialog", "class": "wrapper main" }).append(
                    $("<fieldset/>").append(function() {

                        if (flag) {
                            return $("<div/>").html("Please verify that your OASIS submission was accepted by CMS (Center for Medicare &#38; Medicaid Services) before you complete this step. If this OASIS file was not accepted, do not mark as exported.")
                        }
                        else {
                            return $("<div/>").html("")
                        }
                    }).append(
                        $("<div/>").addClass("strong align-center").html(flag ? "Are you sure that this OASIS file has been accepted by CMS?" : "Are you sure that you want mark this OASIS Completed without Export?"))).append(
                    $("<div/>").Buttons([
                        { Text: "Yes, Mark as " + (flag ? "Exported" : "Completed ( Not Exported )"), Click: function() { Oasis.MarkAsExported(control, type); $(this).closest(".window").Close() } },
                        { Text: "No, Cancel", Click: function() { $(this).closest(".window").Close() } }
                    ])),
                Width: "700px",
                Height: "150px",
                WindowFrame: false
            })
        } else U.Growl("Select at least one OASIS assessment to mark as " + (flag ? "Exported." : "Completed ( Not Exported )."), "error");
    },
    GenerateSubmit: function(control) { if ($("input[name=OasisSelected]:checked", $(control).closest('form')).length > 0 && $("#OasisExport_BranchCode").val() != "00000000-0000-0000-0000-000000000000") { $(control).closest('form').submit(); } else { U.Growl("Select at least one OASIS assessment to generate.", "error"); } },
    Reopen: function(id, patientId, episodeId, assessmentType, actionType) {
        Acore.Modal({
            Name: "Reopen OASIS",
            Content: $("<div/>", { "id": "oasisReopenDialog", "class": "wrapper main" }).append(
                        $("<fieldset/>").append(
                            $("<div/>").append(
                                $("<strong/>", { "text": "Note: " })).append(
                                $("<span/>", { "text": "If this OASIS assessment was already accepted by CMS (Center for Medicare & Medicaid Services), you will have to re-submit this OASIS assessment to CMS if changes are made to OASIS items." }))).append(
                            $("<div/>").addClass("strong").append(
                                $("<label/>", { "for": "Oasis_OasisReopenReason", "text": "Reason" })).append(
                                $("<textarea/>", { "id": "Oasis_OasisReopenReason", "name": "Oasis_OasisReopenReason" }))).append(
                            $("<div/>").addClass("strong align-center").html("Are you sure you want to reopen the assessment?"))).append(
                        $("<div/>").Buttons([
                            {
                                Text: "Yes, Reopen",
                                Click: function() {
                                    var reason = $("#Oasis_OasisReopenReason").val();
                                    U.PostUrl('Oasis/Submit', {
                                        Id: id,
                                        patientId: patientId,
                                        episodeId: episodeId,
                                        assessmentType: assessmentType,
                                        actionType: actionType,
                                        reason: reason
                                    }, function(data) {
                                        if (data.isSuccessful) {
                                            U.Growl(data.errorMessage, "success");
                                            Patient.Rebind();
                                            Schedule.Rebind();
                                            Agency.RebindOasisToExport("OasisExport");
                                            Oasis.RebindExported();
                                            Agency.RebindCaseManagement();
                                            UserInterface.CloseModal();
                                        } else U.Growl(data.errorMessage, "error");
                                    })
                                }
                            }, {
                                Text: "No, Cancel",
                                Click: function() { $(this).closest(".window").Close() }
                            }
                        ])
                    ),
            Width: "700px",
            Height: "225px",
            WindowFrame: false
        })
    },
    RebindToExport: function() { if ($('#generateOasisGrid').data('tGrid') != null) $('#generateOasisGrid').data('tGrid').rebind(); },
    RebindExported: function() { if ($('#exportedOasisGrid').data('tGrid') != null) $('#exportedOasisGrid').data('tGrid').rebind(); },
    AddSupply: function(control, episodeId, patientId, eventId, assessmentType, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&assessmentType=" + assessmentType + "&supplyId=" + supplyId +
            "&quantity=" + quantity + "&date=" + date;
        U.PostUrl("/Oasis/AddSupply", input, function(result) {
            var gridfilter = $("#" + assessmentType + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + assessmentType + "_GenericSupplyDescription").val('');
                $("#" + assessmentType + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + assessmentType + "_SupplyGrid").data('tGrid');
            if (grid != null) grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId, assessmentType: assessmentType });
        });
    },
    SupplyDescription: function(type) {
        $("#" + type + "_GenericSupplyDescription").keyup(function(event) {
            var message = $("#" + type + "_GenericSupplyDescription").val().length;
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyDescription").val(), limit: 20, type: "Description" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    SupplyCode: function(type) {
        $("#" + type + "_GenericSupplyCode").keyup(function(event) {
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyCode").val(), limit: 20, type: "Code" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    blockText: function(control, id) {
        if ($(control).is(':checked')) $(id).attr("disabled", "disabled").val("");
        else $(id).attr("disabled", "");
    },
    unBlockText: function(id) { $(id).attr("disabled", ""); },
    LoadBlankMasterCalendar: function(id, episodeId, patientId) {
        $("#" + id + "_BlankMasterCalendar").load('Oasis/BlankMasterCalendar', { episodeId: episodeId, patientId: patientId, assessmentType: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#" + id + "_BlankMasterCalendar").html('<p>There was an error making the AJAX request</p>');
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            } else if (textStatus == "success") {
                $("#" + id + "_BlankMasterCalendar").removeClass("loading");
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            }
        });
    },
    HideBlankMasterCalendar: function(id) {
        $("#" + id + "_BlankMasterCalendar").empty();
        $("#" + id + "_Show").show();
        $("#" + id + "_Hide").hide();
    },
    goals: function(fieldset) {
        fieldset.find("input[type=checkbox]").each(function() {
            if ($(this).parent().find("span.radio").length) {
                if ($(this).is(":not(:checked)")) $(this).parent().find("span.radio :input").attr("disabled", true);
                $(this).change(function() {
                    if ($(this).is(":checked")) $(this).parent().find("span.radio :input").attr("disabled", false);
                    else $(this).parent().find("span.radio :input").attr("disabled", true);
                });
            }
        });
    },
    interventions: function(fieldset) { Oasis.goals(fieldset); },
    gotoQuestion: function(question, assessmentType, version) {
        var tab = 1, q = parseInt(question.replace(/^[M0]*/, ""));
        if (assessmentType == "StartOfCare" || assessmentType == "ResumptionOfCare") {
            if (q == 1) tab = 17;
            if (q >= 1000 && q <= 1030) tab = 2;
            if (q >= 1032 && q <= 1036) tab = 3;
            if (q == 1100) tab = 5;
            if (q >= 1200 && q <= 1230) tab = 6;
            if (q >= 1240 && q <= 1242) tab = 7;
            if (q >= 1300 && q <= 1350) tab = 8;
            if (q >= 1400 && q <= 1410) tab = 9;
            if (q >= 1600 && q <= 1630) tab = 12;
            if (q >= 1700 && q <= 1750) tab = 14;
            if (q >= 1800 && q <= 1910) tab = 15;
            if (q >= 2000 && q <= 2040) tab = 17;
            if (q >= 2100 && q <= 2110) tab = 18;
            if (q >= 2200 && q <= 2250) tab = 19;
        } else if (assessmentType == "Recertification") {
            if (q == 1) tab = 16;
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 5;
            if (q == 1242) tab = 6;
            if (q >= 1306 && q <= 1350) tab = 7;
            if (q >= 1400 && q <= 1410) tab = 8;
            if (q >= 1610 && q <= 1630) tab = 11;
            if (q >= 1810 && q <= 1910) tab = 14;
            if (q == 2030) tab = 16;
            if (q >= 2200) tab = 17;
        } else if (assessmentType == "FollowUp") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 3;
            if (q == 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q == 1400) tab = 6;
            if (q >= 1610 && q <= 1630) tab = 7;
            if (q >= 1810 && q <= 1860) tab = 8;
            if (q == 2030) tab = 9;
            if (q == 2200) tab = 10;
        } else if (assessmentType == "TransferInPatientNotDischarged" || assessmentType == "TransferInPatientDischarged") {
            if (q >= 1040 && q <= 1055) tab = 2;
            if (q >= 1500 && q <= 1510) tab = 3;
            if (q >= 2004 && q <= 2015) tab = 4;
            if (q >= 2300 && q <= 2310) tab = 5;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2440)) tab = 6;
        } else if (assessmentType == "DischargeFromAgencyDeath") {
            if (q == 903 || q == 906) tab = 2;
        } else if (assessmentType == "DischargeFromAgency") {
            if (version <= 1) {
                if (q >= 1040 && q <= 1050) tab = 2;
                if (q == 1230) tab = 3;
                if (q >= 1242) tab = 4;
                if (q >= 1306 && q <= 1350) tab = 5;
                if (q >= 1400 && q <= 1410) tab = 6;
                if (q >= 1500 && q <= 1510) tab = 7;
                if (q >= 1600 && q <= 1620) tab = 8;
                if (q >= 1700 && q <= 1750) tab = 9;
                if (q >= 1800 && q <= 1890) tab = 10;
                if (q >= 2004 && q <= 2030) tab = 11;
                if (q >= 2100 && q <= 2110) tab = 12;
                if (q >= 2300 && q <= 2310) tab = 13;
                if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 14;
            } else if (version == 2) {
                if (q >= 1040 && q <= 1050) tab = 3;
                if (q == 1230) tab = 5;
                if (q >= 1242) tab = 6;
                if (q >= 1306 && q <= 1350) tab = 7;
                if (q >= 1400 && q <= 1410) tab = 8;
                if (q >= 1500 && q <= 1510) tab = 9;
                if (q >= 1600 && q <= 1620) tab = 11;
                if (q >= 1700 && q <= 1750) tab = 13;
                if (q >= 1800 && q <= 1890) tab = 14;
                if (q >= 2004 && q <= 2030) tab = 15;
                if (q >= 2100 && q <= 2110) tab = 16;
                if (q >= 2300 && q <= 2310) tab = 17;
                if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 18;
            }
        }
        if (Acore.Windows[assessmentType].IsOpen) {
            $("#window_" + assessmentType).WinFocus();
            $('.vertical-tab-list li:nth-child(' + tab + ') a', "#window_" + assessmentType).click();
            if (q != 1) {
                setTimeout(function() {
                    if ($("#" + assessmentType + "_" + question).length)
                        $(".general:nth-child(" + (tab + 1) + ")", "#window_" + assessmentType + "_content").scrollTop(
                        $("#" + assessmentType + "_" + question).closest("fieldset").position().top
                    );
                }, 1000);
            }
        }
    },
    oasisSignatureSubmit: function(form) {
        form.validate({
            submitHandler: function(form) {
                var finishBtn = $(window.parent.document).contents().find("#window_ModalWindow").contents().find("a:contains('Finish')");
                U.BlockButton(finishBtn);
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            window.parent.UserInterface.CloseWindow($("#oasisPageName").val());
                            window.parent.U.Growl(resultObject.errorMessage, 'success');
                            try {
                                window.parent.Patient.Rebind();
                                window.parent.Schedule.Rebind();
                                window.parent.Agency.RebindOasisToExport("OasisExport");
                                window.parent.Oasis.RebindExported();
                                window.parent.Agency.RebindCaseManagement();
                            } catch (e) { }
                            window.parent.UserInterface.CloseModal();
                        } else {
                            window.parent.U.Growl(resultObject.errorMessage, 'error');
                            U.UnBlockButton(finishBtn);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditCorrectionNumber: function() {
        $("#oasisCorrectionChange").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Agency.RebindOasisToExport("OasisExport");
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadCancel: function(Id, type) { U.PostUrl("/Oasis/Inactivate", { Id: Id, type: type }, function(result) { if (result.isValid) { jQuery('<form action="/Oasis/GenerateForCancel" method="post"><input type="hidden" name="Id" value="' + Id + '"></input><input type="hidden" name="type" value="' + type + '"></input></form>').appendTo('body').submit().remove(); } else { U.Growl(result.Message, "error"); } }, function(result) { }); }
}

var StartOfCare = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}
var ResumptionOfCare = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var Recertification = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var FollowUp = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var TransferInPatientNotDischarged = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var TransferInPatientDischarged = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var DischargeFromAgencyDeath = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var DischargeFromAgency = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var NonOasisStartOfCare = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisStartOfCare", "Oasis/NonOasisStartOfCare", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisStartOfCare");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}

var NonOasisRecertification = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisRecertification", "Oasis/NonOasisRecertification", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisRecertification");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}

var NonOasisDischarge = {
FormSubmit: function(AssessmentType, control, actionType, action) { U.HandlerHelperTemplate(AssessmentType, control, actionType, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisDischarge", "Oasis/NonOasisDischarge", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisDischarge");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}