﻿var Billing = {
    _patientId: "",
    _ClaimId: "",
    _ClaimType: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { tabstrip = $("#final-tabs").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { Billing.LoadContent(contentUrl, $("#final-tabs").find("div.t-content.t-state-active")); } } else U.Growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    finalTabStripOnSelect: function(e) {
        Billing.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement)
    },
    finalTabStripOnLoad: function(e) { setTimeout(function() { Acore.OnLoad($(e.target)) }, 500) },
    InitCenter: function() {
        $("#txtSearch_billing_Selection").keyup(function() {
            $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>');
            $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty();
            Billing._patientId = "";
            Billing.RebindPatientList();
        });
        $("select.billingBranchCode").change(function() {
            Insurance.loadInsuarnceDropDown('BillingHistory', '', false, function() {
                $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>');
                $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty();
                Billing._patientId = "";
                Billing.RebindPatientList();
            })
        });
        $("select.billingStatusDropDown").change(function() {
            $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>');
            $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty();
            Billing._patientId = "";
            Billing.RebindPatientList();
        });
        $("select.billingPaymentDropDown").change(function() {
            $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>');
            $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty();
            Billing._patientId = "";
            Billing.RebindPatientList();
        })
    },
    RebindPatientList: function() { var patientGrid = $('#BillingSelectionGrid').data('tGrid'); if (patientGrid != null) { patientGrid.rebind({ branchId: $("select.billingBranchCode").val(), statusId: $("select.billingStatusDropDown").val(), paymentSourceId: $("select.billingPaymentDropDown").val(), name: $("#txtSearch_billing_Selection").val() }); } },
    HideNewRapAndFinalMenu: function() { var gridTr = $('#BillingSelectionGrid').find('.t-grid-content tbody tr'); if (gridTr != undefined && gridTr != null) { if (gridTr.length > 0) { $("#billingHistoryTopMenu").show(); } else { $("#billingHistoryTopMenu").hide(); } } },
    InitRap: function() { U.InitTemplate($("#rapVerification"), function() { UserInterface.CloseWindow('rap'); Billing.ReLoadUpProcessedClaimOnAction(); }, "Rap is successfully verified."); },
    InitFinal: function() { Acore.OnLoad($(contentElement)); },
    ReLoadUnProcessedClaim: function(Id, branchId, insuranceId, controllerAction, type) { $(Id).empty().addClass('loading').load('Billing/' + controllerAction, { branchId: branchId, insuranceId: insuranceId, type: type }, function(responseText, textStatus, XMLHttpRequest) { $(Id).removeClass("loading"); if (textStatus == 'error') $(Id).html(U.AjaxError); }); },
    ReLoadUpProcessedClaimOnAction: function() {
        Billing.ReLoadUnProcessedClaim('#Billing_CenterContentFinal', $('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val(), 'FinalGrid', 'Final');
        Billing.ReLoadUnProcessedClaim('#Billing_CenterContentRap', $('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val(), 'RapGrid', 'Rap');
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl) {
            $(contentElement).html("&#160;").addClass("loading");
            $.get(contentUrl,
                function(data) {
                    $(contentElement).removeClass("loading").html(data);
                });
        }
    },
    FinalTabLoad: function(e) {
        Acore.OnLoad($(e.target))
    },
    loadGenerate: function(control, pageId) { Acore.Open(pageId, 'Billing/ClaimSummary', function() { }, $(":input", $(control)).serializeArray()) },
    Navigate: function(index, id) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            Billing.UnBlockClickAndEnable();
                            var tabstrip = $("#final-tabs").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                            var $link = $(item).find('.t-link');
                            var contentUrl = $link.data('ContentUrl');
                            if (contentUrl != null) Billing.LoadContent(contentUrl, $("#final-tabs").find("div.t-content.t-state-active"));
                            if (index == 1 || index == 2)
                                Billing.ReLoadUnProcessedClaim('#Billing_CenterContentFinal', $('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val(), 'FinalGrid', 'Final');
                        } else U.Growl(result.errorMessage, 'error');
                    },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    NavigateBack: function(index) {
        var tabstrip = $("#final-tabs").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    NoPatientBind: function(id) { Billing.RebindActivity(id) },
    PatientListDataBound: function() {
        if ($("#BillingSelectionGrid .t-grid-content tr").length) {
            if (Billing._patientId == "") $('#BillingSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + Billing._patientId + ')', $('#BillingSelectionGrid')).closest('tr').click();
            $("#billingHistoryTopMenu").show();
        } else {
            $("#billingHistoryClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
            $("#billingHistoryTopMenu").hide();
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            Billing._patientId = patientId;
            Billing._ClaimId = "";
            Billing._ClaimType = "";
            Billing.RebindActivity(patientId, $("#BillingHistory_InsuranceId").val());
        } $("#billingHistoryClaimData").empty();
    },
    RebindActivity: function(id, insuranceId) {
        if (!insuranceId) {
            insuranceId = "0";
        }
        var rebindActivity = $('#BillingHistoryActivityGrid').data('tGrid');
        if (rebindActivity != null) rebindActivity.rebind({ patientId: id, insuranceId: insuranceId });
        if ($('#BillingHistoryActivityGrid').find('.t-grid-content tbody').is(':empty')) $("#billingHistoryClaimData").empty();
    },
    UpdateStatus: function(control) {
        var btn = $(control + " a:contains('Mark Claim(s) As Submitted')");
        U.BlockButton(btn);
        U.PostUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                Billing.ReLoadUpProcessedClaimOnAction();
                UserInterface.CloseWindow('claimSummaryrap');
                UserInterface.CloseWindow('claimSummaryfinal');
                U.Growl(result.errorMessage, "success");
            } else { U.Growl(result.errorMessage, "error"); U.UnBlockButton(btn); }
        }, null)
    },
    FinalComplete: function(id) {
        U.PostUrl('Billing/FinalComplete', { id: id }, function(result) {
            if (result.isSuccessful) {
                Billing.ReLoadUpProcessedClaimOnAction();
                UserInterface.CloseWindow('final');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    GenerateAllCompleted: function(control, pageId) {
        $("input[name=ClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true)
        });
        Billing.loadGenerate(control, pageId);
    },
    NavigateBack: function(index) { var tabstrip = $("#final-tabs").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); },
    NoPatientBind: function(id) { Billing.RebindActivity(id); },
    PatientListDataBound: function() { if ($("#BillingSelectionGrid .t-grid-content tr").length) { if (Billing._patientId == "") $('#BillingSelectionGrid .t-grid-content tr').eq(0).click(); else $('td:contains(' + Billing._patientId + ')', $('#BillingSelectionGrid')).closest('tr').click(); $("#billingHistoryTopMenu").show(); } else { $("#billingHistoryClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>"); $("#billingHistoryTopMenu").hide(); } },
    OnPatientRowSelected: function(e) { if (e.row.cells[2] != undefined) { var patientId = e.row.cells[2].innerHTML; Billing._patientId = patientId; Billing._ClaimId = ""; Billing._ClaimType = ""; Billing.RebindActivity(patientId, $("#BillingHistory_InsuranceId").val()); } $("#billingHistoryClaimData").empty(); },
    RebindActivity: function(id, insuranceId) {
        if (!insuranceId) {
            insuranceId = "0";
        }
        var rebindActivity = $('#BillingHistoryActivityGrid').data('tGrid');
        if (rebindActivity != null) {
            rebindActivity.rebind({ patientId: id, insuranceId: insuranceId });
        }
        if ($('#BillingHistoryActivityGrid').find('.t-grid-content tbody').is(':empty')) {
            $("#billingHistoryClaimData").empty();
        } 
    },
    UpdateStatus: function(control, type) { U.PostUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedClaimOnAction(); UserInterface.CloseWindow('claimSummary' + type); U.Growl(result.errorMessage, "success"); } else { U.Growl(result.errorMessage, "error"); } }, null); },
    FinalComplete: function(id) { U.PostUrl('Billing/FinalComplete', { id: id }, function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedClaimOnAction(); UserInterface.CloseWindow('final'); U.Growl(result.errorMessage, "success"); } else { U.Growl(result.errorMessage, "error"); } }, null); },
    GenerateAllCompleted: function(control, pageId) { $("input[name=ClaimSelected]", $(control)).each(function() { $(this).attr("checked", true) }); Billing.loadGenerate(control, pageId); },
    InitClaim: function() { U.InitTemplate($("#updateClaimForm"), function() { UserInterface.CloseModal(); Billing.RebindActivity(Billing._patientId); Billing.ReLoadUpProcessedClaimOnAction(); }, "Claim updated successfully."); },
    InitNewClaim: function() { U.InitTemplate($("#createClaimForm"), function() { UserInterface.CloseModal(); Billing.RebindActivity(Billing._patientId); Billing.ReLoadUpProcessedClaimOnAction(); }, "Claim created successfully."); },
    InitPendingClaimUpdate: function() { U.InitTemplate($("#insuranceId"), function() { Billing.loadPendingClaimRap(); Billing.loadPendingClaimFinal(); Billing.ReLoadUpProcessedClaimOnAction(); }, "Claim updated successfully."); },
    DeleteClaim: function(patientId, id, type) {
        if (confirm("Are you sure you want to delete this claim?")) {
            U.PostUrl("Billing/DeleteClaim", { patientId: patientId, id: id, type: type }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Billing.RebindActivity(patientId);
                    Billing.ReLoadUpProcessedClaimOnAction();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error")
            })
        }
    },
    OnClaimDataBound: function() {
        if ($("#BillingHistoryActivityGrid .t-grid-content tr").length) $('#BillingHistoryActivityGrid .t-grid-content tbody tr.t-master-row').eq(0).click();
        else $("#billingHistoryClaimData").empty();
    },
    OnClaimRowSelected: function(e) {
        if (e.row.cells[1] != undefined && e.row.cells[9] != undefined && Billing._patientId != undefined && Billing._patientId != "") {
            var claimType = e.row.cells[1].innerHTML;
            var claimId = e.row.cells[9].innerHTML;
            Billing._ClaimId = claimId;
            Billing._ClaimType = claimType;
            Billing.loadClaimInfo(Billing._patientId, claimId, claimType)
        }
    },
    loadClaimInfo: function(patientId, claimId, claimType) {
        $("#billingHistoryClaimData").empty().addClass("loading").load('Billing/ClaimInfo', { patientId: patientId, claimId: claimId, claimType: claimType }, function(responseText, textStatus, XMLHttpRequest) {
            $("#billingHistoryClaimData").removeClass("loading");
            if (textStatus == 'error') $('#billingHistoryClaimData').html(U.AjaxError)
        })
    },
    loadPendingClaimRap: function() {
        $("#pendingClaim_rapContent").empty().addClass("loading").load('Billing/PendingClaimRap', null, function(responseText, textStatus, XMLHttpRequest) {
            $("#pendingClaim_rapContent").removeClass("loading");
            if (textStatus == 'error') $('#pendingClaim_rapContent').html(U.AjaxError)
        })
    },
    loadPendingClaimFinal: function() {
        $("#pendingClaim_finalContent").empty().addClass("loading").load('Billing/PendingClaimFinal', null, function(responseText, textStatus, XMLHttpRequest) {
            $("#pendingClaim_finalContent").removeClass("loading");
            if (textStatus == 'error') $('#pendingClaim_finalContent').html(U.AjaxError)
        })
    },
    OpenUBOFour: function(patientId, claimId, claimType) {
        U.GetAttachment("Billing/UB04Pdf", { patientId: patientId, Id: claimId, type: claimType })
    },
    Sort: function(Id, type, sortBy) {
        var contentId = "#" + Id;
        if (contentId != undefined && contentId != "") {
            var controlContentId = $(contentId);
            if (controlContentId != undefined) {
                var olContentControl = $("ol", controlContentId);
                if (olContentControl != undefined) {
                    olContentControl.addClass("loading");
                    var invert = false;
                    var lowerCaseType = (type).toLowerCase();
                    var currentSort = $("input[name=" + type + "Sort]", controlContentId).val();
                    if (currentSort != undefined) {
                        if (currentSort == sortBy) { invert = $("input[name=IsSortInvert]", controlContentId).val(); }
                        setTimeout(function() {
                            var items = new Array();
                            if (sortBy == lowerCaseType + "-episode") $("." + sortBy + " .very-hidden", olContentControl).each(function() { items.push($(this).text()) });
                            else $("span." + sortBy, olContentControl).each(function() { items.push($(this).text()) });
                            items = items.sort();
                            if (invert == 'true') items.reverse();
                            for (var i = 0; i < items.length; i++) olContentControl.append($("." + sortBy + ":contains('" + items[i] + "')", olContentControl).closest("li"));
                            for (var i = 0; i < items.length; i++) $("li:eq(" + i + ") ." + lowerCaseType + "-checkbox", olContentControl).html((i + 1) + "." + $("li:eq(" + i + ") ." + lowerCaseType + "-checkbox", olContentControl).html().split(".")[1]);
                            $("li:nth-child(even)", olContentControl).removeClass("odd").addClass("even");
                            $("li:nth-child(odd)", olContentControl).removeClass("even").addClass("odd");
                            olContentControl.removeClass("loading");
                            $("input[name=" + type + "Sort]", controlContentId).val(sortBy);
                            $("input[name=IsSortInvert]", controlContentId).val(invert == 'true' ? false : true);
                        }, 100);
                    }
                }
            }
        }
    },
    SubmitClaimDirectly: function(control, type) {
        var btn = $(control + " a:contains('Submit Electronically')");
        U.BlockButton(btn);
        U.PostUrl('Billing/SubmitClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                $(control).closest(".window").Close();
                U.Growl(result.errorMessage, "success");
                Billing.ReLoadUpProcessedClaimOnAction();
            }
            else { U.Growl(result.errorMessage, "error"); U.UnBlockButton(btn); }
        }, null);
    },
    LoadGeneratedClaim: function(control) {
        U.PostUrl("/Billing/CreateANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                jQuery('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                Billing.ReLoadUpProcessedClaimOnAction();
            } else { U.Growl(result.errorMessage, "error"); }
        }, function(result) { });
    },
    onRapDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onRapDetailViewExpand: function(e) { $("#Raps table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Raps table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onFinalDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onFinalDetailViewExpand: function(e) { $("#Finals table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Finals table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onEditClaim: function(e) {
        if (e.dataItem != undefined) {
            var form = $(e.form);
            if (form != null && form != undefined) {
                $(form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; });
                var day = e.dataItem.PaymentDate;
                if (day != null && day != undefined && day.getFullYear() == "1") {
                    $(form).find('input[name=PaymentDate]').val("");
                }
            }
        }
    },
    onClaimDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onClaimDetailViewExpand: function(e) { $("#BillingHistoryActivityGrid table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#BillingHistoryActivityGrid table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    expandFirstRow: function(grid, row) { if (grid.$rows().index(row) == 0) { grid.expandRow(row); } },
    onRapOnRowDataBound: function(e) { var grid = $("#Raps").data('tGrid'); Billing.expandFirstRow(grid, e.row); },
    ReLoadPendingClaimRap: function(branchId, insuranceId) { U.RebindTGrid($("#Raps"), { branchId: branchId, insuranceId: insuranceId }); },
    ReLoadPendingClaimFinal: function(branchId, insuranceId) { U.RebindTGrid($("#Finals"), { branchId: branchId, insuranceId: insuranceId }); },
    InitRemittance: function() {
        $("#externalRemittanceUploadForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("The upload was successful.", "success");
                            $("#Billing_ExternalRemittanceUpload").val('');
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("The upload was successful.", "success");
                            $("#Billing_ExternalRemittanceUpload").val('');
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    RebindSubmittedBatchClaims: function() {
        U.RebindTGrid($("#List_SubmittedClaims"), { ClaimType: $("#SubmittedClaims_ClaimType").val(), StartDate: $("#SubmittedClaims_StartDate").val(), EndDate: $("#SubmittedClaims_EndDate").val() });
        var $exportLink = $('#SubmittedClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/ClaimType=([^&]*)/, 'ClaimType=' + $("#SubmittedClaims_ClaimType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SubmittedClaims_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SubmittedClaims_EndDate").val());
        $exportLink.attr('href', href);
    },
    ReloadEligibilityList: function(patientId) {
        $("#MedicareEligiblity_ReportContent ol").addClass('loading');
        $("#MedicareEligiblity_ReportContent").load('Billing/EligibilityList', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#MedicareEligiblity_ReportContent ol").removeClass("loading");
            if (textStatus == 'error') $('#MedicareEligiblity_ReportContent').html(U.AjaxError);
            $("#MedicareEligiblity_ReportContent ol li:first").addClass("first");
            $("#MedicareEligiblity_ReportContent ol li:last").addClass("last");
        })
    },
    SubmittedClaimDetail: function(Id) {
        Acore.Open("submittedclaimdetail", 'Billing/SubmittedClaimDetail', function() { }, { Id: Id, StartDate: $("#SubmittedClaims_StartDate").val(), EndDate: $("#SubmittedClaims_EndDate").val() })
    },
    RemittanceDelete: function(id) {
        if (confirm("Are you sure you want to delete this remittance?")) U.PostUrl("/Billing/DeleteRemittance", { Id: id }, function(result) {
            if (result.isSuccessful) {
                U.Growl(result.errorMessage, "success");
                U.RebindDataGridContent('BillingRemittance', 'Billing/RemittanceContent', { StartDate: $('#BillingRemittance_StartDate').val(), EndDate: $('#BillingRemittance_EndDate').val() }, null);
            } else U.Growl(result.errorMessage, "error");
        })
    },
    loadAuthorizationContent: function(contentId, authorizationId) {
        $(contentId).load('Billing/AuthorizationContent', { AuthorizationId: authorizationId }, function(responseText, textStatus, XMLHttpRequest) { });
    },
    LoadInsuranceContent: function(contentId, patientId, insuranceId, startDate, endDate, claimTypeIdentifier) {
        $(contentId).load('Billing/InsuranceInfoContent', {
            PatientId: patientId,
            InsuranceId: insuranceId,
            StartDate: startDate,
            EndDate: endDate,
            ClaimTypeIdentifier: claimTypeIdentifier
        }, function(responseText, textStatus, XMLHttpRequest) { })
    },
    onEditRapSnapShotDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined && dataItem.PaymentDate != undefined) {
            var day = new Date(e.dataItem.PaymentDate);
            if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[3].innerHTML = '&#160;';
        }
    },
    onRapPendingDataBound: function(e) {
        if (e != undefined && e.dataItem != undefined && e.dataItem.PaymentDate != undefined) {
            var day = new Date(e.dataItem.PaymentDate);
            if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[1].innerHTML = '&#160;';
        }
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else if (inputs != null) {
            inputs[inputs.length] = { name: "Id", value: Id };
            inputs[inputs.length] = { name: "PatientId", value: PatientId };
            inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
            U.PostUrl("Billing/ChangeFinalSupplyStatus", inputs, function(result) {
                if (result != null && result.isSuccessful) {
                    U.RebindTGrid($("#FinalBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    U.RebindTGrid($("#FinalUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                }
            })
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                U.PostUrl("Billing/FinalSuppliesDelete", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#FinalBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#FinalUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });

                    }
                })
            }
        }
    },
    PostRemittance: function(Id, control) {
        var inputs = $("input:checkbox", $(control)).serializeArray();
        if (inputs != null && inputs != undefined && inputs.length > 0) {
            inputs[inputs.length] = { name: "Id", value: Id };
            U.PostUrl("Billing/PostRemittance", inputs, function(result) {
                if (result != null && result.isSuccessful) {
                    if (result.isSuccessful) {
                        U.RebindPageContent("RemittanceDetail", 'Billing/RemittanceDetailContent', { Id: Id });
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                }
            });
        }
        else {
            alert('Select at least one option.');
            return;
        }
    },
    UnBlockClickAndEnable: function() {
        var element = $("#final-tabs ul li.t-state-active").next();
        element.removeClass("t-state-disabled");
        element.find(".t-link").unbind('click', U.BlockClick);
    }
}