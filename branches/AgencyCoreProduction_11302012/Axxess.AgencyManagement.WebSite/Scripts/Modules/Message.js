﻿var Message = {
    _CKELoaded: false,
    _TokenList: null,
    _Reply: false,
    _Forward: false,
    _MessageId: "",
    AddRemoveRecipient: function(input) {
        if ($("#" + input).prop("checked")) $.data($("#" + input).get(0), "tokenbox", { "token": Message._TokenList.insertToken($("#" + input).attr("value"), $("#" + input).attr("title")) });
        else Message._TokenList.removeToken($.data($("#" + input).get(0), "tokenbox").token);
        Message.PositionBottom();
    },
    Cancel: function() { UserInterface.CloseWindow("newmessage") },
    Delete: function(id, type, bypass) { U.Delete("Message", "Message/Delete", { Id: id, Type: type }, function() { Message.Rebind() }, bypass) },
    Init: function() {
        $("#MessageReplyButton").click(function() { Message.Reply($("#messageId").val()) });
        $("#MessageForwardButton").click(function() { Message.Forward($("#messageId").val()) });
        $("#MessageDeleteButton").click(function() { Message.Delete($("#messageId").val(), $("#messageType").val()); });
        $("#MessagePrintButton").click(function() { U.GetAttachment("Message/MessagePdf", { id: $("#messageId").val(), type: $("#messageType").val() }) });
        $("#MessageDeleteButton2").click(function() { Message.Delete($("#messageId").val(), $("#messageType").val()); });
        $("#MessagePrintButton2").click(function() { U.GetAttachment("Message/MessagePdf", { id: $("#messageId").val(), type: $("#messageType").val() }) });
        $("#inboxType").unbind("change").bind("change", function() {
            Message.Rebind();
            $("#List_Messages .t-grid-header .t-header").eq(0).find("a").text($("#inboxType").find(":selected").text());
        })
    },
    InitCKE: function() {
        CKEDITOR.replace("New_Message_Body", {
            skin: "office2003",
            resize_enabled: false,
            height: "100%",
            removePlugins: "elementspath",
            toolbarCanCollapse: false,
            toolbar: [[
                "Bold", "Italic", "Underline", "-",
                "NumberedList", "BulletedList", "Outdent", "Indent", "-",
                "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-",
                "Link", "Unlink", "-",
                "Font", "FontSize", "TextColor", "-",
                "SpellChecker", "Scayt"]]
        })
    },
    InitNew: function() {
        $("#New_Message_CheckallRecipients").change(function() {
            $("input[name=Recipients]").each(function() {
                if ($("#New_Message_CheckallRecipients").prop("checked") != $(this).prop("checked")) {
                    $(this).prop("checked", $("#New_Message_CheckallRecipients").prop("checked"));
                    Message.AddRemoveRecipient($(this).attr("id"));
                }
            })
        });
        $("#new-message-body-div").html(
            $("<div/>").append(
                $("<textarea/>", { id: "New_Message_Body", name: "Body" })));
        $("#newMessageForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) {
                        if ($("#newMessageForm .token-input-token-facebook").length == 0) {
                            U.Growl("Please Select a Recipient.", "error");
                            return false;
                        }
                        $("textarea[name=Body]").val(CKEDITOR.instances["New_Message_Body"].getData());
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == "Success") {
                            UserInterface.CloseWindow("newmessage");
                            U.Growl("Message has been sent successfully.", "success");
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            UserInterface.CloseWindow("newmessage");
                            U.Growl("Message has been sent successfully.", "success");
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        Message._TokenList = $.fn.tokenInput("#New_Message_Recipents", "Message/Recipients", {
            classes: {
                tokenList: "token-input-list-facebook",
                token: "token-input-token-facebook",
                tokenDelete: "token-input-delete-token-facebook",
                selectedToken: "token-input-selected-token-facebook",
                highlightedToken: "token-input-highlighted-token-facebook",
                dropdown: "token-input-dropdown-facebook",
                dropdownItem: "token-input-dropdown-item-facebook",
                dropdownItem2: "token-input-dropdown-item2-facebook",
                selectedDropdownItem: "token-input-selected-dropdown-item-facebook",
                inputToken: "token-input-input-token-facebook"
            }
        });
        $("#messageTypeHeader").html("New Message");
        if ((Message._Reply || Message._Forward) && Message._MessageId != "") {
            U.PostUrl("Message/Get", { id: Message._MessageId }, function(message) {
                if (Message._Reply) {
                    $("#messageTypeHeader").html("Reply Message");
                    if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
                    if ($("input[name=Recipients][value=" + message.FromId + "]").length) Message.AddRemoveRecipient($("input[name=Recipients][value=" + message.FromId + "]").prop("checked", true).attr("id"));
                } else if (Message._Forward) {
                    $("#messageTypeHeader").html("Forward Message");
                    if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
                }
                $("#New_Message_Subject").val(message.Subject);
                $("#New_Message_PatientId").val(message.PatientId);
                $("#New_Message_Body").val(message.ReplyForwardBody);
                if (!Message._CKELoaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { Message._CKELoaded = true; Message.InitCKE() });
                else Message.InitCKE();
                Message._Reply = Message._Forward = false, Message._MessageId = "";
            });
        } else {
            if (!Message._CKELoaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { Message._CKELoaded = true; Message.InitCKE() });
            else Message.InitCKE();
        }
    },
    InitWidget: function() {
        Message.LoadWidgetMessages();
        $(".mail-controls select").change(function() {
            if (!$(this).find("option:first").prop("selected)")) {
                if ($("#messages-widget-content input:checked").length) {
                    if ($(this).find(":selected").html() == "Archive") { U.Growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Delete") {
                        if (confirm("Are you sure you want to delete all selected messages?")) {
                            $("#messages-widget-content input:checked").each(function() {
                                Message.Delete($(this).val(), $(this).attr("messagetype"), true);
                                $(this).parent().remove();
                            });
                            $(".mail-controls select").val(0);
                            Message.LoadWidgetMessages();
                        }
                    }
                    if ($(this).find(":selected").html() == "Mark as Read") { U.Growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Mark as Unread") { U.Growl("This feature coming soon."); }
                } else {
                    U.Growl("Error: No messages selected to " + $(this).find(":selected").html(), "error");
                    $(this).find("option:first").attr("selected", true);
                }
            }
        })
    },
    LoadWidgetMessages: function() {
        U.PostUrl("Message/WidgetList", {}, function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                $("#messages-widget-content").html("");
                for (var i = 0; i < data.Data.length; i++) {
                    var current = data.Data[i];
                    $("#messages-widget-content").append(
                        $("<div/>", { "class": current.MarkAsRead ? "" : "strong" }).append(
                            $("<input/>", { type: "checkbox", "class": "radio float-left", messagetype: current.Type, value: current.Id })).append(
                            $("<a/>", { href: "javascript:void(0)", "class": "message", html: current.FromName + " &#8211; " + current.Subject }).click(function() {
                                $(this).parent().removeClass("strong");
                                if (Acore.Windows.messageinbox.IsOpen) {
                                    $("#window_messageinbox").WinFocus();
                                    $("#" + $(this).prev().val()).closest("tr").click();
                                } else UserInterface.ShowMessages($(this).prev().val());
                            }).mouseover(function() {
                                $(this).addClass("t-state-hover")
                            }).mouseout(function() {
                                $(this).removeClass("t-state-hover")
                            })
                        )
                    );
                }
            } else $("#messages-widget-content").append($("<h1/>", { "class": "align-center", text: "No Messages Found" })).parent().find(".widget-more").remove();
        });
    },
    OnDataBound: function(e) {
        var isAgencyFrozen = $("#Messages_IsAgencyFrozen").val();
        if (Message._MessageId != "" && $("#" + Message._MessageId).length) {
            $("#" + Message._MessageId).closest("tr").click();
        } else {
            $("#List_Messages .t-grid-content tr").eq(0).click();
            if ($("#List_Messages .t-grid-content tr").length == 0 && !isAgencyFrozen) {
                $("#messageInfoResult").html(
                    $("<div/>", { "class": "ajaxerror" }).append(
                        $("<h1/>", { "text": "No Messages found." })).append(
                        $("<div/>", { "class": "heading" }).Buttons([{
                            Text: "Add New Message",
                            Click: function() {
                                UserInterface.ShowNewMessage()
                            }
}])
                    )
                );
            }
        }
        Message._MessageId = "";
    },
    OnRowSelected: function(e) {
        if (e.row.cells[1] != undefined) {
            var messageId = e.row.cells[1].innerHTML;
            var messageType = e.row.cells[2].innerHTML;
            $("#messageInfoResult").empty().addClass("loading").Load("Message/Data", { id: messageId, type: messageType }, function(r, t, x, e) {
                Message.Init(messageId, messageType);
                $("#messageInfoResult").removeClass("loading");
                if (t == "error") $("#messageInfoResult").html(U.AjaxError);
            });
            $("#" + messageId).removeClass("false").addClass("true");
        }
    },
    PositionBottom: function() {
        $("#new-message-body-div").css("top", parseInt($(".token-input-list-facebook").height() + 137).toString() + "px");
    },
    Rebind: function() { U.RebindTGrid($("#List_Messages"), { inboxType: $("#inboxType").val() }) },
    Reply: function(Id) {
        Message._Reply = true, Message._MessageId = Id;
        Acore.Open("newmessage");
    },
    Forward: function(Id) {
        Message._Forward = true, Message._MessageId = Id;
        Acore.Open("newmessage");
    }
}