﻿var Log = {
    LoadEpisodeLog: function(episodeId, patientId) { Acore.Open("episodelogs", 'Schedule/EpisodeLogs', function() { }, { episodeId: episodeId, patientId: patientId }); },
    LoadPatientLog: function(patientId) { Acore.Open("patientlogs", 'Patient/PatientLogs', function() { }, { patientId: patientId }); },
    LoadMedicationLog: function(patientId) { Acore.Open("medicationlogs", 'Patient/MedicationLogs', function() { }, { patientId: patientId }); },
    LoadPhysicianLog: function(physicianId) { Acore.Open("physicianlogs", 'Physician/PhysicianLogs', function() { }, { physicianId: physicianId }); },
    LoadUserLog: function(userId) { Acore.Open("userlogs", 'User/UserLogs', function() { }, { userId: userId }); },
    LoadReferralLog: function(referralId) { Acore.Open("referrallogs", 'Referral/ReferralLogs', function() { }, { referralId: referralId }); },
    LoadContactLog: function(contactId) { Acore.Open("contactlogs", 'Agency/ContactLogs', function() { }, { contactId: contactId }); },
    LoadInsuranceLog: function(insuranceId) { Acore.Open("insurancelogs", 'Agency/InsuranceLogs', function() { }, { insuranceId: insuranceId }); },
    LoadLocationLog: function(locationId) { Acore.Open("locationlogs", 'Agency/LocationLogs', function() { }, { locationId: locationId }); },
    LoadHospitalLog: function(hospitalId) { Acore.Open("hospitallogs", 'Agency/HospitalLogs', function() { }, { hospitalId: hospitalId }); },
    LoadTemplateLog: function(templateId) { Acore.Open("templatelogs", 'Agency/TemplateLogs', function() { }, { templateId: templateId }); },
    LoadSupplyLog: function(supplyId) { Acore.Open("supplylogs", 'Agency/SupplyLogs', function() { }, { supplyId: supplyId }); },
    LoadClaimLog: function(type, claimId, patientId) { Acore.Open("claimlogs", 'Billing/ClaimLogs', function() { }, { type: type, claimId: claimId, patientId: patientId }); },
    LoadAdjustmentCodeLog: function(adjustmentCodeId) { Acore.Open("adjustmentcodelogs", 'Agency/AdjustmentCodeLogs', function() { }, { adjustmentCodeId: adjustmentCodeId }); }
}

