﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<script type="text/javascript">
    function hideGrid() {
        $(".t-animation-container").hide();
    }
</script>
<div class="main">
    <% using (Html.BeginForm("AddLicense", "User", FormMethod.Post, new { @id = "editUserLicenseForm" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "Edit_UserLicense_Id" }) %>
        <fieldset>
            <legend></legend>
            <div class="column">
                <div class="row">
                    <label>License Type</label>
                    <div class="float-right">
                        <%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "Edit_UserLicense_Type", @class = "valid float-right" })%>
                        <div class="clear"></div>
                        <%= Html.TextBox("OtherLicenseType", "", new { @id = "Edit_UserLicense_TypeOther", @class = "valid float-right", @maxlength = "25" }) %>
                    </div>
                </div>
                <div class="row">
                    <label>License Number</label>
                    <div class="float-right">
                        <input type="text" name="LicenseNumber" id="Edit_UserLicense_LicenseNumber" />
                    </div>
                </div>
                <div class="row">
                    <label>Issue Date</label>
                    <div class="float-right">
                        <input type="text" class="date-picker" name="InitiationDate" id="Edit_UserLicense_InitDate" />
                    </div>
                </div>
                <div class="row">
                    <label>Expiration Date</label>
                    <div class="float-right">
                        <input type="text" class="date-picker" name="ExpirationDate" id="Edit_UserLicense_ExpDate" />
                    </div>
                </div>
                <div class="row">
                    <label>Attachment</label>
                    <div class="float-right">
                        <input type="file" name="Attachment" id="Edit_UserLicense_Attachment" />
                    </div>
                </div>
                <div class="row">
                    <div class="buttons">
                        <ul>
                            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add License</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
           <%-- <table class="form">
                <tbody>
                    <tr>
                        <td>License Type</td>
                        <td>Issue Date</td>
                        <td>Expiration Date</td>
                        <td colspan="2">Attachment</td>
                    </tr>
                    <tr>
                        <td>
                            <%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "Edit_UserLicense_Type", @class = "valid fill" })%>
                            <%= Html.TextBox("OtherLicenseType", "", new { @id = "Edit_UserLicense_TypeOther", @class = "valid fill", @maxlength = "25" }) %>
                        </td>
                        <td><input type="text" class="date-picker" name="InitiationDate" id="Edit_UserLicense_InitDate" /></td>
                        <td><input type="text" class="date-picker" name="ExpirationDate" id="Edit_UserLicense_ExpDate" /></td>
                        <td><input type="file" name="Attachment" id="Edit_UserLicense_Attachment" /></td>
                        <td>
                            <div class="buttons">
                                <ul>
                                    <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add License</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>--%>
        </fieldset>
    <% } %>
    <div class="currentlicenses">
        <%= Html.Telerik().Grid<License>().Name("List_User_Licenses").DataKeys(keys => { keys.Add(M => M.Id).RouteKey("Id"); })
            .Columns(columns =>
            {
            columns.Bound(l => l.LicenseType).ReadOnly().Sortable(true);
            columns.Bound(l => l.LicenseNumber);
            columns.Bound(l => l.InitiationDateFormatted).Title("Issue Date").Sortable(true).Width(130).ReadOnly();
            columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(true).Width(130);
            columns.Bound(l => l.AssetUrl).Title("Attachment").ReadOnly().Sortable(false);
            columns.Bound(l => l.AssetId).Width(0);
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(150).Title("Action");
            })
            .DataBinding(dataBinding => dataBinding.Ajax()
                .Select("LicenseList", "User", new { userId = Model })
                .Update("UpdateLicense", "User", new { userId = Model })
                .Delete("DeleteLicenseFromGrid", "User", new { userId = Model }))
                                                            .Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true)).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events.OnSave("hideGrid"))%>
                                                                              
        %>
    </div>
</div>
<script type="text/javascript">
    U.ShowIfSelectEquals(
        $("#Edit_UserLicense_Type"), "Other",
        $("#Edit_UserLicense_TypeOther"));

    $('#List_User_Licensesform').delegate('input:eq(0)', 'keyup', function(ev) {
        if (ev.keyCode == 13) {
            $(".t-animation-container").hide();
        }
    });
</script>