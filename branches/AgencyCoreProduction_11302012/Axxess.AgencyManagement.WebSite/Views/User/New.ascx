﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<bool>" %>
<span class="wintitle">New User | <%= Current.AgencyName %></span>
<div id="newUserContent" class="wrapper main">
<%  using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "newUserForm" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_User_EmailAddress" class="float-left">E-mail Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_User_EmailAddress", @class = "email required input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_User_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", "", new { @id = "New_User_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_User_MiddleName" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleName", "", new { @id = "New_User_MiddleName", @class = "text input_wrapper mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="New_User_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", "", new { @id = "New_User_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_User_Credentials" class="float-left">Credentials:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", "", new { @id = "New_User_Credentials", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="New_User_OtherCredentials" class="float-left">Other Credentials (specify):</label>
                <div class="float-right"><%= Html.TextBox("CredentialsOther", "", new { @id = "New_User_OtherCredentials", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_User_TitleType" class="float-left">Title:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", "", new { @id = "New_User_TitleType", @class = "TitleType required" })%></div>
            </div>
            <div class="row">
                <label for="New_User_OtherTitleType" class="float-left">Other Title (specify):</label>
                <div class="float-right"><%= Html.TextBox("TitleTypeOther", "", new { @id = "New_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Employment Type:</label>
                <div class="float-right">
                    <%= Html.RadioButton("EmploymentType", "Employee", new { @id = "New_User_EmploymentType_E", @class = "required radio" })%>
                    <label for="New_User_EmploymentType_E" class="inline-radio">Employee</label>
                    <%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "New_User_EmploymentType_C", @class = "required radio" })%>
                    <label for="New_User_EmploymentType_C" class="inline-radio">Contractor</label>
                </div>
            </div>
            <div class="row">
                <label for="New_User_DOB" class="float-left">DOB:</label>
                <div class="float-right"><input type="text" class="date-picker" name="DOB"  id="New_User_DOB" /></div>
            </div>
            <div class="row">
                <label for="New_User_SSN" class="float-left">SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", "", new { @id = "New_User_SSN", @class = "text digits input_wrapper", @maxlength = "9" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_User_CustomId" class="float-left">Agency Custom Employee Id:</label>
                <div class="float-right"><%= Html.TextBox("CustomId", "", new { @id = "New_User_CustomId", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_User_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_User_LocationId", @class = "BranchLocation" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressLine1", "", new { @id = "New_User_AddressLine1", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressLine2", "", new { @id = "New_User_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressCity", "", new { @id = "New_User_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", "", new { @id = "New_User_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Profile.AddressZipCode", "", new { @id = "New_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="New_User_HomePhoneArray1" class="float-left">Home Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_User_MobilePhoneArray1" class="float-left">Mobile Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_User_FaxPhoneArray1" class="float-left">Fax Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "New_User_FaxPhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "New_User_FaxPhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "New_User_FaxPhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right"><%= Html.RadioButton("Profile.Gender", "Female", false, new { @id = "New_User_GenderF", @class = "required radio" })%><label for="New_User_GenderF" class="inline-radio">Female</label><%= Html.RadioButton("Profile.Gender", "Male", false, new { @id = "New_User_GenderM", @class = "required radio" })%><label for="New_User_GenderM" class="inline-radio">Male</label></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Load Previous User</legend>
        <div>
            <%= Html.Users("PreviousUser", "", new { @id = "New_User_Previous" })%>
        </div>
    </fieldset>
    <%= Html.Partial("NewContent", new User()) %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="New_User_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" style="height:100px;" class="fill"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add User</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newuser');">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
<%  if (Model) { %>
    $("#newUserContent").html(U.MessageError("You have reached your maximum number of user accounts", "Please contact Axxess about upgrading your account."));
<%  } %>
    $(".permission").each(function() {
        if ($(this).attr("tooltip") != undefined) $(this).tooltip({
            track: true,
            showURL: false,
            top: 10,
            left: 10,
            extraClass: "calday",
            bodyHandler: function() { return $(this).attr("tooltip") }
        })
    })
     $("#New_User_Previous").change(function() {
     
            $("#New_User_Content").Load("User/NewContent", {
                userId: $("#New_User_Previous").val()
            });
        })
</script>