﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Schedule and Tasks | <%= Current.AgencyName %></span>
<div class="buttons">
    <ul class="float-right"><% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('PatientName','<%=sortParams %>');">Refresh</a></li>
        <li><a href="Export/ScheduleList" >Excel Export</a></li>
    </ul>
    <ul>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('PatientName','<%=sortParams %>');">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('VisitDate','<%=sortParams %>');">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('TaskName','<%=sortParams %>');">Group By Task</a></li>
    </ul>
</div>
<div class="clear"></div>
<div class="buttons">Note: This list shows you items/tasks dated 3 months into the past and 2 weeks into the future. To find older items, look in the Patient's Chart or Schedule Center.</div>
<div id="myScheduledTasksContentId"><% Html.RenderPartial("~/Views/User/ScheduleGrid.ascx"); %></div>