﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<span class="wintitle">List Users | <%= Current.AgencyName %></span>
<div class="fixed-grid-height">
<% using (Html.BeginForm("ActiveUsers", "Export", FormMethod.Post)) { %>
  <div id="ActiveUsers-GridContainer">
    <% Html.RenderPartial("ActiveContent", Model != null && Model.Count > 0 ? Model.Where(u => u.Status == (int)UserStatus.Active).ToList() : new List<User>()); %>
  </div>
<%} %>    
</div><div id="InactiveUsers-GridContainer" class="absolute-grid">
<% using (Html.BeginForm("InactiveUsers", "Export", FormMethod.Post)) { %>
     <% Html.RenderPartial("InActiveContent", Model != null && Model.Count > 0 ? Model.Where(u => u.Status == (int)UserStatus.Inactive).ToList() : new List<User>()); %>
<%} %>    
</div>
<script type="text/javascript">
    $("#List_User .t-grid-toolbar").html("");
    $("#List_UserInactive .t-grid-toolbar").html("");
    $("#List_User .t-grid-content").css({ 'height': '200px' });
    $("#List_UserInactive .t-grid-content").css({ 'height': 'auto' });
    var newUser = "", activeExport = "", inactiveExport = "";
<% var newUser = string.Empty; %>
<% var activeExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen) { %>
    newUser = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewUser(); return false;%22%3ENew User%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    activeExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExport to Excel (Active)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_User .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E" + newUser + "%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EActive Users%3C/div%3E" + activeExport + "%3C/div%3E"));
<% var inactiveExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    inactiveExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExport to Excel (Inactive)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_UserInactive .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EInactive Users%3C/div%3E" + inactiveExport + "%3C/div%3E"));
</script>
