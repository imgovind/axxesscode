﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<% 
   var inactiveAction = string.Empty;
   var visible = false;
   if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen)
   {
       visible = true;
       inactiveAction = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUser('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Activate('<#=Id#>');\">Activate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('<#=Id#>');\">Delete</a>";
   }    
 %>
<%= Html.Telerik().Grid(Model).Name("List_UserInactive").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20);
    columns.Bound(u => u.TitleType).Title("Title").Sortable(true).Width(15);
    columns.Bound(u => u.EmailAddress).Template(u => string.Format("<a href='mailto:{0}'>{0}</a>", u.EmailAddress)).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(20).Sortable(true);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(10);
    columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(10);
    columns.Bound(u => u.EmploymentType).Sortable(true).Width(10);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(8);
    columns.Bound(u => u.Profile.Gender).Title("Gender").Width(6).Sortable(true);
    columns.Bound(u => u.Comments).Template(u => string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"{0}\"> </a>", u.Comments)).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>").Title("").Sortable(false).Width(3);
    columns.Bound(u => u.Id).Width(15).Sortable(false).Template(u => visible ? string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUser('{0}');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Activate('{0}');\">Activate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('{0}');\">Delete</a>", u.Id) : string.Empty).ClientTemplate(inactiveAction).Visible(visible).Title("Action");
})
.ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound"))
//.DataBinding(dataBinding => dataBinding.Ajax().Select("List", "User", new { status = (int)UserStatus.Inactive }))
    .Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order =>
    {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "DisplayName")
        {
            if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
        }
        else if (sortName == "DisplayTitle")
        {
            if (sortDirection == "ASC") order.Add(o => o.DisplayTitle).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayTitle).Descending();
        }
        else if (sortName == "EmailAddress")
        {
            if (sortDirection == "ASC") order.Add(o => o.EmailAddress).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EmailAddress).Descending();
        }
        else if (sortName == "EmploymentType")
        {
            if (sortDirection == "ASC") order.Add(o => o.EmploymentType).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EmploymentType).Descending();
        }
        else if (sortName == "Gender")
        {
            if (sortDirection == "ASC") order.Add(o => o.Profile.Gender).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Profile.Gender).Descending();
        }
    }))
.Sortable()
.Scrollable(scrolling => scrolling.Enabled(true))
    %>
<script type="text/javascript">
    $("#List_UserInactive .t-grid-toolbar").html("");
    $("#List_UserInactive .t-grid-content").css({ 'height': 'auto' });
    var inactiveExport = "";
<% var inactiveExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    inactiveExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export (Inactive)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_UserInactive .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EInactive Users%3C/div%3E" + inactiveExport + "%3C/div%3E"));
$("#List_UserInactive div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "User.RebindInactiveUser('" + U.ParameterByName(link, 'List_UserInactive-orderBy') + "');");
    });
    Agency.ToolTip("#List_UserInactive");
</script>
