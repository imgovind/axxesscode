﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="main">
    <div class="wide-column"><label for="Edit_Rate_UserId" class="float-left">Choose existing rates: </label><div class="float-left"><%= Html.RatedUser("Rated_UserId", Model.AgencyLocationId.ToString(), true, "-- Select User --", new { @id = "Edit_Rate_UserId" })%></div><div class="buttons"><ul class="float-left"><li><a href="javascript:void(0);" onclick="User.LoadRate($('#Edit_Rate_UserId').val(),'<%=Model.Id %>');">Apply</a></li></ul></div></div>
    <div class="buttons">
        <ul class="align-center">
            <li><a href="javascript:void(0);" onclick="User.NewRate('<%= Model.Id %>');">Add Pay Rate</a></li>
        </ul>
    </div>
    <div class="clear"></div>
     <%= Html.Telerik().Grid<UserRate>().Name("List_User_Rates").HtmlAttributes(new { @style = "height:auto; position: relative;margin-bottom: 30px;" })
        .DataKeys(keys =>{ keys.Add(r => r.Id).RouteKey("Id");  })
        .Columns(columns =>
        {
            columns.Bound(e => e.DisciplineTaskName).Title("Task").Sortable(true);
            columns.Bound(e => e.InsuranceName).Title("Insurance").Sortable(true);
            columns.Bound(e => e.RateTypeName).Title("Rate Type").Width(150).Sortable(true);
            columns.Bound(e => e.Rate).Format("${0:#0.00}").Title("User Rate").Width(70).Sortable(true);
            columns.Bound(e => e.MileageRate).Format("${0:#0.00}").Title("Mileage Rate").Width(70).Sortable(true);
            columns.Bound(e => e.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"User.EditRate($('#Edit_User_Id').val(),'<#=Id#>', '<#=Insurance#>');\">Edit</a> | <a  href=\"javascript:void(0);\" onclick=\"User.DeleteRate($('#Edit_User_Id').val(),'<#=Id#>', '<#=Insurance#>');\">Delete</a> ").Title("Action").Width(105);
            
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Rates", "User", new { userId = Model.Id })).Sortable().Footer(false)%>  
</div>