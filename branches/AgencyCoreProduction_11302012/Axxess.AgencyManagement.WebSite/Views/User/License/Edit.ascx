<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LicenseItem>" %>
<% using (Html.BeginForm("UpdateLicenseItem", "User", FormMethod.Post, new { @id = "editLicenseItemForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_LicenseItem_Id" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_LicenseItem_UserId" })%>
<div class="wrapper main">
    <fieldset class="editmed">
        <legend>Edit License</legend>
        <div class="wide-column">
        <% if (Model.IsUser == "No") { %>
            <div class="row">
                <label for="Edit_LicenseItem_FirstName" class="float-left">First Name:</label>
                <div class="float-left"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_LicenseItem_FirstName", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LicenseItem_LastName" class="float-left">Last Name:</label>
                <div class="float-left"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_LicenseItem_LastName", @class = "text input_wrapper" })%></div>
            </div>
        <% } else { %>
            <div class="row">
                <label for="Edit_LicenseItem_DisplayName" class="float-left">License User:</label>
                <div class="float-left"><span id="Edit_LicenseItem_DisplayName" class="bigtext"><%= Model.DisplayName %></span></div>
            </div>
        <% } %>
            <div class="row">
                <label for="Edit_LicenseItem_Type" class="float-left">License Type:</label>
                <div class="float-left"><span><%= Model.LicenseType %></span></div>
            </div>
            <div class="row">
                <label for="Edit_LicenseItem_IssueDate" class="float-left">Issue Date:</label>
                <div class="float-left"><input type="text" class="date-picker" name="IssueDate" value="<%= Model.IssueDate.IsValid() ? Model.IssueDateFormatted : string.Empty %>" id="Edit_LicenseItem_IssueDate" /></div>
            </div>
            <div class="row">
                <label for="Edit_LicenseItem_ExpireDate" class="float-left">Expire Date:</label>
                <div class="float-left"><input type="text" class="date-picker" name="ExpireDate" value="<%= Model.ExpireDate.IsValid() ? Model.ExpireDateFormatted : string.Empty %>" id="Edit_LicenseItem_ExpireDate" /></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Update</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    if ($("#Edit_LicenseItem_StartDate").val() == "1/1/0001") $("#Edit_LicenseItem_StartDate").val("");
    if ($("#Edit_LicenseItem_ExpireDate").val() == "1/1/0001") $("#Edit_LicenseItem_ExpireDate").val("");
</script>