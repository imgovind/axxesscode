﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<ul>
    <li>
        <span class="firstname">First Name</span>
        <span class="lastname">Last Name</span>
        <span class="licensetype">License Type</span>
        <span class="issuedate">Issue Date</span>
        <span class="expiredate">Expire Date</span>
        <span class="isuser">Software User?</span>
         <% if(!Current.IsAgencyFrozen) { %><span class="action"></span><% } %>
    </li>
</ul>
<ol>
<% int i = 1;
   if (Model != null && Model.Count > 0) {
    foreach (var license in Model) { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
        <span class="firstname"><%= license.FirstName %></span>
        <span class="lastname"><%= license.LastName %></span>
        <span class="licensetype"><%= license.AssetUrl %></span>
        <span class="issuedate"><%= license.IssueDateFormatted.IsEqual("01/01/0001") ? string.Empty : license.IssueDateFormatted %></span>
        <span class="expiredate"><%= license.ExpireDateFormatted.IsEqual("01/01/0001") ? string.Empty : license.ExpireDateFormatted %></span>
        <span class="isuser"><%= license.IsUser %></span>
        <% if(!Current.IsAgencyFrozen) { %><span class="action"><a href="javascript:void(0);" onclick="LicenseManager.Edit('<%=license.Id %>', '<%= license.UserId %>');" >Edit</a> | <a href="javascript:void(0);" onclick="LicenseManager.Delete('<%=license.Id %>', '<%= license.UserId %>');">Delete</a></span><% } %>
    </li><%
        i++;
    }
} else { %>
    <li class="align-center"><span class="darkred">No Licenses found</span></li>
<% } %>
</ol>
<script type="text/javascript">
    $(".standard-chart ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>