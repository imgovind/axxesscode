﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<div class="wrapper main">
    <div class="buttons">
        <ul class="float-left">
        <% if(!Current.IsAgencyFrozen) { %>
            <li>
                <a href="javascript:void(0);" onclick="LicenseManager.Add();">Add Non-Software User License</a>
            </li>
            <% } %>
        </ul>
    </div>
    <div class="clear"></div>
    <div id="LicenseManager_List" class="standard-chart">
        <%  Html.RenderPartial("~/Views/User/License/List.ascx", Model); %>
    </div>
</div>

<script type="text/javascript">
    $("#window_licensemanager_content").css({
        "background-color": "#d6e5f3"
    });
</script>
