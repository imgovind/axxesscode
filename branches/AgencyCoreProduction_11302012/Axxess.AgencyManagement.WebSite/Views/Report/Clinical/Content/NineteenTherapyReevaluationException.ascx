﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeTherapyException>>" %>
<% string pagename = "ClinicalNineteenTherapyReevaluationException"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(70);
               columns.Bound(o => o.PatientName).Title("Patient");
               columns.Bound(o => o.EpisodeRange).Title("Episode").Sortable(false).ReadOnly();
               columns.Bound(o => o.ScheduledTherapy).Title("Scheduled").Width(90).Sortable(false).ReadOnly();
               columns.Bound(o => o.CompletedTherapy).Title("Completed").Width(90).Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeDay).Title("Day").Width(50).Sortable(false).ReadOnly();
               columns.Bound(o => o.PTEval).Title("PT Re-eval").Width(90).Sortable(false).ReadOnly();
               columns.Bound(o => o.STEval).Title("ST Re-eval").Width(90).Sortable(false).ReadOnly();
               columns.Bound(o => o.OTEval).Title("OT Re-eval").Width(90).Sortable(false).ReadOnly();
           })
                   //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty }))
                           .Sortable(sorting =>
                              sorting.SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                      if (sortName == "PatientIdNumber")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Descending();
                                          }

                                      }
                                      else if (sortName == "PatientName")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientName).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientName).Descending();
                                          }

                                      }
                                  })
                          )
                                   .Scrollable()
                                           .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','NineteenTherapyReevaluationExceptionContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", PatientId : \"" + $('#<%= pagename %>_Patient').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>