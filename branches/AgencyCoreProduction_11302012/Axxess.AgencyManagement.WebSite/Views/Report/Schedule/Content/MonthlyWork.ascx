﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "ScheduleMonthlyWork"; %>
        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Sortable(false).Title("Task");
         columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
         columns.Bound(p => p.ScheduleDate).Sortable(false).Title("Schedule Date").Width(110);
         columns.Bound(p => p.VisitDate).Sortable(false).Title("Visit Date").Width(100);
         })
        // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, userId = Guid.Empty, month = DateTime.Now.Month, year = DateTime.Now.Year }))
                             .Sortable(sorting =>
                                          sorting.SortMode(GridSortMode.SingleColumn)
                                              .OrderBy(order =>
                                              {
                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                  if (sortName == "PatientName")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.PatientName).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.PatientName).Descending();
                                                      }

                                                  }

                                              })
                                              )
                         .Scrollable().Footer(false)%>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','MonthlyWorkContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", UserId : \"" + $('#<%= pagename %>_UserId').val() + "\", Year : \"" + $('#<%= pagename %>_Year').val() + "\", Month : \"" + $('#<%= pagename %>_Month').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>

