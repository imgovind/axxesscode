﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AddressBookEntry>>" %>
<% string pagename = "PatientAddressList"; %>
  <%= Html.Telerik().Grid(Model).Name(pagename +"Grid").Columns(columns =>
           {
               columns.Bound(e => e.IdNumber).Title("MRN").Width(100);
               columns.Bound(e => e.Name).Width(140);
               columns.Bound(e => e.AddressFirstRow).Sortable(false).Title("Address");
               columns.Bound(e => e.AddressCity).Sortable(false).Title("City").Width(90);
               columns.Bound(e => e.AddressStateCode).Sortable(false).Title("State").Width(50);
               columns.Bound(e => e.AddressZipCode).Sortable(false).Title("Zip Code").Width(60);
               columns.Bound(e => e.PhoneHome).Sortable(false).Title("Home Phone").Width(110);
               columns.Bound(e => e.PhoneMobile).Sortable(false).Title("Mobile Phone").Width(110);
               columns.Bound(e => e.EmailAddress).Sortable(false);
           })
           //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty,  StatusId=1 }))
                 .Sortable(sorting =>
                                sorting.SortMode(GridSortMode.SingleColumn)
                                    .OrderBy(order =>
                                    {
                                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                        if (sortName == "Name")
                                        {
                                            if (sortDirection == "ASC")
                                            {
                                                order.Add(o => o.Name).Ascending();
                                            }
                                            else if (sortDirection == "DESC")
                                            {
                                                order.Add(o => o.Name).Descending();
                                            }
                                        }
                                        else if (sortName == "IdNumber")
                                        {
                                            if (sortDirection == "ASC")
                                            {
                                                order.Add(o => o.IdNumber).Ascending();
                                            }
                                            else if (sortDirection == "DESC")
                                            {
                                                order.Add(o => o.IdNumber).Descending();
                                            }
                                        }
                                       
                                    })
                            )
             .Scrollable()
               .Footer(false)
        %>
        <script type="text/javascript">
            $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
                var link = $(this).attr("href");
                $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PatientAddressListContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
            });
            $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
        </script>
