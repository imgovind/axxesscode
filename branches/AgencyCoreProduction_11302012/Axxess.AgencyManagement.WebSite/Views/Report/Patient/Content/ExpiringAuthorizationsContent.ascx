﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Authorization>>" %>
<% string pagename = "ExpiringAuthorizations"; %>

         <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.DisplayName).Title("Patient");
                columns.Bound(r => r.Status).Title("Status").Sortable(false);
                columns.Bound(r => r.Number1).Title("Number").Sortable(false);
                columns.Bound(r => r.StartDate).Format("{0:MM/dd/yyyy}").Title("Start Date");
                columns.Bound(r => r.EndDate).Format("{0:MM/dd/yyyy}").Title("End Date");
            })
                    // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { Status = 1, BranchCode = Guid.Empty }))
                              .Sortable(sorting =>
                                 sorting.SortMode(GridSortMode.SingleColumn)
                                     .OrderBy(order =>
                                     {
                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                         if (sortName == "DisplayName")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.DisplayName).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.DisplayName).Descending();
                                             }
                                         }
                                         else if (sortName == "StartDate")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.StartDate).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.StartDate).Descending();
                                             }
                                         }
                                         else if (sortName == "EndDate")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.EndDate).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.EndDate).Descending();
                                             }
                                         }
                                         
                                     }))
                                       .Scrollable()
                                                .Footer(false)
        %>
 
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ExpiringAuthorizationsContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
