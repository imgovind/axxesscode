﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "BillingBatch"; %>
<div class="wrapper">
    <fieldset>
        <legend>Billing Batch</legend>
        <div class="column">
              <div class="row"><label class="float-left">Claim Type:</label><div class="float-right"><%= Html.ClaimTypes("ClaimType", new { @id = string.Format("{0}_ClaimType", pagename) })%></div></div>
              <div class="row"><label class="float-left">Date:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="BatchDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_BatchDate" /></div></div>
        </div>
        <div class="column">
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','BillingBatchContent',{{ ClaimType: $('#{0}_ClaimType').val(),BatchDate: $('#{0}_BatchDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
           <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportBillingBatch", new { ClaimType = "ALL", BatchDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/BillingBatch", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
</script>
