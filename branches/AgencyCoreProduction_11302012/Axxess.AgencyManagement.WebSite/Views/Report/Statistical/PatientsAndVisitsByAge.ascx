﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patients And Visits By Age</legend>
        <div class="column">
            <div class="row"><label for="PatientsAndVisitsByAge_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "PatientsAndVisitsByAge_BranchId" })%></div></div>
            <div class="row"><label for="PatientsAndVisitsByAge_Year" class="float-left">Sample Year:</label><div class="float-right"><select id="PatientsAndVisitsByAge_Year" name="Year"><option value="2012" selected="selected">2012</option><option value="2011">2011</option><option value="2010">2010</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/PatientsAndVisitsByAgeReport', '#PatientsAndVisitsByAge_BranchId', '#PatientsAndVisitsByAge_Year');">Request Report</a></li></ul></div>
    </fieldset>
</div>
