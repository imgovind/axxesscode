﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("PPSVisitInformationReport", "Export", FormMethod.Post)) { %>
    <div class="wrapper">
        <fieldset>
            <legend>Visit Information Report (PPS Log)</legend>
            <div class="column">
                <div class="row"><label for="PPSVisitInformation_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "PPSVisitInformation_BranchId" })%></div></div>
                <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="PPSVisitInformation_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="PPSVisitInformation_EndDate" /></div></div>
            </div>
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate Report</a></li></ul></div>
        </fieldset>
    </div>
<%} %>
