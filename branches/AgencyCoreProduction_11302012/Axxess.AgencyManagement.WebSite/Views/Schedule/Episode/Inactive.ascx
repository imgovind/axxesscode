﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EpisodeLean>>" %>
<div id="InactiveEpisodesContent"  class="inactiveepisodes">
   <% Html.RenderPartial("Episode/InactiveGrid", Model); %>
</div>
