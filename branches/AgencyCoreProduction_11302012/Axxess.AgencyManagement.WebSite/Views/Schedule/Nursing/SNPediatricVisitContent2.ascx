﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Vital Signs</th>
            <th>Integ</th>
            <th>Cardio</th>
            <th>Musc</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
            <td>
                <label class="strong float-left">Turgor:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegTurgor0' name='{0}_IntegTurgor' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegTurgor").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegTurgor0">Elastic</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegTurgor1' name='{0}_IntegTurgor' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegTurgor").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegTurgor1">Inelastic</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-left">Skin Temp:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinTemp0' name='{0}_IntegSkinTemp' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinTemp").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinTemp0">Warm</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinTemp1' name='{0}_IntegSkinTemp' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinTemp").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinTemp1">Cool</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-left">Skin Condition:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinCondition0' name='{0}_IntegSkinCondition' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinCondition").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinCondition0">Diaphoretic</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinCondition1' name='{0}_IntegSkinCondition' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinCondition").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinCondition1">Dry</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinCondition2' name='{0}_IntegSkinCondition' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinCondition").Equals("2").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinCondition2">Normal</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinCondition3' name='{0}_IntegSkinCondition' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinCondition").Equals("3").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinCondition3">Rash</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegSkinCondition4' name='{0}_IntegSkinCondition' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegSkinCondition").Equals("4").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegSkinCondition4">Excoriation</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-left">Edema:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegEdema0' name='{0}_IntegEdema' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegEdema").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegEdema0">None</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_IntegEdema1' name='{0}_IntegEdema' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IntegEdema").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_IntegEdema1">Present</label>
                            </div>
                            <div class="extra" id="<%= Model.Type %>_IntegEdema1More">
                                <%= Html.TextBox(Model.Type + "_IntegEdemaPresent", data.AnswerOrEmptyString("IntegEdemaPresent"), new { @id = Model.Type + "_IntegEdemaPresent", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_IntegIncisions" class="float-left">Incisions/Dressings:</label>
                <%= Html.TextArea(Model.Type + "_IntegIncisions", data.AnswerOrEmptyString("IntegIncisions"), new { @id = Model.Type + "_IntegIncisions", @class = "fill"})%>
                <label for="<%= Model.Type %>_IntegComment" class="strong">Comments:</label>  
                <%= Html.TextArea(Model.Type + "_IntegComment", data.AnswerOrEmptyString("IntegComment"), new { @id = Model.Type + "_IntegComment", @class = "fill" })%>
                          
            </td>
            <td>
                <label class="strong float-left">Rhythm:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiorhythm0' name='{0}_Cardiorhythm' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Cardiorhythm").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_Cardiorhythm0">Regular</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiorhythm1' name='{0}_Cardiorhythm' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Cardiorhythm").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_Cardiorhythm1">Irregular</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-left">Rate:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioRate0' name='{0}_CardioRate' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioRate").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioRate0">Strong</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioRate1' name='{0}_CardioRate' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioRate").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioRate1">Weak</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-left">Capillary Refill:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioCapillaryRefill0' name='{0}_CardioCapillaryRefill' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioCapillaryRefill").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioCapillaryRefill0">< 1 sec</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioCapillaryRefill1' name='{0}_CardioCapillaryRefill' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioCapillaryRefill").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioCapillaryRefill1">< 3 sec</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioCapillaryRefill2' name='{0}_CardioCapillaryRefill' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioCapillaryRefill").Equals("2").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioCapillaryRefill2">< 6 sec</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong float-center">Pulse:</label>
                <div class="clear"></div>
                <label class="float-left">Femoral:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioFemoral0' name='{0}_CardioFemoral' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioFemoral").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioFemoral0">L</label>
                            </div>
                            <div class="extra" id="_CardioFemoral0More">
                                <%= Html.TextBox(Model.Type + "_CardioFemoralL", data.AnswerOrEmptyString("CardioFemoralL"), new { @id = Model.Type + "_CardioFemoralL", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioFemoral1' name='{0}_CardioFemoral' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioFemoral").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioFemoral1">R</label>
                            </div>
                            <div class="extra" id="_CardioFemoral1More">
                                <%= Html.TextBox(Model.Type + "_CardioFemoralR", data.AnswerOrEmptyString("CardioFemoralR"), new { @id = Model.Type + "_CardioFemoralR", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Radial:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioRadial0' name='{0}_CardioRadial' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioRadial").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioRadial0">L</label>
                            </div>
                            <div class="extra" id="_CardioRadial0More">
                                <%= Html.TextBox(Model.Type + "_CardioRadialL", data.AnswerOrEmptyString("CardioRadialL"), new { @id = Model.Type + "_CardioRadialL", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioRadial1' name='{0}_CardioRadial' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioRadial").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioRadial1">R</label>
                            </div>
                            <div class="extra" id="_CardioRadial1More">
                                <%= Html.TextBox(Model.Type + "_CardioRadialR", data.AnswerOrEmptyString("CardioRadialR"), new { @id = Model.Type + "_CardioRadialR", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Pedal:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioPedal0' name='{0}_CardioPedal' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioPedal").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioPedal0">L</label>
                            </div>
                            <div class="extra" id="_CardioPedal0More">
                                <%= Html.TextBox(Model.Type + "_CardioPedalL", data.AnswerOrEmptyString("CardioPedalL"), new { @id = Model.Type + "_CardioPedalL", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioPedal1' name='{0}_CardioPedal' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioPedal").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioPedal1">R</label>
                            </div>
                            <div class="extra" id="_CardioPedal1More">
                                <%= Html.TextBox(Model.Type + "_CardioPedalR", data.AnswerOrEmptyString("CardioPedalR"), new { @id = Model.Type + "_CardioPedalR", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioPulse0' name='{0}_CardioPulse' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioPulse").Equals("0").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioPulse0">Cyanosis(general)</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioPulse1' name='{0}_CardioPulse' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioPulse").Equals("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioPulse1">Circumoral cyanosis</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CardioPulse2' name='{0}_CardioPulse' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CardioPulse").Equals("2").ToChecked())%>
                                <label for="<%= Model.Type %>_CardioPulse2">Edema</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_CardioPulseEdema", data.AnswerOrEmptyString("CardioPulseEdema"), new { @id = Model.Type + "_CardioPulseEdema", @class = "oe", @maxlength = "20" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="strong">Comments:</label>
                <%= Html.TextArea(Model.Type + "_CardioComments", data.AnswerOrEmptyString("CardioComments"), new { @id = Model.Type + "_CardioComments", @class = "fill" })%>
                            
            </td>
            <td>
                <label class="float-left">Muscle Tone:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone0' name='{0}_MUSCTone' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone0">Good</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone1' name='{0}_MUSCTone' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone1">Fair</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone2' name='{0}_MUSCTone' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone2">Poor</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone3' name='{0}_MUSCTone' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone3">Relaxed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone4' name='{0}_MUSCTone' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone4">Flaccid</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCTone5' name='{0}_MUSCTone' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCTone").Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCTone5">Spastic</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Extremity Movement:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCMovement0' name='{0}_MUSCMovement' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCMovement").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCMovement0">Normal</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MUSCMovement1' name='{0}_MUSCMovement' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MUSCMovement").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_MUSCMovement1">Abnormal</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_MUSCMovementAbnormal", data.AnswerOrEmptyString("MUSCMovementAbnormal"), new { @id = Model.Type + "_MUSCMovementAbnormal", @class = "oe" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Comments/Problems:</label>
                <%= Html.TextArea(Model.Type + "_MUSCComments", data.AnswerOrEmptyString("MUSCComments"), new { @id = Model.Type + "_MUSCComments", @class = "fill" })%>
                
            </td>
        </tr>
        <tr>
            <th>GI</th>
            <th>GU</th>
            <th colspan="2">Pulm</th>
        </tr>
        <tr>
            <td>
                <label class="float-left">Abdomen</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen0' name='{0}_GIAbdomen' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen0">Soft</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen1' name='{0}_GIAbdomen' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen1">Flat</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen2' name='{0}_GIAbdomen' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen2">Firm</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen3' name='{0}_GIAbdomen' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen3">Tender</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen4' name='{0}_GIAbdomen' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen4">Rigid</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen5' name='{0}_GIAbdomen' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen5">Distended</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GIAbdomen6' name='{0}_GIAbdomen' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GIAbdomen").Contains("6").ToChecked())%>
                                <label for="<%= Model.Type %>_GIAbdomen6">Cramping</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Bowel sounds/patterns:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GIBowel", data.AnswerOrEmptyString("GIBowel"), new { @id = Model.Type + "_GIBowel", @class = "oe", @maxlength = "30" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Comments:</label>
                <%= Html.TextArea(Model.Type + "_GIComments", data.AnswerOrEmptyString("GIComments"), new { @id = Model.Type + "_GIComments", @class = "fill"})%>
                
            </td>
            <td>
                <label class="float-left">Catheter:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GUCatheter0' name='{0}_GUCatheter' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GUCatheter").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_GUCatheter0">Foley</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GUCatheter1' name='{0}_GUCatheter' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GUCatheter").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_GUCatheter0">Other:</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_GUCatheterOther", data.AnswerOrEmptyString("GUCatheterOther"), new { @id = Model.Type + "_GUCatheterOther", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Type/Size:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GUTypeSize", data.AnswerOrEmptyString("GUTypeSize"), new { @id = Model.Type + "_GUTypeSize", @class = "oe", @maxlength = "30" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Last changed:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GULastChanged", data.AnswerOrEmptyString("GULastChanged"), new { @id = Model.Type + "_GULastChanged", @class = "oe", @maxlength = "30" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Ostomy</label>
                <div class="clear"></div>
                <label >Type/Appliance:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GUTypeAppliance", data.AnswerOrEmptyString("GUTypeAppliance"), new { @id = Model.Type + "_GUTypeAppliance", @class = "oe" })%>
                </div>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GUOstomy0' name='{0}_GUOstomy' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GUOstomy").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_GUOstomy0">Diapers</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GUOstomy1' name='{0}_GUOstomy' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GUOstomy").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_GUOstomy1">Training pants</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GUOstomy2' name='{0}_GUOstomy' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GUOstomy").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_GUOstomy2">Toilet trained</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Comments</label>
                <%= Html.TextArea(Model.Type + "_GUComments", data.AnswerOrEmptyString("GUComments"), new { @id = Model.Type + "_GUComments", @class = "fill" })%>
                
            </td>
            <td colspan="2">
                <label class="float-left">RESP:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmResp0' name='{0}_PulmResp' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmResp").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmResp0">Diminished</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_PulmRespDiminished", data.AnswerOrEmptyString("PulmRespDiminished"), new { @id = Model.Type + "_PulmRespDiminished", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmResp1' name='{0}_PulmResp' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmResp").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmResp1">Wheezes</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_PulmRespWheezes", data.AnswerOrEmptyString("PulmRespWheezes"), new { @id = Model.Type + "_PulmRespWheezes", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmResp2' name='{0}_PulmResp' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmResp").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmResp2">Crackles</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_PulmRespCrackles", data.AnswerOrEmptyString("PulmRespCrackles"), new { @id = Model.Type + "_PulmRespCrackles", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Cough:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough0' name='{0}_PulmCough' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough0">Non-productive</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough1' name='{0}_PulmCough' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough0">Productive</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_PulmCoughProductive", data.AnswerOrEmptyString("PulmCoughProductive"), new { @id = Model.Type + "_PulmCoughProductive", @class = "oe", @maxlength = "30" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough2' name='{0}_PulmCough' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough2">Retraction</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough3' name='{0}_PulmCough' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough3">Nasal flare</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough4' name='{0}_PulmCough' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough4">Grunting</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough5' name='{0}_PulmCough' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough5">Cyanosis</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmCough6' name='{0}_PulmCough' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmCough").Contains("6").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmCough6">Paradoxical excursions</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Apnea monitor:Settings:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_PulmCoughApnea", data.AnswerOrEmptyString("PulmCoughApnea"), new { @id = Model.Type + "_PulmCoughApnea", @class = "oe", @maxlength = "30" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Oxygen Use:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUse0' name='{0}_PulmOxygenUse' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUse").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUse0">Yes</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUse1' name='{0}_PulmOxygenUse' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUse").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUse1">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUse2' name='{0}_PulmOxygenUse' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUse").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUse2">Liter/min</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_PulmOxygenUseLiter", data.AnswerOrEmptyString("PulmOxygenUseLiter"), new { @id = Model.Type + "_PulmOxygenUseLiter", @class = "oe", @maxlength = "30" })%>                           
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Via:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUseVia0' name='{0}_PulmOxygenUseVia' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUseVia").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUseVia0">Nasal cannula</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUseVia1' name='{0}_PulmOxygenUseVia' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUseVia").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUseVia1">Tracheostomy</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmOxygenUseVia2' name='{0}_PulmOxygenUseVia' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmOxygenUseVia").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmOxygenUseVia2">Mask</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Pulmonary treatments:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmPulmonary0' name='{0}_PulmPulmonary' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmPulmonary").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmPulmonary0">Nebulizers</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmPulmonary1' name='{0}_PulmPulmonary' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmPulmonary").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmPulmonary1">Aerosol</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PulmPulmonary2' name='{0}_PulmPulmonary' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PulmPulmonary").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PulmPulmonary2">Clapping & postural drainage</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Pain</th>
        </tr>
        <tr>
            
            <td valign="top">
                <label class="strong float-center">Under 3 years of age</label>
                <div class="clear"></div>
                <label class="float-left">Facial Expression</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINFacialExpression0' name='{0}_PAINFacialExpression' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINFacialExpression").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINFacialExpression0">Relaxed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINFacialExpression1' name='{0}_PAINFacialExpression' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINFacialExpression").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINFacialExpression1">Grimace</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Cry:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINCry0' name='{0}_PAINCry' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINCry").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINCry0">No cry</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINCry1' name='{0}_PAINCry' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINCry").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINCry1">Whimper</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINCry2' name='{0}_PAINCry' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINCry").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINCry2">Vigorous</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Breathing patterns:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINBreathing0' name='{0}_PAINBreathing' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINBreathing").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINBreathing0">Relaxed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINBreathing1' name='{0}_PAINBreathing' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINBreathing").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINBreathing1">Changed</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Arms:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINArms0' name='{0}_PAINArms' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINArms").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINArms0">Relaxed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINArms1' name='{0}_PAINArms' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINArms").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINArms1">Flexed/extended</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Legs:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINLegs0' name='{0}_PAINLegs' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINLegs").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINLegs0">Relaxed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINLegs1' name='{0}_PAINLegs' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINLegs").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINLegs1">Flexed/extended</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">State of Arousal:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINState0' name='{0}_PAINState' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINState").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINState0">Sleeping/awake</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINState1' name='{0}_PAINState' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINState").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINState1">Fussy</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <label class="strong float-center">Over 3 years of age</label>
                <div class="clear"></div>
                <label class="float-left">Pain Level</label>
                <div class="float-right">
                    <%  var genericPainLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", ""));%>
                    <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel", @class = "oe" }) %>
                </div>
                
                <div class="clear"></div>
                <label class="float-left">Frequency:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_PAINFrequency", data.AnswerOrEmptyString("PAINFrequency"), new { @id = Model.Type + "_PAINFrequency", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Location:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_PAINLocation", data.AnswerOrEmptyString("PAINLocation"), new { @id = Model.Type + "_PAINLocation", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Pain relief measures:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_PAINReliefMeasures", data.AnswerOrEmptyString("PAINReliefMeasures"), new { @id = Model.Type + "_PAINReliefMeasures", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Effective:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINEffective0' name='{0}_PAINEffective' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINEffective").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINEffective0">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_PAINEffective1' name='{0}_PAINEffective' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PAINEffective").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PAINEffective1">Yes</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Comments:</label>
                <%= Html.TextArea(Model.Type + "_PAINComments", data.AnswerOrEmptyString("PAINComments"), new { @id = Model.Type + "_PAINComments", @class = "fill" })%>
                
            </td>
            <td colspan="2">
                <div class="row">
                    <img src="/Images/painscale.png" alt="Pain Scale Image" /><br />
                    <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <label class="float-left">Significant Clinical Findings Relative to Diagnosis and Care Plan:</label>
                <div class="clear" />
                <%= Html.Templates(Model.Type + "_GenericCarePlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCarePlan" })%>
                <%= Html.TextArea(Model.Type + "_GenericCarePlan", data.AnswerOrEmptyString("GenericCarePlan"),8,20, new { @id = Model.Type + "_GenericCarePlan", @class = "fill" })%>
                
            </td>
            <td>
                <label class="float-left">Narrative:</label><div class="clear" />
                <%= Html.Templates(Model.Type + "_NarrativeTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Narrative" })%>
                <%= Html.TextArea(Model.Type + "_Narrative", data.AnswerOrEmptyString("Narrative"), 8, 20, new { @id = Model.Type + "_Narrative", @class = "fill" })%>
                
            </td>
            <td>
                <label class="float-left">Skilled Intervention or training provided:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkilledInterventionProvided0' name='{0}_SkilledInterventionProvided' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkilledInterventionProvided").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_SkilledInterventionProvided0">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkilledInterventionProvided1' name='{0}_SkilledInterventionProvided' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkilledInterventionProvided").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_SkilledInterventionProvided1">Yes</label>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <label class="float-left">Explain:</label>
                <%= Html.TextArea(Model.Type + "_SkilledInterventionExplain", data.AnswerOrEmptyString("SkilledInterventionExplain"),6,20, new { @id = Model.Type + "_SkilledInterventionExplain", @class = "fill" })%>
                
            </td>
            <td>
                <label class="float-left">Drug:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GenericDrug", data.AnswerOrEmptyString("GenericDrug"), new { @id = Model.Type + "_GenericDrug", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Dose:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GenericDose", data.AnswerOrEmptyString("GenericDose"), new { @id = Model.Type + "_GenericDose", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Route:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GenericRoute", data.AnswerOrEmptyString("GenericRoute"), new { @id = Model.Type + "_GenericRoute", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Time:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_GenericDrugTime", data.AnswerOrEmptyString("GenericDrugTime"), new { @id = Model.Type + "_GenericDrugTime", @class = "time-picker" })%>
                </div>
                <div class="clear"></div>
                <label class="float-left">Case supervisory visit made:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SupervisoryVisit0' name='{0}_SupervisoryVisit' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SupervisoryVisit").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_SupervisoryVisit0">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SupervisoryVisit1' name='{0}_SupervisoryVisit' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SupervisoryVisit").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_SupervisoryVisit1">Yes</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
  
<script type="text/javascript">
    U.HideOptions();
    U.ChangeToRadio("<%= Model.Type %>_IntegTurgor");
    U.ChangeToRadio("<%= Model.Type %>_IntegSkinTemp");
    U.ChangeToRadio("<%= Model.Type %>_IntegSkinCondition");
    U.ChangeToRadio("<%= Model.Type %>_GIAbdomen");
    U.ChangeToRadio("<%= Model.Type %>_Cardiorhythm");
    U.ChangeToRadio("<%= Model.Type %>_CardioRate");
    U.ChangeToRadio("<%= Model.Type %>_CardioCapillaryRefill");
    U.ChangeToRadio("<%= Model.Type %>_PulmOxygenUse"); 
    U.ChangeToRadio("<%= Model.Type %>_PAINEffective");
    U.ChangeToRadio("<%= Model.Type %>_SkilledInterventionProvided");
    U.ChangeToRadio("<%= Model.Type %>_SupervisoryVisit");
</script>