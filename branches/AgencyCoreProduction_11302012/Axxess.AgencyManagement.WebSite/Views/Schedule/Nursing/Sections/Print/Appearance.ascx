﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% if (data.AnswerOrEmptyString("GenericIsAppearanceApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsAppearanceApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Appearance"
<%}else{ %>
printview.span("Facial Expressions",true) + 
printview.col(2,
    printview.span("Sad:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial1", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Expressionless:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial2", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Hostile:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial3", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Worried:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial4", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Avoids Gaze:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial5", possibleAnswers) %>")) +
printview.span("Dress",true) + 
printview.col(2,
    printview.span("Meticulous:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress1", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Clothing, Hygiene Poor:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress2", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Eccentric:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress3", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Seductive:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress4", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Exposed:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress5", possibleAnswers) %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericAppearanceComment").Clean()%>"),
    "Appearance"
<% } %>
