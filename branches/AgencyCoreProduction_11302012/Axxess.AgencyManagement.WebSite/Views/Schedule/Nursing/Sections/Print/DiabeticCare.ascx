﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isDiabeticCareApplied = data.AnswerArray("GenericIsDiabeticCareApplied"); %>
<% if (data.AnswerOrEmptyString("GenericIsDiabeticCareApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsDiabeticCareApplied").ToString() == "1")
   {%>
printview.checkbox("N/A",true),"Diabetic Care"
<%}else{ %>
<%  string[] diabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
<%  string[] diabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
<%  string[] genericHyperglycemia = data.AnswerArray("GenericDiabeticCareHyperglycemia"); %>
<%  string[] genericHypoglycemia = data.AnswerArray("GenericDiabeticCareHypoglycemia"); %>
printview.span("Blood Sugar:",true) +
printview.col(2,
    printview.span("AM:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarAMLevelText").IsNotNullOrEmpty() ? data.AnswerOrEmptyString("GenericBloodSugarAMLevelText").Clean() + " mg/dl " : string.Empty %><%= data.AnswerOrEmptyString("GenericBloodSugarAMLevel").Equals("0") ? string.Empty : data.AnswerOrEmptyString("GenericBloodSugarAMLevel").Clean() %>") +

    printview.span("Noon:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarLevelText").IsNotNullOrEmpty() ? data.AnswerOrEmptyString("GenericBloodSugarLevelText").Clean() + " mg/dl " : string.Empty %><%= data.AnswerOrEmptyString("GenericBloodSugarLevel").Equals("0") ? string.Empty : data.AnswerOrEmptyString("GenericBloodSugarLevel").Clean() %>") +

    printview.span("PM:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarPMLevelText").IsNotNullOrEmpty() ? data.AnswerOrEmptyString("GenericBloodSugarPMLevelText").Clean() + " mg/dl " : string.Empty %><%= data.AnswerOrEmptyString("GenericBloodSugarPMLevel").Equals("0") ? string.Empty : data.AnswerOrEmptyString("GenericBloodSugarPMLevel").Clean() %>") +

    printview.span("HS:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarHSLevelText").IsNotNullOrEmpty() ? data.AnswerOrEmptyString("GenericBloodSugarHSLevelText").Clean() + " mg/dl " : string.Empty %><%= data.AnswerOrEmptyString("GenericBloodSugarHSLevel").Equals("0") ? string.Empty : data.AnswerOrEmptyString("GenericBloodSugarHSLevel").Clean() %>") +

    printview.span("Performed by:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarCheckedBy").Clean() %>")) +
printview.col(3,
    printview.span("Site:<%= data.AnswerOrEmptyString("GenericBloodSugarSiteText").Clean() %>",true) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Left").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Right").ToString().ToLower() %>))+
printview.span("Diabetic Management:",true) +
printview.col(2,
    printview.checkbox("Diet",<%= diabeticCareDiabeticManagement.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Oral Hypogly.",<%= diabeticCareDiabeticManagement.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Exercise",<%= diabeticCareDiabeticManagement.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Insulin",<%= diabeticCareDiabeticManagement.Contains("4").ToString().ToLower() %>)) +
printview.span("Insulin Administered by:",true) +
printview.col(5,
    printview.checkbox("N/A",<%= diabeticCareInsulinAdministeredby.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Pt",<%= diabeticCareInsulinAdministeredby.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("CG",<%= diabeticCareInsulinAdministeredby.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("SN",<%= diabeticCareInsulinAdministeredby.Contains("4").ToString().ToLower() %>)) +
printview.span("S&#38;S of Hyperglycemia:",true) +
printview.col(2,
    printview.checkbox("Fatigue",<%= genericHyperglycemia.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Blurred Vision",<%= genericHyperglycemia.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Polydipsia",<%= genericHyperglycemia.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Polyuria",<%= genericHyperglycemia.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Polyphagia",<%= genericHyperglycemia.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Other <%=genericHyperglycemia.Contains("6")?" "+ data.AnswerOrEmptyString("GenericDiabeticCareHyperglycemiaOther").Clean():string.Empty %>",<%= genericHyperglycemia.Contains("6").ToString().ToLower() %>)) +
printview.span("S&#38;S of Hypoglycemia:",true) +
printview.col(2,
    printview.checkbox("Anxious",<%= genericHypoglycemia.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Dizziness",<%= genericHypoglycemia.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Fatigue",<%= genericHypoglycemia.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Perspiration",<%= genericHypoglycemia.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Weakness",<%= genericHypoglycemia.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Other <%=genericHypoglycemia.Contains("6")?" "+ data.AnswerOrEmptyString("GenericDiabeticCareHypoglycemiaOther").Clean():string.Empty %>",<%= genericHypoglycemia.Contains("6").ToString().ToLower() %>)) +
<%  if (data.AnswerOrEmptyString("GenericInsulinType1").IsNotNullOrEmpty())
    { %>
printview.span("Insulin:",true) +
printview.col(4,
    printview.span("Type",true) +
    printview.span("Dose",true) +
    printview.span("Site",true) +
    printview.span("Route",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinType1").Clean()%>",0,1) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinDose1").Clean()%>",0,1) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinSite1").Clean()%>",0,1) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinRoute1").Clean()%>",0,1) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinType2").Clean()%>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinDose2").Clean()%>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinSite2").Clean()%>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericInsulinRoute2").Clean()%>")) +
<%  } %>
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericDiabeticCareComment").Clean() %>"),

"Diabetic Care"

<%} %>
