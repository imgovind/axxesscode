﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>SN Psychiatric Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "SN Psychiatric Visit" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.Patient.LastName.Clean()%>, <%= Model.Patient.FirstName.Clean()%> <%= Model.Patient.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.Patient.DOBFormatted.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.Patient.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.StartDate.ToShortDateString().ToZeroFilled(), Model.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PainProfile.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MentalStatus.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PatientFamilyTeachings.ascx", Model); %>);
    
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MoodAffect.ascx", Model); %>, 4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/ADL.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Communication.ascx", Model); %>);
    
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/CareCoordination.ascx", Model); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/CarePlan.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/DischargePlanning.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Rapport.ascx", Model); %>);
   
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/NutritionStatus.ascx", Model); %>,2);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/GIBowelFunctions.ascx", Model); %>);
    
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Supervisory.ascx", Model); %>,2);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Response.ascx", Model); %>);
   
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PsychiatricInterventions.ascx", Model); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Goals.ascx", Model); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/RehabPotential.ascx", Model); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Narrative.ascx", Model); %>);
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericPhysicianCommunication").Clean() %>", false), "Physician Communication");
    
    <%  if (Model.IsWoundCareExist) { %>
        <%  for (int i = 1; i < 6; i++) { %>
            <%  if (Model.WoundCare.ContainsKey("GenericLocation" + i) && Model.WoundCare["GenericLocation" + i].Answer.IsNotNullOrEmpty()) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Location:",true) +
                printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericLocation" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Onset Date:",true) +
                printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericOnsetDate" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Wound Type:",true) +
                printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericWoundType" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Pressure Ulcer Stage:",true) +
                printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericPressureUlcerStage" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Measurements:",true) +
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericMeasurementLength" + i).Clean()%> cm",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Width:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericMeasurementWidth" + i).Clean()%> cm",false,1)) +
                printview.col(2,
                    printview.span("Depth:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericMeasurementDepth" + i).Clean()%> cm",false,1))) +
            printview.col(2,
                printview.span("Wound Bed:",true) +
                printview.col(2,
                    printview.span("Granulation %:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericWoundBedGranulation" + i).Clean()%>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Slough %:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericWoundBedSlough" + i).Clean()%>",false,1)) +
                printview.col(2,
                    printview.span("Eschar %:") +
                    printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericWoundBedEschar" + i).Clean()%>",false,1))) +
            printview.col(2,
                printview.span("<strong>Surrounding Tissue</strong>: <%= Model.WoundCare.AnswerOrDefault("GenericSurroundingTissue" + i, "<span class='blank' style='width:4.9em !important;'></span>").Clean()%>") +
                printview.span("<strong>Drainage</strong>: <%= Model.WoundCare.AnswerOrDefault("GenericDrainage" + i, "<span class='blank' style='width:9.9em !important;'></span>").Clean()%>")) +
            printview.col(2,
                printview.span("<strong>Drainage Amount</strong>: <%= Model.WoundCare.AnswerOrDefault("GenericDrainageAmount" + i, "<span class='blank' style='width:5.8em !important;'></span>").Clean() %>") +
                printview.span("<strong>Odor</strong>: <%= Model.WoundCare.AnswerOrDefault("GenericOdor" + i, "<span class='blank' style='width:11.8em !important;'></span>").Clean()%>"))) +
        printview.col(3,
            printview.col(3,
                printview.span("Tunneling:",true) +
                printview.span("Length: <%= Model.WoundCare.AnswerOrDefault("GenericTunnelingLength" + i, "<span class='blank' style='width:3.2em !important;'></span>").Clean()%> cm") +
                printview.span("Time: <%= Model.WoundCare.AnswerOrDefault("GenericTunnelingTime" + i, "<span class='blank' style='width:4.4em !important;'></span>").Clean()%>")) +
            printview.col(3,
                printview.span("Undermining:",true) +
                printview.span("Length: <%= Model.WoundCare.AnswerOrDefault("GenericUnderminingLength" + i, "<span class='blank' style='width:3.2em !important;'></span>").Clean()%>") +
                printview.span("Time: <%= Model.WoundCare.AnswerOrDefault("GenericUnderminingTimeFrom" + i, "<span class='blank' style='width:2em !important;'></span>").Clean()%> to <%= Model.WoundCare.AnswerOrDefault("GenericUnderminingTimeTo" + i, "<span class='blank' style='width:2em !important;'></span>").Clean()%> o&#8217;clock")) +
            printview.col(3,
                printview.span("Device:",true) +
                printview.span("Type: <%= Model.WoundCare.AnswerOrDefault("GenericDeviceType" + i, "<span class='blank' style='width:4em !important;'></span>").Clean()%>") +
                printview.span("Setting: <%= Model.WoundCare.AnswerOrDefault("GenericDeviceSetting" + i, "<span class='blank' style='width:3em !important;'></span>").Clean() %>"))),
        "Wound <%= i %>",1);
            <%  } %>
        <%  } %>
    printview.addsection(
        printview.span("Treatment Performed:",true) +
        printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericTreatmentPerformed").Clean() %>",false,2) +
        printview.span("Narrative:",true) +
        printview.span("<%= Model.WoundCare.AnswerOrEmptyString("GenericNarrative").Clean() %>",false,2));
    <%  } %>
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>