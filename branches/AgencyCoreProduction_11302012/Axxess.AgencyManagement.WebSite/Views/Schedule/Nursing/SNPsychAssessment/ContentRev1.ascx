﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <%--<tr><td colspan="4">Check WNL for Patient if everything in that section is WNL for Patient</td></tr>--%>
        <tr>
            <th>General Appearance &#8212;
                <%  string[] isAppearanceApplied = data.AnswerArray("GenericIsAppearanceApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsAppearanceApplied", string.Empty, new { @id = Model.Type + "_GenericIsAppearanceAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsAppearanceApplied1' name='{0}_GenericIsAppearanceApplied' value='1' type='checkbox' {1} />", Model.Type, isAppearanceApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsAppearanceApplied1">WNL for Patient</label></th>
            <th>Motor Activity &#8212;
                <%  string[] isMotorActivityApplied = data.AnswerArray("GenericIsMotorActivityApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsMotorActivityApplied", string.Empty, new { @id = Model.Type + "_GenericIsMotorActivityAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsMotorActivityApplied1' name='{0}_GenericIsMotorActivityApplied' value='1' type='checkbox' {1} />", Model.Type, isMotorActivityApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsMotorActivityApplied1">WNL for Patient</label></th>
            <th>Speech &#8212;
                <%  string[] isSpeechApplied = data.AnswerArray("GenericIsSpeechApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsSpeechApplied", string.Empty, new { @id = Model.Type + "_GenericIsSpeechAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsSpeechApplied1' name='{0}_GenericIsSpeechApplied' value='1' type='checkbox' {1} />", Model.Type, isSpeechApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsSpeechApplied1">WNL for Patient</label></th>
            <th>Flow of Thought &#8212;
                <%  string[] isFlowOfThoughtApplied = data.AnswerArray("GenericIsFlowOfThoughtApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsFlowOfThoughtApplied", string.Empty, new { @id = Model.Type + "_GenericIsFlowOfThoughtAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsFlowOfThoughtApplied1' name='{0}_GenericIsFlowOfThoughtApplied' value='1' type='checkbox' {1} />", Model.Type, isFlowOfThoughtApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsFlowOfThoughtApplied1">WNL for Patient</label></th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Appearance.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/MotorActivity.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Speech.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/FlowOfThought.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Mood and Affect &#8212;
                <%  string[] isMoodAffectAssessmentApplied = data.AnswerArray("GenericIsMoodAffectAssessmentApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsMoodAffectAssessmentApplied", string.Empty, new { @id = Model.Type + "_GenericIsMoodAffectAssessmentAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsMoodAffectAssessmentApplied1' name='{0}_GenericIsMoodAffectAssessmentApplied' value='1' type='checkbox' {1} />", Model.Type, isMoodAffectAssessmentApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsMoodAffectAssessmentApplied1">WNL for Patient</label></th>
            <th>Sensorium &#8212;
                <%  string[] isSensoriumApplied = data.AnswerArray("GenericIsSensoriumApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsSensoriumApplied", string.Empty, new { @id = Model.Type + "_GenericIsSensoriumAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsSensoriumApplied1' name='{0}_GenericIsSensoriumApplied' value='1' type='checkbox' {1} />", Model.Type, isSensoriumApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsSensoriumApplied1">WNL for Patient</label></th>
            <th>Intellect &#8212;
                <%  string[] isIntellectApplied = data.AnswerArray("GenericIsIntellectApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsIntellectApplied", string.Empty, new { @id = Model.Type + "_GenericIsIntellectAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsIntellectApplied1' name='{0}_GenericIsIntellectApplied' value='1' type='checkbox' {1} />", Model.Type, isIntellectApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsIntellectApplied1">WNL for Patient</label></th>
            <th>Insight and Judgment &#8212;
                <%  string[] isInsightAndJudgmentApplied = data.AnswerArray("GenericIsInsightAndJudgmentApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsInsightAndJudgmentApplied", string.Empty, new { @id = Model.Type + "_GenericIsInsightAndJudgmentAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsInsightAndJudgmentApplied1' name='{0}_GenericIsInsightAndJudgmentApplied' value='1' type='checkbox' {1} />", Model.Type, isInsightAndJudgmentApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsInsightAndJudgmentApplied1">WNL for Patient</label></th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/MoodAffectAssessment.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Sensorium.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Intellect.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/InsightAndJudgment.ascx", Model); %></td>
        </tr>
         <tr>
            <th colspan="2">Interview Behavior &#8212;
                <%  string[] isInterviewBehaviorApplied = data.AnswerArray("GenericIsInterviewBehaviorApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsInterviewBehaviorApplied", string.Empty, new { @id = Model.Type + "_GenericIsInterviewBehaviorAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsInterviewBehaviorApplied1' name='{0}_GenericIsInterviewBehaviorApplied' value='1' type='checkbox' {1} />", Model.Type, isInterviewBehaviorApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsInterviewBehaviorApplied1">WNL for Patient</label></th>
            <th colspan="2">Content of Thought &#8212;
                <%  string[] isContentOfThoughtApplied = data.AnswerArray("GenericIsContentOfThoughtApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsContentOfThoughtApplied", string.Empty, new { @id = Model.Type + "_GenericIsContentOfThoughtAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsContentOfThoughtApplied1' name='{0}_GenericIsContentOfThoughtApplied' value='1' type='checkbox' {1} />", Model.Type, isContentOfThoughtApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsContentOfThoughtApplied1">WNL for Patient</label></th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/InterviewBehavior.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/ContentOfThought.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Comments/Narrative</th>
            <th colspan="2">Homebound Status</th>
        </tr>
        <tr>
            <td colspan="2">
                <div class="align-center">
                    <%= Html.Templates(Model.Type + "_GenericNarrativeCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericComments" })%>
                    <%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), 8, 20, new { @class = "fill", @id = Model.Type + "_GenericComments" })%>
                </div>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/HomeBoundStatus.ascx", Model); %>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsAppearanceApplied1"),
        $("#<%= Model.Type %>_AppearanceContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsMotorActivityApplied1"),
        $("#<%= Model.Type %>_MotorActivityContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsSpeechApplied1"),
        $("#<%= Model.Type %>_SpeechContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsFlowOfThoughtApplied1"),
        $("#<%= Model.Type %>_FlowOfThoughtContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsMoodAffectAssessmentApplied1"),
        $("#<%= Model.Type %>_MoodAffectAssessmentContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsSensoriumApplied1"),
        $("#<%= Model.Type %>_SensoriumContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsIntellectApplied1"),
        $("#<%= Model.Type %>_IntellectContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsInsightAndJudgmentApplied1"),
        $("#<%= Model.Type %>_InsightAndJudgmentContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsInterviewBehaviorApplied1"),
        $("#<%= Model.Type %>_InterviewBehaviorContainer"));
    U.HideIfChecked(
        $("#<%= Model.Type %>_GenericIsContentOfThoughtApplied1"),
        $("#<%= Model.Type %>_ContentOfThoughtContainer"));
</script>