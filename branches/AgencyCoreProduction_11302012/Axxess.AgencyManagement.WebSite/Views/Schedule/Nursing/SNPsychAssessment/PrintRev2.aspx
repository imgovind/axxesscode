﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>SN Psychiatric Assessment<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "SN Psychiatric Assessment" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.Patient.LastName.Clean()%>, <%= Model.Patient.FirstName.Clean()%> <%= Model.Patient.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.Patient.DOBFormatted.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.Patient.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.StartDate.ToShortDateString().ToZeroFilled(), Model.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Appearance.ascx", Model); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MotorActivity.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Speech.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/FlowOfThought.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MoodAffectAssessment.ascx", Model); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Sensorium.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Intellect.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/InsightAndJudgment.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/InterviewBehavior.ascx", Model); %>, 2);
    printview.addspliltablesubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/ContentOfThought.ascx", Model); %>);
    
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PsychiatricInterventions.ascx", Model); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Goals.ascx", Model); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/RehabPotential.ascx", Model); %>);
    
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericComments").Clean() %>", false), "Comments/Narrative");
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model); %>);
    
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>