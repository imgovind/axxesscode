﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Schedule Deviation | <%= Current.AgencyName %></span>
<% string pagename = "AgencyScheduleDeviation"; %>
<% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
<div class="wrapper">
    <div class="buttons float-right"><ul>
    <li><%= Html.ActionLink("Export to Excel", "ScheduleDeviations", "Export", new { BranchCode = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = pagename + "_ExportLink", @class = "excel" })%></li>
    </ul></div>
   <fieldset class="orders-filter">
        <div class="buttons float-right">
             <ul>
                 <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"U.RebindDataGridContent('{0}','Schedule/DeviationContent',{{ BranchCode: $('#{0}_BranchCode').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li>
                 
             </ul>
       </div>
       <label  class="float-left">Branch:</label><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%>
       <label  class="strong">Date Range:</label><input type="text" class="date-picker shortdate" style="width:80px" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /><label class="strong"> To </label><input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
       
       
       
   </fieldset>
   <div id="<%= pagename %>GridContainer" class="list-container">
       <% Html.RenderPartial("DeviationContent", Model); %>
    </div>
</div>

<script type="text/javascript">
    $("#window_scheduledeviation_content").css({
        "background-color": "#d6e5f3"
    });
</script>