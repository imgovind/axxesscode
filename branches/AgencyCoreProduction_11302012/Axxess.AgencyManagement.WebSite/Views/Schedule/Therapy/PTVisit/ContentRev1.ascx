﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Homebound Status</th>
            <th colspan="2">Functional Limitations</th>
            <th colspan="2">Vital Signs</th>
        </tr>
        <tr class="align-left">
            <td colspan="2">
                <% string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericHomeBoundStatus" value="" />
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus1' class='radio' name='{1}_GenericHomeBoundStatus' value='1' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus1">Needs assist with transfer.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus2' class='radio' name='{1}_GenericHomeBoundStatus' value='2' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus2">Needs assist with gait.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus3' class='radio' name='{1}_GenericHomeBoundStatus' value='3' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus3">Needs assist leaving the home.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus4' class='radio' name='{1}_GenericHomeBoundStatus' value='4' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus4">Unable to be up for long period.</label>
                </div>
            </td>
            <td colspan="2">
                <% string[] genericFunctionalLimitations = data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer != "" ? data["GenericFunctionalLimitations"].Answer.Split(',') : null; %>
                <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations1' class='radio' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations1">ROM/ Strength.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations2' class='radio' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations2">Pain.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations3' class='radio' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations3">Safety Techniques.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations4' class='radio' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations4">W/C Mobility.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations5' class='radio' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations5">Balance/ Gait.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations6' class='radio' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations6">Bed Mobility.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations7' class='radio' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations7">Transfer.</label>
                </div>
            </td>
            <td colspan="2">
                <label for="<%= Model.Type %>_GenericPriorBP" class="float-left">Prior BP:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericPriorBP", data.ContainsKey("GenericPriorBP") ? data["GenericPriorBP"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPriorBP" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPostBP" class="float-left">Post BP:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericPostBP", data.ContainsKey("GenericPostBP") ? data["GenericPostBP"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPostBP" })%> </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPriorPulse" class="float-left">Prior Pulse:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericPriorPulse", data.ContainsKey("GenericPriorPulse") ? data["GenericPriorPulse"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPriorPulse" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPostPulse" class="float-left">Post Pulse:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type+"_GenericPostPulse", data.ContainsKey("GenericPostPulse") ? data["GenericPostPulse"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPostPulse" })%> </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Supervisory Visit</th>
            <th colspan="3">Subjective</th>
        </tr>
        <tr>
            <td colspan="3">
                <% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericSupervisoryVisit" value=" " />
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit1' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='1' type='checkbox' {0} />", genericSupervisoryVisit.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit1" class="fixed radio">Supervisory Visit</label>
                </div>
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit2' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='2' type='checkbox' {0} />", genericSupervisoryVisit.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit2" class="fixed radio">LPTA Present</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericSupervisoryVisitComment" class="strong">Comment:</label>
                <div><%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment", @class = "fill" })%></div>
            </td>
            <td colspan="3"><%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"), new { @id = Model.Type + "_GenericSubjective", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="3">Objective</th>
            <th colspan="3">Bed Mobility Training</th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/FormRev1.ascx", Model); %></td>
            <td colspan="3">
                <div class="float-right">
                    100% = Max Assist 50% = Min A<br />
	                90% = Max A; 10% Pt Participation 45% = CG<br />
	                80% = Max A; 20% Pt Participation 30% = SBA<br />
	                70% = Max A; 50% Pt Participation 20% = SBA, S<br />
	                60% = between MOD A. and Min A 10% = SBA, I<br />
                </div>
                <div class="float-left">
                    <div>
                        <label for="<%= Model.Type %>_GenericRolling" class="float-left">Rolling</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericRolling", data.AnswerOrEmptyString("GenericRolling"), new { @id = Model.Type + "_GenericRolling", @class = "sn" })%>
                            X
                            <%= Html.TextBox(Model.Type + "_GenericRollingReps", data.AnswerOrEmptyString("GenericRollingReps"), new { @id = Model.Type + "_GenericRollingReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSupSit" class="float-left">Sup-Sit</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericSupSit", data.AnswerOrEmptyString("GenericSupSit"), new { @id = Model.Type + "_GenericSupSit", @class = "sn" })%>
                            X
                            <%= Html.TextBox(Model.Type + "_GenericSupSitReps", data.AnswerOrEmptyString("GenericSupSitReps"), new { @id = Model.Type + "_GenericSupSitReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericScootingToward" class="float-left">Scooting Toward</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericScootingToward", data.AnswerOrEmptyString("GenericScootingToward"), new { @id = Model.Type + "_GenericScootingToward", @class = "sn" })%>
                            X
                            <%= Html.TextBox(Model.Type + "_GenericScootingTowardReps", data.AnswerOrEmptyString("GenericScootingTowardReps"), new { @id = Model.Type + "_GenericScootingTowardReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSitToStand" class="float-left">Sit to Stand</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericSitToStand", data.AnswerOrEmptyString("GenericSitToStand"), new { @id = Model.Type + "_GenericSitToStand", @class = "sn" })%>
                            X
                            <%= Html.TextBox(Model.Type + "_GenericSitToStandReps", data.AnswerOrEmptyString("GenericSitToStandReps"), new { @id = Model.Type + "_GenericSitToStandReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label class="float-left">Proper Foot Placement</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericProperFootPlacement", "1", data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("1"), new { @id = Model.Type + "_GenericProperFootPlacement1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericProperFootPlacement1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericProperFootPlacement", "0", data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("0"), new { @id = Model.Type + "_GenericProperFootPlacement0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericProperFootPlacement0" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label class="float-left">Proper Unfolding</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericProperUnfolding", "1", data.AnswerOrEmptyString("GenericProperUnfolding").Equals("1"), new { @id = Model.Type + "_GenericProperUnfolding1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericProperUnfolding1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericProperUnfolding", "0", data.AnswerOrEmptyString("GenericProperUnfolding").Equals("0"), new { @id = Model.Type + "_GenericProperUnfolding0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericProperUnfolding0" class="inline-radio">No</label>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="2">Transfer Training</th>
            <th colspan="4">Gait Training</th>
        </tr>
        <tr class="align-left">
            <td colspan="2">
                <div>
                    <label for="<%= Model.Type %>_GenericTransferTraining" class="float-left">Transfer Training</label>
                    <div class="float-right">
                        x
                        <%= Html.TextBox(Model.Type + "_GenericTransferTraining", data.AnswerOrEmptyString("GenericTransferTraining"), new { @id = Model.Type + "_GenericTransferTraining", @class = "sn" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="float-right">
                        <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "1", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1"), new { @id = Model.Type + "_GenericAssistiveDevice1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice1" class="inline-radio">Yes</label>
                        <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "0", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0"), new { @id = Model.Type + "_GenericAssistiveDevice0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericAssistiveDevice0" class="inline-radio">No</label><br />
                        <label for="<%= Model.Type %>_GenericAssistiveDeviceSpecify">Specify:</label>
                        <%= Html.TextBox(Model.Type + "_GenericAssistiveDeviceSpecify", data.AnswerOrEmptyString("GenericAssistiveDeviceSpecify"), new { @id = Model.Type + "_GenericAssistiveDeviceSpecify", @class = "loc" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericBedChairAssist" class="float-left">Bed &#8212; Chair</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericBedChairAssist", data.AnswerOrEmptyString("GenericBedChairAssist"), new { @id = Model.Type + "_GenericBedChairAssist", @class = "sn" })%> % Assist
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChairToiletAssist" class="float-left">Chair &#8212; Toilet</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericChairToiletAssist", data.AnswerOrEmptyString("GenericChairToiletAssist"), new { @id = Model.Type + "_GenericChairToiletAssist", @class = "sn" })%>
                        % Assist
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChairCarAssist" class="float-left">Chair &#8212; Car</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericChairCarAssist", data.AnswerOrEmptyString("GenericChairCarAssist"), new { @id = Model.Type + "_GenericChairCarAssist", @class = "sn" })%>
                        % Assist
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="" class="float-left">Sitting Balance Activities WB UE</label>
                    <div class="float-right">
                        <%= Html.RadioButton(Model.Type + "_GenericSBAWE", "1", data.AnswerOrEmptyString("GenericSBAWE").Equals("1"), new { @id = Model.Type + "_GenericSBAWE1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericSBAWE1" class="inline-radio">Yes</label>
                        <%= Html.RadioButton(Model.Type + "_GenericSBAWE", "0", data.AnswerOrEmptyString("GenericSBAWE").Equals("0"), new { @id = Model.Type + "_GenericSBAWE0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericSBAWE0" class="inline-radio">No</label><br />
                        <label for="<%= Model.Type %>_GenericSBAWEStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSBAWEStaticAssist", data.AnswerOrEmptyString("GenericSBAWEStaticAssist"), new { @id = Model.Type + "_GenericSBAWEStaticAssist", @class = "sn" })%>
                        % Assist<br />
                        <label for="<%= Model.Type %>_GenericSBAWEDynamicAssist">Dynamic:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSBAWEDynamicAssist", data.AnswerOrEmptyString("GenericSBAWEDynamicAssist"), new { @id = Model.Type + "_GenericSBAWEDynamicAssist", @class = "sn" })%>
                        % Assist
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="" class="float-left">Standing Balance Activities</label>
                    <div class="float-right">
                        <label for="<%= Model.Type %>_GenericSBAStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSBAStaticAssist", data.AnswerOrEmptyString("GenericSBAStaticAssist"), new { @id = Model.Type + "_GenericSBAStaticAssist", @class = "sn" })%>
                        % Assist<br />
                        <label for="<%= Model.Type %>_GenericSBADynamicAssist">Dynamic:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSBADynamicAssist", data.AnswerOrEmptyString("GenericSBADynamicAssist"), new { @id = Model.Type + "_GenericSBADynamicAssist", @class = "sn" })%>
                        % Assist
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
            <td colspan="4">
                <div>
                    <label for="<%= Model.Type %>_GenericGaitTrainingFt" class="float-left">Gait Training</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericGaitTrainingFt", data.AnswerOrEmptyString("GenericGaitTrainingFt"), new { @id = Model.Type + "_GenericGaitTrainingFt", @class = "sn" })%>
                        ft. X
                        <%= Html.TextBox(Model.Type + "_GenericGaitTrainingFtX", data.AnswerOrEmptyString("GenericGaitTrainingFtX"), new { @id = Model.Type + "_GenericGaitTrainingFtX", @class = "sn" })%>
                        <% string[] genericWalkDirection = data.AnswerArray("GenericWalkDirection"); %>
                        <input type="hidden" name="<%= Model.Type %>_GenericWalkDirection" value="" />
                        <%= string.Format("<input id='{1}_GenericWalkDirection1' class='radio' name='{1}_GenericWalkDirection' value='1' type='checkbox' {0} />", genericWalkDirection.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericWalkDirection1" class="fixed radio">Walking Sideways.</label>
                        <%= string.Format("<input id='{1}_GenericWalkDirection2' class='radio' name='{1}_GenericWalkDirection' value='2' type='checkbox' {0} />", genericWalkDirection.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericWalkDirection2" class="fixed radio">Walk Backwards.</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label class="float-left">With Device</label>
                    <div class="float-right">
                       <%= Html.RadioButton(Model.Type + "_GenericGaitTrainingWithDevice", "1", data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("1"), new { @id = Model.Type + "_GenericGaitTrainingWithDevice1", @class = "radio" })%>
                       <label for="<%= Model.Type %>_GenericGaitTrainingWithDevice1" class="inline-radio">Yes</label>
                       <%= Html.RadioButton(Model.Type + "_GenericGaitTrainingWithDevice", "0", data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("0"), new { @id = Model.Type + "_GenericGaitTrainingWithDevice0", @class = "radio" })%>
                       <label for="<%= Model.Type %>_GenericGaitTrainingWithDevice0" class="inline-radio">No</label>
                       <%= Html.TextBox(Model.Type + "_GenericGaitTrainingWithDeviceAssist", data.AnswerOrEmptyString("GenericGaitTrainingWithDeviceAssist"), new { @id = Model.Type + "_GenericGaitTrainingWithDeviceAssist", @class = "sn" })%> % Assist
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="margin">
                    <div>
                        <label for="" class="float-left">Turning</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericTurningDistance", data.AnswerOrEmptyString("GenericTurningDistance"), new { @id = Model.Type + "_GenericTurningDistance", @class = "sn" })%>
                            Distance
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="" class="float-left">Uneven</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericUnevenAssist", data.AnswerOrEmptyString("GenericUnevenAssist"), new { @id = Model.Type + "_GenericUnevenAssist", @class = "sn" })%>
                            % Assist
                            <%= Html.TextBox(Model.Type + "_GenericUnevenDevice", data.AnswerOrEmptyString("GenericUnevenDevice"), new { @id = Model.Type + "_GenericUnevenDevice", @class = "sn" })%>
                            Device
                        </div>
                        <div class="clear"></div>
                    </div><div>
                        <label for="" class="float-left">Stairs/ Steps</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericStairsStepsAssist", data.AnswerOrEmptyString("GenericStairsStepsAssist"), new { @id = Model.Type + "_GenericStairsStepsAssist", @class = "sn" })%>
                            % Assist
                            <%= Html.TextBox(Model.Type + "_GenericStairsStepsDevice", data.AnswerOrEmptyString("GenericStairsStepsDevice"), new { @id = Model.Type + "_GenericStairsStepsDevice", @class = "sn" })%>
                            Device
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="" class="float-left">W/C Training</label>
                        <div class="float-right">
                            <%= Html.TextBox(Model.Type + "_GenericWCTrainingAssist", data.AnswerOrEmptyString("GenericWCTrainingAssist"), new { @id = Model.Type + "_GenericWCTrainingAssist", @class = "sn" })%>
                            % Assist
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div>
                    <%  string[] genericTherapyTraning = data.AnswerArray("GenericTherapyTraning"); %>
                    <input type="hidden" name="<%= Model.Type %>_GenericTherapyTraning" value="" />
                    <table class="fixed">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning1' class='radio' name='{1}_GenericTherapyTraning' value='1' type='checkbox' {0} />", genericTherapyTraning.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning1">WB (L)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning2' class='radio' name='{1}_GenericTherapyTraning' value='2' type='checkbox' {0} />", genericTherapyTraning.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning2">WB (R)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning3' class='radio' name='{1}_GenericTherapyTraning' value='3' type='checkbox' {0} />", genericTherapyTraning.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning3">Hip/Knee Extension</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning4' class='radio' name='{1}_GenericTherapyTraning' value='4' type='checkbox' {0} />", genericTherapyTraning.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning4">Stance Phase (L)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning5' class='radio' name='{1}_GenericTherapyTraning' value='5' type='checkbox' {0} />", genericTherapyTraning.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning5">Stance Phase (R)</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning6' class='radio' name='{1}_GenericTherapyTraning' value='6' type='checkbox' {0} />", genericTherapyTraning.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning6">Heel Strike</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning7' class='radio' name='{1}_GenericTherapyTraning' value='7' type='checkbox' {0} />", genericTherapyTraning.Contains("7").ToChecked(),Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning7">Step Length</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning8' class='radio' name='{1}_GenericTherapyTraning' value='8' type='checkbox' {0} />", genericTherapyTraning.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning8">Proper Posture</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericTherapyTraning9' class='radio' name='{1}_GenericTherapyTraning' value='9' type='checkbox' {0} />", genericTherapyTraning.Contains("9").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTherapyTraning9">Speed</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericGaitTrainingOther" class="float-left">Other</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericGaitTrainingOther", data.AnswerOrEmptyString("GenericGaitTrainingOther"), new { @id = Model.Type + "_GenericGaitTrainingOther", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Teaching</th>
            <th colspan="3">Pain</th>
        </tr>
        <tr class="align-left">
            <td colspan="3">
                <% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching1' class='radio' name='{1}_GenericTeaching' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching1">Patient</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching2' class='radio' name='{1}_GenericTeaching' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching2">Caregiver</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching3' class='radio' name='{1}_GenericTeaching' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching3">HEP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching4' class='radio' name='{1}_GenericTeaching' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching4">Safe Transfer</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching5' class='radio' name='{1}_GenericTeaching' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching5">Safe Gait</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther", @class = "float-left" })%>
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
            <td colspan="3">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPriorIntensityOfPain" class="float-left">Pain level prior to therapy:</label>
                                <div class="float-right">
                                    <%  var genericPriorIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPriorIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPriorIntensityOfPain", genericPriorIntensityOfPain, new { @id = Model.Type + "_GenericPriorIntensityOfPain", @class = "oe" })%>
                                </div>                                
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPostIntensityOfPain" class="float-left">Pain level after therapy:</label>
                                <div class="float-right">
                                    <%  var genericPostIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPostIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPostIntensityOfPain", genericPostIntensityOfPain, new { @id = Model.Type + "_GenericPostIntensityOfPain", @class = "oe" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainLocation" class="float-left">Location:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPainLocation", data.AnswerOrEmptyString("GenericPainLocation"), new { @id = Model.Type + "_GenericPainLocation" })%></div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainRelievedBy" class="float-left">Relieved by:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPainRelievedBy", data.AnswerOrEmptyString("GenericPainRelievedBy"), new { @id = Model.Type + "_GenericPainRelievedBy" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Other Comment:</label>
                <div><%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment", @class = "fill" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Assessment</th>
            <th colspan="3">Plan</th>
        </tr>
        <tr>
            <td colspan="3"><%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%></td>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="float-left">Continue Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @id = Model.Type + "_GenericContinuePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="float-left">Change Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @id = Model.Type + "_GenericChangePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericPlanDischarge" class="float-left">Plan Discharge:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @id = Model.Type + "_GenericPlanDischarge", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="6">Narrative</th>
        </tr>
        <tr>
            <td colspan="6">
                <div>
                    <%= Html.Templates(Model.Type + "_GenericNarrativeCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericNarrativeComment" })%>
                    <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 8, 20, new { @id = Model.Type + "_GenericNarrativeComment", @class = "fill" })%>
                </div>
            </td>
         </tr>
    </tbody>
</table>