﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSpeechTherapyDone = data.AnswerArray("GenericSpeechTherapyDone"); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Treatment Diagnosis/Problem</th>
            <th colspan="2">Subjective</th>
        </tr>
        <tr>
            <td colspan="2">
            <%= Html.Templates(Model.Type + "_GenericTreatmentDiagnosisTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTreatmentDiagnosis" })%>
            <%= Html.TextArea(Model.Type + "_GenericTreatmentDiagnosis", data.AnswerOrEmptyString("GenericTreatmentDiagnosis"), 6, 20, new { @id = Model.Type + "_GenericTreatmentDiagnosis", @class = "fill" })%></td>
            <td colspan="2">
            <%= Html.Templates(Model.Type + "_GenericTreatmentOutcomeTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTreatmentOutcome" })%>
            <%= Html.TextArea(Model.Type + "_GenericTreatmentOutcome", data.AnswerOrEmptyString("GenericTreatmentOutcome"), 6, 20, new { @id = Model.Type + "_GenericTreatmentOutcome", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="2">Homebound Reason</th>
            <th colspan="2">Skilled treatment provided this visit</th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></div>
            </td>
            <td colspan="2">
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction1' class='radio' name='{1}_STInstruction' value='1' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction1">Evaluation</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction2' class='radio' name='{1}_STInstruction' value='2' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction2">Dysphagia treatments</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction3' class='radio' name='{1}_STInstruction' value='3' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction3">Safe swallowing evaluation</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction4' class='radio' name='{1}_STInstruction' value='4' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction4">Patient/Family education</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction5' class='radio' name='{1}_STInstruction' value='5' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction5">Language disorders</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction6' class='radio' name='{1}_STInstruction' value='6' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction6">Speech dysphagia instruction program</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction7' class='radio' name='{1}_STInstruction' value='7' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction7">Voice disorders</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction8' class='radio' name='{1}_STInstruction' value='8' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction8">Aural rehabilitation</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction9' class='radio' name='{1}_STInstruction' value='9' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction9">Teach/Develop communication system</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction10' class='radio' name='{1}_STInstruction' value='10' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction10">Speech articulation disorders</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction11' class='radio' name='{1}_STInstruction' value='11' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("11").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction11">Non-oral communication</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction12' class='radio' name='{1}_STInstruction' value='12' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("12").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction12">Pain Management</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction13' class='radio' name='{1}_STInstruction' value='13' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("13").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction13">Alaryngeal speech skills</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction14' class='radio' name='{1}_STInstruction' value='14' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("14").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction14">Language processing</label>
                </div>
                <div class="halfOfTd align-left">
                    <%= string.Format("<input id='{1}_STInstruction15' class='radio' name='{1}_STInstruction' value='15' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("15").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction15">Food texture recommendations</label>
                </div>
                <li class="halfOfTd align-left">
                <span class="option">
                    <%= string.Format("<input id='{1}_STInstruction17' class='radio' name='{1}_STInstruction' value='17' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("17").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction4">Other</label>
                </span>
                <span class="extra inline">
                    <%= Html.TextBox(Model.Type + "_STInstructionOther", data.AnswerOrEmptyString("STInstructionOther"), new { @id = Model.Type + "_STInstructionOther", @class = "" })%>
                </span>
                </li>
                <li class="twohalfOfTd align-left">
                <span class="option">
                    <%= string.Format("<input id='{1}_STInstruction16' class='radio' name='{1}_STInstruction' value='16' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("16").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction16">Establish home maintenance program</label>
                </span>
                <span class="extra inline">
                    <%= string.Format("<input id='{1}_STInstruction16a' class='radio' name='{1}_STInstruction' value='16a' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("16a").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction16a">Copy given to patient</label>
                    <%= string.Format("<input id='{1}_STInstruction16b' class='radio' name='{1}_STInstruction' value='16b' type='checkbox' {0} />", data.AnswerArray("STInstruction").Contains("16b").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_STInstruction16b">Copy attached to chart</label>
                </span>
                </li>
                
                <div class="third align-left"></div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <label class="strong">Assessment(Observations, instructions and measurable outcomes)</label>
                <%= Html.Templates(Model.Type + "_ObservationOutcomesTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_ObservationOutcomes" })%>
                <div><%= Html.TextArea(Model.Type + "_ObservationOutcomes", data.AnswerOrEmptyString("ObservationOutcomes"),6,20, new { @id = Model.Type + "_ObservationOutcomes", @class = "fill" })%></div>
            
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <label class="strong">Progress made towards goals</label>
                <%= Html.Templates(Model.Type + "_ProgressMadeTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_ProgressMade" })%>
                <div><%= Html.TextArea(Model.Type + "_ProgressMade", data.AnswerOrEmptyString("ProgressMade"), 6, 20, new { @id = Model.Type + "_ProgressMade", @class = "fill" })%></div>
            
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <label class="strong">Patient/caregiver response to treatment</label>
                <%= Html.Templates(Model.Type + "_GenericResponseTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_GenericResponse" })%>
                <div><%= Html.TextArea(Model.Type + "_GenericResponse", data.AnswerOrEmptyString("GenericResponse"),6,20, new { @id = Model.Type + "_GenericResponse", @class = "fill" })%></div>
            
            </td>
        </tr>
        <tr>
            <th colspan="4">Teaching</th>
        </tr>
        <tr>
            <td colspan="4">
                <%= Html.Templates(Model.Type + "_TeachingTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_Teaching" })%>
                <div><%= Html.TextArea(Model.Type + "_Teaching", data.AnswerOrEmptyString("Teaching"), 6, 20, new { @id = Model.Type + "_Teaching", @class = "fill" })%></div>
            
            </td>
        </tr>
        <tr>
            <th colspan="4">Plan</th>
        </tr>
        <tr>
            <td colspan="4">
                <%= Html.Templates(Model.Type + "_GenericPlanTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPlan" })%>
                <div><%= Html.TextArea(Model.Type + "_GenericPlan", data.AnswerOrEmptyString("GenericPlan"), 6, 20, new { @id = Model.Type + "_GenericPlan", @class = "fill" })%></div>
            
            </td>
            
        </tr>
    </tbody>
</table>