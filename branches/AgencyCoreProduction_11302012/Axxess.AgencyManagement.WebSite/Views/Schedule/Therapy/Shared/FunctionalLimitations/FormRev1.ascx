﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] functionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<table class="fixed">
    <tbody>
        <tr>
            <td>
                <label class="float-left">Functional Limitations:</label>
                <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations1' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", functionalLimitations.Contains("1").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations1" class="radio">Transfer</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations2' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", functionalLimitations.Contains("2").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations2" class="radio">Gait</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations3' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", functionalLimitations.Contains("3").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations3" class="radio">Strength</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations4' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", functionalLimitations.Contains("4").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations4" class="radio">Bed Mobility</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations5' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", functionalLimitations.Contains("5").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations5" class="radio">Safety Techniques</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations6' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", functionalLimitations.Contains("6").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations6" class="radio">ADL&#8217;s</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations7' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", functionalLimitations.Contains("7").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations7" class="radio">ROM</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations8' name='{1}_GenericFunctionalLimitations' value='8' type='checkbox' {0} />", functionalLimitations.Contains("8").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations8" class="radio">W/C Mobility</label>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericFunctionalLimitationsOther" class="float-left">Other</label>
            </td>
            <td>
                <%= Html.TextBox(Model.Type + "_GenericFunctionalLimitationsOther", data.AnswerOrEmptyString("GenericFunctionalLimitationsOther"), new { @id = Model.Type + "_GenericFunctionalLimitationsOther", @class = "fill" })%>
            </td>
        </tr>
    </tbody>
</table>