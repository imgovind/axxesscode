﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLBathing" class="float-left">Bathing:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLBathing", data.AnswerOrEmptyString("GenericADLBathing"), new { @id = Model.Type + "_GenericADLBathing" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLBathing" class="float-left">UE Dressing:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.AnswerOrEmptyString("GenericADLUEDressing"), new { @id = Model.Type + "_GenericADLUEDressing" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLLEDressing" class="float-left">LE Dressing:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.AnswerOrEmptyString("GenericADLLEDressing"), new { @id = Model.Type + "_GenericADLLEDressing" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.AnswerOrEmptyString("GenericADLGrooming"), new { @id = Model.Type + "_GenericADLGrooming" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLToileting", data.AnswerOrEmptyString("GenericADLToileting"), new { @id = Model.Type + "_GenericADLToileting" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLFeeding" class="float-left">Feeding:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.AnswerOrEmptyString("GenericADLFeeding"), new { @id = Model.Type + "_GenericADLFeeding" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal Prep:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.AnswerOrEmptyString("GenericADLMealPrep"), new { @id = Model.Type + "_GenericADLMealPrep" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLHouseCleaning" class="float-left">House Cleaning:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.AnswerOrEmptyString("GenericADLHouseCleaning"), new { @id = Model.Type + "_GenericADLHouseCleaning" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="float-left">Adaptive Equipment/Assistive Device Use or Needs:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.AnswerOrEmptyString("GenericADLAdaptiveEquipment"), new { @id = Model.Type + "_GenericADLAdaptiveEquipment" })%></div>
</div>
<div class="padnoterow">
    <label class="float-left">Bed Mobility:</label>
    <div class="margin">
        <label for="<%= Model.Type %>_GenericADLBedMobilityRolling" class="float-left">Rolling:</label>
        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilityRolling", data.AnswerOrEmptyString("GenericADLBedMobilityRolling"), new { @id = Model.Type + "_GenericADLBedMobilityRolling" })%></div>
        <div class="clear"></div>
        <label for="<%= Model.Type %>_GenericADLBedMobilitySupineToSit" class="float-left">Supine to Sit:</label>
        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySupineToSit", data.AnswerOrEmptyString("GenericADLBedMobilitySupineToSit"), new { @id = Model.Type + "_GenericADLBedMobilitySupineToSit" })%></div>
        <div class="clear"></div>
        <label for="<%= Model.Type %>_GenericADLBedMobilitySitToStand" class="float-left">Sit to Stand:</label>
        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySitToStand", data.AnswerOrEmptyString("GenericADLBedMobilitySitToStand"), new { @id = Model.Type + "_GenericADLBedMobilitySitToStand" })%></div>
    </div>
</div>
<div class="padnoterow">
    <label class="float-left">Transfers:</label>
    <div class="margin">
        <label for="<%= Model.Type %>_GenericADLTransfersTubShowerToilet" class="float-left">Tub/Shower/Toilet:</label>
        <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLTransfersTubShowerToilet", data.AnswerOrEmptyString("GenericADLTransfersTubShowerToilet"), new { @id = Model.Type + "_GenericADLTransfersTubShowerToilet" })%></div>
        <div class="clear"></div>
        <label for="<%= Model.Type %>_GenericADLTransfersBedToChairWheelchair" class="float-left">Bed to Chair/Wheelchair:</label><div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLTransfersBedToChairWheelchair", data.ContainsKey("GenericADLTransfersBedToChairWheelchair") ? data["GenericADLTransfersBedToChairWheelchair"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLTransfersBedToChairWheelchair" }) %></div>
    </div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericADLWCMobility" class="float-left">W/C Mobility:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLWCMobility", data.AnswerOrEmptyString("GenericADLWCMobility"), new { @id = Model.Type + "_GenericADLWCMobility" })%></div>
</div>
<div class="padnoterow">
    <label class="float-left">Sitting Balance:</label>
    <div class="margin">
        <label class="float-left">Static:</label>
        <div class="float-right">
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "2", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("2"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic2", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceStatic2" class="inline-radio">Good</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "1", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("1"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic1", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceStatic1" class="inline-radio">Fair</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "0", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("0"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic0", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceStatic0" class="inline-radio">Poor</label>
        </div>
        <div class="clear"></div>
        <label class="float-left">Dynamic:</label>
        <div class="float-right">
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "2", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("2"), new { @id = Model.Type + "_GenericADLSittingBalanceDynamic2", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic2" class="inline-radio">Good</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "1", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("1"), new { @id = Model.Type + "_GenericADLSittingBalanceDynamic1", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic1" class="inline-radio">Fair</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "0", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("0"), new { @id = Model.Type + "_GenericADLSittingBalanceDynamic0", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic0" class="inline-radio">Poor</label>
        </div>
    </div>
</div>
<div class="padnoterow">
    <label class="float-left">Standing Balance:</label>
    <div class="margin">
        <label class="float-left">Static:</label>
        <div class="float-right">
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "2", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("2"), new { @id = Model.Type + "_GenericADLStandingBalanceStatic2", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic2" class="inline-radio">Good</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "1", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("1"), new { @id = Model.Type + "_GenericADLStandingBalanceStatic1", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic1" class="inline-radio">Fair</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "0", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("0"), new { @id = Model.Type + "_GenericADLStandingBalanceStatic0", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic0" class="inline-radio">Poor</label>
        </div>
        <div class="clear"></div>
        <label class="float-left">Dynamic:</label>
        <div class="float-right">
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "2", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("2"), new { @id = Model.Type + "_GenericADLStandingBalanceDynamic2", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic2" class="inline-radio">Good</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "1", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("1"), new { @id = Model.Type + "_GenericADLStandingBalanceDynamic1", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic1" class="inline-radio">Fair</label>
            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "0", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("0"), new { @id = Model.Type + "_GenericADLStandingBalanceDynamic0", @class = "radio" })%>
            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic0" class="inline-radio">Poor</label>
        </div>
    </div>
</div>