﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Frequency",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFrequency").Clean()%>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDuration").Clean()%>",0,1)));
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericNarrativeComment").Clean()%>",0,2),
        "Narrative");
</script>