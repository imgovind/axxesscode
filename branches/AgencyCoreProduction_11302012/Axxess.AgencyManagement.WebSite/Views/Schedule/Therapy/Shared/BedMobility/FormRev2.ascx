﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <thead>
        <tr>
            <th></th>
            <th class="strong">Assistance</th>
            <th class="strong">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceRight" class="strong">Rolling to Right</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceRight", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceLeft" class="strong">Rolling to Left</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceLeft", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistance" class="strong">Sit Stand Sit</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySitStandSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistance" class="strong">Sup to Sit</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySupToSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>
<div>
    <label for="<%= Model.Type %>_GenericBedMobilityComment" class="strong">Comment</label>
    <%= Html.Templates(Model.Type + "_BedMobilityTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericBedMobilityComment" })%>
    <%= Html.TextArea(Model.Type + "_GenericBedMobilityComment", data.ContainsKey("GenericBedMobilityComment") ? data["GenericBedMobilityComment"].Answer : string.Empty,4,20, new { @id = Model.Type + "_GenericBedMobilityComment", @class = "fill" })%>
</div>