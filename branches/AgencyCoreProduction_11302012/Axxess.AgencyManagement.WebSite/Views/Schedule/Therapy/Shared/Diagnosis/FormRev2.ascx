﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <tbody>
        <tr>
            <td class="align-left strong">
                Medical Diagnosis:
            </td>
            <td>
                <%= Html.TextBox(Model.Type + "_GenericMedicalDiagnosis", data.AnswerOrEmptyString("GenericMedicalDiagnosis"), new { @id = Model.Type + "_GenericMedicalDiagnosis" }) %>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnset1" class="float-left radio">
                    Onset</label>
                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_MedicalDiagnosisDate"
                    value="<%= data.AnswerOrEmptyString("MedicalDiagnosisDate") %>" id="<%= Model.Type %>_MedicalDiagnosisDate" />
            </td>
        </tr>
        <tr>
            <td class="align-left strong">
            <%if (Model.DisciplineTask == 44)
              { %>
                PT Diagnosis:
                <%}
              else if (Model.DisciplineTask == 47)
              { %>
                OT Diagnosis:
                <%} %>
            </td>
            <td>
                <%= Html.TextBox(Model.Type + "_GenericTherapyDiagnosis", data.AnswerOrEmptyString("GenericTherapyDiagnosis"), new { @id = Model.Type + "_GenericTherapyDiagnosis" })%><br />
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericTherapyDiagnosisOnset1" class="float-left radio">
                    Onset</label>
                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_TherapyDiagnosisDate"
                    value="<%= data.AnswerOrEmptyString("TherapyDiagnosisDate") %>" id="<%= Model.Type %>_TherapyDiagnosisDate" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericMedicalDiagnosisComment" class="strong">
                        Comment</label>
                        <%= Html.Templates(Model.Type + "_MedicalDiagnosisTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericMedicalDiagnosisComment" })%>
                    <%= Html.TextArea(Model.Type + "_GenericMedicalDiagnosisComment", data.ContainsKey("GenericMedicalDiagnosisComment") ? data["GenericMedicalDiagnosisComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalDiagnosisComment", @class = "fill" })%>
                </div>
            </td>
        </tr>
    </tbody>
</table>
