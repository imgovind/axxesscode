﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("<b>Level:   </b><%= data.AnswerOrEmptyString("GenericWCMobilityLevel").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<b>Uneven:    </b><%= data.AnswerOrEmptyString("GenericWCMobilityUneven").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<b>Maneuver:</b><%= data.AnswerOrEmptyString("GenericWCMobilityManeuver").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1)+
            printview.span("<b>ADL:     </b><%= data.AnswerOrEmptyString("GenericWCMobilityADL").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1)),
        "W/C Mobility");
</script>