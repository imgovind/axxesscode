﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div>
    <label for="<%= Model.Type %>_GenericPriorFunctionalStatus" class="strong">Prior Level of Function:</label>
    <%= Html.Templates(Model.Type + "_GenericPriorFunctionalTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPriorFunctionalStatus" })%>
    <%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus", @class = "fill" })%>
</div>
<div class="clear"></div>

<div>
    <label for="<%= Model.Type %>_GenericMedicalHistory" class="strong">Pertinent Medical History:</label>
    <%= Html.Templates(Model.Type + "_GenericMedicalTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericMedicalHistory" })%>
    <%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.AnswerOrEmptyString("GenericMedicalHistory"), new { @id = Model.Type + "_GenericMedicalHistory", @class = "fill" })%>
</div>

