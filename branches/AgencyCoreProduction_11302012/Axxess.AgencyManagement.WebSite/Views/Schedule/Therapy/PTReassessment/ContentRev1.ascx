﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<%  string[] genericChangedPOC = data.AnswerArray("GenericChangedPOC"); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="10">Vital Signs</th>
        </tr>
        <tr>
            <td colspan="10"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Mental Assessment</th>
            <th colspan="6">Medical Diagnosis</th>
        </tr>
        <tr>
            <td colspan="4">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Orientation:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left strong">Level of Consciousness:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentLOC" })%></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericMentalAssessmentComment" class="strong">Comment</label>
                                    <%= Html.Templates(Model.Type + "_MentalAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericMentalAssessmentComment" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericMentalAssessmentComment", data.ContainsKey("GenericMentalAssessmentComment") ? data["GenericMentalAssessmentComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericMentalAssessmentComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="6">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">Pain Assessment</th>
            <th colspan="6">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
            <td colspan="6" rowspan="9"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">DME</th>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">Available:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @class = "", @id = Model.Type + "_GenericDMEAvailable" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">Needs:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @class = "", @id = Model.Type + "_GenericDMENeeds" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">Suggestion:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @class = "", @id = Model.Type + "_GenericDMESuggestion" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Living Situation</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSpeech" class="float-left">Speech:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSpeech", data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSpeech" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentVision" class="float-left">Vision:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentVision", data.AnswerOrEmptyString("GenericPhysicalAssessmentVision"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentVision" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentHearing" class="float-left">Hearing:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentHearing", data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentHearing" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSkin" class="float-left">Skin:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSkin", data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSkin" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEdema" class="float-left">Edema:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEdema", data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEdema" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentMuscleTone" class="float-left">Muscle Tone:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentMuscleTone", data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentMuscleTone" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentCoordination" class="float-left">Coordination:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentCoordination", data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentCoordination" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSensation" class="float-left">Sensation:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSensation", data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSensation" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEndurance" class="float-left">Endurance:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEndurance", data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEndurance" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSafetyAwareness" class="float-left">Safety Awareness:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSafetyAwareness", data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSafetyAwareness" })%></div>
                <div class="clear"></div>            
            </td>
        </tr>
        <tr>
            <th colspan="4">Prior And Current Level Of Function</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LevelOfFunction/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">Homebound Reason</th>
            <th colspan="6">Prior Therapy Received</th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %>
            </td>
            <td colspan="6">
                <%= Html.Templates(Model.Type + "_PriorTherapyTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPriorTherapyReceived" })%>
                <%= Html.TextArea(Model.Type + "_GenericPriorTherapyReceived", data.AnswerOrEmptyString("GenericPriorTherapyReceived"),6,20, new { @id = Model.Type + "_GenericPriorTherapyReceived", @class = "fill" })%>

            </td>
        </tr>
        <tr>
            <th colspan="10">Functional Mobility Key</th>
        </tr>
        <tr>
            <td colspan="10">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>I = Independent</td>
                            <td>S = Supervision</td>
                            <td>VC = Verbal Cue</td>
                            <td>CGA = Contact Guard Assist</td>
                        </tr>
                        <tr>
                            <td>Min A = 25% Assist</td>
                            <td>Mod A = 50% Assist</td>
                            <td>Max A = 75% Assist</td>
                            <td>Total = 100% Assist</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="10">Compare functional status</th>
        </tr>
        <tr>
            <th colspan="5">PRIOR STATUS</th>
            <th colspan="5">CURRENT STATUS</th>
        </tr>
        <tr>
            <td colspan="10">Bed Mobility</td>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev2.ascx", Model); %></td>
            <td colspan="5">
                <table class="fixed">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="strong">Assistance</th>
                            <th class="strong">Assistive Device</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceRight1" class="strong">Rolling to Right</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceRight1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceRight1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceLeft1" class="strong">Rolling to Left</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceLeft1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceLeft1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistance1" class="strong">Sit Stand Sit</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySitStandSitAssistance1", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance1"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice1", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice1"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistance1" class="strong">Sup to Sit</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySupToSitAssistance1", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance1"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice1", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice1"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <label for="<%= Model.Type %>_GenericBedMobilityComment1" class="strong">Comment</label>
                    <%= Html.Templates(Model.Type + "_BedMobilityComment1Templates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericBedMobilityComment1" })%>
                    <%= Html.TextArea(Model.Type + "_GenericBedMobilityComment1", data.ContainsKey("GenericBedMobilityComment1") ? data["GenericBedMobilityComment1"].Answer : string.Empty, new { @id = Model.Type + "_GenericBedMobilityComment1", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="10">Transfers</td>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev2.ascx", Model); %></td>
            <td colspan="5">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <th></th>
                            <th class="strong">Assistance</th>
                            <th class="strong">Assistive Device</th>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferBedChairAssistance1" class="strong">Bed-Chair</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferBedChairAssistance1", data.AnswerOrEmptyString("GenericTransferBedChairAssistance1"), new { @id = Model.Type + "_GenericTransferBedChairAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferBedChairAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferBedChairAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairBedAssistance1" class="strong">Chair-Bed</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairBedAssistance1", data.AnswerOrEmptyString("GenericTransferChairBedAssistance1"), new { @id = Model.Type + "_GenericTransferChairBedAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairBedAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferChairBedAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairToWCAssistance1" class="strong">Chair to W/C</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairToWCAssistance1", data.AnswerOrEmptyString("GenericTransferChairToWCAssistance1"), new { @id = Model.Type + "_GenericTransferChairToWCAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairToWCAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistance1" class="strong">Toilet or BSC</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferToiletOrBSCAssistance1", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance1"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferCanVanAssistance1" class="strong">Car/Van</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferCanVanAssistance1", data.AnswerOrEmptyString("GenericTransferCanVanAssistance1"), new { @id = Model.Type + "_GenericTransferCanVanAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferCanVanAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferCanVanAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferTubShowerAssistance1" class="strong">Tub/Shower</label></td>
                            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferTubShowerAssistance1", data.AnswerOrEmptyString("GenericTransferTubShowerAssistance1"), new { @id = Model.Type + "_GenericTransferTubShowerAssistance1", @class = "fill" })%></td>
                            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTubShowerAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="strong">Static</th>
                            <th class="strong">Dynamic</th>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic1" class="strong">Sitting Balance</label></td>
                            <td><%= Html.StaticBalance(Model.Type + "_GenericTransferSittingBalanceStatic1", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic1"), new { @id = Model.Type + "_GenericTransferSittingBalanceStatic1", @class = "fill" })%></td>
                            <td><%= Html.DynamicBalance(Model.Type + "_GenericTransferSittingDynamicAssist1", data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist1"), new { @id = Model.Type + "_GenericTransferSittingDynamicAssist1", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferStandBalanceStatic1" class="strong">Stand Balance</label></td>
                            <td><%= Html.StaticBalance(Model.Type + "_GenericTransferStandBalanceStatic1", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic1"), new { @id = Model.Type + "_GenericTransferStandBalanceStatic1", @class = "fill" })%></td>
                            <td><%= Html.DynamicBalance(Model.Type + "_GenericTransferStandBalanceDynamic1", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic1"), new { @id = Model.Type + "_GenericTransferStandBalanceDynamic1", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <label for="<%= Model.Type %>_GenericTransferComment1" class="strong">Comment</label>
                    <%= Html.Templates(Model.Type + "_TransferComment1Templates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTransferComment1" })%>
                    <%= Html.TextArea(Model.Type + "_GenericTransferComment1", data.ContainsKey("GenericTransferComment1") ? data["GenericTransferComment1"].Answer : string.Empty, new { @id = Model.Type + "_GenericTransferComment1", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="10">Gait Analysis</td>
        </tr>
        <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev2.ascx", Model); %></td>
        <td colspan="5">
            <%  string[] genericGaitStepStairRail1 = data.AnswerArray("GenericGaitStepStairRail1"); %>
            <div>
                <label for="<%= Model.Type %>_GenericGaitLevelAssist1" class="float-left">Level</label>
                <div class="float-right">
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitLevelAssist1", data.AnswerOrEmptyString("GenericGaitLevelAssist1"), new { @id = Model.Type + "_GenericGaitLevelAssist1", @class = "" })%>
                    X
                    <%= Html.TextBox(Model.Type+"_GenericGaitLevelFeet1", data.AnswerOrEmptyString("GenericGaitLevelFeet1"), new { @class = "sn", @id = Model.Type+"_GenericGaitLevelFeet1" })%>
                    Feet
                </div>
                <div class="clear"></div>
            </div>
            <div>
                <label for="<%= Model.Type %>_GenericGaitUnLevelAssist1" class="float-left">Unlevel</label>
                <div class="float-right">
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitUnLevelAssist1", data.AnswerOrEmptyString("GenericGaitUnLevelAssist1"), new { @id = Model.Type + "_GenericGaitUnLevelAssist1", @class = "" })%>
                    X
                    <%= Html.TextBox(Model.Type+"_GenericGaitUnLevelFeet1", data.AnswerOrEmptyString("GenericGaitUnLevelFeet1"), new { @class = "sn", @id = Model.Type+"_GenericGaitUnLevelFeet1" })%>
                    Feet
                </div>
                <div class="clear"></div>
            </div>
            <div>
                <label for="<%= Model.Type %>_GenericGaitStepStairAssist1" class="float-left">Step/ Stair</label>
                <div class="float-right">
                    <%= string.Format("<input id='{1}_GenericGaitStepStairRail1x' class='radio' name='{1}_GenericGaitStepStairRail1' value='1' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGaitStepStairRail1">No Rail</label>
                    <%= string.Format("<input id='{1}_GenericGaitStepStairRail2x' class='radio' name='{1}_GenericGaitStepStairRail1' value='2' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGaitStepStairRail2">1 Rail</label>
                    <%= string.Format("<input id='{1}_GenericGaitStepStairRail3x' class='radio' name='{1}_GenericGaitStepStairRail1' value='3' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGaitStepStairRail3">2 Rails</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitStepStairAssist1", data.AnswerOrEmptyString("GenericGaitStepStairAssist1"), new { @id = Model.Type + "_GenericGaitStepStairAssist1", @class = "" })%>
                    X
                    <%= Html.TextBox(Model.Type+"_GenericGaitStepStairFeet1", data.AnswerOrEmptyString("GenericGaitStepStairFeet1"), new { @class = "sn", @id = Model.Type+"_GenericGaitStepStairFeet1" })%>
                    Feet
                </div>
                <div class="clear"></div>
            </div>
            <div>
                <label class="float-left">Assistive Device</label>
                <div class="float-right"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericGaitAnalysisAssistiveDevice1", data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice1"), new { @id = Model.Type + "_GenericGaitAnalysisAssistiveDevice1", @class = "" })%>
                </div>
                <div class="clear"></div>
            </div>
            <div>
                <label for="<%= Model.Type %>_GenericGaitComment1" class="strong float-left">Gait Quality/Deviation</label>
                <%= Html.Templates(Model.Type + "_GaitComment1Templates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericGaitComment1" })%>
                <%= Html.TextArea(Model.Type + "_GenericGaitComment1", data.ContainsKey("GenericGaitComment1") ? data["GenericGaitComment1"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment1", @class = "fill" }) %>
            </div>
        </td>
        <tr>
            <td colspan="10">WB</td>
        </tr>
        <tr>
            
            <td colspan="5">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Status:</td>
                            <td><%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus", data.AnswerOrEmptyString("GenericWBStatus"), new { @id = Model.Type + "_GenericWBStatus", @class = "" })%></td>
                            <td><span>Other: </span><%= Html.TextBox(Model.Type + "_GenericWBStatusOther", data.AnswerOrEmptyString("GenericWBStatusOther"), new { @class = "", @id = Model.Type + "_GenericWBStatusOther" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div>
                                    <label for="<%= Model.Type %>_GenericWBSComment" class="strong">Comment</label>
                                    <%= Html.Templates(Model.Type + "_WBSCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericWBSComment" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericWBSComment", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="5">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Status:</td>
                            <td><%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus1", data.AnswerOrEmptyString("GenericWBStatus1"), new { @id = Model.Type + "_GenericWBStatus1", @class = "" })%></td>
                            <td><span>Other: </span><%= Html.TextBox(Model.Type + "_GenericWBStatusOther1", data.AnswerOrEmptyString("GenericWBStatusOther1"), new { @class = "", @id = Model.Type + "_GenericWBStatusOther1" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div>
                                    <label for="<%= Model.Type %>_GenericWBSComment1" class="strong">Comment</label>
                                    <%= Html.Templates(Model.Type + "_WBSComment1Templates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericWBSComment1" })%>
                                    <%= Html.TextArea(Model.Type + "_GenericWBSComment1", data.AnswerOrEmptyString("GenericWBSComment1"), new { @id = Model.Type + "_GenericWBSComment1", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="10">Standardized test:</th>
        </tr>
        <tr>
            <td colspan="5">
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA", data.AnswerOrEmptyString("GenericTinettiPOMA"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp", data.AnswerOrEmptyString("GenericTimedGetUp"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach", data.AnswerOrEmptyString("GenericFunctionalReach"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span><%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther" })%></div>
            </td>
            <td colspan="5">
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA1", data.AnswerOrEmptyString("GenericTinettiPOMA1"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp1", data.AnswerOrEmptyString("GenericTimedGetUp1"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach1", data.AnswerOrEmptyString("GenericFunctionalReach1"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span><%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther1" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <div>Indicate all factors influencing the patient's progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.</div>
                <div><%= Html.TextArea(Model.Type + "_GenericFactors", data.AnswerOrEmptyString("GenericFactors"), new { @class = "fill", @id = Model.Type + "_GenericFactors" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <div>Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician's professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)</div>
                <div><%= Html.TextArea(Model.Type + "_GenericExpectations", data.AnswerOrEmptyString("GenericExpectations"), new { @class = "fill", @id = Model.Type + "_GenericExpectations" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <div>Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.</div>
                <div><%= Html.TextArea(Model.Type + "_GenericRecommendations", data.AnswerOrEmptyString("GenericRecommendations"), new { @class = "fill", @id = Model.Type + "_GenericRecommendations" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <%= Html.RadioButton(Model.Type + "_GenericChangedPOC", "1", data.AnswerOrEmptyString("GenericChangedPOC").Equals("1"), new { @id = Model.Type + "_GenericChangedPOC1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1" class="inline-radio">POC UNCHANGED (PHYSICIAN SIGNATURE NOT REQUIRED) /</label>
                    <%= Html.RadioButton(Model.Type + "_GenericChangedPOC", "0", data.AnswerOrEmptyString("GenericChangedPOC").Equals("0"), new { @id = Model.Type + "_GenericChangedPOC0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs0" class="inline-radio">POC CHANGED (PHYSICIAN SIGNATURE REQUIRED)</label>
                
            </td>
        </tr>
        <tr>
            <th colspan="5">Care Coordination</th>
            <th colspan="5">Skilled Care Provided This Visit</th>
        </tr>
        <tr>
            <td colspan="5">
            <%= Html.Templates(Model.Type + "_CareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCareCoordination" })%>
            <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%></td>
            <td colspan="5">
            <%= Html.Templates(Model.Type + "_SkilledCareProvidedTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSkilledCareProvided" })%>
            <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.ContainsKey("GenericSkilledCareProvided") ? data["GenericSkilledCareProvided"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%></td>
        </tr>       
    </tbody>
</table>