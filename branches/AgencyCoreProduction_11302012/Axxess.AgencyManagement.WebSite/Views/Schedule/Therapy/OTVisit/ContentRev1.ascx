﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Homebound</th>
            <th colspan="2">Functional Limitations</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Pain Assessment</th>
            <th colspan="2">Objective</th>
        </tr>
        <tr>
            <td colspan="2" rowspan="3"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Teaching</th>
        </tr>
        <tr>
            <td colspan="2">
                <%  string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
                <input type="hidden" name=Model.Type + "_GenericTeaching" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching1' class='float-left radio' name='{1}_GenericTeaching' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching1" class="radio">Patient/Family</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching2' class='float-left radio' name={1}_GenericTeaching' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching2" class="radio">Caregiver</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching3' class='float-left radio' name='{1}_GenericTeaching' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching3" class="radio">Correct Use of Adaptive Equipment</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching4' class='float-left radio' name='{1}_GenericTeaching' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching4" class="radio">Safety Technique</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching5' class='float-left radio' name='{1}_GenericTeaching' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching5" class="radio">ADLs</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching6' class='float-left radio' name='{1}_GenericTeaching' value='6' type='checkbox' {0} />", genericTeaching.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching6" class="radio">HEP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching7' class='float-left radio' name='{1}_GenericTeaching' value='7' type='checkbox' {0} />", genericTeaching.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching7" class="radio">Correct Use of Assistive Device</label>
                            </td>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other: (modalities, DME/AE need, consults, etc)</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">ADL Training</th>
            <th colspan="2">Neuromuscular Reeducation</th>
        </tr>
        <tr>
            <td colspan="2" rowspan="3">
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLBathing" class="float-left">Bathing:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLBathing", data.AnswerOrEmptyString("GenericADLBathing"), new { @class = "sn", @id = Model.Type + "_GenericADLBathing" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLUEDressing" class="float-left">UE Dressing:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.AnswerOrEmptyString("GenericADLUEDressing"), new { @class = "sn", @id = Model.Type + "_GenericADLUEDressing" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLLEDressing" class="float-left">LE Dressing:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.AnswerOrEmptyString("GenericADLLEDressing"), new { @class = "sn", @id = Model.Type + "_GenericADLLEDressing" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.AnswerOrEmptyString("GenericADLGrooming"), new { @class = "sn", @id = Model.Type + "_GenericADLGrooming" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLToileting", data.AnswerOrEmptyString("GenericADLToileting"), new { @class = "sn", @id = Model.Type + "_GenericADLToileting" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLFeeding" class="float-left">Feeding:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.AnswerOrEmptyString("GenericADLFeeding"), new { @class = "sn", @id = Model.Type + "_GenericADLFeeding" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal Prep:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.AnswerOrEmptyString("GenericADLMealPrep"), new { @class = "sn", @id = Model.Type + "_GenericADLMealPrep" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLHouseCleaning" class="float-left">House Cleaning:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.AnswerOrEmptyString("GenericADLHouseCleaning"), new { @class = "sn", @id = Model.Type + "_GenericADLHouseCleaning" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLHouseSafety" class="float-left">House Safety:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLHouseSafety", data.AnswerOrEmptyString("GenericADLHouseSafety"), new { @class = "sn", @id = Model.Type + "_GenericADLHouseSafety" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="float-left">Adaptive Equipment/Assistive Device Use or Needs:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.AnswerOrEmptyString("GenericADLAdaptiveEquipment"), new { @class = "sn", @id = Model.Type + "_GenericADLAdaptiveEquipment" })%></div>
                </div>
            </td>
            <td colspan="2">
                <%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="float-left">Sitting Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="float-left">Standing Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div>
                <div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
                    <%= string.Format("<input id='{1}_GenericUEWeightBearing1' class='radio' name='{1}_GenericUEWeightBearing' value='1' type='checkbox' {0} />", genericUEWeightBearing.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericUEWeightBearing1">UE Weight-Bearing Activities</label>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="2">Therapeutic Exercise</th>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tbody>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericTherapeuticExerciseROM" class="float-left">ROM</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseROM"), new { @id = Model.Type + "_GenericTherapeuticExerciseROM" })%></td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMSet"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseROMSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMReps"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseROMReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMReps">reps</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROM" class="float-left">AROM/AAROM</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROM"), new { @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROM" })%></td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMSet"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMReps"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROMReps">reps</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericTherapeuticExerciseResistive" class="float-left">Resistive (Type)</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistive", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistive"), new { @id = Model.Type + "_GenericTherapeuticExerciseResistive" })%></td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveSet"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseResistiveSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveReps"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseResistiveReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseResistiveReps">reps</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericTherapeuticExerciseStretching" class="float-left">Stretching</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretching", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretching"), new { @id = Model.Type + "_GenericTherapeuticExerciseStretching" })%></td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingSet"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseStretchingSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingReps"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseStretchingReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseStretchingReps">reps</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericTherapeuticExerciseOther" class="float-left">Other:</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseOther", data.AnswerOrEmptyString("GenericTherapeuticExerciseOther"), new { @id = Model.Type + "_GenericTherapeuticExerciseOther" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        
        <tr>
            <th colspan="2">Therapeutic/Dynamic Activities</th>
            <th colspan="2">Assessment</th>
        </tr>
        <tr>
            <td colspan="2" rowspan="3">
                <div class="padnoterow">
                    <label class="float-left">Bed Mobility:</label>
                    <div class="margin">
                        <label for="<%= Model.Type %>_GenericBedMobilityRollingReps" class="float-left">Rolling:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityRollingReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingReps", data.AnswerOrEmptyString("GenericBedMobilityRollingReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityRollingAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps" class="float-left">Supine to Sit:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitReps", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilitySupineToSitReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitAssist"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilitySupineToSitAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps" class="float-left">Dynamic Reaching:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingReps", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityDynamicReachingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingAssist", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingAssist"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityDynamicReachingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilityCoordReps" class="float-left">Gross/Fine Motor Coord:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityCoordReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordReps", data.AnswerOrEmptyString("GenericBedMobilityCoordReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityCoordReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityCoordAssist"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityCoordAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityCoordAssist">% assist</label>
                        </div>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericTransfersType" class="float-left">Transfers:</label>
                    <div class="float-right">
                        <label for="<%= Model.Type %>_GenericTransfersType">Type:</label>
                        <%= Html.TextBox(Model.Type + "_GenericTransfersType", data.AnswerOrEmptyString("GenericTransfersType"), new { @id = Model.Type + "_GenericTransfersType" })%>
                        <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps">X</label>
                        <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps" })%>
                        <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordAssist"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist" })%>
                        <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordAssist">% assist</label>
                    </div>
                    <div class="clear"></div>
                    <div class="margin">
                        <label class="float-left">Correct Unfolding:</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "1", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("1"), new { @id = Model.Type + "_GenericCorrectUnfolding1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectUnfolding1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "0", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("0"), new { @id = Model.Type + "_GenericCorrectUnfolding0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectUnfolding0" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <label class="float-left">Correct Foot Placement:</label> 
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "1", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("1") ? true : false, new { @id = Model.Type + "_GenericCorrectFootPlacement1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectFootPlacement1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "0", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("0") ? true : false, new { @id = Model.Type + "_GenericCorrectFootPlacement0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectFootPlacement0" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <label class="float-left">Assistive Device:</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "1", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1"), new { @id = Model.Type + "_GenericAssistiveDevice1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericAssistiveDevice1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "0", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0"), new { @id = Model.Type + "_GenericAssistiveDevice0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericAssistiveDevice0" class="inline-radio">No</label>
                        </div>
                    </div>
                </div>
            </td>
            <td colspan="2">
            <%= Html.Templates(Model.Type + "_GenericAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericAssessment" })%>
            <%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"),6,20, new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="2">Narrative</th>
        </tr>
        <tr>
            <td colspan="2"><%= Html.Templates(Model.Type + "_GenericNarrativeTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericNarrative" })%>
            <%= Html.TextArea(Model.Type + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), 6, 20, new { @id = Model.Type + "_GenericNarrative", @class = "fill" })%></td>
        
        </tr>
        <tr>
            <th colspan="2">W/C Training</th>
            <th colspan="2">Plan</th>
        </tr>
        <tr>
            <td colspan="2">
                <%  string[] genericPropulsionWith = data.AnswerArray("GenericPropulsionWith"); %>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label class="float-left">Propulsion with</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericPropulsionWith" value="" />
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith1' class='float-left radio' name='{1}_GenericPropulsionWith' value='1' type='checkbox' {0} />", genericPropulsionWith.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith1" class="radio">RUE</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith2' class='float-left radio' name='{1}_GenericPropulsionWith' value='2' type='checkbox' {0} />", genericPropulsionWith.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith2" class="radio">LUE</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith3' class='float-left radio' name='{1}_GenericPropulsionWith' value='3' type='checkbox' {0} />", genericPropulsionWith.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith3" class="radio">BUE</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith4' class='float-left radio' name='{1}_GenericPropulsionWith' value='4' type='checkbox' {0} />", genericPropulsionWith.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith4" class="radio">RLE</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith5' class='float-left radio' name='{1}_GenericPropulsionWith' value='5' type='checkbox' {0} />", genericPropulsionWith.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith5" class="radio">LLE</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith6' class='float-left radio' name='{1}_GenericPropulsionWith' value='6' type='checkbox' {0} />", genericPropulsionWith.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith6" class="radio">BLE</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericDistanceFT" class="float-left">Distance</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericDistanceFT", data.AnswerOrEmptyString("GenericDistanceFT"), new { @class = "sn", @id = Model.Type + "_GenericDistanceFT" })%>
                        <label for="<%= Model.Type %>_GenericDistanceFT">ft</label>
                        <label for="<%= Model.Type %>_GenericDistanceFTReps">x</label>
                        <%= Html.TextBox(Model.Type + "_GenericDistanceFTReps", data.AnswerOrEmptyString("GenericDistanceFTReps"), new { @class = "sn", @id = Model.Type + "_GenericDistanceFTReps" })%>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericManagement" class="float-left">Management</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericManagement", data.AnswerOrEmptyString("GenericManagement"), new { @id = Model.Type + "_GenericManagement" })%>
                        <%= Html.TextBox(Model.Type + "_GenericManagementAssist", data.AnswerOrEmptyString("GenericManagementAssist"), new { @class = "sn", @id = Model.Type + "_GenericManagementAssist" })%>
                        <label for="<%= Model.Type %>_GenericManagementAssist">% assist</label>
                    </div>
                </div>
            </td>
            <td colspan="2">
                <%  string[] genericPlan = data.AnswerArray("GenericPlan"); %>
                <input type="hidden" name=Model.Type + "_GenericPlan" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan1' class='float-left radio' name='{1}_GenericPlan' value='1' type='checkbox' {0} />", genericPlan.Contains("1").ToChecked(),Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan1" class="radio">Continue Prescribed Plan</label>
                            </td>
                            <td colspan="2">
                                <%= string.Format("<input id='{1}_GenericPlan6' class='float-left radio' name='{1}_GenericPlan' value='6' type='checkbox' {0} />", genericPlan.Contains("6").ToChecked(),Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan6" class="radio">Change Prescribed Plan</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPlanChangePrescribed", data.AnswerOrEmptyString("GenericPlanChangePrescribed"), new { @id = Model.Type + "_GenericPlanChangePrescribed" })%></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan2' class='float-left radio' name='{1}_GenericPlan' value='2' type='checkbox' {0} />", genericPlan.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan2" class="radio">Plan Discharge</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan3' class='float-left radio' name='{1}_GenericPlan' value='3' type='checkbox' {0} />", genericPlan.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan3" class="radio">In Progress</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan4' class='float-left radio' name='{1}_GenericPlan' value='4' type='checkbox' {0} />", genericPlan.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan4" class="radio">As of Today</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <%= string.Format("<input id='{1}_GenericPlan5' class='float-left radio' name='{1}_GenericPlan' value='5' type='checkbox' {0} />", genericPlan.Contains("5").ToChecked(), Model.Type)%>
                                <span class="float-left">
                                    <label for="<%= Model.Type %>_GenericPlan5">Patient/Family Notified</label>
                                    <%= Html.TextBox(Model.Type + "_GenericPlanPtDaysNotice", data.AnswerOrDefault("GenericPlanPtDaysNotice", "5"), new { @id = Model.Type + "_GenericPlanPtDaysNotice", @class = "small-digit" })%>
                                    <label for="<%= Model.Type %>_GenericPlan5">Days Prior to Discharge</label>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="padnoterow">
                    <div class="float-left">
                        <strong>Agency Notification</strong>
                        <%= Html.TextBox(Model.Type + "_GenericPlanAgencyDaysNotice", data.AnswerOrDefault("GenericPlanAgencyDaysNotice", "5"), new { @id = Model.Type + "_GenericPlanAgencyDaysNotice", @class = "small-digit" })%>
                        <strong>Days Prior?</strong>
                    </div>
                    <div class="float-right">
                        <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "1", data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("1"), new { @id = Model.Type + "_GenericPlanIsAgencyNotification1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification1" class="inline-radio">Yes</label>
                        <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "0", data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("0"), new { @id = Model.Type + "_GenericPlanIsAgencyNotification0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification0" class="inline-radio">No</label>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>