﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EST Reassessment " +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EST Reassessment%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
        
        
printview.addsection(            
            printview.span("Homebound Reason:",true) +
            printview.col(2,
                printview.checkbox("Needs assistance for all activities",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Residual weakness",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Requires assistance to ambulate",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Confusion, unable to go out of home alone",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Unable to safely leave home unassisted",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Severe SOB, SOB upon exertion",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Unable to safely leave home unassisted",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Medical Restrictions",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericHomeboundReasonOther") && data["GenericHomeboundReasonOther"].Answer.IsNotNullOrEmpty() ? " " + data["GenericHomeboundReasonOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.col(2,
                
                    printview.span("Orders for Evaluation Only:",true) +
                    printview.span("")+
                    printview.col(2,
                        printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericOrdersForEvaluationOnly") && data["GenericOrdersForEvaluationOnly"].Answer == "1" ? "true" : "false"%>) +
                        printview.checkbox("No",<%= data != null && data.ContainsKey("GenericOrdersForEvaluationOnly") && data["GenericOrdersForEvaluationOnly"].Answer == "0" ? "true" : "false"%>)) +
                printview.span("<%= data != null && data.ContainsKey("GenericOrdersForEvaluationOnly") && data["GenericOrdersForEvaluationOnly"].Answer == "0" && data.ContainsKey("GenericIfNoOrdersAre") && data["GenericIfNoOrdersAre"].Answer.IsNotNullOrEmpty() ? data["GenericIfNoOrdersAre"].Answer.Clean() : string.Empty %>",0,1) +
                
                    printview.span("Medical Diagnosis",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericMedicalDiagnosis") && data["GenericMedicalDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalDiagnosis"].Answer.Clean() : string.Empty %>",0,1)+
                
                    printview.span("Onset",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericMedicalDiagnosisOnsetDate") && data["GenericMedicalDiagnosisOnsetDate"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalDiagnosisOnsetDate"].Answer.Clean() : ""%>",0,1))+
            printview.span("Precautions",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMedicalPrecautions") && data["GenericMedicalPrecautions"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalPrecautions"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Prior Level of Functioning",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPriorLevelOfFunctioning") && data["GenericPriorLevelOfFunctioning"].Answer.IsNotNullOrEmpty() ? data["GenericPriorLevelOfFunctioning"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Living Situation/Support System",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.IsNotNullOrEmpty() ? data["GenericLivingSituation"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Describe pertinent medical/social history",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPreviousMedicalHistory") && data["GenericPreviousMedicalHistory"].Answer.IsNotNullOrEmpty() ? data["GenericPreviousMedicalHistory"].Answer.Clean() : string.Empty %>",0,2) +
            printview.col(2,
                printview.span("Safe Swallowing Evaluation?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericIsSSE") && data["GenericIsSSE"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericIsSSE") && data["GenericIsSSE"].Answer == "0" ? "true" : "false"%>))) +
            printview.span("<%= data != null && data.ContainsKey("GenericIsSSE") && data["GenericIsSSE"].Answer == "1" && data.ContainsKey("GenericGenericSSESpecify") && data["GenericGenericSSESpecify"].Answer.IsNotNullOrEmpty() ? data["GenericGenericSSESpecify"].Answer.Clean() : string.Empty %>",0,2) +
            printview.col(2,
                printview.span("Video Fluoroscopy?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericIsVideoFluoroscopy") && data["GenericIsVideoFluoroscopy"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericIsVideoFluoroscopy") && data["GenericIsVideoFluoroscopy"].Answer == "0" ? "true" : "false"%>))) +
            printview.span("<%= data != null && data.ContainsKey("GenericIsVideoFluoroscopy") && data["GenericIsVideoFluoroscopy"].Answer == "1" && data.ContainsKey("GenericVideoFluoroscopySpecify") && data["GenericVideoFluoroscopySpecify"].Answer.IsNotNullOrEmpty() ? data["GenericVideoFluoroscopySpecify"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Current Diet Texture",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericCurrentDietTexture") && data["GenericCurrentDietTexture"].Answer.IsNotNullOrEmpty() ? data["GenericCurrentDietTexture"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Pain (describe)",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainDescription") && data["GenericPainDescription"].Answer.IsNotNullOrEmpty() ? data["GenericPainDescription"].Answer.Clean() : string.Empty %>",0,2) +
            printview.col(2,
                printview.span("Impact on Therapy Care Plan",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericIsPainImpactCarePlan") && data["GenericIsPainImpactCarePlan"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericIsPainImpactCarePlan") && data["GenericIsPainImpactCarePlan"].Answer == "0" ? "true" : "false"%>)) +
                printview.span("Liquids",true) +
                printview.col(3,
                    printview.checkbox("Thin",<%= data != null && data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Thickened <%= data != null && data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer.Split(',').Contains("2") && data.ContainsKey("GenericLiquidsThick") && data["GenericLiquidsThick"].Answer.IsNotNullOrEmpty() ? data["GenericLiquidsThick"].Answer.Clean() : string.Empty %>",<%= data != null && data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                    printview.checkbox("Other <%= data != null && data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer.Split(',').Contains("3") && data.ContainsKey("GenericLiquidsOther") && data["GenericLiquidsOther"].Answer.IsNotNullOrEmpty() ? data["GenericLiquidsOther"].Answer.Clean() : string.Empty %>",<%= data != null && data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer.Split(',').Contains("3") ? "true" : "false"%>))),
            "Diagnosis");
     printview.addsection(
            printview.span("%3Cem class=%22small%22%3E4 &#8211; WFL (Within Functional Limits) / 3 &#8211; Mild Impairment / 2 &#8211; Moderate Impairment / 1 &#8211; Severe Impairment / 0 &#8211; Unable to Assess/Did Not Test%3C/em%3E"),
            "Speech/Language Evaluation");
     printview.addsubsection(
            printview.col(2,
                printview.span("Orientation <%= data != null && data.ContainsKey("GenericOrientationScore") && data["GenericOrientationScore"].Answer.IsNotNullOrEmpty() ? data["GenericOrientationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Attention Span <%= data != null && data.ContainsKey("GenericAttentionSpanScore") && data["GenericAttentionSpanScore"].Answer.IsNotNullOrEmpty() ? data["GenericAttentionSpanScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Short Term Memory <%= data != null && data.ContainsKey("GenericShortTermMemoryScore") && data["GenericShortTermMemoryScore"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermMemoryScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Long Term Memory <%= data != null && data.ContainsKey("GenericLongTermMemoryScore") && data["GenericLongTermMemoryScore"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermMemoryScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Judgment <%= data != null && data.ContainsKey("GenericJudgmentScore") && data["GenericJudgmentScore"].Answer.IsNotNullOrEmpty() ? data["GenericJudgmentScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Problem Solving <%= data != null && data.ContainsKey("GenericProblemSolvingScore") && data["GenericProblemSolvingScore"].Answer.IsNotNullOrEmpty() ? data["GenericProblemSolvingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Organization <%= data != null && data.ContainsKey("GenericOrganizationScore") && data["GenericOrganizationScore"].Answer.IsNotNullOrEmpty() ? data["GenericOrganizationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOther") && data["GenericCognitionFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %> <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOtherScore") && data["GenericCognitionFunctionEvaluatedOtherScore"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOtherScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedComment") && data["GenericCognitionFunctionEvaluatedComment"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedComment"].Answer.Clean() : string.Empty %>",0,2),
            "Cognition Function Evaluated(Prior Status)",2); 
     printview.addsubsection(
            printview.col(2,
                printview.span("Orientation <%= data != null && data.ContainsKey("GenericOrientationScore1") && data["GenericOrientationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericOrientationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Attention Span <%= data != null && data.ContainsKey("GenericAttentionSpanScore1") && data["GenericAttentionSpanScore1"].Answer.IsNotNullOrEmpty() ? data["GenericAttentionSpanScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Short Term Memory <%= data != null && data.ContainsKey("GenericShortTermMemoryScore1") && data["GenericShortTermMemoryScore1"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermMemoryScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Long Term Memory <%= data != null && data.ContainsKey("GenericLongTermMemoryScore1") && data["GenericLongTermMemoryScore1"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermMemoryScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Judgment <%= data != null && data.ContainsKey("GenericJudgmentScore1") && data["GenericJudgmentScore1"].Answer.IsNotNullOrEmpty() ? data["GenericJudgmentScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Problem Solving <%= data != null && data.ContainsKey("GenericProblemSolvingScore1") && data["GenericProblemSolvingScore1"].Answer.IsNotNullOrEmpty() ? data["GenericProblemSolvingScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Organization <%= data != null && data.ContainsKey("GenericOrganizationScore1") && data["GenericOrganizationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericOrganizationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOther1") && data["GenericCognitionFunctionEvaluatedOther1"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOther1"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %> <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOtherScore1") && data["GenericCognitionFunctionEvaluatedOtherScore1"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOtherScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedComment1") && data["GenericCognitionFunctionEvaluatedComment1"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Cognition Function Evaluated(Current Status)"); 
     printview.addsubsection(
            printview.col(2,
                printview.span("Oral/Facial Exam <%= data != null && data.ContainsKey("GenericOralFacialExamScore") && data["GenericOralFacialExamScore"].Answer.IsNotNullOrEmpty() ? data["GenericOralFacialExamScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Articulation <%= data != null && data.ContainsKey("GenericArticulationScore") && data["GenericArticulationScore"].Answer.IsNotNullOrEmpty() ? data["GenericArticulationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Prosody <%= data != null && data.ContainsKey("GenericProsodyScore") && data["GenericProsodyScore"].Answer.IsNotNullOrEmpty() ? data["GenericProsodyScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Voice/Respiration <%= data != null && data.ContainsKey("GenericVoiceRespirationScore") && data["GenericVoiceRespirationScore"].Answer.IsNotNullOrEmpty() ? data["GenericVoiceRespirationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Intelligibility <%= data != null && data.ContainsKey("GenericSpeechIntelligibilityScore") && data["GenericSpeechIntelligibilityScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechIntelligibilityScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOther") && data["GenericSpeechFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOtherScore") && data["GenericSpeechFunctionEvaluatedOtherScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOtherScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedComment") && data["GenericSpeechFunctionEvaluatedComment"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedComment"].Answer.Clean() : string.Empty %>",0,2),
            "Speech/Voice Function Evaluated(Prior Status)",2);   
     printview.addsubsection(
            printview.col(2,
                printview.span("Oral/Facial Exam <%= data != null && data.ContainsKey("GenericOralFacialExamScore1") && data["GenericOralFacialExamScore1"].Answer.IsNotNullOrEmpty() ? data["GenericOralFacialExamScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Articulation <%= data != null && data.ContainsKey("GenericArticulationScore1") && data["GenericArticulationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericArticulationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Prosody <%= data != null && data.ContainsKey("GenericProsodyScore1") && data["GenericProsodyScore1"].Answer.IsNotNullOrEmpty() ? data["GenericProsodyScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Voice/Respiration <%= data != null && data.ContainsKey("GenericVoiceRespirationScore1") && data["GenericVoiceRespirationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericVoiceRespirationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Intelligibility <%= data != null && data.ContainsKey("GenericSpeechIntelligibilityScore1") && data["GenericSpeechIntelligibilityScore1"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechIntelligibilityScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOther1") && data["GenericSpeechFunctionEvaluatedOther1"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOther1"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOtherScore1") && data["GenericSpeechFunctionEvaluatedOtherScore1"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOtherScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedComment1") && data["GenericSpeechFunctionEvaluatedComment1"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Speech/Voice Function Evaluated(Current Status)");  
            
     printview.addsubsection(
            printview.col(2,
                printview.span("Word Discrimination <%= data != null && data.ContainsKey("GenericWordDiscriminationScore") && data["GenericWordDiscriminationScore"].Answer.IsNotNullOrEmpty() ? data["GenericWordDiscriminationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("One Step Directions <%= data != null && data.ContainsKey("GenericOneStepDirectionsScore") && data["GenericOneStepDirectionsScore"].Answer.IsNotNullOrEmpty() ? data["GenericOneStepDirectionsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Two Step Directions <%= data != null && data.ContainsKey("GenericTwoStepDirectionsScore") && data["GenericTwoStepDirectionsScore"].Answer.IsNotNullOrEmpty() ? data["GenericTwoStepDirectionsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericComplexSentencesScore") && data["GenericComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericConversationScore") && data["GenericConversationScore"].Answer.IsNotNullOrEmpty() ? data["GenericConversationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Reading <%= data != null && data.ContainsKey("GenericSpeechReadingScore") && data["GenericSpeechReadingScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechReadingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericACFEComment") && data["GenericACFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericACFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Auditory Comprehension Function Evaluated(Prior Status)",2);   
     printview.addsubsection(
            printview.col(2,
                printview.span("Word Discrimination <%= data != null && data.ContainsKey("GenericWordDiscriminationScore1") && data["GenericWordDiscriminationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericWordDiscriminationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("One Step Directions <%= data != null && data.ContainsKey("GenericOneStepDirectionsScore1") && data["GenericOneStepDirectionsScore1"].Answer.IsNotNullOrEmpty() ? data["GenericOneStepDirectionsScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Two Step Directions <%= data != null && data.ContainsKey("GenericTwoStepDirectionsScore1") && data["GenericTwoStepDirectionsScore1"].Answer.IsNotNullOrEmpty() ? data["GenericTwoStepDirectionsScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericComplexSentencesScore1") && data["GenericComplexSentencesScore1"].Answer.IsNotNullOrEmpty() ? data["GenericComplexSentencesScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericConversationScore1") && data["GenericConversationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericConversationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Reading <%= data != null && data.ContainsKey("GenericSpeechReadingScore1") && data["GenericSpeechReadingScore1"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechReadingScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericACFEComment1") && data["GenericACFEComment1"].Answer.IsNotNullOrEmpty() ? data["GenericACFEComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Auditory Comprehension Function Evaluated(Current Status)");   
     
     printview.addsubsection(
            printview.col(2,
                printview.span("Chewing Ability <%= data != null && data.ContainsKey("GenericChewingAbilityScore") && data["GenericChewingAbilityScore"].Answer.IsNotNullOrEmpty() ? data["GenericChewingAbilityScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Oral Stage Management <%= data != null && data.ContainsKey("GenericOralStageManagementScore") && data["GenericOralStageManagementScore"].Answer.IsNotNullOrEmpty() ? data["GenericOralStageManagementScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Pharyngeal Stage Management <%= data != null && data.ContainsKey("GenericPharyngealStageManagementScore") && data["GenericPharyngealStageManagementScore"].Answer.IsNotNullOrEmpty() ? data["GenericPharyngealStageManagementScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Reflex Time <%= data != null && data.ContainsKey("GenericReflexTimeScore") && data["GenericReflexTimeScore"].Answer.IsNotNullOrEmpty() ? data["GenericReflexTimeScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedOther") && data["GenericSwallowingFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>  <%=data["GenericSwallowingFunctionEvaluatedOtherScore"].Answer.Clean()%>") +
                printview.span(""))+
                printview.span("Comment:",true)+
                printview.span("<%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedComment") && data["GenericSwallowingFunctionEvaluatedComment"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedComment"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>",0,2),
                "Swallowing Function Evaluated(Prior Status)",2);
     printview.addsubsection(
            printview.col(2,
                printview.span("Chewing Ability <%= data != null && data.ContainsKey("GenericChewingAbilityScore1") && data["GenericChewingAbilityScore1"].Answer.IsNotNullOrEmpty() ? data["GenericChewingAbilityScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Oral Stage Management <%= data != null && data.ContainsKey("GenericOralStageManagementScore1") && data["GenericOralStageManagementScore1"].Answer.IsNotNullOrEmpty() ? data["GenericOralStageManagementScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Pharyngeal Stage Management <%= data != null && data.ContainsKey("GenericPharyngealStageManagementScore1") && data["GenericPharyngealStageManagementScore1"].Answer.IsNotNullOrEmpty() ? data["GenericPharyngealStageManagementScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Reflex Time <%= data != null && data.ContainsKey("GenericReflexTimeScore1") && data["GenericReflexTimeScore1"].Answer.IsNotNullOrEmpty() ? data["GenericReflexTimeScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedOther1") && data["GenericSwallowingFunctionEvaluatedOther1"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedOther1"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>  <%=data["GenericSwallowingFunctionEvaluatedOtherScore1"].Answer.Clean()%>") +
                printview.span(""))+
                printview.span("Comment:",true)+
                printview.span("<%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedComment1") && data["GenericSwallowingFunctionEvaluatedComment1"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedComment1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>",0,2),
                "Swallowing Function Evaluated(Curret Status)");
    printview.addsubsection(
            printview.col(2,
                printview.span("Augmentative Methods <%= data != null && data.ContainsKey("GenericAugmentativeMethodsScore") && data["GenericAugmentativeMethodsScore"].Answer.IsNotNullOrEmpty() ? data["GenericAugmentativeMethodsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Naming <%= data != null && data.ContainsKey("GenericNamingScore") && data["GenericNamingScore"].Answer.IsNotNullOrEmpty() ? data["GenericNamingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Appropriate <%= data != null && data.ContainsKey("GenericAppropriateScore") && data["GenericAppropriateScore"].Answer.IsNotNullOrEmpty() ? data["GenericAppropriateScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericVEFEComplexSentencesScore") && data["GenericVEFEComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericVEFEConversationScore") && data["GenericVEFEConversationScore"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEConversationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericVEFEComment") && data["GenericVEFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Verbal Expression Function Evaluated(Prior Status)",2);
    printview.addsubsection(
            printview.col(2,
                printview.span("Augmentative Methods <%= data != null && data.ContainsKey("GenericAugmentativeMethodsScore1") && data["GenericAugmentativeMethodsScore1"].Answer.IsNotNullOrEmpty() ? data["GenericAugmentativeMethodsScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Naming <%= data != null && data.ContainsKey("GenericNamingScore1") && data["GenericNamingScore1"].Answer.IsNotNullOrEmpty() ? data["GenericNamingScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Appropriate <%= data != null && data.ContainsKey("GenericAppropriateScore1") && data["GenericAppropriateScore1"].Answer.IsNotNullOrEmpty() ? data["GenericAppropriateScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericVEFEComplexSentencesScore1") && data["GenericVEFEComplexSentencesScore1"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComplexSentencesScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericVEFEConversationScore1") && data["GenericVEFEConversationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEConversationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericVEFEComment1") && data["GenericVEFEComment1"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Verbal Expression Function Evaluated(Current Status)");
     
     printview.addsubsection(
            printview.col(2,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericRFELettersNumbersScore") && data["GenericRFELettersNumbersScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFELettersNumbersScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericRFEWordsScore") && data["GenericRFEWordsScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFEWordsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Sentences <%= data != null && data.ContainsKey("GenericRFESimpleSentencesScore") && data["GenericRFESimpleSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFESimpleSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericRFEComplexSentencesScore") && data["GenericRFEComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Paragraph <%= data != null && data.ContainsKey("GenericParagraphScore") && data["GenericParagraphScore"].Answer.IsNotNullOrEmpty() ? data["GenericParagraphScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")+
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericRFEComment") && data["GenericRFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Reading Function Evaluated(Prior Status)",2);
     printview.addsubsection(
            printview.col(2,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericRFELettersNumbersScore1") && data["GenericRFELettersNumbersScore1"].Answer.IsNotNullOrEmpty() ? data["GenericRFELettersNumbersScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericRFEWordsScore1") && data["GenericRFEWordsScore1"].Answer.IsNotNullOrEmpty() ? data["GenericRFEWordsScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Sentences <%= data != null && data.ContainsKey("GenericRFESimpleSentencesScore1") && data["GenericRFESimpleSentencesScore1"].Answer.IsNotNullOrEmpty() ? data["GenericRFESimpleSentencesScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericRFEComplexSentencesScore1") && data["GenericRFEComplexSentencesScore1"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComplexSentencesScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Paragraph <%= data != null && data.ContainsKey("GenericParagraphScore1") && data["GenericParagraphScore1"].Answer.IsNotNullOrEmpty() ? data["GenericParagraphScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")+
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericRFEComment1") && data["GenericRFEComment1"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Reading Function Evaluated(Current Status)");
            
     printview.addsubsection(
            printview.col(2,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericWFELettersNumbersScore") && data["GenericWFELettersNumbersScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFELettersNumbersScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericWFEWordsScore") && data["GenericWFEWordsScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFEWordsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Sentences <%= data != null && data.ContainsKey("GenericWFESentencesScore") && data["GenericWFESentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFESentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Spelling <%= data != null && data.ContainsKey("GenericWFESpellingScore") && data["GenericWFESpellingScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFESpellingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Formulation <%= data != null && data.ContainsKey("GenericFormulationScore") && data["GenericFormulationScore"].Answer.IsNotNullOrEmpty() ? data["GenericFormulationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Addition/Subtraction <%= data != null && data.ContainsKey("GenericSimpleAdditionSubtractionScore") && data["GenericSimpleAdditionSubtractionScore"].Answer.IsNotNullOrEmpty() ? data["GenericSimpleAdditionSubtractionScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWFEComment") && data["GenericWFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericWFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Writing Function Evaluated(Prior Status)",2);
     printview.addsubsection(
            printview.col(2,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericWFELettersNumbersScore1") && data["GenericWFELettersNumbersScore1"].Answer.IsNotNullOrEmpty() ? data["GenericWFELettersNumbersScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericWFEWordsScore1") && data["GenericWFEWordsScore1"].Answer.IsNotNullOrEmpty() ? data["GenericWFEWordsScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Sentences <%= data != null && data.ContainsKey("GenericWFESentencesScore1") && data["GenericWFESentencesScore1"].Answer.IsNotNullOrEmpty() ? data["GenericWFESentencesScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Spelling <%= data != null && data.ContainsKey("GenericWFESpellingScore1") && data["GenericWFESpellingScore1"].Answer.IsNotNullOrEmpty() ? data["GenericWFESpellingScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Formulation <%= data != null && data.ContainsKey("GenericFormulationScore1") && data["GenericFormulationScore1"].Answer.IsNotNullOrEmpty() ? data["GenericFormulationScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Addition/Subtraction <%= data != null && data.ContainsKey("GenericSimpleAdditionSubtractionScore1") && data["GenericSimpleAdditionSubtractionScore1"].Answer.IsNotNullOrEmpty() ? data["GenericSimpleAdditionSubtractionScore1"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWFEComment1") && data["GenericWFEComment1"].Answer.IsNotNullOrEmpty() ? data["GenericWFEComment1"].Answer.Clean() : string.Empty %>",0,2),
            "Writing Function Evaluated(Current Status)");
            
      printview.addsection(
            printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoalsComment") ? data["GenericShortTermGoalsComment"].Answer.Clean() : string.Empty%>",0,2),
            "Short term goals");
        printview.addsection(
            printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoalsComment") ? data["GenericLongTermGoalsComment"].Answer.Clean() : string.Empty%>",0,2),
            "Long term goals");
            
      printview.addsection(
            printview.span("Assessment",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPatientDesiredOutcomes") && data["GenericPatientDesiredOutcomes"].Answer.IsNotNullOrEmpty() ? data["GenericPatientDesiredOutcomes"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Short Term Outcomes",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericShortTermOutcomes") && data["GenericShortTermOutcomes"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermOutcomes"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Long Term Outcomes",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericLongTermOutcomes") && data["GenericLongTermOutcomes"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermOutcomes"].Answer.Clean() : string.Empty %>",0,2),
            "");                
        printview.addsection(
            printview.col(2,
                printview.checkbox("Evaluation (C1)",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Language Processing",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Establish Rehab Program",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Food Texture Recommendations",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Given to Patient",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Safe Swallowing Evaluation",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Attached to Chart",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Therapy to Increase Articulation, Proficiency, Verbal Expression",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Patient/Family Education",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Lip, Tongue, Facial Exercises to Improve Swallowing/Vocal Skills",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Voice Disorders",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                printview.checkbox("Pain Management",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("12") ? "true" : "false"%>) +
                printview.checkbox("Speech Articulation Disorders",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("13") ? "true" : "false"%>) +
                printview.checkbox("Speech Dysphagia Instruction Program",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("14") ? "true" : "false"%>) +
                printview.checkbox("Dysphagia Treatments",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("15") ? "true" : "false"%>) +
                printview.checkbox("Care of Voice Prosthesis — Removal, Cleaning, Site Maint",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("16") ? "true" : "false"%>) +
                printview.checkbox("Language Disorders",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("17") ? "true" : "false"%>) +
                printview.checkbox("Teach/Develop Comm. System",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("18") ? "true" : "false"%>) +
                printview.checkbox("Aural Rehabilitation (C6)",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("19") ? "true" : "false"%>) +
                printview.checkbox("Trach Inst. and Care",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("20") ? "true" : "false"%>) +
                printview.checkbox("Non-Oral Communication (C8)",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("21") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("22") ? "true" : "false"%>)+
                printview.checkbox("Alaryngeal Speech Skills",<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.Split(',').Contains("23") ? "true" : "false"%>) +
                printview.span("")),
            "Plan of Care");
        printview.addsection(
            printview.col(2,
                printview.span("Frequency and Duration",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericFrequencyAndDuration") && data["GenericFrequencyAndDuration"].Answer.IsNotNullOrEmpty() ? data["GenericFrequencyAndDuration"].Answer.Clean() : string.Empty %>",0,1) +
                printview.span("Rehab Potential",true) +
                printview.col(3,
                    printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "0" ? "true" : "false"%>))));
        printview.addsection(
            printview.span("Equipment Recommendations",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericEquipmentRecommendations") && data["GenericEquipmentRecommendations"].Answer.IsNotNullOrEmpty() ? data["GenericEquipmentRecommendations"].Answer.Clean() : string.Empty %>",0,2));
        printview.addsection(
            printview.span("Safety Issues/ Instruction/ Education",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSafetyIssues") && data["GenericSafetyIssues"].Answer.IsNotNullOrEmpty() ? data["GenericSafetyIssues"].Answer.Clean() : string.Empty %>",0,2));
        printview.addsection(
            printview.span("Comments/ Additional Information",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericAdditionalInformation") && data["GenericAdditionalInformation"].Answer.IsNotNullOrEmpty() ? data["GenericAdditionalInformation"].Answer.Clean() : string.Empty %>",0,2));
        printview.addsection(
            printview.span("Patient/ Caregiver Response to Plan of Care",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericResponseToPOC") && data["GenericResponseToPOC"].Answer.IsNotNullOrEmpty() ? data["GenericResponseToPOC"].Answer.Clean() : string.Empty %>",0,2));
        printview.addsection(
            printview.col(5,
                printview.span("Discharge discussed with:",true) +
                printview.checkbox("Patient Family",<%= data != null && data.ContainsKey("GenericDischargeDiscussedWith") && data["GenericDischargeDiscussedWith"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Case Manager",<%= data != null && data.ContainsKey("GenericDischargeDiscussedWith") && data["GenericDischargeDiscussedWith"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Physician",<%= data != null && data.ContainsKey("GenericDischargeDiscussedWith") && data["GenericDischargeDiscussedWith"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data != null && data.ContainsKey("GenericDischargeDiscussedWithOther") && data["GenericDischargeDiscussedWithOther"].Answer.IsNotNullOrEmpty() ? data["GenericDischargeDiscussedWithOther"].Answer.Clean() : string.Empty %>",<%= data != null && data.ContainsKey("GenericDischargeDiscussedWith") && data["GenericDischargeDiscussedWith"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.span("Care Coordination", true) +
                printview.checkbox("Physician",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("PT",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("OT",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("ST",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.span("") +                
                printview.checkbox("MSW",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("SN",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                
                printview.span("<%=data != null && data.ContainsKey("GenericCareCoordinationOther") && data["GenericCareCoordination"].Answer.Split(',').Contains("7")  ? data["GenericCareCoordinationOther"].Answer.Clean() : ""%>"))+
                printview.span("<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.IsNotNullOrEmpty() ? data["GenericCareCoordination"].Answer.Clean() : string.Empty%>",0,2)+
            printview.span("Plan for Next Visit ",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPlanForNextVisit") && data["GenericPlanForNextVisit"].Answer.IsNotNullOrEmpty() ? data["GenericPlanForNextVisit"].Answer.Clean() : string.Empty%>",0,2)+
            printview.col(2,
                printview.span("PF/Caregiver aware and agreeable to POC",true)+
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCaregiverAgreeable") && data["GenericCaregiverAgreeable"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCaregiverAgreeable") && data["GenericCaregiverAgreeable"].Answer == "0" ? "true" : "false"%>))) );

            printview.addsection(
                printview.span("<%= data != null && data.ContainsKey("GenericTreatmentProvided") && data["GenericTreatmentProvided"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentProvided"].Answer.Clean() : string.Empty%>",0,3),
                "Skilled treatment provided on this visit");
                
            printview.addsection(
                printview.span("Indicate all factors influencing the patient's progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.",true)+
                printview.span("<%= data.AnswerOrEmptyString("GenericFactors").Clean() %>",0,2)+
                printview.span("Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician's professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)",true)+
                printview.span("<%= data.AnswerOrEmptyString("GenericExpectations").Clean() %>",0,2)+
                printview.span("Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.",true)+
                printview.span("<%= data.AnswerOrEmptyString("GenericRecommendations").Clean() %>",0,2),
                "");
            printview.addsection(
                printview.col(2,
                    printview.checkbox("POC UNCHANGED (PHYSICIAN SIGNATURE NOT REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("1").ToString().ToLower() %>) +
                    printview.checkbox("POC CHANGED (PHYSICIAN SIGNATURE REQUIRED)",<%= data.AnswerArray("GenericChangedPOC").Contains("0").ToString().ToLower() %>) ),
                    "");
                    
printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>


</body>
</html>