﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.STEvaluation.ToString(), "Speech Therapy Evaluation" },
    { DisciplineTasks.STDischarge.ToString(), "Speech Therapy Discharge" },
    { DisciplineTasks.STReEvaluation.ToString(), "Speech Therapy ReEvaluation" },
    { DisciplineTasks.STMaintenance.ToString(), "Speech Therapy Maintenance Visit" }
}; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %><% =dictonary.ContainsKey(Model.Type)? dictonary[Model.Type]:"" %><%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E" +
            "<%= dictonary.ContainsKey(Model.Type)? dictonary[Model.Type]:"" %>" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3ESpeech Therapy <%= Model.Type == "STDischarge" ? "Discharge" : (Model.Type == "STReEvaluation" ? "Re-" : "") + "Evaluation" %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        
        printview.addsection(
            printview.span("%3Cem class=%22small%22%3E4 &#8211; WFL (Within Functional Limits) / 3 &#8211; Mild Impairment / 2 &#8211; Moderate Impairment / 1 &#8211; Severe Impairment / 0 &#8211; Unable to Assess/Did Not Test%3C/em%3E"),
            "Patient's status upon discharge");
        printview.addsubsection(
            printview.col(2,
                printview.span("Orientation <%= data != null && data.ContainsKey("GenericOrientationScore") && data["GenericOrientationScore"].Answer.IsNotNullOrEmpty() ? data["GenericOrientationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Attention Span <%= data != null && data.ContainsKey("GenericAttentionSpanScore") && data["GenericAttentionSpanScore"].Answer.IsNotNullOrEmpty() ? data["GenericAttentionSpanScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Short Term Memory <%= data != null && data.ContainsKey("GenericShortTermMemoryScore") && data["GenericShortTermMemoryScore"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermMemoryScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Long Term Memory <%= data != null && data.ContainsKey("GenericLongTermMemoryScore") && data["GenericLongTermMemoryScore"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermMemoryScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Judgment <%= data != null && data.ContainsKey("GenericJudgmentScore") && data["GenericJudgmentScore"].Answer.IsNotNullOrEmpty() ? data["GenericJudgmentScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Problem Solving <%= data != null && data.ContainsKey("GenericProblemSolvingScore") && data["GenericProblemSolvingScore"].Answer.IsNotNullOrEmpty() ? data["GenericProblemSolvingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Organization <%= data != null && data.ContainsKey("GenericOrganizationScore") && data["GenericOrganizationScore"].Answer.IsNotNullOrEmpty() ? data["GenericOrganizationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOther") && data["GenericCognitionFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %> <%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedOtherScore") && data["GenericCognitionFunctionEvaluatedOtherScore"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedOtherScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericCognitionFunctionEvaluatedComment") && data["GenericCognitionFunctionEvaluatedComment"].Answer.IsNotNullOrEmpty() ? data["GenericCognitionFunctionEvaluatedComment"].Answer.Clean() : string.Empty %>",0,2),
            "Cognition Function Evaluated",2);
        printview.addsubsection(
            printview.col(2,
                printview.span("Oral/Facial Exam <%= data != null && data.ContainsKey("GenericOralFacialExamScore") && data["GenericOralFacialExamScore"].Answer.IsNotNullOrEmpty() ? data["GenericOralFacialExamScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Articulation <%= data != null && data.ContainsKey("GenericArticulationScore") && data["GenericArticulationScore"].Answer.IsNotNullOrEmpty() ? data["GenericArticulationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Prosody <%= data != null && data.ContainsKey("GenericProsodyScore") && data["GenericProsodyScore"].Answer.IsNotNullOrEmpty() ? data["GenericProsodyScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Voice/Respiration <%= data != null && data.ContainsKey("GenericVoiceRespirationScore") && data["GenericVoiceRespirationScore"].Answer.IsNotNullOrEmpty() ? data["GenericVoiceRespirationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Intelligibility <%= data != null && data.ContainsKey("GenericSpeechIntelligibilityScore") && data["GenericSpeechIntelligibilityScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechIntelligibilityScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOther") && data["GenericSpeechFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> <%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedOtherScore") && data["GenericSpeechFunctionEvaluatedOtherScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedOtherScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericSpeechFunctionEvaluatedComment") && data["GenericSpeechFunctionEvaluatedComment"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechFunctionEvaluatedComment"].Answer.Clean() : string.Empty %>",0,2),
            "Speech/Voice Function Evaluated");
        printview.addsubsection(
            printview.col(2,
                printview.span("Word Discrimination <%= data != null && data.ContainsKey("GenericWordDiscriminationScore") && data["GenericWordDiscriminationScore"].Answer.IsNotNullOrEmpty() ? data["GenericWordDiscriminationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("One Step Directions <%= data != null && data.ContainsKey("GenericOneStepDirectionsScore") && data["GenericOneStepDirectionsScore"].Answer.IsNotNullOrEmpty() ? data["GenericOneStepDirectionsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Two Step Directions <%= data != null && data.ContainsKey("GenericTwoStepDirectionsScore") && data["GenericTwoStepDirectionsScore"].Answer.IsNotNullOrEmpty() ? data["GenericTwoStepDirectionsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericComplexSentencesScore") && data["GenericComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericConversationScore") && data["GenericConversationScore"].Answer.IsNotNullOrEmpty() ? data["GenericConversationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Speech Reading <%= data != null && data.ContainsKey("GenericSpeechReadingScore") && data["GenericSpeechReadingScore"].Answer.IsNotNullOrEmpty() ? data["GenericSpeechReadingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericACFEComment") && data["GenericACFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericACFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Auditory Comprehension Function Evaluated",2);
        printview.addsubsection(
            printview.col(2,
                printview.span("Chewing Ability <%= data != null && data.ContainsKey("GenericChewingAbilityScore") && data["GenericChewingAbilityScore"].Answer.IsNotNullOrEmpty() ? data["GenericChewingAbilityScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Oral Stage Management <%= data != null && data.ContainsKey("GenericOralStageManagementScore") && data["GenericOralStageManagementScore"].Answer.IsNotNullOrEmpty() ? data["GenericOralStageManagementScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Pharyngeal Stage Management <%= data != null && data.ContainsKey("GenericPharyngealStageManagementScore") && data["GenericPharyngealStageManagementScore"].Answer.IsNotNullOrEmpty() ? data["GenericPharyngealStageManagementScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Reflex Time <%= data != null && data.ContainsKey("GenericReflexTimeScore") && data["GenericReflexTimeScore"].Answer.IsNotNullOrEmpty() ? data["GenericReflexTimeScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Other <%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedOther") && data["GenericSwallowingFunctionEvaluatedOther"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> ") +
                printview.span(""))+
                printview.span("Comment:",true)+
                printview.span("<%= data != null && data.ContainsKey("GenericSwallowingFunctionEvaluatedOtherScore") && data["GenericSwallowingFunctionEvaluatedOtherScore"].Answer.IsNotNullOrEmpty() ? data["GenericSwallowingFunctionEvaluatedOtherScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>",0,2),
                "Swallowing Function Evaluated");
        printview.addsubsection(
            printview.col(2,
                printview.span("Augmentative Methods <%= data != null && data.ContainsKey("GenericAugmentativeMethodsScore") && data["GenericAugmentativeMethodsScore"].Answer.IsNotNullOrEmpty() ? data["GenericAugmentativeMethodsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Naming <%= data != null && data.ContainsKey("GenericNamingScore") && data["GenericNamingScore"].Answer.IsNotNullOrEmpty() ? data["GenericNamingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Appropriate <%= data != null && data.ContainsKey("GenericAppropriateScore") && data["GenericAppropriateScore"].Answer.IsNotNullOrEmpty() ? data["GenericAppropriateScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericVEFEComplexSentencesScore") && data["GenericVEFEComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Conversation <%= data != null && data.ContainsKey("GenericVEFEConversationScore") && data["GenericVEFEConversationScore"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEConversationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericVEFEComment") && data["GenericVEFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericVEFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Verbal Expression Function Evaluated",2);
        printview.addsubsection(
            printview.col(2,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericRFELettersNumbersScore") && data["GenericRFELettersNumbersScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFELettersNumbersScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericRFEWordsScore") && data["GenericRFEWordsScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFEWordsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Sentences <%= data != null && data.ContainsKey("GenericRFESimpleSentencesScore") && data["GenericRFESimpleSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFESimpleSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Complex Sentences <%= data != null && data.ContainsKey("GenericRFEComplexSentencesScore") && data["GenericRFEComplexSentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComplexSentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Paragraph <%= data != null && data.ContainsKey("GenericParagraphScore") && data["GenericParagraphScore"].Answer.IsNotNullOrEmpty() ? data["GenericParagraphScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")+
                printview.span(""))+
            printview.span("Comments") +
            printview.span("<%= data != null && data.ContainsKey("GenericRFEComment") && data["GenericRFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericRFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Reading Function Evaluated");
        
        printview.addsection(
            printview.col(3,
                printview.span("Letters/Numbers <%= data != null && data.ContainsKey("GenericWFELettersNumbersScore") && data["GenericWFELettersNumbersScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFELettersNumbersScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Words <%= data != null && data.ContainsKey("GenericWFEWordsScore") && data["GenericWFEWordsScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFEWordsScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Sentences <%= data != null && data.ContainsKey("GenericWFESentencesScore") && data["GenericWFESentencesScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFESentencesScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Spelling <%= data != null && data.ContainsKey("GenericWFESpellingScore") && data["GenericWFESpellingScore"].Answer.IsNotNullOrEmpty() ? data["GenericWFESpellingScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Formulation <%= data != null && data.ContainsKey("GenericFormulationScore") && data["GenericFormulationScore"].Answer.IsNotNullOrEmpty() ? data["GenericFormulationScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>") +
                printview.span("Simple Addition/Subtraction <%= data != null && data.ContainsKey("GenericSimpleAdditionSubtractionScore") && data["GenericSimpleAdditionSubtractionScore"].Answer.IsNotNullOrEmpty() ? data["GenericSimpleAdditionSubtractionScore"].Answer.Clean() : "%3Cspan class=%22float-right blank%22 style=%22width:3em !important;margin-right:1em;%22%3E%3C/span%3E"%>")) +
            printview.span("Comments",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWFEComment") && data["GenericWFEComment"].Answer.IsNotNullOrEmpty() ? data["GenericWFEComment"].Answer.Clean() : string.Empty %>",0,2),
            "Writing Function Evaluated");
        
        printview.addsubsection(
            printview.col(2,
                printview.checkbox("Reached Maximum Potential",<%= data.AnswerArray("GenericReasonForDischarge").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Expired",<%= data.AnswerArray("GenericReasonForDischarge").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Per pt/family request",<%= data.AnswerArray("GenericReasonForDischarge").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("No longer Homebound",<%= data.AnswerArray("GenericReasonForDischarge").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Goals met",<%= data.AnswerArray("GenericReasonForDischarge").Contains("5").ToString().ToLower()%>)+
                printview.checkbox("Prolonged on hold status",<%= data.AnswerArray("GenericReasonForDischarge").Contains("6").ToString().ToLower()%>)+
                printview.checkbox("Moved out of service area",<%= data.AnswerArray("GenericReasonForDischarge").Contains("7").ToString().ToLower()%>)+
                printview.checkbox("Hospitalized",<%= data.AnswerArray("GenericReasonForDischarge").Contains("8").ToString().ToLower()%>)),
            "Reason for discharge",2);
            
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("ConditionOfDischarge").Clean()%>"),
            "Condition of patient upon discharge");
            
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("TreatmentProvided").Clean()%>"),
            "Skilled treatment provided this visit",2);
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("SummaryTreatmentProvided").Clean()%>"),
            "Summary of treatment provided");
        printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("ProgressMade").Clean()%>"),
            "Summary of progress made");
      
     <%
    }).Render(); %>
</html>
