﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using(Html.BeginForm("AddAsset", "Patient", FormMethod.Post, new { @id="UploadAsset_Form"})) { %>
<%= Html.Hidden("patientId", Model.Id)%>
<div class="wrapper main">
    <fieldset>
        <legend>Upload Patient Asset</legend>
        <div class="wide column">
            <div class="row align-center">
                <h1><%= Model.DisplayName %></h1>
            </div>
            <div class="row align-center">
                <label for="UploadAsset_Asset" class="float-left strong">New Asset</label>
                <div><input id="UploadAsset_Asset" type="file" name="Asset" multiple="multiple" /></div>
            </div>
            <div class="row align-center">
                <label class="float-left strong">File Name</label>
                <div><input type="text" /></div>
            </div>
            <div class="row align-center">
                <label for="UploadAsset_Cassification" class="float-left strong">Select Classification</label>
                <div id="UploadAsset_Classification"> 
                    <select>
                        <option>Hello</option>
                        <option>Hello1</option>
                    </select>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li>
                <a id="UploadAsset_Submit" href="javascript:void(0);" onclick="$('#UploadAsset_Form').submit()">Submit</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a>
            </li>
        </ul>
    </div>
</div>


<% } %>