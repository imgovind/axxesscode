﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Assets | <%= Model.DisplayName %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<Asset>()
        .Name("List_PatientAssets")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(c => c.FileName).ClientTemplate("<a href=\"/Asset/<#=Id#>\"><#=FileName#></a>").Title("File Name").Sortable(true);
            columns.Bound(c => c.CreatedDateFormatted).Title("Created").Width(120);
            columns.Bound(c => c.ModifiedDateFormatted).Title("Modified").Width(120);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("GetAssets", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
    $("#List_PatientAssets .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $("#List_PatientAssets .t-grid-content").css({ height: "auto", top: 59 });
</script>
