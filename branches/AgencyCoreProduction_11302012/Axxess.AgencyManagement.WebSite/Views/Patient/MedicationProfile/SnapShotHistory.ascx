﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Signed Medication Profiles | <%= Model.DisplayName %></span>
<div class="wrapper">
    <%= Html.Telerik().Grid<MedicationProfileHistory>()
        .Name("List_MedicationProfileSnapshotHistory_Grid")
        .DataKeys(keys => {
            keys.Add(m => m.Id);
        })
        .DataBinding(dataBinding => {
            dataBinding.Ajax()
                .Select("MedicationSnapshotHistory", "Patient", new { PatientId = Model.Id })
                .Update("UpdateMedicationSnapshotHistory", "Patient", new { PatientId = Model.Id })
                .Delete("DeleteMedicationSnapshotHistory", "Patient", new { PatientId = Model.Id });
        })
        .Columns(columns => {
            columns.Bound(m => m.UserName).Title("Signed By").Sortable(true).ReadOnly();
            columns.Bound(m => m.SignedDate).Title("Signed Date").Width(120).Sortable(true);
            columns.Bound(m => m.Id).Width(30).ReadOnly().ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ Url: '/Patient/MedicationProfileSnapshotPrint/<#=Id#>', PdfUrl: '/Patient/MedicationSnapshotPdf', PdfData: { 'id': '<#=Id#>' } });\"><span class=\"img icon print\"></span></a>").Sortable(false).Title("");
            columns.Command(commands => {
                if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { commands.Edit(); }
                if(Current.HasRight(Permissions.DeleteTasks) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { commands.Delete(); }
            }).Width(150).Title("Action");
        }).ClientEvents(e => e.OnRowDataBound("Patient.OnMedProfileSnapShotHistoryRowDataBound"))
        .Editable(editing => editing.Mode(GridEditMode.InLine)).Scrollable().Sortable() 
    %>
</div>
<script type="text/javascript">
    $(".t-grid-content").css({ 'height': 'auto' });
</script>

