﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Pending Patient Admissions | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("PendingAdmissionsXls", "Patient", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html.Telerik()
        .Grid<PendingPatient>()
        .Name("List_PatientPending_Grid")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(120);
            columns.Bound(p => p.DisplayName).Title("Patient").Sortable(false);
            columns.Bound(p => p.PrimaryInsuranceName).Title("Primary Insurance").Sortable(false);
            columns.Bound(p => p.Branch).Title("Branch").Sortable(false);
            columns.Bound(p => p.CreatedDateFormatted).Title("Date Added").Width(100).Sortable(false);
            columns.Bound(p => p.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowAdmitPatientModal('<#=Id#>', '<#=Type#>');\" class=\"\">Admit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowNonAdmitPatientModal('<#=Id#>');\" class=\"\">Non-Admit</a>").Title("Action").Width(180).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("PendingList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } %>
<script type="text/javascript">
    $("#List_PatientPending_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients) && !Current.IsAgencyFrozen) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>