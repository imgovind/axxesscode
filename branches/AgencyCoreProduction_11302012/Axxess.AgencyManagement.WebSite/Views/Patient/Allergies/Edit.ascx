﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Allergy>" %>
<% using (Html.BeginForm("UpdateAllergy", "Patient", FormMethod.Post, new { @id = "editAllergyForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Allergy_Id" })%>
<%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Allergy_ProfileId" })%>
<div class="wrapper main">
    
    <fieldset class="newallergy">
        <legend>Edit Allergy</legend>
        <div class="wide-column">
            <div class="row"><label for="Edit_Allergy_Name" class="float-left">Name:</label><div class="float-left"><%= Html.TextBox("Name", Model != null && Model.Name.IsNotNullOrEmpty() ? Model.Name : string.Empty, new { @id = "Edit_Allergy_Name", @class = "longtext input_wrapper required", @maxlength = "120" })%></div></div>
            <div class="row"><label for="Edit_Allergy_Type" class="float-left">Type:</label><div class="float-left"><%= Html.TextBox("Type", Model != null && Model.Type.IsNotNullOrEmpty() ? Model.Type : string.Empty, new { @id = "Edit_Allergy_Type", @class = "input_wrapper", @maxlength = "50" })%><br /><em>(e.g. Medication, Food, Animal, Plants, Environmental)</em></div></div>
         </div>   
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
    </ul></div>
</div>
<%} %>