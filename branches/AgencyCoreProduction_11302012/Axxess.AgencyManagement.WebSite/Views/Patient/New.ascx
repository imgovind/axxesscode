﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">New Patient | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Patient", FormMethod.Post, new { @id = "newPatientForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_FirstName" class="float-left"><span class="green">(M0040)</span> First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", (Model != null && Model.FirstName.IsNotNullOrEmpty()) ? Model.FirstName : string.Empty, new { @id = "New_Patient_FirstName", @class = "text input_wrapper required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_MiddleInitial" class="float-left"><span class="green">(M0040)</span> MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial", (Model != null && Model.MiddleInitial.IsNotNullOrEmpty()) ? Model.MiddleInitial : string.Empty, new { @id = "New_Patient_MiddleInitial", @class = "text input_wrapper mi", @maxlength = "1" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_LastName" class="float-left"><span class="green">(M0040)</span> Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", (Model != null && Model.LastName.IsNotNullOrEmpty()) ? Model.LastName : string.Empty, new { @id = "New_Patient_LastName", @class = "text input_wrapper required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label class="float-left"><span class="green">(M0069)</span> Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", Model != null && Model.Gender == "Female", new { @id = "New_Patient_Gender_F", @class = "required radio" })%>
                    <label for="New_Patient_Gender_F" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model != null && Model.Gender == "Male", new { @id = "New_Patient_Gender_M", @class = "required radio" }) %>
                    <label for="New_Patient_Gender_M" class="inline-radio">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_DOB" class="float-left"><span class="green">(M0066)</span> Date of Birth:</label>
                <div class="float-right"><input name="DOB" type="text" class="date-picker required" value="<%= Model != null && Model.DOB != DateTime.MinValue ? Model.DOBFormatted : string.Empty %>" id="New_Patient_DOB" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right">
                    <%= Html.MartialStatus("MaritalStatus", Model!=null? Model.MaritalStatus :string.Empty,true,"** Select **","0", new { @id = "New_Patient_MaritalStatus", @class = "input_wrapper" }) %>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Height</label>
                <div class="float-right">
                    <%= Html.TextBox("Height", Model != null ? Model.Height.ToString() : string.Empty, new { @id = "New_Patient_Height", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", Model != null && Model.HeightMetric == 0, new { @id = "New_Patient_HeightMetric0", @class = "radio" })%>
                    <label for="New_Patient_HeightMetric0" style="display:inline-block;width:20px!important">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model != null && Model.HeightMetric == 1, new { @id = "New_Patient_HeightMetric1", @class = "radio" })%>
                    <label for="New_Patient_HeightMetric1" style="display:inline-block;width:20px!important">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Weight</label>
                <div class="float-right">
                    <%= Html.TextBox("Weight", Model != null ? Model.Weight.ToString() : string.Empty, new { @id = "New_Patient_Weight", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model != null && Model.WeightMetric == 0, new { @id = "New_Patient_WeightMetric0", @class = "radio" })%>
                    <label for="New_Patient_WeightMetric0" style="display:inline-block;width:20px!important">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model != null && Model.WeightMetric == 1, new { @id = "New_Patient_WeightMetric1", @class = "radio" })%>
                    <label for="New_Patient_WeightMetric1" style="display:inline-block;width:20px!important">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_CaseManager" class="float-left">Case Manager:</label>
                <div class="float-right"><%= Html.CaseManagers("CaseManagerId", "", new { @id = "New_Patient_CaseManager", @class = "Users requireddropdown valid" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_Assign" class="float-left">Assign to Clinician/Case Manager:</label>
                <div class="float-right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "New_Patient_Assign", @class = "requireddropdown Users  valid" })%></div>
            </div>
            <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowPatientEligibility($('#New_Patient_MedicareNumber').val(), $('#New_Patient_LastName').val(),$('#New_Patient_FirstName').val(),$('#New_Patient_DOB').val(),$('input[name=Gender]:checked').val());">Verify Medicare Eligibility</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PatientID" class="float-left"><span class="green">(M0020)</span>Patient ID/MR Number:</label>
                <div class="float-right">
                    <%= Html.TextBox("PatientIdNumber", "", new { @id = "New_Patient_PatientID", @class = "text input_wrapper required", @maxlength = "30" })%><br />
                    <span>Last MR Number Used: <b><%= Current.LastUsedPatientId %></b></span>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicareNumber" class="float-left"><span class="green">(M0063)</span> Medicare Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : "", new { @id = "New_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicaidNumber" class="float-left"><span class="green">(M0065)</span> Medicaid Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : "", new { @id = "New_Patient_MedicaidNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_SSN" class="float-left"><span class="green">(M0064)</span> SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", (Model != null && Model.SSN.IsNotNullOrEmpty()) ? Model.SSN : "", new { @id = "New_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "11" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="StartOfCareDate" id="New_Patient_StartOfCareDate" onchange="Patient.OnSocChange()" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_EpisodeStartDate" class="float-left">Episode Start Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="EpisodeStartDate" id="New_Patient_EpisodeStartDate" /></div>
            </div>
            <div class="row">
                <label class="float-left">DNR:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsDNR", "true", Model != null ? Model.IsDNR : false, new { @id = "New_Patient_IsDNR1", @class = "radio" })%>
                    <label for="New_Patient_IsDNR1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsDNR", "false", Model != null ? !Model.IsDNR : true, new { @id = "New_Patient_IsDNR2", @class = "radio" })%>
                    <label for="New_Patient_IsDNR2" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_Patient_LocationId", @class = "BranchLocation requireddropdown" })%></div>
            </div>
            <div class="row">
                <input id="New_Patient_CreateEpisode" type="checkbox" checked="checked" value="true" name="ShouldCreateEpisode" class="radio float-left" />
                <label for="New_Patient_CreateEpisode" class="radio">Create Episode &#38; Schedule Start of Care Visit after saving</label>
                <em>(If this box is unchecked, patient will be added to list of pending admissions)</em>
            </div>
            <div class="row">
                <input id="New_Patient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" class="radio float-left" />
                <label for="New_Patient_IsFaceToFaceEncounterCreated" class="radio">Create a face to face encounter </label>
                <em>(this is applicable for SOC date after 01/01/2011)</em>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity<span class="required-red">*</span></legend>
        <table class="form"><tbody>
            <tr>
                <td><input id="New_Patient_RaceAmericanIndian" type="checkbox" value="0" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceAmericanIndian" class="radio">American Indian or Alaska Native</label></td>
                <td><input id="New_Patient_RaceAsian" type="checkbox" value="1" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceAsian" class="radio">Asian</label></td>
                <td><input id="New_Patient_RaceBlack" type="checkbox" value="2" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceBlack" class="radio">Black or African-American</label></td>
                <td><input id="New_Patient_RaceHispanic" type="checkbox" value="3" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceHispanic" class="radio">Hispanic or Latino</label></td>
            </tr><tr>
                <td><input id="New_Patient_RaceHawaiian" type="checkbox" value="4" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceHawaiian" class="radio">Native Hawaiian or Pacific Islander</label></td>
                <td><input id="New_Patient_RaceWhite" type="checkbox" value="5" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceWhite" class="radio">White</label></td>
                <td colspan="2"><input id="New_Patient_RaceUnknown" type="checkbox" value="6" name="EthnicRaces" class="required radio float-left" /><label for="New_Patient_RaceUnknown" class="radio">Unknown</label></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span><span class="required-red">*</span></legend>
        <table class="form"><tbody>
            <tr>
                <td><input id="New_Patient_PaymentSourceNone" type="checkbox" value="0" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceNone" class="radio"> None; no charge for current services</label></td>
                <td><input id="New_Patient_PaymentSourceMedicare" type="checkbox" value="1" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceMedicare" class="radio"> Medicare (traditional fee-for-service)</label></td>
                <td><input id="New_Patient_PaymentSourceMedicareHmo" type="checkbox" value="2" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceMedicareHmo" class="radio"> Medicare (HMO/ managed care)</label></td>
                <td><input id="New_Patient_PaymentSourceMedicaid" type="checkbox" value="3" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceMedicaid" class="radio"> Medicaid (traditional fee-for-service)</label></td>
            </tr><tr>
                <td><input id="New_Patient_PaymentSourceMedicaidHmo" type="checkbox" value="4" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceMedicaidHmo" class="radio"> Medicaid (HMO/ managed care)</label></td>
                <td><input id="New_Patient_PaymentSourceWorkers" type="checkbox" value="5" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceWorkers" class="radio"> Workers' compensation</label></td>
                <td><input id="New_Patient_PaymentSourceTitleProgram" type="checkbox" value="6" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceTitleProgram" class="radio"> Title programs (e.g., Titile III,V, or XX)</label></td>
                <td><input id="New_Patient_PaymentSourceOtherGovernment" type="checkbox" value="7" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceOtherGovernment" class="radio"> Other government (e.g.,CHAMPUS,VA,etc)</td>
            </tr><tr>
                <td><input id="New_Patient_PaymentSourcePrivate" type="checkbox" value="8" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourcePrivate" class="radio"> Private insurance</td>
                <td><input id="New_Patient_PaymentSourcePrivateHmo" type="checkbox" value="9" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourcePrivateHmo" class="radio"> Private HMO/ managed care</td>
                <td><input id="New_Patient_PaymentSourceSelf" type="checkbox" value="10" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceSelf" class="radio"> Self-pay</td>
                <td><input id="New_Patient_PaymentSourceUnknown" type="checkbox" value="11" name="PaymentSources" class="required radio float-left" /><label for="New_Patient_PaymentSourceUnknown" class="radio"> Unknown</td>
            </tr><tr>
                <td colspan='4'><input type="checkbox" id="New_Patient_PaymentSource" value="12" name="PaymentSources" class="radio float-left" /> <label for="New_Patient_PaymentSource" class="radio more">Other (specify)</label><%= Html.TextBox("OtherPaymentSource", "", new { @id = "New_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" }) %></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", (Model != null && Model.AddressLine1.IsNotNullOrEmpty()) ? Model.AddressLine1.ToString() : "", new { @id = "New_Patient_AddressLine1", @class = "text required input_wrapper", @maxlength="50" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", (Model != null && Model.AddressLine2.IsNotNullOrEmpty()) ? Model.AddressLine2.ToString() : "", new { @id = "New_Patient_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", (Model != null && Model.AddressCity.IsNotNullOrEmpty()) ? Model.AddressCity.ToString() : "", new { @id = "New_Patient_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", (Model != null && Model.AddressStateCode.IsNotNullOrEmpty()) ? Model.AddressStateCode.ToString() : "", new { @id = "New_Patient_AddressStateCode", @class = "AddressStateCode requireddropdown valid" })%><%= Html.TextBox("AddressZipCode", (Model != null && Model.AddressZipCode.IsNotNullOrEmpty()) ? Model.AddressZipCode.ToString() : "", new { @id = "New_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "9" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right"><%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 3 ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "New_Patient_HomePhone1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 6 ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "New_Patient_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10 ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "New_Patient_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MobilePhone1" class="float-left">Mobile Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneMobileArray" id="New_Patient_MobilePhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneMobileArray" id="New_Patient_MobilePhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneMobileArray" id="New_Patient_MobilePhone3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_Email" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", (Model != null && Model.EmailAddress.IsNotNullOrEmpty()) ? Model.EmailAddress : "", new { @id = "New_Patient_Email", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
        </div>
    </fieldset>
    <% var physicians = Model != null && Model.Physicians.IsNotNullOrEmpty() ? Model.Physicians.ToObject<List<Physician>>().OrderByDescending(p => p.IsPrimary).ToList() : new List<Physician>(); %>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PhysicianDropDown1" class="float-left">Primary Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", physicians.Count > 0 ? physicians[0].Id.ToString() : string.Empty, new { @id = "New_Patient_PhysicianDropDown1", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.AddPhysRow(this)">add row</a></div>
            </div>
            <div class="row <%= physicians.Count > 1 ? string.Empty : "hidden" %>">
                <label for="New_Patient_PhysicianDropDown2" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", physicians.Count > 1 ? physicians[1].Id.ToString() : string.Empty, new { @id = "New_Patient_PhysicianDropDown2", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row <%= physicians.Count > 2 ? string.Empty : "hidden" %>">
                <label for="New_Patient_PhysicianDropDown3" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", physicians.Count > 2 ? physicians[2].Id.ToString() : string.Empty, new { @id = "New_Patient_PhysicianDropDown3", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row <%= physicians.Count > 3 ? string.Empty : "hidden" %>">
                <label for="New_Patient_PhysicianDropDown4" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", physicians.Count > 3 ? physicians[3].Id.ToString() : string.Empty, new { @id = "New_Patient_PhysicianDropDown4", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row <%= physicians.Count > 4 ? string.Empty : "hidden" %>">
                <label for="New_Patient_PhysicianDropDown5" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", physicians.Count > 4 ? physicians[4].Id.ToString() : string.Empty, new { @id = "New_Patient_PhysicianDropDown5", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <%  var htmlAttributesPrimary = new Dictionary<string, string>(); %>
                <%  htmlAttributesPrimary.Add("id", "New_Patient_PrimaryInsurance"); %>
                <%  htmlAttributesPrimary.Add("class", "Insurances requireddropdown"); %>
                <label for="New_Patient_PrimaryInsurance" class="float-left">Primary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("PrimaryInsurance", "",Guid.Empty, true, htmlAttributesPrimary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_PrimaryInsuranceContent"></div>
            </div>
            <div class="row">
                <%  var htmlAttributesSecondary = new Dictionary<string, string>(); %>
                <%  htmlAttributesSecondary.Add("id", "New_Patient_SecondaryInsurance"); %>
                <%  htmlAttributesSecondary.Add("class", "Insurances"); %>
                <label for="New_Patient_SecondaryInsurance" class="float-left">Secondary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("SecondaryInsurance", "", Guid.Empty, true, htmlAttributesSecondary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_SecondaryInsuranceContent"></div>
            </div>
            <div class="row">
                <%  var htmlAttributesTertiary = new Dictionary<string, string>(); %>
                <%  htmlAttributesTertiary.Add("id", "New_Patient_TertiaryInsurance"); %>
                <%  htmlAttributesTertiary.Add("class", "Insurances"); %>
                <label for="New_Patient_TertiaryInsurance" class="float-left">Tertiary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("TertiaryInsurance", "", Guid.Empty, true, htmlAttributesTertiary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_TertiaryInsuranceContent"></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Emergency Triage <span class="light">(Select one)</span><span class="required-red">*</span></legend>
        <div class="column">
            <div class="row">
                <input id="New_Patient_Triage1" type="checkbox" value="1" name="Triage" class="required radio Triage float-left" />
                <div class="float-left strong">&#160;1.&#160;</div>
                <label class="normal margin" for="New_Patient_Triage1"><strong>Life threatening</strong> (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</label>
            </div>
            <div class="row">
                <input id="New_Patient_Triage2" type="checkbox" value="2" name="Triage" class="required radio Triage float-left" />
                <div class="float-left strong">&#160;2.&#160;</div>
                <label class="normal margin" for="New_Patient_Triage2"><strong>Not life threatening but would suffer severe adverse effects</strong> from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</label>
            </div>
            <div class="row">
                <input id="New_Patient_Triage3" type="checkbox" value="3" name="Triage" class="required radio Triage float-left" />
                <div class="float-left strong">&#160;3.&#160;</div>
                <label class="normal margin" for="New_Patient_Triage3"><strong>Visits could be postponed 24-48</strong> hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</label>
            </div>
            <div class="row">
                <input id="New_Patient_Triage4" type="checkbox" value="4" name="Triage" class="required radio Triage float-left" />
                <div class="float-left strong">&#160;4.&#160;</div>
                <label class="normal margin" for="New_Patient_Triage4"><strong>Visits could be postponed 72-96</strong> hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</label>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_EmergencyContactFirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.FirstName", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.FirstName : string.Empty, new { @id = "New_Patient_EmergencyContactFirstName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_EmergencyContactLastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.LastName", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.LastName : string.Empty, new { @id = "New_Patient_EmergencyContactLastName", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_EmergencyContactRelationship" class="float-left">Relationship:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.Relationship", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.Relationship : string.Empty, new { @id = "New_Patient_EmergencyContactRelationship", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_EmergencyContactPhonePrimary1" class="float-left">Primary Phone:</label>
                <div class="float-right">
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 3 ? Model.EmergencyContact.PrimaryPhone.Substring(0, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary1" maxlength="3" /> 
                    - 
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 6 ? Model.EmergencyContact.PrimaryPhone.Substring(3, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary2" maxlength="3" /> 
                    - 
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() && Model.EmergencyContact.PrimaryPhone.Length >= 10 ? Model.EmergencyContact.PrimaryPhone.Substring(6, 4) : "" %>" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_EmergencyContactPhoneAlternate1" class="float-left">Alternate Phone:</label>
                <div class="float-right">
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 3 ? Model.EmergencyContact.AlternatePhone.Substring(0, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate1" maxlength="3" />
                    - 
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 6 ? Model.EmergencyContact.AlternatePhone.Substring(3, 3) : "" %>" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate2" maxlength="3" />
                    -
                    <input type="text" value="<%= Model != null && Model.EmergencyContact != null && Model.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() && Model.EmergencyContact.AlternatePhone.Length >= 10 ? Model.EmergencyContact.AlternatePhone.Substring(6, 4) : "" %>" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_EmergencyContactEmail" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.EmailAddress", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.EmailAddress : string.Empty, new { @id = "New_Patient_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Evacuation Zone</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Evacuation Zone</label>
                <div class="float-right">
                    <select id="New_Patient_EvacuationZone" class="input_wrapper" name="EvacuationZone">
                        <option value=""></option>
                        <option value="A">Zone A</option>
                        <option value="B">Zone B</option>
                        <option value="C">Zone C</option>
                        <option value="D">Zone D</option>
                        <option value="E">Zone E</option>
                        <option value="NE">Zone NE</option>
                        <option value="SN">Zone SN</option>
                    </select>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Pharmacy Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PharmacyName" class="float-left">Name:</label>
                <div class="float-right"><%= Html.TextBox("PharmacyName", "", new { @id = "New_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_PharmacyPhone" class="float-left">Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone3" maxlength="4" /></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_ReferralPhysician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician", (Model != null && !Model.ReferrerPhysician.IsEmpty()) ? Model.ReferrerPhysician.ToString() : "", new { @id = "New_Patient_ReferrerPhysician", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_Patient_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource > 0) ? Model.AdmissionSource.ToString() : "", new { @id = "New_Patient_AdmissionSource", @class = "AdmissionSource requireddropdown required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", (Model != null && Model.OtherReferralSource.IsNotNullOrEmpty()) ? Model.OtherReferralSource : "", new { @id = "New_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_PatientReferralDate" class="float-left"><span class="green">(M0104)</span> Referral Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="ReferralDate" id="New_Patient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_InternalReferral" class="float-left">Internal Referral Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", (Model != null && !Model.InternalReferral.IsEmpty()) ? Model.InternalReferral.ToString() : "", new { @id = "New_Patient_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <table class="form">
            <tbody>
                <%string[] servicesRequired = Model != null && Model.ServicesRequired != null && Model.ServicesRequired.IsNotNullOrEmpty() ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <tr>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredSNV' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredSNV" class="radio">SN</label></td>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredHHA' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredHHA" class="radio">HHA</label></td>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredPT' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredPT" class="radio">PT</label></td>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredOT' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredOT" class="radio">OT</label></td>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredST' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredST" class="radio">ST</label></td>
                    <td><%= string.Format("<input id ='New_Patient_ServiceRequiredMSW' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServiceRequiredMSW" class="radio">MSW</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <table class="form">
            <%  string[] DME = (Model != null && Model.DME.IsNotNullOrEmpty()) ? Model.DME.Split(';') : null;  %>
            <input type="hidden" value=" " class="radio" name="DMECollection" />
            <tbody>
                <tr class="firstrow">
                    <td><%= string.Format("<input id='New_Patient_DMECollectionBed' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionBed" class="radio">Bedside Commode</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionCane' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionCane" class="radio">Cane</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionToilet' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionToilet" class="radio">Elevated Toilet Seat</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionBars' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionBars" class="radio">Grab Bars</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionHospitalBed' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionHospitalBed" class="radio">Hospital Bed</label></td>
                </tr><tr>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionNebulizer' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionNebulizer" class="radio">Nebulizer</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionOxygen' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionOxygen" class="radio">Oxygen</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionTub' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionTub" class="radio">Tub/Shower Bench</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionWalker' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionWalker" class="radio">Walker</label></td>
                    <td><%= string.Format("<input id='New_Patient_DMECollectionWheelChair' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollectionWheelChair" class="radio">Wheelchair</label></td>
                </tr><tr>
                    <td colspan="5"><%= string.Format("<input id='New_Patient_DMEOther' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMEOther" class="radio">Other</label><%= Html.TextBox("OtherDME", (Model != null && Model.OtherDME.IsNotNullOrEmpty()) ? Model.OtherDME : "", new { @id = "New_Patient_OtherDME", @class = "text", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : "", new { @id = "New_Patient_Comments", @class = "fill", @maxcharacters = "500" })%>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "1", new { @id = "New_Patient_Status" })%>     
    <%= Html.Hidden("ReferralId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : "false", new { @id = "New_Patient_ReferralId" })%>     
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Patient_Status').val('3');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Patient_Status').val('1');$(this).closest('form').submit();">Admit Patient</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newpatient');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $("select.Physicians").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("select.ReferrerPhysician").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("select.Insurances").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('newinsurance');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("#New_Patient_LocationId").change(function() { Agency.LoadAgencyInsurances($(this).val(), "New_Patient_PrimaryInsurance"); Agency.LoadAgencyInsurances($(this).val(), "New_Patient_SecondaryInsurance"); Agency.LoadAgencyInsurances($(this).val(), "New_Patient_TertiaryInsurance"); });
    $("#New_Patient_PrimaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_PrimaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Primary'); } else { $("#New_Patient_PrimaryInsuranceContent").empty(); } });
    $("#New_Patient_SecondaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_SecondaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Secondary'); } else { $("#New_Patient_SecondaryInsuranceContent").empty(); } });
    $("#New_Patient_TertiaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_TertiaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Tertiary'); } else { $("#New_Patient_TertiaryInsuranceContent").empty(); } });
    if (!$("#New_Patient_SSN").val())
    {
        $("#New_Patient_SSN").mask("999999999", { placeholder: "" });
    }
</script>
<% } %>