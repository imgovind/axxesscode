﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Communication Notes | <%= Model.DisplayName%></span>
<div class="wrapper">
<% var notAuditor = !Current.IfOnlyRole(AgencyRoles.Auditor); %>
<%= Html
        .Telerik()
        .Grid<CommunicationNote>()
        .Name("List_PatientCommunicationNote")
        .ToolBar(commnds => commnds.Custom())
        .DataKeys(keys => {
            keys.Add(o => o.Id).RouteKey("id");
        })
        .Columns(columns => {
            columns.Bound(c => c.UserDisplayName).Title("User").Sortable(true).ReadOnly();
            columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(true).ReadOnly();
            columns.Bound(c => c.StatusName).Title("Status").Sortable(true).ReadOnly();
            columns.Bound(c => c.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30).Sortable(false);
            columns.Bound(c => c.Id).Visible(notAuditor && !Current.IsAgencyFrozen).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.LoadEditCommunicationNote('<#=PatientId#>','<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteCommunicationNote('<#=Id#>','<#=PatientId#>');\" class=\"deletePatient\">Delete</a>").Title("Action").Width(180);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("PatientCommunicationNotes", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_PatientCommunicationNote .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $(".t-grid-content").css("height", "auto");
</script>
