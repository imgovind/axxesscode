﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Message>" %>
<%  if (Model != null) { %>
<div class="message-content" id="message-content">
    <input type="hidden" id="messageId" value="<%= Model.Id %>" />
    <input type="hidden" id="messageType" value="<%= (int)Model.Type %>" />
    <div class="message-content-panel" id="message-content-panel">
        <div class="message-header-container">
            <div class="message-header-row">
                <label for="messageSender">From:</label>
                <span id="messageSender"><%= Model.FromName %></span>
            </div>
            <div class="message-header-row">
                <label for="messageRecipients">To:</label>
                <span id="messageRecipients"><%= Model.RecipientNames %></span>
            </div>
            <div class="message-header-row">
                <label for="messageDate">Sent:</label>
                <span id="messageDate"><%= Model.MessageDate %></span>
            </div>
            <div class="message-header-row">
                <label for="messageSubject">Subject:</label>
                <span id="messageSubject"><%= Model.Subject %></span>
            </div>
    <%  if (Model.Type == MessageType.User || Model.Type == MessageType.Sent) { %>
            <div class="message-header-row">
                <label for="messageRegarding">Regarding:</label>
                <span id="messageRegarding"><%= Model.PatientName %></span>
            </div>
            <%  if (Model.HasAttachment) { %>
            <div class="message-header-row">
                <label for="messageAttachment">Attachment:</label>
                <span id="messageAttachment"><%= Html.Asset(Model.AttachmentId) %></span>
            </div>
            <%  } %>
            <div class="message-header-row message-header-buttons">
                <div class="buttons float-left">
                    <ul>
                        <% if (!Model.IsDeprecated) { %><li><a href="javascript:void(0);" id="MessageReplyButton">Reply</a></li><% } %>
                        <% if (!Model.IsDeprecated) { %><li><a href="javascript:void(0);" id="MessageForwardButton">Forward</a></li><% } %>
                        <% if (!Model.IsDeprecated && Model.Type == MessageType.User) { %><li><a href="javascript:void(0);" id="MessageDeleteButton">Delete</a></li><% } %>
                        <li><a href="javascript:void(0);" id="MessagePrintButton">Print</a></li>
                    </ul>
                </div>
            </div>
    <%  } else { %>
            <div class="message-header-row message-header-buttons">
                <div class="buttons float-left">
                    <ul>
                        <% if (!Model.IsDeprecated) { %><li><a href="javascript:void(0);" id="MessageDeleteButton2">Delete</a></li><%  } %>
                        <li><a href="javascript:void(0);" id="MessagePrintButton2">Print</a></li>
                    </ul>
                </div>
            </div>
    <%  } %>
        </div>
        <div id="message-body-container" class="message-body-container">
            <div id="message-body"><%= Model.Body %></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#message-content .message-header-row").each(function() {
        if ($(this).find("span").prop("scrollHeight") > $(this).find("span").prop("offsetHeight")) $(this).append(unescape("%3Ca href=%22javascript:void(0);%22 class=%22more%22 tooltip=%22" + $(this).find("span").html() + "%22%3EMore&#187;%3C/a%3E"));
        $(this).find("span").css("overflow", "hidden")
    });
    U.ToolTip($("#message-content .message-header-row a.more"), "calday");
</script>
<%  } else { %> 
<script type="text/javascript">
    $("#messageInfoResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Messages found." })).append(
            $("<div/>", { "class": "heading" }).Buttons([
            <% if(!Current.IsAgencyFrozen) { %>
            {
                Text: "Add New Message",
                Click: function() {
                    UserInterface.ShowNewMessage()
                }
            }
            <% } %>
            ])
        )
    );
</script>
<%  } %>