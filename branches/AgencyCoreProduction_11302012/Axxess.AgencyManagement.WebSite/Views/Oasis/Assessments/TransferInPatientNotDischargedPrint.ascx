<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Demographics.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/RiskAssessment.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Cardiac.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Medications.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/EmergentCare.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/TransferDischargeDeath.ascx", Model); %>
