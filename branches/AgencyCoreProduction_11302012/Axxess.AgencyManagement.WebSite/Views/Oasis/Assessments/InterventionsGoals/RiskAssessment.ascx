<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<% string[] riskAssessmentIntervention = data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer != "" ? data["485RiskAssessmentIntervention"].Answer.Split(',') : null; %>
<% string[] riskAssessmentGoals = data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer != "" ? data["485RiskAssessmentGoals"].Answer.Split(',') : null; %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485RiskAssessmentIntervention" value=" " />
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RiskAssessmentIntervention1' name='{0}_485RiskAssessmentIntervention' value='1' type='checkbox' {1} />", Model.TypeName, riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("1") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485RiskAssessmentIntervention1" class="radio">SN to assist patient to obtain ERS button.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RiskAssessmentIntervention2' name='{0}_485RiskAssessmentIntervention' value='2' type='checkbox' {1} />", Model.TypeName, riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("2") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485RiskAssessmentIntervention2" class="radio">SN to develop individualized emergency plan with patient.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RiskAssessmentIntervention3' name='{0}_485RiskAssessmentIntervention' value='3' type='checkbox' {1} />", Model.TypeName, riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("3") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485RiskAssessmentIntervention3" class="radio">SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.</label>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RiskInterventionComments" class="float-left">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485RiskInterventionTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485RiskInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485RiskInterventionComments", data.ContainsKey("485RiskInterventionComments") ? data["485RiskInterventionComments"].Answer : "", 5, 70, new { @id = Model.TypeName + "_485RiskInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485RiskAssessmentGoals" value=" " />
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RiskAssessmentGoals1' name='{0}_485RiskAssessmentGoals' value='1' type='checkbox' {1} />", Model.TypeName, riskAssessmentGoals != null && riskAssessmentGoals.Contains("1") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485RiskAssessmentGoals1" class="radio">The patient will have no hospitalizations during the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RiskAssessmentGoals2' name='{0}_485RiskAssessmentGoals' value='2' type='checkbox' {1} />", Model.TypeName, riskAssessmentGoals != null && riskAssessmentGoals.Contains("2") ? "checked='checked'" : "")%>
                    <span class="radio">
                        <label for="StartOfCare_485RiskAssessmentGoals2">The</label>
                        <%  var verbalizeEmergencyPlanPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer != "" ? data["485VerbalizeEmergencyPlanPerson"].Answer : "Patient/Caregiver"); %>
                        <%= Html.DropDownList(Model.TypeName + "_485VerbalizeEmergencyPlanPerson", verbalizeEmergencyPlanPerson) %>
                        <label for="<%= Model.TypeName %>_485RiskAssessmentGoals2">will verbalize understanding of individualized emergency plan by the end of the episode.</label>
                    </span>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RiskGoalComments" class="float-left">Additional Goals</label>
            <%= Html.Templates(Model.TypeName + "_485RiskGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485RiskGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485RiskGoalComments", data.ContainsKey("485RiskGoalComments") ? data["485RiskGoalComments"].Answer : "", 5, 70, new { @id = Model.TypeName + "_485RiskGoalComments", @title = "(485 Locator 22) Goals" }) %>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.goals($(".goals"));
</script>