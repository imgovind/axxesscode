﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PatientHistoryForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", AssessmentCategory.PatientHistory.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <fieldset class="half float-left">
         <div class="column">
            <label for="<%= Model.Type %>_GenericLastVisitDate" class="float-left">Last Physician Visit Date:</label>
            <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_GenericLastVisitDate" value="<%= data.AnswerOrEmptyString("GenericLastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("GenericLastVisitDate") : string.Empty %>"  id="<%= Model.Type %>_GenericLastVisitDate" /></div>
         </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Vital Signs</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPulseApical" class="float-left">Apical Pulse:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseApical", data.AnswerOrEmptyString("GenericPulseApical"), new { @id = Model.TypeName + "_GenericPulseApical", @title = "(Optional) Apical Pulse", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericPulseApicalRegular' class='radio' name='{0}_GenericPulseApicalRegular' value='1' type='checkbox' {0} title ='(Optional) Apical Pulse, Regular' />", data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseApicalRegular">(Reg)</label>
                    <%= string.Format("<input id='{0}_GenericPulseApicalIrregular' class='radio' name='{0}_GenericPulseApicalRegular' value='2' type='checkbox' {0} title ='(Optional) Apical Pulse, Irregular' />", data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseApicalIrregular">(Irreg)</label>
                </div> 
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPulseRadial" class="float-left">Radial Pulse:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseRadial", data.AnswerOrEmptyString("GenericPulseRadial"), new { @id = Model.TypeName + "_GenericPulseRadial", @title = "(Optional) Radial Pulse", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericPulseRadialRegular' class='radio' name='{0}_GenericPulseRadialRegular' value='1' type='checkbox' {0} title ='(Optional) Radial Pulse, Regular' />", data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseRadialRegular">(Reg)</label>
                    <%= string.Format("<input id='{0}_GenericPulseRadialIrregular' class='radio' name='{0}_GenericPulseRadialRegular' value='2' type='checkbox' {0} title ='(Optional) Radial Pulse, Irregular' />", data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseRadialIrregular">(Irreg)</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHeight" class="float-left">Height:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericHeight", data.AnswerOrEmptyString("GenericHeight"), new { @id = Model.TypeName + "_GenericHeight", @title = "(Optional) Height", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWeight" class="float-left">Weight:</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @id = Model.TypeName + "_GenericWeight", @title = "(Optional) Weight", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericWeightActual' class='radio' name='{0}_GenericWeightActualStated' value='1' type='checkbox' {0} title ='(Optional) Weight, Actual' />", data.AnswerOrEmptyString("GenericWeightActualStated").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericWeightActual">Actual</label>
                    <%= string.Format("<input id='{0}_GenericWeightStated' class='radio' name='{0}_GenericWeightActualStated' value='2' type='checkbox' {0} title ='(Optional) Weight, Stated' />", data.AnswerOrEmptyString("GenericWeightActualStated").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericWeightStated">Stated</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericTemp" class="float-left">Temp:</label>
                <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @id = Model.TypeName + "_GenericTemp", @title = "(Optional) Temperature", @class = "vitals" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericResp" class="float-left">Resp:</label>
                <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @id = Model.TypeName + "_GenericResp", @title = "(Optional) Respirations", @class = "vitals" })%></div>
            </div>
            <div class="row">
                <label class="float-left">BP Lying</label>
                <div class="float-right">
                    <label for="<%= Model.TypeName %>_GenericBPLeftLying">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftLying", data.AnswerOrEmptyString("GenericBPLeftLying"), new { @id = Model.TypeName + "_GenericBPLeftLying", @title = "(Optional) Blood Pressure, Lying/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightLying">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightLying", data.AnswerOrEmptyString("GenericBPRightLying"), new { @id = Model.TypeName + "_GenericBPRightLying", @title = "(Optional) Blood Pressure, Lying/Right", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">BP Sitting</label>
                <div class="float-right">
                    <label for="<%= Model.TypeName %>_GenericBPLeftSitting">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftSitting", data.AnswerOrEmptyString("GenericBPLeftSitting"), new { @id = Model.TypeName + "_GenericBPLeftSitting", @title = "(Optional) Blood Pressure, Sitting/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightSitting">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightSitting", data.AnswerOrEmptyString("GenericBPRightSitting"), new { @id = Model.TypeName + "_GenericBPRightSitting", @title = "(Optional) Blood Pressure, Sitting/Right", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">BP Standing</label>
                <div class="float-right oasis">
                    <label for="<%= Model.TypeName %>_GenericBPLeftStanding">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftStanding", data.AnswerOrEmptyString("GenericBPLeftStanding"), new { @id = Model.TypeName + "_GenericBPLeftStanding", @title = "(Optional) Blood Pressure, Standing/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightStanding">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightStanding", data.AnswerOrEmptyString("GenericBPRightStanding"), new { @id = Model.TypeName + "_GenericBPRightStanding", @title = "(Optional) Blood Pressure, Standing/Right", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left loc485">
        <legend>Allergies </legend>
        <%--<div class="column">
            <div class="row">
                <%= Html.Hidden(Model.TypeName + "_485Allergies", " ", new { @id = Model.TypeName + "_485AllergiesHidden" })%>
                <%= Html.RadioButton(Model.TypeName + "_485Allergies", "No", data.AnswerOrEmptyString("485Allergies").Equals("No"), new { @id = Model.TypeName + "Allergies_NKA", @title = "(485 Locator 17) Allergies, No Known Allergies", @class = "radio" })%>
                <label for="<%= Model.TypeName %>_Allergies_NKA">NKA (Food/Drugs/Latex)</label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <%= Html.RadioButton(Model.TypeName + "_485Allergies", "Yes", data.AnswerOrEmptyString("485Allergies").Equals("Yes"), new { @id = Model.TypeName + "Allergies_Specify", @title = "(485 Locator 17) Allergies, Allergic To", @class = "radio" })%>
                <label for="<%= Model.TypeName %>_Allergies_Specify">Allergic to:</label>
                <%= Html.TextArea(Model.TypeName + "_485AllergiesDescription", data.AnswerOrEmptyString("485AllergiesDescription"), new { @id = Model.TypeName + "_485AllergiesDescription", @title = "(485 Locator 17) Allergies" })%>
            </div>
        </div>--%>
        <%  var allergyProfile = (Model != null && Model.AllergyProfile.IsNotNullOrEmpty()) ? Model.AllergyProfile.ToObject<AllergyProfile>() : new AllergyProfile(); %>
        <div class="buttons">
        <ul class="float-left">
            <% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %><li><a href="javascript:void(0);" onclick="Allergy.Add('<%= allergyProfile.Id %>', '<%= Model.Type %>');">Add Allergy</a></li><% } %>
            <li><a href="javascript:void(0);" onclick="Allergy.Refresh('<%= allergyProfile.Id %>', '<%= Model.Type %>');">Refresh Allergies</a></li>
        </ul>
        </div>
        <div class="clear"></div>
        <% allergyProfile.Type = Model.Type.ToString(); %>
        <div id="<%= Model.Type %>_AllergyProfileList"><% Html.RenderPartial("~/Views/Patient/Allergies/List.ascx", allergyProfile); %></div>
    
    </fieldset>
    <div class="clear"></div>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.ChangeToRadio("<%= Model.TypeName %>_GenericPulseApicalRegular");
    U.ChangeToRadio("<%= Model.TypeName %>_GenericPulseRadialRegular");
    U.ChangeToRadio("<%= Model.TypeName %>_GenericWeightActualStated");
//    U.ShowIfRadioEquals("<%= Model.TypeName %>_485Allergies","Yes",
//        $("#<%= Model.TypeName %>_485AllergiesDescription"));
    
    
</script>