<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<% var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<script type="text/javascript">
printview.addsection(
"","Care Management");

<%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        "%3Ctable%3E%3Ctr%3E%3Cth colspan=%227%22%3E" +
        printview.span("(M2100) Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed. (Check only one box in each row.)",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3E" +
        printview.span("Type of Assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("No assistance needed in this area",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) currently provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) need training/ supportive services to provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) not likely to provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Unclear if Caregiver(s) will provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Assistance needed, but no Caregiver(s) available",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100ADLAssistance").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100IADLAssistance").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Medication administration (e.g., oral, inhaled or injectable)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Medical procedures/ treatments (e.g., changing wound dressing)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Supervision and safety (e.g., due to cognitive impairment)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("g. Advocacy or facilitation of patient’s participation in appropriate medical care (includes transporta-tion to or from appointments)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("04").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("05").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3C/table%3E","OASIS M2100");
    printview.addsection(
        printview.span("(M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?",true) +
        printview.col(3,
            printview.checkbox("1 &#8211; At least daily",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Three or more times per week",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; One to two times per week",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Received, but less often than weekly",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("04").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; No assistance received",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("05").ToString().ToLower() %>)+
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("UK").ToString().ToLower() %>)),
            "OASIS M2110");
<%  } %>
</script>