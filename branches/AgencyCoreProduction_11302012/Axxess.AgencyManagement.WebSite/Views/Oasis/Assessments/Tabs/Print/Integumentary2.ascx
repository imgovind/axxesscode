﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var SkinColor = data.AnswerArray("GenericSkinColor"); %>
<%  var SkinCondition = data.AnswerArray("GenericSkinCondition"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 6 || Model.AssessmentTypeNum % 10 > 8) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Skin Turgor:",true) +
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Good").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Fair").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Poor").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("Skin Color:",true) +
                printview.checkbox("Pink/WNL",<%= SkinColor.Contains("1").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Pallor",<%= SkinColor.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Jaundice",<%= SkinColor.Contains("3").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.checkbox("Cyanotic",<%= SkinColor.Contains("4").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Other",<%= SkinColor.Contains("5").ToString().ToLower() %>) +
                printview.span("<%= SkinColor.Contains("5") ? data.AnswerOrEmptyString("GenericSkinColorOther").Clean() : string.Empty %>",0,1))) +
        printview.col(2,
            printview.col(2,
                printview.span("Condition:",true) +
                printview.checkbox("Dry",<%= SkinCondition.Contains("1").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Wound",<%= SkinCondition.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Ulcer",<%= SkinCondition.Contains("3").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.checkbox("Incision",<%= SkinCondition.Contains("4").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Rash",<%= SkinCondition.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Other <%= SkinCondition.Contains("6") ? data.AnswerOrEmptyString("GenericSkinConditionOther").Clean() : string.Empty %>",<%= SkinCondition.Contains("6").ToString().ToLower() %>))) +
        printview.col(2,
            printview.span("Instructed on measures to control infections?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericInstructedControlInfections").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericInstructedControlInfections").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("Nail Condition:",true) +
                printview.col(2,
                    printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericNails").Equals("Good").ToString().ToLower() %>) +
                    printview.checkbox("Problem",<%= data.AnswerOrEmptyString("GenericNails").Equals("Problem").ToString().ToLower() %>))) +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericNails").Equals("Problem") ? data.AnswerOrEmptyString("GenericNailsProblemOther").Clean() : string.Empty %>",false,1) +
                printview.span("<strong>Skin Temp.:</strong><%= data.AnswerOrEmptyString("GenericSkinTemp").Equals("1") ? "Warm" : string.Empty %><%= data.AnswerOrEmptyString("GenericSkinTemp").Equals("2") ? "Cool" : string.Empty %><%= data.AnswerOrEmptyString("GenericSkinTemp").Equals("3") ? "Clammy" : string.Empty %><%= data.AnswerOrEmptyString("GenericSkinTemp").Equals("4") ? "Other" : string.Empty %>")) +
            printview.span("Is patient using pressure-relieving device(s)?",true) +
            printview.col(3,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPressureRelievingDevice").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPressureRelievingDevice").Equals("0").ToString().ToLower() %>) +
                printview.span("Type <%= data.AnswerOrEmptyString("GenericPressureRelievingDeviceType").Equals("1") ? "Low Air Mattress" : string.Empty %><%= data.AnswerOrEmptyString("GenericPressureRelievingDeviceType").Equals("2") ? "Gel Cushion" : string.Empty %><%= data.AnswerOrEmptyString("GenericPressureRelievingDeviceType").Equals("3") ? "Egg Crate" : string.Empty %><%= data.AnswerOrEmptyString("GenericPressureRelievingDeviceType").Equals("4") ? data.AnswerOrDefault("GenericPressureRelievingDeviceTypeOther", "Other") : string.Empty %>"))) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericIntegumentaryStatusComments").Clean() %>",false,2),
        "Integumentary Status");
    
  
    <%  if (Model.AssessmentTypeNum != 11 || Model.AssessmentTypeNum != 14) { %>
        <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1300) " : string.Empty %>Pressure Ulcer Assessment: Was this patient assessed for Risk of Developing Pressure Ulcers?",true) +
        printview.checkbox("0 &#8211; No assessment conducted",<%= data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Yes, based on an evaluation of clinical factors, e.g., mobility, incontinence, nutrition, etc., without use of standardized tool",<%= data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Yes, using a standardized tool, e.g., Braden, Norton, other",<%= data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("02").ToString().ToLower() %>));
            <%  if (!data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("00")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "((M1302) " : string.Empty %>Does this patient have a Risk of Developing Pressure Ulcers?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("1").ToString().ToLower() %>)));
            <%  } %>
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1306) " : string.Empty %>Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &#8220;unstageable&#8221;?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("1").ToString().ToLower() %>)));
        <%  if (!data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("00")) { %>
            <%  if (Model.AssessmentTypeNum % 10 > 8) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1307) " : string.Empty %>The Oldest Non-epithelialized Stage II Pressure Ulcer that is present at discharge",true) +
        printview.checkbox("1 &#8211; Was present at the most recent SOC/ROC assessment",<%= data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Developed since the most recent SOC/ROC assessment: record date pressure ulcer first identified: <%= data.AnswerOrDefault("M1307NonEpithelializedStageTwoUlcerdate", "<span class='blank'></span>").Clean() %>",<%= data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("NA &#8211; No non-epithelialized Stage II pressure ulcers are present at discharge",<%= data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("NA").ToString().ToLower() %>));
            <%  } %>
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%226%22%3E" +
        printview.span("<%= isOasis ? "(M1308) " : string.Empty %>Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage:",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%224%22%3E" +
        printview.span("Stage Description&#8212; Unhealed Pressure Ulcer",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 1",true) +
        printview.span("Number Currently Present") +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 2",true) +
        printview.span("Number of those listed in Column 1 that were present on admission") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerAdmission").Clean() %>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerAdmission").Clean() %>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageFourUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageIVUlcerAdmission").Clean() %>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or device.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerAdmission").Clean() %>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").Clean() %>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.3 Unstageable: Suspected deep tissue injury in evolution.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").Clean() %>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").Clean() %>") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
        <%  } %>
        <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1310) " : string.Empty %>Pressure Ulcer Length: Longest length &#8220;head-to-toe&#8221;",true) +
        printview.span("<%= data.AnswerOrDefault("M1310PressureUlcerLength", "<span class='blank'></span>").Clean() %>.<%= data.AnswerOrDefault("M1310PressureUlcerLengthDecimal", "<span class='blank'></span>").Clean() %>cm",false));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1312) " : string.Empty %>Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular to the length",true) +
        printview.span("<%= data.AnswerOrDefault("M1312PressureUlcerWidth", "<span class='blank'></span>").Clean() %>.<%= data.AnswerOrDefault("M1312PressureUlcerWidthDecimal", "<span class='blank'></span>").Clean() %>cm",false));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1314) " : string.Empty %>Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface to the deepest area",true) +
        printview.span("<%= data.AnswerOrDefault("M1314PressureUlcerDepth", "<span class='blank'></span>").Clean() %>.<%= data.AnswerOrDefault("M1314PressureUlcerDepthDecimal", "<span class='blank'></span>").Clean() %>cm",false));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1320) " : string.Empty %>Status of Most Problematic (Observable) Pressure Ulcer",true) +
        printview.col(5,
            printview.checkbox("0 &#8211; Newly epithelialized",<%= data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Fully granulating",<%= data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Early/partial granulation",<%= data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Not healing",<%= data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; No observable pressure ulcer",<%= data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("NA").ToString().ToLower() %>)));
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1322) " : string.Empty %>Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.",true) +
        printview.col(5,
            printview.checkbox("0 &#8211; Zero",<%= data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; One",<%= data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Two",<%= data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Three",<%= data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Four or More",<%= data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("04").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1324) " : string.Empty %>Stage of Most Problematic Unhealed (Observable) Pressure Ulcer",true) +
        printview.col(4,
            printview.checkbox("1 &#8211; Stage I",<%= data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Stage II",<%= data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Stage III",<%= data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Stage IV",<%= data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("04").ToString().ToLower() %>)) +
        printview.checkbox("NA &#8211; No observable pressure ulcer or unhealed pressure ulcer",<%= data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("NA").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1330) " : string.Empty %>Does this patient have a Stasis Ulcer?",true) +
        printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1330StasisUlcer").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Yes, patient has BOTH observable and unobservable stasis ulcers",<%= data.AnswerOrEmptyString("M1330StasisUlcer").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Yes, patient has observable stasis ulcers ONLY",<%= data.AnswerOrEmptyString("M1330StasisUlcer").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)",<%= data.AnswerOrEmptyString("M1330StasisUlcer").Equals("03").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1332) " : string.Empty %>Current Number of (Observable) Stasis Ulcer(s)",true) +
        printview.col(4,
            printview.checkbox("1 &#8211; One",<%= data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Two",<%= data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Three",<%= data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Four or more",<%= data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("04").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1334) " : string.Empty %>Status of Most Problematic (Observable) Stasis Ulcer",true) +
        printview.col(4,
            printview.checkbox("0 &#8211; Newly epithelialized",<%= data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Fully granulating",<%= data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Early/partial granulation",<%= data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Not healing",<%= data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("03").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1340) " : string.Empty %>Does this patient have a Surgical Wound? ",true) +
        printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1340SurgicalWound").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Yes, patient has at least one (observable) surgical wound",<%= data.AnswerOrEmptyString("M1340SurgicalWound").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Surgical wound known but not observable due to non-removable dressing",<%= data.AnswerOrEmptyString("M1340SurgicalWound").Equals("02").ToString().ToLower() %>));
        <%  if (!data.AnswerOrEmptyString("M1340SurgicalWound").Equals("00")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1342) " : string.Empty %>Status of Most Problematic (Observable) Surgical Wound ",true) +
        printview.col(4,
            printview.checkbox("0 &#8211; Newly epithelialized",<%= data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Fully granulating",<%= data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Early/partial granulation",<%= data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Not healing",<%= data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("03").ToString().ToLower() %>)));
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1350) " : string.Empty %>Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("1").ToString().ToLower() %>)));
    <%  } %>
    
        <%  for (int i = 1; i < 6; i++) { %>
            <%  if (data.AnswerOrEmptyString("GenericLocation" + i).IsNotNullOrEmpty()) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Location:",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLocation" + i).Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Onset Date:",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericOnsetDate" + i).Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Wound Type:",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericWoundType" + i).Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Pressure Ulcer Stage:",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericPressureUlcerStage" + i).Equals("0") ? string.Empty : data.AnswerOrEmptyString("GenericPressureUlcerStage" + i).Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Measurements:",true) +
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericMeasurementLength" + i).Clean() %>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Width:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericMeasurementWidth" + i).Clean() %>",false,1)) +
                printview.col(2,
                    printview.span("Depth:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericMeasurementDepth" + i).Clean() %>",false,1))) +
            printview.col(2,
                printview.span("Wound Bed:",true) +
                printview.col(2,
                    printview.span("Granulation %:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericWoundBedGranulation" + i).Clean() %>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Slough %:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericWoundBedSlough" + i).Clean() %>",false,1)) +
                printview.col(2,
                    printview.span("Eschar %:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericWoundBedEschar" + i).Clean() %>",false,1))) +
            printview.col(2,
                printview.span("<strong>Surrounding Tissue</strong>: <%= !data.AnswerOrEmptyString("GenericSurroundingTissue" + i).Equals("0") ? data.AnswerOrEmptyString("GenericSurroundingTissue" + i).Clean() : string.Empty %>") +
                printview.span("<strong>Drainage</strong>: <%= !data.AnswerOrEmptyString("GenericDrainage" + i).Equals("0") ? data.AnswerOrEmptyString("GenericDrainage" + i).Clean() : string.Empty %>")) +
            printview.col(2,
                printview.span("<strong>Drainage Amount</strong>: <%= !data.AnswerOrEmptyString("GenericDrainageAmount" + i).Equals("0") ? data.AnswerOrEmptyString("GenericDrainageAmount" + i).Clean() : string.Empty %>") +
                printview.span("<strong>Odor</strong>: <%= !data.AnswerOrEmptyString("GenericOdor" + i).Equals("0") ? data.AnswerOrEmptyString("GenericOdor" + i).Clean() : string.Empty %>"))) +
        printview.col(3,
            printview.col(3,
                printview.span("Tunneling:",true) +
                printview.span("Length: <%= data.AnswerOrEmptyString("GenericTunnelingLength" + i).Clean() %>") +
                printview.span("Time: <%= data.AnswerOrEmptyString("GenericTunnelingTime" + i).Clean() %>")) +
            printview.col(3,
                printview.span("Undermining:",true) +
                printview.span("Length: <%= data.AnswerOrEmptyString("GenericUnderminingLength" + i).Clean() %>") +
                printview.span("Time: <%= data.AnswerOrEmptyString("GenericUnderminingTimeFrom" + i).Clean() %> to <%= data.AnswerOrEmptyString("GenericUnderminingTimeTo" + i) %> o&#8217;clock")) +
            printview.col(3,
                printview.span("Device:",true) +
                printview.span("Type: <%= data.AnswerOrEmptyString("GenericDeviceType" + i).Clean() %>") +
                printview.span("Setting: <%= data.AnswerOrEmptyString("GenericDeviceSetting" + i).Clean() %>"))),
        "Wound <%= i %>");
            <%  } %>
        <%  } %>
<%  } %>
</script>