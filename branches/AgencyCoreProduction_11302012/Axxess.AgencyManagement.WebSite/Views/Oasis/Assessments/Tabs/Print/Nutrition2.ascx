﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var Nutrition = data.AnswerArray("GenericNutrition"); %>
<%  var NutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
<%  var NutritionalHealth = data.AnswerArray("GenericNutritionalHealth"); %>
<%  var NutritionDiffect = data.AnswerArray("GenericNutritionDiffect"); %>
<%  var NutritionalReqs = data.AnswerArray("485NutritionalReqs"); %>
<%  var NutritionalReqsEnteral = data.AnswerArray("485NutritionalReqsEnteral"); %>
<%  var NutritionalReqsEnteralVia = data.AnswerArray("485NutritionalReqsEnteralVia"); %>
<%  var genericProblem = data.AnswerArray("GenericProblem"); %>
<script type="text/javascript">

    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= Nutrition.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Dysphagia",<%= Nutrition.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Appetite",<%= Nutrition.Contains("3").ToString().ToLower() %>,true)) +
        printview.col(4,
            printview.checkbox("Weight",<%= Nutrition.Contains("4").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Loss",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToString().ToLower() %>) +
            printview.checkbox("Gain",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToString().ToLower() %>) +
            printview.checkbox("Diet",<%= Nutrition.Contains("5").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Adequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToString().ToLower() %>) +
             printview.checkbox("Diet Type",<%= Nutrition.Contains("9").ToString().ToLower() %>,true) +
            printview.span("<%= Nutrition.Contains("9") ? data.AnswerOrEmptyString("GenericNutritionDietType") : string.Empty %>") +
            printview.checkbox("Inadequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToString().ToLower() %>) +
            printview.checkbox("Enteral Feeding",<%= Nutrition.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("NG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("PEG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("2")).ToString().ToLower() %>) +
            printview.checkbox("Dobhoff",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Tube Placement Checked",<%= Nutrition.Contains("7").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Residual Checked",<%= Nutrition.Contains("8").ToString().ToLower() %>,true) +
            printview.span("Amount: <%= data.AnswerOrDefault("GenericNutritionResidualCheckedAmount", "<span class='short blank'></span>").Clean() %>ml") +


                printview.checkbox("Throat problems?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("1")).ToString().ToLower() %>) +
                printview.checkbox("Sore throat?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("2")).ToString().ToLower() %>) +
                printview.checkbox("Dentures?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("3")).ToString().ToLower() %>) +
                printview.checkbox("Hoarseness?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("4")).ToString().ToLower() %>) +
                printview.checkbox("Dental problems?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("5")).ToString().ToLower() %>) +
                printview.checkbox("Problems chewing?",<%= (Nutrition.Contains("8")&&genericProblem.Contains("6")).ToString().ToLower() %>) +
                printview.checkbox("Other:",<%= (Nutrition.Contains("8")&&genericProblem.Contains("7")).ToString().ToLower() %>) +
        printview.span("<%= Nutrition.Contains("8") && genericProblem.Contains("7") ? data.AnswerOrEmptyString("GenericNutritionOther") : string.Empty %>")) +
                  
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNutritionComments").Clean() %>",false,2),
        "Nutrition");
</script>