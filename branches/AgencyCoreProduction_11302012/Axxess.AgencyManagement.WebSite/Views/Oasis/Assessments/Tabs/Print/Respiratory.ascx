<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var RespiratoryCondition = data.AnswerArray("GenericRespiratoryCondition"); %>
<%  var RespiratorySounds = data.AnswerArray("GenericRespiratorySounds"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 6 || Model.AssessmentTypeNum % 10 > 8) { %>
    <%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
            printview.checkbox("WNL (Within Normal Limits)",<%= RespiratoryCondition.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Lung Sounds",<%= RespiratoryCondition.Contains("2").ToString().ToLower() %>,true) +
       <%if(RespiratoryCondition.Contains("2")){ %>
            printview.col(4,
                printview.checkbox("CTA <%=RespiratorySounds.Contains("1")?data.AnswerOrEmptyString("GenericLungSoundsCTAText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Rales <%=RespiratorySounds.Contains("2")?data.AnswerOrEmptyString("GenericLungSoundsRalesText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Rhonchi <%=RespiratorySounds.Contains("3")?data.AnswerOrEmptyString("GenericLungSoundsRhonchiText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Wheezes <%=RespiratorySounds.Contains("4")?data.AnswerOrEmptyString("GenericLungSoundsWheezesText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Crackles <%=RespiratorySounds.Contains("5")?data.AnswerOrEmptyString("GenericLungSoundsCracklesText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Diminished <%=RespiratorySounds.Contains("6")?data.AnswerOrEmptyString("GenericLungSoundsDiminishedText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Absent <%=RespiratorySounds.Contains("7")?data.AnswerOrEmptyString("GenericLungSoundsAbsentText").Clean() : string.Empty %>",<%=RespiratorySounds.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Stridor <%=RespiratorySounds.Contains("8")?data.AnswerOrEmptyString("GenericLungSoundsStridorText").Clean() : string.Empty %>",<%= RespiratorySounds.Contains("8").ToString().ToLower() %>)) +
            
       <%} %>
            printview.checkbox("Cough:",<%= RespiratoryCondition.Contains("3").ToString().ToLower() %>,true) +
        <%if(RespiratoryCondition.Contains("3")){ %>
            printview.col(2,
                printview.span("<%= (data.AnswerOrEmptyString("GenericCoughList").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("2") ? "Productive" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("3") ? "Nonproductive" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("4") ? "Other" : string.Empty)%>",true)+
                printview.span("Describe:<%=data.AnswerOrEmptyString("GenericCoughDescribe").Clean() %>",true))+
        <%} %>
            printview.checkbox("O<sub>2</sub>:",<%= RespiratoryCondition.Contains("4").ToString().ToLower() %>,true) +
        <%if(RespiratoryCondition.Contains("4")){ %>
            printview.col(3,
                printview.span("At:<%=data.AnswerOrEmptyString("Generic02AtText").Clean()%> ",true)+
                printview.span("<%=(data.AnswerOrEmptyString("GenericLPMVia").Equals("1") ? "LPM via: Nasal Cannula" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("2") ? "LPM via: Mask" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("3") ? "LPM via: Trach" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("4") ? "LPM via: Other" : string.Empty) %>",true)+
                printview.span("<%=(data.AnswerOrEmptyString("GenericOxygenRate").Equals("1") ? "Continuous" : string.Empty) + (data.AnswerOrEmptyString("GenericOxygenRate").Equals("2") ? "Intermittent" : string.Empty)%>",true))+
        <%} %>
        printview.checkbox("O<sub>2</sub> Sat:",<%= RespiratoryCondition.Contains("5").ToString().ToLower() %>,true) +
        <%if(RespiratoryCondition.Contains("5")){ %>
            printview.col(2,
                printview.span("<%=data.AnswerOrEmptyString("Generic02SatText").Clean() %>",true)+
                printview.span("<%= data.AnswerOrEmptyString("Generic02SatList").Equals("0") ? string.Empty : "On " + data.AnswerOrEmptyString("Generic02SatList").Clean() %>",false,1)) +
        
        <%} %>
            printview.checkbox("Nebulizer <%=RespiratoryCondition.Contains("6")?data.AnswerOrEmptyString("GenericNebulizerText").Clean():string.Empty %>",<%= RespiratoryCondition.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("Tracheostomy",<%= RespiratoryCondition.Contains("7").ToString().ToLower() %>,true) +
        
           printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryComments").Clean() %>",false,2),
        "Respiratory");
    <%  } %>
    <%  if (Model.AssessmentTypeNum != 11 && Model.AssessmentTypeNum != 14) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1400) " : string.Empty %>When is the patient dyspneic or noticeably Short of Breath?",true) +
        printview.col(3,
        printview.checkbox("0 &#8211; Patient is not short of breath",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; When walking 20+ feet or climbing stairs",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; With moderate exertion",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; With minimal exertion",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; At rest (during day or night)",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("04").ToString().ToLower() %>)),
        "OASIS M1400");
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1410) " : string.Empty %>Respiratory Treatments utilized at home:",true) +
        printview.col(4,
            printview.checkbox("1 &#8211; Oxygen",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsOxygen").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Ventilator ",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsVentilator").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Bi-level positive pressure",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsContinuous").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsNone").Equals("1").ToString().ToLower() %>)),
            "OASIS M1410");
    <%  } %>
    <% if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Respiratory.ascx", Model); %>
<%  } %>
</script>