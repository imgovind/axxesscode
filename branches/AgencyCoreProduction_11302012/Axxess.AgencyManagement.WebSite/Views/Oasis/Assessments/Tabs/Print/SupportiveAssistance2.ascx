﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Names of organizations providing assistance",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericSupportiveAssistanceName").Clean() %>",0,2),
        "Supportive Assistance");
</script>