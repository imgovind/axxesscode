<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "EliminationForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Elimination.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
     <% Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>GU</legend>
        <%  string[] genericGU = data.AnswerArray("GenericGU"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericGU", "", new { @id = Model.TypeName + "_GenericGUHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Within Normal Limits' id='{0}_GenericGU1' name='{0}_GenericGU' value='1' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Incontinence' id='{0}_GenericGU2' name='{0}_GenericGU' value='2' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU2" class="radio">Incontinence</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Bladder Distention' id='{0}_GenericGU3' name='{0}_GenericGU' value='3' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU3" class="radio">Bladder Distention</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Discharge' id='{0}_GenericGU4' name='{0}_GenericGU' value='4' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU4" class="radio">Discharge</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Frequency' id='{0}_GenericGU5' name='{0}_GenericGU' value='5' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU5" class="radio">Frequency</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Dysuria' id='{0}_GenericGU6' name='{0}_GenericGU' value='6' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU6" class="radio">Dysuria</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Retention' id='{0}_GenericGU7' name='{0}_GenericGU' value='7' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU7" class="radio">Retention</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Urgency' id='{0}_GenericGU8' name='{0}_GenericGU' value='8' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU8" class="radio">Urgency</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Oliguria' id='{0}_GenericGU9' name='{0}_GenericGU' value='9' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU9" class="radio">Oliguria</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Nocturia' id='{0}_GenericGU13' name='{0}_GenericGU' value='13' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("13").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU13" class="radio">Nocturia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Catheter/Device' id='{0}_GenericGU10' name='{0}_GenericGU' value='10' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("10").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU10" class="inline-radio">Catheter/Device:</label>
                        <div class="<%= Model.TypeName %>_GenericGU10More float-right">
                            <%  var genericGUCatheterList = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Foley Catheter ", Value = "2" },
                                    new SelectListItem { Text = "Condom Catheter", Value = "3" },
                                    new SelectListItem { Text = "Suprapubic Catheter", Value = "4" },
                                    new SelectListItem { Text = "Urostomy", Value = "5" },
                                    new SelectListItem { Text = "Other", Value = "6" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericGUCatheterList", "0")); %>
                            <%= Html.DropDownList(Model.TypeName + "_GenericGUCatheterList", genericGUCatheterList, new { @title = "(Optional) Gastrourinary, Catheter/Device Type", @class = "oe" })%>
                        </div>
                        <div class="clear"></div>
                        <div class="<%= Model.TypeName %>_GenericGU10More margin">
                            <label for="<%= Model.TypeName %>_GenericGUCatheterLastChanged">Last Changed</label>
                            <div class="float-right"><input type="text" class="date-picker oe" name="<%= Model.TypeName %>_GenericGUCatheterLastChanged" value="<%= data.AnswerOrEmptyString("GenericGUCatheterLastChanged") %>" id="<%= Model.TypeName %>_GenericGUCatheterLastChanged" title="(Optional) Gastrourinary, Catheter/Device, Date Last Changed" /></div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <%= Html.TextBox(Model.TypeName + "_GenericGUCatheterFrequency", data.AnswerOrEmptyString("GenericGUCatheterFrequency"), new { @id = Model.TypeName + "_GenericGUCatheterFrequency", @class = "vitals", @maxlength = "5", @title = "(Optional) Gastrourinary, Catheter/Device, Frequency" })%>
                                <label for="<%= Model.TypeName %>_GenericGUCatheterFrequency">Fr</label>
                                <%= Html.TextBox(Model.TypeName + "_GenericGUCatheterAmount", data.AnswerOrEmptyString("GenericGUCatheterAmount"), new { @id = Model.TypeName + "_GenericGUCatheterAmount", @class = "vitals", @maxlength = "5", @title = "(Optional) Gastrourinary, Catheter/Device, Amount" })%>
                                <label for="<%= Model.TypeName %>_GenericGUCatheterAmount">cc</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, Urine' id='{0}_GenericGU11' name='{0}_GenericGU' value='11' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("11").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU11" class="radio">Urine:</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericGU11More" class="float-right">
                            <%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericGUUrine", "", new { @id = Model.TypeName + "_GenericGUUrineHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Clear' id='{0}_GenericGUUrine6' name='{0}_GenericGUUrine' value='6' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("6").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine6" class="fixed inline-radio">Clear</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Cloudy' id='{0}_GenericGUUrine1' name='{0}_GenericGUUrine' value='1' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine1" class="fixed inline-radio">Cloudy</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Odorous' id='{0}_GenericGUUrine2' name='{0}_GenericGUUrine' value='2' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine2" class="fixed inline-radio">Odorous</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Sediment' id='{0}_GenericGUUrine3' name='{0}_GenericGUUrine' value='3' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("3").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine3" class="fixed inline-radio">Sediment</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Hematuria' id='{0}_GenericGUUrine4' name='{0}_GenericGUUrine' value='4' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("4").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine4" class="fixed inline-radio">Hematuria</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrourinary, Urine, Other' id='{0}_GenericGUUrine5' name='{0}_GenericGUUrine' value='5' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("5").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericGUUrine5" class="fixed inline-radio">Other</label>
                            </div>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericGUUrine5More" class="float-right">
                            <label for="<%= Model.TypeName %>_GenericGUOtherText"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericGUOtherText", data.AnswerOrEmptyString("GenericGUOtherText"), new { @id = Model.TypeName + "_GenericGUOtherText", @class = "st", @maxlength = "20", @title = "(Optional) Gastrourinary, Urine, Specify Other" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrourinary, External Genitalia' id='{0}_GenericGU12' name='{0}_GenericGU' value='12' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("12").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericGU12" class="radio">External Genitalia:</label>
                        <div id="<%= Model.TypeName %>_GenericGU12More">
                            <div class="float-right">
                                <%= Html.Hidden(Model.TypeName + "_GenericGUNormal", "", new { @id = Model.TypeName + "_GenericGUNormalHidden" })%>
                                <%= Html.RadioButton(Model.TypeName + "_GenericGUNormal", "1", data.AnswerOrEmptyString("GenericGUNormal").Equals("1"), new { @id = Model.TypeName + "_GenericGUNormal1", @class = "no_float radio deselectable", @title = "(Optional) Gastrourinary, External Genitalia, Normal" })%>
                                <label for="<%= Model.TypeName %>_GenericGUNormal1" class="inline-radio">Normal</label>
                                <%= Html.RadioButton(Model.TypeName + "_GenericGUNormal", "0", data.AnswerOrEmptyString("GenericGUNormal").Equals("0"), new { @id = Model.TypeName + "_GenericGUNormal0", @class = "no_float radio deselectable", @title = "(Optional) Gastrourinary, External Genitalia, Abnormal" })%>
                                <label for="<%= Model.TypeName %>_GenericGUNormal0" class="inline-radio">Abnormal</label>
                            </div>
                            <div class="clear"></div>
                            <%= Html.Hidden(Model.TypeName + "_GenericGUClinicalAssessment", "", new { @id = Model.TypeName + "_GenericGUClinicalAssessmentHidden" })%>
                            <div class="margin">
                                <label>As per:</label>
                                <div class="float-right">
                                    <%= Html.RadioButton(Model.TypeName + "_GenericGUClinicalAssessment", "1", data.AnswerOrEmptyString("GenericGUClinicalAssessment").Equals("1"), new { @id = Model.TypeName + "_GenericGUClinicalAssessment1", @class = "no_float radio deselectable", @title = "(Optional) Gastrourinary, External Genitalia, As per Clinician Assessment" })%>
                                    <label for="<%= Model.TypeName %>_GenericGUClinicalAssessment1" class="inline-radio">Clinician Assessment</label>
                                    <br />
                                    <%= Html.RadioButton(Model.TypeName + "_GenericGUClinicalAssessment", "0", data.AnswerOrEmptyString("GenericGUClinicalAssessment").Equals("0"), new { @id = Model.TypeName + "_GenericGUClinicalAssessment0", @class = "no_float radio deselectable", @title = "(Optional) Gastrourinary, External Genitalia, As per Patient/Caregiver Report" })%>
                                    <label for="<%= Model.TypeName %>_GenericGUClinicalAssessment0" class="inline-radio">Pt/CG Report</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Dialysis</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Is patient on dialysis?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericPatientOnDialysis", "", new { @id = Model.TypeName + "_GenericPatientOnDialysisHidden" })%>
                <div class="float-right">
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientOnDialysis", "1", data.AnswerOrEmptyString("GenericPatientOnDialysis").Equals("1"), new { @id = Model.TypeName + "_GenericPatientOnDialysis1", @class = "radio", @title = "(Optional) Dialysis, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientOnDialysis1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientOnDialysis", "0", data.AnswerOrEmptyString("GenericPatientOnDialysis").Equals("0"), new { @id = Model.TypeName + "_GenericPatientOnDialysis0", @class = "radio", @title = "(Optional) Dialysis, No" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientOnDialysis0" class="inline-radio">No</label>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="<%= Model.TypeName %>_Dialysis" class="wide-column">
            <div class="row">
                <%  string[] genericDialysis = data.AnswerArray("GenericDialysis"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDialysis", "", new { @id = Model.TypeName + "_GenericDialysisHidden" })%>
                <label class="strong">Dialysis Type</label>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, Peritoneal Dialysis' id='{0}_GenericDialysis1' name='{0}_GenericDialysis' value='1' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis1" class="radio">Peritoneal Dialysis</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, CCPD' id='{0}_GenericDialysis2' name='{0}_GenericDialysis' value='2' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis2" class="radio">CCPD (Continuous Cyclic Peritoneal Dialysis)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, IPD' id='{0}_GenericDialysis3' name='{0}_GenericDialysis' value='3' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis3" class="radio">IPD (Intermittent Peritoneal Dialysis)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, CAPD' id='{0}_GenericDialysis4' name='{0}_GenericDialysis' value='4' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis4" class="radio">CAPD (Continuous Ambulatory Peritoneal Dialysis)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, Hemodialysis' id='{0}_GenericDialysis5' name='{0}_GenericDialysis' value='5' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis5" class="radio">Hemodialysis</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericDialysis5More" class="margin">
                            <%  string[] genericDialysisHemodialysis = data.AnswerArray("GenericDialysisHemodialysis"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericDialysisHemodialysis", "", new { @id = Model.TypeName + "_GenericDialysisHemodialysisHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Dialysis Type, Hemodialysis, AV Graft/Fistula Site' id='{0}_GenericDialysisHemodialysis1' name='{0}_GenericDialysisHemodialysis' value='1' type='checkbox' {1} />", Model.TypeName, genericDialysisHemodialysis.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDialysisHemodialysis1" class="radio">AV Graft/ Fistula Site:</label>
                            </div>
                            <div id="<%= Model.TypeName %>_GenericDialysisHemodialysis1More" class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDialysisHemodialysisGriftAV", data.AnswerOrEmptyString("GenericDialysisHemodialysisGriftAV"), new { @id = Model.TypeName + "_GenericDialysisHemodialysisGriftAV", @maxlength = "25", @title = "(Optional) Dialysis Type, Hemodialysis, AV Graft/Fistula Site" })%></div>
                            <div class="clear"></div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Dialysis Type, Hemodialysis, Central Venous Catheter Access Site' id='{0}_GenericDialysisHemodialysis2' name='{0}_GenericDialysisHemodialysis' value='2' type='checkbox' {1} />", Model.TypeName, genericDialysisHemodialysis.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDialysisHemodialysis2" class="radio">Central Venous Catheter Access Site:</label>
                            </div>
                            <div id="<%= Model.TypeName %>_GenericDialysisHemodialysis2More" class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDialysisHemodialysisCentralVenousAV", data.AnswerOrEmptyString("GenericDialysisHemodialysisCentralVenousAV"), new { @id = Model.TypeName + "_GenericDialysisHemodialysisCentralVenousAV", @maxlength = "25", @title = "(Optional) Dialysis Type, Hemodialysis, Central Venous Catheter Access Site" })%></div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Catheter Site Free of Infection' id='{0}_GenericDialysis6' name='{0}_GenericDialysis' value='6' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis6" class="radio">Catheter site free from signs and symptoms of infection</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Dialysis Type, Other' id='{0}_GenericDialysis7' name='{0}_GenericDialysis' value='7' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDialysis7" class="radio">Other:</label>
                        <div id="<%= Model.TypeName %>_GenericDialysis7More">
                            <label for="<%= Model.TypeName %>_GenericDialysisOtherDesc"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericDialysisOtherDesc", data.AnswerOrEmptyString("GenericDialysisOtherDesc"), new { @id = Model.TypeName + "_GenericDialysisOtherDesc", @maxlength = "25", @title = "(Optional) Dialysis Type, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="<%= Model.TypeName %>_GenericDialysisCenter" class="strong">Dialysis Center:</label>
                    <%= Html.TextArea(Model.TypeName + "_GenericDialysisCenter", data.AnswerOrEmptyString("GenericDialysisCenter"), 3, 100, new { @id = Model.TypeName + "_GenericDialysisCenter", @maxlength = "25", @title = "(Optional) Dialysis Center" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Urinary</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M1600">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1600" class="green" onclick="Oasis.ToolTip('M1600')">(M1600)</a>
                    Has this patient been treated for a Urinary Tract Infection in the past 14 days?
                </label>    
                <%= Html.Hidden(Model.TypeName + "_M1600UrinaryTractInfection", "", new { @id = Model.TypeName + "_M1600UrinaryTractInfectionHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1600UrinaryTractInfection", "00", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("00"), new { @id = Model.TypeName + "_M1600UrinaryTractInfection0", @title = "(OASIS M1600) Urinary Tract Infection in the Past 14 Days, No" })%>
                        <label for="<%= Model.TypeName %>_M1600UrinaryTractInfection0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1600UrinaryTractInfection", "01", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("01"), new { @id = Model.TypeName + "_M1600UrinaryTractInfection1", @title = "(OASIS M1600) Urinary Tract Infection in the Past 14 Days, Yes" })%>
                        <label for="<%= Model.TypeName %>_M1600UrinaryTractInfection1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1600UrinaryTractInfection", "NA", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("NA"), new { @id = Model.TypeName + "_M1600UrinaryTractInfectionNA", @title = "(OASIS M1600) Urinary Tract Infection in the Past 14 Days, Prophylactic Treatment" })%>
                        <label for="<%= Model.TypeName %>_M1600UrinaryTractInfectionNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient on prophylactic treatment</span>
                        </label>
                    </div>
                <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1600UrinaryTractInfection", "UK", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("UK"), new { @id = Model.TypeName + "_M1600UrinaryTractInfectionUK", @title = "(OASIS M1600) Urinary Tract Infection in the Past 14 Days, Unknown" })%>
                        <label for="<%= Model.TypeName %>_M1600UrinaryTractInfectionUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                <%  } %>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1600')" title="More Information about M1600">?</div>
                </div>
            </div>
            <%  } %>
            <div class="row" id="<%= Model.TypeName %>_M1610">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1610" class="green" onclick="Oasis.ToolTip('M1610')">(M1610)</a>
                    Urinary Incontinence or Urinary Catheter Presence:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1610UrinaryIncontinence", "", new { @id = Model.TypeName + "_M1610UrinaryIncontinenceHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1610UrinaryIncontinence", "00", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("00"), new { @id = Model.TypeName + "_M1610UrinaryIncontinence0", @title = "(OASIS M1610) Urinary Incontinence/Urinary Catheter, No" })%>
                        <label for="<%= Model.TypeName %>_M1610UrinaryIncontinence0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No incontinence or catheter <em>(includes anuria or ostomy for urinary drainage)</em></span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1610UrinaryIncontinence", "01", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("01"), new { @id = Model.TypeName + "_M1610UrinaryIncontinence1", @title = "(OASIS M1610) Urinary Incontinence/Urinary Catheter, Yes" })%>
                        <label for="<%= Model.TypeName %>_M1610UrinaryIncontinence1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient is incontinent</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1610UrinaryIncontinence", "02", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("02"), new { @id = Model.TypeName + "_M1610UrinaryIncontinence2", @title = "(OASIS M1610) Urinary Incontinence/Urinary Catheter, Urinary Catheter" })%>
                        <label for="<%= Model.TypeName %>_M1610UrinaryIncontinence2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient requires a urinary catheter <em>(i.e., external, indwelling, intermittent, suprapubic)</em></span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1610')" title="More Information about M1610">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M1615">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1615" class="green" onclick="Oasis.ToolTip('M1615')">(M1615)</a>
                    When does Urinary Incontinence occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "", new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccurHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "00", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("00"), new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccur0", @title = "(OASIS M1615) Urinary  Incontinence, Timed-Voiding Defers Incontinence" })%>
                        <label for="<%= Model.TypeName %>_M1615UrinaryIncontinenceOccur0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Timed-voiding defers incontinence</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "01", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("01"), new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccur1", @title = "(OASIS M1615) Urinary Incontinence, Occasional Stress Incontinence" })%>
                        <label for="<%= Model.TypeName %>_M1615UrinaryIncontinenceOccur1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Occasional stress incontinence</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "02", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("02"), new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccur2", @title = "(OASIS M1615) Urinary Incontinence, Night" })%>
                        <label for="<%= Model.TypeName %>_M1615UrinaryIncontinenceOccur2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">During the night only</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "03", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("03"), new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccur3", @title = "(OASIS M1615) Urinary Incontinence, Day" })%>
                        <label for="<%= Model.TypeName %>_M1615UrinaryIncontinenceOccur3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">During the day only</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "04", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("04"), new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccur4", @title = "(OASIS M1615) Urinary Incontinence, Day and Night" })%>
                        <label for="<%= Model.TypeName %>_M1615UrinaryIncontinenceOccur4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">During the day and night</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1615')" title="More Information about M1615">?</div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Bowels</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1620">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1620" class="green" onclick="Oasis.ToolTip('M1620')">(M1620)</a>
                    Bowel Incontinence Frequency:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1620BowelIncontinenceFrequency", "", new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequencyHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "00", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("00"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency0", @title = "(OASIS M1620) Bowel Incontinence, Very Rarely or Never" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Very rarely or never has bowel incontinence</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "01", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("01"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency1", @title = "(OASIS M1620) Bowel Incontinence, Less than Once Weekly" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Less than once weekly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "02", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("02"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency2", @title = "(OASIS M1620) Bowel Incontinence, One to Three Times Weekly" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">One to three times weekly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "03", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("03"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency3", @title = "(OASIS M1620) Bowel Incontinence, Four to Six Times Weekly" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Four to six times weekly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "04", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("04"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency4", @title = "(OASIS M1620) Bowel Incontinence, Daily" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">On a daily basis</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "05", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("05"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequency5", @title = "(OASIS M1620) Bowel Incontinence, More than Once Daily" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequency5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">More often than once daily</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "NA", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("NA"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequencyNA", @title = "(OASIS M1620) Bowel Incontinence, Ostomy" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequencyNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient has ostomy for bowel elimination</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1620BowelIncontinenceFrequency", "UK", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("UK"), new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequencyUK", @title = "(OASIS M1620) Bowel Incontinence, Unknown" })%>
                        <label for="<%= Model.TypeName %>_M1620BowelIncontinenceFrequencyUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1620')" title="More Information about M1620">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
            <div class="row" id="<%= Model.TypeName %>_M1630">
                <div class="strong">
                    <a href="javascript:void(0)" title="More Information about M1630" class="green" onclick="Oasis.ToolTip('M1630')">(M1630)</a>
                    Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days):
                    <div class="margin">
                        a) was related to an inpatient facility stay, or<br />
                        b) necessitated a change in medical or treatment regimen?
                    </div>
                </div>
                <%= Html.Hidden(Model.TypeName + "_M1630OstomyBowelElimination", "", new { @id = Model.TypeName + "_M1630OstomyBowelEliminationHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1630OstomyBowelElimination", "00", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("00"), new { @id = Model.TypeName + "_M1630OstomyBowelElimination0", @title = "(OASIS M1630) Ostomy, No" })%>
                        <label for="<%= Model.TypeName %>_M1630OstomyBowelElimination0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Patient does not have an ostomy for bowel elimination.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1630OstomyBowelElimination", "01", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("01"), new { @id = Model.TypeName + "_M1630OstomyBowelElimination1", @title = "(OASIS M1630) Ostomy, Did Not Necessitate Change in Medical or Treatment Regimen" })%>
                        <label for="<%= Model.TypeName %>_M1630OstomyBowelElimination1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient&#8217;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1630OstomyBowelElimination", "02", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("02"), new { @id = Model.TypeName + "_M1630OstomyBowelElimination2", @title = "(OASIS M1630) Ostomy, Did Necessitate Change in Medical or Treatment Regimen" })%>
                        <label for="<%= Model.TypeName %>_M1630OstomyBowelElimination2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1630')" title="More Information about M1630">?</div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>GI</legend>
        <%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericDigestive", "", new { @id = Model.TypeName + "_GenericDigestiveHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Within Normal Limits' id='{0}_GenericDigestive1' name='{0}_GenericDigestive' value='1' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Bowel Sounds' id='{0}_GenericDigestive2' name='{0}_GenericDigestive' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive2" class="inline-radio">Bowel Sounds:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestive2More" class="float-right">
                            <%  var bowelSounds = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Present/WNL x4 quadrants", Value = "1" },
                                    new SelectListItem { Text = "Hyperactive", Value = "2" },
                                    new SelectListItem { Text = "Hypoactive", Value = "3" },
                                    new SelectListItem { Text = "Absent", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveBowelSoundsType", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = Model.TypeName + "_GenericDigestiveBowelSoundsType", @title = "(Optional) Gastrointestinal, Bowel Sounds", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Abdominal Palpation' id='{0}_GenericDigestive3' name='{0}_GenericDigestive' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive3" class="inline-radio">Abdominal Palpation:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestive3More" class="float-right">
                            <%  var abdominalPalpation = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Soft/WNL", Value = "1" },
                                    new SelectListItem { Text = "Firm", Value = "2" },
                                    new SelectListItem { Text = "Tender", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericAbdominalPalpation", "0")); %>
                            <%= Html.DropDownList(Model.TypeName + "_GenericAbdominalPalpation", abdominalPalpation, new { @id = Model.TypeName + "_GenericAbdominalPalpation", @title = "(Optional) Gastrointestinal, Abdominal Palpation", @class = "float-right" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Bowel Incontinence' id='{0}_GenericDigestive4' name='{0}_GenericDigestive' value='4' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive4" class="radio">Bowel Incontinence</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Nausea' id='{0}_GenericDigestive5' name='{0}_GenericDigestive' value='5' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive5" class="radio">Nausea</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Vomiting' id='{0}_GenericDigestive6' name='{0}_GenericDigestive' value='6' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive6" class="radio">Vomiting</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, GERD' id='{0}_GenericDigestive7' name='{0}_GenericDigestive' value='7' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive7" class="radio">GERD</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Abd Girth' id='{0}_GenericDigestive8' name='{0}_GenericDigestive' value='8' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive8" class="inline-radio">Abd Girth:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestive8More" class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveAbdGirthLength", data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength"), new { @id = Model.TypeName + "_GenericDigestiveAbdGirthLength", @class = "vitals", @maxlength = "5", @title = "(Optional) Gastrointestinal, Abd Girth Length" })%></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="strong">Elimination</div>
                <%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBM", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM' id='{0}_GenericDigestive11' name='{0}_GenericDigestive' value='11' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("11").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestive11" class="inline-radio">Last BM:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestive11More" class="float-right">
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMDate">Date:</label>
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" id="<%= Model.TypeName %>_GenericDigestiveLastBMDate" title="(Optional) Gastrointestinal, Elimination, Last BM Date" />
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Within Normal Limits' id='{0}_GenericDigestiveLastBM1' name='{0}_GenericDigestiveLastBM' value='1' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveLastBM1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool' id='{0}_GenericDigestiveLastBM2' name='{0}_GenericDigestiveLastBM' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveLastBM2" class="radio">Abnormal Stool:</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericDigestiveLastBM2More">
                            <%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMAbnormalStoolHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool, Gray' id='{0}_GenericDigestiveLastBMAbnormalStool1' name='{0}_GenericDigestiveLastBMAbnormalStool' value='1' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBMAbnormalStool.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDigestiveLastBMAbnormalStool1" class="fixed inline-radio">Gray</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool, Tarry' id='{0}_GenericDigestiveLastBMAbnormalStool2' name='{0}_GenericDigestiveLastBMAbnormalStool' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBMAbnormalStool.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDigestiveLastBMAbnormalStool2" class="fixed inline-radio">Tarry</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool, Frash Blood' id='{0}_GenericDigestiveLastBMAbnormalStool3' name='{0}_GenericDigestiveLastBMAbnormalStool' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBMAbnormalStool.Contains("3").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDigestiveLastBMAbnormalStool3" class="fixed inline-radio">Fresh Blood</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool, Black' id='{0}_GenericDigestiveLastBMAbnormalStool4' name='{0}_GenericDigestiveLastBMAbnormalStool' value='4' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBMAbnormalStool.Contains("4").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDigestiveLastBMAbnormalStool4" class="fixed inline-radio">Black</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Constipation' id='{0}_GenericDigestiveLastBM3' name='{0}_GenericDigestiveLastBM' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveLastBM3" class="radio">Constipation:</label>
                        <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBMConstipationType", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMConstipationTypeHidden" })%>
                        <div id="<%= Model.TypeName %>_GenericDigestiveLastBM3More" class="float-right">
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMConstipationType", "Chronic", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic"), new { @id = Model.TypeName + "_GenericDigestiveLastBMConstipationTypeChronic", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Constipation, Chronic" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMConstipationTypeChronic" class="inline-radio">Chronic</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMConstipationType", "Acute", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute"), new { @id = Model.TypeName + "_GenericDigestiveLastBMConstipationTypeAcute", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Constipation, Acute" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMConstipationTypeAcute" class="inline-radio">Acute</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMConstipationType", "Occasional", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional"), new { @id = Model.TypeName + "_GenericDigestiveLastBMConstipationTypeOccasional", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Constipation, Occasional" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMConstipationTypeOccasional" class="inline-radio">Occasional</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Elimination, Diarrhea' id='{0}_GenericDigestiveLastBM4' name='{0}_GenericDigestiveLastBM' value='4' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveLastBM4" class="radio">Diarrhea:</label>
                        <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMDiarrheaTypeHidden" })%>
                        <div id="<%= Model.TypeName %>_GenericDigestiveLastBM4More" class="float-right">
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", "Chronic", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic"), new { @id = Model.TypeName + "_GenericDigestiveLastBMDiarrheaTypeChronic", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Diarrhea, Chronic" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMDiarrheaTypeChronic" class="inline-radio">Chronic</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", "Acute", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute"), new { @id = Model.TypeName + "_GenericDigestiveLastBMDiarrheaTypeAcute", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Diarrhea, Acute" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMDiarrheaTypeAcute" class="inline-radio">Acute</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", "Occasional", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional"), new { @id = Model.TypeName + "_GenericDigestiveLastBMDiarrheaTypeOccasional", @class = "no_float", @title = "(Optional) Gastrointestinal, Elimination, Diarrhea, Occasional" })%>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMDiarrheaTypeOccasional" class="inline-radio">Occasional</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="strong">Ostomy:</div>
                <%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDigestiveOstomy", "", new { @id = Model.TypeName + "_GenericDigestiveOstomyHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Ostomy Type' id='{0}_GenericDigestiveOstomy1' name='{0}_GenericDigestiveOstomy' value='1' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveOstomy1" class="inline-radio">Ostomy Type:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestiveOstomy1More" class="float-right">
                            <%  var ostomy = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Ileostomy ", Value = "2" },
                                    new SelectListItem { Text = "Colostomy", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveOstomyType", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveOstomyType", ostomy, new { @id = Model.TypeName + "_GenericDigestiveOstomyType", @title = "(Optional) Gastrointestinal, Ostomy Type", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Ostomy, Stoma Appearance' id='{0}_GenericDigestiveOstomy2' name='{0}_GenericDigestiveOstomy' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveOstomy2" class="inline-radio">Stoma Appearance:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestiveOstomy2More" class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveStomaAppearance", data.AnswerOrEmptyString("GenericDigestiveStomaAppearance"), new { @id = Model.TypeName + "_GenericDigestiveStomaAppearance", @class = "st", @maxlength = "15", @title = "(Optional) Gastrointestinal, Ostomy, Stoma Appearance" })%></div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Gastrointestinal, Ostomy, Surrounding Skin' id='{0}_GenericDigestiveOstomy3' name='{0}_GenericDigestiveOstomy' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDigestiveOstomy3" class="inline-radio">Surrounding Skin:</label>
                        <div id="<%= Model.TypeName %>_GenericDigestiveOstomy3More" class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveSurSkinType", data.AnswerOrEmptyString("GenericDigestiveSurSkinType"), new { @id = Model.TypeName + "_GenericDigestiveSurSkinType", @class = "st", @maxlength = "15", @title = "(Optional) Gastrointestinal, Ostomy, Surrounding Skin" })%></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericGUDigestiveComments">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericGUDigestiveComments", data.AnswerOrEmptyString("GenericGUDigestiveComments"), 5, 70, new { @id = Model.TypeName + "_GenericGUDigestiveComments", @title = "(Optional) Gastrointestinal Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Elimination.ascx", Model); %>
    <%  } %>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericGU10"),
        $(".<%= Model.TypeName %>_GenericGU10More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericGU11"),
        $("#<%= Model.TypeName %>_GenericGU11More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericGUUrine5"),
        $("#<%= Model.TypeName %>_GenericGUUrine5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericGU12"),
        $("#<%= Model.TypeName %>_GenericGU12More"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericPatientOnDialysis", "1",
        $("#<%= Model.TypeName %>_Dialysis"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDialysis5"),
        $("#<%= Model.TypeName %>_GenericDialysis5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDialysisHemodialysis1"),
        $("#<%= Model.TypeName %>_GenericDialysisHemodialysis1More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDialysisHemodialysis2"),
        $("#<%= Model.TypeName %>_GenericDialysisHemodialysis2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDialysis7"),
        $("#<%= Model.TypeName %>_GenericDialysis7More"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_M1610UrinaryIncontinence", "01",
        $("#<%= Model.TypeName %>_M1615"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestive2"),
        $("#<%= Model.TypeName %>_GenericDigestive2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestive3"),
        $("#<%= Model.TypeName %>_GenericDigestive3More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestive8"),
        $("#<%= Model.TypeName %>_GenericDigestive8More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestive11"),
        $("#<%= Model.TypeName %>_GenericDigestive11More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM2"),
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM3"),
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM3More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM4"),
        $("#<%= Model.TypeName %>_GenericDigestiveLastBM4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy1"),
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy1More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy2"),
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy3"),
        $("#<%= Model.TypeName %>_GenericDigestiveOstomy3More"));
</script>