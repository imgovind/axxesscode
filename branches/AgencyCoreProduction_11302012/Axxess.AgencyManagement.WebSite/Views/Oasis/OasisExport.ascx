﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AssessmentExport>>" %>
<span class="wintitle">OASIS Export | <%= Current.AgencyName %></span>
<% string pagename = "OasisExport"; %>
<%  using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = pagename+ "_GenerateOasisForm" })) { %>
    <div class="wraper grid-bg" >
    <div class="float-right buttons">
        <ul>
            <li><%= Html.ActionLink("Export to Excel", "OasisExport", "Export", new { BranchId = ViewData["BranchId"], paymentSources = "1" }, new { id =  pagename +"_ExportLink" })%></li>
        </ul>
    </div>
        <fieldset class="orders-filter">
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="Agency.RebindOasisToExport('<%=pagename %>');">Generate</a></li>
                </ul>
                
            </div>
           
           <label class="strong">Branch:</label><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = pagename + "_BranchId" })%>
           <label class="strong">Payment Source:</label>
               
                    <select id="<%= pagename %>_PaymentSources" name="paymentSources" multiple="multiple">
                        <option value="0">None; no charge for current services</option>
                        <option value="1" selected="selected">Medicare (traditional fee-for-service)</option>
                        <option value="2">Medicare (HMO/ managed care)</option>
                        <option value="3">Medicaid (traditional fee-for-service)</option>
                        <option value="4">Medicaid (HMO/ managed care)</option>
                        <option value="5">Workers' compensation</option>
                        <option value="6">Title programs (e.g., Title III,V, or XX)</option>
                        <option value="7">Other government (e.g.,CHAMPUS,VA,etc)</option>
                        <option value="8">Private insurance</option>
                        <option value="9">Private HMO/ managed care</option>
                        <option value="10">Self-pay</option>
                        <option value="11">Unknown</option>
                        <option value="12">Other</option>
                    </select>
                    <div class="float-left"><input id="<%=pagename %>_SelectAll" class="radio" type="checkbox" /><label for="<%=pagename %>_SelectAll" >Check/Uncheck All</label></div>
        
           </fieldset>  
           
        <div id="<%=pagename %>_GridContainer"><% Html.RenderPartial("Content/OasisExport", Model); %></div>
    </div>
    <div id="<%=pagename %>_BottomPanel" style="position:absolute;right:0;bottom:0;left:0">
            <div class="buttons">
                <ul>
                    <li>
                        <a href="javascript:void(0);" onclick="Oasis.GenerateSubmit($(this));">Generate OASIS File</a>
                    </li>
                    <% if(!Current.IsAgencyFrozen) { %>
                    <li>
                        <a href="javascript:void(0);" onclick="Oasis.MarkAsSubmited('#<%=pagename %>_GenerateOasisForm','Exported');">Mark Selected As Exported</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" onclick="Oasis.MarkAsSubmited('#<%=pagename %>_GenerateOasisForm','CompletedNotExported');">Mark Selected As Completed ( Not Exported )</a>
                    </li>
                    <% } %>
                </ul>
            </div>
        </div>
<% } %>
<script type="text/javascript">
    $("#<%=pagename %>_PaymentSources").multiSelect({
        selectAll: false,
        noneSelected: '- Select Payment Source(s) -',
        oneOrMoreSelected: '*'
    });
    $("#window_oasisExport_content").css({
        "background-color": "#d6e5f3"
    });
    $("#<%=pagename %>_BranchCode").change(function() { Agency.RebindOasisToExport('<%=pagename %>'); });
    $("#<%=pagename %>_SelectAll").change(function() { $("#<%=pagename %>_GridContainer input[name='OasisSelected']").each(function() { $(this).prop("checked", $("#<%=pagename %>_SelectAll").prop("checked")); }) });
</script>