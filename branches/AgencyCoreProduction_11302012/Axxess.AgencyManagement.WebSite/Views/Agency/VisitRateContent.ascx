﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<% var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<fieldset>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_LocationCost_Id" })%>
    <div class="wide-column">
        <div class="column"><div class="row"><label class="float-left">Payor:</label><div class="float-right"><%=Model.InsuranceName%></div></div><div class="row"><label class="float-left">Submitter Name: </label><div class="float-right"><%=Model.SubmitterName%></div></div><div class="row"><label class="float-left">Contact Person Name: </label><div class="float-right"><%=string.Format("{0}, {1}", Model.ContactPersonLastName, Model.ContactPersonFirstName)%></div></div></div>
        <div class="column"><div class="row"><label class="float-left">Submitter Id:</label><div class="float-right"><%=Model.SubmitterId%></div></div><div class="row"><label class="float-left">Submitter Phone: </label><div class="float-right"><%=Model.SubmitterPhone.ToPhone()%></div></div><div class="row"><label class="float-left">Contact Person Phone: </label><div class="float-right"><%=Model.ContactPersonPhone.ToPhone()%></div></div></div>
        <div class="clear"></div>
        <label for="Ub04Locator81cca" class="float-left">UB04 Locator 81CCa:</label><%= Html.Hidden("Ub04Locator81", "Locator1")%><div class="float-left"><%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "Agency_Locator1_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "Agency_Locator1_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "Agency_Locator1_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
        <label for="Ub04Locator81ccb" class="float-left">UB04 Locator 81CCb:</label><%= Html.Hidden("Ub04Locator81", "Locator2")%><div class="float-left"><%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "Agency_Locator2_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "Agency_Locator2_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "Agency_Locator2_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
        <label for="Ub04Locator81ccc" class="float-left">UB04 Locator 81CCc:</label><%= Html.Hidden("Ub04Locator81", "Locator3")%><div class="float-left"><%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "Agency_Locator3_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "Agency_Locator3_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "Agency_Locator3_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
        <label for="Ub04Locator81ccd" class="float-left">UB04 Locator 81CCd:</label><%= Html.Hidden("Ub04Locator81", "Locator4")%><div class="float-left"><%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "Agency_Locator4_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "Agency_Locator4_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "Agency_Locator4_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset>
    <legend>Medicare Address</legend>
    <p style="font-size:xx-small;font-style:italic;">This is not required</p>
    <div class="column">
        <div class="row"><label for="Edit_Insurance_MedicareAddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("MedicareAddressLine1", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressLine1 : "", new { @id = "Edit_Insurance_MedicareAddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
        <div class="row"><label for="Edit_Insurance_MedicareAddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("MedicareAddressLine2", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressLine2 : "", new { @id = "Edit_Insurance_MedicareAddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
    </div>
    <div class="column">
        <div class="row"><label for="Edit_Insurance_MedicareAddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("MedicareAddressCity", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressCity : "", new { @id = "Edit_Insurance_MedicareAddressCity", @class = "text input_wrapper" })%></div></div>
        <div class="row"><label for="Edit_Insurance_MedicareAddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "MedicareAddressStateCode", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressStateCode : "", new { @id = "Edit_Insurance_MedicareAddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("MedicareAddressZipCode", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressZipCode : "", new { @id = "Edit_Insurance_MedicareAddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div></div>
    </div>
</fieldset>

<div class="clear"></div>
<div class="buttons"><ul class="align-center"><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewLocationBillData('<%= Model.Id %>');">Add Visit Information</a></li></ul></div><div class="clear"></div>
<%= Html.Telerik()
    .Grid<ChargeRate>()
        .HtmlAttributes(new { @style = "height:auto; position: relative;margin-bottom: 30px;" })
          .Name("Edit_Location_BillDatas")
            .DataKeys(keys =>{ keys.Add(r => r.Id).RouteKey("Id");  })
                .Columns(columns =>
                {
                    columns.Bound(e => e.DisciplineTaskName).Title("Task");
                    columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                    columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
                    columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                    columns.Bound(e => e.Charge).Format("${0:#0.00}").Title("Rate").Width(45);
                    columns.Bound(e => e.Modifier).Title("Modifier").Width(80);
                    columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
                    //columns.Bound(e => e.Unit).Title("Unit Per Visit").Width(65);
                    columns.Bound(e => e.Id).ClientTemplate("<a  href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditLocationBillData($('#Edit_LocationCost_Id').val(),'<#=Id#>');\" >Edit</a> | <a  href=\"javascript:void(0);\" onclick=\"Agency.DeleteLocationBillData($('#Edit_LocationCost_Id').val(),'<#=Id#>');\" >Delete</a> ").Title("Action").Width(85);

                })
                .DataBinding(dataBinding => dataBinding.Ajax().Select("LocationBillDatas", "Agency", new { locationId = Model.Id }))
                 .Sortable()
                  .Footer(false)%>

