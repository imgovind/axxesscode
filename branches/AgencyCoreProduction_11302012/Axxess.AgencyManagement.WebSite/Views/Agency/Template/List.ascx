﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Templates | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Templates", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencyTemplate>()
        .Name("List_Template")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.Title).Title("Name").Sortable(true);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditTemplate('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Template.Delete('<#=Id#>');\" class=\"deleteTemplate\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Template"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_Template .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Template", Click: UserInterface.ShowNewTemplate } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>