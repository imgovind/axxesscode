﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Adjustment Code | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Add", "AdjustmentCode", FormMethod.Post, new { @id = "newAdjustmentCodeForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="New_AdjustmentCode_Code" class="strong">Code</label>
                <div class="float-right">
                    <%= Html.TextBox("Code", "", new { @id = "New_AdjustmentCode_Code", @class = "required", @maxlength = "100", @style = "width: 300px;" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_AdjustmentCode_Description" class="strong">Description</label>
                <div class="float-right">
                    <%= Html.TextBox("Description", "", new { @id = "New_AdjustmentCode_Description", @class = "required", @maxlength = "100", @style = "width: 300px;" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>
