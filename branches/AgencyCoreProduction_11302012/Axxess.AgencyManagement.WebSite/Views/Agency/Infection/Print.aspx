﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Infection>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : string.Empty %>Infection Log<%= " | " + Model.PatientName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
    <div class="page largerfont">
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
                        <% if (location == null) location = Model.Agency.GetMainOffice(); %>
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "" %>
                        <%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : ""%> <%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() : ""%><br />
                        <%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : ""%>
                    </td><th class="h1">Infection Log</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="bicol">
                            <span class="big">Patient Name: <%= Model.Patient != null ? Model.Patient.DisplayName : string.Empty %></span>
                            <span class="big">MR: <%= Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty %></span><br />
                        </span>
                        <span class="quadcol">
                            <span><strong>Physician:</strong></span>
                            <span><%= Model.PhysicianName.ToTitleCase()%></span>
                            <span><strong>Date of Infection:</strong></span>
                            <span><%= Model.InfectionDate.ToString("MM/dd/yyy")%></span>
                            <span><strong>Primary Diagnosis:</strong></span>
                            <span><%= Model.Diagnosis != null && Model.Diagnosis.ContainsKey("PrimaryDiagnosis") ? Model.Diagnosis["PrimaryDiagnosis"] : ""%></span>
                            <span><strong>Secondary Diagnosis:</strong></span>
                            <span><%= Model.Diagnosis != null && Model.Diagnosis.ContainsKey("SecondaryDiagnosis") ? Model.Diagnosis["SecondaryDiagnosis"] : ""%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Type of Infection</h3>
        <div class="tricol"><%string[] typeOfInfection = Model.InfectionType.IsNotNullOrEmpty() ? Model.InfectionType.Split(';') : null; %>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Gastrointestinal") ? "X" : string.Empty %></span><strong></strong>Gastrointestinal</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Respiratory") ? "X" : string.Empty %></span><strong></strong>Respiratory</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Skin") ? "X" : string.Empty %></span><strong></strong>Skin</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Wound") ? "X" : string.Empty %></span><strong></strong>Wound</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Urinary") ? "X" : string.Empty %></span><strong></strong>Urinary</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Other") ? "X" : string.Empty %></span><strong></strong>Other (Specify)<%= Model.InfectionTypeOther%></span>
        </div><div class="quadcol">
            <span><strong>Treatment Prescribed:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.TreatmentPrescribed.IsNotNullOrEmpty() && Model.TreatmentPrescribed == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.TreatmentPrescribed.IsNotNullOrEmpty() && Model.TreatmentPrescribed == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.TreatmentPrescribed.IsNotNullOrEmpty() && Model.TreatmentPrescribed == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div>
            <span><strong>Treatment/Antibiotic:</strong></span>
            <span><%= Model.Treatment.IsNotNullOrEmpty() ? Model.Treatment : "<br /><br /><br /><br />"%></span>
        </div>
        <h3>Notifications</h3>
        <div class="quadcol">
            <span><strong>M.D. Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>New Orders:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div>
            <span><strong>Narrative:</strong></span>
            <span><%= Model.Orders.IsNotNullOrEmpty() ? Model.Orders : "<br /><br /><br /><br />"%></span>
        </div><div>
            <span><strong>Follow Up:</strong></span>
            <span><%= Model.FollowUp.IsNotNullOrEmpty() ? Model.FollowUp : "<br /><br /><br /><br />"%></span>
        </div><div class="quadcol">
            <span><strong>Signature:</strong></span>
            <span><%= Model.SignatureText %></span>
            <span><strong>Date:</strong></span>
            <span><%= Model.SignatureDate.ToString("MM/dd/yyyy") %></span>
        </div>
    </div>
</body>
</html>
