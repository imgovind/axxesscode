﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
<div class="two-thirds" style="height:146px"> 
    <table width="100%">
        <thead>
            <tr>
                <th colspan="2" class="bigtext"><strong><%=Model.Type %></strong></th>
            </tr>
        </thead>
        <tbody>
            <tr>
<%  if (Model.Visible) { %>
                <td style="width:50%;padding:0 10px">
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Patient Name:</td>
                                <td class="align-left strong"><%= Model.PatientName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient MRN:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientIdNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Medicare Number:&#160;</td>
                                <td class="align-left strong"><%= Model.MedicareNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Insurance/Payer:&#160;</td>
                                <td class="align-left strong"><%= Model.PayorName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">HIPPS:&#160;</td>
                                <td class="align-left strong"><%= Model.HIPPS %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Key:&#160;</td>
                                <td class="align-left strong"><%= Model.ClaimKey %></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width:50%;padding:0 10px">
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">HHRG (Grouper):&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.Hhrg %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Labor Portion:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.LaborAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Non-Labor:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.NonLaborAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Supply Reimbursement:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.NonRoutineSuppliesAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Episode Prospective Pay:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.TotalProspectiveAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Prospective Pay:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.ClaimAmount %></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
<%  } else { %>
                <td colspan="2" align="center">
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Patient Name:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient MRN:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientIdNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Medicare Number:&#160;</td>
                                <td class="align-left strong"><%= Model.MedicareNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Insurance/Payer:&#160;</td>
                                <td class="align-left strong"><%= Model.PayorName %></td>
                            </tr>
    <%  if (Model.Visible) { %>
                            <tr>
                                <td class="align-right">HIPPS:&#160;</td>
                                <td class="align-left strong"><%= Model.HIPPS %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Key:&#160;</td>
                                <td class="align-left strong"><%= Model.ClaimKey %></td>
                            </tr>
    <%  } %>
                        </tbody>
                    </table>
                </td>
<%  } %>
            </tr>
        </tbody>
    </table>
</div>
<div class="one-third encapsulation">
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadClaimLog('{0}','{1}','{2}');\" >Activity Logs</a>",Model.Type,Model.Id, Model.PatientId)%></div>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowClaimRemittance('{0}','{1}');\" >Remittance</a>", Model.Id, Model.Type)%></div>
</div>