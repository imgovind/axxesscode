﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisitForm" })) { %>
<%= Html.Hidden("Id",Model.Id) %>
<%= Html.Hidden("episodeId",Model.EpisodeId) %>
<%= Html.Hidden("patientId",Model.PatientId) %>
<div class="wrapper main">
    <div class="billing">
        <h3 class="align-center">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3><%
   if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0)
      {
          foreach (var billCategoryKey in Model.BillVisitDatas.Keys)
          {
              var billCategoryVisits = Model.BillVisitDatas[billCategoryKey];
              var isBillable = billCategoryKey == BillVisitCategory.Billable;
              if (billCategoryVisits != null && billCategoryVisits.Count > 0)
               { %>
        <ul>
            <li class="align_center"><h3><%= billCategoryKey.GetDescription() %></h3></li>
            <li>
                <span class="rap-checkbox"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Scheduled Date</span>
                <span class="visitdate">Visit Date</span>
                <span class="visithcpcs">HCPCS</span>
                <span class="visitstatus">Status</span>
                <span class="visitunits">Units</span>
                <span class="visitcharge">Charge</span>
            </li>
        </ul><%
            foreach (var disciplineVisitKey in billCategoryVisits.Keys)
          {
                  var visits = billCategoryVisits[disciplineVisitKey];
                  if (visits != null && visits.Count > 0)
                   { %>
        <ol>
            <li class="discpiline-title"><span><%=  disciplineVisitKey.GetDescription() %></span></li><% 
                        int i = 1;
                        foreach (var visit in visits) { %> 
            <li class="<%= i % 2 != 0 ? "odd notready" : "even notready" %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                <label for="<%= "Visit" + visit.EventId.ToString() %>">
                    <span class="rap-checkbox"><%= i%>. <input class="radio" name="Visit" type="checkbox" id="<%= "Visit" + visit.EventId.ToString() %>" value="<%= isBillable ? visit.EventId.ToString() + "\" checked=\"checked" : visit.EventId.ToString() %>" /></span>
                    <span class="visittype"><%= visit.DisciplineTaskName%></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visithcpcs"><%= visit.HCPCSCode %></span>
                    <span class="visitstatus"><%= visit.StatusName %></span>
                    <span class="visitunits"><% = visit.Unit %></span>
                    <span class="visitcharge"><% =string.Format("${0:#0.00}", visit.Charge)%></span>
                </label>
            </li><% i++;} %>
        </ol><%
                    }
                }
            }
        }
    } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(0);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Billing.Navigate(2, '#billingVisitForm');
    $("#final-tabs-2 ol").each(function() { $("li:last", $(this)).addClass("last"); });
</script>
