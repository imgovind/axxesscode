﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div id="supplyTab" class="wrapper main">
<%  using (Html.BeginForm("ManagedSupplyVerify", "Billing", FormMethod.Post, new { @id = "managedBillingSupplyForm" })){ %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "ManagedSupply_PatientId" })%>
    <div class="billing">
        <h3 class="align_center">Episode: <%= Model.EpisodeStartDate.ToShortDateString()%> &#8211; <%= Model.EpisodeEndDate.ToShortDateString()%></h3>
        <div id="ManagedBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>().Name("ManagedBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).ToolBar(commands => {commands.Insert().ButtonType(GridButtonType.Text);}).DataBinding(dataBinding => { dataBinding.Ajax()
                    .Select("ManagedCareSupplyBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId })
                    .Insert("ManagedCareSupplyBillableAdd", "Billing", new { Id = Model.Id, patientId = Model.PatientId})
                    .Update("ManagedCareSupplyBillableUpdate", "Billing", new { Id = Model.Id, patientId = Model.PatientId, IsBillable = true });
                }).HtmlAttributes(new { style = "position:relative;" }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Command(commands => { commands.Edit(); }).Width(140).Title("Action");
                }).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events
                    .OnEdit("Supply.OnSupplyEdit")
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
        <div id="ManagedUnBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Non-Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>().Name("ManagedUnBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).HtmlAttributes(new { style = "position:relative;" }).ToolBar(commands => { commands.Custom(); }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").ReadOnly().Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Command(commands => { commands.Edit(); }).Width(140).Title("Action");
                }).DataBinding(dataBinding =>{ dataBinding.Ajax()
                    .Select("ManagedCareSupplyUnBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId })
                    .Update("ManagedCareSupplyBillableUpdate", "Billing", new { Id = Model.Id, patientId = Model.PatientId, IsBillable = false });
                }).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events
                    .OnEdit("Supply.OnSupplyEdit")
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="ManagedBilling.NavigateBack(2);">Back</a></li>
            <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    ManagedBilling.Navigate(4, '#managedBillingSupplyForm', $("#ManagedSupply_PatientId").val());
    $("#ManagedBillingSupplyGrid .t-grid-toolbar a.t-grid-add").text("Add New Supply");
    $("#ManagedBillingSupplyGrid .t-grid-toolbar").append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"ManagedBilling.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#ManagedBillingSupplyGrid'),false);\"> Mark As Non-Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"ManagedBilling.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#ManagedBillingSupplyGrid'));\"> Delete </a> <lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#ManagedUnBillingSupplyGrid .t-grid-toolbar").empty().append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"ManagedBilling.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#ManagedUnBillingSupplyGrid'),true);\"> Mark As Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"ManagedBilling.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#ManagedUnBillingSupplyGrid'));\"> Delete </a><lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#managedBillingSupplyForm #ManagedBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
    $("#managedBillingSupplyForm #ManagedUnBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
</script>