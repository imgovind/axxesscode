﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("AddManagedClaimPayment", "Billing", FormMethod.Post, new { @id = "newManagedClaimPaymentForm" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Post Payment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Payment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("PaymentAmount", "", new {@class = "text input_wrapper required currency", @maxlength = "" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">Payment Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="PaymentDate" value="" id="PaymentDateValue" /></div>
            </div>
            <div class="row">
                <label class="float-left">Payor:</label>
                <div class="float-right"><%= Html.InsurancesNoneMedicare("Payor", "", true, "-- Select Payor --", new { @class = "requireddropdown" })%></div>
            </div>
             <div class="row">
                <label class="float-left">Comment:</label>
                <div class=""><%= Html.TextArea("Comments", "") %></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Post Payment</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>