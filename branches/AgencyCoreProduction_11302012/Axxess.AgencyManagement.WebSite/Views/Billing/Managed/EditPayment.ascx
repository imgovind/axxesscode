﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimPayment>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateManagedClaimPaymentDetails", "Billing", FormMethod.Post, new { @id = "updateManagedClaimPaymentForm" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("ClaimId", Model.ClaimId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Update Payment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Payment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("PaymentAmount", String.Format("{0:0.00}",Model.Payment), new {@class = "text input_wrapper required", @maxlength = "" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Payment Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="PaymentDateValue" value="<%= Model.PaymentDate <= DateTime.MinValue ? "" : Model.PaymentDate.ToShortDateString() %>" id="PaymentDateValue" /></div>
            </div>
            <div class="row">
                <label class="float-left">Payor:</label>
                <div class="float-right"><%= Html.InsurancesNoneMedicare("Payor", Model.Payor.ToString(), true, "-- Select Payor --", new { @class = "requireddropdown" })%></div>
            </div>
             <div class="row">
                <label class="float-left">Comment:</label>
                <div class=""><%= Html.TextArea("Comments", Model.Comments) %></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>