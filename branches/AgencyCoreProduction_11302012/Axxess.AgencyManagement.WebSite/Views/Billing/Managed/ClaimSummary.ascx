﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateManageBilling" }))%><% { %>
<%if ((Model != null && Model.Claims != null && Model.Claims.Count > 0))
  {%>
<%} else {%><div class="row"><b>Select the claim you want to generate.</b></div><%} %>
<% if (Model != null){ %>
<% if (Model.Claims != null && Model.Claims.Count > 0)
   {%>
<div class="row">
    <table class="claim">
        <thead>
        <tr><th colspan="2"><%=string.Format("{0} | {1}",Model.BranchName,Model.InsuranceName) %></th></tr>
        <tr><th>Patient Name</th><th>Episode Date</th></tr>
        </thead>
        <tbody>
            <% int j = 1;%>
            <% foreach (var final in Model.Claims)
               {%>
            <% if (j % 2 == 0){%><tr class="even"><%}else{ %>
                <tr class="odd"><%} %>
                    <td><%=string.Format("<input type='hidden' id='ManagedClaimSelected{0}' name='ManagedClaimSelected' value='{0}' />", final.Id)%><%=j %>.&#160;&#160;&#160;<%=final.DisplayName + "(" + final.PatientIdNumber + ")"%></td>
                    <td align="center"><%=final.DateRange%></td>
                </tr>
                <% j++;} %>
        </tbody>
    </table>
</div><% }%><div class="clear"></div><% } %>
<%if ((Model.Claims != null && Model.Claims.Count > 0))
  {%>
<% = Html.Hidden("StatusType","Submit") %>
<% = Html.Hidden("BranchId", Model.BranchId)%>
<% = Html.Hidden("PrimaryInsurance", Model.Insurance)%>
<div class="buttons"><ul class=""><% if (Model.IsElectronicSubmssion){ %><li class="align-left"> <a href="javascript:void(0);" onclick="ManagedBilling.SubmitClaimDirectly('#generateManageBilling');">Submit Electronically</a></li><%} else{ %><li class="align-left"> <a href="javascript:void(0);" onclick="ManagedBilling.LoadGeneratedManagedClaim('#generateManageBilling');">Download Claim(s)</a></li><%}%></ul></div>
 <% if (!Model.IsElectronicSubmssion){ %><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.UpdateStatus('#generateManageBilling');">Mark Claim(s) As Submitted</a></li></ul></div><%} %>
<%}%><%}%>

