﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  using (Html.BeginForm("ManagedVisitVerify", "Billing", FormMethod.Post, new { @id = "managedBillingVisitForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id= "ManagedVisit_PatientId"})%>
<div class="wrapper main">
    <div class="billing">
        <h3 class="align_center">Date Range: <%= Model.EpisodeStartDate.ToShortDateString()%> &#8211; <%= Model.EpisodeEndDate.ToShortDateString()%></h3>
    <%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
        <%  foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
            <%  var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
            <%  var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
            <%  if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
        <ul>
            <li class="align_center">
                <h3><%= billCategoryKey.GetDescription()%></h3>
            </li>
            <li>
                <span class="rap-checkbox"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Scheduled Date</span>
                <span class="visitdate">Visit Date</span>
                <span class="visithcpcs">HCPCS</span>
                <span class="visitmodifiers">Modifiers</span>
                <span class="visitstatus">Status</span>
                <span class="visitunits">Units</span>
                <span class="visitcharge">Charge</span>
            </li>
        </ul>
                <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
                    <%  var visits = billCategoryVisits[disciplineVisitKey]; %>
                    <%  if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="discpiline-title">
                <span><%=  disciplineVisitKey.GetDescription() %></span>
            </li>
                        <%  int i = 1; %>
                        <%  foreach (var visit in visits) { %> 
            <li class="<%= i % 2 != 0 ? "odd notready" : "even notready" %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                <label for="<%= "Visit" + visit.EventId.ToString() %>">
                    <span class="rap-checkbox">
                            <%  if (!visit.IsExtraTime) { %>
                        <%= i%>. <input class="radio" name="Visit" type="checkbox" id="<%="Visit" + visit.EventId.ToString() %>" value="<%= isBillable ? visit.EventId.ToString() + "\" checked=\"checked"  : visit.EventId.ToString() %>" />
                            <%  } %>
                    </span>
                    <span class="visittype"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName %></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "" %></span>
                    <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "") %></span>
                    <span class="visithcpcs"><%= visit.HCPCSCode %></span>
                    <span class="visitmodifiers"><%= visit.Modifier %>&nbsp;<%= visit.Modifier2 %>&nbsp;<%= visit.Modifier3 %>&nbsp;<%= visit.Modifier4 %></span>
                    <span class="visitstatus"><%= visit.StatusName %></span>
                    <span class="visitunits"><%= visit.Unit %></span>
                    <span class="visitcharge"><%= string.Format("${0:#0.00}", visit.Charge) %></span>
                </label>
            </li>
                            <%  if (!visit.IsExtraTime) i++; %>
                        <%  } %>
        </ol>
                    <%  } %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ManagedBilling.NavigateBack(1);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
</div>
<%  } %>
<script type="text/javascript">
    ManagedBilling.Navigate(3, "#managedBillingVisitForm", $("#ManagedVisit_PatientId").val());
    $("#ManagedClaimTabStrip-3 ol").each(function() { $("li:last", $(this)).addClass("last") });
</script>