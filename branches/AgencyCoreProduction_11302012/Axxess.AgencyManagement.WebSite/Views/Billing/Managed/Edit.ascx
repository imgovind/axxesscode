﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<span class="wintitle">Managed Care Claim | <%= Model.DisplayName %></span>
<%  Html.Telerik().TabStrip().HtmlAttributes(new { @style = "height:auto" }).Name("ManagedClaimTabStrip")
        .Items(parent => {
            parent.Add().Text("Step 1 of 5:^Demographics").LoadContentFrom("ManagedClaimInfo", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Selected(true);
            parent.Add().Text("Step 2 of 5:^Verify Insurance").LoadContentFrom("ManagedClaimInsurance", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Enabled(Model.IsInfoVerified);
            parent.Add().Text("Step 3 of 5:^Verify Visits").LoadContentFrom("ManagedClaimVisit", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Enabled(Model.IsInsuranceVerified || Model.IsVisitVerified);
            parent.Add().Text("Step 4 of 5:^Verify Supplies").LoadContentFrom("ManagedClaimSupply", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Enabled(Model.IsVisitVerified || Model.IsSupplyVerified);
            parent.Add().Text("Step 5 of 5:^Summary").LoadContentFrom("ManagedSummary", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Enabled(Model.IsSupplyVerified);
        }).ClientEvents(events => events.OnSelect("ManagedBilling.managedClaimTabStripOnSelect")).Render(); %>
<script type="text/javascript">    $("#ManagedClaimTabStrip li.t-item a").each(function() { if ($(this).closest("li").hasClass("t-state-disabled")) { $(this).click(U.BlockClick) } $(this).html("<span><strong>" + ($(this).html().substring(0, $(this).html().indexOf(":^") + 1) + "</strong><br/>" + $(this).html().substring($(this).html().indexOf(":^") + 2)) + "</span>") });</script>
