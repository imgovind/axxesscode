﻿namespace Axxess.DataLoader
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    public class ProcedureCode_Copy
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        [SubSonicIgnore]
        public string FormatCode
        {
            get
            {
                return string.Format("{0}.{1}", this.Code.Substring(0, 2).PadLeft(2, '0'), this.Code.Substring((2)));
            }
        }
    }
}