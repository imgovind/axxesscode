﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class MsjOnePhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\bchsphysicians.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\bchsphysicians_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    if (dataRow.GetValue(9).IsNotNullOrEmpty() && !dataRow.GetValue(9).IsEqual("0"))
                                    {
                                        var physicianData = new AgencyPhysician();
                                        physicianData.Id = Guid.NewGuid();
                                        physicianData.AgencyId = agencyId;
                                        physicianData.IsDeprecated = false;
                                        physicianData.LastName = dataRow.GetValue(0);
                                        physicianData.FirstName = dataRow.GetValue(1);

                                        physicianData.AddressLine1 = dataRow.GetValue(2);
                                        physicianData.AddressLine2 = string.Empty;
                                        physicianData.AddressCity = dataRow.GetValue(3);
                                        physicianData.AddressStateCode = dataRow.GetValue(4);
                                        physicianData.AddressZipCode = dataRow.GetValue(5).Length >= 5 ? dataRow.GetValue(5).Substring(0, 5) : string.Empty;

                                        physicianData.PhoneWork = dataRow.GetValue(6).ToPhoneDB();
                                        physicianData.FaxNumber = dataRow.GetValue(7).ToPhoneDB();
                                        physicianData.UPIN = dataRow.GetValue(8);
                                        physicianData.NPI = dataRow.GetValue(9);

                                        physicianData.LicenseNumber = dataRow.GetValue(10);

                                        physicianData.Created = DateTime.Now;
                                        physicianData.Modified = DateTime.Now;

                                        var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                        if (physician == null)
                                        {
                                            if (Database.Add(physicianData))
                                            {
                                                Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
