﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class SynergyTwoPhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\PhysiciaLlist.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PhysiciaLlist_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    int count = 0;
                                    AgencyPhysician physicianData = null;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty() && !dataRow.GetValue(1).Trim().Equals("Name")&&!dataRow.GetValue(7).Trim().Equals("Address"))
                                        {
                                            count++;
                                            if (count % 2 == 1)
                                            {
                                                physicianData = new AgencyPhysician();
                                                physicianData.Id = Guid.NewGuid();
                                                physicianData.AgencyId = agencyId;
                                                physicianData.IsDeprecated = false;
                                                if(dataRow.GetValue(11).IsNotNullOrEmpty())
                                                    physicianData.UPIN = dataRow.GetValue(0);
                                                physicianData.NPI = dataRow.GetValue(14);
                                                var nameArray = dataRow.GetValue(1).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    physicianData.LastName = nameArray[0].Trim();
                                                    physicianData.FirstName = nameArray[1].Trim();
                                                }
                                                physicianData.Comments += string.Format("Code:{0}.", dataRow.GetValue(7));
                                                if(dataRow.GetValue(8).IsNotNullOrEmpty())
                                                    physicianData.Credentials=dataRow.GetValue(8);
                                                if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                                    physicianData.Licenses = dataRow.GetValue(15);
                                                if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                                {
                                                    physicianData.PhoneWork = dataRow.GetValue(21).Substring(2).ToPhoneDB();
                                                }
                                                if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                                {
                                                    if(dataRow.GetValue(26).Trim().Equals("M"))
                                                        physicianData.Gender="Male";
                                                    if(dataRow.GetValue(26).Trim().Equals("F"))
                                                        physicianData.Gender="Female";
                                                }
                                                if(dataRow.GetValue(28).IsNotNullOrEmpty())
                                                    physicianData.Comments+=string.Format("Ethnicity:{0}.",dataRow.GetValue(28));
                                                if(dataRow.GetValue(33).IsNotNullOrEmpty())
                                                    physicianData.Comments+=string.Format("Employer:{0}.",dataRow.GetValue(33));
                                            }else if(count%2==0)
                                            {
                                                if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                                {
                                                    var addressArray=dataRow.GetValue(7).Split(',');
                                                    if (addressArray[0].Trim() != "")
                                                    {
                                                        var addressPlusCityArray = addressArray[0].Trim().Split(' ');
                                                            physicianData.AddressCity = addressPlusCityArray[addressPlusCityArray.Length - 1];
                                                            physicianData.AddressLine1 = addressArray[0].Trim().Substring(0, (addressArray[0].Trim().Length - physicianData.AddressCity.Length));
                                                       
                                                    }
                                                    if (addressArray[1].Trim() != "")
                                                    {
                                                        var statePlusZipArray = addressArray[1].Trim().Split(' ');
                                                        if (statePlusZipArray.Length > 1)
                                                        {
                                                            physicianData.AddressStateCode = statePlusZipArray[0];
                                                            physicianData.AddressZipCode = statePlusZipArray[1];
                                                        }
                                                        else
                                                            physicianData.AddressStateCode = addressArray[1].Trim();
                                                    }
                                                    
                                                }
                                                if(dataRow.GetValue(17).IsNotNullOrEmpty())
                                                {
                                                    physicianData.FaxNumber=dataRow.GetValue(17).Trim().Substring(5).ToPhoneDB();
                                                }
                                                if(dataRow.GetValue(25).IsNotNullOrEmpty())
                                                {
                                                    physicianData.EmailAddress=dataRow.GetValue(25).Trim().Substring(2);
                                                }
                                                physicianData.Created = DateTime.Now;
                                                physicianData.Modified = DateTime.Now;

                                                var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                                if (physician == null)
                                                {
                                                    if (Database.Add(physicianData))
                                                    {
                                                        Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
