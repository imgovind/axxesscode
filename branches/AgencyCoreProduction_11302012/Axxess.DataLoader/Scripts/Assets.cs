﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    public static class AssetScript
    {
        public static void Run(Guid agencyId)
        {
            var episodes = Database.GetEpisodes(agencyId);
            var notes = Database.GetPatientVisitNotes(agencyId);
            if (notes != null && notes.Count > 0)
            {
                TransferAssetsFromVisitNotes(notes, episodes);
            }

            var socList = Database.GetAssessments<StartOfCareAssessment>(agencyId);
            if (socList != null && socList.Count > 0)
            {
                TransferAssets<StartOfCareAssessment>(socList, episodes);
            }

            var rocList = Database.GetAssessments<ResumptionofCareAssessment>(agencyId);
            if (rocList != null && rocList.Count > 0)
            {
                TransferAssets<ResumptionofCareAssessment>(rocList, episodes);
            }

            var recertList = Database.GetAssessments<RecertificationAssessment>(agencyId);
            if (recertList != null && recertList.Count > 0)
            {
                TransferAssets<RecertificationAssessment>(recertList, episodes);
            }

            var followupList = Database.GetAssessments<FollowUpAssessment>(agencyId);
            if (followupList != null && followupList.Count > 0)
            {
                TransferAssets<FollowUpAssessment>(followupList, episodes);
            }

            var deathList = Database.GetAssessments<DeathAtHomeAssessment>(agencyId);
            if (deathList != null && deathList.Count > 0)
            {
                TransferAssets<DeathAtHomeAssessment>(deathList, episodes);
            }

            var dischargeList = Database.GetAssessments<DischargeFromAgencyAssessment>(agencyId);
            if (dischargeList != null && dischargeList.Count > 0)
            {
                TransferAssets<DischargeFromAgencyAssessment>(dischargeList, episodes);
            }

            var transferList = Database.GetAssessments<TransferDischargeAssessment>(agencyId);
            if (transferList != null && transferList.Count > 0)
            {
                TransferAssets<TransferDischargeAssessment>(transferList, episodes);
            }

            var transferNotList = Database.GetAssessments<TransferNotDischargedAssessment>(agencyId);
            if (transferNotList != null && transferNotList.Count > 0)
            {
                TransferAssets<TransferNotDischargedAssessment>(transferNotList, episodes);
            }
        }

        private static void TransferAssets<T>(List<T> list, List<PatientEpisode> episodes) where T : Assessment, new()
        {
            Console.WriteLine("Item Count: {0}", list.Count);
            list.ForEach(a =>
            {
                if (a.OasisData.IsNotNullOrEmpty())
                {
                    a.Questions = a.OasisData.ToObject<List<Question>>();
                    var data = a.ToDictionary();
                    if (data != null)
                    {
                        var assetList = new List<Guid>();
                        for (int i = 0; i < 5; i++)
                        {
                            var assetId = data.AnswerOrEmptyGuid(string.Format("GenericUploadFile{0}", i));
                            if (!assetId.IsEmpty())
                            {
                                Console.WriteLine("Asset {0} {1}", i, assetId);
                                assetList.Add(assetId);
                            }
                        }

                        if (assetList.Count > 0)
                        {
                            var episode = episodes.Find(e => e.AgencyId == a.AgencyId && e.PatientId == a.PatientId && e.Id == a.EpisodeId);
                            if (episode != null && episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                            {
                                var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var item = schedule.FirstOrDefault(e => e.EventId == a.Id);
                                if (item != null)
                                {
                                    assetList.ForEach(asset =>
                                    {
                                        if (!item.Assets.Contains(asset))
                                        {
                                            item.Assets.Add(asset);
                                        }
                                    });

                                    episode.Schedule = schedule.ToXml();
                                    if (Database.Update<PatientEpisode>(episode))
                                    {
                                        Console.WriteLine("Episode Updated");
                                        Console.WriteLine();
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        private static void TransferAssetsFromVisitNotes(List<PatientVisitNote> list, List<PatientEpisode> episodes)
        {
            Console.WriteLine("Item Count: {0}", list.Count);
            list.ForEach(v =>
            {
                if (v.WoundNote.IsNotNullOrEmpty())
                {
                    var data = v.ToWoundDictionary();
                    if (data != null)
                    {
                        var assetList = new List<Guid>();
                        for (int i = 0; i < 5; i++)
                        {
                            var assetId = data.AnswerOrEmptyGuid(string.Format("GenericUploadFile{0}", i));
                            if (!assetId.IsEmpty())
                            {
                                Console.WriteLine("Asset {0} {1}", i, assetId);
                                assetList.Add(assetId);
                            }
                        }

                        if (assetList.Count > 0)
                        {
                            var episode = episodes.Find(e => e.AgencyId == v.AgencyId && e.PatientId == v.PatientId && e.Id == v.EpisodeId);
                            if (episode != null && episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                            {
                                var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var item = schedule.FirstOrDefault(e => e.EventId == v.Id);
                                if (item != null)
                                {
                                    assetList.ForEach(asset =>
                                    {
                                        if (!item.Assets.Contains(asset))
                                        {
                                            item.Assets.Add(asset);
                                        }
                                    });

                                    episode.Schedule = schedule.ToXml();
                                    if (Database.Update<PatientEpisode>(episode))
                                    {
                                        Console.WriteLine("Episode Updated");
                                        Console.WriteLine();
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}