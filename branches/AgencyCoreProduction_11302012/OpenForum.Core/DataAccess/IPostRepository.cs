﻿using System.Linq;
using System.Collections.Generic;
using OpenForum.Core.Models;

namespace OpenForum.Core.DataAccess
{
    public interface IPostRepository
    {
        IQueryable<Post> Find();
        IQueryable<Post> Search(string query);
        Post FindById(int id);
        void SubmitPost(Post post);
        void SubmitReply(Reply reply);
        Reply FindReplyById(int replyId);

        List<Reply> FindReplies(int postId);
    }
}
