﻿
namespace OpenForum.Core.ViewModels
{
    public interface ITitledViewModel
    {
        string PageTitle { get; set; }
    }
}
