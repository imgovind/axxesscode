﻿namespace Axxess.Api
{
    using Axxess.Api.Contracts;

    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    public class ReportAgent : BaseAgent<IReportService>
    {
        #region Overrides

        public override string ToString()
        {
            return "ReportService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Report Methods

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.CahpsExport(agencyId,agencyLocationId, sampleMonth, sampleYear), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.CahpsExportByPaymentSources(agencyId, agencyLocationId, sampleMonth, sampleYear, paymentSources), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> PPSEpisodeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.PPSEpisodeInformation(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> PPSVisitInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.PPSVisitInformation(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> PPSChargeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.PPSChargeInformation(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> PPSPaymentInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.PPSPaymentInformation(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<PatientsAndVisitsByAgeResult> PatientsAndVisitsByAge(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<PatientsAndVisitsByAgeResult>();
            BaseAgent<IReportService>.Call(c => export = c.PatientsAndVisitsByAge(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<DischargeByReasonResult> DischargesByReason(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<DischargeByReasonResult>();
            BaseAgent<IReportService>.Call(c => export = c.DischargesByReason(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<PrimaryPaymentSourceResult> VisitsByPrimaryPaymentSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<PrimaryPaymentSourceResult>();
            BaseAgent<IReportService>.Call(c => export = c.VisitsByPrimaryPaymentSource(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<VisitByStaffTypeResult> VisitsByStaffType(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<VisitByStaffTypeResult>();
            BaseAgent<IReportService>.Call(c => export = c.VisitsByStaffType(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<ReferralSourceResult> AdmissionsByReferralSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<ReferralSourceResult>();
            BaseAgent<IReportService>.Call(c => export = c.AdmissionsByReferralSource(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<PrincipalDiagnosisResult> PatientsVisitsByPrincipalDiagnosis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<PrincipalDiagnosisResult>();
            BaseAgent<IReportService>.Call(c => export = c.PatientsVisitsByPrincipalDiagnosis(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public CostReportResult CostReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var report = new CostReportResult();
            BaseAgent<IReportService>.Call(c => report = c.CostReport(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return report;
        }

        public List<Dictionary<string, string>> TherapyManagement(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.TherapyManagement(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> HHRGReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.HHRGReport(agencyId, agencyLocationId, startDate, endDate), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> UnbilledVisitsForManagedClaims(Guid agencyId, Guid agencyLocationId, int insurance, int status, DateTime startDate, DateTime endDate)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.UnbilledVisitsForManagedClaims(agencyId, agencyLocationId, insurance, status, startDate, endDate), this.ToString());
            return export;
        }


        #endregion

    }
}
