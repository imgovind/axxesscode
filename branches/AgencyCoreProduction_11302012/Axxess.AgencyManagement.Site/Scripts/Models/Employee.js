﻿var Employee = {
    Init: function() {
        $(".names").alpha({ nocaps: false });
        $(".numeric").numeric();

        $("#btnAdd_Employee_GeneratePassword").click(function() {
            U.postUrl("/LookUp/NewShortGuid", null, function(data) {
                $("#txtAdd_Employee_Password").val(data.text);
            });
        });

        $("#newEmployeeForm").validate({
            messages: {
                EmailAddress: "",
                Password: "",
                FirstName: "",
                LastName: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newEmployeeValidaton").empty();
                            $("#newEmployeeValidaton").hide();
                            var text = "<div style=\"color:green;\"><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved</strong></li></ul></div>";
                            $("#newEmployeeValidaton").append(text);
                            $("#newEmployeeValidaton").show();
                            //Employee.RebindList();
                            Employee.Close($("#newEmployeeForm"));
                        }
                        else {
                            $("#newEmployeeValidaton").empty();
                            $("#newEmployeeValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage'><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below </strong></li> <li><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newEmployeeValidaton").append(text);
                            $("#newEmployeeValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editEmployeeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        var resultObject = eval(result);

                        if (resultObject.isSuccessful) {
                            $("#editEmployeeValidaton").empty();
                            $("#editEmployeeValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully edited !</strong></li></ul></div>";
                            $("#editEmployeeValidaton").append(text);
                            $("#editEmployeeValidaton").show();

                        }
                        else {
                            $("#editEmployeeValidaton").empty();
                            $("#editEmployeeValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage'><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editEmployeeValidaton").append(text);
                            $("#editEmployeeValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#userProfileForm").validate({
            messages: {
                AddressLine1: "",
                AddressCity: "",
                AddressZipCode: "",
                AddressStateCode: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#userProfileValidaton").empty();
                            $("#userProfileValidaton").hide();
                            var text = "<div style=\"color:green;\"><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved</strong></li></ul></div>";
                            $("#userProfileValidaton").append(text);
                            $("#userProfileValidaton").show();
                            Employee.Close($("#userProfileForm"));
                        }
                        else {
                            $("#userProfileValidaton").empty();
                            $("#userProfileValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage'><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below </strong></li> <li><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#userProfileValidaton").append(text);
                            $("#userProfileValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    New: function() {
        $("#newEmployeeValidaton").empty();
        $("#newEmployeeValidaton").hide();
        $("#newEmployeeForm label.error").hide();
        $("#newEmployeeForm").clearForm();
        U.postUrl("/LookUp/NewShortGuid", null, function(data) {
            $("#txtAdd_Employee_Password").val(data.text);
        });
    },
    Profile: function() {
        $("#userProfileValidaton").empty().hide();
        $("#userProfileForm label.error").hide();
        $("#userProfileForm").clearForm();

        U.postUrl("/User/GetProfile", null, function(result) {
            loadProfile(eval(result));
        });

        loadProfile = function(profile) {
            if (profile != null) {
                $("#txtNew_UserProfile_AddressLine1").val((profile.AddressLine1 !== null ? profile.AddressLine1 : ""));
                $("#txtNew_UserProfile_AddressLine2").val((profile.AddressLine2 !== null ? profile.AddressLine2 : ""));
                $("#txtNew_UserProfile_AddressCity").val((profile.AddressCity !== null ? profile.AddressCity : ""));
                $("#txtNew_UserProfile_AddressStateCode").val((profile.AddressStateCode !== null ? profile.AddressStateCode : ""));
                $("#txtNew_UserProfile_AddressZipCode").val((profile.AddressZipCode !== null ? profile.AddressZipCode : ""));

                if (profile.PhoneHome !== null) {
                    var homePhone = profile.PhoneHome;
                    $("#txtNew_UserProfile_HomePhone1").val(homePhone.substring(0, 3));
                    $("#txtNew_UserProfile_HomePhone2").val(homePhone.substring(3, 6));
                    $("#txtNew_UserProfile_HomePhone3").val(homePhone.substring(6, 10));
                }

                if (profile.PhoneMobile !== null) {
                    var mobilePhone = profile.PhoneMobile;
                    $("#txtNew_UserProfile_MobilePhone1").val(mobilePhone.substring(0, 3));
                    $("#txtNew_UserProfile_MobilePhone2").val(mobilePhone.substring(3, 6));
                    $("#txtNew_UserProfile_MobilePhone3").val(mobilePhone.substring(6, 10));
                }
            }
        };
    },
    Delete: function(cont, id) {
        var row = cont.parents('tr:first');
        if (confirm("Are you sure you want to delete this Employee?")) {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/User/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        $(row).remove();
                    }
                }
            });
        }
    },
    Edit: function(id) {
        $("#editEmployeeForm label.error").hide();
        $("#editEmployeeForm").clearForm();
        $("#editEmployeeValidaton").empty().hide();

        var data = 'id=' + id;
        $.ajax({
            url: '/User/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var employee = eval(result);
                loadEmployee(employee);
            }
        });

        loadEmployee = function(employee) {
            $("#txtEdit_EmployeeID").val(id);
            $("#txtEdit_PhysicianEmployee").val((employee.PhysicianEmployee !== null ? employee.PhysicianEmployee : " "));
            $("#txtEdit_Employee_Date").val((employee.PhysicianEmployeeDate !== null ? new Date(employee.PhysicianEmployeeDate).getMonth() + "/" + new Date(employee.PhysicianEmployeeDate).getDate() + "/" + new Date(employee.PhysicianEmployeeDate).getFullYear() : " "));
            $("#txtEdit_Employee_EmployeeSource").val((employee.EmployeeSource !== null ? employee.EmployeeSource : " "));
            $("#txtEdit_Referrer_FirstName").val((employee.ReferrerFirstName !== null ? employee.ReferrerFirstName : " "));
            $("#txtEdit_Referrer_LastName").val((employee.ReferrerLastName !== null ? employee.ReferrerLastName : " "));
            $('input[name=Gender][value=' + employee.Gender.toString() + ']').attr('checked', true);
        };
    },
    NewEmployee: function() {
        var refer = $('#List_Employee_NewButton');
        href = "javascript:void(0);";
        refer.attr('href', href);
    },
    RebindList: function() {
        var EmployeeGrid = $('#ExistingEmployeeGrid').data('tGrid');
        EmployeeGrid.rebind();
    },
    Close: function(control) {
        control.closest('div.window').hide();
    }
}