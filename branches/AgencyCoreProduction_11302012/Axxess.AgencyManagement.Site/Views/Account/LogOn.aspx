﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login - Axxess Home Health Management System</title>
    <link type="text/css" href="/Content/account.css" rel="Stylesheet" />
</head>
<body>
    <div id="login-wrapper">
        <div class="box-header login">
            AXXESS&trade; LOGIN
        </div>
        <div class="box">
            <div id="messages" class="notification tip">
                <span class="strong">Are you in a public place?</span><br />
                Remember to log out of your session.
            </div>
            <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login" })) %>
            <% { %>
            <div class="row">
                <%= Html.LabelFor(m => m.UserName) %>
                <%= Html.TextBoxFor(m => m.UserName, new { @id="txtUsername", @class = "required" })%>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.Password) %>
                <%= Html.PasswordFor(m => m.Password, new { @id="txtPassword", @class = "required" })%>
            </div>
            <div class="row tr">
                <input type="checkbox" id="rememberme" checked="checked" class="checkbox" />
                <label class="checkbox tl strong" for="rememberme" style="width: 105px">
                    Remember me</label>
                <input type="submit" value="Login" class="button" style="width: 90px!important;" />
                <br />
                <a href="/Reset">Forgot your password?</a>
            </div>
            <% } %>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/jquery.validate.js")
             .Add("/Plugins/jquery.form.js")
             .Add("/Plugins/jquery.blockUI.js")
             .Add("/Models/Utility.js")
             .Add("/Models/Account.js")
             .Compress(true))
        .OnDocumentReady(() =>
        { 
    %>
    Logon.Init();
    <% 
        }).Render(); %>
</body>
</html>
