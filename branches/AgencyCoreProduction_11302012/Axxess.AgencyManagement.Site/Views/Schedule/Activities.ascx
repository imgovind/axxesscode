﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleActivityArgument>" %>
<%Html.Telerik().Grid<ScheduleEvent>()
                                                .Name("ScheduleActivityGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(o => o.EventId)
                                              .ClientTemplate("<input type='checkbox' name='checkedRecords' value='<#= EventId #>' />")
                                              .Title("Select")
                                              .Width(50)
                                              .HtmlAttributes(new { style = "text-align:center" });
                                                    columns.Bound(p => p.DisciplineTaskName)
                                                        .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('<#=EpisodeId#>');<#=Url#>\"><#=DisciplineTaskName#></a>")
                                                        .Title("Task");
                                                    columns.Bound(p => p.EventDate).Format("{0:MM/dd/yyyy}").Title("Target Date").Width(100);
                                                    columns.Bound(p => p.UserName);
                                                    columns.Bound(p => p.StatusName);
                                                    columns.Bound(p => p.EventId)
                           .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"<#=Url#>\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Schedule.Delete($(this),'<#=EventId#>','<#=UserId#>');\" >Delete</a>|<span><a  value=\"a\" href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this),'<#=EpisodeId#>','<#=PatientId#>','<#=EventId#>','<#=UserId#>');\" >ReAssign</a></span> ")
                           .Title("Action").Width(180);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false).HtmlAttributes(new { Style = "min-width:100px;" })
                                                .Render();                                                
                                                                                                
%>