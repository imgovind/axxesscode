﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpEliminationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1610) Urinary Incontinence or Urinary Catheter Presence:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1610UrinaryIncontinence", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No incontinence or catheter (includes anuria or ostomy for urinary drainage)
            [ Go to M1620 ]<br />
            <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient is incontinent<br />
            <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
            suprapubic) [ Go to M1620 ]
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1620) Bowel Incontinence Frequency:
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1620BowelIncontinenceFrequency", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Very rarely or never has bowel incontinence<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Less than once weekly<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - One to three times weekly<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Four to six times weekly<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - On a daily basis<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - More often than once daily<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient has ostomy for bowel elimination<br />
                <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "UK", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unknown [Omit “UK” option on FU, DC]
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel
                elimination that (within the last 14 days): a) was related to an inpatient facility
                stay, or b) necessitated a change in medical or treatment regimen?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1630OstomyBowelElimination", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "00", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient does not have an ostomy for bowel elimination.<br />
            <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "01", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient's ostomy was not related to an inpatient stay and did not necessitate
            change in medical or treatment regimen.<br />
            <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "02", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - The ostomy was related to an inpatient stay or did necessitate change in medical
            or treatment regimen.
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
