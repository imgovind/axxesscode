﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareTherapyNeedForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode
                for which this assessment will define a case mix group, what is the indicated need
                for therapy visits (total of reasonable and necessary physical, occupational, and
                speech-language pathology visits combined)? (Enter zero [ “000” ] if no therapy
                visits indicated.)
            </div>
        </div>
        <div class="padding">
            <%=Html.TextBox("StartOfCare_M2200NumberOfTherapyNeed", data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : "", new { @id = "StartOfCare_M2200NumberOfTherapyNeed"})%>
            &nbsp;Number of therapy visits indicated (total of physical, occupational and speech-language
            pathology combined).<br />
            <input name="StartOfCare_M2200TherapyNeed" value="" type="hidden" />
            <input name="StartOfCare_M2200TherapyNeed" value="1" type="checkbox" '<% if(data.ContainsKey("M2200TherapyNeed") && data["M2200TherapyNeed"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;NA
            - Not Applicable: No case mix group defined by this assessment.
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M2250) Plan of Care Synopsis: (Check only one box in each row.) Does the physician-ordered
                plan of care include the following:
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <th>
                Plan / Intervention
            </th>
            <th>
                No
            </th>
            <th>
                Yes
            </th>
            <th colspan="2">
                Not Applicable
            </th>
        </tr>
        <tr>
            <td>
                a. Patient-specific parameters for notifying physician of changes in vital signs
                or other clinical findings
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250PatientParameters", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "00", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "01", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "NA", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Physician has chosen not to establish patient-specific parameters for this patient.
                Agency will use standardized clinical guidelines accessible for all care providers
                to reference
            </td>
        </tr>
        <tr>
            <td>
                b. Diabetic foot care including monitoring for the presence of skin lesions on the
                lower extremities and patient/caregiver education on proper foot care
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250DiabeticFoot", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "00", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "01", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "NA", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Patient is not diabetic or is bilateral amputee
            </td>
        </tr>
        <tr>
            <td>
                c. Falls prevention interventions
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250FallsPrevention", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "00", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "01", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "NA", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Patient is not assessed to be at risk for falls
            </td>
        </tr>
        <tr>
            <td>
                d. Depression intervention(s) such as medication, referral for other treatment,
                or a monitoring plan for current treatment
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250DepressionPrevention", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "00", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "01", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "NA", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Patient has no diagnosis or symptoms of depression
            </td>
        </tr>
        <tr>
            <td>
                e. Intervention(s) to monitor and mitigate pain
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250MonitorMitigatePainIntervention", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "00", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "01", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "NA", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                No pain identified
            </td>
        </tr>
        <tr>
            <td>
                f. Intervention(s) to prevent pressure ulcers
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250PressureUlcerIntervention", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "00", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "01", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "NA", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Patient is not assessed to be at risk for pressure ulcers
            </td>
        </tr>
        <tr>
            <td>
                g. Pressure ulcer treatment based on principles of moist wound healing OR order
                for treatment based on moist wound healing has been requested from physician
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_M2250PressureUlcerTreatment", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "00", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "01", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "NA", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
            </td>
            <td>
                Patient has no pressure ulcers with need for moist wound healing
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
