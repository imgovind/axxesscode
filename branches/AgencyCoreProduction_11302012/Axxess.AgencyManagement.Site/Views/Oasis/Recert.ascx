﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="oasisAssWindowContainer">
    <div id="editRecertificationTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
        <ul>
            <li><a href="#clinicalRecord_recertification">Clinical Record Items</a></li>
            <li><a href="#patienthistory_recertification">Patient History & Diagnoses</a></li>
            <li><a href="#riskassessment_recertification">Risk Assessment</a></li>
            <li><a href="#prognosis_recertification">Prognosis</a></li>
            <li><a href="#supportiveassistance_recertification">Supportive Assistance</a></li>
            <li><a href="#sensorystatus_recertification">Sensory Status</a></li>
            <li><a href="#pain_recertification">Pain</a></li>
            <li><a href="#integumentarystatus_recertification">Integumentary Status</a></li>
            <li><a href="#respiratorystatus_recertification">Respiratory Status</a></li>
            <li><a href="#endocrine_recertification">Endocrine</a></li>
            <li><a href="#cardiacstatus_recertification">Cardiac Status</a></li>
            <li><a href="#eliminationstatus_recertification">Elimination Status</a></li>
            <li><a href="#nutrition_recertification">Nutrition</a></li>
            <li><a href="#behaviourialstatus_recertification">Neuro/Behaviourial Status</a></li>
            <li><a href="#adl_recertification">ADL/IADLs</a></li>
            <li><a href="#suppliesworksheet_recertification">Supplies Worksheet</a></li>
            <li><a href="#medications_recertification">Medications</a></li>
            <li><a href="#therapyneed_recertification">Therapy Need & Plan Of Care</a></li>
            <li><a href="#ordersdisciplinetreatment_recertification">Orders for Discipline and Treatment</a></li>
        </ul>
        <div style="width: 179px;">
            <input id="recertificationValidation" type="button" value="Validate" onclick="Recertification.Validate('<%=Model.Id%>');" /></div>
        <div id="clinicalRecord_recertification" class="general abs">
            <% Html.RenderPartial("~/Views/Oasis/Recertification/NewDemographics.ascx", Model); %>
        </div>
        <div id="patienthistory_recertification" class="general abs">
        </div>
        <div id="riskassessment_recertification" class="general abs">
        </div>
        <div id="prognosis_recertification" class="general abs">
        </div>
        <div id="supportiveassistance_recertification" class="general abs">
        </div>
        <div id="sensorystatus_recertification" class="general abs">
        </div>
        <div id="pain_recertification" class="general abs">
        </div>
        <div id="integumentarystatus_recertification" class="general abs">
        </div>
        <div id="respiratorystatus_recertification" class="general abs">
        </div>
        <div id="endocrine_recertification" class="general abs">
        </div>
        <div id="cardiacstatus_recertification" class="general abs">
        </div>
        <div id="eliminationstatus_recertification" class="general abs">
        </div>
        <div id="nutrition_recertification" class="general abs">
        </div>
        <div id="behaviourialstatus_recertification" class="general abs">
        </div>
        <div id="adl_recertification" class="general abs">
        </div>
        <div id="suppliesworksheet_recertification" class="general abs">
        </div>
        <div id="medications_recertification" class="general abs">
        </div>
        <div id="therapyneed_recertification" class="general abs">
        </div>
        <div id="ordersdisciplinetreatment_recertification" class="general abs">
        </div>
    </div>
</div>
