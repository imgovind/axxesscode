﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="oasisAssWindowContainer">
    <div id="followUpTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
        <ul>
            <li><a href="#clinicalRecord_followUp">Clinical Record Items</a></li>
            <li><a href="#patienthistory_followUp">Patient History & Diagnoses</a></li>
            <li><a href="#sensorystatus_followUp">Sensory Status</a></li>
            <li><a href="#pain_followUp">Pain</a></li>
            <li><a href="#integumentarystatus_followUp">Integumentary Status</a></li>
            <li><a href="#respiratorystatus_followUp">Respiratory Status</a></li>
            <li><a href="#eliminationstatus_followUp">Elimination Status</a></li>
            <li><a href="#adl_followUp">ADL/IADLs</a></li>
            <li><a href="#medications_followUp">Medications</a></li>
            <li><a href="#therapyneed_followUp">Therapy Need & Plan Of Care</a></li>
        </ul>
        <div style="width: 179px;">
            <input id="followUpValidation" type="button" value="Validate" onclick="FollowUp.Validate('<%=Model.Id%>');" /></div>
        <div id="clinicalRecord_followUp" class="general abs">
            <% Html.RenderPartial("~/Views/Oasis/Followup/NewDemographics.ascx", Model); %>
        </div>
        <div id="patienthistory_followUp" class="general abs">
        </div>
        <div id="sensorystatus_followUp" class="general abs">
        </div>
        <div id="pain_followUp" class="general abs">
        </div>
        <div id="integumentarystatus_followUp" class="general abs">
        </div>
        <div id="respiratorystatus_followUp" class="general abs">
        </div>
        <div id="eliminationstatus_followUp" class="general abs">
        </div>
        <div id="adl_followUp" class="general abs">
        </div>
        <div id="medications_followUp" class="general abs">
        </div>
        <div id="therapyneed_followUp" class="general abs">
        </div>
    </div>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/FollowUp.js"))
       .OnDocumentReady(() =>
        {%>
FollowUp.Init();
<%}); 
%>