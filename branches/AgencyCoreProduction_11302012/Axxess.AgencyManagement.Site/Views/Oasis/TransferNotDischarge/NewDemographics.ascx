﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedDemographicsForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0010) CMS Certification Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0010CertificationNumber", data.ContainsKey("M0010CertificationNumber") ? data["M0010CertificationNumber"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0010CertificationNumber" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0014) Branch State:</div>
        </div>
        <div class="right marginOasis">
            <select class="AddressStateCode" name="TransferInPatientNotDischarged_M0014BranchState" id="TransferInPatientNotDischarged_M0014BranchState">
                <option value="" selected>** Select State **</option>
            </select>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0016) Branch ID Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0016BranchId", data.ContainsKey("M0016BranchId") ? data["M0016BranchId"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0016BranchId" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0018) National Provider Identifier (NPI)</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0018NationalProviderId", data.ContainsKey("M0018NationalProviderId") ? data["M0018NationalProviderId"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0018NationalProviderId" })%>
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown" value=" " />
            <input type="checkbox" id="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown" name="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown"
                value="1"  '<% if(data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;UK
            – Unknown or Not Available</div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0020) Patient ID Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0020PatientIdNumber", data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0020PatientIdNumber" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0030) Start of Care Date:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0030SocDate", data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0030SocDate" })%>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                Episode Start Date:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_GenericEpisodeStartDate", data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_GenericEpisodeStartDate" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0032) Resumption of Care Date:
            </div>
        </div>
        <div class="padding">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0032ROCDate", data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0032ROCDate" , @disabled="disabled"})%>
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0032ROCDateNotApplicable" value="" />
            <input id="TransferInPatientNotDischarged_M0032ROCDateNotApplicable" name="TransferInPatientNotDischarged_M0032ROCDateNotApplicable"
                type="checkbox" value="1" '<% if(data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            &nbsp;NA - Not Applicable
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0040) Patient Name:</div>
        </div>
        <div class="right marginOasis">
            <div class="padding">
                Suffix :
                <%=Html.TextBox("TransferInPatientNotDischarged_M0040Suffix", data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0040Suffix", @style = "width: 20px;" })%>
                &nbsp; First :
                <%=Html.TextBox("TransferInPatientNotDischarged_M0040FirstName", data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0040FirstName"})%>
                <br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MI :
                <%=Html.TextBox("TransferInPatientNotDischarged_M0040MI", data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0040MI", @style = "width: 20px;" })%>
                &nbsp; &nbsp;Last:
                <%=Html.TextBox("TransferInPatientNotDischarged_M0040LastName", data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0040LastName" })%>
            </div>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0050) Patient State of Residence:</div>
        </div>
        <div class="right marginOasis">
            <select class="AddressStateCode" name="TransferInPatientNotDischarged_M0050PatientState" id="TransferInPatientNotDischarged_M0050PatientState">
                <option value="0" selected>** Select State **</option>
            </select>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0060) Patient Zip Code:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0060PatientZipCode", data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0060PatientZipCode" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0063) Medicare Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0063PatientMedicareNumber", data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0063PatientMedicareNumber" })%>
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown" value="" />
            <input id="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown" name="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown"
                type="checkbox" value="1" '<% if( data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            &nbsp;NA – No Medicare</div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0064) Social Security Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0064PatientSSN", data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0064PatientSSN" })%>
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0064PatientSSNUnknown" value="" />
            <input id="TransferInPatientNotDischarged_M0064PatientSSNUnknown" name="TransferInPatientNotDischarged_M0064PatientSSNUnknown"
                type="checkbox" value="1" '<% if( data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            &nbsp;UK – Unknown or Not Available</div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0065) Medicaid Number:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0065PatientMedicaidNumber", data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0065PatientMedicaidNumber" })%>
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown" value="" />
            <input id="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown" name="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown"
                type="checkbox" value="1" '<% if( data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            &nbsp;NA – No Medicaid</div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0066) Birth Date:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0066PatientDoB", data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0066PatientDoB" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow  title">
            <div class="padding">
                (M0069) Gender:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.Hidden("TransferInPatientNotDischarged_M0069Gender", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0069Gender", "1", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "1" ? true : false, new { @id = "" })%>Male
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0069Gender", "2", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "2" ? true : false, new { @id = "" })%>Female
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0080) Discipline of Person Completing Assessment:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("TransferInPatientNotDischarged_M0080DisciplinePerson", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "01", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - RN
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "02", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "02" ? true : false, new { @id = "" })%>
            &nbsp;2 - PT
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "03", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "03" ? true : false, new { @id = "" })%>
            &nbsp;3 - SLP/ST
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "04", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "04" ? true : false, new { @id = "" })%>
            &nbsp;4 - OT
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M0090) Date Assessment Completed:</div>
        </div>
        <div class="right marginOasis">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0090AssessmentCompleted", data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0090AssessmentCompleted" })%>
        </div>
    </div>
</div>
<div class="rowOasis assessmentType">
    <div class="insiderow title">
        <div class="padding">
            (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
    </div>
    <div class="insideCol">
        <div class="insiderow margin">
            <u>Start/Resumption of Care</u></div>
        <div class="insiderow margin">
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="01" />&nbsp;1
            – Start of care—further visits planned<br />
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="03"  />&nbsp;3
            – Resumption of care (after inpatient stay)<br />
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow">
            <u>Follow-Up</u></div>
        <div class="insiderow">
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="04" />&nbsp;4
            – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="05" />&nbsp;5
            – Other follow-up [ Go to M0110 ]<br />
        </div>
    </div>
    <div class="insideColFull">
        <div class="insiderow margin">
            <u>Transfer to an Inpatient Facility</u></div>
        <div class="insiderow margin">
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="06" checked="checked"/>&nbsp;6
            – Transferred to an inpatient facility—patient not discharged from agency [ Go to
            M1040]<br />
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="07" />&nbsp;7
            – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
            ]<br />
        </div>
    </div>
    <div class="insideColFull">
        <div class="insiderow margin">
            <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
        <div class="insiderow margin">
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="08" />&nbsp;8
            – Death at home [ Go to M0903 ]<br />
            <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="09" />&nbsp;9
            – Discharge from agency [ Go to M1040 ]<br />
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M0102) Date of Physician-ordered Start of Care (Resumption of Care): If the physician
                indicated a specific start of care (resumption of care) date when the patient was
                referred for home health services, record the date specified.</div>
        </div>
        <div class="padding">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0102PhysicianOrderedDate", data.ContainsKey("M0102PhysicianOrderedDate") ? data["M0102PhysicianOrderedDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0102PhysicianOrderedDate" })%>
            [ Go to M0110, if date entered ]<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable" value="" />
            <input id="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable" name="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable"
                type="checkbox" value="1" '<% if( data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;NA
            –No specific SOC date ordered by physician
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M0104) Date of Referral: Indicate the date that the written or verbal referral
                for initiation or resumption of care was received by the HHA.
            </div>
        </div>
        <div class="padding">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0104ReferralDate", data.ContainsKey("M0104ReferralDate") ? data["M0104ReferralDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0104ReferralDate" })%>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M0110) Episode Timing: Is the Medicare home health payment episode for which this
                assessment will define a case mix group an “early” episode or a “later” episode
                in the patient’s current sequence of adjacent Medicare home health payment episodes?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("TransferInPatientNotDischarged_M0110EpisodeTiming", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0110EpisodeTiming", "01", data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - Early
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0110EpisodeTiming", "02", data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? true : false, new { @id = "" })%>
            Later
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0110EpisodeTiming", "UK", data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? true : false, new { @id = "" })%>
            - Unknown
            <%=Html.RadioButton("TransferInPatientNotDischarged_M0110EpisodeTiming", "NA", data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? true : false, new { @id = "" })%>
            - Not Applicable: No Medicare case mix group
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow title">
        <div class="padding">
            (M0140) Race/Ethnicity: (Mark all that apply.)</div>
    </div>
    <div class="insideCol">
        <div class="padding">
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceAMorAN" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceAMorAN" type="checkbox" value="1" '<% if( data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;1
            - American Indian or Alaska Native<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceAsia" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceAsia" type="checkbox" value="1"  '<% if( data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;2
            - Asian<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceBalck" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceBalck" type="checkbox" value="1"  '<% if( data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;3
            - Black or African-American<br />
        </div>
    </div>
    <div class="insideCol">
        <div class="padding">
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceHispanicOrLatino" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceHispanicOrLatino" type="checkbox" value="1" '<% if( data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;4
            - Hispanic or Latino<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceNHOrPI" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceNHOrPI" type="checkbox" value="1" '<% if( data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;5
            - Native Hawaiian or Pacific Islander<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceWhite" value="" />
            <input name="TransferInPatientNotDischarged_M0140RaceWhite" type="checkbox" value="1" '<% if( data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;6
            - White
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull title">
        <div class="padding">
            (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
    </div>
    <div class="insideCol ">
        <div class="padding">
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceNone" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceNone" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;0
            - None; no charge for current services<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCREFFS" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCREFFS" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;1
            - Medicare (traditional fee-for-service)<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCREHMO" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCREHMO" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;2
            - Medicare (HMO/managed care/Advantage plan)<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCAIDFFS" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCAIDFFS" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;3
            - Medicaid (traditional fee-for-service)<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMACIDHMO" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceMACIDHMO" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;4
            - Medicaid (HMO/managed care)
            <br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceWRKCOMP" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceWRKCOMP" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;5
            - Workers' compensation<br />
            <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceTITLPRO" value="" />
            <input name="TransferInPatientNotDischarged_M0150PaymentSourceTITLPRO" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;6
            - Title programs (e.g., Title III, V, or XX)<br />
        </div>
    </div>
    <div class="insideCol adjust">
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceOTHGOVT" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourceOTHGOVT" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;7
        - Other government (e.g., TriCare, VA, etc.)<br />
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourcePRVINS" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourcePRVINS" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;8
        - Private insurance<br />
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourcePRVHMO" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourcePRVHMO" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;9
        - Private HMO/managed care<br />
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceSelfPay" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourceSelfPay" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;10
        - Self-pay<br />
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceOtherSRS" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourceOtherSRS" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;11
        - Other (specify)&nbsp;&nbsp;&nbsp;
        <%=Html.TextBox("TransferInPatientNotDischarged_M0150PaymentSourceOther", data.ContainsKey("M0150PaymentSourceOther") ? data["M0150PaymentSourceOther"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0150PaymentSourceOther" })%><br />
        <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceUnknown" value="" />
        <input name="TransferInPatientNotDischarged_M0150PaymentSourceUnknown" type="checkbox" value="1" '<% if( data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;UK
        - Unknown<br />
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
