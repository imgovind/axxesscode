﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareNutritionForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] genericNutrition = data.ContainsKey("GenericNutrition") && data["GenericNutrition"].Answer != "" ? data["GenericNutrition"].Answer.Split(',') : null; %>
        <tbody>
            <tr>
                <th>
                    Nutrition
                </th>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>
                            <input type="hidden" name="ResumptionOfCare_GenericNutrition" value=" " />
                            <input name="ResumptionOfCare_GenericNutrition" value="1" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("1")  ){ %>checked="checked"<% }%>'" />
                            WNL (Within Normal Limits) </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>
                            <input name="ResumptionOfCare_GenericNutrition" value="2" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            Dysphagia </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>
                            <input name="ResumptionOfCare_GenericNutrition" value="3" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            Decreased Appetite </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="columns">
                        <li class="spacer">
                            <input name="ResumptionOfCare_GenericNutrition" value="4" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            Weight Loss/Gain </li>
                        <li class="spacer">
                            <%=Html.Hidden("ResumptionOfCare_GenericNutritionWeightGainLoss", " ", new { @id = "" })%>
                            <%=Html.RadioButton("ResumptionOfCare_GenericNutritionWeightGainLoss", "Loss", data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Loss" ? true : false, new { @id = "" })%>
                            Loss
                            <%=Html.RadioButton("ResumptionOfCare_GenericNutritionWeightGainLoss", "Gain", data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Gain" ? true : false, new { @id = "" })%>
                            Gain </li>
                        <li>Amount:
                            <%=Html.TextBox("ResumptionOfCare_GenericNutritionWeightAmount", data.ContainsKey("GenericNutritionWeightAmount") ? data["GenericNutritionWeightAmount"].Answer : "", new { @id = "ResumptionOfCare_GenericNutritionWeightAmount", @size = "10", @maxlength = "10" })%>
                        </li>
                        <li>in:
                            <%=Html.TextBox("ResumptionOfCare_GenericNutritionWeightAmountIn", data.ContainsKey("GenericNutritionWeightAmountIn") ? data["GenericNutritionWeightAmountIn"].Answer : "", new { @id = "ResumptionOfCare_GenericNutritionWeightAmountIn", @size = "15", @maxlength = "150" })%>
                            <i>(how long)</i> </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>
                            <input name="ResumptionOfCare_GenericNutrition" value="5" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("5")  ){ %>checked="checked"<% }%>'" />
                            Meals Prepared Appropriately </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="columns">
                        <li class="spacer">
                            <input name="ResumptionOfCare_GenericNutrition" value="6" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("6")  ){ %>checked="checked"<% }%>'" />
                            Diet </li>
                        <li>
                            <%=Html.Hidden("ResumptionOfCare_GenericNutritionDietAdequate", " ", new { @id = "" })%>
                            <%=Html.RadioButton("ResumptionOfCare_GenericNutritionDietAdequate", "Adequate", data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Adequate" ? true : false, new { @id = "" })%>
                            Adequate
                            <%=Html.RadioButton("ResumptionOfCare_GenericNutritionDietAdequate", "Inadequate", data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Inadequate" ? true : false, new { @id = "" })%>
                            Inadequate </li>
                        <li class="littleSpacer"></li>
                        <li>
                            <%string[] genericNutritionDiet = data.ContainsKey("GenericNutritionDiet") && data["GenericNutritionDiet"].Answer != "" ? data["GenericNutritionDiet"].Answer.Split(',') : null; %>
                            <input type="hidden" name="ResumptionOfCare_GenericNutritionDiet" name=" " />
                            <input name="ResumptionOfCare_GenericNutritionDiet" value="1" type="checkbox" '<% if( genericNutritionDiet!=null && genericNutritionDiet.Contains("1")  ){ %>checked="checked"<% }%>'" />
                            NG </li>
                        <li>
                            <input name="ResumptionOfCare_GenericNutritionDiet" value="2" type="checkbox" '<% if( genericNutritionDiet!=null && genericNutritionDiet.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            PEG </li>
                        <li>
                            <input name="ResumptionOfCare_GenericNutritionDiet" value="3" type="checkbox" '<% if( genericNutritionDiet!=null && genericNutritionDiet.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            Dobhoff </li>
                        <li>
                            <input name="ResumptionOfCare_GenericNutritionDiet" value="4" type="checkbox" '<% if( genericNutritionDiet!=null && genericNutritionDiet.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            Tube Placement Checked </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_GenericNutrition" value="7" type="checkbox" '<% if( genericNutrition!=null && genericNutrition.Contains("7")  ){ %>checked="checked"<% }%>'" />
                            Residual Checked, </li>
                        <li>Amount:
                            <%=Html.TextBox("ResumptionOfCare_GenericNutritionResidualCheckedAmount", data.ContainsKey("GenericNutritionResidualCheckedAmount") ? data["GenericNutritionResidualCheckedAmount"].Answer : "", new { @id = "ResumptionOfCare_GenericNutritionResidualCheckedAmount", @size = "5", @maxlength = "5" })%>
                            cc </li>
                    </ul>
                    <ul class="columns">
                        <li class="littleSpacer">&nbsp; </li>
                        <li>
                            <%string[] genericNutritionResidualProblem = data.ContainsKey("GenericNutritionResidualProblem") && data["GenericNutritionResidualProblem"].Answer != "" ? data["GenericNutritionResidualProblem"].Answer.Split(',') : null; %>
                            <input type="hidden" name="ResumptionOfCare_GenericNutritionResidualProblem" value=" " />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="1" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("1")  ){ %>checked="checked"<% }%>'" />
                            Throat problems?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="2" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            Hoarseness?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="3" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            Sore throat?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="4" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            Dental problems?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="5" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("5")  ){ %>checked="checked"<% }%>'" />
                            Dentures?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="6" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("6")  ){ %>checked="checked"<% }%>'" />
                            Problems chewing?
                            <br />
                            <input name="ResumptionOfCare_GenericNutritionResidualProblem" value="7" type="checkbox" '<% if( genericNutritionResidualProblem!=null && genericNutritionResidualProblem.Contains("7")  ){ %>checked="checked"<% }%>'" />
                            Other: &nbsp;
                            <%=Html.TextBox("ResumptionOfCare_GenericNutritionOtherDetails", data.ContainsKey("GenericNutritionOtherDetails") ? data["GenericNutritionOtherDetails"].Answer : "", new { @id = "ResumptionOfCare_GenericNutritionOtherDetails", @size = "15", @maxlength = "15" })%>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>Comments:<br />
                            <%=Html.TextArea("ResumptionOfCare_GenericNutritionComments", data.ContainsKey("GenericNutritionComments") ? data["GenericNutritionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericNutritionComments", @style = "width: 99%;" })%>
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br />
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] genericNutritionalHealth = data.ContainsKey("GenericNutritionalHealth") && data["GenericNutritionalHealth"].Answer != "" ? data["GenericNutritionalHealth"].Answer.Split(',') : null; %>
        <tr>
            <th width="60%">
                Nutritional Health Screen
            </th>
            <th width="5%">
                Yes
            </th>
            <th width="35%">
                Score
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="ResumptionOfCare_GenericNutritionalHealth" value=" " />
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="1" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Without reason, has lost more than 10 lbs, in the last 3 months </li>
                </ul>
            </td>
            <td>
                15
            </td>
            <td rowspan="12">
                <%string[] genericGoodNutritionScore = data.ContainsKey("GenericGoodNutritionScore") && data["GenericGoodNutritionScore"].Answer != "" ? data["GenericGoodNutritionScore"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="ResumptionOfCare_GenericGoodNutritionScore" value="" />
                        <input name="ResumptionOfCare_GenericGoodNutritionScore" value="1" type="checkbox" '<% if( genericGoodNutritionScore!=null && genericGoodNutritionScore.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Good Nutritional Status (Score 0 - 25) </li>
                </ul>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericGoodNutritionScore" value="2" type="checkbox" '<% if( genericGoodNutritionScore!=null && genericGoodNutritionScore.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Moderate Nutritional Risk (Score 25 - 55) </li>
                </ul>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericGoodNutritionScore" value="3" type="checkbox" '<% if( genericGoodNutritionScore!=null && genericGoodNutritionScore.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        High Nutritional Risk (Score 55 - 100)<br />
                        <br />
                    </li>
                </ul>
                <ul>
                    <li>Nutritional Status Comments:<br />
                        <%=Html.TextArea("ResumptionOfCare_GenericNutritionalStatusComments", data.ContainsKey("GenericNutritionalStatusComments") ? data["GenericNutritionalStatusComments"].Answer : "",10, 50, new { @id = "ResumptionOfCare_GenericNutritionalStatusComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
                <ul>
                    <li>
                        <br />
                        <br />
                        <%string[] genericNutritionDiffect = data.ContainsKey("GenericNutritionDiffect") && data["GenericNutritionDiffect"].Answer != "" ? data["GenericNutritionDiffect"].Answer.Split(',') : null; %>
                        <input type="hidden" name="ResumptionOfCare_GenericNutritionDiffect" value=" " />
                        <input name="ResumptionOfCare_GenericNutritionDiffect" value="1" type="checkbox" '<% if( genericNutritionDiffect!=null && genericNutritionDiffect.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Non-compliant with prescribed diet </li>
                </ul>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionDiffect" value="2" type="checkbox" '<% if( genericNutritionDiffect!=null && genericNutritionDiffect.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Over/under weight by 10%<br />
                        <br />
                    </li>
                </ul>
                <ul>
                    <li>Meals prepared by:<br />
                        <%=Html.TextArea("ResumptionOfCare_GenericMealsPreparedBy", data.ContainsKey("GenericMealsPreparedBy") ? data["GenericMealsPreparedBy"].Answer : "", 10, 50, new { @id = "ResumptionOfCare_GenericMealsPreparedBy", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="2" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Has an illness or condition that made pt change the type and/or amount of food eaten
                    </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="3" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Has open decubitus, ulcer, burn or wound </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="4" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Eats fewer than 2 meals a day </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="5" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Has a tooth/mouth problem that makes it hard to eat </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="6" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        Has 3 or more drinks of beer, liquor or wine almost every day </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="7" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        Does not always have enough money to buy foods needed </li>
                </ul>
            </td>
            <td>
                10
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="8" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        Eats few fruits or vegetables, or milk products </li>
                </ul>
            </td>
            <td>
                5
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="9" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Eats alone most of the time </li>
                </ul>
            </td>
            <td>
                5
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="10" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("10")  ){ %>checked="checked"<% }%>'" />
                        Takes 3 or more prescribed or OTC medications a day </li>
                </ul>
            </td>
            <td>
                5
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="11" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("11")  ){ %>checked="checked"<% }%>'" />
                        Is not always physically able to cook and/or feed self and has no caregiver to assist
                    </li>
                </ul>
            </td>
            <td>
                5
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericNutritionalHealth" value="12" type="checkbox" '<% if( genericNutritionalHealth!=null && genericNutritionalHealth.Contains("12")  ){ %>checked="checked"<% }%>'" />
                        Frequently has diarrhea or constipation </li>
                </ul>
            </td>
            <td>
                5
            </td>
        </tr>
    </table>
</div>
<br />
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <th colspan="2">
                    Enter Physician's Orders or Diet Requirements (locator #16)
                </th>
            </tr>
            <tr>
                <td>
                    <%string[] nutritionalReqs = data.ContainsKey("485NutritionalReqs") && data["485NutritionalReqs"].Answer != "" ? data["485NutritionalReqs"].Answer.Split(',') : null; %>
                    <ul class="columns">
                        <li>
                            <input type="hidden" name="ResumptionOfCare_485NutritionalReqs" value=" " />
                            <input name="ResumptionOfCare_485NutritionalReqs" value="1" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsSodiumAmount", data.ContainsKey("485NutritionalReqsSodiumAmount") ? data["485NutritionalReqsSodiumAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsSodiumAmount", @size = "5", @maxlength = "5" })%>
                        </li>
                        <li>Sodium</li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="2" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>No Added Salt</li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="3" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsCalorieADADietAmount", data.ContainsKey("485NutritionalReqsCalorieADADietAmount") ? data["485NutritionalReqsCalorieADADietAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsCalorieADADietAmount", @size = "5", @maxlength = "5" })%>
                        </li>
                        <li>Calorie ADA Diet </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="4" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Regular </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="5" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>High Protein </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsHighProteinAmount", data.ContainsKey("485NutritionalReqsHighProteinAmount") ? data["485NutritionalReqsHighProteinAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsHighProteinAmount", @size = "5", @maxlength = "5" })%>
                        </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="6" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Low Protein </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsLowProteinAmount", data.ContainsKey("485NutritionalReqsLowProteinAmount") ? data["485NutritionalReqsLowProteinAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsLowProteinAmount", @size = "5", @maxlength = "5" })%>
                        </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="7" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Carbohydrate </li>
                        <li>
                            <%=Html.Hidden("ResumptionOfCare_485NutritionalReqsCarbLevel", " ", new { @id = "" })%>
                            <%=Html.RadioButton("ResumptionOfCare_485NutritionalReqsCarbLevel", "1", data.ContainsKey("485NutritionalReqsCarbLevel") && data["485NutritionalReqsCarbLevel"].Answer == "1" ? true : false, new { @id = "" })%>
                            Low </li>
                        <li>
                            <%=Html.RadioButton("ResumptionOfCare_485NutritionalReqsCarbLevel", "2", data.ContainsKey("485NutritionalReqsCarbLevel") && data["485NutritionalReqsCarbLevel"].Answer == "1" ? true : false, new { @id = "" })%>
                            High </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="8" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Mechanical Soft </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="9" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>High Fiber </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="10" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("10")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Supplement: </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsSupplementType", data.ContainsKey("485NutritionalReqsSupplementType") ? data["485NutritionalReqsSupplementType"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsSupplementType", @size = "15", @maxlength = "15" })%>
                        </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="11" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("11")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Renal Diet </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="12" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("12")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Coumadin Diet </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="13" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("13")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Fluid Restriction </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsFluidResAmount", data.ContainsKey("485NutritionalReqsFluidResAmount") ? data["485NutritionalReqsFluidResAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsFluidResAmount", @size = "15", @maxlength = "15" })%>
                            cc/24 hours </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="14" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("14")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Other: </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsPhyDietOtherName", data.ContainsKey("485NutritionalReqsPhyDietOtherName") ? data["485NutritionalReqsPhyDietOtherName"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsPhyDietOtherName", @size = "15", @maxlength = "15" })%>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="15" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("15")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>No Concentrated Sweets </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="16" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("16")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Heart Healthy </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="17" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("17")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Low Cholesterol </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="18" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("18")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Low Fat </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="19" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("19")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>Enteral Nutrition </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralDesc", data.ContainsKey("485NutritionalReqsEnteralDesc") ? data["485NutritionalReqsEnteralDesc"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralDesc", @size = "10", @maxlength = "10" })%>
                            (formula) </li>
                    </ul>
                    <ul class="columns">
                        <li>&nbsp; </li>
                        <li>Amount </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralAmount", data.ContainsKey("485NutritionalReqsEnteralAmount") ? data["485NutritionalReqsEnteralAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralAmount", @size = "5", @maxlength = "5" })%>
                            cc/day via </li>
                    </ul>
                    <ul class="columns">
                        <%string[] nutritionalReqsEnteral = data.ContainsKey("485NutritionalReqsEnteral") && data["485NutritionalReqsEnteral"].Answer != "" ? data["485NutritionalReqsEnteral"].Answer.Split(',') : null; %>
                        <li>&nbsp; </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value=" " type="hidden" />
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="1" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralPumpType", data.ContainsKey("485NutritionalReqsEnteralPumpType") ? data["485NutritionalReqsEnteralPumpType"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralPumpType", @size = "15", @maxlength = "15" })%>
                            Pump </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="2" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            Gravity </li>
                    </ul>
                    <ul class="columns">
                        <li>&nbsp; </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="3" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            PEG </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="4" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            NG </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="5" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("5")  ){ %>checked="checked"<% }%>'" />
                            Dobhoff </li>
                    </ul>
                    <ul class="columns">
                        <li>&nbsp; </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="6" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("6")  ){ %>checked="checked"<% }%>'" />
                            Continuous </li>
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqsEnteral" value="7" type="checkbox" '<% if( nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("7")  ){ %>checked="checked"<% }%>'" />
                            Bolus </li>
                    </ul>
                    <ul class="columns">
                        <li>
                            <input name="ResumptionOfCare_485NutritionalReqs" value="20" type="checkbox" '<% if( nutritionalReqs!=null && nutritionalReqs.Contains("20")  ){ %>checked="checked"<% }%>'" />
                        </li>
                        <li>TPN </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsTPNAmount", data.ContainsKey("485NutritionalReqsTPNAmount") ? data["485NutritionalReqsTPNAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsTPNAmount", @size = "15", @maxlength = "15" })%>
                            @cc/hr </li>
                    </ul>
                    <ul class="columns">
                        <li>&nbsp; </li>
                        <li>via </li>
                        <li>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsTPNVia", data.ContainsKey("485NutritionalReqsTPNVia") ? data["485NutritionalReqsTPNVia"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsTPNVia", @size = "15", @maxlength = "15" })%>
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br />
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] nutritionInterventions = data.ContainsKey("485NutritionInterventions") && data["485NutritionInterventions"].Answer != "" ? data["485NutritionInterventions"].Answer.Split(',') : null; %>
        <tbody>
            <tr>
                <th colspan="2">
                    Interventions
                </th>
            </tr>
            <tr>
                <td width="15px">
                    <input type="hidden" name="ResumptionOfCare_485NutritionInterventions" value=" " />
                    <input name="ResumptionOfCare_485NutritionInterventions" value="1" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct
                    <%var instructOnDietPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructOnDietPerson") && data["485InstructOnDietPerson"].Answer != "" ? data["485InstructOnDietPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructOnDietPerson", instructOnDietPerson)%>
                    on
                    <%=Html.TextBox("ResumptionOfCare_485InstructOnDietDesc", data.ContainsKey("485InstructOnDietDesc") ? data["485InstructOnDietDesc"].Answer : "", new { @id = "ResumptionOfCare_485InstructOnDietDesc", @size = "100", @maxlength = "200" })%>
                    diet
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="2" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to assess patient for diet compliance
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="3" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructKeepDietLogPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructKeepDietLogPerson") && data["485InstructKeepDietLogPerson"].Answer != "" ? data["485InstructKeepDietLogPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructKeepDietLogPerson", instructKeepDietLogPerson)%>
                    to keep a diet log
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="4" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructPromoteOralIntakePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructPromoteOralIntakePerson") && data["485InstructPromoteOralIntakePerson"].Answer != "" ? data["485InstructPromoteOralIntakePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructPromoteOralIntakePerson", instructPromoteOralIntakePerson)%>
                    on methods to promote oral intake
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="5" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructParenteralNutritionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructParenteralNutritionPerson") && data["485InstructParenteralNutritionPerson"].Answer != "" ? data["485InstructParenteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructParenteralNutritionPerson", instructParenteralNutritionPerson)%>
                    on parenteral nutrition and the care/use of equipment, to include:
                    <%=Html.TextBox("ResumptionOfCare_485InstructParenteralNutritionInclude", data.ContainsKey("485InstructParenteralNutritionInclude") ? data["485InstructParenteralNutritionInclude"].Answer : "", new { @id = "ResumptionOfCare_485InstructParenteralNutritionInclude", @size = "40", @maxlength = "40" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="6" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructEnteralNutritionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructEnteralNutritionPerson") && data["485InstructEnteralNutritionPerson"].Answer != "" ? data["485InstructEnteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructEnteralNutritionPerson", instructEnteralNutritionPerson)%>
                    on enteral nutrition and the care/use of equipment, to include
                    <%=Html.TextBox("ResumptionOfCare_485InstructEnteralNutritionInclude", data.ContainsKey("485InstructEnteralNutritionInclude") ? data["485InstructEnteralNutritionInclude"].Answer : "", new { @id = "ResumptionOfCare_485InstructEnteralNutritionInclude", @size = "40", @maxlength = "40" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="7" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructCareOfTubePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructCareOfTubePerson") && data["485InstructCareOfTubePerson"].Answer != "" ? data["485InstructCareOfTubePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructCareOfTubePerson", instructCareOfTubePerson)%>
                    on proper care of
                    <%=Html.TextBox("ResumptionOfCare_485InstructCareOfTubeDesc", data.ContainsKey("485InstructCareOfTubeDesc") ? data["485InstructCareOfTubeDesc"].Answer : "", new { @id = "ResumptionOfCare_485InstructCareOfTubeDesc", @size = "20", @maxlength = "20" })%>
                    tube
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="8" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to change
                    <%=Html.TextBox("ResumptionOfCare_485ChangeTubeEveryType", data.ContainsKey("485ChangeTubeEveryType") ? data["485ChangeTubeEveryType"].Answer : "", new { @id = "ResumptionOfCare_485ChangeTubeEveryType", @size = "10", @maxlength = "10" })%>
                    tube every
                    <%=Html.TextBox("ResumptionOfCare_485ChangeTubeEveryFreq", data.ContainsKey("485ChangeTubeEveryFreq") ? data["485ChangeTubeEveryFreq"].Answer : "", new { @id = "ResumptionOfCare_485ChangeTubeEveryFreq", @size = "20", @maxlength = "10" })%>
                    beginning
                    <%=Html.TextBox("ResumptionOfCare_485ChangeTubeEveryDate", data.ContainsKey("485ChangeTubeEveryDate") ? data["485ChangeTubeEveryDate"].Answer : "", new { @id = "ResumptionOfCare_485ChangeTubeEveryDate", @size = "10", @maxlength = "20" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="9" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("9")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to irrigate
                    <%=Html.TextBox("ResumptionOfCare_485IrrigateTubeWithType", data.ContainsKey("485IrrigateTubeWithType") ? data["485IrrigateTubeWithType"].Answer : "", new { @id = "ResumptionOfCare_485IrrigateTubeWithType", @size = "10", @maxlength = "10" })%>
                    tube with
                    <%=Html.TextBox("ResumptionOfCare_485IrrigateTubeWithAmount", data.ContainsKey("485IrrigateTubeWithAmount") ? data["485IrrigateTubeWithAmount"].Answer : "", new { @id = "ResumptionOfCare_485IrrigateTubeWithAmount", @size = "10", @maxlength = "20" })%>
                    cc of
                    <%=Html.TextBox("ResumptionOfCare_485IrrigateTubeWithDesc", data.ContainsKey("485IrrigateTubeWithDesc") ? data["485IrrigateTubeWithDesc"].Answer : "", new { @id = "ResumptionOfCare_485IrrigateTubeWithDesc", @size = "10", @maxlength = "20" })%>
                    <%=Html.Hidden("ResumptionOfCare_485IrrigateTubeWithFreq", " ", new { @id = "" })%>
                    <%=Html.RadioButton("ResumptionOfCare_485IrrigateTubeWithFreq", "1", data.ContainsKey("485IrrigateTubeWithFreq") && data["485IrrigateTubeWithFreq"].Answer == "1" ? true : false, new { @id = "" })%>
                    every
                    <%=Html.TextBox("ResumptionOfCare_485IrrigateTubeWithEvery", data.ContainsKey("485IrrigateTubeWithEvery") ? data["485IrrigateTubeWithEvery"].Answer : "", new { @id = "ResumptionOfCare_485IrrigateTubeWithEvery", @size = "10", @maxlength = "20" })%>
                    <%=Html.RadioButton("ResumptionOfCare_485IrrigateTubeWithFreq", "0", data.ContainsKey("485IrrigateTubeWithFreq") && data["485IrrigateTubeWithFreq"].Answer == "0" ? true : false, new { @id = "" })%>
                    as needed for
                    <%=Html.TextBox("ResumptionOfCare_485IrrigateTubeWithAsNeeded", data.ContainsKey("485IrrigateTubeWithAsNeeded") ? data["485IrrigateTubeWithAsNeeded"].Answer : "", new { @id = "ResumptionOfCare_485IrrigateTubeWithAsNeeded", @size = "10", @maxlength = "20" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionInterventions" value="10" type="checkbox" '<% if( nutritionInterventions!=null && nutritionInterventions.Contains("10")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    SN to instruct the
                    <%var instructFreeWaterPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructFreeWaterPerson") && data["485InstructFreeWaterPerson"].Answer != "" ? data["485InstructFreeWaterPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructFreeWaterPerson", instructFreeWaterPerson)%>
                    to give
                    <%=Html.TextBox("ResumptionOfCare_485InstructFreeWaterAmount", data.ContainsKey("485InstructFreeWaterAmount") ? data["485InstructFreeWaterAmount"].Answer : "", new { @id = "ResumptionOfCare_485InstructFreeWaterAmount", @size = "10", @maxlength = "10" })%>
                    cc of free water every
                    <%=Html.TextBox("ResumptionOfCare_485InstructFreeWaterEvery", data.ContainsKey("485InstructFreeWaterEvery") ? data["485InstructFreeWaterEvery"].Answer : "", new { @id = "ResumptionOfCare_485InstructFreeWaterEvery", @size = "10", @maxlength = "10" })%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Additional Orders: &nbsp;
                    <%var nutritionOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485NutritionOrderTemplates") && data["485NutritionOrderTemplates"].Answer != "" ? data["485NutritionOrderTemplates"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_485NutritionOrderTemplates", nutritionOrderTemplates)%>
                    <br />
                    <%=Html.TextArea("ResumptionOfCare_485NutritionComments", data.ContainsKey("485NutritionComments") ? data["485NutritionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485NutritionComments", @style = "width: 99%;" })%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br />
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] nutritionGoals = data.ContainsKey("485NutritionGoals") && data["485NutritionGoals"].Answer != "" ? data["485NutritionGoals"].Answer.Split(',') : null; %>
        <tbody>
            <tr>
                <th colspan="2">
                    Goals
                </th>
            </tr>
            <tr>
                <td width="15px">
                    <input type="hidden" name="ResumptionOfCare_485NutritionGoals" value=" " />
                    <input name="ResumptionOfCare_485NutritionGoals" value="1" type="checkbox" '<% if( nutritionGoals!=null && nutritionGoals.Contains("1")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    Patient will maintain
                    <%=Html.TextBox("ResumptionOfCare_485MaintainDietComplianceType", data.ContainsKey("485MaintainDietComplianceType") ? data["485MaintainDietComplianceType"].Answer : "", new { @id = "ResumptionOfCare_485MaintainDietComplianceType", @size = "100", @maxlength = "200" })%>
                    diet compliance during the episode
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionGoals" value="2" type="checkbox" '<% if( nutritionGoals!=null && nutritionGoals.Contains("2")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    The
                    <%var demonstrateDietCompliancePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateDietCompliancePerson") && data["485DemonstrateDietCompliancePerson"].Answer != "" ? data["485DemonstrateDietCompliancePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DemonstrateDietCompliancePerson", demonstrateDietCompliancePerson)%>
                    will demonstrate compliance with maintaining a diet log during the episode
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionGoals" value="3" type="checkbox" '<% if( nutritionGoals!=null && nutritionGoals.Contains("3")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    The
                    <%var demonstrateEnteralNutritionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateEnteralNutritionPerson") && data["485DemonstrateEnteralNutritionPerson"].Answer != "" ? data["485DemonstrateEnteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DemonstrateEnteralNutritionPerson", demonstrateEnteralNutritionPerson)%>
                    will demonstrate proper care/use of enteral nutrition equipment by
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateEnteralNutritionDate", data.ContainsKey("485DemonstrateEnteralNutritionDate") ? data["485DemonstrateEnteralNutritionDate"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateEnteralNutritionDate", @size = "10", @maxlength = "20" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionGoals" value="4" type="checkbox" '<% if( nutritionGoals!=null && nutritionGoals.Contains("4")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    The
                    <%var demonstrateParenteralNutritionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateParenteralNutritionPerson") && data["485DemonstrateParenteralNutritionPerson"].Answer != "" ? data["485DemonstrateParenteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DemonstrateParenteralNutritionPerson", demonstrateParenteralNutritionPerson)%>
                    will demonstrate proper care/use of parenteral nutrition equipment by
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateParenteralNutritionDate", data.ContainsKey("485DemonstrateParenteralNutritionDate") ? data["485DemonstrateParenteralNutritionDate"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateParenteralNutritionDate", @size = "10", @maxlength = "20" })%>
                </td>
            </tr>
            <tr>
                <td width="15px">
                    <input name="ResumptionOfCare_485NutritionGoals" value="5" type="checkbox" '<% if( nutritionGoals!=null && nutritionGoals.Contains("5")){ %>checked="checked"<% }%>'" />
                </td>
                <td>
                    The
                    <%var demonstrateCareOfTubePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateCareOfTubePerson") && data["485DemonstrateCareOfTubePerson"].Answer != "" ? data["485DemonstrateCareOfTubePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DemonstrateCareOfTubePerson", demonstrateCareOfTubePerson)%>
                    will demonstrate proper care of
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateCareOfTubeType", data.ContainsKey("485DemonstrateCareOfTubeType") ? data["485DemonstrateCareOfTubeType"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateCareOfTubeType", @size = "10", @maxlength = "10" })%>
                    tube by
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateCareOfTubeDate", data.ContainsKey("485DemonstrateCareOfTubeDate") ? data["485DemonstrateCareOfTubeDate"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateCareOfTubeDate", @size = "10", @maxlength = "20" })%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Additional Goals: &nbsp;
                    <%var nutritionGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485NutritionGoalTemplates") && data["485NutritionGoalTemplates"].Answer != "" ? data["485NutritionGoalTemplates"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_485NutritionGoalTemplates", nutritionGoalTemplates)%>
                    <br />
                    <%=Html.TextArea("ResumptionOfCare_485NutritionGoalComments", data.ContainsKey("485NutritionGoalComments") ? data["485NutritionGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485NutritionGoalComments", @style = "width: 99%;" })%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="ROC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="ROC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
