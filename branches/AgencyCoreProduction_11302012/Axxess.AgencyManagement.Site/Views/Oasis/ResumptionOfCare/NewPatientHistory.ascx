﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCarePatientHistoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Allergies(locator #17)
            </th>
        </tr>
        <tr>
            <td>
                <%=Html.Hidden("ResumptionOfCare_485Allergies", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_485Allergies", "No", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? true : false, new { @id = "" })%>
                NKA (Food/Drugs/Latex)
            </td>
        </tr>
        <tr>
            <td>
                <%=Html.RadioButton("ResumptionOfCare_485Allergies", "Yes", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Allergic to:<br />
                <%=Html.TextArea("ResumptionOfCare_485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "", 4, 5, new { @id = "", @style = "width: 90%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="7">
                Vital Signs
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="littleSpacer">Pulse: </li>
                    <li class="littleSpacer">Apical: </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericPulseApical", data.ContainsKey("GenericPulseApical") ? data["GenericPulseApical"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseApical",@size="7",@maxlength="7" })%>
                    </li>
                    <li>
                        <%=Html.Hidden("ResumptionOfCare_GenericPulseApicalRegular", " ", new { @id = "" })%>
                        <%=Html.RadioButton("ResumptionOfCare_GenericPulseApicalRegular", "1", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "1" ? true : false, new { @id = "" })%>
                        (Reg) </li>
                    <li>
                        <%=Html.RadioButton("ResumptionOfCare_GenericPulseApicalRegular", "2", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "2" ? true : false, new { @id = "" })%>
                        (Irreg) </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="littleSpacer">Radial: </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericPulseRadial", data.ContainsKey("GenericPulseRadial") ? data["GenericPulseRadial"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseRadial", @size = "7", @maxlength = "7" })%>
                    </li>
                    <li>
                        <%=Html.Hidden("ResumptionOfCare_GenericPulseRadialRegular", " ", new { @id = "" })%>
                        <%=Html.RadioButton("ResumptionOfCare_GenericPulseRadialRegular", "1", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "1" ? true : false, new { @id = "" })%>
                        (Reg) </li>
                    <li>
                        <%=Html.RadioButton("ResumptionOfCare_GenericPulseRadialRegular", "2", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "2" ? true : false, new { @id = "" })%>
                        (Irreg) </li>
                </ul>
            </td>
            <td>
                <ul class="columns">
                    <li class="littleSpacer">Height: </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericHeight", data.ContainsKey("GenericHeight") ? data["GenericHeight"].Answer : "", new { @id = "ResumptionOfCare_GenericHeight", @size = "7", @maxlength = "7" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">Weight: </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericWeight", data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer : "", new { @id = "ResumptionOfCare_GenericWeight", @size = "7", @maxlength = "7" })%>
                    </li>
                </ul>
            </td>
            <td rowspan="2">
                <ul class="columns">
                    <li class="littleSpacer">BP </li>
                    <li class="littleSpacer">Lying </li>
                    <li class="littleSpacer">Sitting </li>
                    <li class="littleSpacer">Standing </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">Left </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPLeftLying", data.ContainsKey("GenericBPLeftLying") ? data["GenericBPLeftLying"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftLying", @size = "8", @maxlength = "8" })%>
                    </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPLeftSitting", data.ContainsKey("GenericBPLeftSitting") ? data["GenericBPLeftSitting"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftSitting", @size = "8", @maxlength = "8" })%>
                    </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPLeftStanding", data.ContainsKey("GenericBPLeftStanding") ? data["GenericBPLeftStanding"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftStanding", @size = "8", @maxlength = "8" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">Right</li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPRightLying", data.ContainsKey("GenericBPRightLying") ? data["GenericBPRightLying"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightLying", @size = "8", @maxlength = "8" })%>
                    </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPRightSitting", data.ContainsKey("GenericBPRightSitting") ? data["GenericBPRightSitting"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightSitting", @size = "8", @maxlength = "8" })%>
                    </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericBPRightStanding", data.ContainsKey("GenericBPRightStanding") ? data["GenericBPRightStanding"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightStanding", @size = "8", @maxlength = "8" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="littleSpacer">Temp: </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("ResumptionOfCare_GenericTemp", data.ContainsKey("GenericTemp") ? data["GenericTemp"].Answer : "", new { @id = "ResumptionOfCare_GenericTemp", @size = "8", @maxlength = "8" })%>
                    </li>
                    <li class="littleSpacer">Resp: </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericResp", data.ContainsKey("GenericResp") ? data["GenericResp"].Answer : "", new { @id = "ResumptionOfCare_GenericResp", @size = "8", @maxlength = "8" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul class="columns">
                    <li class="littleSpacer">
                        <%=Html.Hidden("ResumptionOfCare_GenericVitalSignsActualStated", " ", new { @id = "" })%>
                        <%=Html.RadioButton("ResumptionOfCare_GenericVitalSignsActualStated", "1", data.ContainsKey("GenericVitalSignsActualStated") && data["GenericVitalSignsActualStated"].Answer == "1" ? true : false, new { @id = "" })%>
                        Actual </li>
                    <li>
                        <%=Html.RadioButton("ResumptionOfCare_GenericVitalSignsActualStated", "2", data.ContainsKey("GenericVitalSignsActualStated") && data["GenericVitalSignsActualStated"].Answer == "2" ? true : false, new { @id = "" })%>
                        Stated </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th>
                Notify Physician of:
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Temperature greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericTempGreaterThan", data.ContainsKey("GenericTempGreaterThan") ? data["GenericTempGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericTempGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericTempLessThan", data.ContainsKey("GenericTempLessThan") ? data["GenericTempLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericTempLessThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Pulse greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericPulseGreaterThan", data.ContainsKey("GenericPulseGreaterThan") ? data["GenericPulseGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericPulseLessThan", data.ContainsKey("GenericPulseLessThan") ? data["GenericPulseLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseLessThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Respirations greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericRespirationGreaterThan", data.ContainsKey("GenericRespirationGreaterThan") ? data["GenericRespirationGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericRespirationLessThan", data.ContainsKey("GenericRespirationLessThan") ? data["GenericRespirationLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationLessThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Systolic BP greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericSystolicBPGreaterThan", data.ContainsKey("GenericSystolicBPGreaterThan") ? data["GenericSystolicBPGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericSystolicBPGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericSystolicBPLessThan", data.ContainsKey("GenericSystolicBPLessThan") ? data["GenericSystolicBPLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Diastolic BP greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericDiastolicBPGreaterThan", data.ContainsKey("GenericDiastolicBPGreaterThan") ? data["GenericDiastolicBPGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericDiastolicBPGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericDiastolicBPLessThan", data.ContainsKey("GenericDiastolicBPLessThan") ? data["GenericDiastolicBPLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericDiastolicBPLessThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>O2 Sat less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_Generic02SatLessThan", data.ContainsKey("Generic02SatLessThan") ? data["Generic02SatLessThan"].Answer : "", new { @id = "ResumptionOfCare_Generic02SatLessThan", @size = "5", @maxlength = "5" })%>
                        % </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Fasting blood sugar greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericFastingBloodSugarGreaterThan", data.ContainsKey("GenericFastingBloodSugarGreaterThan") ? data["GenericFastingBloodSugarGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericFastingBloodSugarGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericFastingBloodSugarLessThan", data.ContainsKey("GenericFastingBloodSugarLessThan") ? data["GenericFastingBloodSugarLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericFastingBloodSugarLessThan", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Random blood sugar greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericRandomBloddSugarGreaterThan", data.ContainsKey("GenericRandomBloddSugarGreaterThan") ? data["GenericRandomBloddSugarGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRandomBloddSugarGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>or less than (<) </li>
                    <%=Html.TextBox("ResumptionOfCare_GenericFastingBloodSugarLessThan", data.ContainsKey("GenericFastingBloodSugarLessThan") ? data["GenericFastingBloodSugarLessThan"].Answer : "", new { @id = "GenericFastingBloodSugarLessThan", @size = "5", @maxlength = "5" })%>
                    <li></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Weight greater than (>) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericWeightGreaterThan", data.ContainsKey("GenericWeightGreaterThan") ? data["GenericWeightGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericWeightGreaterThan", @size = "5", @maxlength = "5" })%>
                    </li>
                    <li>lbs or less than (<) </li>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericWeightLessThan", data.ContainsKey("GenericWeightLessThan") ? data["GenericWeightLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericWeightLessThan", @size = "5", @maxlength = "5" })%>
                        lbs </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull title">
        <div class="padding">
            (M1000) From which of the following Inpatient Facilities was the patient discharged
            during the past 14 days? (Mark all that apply.)
        </div>
    </div>
    <div class="insideCol">
        <div class="padding">
            <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesLTC" value="" />
            <input name="ResumptionOfCare_M1000InpatientFacilitiesLTC" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesLTC") && data["M1000InpatientFacilitiesLTC"].Answer == "1" ){ %>checked="checked"<% }%>'" />1
            - Long-term nursing facility (NF)<br />
            <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesSNF" value="" />
            <input name="ResumptionOfCare_M1000InpatientFacilitiesSNF" value="1" type="checkbox"   '<% if( data.ContainsKey("M1000InpatientFacilitiesSNF") && data["M1000InpatientFacilitiesSNF"].Answer == "1" ){ %>checked="checked"<% }%>'" />2
            - Skilled nursing facility (SNF / TCU)<br />
            <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesIPPS" value="" />
            <input name="ResumptionOfCare_M1000InpatientFacilitiesIPPS" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesIPPS") && data["M1000InpatientFacilitiesIPPS"].Answer == "1" ){ %>checked="checked"<% }%>'" />3
            - Short-stay acute hospital (IPPS)<br />
            <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesLTCH" value="" />
            <input name="ResumptionOfCare_M1000InpatientFacilitiesLTCH" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesLTCH") && data["M1000InpatientFacilitiesLTCH"].Answer == "1" ){ %>checked="checked"<% }%>'" />4
            - Long-term care hospital (LTCH)<br />
        </div>
    </div>
    <div class="insideCol">
        <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesIRF" value="" />
        <input name="ResumptionOfCare_M1000InpatientFacilitiesIRF" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesIRF") && data["M1000InpatientFacilitiesIRF"].Answer == "1" ){ %>checked="checked"<% }%>'" />5
        - Inpatient rehabilitation hospital or unit (IRF)<br />
        <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesPhych" value="" />
        <input name="ResumptionOfCare_M1000InpatientFacilitiesPhych" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesPhych") && data["M1000InpatientFacilitiesPhych"].Answer == "1" ){ %>checked="checked"<% }%>'" />6
        - Psychiatric hospital or unit<br />
        <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesOTHR" value="" />
        <input name="ResumptionOfCare_M1000InpatientFacilitiesOTHR" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesOTHR") && data["M1000InpatientFacilitiesOTHR"].Answer == "1" ){ %>checked="checked"<% }%>'" />7
        - Other (specify)
        <%=Html.TextBox("ResumptionOfCare_M1000InpatientFacilitiesOther", data.ContainsKey("M1000InpatientFacilitiesOther") ? data["M1000InpatientFacilitiesOther"].Answer : "", new { @id = "ResumptionOfCare_M1000InpatientFacilitiesOther", @size = "5", @maxlength = "5" })%>
        <br />
        <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesNone" value="" />
        <input name="ResumptionOfCare_M1000InpatientFacilitiesNone" value="1" type="checkbox"  '<% if( data.ContainsKey("M1000InpatientFacilitiesOther") && data["M1000InpatientFacilitiesOther"].Answer == "1" ){ %>checked="checked"<% }%>'" />NA
        - Patient was not discharged from an inpatient facility [Go to M1016 ]<br />
    </div>
</div>
<div class="rowOasis" id="roc_M1005">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1005) Inpatient Discharge Date (most recent):</div>
        </div>
        <div class="padding">
            <%=Html.TextBox("ResumptionOfCare_M1005InpatientDischargeDate", data.ContainsKey("M1005InpatientDischargeDate") ? data["M1005InpatientDischargeDate"].Answer : "", new { @id = "ResumptionOfCare_M1005InpatientDischargeDate", @size = "5", @maxlength = "5" })%>
            <br />
            <input type="hidden" name="ResumptionOfCare_M1005InpatientDischargeDateUnknown" value=" " />
            <input id="ResumptionOfCare_M1005InpatientDischargeDateUnknown" name="ResumptionOfCare_M1005InpatientDischargeDateUnknown"
                type="checkbox" value="1" '<% if( data.ContainsKey("M1005InpatientDischargeDateUnknown") && data["M1005InpatientDischargeDateUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />UK
            - Unknown</div>
    </div>
</div>
<div class="rowOasis" id="roc_M1010">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1010) List each Inpatient Diagnosis and ICD-9-C M code at the level of highest
                specificity for only those conditions treated during an inpatient stay within the
                last 14 days (no E-codes, or V-codes):</div>
        </div>
        <div class="padding">
            <div class="row485">
                <table cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>
                                <u>Inpatient Facility Diagnosis(locator #11)</u>
                            </th>
                            <th>
                                <u>ICD-9-C M Code</u>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="ICDText">
                                a.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis1", data.ContainsKey("M1010InpatientFacilityDiagnosis1") ? data["M1010InpatientFacilityDiagnosis1"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode1", data.ContainsKey("M1010InpatientFacilityDiagnosisCode1") ? data["M1010InpatientFacilityDiagnosisCode1"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                b.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis2", data.ContainsKey("M1010InpatientFacilityDiagnosis2") ? data["M1010InpatientFacilityDiagnosis2"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode2", data.ContainsKey("M1010InpatientFacilityDiagnosisCode2") ? data["M1010InpatientFacilityDiagnosisCode2"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                c.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis3", data.ContainsKey("M1010InpatientFacilityDiagnosis3") ? data["M1010InpatientFacilityDiagnosis3"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode3", data.ContainsKey("M1010InpatientFacilityDiagnosisCode3") ? data["M1010InpatientFacilityDiagnosisCode3"].Answer : "", new { @class = "ICD" })%><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                d.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis4", data.ContainsKey("M1010InpatientFacilityDiagnosis4") ? data["M1010InpatientFacilityDiagnosis4"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode4", data.ContainsKey("M1010InpatientFacilityDiagnosisCode4") ? data["M1010InpatientFacilityDiagnosisCode4"].Answer : "", new { @class = "ICD" })%><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                e.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis5", data.ContainsKey("M1010InpatientFacilityDiagnosis5") ? data["M1010InpatientFacilityDiagnosis5"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode5", data.ContainsKey("M1010InpatientFacilityDiagnosisCode5") ? data["M1010InpatientFacilityDiagnosisCode5"].Answer : "", new { @class = "ICD" })%><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                f.<%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis6", data.ContainsKey("M1010InpatientFacilityDiagnosis6") ? data["M1010InpatientFacilityDiagnosis6"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode6", data.ContainsKey("M1010InpatientFacilityDiagnosisCode6") ? data["M1010InpatientFacilityDiagnosisCode6"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="rowOasis" id="roc_M1012">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1012) List each Inpatient Procedure and the associated ICD-9-C M procedure code
                relevant to the plan of care.</div>
        </div>
        <div class="padding">
            <div class="row485">
                <table cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>
                                <u>Inpatient Procedure (locator #12)</u>
                            </th>
                            <th>
                                <u>Procedure Code</u>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="ICDText">
                                a.<%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure1", data.ContainsKey("M1012InpatientFacilityProcedure1") ? data["M1012InpatientFacilityProcedure1"].Answer : "", new { @class = "procedureDiagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode1", data.ContainsKey("M1012InpatientFacilityProcedureCode1") ? data["M1012InpatientFacilityProcedureCode1"].Answer : "", new { @class = "procedureICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                b.<%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure2", data.ContainsKey("M1012InpatientFacilityProcedure2") ? data["M1012InpatientFacilityProcedure2"].Answer : "", new { @class = "procedureDiagnosis" })%><br />
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode2", data.ContainsKey("M1012InpatientFacilityProcedureCode2") ? data["M1012InpatientFacilityProcedureCode2"].Answer : "", new { @class = "procedureICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                c.<%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure3", data.ContainsKey("M1012InpatientFacilityProcedure3") ? data["M1012InpatientFacilityProcedure3"].Answer : "", new { @class = "procedureDiagnosis" })%><br />
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode3", data.ContainsKey("M1012InpatientFacilityProcedureCode3") ? data["M1012InpatientFacilityProcedureCode3"].Answer : "", new { @class = "procedureICD" })%><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                d.<%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure4", data.ContainsKey("M1012InpatientFacilityProcedure4") ? data["M1012InpatientFacilityProcedure4"].Answer : "", new { @class = "procedureDiagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode4", data.ContainsKey("M1012InpatientFacilityProcedureCode4") ? data["M1012InpatientFacilityProcedureCode4"].Answer : "", new { @class = "procedureICD" })%>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable"
                                    value="" />
                                <input id="ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable"
                                    type="checkbox" value="1"  '<% if( data.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") && data["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer == "1" ){ %>checked="checked"<% }%>'" />NA
                                - Not applicable
                            </td>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown"
                                    value="" />
                                <input id="ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown"
                                    type="checkbox" value="1"  '<% if( data.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && data["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />UK
                                - Unknown
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1016) Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days:
                List the patient's Medical Diagnoses and ICD-9-C M codes at the level of highest
                specificity for those conditions requiring changed medical or treatment regimen
                within the past 14 days (no surgical, E-codes, or V-codes):<br />
            </div>
        </div>
        <div class="padding">
            <div class="row485">
                <table cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>
                                <u>Changed Medical Regimen Diagnosis(locator #17)</u>
                            </th>
                            <th>
                                <u>ICD-9-C M Code</u>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="ICDText">
                                a.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis1", data.ContainsKey("M1016MedicalRegimenDiagnosis1") ? data["M1016MedicalRegimenDiagnosis1"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode1", data.ContainsKey("M1016MedicalRegimenDiagnosisCode1") ? data["M1016MedicalRegimenDiagnosisCode1"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                b.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis2", data.ContainsKey("M1016MedicalRegimenDiagnosis2") ? data["M1016MedicalRegimenDiagnosis2"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode2", data.ContainsKey("M1016MedicalRegimenDiagnosisCode2") ? data["M1016MedicalRegimenDiagnosisCode2"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                c.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis3", data.ContainsKey("M1016MedicalRegimenDiagnosis3") ? data["M1016MedicalRegimenDiagnosis3"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode3", data.ContainsKey("M1016MedicalRegimenDiagnosisCode3") ? data["M1016MedicalRegimenDiagnosisCode3"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                d.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis4", data.ContainsKey("M1016MedicalRegimenDiagnosis4") ? data["M1016MedicalRegimenDiagnosis4"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode4", data.ContainsKey("M1016MedicalRegimenDiagnosisCode4") ? data["M1016MedicalRegimenDiagnosisCode4"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                e.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis5", data.ContainsKey("M1016MedicalRegimenDiagnosis5") ? data["M1016MedicalRegimenDiagnosis5"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode5", data.ContainsKey("M1016MedicalRegimenDiagnosisCode5") ? data["M1016MedicalRegimenDiagnosisCode5"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ICDText">
                                f.<%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis6", data.ContainsKey("M1016MedicalRegimenDiagnosis6") ? data["M1016MedicalRegimenDiagnosis6"].Answer : "", new { @class = "diagnosis" })%>
                            </td>
                            <td align="right" class="ICDCode">
                                <%=Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode6", data.ContainsKey("M1016MedicalRegimenDiagnosisCode6") ? data["M1016MedicalRegimenDiagnosisCode6"].Answer : "", new { @class = "ICD" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="insideColFull">
            <div class="padding">
                <input type="hidden" name="ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable"
                    value="" />
                <input id="ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable" name="ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable"
                    type="checkbox" value="1"  '<% if( data.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && data["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1" ){ %>checked="checked"<% }%>'" />NA
                - Not applicable (no medical or treatment regimen changes within the past 14 days)</div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1018) Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay
                Within Past 14 Days: If this patient experienced an inpatient facility discharge
                or change in medical or treatment regimen within the past 14 days, indicate any
                conditions which existed prior to the inpatient stay or change in medical or treatment
                regimen. (Mark all that apply.)</div>
        </div>
        <div class="insideColFull">
            <div class="insideColFull">
                <div class="padding ">
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI" value="1" type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI") && data["M1018ConditionsPriorToMedicalRegimenUI"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    1 - Urinary incontinence<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH" value="1"
                        type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH") && data["M1018ConditionsPriorToMedicalRegimenCATH"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    2 - Indwelling/suprapubic catheter<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain" value="1"
                        type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain") && data["M1018ConditionsPriorToMedicalRegimenPain"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    3 - Intractable pain<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN" value="1"
                        type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN") && data["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    4 - Impaired decision-making<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive" value="1"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive") && data["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer == "1" ){ %>checked="checked"<% }%>'"
                        type="checkbox" />
                    5 - Disruptive or socially inappropriate behavior<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss" value="1"
                        type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss") && data["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    6 - Memory loss to the extent that supervision required<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone" value="1"
                        type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone") && data["M1018ConditionsPriorToMedicalRegimenNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    7 - None of the above<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA" value="1" type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA") && data["M1018ConditionsPriorToMedicalRegimenNA"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    NA - No inpatient facility discharge and no change in medical or treatment regimen
                    in past 14 days<br />
                    <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK"
                        value="" />
                    <input name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK" value="1" type="checkbox"  '<% if( data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK") && data["M1018ConditionsPriorToMedicalRegimenUK"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                    UK - Unknown<br />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Past Medical History <i>(Mark all that apply)</i>
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="littleSpacer">
                        <input type="hidden" name="ResumptionOfCare_GenericMedicalHistory" />
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="1" type="checkbox"  '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                        CHF </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="2" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "2" ){ %>checked="checked"<% }%>'" />
                        Cardiomyopathy </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="3" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "3" ){ %>checked="checked"<% }%>'" />
                        Arrhythmia </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="4" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "4" ){ %>checked="checked"<% }%>'" />
                        Chest Pain </li>
                    <li class="littleSpacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="5" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "5" ){ %>checked="checked"<% }%>'" />
                        MI </li>
                    <li class="littleSpacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="6" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "6" ){ %>checked="checked"<% }%>'" />
                        CAD </li>
                    <li class="littleSpacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="7" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "7" ){ %>checked="checked"<% }%>'" />
                        HTN </li>
                    <li class="littleSpacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="8" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "8" ){ %>checked="checked"<% }%>'" />
                        PVD </li>
                    <li class="littleSpacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="9" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "9" ){ %>checked="checked"<% }%>'" />
                        Murmur </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="10" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "10" ){ %>checked="checked"<% }%>'" />
                        Cancer </li>
                    <li><i>(specify type)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryCancerType", data.ContainsKey("GenericMedicalHistoryCancerType") ? data["GenericMedicalHistoryCancerType"].Answer : "", new { @size="40",@maxlength="40" })%>
                        In remission?
                        <%=Html.Hidden("ResumptionOfCare_GenericMedicalHistoryCancerRemission", "")%>
                        <input type="hidden" name="ResumptionOfCare_GenericMedicalHistoryCancerRemission"
                            value=" " />
                        <%=Html.RadioButton("ResumptionOfCare_GenericMedicalHistoryCancerRemission", "1", data.ContainsKey("GenericMedicalHistoryCancerRemission") && data["GenericMedicalHistoryCancerRemission"].Answer == "1" ? true : false, new { @id = "" })%>
                        Yes
                        <%=Html.RadioButton("ResumptionOfCare_GenericMedicalHistoryCancerRemission", "0", data.ContainsKey("GenericMedicalHistoryCancerRemission") && data["GenericMedicalHistoryCancerRemission"].Answer == "0" ? true : false, new { @id = "" })%>
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="11" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "11" ){ %>checked="checked"<% }%>'" />
                        Osteoarthritis/DJD </li>
                    <li><i>(specify sites affected)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryOsteoarthritisSites", data.ContainsKey("GenericMedicalHistoryOsteoarthritisSites") ? data["GenericMedicalHistoryOsteoarthritisSites"].Answer : "", new { @size = "60", @maxlength = "60" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="12" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "12" ){ %>checked="checked"<% }%>'" />
                        Rheumatoid Arthritis </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="13" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "13" ){ %>checked="checked"<% }%>'" />
                        Gait Problems </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="14" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "14" ){ %>checked="checked"<% }%>'" />
                        Fractures </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="15" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "15" ){ %>checked="checked"<% }%>'" />
                        Falls </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="16" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "16" ){ %>checked="checked"<% }%>'" />
                        Joint Replacement </li>
                    <li><i>(specify joint)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryJointReplacementLocation", data.ContainsKey("GenericMedicalHistoryJointReplacementLocation") ? data["GenericMedicalHistoryJointReplacementLocation"].Answer : "", new { @size = "50", @maxlength = "50" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="17" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "17" ){ %>checked="checked"<% }%>'" />
                        CVA </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="18" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "18" ){ %>checked="checked"<% }%>'" />
                        TIA </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="19" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "19" ){ %>checked="checked"<% }%>'" />
                        MS </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="20" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "20" ){ %>checked="checked"<% }%>'" />
                        Hemiplegia </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="21" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "21" ){ %>checked="checked"<% }%>'" />
                        Seizures </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="22" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "22" ){ %>checked="checked"<% }%>'" />
                        Headaches </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="23" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "23" ){ %>checked="checked"<% }%>'" />
                        Dizziness/Vertigo </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="24" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "24" ){ %>checked="checked"<% }%>'" />
                        IBS </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="25" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "25" ){ %>checked="checked"<% }%>'" />
                        Crohn's Disease </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="26" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "26" ){ %>checked="checked"<% }%>'" />
                        Diverticulitis/Diverticulosis </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="27" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "27" ){ %>checked="checked"<% }%>'" />
                        Constipation </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="28" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "28" ){ %>checked="checked"<% }%>'" />
                        Diarrhea </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="29" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "29" ){ %>checked="checked"<% }%>'" />
                        Fecal Incontinence </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="30" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "30" ){ %>checked="checked"<% }%>'" />
                        Liver/Gallbladder Problems </li>
                    <li><i>(specify)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryLiverGallBladderDesc", data.ContainsKey("GenericMedicalHistoryLiverGallBladderDesc") ? data["GenericMedicalHistoryLiverGallBladderDesc"].Answer : "", new { @size = "60", @maxlength = "60" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="31" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "31" ){ %>checked="checked"<% }%>'" />
                        Depression </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="32" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "32" ){ %>checked="checked"<% }%>'" />
                        Anxiety </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="33" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "33" ){ %>checked="checked"<% }%>'" />
                        Dementia </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="34" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "34" ){ %>checked="checked"<% }%>'" />
                        Alzheimer's </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="35" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "35" ){ %>checked="checked"<% }%>'" />
                        Substance Abuse </li>
                    <li><i>(specify)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistorySubstanceAbuseDesc", data.ContainsKey("GenericMedicalHistorySubstanceAbuseDesc") ? data["GenericMedicalHistorySubstanceAbuseDesc"].Answer : "", new { @size = "80", @maxlength = "80" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="36" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "36" ){ %>checked="checked"<% }%>'" />
                        Mental Disorder </li>
                    <li><i>(specify)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryMentalDisorderDesc", data.ContainsKey("GenericMedicalHistoryMentalDisorderDesc") ? data["GenericMedicalHistoryMentalDisorderDesc"].Answer : "", new { @size = "80", @maxlength = "80" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="37" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "37" ){ %>checked="checked"<% }%>'" />
                        Pressure Ulcer </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="38" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "38" ){ %>checked="checked"<% }%>'" />
                        Stasis Ulcer </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="39" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "39" ){ %>checked="checked"<% }%>'" />
                        Diabetic Ulcer </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="40" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "40" ){ %>checked="checked"<% }%>'" />
                        Trauma Wound </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="41" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "41" ){ %>checked="checked"<% }%>'" />
                        Other </li>
                    <li><i>(specify)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryOtherDetails", data.ContainsKey("GenericMedicalHistoryOtherDetails") ? data["GenericMedicalHistoryOtherDetails"].Answer : "", new { @size = "80", @maxlength = "80" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="42" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "42" ){ %>checked="checked"<% }%>'" />
                        Chronic Kidney Disease </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="43" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "43" ){ %>checked="checked"<% }%>'" />
                        Renal Failure </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="44" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "44" ){ %>checked="checked"<% }%>'" />
                        Dialysis </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="45" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "45" ){ %>checked="checked"<% }%>'" />
                        Anemia </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="46" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "46" ){ %>checked="checked"<% }%>'" />
                        Abnormal Coagulation </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="47" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "47" ){ %>checked="checked"<% }%>'" />
                        Blood Clots </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="48" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "48" ){ %>checked="checked"<% }%>'" />
                        Diabetes </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="49" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "49" ){ %>checked="checked"<% }%>'" />
                        Thyroid Problems </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="50" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "50" ){ %>checked="checked"<% }%>'" />
                        COPD </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="51" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "51" ){ %>checked="checked"<% }%>'" />
                        Asthma </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="52" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "52" ){ %>checked="checked"<% }%>'" />
                        Chronic Obstructive Bronchitis </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="53" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "53" ){ %>checked="checked"<% }%>'" />
                        Emphysema </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="54" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "54" ){ %>checked="checked"<% }%>'" />
                        Chronic Obstructive Asthma </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="55" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "55" ){ %>checked="checked"<% }%>'" />
                        Urinary Incontinence </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="56" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "56" ){ %>checked="checked"<% }%>'" />
                        Urinary Retention </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="57" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "57" ){ %>checked="checked"<% }%>'" />
                        BPH </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="58" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "58" ){ %>checked="checked"<% }%>'" />
                        Recent/Frequent UTI </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="59" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "59" ){ %>checked="checked"<% }%>'" />
                        Tuberculosis </li>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="60" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "60" ){ %>checked="checked"<% }%>'" />
                        Hepatitis </li>
                    <li><i>(specify)</i>
                        <input type="text" name="ResumptionOfCare_GenericMedicalHistoryHepatitisDetails"
                            id="ResumptionOfCare_GenericMedicalHistoryHepatitisDetails" size="60" maxlength="80"
                            value="" />
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="61" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "61" ){ %>checked="checked"<% }%>'" />
                        Infectious Disease </li>
                    <li><i>(specify)</i>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryInfectiousDiseaseDetails", data.ContainsKey("GenericMedicalHistoryInfectiousDiseaseDetails") ? data["GenericMedicalHistoryInfectiousDiseaseDetails"].Answer : "", new { @size = "80", @maxlength = "80" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li class="spacer">
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="62" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "62" ){ %>checked="checked"<% }%>'" />
                        Tobacco Dependence </li>
                    <li class="spacer">Type:
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceType", data.ContainsKey("GenericMedicalHistoryTobaccoDependenceType") ? data["GenericMedicalHistoryTobaccoDependenceType"].Answer : "", new { @size = "10", @maxlength = "60", @id = "ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceType" })%>
                    </li>
                    <li class="spacer">Amount:
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceLength", data.ContainsKey("GenericMedicalHistoryTobaccoDependenceLength") ? data["GenericMedicalHistoryTobaccoDependenceLength"].Answer : "", new { @size = "10", @maxlength = "60", @id = "ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceLength" })%>
                    </li>
                    <li>Length of Time Used:
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceAmt", data.ContainsKey("GenericMedicalHistoryTobaccoDependenceAmt") ? data["GenericMedicalHistoryTobaccoDependenceAmt"].Answer : "", new { @size = "10", @maxlength = "60", @id = "ResumptionOfCare_GenericMedicalHistoryTobaccoDependenceAmt" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="63" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "63" ){ %>checked="checked"<% }%>'" />
                        Vision Problems </li>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="64" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "64" ){ %>checked="checked"<% }%>'" />
                        Hearing Loss </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <%=Html.TextBox("ResumptionOfCare_GenericMedicalHistory", data.ContainsKey("GenericMedicalHistory") ? data["GenericMedicalHistory"].Answer : "", new { @size = "10", @maxlength = "60", @id = "ResumptionOfCare_GenericMedicalHistory" })%>
                        Other </li>
                    <li>
                        <input type="text" name="ResumptionOfCare_GenericMedicalHistoryExtraDetails" id="ResumptionOfCare_GenericMedicalHistoryExtraDetails"
                            size="80" maxlength="80" value="" />
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="ResumptionOfCare_GenericMedicalHistory" value="66" type="checkbox" '<% if( data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer == "66" ){ %>checked="checked"<% }%>'" />
                        Past Surgical History: </li>
                    <li>
                        <%=Html.TextArea("ResumptionOfCare_GenericMedicalHistoryPastSuguriesDetails", data.ContainsKey("GenericMedicalHistoryPastSuguriesDetails") ? data["GenericMedicalHistoryPastSuguriesDetails"].Answer : "", 4, 70, new { @id = "ResumptionOfCare_GenericMedicalHistoryPastSuguriesDetails"})%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1020/1022/1024) Diagnoses, Symptom Control, and Payment Diagnoses: List each diagnosis
                for which the patient is receiving home care (Column 1) and enter its ICD-9-C M
                code at the level of highest specificity (no surgical/procedure codes) (Column 2).
                Diagnoses are listed in the order that best reflect the seriousness of each condition
                and support the disciplines and services provided. Rate the degree of symptom control
                for each condition (Column 2). Choose one value that represents the degree of symptom
                control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes
                (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed
                if multiple coding is indicated for any diagnoses. If a V-code is reported in place
                of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and
                4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare
                P P S case mix group. Do not assign symptom control ratings for V- or E-codes.<br />
                <b>Code each row according to the following directions for each column:<br />
                </b>Column 1: Enter the description of the diagnosis.<br />
                Column 2: Enter the ICD-9-C M code for the diagnosis described in Column 1;<br />
                Rate the degree of symptom control for the condition listed in Column 1 using the
                following scale:<br />
                0 - Asymptomatic, no treatment needed at this time<br />
                1 - Symptoms well controlled with current therapy<br />
                2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs
                ongoing monitoring<br />
                3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and
                dose monitoring<br />
                4 - Symptoms poorly controlled; history of re-hospitalizations Note that in Column
                2 the rating for symptom control of each diagnosis should not be used to determine
                the sequencing of the diagnoses listed in Column 1. These are separate items and
                sequencing may not coincide. Sequencing of diagnoses should reflect the seriousness
                of each condition and support the disciplines and services provided.<br />
                Column 3: (OPTIONAL) If a V-code is assigned to any row in Column 2, in place of
                a case mix diagnosis, it may be necessary to complete optional item M1024 Payment
                Diagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.<br />
                Column 4: (OPTIONAL) If a V-code in Column 2 is reported in place of a case mix
                diagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,
                enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns
                3 and 4. For example, if the case mix diagnosis is a manifestation code, record
                the diagnosis description and ICD-9-C M code for the underlying condition in Column
                3 of that row and the diagnosis description and ICD-9-C M code for the manifestation
                in Column 4 of that row. Otherwise, leave Column 4 blank in that row.
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2">
                (M1020) Primary Diagnosis & (M1022) Other Diagnoses
            </th>
            <th colspan="2">
                (M1024) Payment Diagnoses (OPTIONAL)
            </th>
        </tr>
        <tr>
            <th>
                Column 1
            </th>
            <th>
                Column 2
            </th>
            <th>
                Column 3
            </th>
            <th>
                Column 4
            </th>
        </tr>
        <tr>
            <td>
                Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition
                and support the disciplines and services provided.)
            </td>
            <td>
                ICD-9-C M and symptom control rating for each condition. Note that the sequencing
                of these ratings may not match the sequencing of the diagnoses
            </td>
            <td>
                Complete if a V-code is assigned under certain circumstances to Column 2 in place
                of a case mix diagnosis.
            </td>
            <td>
                Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis
                that is a multiple coding situation (e.g., a manifestation code).
            </td>
        </tr>
        <tr>
            <td>
                Description
            </td>
            <td>
                ICD-9-C M / Symptom Control Rating
            </td>
            <td>
                Description/ ICD-9-C M
            </td>
            <td>
                Description/ ICD-9-C M
            </td>
        </tr>
        <tr>
            <td valign="top" class="ICDText">
                <u>(M1020) Primary Diagnosis<br />
                </u>a.<%=Html.TextBox("ResumptionOfCare_M1020PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1020PrimaryDiagnosis" })%>
                <br />
                <br />
            </td>
            <td valign="top">
                <u>(V-codes are allowed)<br />
                </u>a.<%=Html.TextBox("ResumptionOfCare_M1020ICD9M", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1020ICD9M" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1020SymptomControlRating" id="ResumptionOfCare_M1020SymptomControlRating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td valign="top">
                <u>(V- or E-codes NOT allowed)<br />
                </u>a.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesA3", data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesA3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MA3", data.ContainsKey("M1024ICD9MA3") ? data["M1024ICD9MA3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MA3" })%>
            </td>
            <td valign="top">
                <u>(V- or E-codes NOT allowed)</u>
                <br />
                a.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesA4", data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesA4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MA4", data.ContainsKey("M1024ICD9MA4") ? data["M1024ICD9MA4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MA4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                <u>(M1022) Other Diagnoses<br />
                </u>b.<%=Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis1", data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis1" })%>
                <br />
            </td>
            <td>
                <u>(V- or E-codes are allowed)<br />
                </u>b.<%=Html.TextBox("ResumptionOfCare_M1022ICD9M1", data.ContainsKey("M1022ICD9M1") ? data["M1022ICD9M1"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1022ICD9M1" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1022OtherDiagnose1Rating" id="ResumptionOfCare_M1022OtherDiagnose1Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                b.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesB3", data.ContainsKey("M1024PaymentDiagnosesB3") ? data["M1024PaymentDiagnosesB3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesB3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MB3", data.ContainsKey("M1024ICD9MB3") ? data["M1024ICD9MB3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MB3" })%>
            </td>
            <td>
                b.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesB4", data.ContainsKey("M1024PaymentDiagnosesB4") ? data["M1024PaymentDiagnosesB4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesB4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MB4", data.ContainsKey("M1024ICD9MB4") ? data["M1024ICD9MB4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MB4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                c.<%=Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis2", data.ContainsKey("M1022PrimaryDiagnosis2") ? data["M1022PrimaryDiagnosis2"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis2" })%>
                <br />
            </td>
            <td>
                c.<%=Html.TextBox("ResumptionOfCare_M1022ICD9M2", data.ContainsKey("M1022ICD9M2") ? data["M1022ICD9M2"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1022ICD9M2" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1022OtherDiagnose2Rating" id="ResumptionOfCare_M1022OtherDiagnose2Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                c.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesC3", data.ContainsKey("M1024PaymentDiagnosesC3") ? data["M1024PaymentDiagnosesC3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesC3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MC3", data.ContainsKey("M1024ICD9MC3") ? data["M1024ICD9MC3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MC3" })%>
            </td>
            <td>
                c.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesC4", data.ContainsKey("M1024PaymentDiagnosesC4") ? data["M1024PaymentDiagnosesC4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesC4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MC4", data.ContainsKey("M1024ICD9MC4") ? data["M1024ICD9MC4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MC4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                d.<%=Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis3", data.ContainsKey("M1022PrimaryDiagnosis3") ? data["M1022PrimaryDiagnosis3"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis3" })%>
                <br />
            </td>
            <td>
                d.<%=Html.TextBox("ResumptionOfCare_M1022ICD9M3", data.ContainsKey("M1022ICD9M3") ? data["M1022ICD9M3"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1022ICD9M3" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1022OtherDiagnose3Rating" id="ResumptionOfCare_M1022OtherDiagnose3Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                d.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesD3", data.ContainsKey("M1024PaymentDiagnosesD3") ? data["M1024PaymentDiagnosesD3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesD3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MD3", data.ContainsKey("M1024ICD9MD3") ? data["M1024ICD9MD3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MD3" })%>
            </td>
            <td>
                d.
                <%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesD4", data.ContainsKey("M1024PaymentDiagnosesD4") ? data["M1024PaymentDiagnosesD4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesD4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MD4", data.ContainsKey("M1024ICD9MD4") ? data["M1024ICD9MD4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MD4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                e.<%=Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis4", data.ContainsKey("M1022PrimaryDiagnosis4") ? data["M1022PrimaryDiagnosis4"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis4" })%>
                <br />
            </td>
            <td>
                e.<%=Html.TextBox("ResumptionOfCare_M1022ICD9M4", data.ContainsKey("M1022ICD9M4") ? data["M1022ICD9M4"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1022ICD9M4" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1022OtherDiagnose4Rating" id="ResumptionOfCare_M1022OtherDiagnose4Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                e.
                <%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesE3", data.ContainsKey("M1024PaymentDiagnosesE3") ? data["M1024PaymentDiagnosesE3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesE3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9ME3", data.ContainsKey("M1024ICD9ME3") ? data["M1024ICD9ME3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9ME3" })%>
            </td>
            <td>
                e.
                <%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesE4", data.ContainsKey("M1024PaymentDiagnosesE4") ? data["M1024PaymentDiagnosesE4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesE4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9ME4", data.ContainsKey("M1024ICD9ME4") ? data["M1024ICD9ME4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9ME4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                f.<%=Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis5", data.ContainsKey("M1022PrimaryDiagnosis5") ? data["M1022PrimaryDiagnosis5"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis5" })%>
                <br />
            </td>
            <td>
                f.<%=Html.TextBox("ResumptionOfCare_M1022ICD9M5", data.ContainsKey("M1022ICD9M5") ? data["M1022ICD9M5"].Answer : "", new { @class = "ICD", @id = "ResumptionOfCare_M1022ICD9M5" })%>
                <br />
                <b>Severity:</b>
                <select name="ResumptionOfCare_M1022OtherDiagnose5Rating" id="ResumptionOfCare_M1022OtherDiagnose5Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                f.
                <%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesF3", data.ContainsKey("M1024PaymentDiagnosesF3") ? data["M1024PaymentDiagnosesF3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesF3" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MF3", data.ContainsKey("M1024ICD9MF3") ? data["M1024ICD9MF3"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MF3" })%>
            </td>
            <td>
                f.<%=Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesF4", data.ContainsKey("M1024PaymentDiagnosesF4") ? data["M1024PaymentDiagnosesF4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "ResumptionOfCare_M1024PaymentDiagnosesF4" })%>
                <br />
                <%=Html.TextBox("ResumptionOfCare_M1024ICD9MF4", data.ContainsKey("M1024ICD9MF4") ? data["M1024ICD9MF4"].Answer : "", new { @class = "ICDM1024 pad", @id = "ResumptionOfCare_M1024ICD9MF4" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1030) Therapies the patient receives at home: (Mark all that apply.)
            </div>
        </div>
        <div class="padding">
            <input name="ResumptionOfCare_M1030HomeTherapiesInfusion" value=" " type="hidden" />
            <input name="ResumptionOfCare_M1030HomeTherapiesInfusion" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ){ %>checked="checked"<% }%>'" />1
            - Intravenous or infusion therapy (excludes TPN)<br />
            <input name="ResumptionOfCare_M1030HomeTherapiesParNutrition" value=" " type="hidden" />
            <input name="ResumptionOfCare_M1030HomeTherapiesParNutrition" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ){ %>checked="checked"<% }%>'" />2
            - Parenteral nutrition (TPN or lipids)<br />
            <input name="ResumptionOfCare_M1030HomeTherapiesEntNutrition" value=" " type="hidden" />
            <input name="ResumptionOfCare_M1030HomeTherapiesEntNutrition" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ){ %>checked="checked"<% }%>'" />3
            - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial
            entry into the alimentary canal)<br />
            <input name="ResumptionOfCare_M1030HomeTherapiesNone" value=" " type="hidden" />
            <input name="ResumptionOfCare_M1030HomeTherapiesNone" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />4
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="ROC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="ROC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
