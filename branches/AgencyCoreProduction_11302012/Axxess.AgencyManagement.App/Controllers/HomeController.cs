﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Xml;
    using System.Web.Mvc;
    using System.ServiceModel.Syndication;

    using Enums;
    using Domain;
    using Services;
    using Security;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly IUserService userService;
        private readonly ILoginRepository loginRepository;
        private readonly IErrorRepository errorRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.userService = userService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.errorRepository = membershipDataProvider.ErrorRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.aspx");
        }
       
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Logs()
        {
            return View("~/Views/Shared/Logs.aspx", errorRepository.GetSome());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Updates()
        {
            return View();
        }

        #endregion
    }
}
