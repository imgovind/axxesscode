﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;
    using Axxess.AgencyManagement.Enums;

    public class MissedVisitsExporter : BaseExporter
    {
        private IList<ScheduleEvent> missedVisit;
        private DateTime StartDate;
        private DateTime EndDate;
        public MissedVisitsExporter(IList<ScheduleEvent> missedVisit, DateTime startDate, DateTime endDate)
            : base()
        {
            this.missedVisit = missedVisit;
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Missed Visits";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("MissedVisits");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Missed Visits");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Employee");
            headerRow.CreateCell(4).SetCellValue("Original Status");
            headerRow.CreateCell(5).SetCellValue("Scheduled Date");
            headerRow.CreateCell(6).SetCellValue("Visit Date");

            if (this.missedVisit.Count > 0)
            {
                int i = 2;
                this.missedVisit.ForEach(v =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(v.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(v.PatientName);
                    dataRow.CreateCell(2).SetCellValue(v.DisciplineTaskName.ToTitleCase());
                    dataRow.CreateCell(3).SetCellValue(v.UserName);
                    int status = v.Status.IsNotNullOrEmpty() && v.Status.IsInteger() ? v.Status.ToInteger() : 0;
                    dataRow.CreateCell(4).SetCellValue(status != 0 ? ((ScheduleStatus)status).GetDescription() : string.Empty);
                    if (v.EventDate.IsValidDateAndNotMin())
                    {
                        DateTime visitDate = DateTime.Parse(v.EventDate);
                        var createdDateCell = dataRow.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(5).SetCellValue(string.Empty);
                    }
                    if (v.VisitDate.IsValidDateAndNotMin())
                    {
                        DateTime visitDate = DateTime.Parse(v.VisitDate);
                        var createdDateCell = dataRow.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(6).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Missed Visits: {0}", missedVisit.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(7);
        }
    }
}
