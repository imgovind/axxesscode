﻿namespace Axxess.AgencyManagement.App.Exports {
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class NonAdmitExporter : BaseExporter {
        private IList<NonAdmit> admissions;
        public NonAdmitExporter(IList<NonAdmit> admissions) : base() {
            this.admissions = admissions;
        }

        protected override void InitializeExcel() {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Non Admitted Patients";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet() {
            var sheet = base.workBook.CreateSheet("Non Admitted Patients");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Non Admitted Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Insurance");
            headerRow.CreateCell(3).SetCellValue("Policy #");
            headerRow.CreateCell(4).SetCellValue("Address");
            headerRow.CreateCell(5).SetCellValue("Date of Birth");
            headerRow.CreateCell(6).SetCellValue("Phone");
            headerRow.CreateCell(7).SetCellValue("Gender");
            headerRow.CreateCell(8).SetCellValue("Non-Admit Reason");
            headerRow.CreateCell(9).SetCellValue("Non-Admit Date");
            if (this.admissions.Count > 0) {
                int i = 2;
                this.admissions.ForEach(a => {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(a.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(a.InsuranceName);
                    dataRow.CreateCell(3).SetCellValue(a.PolicyNumber);
                    dataRow.CreateCell(4).SetCellValue(a.AddressFullFormatted);
                    DateTime dateOfBirth = a.DateOfBirth.ToDateTimeOrMin();
                    if (dateOfBirth != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(dateOfBirth);
                    }
                    else
                    {
                        dataRow.CreateCell(5).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(6).SetCellValue(a.Phone);
                    dataRow.CreateCell(7).SetCellValue(a.Gender);
                    dataRow.CreateCell(8).SetCellValue(a.NonAdmissionReason);
                    DateTime nonAdmitDate = a.NonAdmitDate.ToDateTimeOrMin();
                    if (nonAdmitDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(9);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(nonAdmitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(9).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Non Admitted Patients: {0}", admissions.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(10);
        }
    }
}
