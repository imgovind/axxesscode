﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml;
    using System.Xml.Xsl;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Reflection;
    using System.Collections.Generic;

    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.LookUp.Domain;
    using System.Web;
    using System.IO;
    using Axxess.AgencyManagement.App.Common;
    using Telerik.Web.Mvc.UI;
using System.Linq.Expressions;
    using System.Web.Routing;

    public static class HtmlHelperExtensions
    {
        #region Data Repositories

        private static IUserRepository userRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        private static IAssetRepository assetRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AssetRepository;
            }
        }

        private static IPatientRepository patientRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static IPhysicianRepository physicianRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PhysicianRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static IAssessmentRepository assessmentRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.OasisAssessmentRepository;
            }
        }

        private static ICachedDataRepository cachedDataRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.CachedDataRepository;
            }
        }

        private static IBillingRepository billingRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.BillingRepository;
            }
        }


        #endregion

        #region HtmlHelper Extensions

        public static MvcHtmlString AssemblyVersion(this HtmlHelper html)
        {
            System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
            var versionText = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            return MvcHtmlString.Create(versionText);
        }

        public static MvcHtmlString ZipCode(this HtmlHelper html)
        {
            var zipCode = "75243";
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null && agency.MainLocation != null)
            {
                zipCode = agency.MainLocation.AddressZipCode;
            }
            return MvcHtmlString.Create(zipCode);
        }

        public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId)
        {
            var sb = new StringBuilder();
            if (!assetId.IsEmpty())
            {
                var asset = assetRepository.Get(assetId, Current.AgencyId);
                if (asset != null)
                {
                    sb.AppendFormat("<a href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipient-list\" id=\"recipient-list\">");
            sb.AppendLine("<div class=\"inbox-subheader\"><span>Recipient List</span></div>");
            sb.AppendLine("<div class=\"recipient-panel\"><div class=\"recipient\"><input type=\"checkbox\" class=\"contact\" id=\"New_Message_CheckallRecipients\" />&#160;<label for=\"New_Message_CheckallRecipients\" class=\"bold\">Select All</label></div>");

            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0)
            {
                int counter = 1;
                recipients.ForEach(r =>
                {

                        sb.AppendFormat("<div class=\"recipient\"><input name=\"Recipients\" type=\"checkbox\" id=\"New_Message_Recipient_{0}\" value=\"{1}\" title=\"{2}\" onclick=\"Message.AddRemoveRecipient('New_Message_Recipient_{0}');\" />&#160;<label for=\"New_Message_Recipient_{0}\">{2}</label></div>", counter.ToString(), r.Id.ToString(), r.DisplayName);
                        sb.AppendLine();
                        counter++;
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html, string prefix, List<Guid> selectedRecipients) {
            var sb = new StringBuilder();
            if (selectedRecipients == null)
            {
                selectedRecipients = new List<Guid>();
            }
            sb.AppendLine("<table class=\"fixed\"><tbody><tr><td>");
            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0) {
                int totalItems = recipients.Count;
                for (int i = 0; i < totalItems / 3; i++) sb.AppendFormat("<input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\" style=\"float: left; width: 20px;\"{4} />&#160;<label for=\"{0}_Recipient{1}\" class=\"float-left\">{3}</label><div class='clear'></div>", prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked" : string.Empty);
                sb.Append("</td><td>");
                for (int i = totalItems / 3; i < (totalItems / 3) * 2; i++) sb.AppendFormat("<input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\" style=\"float: left; width: 20px;\"{4} />&#160;<label for=\"{0}_Recipient{1}\" class=\"float-left\">{3}</label><div class='clear'></div>", prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked" : string.Empty);
                sb.Append("</td><td>");
                for (int i = (totalItems / 3) * 2; i < totalItems; i++) sb.AppendFormat("<input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\" style=\"float: left; width: 20px;\"{4} />&#160;<label for=\"{0}_Recipient{1}\" class=\"float-left\">{3}</label><div class='clear'></div>", prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked" : string.Empty);
            }
            sb.AppendLine("</td></tr></tbody></table>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Templates(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text = "-- Select Template --", Value = "" });
            items.Insert(items.Count, new SelectListItem { Text = "-- Erase --", Value = "empty" });
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            if (templates != null && templates.Count > 0)
            {
                items.Insert(items.Count, new SelectListItem { Text = "------------------------------", Value = "spacer" });
                templates.ForEach(t => {
                    items.Add(new SelectListItem { Text = t.Title, Value = t.Id.ToString() });
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Status(this HtmlHelper html, string name, string status, int disciplineTask, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (disciplineTask > 0)
            {
                DisciplineTasks task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                Array scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
                foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
                {
                    if (scheduleStatus.GetGroup().IsEqual(taskGroup))
                    {
                        int statusId = (int)scheduleStatus;
                        if (status.IsEqual(statusId.ToString()))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = scheduleStatus.GetDescription(),
                                Value = statusId.ToString(),
                                Selected = true
                            });
                        }
                    }
                }

                string discipline = task.GetCustomCategory();
                switch (discipline.ToLower())
                {
                    case "sn":
                    case "pt":
                    case "ot":
                    case "st":
                    case "hha":
                    case "notes":
                    case "msw":
                        if (disciplineTask == (int)DisciplineTasks.PTEvaluation 
                            || disciplineTask == (int)DisciplineTasks.OTEvaluation 
                            || disciplineTask == (int)DisciplineTasks.STEvaluation 
                            || disciplineTask == (int)DisciplineTasks.PTReEvaluation 
                            || disciplineTask == (int)DisciplineTasks.OTReEvaluation 
                            || disciplineTask == (int)DisciplineTasks.STReEvaluation
                            || disciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted).ToString() && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", (ScheduleStatus.EvalReturnedWPhysicianSignature).GetDescription()),
                                    Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                    Selected = false
                                });
                                items.Add(new SelectListItem
                                {
                                    Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                            else
                            {
                                if (status != ((int)ScheduleStatus.NoteCompleted).ToString() && status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                        Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                        Selected = false
                                    });
                                }
                                if (status == ((int)ScheduleStatus.NoteCompleted).ToString() && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = string.Format("Completed - {0}", (ScheduleStatus.EvalReturnedWPhysicianSignature).GetDescription()),
                                        Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                        Selected = false
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                        }
                        break;
                    case "485":
                    case "486":
                    case "order":
                    case "nonoasis485":
                        if (status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", (ScheduleStatus.OrderReturnedWPhysicianSignature).GetDescription()),
                                Value = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "reportsandnotes":
                        if (status != ((int)ScheduleStatus.ReportAndNotesCompleted).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = (ScheduleStatus.ReportAndNotesCompleted).GetDescription(),
                                Value = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "oasis":
                    case "nonoasis":
                        if (status != ((int)ScheduleStatus.OasisExported).ToString() && status != ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", (ScheduleStatus.OasisExported).GetDescription()),
                                Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                Selected = false
                            });
                            items.Add(new SelectListItem
                            {
                                Text = (ScheduleStatus.OasisCompletedNotExported).GetDescription(),
                                Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                Selected = false
                            });
                        }
                        else
                        {
                            if (status == ((int)ScheduleStatus.OasisExported).ToString() && status != ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                            {
                                items.Add(new SelectListItem
                                   {
                                       Text = (ScheduleStatus.OasisCompletedNotExported).GetDescription(),
                                       Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                       Selected = false
                                   });
                            }
                            else if (status != ((int)ScheduleStatus.OasisExported).ToString() && status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", (ScheduleStatus.OasisExported).GetDescription()),
                                    Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                    Selected = false
                                });
                            }

                        }
                        break;
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString VisitStatus(this HtmlHelper html, string name, string status, int disciplineTask,bool isSelectTitle, string title, string defaultValue, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (disciplineTask > 0)
            {
                DisciplineTasks task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                Array scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
                foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
                {
                    if (scheduleStatus.GetGroup().IsEqual(taskGroup))
                    {
                        int statusId = (int)scheduleStatus;
                        items.Add(new SelectListItem
                        {
                            Text = scheduleStatus.GetDescription(),
                            Value = statusId.ToString(),
                            Selected = status.IsEqual(statusId.ToString())
                        });
                    }
                }
                if (isSelectTitle)
                {
                    items.Insert(0, new SelectListItem
                    {
                        Text = title,
                        Value = defaultValue
                    });
                }
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BillableVisitStatus(this HtmlHelper html, string name, string status, bool isSelectTitle, string title, string defaultValue, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var visitGroup = DisciplineTasks.SkilledNurseVisit.GetCustomGroup();
            var oasisGroup = DisciplineTasks.OASISCStartofCare.GetCustomGroup();

            Array scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
            foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
            {
                if (!((int)scheduleStatus >= 400 && (int)scheduleStatus < 405))
                {
                    if (scheduleStatus.GetGroup().IsEqual(visitGroup) || scheduleStatus.GetGroup().IsEqual(oasisGroup))
                    {
                        int statusId = (int)scheduleStatus;
                        string group = scheduleStatus.GetGroup().IsEqual(visitGroup) ? "Note" : "Oasis";
                        items.Add(new SelectListItem
                        {
                            Text = group + " - " + scheduleStatus.GetDescription(),
                            Value = statusId.ToString(),
                            Selected = status.IsEqual(statusId.ToString())
                        });
                    }
                }
            }
            if (isSelectTitle)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = defaultValue
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = agencyRepository.All();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousNotes(this HtmlHelper html, IDictionary<Guid, string> previousNotes, object htmlAttributes)
        {
            var items = previousNotes.Select(e => new SelectListItem
                        {
                            Text = e.Value,
                            Value = e.Key.ToString()
                        }).ToList() ?? new List<SelectListItem>();

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Previous Notes --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList("PreviousNotes", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousAssessments(this HtmlHelper html, Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate, object htmlAttributes)
        {
            int assessmentTypeNum = (int)assessmentType;
            var items = new List<SelectListItem>();
            var scheduleEvents = new List<ScheduleEvent>();
            if (assessmentType > 0)
            {
                var episodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduleDate.AddMonths(-6), scheduleDate);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(e =>
                    {
                        var evets = e.Schedule.ToObject<List<ScheduleEvent>>().Where(v => !v.IsMissedVisit && !v.IsDeprecated && v.EventDate.IsValidDate() && v.EventDate.ToDateTime().Date < scheduleDate.Date && (assessmentType <= 9 ? (v.IsOASISSOC() || v.IsOASISROC() || v.IsOASISRecert() ||v.IsOASISDischarge() || v.IsOASISDOD() || v.IsOASISFollowUp() || v.IsOASISTransferDischarge() || v.IsOASISTransfer()) : (v.IsNONOASISSOC() || v.IsNONOASISRecert() || v.IsNONOASISDischarge())) & (v.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || v.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || v.Status == ((int)ScheduleStatus.OasisExported).ToString() || v.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                        if (evets != null && evets.Count > 0)
                        {
                            scheduleEvents.AddRange(evets);
                        }
                    });
                }
            }

            var shortList = scheduleEvents.OrderByDescending(v => v.EventDate.ToDateTime().Date).Take(5).ToList();
            if (shortList != null && shortList.Count > 0)
            {
                shortList.ForEach(v =>
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), v.DisciplineTask))
                    {
                        items.Add(new SelectListItem { Text = string.Format("{0} {1} by {2}", ((DisciplineTasks)v.DisciplineTask).GetDescription(), v.EventDate.ToZeroFilled(), UserEngine.GetName(v.UserId, Current.AgencyId)), Value = string.Format("{0}_{1}", v.EventId, ((DisciplineTasks)v.DisciplineTask).ToString()) });
                    }
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select --",
                Value = string.Empty
            });
            return html.DropDownList("PreviousAssessments", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousCarePlans(this HtmlHelper html, Guid patientId, Guid assessmentId, string planOfCare, DateTime scheduleDate, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var scheduleEvents = new List<ScheduleEvent>();
            var episodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduleDate.AddMonths(-12), scheduleDate);
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(e =>
                {                                                                                                                                                                                               //(assessmentType < 5 ? (v.IsOASISSOC() || v.IsOASISROC() || v.IsOASISRecert()) : (v.IsNONOASISSOC() || v.IsNONOASISRecert()))
                    var evets = e.Schedule.ToObject<List<ScheduleEvent>>().Where(v => !v.IsMissedVisit && !v.IsDeprecated && v.EventDate.IsValidDate() && v.EventDate.ToDateTime().Date < scheduleDate.Date && v.DisciplineTask.ToName<DisciplineTasks>(0) == planOfCare && (v.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString() || v.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString() || v.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || v.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString())).ToList();
                    if (evets != null && evets.Count > 0)
                    {
                        scheduleEvents.AddRange(evets);
                    }
                });
            }

            var shortList = scheduleEvents.OrderByDescending(v => v.EventDate.ToDateTime().Date).Take(5).ToList();
            if (shortList != null && shortList.Count > 0)
            {
                shortList.ForEach(v =>
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), v.DisciplineTask))
                    {
                        items.Add(new SelectListItem { 
                            Text = string.Format("{0} {1} by {2}", ((DisciplineTasks)v.DisciplineTask).GetDescription(), v.EventDate.ToZeroFilled(), UserEngine.GetName(v.UserId, Current.AgencyId)),
                            Value = string.Format("{0}_{1}_{2}", v.EventId, v.EpisodeId, ((DisciplineTasks)v.DisciplineTask).ToString()) });
                    }
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select --",
                Value = string.Empty
            });
            return html.DropDownList("PreviousAssessments", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsuranceListByBranch(this HtmlHelper html, string name, string value,string branchId, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (branchId.IsNotNullOrEmpty() && branchId.IsGuid())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId.ToGuid());
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = standardInsurance.Id.ToString().IsEqual(value) });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = standardInsurance.Id.ToString().IsEqual(value) });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = standardInsurance.Id.ToString().IsEqual(value) });
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(i =>
                {
                    items.Add(new SelectListItem { Text = i.Name, Value = i.Id.ToString(), Selected = i.Id.ToString().IsEqual(value) });
                });
            }
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                         {
                             Text = standardInsurance.Name,
                             Value = standardInsurance.Id.ToString(),
                             Selected = (standardInsurance.Id.ToString().IsEqual(value))
                         });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesFilter(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            items.Add(new SelectListItem()
                {
                    Text = "No Insurance assigned",
                    Value = "-1"
                });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                if (i.PayorType == 2 || i.PayorType == 3)
                {
                    items.Add(new SelectListItem() { Text = i.Name, Value = i.Id.ToString(), Selected = (i.Id.ToString().IsEqual(value)) });
                }
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
        public static MvcHtmlString RatedUser(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var users = userRepository.GetRatedUserByBranch(value.ToGuid(), Current.AgencyId);
            users.ForEach(u =>
                {
                    items.Add(new SelectListItem()
                    {
                        Text=u.DisplayName,
                        Value=u.Id.ToString(),
                        Selected=false
                    });
                });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesNoneMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });

                    }
                }
            }

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Insurance --",
                Value = "0"
            });

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, IDictionary<string, string> htmlAttributes)
        {
            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes.ToString(), "0", 0);
            if (addNew)
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance =>
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", insurance.Id.ToString(), 1, ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString ReportBranchList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- All Branches --",
                Value = Guid.Empty.ToString(),
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BranchOnlyList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MedicationTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "New", Value = "N", Selected = value.IsEqual("N") });
            items.Add(new SelectListItem { Text = "Changed", Value = "C", Selected = value.IsEqual("C") });
            items.Add(new SelectListItem { Text = "Unchanged", Value = "U", Selected = value.IsEqual("U") });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var taskList = new List<Item>();
            var tasks = lookupRepository.DisciplineTasks(discipline);
            tasks.ForEach(task =>
            {
               // var typeofTask = task.BillDisciplineIdentify();
                //var rate = new ChargeRate();
                //BillingService.SetBillRateValue(rate, task);
                taskList.Add(new Item { Name = string.Format("{0} ({1})", task.Task, task.GCode), Value = task.Id.ToString() });
            });

            tempItems = from task in taskList
                        select new SelectListItem
                        {
                            Text = task.Name,
                            Value = task.Id.ToString(),
                            Selected = (task.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Task --",
                Value = ""
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MultipleDisciplineTasks(this HtmlHelper html, string name, string value, IDictionary<string, string> htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == "Nursing" || d.Discipline == "PT" || d.Discipline == "ST" || d.Discipline == "OT" || d.Discipline == "HHA" || d.Discipline == "MSW" || d.Discipline == "Orders" || d.Discipline == "ReportsAndNotes")
                .OrderBy(d => d.Task)
                .ToList();

            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value=''>-- Select Discipline --</option>", name, attributes.ToString());
            tasks.ForEach(task =>
            {
                selectList.AppendFormat("<option value='{0}' IsBillable='{1}' data='{2}'{3}>{4}</option>", task.Id.ToString(), task.IsBillable, task.Discipline, ((task.Id.ToString().IsEqual(value)) ? " selected" : ""), task.Task);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString StatusPatients(this HtmlHelper html, string name, string excludedValue, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array patientStatusValues = Enum.GetValues(typeof(PatientStatus));
            foreach (PatientStatus patientStatus in patientStatusValues)
            {
                var statusId = (int)patientStatus;
                if (statusId.ToString() != excludedValue && statusId != (int)PatientStatus.Deprecated)
                {
                    items.Add(new SelectListItem
                    {
                        Text = patientStatus.GetDescription(),
                        Value = statusId.ToString()
                    });
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DischargeReasons(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array dischargeReasonsValues = Enum.GetValues(typeof(DischargeReasons));
            foreach (DischargeReasons dischargeReasons in dischargeReasonsValues)
            {
                items.Add(new SelectListItem
                {
                    Text = dischargeReasons.GetDescription(),
                    Value = ((int)dischargeReasons).ToString()
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Reason --",
                Value = ""
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, SelectListTypes listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.States:
                    var states = lookupRepository.States();
                    tempItems = from state in states
                                select new SelectListItem
                                {
                                    Text = state.Name,
                                    Value = state.Code,
                                    Selected = (state.Code.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select State --",
                        Value = " "
                    });
                    break;
                case SelectListTypes.Insurance:
                    var insurances = agencyRepository.GetInsurances(Current.AgencyId);
                    tempItems = from insurance in insurances
                                select new SelectListItem
                                {
                                    Text = insurance.Name,
                                    Value = insurance.Id.ToString(),
                                    Selected = (insurance.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Insurance --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.AuthorizationStatus:
                    var authStatusTypes = new List<string>();
                    Array authStatusValues = Enum.GetValues(typeof(AuthorizationStatusTypes));
                    foreach (AuthorizationStatusTypes authStatusType in authStatusValues)
                    {
                        authStatusTypes.Add(authStatusType.GetDescription());
                    }
                    tempItems = from type in authStatusTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.CahpsVendors:
                    var cahpsVendors = new Dictionary<string, int>();
                    Array cahpsVendorValues = Enum.GetValues(typeof(CahpsVendors));
                    foreach (CahpsVendors cahpsVendor in cahpsVendorValues)
                    {
                        cahpsVendors.Add(cahpsVendor.GetDescription(), (int)cahpsVendor);
                    }

                    tempItems = from type in cahpsVendors
                                select new SelectListItem
                                {
                                    Text = type.Key,
                                    Value = type.Value.ToString(),
                                    Selected = (type.Value.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.ContactTypes:
                    var contactTypes = new List<string>();
                    Array contactTypeValues = Enum.GetValues(typeof(ContactTypes));
                    foreach (ContactTypes contactType in contactTypeValues)
                    {
                        contactTypes.Add(contactType.GetDescription());
                    }

                    tempItems = from type in contactTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Contact Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.LicenseTypes:
                    var licenseTypes = new List<string>();
                    Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
                    foreach (LicenseTypes licenseType in licenseTypeValues)
                    {
                        licenseTypes.Add(licenseType.GetDescription());
                    }

                    tempItems = from type in licenseTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select License Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.CredentialTypes:
                    var credentialTypes = new List<string>();
                    Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
                    foreach (CredentialTypes credentialType in credentialValues)
                    {
                        credentialTypes.Add(credentialType.GetDescription());
                    }

                    tempItems = from type in credentialTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Unknown --",
                        Value = ""
                    });
                    break;
                case SelectListTypes.TitleTypes:
                    var titleTypes = new List<string>();
                    Array titleValues = Enum.GetValues(typeof(TitleTypes));
                    foreach (TitleTypes titleType in titleValues)
                    {
                        titleTypes.Add(titleType.GetDescription());
                    }

                    tempItems = from type in titleTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Title Type --",
                        Value = "",
                    });
                    break;
                case SelectListTypes.AdmissionSources:
                    var adminSources = lookupRepository.AdmissionSources();
                    tempItems = from source in adminSources
                                select new SelectListItem
                                {
                                    Text = string.Format("({0}) {1}", source.Code, source.Description),
                                    Value = source.Id.ToString(),
                                    Selected = (source.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Admission Source --",
                        Value = "0",
                    });
                    break;
                case SelectListTypes.Users:
                    var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
                    users = users.OrderBy(u => u.DisplayName).ToList();
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;
                case SelectListTypes.Branches:
                    var branches = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branches
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;

                case SelectListTypes.BranchesReport:
                    var branchesReport = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branchesReport
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "All",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Patients:
                    if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                    {
                        var patients1 = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId).ToList();
                        var patients2 = patientRepository.FindPatientOnly((int)PatientStatus.Discharged, Current.AgencyId).ToList();
                        var patients = patients1.Union(patients2).ToList();
                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayNameWithMi.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    else if (Current.IsClinicianOrHHA)
                    {
                        var patients1 = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);
                        var patients2 = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);
                        var patients = patients1.Union(patients2).ToList();
                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayName.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Physicians:
                    var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
                    tempItems = from physician in physicians
                                select new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                                    Value = physician.Id.ToString(),
                                    Selected = (physician.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Physician --",
                        Value = Guid.Empty.ToString()
                    });

                    break;
                case SelectListTypes.PaymentSource:
                    var paymentSources = lookupRepository.PaymentSources();
                    tempItems = from paymentSource in paymentSources
                                select new SelectListItem
                                {
                                    Text = paymentSource.Name,
                                    Value = paymentSource.Id.ToString(),
                                    Selected = (paymentSource.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                  {
                      Text = "-- Select Payment Source --",
                      Value = "0"
                  });
                    items.Insert(11, new SelectListItem
                    {
                        Text = "Contract",
                        Value = "13"
                    });
                    break;
                default:
                    break;

                case SelectListTypes.MapAddress:
                    var addresses = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from address in addresses
                                select new SelectListItem
                                {
                                    Text = address.Name,
                                    Value = string.Format("{0} {1}, {2}, {3} {4}", address.AddressLine1, address.AddressLine2, address.AddressCity, address.AddressStateCode, address.AddressZipCode)
                                };
                    items = tempItems.ToList();
                    if (Current.UserAddress.IsNotNullOrEmpty())
                    {
                        items.Insert(0, new SelectListItem
                        {
                            Text = "Your Address",
                            Value = Current.UserAddress
                        });
                    }
                    items.Add(new SelectListItem
                    {
                        Text = "Specify Address",
                        Value = "specify"
                    });
                    break;
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PaymentSourceWithOutMedicareTradition(this HtmlHelper html,  string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var paymentSources = lookupRepository.PaymentSources();
            tempItems = from paymentSource in paymentSources where paymentSource.Id!=3
                        select new SelectListItem
                        {
                            Text = paymentSource.Name,
                            Value = paymentSource.Id.ToString(),
                            Selected = (paymentSource.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Payment Source --",
                Value = "0"
            });
            items.Insert(11, new SelectListItem
            {
                Text = "Contract",
                Value = "13"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectListWithBranchAndStatus(this HtmlHelper html, SelectListTypes listType, string name, string value, Guid branchId, int status, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.Users:
                    var users = new List<User>();
                    if (status == 0)
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersOnly(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                    }
                    else
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersByStatus(Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }

                    }
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;

                case SelectListTypes.Patients:
                    var patients = patientRepository.Find(status, branchId, Current.AgencyId);
                    tempItems = from patient in patients
                                select new SelectListItem
                                {
                                    Text = patient.DisplayNameWithMi.Trim().ToUpperCase(),
                                    Value = patient.Id.ToString(),
                                    Selected = (patient.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;

            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Physicians(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
            tempItems = from physician in physicians
                        select new SelectListItem
                        {
                            Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                            Value = physician.Id.ToString(),
                            Selected = (physician.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Physician --",
                Value = Guid.Empty.ToString()
            });

            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Physician **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PermissionList(this HtmlHelper html, string category, List<string> userPermissions)
        {
            var sb = new StringBuilder();
            sb.Append("<table class=\"form\">");
            sb.AppendFormat("<tr><td><strong>{0}</strong>&#160;-&#160;", category);
            sb.AppendFormat("<input id=\"New_User_{0}Permissions\" type=\"checkbox\" onclick=\"User.SelectPermissions($(this), 'input.{0}:checkbox');\" class=\"radio\" value=\"\" />", category.Replace(" ", "").Replace("/", ""));
            sb.AppendFormat("&#160;<label for=\"New_User_{0}Permissions\">Select all {1}</label></td></tr>", category.Replace(" ", "").Replace("/", ""), category);

            Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
            foreach (Permissions permission in permissions)
            {
                Permission p = permission.GetPermission();
                if (p.Category == category)
                {
                    sb.AppendFormat("<tr><td class=\"permission\" tooltip=\"{0}\">", p.Tip);
                    ulong id = (ulong)permission;
                    string controlChecked = string.Empty;
                    string controlId = string.Format("New_User_Permission_{0}", id.ToString());
                    if (userPermissions != null && userPermissions.Count > 0)
                    {
                        controlId = string.Format("Edit_User_Permission_{0}", id.ToString());
                        if (userPermissions.Contains(id.ToString()))
                        {
                            controlChecked = " Checked=\"Checked\"";
                        }
                    }

                    sb.AppendFormat("<input id=\"{0}\" type=\"checkbox\" value=\"{1}\" name=\"PermissionsArray\" class=\"{2} required radio float-left\"{3} />", controlId.ToString(), id.ToString(), category.Replace(" ", "").Replace("/", ""), controlChecked);
                    sb.AppendFormat("<label for=\"{0}\" class=\"radio bold\">{1}<span style=\"font-weight:normal;\"> - {2}</span></label>", controlId.ToString(), p.Description, p.LongDescription);
                    sb.AppendLine("</td></tr>");
                }
            }
            sb.AppendLine("</table>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CaseManagers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetCaseManagerUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Case Manager --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Auditors(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetAuditors(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Auditor --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString HHAides(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetHHAUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Home Health Aide --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PTTherapists(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetPTUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select PTA --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString OTTherapists(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetOTUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select COTA --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LVNs(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetLVNUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select LVN/LPN --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            return Clinicians(html, name, value, "-- Select Clinician --", htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, string title, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetClinicalUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
            users = users.OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Users --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Patients(this HtmlHelper html, string name, string value, int status, string title, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                var patients = patientRepository.Find(status, Guid.Empty, Current.AgencyId);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayNameWithMi.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
            else if (Current.IsClinicianOrHHA)
            {
                var patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)status);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayNameWithMi.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
           
            items = tempItems.OrderBy(i => i.Text).ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(e => items.Add(new SelectListItem
                {
                    Text = string.Format("{0} - {1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")),
                    Value = e.Id.ToString(),
                    Selected = (e.Id.ToString().IsEqual(value))
                }));
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (!patientId.IsEmpty())
            {
                var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
                if (episodes != null && episodes.Count > 0)
                {
                    items = episodes.Select(e => new SelectListItem()
                    {
                        Text = string.Format("{0} - {1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")),
                        Value = e.Id.ToString(),
                        Selected = (e.Id.ToString().IsEqual(value))
                    }).ToList();
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Months(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            List<SelectListItem> tempItems = new List<SelectListItem>();

            int currentYear = DateTime.Now.Year;
            if (start < currentYear)
            {
                for (int val = start; val <= currentYear; val++)
                {
                    tempItems.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = (val.ToString().IsEqual(value))
                    });
                }
            }
            tempItems.Reverse();
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTypes(this HtmlHelper html, string name, int disciplineTask, Guid patientId, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var formGroup = task.GetFormGroup();
                if (formGroup.IsNotNullOrEmpty())
                {
                    var disciplineTaskArray = Enum.GetValues(typeof(DisciplineTasks));
                    var disciplineTasks = lookupRepository.DisciplineTasks() ?? new List<DisciplineTask>();
                    var agencyChargeRates = new List<ChargeRate>();
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                        if (location != null && location.IsLocationStandAlone)
                        {
                            if (location.BillData.IsNotNullOrEmpty())
                            {
                                agencyChargeRates = location.BillData.ToObject<List<ChargeRate>>();
                            }
                        }
                        else
                        {
                            var mainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                            if (mainLocation != null && mainLocation.BillData.IsNotNullOrEmpty())
                            {
                                agencyChargeRates = mainLocation.BillData.ToObject<List<ChargeRate>>();
                            }
                        }
                    }
                    var gcode = string.Empty;
                    if (agencyChargeRates != null && agencyChargeRates.Count > 0)
                    {
                        foreach (DisciplineTasks disciplineTaskItem in disciplineTaskArray)
                        {
                            if (formGroup.IsEqual(disciplineTaskItem.GetFormGroup()))
                            {
                                var rate = agencyChargeRates.SingleOrDefault(r => r.Id == (int)disciplineTaskItem);
                                if (rate != null && rate.Code.IsNotNullOrEmpty())
                                {
                                    gcode = rate.Code;
                                }
                                else
                                {
                                    var data = disciplineTasks.SingleOrDefault(d => d.Id == (int)disciplineTaskItem);
                                    if (data != null)
                                    {
                                        gcode = data.GCode;
                                    }
                                }
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", disciplineTaskItem.GetDescription(), gcode),
                                    Value = ((int)disciplineTaskItem).ToString(),
                                    Selected = ((int)disciplineTaskItem == disciplineTask ? true : false)
                                });
                            }
                        }
                    }
                }
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static void RenderXML(this HtmlHelper html, XmlDocument xmlDocument, string XSLTPath, Dictionary<string, string> xslArgParams)
        {
            ViewContext context = html.ViewContext;
            XsltArgumentList xslArgs = new XsltArgumentList();
            if (xslArgParams != null)
            {
                foreach (string key in xslArgParams.Keys)
                {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                }
            }
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(XSLTPath);
            t.Transform(xmlDocument, xslArgs, context.HttpContext.Response.Output);
        }

        public static MvcHtmlString ClaimTypes(this HtmlHelper html, string name, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array claimTypes = Enum.GetValues(typeof(ClaimType));
            foreach (ClaimType claimType in claimTypes)
            {
                items.Add(new SelectListItem
                {
                    Text = claimType.GetDescription(),
                    Value = claimType.ToString(),
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "All ",
                Selected=true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UB4PatientStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var ub4PatientStatusValues = Enum.GetValues(typeof(UB4PatientStatus));
            if (ub4PatientStatusValues != null && ub4PatientStatusValues.Length > 0)
            {
                foreach (UB4PatientStatus status in ub4PatientStatusValues)
                {
                        items.Add(new SelectListItem
                        {
                            Text = status.GetDescription(),
                            Value = ((int)status).ToString().PadLeft(2,'0'),
                            Selected = value == ((int)status).ToString().PadLeft(2, '0')
                        });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Patient Status --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BillType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billType = Enum.GetValues(typeof(BillType));
            if (billType != null && billType.Length > 0)
            {
                foreach (BillType bill in billType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = bill.GetDescription(),
                        Value = ((int)bill).ToString(),
                        Selected = value == ((int)bill).ToString()
                    });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Bill Type --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UnitType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billUnitType = Enum.GetValues(typeof(BillUnitType));
            if (billUnitType != null && billUnitType.Length > 0)
            {
                foreach (BillUnitType unit in billUnitType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = unit.GetDescription(),
                        Value = ((int)unit).ToString(),
                        Selected = value == ((int)unit).ToString()
                    });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Unit Type --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsuranceDisciplineTask(this HtmlHelper html, string name, int disciplineTask, int insuranceId, bool isTaskIncluded ,object htmlAttributes)
        {
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            var disciplines = new List<int>();
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                disciplines = insurance.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
            }
            var items = new List<SelectListItem>();

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString() ) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote )
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ClaimDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid claimId, bool isTaskIncluded, object htmlAttributes)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, claimId);
            var disciplines = new List<int>();
            if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            {
                AgencyInsurance insurance = claim.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
                {
                    disciplines = insurance.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
                }
            }
            var items = new List<SelectListItem>();

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString()) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote)
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LocationCostDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid locationId, bool isTaskIncluded, object htmlAttributes)
        {
            var location = agencyRepository.FindLocation( Current.AgencyId,locationId);
            var disciplines = new List<int>();
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                disciplines = location.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
            }
            var items = new List<SelectListItem>();

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString()) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote)
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UserDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid userId, string insurance, bool isTaskIncluded, object htmlAttributes)
        {
            var disciplines = new List<int>();
            var items = new List<SelectListItem>();

            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty() && insurance.IsNotNullOrEmpty())
            {
                disciplines = user.Rates.ToObject<List<UserRate>>().Where(r => r.Insurance == insurance).Select(i => i.Id).ToList();
            }

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString()) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote)
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesMedicareByBranch(this HtmlHelper html, string name, string value,Guid branchId, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(i =>
                {
                    if (i.PayorType == 2)
                    {
                        items.Add(new SelectListItem() { Text = i.Name, Value = i.Id.ToString(), Selected = (i.Id.ToString().IsEqual(value)) });
                    }
                });
            }
           
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesByBranch(this HtmlHelper html, string name, string value, Guid branchId, bool addNew, IDictionary<string, string> htmlAttributes)
        {
            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes.ToString(), "0", 0);
            if (addNew)
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
          

            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances = agencyInsurances.OrderBy(a => a.Name).ToList();
                agencyInsurances.ForEach(insurance =>
                {
                    selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", insurance.Id.ToString(), 1, ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name);
                });
            }
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString InsurancesByBranch(this HtmlHelper html, string name, string value, Guid branchId, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });

                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = standardInsurance.Name,
                                    Value = standardInsurance.Id.ToString(),
                                    Selected = (standardInsurance.Id.ToString().IsEqual(value))
                                });

                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });

                        }
                    }
                }
            }

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Insurance --",
                Value = "0"
            });

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BillStatus(this HtmlHelper html, string name, string value, bool isSelectIncluded ,object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billStatus = Enum.GetValues(typeof(BillingStatus));
            if (billStatus != null && billStatus.Length > 0)
            {
                foreach (BillingStatus status in billStatus)
                {
                    if (!items.Exists(i => i.Text.Equals(status.GetDescription())))
                    {
                        items.Add(new SelectListItem
                        {
                            Text = status.GetDescription(),
                            Value = ((int)status).ToString(),
                            Selected = value == ((int)status).ToString()
                        });
                    }
                }
            }
            if (isSelectIncluded)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = " -- Select Bill Status --",
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ManagedBillStatus(this HtmlHelper html, string name, string value, bool isSelectIncluded, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billStatus = Enum.GetValues(typeof(ManagedClaimStatus));
            if (billStatus != null && billStatus.Length > 0)
            {
                foreach (ManagedClaimStatus status in billStatus)
                {
                    items.Add(new SelectListItem
                    {
                        Text = status.GetDescription(),
                        Value = ((int)status).ToString(),
                        Selected = value == ((int)status).ToString()
                    });
                }
            }
            if (isSelectIncluded)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = " -- Select Bill Status --",
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TherapyAssistance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(TherapyAssistance));
            foreach (TherapyAssistance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Assistance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Sensory(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(Sensory));
            foreach (Sensory item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString StaticBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(StaticBalance));
            foreach (StaticBalance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Balance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DynamicBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(DynamicBalance));
            foreach (DynamicBalance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Balance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static string ProperCheckBox(this HtmlHelper html, string name, string key, IDictionary<string, NotesQuestion> data)
        {
            return string.Format("<input name='{0}_" + name + "' value='true' type='checkbox' {1} />", key, (data.ContainsKey(name) && data[name].Answer == "true").ToChecked());
        }

        public static MvcHtmlString UserRateTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var array = Enum.GetValues(typeof(UserRateTypes));
            if (array != null && array.Length > 0)
            {
                foreach (UserRateTypes item in array)
                {
                    if ((int)item != 0)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = item.GetDescription(),
                            Value = ((int)item).ToString(),
                            Selected = value == ((int)item).ToString()
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Rate Type --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TherapyAssistiveDevice(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(TherapyAssistiveDevices));
            foreach (TherapyAssistiveDevices item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Device --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString WeightBearingStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(WeightBearingStatus));
            foreach (WeightBearingStatus item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Status --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsuranceRelationships(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            //Since this feature was originally implemented using strings instead of an id, this will allow the program to convert the old values to their new ones
            if (value.IsNotNullOrEmpty())
            {
                if (value.Equals("Self"))
                {
                    value = "2";
                }
                else if (value.Equals("Spouse"))
                {
                    value = "1";
                }
                else if (value.Equals("Child"))
                {
                    value = "3";
                }
                else if (value.Equals("Other"))
                {
                    value = "9";
                }
            }
            else
            {
                value = "2";
            }
            var items = new List<SelectListItem>();
            IList<Relationship> list = lookupRepository.Relationships();
            foreach (Relationship item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Description,
                    Value = item.Id.ToString(),
                    Selected = value == item.Id.ToString()
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MedicationInformationLink(this HtmlHelper html, string drugId)
        {
            if (drugId.IsNotNullOrEmpty())
            {
                return html.ActionLink("Teaching Guide", "MedicationEducation", "Patient", new { DrugId = drugId }, new { @class = "teachingguide", target = "_blank" });
                //return MvcHtmlString.Create(string.Format("<a class='teachingguide' target='_blank' href='{0}'>Teaching Guide</a>", url));
            }
            else
            {
                return MvcHtmlString.Empty;
            }
        }

        public static MvcHtmlString RenderHtml(this HtmlHelper html, string serverPath)
        {
            
            //var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), serverPath);
            //var stream = File.OpenText(filePath);
            //if (serverPath.IsNotNullOrEmpty())
            //{
            //    var htmlResult = FileGenerator.RemoteDownload(AppSettings.LexiFileServerIP, AppSettings.LexiFileMainDirectory, serverPath);
            //    if (htmlResult.IsNotNullOrEmpty())
            //    {
            //        htmlResult = htmlResult.Replace("../styles", "../HTML/styles").Replace("../javascript", "../HTML/javascript")
            //            .Replace("../images", "../HTML/images").Replace("../symbols", "../HTML/symbols");
            //        return MvcHtmlString.Create(htmlResult);
            //    }
            //}
            return MvcHtmlString.Create("");
        }

        public static MvcHtmlString GeneralStatusDropDown(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { 
                Text = "Mild",
                Value = "1",
                Selected = "1".Equals(value)
            });
            items.Add(new SelectListItem()
            {
                Text = "Moderate",
                Value = "2",
                Selected = "2".Equals(value)
            });
            items.Add(new SelectListItem()
            {
                Text = "Severe",
                Value = "3",
                Selected = "3".Equals(value)
            });
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Level -- ",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AllDisciplineTaskList(this HtmlHelper html, string name, string value, string title, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == "Nursing" || d.Discipline == "PT" || d.Discipline == "ST" || d.Discipline == "OT" || d.Discipline == "HHA" || d.Discipline == "MSW" || d.Discipline == "Orders" || d.Discipline == "ReportsAndNotes")
                .OrderBy(d => d.Task)
                .ToList();


            tempItems = from task in tasks
                        select new SelectListItem
                        {
                            Text = task.Task,
                            Value = task.Id.ToString(),
                            Selected = (task.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = "0"
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AdjustmentCodes(this HtmlHelper helper, string name, string value, string title, object htmlAttributes)
        {
            var codes = agencyRepository.GetAdjustmentCodes(Current.AgencyId);
            IEnumerable<SelectListItem> tempItems = null;
            tempItems = from code in codes
                    select new SelectListItem
                    {
                        Text = code.Code + " - " + code.Description,
                        Value = code.Id.ToString(),
                        Selected = (code.Id.ToString().IsEqual(value))
                    };
            List<SelectListItem> items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = ""
            });
            return helper.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
        public static MvcHtmlString MartialStatus(this HtmlHelper html, string name, string value, bool isTitle, string title,string deafultValue, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(MartialStatus));
            foreach (MartialStatus item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = (item).ToString(),
                    Selected = value == (item).ToString()
                });

            }
            if (isTitle)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = deafultValue
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodesExceptCurrent(this HtmlHelper html, string name, string value, Guid patientId, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
            if (episodes != null && episodes.Count > 0)
            {
                foreach (var e in episodes)
                {
                    if (!e.Id.ToString().IsEqual(value))
                    {
                        items.Add(new SelectListItem
                        {
                            Text = string.Format("{0} - {1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")),
                            Value = e.Id.ToString()
                        });
                    }
                }
            }
            return html.DropDownList(name, items, htmlAttributes);
        }


        #endregion
    }
}
