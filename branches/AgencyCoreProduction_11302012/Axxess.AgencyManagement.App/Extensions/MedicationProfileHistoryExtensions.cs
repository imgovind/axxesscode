﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;

using Axxess.AgencyManagement.Domain;

    public static class MedicationProfileHistoryExtensions
    {
        public static MedicationProfile ToProfile(this MedicationProfileHistory medProfileHistory)
        {
            var medProfile = new MedicationProfile();

            if (medProfileHistory != null)
            {
                medProfile.AgencyId = medProfileHistory.AgencyId;
                medProfile.Id = medProfileHistory.Id;
                medProfile.Medication = medProfileHistory.Medication;
                medProfile.Allergies = medProfileHistory.Allergies;
                medProfile.Modified = medProfileHistory.Modified;
                medProfile.Created = medProfileHistory.Created;
                medProfile.PatientId = medProfileHistory.PatientId;
                medProfile.PharmacyName = medProfileHistory.PharmacyName;
                medProfile.PharmacyPhone = medProfileHistory.PharmacyPhone;
            }

            return medProfile;
        }
    }
}
