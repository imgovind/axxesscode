﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class AllergyProfilePdf : AxxessPdf {
        public AllergyProfilePdf(AllergyProfileViewData data) {
            this.SetType(PdfDocs.AllergyProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 3, 1 },true) };
            var allergies = data != null && data.AllergyProfile != null && data.AllergyProfile.Allergies.IsNotNullOrEmpty() ? data.AllergyProfile.Allergies.ToObject<List<Allergy>>().ToList() : new List<Allergy>();
            if (allergies.Count > 0) {
                foreach (var allergy in allergies) {
                    if (allergy != null && !allergy.IsDeprecated) {
                        AxxessCell name = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        name.AddElement(new Chunk(allergy.Name.IsNotNullOrEmpty() ? allergy.Name : string.Empty, fonts[0]));
                        type.AddElement(new Chunk(allergy.Type.IsNotNullOrEmpty() ? allergy.Type : string.Empty, fonts[0]));
                        content[0].AddCell(name);
                        content[0].AddCell(type);
                    }
                }
                this.SetContent(content);
            } else this.SetContent(new IElement[] { new Chunk("") });
            this.SetMargins(new float[] { 130, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}