﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    class MissedVisitPdf : AxxessPdf {
        public MissedVisitPdf(MissedVisit data) {
            this.SetType(PdfDocs.MissedVisit);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Comments.IsNotNullOrEmpty() ? data.Comments : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 335, 33, 105, 33 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("visittype", data != null && data.DisciplineTaskName.IsNotNullOrEmpty() ? data.DisciplineTaskName : string.Empty);
            fieldmap[0].Add("visitdate", data != null && data.EventDate.IsNotNullOrEmpty() ? data.EventDate : string.Empty);
            fieldmap[0].Add("ordergen", (data != null && data.Patient != null ? (data.IsOrderGenerated ? "Yes" : "No") : string.Empty));
            fieldmap[0].Add("physnotice", (data != null && data.Patient != null ? (data.IsPhysicianOfficeNotified ? "Yes" : "No") : string.Empty));
            fieldmap[0].Add("reason", data != null && data.Reason.IsNotNullOrEmpty() ? data.Reason : string.Empty);
            fieldmap[0].Add("clinsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("clinsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", (
                data != null && data.Patient != null ?
                    (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) +
                    (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) +
                    (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n")
                : string.Empty));
            fieldmap[1].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            this.SetFields(fieldmap);
        }
    }
}