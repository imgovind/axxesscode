﻿namespace Axxess.AgencyManagement.App.ViewData
{
   public class PatientScheduleViewData : PatientViewData
    {
       public EpisodeViewData Episode { get; set; }
    }
}
