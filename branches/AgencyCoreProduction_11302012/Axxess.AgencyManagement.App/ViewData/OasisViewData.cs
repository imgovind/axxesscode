﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Axxess.OasisC.Domain;
    using Axxess.Core.Infrastructure;

    public class OasisViewData : JsonViewData
    {
        public Guid assessmentId
        {
            get;
            set;
        }
        public IAssessment Assessment
        {
            get;
            set;
        }
    }
}