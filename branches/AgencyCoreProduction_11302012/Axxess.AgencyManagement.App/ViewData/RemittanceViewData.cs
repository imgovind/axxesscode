﻿
namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class RemittanceViewData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string RemitId { get; set; }

    }
}
