﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Repositories;

    public class ReferralViewData 
    { 
        public Guid Id { get;  set; }

        public Guid AgencyId { get; set; }

        public string ReferralDate { get; set; }

        public string ReferrerFirstName { get ;  set; }

        public string ReferrerLastName { get; set; }

        public string ReferralSource { get;  set; }

        public string FirstName { get;  set; }

        public string LastName { get;  set; }

        public string MedicareNo { get;  set; }

        public string MedicaidNo { get;  set; }

        public string SSN { get; set; }

        public string DateOfBirth { get; set; }

        public string Gender { get;  set; }

        public string HomePhone { get; set; }

        public  string []  HomePhoneArray { get;  set; }

        public string Email { get; set; }

        public string AddressLine1 { get;  set; }

        public string AddressLine2 { get;  set; }

        public string AddressCity { get;  set; }

        public string AddressStateCode { get;  set; }

        public string AddressZipCode { get;  set; }        

        public string ServicesRequired { get;  set; }

        public string[] ServicesRequiredCollection { get; set; }

        public string DME{ get; set; }

        public string[] DMECollection { get; set; }

        public string OtherDME { get;  set; }

        public string PhysicianFirstName { get;  set; }

        public string PhysicianLastName { get;  set; }

        public string PhysicianNPI { get;  set; }

        public string PhysicianPhone { get;  set; }

        public string[] PhysicianPhoneArray { get; set; }

        public string PhysicianFax { get;  set; }

        public string PhysicianEmail { get;  set; }

        public Guid AssignedTo { get;  set; }

        public DateTime Created { get;  set; }

        public DateTime Modified { get;  set; }

        public string Status { get; set; }

        public DateTime VerificationDate { get; set; }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
    }
}
