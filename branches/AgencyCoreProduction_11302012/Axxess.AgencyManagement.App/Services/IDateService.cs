﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;

    public interface IDateService
    {
        DateRange GetDateRange(string rangeIdentifier,Guid patientId);
    }
}
