﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Extensions;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;

    using Enums;
    using Domain;
    using Common;
    using ViewData;
    using Axxess.OasisC.Domain;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentService assessmentService;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAssessmentService assessmentService, ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.assessmentService = assessmentService;
        }

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    if (zipCode != null)
                    {
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    }
                    var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                    if (defaultDisciplineTasks != null)
                    {
                        var costRates = new List<ChargeRate>();
                        defaultDisciplineTasks.ForEach(r =>
                        {
                            if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
                            {
                                costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
                            }
                        });
                        location.BillData = costRates.ToXml();
                    }
                    //location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
                    location.Id = Guid.NewGuid();
                    if (agencyRepository.AddLocation(location))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AllowWeekendAccess = true,
                            AgencyName = agency.Name,
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public List<UserVisit> GetSchedule()
        {
            var schedule = new List<UserVisit>();
            var userEvents = userRepository.GetSchedules(Current.AgencyId);
            if (userEvents != null && userEvents.Count > 0)
            {
                userEvents.ForEach(ue =>
                {
                    var user = userRepository.Get(ue.UserId, Current.AgencyId);
                    if (user != null && !ue.PatientId.IsEmpty())
                    {
                        var patient = patientRepository.GetPatientOnly(ue.PatientId, Current.AgencyId);
                        if (patient != null && !ue.EpisodeId.IsEmpty())
                        {
                            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, ue.EpisodeId, patient.Id);
                            if (episode != null && episode.IsActive == true && episode.IsDischarged == false)
                            {
                                var visitNote = string.Empty;
                                if (episode.Schedule.IsNotNullOrEmpty())
                                {
                                    var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                    if (scheduleList != null && scheduleList.Count > 0)
                                    {
                                        var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
                                        if (scheduledEvent != null && scheduledEvent.Comments.IsNotNullOrEmpty())
                                        {
                                            visitNote = scheduledEvent.Comments;
                                        }

                                    }
                                }
                                schedule.Add(new UserVisit
                                {
                                    Id = ue.EventId,
                                    UserId = ue.UserId,
                                    EpisodeId = episode.Id,
                                    PatientId = patient.Id,
                                    VisitDate = ue.EventDate.ToZeroFilled(),
                                    EpisodeNotes = episode.Detail.Comments,
                                    VisitNotes = visitNote,
                                    PatientName = patient.DisplayName,
                                    UserDisplayName = user.DisplayName,
                                    StatusName = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), ue.Status)).GetDescription(),
                                    TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
                                });
                            }
                        }
                    }
                });
            }
            return schedule;
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var events = new List<PatientEpisodeEvent>();
            var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, branchId, status, startDate, endDate);
            if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
            {
                var episodeWithEventsDictionary = agencyPatientEpisodes.ToDictionary(g => g.Id,
                   g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                         !s.IsDeprecated
                         && s.EventDate.IsValidDate()
                         && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
                          && s.EventDate.ToDateTime().IsBetween(startDate, endDate)
                         && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
                                       || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                                       || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
                                       || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
                                       || s.IsMissedVisit)
                     ).ToList() : new List<ScheduleEvent>()
                    );
                if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
                {
                    var episodeIds = episodeWithEventsDictionary.Keys.ToList();
                    var userIds = new List<Guid>();
                    var users = new List<User>();
                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                    if (returnComments != null && returnComments.Count > 0)
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds != null && returnUserIds.Count > 0)
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }
                    var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                    {
                        userIds.AddRange(scheduleUserIds);

                    }
                    if (userIds != null && userIds.Count > 0)
                    {
                        users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                    }
                    var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

                    episodeWithEventsDictionary.ForEach((key, value) =>
                    {
                        if (value != null && value.Count > 0)
                        {
                            var episode = agencyPatientEpisodes.FirstOrDefault(e => e.Id == key);
                            if (episode != null)
                            {
                                value.ForEach(s =>
                                {
                                    var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
                                    var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

                                    var userName = string.Empty;
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            userName = user.DisplayName;
                                        }
                                    }
                                    if (s.IsMissedVisit)
                                    {
                                        var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                                        if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
                                        {
                                            var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                            Common.Url.Set(s, false, false);
                                            events.Add(new PatientEpisodeEvent
                                            {
                                                EventId = s.EventId,
                                                EpisodeId = s.EpisodeId,
                                                PatientId = s.PatientId,
                                                PrintUrl = s.PrintUrl,
                                                Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
                                                PatientName = episode.DisplayName,
                                                TaskName = s.DisciplineTaskName,
                                                UserName = userName,
                                                YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                                RedNote = redNote,
                                                BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                                EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                                            });
                                        }
                                    }
                                    else
                                    {
                                        if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
                                            || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                                            || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
                                            || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
                                        {
                                            var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                            Common.Url.Set(s, false, false);
                                            events.Add(new PatientEpisodeEvent
                                            {
                                                EventId = s.EventId,
                                                EpisodeId = s.EpisodeId,
                                                PatientId = s.PatientId,
                                                PrintUrl = s.PrintUrl,
                                                Status = s.StatusName,
                                                PatientName = episode.DisplayName,
                                                TaskName = s.DisciplineTaskName,
                                                UserName = userName,
                                                YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                                RedNote = redNote,
                                                BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                                EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }


        public List<PatientEpisodeEvent> GetPatientCaseManagerSchedule(Guid patientId)
        {
            var events = new List<PatientEpisodeEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, patientId);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                var users = new List<User>();
                patientEpisodes.ForEach(e =>
                {
                    if (e.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
                                        || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                                        || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
                                        || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
                                        || s.Status == ((int)ScheduleStatus.NoteMissedVisitPending).ToString()));
                        if (scheduledEvents != null && scheduledEvents.Count() > 0)
                        {
                            var scheduleUserIds = scheduledEvents.Where(s => !s.UserId.IsEmpty() && !users.Exists(us => us.Id == s.UserId)).Select(s => s.UserId).Distinct().ToList();
                            if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                            {
                                var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, scheduleUserIds) ?? new List<User>();
                                if (scheduleUsers != null && scheduleUsers.Count > 0)
                                {
                                    users.AddRange(scheduleUsers);
                                }
                            }
                            var eventIds = scheduledEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

                            scheduledEvents.ForEach(s =>
                            {
                                var userName = string.Empty;
                                if (!s.UserId.IsEmpty())
                                {
                                    var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                    if (user != null)
                                    {
                                        userName = user.DisplayName;
                                    }
                                }
                                if (s.IsMissedVisit)
                                {
                                    var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId);//patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                                    if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
                                    {
                                        var details = e.Details.IsNotNullOrEmpty() ? e.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                        Common.Url.Set(s, false, false);
                                        events.Add(new PatientEpisodeEvent
                                        {
                                            PrintUrl = s.PrintUrl,
                                            Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
                                            PatientName = e.PatientName,
                                            TaskName = s.DisciplineTaskName,
                                            UserName = userName,
                                            EpisodeRange = string.Format("{0} - {1}", e.StartDate, e.EndDate),
                                            YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                            RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : string.Empty,
                                            BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                            EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                            CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                                        });
                                    }
                                }
                                else
                                {
                                    if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
                                        || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                                        || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
                                        || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
                                    {
                                        var details = e.Details.IsNotNullOrEmpty() ? e.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                        Common.Url.Set(s, false, false);
                                        events.Add(new PatientEpisodeEvent
                                        {
                                            PrintUrl = s.PrintUrl,
                                            Status = s.StatusName,
                                            PatientName = e.PatientName,
                                            TaskName = s.DisciplineTaskName,
                                            UserName = userName,
                                            EpisodeRange = string.Format("{0} - {1}", e.StartDate, e.EndDate),
                                            YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                            RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : string.Empty,
                                            BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                            EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                            CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                                        });
                                    }
                                }
                            });
                        }
                    }
                    e.Schedule = string.Empty;
                });
            }
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        public List<RecertEvent> GetRecertsPastDue()
        {
            var pastDueRecerts = new List<RecertEvent>();
            var recertEpisodes = patientRepository.GetPastDueRecertsLean(Current.AgencyId);
            if (recertEpisodes != null && recertEpisodes.Count > 0)
            {
                recertEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && 
                                ((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
                                || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
                                && (s.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || s.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisSaved).ToString())).ToList();
                            if (dischargeSchedules != null && dischargeSchedules.Count > 0)
                            {
                            }
                            else
                            {
                                var recertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                if (recertSchedule != null)
                                {
                                    if ((recertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                                    {

                                    }
                                    else
                                    {
                                        if (!recertSchedule.UserId.IsEmpty())
                                        {
                                            r.AssignedTo = UserEngine.GetName(recertSchedule.UserId, Current.AgencyId);
                                        }
                                        r.Status = recertSchedule.StatusName;
                                        r.Schedule = string.Empty;
                                        pastDueRecerts.Add(r);

                                    }
                                }
                                else
                                {
                                    r.AssignedTo = "Unassigned";
                                    r.Status = "Not Scheduled";
                                    r.Schedule = string.Empty;
                                    pastDueRecerts.Add(r);
                                }
                            }
                        }
                        else
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            pastDueRecerts.Add(r);
                        }

                    }
                    else
                    {
                        r.AssignedTo = "Unassigned";
                        r.Status = "Not Scheduled";
                        r.Schedule = string.Empty;
                        pastDueRecerts.Add(r);
                    }
                });
            }

            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var recertEpisodes = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate.AddDays(5));
            var pastDueRecerts = new List<RecertEvent>();
            if (recertEpisodes != null && recertEpisodes.Count > 0)
            {
                recertEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) &&
                                ((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || 
                                s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
                                || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
                                && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || 
                                s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                            if (dischargeSchedules != null && dischargeSchedules.Count > 0)
                            {
                            }
                            else
                            {
                                var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                if (episodeRecertSchedule != null)
                                {
                                    if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                                    {
                                    }
                                    else
                                    {
                                        if (episodeRecertSchedule.EventDate.ToDateTime().Date < endDate.Date)
                                        {
                                            if (!episodeRecertSchedule.UserId.IsEmpty())
                                            {
                                                r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
                                            }
                                            r.Status = episodeRecertSchedule.StatusName;
                                            r.Schedule = string.Empty;
                                            r.DateDifference = endDate.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
                                            pastDueRecerts.Add(r);
                                        }
                                    }
                                }
                                else
                                {
                                    if (r.TargetDate.Date < endDate.Date)
                                    {
                                        r.AssignedTo = "Unassigned";
                                        r.Status = "Not Scheduled";
                                        r.Schedule = string.Empty;
                                        r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                        pastDueRecerts.Add(r);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (r.TargetDate.Date < endDate.Date)
                            {
                                r.AssignedTo = "Unassigned";
                                r.Status = "Not Scheduled";
                                r.Schedule = string.Empty;
                                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                pastDueRecerts.Add(r);
                            }
                        }
                    }
                    else
                    {
                        if (r.TargetDate.Date < endDate.Date)
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                            pastDueRecerts.Add(r);
                        }
                    }
                });
            }
            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            //return patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            var pastDueRecerts = new List<RecertEvent>();
            var recertEpisodes = patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            if (recertEpisodes != null && recertEpisodes.Count > 0)
            {
                recertEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && 
                                (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || 
                                 s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT ||  s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge ||
                                s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || 
                                s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || 
                                s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || 
                                s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                            if (dischargeSchedules != null && dischargeSchedules.Count > 0)
                            {
                            }
                            else
                            {
                                var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                if (episodeRecertSchedule != null)
                                {
                                    if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                                    {
                                    }
                                    else
                                    {
                                        if (episodeRecertSchedule.EventDate.ToDateTime().Date < DateTime.Today.Date)
                                        {
                                            if (!episodeRecertSchedule.UserId.IsEmpty())
                                            {
                                                r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
                                            }
                                            r.Status = episodeRecertSchedule.StatusName;
                                            r.Schedule = string.Empty;
                                            r.DateDifference = DateTime.Today.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
                                            pastDueRecerts.Add(r);
                                        }
                                    }
                                }
                                else
                                {
                                    if (r.TargetDate.Date < DateTime.Today.Date)
                                    {
                                        r.AssignedTo = "Unassigned";
                                        r.Status = "Not Scheduled";
                                        r.Schedule = string.Empty;
                                        r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                        pastDueRecerts.Add(r);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (r.TargetDate.Date < DateTime.Today.Date)
                            {
                                r.AssignedTo = "Unassigned";
                                r.Status = "Not Scheduled";
                                r.Schedule = string.Empty;
                                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                pastDueRecerts.Add(r);
                            }
                        }
                    }
                    else
                    {
                        if (r.TargetDate.Date < DateTime.Today.Date)
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                            pastDueRecerts.Add(r);
                        }
                    }
                });
            }
            return pastDueRecerts.Take(5).ToList();
        }

        public List<RecertEvent> GetRecertsUpcoming()
        {
            var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId);
            var upcomingRecets = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
                        if (schedule != null)
                        {
                            if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                            {
                            }
                            else
                            {
                                if (!schedule.UserId.IsEmpty())
                                {
                                    r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
                                }
                                r.Status = schedule.StatusName;
                                r.Schedule = string.Empty;
                                upcomingRecets.Add(r);
                            }
                        }
                        else
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            upcomingRecets.Add(r);
                        }
                    }
                    else
                    {
                        r.AssignedTo = "Unassigned";
                        r.Status = "Not Scheduled";
                        r.Schedule = string.Empty;
                        upcomingRecets.Add(r);
                    }
                });
            }
            return upcomingRecets;
        }

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            var upcomingRecets = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    //var lastFiveDaysStart=r.TargetDate.AddDays(-6);
                    //if ((r.TargetDate.AddDays(-1).Date <= endDate && r.TargetDate.AddDays(-1).Date >= startDate) || (lastFiveDaysStart.Date >= startDate.Date && lastFiveDaysStart.Date <= endDate.Date))
                    //{
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
                        if (schedule != null)
                        {
                            if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                            {
                            }
                            else
                            {
                                if (schedule.EventDate.ToDateTime().Date >= startDate.Date && schedule.EventDate.ToDateTime().Date <= endDate.Date)
                                {
                                    if (!schedule.UserId.IsEmpty())
                                    {
                                        r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
                                    }
                                    r.Status = schedule.StatusName;
                                    r.Schedule = string.Empty;
                                    r.ScheduledDate = schedule.EventDate.ToDateTime();
                                    r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
                                    upcomingRecets.Add(r);
                                }
                            }
                        }
                        else
                        {
                            if (r.TargetDate.AddDays(-1).Date < endDate.Date)
                            {
                                r.AssignedTo = "Unassigned";
                                r.Status = "Not Scheduled";
                                r.Schedule = string.Empty;
                                r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
                                upcomingRecets.Add(r);
                            }
                        }
                    }
                    else
                    {
                        if (r.TargetDate.AddDays(-1).Date < endDate.Date)
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
                            upcomingRecets.Add(r);
                        }
                    }
                    //}
                });
            }
            return upcomingRecets;
        }

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return patientRepository.GetUpcomingRecertsWidgetLean(Current.AgencyId);
        }

        public List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            return insuranceList;
        }

        public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId = po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                CreatedDate = po.OrderDateFormatted

                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!ffe.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (ffe.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                                });
                            }
                        });
                    }
                }
                var evalOrdersSchedule = schedules.Where(s =>
                        (s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary
                        || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment) && s.Status.IsEqual(status[1].ToString())).ToList();
                
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                EpisodeId = eval.EpisodeId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                SendDate = eval.SentDate
                            });
                        });
                    }
                }
            }
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType=OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        task = DisciplineTasks.MSWEvaluationAssessment;
                        break;
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        task = DisciplineTasks.SixtyDaySummary;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0 && status.Count > 1)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId = po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = po.PhysicianName.IsNotNullOrEmpty() ? po.PhysicianName : physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
                                SendDate = po.SentDate,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate,
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);
                            
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                                    SendDate = ffe.RequestDate,
                                    PhysicianSignatureDate = ffe.SignatureDate
                                });
                            }
                        });
                    }
                }

                var evalOrdersSchedule = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        ||s.DisciplineTask==(int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.FirstOrDefault(s => s.EventId == eval.Id && s.EpisodeId==eval.EpisodeId);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    EpisodeId = eval.EpisodeId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                    SendDate = eval.SentDate,
                                    PhysicianSignatureDate = eval.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public Order GetOrder(Guid id, Guid patientId,Guid episodeId, string type)
        {
            var order = new Order();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
            {
                var typeEnum = ((OrderType)type.ToInteger()).ToString();
                if (typeEnum.IsNotNullOrEmpty())
                {
                    switch (typeEnum)
                    {
                        case "PhysicianOrder":
                            var physicianOrder = patientRepository.GetOrder(id, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                order = new Order
                                {
                                    Id = physicianOrder.Id,
                                    PatientId = physicianOrder.PatientId,
                                    EpisodeId = physicianOrder.EpisodeId,
                                    Type = OrderType.PhysicianOrder,
                                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                    Number = physicianOrder.OrderNumber,
                                    PatientName = physicianOrder.DisplayName,
                                    PhysicianName = physicianOrder.PhysicianName,
                                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                                    SendDate = physicianOrder.SentDate,
                                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "FaceToFaceEncounter":
                            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                            if (faceToFaceEncounter != null)
                            {
                                order = new Order
                                {
                                    Id = faceToFaceEncounter.Id,
                                    PatientId = faceToFaceEncounter.PatientId,
                                    EpisodeId = faceToFaceEncounter.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = faceToFaceEncounter.OrderNumber,
                                    PatientName = faceToFaceEncounter.DisplayName,
                                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                                    SendDate = faceToFaceEncounter.RequestDate,
                                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                                };
                            }
                            break;
                        case "HCFA485":
                        case "NonOasisHCFA485":
                            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                            if (planofCare != null)
                            {
                                order = new Order
                                {
                                    Id = planofCare.Id,
                                    PatientId = planofCare.PatientId,
                                    EpisodeId = planofCare.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
                                    Number = planofCare.OrderNumber,
                                    PatientName = planofCare.PatientName,
                                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                                    SendDate = planofCare.SentDate,
                                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "HCFA485StandAlone":
                            var planofCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                            if (planofCareStandAlone != null)
                            {
                                order = new Order
                                {
                                    Id = planofCareStandAlone.Id,
                                    PatientId = planofCareStandAlone.PatientId,
                                    EpisodeId = planofCareStandAlone.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = planofCareStandAlone.OrderNumber,
                                    PatientName = planofCareStandAlone.PatientName,
                                    ReceivedDate = planofCareStandAlone.ReceivedDate > DateTime.MinValue ? planofCareStandAlone.ReceivedDate : planofCareStandAlone.SentDate,
                                    SendDate = planofCareStandAlone.SentDate,
                                    PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtEvaluation":
                            var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptEval.Id,
                                    PatientId = ptEval.PatientId,
                                    EpisodeId = ptEval.EpisodeId,
                                    Type = OrderType.PtEvaluation,
                                    Text = DisciplineTasks.PTEvaluation.GetDescription(),
                                    Number = ptEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
                                    SendDate = ptEval.SentDate,
                                    PhysicianSignatureDate = ptEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtReEvaluation":
                            var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptReEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptReEval.Id,
                                    PatientId = ptReEval.PatientId,
                                    EpisodeId = ptReEval.EpisodeId,
                                    Type = OrderType.PtReEvaluation,
                                    Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                                    Number = ptReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
                                    SendDate = ptReEval.SentDate,
                                    PhysicianSignatureDate = ptReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtEvaluation":
                            var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otEval != null)
                            {
                                order = new Order
                                {
                                    Id = otEval.Id,
                                    PatientId = otEval.PatientId,
                                    EpisodeId = otEval.EpisodeId,
                                    Type = OrderType.OtEvaluation,
                                    Text = DisciplineTasks.OTEvaluation.GetDescription(),
                                    Number = otEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
                                    SendDate = otEval.SentDate,
                                    PhysicianSignatureDate = otEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtReEvaluation":
                            var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otReEval != null)
                            {
                                order = new Order
                                {
                                    Id = otReEval.Id,
                                    PatientId = otReEval.PatientId,
                                    EpisodeId = otReEval.EpisodeId,
                                    Type = OrderType.OtReEvaluation,
                                    Text = DisciplineTasks.OTReEvaluation.GetDescription(),
                                    Number = otReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
                                    SendDate = otReEval.SentDate,
                                    PhysicianSignatureDate = otReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StEvaluation":
                            var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stEval != null)
                            {
                                order = new Order
                                {
                                    Id = stEval.Id,
                                    PatientId = stEval.PatientId,
                                    EpisodeId = stEval.EpisodeId,
                                    Type = OrderType.StEvaluation,
                                    Text = DisciplineTasks.STEvaluation.GetDescription(),
                                    Number = stEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
                                    SendDate = stEval.SentDate,
                                    PhysicianSignatureDate = stEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StReEvaluation":
                            var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stReEval != null)
                            {
                                order = new Order
                                {
                                    Id = stReEval.Id,
                                    PatientId = stReEval.PatientId,
                                    EpisodeId = stReEval.EpisodeId,
                                    Type = OrderType.StReEvaluation,
                                    Text = DisciplineTasks.STReEvaluation.GetDescription(),
                                    Number = stReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
                                    SendDate = stReEval.SentDate,
                                    PhysicianSignatureDate = stReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PTDischarge":
                            var ptDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = ptDischarge.Id,
                                    PatientId = ptDischarge.PatientId,
                                    EpisodeId = ptDischarge.EpisodeId,
                                    Type = OrderType.PTDischarge,
                                    Text = DisciplineTasks.PTDischarge.GetDescription(),
                                    Number = ptDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptDischarge.ReceivedDate > DateTime.MinValue ? ptDischarge.ReceivedDate : ptDischarge.SentDate,
                                    SendDate = ptDischarge.SentDate,
                                    PhysicianSignatureDate = ptDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "SixtyDaySummary":
                            var sixtyDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (sixtyDaySummary != null)
                            {
                                order = new Order
                                {
                                    Id = sixtyDaySummary.Id,
                                    PatientId = sixtyDaySummary.PatientId,
                                    EpisodeId = sixtyDaySummary.EpisodeId,
                                    Type = OrderType.SixtyDaySummary,
                                    Text = DisciplineTasks.SixtyDaySummary.GetDescription(),
                                    Number = sixtyDaySummary.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = sixtyDaySummary.ReceivedDate > DateTime.MinValue ? sixtyDaySummary.ReceivedDate : sixtyDaySummary.SentDate,
                                    SendDate = sixtyDaySummary.SentDate,
                                    PhysicianSignatureDate = sixtyDaySummary.PhysicianSignatureDate
                                };
                            }
                            break;
                    }
                }
            }

            return order;
        }

        public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                EpisodeId=po.EpisodeId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = po.OrderDateFormatted,
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }

                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    EpisodeId = poc.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    EpisodeId = ffe.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    SentDate = ffe.SentDate.ToString("MM/dd/yyyy"),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = DateTime.Today,
                                    PhysicianSignatureDate = ffe.SignatureDate

                                });
                            });
                    }
                }

                var evalSchedules = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
                        || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();

                if (evalSchedules != null && evalSchedules.Count > 0)
                {
                    var evalIds = evalSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int>() { (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically }, evalIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                EpisodeId = eval.EpisodeId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = eval.PhysicianSignatureDate
                            });
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;

            var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
            formCollection.Remove("SendAutomatically");

            var status = (int)ScheduleStatus.OrderSentToPhysician;
            var physician = new AgencyPhysician();

            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                        if(order.PhysicianData.IsNotNullOrEmpty()) physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (order != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, order.EpisodeId, order.PatientId, order.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (!patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                        if (scheduledEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), DisciplineTasks.PhysicianOrder, string.Empty);
                                        }
                                    }
                                }
                            }

                        }

                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId,episodeId,patientId, pocId);
                        if(planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {

                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = (status).ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (!planofCareRepository.Update(planofCare))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                        if (scheduledEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocOrderId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.GetStandAlone(Current.AgencyId,episodeId,patientId, pocOrderId);
                        if(planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {

                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (!planofCareRepository.UpdateStandAlone(planofCare))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                        if (scheduledEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }

                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var facetofaceId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                        if(faceToFaceEncounter.PhysicianData.IsNotNullOrEmpty()) physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (faceToFaceEncounter != null)
                        {

                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (!patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                        if (scheduledEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation
                    || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation
                    || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation
                    || key.ToInteger() == (int)OrderType.MSWEvaluation) || key.ToInteger() == (int)OrderType.PTDischarge
                    || key.ToInteger() == (int)OrderType.SixtyDaySummary)
                {
                    status = (int)ScheduleStatus.EvalSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var evalId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                        if(evalOrder.PhysicianData.IsNotNullOrEmpty()) physician = evalOrder.PhysicianData.ToObject<AgencyPhysician>();
                        if (evalOrder != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, evalOrder.EpisodeId, evalOrder.PatientId, evalOrder.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    evalOrder.Status = status;
                                    evalOrder.SentDate = DateTime.Now;
                                    if (!patientRepository.UpdateVisitNote(evalOrder))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                        if (scheduledEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                }
                            }

                        }
                    });
                }
                if (result && physician!=null)
                {
                    string subject = string.Format("{0} has sent you an order", Current.AgencyName);
                    string lastName = physician.LastName.Trim().Substring(0, 1).ToUpper() + physician.LastName.Trim().Substring(1).ToLower();
                    string bodyText = MessageBuilder.PrepareTextFrom("PhysicianOrderNotification", "recipientlastname", lastName, "senderfullname", Current.AgencyName);
                    if (physician.EmailAddress.IsNotNullOrEmpty() && physician.EmailAddress.Trim().IsNotNullOrEmpty())
                    {
                        Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    }
                }
            }

            return result;
        }

        public void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            if (!physicianSignatureDate.IsValid())
            {
                physicianSignatureDate = DateTime.Today;
            }
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, order.EpisodeId, order.PatientId, order.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if(!order.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            order.ReceivedDate = receivedDate;
                            order.PhysicianSignatureDate = physicianSignatureDate;
                            patientRepository.UpdateOrderModel(order);
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                if (planofCare != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!planofCare.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            planofCare.ReceivedDate = receivedDate;
                            planofCare.PhysicianSignatureDate = physicianSignatureDate;
                            planofCareRepository.Update(planofCare);
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                if (pocStandAlone != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, pocStandAlone.EpisodeId, pocStandAlone.PatientId, pocStandAlone.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!pocStandAlone.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            pocStandAlone.ReceivedDate = receivedDate;
                            pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                            planofCareRepository.UpdateStandAlone(pocStandAlone);
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }

            if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText  = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            faceToFaceEncounter.ReceivedDate = receivedDate;
                            faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                            patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }
            if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
                || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
                || type == OrderType.SixtyDaySummary)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, eval.EpisodeId, eval.PatientId, eval.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                            else if (!eval.PhysicianId.IsEmpty())
                            {
                                physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                                }
                            }
                            eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                            eval.ReceivedDate = receivedDate;
                            eval.PhysicianSignatureDate = physicianSignatureDate;
                            patientRepository.UpdateVisitNote(eval);
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }
        }

        public bool UpdateOrderDates(Guid id, Guid patientId,Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    order.ReceivedDate = receivedDate;
                    order.SentDate = sendDate;
                    order.PhysicianSignatureDate = physicianSignatureDate;
                    if (order.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!order.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateOrderModel(order);
                }
            }
            else if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, id);
                if (planofCare != null)
                {
                    planofCare.ReceivedDate = receivedDate;
                    planofCare.SentDate = sendDate;
                    planofCare.PhysicianSignatureDate = physicianSignatureDate;
                    if (planofCare.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!planofCare.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.Update(planofCare);
                }
            }
            else if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, id);
                if (pocStandAlone != null)
                {
                    pocStandAlone.ReceivedDate = receivedDate;
                    pocStandAlone.SentDate = sendDate;
                    pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                    if (pocStandAlone.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!pocStandAlone.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.UpdateStandAlone(pocStandAlone);
                }
            }
            else if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    faceToFaceEncounter.RequestDate = sendDate;
                    faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                    if (faceToFaceEncounter.SignatureText.IsNullOrEmpty())
                    {
                        var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
                || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
                || type == OrderType.SixtyDaySummary)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    eval.ReceivedDate = receivedDate;
                    eval.SentDate = sendDate;
                    eval.PhysicianSignatureDate = physicianSignatureDate;
                    if (eval.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateVisitNote(eval);
                }
            }
            return result;
        }

        public List<AddressViewData> GetAgencyFullAddress()
        {
            var listOfAddress = agencyRepository.GetBranches(Current.AgencyId);
            var address = new List<AddressViewData>();
            if (listOfAddress != null && listOfAddress.Count > 0)
            {
                listOfAddress.ForEach(l =>
                {
                    address.Add(new AddressViewData { Name = l.Name, FullAddress = string.Format("{0} {1}, {2}, {3} {4}", l.AddressLine1, l.AddressLine2, l.AddressCity, l.AddressStateCode, l.AddressZipCode) });
                });
            }
            return address;
        }

        public List<Infection> GetInfections(Guid agencyId)
        {
            var infections = agencyRepository.GetInfections(Current.AgencyId).ToList();
            if (infections != null && infections.Count > 0)
            {
                infections.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, i.EpisodeId, i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return infections;
        }

        public List<Incident> GetIncidents(Guid agencyId)
        {
            var incidents = agencyRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents != null && incidents.Count > 0)
            {
                incidents.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }

                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, i.EpisodeId, i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return incidents;
        }

        public bool ProcessInfections(string button, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                if (infection != null)
                {
                    if (!infection.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, infection.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null) userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                    }
                    var description = string.Empty;
                    if (button == "Approve")
                    {
                        infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                        infection.Modified = DateTime.Now;
                        if (agencyRepository.UpdateInfectionModal(infection))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                        infection.Modified = DateTime.Now;
                        infection.SignatureText = string.Empty;
                        infection.SignatureDate = DateTime.MinValue;
                        if (agencyRepository.UpdateInfectionModal(infection))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                shouldUpdateEpisode = true;
                                description = "Returned By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessIncidents(string button, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                if (incident != null)
                {
                    if (!incident.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, incident.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null) userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                    }
                    var description = string.Empty;
                    var action = new Actions();
                    if (button == "Approve")
                    {
                        incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                        incident.Modified = DateTime.Now;
                        if (agencyRepository.UpdateIncidentModal(incident))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                shouldUpdateEpisode = true;
                                description = "Approved by " + Current.UserFullName;
                                action = Actions.Approved;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                        incident.Modified = DateTime.Now;
                        incident.SignatureText = string.Empty;
                        incident.SignatureDate = DateTime.MinValue;
                        if (agencyRepository.UpdateIncidentModal(incident))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                shouldUpdateEpisode = true;
                                description = "Returned by " + Current.UserFullName;
                                action = Actions.Returned;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                        }
                    }
                }
            }
            return result;
        }

        public List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (IsMedicareTradIncluded)
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });
                        }
                    }
                }
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "All",
                    Value = "0"
                });
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return items;
        }

        public List<SelectListItem> Branchs(string value, bool IsAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Branches --",
                    Value = Guid.Empty.ToString(),
                });
            }
            return items;
        }

        public Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
            if (infection != null)
            {
                infection.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                infection.Patient = patientRepository.GetPatientOnly(infection.PatientId, Current.AgencyId);

                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                        Assessment assessment = assessmentService.GetEpisodeAssessment(episode);
                        infection.Diagnosis = new Dictionary<string, string>();
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                            infection.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "");
                            infection.Diagnosis.Add("ICD9M", oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "");
                            infection.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "");
                            infection.Diagnosis.Add("ICD9M2", oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "");
                        }
                    }
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(infection.PhysicianId, Current.AgencyId);
                    if (physician != null) infection.PhysicianName = physician.DisplayName;
                }
            }
            return infection;
        }

        public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
            if (incident != null)
            {
                incident.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                incident.Patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);

                incident.PatientName =incident.Patient.LastName+", "+ incident.Patient.FirstName;
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!incident.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(incident.PhysicianId, Current.AgencyId);
                    if (physician != null) incident.PhysicianName = physician.DisplayName;
                }
            }
            return incident;
        }

        public List<PatientEpisodeEvent> GetPrintQueue()
        {
            var events = new List<PatientEpisodeEvent>();
            var agencyPatientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId);
            agencyPatientEpisodes.ForEach(e =>
            {
                if (e.Schedule.IsNotNullOrEmpty())
                {
                    var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.IsDeprecated == false && s.IsMissedVisit == false && s.InPrintQueue == true);
                    if (scheduledEvents != null)
                    {
                        scheduledEvents.ForEach(s =>
                        {
                            events.Add(new PatientEpisodeEvent
                            {
                                Status = s.StatusName,
                                PatientName = e.PatientName,
                                TaskName = s.DisciplineTaskName,
                                PrintUrl = Url.Download(s, false),
                                UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                            });
                        });
                    }
                }
                e.Schedule = string.Empty;
            });
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        public List<PatientEpisodeEvent> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var events = new List<PatientEpisodeEvent>();
            List<PatientEpisodeData> agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, BranchId, StartDate, EndDate);
            
            agencyPatientEpisodes.ForEach(e =>
            {
                if (e.Schedule.IsNotNullOrEmpty())
                {
                    var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime()>=StartDate && s.EventDate.ToDateTime()<=EndDate && s.IsDeprecated == false && s.IsMissedVisit == false && s.InPrintQueue == true);
                    if (scheduledEvents != null)
                    {
                        scheduledEvents.ForEach(s =>
                        {
                            events.Add(new PatientEpisodeEvent
                            {
                                Status = s.StatusName,
                                PatientName = e.PatientName,
                                TaskName = s.DisciplineTaskName,
                                PrintUrl = Url.Download(s, true),
                                UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                            });
                        });
                    }
                }
                e.Schedule = string.Empty;
            });
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }
    }
}
