﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData; 

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;

    public interface IPatientService
    {
        bool AddPatient(Patient patient);
        bool EditPatient(Patient patient);
        bool DeletePatient(Guid id);
        bool AdmitPatient(PendingPatient pending);
        bool NonAdmitPatient(PendingPatient pending);
        bool SetPatientPending(Guid patientId);
        bool DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReasonComments);
        //bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        bool ActivatePatient(Guid patientId);
        bool ActivatePatient(Guid patientId, DateTime startOfCareDate);
        bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles);
        bool UpdatePatientForPhotoRemove(Patient patient);

        bool AddPrimaryEmergencyContact(Patient patient);
        PatientAdmissionDate GetIfExitOrCreate(Guid patientId);
        bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId);
        bool EditEmergencyContact(PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid id, Guid patientId);

        PatientProfile GetProfile(Guid id);
        string GetAllergies(Guid patientId);

        bool CreateMedicationProfile(Patient patient, Guid medId);
        bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool DeleteMedication(Guid medicationProfileId, Guid medicationId);
        bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate);
        bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId);
        List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory);
        MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid id);
        MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid id);
        AllergyProfileViewData GetAllergyProfilePrint(Guid id);
        DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected);
       
        bool LinkPhysicians(Patient patient);
        bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkPhysician(Guid patientId, Guid physicianId);
        Guid GetPrimaryPhysicianId(Guid patientId, Guid agencyId);
        void AddPhysicianOrderUserAndScheduleEvent(PhysicianOrder physicianOrder, out PhysicianOrder physicianOrderOut);
        bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType);
        List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate);
        List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId);
        bool DeletePhysicianOrder(Guid orderId, Guid patientId, Guid episodeId);
        PhysicianOrder GetOrderPrint();
        PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId);

        void AddCommunicationNoteUserAndScheduleEvent(CommunicationNote communicationNote, out CommunicationNote communicationNoteOut);
        bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId);
        List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate);
        List<CommunicationNote> GetCommunicationNotes(Guid patientId);
        bool DeleteCommunicationNote(Guid Id, Guid patientId);
        CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId);

        bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        FaceToFaceEncounter GetFaceToFacePrint();
        FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId);

        PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent);
        PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode, ScheduleEvent scheduleEvent);
        bool AddEpisode(PatientEpisode patientEpisode);
        bool UpdateEpisode(PatientEpisode episode);
        bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate);
        bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);

        bool CreateEpisodeAndClaims(Patient patient);
        void DeleteEpisodeAndClaims(Patient patient);
        void SetInsurance(Patient patient);
        bool AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates);
        bool UpdateEpisode(Guid episodeId, Guid patientId, string jsonString);
        bool UpdateEpisode(Guid episodeId, Guid patientId, string disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate);
        bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline);
        List<ScheduleEvent> GetScheduledEvents(Guid patientId, string discipline, DateRange range);
        List<ScheduleEvent> GetDeletedTasks(Guid patientId);
        ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId);
        bool Reopen(Guid episodeId, Guid patientId, Guid eventId);
        bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId);
        bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid userId, int task);
        bool DeleteSchedules(Guid patientId, Guid episodeId, List<Guid> eventsToBeDeleted);
        bool DeleteScheduleEventAsset(Guid episodeId, Guid patientId, Guid eventId, Guid assetId);

        bool ReassignSchedules(Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);
        bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId);
        bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);

        PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline);
        PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId);
        PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, string discipline);
        PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, DateTime date, string discipline);

        bool SaveNotes(string button, FormCollection formCollection);
        bool ProcessMissedVisitNotes(string button, Guid Id);
        bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId);

        bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId);

        Rap CreateRap(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician);
        Final CreateFinal(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician);
        bool AddClaim(Guid patientId, Guid episodeId, string type, int insuranceId);
        bool DeleteClaim(Guid patientId, Guid Id, string type);

        ManagedClaim CreateManagedClaim(Patient patient, DateTime startDate, DateTime endDate, int insuranceId);
        bool AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId);

        bool AddMissedVisit(MissedVisit missedVisit);

        bool IsValidImage(HttpFileCollectionBase httpFiles);

        bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId);
       
        PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
        List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId);

        string GetScheduledEventUrl(PatientEpisode episode, DisciplineTasks task);

        PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId);
       
        void AddInfectionUserAndScheduleEvent(Infection infection, out Infection infectionOut);
        void AddIncidentUserAndScheduleEvent(Incident incident, out Incident incidentOut);
       
        List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId);
        List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate);
        List<VitalSign> GetVitalSignsForSixtyDaySummary(Guid patientId, Guid episodeId, DateTime date);

        DisciplineTask GetDisciplineTask(int disciplineTaskId);

        List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int count);
       
        List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task);
        List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId);
        List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId);

        IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent, int version, int type);
        IDictionary<Guid, string> GetPreviousPediatricVisitNotes(Guid patientId, ScheduleEvent scheduledEvent, int version);
        IDictionary<Guid, string> GetPreviousPTNotes(Guid patientId, ScheduleEvent scheduledEvent, int version);
        IDictionary<Guid, string> GetPreviousPTEvals(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousPTDischarges(Guid patientId, ScheduleEvent scheduledEvent);

        IDictionary<Guid, string> GetPreviousOTNotes(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousOTEvals(Guid patientId, ScheduleEvent scheduledEvent);

        IDictionary<Guid, string> GetPreviousSTNotes(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousSTEvals(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousSTReassessment(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousHHANotes(Guid patientId, ScheduleEvent scheduledEvent);
        IDictionary<Guid, string> GetPreviousMSWProgressNotes(Guid patientId, ScheduleEvent scheduledEvent);

        IDictionary<Guid, string> GetPreviousCarePlans(Guid patientId, ScheduleEvent scheduledEvent);

        IDictionary<Guid, string> GetPreviousISOC(Guid patientId, ScheduleEvent scheduledEvent);

        bool AddAllergy(Allergy allergy);
        bool UpdateAllergy(Allergy allergy);
        bool CreateAllergyProfile(Guid patientId);
        bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted);

        bool AddHospitalizationLog(FormCollection formCollection);
        bool UpdateHospitalizationLog(FormCollection formCollection);
        List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId);
        HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId);

        List<NonAdmit> GetNonAdmits();
        List<PendingPatient> GetPendingPatients();
        PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId,string insuranceId , string insuranceType);
        bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id);

        string GetInsurance(string insurance);
        List<PatientData> GetPatients(Guid agencyId, Guid branchId, int status);
        List<PatientData> GetDeletedPatients(Guid agencyId, Guid branchId);

        PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline);
        PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline, out IDictionary<string, NotesQuestion> pocQuestions);

        List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        FrequenciesViewData GetPatientEpisodeFrequencyData(Guid episodeId, Guid patientId);

        List<ScheduleEvent> GetScheduledEventsWithUsers(Guid patientId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetScheduledEventsByStatus(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int status);
        List<ScheduleEvent> GetScheduledEventsByType(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int type);
        List<ScheduleEvent> GetScheduledEventsForDelete(Guid episodeId, Guid patientId, string discipline);

        string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId);
        bool AddReturnComments(Guid eventId, Guid episodeId, string comment);
        bool EditReturnComments(int id, string comment);
        bool DeleteReturnComments(int id);

        bool AddAsset(Guid patientId, HttpFileCollectionBase files);
        List<Asset> GetAssets(Guid patientId);
    }
}
