﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using ViewData;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    
    using Axxess.AgencyManagement.App.Enums;

    public interface IBillingService
    {
        bool AddRap(Rap rap);
        bool AddFinal(Final final);
        string GenerateJsonRAP(List<Guid> rapToGenerate,ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Rap> raps,  bool isHMO, AgencyLocation branch);
        string GenerateJsonFinal(List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Final> finlas, bool isHMO, AgencyLocation branch);
        bool GenerateDirect(List<Guid> ClaimSelected, string Type, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId);
        bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> visit);
        bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, bool IsSupplyNotBillable);
        Final GetFinalWithSupplies(Guid episodeId, Guid patientId);
        Bill AllUnProcessedBill(Guid branchId, int insuranceId, string sortType, string claimType, bool isUsersNeeded);
        List<Bill> AllUnProcessedBillList(Guid branchId, int insuranceId, string claimType, string parentSortType, string columnSortType, bool isUsersNeeded);
        List<Claim> AllUnProcessedRaps(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded);
        List<Claim> AllUnProcessedFinals(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded);

        Bill ClaimToGenerate(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type);

        IList<ClaimHistoryLean> Activity(Guid patientId, int insuranceId);
        IList<PendingClaimLean> PendingClaimRaps(Guid branchId, string primaryInsurance);
        IList<PendingClaimLean> PendingClaimFinals(Guid branchId, string primaryInsurance);
        IList<ClaimLean> AccountsReceivables(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<ClaimLean> AccountsReceivableRaps(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<ClaimLean> AccountsReceivableFinals(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<TypeOfBill> GetAllUnProcessedBill();
        List<Supply> GetSupply(ScheduleEvent scheduleEvent);
        double GetSupplyReimbursement(char type, int year, out PPSStandard ppsStandardOut);
        double GetSupplyReimbursement(char type, int year);
        Rap GetRap(Guid patientId, Guid episodeId);
        Final GetFinalInfo(Guid patientId, Guid episodeId);
        bool UpdateRapStatus(List<Guid> rapToGenerate,  string statusType);
        bool UpdateFinalStatus(List<Guid> finalToGenerate,  string statusType);
        bool FinalComplete(Guid Id);
        UBOFourViewData GetUBOFourInfo(Guid patientId, Guid claimId, string type);
        HCFA1500ViewData GetHCFA1500Info(Guid patientId, Guid claimId, string type);
        HCFA1500ViewData GetHCFA1500InfoForManagedClaim(Guid patientId, Guid claimId);
        ClaimInfoSnapShotViewData GetClaimSnapShotInfo(Guid patientId, Guid claimId, string type);
        ClaimViewData GetClaimViewData(Guid patientId, Guid Id, string type);
        bool UpdateProccesedClaimStatus(Patient patient, Guid Id, string type, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status,  string comment);
        bool UpdatePendingClaimStatus(FormCollection formCollection, string[] rapIds, string[] finalIds);
        bool UpdateRapClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status);
        bool UpdateFinalClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status);
        bool IsEpisodeHasClaim(Guid episodeId, Guid patientId, string type);
        IList<ClaimSnapShotViewData> ClaimSnapShots(Guid Id, string type);
        bool UpdateSnapShot(Guid Id, long batchId, double paymentAmount, DateTime paymentDate, int status, string type);
        ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id);
        List<SelectListItem> GetManagedClaimEpisodes(Guid patientId, Guid managedClaimId);
        bool ManagedVerifyInfo(ManagedClaim claim);
        bool ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> visit);
        bool ManagedVisitSupply(Guid Id, Guid patientId);
        bool UpdateProccesedManagedClaimStatus(Patient patient, Guid Id, string claimDate, int status, string comment);
        ManagedClaimSnapShotViewData GetManagedClaimSnapShotInfo(Guid patientId, Guid claimId);
        bool ManagedComplete(Guid Id, Guid patientId, double total);
        List<Bill> GetClaimsPrint(Guid branchId, int insuranceId, string parentSortType, string columnSortType, string claimType);
        Final GetFinalPrint(Guid episodeId, Guid patientId);
        Rap GetRapPrint(Guid episodeId, Guid patientId);
        UBOFourViewData GetManagedUBOFourInfo(Guid patientId, Guid claimId);
        string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo,  AgencyInsurance payerInfo, out List<ManagedClaim> managedClaims ,AgencyLocation branch);
        bool GenerateManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId);
        Bill ManagedBill(Guid branchId, int insuranceId, int status);
        Bill ManagedClaimToGenerate(List<Guid> managedClaimToGenerate, Guid branchId, int primaryInsurance);
        bool UpdateManagedClaimStatus(List<Guid> managedClaimToGenerate, string statusType);
        bool AddRemittanceUpload(HttpPostedFileBase file);
        List<ClaimInfoDetail> GetSubmittedBatchClaims(int batchId);
        ManagedClaimEpisodeData GetEpisodeAssessmentData(Guid episodeId, Guid patientId);

        IList<ClaimInfoDetail> BillingBatch(string claimType, DateTime batchDate);
        InsuranceAuthorizationViewData InsuranceWithAuthorization(Guid patientId, int insuranceId, DateTime startDate, DateTime endDate);
        Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillableVisitsData(Guid branchId, List<ScheduleEvent> visits, ClaimType claimType, Dictionary<int, ChargeRate> chargeRates, bool isLimitApplied);
        List<BillSchedule> BillableVisitSummary(Guid branchId, List<ScheduleEvent> visits, ClaimType claimType, Dictionary<int, ChargeRate> chargeRates, bool isLimitApplied);
        Dictionary<int, ChargeRate> MedicareBillRate(Dictionary<int, ChargeRate> disciplineRates);
        AgencyInsurance CMSInsuranceToAgencyInsurance(Guid branchId, int insuranceId);
        Dictionary<int, ChargeRate> FinalToCharegRates(Final claim, out AgencyInsurance agencyInsurance);
        Dictionary<int, ChargeRate> ManagedToCharegRates(ManagedClaim claim, out AgencyInsurance agencyInsurance);
        AgencyInsurance RapToInsurance(Rap claim);
        AgencyInsurance FinalToInsurance(Final claim);
        AgencyInsurance ManagedToInsurance(ManagedClaim claim);
        double MedicareSupplyTotal(Final final);
        bool PostRemittance(Guid Id, List<string> Episodes);
        IList<Revenue> GetUnearnedRevenue(Guid branchId, int insurance, DateTime endDate);
        IList<Revenue> GetUnbilledRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<Revenue> GetEarnedRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<Revenue> GetEarnedRevenueByEpisodeDays(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<Revenue> GetRevenueReport(Guid branchId, int insurance, DateTime startDate, DateTime endDate);

        bool AddManagedClaimPayment(ManagedClaimPayment payment);
        bool DeleteManagedClaimPayment(Guid patientId, Guid claimId, Guid id);
        bool UpdateManagedClaimPayment(Guid patientId, Guid claimId, Guid id, double payment, string paymentDate, int payor, string comments);

        bool AddManagedClaimAdjustment(ManagedClaimAdjustment adjustment);
        bool DeleteManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id);
        bool UpdateManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id, double adjustment, Guid typeId, string comments);

        InvoiceViewData GetManagedInvoiceInfo(Guid patientId, Guid claimId, bool isForPatient);

        List<ClaimLean> GetManagedClaimsWithPaymentData(Guid branchId, int status, DateTime startDate, DateTime endDate);
    }
}
