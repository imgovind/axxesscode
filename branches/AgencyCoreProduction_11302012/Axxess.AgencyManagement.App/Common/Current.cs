﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web;
    using System.Reflection;
    using System.Security.Principal;
    using System.Collections.Generic;

    using OpenForum.Core.Models;

    using Enums;
    using Domain;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public static class Current
    {
        public static Func<DateTime> Time = () => DateTime.UtcNow;
        public static Func<DateTime> EndofMonth = () => { return DateTime.Now.AddMonths(1).AddDays(-(DateTime.Now.Day)); };
        public static Func<DateTime> StartofMonth = () => { return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); };

        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public static ForumUser ForumUser
        {
            get
            {
                return new ForumUser(Current.UserId, Current.UserFullName, null, null);
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            }
        }

        public static AxxessIdentity User
        {
            get
            {
                AxxessIdentity identity = null;
                if (HttpContext.Current.User is WindowsIdentity)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessPrincipal)
                {
                    AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                    identity = (AxxessIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid UserId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.UserId;
                }

                return Guid.Empty;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static Guid AgencyId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyId;
                }

                return Guid.Empty;
            }
        }

        private static readonly IPatientRepository patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;

        public static string LastUsedPatientId
        {
            get
            {
                return patientRepository.LastPatientId(Current.AgencyId);
            }
        }

        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public static int MaxAgencyUserCount
        {
            get
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    return agency.Package == 0 ? 100000000 : agency.Package;
                }

                return 0;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        public static string AgencyName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyName;
                }

                return string.Empty;
            }
        }

        public static string UserFullName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.FullName;
                }

                return string.Empty;
            }
        }

        public static string UserAddress
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Address;
                }

                return string.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                //if (!HttpContext.Current.Request.IsLocal)
                //{
                //    return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                //}
                //return HttpContext.Current.Request.UserHostAddress;
                var ipString = string.Empty;
                if (string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
                {
                    ipString = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                else
                {
                    ipString = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }

                return ipString;
            }
        }

        public static string ServerName
        {
            get
            {
                return Environment.MachineName;
            }
        }

        public static string ServerIp
        {
            get
            {
                return HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            }
        }

        public static bool IsIpAddressRestricted
        {
            get
            {
                var result = true;

                if (Current.IpAddress.StartsWith("10.0.1")
                    || Current.IpAddress.StartsWith("97.79.164.66")
                    || Current.IpAddress.StartsWith("10.0.5")
                    || Current.IpAddress.IsEqual("127.0.0.1"))
                {
                    result = false;
                }
                return result;
            }
        }

        public static bool CanContinue
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    if (User.Session.AutomaticLogoutTime.IsNotNullOrEmpty())
                    {
                        var autoLogoutTime = new Time(User.Session.AutomaticLogoutTime);
                        if (autoLogoutTime != null && DateTime.Now.TimeOfDay >= autoLogoutTime.TimeOfDay)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public static bool HasRight(Permissions permission)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (HttpContext.Current.User is AxxessPrincipal)
            {
                AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                result = principal.HasPermission(permission);
            }
            return result;
        }

        public static bool IsInRole(AgencyRoles roleId)
        {
            var result = false;
            if (roleId > 0)
            {
                var role = ((int)roleId).ToString();
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    var agencyRoles = User.Session.AgencyRoles.Split(';');
                    if (agencyRoles.Length > 0)
                    {
                        foreach (string agencyRole in agencyRoles)
                        {
                            if (role.IsEqual(agencyRole))
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsAgencyAdmin
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsAgencyAdmin();
                }
                return false;
            }
        }

        public static bool IsDirectorOfNursing
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsDirectorOfNursing();
                }
                return false;
            }
        }

        public static bool IsClinician
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinician();
                }
                return false;
            }
        }

        public static bool IsClinicianOrHHA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinicianOrHHA();
                }
                return false;
            }
        }

        public static bool IsCaseManager
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsCaseManager();
                }
                return false;
            }
        }

        public static bool IsBiller
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsBiller();
                }
                return false;
            }
        }

        public static bool IsClerk
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClerk();
                }
                return false;
            }
        }

        public static bool IsScheduler
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsScheduler();
                }
                return false;
            }
        }

        public static bool IsQA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsQA();
                }
                return false;
            }
        }

        public static bool IsOfficeManager
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsOfficeManager();
                }
                return false;
            }
        }

        public static bool IsCommunityLiason
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsCommunityLiason();
                }
                return false;
            }
        }

        public static bool IfOnlyRole(AgencyRoles role)
        {
            var result = false;
            if (User != null && User.Session != null && User.Session.AgencyRoles != null)
            {
                var userRoles = User.Session.AgencyRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                if (userRoles.Length == 1 && userRoles[0].IsEqual(((int)role).ToString()))
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool IsAgencyFrozen
        {
            get
            {
                if (User != null && User.Session != null && User.Session.IsAgencyFrozen)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool IsUserPrime
        {
            get
            {
                if (User != null && User.Session != null && User.Session.IsPrimary)
                {
                    return true;
                }
                return false;
            }
        }

        public static List<SingleUser> ActiveUsers
        {
            get
            {
                return authenticationAgent.GetUsers(true);
            }
        }

        public static List<SingleUser> InActiveUsers
        {
            get
            {
                return authenticationAgent.GetUsers(false);
            }
        }

        public static bool OasisVendorExist
        {
            get
            {
                if (User != null && User.Session != null )
                {
                    return User.Session.OasisVendorExist;
                }
                return false;
            }
        }
    }
}
