﻿namespace Axxess.Api.Contracts
{
    using System;
    public static class ScheduleEventExtensions
    {
        public static bool IsSkilledCare(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "Nursing"
                    || scheduleEvent.Discipline == "PT"
                    || scheduleEvent.Discipline == "OT"
                    || scheduleEvent.Discipline == "ST"
                    )
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompleted(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline) && !string.IsNullOrEmpty(scheduleEvent.Status))
            {
                if (scheduleEvent.Status == "115" || scheduleEvent.Status == "125" ||
                    scheduleEvent.Status == "130" || scheduleEvent.Status == "135" ||
                    scheduleEvent.Status == "145" || scheduleEvent.Status == "215" ||
                    scheduleEvent.Status == "220" || scheduleEvent.Status == "225" ||
                    scheduleEvent.Status == "240" || scheduleEvent.Status == "400" ||
                    scheduleEvent.Status == "420" || scheduleEvent.Status == "425" ||
                    scheduleEvent.Status == "445" || scheduleEvent.Status == "450" ||
                    scheduleEvent.Status == "455" || scheduleEvent.Status == "460" ||
                    scheduleEvent.Status == "520" || scheduleEvent.Status == "525")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "ST")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "OT")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPTNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "PT")
                {
                    return true;
                }
            }
            return false;
        }


        public static bool IsHhaNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "HHA")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsMSW(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && !string.IsNullOrEmpty(scheduleEvent.Discipline))
            {
                if (scheduleEvent.Discipline == "MSW" && scheduleEvent.DisciplineTask != 110)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsStartofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 13
                    || scheduleEvent.DisciplineTask == 89
                    || scheduleEvent.DisciplineTask == 61
                    || scheduleEvent.DisciplineTask == 112)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsRecertificationAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 8
                    || scheduleEvent.DisciplineTask == 73
                    || scheduleEvent.DisciplineTask == 66
                    || scheduleEvent.DisciplineTask == 90)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsResumptionofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                if (scheduleEvent.DisciplineTask == 9
                    || scheduleEvent.DisciplineTask == 69
                    || scheduleEvent.DisciplineTask == 62)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetAssessmentType(this ScheduleEvent scheduleEvent)
        {
            var assessmentType = string.Empty;
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                switch (scheduleEvent.DisciplineTask)
                {
                    case 13:
                        assessmentType = "OASISCStartofCare";
                        break;
                    case 89:
                        assessmentType = "NonOASISStartofCare";
                        break;
                    case 61:
                        assessmentType = "OASISCStartofCarePT";
                        break;
                    case 112:
                        assessmentType = "OASISCStartofCareOT";
                        break;
                    case 8:
                        assessmentType = "OASISCRecertification";
                        break;
                    case 73:
                        assessmentType = "OASISCRecertificationOT";
                        break;
                    case 66:
                        assessmentType = "OASISCRecertificationPT";
                        break;
                    case 90:
                        assessmentType = "NonOASISRecertification";
                        break;
                    case 9:
                        assessmentType = "OASISCResumptionofCare";
                        break;
                    case 69:
                        assessmentType = "OASISCResumptionofCareOT";
                        break;
                    case 62:
                        assessmentType = "OASISCResumptionofCarePT";
                        break;
                }
            }
            return assessmentType;
        }
    }
}
