﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Cache/2011/11/")]
    public class UniqueEntity
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public string Xml { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
