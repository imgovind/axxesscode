﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/LoginResult/2012/03/")]
    public class LoginResult
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public bool IsAuthenticated { get; set; }
    }
}