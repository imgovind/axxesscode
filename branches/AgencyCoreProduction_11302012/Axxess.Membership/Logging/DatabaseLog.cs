﻿namespace Axxess.Membership.Logging
{
    using System;
    using System.Web;
    using System.Text;
    using System.Reflection;
    using System.Diagnostics;

    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;

    public class DatabaseLog : ILog
    {
        #region ILog Members

        public void Info(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Info.ToString()
                });
        }

        public void Warning(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Warn.ToString()
                });
        }

        public void Error(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Error.ToString()
                });
        }

        public void Exception(Exception exception)
        {
            if (exception != null)
            {
                var error = new Error
                {
                    Created = DateTime.Now,
                    Message = exception.Message,
                    Server = Environment.MachineName,
                    Type = LogPriority.Fatal.ToString()
                };

                error.Detail.ExceptionType = exception.GetType().ToString();
                error.Detail.Text = exception.StackTrace;

                if (exception is HttpException)
                {
                    HttpException httpException = exception as HttpException;
                    error.Detail.ExceptionType = httpException.GetHttpCode().ToString();
                }

                if (exception.InnerException != null)
                {
                    error.Detail.InnerError = exception.InnerException.ToString();
                }

                LogEngine.Instance.Add(error);
            }
        }

        #endregion
    }
}
