﻿namespace Axxess.AgencyManagement.Domain
{
    public class PatientSchedule : Patient
    {
        public PatientEpisode Episode { get; set; }
    }
}
