﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class ReferralEmergencyContact : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Relationship { get; set; }
        public bool IsPrimary { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        [SubSonicIgnore]
        public string PrimaryPhoneFormatted { get { return this.PrimaryPhone.ToPhone(); } }
        
        [SubSonicIgnore]
        public bool HasValue 
        { 
            get 
            {
                if (this.FirstName.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.LastName.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.PrimaryPhone.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.Relationship.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.AlternatePhone.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.EmailAddress.IsNotNullOrEmpty())
                {
                    return true;
                }
                else
                { 
                    return false; 
                }
            } 
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
              AddValidationRule(new Validation(() => this.HasValue &&
                               (string.IsNullOrEmpty(this.FirstName) || 
                               string.IsNullOrEmpty(this.LastName) || 
                               (string.IsNullOrEmpty(this.PrimaryPhone) && this.PhonePrimaryArray.Count == 0)), "Patient emergency contact requires a first name, last name, and a primary phone number.<br/>"));
        }

        #endregion

    }
}
