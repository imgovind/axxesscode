﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using SubSonic.SqlGeneration.Schema;

    public class PatientAsset
    {
        [SubSonicPrimaryKey]
        public Guid PatientId { get; set; }
        public Guid AssetId { get; set; }
    }
}
