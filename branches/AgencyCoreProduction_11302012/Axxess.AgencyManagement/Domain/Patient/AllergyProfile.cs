﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    public class AllergyProfile
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string Allergies { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string Type { get; set; }

        public override string ToString()
        {
            var allergies = new StringBuilder();
            if (this.Allergies.IsNotNullOrEmpty())
            {
                var allergyList = this.Allergies.ToObject<List<Allergy>>().Where(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList();
                allergyList.ForEach(a =>
                {
                    allergies.AppendFormat("{0}, ", a.Name);
                });
                if (allergies.ToString().IsNotNullOrEmpty())
                {
                    return allergies.Remove(allergies.Length - 2, 2).ToString();
                }
            }
            return "NKA (Food/Drugs/Latex/Environment)";
        }
    }
}
