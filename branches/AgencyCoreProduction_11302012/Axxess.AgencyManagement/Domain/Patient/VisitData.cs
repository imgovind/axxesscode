﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    public class VisitData
    {
        public string Frequency { get; set; }
        public int TotalAllowed { get; set; }
        public int Count { get; set; }
        public string Message { 
            get 
            {
                if (TotalAllowed != -1)
                {
                    if (Count > TotalAllowed)
                    {
                        return "There are more visits than frequency specified. Remove ";
                    }
                    else if (Count < TotalAllowed)
                    {
                        return "There are fewer visits than frequency";
                    }
                }
                return string.Empty;
            } 
        }

        public VisitData(string freq, int totalAllowed, int count) 
        {
            Frequency = freq;
            TotalAllowed = totalAllowed;
            Count = count;
        }

        public VisitData(string freq, int totalAllowed)
            : this(freq, totalAllowed, 0) { }

        public VisitData(string freq)
            : this(freq, -1, 0) { }
       
        public VisitData() : this("", 0) {}
    }

    public class TextNumberPair
    {
        public string Text { get; set; }
        public int Number { get; set; }

        public TextNumberPair(string text, int number) 
        {
            Text = text;
            Number = number;
        }

        public TextNumberPair()
        {
            Text = "";
            Number = 0;
        }
    }
}
