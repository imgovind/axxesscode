﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Enums;

    using Axxess.Core.Extension;

    public class PatientHospitalizationData
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PatientId { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string HospitalizationDate { get; set; }
        public string LastHomeVisitDate { get; set; }
        public string DisplayName { get { return string.Concat(this.LastName.ToTitleCase(), ", ", this.FirstName.ToTitleCase()); } }
        public string User { get; set; }
        public int SourceId { get; set; }
        public string Source { get { return SourceId.ToEnum<TransferSourceTypes>(TransferSourceTypes.User).GetDescription(); } }
    }
}
