﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;


   public class ClaimLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public long BatchId { get; set; }
        public string Type { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AdmissionSource { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string DiagnosisCode { get; set; }
        public string AssessmentType { get; set; }
        public string HippsCode { get; set; }
        public int PrimaryInsuranceId { get; set; }
        public int Status { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public DateTime ClaimDate { get; set; }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public string Comment { get; set; }
        public double SupplyTotal { get; set; }
        public string TypeName
        {
            get
            {
                return this.Type.ToUpperCase();
            }

        }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus), this.Status) ? ((BillingStatus)this.Status).GetDescription() : string.Empty;

            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string ClaimDateFormatted
        {
            get
            {
                return ClaimDate.Date > DateTime.MinValue.Date ? ClaimDate.ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Format("{0}, {1} {2}",this.LastName, this.FirstName, this.MiddleInitial); 
            }
        }

        public double AdjustmentAmount { get; set; }
        public double Balance
        {
            get
            {
                return this.ClaimAmount - this.PaymentAmount - AdjustmentAmount;
            }
        }

        public bool IsHMO { get; set; }
        public string HealthPlanId { get; set; }
        public string PayerId { get; set; }
        public string PayorName { get; set; }
        public string PayorAuthorizationCode { get; set; }
        public string OtherProviderId { get; set; }
        public string ProviderId { get; set; }
        public string ProviderSubscriberId { get; set; }

        public double Amount30 { get { return this.DateDifference <= 30 ? this.ClaimAmount : 0; } }
        public double Amount60 { get { return this.DateDifference <= 60 && this.DateDifference >= 31 ? this.ClaimAmount : 0; } }
        public double Amount90 { get { return this.DateDifference <= 90 && this.DateDifference >= 61 ? this.ClaimAmount : 0; } }
        public double AmountOver90 { get { return this.DateDifference >= 90 ? this.ClaimAmount : 0; } }

        public int DateDifference { get { return (DateTime.Now.Subtract(this.ClaimDate)).Days; } }

    }
}
