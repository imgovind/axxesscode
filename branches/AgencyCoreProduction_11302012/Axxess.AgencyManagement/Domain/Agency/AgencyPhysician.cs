﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.ComponentModel.DataAnnotations;

    public class AgencyPhysician : EntityBase//, IXmlSerializable
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid LoginId { get; set; }
        public string NPI { get; set; }
        public string UPIN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string Credentials { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneAlternate { get; set; }
        public string FaxNumber { get; set; }     
        public string EmailAddress { get; set; }         
        public string LicenseNumber { get; set; }
        public string LicenseStateCode { get; set; }        
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public bool PhysicianAccess { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Licenses { get; set; }

        #endregion

        #region Domain
        
        [SubSonicIgnore]
        public bool IsPecosVerified { get; set; }
        [SubSonicIgnore]
        public string PecosImage
        {
            get
            {
                if (IsPecosVerified)
                {
                    return "<span class='img icon success-small'></span>";
                }
                else
                {
                    return "<span class='img icon error-small'></span>";
                }
            }
        }
        [SubSonicIgnore]
        public string PhoneWorkFormatted { get { return this.PhoneWork.ToPhone(); } }
        [SubSonicIgnore]
        public string PhoneAlternateFormatted { get { return this.PhoneAlternate.ToPhone(); } }
        [SubSonicIgnore]
        public string FaxNumberFormatted { get { return this.FaxNumber.ToPhone(); } }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2}, {3} {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0}, {1}, {2} {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public bool Primary { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> PhoneWorkArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> PhoneAltArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName, " ", this.Credentials);
            }
        }

        [SubSonicIgnore]
        public List<PhysicainLicense> LicensesArray { get; set; }
       
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Physician address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Physician city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Physician state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Physician zip is required."));
            if (this.PhoneWork.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneWorkArray.Count != 3, "Physician work phone is empty or not in the right format."));
            }
        }

        #endregion


        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return (null);
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            reader.ReadToDescendant("Id");
            Id = new Guid(reader.ReadElementString("Id"));
            AgencyId = new Guid(reader.ReadElementString("AgencyId"));
            LoginId = new Guid(reader.ReadElementString("LoginId"));
            NPI = reader.ReadElementString("NPI");
            UPIN = reader.ReadElementString("UPIN");
            FirstName = reader.ReadElementString();
            LastName = reader.ReadElementString();
            Credentials = reader.ReadElementString();
            EmailAddress = reader.ReadElementString();
            AddressLine1 = reader.ReadElementString();
            AddressLine2 = reader.ReadElementString();
            AddressCity = reader.ReadElementString();
            AddressStateCode = reader.ReadElementString();
            AddressZipCode = reader.ReadElementString();
            PhoneWork = reader.ReadElementString();
            FaxNumber = reader.ReadElementString();
            Comments = reader.ReadElementString();
            IsDeprecated = reader.ReadElementString().ToBoolean();
            PhysicianAccess = reader.ReadElementString().ToBoolean();
            Created = reader.ReadElementString().ToDateTime();
            Modified = reader.ReadElementString().ToDateTime();
            Primary = reader.ReadElementString().ToBoolean();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("Id");
            writer.WriteString(Id.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("AgencyId");
            writer.WriteString(AgencyId.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LoginId");
            writer.WriteString(LoginId.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("NPI");
            writer.WriteString(NPI);
            writer.WriteEndElement();

            writer.WriteStartElement("UPIN");
            writer.WriteString(UPIN);
            writer.WriteEndElement();

            writer.WriteStartElement("FirstName");
            writer.WriteString(FirstName.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("LastName");
            writer.WriteString(LastName.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("Credentials");
            writer.WriteString(Credentials.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("Email");
            writer.WriteString(EmailAddress.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("AddressLine1");
            writer.WriteString(AddressLine1.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("AddressLine2");
            writer.WriteString(AddressLine2.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("AddressCity");
            writer.WriteString(AddressCity.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("AddressStateCode");
            writer.WriteString(AddressStateCode.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("AddressZipCode");
            writer.WriteString(AddressZipCode.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("PhoneWork");
            writer.WriteString(PhoneWork.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("FaxNumber");
            writer.WriteString(FaxNumber.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("Comments");
            writer.WriteString(Comments.SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsDeprecated");
            writer.WriteString(IsDeprecated.ToString().SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("PhysicianAccess");
            writer.WriteString(PhysicianAccess.ToString().SanitizeXmlString());
            writer.WriteEndElement();

            writer.WriteStartElement("Created");
            writer.WriteString(Created.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Modified");
            writer.WriteString(Modified.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Primary");
            writer.WriteString(Primary.ToString());
            writer.WriteEndElement();
        }

        #endregion
    }
}
