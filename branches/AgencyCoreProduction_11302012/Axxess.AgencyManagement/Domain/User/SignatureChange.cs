﻿namespace Axxess.AgencyManagement.Domain
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class SignatureChange
    {
        public string CurrentSignature { get; set; }
        public string NewSignature { get; set; }
        public string NewSignatureConfirm { get; set; }
    }
}
