﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Axxess.AgencyManagement.Enums;

    public class ManagedClaimPayment
    {
        public Guid Id { get; set; }
        public Guid ClaimId { get; set; }
        public Guid PatientId { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
