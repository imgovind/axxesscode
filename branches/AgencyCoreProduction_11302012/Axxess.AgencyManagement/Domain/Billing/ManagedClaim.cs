﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Axxess.AgencyManagement.Enums;

    public class ManagedClaim : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }

        public string PatientIdNumber { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicIgnore]
        public string MiddleInitial { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }

        public DateTime StartofCareDate { get; set; }

        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }

        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }

        public string Type { get; set; }

        public bool AreOrdersComplete { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsGenerated { get; set; }
        public string DiagnosisCode { get; set; }
        public string ConditionCodes { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public bool IsRapGenerated { get; set; }
        public string VerifiedVisits { get; set; }
        public string Supply { get; set; }
        public double SupplyTotal { get; set; }
        public int PrimaryInsuranceId { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public double ProspectivePay { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }

        public string AssessmentType { get; set; }
        public string AdmissionSource { get; set; }
        public int PatientStatus { get; set; }
        public string UB4PatientStatus { get; set; }
        public DateTime ClaimDate { get; set; }
        public string Comment { get; set; }
        public DateTime DischargeDate { get; set; }
        public string HealthPlanId { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string Relationship { get; set; }
        public Guid Authorization { get; set; }
        public string AuthorizationNumber { get; set; }
        public string AuthorizationNumber2 { get; set; }
        public string AuthorizationNumber3 { get; set; }
        public string CBSA { get; set; }
        public string SupplyCode { get; set; }
        public string Ub04Locator81cca { get; set; }
        public string Ub04Locator39 { get; set; }
        public string Ub04Locator31 { get; set; }
        public string Ub04Locator32 { get; set; }
        public string Ub04Locator33 { get; set; }
        public string Ub04Locator34 { get; set; }
        public string HCFALocators { get; set; }

        public string Insurance { get; set; }

        public bool IsHomeHealthServiceIncluded { get; set; }

        [SubSonicIgnore]
        public AgencyInsurance AgencyInsurance { get; set; }
        [SubSonicIgnore]
        public Guid AgencyLocationId { get; set; }
        [SubSonicIgnore]
        public bool HasMultipleEpisodes { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1} {2}", this.FirstName, this.LastName, this.MiddleInitial);
            }
        }
        [SubSonicIgnore]
        public string ClaimDateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }
        [SubSonicIgnore]
        public List<ScheduleEvent> Visits { get; set; }
        [SubSonicIgnore]
        public string Primary { get; set; }
        [SubSonicIgnore]
        public string Second { get; set; }
        [SubSonicIgnore]
        public string Third { get; set; }
        [SubSonicIgnore]
        public string Fourth { get; set; }
        [SubSonicIgnore]
        public string Fifth { get; set; }
        [SubSonicIgnore]
        public string Sixth { get; set; }
        [SubSonicIgnore]
        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillVisitDatas { get; set; }

        [SubSonicIgnore]
        public List<BillSchedule> BillVisitSummaryDatas { get; set; }

        [SubSonicIgnore]
        public string ConditionCode18 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode19 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode20 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode21 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode22 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode23 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode24 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode25 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode26 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode27 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode28 { get; set; }

        [SubSonicIgnore]
        public bool IsMedicareHMO { get; set; }

        [SubSonicIgnore]
        public int UnitType { get; set; }

        [SubSonicIgnore]
        public List<SelectListItem> Authorizations { get; set; }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient First Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient Last Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PatientIdNumber), "Patient Medical Record Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.IsuranceIdNumber), "Patient Medicare Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient Gender is required."));
            AddValidationRule(new Validation(() => !this.DOB.IsValid() || (this.DOB.Date <= DateTime.MinValue.Date), "Patient DOB is not valid date."));
            if (this.DOB.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.DOB.Date < DateTime.Now.Date), "Patient DOB cannot be a future date."));
            }
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date <= DateTime.MinValue.Date, "Claim Start Date is not valid."));
            AddValidationRule(new Validation(() => this.EpisodeEndDate.Date <= DateTime.MinValue.Date, "Claim End Date is not valid."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date > this.EpisodeEndDate.Date, "Claim Start Date must be less than the Claim End Date."));

            if (this.StartofCareDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.EpisodeStartDate.Date >= this.StartofCareDate.Date), "Episode Start Date has to be greater than or equal to the Patient Admission date."));
                AddValidationRule(new Validation(() => !(this.StartofCareDate.Date <= DateTime.Now.Date), "Admission Date has to be less than or equal to Today's Date."));
                if (this.DOB.IsValid())
                {
                    AddValidationRule(new Validation(() => !(this.StartofCareDate.Date > this.DOB.Date), "Admission Date has to be greater than the Patient's DOB."));
                }
            }

            AddValidationRule(new Validation(() => (this.FirstBillableVisitDate.Date <= DateTime.MinValue.Date), "First Billable Visit date is not valid date."));
            if (this.FirstBillableVisitDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.StartofCareDate.Date), "First Billable Visit date has to be greater than or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.EpisodeStartDate.Date), "First Billable Visit date has to be greater than or equal to Episode Start Date."));
            }
            if (this.IsManagedClamDischage())
            {
                AddValidationRule(new Validation(() => (this.DischargeDate.Date < DateTime.MinValue.Date), "Discharge Date to be valid date."));
                AddValidationRule(new Validation(() => (this.DischargeDate.Date <= this.EpisodeStartDate.Date), "Discharge Date must be greater than the Episode Start Date."));
            }
            AddValidationRule(new Validation(() => !this.StartofCareDate.IsValid() || (this.StartofCareDate.Date <= DateTime.MinValue.Date), "Admission Date is not valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianLastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianFirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianNPI), "Physician NPI is required."));
        }

        #endregion
    }
}
