﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class BillSchedule
    {
        public Guid EventId { get; set; }
        public string DisciplineTaskName { get; set; }
        public string PereferredName { get; set; }
        public string EventDate { get; set; }
        public string VisitDate { get; set; }
        public string RevenueCode { get; set; }
        public string HCPCSCode { get; set; }
        public string Modifier { get; set; }
        public string Modifier2 { get; set; }
        public string Modifier3 { get; set; }
        public string Modifier4 { get; set; }
        public string StatusName { get; set; }
        public int Unit { get; set; }
        public double Charge { get; set; }
        public bool IsExtraTime  { get; set; }
        public List<BillSchedule> UnderlyingVisits { get; set; }
    }
}
