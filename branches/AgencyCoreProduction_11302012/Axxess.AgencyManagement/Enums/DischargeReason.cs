﻿
namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum DischargeReason
    {
        [Description("Goals Met")]
        GoalsMet = 1,
        [Description("To Nursing Home")]
        ToNursingHome = 2,
        [Description("Deceased")]
        Deceased = 3,
        [Description("Noncompliant")]
        Noncompliant = 4,
        [Description("To Hospital")]
        ToHospital = 5,
        [Description("Moved from Service Area")]
        MovedfromServiceArea = 6,
        [Description("Refused Care")]
        RefusedCare = 7,
        [Description("No Longer Homebound")]
        NoLongerHomebound = 8,
        [Description("Other")]
        Other = 9
    }
}
