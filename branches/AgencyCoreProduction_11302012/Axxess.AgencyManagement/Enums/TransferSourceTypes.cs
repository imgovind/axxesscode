﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum TransferSourceTypes : byte
    {
        [Description("User-Generated Transfer")]
        User,
        [Description("OASIS-C Transfer Assessment")]
        Oasis
    }
}
