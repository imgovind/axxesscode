﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum PatientStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Discharge")]
        Discharged = 2,
        [Description("Pending")]
        Pending = 3,
        [Description("Non-Admit")]
        NonAdmission = 4,
        [Description("Deprecate")]
        Deprecated = 5
    }
}
