﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public Asset Get(Guid id, Guid agencyId)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId && a.IsDeprecated == false);
        }

        public bool Delete(Guid id)
        {
            var asset = database.Single<Asset>(a => a.Id == id);
            if (asset != null)
            {
                asset.IsDeprecated = true;
                asset.Modified = DateTime.Now;
                database.Update<Asset>(asset);
                return true;
            }
            return false;
        }

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }

        public List<Asset> GetPatientAssets(Guid patientId)
        {
            List<Asset> list = new List<Asset>();

            var patientAssetScript = string.Format(@"SELECT a.Id, a.AgencyId, a.FileName, a.FileSize, a.ContentType, a.Created, a.Modified "+
                "FROM assets a INNER JOIN patientassets pa "+
                "ON a.Id = pa.AssetId "+
                "WHERE a.IsDeprecated = 0 AND pa.PatientId = '{0}'", patientId);
            using (var cmd = new FluentCommand<Asset>(patientAssetScript))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new Asset
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    FileName = reader.GetString("FileName"),
                    FileSize = reader.GetString("FileSize"),
                    ContentType = reader.GetString("ContentType"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                }).AsList();
            }

            return list;
        }

        
        public bool DeletePatientAsset(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool AddPatientAsset(Asset asset, Guid patientId)
        {
            bool result = false;
            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                var pa = new PatientAsset()
                {
                    AssetId = asset.Id,
                    PatientId = patientId
                };
                database.Add<PatientAsset>(pa);
                result = true;
            }
            return result;
        }

        public Asset GetPatientAsset(Guid id, Guid patientId, Guid agencyId)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
