﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%= string.Format("{0}{1}{2}", "<script type='text/javascript'>acore.renamewindow('New Location | ", Model.Name, "','newagencylocation');</script>")%>
<% using (Html.BeginForm("Add", "Location", FormMethod.Post, new { @id = "newLocationForm" })) { %>
<%= Html.Hidden("AgencyId", Model.Id, new { @id = "New_Location_AgencyId" })%>
<div class="wrapper main">
    <fieldset>
        <legend>Information</legend>
        <div class="column"><div class="row"><label for="New_Location_Name" class="float-left">Location Name:</label><div class="float-right"> <%=Html.TextBox("Name", "", new { @id = "New_Location_Name", @class = "text input_wrapper required", @maxlength = "20" })%></div></div></div>   
        <div class="column"><div class="row"><label for="New_Location_CustomId" class="float-left">Custom Id:</label><div class="float-right"> <%=Html.TextBox("CustomId", "", new { @id = "New_Location_CustomId", @class = "text input_wrapper" })%></div></div></div>
    </fieldset> 
    <fieldset>
       <legend>Branch Information </legend>
       <div class="float-left"><%=Html.CheckBox("IsLocationStandAlone", false, new { @id = "New_Location_IsLocationStandAlone", @class = "radio" })%>&nbsp;<label for="New_Location_IsLocationStandAlone">Check here if this branch has a different Medicare Provider Number.</label></div>
       <div class="clear"></div>
       <div id="New_Location_StandAloneInfoContent"></div>
    </fieldset>
    <fieldset>
       <legend>Submitter Information (Medicare)</legend>
       <div class="wide-column"><div class="row"><label for="New_Location_BranchID" class="float-left">(M0016)Branch ID Number(For OASIS Submission):</label><div class="float-left"><%var branchId = new SelectList(new[] { new SelectListItem { Text = "-- Select Branch Id --", Value = "0" }, new SelectListItem { Text = "N", Value = "N" }, new SelectListItem { Text = "P", Value = "P" }, new SelectListItem { Text = "Other", Value = "Other" }}, "Value", "Text", "0"); %><%= Html.DropDownList("BranchId", branchId, new { @id = "New_Location_BranchId", @class = "requireddropdown" })%></div><%=Html.TextBox("BranchIdOther", "", new { @id = "New_Location_BranchIdOther", @class = string.Format("text input_wrapper hidden") })%> </div></div>
    </fieldset> 
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="New_Location_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"> <%=Html.TextBox("AddressLine1", "", new { @id = "New_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"> <%=Html.TextBox("AddressLine2", "", new { @id = "New_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Location_AddressCity" class="float-left">City:</label><div class="float-right"> <%=Html.TextBox("AddressCity", "", new { @id = "New_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressStateCode" class="float-left">State:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Location_AddressStateCode", @class = "AddressStateCode required valid" })%></div></div>
            <div class="row"><label for="New_Location_AddressZipCode" class="float-left">Zip:</label><div class="float-right"><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%> - <%= Html.TextBox("AddressZipCodeFour", "", new { @id = "New_Location_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="New_Location_PhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_long" name="PhoneArray" id="New_Location_PhoneArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Location_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Location_FaxNumberArray3" maxlength="4" /></div></div> 
            <div class="row"><label for="New_Location_IsMainOffice">Is Main Office?</label><div class="float-right"><input type="checkbox" name="IsMainOffice" class="radio" id="New_Location_IsMainOffice" /></div></div>
        </div>
        <table class="form"><tbody><tr class="line-seperated vert"><td><label for="New_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", "", new { @id = "New_Location_Comments" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newlocation');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $("#New_Location_IsLocationStandAlone").change(function() {
        if ($(this).is(':checked')) Agency.LoadLocationStandAloneContent("#New_Location_StandAloneInfoContent", "<%=Model.Id%>", "<%= Guid.Empty %>");
        else $("#New_Location_StandAloneInfoContent").empty();
    });
    $("#New_Location_BranchId").change(function() {
        if ($(this).val() == 'Other') $("#New_Location_BranchIdOther").removeClass("hidden");
        else $("#New_Location_BranchIdOther").removeClass("hidden").addClass("hidden");
    });
</script>