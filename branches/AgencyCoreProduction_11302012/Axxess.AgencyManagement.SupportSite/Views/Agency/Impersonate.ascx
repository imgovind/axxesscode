﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Impersonate", "Agency", FormMethod.Post, new { @id = "impersonateAgencyForm" }))%>
<%  { %>
    <div class="form_wrapper">
        <fieldset>
            <legend>Impersonate Agency</legend>
            <div class="wide_column">
                <div class="row"><label for="Impersonate_Agency_AgencyId">Agency:</label><div class="float_right"><%= Html.Agencies("AgencyId", "", new { @id = "Impersonate_Agency_AgencyId", @class = "required" })%></div></div>
            </div>
        </fieldset>
        <div class="buttons"><ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        </ul></div>
    </div>
<%} %>