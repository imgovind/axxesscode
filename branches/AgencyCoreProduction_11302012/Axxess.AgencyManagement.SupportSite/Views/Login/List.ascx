﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Axxess.Membership.Domain.Login>().Name("List_Logins").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(l => l.DisplayName).Title("Display Name").Sortable(true);
    columns.Bound(l => l.EmailAddress).Title("E-mail Address").Sortable(true);
    columns.Bound(l => l.IsAxxessAdmin).Title("Is Admin").Sortable(false);
    columns.Bound(l => l.IsAxxessSupport).Title("Is Support").Sortable(false);
    columns.Bound(l => l.IsActive).Title("Active").Width(150).Sortable(false);
    columns.Bound(l => l.CreatedFormatted).Title("Created").Width(110);
    columns.Bound(l => l.Id).Title("Action").Sortable(false).Width(130).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditLogin('<#=Id#>');\">Edit</a>");
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Login")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Logins .t-grid-toolbar").html("");
    $("#List_Logins .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22acore.open('newlogin'); return false;%22%3ENew Login%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>