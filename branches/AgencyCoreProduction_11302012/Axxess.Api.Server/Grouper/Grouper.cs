﻿namespace Axxess.Api.Server
{
    using System;
    using System.Runtime.InteropServices;

    public class Grouper
    {
        public const string ModuleName = "HomeHealthJavaBridge.dll";

        [DllImport(ModuleName, EntryPoint = "?process_record_v203@@YGXPAD0000@Z")]
        public static extern void GetHippsCode([MarshalAs(UnmanagedType.VBByRefStr)] ref string outData, [MarshalAs(UnmanagedType.VBByRefStr)] ref string oasisRecord, [MarshalAs(UnmanagedType.VBByRefStr)] ref string oasisTreatment, [MarshalAs(UnmanagedType.VBByRefStr)] ref string version, [MarshalAs(UnmanagedType.VBByRefStr)] ref string invalidFlag);

    }
}
