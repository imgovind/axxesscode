﻿namespace Axxess.Api.Server
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class OasisValidationService : MarshalByRefObject, IValidationService
    {
        #region IValidationService Members

        public List<ValidationError> ValidateAssessment(string oasisDataString)
        {
            return OasisValidator.CheckDataString(oasisDataString);
        }

        #endregion

        #region IService Members

        public bool Ping()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}