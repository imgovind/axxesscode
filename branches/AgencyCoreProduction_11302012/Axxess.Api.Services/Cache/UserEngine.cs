﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Contracts;

    public sealed class UserEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly UserEngine instance = new UserEngine();
        }

        #endregion

        #region Public Instance

        public static UserEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private SafeList<UniqueEntity> users;

        #endregion

        #region Private Constructor / Methods

        private UserEngine()
        {
            this.Load();
        }

        private void Load()
        {
            this.users = new SafeList<UniqueEntity>();
            var userList = DataProvider.GetUserNames();
            if (userList != null && userList.Count > 0)
            {
                userList.ForEach(user =>
                {
                    this.users.Add(new UniqueEntity { Id = user.Id, AgencyId = user.AgencyId, Name = user.DisplayName });
                });
            }
        }

        private void Load(Guid agencyId)
        {
            var userList = DataProvider.GetUserNames(agencyId);
            if (userList != null && userList.Count > 0)
            {
                userList.ForEach(user =>
                {
                    var item = this.users.Single(u => u.Id.ToString().IsEqual(user.Id.ToString()) && u.AgencyId.ToString().IsEqual(user.AgencyId.ToString()));
                    if (item == null)
                    {
                        this.users.Add(new UniqueEntity { Id = user.Id, AgencyId = user.AgencyId, Name = user.DisplayName });
                    }
                    else
                    {
                        item.Name = user.DisplayName;
                    }
                });
            }
        }

        #endregion

        #region Public Methods

        public string GetName(Guid userId, Guid agencyId)
        {
            var user = this.users.Single(p => p.Id.ToString().IsEqual(userId.ToString()) && p.AgencyId.ToString().IsEqual(agencyId.ToString()));
            if (user != null)
            {
                return user.Name;
            }
            return string.Empty;
        }

        public void Refresh(Guid agencyId)
        {
            Load(agencyId);
        }

        public List<UserData> GetUsers()
        {
            var userList = new List<UserData>();
            this.users.ForEach(u =>
            {
                userList.Add(new UserData
                {
                    Id = u.Id,
                    DisplayName = u.Name,
                    AgencyId = u.AgencyId
                });
            });
            return userList;
        }

        #endregion

    }
}
