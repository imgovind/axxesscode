﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    internal class NovaetusDataSpecification : IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal NovaetusDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<Dictionary<string, string>> GetItems(List<int> paymentSources)
        {
            var result = new List<Dictionary<string, string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);

                List<EpisodeData> servedEpisodes = null;
                if (paymentSources != null && paymentSources.Count > 0)
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast, paymentSources);
                }
                else
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast);
                }
                if (servedEpisodes != null && servedEpisodes.Count > 0)
                {
                    Windows.EventLog.WriteEntry(string.Format("Patient Episodes for Agency: {0}", servedEpisodes.Count), System.Diagnostics.EventLogEntryType.Information);
                    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                    if (patientIds != null && patientIds.Count > 0)
                    {
                        var totalServed = patientIds.Count;
                        Windows.EventLog.WriteEntry(string.Format("Total Patients Served: {0}", totalServed), System.Diagnostics.EventLogEntryType.Information);
                        patientIds.ForEach(patientId =>
                        {
                            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                            if (episodes != null && episodes.Count > 0)
                            {
                                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);

                                var patientData = episodes.FirstOrDefault();
                                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                                if (assessment != null)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var collection = new Dictionary<string, string>();

                                    collection.Add("NovaetusClientNo", agency.CahpsVendorClientId.IsNotNullOrEmpty() ? agency.CahpsVendorClientId : "M");
                                    collection.Add("provider-name", agency.Name.IsNotNullOrEmpty() ? agency.Name : "M");
                                    collection.Add("provider-id", agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "M");
                                    collection.Add("npi", agency.NationalProviderNumber.IsNotNullOrEmpty() ? agency.NationalProviderNumber : "M");
                                    collection.Add("SampleMonth", sampleMonth.ToString());
                                    collection.Add("SampleYear", sampleYear.ToString());
                                    collection.Add("TotalPatientsServed", totalServed.ToString());
                                    collection.Add("FirstName", assessmentData.AnswerOrDefault("M0040FirstName", "M"));
                                    collection.Add("MiddleInitial", assessmentData.AnswerOrEmptyString("M0040MI"));
                                    collection.Add("LastName", assessmentData.AnswerOrDefault("M0040LastName", "M"));
                                    collection.Add("Gender", assessmentData.AnswerOrDefault("M0069Gender", "M"));
                                    collection.Add("DOB", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().IsValid() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("dd/MM/yyyy") : "M");
                                    collection.Add("Address 1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : "M");
                                    collection.Add("Address 2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : "M");
                                    collection.Add("City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : "M");
                                    collection.Add("State", assessmentData.AnswerOrDefault("M0050PatientState", "M"));
                                    collection.Add("Zip", assessmentData.AnswerOrDefault("M0060PatientZipCode", "M"));
                                    collection.Add("Telephone", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : "M");
                                    collection.Add("MedicalRecordNumber", assessmentData.AnswerOrDefault("M0020PatientIdNumber", "M"));
                                    collection.Add("SSN", assessmentData.AnswerOrEmptyString("M0064PatientSSNUnknown").IsEqual("1") ? "M" : (assessmentData.AnswerOrDefault("M0064PatientSSN", "M")));
                                    collection.Add("NumberOfSkilledVisits", skilledVisitCount);
                                    collection.Add("LookbackPeriodVisits", lookbackVisitCount);
                                    collection.Add("AdmissionSourceHospital", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") || assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdmissionSourceRehab", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdmissionSourceSNF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission SourceOtherNursingFacility", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission SourceOtherInpatientFacility", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                    collection.Add("AdmisionSourceCommunity", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");
                                    collection.Add("PayerMedicare", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("PayerMedicaid", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("PayerPrivate", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                                    collection.Add("PayerOther", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                                    collection.Add("HMOIndicator", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("DuallyEligibleforMedicareMedicaid", CheckForDualElligibility(assessmentData));
                                    collection.Add("PrimaryDiagnosis", assessmentData.AnswerOrDefault("M1020ICD9M", "M"));
                                    collection.Add("OtherDiagnosis1", assessmentData.AnswerOrDefault("M1022ICD9M1", "M"));
                                    collection.Add("OtherDiagnosis2", assessmentData.AnswerOrDefault("M1022ICD9M2", "M"));
                                    collection.Add("OtherDiagnosis3", assessmentData.AnswerOrDefault("M1022ICD9M3", "M"));
                                    collection.Add("OtherDiagnosis4", assessmentData.AnswerOrDefault("M1022ICD9M4", "M"));
                                    collection.Add("OtherDiagnosis5", assessmentData.AnswerOrDefault("M1022ICD9M5", "M"));
                                    collection.Add("CareRelatedtoSurgicalDischarge", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "0");
                                    collection.Add("EndStageRenalDisiease", CheckForEndStageRenalDisiease(assessmentData));
                                    collection.Add("ADLDeficits", CheckForADLDeficits(assessmentData));
                                    collection.Add("ADLDressUpper", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : "M");
                                    collection.Add("ADLDressLower", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : "M");
                                    collection.Add("ADLBathing", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : "M");
                                    collection.Add("ADLToileting", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : "M");
                                    collection.Add("ADLTransferring", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : "M");
                                    collection.Add("ADLFeed", assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : "M");
                                    collection.Add("Type of Sampling", "2");
                                    collection.Add("Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));

                                    result.Add(collection);
                                }
                            }
                        });
                    }
                }
                else
                {
                    Windows.EventLog.WriteEntry(string.Format("No Patients found for AgencyId: {0}", agency.Name), System.Diagnostics.EventLogEntryType.Warning);
                }
            }
            else
            {
                Windows.EventLog.WriteEntry("Agency is null.", System.Diagnostics.EventLogEntryType.Error);
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "1";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "2";
                }
                else if (language.ToLowerInvariant().Contains("chinese"))
                {
                    result = "3";
                }
                else if (language.ToLowerInvariant().Contains("russian"))
                {
                    result = "4";
                }
                else if (language.ToLowerInvariant().Contains("vietnamese"))
                {
                    result = "5";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        private string CheckForDualElligibility(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if ((data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1"))
                   && (data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1")))
                {
                    result = "1";
                }
            }

            return result;
        }

        #endregion
    }
}
