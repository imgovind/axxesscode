﻿namespace Axxess.Api.Services
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;

    public class ReportService : BaseService, IReportService
    {
        #region IReportService Members
        
        public List<Dictionary<string, string>> CahpsExport(Guid agencyId ,Guid agencyLocationId, int sampleMonth, int sampleYear)
        {
            return CahpsExportByPaymentSources(agencyId, agencyLocationId, sampleMonth, sampleYear, new List<int>());
        }

        public List<Dictionary<string, string>> CahpsExportByPaymentSources(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var collection = new List<Dictionary<string, string>>();
            IDataSpecification dataSpecification = null;

            try
            {
                var agency = Reports.GetAgencyLocation(agencyId, agencyLocationId)??new AgencyData();
                if (!agency.IsLocationStandAlone)
                {
                    agency = Reports.GetAgency(agencyId)??new AgencyData();
                }
                agency.LocationId = agencyLocationId;
                
                if (agency != null)
                {
                    switch (agency.CahpsVendor)
                    {
                        case 1: // DSS Research
                            dataSpecification = new DssResearchDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case 2: // Arkansas
                            break;
                        case 3: // Novaetus
                            dataSpecification = new NovaetusDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)4: // Deyta
                        case (int)6: // Fields Research
                            dataSpecification = new DeytaDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)5: // Pinnacle
                            dataSpecification = new PinnacleDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)7: // PressGaney
                            dataSpecification = new PressGaneyDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)8: // Ocs
                            dataSpecification = new OcsDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)9: // Fazzi
                            dataSpecification = new FazziDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)10: // Strategic Healthcare
                            dataSpecification = new ShpDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collection;
        }

        #endregion
    }
}