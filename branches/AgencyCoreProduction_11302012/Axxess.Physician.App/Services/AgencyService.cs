﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class AgencyService : IAgencyService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region IAgencyService Members

        #endregion

    }
}
