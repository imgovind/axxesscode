﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reset Password - Axxess MD</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="forgotPassword-wrapper">
        <div id="forgotpassword-window">
            <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Physician Portal - Forgot Password</span></div>
            <div class="box">
                <div id="messages"></div>
                <div id="forgotPasswordFormContainer">
                    <% using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "forgotPasswordForm", @class = "forgotPassword" })) %>
                    <% { %>
                    <div class="row">
                        <%= Html.LabelFor(a => a.EmailAddress)%>
                        <%= Html.TextBoxFor(a => a.EmailAddress, new { @class = "required" })%>
                    </div>
                    <div class="row">
                        <%= Html.LabelFor(a => a.captchaValid) %>
                        <span>Enter both words below, separated by a space. </span>
                        <%= Html.GenerateCaptcha() %>
                    </div>
                    <div class="row tr">
                        <input type="button" value="Send" onclick="$(this).closest('form').submit();" class="button" style="width: 90px!important;" />
                        <br />
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(false)
         .DefaultGroup(group => group
             .Add("jquery-1.7.1.min.js")
             .Add("Plugins/Other/blockui.min.js")
             .Add("Plugins/Other/form.min.js")
             .Add("Plugins/Other/validate.min.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
             .Compress(true)
             .Combined(true)
             .CacheDurationInDays(1)
             .Version(Current.AssemblyVersion))
        .OnDocumentReady(() =>
        { 
    %>
    ResetPassword.Init();
    <% 
        }).Render(); %>
</body>
</html>
