﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Face-To-Face Encounters</span>
<div class="wrapper">
    <%= Html.Telerik().Grid<FaceToFaceEncounter>().Name("List_FaceToFace").ToolBar(commnds => commnds.Custom()).Columns(columns => {
        columns.Bound(f => f.AgencyName).Title("Agency").Sortable(true);
        columns.Bound(f => f.DisplayName).Title("Patient").Sortable(true);
        columns.Bound(f => f.StatusName).Title("Status").Sortable(true);
        columns.Bound(f => f.RequestDateFormatted).Title("Request Date").Sortable(true).Width(100);
        columns.Bound(f => f.PrintUrl).Title(" ").Width(35);
        columns.Bound(f => f.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditFaceToFaceEncounter('<#=AgencyId#>','<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Edit</a>").Title("Action").Width(150);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("FaceToFaceGrid", "Order")).Pageable(paging => paging.PageSize(100)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>