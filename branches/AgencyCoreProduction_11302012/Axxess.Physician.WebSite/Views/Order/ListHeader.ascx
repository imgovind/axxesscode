﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons">
    <ul>
        <li><a class="groupLink" onclick="Order.Load('AgencyName');">Group By Agency</a></li>
        <li><a class="groupLink" onclick="Order.Load('PatientName');">Group By Patient</a></li>
        <li><a class="groupLink" onclick="Order.Load('TypeDescription');">Group By Type</a></li>
        <li><a class="groupLink" onclick="Order.Load('OrderDate');">Group By Date</a></li>
    </ul>
</div>
<div id="orderListContent"><% Html.RenderPartial("~/Views/Order/ListView.ascx"); %></div>