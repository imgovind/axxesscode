﻿using System;
using System.IO;
using System.Linq;
using System.Data;

using HtmlAgilityPack;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class VisiTrackPhysicianIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\DoctorListing.HTM");
        private static string output = Path.Combine(App.Root, "Files\\HomeCareSolutionsDoctorListing.txt");

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.Load(input);

                int counter = 0;
                int columnNumber = 1;
                AgencyPhysicianData physicianData = null;
                htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                {
                    if (htmlNode.Name.IsEqual("html"))
                    {
                        htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                        {
                            if (bodyNode.Name.IsEqual("body"))
                            {
                                bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                {
                                    if (tableNode.IsPhysicianDataRow())
                                    {
                                        physicianData = new AgencyPhysicianData();
                                        physicianData.AgencyId = "c3b5d1cc-d98d-4deb-99f8-8fb5cb0166c7";

                                        tableNode.ChildNodes.ToList().ForEach(rowNode =>
                                        {
                                            if (rowNode.Name.IsEqual("tr"))
                                            {
                                                rowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                {
                                                    if (dataNode.Name.IsEqual("td"))
                                                    {
                                                        dataNode.SetPhysicianData(columnNumber, physicianData);
                                                        columnNumber++;
                                                    }
                                                });
                                            }
                                        });
                                        columnNumber = 1;
                                        textWriter.WriteLine(new AgencyPhysicianScript(physicianData).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                        Console.WriteLine("{0}) {1} {2}", ++counter, physicianData.FirstName, physicianData.LastName);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }
}
