﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    
    using Kent.Boogaart.KBCsv;
    using Axxess.Core.Extension;

    public static class ManualCsvIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\blessedhhs.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Blessedhhs_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "4bde6fa8-76d0-456b-aa5e-faeeafa252db";
                                patientData.AgencyLocationId = "40dd61aa-e5cf-44e1-a3c8-893742ef283d";
                                patientData.PatientNumber = record.GetValue(20);
                                patientData.Gender = record.GetValue(11).IsEqual("2") ? "Female" : "Male";
                                patientData.MedicareNumber = "";
                                patientData.FirstName = record.GetValue(8).ToTitleCase();
                                patientData.LastName = record.GetValue(10).ToTitleCase();
                                patientData.MiddleInitial = record.GetValue(9).ToTitleCase();
                                patientData.BirthDate = record.GetValue(12).ToMySqlDate(0);
                                patientData.AddressLine1 = record.GetValue(14).ToTitleCase();
                                patientData.AddressLine2 = record.GetValue(15).ToTitleCase();
                                patientData.AddressCity = record.GetValue(16).ToTitleCase();
                                patientData.AddressState = record.GetValue(17);
                                patientData.AddressZipCode = record.GetValue(18).Substring(0, 5);
                                patientData.StartofCareDate = record.GetValue(13).ToMySqlDate(0);
                                patientData.Phone = record.GetValue(19).ToPhoneDB();
                                patientData.PatientStatusId = "1" ;
                                patientData.Comments = string.Format("Primary Diagnosis Code: {0}. ", record.GetValue(44));
                                patientData.Comments += string.Format("Other Diagnosis Code 1: {0}. ", record.GetValue(45));
                                patientData.Comments += string.Format("Other Diagnosis Code 2: {0}. ", record.GetValue(46));
                                patientData.Comments += string.Format("Other Diagnosis Code 3: {0}. ", record.GetValue(47));
                                patientData.Comments += string.Format("Other Diagnosis Code 4: {0}. ", record.GetValue(48));
                                patientData.Comments += string.Format("Other Diagnosis Code 5: {0}. ", record.GetValue(49));

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
