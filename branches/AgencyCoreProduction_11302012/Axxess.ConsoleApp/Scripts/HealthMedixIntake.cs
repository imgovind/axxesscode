﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class HealthMedixIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\Algonquin.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Algonquin_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            int counter = 0;
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                PatientData patientData = null;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        patientData = new PatientData();
                                        patientData.AgencyId = "abc2fbb2-807e-4dd0-987f-608e5d75558a";
                                        patientData.AgencyLocationId = "f1a834c7-4ec6-4aba-a4dd-351db2cdb1d0";
                                        patientData.AddressLine1 = dataRow.GetValue(4);
                                        patientData.AddressLine2 = "";
                                        patientData.AddressCity = dataRow.GetValue(6);
                                        patientData.AddressState = dataRow.GetValue(7);
                                        patientData.AddressZipCode = dataRow.GetValue(8);
                                        patientData.AddressCounty = dataRow.GetValue(9);
                                        patientData.MaritalStatus = "Unknown";
                                        patientData.Phone = dataRow.GetValue(12).ToPhoneDB();
                                        patientData.PatientStatusId = "1";

                                        if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                        {
                                            var nameIdArray = dataRow.GetValue(0).Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameIdArray != null && nameIdArray.Length > 1)
                                            {
                                                patientData.PatientNumber = nameIdArray[1].Replace(")", "").Trim();

                                                var nameArray = nameIdArray[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    patientData.LastName = nameArray[0].Trim();
                                                    var firstMiddleArray = nameArray[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (firstMiddleArray != null && firstMiddleArray.Length > 1)
                                                    {
                                                        patientData.FirstName = firstMiddleArray[0];
                                                        patientData.MiddleInitial = firstMiddleArray[1];
                                                    }
                                                    else
                                                    {
                                                        patientData.FirstName = nameArray[1].Trim();
                                                    }
                                                }
                                            }
                                        }

                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                        counter++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
