﻿using System;
using System.Data;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp
{
    public static class DataRowExtensions
    {
        public static string GetValue(this DataRow dataRow, int columnIndex)
        {
            if (dataRow != null && columnIndex < dataRow.ItemArray.Length)
            {
                if (dataRow[columnIndex] != null && dataRow[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRow[columnIndex].ToString().Trim().Replace("'", "");
                }
            }
            return string.Empty;
        }

        public static bool IsEmpty(this DataRow dataRow)
        {
            var result = true;
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                {
                    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public static string ToPhoneDB(this string text)
        {
            if (text.IsNotNullOrEmpty() && text.Length >= 10)
            {
                return text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            }
            return string.Empty;
        }

        public static string ToMySqlDate(this string text, int addDays)
        {
            if (text.IsNotNullOrEmpty() && text.Length > 6)
            {
                var date = text.Replace("-","").Trim().ToDateTime();
                if (addDays > 0)
                {
                    return date.AddDays(addDays).ToString("yyyy-M-d");
                }
                return date.ToString("yyyy-M-d");
            }
            return string.Empty;
        }

        public static string ToMySqlDateString(this string text)
        {
            if (text.IsNotNullOrEmpty())
            {
                if (text.Length == 7)
                {
                    var dateTime = new DateTime(int.Parse(text.Substring(3, 4)), int.Parse(text.Substring(0, 1)), int.Parse(text.Substring(1, 2)));
                    return dateTime.ToString("yyyy-M-d");
                }
                else if (text.Length == 8)
                {
                    var dateTime = new DateTime(int.Parse(text.Substring(4, 4)), int.Parse(text.Substring(0, 2)), int.Parse(text.Substring(2, 2)));
                    return dateTime.ToString("yyyy-M-d");
                }
               
            }
            return string.Empty;
        }

        public static bool IsCBSAValid(this DataRow dataRow, params string[] inputs)
        {
            bool result = true;
            if (dataRow != null && inputs.Length > 0)
            {
                foreach (var input in inputs)
                {
                    if (input.IsNullOrEmpty())
                    {
                        return false;
                    }
                }
            }
            return result;
        }
    }
}
