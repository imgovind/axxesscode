﻿namespace Axxess.ConsoleApp
{
    using System;

    public class AgencyPhysicianData
    {
        #region Constructor

        public AgencyPhysicianData()
        {
            Created = "CURDATE()";
            Modified = "CURDATE()";
            Comments = string.Empty;

            Id = Guid.NewGuid();
            LoginId = Guid.Empty;
        }

        #endregion

        #region Members

        public Guid Id { get; set; }
        public string AgencyId { get; set; }
        public Guid LoginId { get; set; }
        public string NPI { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Credentials { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZipCode { get; set; }
        public string StartofCareDate { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneAlternate { get; set; }
        public string FaxNumber { get; set; }
        public string LicenseNumber { get; set; }
        public string Comments { get; set; }

        public string Created { get; set; }
        public string Modified { get; set; }

        #endregion
    }
}
