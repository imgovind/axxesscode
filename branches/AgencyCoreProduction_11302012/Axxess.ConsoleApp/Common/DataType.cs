﻿using System;
namespace Axxess.ConsoleApp
{
    public enum DataType : byte
    {
        Text,
        Number,
        Date
    }
}
