﻿using System;
using System.Linq;
using System.Data;

using HtmlAgilityPack;
using Axxess.Core.Extension;

namespace Axxess.ConsoleApp
{
    public static class HtmlNodeExtensions
    {
        public static void SetPatientData(this HtmlNode node, int tableNumber, int columnNumber, PatientData patient)
        {
            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        if (columnNumber == 2)
                        {
                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (firstName.Length > 1)
                            {
                                patient.FirstName = firstName[0].Trim();
                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                            }
                            else
                            {
                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                            }
                        }
                        if (columnNumber == 3)
                        {
                            patient.StartofCareDate = node.InnerText.ToMySqlDate();
                        }
                        if (columnNumber == 4)
                        {
                            if (node.InnerText.IsNotNullOrEmpty())
                            {
                                var dateRange = node.InnerText.Split('-');
                                if (dateRange.Length > 1)
                                {
                                    patient.EpisodeStart = dateRange[0].ToMySqlDate();
                                    patient.EpisodeEnd = dateRange[1].ToMySqlDate();

                                    if (patient.EpisodeEnd.IsNotNullOrEmpty())
                                    {
                                        var episodeEndDate = dateRange[1].ToDateTime();
                                        if (episodeEndDate.Year < 2011)
                                        {
                                            patient.PatientStatusId = "2";
                                        }
                                    }
                                }
                                patient.Comments += string.Format("Last Certification Period: {0}. ", node.InnerText);
                            }
                        }
                        if (columnNumber == 5)
                        {
                            patient.Phone = node.InnerText.ToPhoneDB();
                        }

                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                {
                                    patient.PrimaryInsurance = "1";
                                }
                                else
                                {
                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Trim());
                                }
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Acuity Level: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        break;
                    case 2:
                        if (columnNumber == 2)
                        {
                            patient.PatientNumber = node.InnerText.Trim().Replace("MR# ", "");
                        }
                        if (columnNumber == 5)
                        {

                            var address = node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "");
                            var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (addressArray.Length > 1)
                            {
                                patient.AddressLine1 = addressArray[0];
                                var cityStateZip = addressArray[1].Split(',');
                                if (cityStateZip.Length > 1)
                                {
                                    patient.AddressCity = cityStateZip[0];

                                    var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    if (stateZip.Length > 1)
                                    {
                                        patient.AddressState = stateZip[0];
                                        patient.AddressZipCode = stateZip[1];
                                    }
                                }
                            }
                        }
                        if (columnNumber == 6)
                        {
                            if (patient.PrimaryInsurance.IsEqual("1"))
                            {
                                patient.MedicareNumber = node.InnerText.Trim();
                            }
                            else
                            {
                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Code: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Trim());
                            }
                        }
                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("CM Phone: {0}. ", node.InnerText.Trim());
                            }
                        }

                        break;
                    case 3:
                        if (columnNumber == 2)
                        {
                            patient.BirthDate = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                        }

                        break;
                }
            }
        }

        public static void SetPhysicianData(this HtmlNode node, int columnNumber, AgencyPhysicianData physician)
        {
            if (node != null && node.InnerHtml.IsNotNullOrEmpty() && physician != null)
            {
                var nodeValue = node.InnerHtml.Trim().Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "");
                if (columnNumber == 2)
                {
                    physician.LastName = node.InnerText.Split(',')[0].Trim();

                    var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (firstName.Length > 1)
                    {
                        physician.FirstName = firstName[0].Trim();
                        physician.MiddleName = firstName[1].Trim().Replace(".", "");
                    }
                    else
                    {
                        physician.FirstName = node.InnerText.Split(',')[1].Trim();
                    }
                }
                if (columnNumber == 4)
                {
                    physician.AddressLine1 = nodeValue;
                }
                if (columnNumber == 5)
                {
                    physician.AddressCity = nodeValue;
                }
                if (columnNumber == 6)
                {
                    physician.AddressState = nodeValue;
                }
                if (columnNumber == 7)
                {
                    physician.AddressZipCode = nodeValue;
                }
                if (columnNumber == 8)
                {
                    physician.PhoneWork = nodeValue.ToPhoneDB();
                }
                if (columnNumber == 9)
                {
                    physician.FaxNumber = nodeValue.ToPhoneDB();
                }

                if (columnNumber == 11)
                {
                    physician.NPI = nodeValue;
                }

                if (columnNumber == 12)
                {
                    physician.LicenseNumber = nodeValue;
                }

                if (columnNumber == 13 && nodeValue.IsNotNullOrEmpty())
                {
                    physician.Comments = string.Format("Tax Id: {0}. ", nodeValue);
                }

            }
        }

        public static string ToMySqlDate(this string text)
        {
            if (text.IsNotNullOrEmpty() && text != " ")
            {
                var date = text.ToDateTime();
                return date.ToString("yyyy-M-d");
            }
            return string.Empty;
        }

        public static bool IsDataRow(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Name"))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsPhysicianDataRow(this HtmlNode node)
        {
            int count = 0;
            if (node != null && node.Name.IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1 && dataNode.InnerText.IsEqual("Doctor"))
                                {
                                    count--;
                                }
                                if (count == 3 && dataNode.InnerText.IsEqual("Address"))
                                {
                                    count--;
                                }
                                else
                                {
                                    count++;
                                }
                            }
                        });
                    }
                });

                if (count == 13)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
