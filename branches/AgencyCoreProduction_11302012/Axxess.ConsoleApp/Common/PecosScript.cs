﻿namespace Axxess.ConsoleApp
{
    public class PecosScript
    {
        private PecosData pecosData;
        private const string PECOS_INSERT = "INSERT INTO `pecosphysicians_copy`(`Id`,`Last`,`First`) VALUES ('{0}', '{1}', '{2}');";

        public PecosScript(PecosData pecosData)
        {
            this.pecosData = pecosData;
        }

        public override string ToString()
        {
            return string.Format(PECOS_INSERT, pecosData.Id, pecosData.LastName, pecosData.FirstName);
        }
    }
}
