﻿namespace Axxess.LookUp.Domain
{
    using System;
    using Axxess.Core.Extension;

    [Serializable]
    public class DiagnosisCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }

        public string FormatCode
        {
            get
            {
                string formattedCode = "";
                if (Code.IsNotNullOrEmpty())
                {
                    if (Code.Trim().StartsWith("E") && Code.Trim().Length >= 0)
                    {
                        formattedCode = string.Format("{0}.{1}", this.Code.Substring(0, 4).PadLeft(4, '0'), this.Code.Substring((4)));
                    }
                    else if (Code.Trim().Length > 0)
                    {
                        formattedCode = string.Format("{0}.{1}", this.Code.Substring(0, 3).PadLeft(3, '0'), this.Code.Substring((3)));
                    }
                    else
                    {
                        formattedCode = string.Empty;
                    }
                }
                return formattedCode;
            }
        }

        public string FormatCodeTrim
        {
            get
            {
                string formattedCode = "";
                if (Code.IsNotNullOrEmpty())
                {
                    if (Code.Trim().StartsWith("E") && Code.Trim().Length >= 0)
                    {
                        formattedCode = string.Format("{0}.{1}", this.Code.Substring(0, 4).PadLeft(4, '0'), this.Code.Substring((4)));
                    }
                    else if (Code.Trim().Length > 0)
                    {
                        formattedCode = string.Format("{0}.{1}", this.Code.Substring(0, 3).PadLeft(3, '0'), this.Code.Substring((3)));
                    }
                    else
                    {
                        formattedCode = string.Empty;
                    }
                }
                return formattedCode.TrimEndPeriod();
            }
        }
    }
}
