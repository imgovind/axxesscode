﻿namespace Axxess.LookUp.Domain
{
    public class PecosPhysician
    {
        public string Id { get; set; }
        public string Last { get; set; }
        public string First { get; set; }
    }
}
