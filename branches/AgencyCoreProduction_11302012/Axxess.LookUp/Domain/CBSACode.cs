﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
    public class CBSACode
    {
        public int Id { get; set; }
        public string Zip { get; set; }
        public string CityName { get; set; }
        public string CountyCode { get; set; }
        public string CountyTitle { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string CBSA { get; set; }
        public double WITwoSeven { get; set; }
        public double WITwoEight { get; set; }
        public double WITwoNine { get; set; }
        public double WITwoTen { get; set; }
        public double WITwoEleven { get; set; }
        public double WITwoTwelve { get; set; }
        public double Variance { get; set; }
    }
}
