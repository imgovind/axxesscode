﻿namespace Axxess.AgencyManagement.SupportApp.Security
{
    using System;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Domain;
    
    [Serializable]
    public class AxxessSupportPrincipal : IPrincipal
    {
        #region Private Members

        private Roles roleId;
        private AxxessSupportIdentity identity;

        #endregion

        #region Constructor

        public AxxessSupportPrincipal(AxxessSupportIdentity identity)
        {
            this.identity = identity;
        }

        #endregion

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return identity; }
        }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
