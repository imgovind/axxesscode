﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;

    public interface IAssessment
    {
        Guid AgencyId { get; set; }
        Guid Id { get; set; }
        Guid PatientId { get; set; }
        Guid EpisodeId { get; set; }
        Guid UserId { get; set; }
        int Status { get; set; }
        string OasisData { get; set; }
        string ClaimKey { get; set; }
        string HippsCode { get; set; }
        string HippsVersion { get; set; }
        string SubmissionFormat { get; set; }
        string CancellationFormat { get; set; }
        int VersionNumber { get; set; }
        string Supply { get; set; }
        string SignatureText { get; set; }
        string TimeIn { get; set; }
        string TimeOut { get; set; }
        bool IsDeprecated { get; set; }
        bool IsValidated { get; set; }
        DateTime AssessmentDate { get; set; }
        DateTime SignatureDate { get; set; }
        DateTime ExportedDate { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }

        AssessmentType Type { get; set; }
        string MedicationProfile { get; set; }
        string PatientName { get; set; }
        string Insurance { get; set; }
        bool IsNew { get; set; }
        string ValidationError { get; set; }
        List<Question> Questions { get; set; }
        //int Version { get; set; }
       

    }
}
