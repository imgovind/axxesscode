﻿namespace Axxess.Api.Membership
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    public partial class AuthenticationWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public AuthenticationWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "AuthenticationService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(AuthenticationService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("Authentication Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("Authentication Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
