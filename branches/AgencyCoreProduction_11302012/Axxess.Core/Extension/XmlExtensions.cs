﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Diagnostics;

    using Axxess.Core.Infrastructure;
    using System.Collections;
    using System.Text;

    public static class XmlExtensions
    {
        [DebuggerStepThrough]
        public static string ToXml<T>(this T instance)
        {
            if (instance == null)
                return string.Empty;

            return Serializer.Serialize(instance);
        }

        [DebuggerStepThrough]
        public static T ToObject<T>(this string text)
        {
            if (text.IsNullOrEmpty())
                return default(T);
            return Serializer.Deserialize<T>(text);
        }

        [DebuggerStepThrough]
        public static string SanitizeXmlString(this string xml)
        {
            if (xml.IsNullOrEmpty())
            {
                return string.Empty;
            }

            StringBuilder buffer = new StringBuilder(xml.Length);

            foreach (char c in xml)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
                else
                {
                    buffer.Append(" ");
                }
            }
            return buffer.ToString();
        }


        [DebuggerStepThrough]
        public static bool IsLegalXmlChar(this int character)
        {
            return 
            (
                character == 0x9                                ||
                character == 0xA                                ||
                character == 0xD                                ||
               (character >= 0x20    && character <= 0xD7FF  )  ||
	           (character >= 0xE000  && character <= 0xFFFD  )  ||
	           (character >= 0x10000 && character <= 0x10FFFF)
            );
        }
    }
}
