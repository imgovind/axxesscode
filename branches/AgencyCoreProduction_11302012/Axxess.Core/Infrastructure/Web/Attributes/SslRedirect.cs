﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using Extension;

    public class SslRedirect : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CoreSettings.UseSSL)
            {
                if (!filterContext.HttpContext.Request.IsSecureConnection)
                {
                    string path = filterContext.HttpContext.Request.Url.AbsolutePath.Substring(1);

                    if (!path.StartsWithAny(CoreSettings.NonSecurePaths))
                    {
                        filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.Url.ToString().ToLower().Replace("http:", "https:"));
                        filterContext.Result.ExecuteResult(filterContext);
                    }
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }

}
