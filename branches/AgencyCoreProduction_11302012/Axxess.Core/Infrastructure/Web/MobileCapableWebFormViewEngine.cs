﻿namespace Axxess.Core.Infrastructure {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    public class MobileCapableWebFormViewEngine : WebFormViewEngine {
        private bool MobileBrowserCheck(HttpRequestBase Request) {
            String UserAgent = Request.UserAgent.ToLower(System.Globalization.CultureInfo.CurrentCulture);
            String[] MobileOS = { "iphone", "ipod", "ipad", "android", "blackberry", "webos" };
            return Request.Browser.IsMobileDevice || MobileOS.Any(OS => UserAgent.Contains(OS));
        }
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache) {
            ViewEngineResult result = null;
            if (CoreSettings.MobileRedirect && (MobileBrowserCheck(controllerContext.HttpContext.Request) || CoreSettings.MobileTestEnvironment)) result = base.FindView(controllerContext, "Mobile/" + viewName, masterName, useCache);
            if (result == null || result.View == null) result = base.FindView(controllerContext, viewName, masterName, useCache);
            return result;
        }
        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache) {
            ViewEngineResult result = null;
            if (CoreSettings.MobileRedirect && (MobileBrowserCheck(controllerContext.HttpContext.Request) || CoreSettings.MobileTestEnvironment)) result = base.FindPartialView(controllerContext, "Mobile/" + partialViewName, useCache);
            if (result == null || result.View == null) result = base.FindPartialView(controllerContext, partialViewName, useCache);
            return result;
        }
    }
}
