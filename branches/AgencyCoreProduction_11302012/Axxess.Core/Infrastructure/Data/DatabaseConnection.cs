﻿namespace Axxess.Core.Infrastructure
{
    using MySql.Data.MySqlClient;

    public class DatabaseConnection
    {
        public MySqlConnection Connection { get; set; }

        public DatabaseConnection(string connectionString)
        {
            this.Connection = new MySqlConnection(connectionString);
            this.Connection.Open();
        }
    }

}
