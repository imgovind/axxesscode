﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Text;
    using System.Security.Cryptography;

    using Extension;

    public sealed class SaltedHash
    {
        #region Private Members
        private HashAlgorithm hashProvider;
        private int saltLength;
        #endregion

        #region Constructors
        public SaltedHash(HashAlgorithm HashAlgorithm, int theSaltLength)
        {
            hashProvider = HashAlgorithm;
            saltLength = theSaltLength;
        }

        public SaltedHash()
            : this(new SHA512Managed(), 15)
        {
        }
        #endregion

        #region Private Methods

        private byte[] ComputeHash(byte[] data, byte[] salt)
        {
            byte[] dataAndSalt = new byte[data.Length + saltLength];

            Array.Copy(data, dataAndSalt, data.Length);
            Array.Copy(salt, 0, dataAndSalt, data.Length, saltLength);

            return hashProvider.ComputeHash(dataAndSalt);
        }
        #endregion

        #region  Public Methods
        public void GetHashAndSalt(string password, out string hash, out string salt)
        {
            byte[] hashBytes;
            byte[] saltBytes = new byte[saltLength];

            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
            random.GetNonZeroBytes(saltBytes);

            Encoding encoding = Encoding.GetEncoding("Windows-1252");

            hashBytes = ComputeHash(encoding.GetBytes(password), saltBytes);

            hash = Convert.ToBase64String(hashBytes);
            salt = Convert.ToBase64String(saltBytes);
        }

        public bool VerifyHashAndSalt(string data, string hash, string salt)
        {
            if (data.IsNotNullOrEmpty() && hash.IsNotNullOrEmpty() && salt.IsNotNullOrEmpty())
            {
                byte[] hashToVerify = Convert.FromBase64String(hash);
                byte[] saltToVerify = Convert.FromBase64String(salt);
                byte[] dataToVerify = Encoding.UTF8.GetBytes(data);

                byte[] newHash = ComputeHash(dataToVerify, saltToVerify);

                if (newHash.Length != hashToVerify.Length) return false;

                for (int i = 0; i < hashToVerify.Length; i++)
                    if (!hashToVerify[i].Equals(newHash[i]))
                        return false;

                return true;
            }

            return false;
        }
        #endregion
    }
}
