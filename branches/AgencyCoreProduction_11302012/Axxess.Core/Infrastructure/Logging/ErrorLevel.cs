﻿using System;

namespace Axxess.Core.Infrastructure
{
    public enum ErrorLevel
    {
        None,
        Trivial,
        Regular,
        Important,
        Critical
    }
}
