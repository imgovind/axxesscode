﻿namespace Axxess.Core
{
    using System;

    public class Permission
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public string Category { get; set; }
        public string Tip { get; set; }
    }
}
