﻿namespace Axxess.Core
{
    using System;
    using System.ComponentModel;

    [TypeConverter(typeof(ShortGuidConverter))]
    public class ShortGuid : IComparable<ShortGuid>
    {
        private string shortGuid;
        public ShortGuid(string shortGuid)
        {
            if (shortGuid.Length != 8)
            {
                throw new FormatException("Input string was not the correct format");
            }
            this.shortGuid = shortGuid;
        }

        public override string ToString()
        {
            return shortGuid;
        }

        public static ShortGuid NewId()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            return new ShortGuid(string.Format("{0:x}", i - DateTime.Now.Ticks).Substring(0, 8));
        }

        public static ShortGuid Empty
        {
            get
            {
                return new ShortGuid("00000000");
            }
        }

        #region IComparable<ShortGuid> Members

        public int CompareTo(ShortGuid other)
        {
            return string.Compare(this.ToString(), other.ToString());
        }

        #endregion
    }
}