﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Axxess.Membership.Domain.Login>" %>
<% using (Html.BeginForm("UpdateUserEmail", "Agency", FormMethod.Post, new { @id = "editEmailForm" })) {%>
<%= Html.Hidden("LoginId", Model.Id, new { @id = "Edit_Email_LoginId" }) %>
<div class="form-wrapper">
    <fieldset>
        <legend>Change Email Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Email_DisplayName">Dashboard Name:</label><div class="float-right"><%=Html.TextBox("DisplayName", Model.DisplayName, new { @id = "Edit_Email_DisplayName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Email_EmailAddress">E-mail Address:</label><div class="float-right"><%=Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Email_EmailAddress", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editagencyuser');">Close</a></li>
    </ul></div>
</div>
<% } %>

<% using (Html.BeginForm("UpdateUserPassword", "Agency", FormMethod.Post, new { @id = "editPasswordForm" })) {%>
<%= Html.Hidden("LoginId", Model.Id, new { @id = "Edit_Password_LoginId" }) %>
<div class="form-wrapper">
    <fieldset>
        <legend>Change Password/Signature</legend>
        <div class="column">
            <div class="row"><label for="Edit_Password_Password">New Password:</label><div class="float-right"><%=Html.TextBox("Password", "", new { @id = "Edit_Password_Password", @class = "text required input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Password_Signature">New Signature:</label><div class="float-right"><%=Html.TextBox("Signature", "", new { @id = "Edit_Password_Signature", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editagencyuser');">Close</a></li>
    </ul></div>
</div>
<% } %>
