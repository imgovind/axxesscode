﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%= string.Format("{0}{1}{2}", "<script type='text/javascript'>acore.renamewindow('New Support Note | ", Model.Name, "','newnote');</script>")%>
<% using (Html.BeginForm("AddNote", "Agency", FormMethod.Post, new { @id = "newNoteForm" })) { %>
<%= Html.Hidden("AgencyId", Model.Id, new { @id = "New_Note_AgencyId" })%>
<%= Html.Hidden("LoginId", Current.LoginId, new { @id = "New_Note_LoginId" })%>
<div class="wrapper main">
    <fieldset>
        <legend>Details</legend>
        <div class="column"><div class="row"><label class="float-left">Customer Support Representative:</label><div class="float-right"><span><%= Current.DisplayName %></span></div></div></div>   
        <div class="column"><div class="row"><label class="float-left">Date & Time of Call:</label><div class="float-right"><%= DateTime.Now.ToString("MM/dd/yyy hh:mm:ss tt") %></div></div></div>
    </fieldset> 
    <fieldset>
        <legend>Notes</legend>
        <table class="form"><tbody><tr><td><label for="New_Note_Comments">Comments:</label><div><%= Html.TextArea("Comments", "", new { @id = "New_Note_Comments", @class="required", @style="height: 300px;" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newnote');">Cancel</a></li>
    </ul></div>
</div>
<%} %>