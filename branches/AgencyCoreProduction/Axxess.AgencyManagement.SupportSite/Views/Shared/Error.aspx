﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title></title>
    <style type="text/css">
        html{height:100%;}
        body{overflow:hidden;background:#d7d7d7;}
        div{position:fixed;top:50%;left:50%;width:30em;height:10em;background-color:#fff;margin:-6em 0 0 -17em;text-align:center;border-radius:1em;-moz-border-radius:1em;-webkit-border-radius:1em;font:1em 'Lucida Grande',Arial,'Liberation Sans',FreeSans,sans-serif;padding:1em;}
        img{vertical-align:middle;}
        #logo{float:left;margin:1.5em 0;}
        em{position:absolute;bottom:1em;right:1em;font-size:.7em;}
    </style>
</head>
<body>
    <div>
        <img src="/images/gui/axxess_logo.jpeg" alt="Axxess Logo" id="logo" />
        <h1><img src="/Images/icons/error.png" /> Application Error</h1>
        <hr />
        <p>There was a problem with the application, please try again.  If problem persists, contact Axxess Healthcare Consult.</p>
        <em>HTTP 500</em>
    </div>
</body>
</html>
