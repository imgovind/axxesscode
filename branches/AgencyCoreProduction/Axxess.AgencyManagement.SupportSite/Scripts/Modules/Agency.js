﻿var Agency = {
    Edit: function(Id) { acore.open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    Delete: function(Id) { U.DeleteTemplate("Agency", Id); },
    InitEdit: function() {
        U.PhoneAutoTab('Edit_Agency_SubmitterPhone');
        U.PhoneAutoTab('Edit_Agency_SubmitterFax');
        $(".names").alpha({ nocaps: false });

        if ($("#Edit_Agency_AxxessBiller").is(':checked')) {
            if ($("#Edit_Agency_Payor").val() == "1") {
                $("#Edit_Agency_SubmitterId").val("SW23071");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
            } else if ($("#Edit_Agency_Payor").val() == "2") {
                $("#Edit_Agency_SubmitterId").val("CH0001841");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
            } else if ($("#Edit_Agency_Payor").val() == "3") {
                $("#Edit_Agency_SubmitterId").val("IA003133");
                $("#Edit_Agency_SubmitterName").val("Axxess Consult Inc");
            } else if ($("#Edit_Agency_Payor").val() == "4") {
                $("#Edit_Agency_SubmitterId").val("CHA075040");
                $("#Edit_Agency_SubmitterName").val("Axxess Technolgoy Solutions");
            }

            $("#Edit_Agency_SubmitterPhone1").val("214");
            $("#Edit_Agency_SubmitterPhone2").val("575");
            $("#Edit_Agency_SubmitterPhone3").val("7711");
            $("#Edit_Agency_SubmitterFax1").val("214");
            $("#Edit_Agency_SubmitterFax2").val("575");
            $("#Edit_Agency_SubmitterFax3").val("7722");
        }

        $("#Edit_Agency_AxxessBiller").click(function() {
            if ($(this).is(':checked')) {
                if ($("#Edit_Agency_Payor").val() == "1") {
                    $("#Edit_Agency_SubmitterId").val("SW23071");
                    $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#Edit_Agency_Payor").val() == "2") {
                    $("#Edit_Agency_SubmitterId").val("CH0001841");
                    $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#Edit_Agency_Payor").val() == "3") {
                    $("#Edit_Agency_SubmitterId").val("IA003133");
                    $("#Edit_Agency_SubmitterName").val("Axxess Consult Inc");
                } else if ($("#Edit_Agency_Payor").val() == "4") {
                    $("#Edit_Agency_SubmitterId").val("CHA075040");
                    $("#Edit_Agency_SubmitterName").val("Axxess Technolgoy Solutions");
                }

                $("#Edit_Agency_SubmitterPhone1").val("214");
                $("#Edit_Agency_SubmitterPhone2").val("575");
                $("#Edit_Agency_SubmitterPhone3").val("7711");
                $("#Edit_Agency_SubmitterFax1").val("214");
                $("#Edit_Agency_SubmitterFax2").val("575");
                $("#Edit_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#Edit_Agency_SubmitterId").add("#Edit_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });

        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#Edit_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#Edit_Agency_TrialPeriod").removeAttr("disabled");
        });

        U.InitEditTemplate("Agency");
    },
    EditUser: function(loginId) { acore.open("editagencyuser", 'Agency/EditUser', function() { Agency.InitEditUser(); }, { loginId: loginId }); },
    Users: function(Id) { acore.open("listagencyusers", 'Agency/UserGrid', function() { }, { agencyId: Id }); },
    Physicians: function(Id) { acore.open("listagencyphysicians", 'Agency/PhysicianGrid', function() { }, { agencyId: Id }); },
    Locations: function(Id) { acore.open("listagencylocations", 'Agency/LocationGrid', function() { }, { agencyId: Id }); },
    Notes: function(Id) { acore.open("listagencynotes", 'Agency/NoteGrid', function() { }, { agencyId: Id }); },
    Templates: function(Id) {
        if (confirm("Before proceeding, verify that templates have not been previously loaded for this agency.\r\nAre you sure you want to load templates for this Agency?")) {
            U.PostUrl("Agency/Templates", { agencyId: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Supplies: function(Id) {
        if (confirm("Before proceeding, verify that supplies have not been previously loaded for this agency.\r\nAre you sure you want to load supplies for this Agency?")) {
            U.PostUrl("Agency/Supplies", { agencyId: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    InitEditUser: function() {
        $("#editEmailForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Agency.RebindUserList();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editPasswordForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Agency.RebindUserList();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNew: function() {
        Lookup.LoadStates();
        U.PhoneAutoTab('New_Agency_Phone');
        U.PhoneAutoTab('New_Agency_Fax');
        U.PhoneAutoTab('New_Agency_SubmitterPhone');
        U.PhoneAutoTab('New_Agency_SubmitterFax');
        U.PhoneAutoTab('New_Agency_ContactPhone');
        $(".names").alpha({ nocaps: false });

        $("input[name=New_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_ContactPersonEmail").val($("#New_Agency_AdminUsername").val());
                $("#New_Agency_ContactPersonFirstName").val($("#New_Agency_AdminFirstName").val());
                $("#New_Agency_ContactPersonLastName").val($("#New_Agency_AdminLastName").val());
            } else $("#New_Agency_ContactPersonEmail").add("#New_Agency_ContactPersonFirstName").add("#New_Agency_ContactPersonLastName").val('');
        });
        $("#New_Agency_AxxessBiller").click(function() {
            if ($(this).is(':checked')) {
                if ($("#New_Agency_Payor").val() == "1") {
                    $("#New_Agency_SubmitterId").val("SW23071");
                    $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#New_Agency_Payor").val() == "2") {
                    $("#New_Agency_SubmitterId").val("CH0001841");
                    $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#New_Agency_Payor").val() == "3") {
                    $("#New_Agency_SubmitterId").val("IA003133");
                    $("#New_Agency_SubmitterName").val("Axxess Consult Inc");
                } else if ($("#New_Agency_Payor").val() == "4") {
                    $("#New_Agency_SubmitterId").val("CHA075040");
                    $("#New_Agency_SubmitterName").val("Axxess Technolgoy Solutions");
                }

                $("#New_Agency_SubmitterPhone1").val("214");
                $("#New_Agency_SubmitterPhone2").val("575");
                $("#New_Agency_SubmitterPhone3").val("7711");
                $("#New_Agency_SubmitterFax1").val("214");
                $("#New_Agency_SubmitterFax2").val("575");
                $("#New_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#New_Agency_SubmitterId").add("#New_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#New_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#New_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    RebindList: function() { U.RebindTGrid($('#List_Agencies')); },
    RebindUserList: function() { U.RebindTGrid($('#List_Agencies_Users')); },
    FilterTGrid: function(text) {
        search = text.split(" ");
        $("tr", "#List_Agencies .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#List_Agencies .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#List_Agencies .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#List_Agencies .t-grid-content").addClass("t-alt");
    },
    ListRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.AttentionLevel == "red") $(e.row).addClass('redrow');
        else if (dataItem.AttentionLevel == "blue") $(e.row).addClass('bluerow');
        else $(e.row).addClass('blackrow');

        if (dataItem.PopUpContent != undefined) {
            $(e.row).tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday",
                bodyHandler: function() { return dataItem.PopUpContent; }
            });
        }
    },
    LoadLocationStandAloneContent: function(contentId, agencyId, locationId) {
        $(contentId).load('Agency/StandAloneContent', { agencyId: agencyId, locationId: locationId }, function(responseText, textStatus, XMLHttpRequest) { });
    }
}