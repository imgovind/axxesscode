﻿<html>
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <style type="text/css" media="screen">
		    p {
			    margin: 0 0 10px 0;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
			    font-family: arial, sans-serif;
			    font-size: 13px;
		    }

		    body, div, td, th, textarea, input, h2, h3 {
			    font-family: arial, sans-serif;
			    font-size: 12px;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
		    }
		    ol, ul {
 	            list-style: none;
 	            margin: 0;
            }
	    </style>
	</head>
	<body>
		<p>Dear <%=displayname%>,</p>
		<p>Your agency has exceeded your user subscription plan limit. You have been upgraded automatically to the next user subscription plan.</p>
		<p><strong>Old subscription Plan: <%=previouspackage%></strong></p>
		<p><strong>New Subscription Plan: <%=newpackage%></strong></p>
		<p>You will be charged at the new subscription plan starting next month.
		<hr />
		<p>No further action is required from you.</p>
		<p style="font-family: arial, sans-serif; font-size: 11px;">This is an automated e-mail, please do not reply.</p>
		<p style="font-family: arial, sans-serif; font-size: 11px;">This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>