﻿<%@ Page Language="C#"%>
<script runat="server" type="text/C#">
    public float GetCpuUsage()
    {
        var counter = new System.Diagnostics.PerformanceCounter();
        counter.CategoryName = "Processor";
        counter.CounterName = "% Processor Time";
        counter.InstanceName = "_Total";
        counter.NextValue();
        System.Threading.Thread.Sleep(1000);
        return counter.NextValue();
    }    
</script>
<% if (!Current.IsIpAddressRestricted) { %>
    <% var cpuUsage = 100 - GetCpuUsage(); %>
    <%= string.Format(cpuUsage.ToString("0.00"))%>        
<% } %>

