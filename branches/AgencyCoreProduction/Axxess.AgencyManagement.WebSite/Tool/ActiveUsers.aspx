﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Active User Count</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
<% if (!Current.IsIpAddressRestricted) { %>
    <%= string.Format("{0} sessions", SessionMonitor.Instance.Count) %>
<% } %>
</asp:Content>