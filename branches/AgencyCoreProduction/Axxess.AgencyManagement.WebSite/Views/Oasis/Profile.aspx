﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<AssessmentPrint>" %><%
var location = Model != null ? Model.Location : new AgencyLocation(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Oasis/profile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%=  ((Current.AgencyName + "<br />") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? ", "+ location.AddressLine2.Clean() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") : "")).Clean() %>"
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>
