﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ValidationInfoViewData>" %>
<%  var dictonary = new Dictionary<string, string>() {
        { AssessmentType.StartOfCare.ToString(), "StartOfCare" },
        { AssessmentType.ResumptionOfCare.ToString(), "ResumptionOfCare" },
        { AssessmentType.Recertification.ToString(), "Recertification" },
        { AssessmentType.FollowUp.ToString(), "FollowUp" },
        { AssessmentType.TransferInPatientNotDischarged.ToString(), "TransferInPatientNotDischarged" },
        { AssessmentType.TransferInPatientDischarged.ToString(), "TransferInPatientDischarged" },
        { AssessmentType.DischargeFromAgencyDeath.ToString(), "DischargeFromAgencyDeath" },
        { AssessmentType.DischargeFromAgency.ToString(), "DischargeFromAgency" }
    }; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>OASIS-C Validation</title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
                .Add("jquery-ui-1.7.1.custom.css")
                .Add("desktop.css")
                .Add("validation.css")
                .Add("telerik.common.css")
                .Add("telerik.office2007.css")
                .Add("Site.css")
                .Combined(true)
                .Compress(true)
                .CacheDurationInDays(1)
                .Version(Current.AssemblyVersion)) %>
        <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
                // jQuery
                .Add("jquery-1.6.2.min.js")
                // jQuery UI Plugins
                .Add("Plugins/jQueryUI/jquery.ui.core.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.widget.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.mouse.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.position.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.autocomplete.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.datepicker.min.js")
                // Other Plugins
                .Add("Plugins/Other/form.min.js")
                .Add("Plugins/Other/validate.min.js")
                .Add("Plugins/Other/jgrowl.min.js")
                .Add("Plugins/Other/alphanumeric.min.js")
                .Add("Plugins/Other/maxlength.min.js")
                // Custom Plugins
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "AutoComplete.js")
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Datepicker.js")
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "FormElements.js")
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "TimePicker.js")
                // Modules            
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Oasis.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "OasisValidation.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Agency.js")

                .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
    </head>
    <body style="overflow:auto">
        <div class="wrapper main" >
<% if (Model.ValidationErrors != null && Model.ValidationErrors.Count > 0) { %>
            <div class="title">
                You have
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count()%> error<%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() != 1 ? "s" : string.Empty%></strong>
                and
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count()%> warning<%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count() != 1 ? "s" : string.Empty%></strong>
                .
            </div>
    <% foreach (var data in Model.ValidationErrors) { %>
            <a class="error-anchor" href="javascript:void(0)" onclick="window.parent.Oasis.gotoQuestion('<%= data.ErrorDup.Substring(0, 5) %>','<%= dictonary[Model.AssessmentType] %>', <%= Model.Version %>);window.parent.Acore.CloseDialog()" class="<%= data.ErrorType == "FATAL" ? "red" : "" %>">
                <div>
                    <%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<span class='img icon error'></span>" : "<span class='img icon warning'></span>"%>
                    <span style="display:inline-block; overflow:hidden; width:231px;"><%= data.ErrorDup != "M0001" ? data.ErrorDup : ""%></span>
                    <span class="description"><%= data.Description%></span>
                </div>
            </a>
    <% } %>
<%  } %>
<% if (Model.IsErrorFree){ %>
        <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString()
            || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString()
            || Model.AssessmentType == AssessmentType.Recertification.ToString()
            || Model.AssessmentType == AssessmentType.FollowUp.ToString()) { %>
            <%  if (Current.HasRight(Permissions.ViewHHRGCalculations)){ %>
            <div class="hipps">
                <span>
                    <label>HIPPS Code:</label>
                    <strong><%= Model.HIPPSCODE%></strong>
                </span>
                <span>
                    <label>OASIS Claim Matching Key:</label>
                    <strong><%= Model.HIPPSKEY%></strong>
                </span>
                <span>
                    <label>HHRG Code:</label>
                    <strong><%= Model.HHRG%></strong>
                </span>
            </div>
            <div class="hipps">
                <span>
                    <label>Episode Payment Rate:</label>
                    <strong><%= Model.StandardPaymentRate != 0 ? string.Format("${0}", Math.Round(Model.StandardPaymentRate, 2).ToString()) : ""%></strong>
                </span>
            </div>
            <%  } %>
        <%  } %>
        <% if (Model.Status != ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() && Model.Status != ((int)ScheduleStatus.OasisCompletedExportReady).ToString()){ %>
            <%  using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "oasisValidationForm" })){ %>
                <%= Html.Hidden(Model.AssessmentType + "_PatientId", Model.PatientId)%>
                <%= Html.Hidden(Model.AssessmentType + "_EpisodeId", Model.EpisodeId)%>
                <%= Html.Hidden(Model.AssessmentType + "_Id", Model.AssessmentId)%>
                <%= Html.Hidden("OasisValidationType", Model.AssessmentType)%>
                <%= Html.Hidden("oasisPageName", dictonary[Model.AssessmentType])%>
                    <input type="hidden" id="oasisval_submit"  onclick="$(this).closest('form').submit();"  />
                    <div class="oasissignature">
                        <table>
                            <tbody>
                    <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()){ %>
                              <tr>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_TimeIn" class="float-left bigtext">Time In:</label>
                                        <div class="float-right"><%= Html.TextBox(Model.AssessmentType + "_TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "time-picker required" })%></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_TimeOut" class="float-left bigtext">Time Out:</label>
                                        <div class="float-right"><%= Html.TextBox(Model.AssessmentType + "_TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "time-picker required" })%></div>
                                    </td>
                              </tr>
                    <%  } %>
                    <%  if (Model.AssessmentType == AssessmentType.DischargeFromAgency.ToString() || Model.AssessmentType == AssessmentType.DischargeFromAgencyDeath.ToString()){ %>
                              <tr>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_TimeIn" class="float-left bigtext">Time In:</label>
                                        <div class="float-right"><%= Html.TextBox(Model.AssessmentType + "_TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "time-picker" })%></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_TimeOut" class="float-left bigtext">Time Out:</label>
                                        <div class="float-right"><%= Html.TextBox(Model.AssessmentType + "_TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "time-picker" })%></div>
                                    </td>
                              </tr>
                    <%  } %>
                              <tr>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_ValidationClinician" class="float-left bigtext">Clinician Signature:</label>
                                        <div class="float-right"><%= Html.Password(Model.AssessmentType + "_ValidationClinician", "", new { @id = Model.AssessmentType + "_ValidationClinician", @class = "required" })%></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <label for="<%= Model.AssessmentType%>_ValidationSignatureDate" class="float-left bigtext">Date:</label>
                                        <div class="float-right"><%= String.Format("<input type='text' id='{0}_ValidationSignatureDate' name='{0}_ValidationSignatureDate' class='date-picker required' mindate='{1}' maxdate='{2}' />", Model.AssessmentType, Model.EpisodeStartDate.ToShortDateString(), Model.EpisodeEndDate.ToShortDateString())%></div>
                                    </td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
            <%  } %>
        <% } else { %>
            <div class="oasissignature">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label><strong>The OASIS Validation Check was successful and returned no errors. Click the Exit button to return to the OASIS assessment to approve or return to the clinician. <br /><br /><em>Please note that this document has been signed and completed already. To sign this document, please reopen it from the Patient Charts or the Schedule Center.</em></strong></label> 
                            </td>
                        </tr>
                    </tbody>
                </table>
             </div> 
        <% } %>
<% } %>
        </div>
        <script type="text/javascript">
            <%  if (Model.IsErrorFree) { %>
                  <%  if (Model.Status != ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() && Model.Status != ((int)ScheduleStatus.OasisCompletedExportReady).ToString()) { %>
                       Oasis.oasisSignatureSubmit($("#oasisValidationForm"));
                       var finishOnClick = "$('#printview').contents().find('#oasisval_submit').click()";
                        <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() ||
                            Model.AssessmentType == AssessmentType.Recertification.ToString() ||
                            Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() ||
                            Model.AssessmentType == AssessmentType.FollowUp.ToString() ||
                            Model.AssessmentType == AssessmentType.DischargeFromAgency.ToString() || 
                            Model.AssessmentType == AssessmentType.DischargeFromAgencyDeath.ToString()) { %>
                            finishOnClick = "if(OasisValidation.CheckTimeInOut('<%= Model.AssessmentType %>', false) === false){" + finishOnClick + "}else{return false}";
                        <% } %>
                        window.parent.Acore.OnLoad($(".main"));
                         $("#printbutton", window.parent.document).parent().html(
                            $("<a/>", { "href": "javascript:void(0)", "text": "Finish", "onclick": finishOnClick })
                        ).next().find("a").text("Cancel");
                    <%  } else{%>
                     $("#printbutton", window.parent.document).parent().remove();
                     $("#print-controls li:last a:last", window.parent.document).text("Exit");
            <% } } %>
        </script>
    </body>
</html>