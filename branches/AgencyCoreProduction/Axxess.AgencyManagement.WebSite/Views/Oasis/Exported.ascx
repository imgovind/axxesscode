﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Exported OASIS Assessments | <%= Current.AgencyName %></span>
<% var visible = Current.HasRight(Permissions.ReopenDocuments) && !Current.IsAgencyFrozen; %>
<% using (Html.BeginForm("ExportedOasis", "Export", FormMethod.Post)) { %>
<div class="wrapper grid-bg">
    <div class="buttons float-right"><ul><li><%= Html.ActionLink("Export to Excel", "ExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "ExportedOasis_ExportLink" })%></li></ul></div>
    <fieldset class="orders-filter">
        
        <div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindExportedOasis();">Generate</a></li></ul></div>
        <label class="strong">Branch:
            <%= Html.UserBranchList("BranchId", ViewData["BranchId"].ToString(),false,"", new { @id = "ExportedOasis_BranchCode" })%>
        </label>
        <label class="strong">Status:
            <select id="ExportedOasis_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select>
        </label>
        <div>
            <label class="strong" for="ExportedOasis_StartDate">Exported Date Range:
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ExportedOasis_StartDate" />
            </label>
            <label class="strong" for="ExportedOasis_EndDate">To
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ExportedOasis_EndDate" />
            </label>
        </div>
    </fieldset>   
    <%  Html.Telerik().Grid<AssessmentExport>().Name("exportedOasisGrid").HtmlAttributes(new { @style = "top:70px;" }).Columns(columns =>
        {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDateFormatted).Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.ExportedDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Exported Date").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Cancel").Width(110).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('{0}','{1}');\" >Generate Cancel</a>", o.AssessmentId,  o.AssessmentType)%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('<#= AssessmentId#>','<#= AssessmentType#>');\" >Generate Cancel</a>").Sortable(false).Visible(visible).Sortable(false);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(60).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');\" >Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');\" >Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Exported", "Oasis", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
</div>
<script type="text/javascript">
    if (Acore.Mobile) $('#window_oasisExported #exportedOasisGrid').css('top', 130);
    $("#window_oasisExported #exportedOasisGrid .t-grid-content").css({ "height": "auto", "top": "26px" });
    $("#window_oasisExported_content").css({
        "background-color": "#d6e5f3"
    });
</script>
<% } %>

