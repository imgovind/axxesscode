﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<AssessmentPrint>" %>
<%
var data = Model.Data;
var location = Model != null ? Model.Location ?? new AgencyLocation() : new AgencyLocation();
var oasisDocument = new Dictionary<string, string>() {
        { AssessmentType.StartOfCare.ToString(), "START OF CARE" },
        { AssessmentType.ResumptionOfCare.ToString(), "RESUMPTION OF CARE" },
        { AssessmentType.Recertification.ToString(), "RECERTIFICATION" },
        { AssessmentType.FollowUp.ToString(), "FOLLOW-UP" },
        { AssessmentType.TransferInPatientNotDischarged.ToString(), "TRANSFER NOT DISCHARGED" },
        { AssessmentType.TransferInPatientDischarged.ToString(), "TRANSFER DISCHARGE" },
        { AssessmentType.DischargeFromAgencyDeath.ToString(), "DEATH AT HOME" },
        { AssessmentType.DischargeFromAgency.ToString(), "DISCHARGE FROM AGENCY" },
         { AssessmentType.NonOasisStartOfCare.ToString(), "START OF CARE" },
         { AssessmentType.NonOasisRecertification.ToString(), "RECERTIFICATION" },
         { AssessmentType.NonOasisDischarge.ToString(), "DISCHARGE FROM AGENCY" }
    };
var isOasis = !Model.Type.ToString().Contains("NonOasis");
var typeName = string.Format("{0}{1} {2}", (isOasis ? "OASIS- C" : "NON-OASIS"), Model.Discipline.IsNotNullOrEmpty() && (Model.Discipline.IsEqual("PT") || Model.Discipline.IsEqual("OT") || Model.Discipline.IsEqual("ST")) ? " (" + Model.Discipline + ")" : string.Empty, oasisDocument.ContainsKey(Model.Type.ToString()) ? oasisDocument[Model.Type.ToString()] : string.Empty);
     %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Current.AgencyName + " | " + typeName%></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    <% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Current.AgencyName.Clean() + "%3Cbr /%3E"  %><%= location != null && location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : string.Empty %><%= location != null && location.AddressLine2.IsNotNullOrEmpty() ? ", "+location.AddressLine2.Clean() : string.Empty %>%3Cbr /%3E<%= location != null && location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : string.Empty %><%= location != null && location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %><%= location != null && location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E<%=typeName %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EAssessment Date:%3C/strong%3E<%= Model.AssessmentDate != null && Model.AssessmentDate.IsValid() ?  Model.AssessmentDate.ToShortDateString().Clean() : "" %>%3C/span%3E%3Cspan%3E" +
            "%3Cstrong%3EVisit Date:%3C/strong%3E<%= Model.VisitDate.IsValid() ? Model.VisitDate.ToShortDateString().Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime in:%3C/strong%3E<%= Model.TimeIn != null ?  Model.TimeIn.Clean() : "" %>%3C/span%3E%3Cspan%3E" +
            "%3Cstrong%3ETime Out:%3C/strong%3E<%= Model.TimeOut != null ?  Model.TimeOut.Clean() : "" %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Current.AgencyName.Clean() + "%3Cbr /%3E" %><%= location != null && location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty %><%= location != null && location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : string.Empty %>%3Cbr /%3E<%= location != null && location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %><%= location != null && location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %><%= location != null && location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E<%=typeName %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
</script>
<% var printPage = string.Format("Assessments/{0}Print{1}", Model.Type.ToString(), Model.Version > 1 ? Model.Version.ToString() : string.Empty); %>
<%= printPage %>
<% Html.RenderPartial(printPage, Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Signature:", true) +
            printview.span("Date", true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>", false, 1) +
            printview.span("<%= Model != null && Model.SignatureDate != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>", false, 1)),
        "Signature");
</script>
</body>
</html>
