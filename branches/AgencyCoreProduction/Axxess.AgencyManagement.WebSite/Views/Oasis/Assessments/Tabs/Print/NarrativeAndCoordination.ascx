﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var Conclusions = data.AnswerArray("485Conclusions"); %>
<%  var RehabilitationPotential = data.AnswerArray("485RehabilitationPotential"); %>
<%  var DischargePlans = data.AnswerArray("485DischargePlans"); %>
<%  var DischargePlansReason = data.AnswerArray("485DischargePlansReason"); %>
<%  var SIResponse = data.AnswerArray("485SIResponse"); %>
<%  var ConferencedWith = data.AnswerArray("485ConferencedWith"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 5||(Model.AssessmentTypeNum % 10==9 && Model.Version==4)) { %>
    
    printview.addsection(
        <%  if (Model.AssessmentTypeNum == 1) { %>
        printview.span("Initial Summary of Patient History:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485SkilledInitialSummaryComments").Clean() %>",false,2) +
        <% } %>
        
        printview.span("Skilled Intervention/Teaching:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485SkilledInterventionComments").Clean() %>",false,2) +
        printview.col(2,
            printview.checkbox("Verbalizes <%= SIResponse.Contains("1") ? data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPercent") : string.Empty %> understanding of teaching.",<%= SIResponse.Contains("1").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("1") && data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("1") && data.AnswerOrEmptyString("485SIVerbalizedUnderstandingCG").Equals("1")).ToString().ToLower() %>)) +
            printview.checkbox("Able to return correct demonstration of procedure.",<%= SIResponse.Contains("3").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("3") && data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDUREPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("3") && data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDURECG").Equals("1")).ToString().ToLower() %>)) +
            printview.checkbox("Unable to return correct demonstration of procedure.",<%= SIResponse.Contains("4").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("4") && data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDUREPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("4") && data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDURECG").Equals("1")).ToString().ToLower() %>))),
        "");
    printview.addsection(
        printview.span("Conferenced with",true) +
        printview.col(7,
            printview.checkbox("MD",<%= ConferencedWith.Contains("MD").ToString().ToLower() %>) +
            printview.checkbox("SN",<%= ConferencedWith.Contains("SN").ToString().ToLower() %>) +
            printview.checkbox("PT",<%= ConferencedWith.Contains("PT").ToString().ToLower() %>) +
            printview.checkbox("OT",<%= ConferencedWith.Contains("OT").ToString().ToLower() %>) +
            printview.checkbox("ST",<%= ConferencedWith.Contains("ST").ToString().ToLower() %>) +
            printview.checkbox("MSW",<%= ConferencedWith.Contains("MSW").ToString().ToLower() %>) +
            printview.checkbox("HHA",<%= ConferencedWith.Contains("HHA").ToString().ToLower() %>)) +
        printview.col(4,
            printview.span("Name:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485ConferencedWithName").Clean() %>",false,1) +
            printview.span("Regarding:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485SkilledInterventionRegarding").Clean() %>",false,1)),
        "Coordination of Care");
    printview.addsection(
        printview.span("Reason:<%=data.AnswerOrEmptyString("GenericDischargeReasonId").StringIntToEnumDescriptionFactory("DischargeReason").Clean() %>")+
        printview.span("Comments:")+
        printview.span("<%=data.AnswerOrEmptyString("GenericDischargeReasonComments").Clean() %>",2),
        "Discharge Reason");
<%  } %>
</script>