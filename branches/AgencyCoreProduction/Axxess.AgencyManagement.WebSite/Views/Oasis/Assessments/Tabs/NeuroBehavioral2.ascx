﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10< 5 || Model.AssessmentTypeNum.ToInteger() % 10 == 9)
    { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "BehaviourialForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden("categoryType", AssessmentCategory.NeuroBehavioral.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Neurological</legend>
        <%  string[] genericNeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalStatus", " ", new { @id = Model.TypeName + "_GenericNeurologicalStatusHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, LOC' id='{0}_GenericNeurologicalStatus1' name='{0}_GenericNeurologicalStatus' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("1").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus1" class="inline-radio">LOC:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericNeurologicalStatus1More" class="float-right">
                            <div class="float-right">
                                <%  var LOC = new SelectList(new[]{
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Alert", Value = "1" },
                                        new SelectListItem { Text = "Lethargic", Value = "2" },
                                        new SelectListItem { Text = "Comatose", Value = "3" },
                                        new SelectListItem { Text = "Disoriented", Value = "4" },
                                        new SelectListItem { Text = "Forgetful", Value = "5"},
                                        new SelectListItem { Text = "Other", Value = "6" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalLOC", "0"));    %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalLOC", LOC, new { @id = Model.TypeName + "_GenericNeurologicalLOC", @title = "(Optional) Neurological, LOC Condition", @class = "oe" })%>
                            </div>
                            <div class="clear"></div>
                            <div class="float-right">
                                <div>Oriented to:</div>
                                <%  string[] genericNeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
                                <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalOriented", " ", new { @id = Model.TypeName + "_GenericNeurologicalOrientedHidden" })%>
                                <%= string.Format("<input title='(Optional) Neurological, LOC, Orientation, Person' id='{0}_GenericNeurologicalOriented1' class='no_float' name='{0}_GenericNeurologicalOriented' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("1").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalOriented1" class="inline-radio">Person</label>
                                <%= string.Format("<input title='(Optional) Neurological, LOC, Orientation, Place' id='{0}_GenericNeurologicalOriented2' class='no_float' name='{0}_GenericNeurologicalOriented' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("2").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalOriented2" class="inline-radio">Place</label>
                                <%= string.Format("<input title='(Optional) Neurological, LOC, Orientation, Time' id='{0}_GenericNeurologicalOriented3' class='no_float' name='{0}_GenericNeurologicalOriented' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("3").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalOriented3" class="inline-radio">Time</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, Pupils' id='{0}_GenericNeurologicalStatus2' name='{0}_GenericNeurologicalStatus' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("2").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus2" class="inline-radio">Pupils:</label>
                        </div>
                        <div class="<%= Model.TypeName %>_GenericNeurologicalStatus2More float-right">
                            <%  var pupils = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "PERRLA/WNL", Value = "1" },
                                    new SelectListItem { Text = "Sluggish", Value = "2" },
                                    new SelectListItem { Text = "Non-Reactive", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalPupils", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalPupils", pupils, new { @id = Model.TypeName + "_GenericNeurologicalPupils", @title = "(Optional) Neurological, Pupils Condition", @class = "oe" })%>
                        </div>
                        <div class="clear"></div>
                        <div class="<%= Model.TypeName %>_GenericNeurologicalStatus2More float-right">
                            <%= Html.RadioButton(Model.TypeName + "_GenericNeurologicalPupilsPosition", "0", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("0"), new { @id = "GenericNeurologicalPupilsPosition0", @class = "no_float radio deselectable", @title = "(Optional) Neurological, Pupils Condition, Bilateral" })%>
                            <label for="GenericNeurologicalPupilsPosition0" class="inline-radio">Bilateral</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNeurologicalPupilsPosition", "1", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("1"), new { @id = "GenericNeurologicalPupilsPosition1", @class = "no_float radio deselectable", @title = "(Optional) Neurological, Pupils Condition, Left" })%>
                            <label for="GenericNeurologicalPupilsPosition1" class="inline-radio">Left</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNeurologicalPupilsPosition", "2", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("2"), new { @id = "GenericNeurologicalPupilsPosition2", @class = "no_float radio deselectable", @title = "(Optional) Neurological, Pupils Condition, Right" })%>
                            <label for="GenericNeurologicalPupilsPosition2" class="inline-radio">Right</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, Vision' id='{0}_GenericNeurologicalStatus3' name='{0}_GenericNeurologicalStatus' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("3").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus3" class="radio">Vision:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericNeurologicalStatus3More" class="float-right">
                            <%  string[] genericNeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalVisionStatus", " ", new { @id = Model.TypeName + "_GenericNeurologicalVisionStatus" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Within Normal Limits' id='{0}_GenericNeurologicalVisionStatus1' name='{0}_GenericNeurologicalVisionStatus' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("1").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus1" class="fixed inline-radio">WNL</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Blurred Vision' id='{0}_GenericNeurologicalVisionStatus2' name='{0}_GenericNeurologicalVisionStatus' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("2").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus2" class="fixed inline-radio">Blurred Vision</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Cataracts' id='{0}_GenericNeurologicalVisionStatus3' name='{0}_GenericNeurologicalVisionStatus' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("3").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus3" class="fixed inline-radio">Cataracts</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Corrective Lenses' id='{0}_GenericNeurologicalVisionStatus4' name='{0}_GenericNeurologicalVisionStatus' value='4' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("4").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus4" class="fixed inline-radio">Corrective Lenses</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Glaucoma' id='{0}_GenericNeurologicalVisionStatus5' name='{0}_GenericNeurologicalVisionStatus' value='5' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("5").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus5" class="fixed inline-radio">Glaucoma</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Neurological, Vision, Legally Blind' id='{0}_GenericNeurologicalVisionStatus6' name='{0}_GenericNeurologicalVisionStatus' value='6' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("6").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus6" class="fixed inline-radio">Legally Blind</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, Speech' id='{0}_GenericNeurologicalStatus4' name='{0}_GenericNeurologicalStatus' value='4' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("4").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus4" class="inline-radio">Speech:</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericNeurologicalStatus4More" class="float-right">
                            <%  var speech = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Clear", Value = "1" },
                                    new SelectListItem { Text = "Slurred", Value = "2" },
                                    new SelectListItem { Text = "Aphasic", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalSpeech", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalSpeech", speech, new { @id = Model.TypeName + "_GenericNeurologicalSpeech", @title = "(Optional) Neurological, Specify Speech", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, Paralysis' id='{0}_GenericNeurologicalStatus5' name='{0}_GenericNeurologicalStatus' value='5' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus5" class="inline-radio">Paralysis</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericNeurologicalStatus5More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericNeurologicalParalysisLocation", data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation"), new { @id = Model.TypeName + "_GenericNeurologicalParalysisLocation", @class = "loc", @maxlength = "20", @title = "(Optional) Neurological, Paralysis Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Neurological, Quadriplegia' id='{0}_GenericNeurologicalStatus6' name='{0}_GenericNeurologicalStatus' value='6' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNeurologicalStatus6" class="radio">Quadriplegia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Neurological, Paraplegia' id='{0}_GenericNeurologicalStatus7' name='{0}_GenericNeurologicalStatus' value='7' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNeurologicalStatus7" class="radio">Paraplegia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Neurological, Seizures' id='{0}_GenericNeurologicalStatus8' name='{0}_GenericNeurologicalStatus' value='8' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNeurologicalStatus8" class="radio">Seizures</label>
                    </div>
                    <div class="option">
                        <div class="float-left">
                            <%= string.Format("<input title='(Optional) Neurological, Tremors' id='{0}_GenericNeurologicalStatus9' name='{0}_GenericNeurologicalStatus' value='9' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("9").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus9" class="inline-radio">Tremors</label>
                        </div>
                        <div id="<%= Model.TypeName %>_GenericNeurologicalStatus9More" class="float-right">
                            <em>(location)</em>
                            <%= Html.TextBox(Model.TypeName + "_GenericNeurologicalTremorsLocation", data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation"), new { @id = Model.TypeName + "_GenericNeurologicalTremorsLocation", @class = "loc", @maxlength = "20", @title = "(Optional) Neurological, Tremors Location" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Neurological, Dizziness' id='{0}_GenericNeurologicalStatus10' name='{0}_GenericNeurologicalStatus' value='10' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("10").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNeurologicalStatus10" class="radio">Dizziness</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Neurological, Headache' id='{0}_GenericNeurologicalStatus11' name='{0}_GenericNeurologicalStatus' value='11' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("11").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNeurologicalStatus11" class="radio">Headache</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNeuroEmoBehaviorComments">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericNeuroEmoBehaviorComments", data.AnswerOrEmptyString("GenericNeuroEmoBehaviorComments"), 5, 70, new { @id = Model.TypeName + "_GenericNeuroEmoBehaviorComments", @title = "(Optional) Neurological, Comments" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Psychosocial</legend>
        <%  string[] genericPsychosocial = data.AnswerArray("GenericPsychosocial"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericPsychosocial", " ", new { @id = Model.TypeName + "_GenericPsychosocialHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, No Issues Identified' id='{0}_GenericPsychosocial1' name='{0}_GenericPsychosocial' value='1' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial1" class="radio">WNL (No Issues Identified)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Poor Home Environment' id='{0}_GenericPsychosocial2' name='{0}_GenericPsychosocial' value='2' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial2" class="radio">Poor Home Environment</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Poor Coping Skills' id='{0}_GenericPsychosocial3' name='{0}_GenericPsychosocial' value='3' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Poor Coping Skills</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Agitated' id='{0}_GenericPsychosocial4' name='{0}_GenericPsychosocial' value='4' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Agitated</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Depressed Mood' id='{0}_GenericPsychosocial5' name='{0}_GenericPsychosocial' value='5' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Depressed Mood</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Impaired Decision Making' id='{0}_GenericPsychosocial6' name='{0}_GenericPsychosocial' value='6' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Impaired Decision Making</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Demonstrated/Expressed Anxiety' id='{0}_GenericPsychosocial7' name='{0}_GenericPsychosocial' value='7' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Demonstrated/Expressed Anxiety</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Inappropriate Behavior' id='{0}_GenericPsychosocial8' name='{0}_GenericPsychosocial' value='8' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Inappropriate Behavior</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Psychosocial, Irritability' id='{0}_GenericPsychosocial9' name='{0}_GenericPsychosocial' value='9' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("9").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericPsychosocial3" class="radio">Irritability</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPsychosocialComments">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericPsychosocialComments", data.AnswerOrEmptyString("GenericPsychosocialComments"), 5, 70, new { @id = Model.TypeName + "_GenericPsychosocialComments", @title = "(Optional) Psychosocial, Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
    <fieldset class="oasis">
        <legend>Cognition</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1700">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1700" class="green" onclick="Oasis.ToolTip('M1700')">(M1700)</a>
                    Cognitive Functioning: Patient&#8217;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1700CognitiveFunctioning", "", new { @id = Model.TypeName + "_M1700CognitiveFunctioningHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1700CognitiveFunctioning", "00", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("00"), new { @id = Model.TypeName + "_M1700CognitiveFunctioning0", @title = "(OASIS M1700) Cognitive Functioning, Alert/Oriented" })%>
                        <label for="<%= Model.TypeName %>_M1700CognitiveFunctioning0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1700CognitiveFunctioning", "01", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("01"), new { @id = Model.TypeName + "_M1700CognitiveFunctioning1", @title = "(OASIS M1700) Cognitive Functioning, Requires Prompting" })%>
                        <label for="<%= Model.TypeName %>_M1700CognitiveFunctioning1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1700CognitiveFunctioning", "02", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("02"), new { @id = Model.TypeName + "_M1700CognitiveFunctioning2", @title = "(OASIS M1700) Cognitive Functioning, Requires Assistance and Some Direction" })%>
                        <label for="<%= Model.TypeName %>_M1700CognitiveFunctioning2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1700CognitiveFunctioning", "03", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("03"), new { @id = Model.TypeName + "_M1700CognitiveFunctioning3", @title = "(OASIS M1700) Cognitive Functioning, Requires Considerable Assistance" })%>
                        <label for="<%= Model.TypeName %>_M1700CognitiveFunctioning3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1700CognitiveFunctioning", "04", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("04"), new { @id = Model.TypeName + "_M1700CognitiveFunctioning4", @title = "(OASIS M1700) Cognitive Functioning, Totally Dependent" })%>
                        <label for="<%= Model.TypeName %>_M1700CognitiveFunctioning4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1700');" title="More Information about M1700">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1710">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1710" class="green" onclick="Oasis.ToolTip('M1710')">(M1710)</a>
                    When Confused (Reported or Observed Within the Last 14 Days):
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1710WhenConfused", "", new { @id = Model.TypeName + "_M1710WhenConfusedHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "00", data.AnswerOrEmptyString("M1710WhenConfused").Equals("00"), new { @id = Model.TypeName + "_M1710WhenConfused0", @title = "(OASIS M1710) Confused, Never" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfused0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Never</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "01", data.AnswerOrEmptyString("M1710WhenConfused").Equals("01"), new { @id = Model.TypeName + "_M1710WhenConfused1", @title = "(OASIS M1710) Confused, New/Complex Situations" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfused1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">In new or complex situations only</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "02", data.AnswerOrEmptyString("M1710WhenConfused").Equals("02"), new { @id = Model.TypeName + "_M1710WhenConfused2", @title = "(OASIS M1710) Confused, Awakening/Night" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfused2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">On awakening or at night only</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "03", data.AnswerOrEmptyString("M1710WhenConfused").Equals("03"), new { @id = Model.TypeName + "_M1710WhenConfused3", @title = "(OASIS M1710) Confused, Day/Evening" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfused3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">During the day and evening, but not constantly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "04", data.AnswerOrEmptyString("M1710WhenConfused").Equals("04"), new { @id = Model.TypeName + "_M1710WhenConfused4", @title = "(OASIS M1710) Confused, Constantly" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfused4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Constantly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1710WhenConfused", "NA", data.AnswerOrEmptyString("M1710WhenConfused").Equals("NA"), new { @id = Model.TypeName + "_M1710WhenConfusedNA", @title = "(OASIS M1710) Confused, Patient Nonresponsive" })%>
                        <label for="<%= Model.TypeName %>_M1710WhenConfusedNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient nonresponsive</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1710');" title="More Information about M1710">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1720">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1720" class="green" onclick="Oasis.ToolTip('M1720')">(M1720)</a>
                    When Anxious (Reported or Observed Within the Last 14 Days):
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1720WhenAnxious", "", new { @id = Model.TypeName + "_M1720WhenAnxiousHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1720WhenAnxious", "00", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("00"), new { @id = Model.TypeName + "_M1720WhenAnxious0", @title = "(OASIS M1720) Anxious, Never" })%>
                        <label for="<%= Model.TypeName %>_M1720WhenAnxious0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">None of the time</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1720WhenAnxious", "01", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("01"), new { @id = Model.TypeName + "_M1720WhenAnxious1", @title = "(OASIS M1720) Anxious, Less Often than Daily" })%>
                        <label for="<%= Model.TypeName %>_M1720WhenAnxious1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Less often than daily</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1720WhenAnxious", "02", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("02"), new { @id = Model.TypeName + "_M1720WhenAnxious2", @title = "(OASIS M1720) Anxious, Daily" })%>
                        <label for="<%= Model.TypeName %>_M1720WhenAnxious2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Daily, but not constantly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1720WhenAnxious", "03", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("03"), new { @id = Model.TypeName + "_M1720WhenAnxious3", @title = "(OASIS M1720) Anxious, All of the Time" })%>
                        <label for="<%= Model.TypeName %>_M1720WhenAnxious3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">All of the time</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1720WhenAnxious", "NA", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("NA"), new { @id = Model.TypeName + "_M1720WhenAnxiousNA", @title = "(OASIS M1720) Anxious, Patient Nonresponsive" })%>
                        <label for="<%= Model.TypeName %>_M1720WhenAnxiousNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient nonresponsive</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1720');" title="More Information about M1720">?</div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 9) { %>
    <fieldset class="oasis nomobile">
        <legend>Depression Screening</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1730">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1730" class="green" onclick="Oasis.ToolTip('M1730')">(M1730)</a>
                    Depression Screening: Has the patient been screened for depression, using a standardized depression screening tool?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1730DepressionScreening", "", new { @id = Model.TypeName + "_M1730DepressionScreeningHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1730DepressionScreening", "00", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("00"), new { @id = Model.TypeName + "_M1730DepressionScreening0", @title = "(OASIS M1720) Depression Screening, No" })%>
                        <label for="<%= Model.TypeName %>_M1730DepressionScreening0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1730DepressionScreening", "01", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("01"), new { @id = Model.TypeName + "_M1730DepressionScreening1", @title = "(OASIS M1720) Depression Screening, PHQ-2" })%>
                        <label for="<%= Model.TypeName %>_M1730DepressionScreening1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, patient was screened using the PHQ-2&#169;* scale. (Instructions for this two-question tool: Ask patient: &#8220;Over the last two weeks, how often have you been bothered by any of the following problems&#8221;)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1730DepressionScreening", "02", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("02"), new { @id = Model.TypeName + "_M1730DepressionScreening2", @title = "(OASIS M1720) Depression Screening, Standardized Assessment Results in Further Evaluation" })%>
                        <label for="<%= Model.TypeName %>_M1730DepressionScreening2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, with a different standardized assessment-and the patient meets criteria for further evaluation for depression.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1730DepressionScreening", "03", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("03"), new { @id = Model.TypeName + "_M1730DepressionScreening3", @title = "(OASIS M1720) Depression Screening, Standardized Assessment Results in No Further Evaluation" })%>
                        <label for="<%= Model.TypeName %>_M1730DepressionScreening3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Yes, patient was screened with a different standardized assessment-and the patient does not meet criteria for further evaluation for depression.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1730');" title="More Information about M1730">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_Phq2">
                <div class="strong">PHQ-2&#169;*</div>
                <table class="form align-center">
                    <thead>
                        <tr>
                            <th></th>
                            <th>
                                Not at all<br />
                                (0 &#8211; 1 day)
                            </th>
                            <th>
                                Several days<br />
                                (2 &#8211; 6 days)
                            </th>
                            <th>
                                More than half of the days<br />
                                (7 &#8211; 11 days)
                            </th>
                            <th>
                                Nearly every day<br />
                                (12 &#8211; 14 days)
                            </th>
                            <th>N/A Unable to respond</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup padfix">
                        <tr>
                            <td class="align-left strong">
                                a) Little interest or pleasure in doing things
                                <%= Html.Hidden(Model.TypeName + "_M1730DepressionScreeningInterest", "", new { @id = Model.TypeName + "_M1730DepressionScreeningInterestHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningInterest0' name='{0}_M1730DepressionScreeningInterest' value='00' title='(Optional) PHQ-2, Not at all' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("00").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterest0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningInterest1' name='{0}_M1730DepressionScreeningInterest' value='01' title='(Optional) PHQ-2, Reduced Interest, Several Days' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("01").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterest1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningInterest2' name='{0}_M1730DepressionScreeningInterest' value='02' title='(Optional) PHQ-2, Reduced Interest, More than Half of the Days' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("02").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterest2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningInterest3' name='{0}_M1730DepressionScreeningInterest' value='03' title='(Optional) PHQ-2, Reduced Interest, Nearly Every Day' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("03").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterest3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningInterestNA' name='{0}_M1730DepressionScreeningInterest' value='NA' title='(Optional) PHQ-2, Reduced Interest, Unable to Respond' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("NA").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterestNA" class="radio">NA</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="strong align-left">
                                b) Feeling down, depressed, or hopeless?
                                <%= Html.Hidden(Model.TypeName + "_M1730DepressionScreeningHopeless")%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningHopeless0' name='{0}_M1730DepressionScreeningHopeless' value='00' title='(Optional) PHQ-2, Depression/Despair, Not at All' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("00").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopeless0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningHopeless1' name='{0}_M1730DepressionScreeningHopeless' value='01' title='(Optional) PHQ-2, Depression/Despair, Several Days' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("01").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopeless1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningHopeless2' name='{0}_M1730DepressionScreeningHopeless' value='02' title='(Optional) PHQ-2, Depression/Despair,  More than Half of the Days' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("02").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopeless2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningHopeless3' name='{0}_M1730DepressionScreeningHopeless' value='03' title='(Optional) PHQ-2, Depression/Despair, Nearly Every Day' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("03").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopeless3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_M1730DepressionScreeningHopelessNA' name='{0}_M1730DepressionScreeningHopeless' value='NA' title='(Optional) PHQ-2, Depression/Despair, Unable to Respond' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("NA").ToChecked())%>
                                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopelessNA" class="radio">NA</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float-right"><em>Copyright&#169; Pfizer Inc. All rights reserved. Reproduced with permission.</em></div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset class="oasis">
        <legend>Cognition</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1740">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1740" class="green" onclick="Oasis.ToolTip('M1740')">(M1740)</a>
                    Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed):
                    <em>(Mark all that apply.)</em>
                </label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficitHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Memory Deficit' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsImpDes", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsImpDesHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Impaired Decision-Making' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsImpDes").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsImpDes">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsVerbal", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsVerbalHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Verbal Disruption' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsVerbal").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsVerbal">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsPhysical", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsPhysicalHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Physical Aggression' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsPhysical").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsPhysical">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsSIB", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsSIBHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Disruptive/Infantile' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsSIB' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsSIB' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsSIB").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsSIB">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsDelusional", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsDelusionalHidden" })%>
                        <input name="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsDelusional" value="" type="hidden" />
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, Delusional/Hallucinatory/Paranoid' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsDelusional").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsDelusional">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">Delusional, hallucinatory, or paranoid behavior</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsNone", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsNoneHidden" })%>
                        <%= string.Format("<input title='(OASIS M1720) Weekly Psychiatric Symptoms, None of the Above' id='{0}_M1740CognitiveBehavioralPsychiatricSymptomsNone' class='M1740' name='{0}_M1740CognitiveBehavioralPsychiatricSymptomsNone' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsNone").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsNone">
                            <span class="float-left">7 &#8211;</span>
                            <span class="normal margin">None of the above behaviors demonstrated</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1740');" title="More Information about M1740">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1745">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1745" class="green" onclick="Oasis.ToolTip('M1745')">(M1745)</a>
                    Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "", new { @id = Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequencyHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("00"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency0", @title = "(OASIS M1745) Disruptive Behavior Frequency, Never" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Never</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("01"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency1", @title = "(OASIS M1745) Disruptive Behavior Frequency, Less than Monthly" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Less than once a month</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("02"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency2", @title = "(OASIS M1745) Disruptive Behavior Frequency, Monthly" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Once a month</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("03"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency3", @title = "(OASIS M1745) Disruptive Behavior Frequency, Several Times per Month" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Several times each month</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("04"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency4", @title = "(OASIS M1745) Disruptive Behavior Frequency, Several Times per Week" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Several times a week</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("05"), new { @id = "M1745DisruptiveBehaviorSymptomsFrequency5", @title = "(OASIS M1745) Disruptive Behavior Frequency, Daily" })%>
                        <label for="M1745DisruptiveBehaviorSymptomsFrequency5">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">At least daily</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1745');" title="More Information about M1745">?</div>
                </div>
            </div>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M1750">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1750" class="green" onclick="Oasis.ToolTip('M1750')">(M1750)</a>
                    Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1750PsychiatricNursingServicing", "", new { @id = Model.TypeName + "_M1750PsychiatricNursingServicingHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1750PsychiatricNursingServicing", "0", data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("0"), new { @id = "M1750PsychiatricNursingServicing0", @title = "(OASIS M1750) Psychiatric Nursing Services, No" })%>
                        <label for="M1750PsychiatricNursingServicing0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                        <div class="clear"></div>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1750PsychiatricNursingServicing", "1", data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("1"), new { @id = "M1750PsychiatricNursingServicing1", @title = "(OASIS M1750) Psychiatric Nursing Services, Yes" })%>
                        <label for="M1750PsychiatricNursingServicing1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1750');" title="More Information about M1750">?</div>
                </div>
            </div>
        <%  } %>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/NeuroBehavioral.ascx", Model); %>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">    
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.ChangeToRadio("<%= Model.TypeName %>_M1730DepressionScreeningInterest");
    U.ChangeToRadio("<%= Model.TypeName %>_M1730DepressionScreeningHopeless");
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus1"),
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus1More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus2"),
        $(".<%= Model.TypeName %>_GenericNeurologicalStatus2More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus3"),
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus3More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus4"),
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus5"),
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus9"),
        $("#<%= Model.TypeName %>_GenericNeurologicalStatus9More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485MentalStatus8"),
        $("#<%= Model.TypeName %>_485MentalStatus8More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericBehaviour8"),
        $("#<%= Model.TypeName %>_GenericBehaviour8More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericPsychosocial5"),
        $("#<%= Model.TypeName %>_GenericPsychosocial5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485PatientStrengths5"),
        $("#<%= Model.TypeName %>_485PatientStrengths5More"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_M1730DepressionScreening", "01",
        $("#<%= Model.TypeName %>_Phq2"));
    U.NoneOfTheAbove(
        $("#<%= Model.TypeName %>_M1740CognitiveBehavioralPsychiatricSymptomsNone"),
        $("<%= Model.TypeName %>_M1740 .M1740"));
</script>