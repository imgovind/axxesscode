<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var TherapyInterventions = data.AnswerArray("485TherapyInterventions"); %>
<%  if (TherapyInterventions.Length > 0 || (data.ContainsKey("485TherapyComments") && data["485TherapyComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (TherapyInterventions.Contains("1")) { %>
    printview.checkbox("Physical therapist to evaluate and submit plan of treatment.",true) +
    <%  } %>
    <%  if (TherapyInterventions.Contains("2")) { %>
    printview.checkbox("Occupational therapist to evaluate and submit plan of treatment.",true) +
    <%  } %>
    <%  if (TherapyInterventions.Contains("3")) { %>
    printview.checkbox("Speech therapist to evaluate and submit plan of treatment.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485TherapyComments") && data["485TherapyComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485TherapyComments").Clean()%>") +
    <%  } %>
    "","Therapy Need Interventions");
<%  } %>