<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<% string[] nursingInterventions = data.AnswerArray("485NursingInterventions"); %>
<% string[] nursingGoals = data.AnswerArray("485NursingGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485NursingInterventions" value=" " />
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions1' name='{0}_485NursingInterventions' value='1' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485NursingInterventions1">Physical therapy to evaluate.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions2' name='{0}_485NursingInterventions' value='2' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("2").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485NursingInterventions2">Occupational therapy to evaluate.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions3' name='{0}_485NursingInterventions' value='3' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("3").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485NursingInterventions3">SN to assess/instruct on pain management, proper body mechanics and safety measures.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions4' name='{0}_485NursingInterventions' value='4' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("4").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485NursingInterventions4" class="radio">SN to assess for patient adherence to appropriate activity levels.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions5' name='{0}_485NursingInterventions' value='5' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("5").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485NursingInterventions5" class="radio">SN to assess patient&#8217;s compliance with home exercise program.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions6' name='{0}_485NursingInterventions' value='6' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("6").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NursingInterventions6">SN to instruct the</label>
                        <%  var instructRomExcercisePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.ContainsKey("485InstructRomExcercisePerson") && data["485InstructRomExcercisePerson"].Answer != "" ? data["485InstructRomExcercisePerson"].Answer : "Patient/Caregiver");%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructRomExcercisePerson", instructRomExcercisePerson)%>
                        <label for="<%= Model.TypeName %>_485NursingInterventions6">on proper ROM exercises and body alignment techniques.</label>
                    </span>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT" || Model.Discipline == "ST")
    { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions7' name='{0}_485NursingInterventions' value='7' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("7").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NursingInterventions7">HHA (freq)</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructHhaFreq", data.AnswerOrEmptyString("485InstructHhaFreq"), new { @id = Model.TypeName + "_485InstructHhaFreq", @class = "zip" })%>
                        <label for="<%= Model.TypeName %>_485NursingInterventions7">assistance with ADLs/IADLs.</label>
                    </span>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485ADLComments">Additional Orders:</label>
            <%= Html.ToggleTemplates(Model.TypeName + "_485ADLOrderTemplates", "", "#" + Model.TypeName + "_485ADLComments")%>
            <%= Html.TextArea(Model.TypeName + "_485ADLComments", data.ContainsKey("485ADLComments") ? data["485ADLComments"].Answer : "", 5, 70, new { @id = Model.TypeName + "_485ADLComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485NursingGoals" id="<%= Model.TypeName %>_485EstablishHomeExercisePT" />
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals1' name='{0}_485NursingGoals' value='1' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("1") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485NursingGoals1" class="radio">Patient will have increased mobility, self care, endurance, ROM and decreased pain by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals2' name='{0}_485NursingGoals' value='2' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("2") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485NursingGoals2" class="radio">Patient will maintain optimal joint function, increased mobility and independence in ADL&#8217;s by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals3' name='{0}_485NursingGoals' value='3' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("3") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485NursingGoals3" class="radio">Patient&#8217;s strength, endurance and mobility will be improved.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals4' name='{0}_485NursingGoals' value='4' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("4") ? "checked='checked'" : "")%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NursingGoals4">The</label>
                        <%  var demonstrateROMExcercisePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.ContainsKey("485DemonstrateROMExcercisePerson") && data["485DemonstrateROMExcercisePerson"].Answer != "" ? data["485DemonstrateROMExcercisePerson"].Answer : "Patient/Caregiver");%>
                        <%= Html.DropDownList(Model.TypeName + "_485DemonstrateROMExcercisePerson", demonstrateROMExcercisePerson)%>
                        <label for="<%= Model.TypeName %>_485NursingGoals4">will demonstrate proper ROM exercise and body alignment techniques.</label>
                    </span>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT" || Model.Discipline == "ST")
    { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals5' name='{0}_485NursingGoals' value='5' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("5") ? "checked='checked'" : "")%>
                    <label for="<%= Model.TypeName %>_485NursingGoals5" class="radio">Patient&#8217;s ADL/IADL needs will be met with assistance of HHA.</label>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485ADLGoalComments">Additional Goals:</label>
            <%= Html.ToggleTemplates(Model.TypeName + "_485ADLGoalTemplates", "", "#" + Model.TypeName + "_485ADLGoalComments")%>
            <%= Html.TextArea(Model.TypeName + "_485ADLGoalComments", data.ContainsKey("485ADLGoalComments") ? data["485ADLGoalComments"].Answer : "", 5, 70, new { @id = Model.TypeName + "_485ADLGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>