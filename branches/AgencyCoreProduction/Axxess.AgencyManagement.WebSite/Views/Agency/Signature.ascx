﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <fieldset class="editmed">
        <legend>Verify Signature</legend>
        <div>Before proceeding, enter your signature in the textbox provided below to confirm that you are authorized to make changes to your company information.</div>
        <div class="wide-column">
            <div class="row">
                <label for="checkSignature" class="float-left">Signature:</label>
                <div class="float-left"><%= Html.Password("signature", "", new { @id = "checkSignature", @class = "text input_wrapper" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="Agency.CheckSignature();">Proceed</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>

