﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Insurance/Payor | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Insurance", FormMethod.Post, new { @id = "newInsuranceForm" })) { %>
    <% var Model = new AgencyInsurance { InvoiceType = (int)InvoiceType.UB }; %>
    <% var locator = new Dictionary<string, Locator>(); %>
    <div class="wrapper main">
        <fieldset>
            <legend>Information</legend>
            <div class="column">
                <div class="row"><label for="New_Insurance_Name" class="float-left">Insurance/Payor Name:</label><div class="float-right"><%= Html.TextBox("Name", "", new { @id = "New_Insurance_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
                <div class="row"><label for="New_Insurance_PayorType" class="float-left">Payor Type:</label><div class="float-right"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", "", new { @id = "New_Insurance_PayorType", @class = "requireddropdown valid" })%></div></div>
                <div class="row"><label for="New_Insurance_InvoiceType" class="float-left">Invoice Type:</label><div class="float-right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "New_Insurance_InvoiceType" })%></div></div>
                <div class="row"><label for="New_Insurance_BillType" class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "Institutional", Value = "institutional" }, new SelectListItem { Text = "Professional", Value = "professional" } }, "Value", "Text", "institutional"); %><%= Html.DropDownList("BillType", billType, new { @id = "New_Insurance_BillType" })%></div></div>
                 <div class="row">
                    <div class="float-left">
                        <%= Html.CheckBox("HasContractWithAgency", false, new { @id = "New_Insurance_HasContractWithAgency", @class = "radio float-left" })%>&nbsp;
                        <label for="New_Insurance_HasContractWithAgency"> Check here if you have a contract with this insurance.</label>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="row"><label for="New_Insurance_PayorId" class="float-left">Payor ID:</label><div class="float-right"><%= Html.TextBox("PayorId", "", new { @id = "New_Insurance_PayorId", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
                <div class="row"><label for="New_Insurance_ProviderId" class="float-left">Provider ID/Code:</label><div class="float-right"><%= Html.TextBox("ProviderId", "", new { @id = "New_Insurance_ProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
                <div class="row"><label for="New_Insurance_OtherProviderId" class="float-left">Other Provider ID:</label><div class="float-right"><%= Html.TextBox("OtherProviderId", "", new { @id = "New_Insurance_OtherProviderId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
                <div class="row"><label for="New_Insurance_ProviderSubscriberId" class="float-left">Provider Subscriber ID:</label><div class="float-right"><%= Html.TextBox("ProviderSubscriberId", "", new { @id = "New_Insurance_ProviderSubscriberId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
                <div class="row"><label for="New_Insurance_SubmitterId" class="float-left">Submitter ID:</label><div class="float-right"><%= Html.TextBox("SubmitterId", "", new { @id = "New_Insurance_SubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            </div><div class="clear"></div>
        </fieldset>
        <fieldset>
            <legend>Locator Specification</legend>
                <div id="New_Insurance_InvoiceLocators" class="<%= (Model.InvoiceType != (int)InvoiceType.UB && Model.InvoiceType != (int)InvoiceType.HCFA)  ? "" : "hidden" %>">
                    <div class="column">
                        <div class="row">
                            <label class="float-left">N/A</label>
                        </div>
                    </div>
                </div>
            <div id="New_Insurance_Ub04Locators" class="<%= Model.InvoiceType == (int)InvoiceType.UB ? "" : "hidden" %>">
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <%= Html.Hidden("Ub04Locator81cca", "Locator2")%>
                <%= Html.Hidden("Ub04Locator81cca", "Locator3")%>
                <%= Html.Hidden("Ub04Locator81cca", "Locator4")%>
                <%= Html.Hidden("Ub04Locator81ccaPart2", "Locator76")%>
                <%= Html.Hidden("Ub04Locator81ccaPart2", "Locator77")%>
                <%= Html.Hidden("Ub04Locator81ccaPart2", "Locator78")%>
                <%= Html.Hidden("Ub04Locator81ccaPart2", "Locator79")%>
                <% var physicianType77 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "Primary Care Physician", Value = "PCP" }, new SelectListItem { Text = "Referring Physician", Value = "referring" }, new SelectListItem { Text = "Other", Value = "other" } }, "Value", "Text", locator.ContainsKey("Locator77") ? locator["Locator77"].PhysicianType : string.Empty); %>
                <% var physicianType78 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "Primary Care Physician", Value = "PCP" }, new SelectListItem { Text = "Referring Physician", Value = "referring" }, new SelectListItem { Text = "Other", Value = "other" } }, "Value", "Text", locator.ContainsKey("Locator78") ? locator["Locator78"].PhysicianType : string.Empty); %>
                <% var physicianType79 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "Primary Care Physician", Value = "PCP" }, new SelectListItem { Text = "Referring Physician", Value = "referring" }, new SelectListItem { Text = "Other", Value = "other" } }, "Value", "Text", locator.ContainsKey("Locator79") ? locator["Locator79"].PhysicianType : string.Empty); %>
                <div class="wide-column">
                    <div class="onethird">
                        <%=string.Format("<input type='checkbox' name='Locator76_Customized' value='1' class='radio float-left LocatorCheckBox' {0} />", locator.ContainsKey("Locator76") && locator["Locator76"].Customized ? "checked" : string.Empty)%>
                        <label class="float-left">
                            UB04 Locator 76 Attending</label>
                    </div>
                    <div id="Locator76_Customized" class="twothird">
                        <div class="float-right">
                            <label for="New_Insurance_Locator76_Code1">
                                NPI</label><%= Html.TextBox("Locator76_Code1", locator.ContainsKey("Locator76") ? locator["Locator76"].Code1 : string.Empty, new { @id = "New_Insurance_Locator76_Code1", @class = "name_short input_wrapper" })%>
                            <label for="New_Insurance_Locator76_Code2">
                                Last</label><%= Html.TextBox("Locator76_Code2", locator.ContainsKey("Locator76") ? locator["Locator76"].Code2 : string.Empty, new { @id = "New_Insurance_Locator76_Code2", @class = "text input_wrapper" })%>
                            <label for="New_Insurance_Locator76_Code3">
                                First</label><%= Html.TextBox("Locator76_Code3", locator.ContainsKey("Locator76") ? locator["Locator76"].Code3 : string.Empty, new { @id = "New_Insurance_Locator76_Code3", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                    <div class="clear" />
                    <div class="onethird">
                        <%=string.Format("<input type='checkbox' name='Locator77_Customized' value='1' class='radio float-left LocatorCheckBox' {0} />", locator.ContainsKey("Locator77") && locator["Locator77"].Customized ? "checked" : string.Empty)%>
                        <label class="float-left">
                            UB04 Locator 77 Operating</label>
                    </div>
                    <div id="Locator77_Customized" class="twothird">
                        <%= Html.DropDownList("Locator77_PhysicianType", physicianType77, new { @id = "New_Locator77_PhysicianType", @class="PhysicianType" })%>
                        <div class="float-right">
                            <label for="New_Insurance_Locator77_Code1">
                                NPI</label><%= Html.TextBox("Locator77_Code1", locator.ContainsKey("Locator77") ? locator["Locator77"].Code1 : string.Empty, new { @id = "New_Insurance_Locator77_Code1", @class = "name_short input_wrapper" })%>
                            <label for="New_Insurance_Locator77_Code2">
                                Last</label><%= Html.TextBox("Locator77_Code2", locator.ContainsKey("Locator77") ? locator["Locator77"].Code2 : string.Empty, new { @id = "New_Insurance_Locator77_Code2", @class = "text input_wrapper" })%>
                            <label for="New_Insurance_Locator77_Code3">
                                First</label><%= Html.TextBox("Locator77_Code3", locator.ContainsKey("Locator77") ? locator["Locator77"].Code3 : string.Empty, new { @id = "New_Insurance_Locator77_Code3", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                    <div class="clear" />
                    <div class="onethird">
                        <%=string.Format("<input type='checkbox' name='Locator78_Customized' value='1' class='radio float-left LocatorCheckBox' {0} />", locator.ContainsKey("Locator78") && locator["Locator78"].Customized ? "checked" : string.Empty)%>
                        <label class="float-left">
                            UB04 Locator 78 Other</label>
                    </div>
                    <div id="Locator78_Customized" class="twothird">
                        <%= Html.DropDownList("Locator78_PhysicianType", physicianType78, new { @id = "New_Locator78_PhysicianType", @class = "PhysicianType" })%>
                        <div class="float-right">
                            <label for="New_Insurance_Locator78_Code1">
                                NPI</label><%= Html.TextBox("Locator78_Code1", locator.ContainsKey("Locator78") ? locator["Locator78"].Code1 : string.Empty, new { @id = "New_Insurance_Locator78_Code1", @class = "name_short input_wrapper" })%>
                            <label for="New_Insurance_Locator78_Code2">
                                Last</label><%= Html.TextBox("Locator78_Code2", locator.ContainsKey("Locator78") ? locator["Locator78"].Code2 : string.Empty, new { @id = "New_Insurance_Locator78_Code2", @class = "text input_wrapper" })%>
                            <label for="New_Insurance_Locator78_Code3">
                                First</label><%= Html.TextBox("Locator78_Code3", locator.ContainsKey("Locator78") ? locator["Locator78"].Code3 : string.Empty, new { @id = "New_Insurance_Locator78_Code3", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                    <div class="clear" />
                    <div class="onethird">
                        <%=string.Format("<input type='checkbox' name='Locator79_Customized' value='1' class='radio float-left LocatorCheckBox' {0} />", locator.ContainsKey("Locator79") && locator["Locator79"].Customized ? "checked" : string.Empty)%>
                        <label class="float-left">
                            UB04 Locator 79 Other</label>
                    </div>
                    <div id="Locator79_Customized" class="twothird">
                        <%= Html.DropDownList("Locator79_PhysicianType", physicianType79, new { @id = "New_Locator79_PhysicianType", @class="PhysicianType" })%>
                        <div class="float-right">
                            <label for="New_Insurance_Locator79_Code1">
                                NPI</label><%= Html.TextBox("Locator79_Code1", locator.ContainsKey("Locator79") ? locator["Locator79"].Code1 : string.Empty, new { @id = "New_Insurance_Locator79_Code1", @class = "name_short input_wrapper" })%>
                            <label for="New_Insurance_Locator79_Code2">
                                Last</label><%= Html.TextBox("Locator79_Code2", locator.ContainsKey("Locator79") ? locator["Locator79"].Code2 : string.Empty, new { @id = "New_Insurance_Locator79_Code2", @class = "text input_wrapper" })%>
                            <label for="New_Insurance_Locator79_Code3">
                                First</label><%= Html.TextBox("Locator79_Code3", locator.ContainsKey("Locator79") ? locator["Locator79"].Code3 : string.Empty, new { @id = "New_Insurance_Locator79_Code3", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                </div>
                <div class="clear" />
                <div class="wide-column">
                    <div class="row">
                        <label for="New_Insurance_Ub04Locator81cca" class="float-left">
                            UB04 Locator 81CCa:</label><div class="float-right">
                                <%= Html.TextBox("Locator1_Code1", locator.ContainsKey("Locator1") ? locator["Locator1"].Code1 : string.Empty, new { @id = "New_Insurance_Locator1_Code1", @class = "text sn", @maxlength = "2" })%>
                                <%= Html.TextBox("Locator1_Code2", locator.ContainsKey("Locator1") ? locator["Locator1"].Code2 : string.Empty, new { @id = "New_Insurance_Locator1_Code2", @class = "text input_wrapper", @maxlength = "10" })%><%= Html.TextBox("Locator1_Code3", locator.ContainsKey("Locator1") ? locator["Locator1"].Code3 : string.Empty, new { @id = "New_Insurance_Locator1_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
                        <div class="clear">
                        </div>
                        <label for="New_Insurance_Ub04Locator81cca" class="float-left">
                            UB04 Locator 81CCb:</label><div class="float-right">
                                <%= Html.TextBox("Locator2_Code1", locator.ContainsKey("Locator2") ? locator["Locator2"].Code1 : string.Empty, new { @id = "New_Insurance_Locator2_Code1", @class = "text sn", @maxlength = "2" })%>
                                <%= Html.TextBox("Locator2_Code2", locator.ContainsKey("Locator2") ? locator["Locator2"].Code2 : string.Empty, new { @id = "New_Insurance_Locator2_Code2", @class = "text input_wrapper", @maxlength = "10" })%><%= Html.TextBox("Locator2_Code3", locator.ContainsKey("Locator2") ? locator["Locator2"].Code3 : string.Empty, new { @id = "New_Insurance_Locator2_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
                        <div class="clear">
                        </div>
                        <label for="New_Insurance_Ub04Locator81cca" class="float-left">
                            UB04 Locator 81CCc:</label><div class="float-right">
                                <%= Html.TextBox("Locator3_Code1", locator.ContainsKey("Locator3") ? locator["Locator3"].Code1 : string.Empty, new { @id = "New_Insurance_Locator3_Code1", @class = "text sn", @maxlength = "2" })%>
                                <%= Html.TextBox("Locator3_Code2", locator.ContainsKey("Locator3") ? locator["Locator3"].Code2 : string.Empty, new { @id = "New_Insurance_Locator3_Code2", @class = "text input_wrapper", @maxlength = "10" })%><%= Html.TextBox("Locator3_Code3", locator.ContainsKey("Locator3") ? locator["Locator3"].Code3 : string.Empty, new { @id = "New_Insurance_Locator3_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
                        <div class="clear">
                        </div>
                        <label for="New_Insurance_Ub04Locator81cca" class="float-left">
                            UB04 Locator 81CCd:</label><div class="float-right">
                                <%= Html.TextBox("Locator4_Code1", locator.ContainsKey("Locator4") ? locator["Locator4"].Code1 : string.Empty, new { @id = "New_Insurance_Locator4_Code1", @class = "text sn", @maxlength = "2" })%>
                                <%= Html.TextBox("Locator4_Code2", locator.ContainsKey("Locator4") ? locator["Locator4"].Code2 : string.Empty, new { @id = "New_Insurance_Locator4_Code2", @class = "text input_wrapper", @maxlength = "10" })%><%= Html.TextBox("Locator4_Code3", locator.ContainsKey("Locator4") ? locator["Locator4"].Code3 : string.Empty, new { @id = "New_Insurance_Locator4_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
                    </div>
                </div>
                <div class="clear" />
            </div>
        <div id="New_Insurance_HCFALocators" class="<%= Model.InvoiceType == (int)InvoiceType.HCFA ? "" : "hidden" %>">
                    <div class="column">
                        <div class="row">
                            <label class="float-left">HCFA1500 Locator 26: Display Patient MRN ?</label>
                            <div class="float-right">
                                <%= Html.Hidden("HCFALocators", "26Locator")%>
                                <%= locator.ContainsKey("26Locator") ? "<input type='checkbox' name='26Locator_Code1' value='true' id='New_Insurance_Locator26' class='radio float-left' checked='checked'>" : "<input type='checkbox' name='26Locator_Code1' value='true' id='New_Insurance_Locator26' class='radio float-left'>" %>
                                <label class="float-left">Yes</label>
                                <%= Html.Hidden("26Locator_Type", (int)LocatorType.Boolean)%>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <label class="float-left">HCFA1500 Locator 30: Display Amount Due ?</label>
                            <div class="float-right">
                                <%= Html.Hidden("HCFALocators", "30Locator")%>
                                <%= locator.ContainsKey("30Locator") ? "<input type='checkbox' name='30Locator_Code1' value='true' id='New_Insurance_Locator30' class='radio float-left' checked='checked'>" : "<input type='checkbox' name='30Locator_Code1' value='true' id='New_Insurance_Locator30' class='radio float-left'>" %>
				                <label class="float-left">Yes</label>
                                <%= Html.Hidden("30Locator_Type", (int)LocatorType.Boolean)%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="New_Insurance_HCFALocator" class="float-left">HCFA1500 Locator 31: </label>
                            <div class="float-right">
                                <%= Html.Hidden("HCFALocators", "31Locator")%>
                                <%= Html.TextBox("31Locator_Code1", locator.ContainsKey("31Locator") ? locator["31Locator"].Code1.ToString() : "Signature on File", new { @id = "New_Insurance_Locator31", @class = "text input_wrapper"})%> 
                                <%= Html.Hidden("31Locator_Type", (int)LocatorType.String)%>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="row">
                            <label class="float-left">HCFA1500 Locator 29: Display Amount Paid ?</label>
                            <div class="float-right">
                                <%= Html.Hidden("HCFALocators", "29Locator")%>
                                <%= locator.ContainsKey("29Locator") ? "<input type='checkbox' name='29Locator_Code1' value='true' id='New_Insurance_Locator29' class='radio float-left' checked='checked'>" : "<input type='checkbox' name='29Locator_Code1' value='true' id='New_Insurance_Locator29' class='radio float-left'>" %>
                                <label class="float-left">Yes</label>
                                <%= Html.Hidden("29Locator_Type", (int)LocatorType.Boolean)%>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <label for="New_Insurance_HCFALocator" class="float-left">HCFA1500 Locator 32: Display Service Facility Location?</label>
                        </div>
                        <div class="row">
                            <%= Html.Hidden("HCFALocators", "32Locator")%>
                            <%= Html.RadioButton("32Locator_Code1", "0", locator.ContainsKey("32Locator") ? (locator["32Locator"].Code1.Equals("0") ? true : false) : true, new { @id = "New_Insurance_Locator32_Code1_no", @class = "required radio" })%>
                            <label for="32Locator_No" class="inline-radio">No</label>
                            <%= Html.RadioButton("32Locator_Code1", "1", locator.ContainsKey("32Locator") ? (locator["32Locator"].Code1.Equals("1") ? true : false) : false, new { @id = "New_Insurance_Locator32_Code1_yess", @class = "required radio" })%>
                            <label for="32Locator_YesSameAs33" class="inline-radio">Yes (Same as Locator 33)</label>
                            <%= Html.RadioButton("32Locator_Code1", "2", locator.ContainsKey("32Locator") ? (locator["32Locator"].Code1.Equals("2") ? true : false) : false, new { @id = "New_Insurance_Locator32_Code1_yesd", @class = "required radio" })%>
                            <label for="32Locator_YesDiff" class="inline-radio">Yes (Different)</label>
                            <%= Html.Hidden("32Locator_Type", (int)LocatorType.String)%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="New_Insurance_HCFALocators32Address">
                        <div class="column">
                            <div class="row">
                                <label class="float-left">Service Facility Location Name:</label>
                                <div class="float-right">
                                    <%= Html.Hidden("HCFALocators", "32LocatorName")%>
                                    <%= Html.TextBox("32LocatorName_Code1", locator.ContainsKey("32LocatorName") ? locator["32LocatorName"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorName", @class = "text input_wrapper" })%> 
                                    <%= Html.Hidden("32LocatorName_Type", (int)LocatorType.String)%>
                                </div>
                            </div>
                            <div class="row">
                                <label class="float-left">Address Line 1</label>
                                <div class="float-right">
                                    <%= Html.Hidden("HCFALocators", "32LocatorAddressLine1")%>
                                    <%= Html.TextBox("32LocatorAddressLine1_Code1", locator.ContainsKey("32LocatorAddressLine1") ? locator["32LocatorAddressLine1"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorAddressLine1", @class = "text input_wrapper"})%> 
                                    <%= Html.Hidden("32LocatorAddressLine1_Type", (int)LocatorType.String)%>
                                </div>
                            </div>
                            <div class="row">
                                <label class="float-left">Address Line 2</label>
                                <div class="float-right">
                                    <%= Html.Hidden("HCFALocators", "32LocatorAddressLine2")%>
                                    <%= Html.TextBox("32LocatorAddressLine2_Code1", locator.ContainsKey("32LocatorAddressLine2") ? locator["32LocatorAddressLine2"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorAddressLine2", @class = "text input_wrapper"})%> 
                                    <%= Html.Hidden("32LocatorAddressLine2_Type", (int)LocatorType.String)%>
                                </div>
                            </div>
                            
                        </div>
                        <div class="column">
                            <div class="row">
                                <label class="float-left">Service Facility Location NPI:</label>
                                <div class="float-right">
                                    <%= Html.Hidden("HCFALocators", "32LocatorNPI")%>
                                    <%= Html.TextBox("32LocatorNPI_Code1", locator.ContainsKey("32LocatorNPI") ? locator["32LocatorNPI"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorNPI", @class = "text input_wrapper" })%> 
                                    <%= Html.Hidden("32LocatorNPI_Type", (int)LocatorType.String)%>
                                </div>
                            </div>
                            <div class="row">
                                <label class="float-left">City</label>
                                <div class="float-right">
                                    <%= Html.Hidden("HCFALocators", "32LocatorAddressCity")%>
                                    <%= Html.TextBox("32LocatorAddressCity_Code1", locator.ContainsKey("32LocatorAddressCity") ? locator["32LocatorAddressCity"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorAddressCity", @class = "text input_wrapper"})%> 
                                    <%= Html.Hidden("32LocatorAddressCity_Type", (int)LocatorType.String)%>
                                </div>
                            </div>
                            <div class="row">
                                <%= Html.Hidden("HCFALocators", "32LocatorAddressState")%>
                                <%= Html.Hidden("HCFALocators", "32LocatorAddressZip")%>
                                <label for="New_Patient_AddressStateCode" class="float-left"><span></span> State, <span></span> Zip</label>
                                <div class="float-right"><%= Html.States("32LocatorAddressState_Code1", locator.ContainsKey("32LocatorAddressState") ? locator["32LocatorAddressState"].Code1.ToString() : string.Empty, new { @id = "New_Insurance_32LocatorAddressState", @class = "AddressStateCode requireddropdown valid" })%><%= Html.TextBox("32LocatorAddressZip_Code1", locator.ContainsKey("32LocatorAddressZip") ? locator["32LocatorAddressZip"].Code1 : string.Empty, new { @id = "New_Insurance_32LocatorAddressZip", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div>
                                <%= Html.Hidden("32LocatorAddressState_Type", (int)LocatorType.String)%>
                                <%= Html.Hidden("32LocatorAddressZip_Type", (int)LocatorType.String)%>
                            </div>
                        </div>
                    </div>
                </div>
        </fieldset>
        <fieldset>
            <legend>EDI Information</legend>
            <div class="wide-column"> 
                <div class="float-left">
                    <%= Html.CheckBox("IsAxxessTheBiller",false, new { @id = "New_Insurance_IsAxxessTheBiller", @class = "radio float-left" })%>&nbsp;
                    <label for="New_Insurance_IsAxxessTheBiller"> Check here if claims are electronically submitted to your clearing house through Axxess™.</label>
                </div>
                <div class="row">
                <label for="New_Insurance_ClearingHouse" class="float-left">Clearing House:</label>
                    <div class="float-left">
                    <%  var clearingHouse = new SelectList(new[] {
                                new SelectListItem { Text = string.Empty, Value = "0" },
                                new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                                new SelectListItem { Text = "Claimsnet", Value = "Claimsnet" },
                            }, "Value", "Text","0"); %>
                        <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "New_Insurance_ClearingHouse" })%></div></div></div><div class="clear"></div>
            <div id="New_Insurance_EdiInformation">
              <div class="column">
                <div class="row"><label for="New_Insurance_InterchangeReceiverId" class="float-left">Interchange Receiver ID:</label><div class="float-right"><%  var interchangeReceiverId = new SelectList(new[] {
                                new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                                new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                                new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                                new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                                new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                                new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                                new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                                new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                                new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                            }, "Value", "Text","28"); %>
                
                <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "New_Insurance_InterchangeReceiverId", @class = "valid" })%></div></div>
                <div class="row"><label for="New_Insurance_ClearingHouseSubmitterId" class="float-left">Clearing House Submitter ID:</label><div class="float-right"><%= Html.TextBox("ClearingHouseSubmitterId", "", new { @id = "New_Insurance_ClearingHouseSubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            </div>
              <div class="column">
                <div class="row"><label for="New_Insurance_SubmitterName" class="float-left">Submitter Name:</label><div class="float-right"><%= Html.TextBox("SubmitterName", "", new { @id = "New_Insurance_SubmitterName", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
                <div class="row"><label for="New_Insurance_SubmitterPhone1" class="float-left">Submitter Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short"name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone3" maxlength="4" /></div></div>
            </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Insurance Address</legend>
            <div class="column">
                <div class="row"><label for="New_Insurance_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Insurance_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
                <div class="row"><label for="New_Insurance_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Insurance_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label for="New_Insurance_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Insurance_AddressCity", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
                <div class="row"><label for="New_Insurance_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.States("AddressStateCode", "", new { @id = "New_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div></div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Insurance Contact Person</legend>
            <div class="column">
                <div class="row"><label for="New_Insurance_ContactPersonFirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
                <div class="row"><label for="New_Insurance_ContactPersonLastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "New_Insurance_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
                <div class="row"><label for="New_Insurance_ContactEmailAddress" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("ContactEmailAddress", "", new { @id = "New_Insurance_ContactEmailAddress", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
                <div class="row"><label for="New_Insurance_PhoneNumberArray1" class="float-left">Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short"name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray3" maxlength="4" /></div></div>
                <div class="row"><label for="New_Insurance_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Insurance_FaxNumberArray3" maxlength="4" /></div></div>
            </div>
            <div class="column">
                <div class="row"><label for="New_Insurance_CurrentBalance" class="float-left">Current Balance:</label><div class="float-right"><%= Html.TextBox("CurrentBalance", "", new { @id = "New_Insurance_CurrentBalance", @class = "text input_wrapper" })%></div></div>
                <div class="row"><label for="New_Insurance_WorkWeekStartDay" class="float-left">Work Week Begins:</label><div class="float-right"><% var workWeekStartDay = new SelectList(new[]{ new SelectListItem { Text = "Sunday", Value = "1" },new SelectListItem { Text = "Monday", Value = "2" } }, "Value", "Text");%><%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "New_Insurance_WorkWeekStartDay" })%></div></div><br />
                <div class="row"><label for="New_Insurance_VisitAuthReq" class="float-left">Visit Authorization Required:</label><div class="float-right"><%= Html.RadioButton("IsVisitAuthorizationRequired", "1", new {  @class = "radio" })%><label class="inline-radio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired", "0", true, new { @id = "New_Insurance_VisitAuthReq", @class = "radio" })%><label class="inline-radio">No</label></div></div><br />
            </div>
        </fieldset>
        <fieldset>
            <legend>Load Visit Information from existing Insurance</legend>
             <div class="column">  
                   <label for="New_Insurance_OldInsuranceId" class="float-left">Choose existing Insurance: </label><div class="float-left"><%= Html.AllInsurances("OldInsuranceId", "0", true, "-- Select Insurance --", new { })%></div>
             </div>
         </fieldset>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
                <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newinsurance');">Cancel</a></li>
            </ul>
        </div>
    </div>
<% } %>
<script type="text/javascript">
    U.HideIfChecked($("#New_Insurance_IsAxxessTheBiller"), $("#New_Insurance_EdiInformation"));
    $("#New_Insurance_InvoiceType").change(function() {
        var selectedValue = $(this).val();
        if (selectedValue == 1) {
            $("#New_Insurance_Ub04Locators").removeClass("hidden");
            $("#New_Insurance_HCFALocators").addClass("hidden");
            $("#New_Insurance_InvoiceLocators").addClass("hidden");
        } else if (selectedValue == 2) {
            $("#New_Insurance_HCFALocators").removeClass("hidden");
            $("#New_Insurance_Ub04Locators").addClass("hidden");
            $("#New_Insurance_InvoiceLocators").addClass("hidden");
        } else {
            $("#New_Insurance_InvoiceLocators").removeClass("hidden");
            $("#New_Insurance_Ub04Locators").addClass("hidden");
            $("#New_Insurance_HCFALocators").addClass("hidden");
        }
    });
    U.HideIfChecked($("#New_Insurance_Locator32_Code1_yess"), $("#New_Insurance_HCFALocators32Address"));
    U.HideIfChecked($("#New_Insurance_Locator32_Code1_no"), $("#New_Insurance_HCFALocators32Address"));
    U.ShowIfChecked($("#New_Insurance_Locator32_Code1_yesd"), $("#New_Insurance_HCFALocators32Address"));
    $(".LocatorCheckBox").each(function() {
        var id = $(this).attr('Name');
        if ($(this).is(':checked')) {
            $('#' + id).show();
        } else {
            $('#' + id).hide();
        }
    });
    $(".LocatorCheckBox").change(function() {
        var id = $(this).attr('Name');
        if ($(this).is(':checked')) {
            $('#' + id).show();
        } else {
            $('#' + id).hide();
        }
    });
    $('.PhysicianType').change(function() {
        if ($(this).val() == "other") {
            $(this).next('div').show();
        } else {
            $(this).next('div').hide();
        }
    });
    $('.PhysicianType').each(function() {
        if ($(this).val() == "other") {
            $(this).next('div').show();
        } else {
            $(this).next('div').hide();
        }
    });
</script>
