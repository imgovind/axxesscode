﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyAdjustmentCode>" %>
<span class="wintitle">Edit Adjustment Code | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Update", "AdjustmentCode", FormMethod.Post, new { @id = "editAdjustmentCodeForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_AdjustmentCode_Id" }) %>
<div class="wrapper main">
    
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_AdjustmentCode_Code" class="strong">Code</label>
                <div class="float-right">
                    <%= Html.TextBox("Code", Model.Code, new { @id = "Edit_AdjustmentCode_Code", @class = "required", @maxlength = "100", @style = "width: 300px;" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_AdjustmentCode_Description" class="strong">Description</label>
                <div class="float-right">
                    <%= Html.TextBox("Description", Model.Description, new { @id = "Edit_AdjustmentCode_Description", @class = "required", @maxlength = "100", @style = "width: 300px;" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadAdjustmentCodeLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" class="close">Exit</a></li>
    </ul></div>
</div>
<% } %>
