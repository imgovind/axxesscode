﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <%= Html.Telerik().Grid<UserVisit>().Name("List_Agency_Schedule").Columns(columns =>
        {
            columns.Bound(v => v.PatientName);
            columns.Bound(v => v.TaskName).Title("Task");
            columns.Bound(v => v.VisitDate).Format("{0:MM/dd/yyyy}").Title("Date").Width(100).Visible(false);
            columns.Bound(v => v.StatusName).Width(80).Sortable(false).Title("Status");
            columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=Notes#>\"></a>");
            columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"alert('<#=Id#>');\">Missed Visit Form</a>").Title("Action").Width(180);
        })
        .ClientEvents(c => c.OnRowDataBound("Schedule.ToolTip"))
        .Footer(false)
        .Sortable(sorting => sorting.OrderBy(sortOrder => sortOrder.Add(s => s.VisitDate).Descending()))
        .Groupable(settings => settings.Groups(groups => { groups.Add(v => v.VisitDate); }))
        .DataBinding(dataBinding => dataBinding.Ajax()
        .Select("ScheduleList", "Agency"))
        .Pageable(paging => paging.PageSize(10))
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $(".t-group-indicator").hide();
    $(".t-grid-content").css({ 'height': 'auto' });
</script>