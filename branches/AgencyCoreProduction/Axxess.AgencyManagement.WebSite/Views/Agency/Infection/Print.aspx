﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<Infection>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
 <% var location = Model.LocationProfile; %>
<head>
    <title><%=location.Name.IsNotNullOrEmpty() ? location.Name + " | " : string.Empty%>Infection Log<%= " | " + Model.Data.PatientName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
    <div class="page largerfont">
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= location.Name.IsNotNullOrEmpty() ? location.Name + "<br />" : ""%>
                        <%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : ""%> <%= location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() : ""%><br />
                        <%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : ""%>
                    </td><th class="h1">Infection Log</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="bicol">
                            <span class="big">Patient Name: <%= Model.PatientProfile != null ? Model.PatientProfile.ToDisplayName() : string.Empty %></span>
                            <span class="big">MR: <%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : string.Empty %></span><br />
                        </span>
                        <span class="quadcol">
                            <span><strong>Physician:</strong></span>
                            <span><%= Model.Data.PhysicianName.ToTitleCase()%></span>
                            <span><strong>Date of Infection:</strong></span>
                            <span><%= Model.Data.InfectionDate.ToString("MM/dd/yyy")%></span>
                            <span><strong>Primary Diagnosis:</strong></span>
                            <span><%= Model.Data.Diagnosis != null && Model.Data.Diagnosis.ContainsKey("PrimaryDiagnosis") ? Model.Data.Diagnosis["PrimaryDiagnosis"] : ""%></span>
                            <span><strong>Secondary Diagnosis:</strong></span>
                            <span><%= Model.Data.Diagnosis != null && Model.Data.Diagnosis.ContainsKey("SecondaryDiagnosis") ? Model.Data.Diagnosis["SecondaryDiagnosis"] : ""%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Type of Infection</h3>
        <div class="tricol"><%string[] typeOfInfection = Model.Data.InfectionType.IsNotNullOrEmpty() ? Model.Data.InfectionType.Split(';') : null; %>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Gastrointestinal") ? "X" : string.Empty %></span><strong></strong>Gastrointestinal</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Respiratory") ? "X" : string.Empty %></span><strong></strong>Respiratory</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Skin") ? "X" : string.Empty %></span><strong></strong>Skin</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Wound") ? "X" : string.Empty %></span><strong></strong>Wound</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Urinary") ? "X" : string.Empty %></span><strong></strong>Urinary</span>
            <span class="checklabel"><span class="checkbox"><%= typeOfInfection != null && typeOfInfection.Contains("Other") ? "X" : string.Empty %></span><strong></strong>Other (Specify)<%= Model.Data.InfectionTypeOther%></span>
        </div><div class="quadcol">
            <span><strong>Treatment Prescribed:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.TreatmentPrescribed.IsNotNullOrEmpty() && Model.Data.TreatmentPrescribed == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.TreatmentPrescribed.IsNotNullOrEmpty() && Model.Data.TreatmentPrescribed == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.TreatmentPrescribed.IsNotNullOrEmpty() && Model.Data.TreatmentPrescribed == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div>
            <span><strong>Treatment/Antibiotic:</strong></span>
            <span><%= Model.Data.Treatment.IsNotNullOrEmpty() ? Model.Data.Treatment : "<br /><br /><br /><br />"%></span>
        </div>
        <h3>Notifications</h3>
        <div class="quadcol">
            <span><strong>M.D. Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>New Orders:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div>
            <span><strong>Narrative:</strong></span>
            <span><%= Model.Data.Orders.IsNotNullOrEmpty() ? Model.Data.Orders : "<br /><br /><br /><br />"%></span>
        </div><div>
            <span><strong>Follow Up:</strong></span>
            <span><%= Model.Data.FollowUp.IsNotNullOrEmpty() ? Model.Data.FollowUp : "<br /><br /><br /><br />"%></span>
        </div><div class="quadcol">
            <span><strong>Signature:</strong></span>
            <span><%= Model.Data.SignatureText %></span>
            <span><strong>Date:</strong></span>
            <span><%= Model.Data.SignatureDate.ToString("MM/dd/yyyy") %></span>
        </div>
    </div>
</body>
</html>
