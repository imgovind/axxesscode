﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LocationPlanViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Upgrade", "Agency", FormMethod.Post, new { @id = "EditAgencyPackage_Form" })) { %>
<%= Html.Hidden("CurrentPackageId", Model.CurrentPlan, new { @id = "EditAgencyPackage_CurrentPackage" })%>
<%= Html.Hidden("AgencyLocationId", Model.Id, new { @id = "EditAgencyPackage_AgencyLocationId" })%>
    <fieldset>
        <legend class="bigtext">Monthly Subscription Plans</legend>
        <div class="wide-column">
            <div class="row bigtext"><%= string.Format("Your Current Subscription Plan - {0}", Model.CurrentPlanDescription)%></div>
            <div class="row"><%= string.Format("You have created {0} {1} for this location/branch.", Model.Count, Model.IsUserPlan ? "users" : "patients")%></div>
            <div class="row">Your organization can request a change to your subscription plan by selecting a new plan below and clicking the 'Submit Request' button.</div>
        </div>
        <div class="wide-column">
            <div class="row">
            <% if (Model.IsUserPlan) { %>
                <div class="checkgroup">
                    <% if (Model.CurrentPlan < 5) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option1' type='radio' value='5' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("5").ToChecked())%>
                        <label for="EditAgencyPackage_Option1">1 to 5 users ($499 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 10) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option2' type='radio' value='10' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("10").ToChecked())%>
                        <label for="EditAgencyPackage_Option2">6 to 10 users ($699 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 20) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option3' type='radio' value='20' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("20").ToChecked())%>
                        <label for="EditAgencyPackage_Option3">11 to 20 users ($899 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 30) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option4' type='radio' value='30' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("30").ToChecked())%>
                        <label for="EditAgencyPackage_Option4">21 to 30 users ($1099 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 40) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option5' type='radio' value='40' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("40").ToChecked())%>
                        <label for="EditAgencyPackage_Option5">31 to 40 users ($1299 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option6' type='radio' value='100' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100").ToChecked())%>
                        <label for="EditAgencyPackage_Option6">41 to 100 users ($1499 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 200) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option7' type='radio' value='200' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100").ToChecked())%>
                        <label for="EditAgencyPackage_Option7">101 to 200 users ($1699 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100000) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option8' type='radio' value='100000' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100000").ToChecked())%>
                        <label for="EditAgencyPackage_Option8">Over 200 users (Call for pricing)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100001) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option9' type='radio' value='100001' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100001").ToChecked())%>
                        <label for="EditAgencyPackage_Option9">Premier Plan (call for pricing)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100002) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option10' type='radio' value='100002' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100002").ToChecked())%>
                        <label for="EditAgencyPackage_Option10">Enterprise Plan (call for pricing)</label>
                    </div>
                    <% } %>
                </div>
            <% } else { %>
            <div class="checkgroup">
                    <% if (Model.CurrentPlan < 25) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option1' type='radio' value='25' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("25").ToChecked())%>
                        <label for="EditAgencyPackage_Option1">1 to 25 patients ($499 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 50) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option2' type='radio' value='50' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("50").ToChecked())%>
                        <label for="EditAgencyPackage_Option2">26 to 50 patients ($699 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 75) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option3' type='radio' value='75' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("75").ToChecked())%>
                        <label for="EditAgencyPackage_Option3">51 to 75 patients ($899 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option4' type='radio' value='100' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100").ToChecked())%>
                        <label for="EditAgencyPackage_Option4">76 to 100 patients ($1099 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 150) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option5' type='radio' value='150' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("150").ToChecked())%>
                        <label for="EditAgencyPackage_Option5">101 to 150 patients ($1299 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 200) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option6' type='radio' value='200' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("200").ToChecked())%>
                        <label for="EditAgencyPackage_Option6">151 to 200 patients ($1499 per month)</label>
                    </div>
                    <% } %>
                     <% if (Model.CurrentPlan < 300) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option7' type='radio' value='200' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("200").ToChecked())%>
                        <label for="EditAgencyPackage_Option7">201 to 300 patients ($1699 per month)</label>
                    </div>
                    <% } %>
                    <% if (Model.CurrentPlan < 100000) { %>
                    <div class="option">
                        <%= string.Format("<input id='EditAgencyPackage_Option8' type='radio' value='100000' name='RequestedPackageId' class='RequestedPackageId' {0} />", Model.NextPlan.ToString().IsEqual("100000").ToChecked())%>
                        <label for="EditAgencyPackage_Option8">Over 300 patients (call for pricing)</label>
                    </div>
                    <% } %>
                </div>
            <% } %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend class="bigtext">Annual Subscription Plans</legend>
        <div class="wide-column">
            <div class="row">We are offering a promotion for a limited time that allows your organization to pay your software subscriptions in advance at a discounted price.</div>
        </div>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                       <input id='EditAgencyPackage_AnnualOption1' type='radio' value='1' name='AnnualPlanId' class=''/>
                        <label for="EditAgencyPackage_AnnualOption1">5% Discount for 6 month payment</label>
                    </div>
                    <div class="option">
                        <input id='EditAgencyPackage_AnnualOption2' type='radio' value='2' name='AnnualPlanId' class=''/>
                        <label for="EditAgencyPackage_AnnualOption2">10% Discount for full year payment</</label>
                    </div>
                    <div class="option">
                        <input id='EditAgencyPackage_AnnualOption3' type='radio' value='3' name='AnnualPlanId' class=''/>
                        <label for="EditAgencyPackage_AnnualOption3">15% Discount for 2 years payment</</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="EditAgencyPackage_Comments" name="Comments" maxcharacters="500" class="taller"></textarea>
            </div>
        </div>
    </fieldset>
    <fieldset class="editmed">
        <legend>Authorization</legend>
        <div>I, <strong><%= Current.UserFullName %></strong>, an authorized official of <strong><%= Current.AgencyName %></strong>, hereby authorize Axxess Technology solutions, Inc. to change my subscription plan as selected above.</div>
        <div class="wide-column">
            <div class="row">
                <label for="EditAgencyPackage_Signature" class="float-left">Signature:</label>
                <div class="float-left"><%= Html.Password("signature", "", new { @id = "EditAgencyPackage_Signature", @class = "required text input_wrapper" })%></div>
            </div>
        </div>
        <br />
        <div class="wide-column">
            <div class="row">
                <span class="tinytext">All terms of the existing agreement remain in place and changes can only be made in writing and executed between Axxess and your organization. In the event that you choose to cancel your software subscription before the expiration of the term of your agreement, payments made in advance for the use of the software are non-refundable. In the event of a change to your subscription plan, necessary accounting adjustments will be made and your subscription payment will be adjusted accordingly.</span>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="EditAgencyPackage_Submit">Submit Request</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>