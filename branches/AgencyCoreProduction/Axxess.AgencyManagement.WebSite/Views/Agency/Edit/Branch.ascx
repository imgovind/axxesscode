﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
 <% using (Html.BeginForm("UpdateAgency", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
<%if(Model.MainLocation.IsMainOffice || Model.MainLocation.IsLocationStandAlone){ %>
<fieldset>
        <legend>Company Information</legend>
        <div class="column strong">
            <div class="row"><label for="Edit_Agency_CompanyName">Company Name:</label><div class="float-right">
            <%if(Model.MainLocation.IsLocationStandAlone){ %>
            <label><%=Model.Name %></label>
            <%}else{ %>
            <%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Agency_CompanyName", @maxlength = "75", @class = "text required input_wrapper" })%>
            <%} %>
            </div>
            </div>
            <div class="row"><label for="Edit_Agency_TaxId">Tax Id:</label><div class="float-right"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_TaxIdType">Tax Id Type:</label><div class="float-right"><%var taxIdTypes = new SelectList(new[] { new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" }, new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" }, new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" } }, "Value", "Text", Model.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "Edit_Agency_TaxIdType", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float-right"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = "Edit_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPhoneArray1">Contact Person Phone:</label><div class="float-right"><%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : "", new { @id = "Edit_Agency_ContactPhoneArray1", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : "", new { @id = "Edit_Agency_ContactPhoneArray2", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : "", new { @id = "Edit_Agency_ContactPhoneArray3", @class = "input_wrappermultible autotext required numeric phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Agency_CahpsVendor">CAHPS Vendor:</label><div class="float-right"><%= Html.CahpsVendors("CahpsVendor", Model.CahpsVendor.ToString(), new { @id = "Edit_Agency_CahpsVendor", @class = "valid" })%></div></div>
        </div>
        <div class="column strong">
            <div class="row"><label for="Edit_Agency_NationalProviderNo">National Provider Number:</label><div class="float-right"><%=Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float-right"><%=Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
    <%} %>
  <fieldset>
        <legend>Location</legend>
<%= Html.Hidden("Location.Id", Model.MainLocation.Id, new { @id = "Edit_AgencyInfo_Id" })%>
<div class="column">
    <div class="row"><label for="Edit_AgencyInfo_Name" class="float-left">Name:</label><div class="float-right"><%=Html.TextBox("Location.Name", Model.MainLocation.Name, new { @id = "Edit_AgencyInfo_Name", @class = "text input_wrapper required", @maxlength = "50" })%></div></div>
    <div class="row"><label for="Edit_AgencyInfo_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("Location.AddressLine1", Model.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.MainLocation.AddressLine1 : string.Empty, new { @id = "Edit_AgencyInfo_AddressLine1", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="Edit_AgencyInfo_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("Location.AddressLine2", Model.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.MainLocation.AddressLine2 : string.Empty, new { @id = "Edit_AgencyInfo_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="Edit_AgencyInfo_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("Location.AddressCity", Model.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.MainLocation.AddressCity : string.Empty, new { @id = "Edit_AgencyInfo_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="Edit_AgencyInfo_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.States("Location.AddressStateCode", Model.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.MainLocation.AddressStateCode : string.Empty, new { @id = "Edit_AgencyInfo_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Location.AddressZipCode", Model.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.MainLocation.AddressZipCode : string.Empty, new { @id = "Edit_AgencyInfo_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "9" })%></div></div>
</div>
<div class="column">
    <div class="row"><label for="Edit_Location_PhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><%=Html.TextBox("Location.PhoneArray", Model.MainLocation.PhoneWork.IsNotNullOrEmpty() && Model.MainLocation.PhoneWork.Length >= 3 ? Model.MainLocation.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Location_PhoneArray1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("Location.PhoneArray", Model.MainLocation.PhoneWork.IsNotNullOrEmpty() && Model.MainLocation.PhoneWork.Length >= 6 ? Model.MainLocation.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("Location.PhoneArray", Model.MainLocation.PhoneWork.IsNotNullOrEmpty() && Model.MainLocation.PhoneWork.Length >= 10 ? Model.MainLocation.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
    <div class="row"><label for="Edit_Location_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><%=Html.TextBox("Location.FaxNumberArray", Model.MainLocation.FaxNumber.IsNotNullOrEmpty() && Model.MainLocation.FaxNumber.Length >= 3 ? Model.MainLocation.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Location_FaxNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("Location.FaxNumberArray", Model.MainLocation.FaxNumber.IsNotNullOrEmpty() && Model.MainLocation.FaxNumber.Length >= 6 ? Model.MainLocation.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("Location.FaxNumberArray", Model.MainLocation.FaxNumber.IsNotNullOrEmpty() && Model.MainLocation.FaxNumber.Length >= 10 ? Model.MainLocation.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
</div>
</fieldset>
  <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%} %>
