﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<Incident>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Incident/Accident Log<%= Model.PatientProfile %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
    <div class="page largerfont">
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : "" %>
                        <%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : ""%> <%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.LocationProfile.AddressLine2.Clean() : ""%><br />
                        <%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode : ""%>
                    </td><th class="h1">Incident/Accident Log</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="bicol">
                            <span class="big">Patient Name: <%= Model.PatientProfile != null ? Model.PatientProfile.ToDisplayName() : string.Empty%></span>
                            <span class="big">MR: <%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : string.Empty%></span><br />
                        </span>
                        <span class="quadcol">
                            <span><strong>Physician:</strong></span>
                            <span><%= Model.Data.PhysicianName.ToTitleCase()  %></span>
                            <span><strong>Type of Incident:</strong></span>
                            <span><%= Model.Data.IncidentType %></span>
                            <span><strong>Date of Incident:</strong></span>
                            <span><%= Model.Data.IncidentDate.ToString("MM/dd/yyyy")%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Individual(s) Involved</h3>
        <div class="quadcol"><%string[] individualInvolved = Model.Data.IndividualInvolved.IsNotNullOrEmpty() ? Model.Data.IndividualInvolved.Split(';') : null; %>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Patient") ? "X" : string.Empty %></span><strong></strong>Patient</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Caregiver") ? "X" : string.Empty %></span><strong></strong>Caregiver</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Employee/Contractor") ? "X" : string.Empty %></span><strong></strong>Employee/Contractor</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Other") ? "X" : string.Empty %></span><strong></strong>Other (Specify)<%= Model.Data.IndividualInvolvedOther%></span>
        </div><div >
            <span><strong>Describe Incident/Accident:</strong></span>
            <span><%= Model.Data.Description.IsNotNullOrEmpty() ? Model.Data.Description : "<br /><br /><br /><br />"%></span>
        </div><div>
            <span><strong>Action Taken/Interventions Performed:</strong></span>
            <span><%= Model.Data.ActionTaken.IsNotNullOrEmpty() ? Model.Data.ActionTaken : "<br /><br /><br /><br />"%></span>
        </div>
        <div>
            <span><strong>Orders:</strong></span>
            <span><%= Model.Data.Orders.IsNotNullOrEmpty() ? Model.Data.Orders : "<br /><br /><br /><br />"%></span>
        </div>
        <div>
            <span><strong>Follow Up:</strong></span>
            <span><%= Model.Data.FollowUp.IsNotNullOrEmpty() ? Model.Data.FollowUp : "<br /><br /><br /><br />"%></span>
        </div>
        <h3>Notifications</h3>
        <div class="quadcol">
            <span><strong>M.D. Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.MDNotified.IsNotNullOrEmpty() && Model.Data.MDNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>Family/CG Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.FamilyNotified.IsNotNullOrEmpty() && Model.Data.FamilyNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.FamilyNotified.IsNotNullOrEmpty() && Model.Data.FamilyNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.FamilyNotified.IsNotNullOrEmpty() && Model.Data.FamilyNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>New Orders:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.Data.NewOrdersCreated.IsNotNullOrEmpty() && Model.Data.NewOrdersCreated == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div class="quadcol">
            <span><strong>Signature:</strong></span>
            <span><%= Model.Data.SignatureText %></span>
            <span><strong>Date:</strong></span>
            <span><%= Model.Data.SignatureText.IsNotNullOrEmpty()?Model.Data.SignatureDate.ToString("MM/dd/yyyy"):string.Empty %></span>
        </div>
    </div>
</body>
</html>
