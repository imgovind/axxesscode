﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Upload Type | <%=Current.AgencyName %></span>
<% using (Html.BeginForm("UploadTypes", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<UploadType>()
        .Name("List_UploadType")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.Type).Sortable(true).Title("Type");
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUploadType('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"UploadType.Delete('<#=Id#>');\" class=\"deleteUploadType\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "UploadType"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>

<script type="text/javascript">
$("#List_UploadType .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Upload Type", Click: UserInterface.ShowNewUploadType } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $("#List_UploadType .t-grid-content").css({ height: "auto", top: 59});
</script>
<% } %>
