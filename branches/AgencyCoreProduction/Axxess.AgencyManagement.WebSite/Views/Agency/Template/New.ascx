﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Template | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Add", "Template", FormMethod.Post, new { @id = "newTemplateForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div>
                <label for="New_Template_Title" class="strong">Name</label><br />
                <%= Html.TextBox("Title", "", new { @id = "New_Template_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%>
                <span class='required-red'>*</span>
            </div>
            <br />
            <div>
                <label for="New_Template_Text" class="strong">Text</label><br />
                <textarea id="New_Template_Text" name="Text" cols="5" rows="6" style="height:200px" class="fill" maxcharacters="5000"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newtemplate');">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>
