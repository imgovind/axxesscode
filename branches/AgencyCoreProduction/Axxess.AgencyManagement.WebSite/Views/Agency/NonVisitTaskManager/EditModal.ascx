﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserNonVisitTask>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "NonVisitTaskManager", FormMethod.Post, new { @id = "editNonVisitTaskManagerForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_UserNonVisitTask_Id" }) %>
    <fieldset>
        <legend>Edit User Task</legend>
        <div class="wide_column">
        <table class="form">
            <tr><td class="width100"><label for="UserId" class="float-left"><strong>User:</strong></label></td><td><%= Html.UserWithBranchAndStatus("UserId", Model.UserId.ToString(),"-- Select User--", Guid.Empty, 1, new { @id = "UserId", @class = "valid" })%></td></tr>
            <tr><td class="width100"><label for="TaskId" class="float-left"><strong>Task:</strong></label></td><td><%= Html.AllNonVisitTaskList("TaskId", Model.TaskId.ToString(), Current.AgencyId, "--Select Task--", new { @id = "TaskId", @class = "valid" })%></td></tr>
            <tr><td class="width100"><label for="TaskDate" class="float-left"><strong>Task Date:</strong></label></td><td><input type="text" class="date-picker shortdate" name="TaskDate" value="<%= Model.TaskDate.ToShortDateString() %>" id="TaskDate" /></td></tr>
            <tr><td class="width100"><label for="TimeIn" class="float-left"><strong>Time In:</strong></label></td><td><input type="text" size="10" id="TimeIn" name="TimeIn" class="time-picker tasktimein" value="<%= Model.TimeIn.ToShortTimeString() %>" /></td></tr>
            <tr><td class="width100"><label for="TimeOut" class="float-left"><strong>Time Out:</strong></label></td><td><input type="text" size="10" id="TimeOut" name="TimeOut" class="time-picker tasktimeout" value="<%= Model.TimeOut.ToShortTimeString() %>" /></td></tr>
            <tr><td class="width100"><label for="PaidDate" class="float-left"><strong>Paid Date:</strong></label></td><td><input type="text" class="date-picker shortdate" name="PaidDate" value="<%= Model.PaidDate == DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PaidDate.ToShortDateString() %>" id="PaidDate" /></td></tr>
            <tr><td class="width100"><label for="PaidStatus" class="float-left"><strong>Paid Status:</strong></label></td><td><%= string.Format("<input id='PaidStatus' class='radio' value='true' name='PaidStatus' type='checkbox' {0} />", Model.PaidStatus ? "checked" : string.Empty)%></td></tr>
            <tr><td class="width100"><label for="Comments" class="float-left"><strong>Comments:</strong></label></td><td><textarea class="usertask-textarea-edit" id="Comments" name="Comments" cols="5" rows="2" maxcharacters="2000"><%= Model.Comments %></textarea></td></tr>
        </table>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>