﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserNonVisitTask>" %>
<span class="wintitle">New User Task |
    <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Add", "NonVisitTaskManager", FormMethod.Post, new { @id = "newNonVisitTaskManagerForm" }))
    { %>
<div class="wrapper main">
    
   <fieldset class="nonvisittask">
        <legend>New User Task</legend>
         <input type="hidden" name="NonVisitTasks.Index" value="0" class="indexvalue" />
         <%--<div class="usertask-delete hidden"><a onclick="NonVisitTaskManager.RemoveUserTask(this)" href="javascript:void(0);"><span class="img icon error-small" style="background-position: -118px -108px; height: 22px; width: 22px;"></span></a></div>--%>
         <div class="usertask-delete" style="display:none;"><a onclick="NonVisitTaskManager.RemoveUserTask(this)" href="javascript:void(0);"><span class="img icon delete-small"></span></a></div>
        <div class="column">
            <div class="row">
                <label for="NonVisitTask_UserId_1" class="float-left">
                    User:</label>
                <div class="float-right">
                    <%= Html.UserWithBranchAndStatus("NonVisitTasks[0].UserId", Guid.Empty.ToString(), "-- Select User--", Guid.Empty, 1, new { @id = "NonVisitTask_UserId_1", @class = "valid requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="NonVisitTask_TaskId_1" class="float-left">
                    Task:</label>
                <div class="float-right">
                    <%= Html.AllNonVisitTaskList("NonVisitTasks[0].TaskId", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "NonVisitTask_TaskId_1", @class = "valid requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="NonVisitTask_TaskDate_1" class="float-left">
                    Task Date:</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="NonVisitTasks[0].TaskDate" id="NonVisitTask_TaskDate_1" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NonVisitTask_TimeIn_1" class="float-left">
                    Time In:</label>
                <div class="float-right">
                    <input type="text" size="3" id="NonVisitTask_TimeIn_1" name="NonVisitTasks[0].TimeIn" class="time-picker required" value="" /></div>
            </div>
            <div class="row">
                <label for="NonVisitTask_TimeOut_1" class="float-left">
                    Time Out:</label>
                <div class="float-right">
                    <input type="text" size="3" id="NonVisitTask_TimeOut_1" name="NonVisitTasks[0].TimeOut" class="time-picker required" value="" /></div>
            </div>
            <div class="row">
                <label for="NonVisitTask_Comments_1" class="float-left">
                    Comments:</label>
                <div class="float-right">
                    <textarea class="usertask-textarea-new" id="NonVisitTask_Comments_1" name="NonVisitTasks[0].Comments" cols="5" rows="2" maxcharacters="2000"></textarea>
                </div>
            </div>
        </div>

    </fieldset>
  
    <div class="buttons">
        <ul>
            <li><a class="button" onclick="NonVisitTaskManager.AddUserTask($(this).closest('div.buttons'))" href="javascript:void(0);">Add More Tasks</a></li>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>