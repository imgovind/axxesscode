﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyTeam>>" %>
<span class="wintitle">List Care Teams | <%=Current.AgencyName %></span>
<div class="wrapper main">
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-team">New Care Team</a></li>
        </ul>
        <ul class="fr">
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="acore-grid">
        <ul>
            <li><h3 class="ac strong">Care Teams</h3></li>
            <li class="ac"><input type="text" class="inline grid-search" /></li>
        </ul>
<%  if (Model != null && Model.Count() > 0) { %>
        <ol>
    <%  foreach(var team in Model) { %>
            <li>
                <span class="teams-tooltip"><%= team.UserToolTip %></span>
                <span class="grid-threequarters"><%= team.Name %></span>
                <span>
                    <a class="link teams-manage-patients" guid="<%= team.Id %>">Manage Patients</a>
                    |
                    <a class="link teams-manage-users" guid="<%= team.Id %>">Manage Users</a>
                    |
                    <a class="link teams-delete" guid="<%= team.Id %>">Delete</a>
                </span>
            </li>
    <%  } %>
        </ol>
<%  } else { %>
        <h1 class="blue">No Care Teams Found</h1>
<%  } %>
    </div>
</div>