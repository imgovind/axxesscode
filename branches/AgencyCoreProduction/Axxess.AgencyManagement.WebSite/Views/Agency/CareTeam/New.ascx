﻿<%@  Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<span class="wintitle">New Care Team | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "CareTeam", FormMethod.Post, new { @id = "NewCareTeam_Form" })) {%>
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="NewCareTeam_Name" class="fl strong">Care Team Name</label>
                <div class="fr"><%= Html.TextBox("Name", string.Empty, new { @id = "NewCareTeam_Name", @class = "required", @maxlength = "50" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="acore-grid limited">
        <ul>
            <li><h3 class="strong ac">Members of the Group</h3></li>
        </ul>
        <ol>
    <%  foreach(var user in Model) { %>
            <li>
                <input name="UserArray" type="checkbox" value="<%= user.Id %>" />
                <label><%= user.DisplayName %></label>
            </li>
    <%  } %>
        </ol>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>