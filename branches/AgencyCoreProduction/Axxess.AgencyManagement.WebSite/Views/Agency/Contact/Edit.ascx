﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyContact>" %>
<span class="wintitle">Edit Contact | <%= Model.DisplayName %></span>
<%  using (Html.BeginForm("Update", "Contact", FormMethod.Post, new { @id = "editContactForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Contact_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <legend>Contact Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Contact_CompanyName">Company Name:</label><div class="float-right"><%= Html.TextBox("CompanyName", Model.CompanyName, new { @id = "Edit_Contact_CompanyName", @maxlength = "100", @class = "text" })%></div></div>
            <div class="row"><label for="Edit_Contact_FirstName">Contact First Name:</label><div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Contact_FirstName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_Contact_LastName">Contact Last Name:</label><div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Contact_LastName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_Contact_Email">Contact Email:</label><div class="float-right"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Contact_EmailAddress", @class = "text email input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Contact_Type">Contact Type:</label><div class="float-right"><%= Html.ContactTypes("ContactType", Model.ContactType, new { @id = "Edit_Contact_Type", @class = "ContactType required valid" })%></div></div>
            <div class="row"><label for="Edit_Contact_OtherContactType">Other Contact Type (specify):</label><div class="float-right"><%= Html.TextBox("ContactTypeOther", Model.ContactTypeOther, new { @id = "Edit_Contact_OtherContactType", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Contact_AddressLine1">Address:</label><div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Contact_AddressLine1", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Contact_AddressLine2">&#160;</label><div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Contact_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Contact_AddressCity">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Contact_AddressCity", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Contact_AddressStateCode">State, Zip:</label><div class="float-right"><%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Contact_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Contact_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "9" })%></div></div>
            <div class="row"><label for="Edit_Contact_PhonePrimary1">Office Phone:</label><div class="float-right"><%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(0, 3) : "", new { @id = "Edit_Contact_PhonePrimary1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(3, 3) : "", new { @id = "Edit_Contact_PhonePrimary2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(6, 4) : "", new { @id = "Edit_Contact_PhonePrimary3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Contact_PhoneExtension">Office Phone Extension:</label><div class="float-right"><%= Html.TextBox("PhoneExtension", Model.PhoneExtension, new { @id = "Edit_Contact_PhoneExtension", @maxlength = "4", @class = "numeric phone_long" })%></div></div>
            <div class="row"><label for="Edit_Contact_PhoneAlternate1">Mobile Phone:</label><div class="float-right"><%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(0, 3) : "", new { @id = "Edit_Contact_PhoneAlternate1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(3, 3) : "", new { @id = "Edit_Contact_PhoneAlternate2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(6, 4) : "", new { @id = "Edit_Contact_PhoneAlternate3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Contact_Fax1">Fax Number:</label><div class="float-right"><%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Contact_Fax1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Contact_Fax2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Contact_Fax3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : "", new { @id = "Edit_Contact_Comments", @class = "fill", @maxcharacters = "1000" })%>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadContactLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editcontact');">Exit</a></li>
    </ul></div>
</div>
<% } %>
