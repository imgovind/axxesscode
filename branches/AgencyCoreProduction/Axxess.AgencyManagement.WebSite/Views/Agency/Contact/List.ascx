﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Contact | <%= Current.AgencyName%></span>
<%  using (Html.BeginForm("Contacts", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyContact>().Name("List_Contact").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(c => c.DisplayName).Title("Name").Width(150).Sortable(true);
            columns.Bound(c => c.CompanyName).Title("Company").Sortable(true);
            columns.Bound(c => c.ContactType).Title("Type").Width(150).Sortable(true);
            columns.Bound(c => c.PhonePrimaryExtended).Title("Phone").Width(160).Sortable(false);
            columns.Bound(c => c.FaxNumberFormatted).Title("Fax").Width(120).Sortable(false);
            columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
            columns.Bound(c => c.Comments).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>").Title("").Sortable(false).Width(35);
            columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditContact('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Contact.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
}).ClientEvents(c => c.OnRowDataBound("Contact.ListRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Contact")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<% } %>
<script type="text/javascript">
$("#List_Contact .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageContact) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Contact", Click: UserInterface.ShowNewContact } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>