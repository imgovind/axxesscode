﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Manage Company Information | <%= Current.AgencyName %></span>
<div id="EditAgency_Tabs" class="tabs vertical-tabs vertical-tabs-left">
    <ul class="vertical-tab-list strong">
        <li><a href="#EditAgency_Information">Company Information</a></li>
        <li><a href="#EditAgency_UpgradeInfo">Subscription Plan</a></li>
    </ul>
    <div id="EditAgency_Information" class="general"><% Html.RenderPartial("Edit/Information"); %></div>
    <div id="EditAgency_UpgradeInfo" class="general"></div>
</div>
