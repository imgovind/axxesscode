﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Past Due Recerts | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg" >
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
            <div class="float-right buttons">
                    <li><%= Html.ActionLink("Export to Excel", "RecertsPastDue", "Export", new { BranchId = Guid.Empty, InsuranceId = Model, StartDate = DateTime.Now.AddDays(-59) }, new { id = "AgencyPastDueRecet_ExportLink" })%></li>
            </div>
<%  } %>
<fieldset class="orders-filter">
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Agency.RebindAgencyPastDueRecet();">Generate</a></li>
            </ul>
        </div>
        <label class="strong">Branch:
            <%= Html.UserBranchList( "BranchId", Guid.Empty.ToString(),false,"", new { @id = "AgencyPastDueRecet_BranchCode" })%>
        </label>
        <label class="strong">Insurance:
            <%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyPastDueRecet_InsuranceId", @class = "Insurances" })%>
        </label>
        <div></div>
        <label class="strong">Due Date From :
            <input type="text" name="StartDate" class="date-picker shortdate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyPastDueRecet_StartDate" />
        </label>
        <label id="AgencyPastDueRecerts_Search"></label>
</fieldset>
        
          <%= Html.Telerik().Grid<RecertEvent>().Name("List_PastDueRecerts").HtmlAttributes(new { @style = "top:80px;" }).Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.Status).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
            columns.Bound(r => r.DateDifference).Title("Past Dates").Width(60);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsPastDue", "Agency", new { BranchId = Guid.Empty, InsuranceId=Model , StartDate = DateTime.Now.AddDays(-60) })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>   
  

<script type="text/javascript">
    if (Acore.Mobile) $('#List_PastDueRecerts').css('top', 165);
    $("#AgencyPastDueRecerts_Search").GridSearchById("#List_PastDueRecerts");
    $("#window_listpastduerecerts_content").css({
        "background-color": "#d6e5f3"
    });
    $("#window_listpastduerecerts #List_PastDueRecerts .t-grid-content").css({ "height": "auto", "top": "26px" });
    $('.grid-search').css({ 'position': 'relative', 'left': '0', 'margin-left': '0' });
</script>