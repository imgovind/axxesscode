﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var groupName = ViewData["GroupName"].ToString(); %>
<% var startDate = ViewData["StartDate"]; %>
<% var endDate = ViewData["EndDate"]; %>
<% var branchId = ViewData["BranchId"]; %>
<%= Html.Telerik().Grid<PatientEpisodeEvent>().Name("printQueueGrid").Columns(columns =>
{
    columns.Bound(s => s.CustomValue).ClientTemplate("<input name='CustomValue' type='checkbox' value='<#= CustomValue #>'/>").Title("").Width(30).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
    columns.Bound(s => s.PatientName);
    columns.Bound(s => s.EventDate).Width(100);
    columns.Bound(s => s.TaskName).Title("Task").Width(250);
    columns.Bound(s => s.Status).Width(200);
    columns.Bound(s => s.UserName).Width(150);
    columns.Bound(s => s.PrintUrl).Width(30).Title("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false);
})
        .Groupable(settings => settings.Groups(groups =>
        {
            var data = ViewData["GroupName"].ToString();
            
            if (data == "PatientName")
            {
                groups.Add(s => s.PatientName);
            }
            else if (data == "EventDate")
            {
                groups.Add(s => s.EventDate);
            }
            else if (data == "DisciplineTaskName")
            {
                groups.Add(s => s.TaskName);
            }
            else if (data == "UserName")
            {
                groups.Add(s => s.UserName);
            }
            else
            {
                groups.Add(s => s.EventDate);
            }
        })).DataBinding(dataBinding => dataBinding.Ajax().Select("PrintQueueGrid", "Agency", new { BranchId = branchId, StartDate = startDate, EndDate = endDate }))
            .ClientEvents(evnts => evnts.OnDataBound("Agency.OnPrintQueueDataBound").OnDataBinding("Agency.OnPrintQueueDataBinding"))
        .Scrollable().Sortable().Footer(false)%>
