﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Print Queue | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "printQueueForm" })) { %>

<div class="buttons wrapper">
    <div class="float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="Agency.PrintQueueExport();">Export to Excel</a></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
        <ul class="float-right">
            <li><a href="javascript:void(0);" onclick="Agency.RebindPrintQueue();">Generate</a></li>
        </ul>
        <label class="strong">
            Branch:
            <%=  Html.UserBranchList("BranchId", Guid.Empty.ToString(),false,"", new { @id = "Print_BranchCode" })%>
        </label>
        <label class="strong">
            Date:
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-7).ToShortDateString() %>" id="Print_StartDate" /><label class="strong"> To:</label><input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="Print_EndDate" />
        </label>
        
        <ul>
            <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('PatientName');">Group By Patient</a></li>
            <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('EventDate');">Group By Date</a></li>
            <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('DisciplineTaskName');">Group By Task</a></li>
            <li><a href="javascript:void(0);" onclick="Agency.LoadPrintQueue('UserName');">Group By Clinician</a></li>
        </ul>
    </fieldset>
</div>
<div id="printQueueContentId" style='<%= Current.IsAgencyFrozen ? "bottom:0px;" : "" %>'><% Html.RenderPartial("~/Views/Agency/PrintQueue/Content.ascx", Model); %></div>
    <% if(!Current.IsAgencyFrozen) { %>
    <div class="buttons abs_bottom">
        <%= Html.Hidden("CommandType", "Print", new {@id="PrintQueueUpdate_Type" })%>
        <ul>
            <li><a href="javascript:void(0);" onclick="MarkAsPrinted('Print');">Mark As Printed</a></li>
        </ul>
    </div>
    <% } %>
<% } %>
<script type="text/javascript">
    $("#printQueueContentId .t-group-indicator").hide();
    $("#printQueueContentId .t-grouping-header").remove();
    if (Acore.Mobile) $("#printQueueContentId").css('top', 120);
    else $("#printQueueContentId").css({'top':'80px'});
    $(".t-grid-content", "#window_printqueue").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    $("#window_printqueue_content").css({
        "background-color": "#d6e5f3"
    });
    function MarkAsPrinted(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            Schedule.BulkUpdate('#printQueueForm');
        } else { U.Growl("Select at least one item to mark as printed.", "error"); }
    }
</script>