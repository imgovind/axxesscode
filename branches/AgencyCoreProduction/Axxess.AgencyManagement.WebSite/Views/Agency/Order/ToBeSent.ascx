﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders To Be Sent | <%= Current.AgencyName %></span>
<div id="List_OrdersToBeSent_Container" class="wrapper">
    <div class="float-right buttons">
        <% if(!Current.IsAgencyFrozen) { %>
        <ul>
            <li><a id="List_OrdersToBeSent_SendButton" href="javascript:void(0);" onclick="Agency.MarkOrdersAsSent('#List_OrdersToBeSent_Container');">Send Electronically</a></li>
        </ul>
        <% } %>
        <br />
        <ul>
            <li><%= Html.ActionLink("Export to Excel", "OrdersToBeSent", "Export", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersToBeSent_ExportLink", @class = "excel" })%></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Agency.RebindOrdersToBeSent();">Generate</a></li>
            </ul>
        </div>
        <label class="strong">Branch:
            <div style="display:inline-block"><%=  Html.UserBranchList("BranchId", Guid.Empty.ToString(), false, "", new { @id = "OrdersToBeSent_BranchId" })%></div>
        </label>
        <label class="strong" for="List_OrdersToBeSent_SendType">Filter by:
            <select id="List_OrdersToBeSent_SendType" class="SendAutomatically" name="SendAutomatically">
                <option value="true">Electronic Orders</option>
                <option value="false">Manual Orders (Fax, Mail, etc)</option>
            </select>
        </label>
        <div>
            <label class="strong" for="OrdersToBeSent_StartDate">
                Date Range:
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersToBeSent_StartDate" />
            </label>
            <label class="strong" for="OrdersToBeSent_EndDate">
                To
                <input type="text" class="date-picker shortdate" name="EndtDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersToBeSent_EndDate" />
            </label>
        </div>
        <div id="OrdersToBeSent_Search"></div>
    </fieldset>
    
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersToBeSent").Columns(columns => {
            columns.Bound(o => o.Type).Title("<input type='checkbox' id='Order_CheckAll' />").ClientTemplate("<input type='checkbox' class='OrdersToBeSent' name='<#=Type#>' value='<#=Id#>_<#=PatientId#>_<#=EpisodeId#>' />").Width(35).Sortable(false).Visible(!Current.IsAgencyFrozen);
            columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(true);
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.Text).Title("Type").Sortable(true);
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true);
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true);
            columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersToBeSent", "Agency", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59).ToShortDateString(), EndDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#OrdersToBeSent_Search").append(
        $("<div/>").GridSearchById("#List_OrdersToBeSent")
    );
    $('#Order_CheckAll').change(function() {
        if ($(this).attr('checked')) {
            $(":checkbox").attr("checked", true);
        } else {
            $(":checkbox").attr("checked", false);
        }
    });
    $("#window_orderstobesent_content").css({
        "background-color": "#d6e5f3"
    }).find("#List_OrdersToBeSent").css({
        "height": "auto",
        "top": ( Acore.Mobile ? "160px" : "100px" )
    }).find(".t-grid-content").css({
        "height": "auto",
        "top": "26px"
    });
    $("#List_OrdersToBeSent_SendType").change(function() {
        Agency.RebindOrdersToBeSent();
        if ($(this).val() == 'true') $("#List_OrdersToBeSent_SendButton").text("Send Electronically");
        else $("#List_OrdersToBeSent_SendButton").text("Send Manually");
    });
    $('.grid-search').css({ 'position': 'relative', 'left': '0', 'margin-left': '0' });
</script>