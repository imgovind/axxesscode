﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% var print = "<a href='javascript:void(0)' onclick=\"U.GetAttachment('Patient/AuthorizationPdf', { patientId: '" + Model.Id + "', id: '<#=Id#>' })\"><span class='img icon print'></span></a>"; %>
<span class="wintitle">Authorization List | <%= Model.DisplayName %></span>
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>
    <div class="buttons float-left">
        <a href="javascript:void(0);" onclick="Authorization.Add()">Add Authorization</a>
    </div>
<%} %>
<div class="wrapper">
<%= Html.Telerik().Grid<Authorization>().Name("List_Authorizations").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(a => a.Number1).Title("Authorization Number 1").Width(150).Sortable(false);
            columns.Bound(a => a.Number2).Title("Authorization Number 2").Width(150).Sortable(false);
            columns.Bound(a => a.Number3).Title("Authorization Number 3").Width(150).Sortable(false);
            columns.Bound(a => a.Branch).Width(200).Sortable(false);
            columns.Bound(a => a.StartDateFormatted).Title("Start Date").Width(120).Sortable(false);
            columns.Bound(a => a.EndDateFormatted).Title("End Date").Width(120).Sortable(false);
            columns.Bound(a => a.Status).Sortable(false);
            columns.Bound(a => a.Id).ClientTemplate(print).Title(" ").Sortable(false).Width(30);
            columns.Bound(a => a.Url).ClientTemplate("<#=Url#>").Title("Action").Sortable(false).Width(150).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Authorization", new { patientId = Model != null ? Model.Id : Guid.Empty}))
        .Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Authorizations .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { U.GetAttachment("Export/Authorizations", { patientId: "<%= Model.Id %>" }) } } ])
    )<% } %>
    <% if (Current.HasRight(Permissions.ManageInsurance)) { %>.append(
        $("<div/>").addClass("float-left").Buttons([{Text:"Add Authorization", Click:function() {Patient.LoadNewAuthorization("<%= Model.Id %>")}}])
    )<%} %>;
    $(".t-grid-content").css("height", "auto");
</script>