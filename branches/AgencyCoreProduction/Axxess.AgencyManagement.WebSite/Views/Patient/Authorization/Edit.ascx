﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<span class="wintitle">Edit Authorization | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Update", "Authorization", FormMethod.Post, new { @id = "editAuthorizationForm" })) { %>
<div class="wrapper main">
    
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Authorization_Id" })%> 
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Authorization_PatientId" })%>     
    <fieldset>
        <legend>Detail</legend>
        <div><span class="bigtext"><%=Model.DisplayName %></span></div>
        <div class="column">
            <div class="row"><label for="Edit_Authorization_StartDate" class="float-left">Start Date:</label><div class="float-right"><input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" id="Edit_Authorization_StartDate" /></div></div>
            <div class="row"><label for="Edit_Authorization_EndDate" class="float-left">End Date:</label><div class="float-right"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate.ToShortDateString() %>" id="Edit_Authorization_EndDate" /></div></div>
            <div class="row"><label for="Edit_Authorization_LocationId" class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("AgencyLocationId", Model.AgencyLocationId.ToString(),false,"", new { @id = "Edit_Authorization_LocationId", @class = "BranchLocation" })%></div></div>
            <div class="row"><label for="Edit_Authorization_Status" class="float-left">Status:</label><div class="float-right"><%= Html.AuthorizationStatus("Status", Model.Status, new { @id = "Edit_Authorization_Status" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Authorization_Insurance" class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("Insurance", Model.Insurance, true, new { @id = "Edit_Authorization_Insurance", @class = "Insurances requireddropdown" })%></div></div>
            <div class="row"><label for="Edit_Authorization_AuthNumber1" class="float-left">Authorization Number 1:</label><div class="float-right"><%= Html.TextBox("Number1", Model.Number1, new { @id = "Edit_Authorization_AuthNumber1", @class = "text input_wrapper required", @maxlength = "30" })%></div></div>
            <div class="row"><label for="Edit_Authorization_AuthNumber2" class="float-left">Authorization Number 2:</label><div class="float-right"><%= Html.TextBox("Number2", Model.Number2, new { @id = "Edit_Authorization_AuthNumber2", @class = "text input_wrapper ", @maxlength = "30" })%></div></div>
            <div class="row"><label for="Edit_Authorization_AuthNumber3" class="float-left">Authorization Number 3:</label><div class="float-right"><%= Html.TextBox("Number3", Model.Number3, new { @id = "Edit_Authorization_AuthNumber3", @class = "text input_wrapper ", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
       <legend>Visits or Hours Authorized</legend>
       <div class="column">
           <table class="fixed">
            <col width="60" /><col width="35" /><col width="150" />
            <tbody><tr>
                    <td><label for="Edit_Authorization_SNVisit" class="radio">SN Count:</label></td>
                    <td><%= Html.TextBox("SNVisit", Model.SNVisit, new { @id = "Edit_Authorization_SNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                     <% var countVisitTypeSN = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.SNVisitCountType); %>
                    <td><%= Html.DropDownList("SNVisitCountType", countVisitTypeSN, new { @id = "Edit_Authorization_SNVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_PTVisit" class="radio">PT Count:</label></td>
                    <td><%= Html.TextBox("PTVisit", Model.PTVisit, new { @id = "Edit_Authorization_PTVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                     <% var countVisitTypePT = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.PTVisitCountType); %>
                    <td><%= Html.DropDownList("PTVisitCountType", countVisitTypePT, new { @id = "Edit_Authorization_PTVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_OTVisit" class="radio">OT Count:</label></td>
                    <td><%= Html.TextBox("OTVisit", Model.OTVisit, new { @id = "Edit_Authorization_OTVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                    <% var countVisitTypeOT = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.OTVisitCountType); %>
                    <td><%= Html.DropDownList("OTVisitCountType", countVisitTypeOT, new { @id = "Edit_Authorization_OTVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_STVisit" class="radio">ST Count:</label></td>
                    <td><%= Html.TextBox("STVisit", Model.STVisit, new { @id = "Edit_Authorization_STVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                     <% var countVisitTypeST = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.SNVisitCountType); %>
                    <td><%= Html.DropDownList("STVisitCountType", countVisitTypeST, new { @id = "Edit_Authorization_STVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_MSWVisit" class="radio">MSW Count:</label></td>
                    <td><%= Html.TextBox("MSWVisit", Model.MSWVisit, new { @id = "Edit_Authorization_MSWVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                    <% var countVisitTypeMSW = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.MSWVisitCountType); %>
                    <td><%= Html.DropDownList("MSWVisitCountType", countVisitTypeMSW, new { @id = "Edit_Authorization_MSWVisitCountType", @class = "shortselect" })%></td>
                </tr>
            </tbody></table>
        </div><div class="column">
           <table class="fixed">
            <col width="60" /><col width="35" /><col width="150" />
            <tbody><tr>
                    <td><label for="Edit_Authorization_HHAVisit" class="radio">HHA Count:</label></td>
                    <td><%= Html.TextBox("HHAVisit", Model.HHAVisit, new { @id = "Edit_Authorization_HHAVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                     <% var countVisitTypeHHA = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.HHAVisitCountType); %>
                    <td><%= Html.DropDownList("HHAVisitCountType", countVisitTypeHHA, new { @id = "Edit_Authorization_HHAVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_DieticianVisit" class="radio">Dietician Count:</label></td>
                    <td><%= Html.TextBox("DieticianVisit", Model.DieticianVisit, new { @id = "Edit_Authorization_DieticianVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                    <% var countVisitTypeDietician = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.DieticianVisitCountType); %>
                    <td><%= Html.DropDownList("DieticianVisitCountType", countVisitTypeDietician, new { @id = "Edit_Authorization_DieticianVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_RNVisit" class="radio">RN Count:</label></td>
                    <td><%= Html.TextBox("RNVisit", Model.RNVisit, new { @id = "Edit_Authorization_RNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                    <% var countVisitTypeRN = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.RNVisitCountType); %>
                    <td><%= Html.DropDownList("RNVisitCountType", countVisitTypeRN, new { @id = "Edit_Authorization_RNVisitCountType", @class = "shortselect" })%></td>
                </tr><tr><td><label for="Edit_Authorization_LVNVisit" class="radio">LVN Count:</label></td>
                    <td><%= Html.TextBox("LVNVisit", Model.LVNVisit, new { @id = "Edit_Authorization_LVNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                     <% var countVisitTypeLVN = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", Model.LVNVisitCountType); %>
                    <td><%= Html.DropDownList("LVNVisitCountType", countVisitTypeLVN, new { @id = "Edit_Authorization_LVNVisitCountType", @class = "shortselect" })%></td>
                </tr>
            </tbody></table>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row"><p class="charsRemaining"></p><%= Html.TextArea("Comments", Model.Comments, 6, 5, new { @id = "Edit_Authorization_Comments", @maxcharacters = "500" })%></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editauthorization');">Exit</a></li>
    </ul></div>
</div>
<%}%>
