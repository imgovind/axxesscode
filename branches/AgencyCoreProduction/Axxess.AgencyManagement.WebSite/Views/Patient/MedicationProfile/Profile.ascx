﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<%= Html.Hidden("Id", Model.MedicationProfile.Id, new  {@id = "medicationProfileId" })%>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Medication Profile</th>
            </tr>
            <tr>
                <td colspan="4">
                    <span class="bigtext"><%= Model.Patient.DisplayName %></span>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="half">
                        <label for="MedProfile_EpisodeRange" class="float-left">Current Episode:</label>
                        <div class="float-right">
                            <span id="MedProfile_EpisodeRange"><%= string.Format(" {0} – {1}", Model != null && Model.StartDate != null ? Model.StartDate.ToShortDateString() : string.Empty, Model != null && Model.StartDate != null ? Model.EndDate.ToShortDateString() : "No current episode") %></span>
                        </div>
                    </div>
                    <div class="half">
                        <label for="MedProfile_Allergies" class="float-left">Allergies:</label>
                        <div class="float-right">
                            <span id="MedProfile_Allergies"><%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty %></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="half">
                        <label for="MedProfile_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <div class="float-right">
                            <span id="MedProfile_PrimaryDiagnosis"><%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : string.Empty %></span>
                        </div>
                    </div>
                    <div class="half">
                        <label for="MedProfile_SecondaryDiagnosis" class="float-left">Secondary Diagnosis:</label>
                        <div class="float-right">
                            <span id="MedProfile_SecondaryDiagnosis"><%= data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : string.Empty %></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="half">
                        <label for="MedProfile_PrimaryPharmacyName" class="float-left">Primary Pharmacy:</label>
                        <div class="float-right">
                            <span id="MedProfile_PrimaryPharmacyName"><%= Model != null && Model.PrimaryPharmacy != null && Model.PrimaryPharmacy.Name.IsNotNullOrEmpty() ? Model.PrimaryPharmacy.Name : string.Empty%></span>
                        </div>
                    </div>
                    <div class="half">
                        <div class="float-left">
                            <label for="MedProfile_PrimaryPharmacyPhone" class="float-left ">Pharmacy Phone:</label>
                            <div class="float-right ">
                                <span id="MedProfile_PrimaryPharmacyPhone"><%= Model != null && Model.PrimaryPharmacy != null && Model.PrimaryPharmacy.Phone.IsNotNullOrEmpty() ? Model.PrimaryPharmacy.Phone.ToPhone() : string.Empty%></span>
                            </div>
                        </div>
                        <div class="float-right" style="min-width:170px">
                            <label for="MedProfile_PrimaryPharmacyFax" class="float-left">Pharmacy Fax:</label>
                            <div class="float-right">
                                <span id="Span1"><%= Model != null && Model.PrimaryPharmacy != null && Model.PrimaryPharmacy.FaxNumber.IsNotNullOrEmpty() ? Model.PrimaryPharmacy.FaxNumber.ToPhone() : string.Empty%></span>
                            </div>
                       </div> 
                    </div>
                    <div class="clear"></div>
                     <div class="half">
                        <label for="MedProfile_SecondaryPharmacyName" class="float-left">Secondary Pharmacy:</label>
                        <div class="float-right">
                            <span id="MedProfile_SecondaryPharmacyName"><%= Model != null && Model.SecondaryPharmacy != null && Model.SecondaryPharmacy.Name.IsNotNullOrEmpty() ? Model.SecondaryPharmacy.Name : string.Empty%></span>
                        </div>
                    </div>
                    <div class="half">
                        <div class="float-left">
                            <label for="MedProfile_SecondaryPharmacyPhone" class="float-left ">Pharmacy Phone:</label>
                            <div class="float-right ">
                                <span id="MedProfile_SecondaryPharmacyPhone"><%= Model != null && Model.SecondaryPharmacy != null && Model.SecondaryPharmacy.Phone.IsNotNullOrEmpty() ? Model.SecondaryPharmacy.Phone.ToPhone() : string.Empty%></span>
                            </div>
                        </div>
                        <div class="float-right" style="min-width:170px">
                            <label for="MedProfile_SecondaryPharmacyFax" class="float-left">Pharmacy Fax:</label>
                            <div class="float-right">
                                <span id="MedProfile_SecondaryPharmacyFax"><%= Model != null && Model.SecondaryPharmacy != null && Model.SecondaryPharmacy.FaxNumber.IsNotNullOrEmpty() ? Model.SecondaryPharmacy.FaxNumber.ToPhone() : string.Empty%></span>
                            </div>
                       </div> 
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="buttons">
        <ul class="float-left">
            <% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
            <li><a href="javascript:void(0);" onclick="Medication.Add('<%= Model.MedicationProfile.Id %>');">Add Medication</a></li>
            <li><a href="javascript:void(0);" onclick="Patient.LoadMedicationProfileSnapshot('<%= Model.Patient.Id%>');">Sign Medication Profile</a></li>
            <% } %>
            <li><a href="javascript:void(0);" onclick="Acore.OpenPrintView({ Url: '/Patient/MedicationProfilePrint/<%=Model.Patient.Id %>', PdfUrl: '/Patient/MedicationProfilePdf', PdfData: { 'id': '<%=Model.Patient.Id %>' } });">Print Medication Profile</a></li>
        </ul>
        <ul class="float-right">
             <li class="red">
                <a href="javascript:void(0);" onclick="Medication.DrugDrugInteractions('<%= Model.MedicationProfile.Id %>');">Drug Interactions</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="Patient.LoadMedicationProfileSnapshotHistory('<%= Model.Patient.Id%>');">Signed Medication Profiles</a>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <div id="MedProfile_medications">
        <%  Html.RenderPartial("~/Views/Patient/MedicationProfile/Medication/List.ascx", Model.MedicationProfile); %>
    </div>
    <div class="buttons">
        <ul>
            <li>
                <a href="javascript:void(0);" onclick="Medication.Refresh('<%= Model.MedicationProfile.Id %>');">Refresh Medications</a>
            </li>
        </ul>
    </div>
    <div class="activity-log">
        <% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadMedicationLog('{0}');\" >Activity Logs</a>", Model.MedicationProfile.PatientId)%>
    </div>
</div>
<script type="text/javascript">
    $(".medprofile ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last")
    })
</script>