﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<MedicationProfileSnapshotViewData>>" %><%
var medications = Model != null&& Model.Data.MedicationProfile !=null && Model.Data.MedicationProfile.Medication.IsNotNullOrEmpty() ? Model.Data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.LocationProfile != null && Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + " | " : "" %>Medication Profile<%= Model != null && Model.PatientProfile != null ? (" | " + (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.Clean().ToTitleCase() : "") + ", " + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.Clean().ToTitleCase() : "") + " " + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.Clean().ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/medprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.LocationProfile; %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.LocationProfile != null ? (Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= (Model != null && Model.PatientProfile != null ? (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "mr": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientProfile.PatientIdNumber.Clean() : string.Empty %>",
            "physician": "<%= Model.Data.PhysicianName.Clean().ToTitleCase() %>",
            "episode": "<%= Model != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid() ? string.Format(" {0}&#8211;{1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()).Clean() : string.Empty %>",
            "pripharmacy": "<%= Model != null && Model.Data.PharmacyName.IsNotNullOrEmpty() ? Model.Data.PharmacyName.Clean() : string.Empty%>",
            "pripharmphone": "<%= Model != null && Model.Data.PharmacyPhone.IsNotNullOrEmpty() ? Model.Data.PharmacyPhone.ToPhone() : string.Empty%>",
            "pripharmfax": "<%= Model != null && Model.Data.PharmacyFax.IsNotNullOrEmpty() ? Model.Data.PharmacyFax.ToPhone() : string.Empty%>",
            "secpharmacy": "<%= Model != null && Model.Data.SecondaryPharmacyName.IsNotNullOrEmpty() ? Model.Data.SecondaryPharmacyName.Clean() : string.Empty%>",
            "secpharmphone": "<%= Model != null && Model.Data.SecondaryPharmacyPhone.IsNotNullOrEmpty() ? Model.Data.SecondaryPharmacyPhone.ToPhone() : string.Empty%>",
            "secpharmfax": "<%= Model != null && Model.Data.SecondaryPharmacyFax.IsNotNullOrEmpty() ? Model.Data.SecondaryPharmacyFax.ToPhone() : string.Empty%>",
  
            "pridiagnosis": "<%= Model != null && Model.Data.PrimaryDiagnosis.IsNotNullOrEmpty() ? Model.Data.PrimaryDiagnosis : "" %>",
            "secdiagnosis": "<%= Model != null && Model.Data.SecondaryDiagnosis.IsNotNullOrEmpty() ? Model.Data.SecondaryDiagnosis : string.Empty %>",
            "allergies": "<%= Model != null && Model.Data.Allergies.IsNotNullOrEmpty() ? Model.Data.Allergies.Clean() : string.Empty %>",
            "sign": "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : string.Empty %>",
            "signdate": "<%= Model != null && Model.Data.SignatureDate.IsValid() ? Model.Data.SignatureDate.ToShortDateString().Clean() : string.Empty %>"
        };
        PdfPrint.BuildSections([ <%
if (medications.Count > 0) {
    bool first = true;
    foreach (var medication in medications) if (medication.MedicationCategory == "Active") {
        if (first) { %>
                { Content: [ [ "<h3>Active Medications</h3>" ] ] },
                {
                    Content: [
                        [
                            "<strong>LS</strong>",
                            "<strong>Start Date</strong>",
                            "<strong>Medication &#38; Dosage</strong>",
                            "<strong>Frequency</strong>",
                            "<strong>Route</strong>",
                            "",
                            "<strong>Classification</strong>"
                        ]
                    ]
                }, <%
            first = false;
        } %>     {
                    Content: [
                        [
                            PdfPrint.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
                            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
                            "<%= medication.MedicationDosage.Clean() %>",
                            "<%= medication.Frequency.Clean() %>",
                            "<%= medication.Route.Clean() %>",
                            "<%= medication.MedicationType.Value.Clean() %>",
                            "<%= medication.Classification.Clean() %>"
                        ]
                    ]
                },<%
    }
    first = true;
    foreach (var medication in medications) if (medication.MedicationCategory == "DC") {
        if (first) { %>
                 { Content: [ [ "<h3>Discontinued Medications</h3>" ] ] },
                {
                    Content: [
                        [
                            "<strong>LS</strong>",
                            "<strong>Start Date</strong>",
                            "<strong>Medication &#38; Dosage</strong>",
                            "<strong>Frequency</strong>",
                            "<strong>Route</strong>",
                            "",
                            "<strong>Classification</strong>",
                            "<strong>D/C Date</strong>"
                        ]
                    ]
                } <%
            first = false;
        } %>    , {
                    Content: [
                        [
                            PdfPrint.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
                            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
                            "<%= medication.MedicationDosage.Clean() %>",
                            "<%= medication.Frequency.Clean() %>",
                            "<%= medication.Route.Clean() %>",
                            "<%= medication.MedicationType.Value.Clean() %>",
                            "<%= medication.Classification.Clean() %>",
                            "<%= medication.DCDate.IsValid() ? medication.DCDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty%>"
                        ]
                    ]
                } <%
    }
} %>
        ]);
    <% }).Render(); %>
</body>
</html>