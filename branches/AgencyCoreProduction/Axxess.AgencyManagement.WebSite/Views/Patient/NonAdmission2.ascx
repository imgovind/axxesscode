﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="nonAdmissionContainer" class="hidden">
  <fieldset>
    <legend>Non-Admission Information</legend>
    <table class="form"><tbody>   
        <tr>
            <td><label for="ChangeStatus_NonAdmitDate" class="bold">Non-Admit Date:</label></td><td><label for="ChangeStatus_ReasonInAppropriate" class="bold">Reason Not Admitted:</label></td>
        </tr>
        <tr>
            <td><input type="text" class="date-picker required" name="NonAdmitDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="ChangeStatus_NonAdmitDate" /></td>
            <td><%= Html.NonAdmissionReasons("Reason", "", new { @id = "ChangeStatus_NonAdmissionReason", @class = "required dropdown" })%></td>
        </tr>
        <tr>
            <td colspan="2"><label for="Comment"><strong>Comments:</strong></label><div><%= Html.TextArea("NonAdmissionComments", "")%></div></td>
        </tr>
    </tbody></table>
   </fieldset>
</div>