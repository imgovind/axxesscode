﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PendingPatient>" %>
<% using (Html.BeginForm("PatientReadmit", "Patient", FormMethod.Post, new { @id = "readmitForm"})){ %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Patient_Id" })%>
<%= Html.Hidden("IsAdmit", "true", new { @id = "NonAdmit_Patient_IsAdmit" })%>
<%= Html.Hidden("Type", Model.Type)%>
<div class="wrapper main">
    
    <fieldset>
        <legend>Admit Patient <span class="bigtext align-center">(<%= Model.DisplayName %>)</span></legend>
        <div class="column">
            <div class="row"><label for="Admit_Patient_PatientID" class="float-left"><span class="green">(M0020)</span> Patient ID:</label><div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Admit_Patient_PatientID", @class = "text input_wrapper", @maxlength = "50" }) %></div></div>
            <div class="row"><label for="Admit_Patient_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date:(Re-admission Date):</label><div class="float-right"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_StartOfCareDate" onchange="OnSocChange();" /></div></div>
            <div class="row"><label for="Admit_Patient_EpisodeStartDate" class="float-left">Episode Start Date:</label><div class="float-right"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_EpisodeStartDate" /></div></div>
            <div class="row"><label for="Admit_Patient_PatientReferralDate" class="float-left">Referral Date:</label><div class="float-right"><input type="text" class="date-picker" name="ReferralDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_PatientReferralDate" /></div></div>
            <div class="row"><label for="Admit_Patient_CaseManager" class="float-left">Case Manager:</label><div class="float-right"><%= Html.CaseManagers("CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "Admit_Patient_CaseManager", @class = "Users requireddropdown valid" })%></div></div>
            <div class="row">
                <label for="Admit_Patient_Physician" class="float-left">Physician:</label>
                <div class="float-right">
                    <%= Html.TextBox("PrimaryPhysician", Model.PrimaryPhysician.ToString(), new { @id = "Admit_Patient_PrimaryPhysician", @class = "Physicians required" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="Admit_Patient_LocationId" class="float-left">Agency Branch:</label><div class="float-right"><%= Html.UserBranchList("AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, true, "-- Select Branche --", new { @id = "Admit_Patient_LocationId", @class = "BranchLocation requireddropdown" })%></div></div>
            <div class="row"><label for="Admit_Patient_PrimaryInsurance" class="float-left">Primary Insurance:</label><div class="float-right"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance, Model.AgencyLocationId, false, new { @id = "Admit_Patient_PrimaryInsurance", @class = "requireddropdown valid" })%></div></div>
            <div class="row"><label for="Admit_Patient_SecondaryInsurance" class="float-left">Secondary Insurance:</label><div class="float-right"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance, Model.AgencyLocationId, false, new { @id = "Admit_Patient_SecondaryInsurance" })%></div></div>
            <div class="row"><label for="Admit_Patient_TertiaryInsurance" class="float-left">Tertiary Insurance:</label><div class="float-right"><%= Html.InsurancesByBranch("TertiaryInsurance", Model.TertiaryInsurance, Model.AgencyLocationId, false, new { @id = "Admit_Patient_TertiaryInsurance" })%></div></div>
            <div class="row"><label for="Admit_Patient_Assign" class="float-left">Assign to Clinician:</label><div class="float-right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Patient_Assign", @class = "requireddropdown Users  valid" })%></div></div>
            <div class="row"><input id="Admit_Patient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" class="radio float-left" />Create a face to face encounter <em>(this is applicable for SOC date after 01/01/2011)</em></div>
        </div>
        
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Re-admit</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $("#Admit_Patient_LocationId").change(function() {
        Agency.LoadAgencyInsurances($(this).val(), "Admit_Patient_PrimaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "Admit_Patient_SecondaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "Admit_Patient_TertiaryInsurance");
    });
    function OnSocChange() {
        var e = $("#Admit_Patient_StartOfCareDate").datepicker("getDate");
        $("#Admit_Patient_EpisodeStartDate").datepicker("setDate", e);
        $("#Admit_Patient_EpisodeStartDate").datepicker("option", "mindate", e);
    }
    $("#Admit_Patient_PrimaryPhysician").PhysicianInput();
    
</script>
<% } %>
