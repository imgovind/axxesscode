﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Patient>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Axxess&#8482; Technology Solutions - Driving Directions to Patient Powered by Google&#174;</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("desktop.css")
        .Add("forms.css")
        .Add("Site.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
</head>
<body style="overflow:auto">
    <div class="navigation">
        <div class="start address-block">
            <div>
                <label class="strong">Starting Address:</label><input name="start" type="hidden" value="" />
                <%= Html.MapAddress( "startaddress_select",new { @id = "Map_startaddress_select", @class = "float-right required" })%>
                <div class="clear"></div>
            </div>
            <div id="Map_startaddress">
                <input type="text" id="Map_startaddr" value="" class="mapaddr" /><br />
                <input type="text" id="Map_startcity" value="" class="mapcity" />
                <%= Html.States("startstate", Model.AddressStateCode, new { @id = "Map_startstate", @class = "mapcity" })%>
                <%= Html.TextBox("startzip", "", new { @id = "Map_startzip", @class = "mapzip", @maxlength = "9" })%>
            </div>
        </div>
        <div class="end address-block">
            <div><label class="strong">Patient&#8217;s Address</label><input name="end" type="hidden" value="<%= Model.AddressFull %>" /></div> 
            <div>
                <input type="text" id="Map_endaddr" value="<%= Model.AddressLine1 %> <%= Model.AddressLine2 %>" class="mapaddr" /><br />
                <input type="text" id="Map_endcity" value="<%= Model.AddressCity %>" class="end mapcity" />
                <%= Html.States("endstate", Model.AddressStateCode, new { @id = "Map_endstate", @class = "mapcity" })%>
                <%= Html.TextBox("endzip", Model.AddressZipCode, new { @id = "Map_endzip", @class = "mapzip", @maxlength = "9" })%>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" id="Map_Recalculate">Recalculate Directions</a></li>
            </ul>
        </div>
    </div>
    <div id="directions"></div>
    <div id="map"></div>
    <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Scripts/Plugins/Other/layout.min.js"></script>
    <script type="text/javascript" src="/Scripts/Modules/<%= AppSettings.UseMinifiedJs ? "min/" : "" %>Map.js"></script>
    <script type="text/javascript">
        GoogleMap.init($("#Map_startaddress_select option:selected").val(), $("input[name=end]").val(), "<%= AppSettings.GoogleAPIKey %>");
    </script>
</body>
</html>