﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Edit Patient | <%= Model.DisplayName %></span>
<% using (Html.BeginForm("Edit", "Patient", FormMethod.Post, new { @id = "editPatientForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Patient_Id" })%>  
<%= Html.Hidden("Status", Model.Status, new { @id = "Edit_Patient_Status" })%>    
<div class="wrapper main">
    <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Patient_FirstName" class="float-left"><span class="green">(M0040)</span> First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MiddleInitial" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "Edit_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label class="float-left"><span class="green">(M0069)</span> Gender:</label>
                <div class="float-right"><%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "Edit_Patient_GenderF", @class = "required radio" })%><label for="Edit_Patient_GenderF" class="inline-radio">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "Edit_Patient_GenderM", @class = "required radio" })%><label for="Edit_Patient_GenderM" class="inline-radio">Male</label></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_DOB" class="float-left"><span class="green">(M0066)</span> Date of Birth:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Edit_Patient_DOB" /></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right">
                    <%= Html.MartialStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "** Select **", "0", new { @id = "Edit_Patient_MaritalStatus", @class = "input_wrapper" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Height</label>
                <div class="float-right">
                    <%= Html.TextBox("Height", Model.Height > 0 ? Model.Height.ToString() : string.Empty, new { @id = "Edit_Patient_Height", @class = "numeric vitals", @maxlength = "3" }) %>
                    <%= Html.RadioButton("HeightMetric", "0", Model.HeightMetric == 0, new { @id = "Edit_Patient_HeightMetric0", @class = "radio" })%>
                    <label for="Edit_Patient_HeightMetric0" style="display:inline-block;width:20px!important">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model.HeightMetric == 1, new { @id = "Edit_Patient_HeightMetric1", @class = "radio" })%>
                    <label for="Edit_Patient_HeightMetric1" style="display:inline-block;width:20px!important">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Weight</label>
                <div class="float-right">
                    <%= Html.TextBox("Weight", Model.Weight > 0 ? Model.Weight.ToString() : string.Empty, new { @id = "Edit_Patient_Weight", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model.WeightMetric == 0, new { @id = "Edit_Patient_WeightMetric0", @class = "radio" })%>
                    <label for="Edit_Patient_WeightMetric0" style="display:inline-block;width:20px!important">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model.WeightMetric == 1, new { @id = "Edit_Patient_WeightMetric1", @class = "radio" })%>
                    <label for="Edit_Patient_WeightMetric1" style="display:inline-block;width:20px!important">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Patient_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.UserBranchList("AgencyLocationId", Model.AgencyLocationId.ToString(),false,"", new { @id = "Edit_Patient_LocationId", @class = "BranchLocation" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Patient_PatientID" class="float-left"><span class="green">(M0020)</span> Patient ID:</label>
                <div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Edit_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MedicareNumber" class="float-left"><span class="green">(M0063)</span> Medicare Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MedicaidNumber" class="float-left"><span class="green">(M0065)</span> Medicaid Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Patient_MedicaidNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_SSN" class="float-left"><span class="green">(M0064)</span> SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Patient_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= !Model.StartofCareDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_StartOfCareDate" /></div>
            </div>
            <%if(Model.Status==(int)PatientStatus.Discharged){ %>
             <div class="row">
                <label for="Edit_Patient_DischargeDate" class="float-left"> Discharge Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="DischargeDate" value="<%= !Model.DischargeDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DischargeDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_DischargeDate" /></div>
            </div>
            <%} %>
            <div class="row">
                <label for="Edit_Patient_CaseManager" class="float-left">Case Manager:</label>
                <div class="float-right"><%= Html.CaseManagers("CaseManagerId", Model.CaseManagerId.ToString(), new { @id = "Edit_Patient_CaseManager", @class = "Users requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_Assign" class="float-left">Clinician:</label>
                <div class="float-right"><%= Html.Clinicians("UserId",  Model.UserId.ToString(), new { @id = "Edit_Patient_Assign", @class = "Employees input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label class="float-left">DNR:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsDNR", "true", Model.IsDNR, new { @id = "Edit_Patient_IsDNR1", @class = "radio" })%>
                    <label for="Edit_Patient_IsDNR1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsDNR", "false", !Model.IsDNR, new { @id = "Edit_Patient_IsDNR2", @class = "radio" })%>
                    <label for="Edit_Patient_IsDNR2" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowPatientEligibility($('#Edit_Patient_MedicareNumber').val(), $('#Edit_Patient_LastName').val(),$('#Edit_Patient_FirstName').val(),$('#Edit_Patient_DOB').val(),$('input[name=Gender]:checked').val());">Verify Medicare Eligibility</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity <span class="required-red">*</span></legend>
        <table class="form">
            <tbody>
                <%string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces0' type='checkbox' value='0' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("0") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces0" class="radio">American Indian or Alaska Native</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces1' type='checkbox' value='1' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("1") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces1" class="radio">Asian</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces2' type='checkbox' value='2' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("2") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces2" class="radio">Black or African-American</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces3' type='checkbox' value='3' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("3") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces3" class="radio">Hispanic or Latino</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces4' type='checkbox' value='4' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("4") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces4" class="radio">Native Hawaiian or Pacific Islander</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces5' type='checkbox' value='5' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("5") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces5" class="radio">White</label></td>
                    <td colspan="2"><%= string.Format("<input id='EditPatient_EthnicRaces6' type='checkbox' value='6' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("6") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces6" class="radio">Unknown</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span><span class="required-red">*</span></legend>
        <table class="form">
            <tbody>
                <%string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceNone' type='checkbox' value='0' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("0") ? "checked='checked'" : "" )%><label for="Edit_Patient_PaymentSourceNone" class="radio">None; no charge for current services</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicare' type='checkbox' value='1' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("1") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicare" class="radio">Medicare (traditional fee-for-service)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicareHmo' type='checkbox' value='2' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("2") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicareHmo" class="radio">Medicare (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaid' type='checkbox' value='3' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("3") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicaid" class="radio">Medicaid (traditional fee-for-service)</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaidHmo' type='checkbox' value='4' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("4") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicaidHmo" class="radio">Medicaid (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceWorkers' type='checkbox' value='5' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("5") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceWorkers" class="radio">Workers&#8217; compensation</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceTitleProgram' type='checkbox' value='6' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("6") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceTitleProgram" class="radio">Title programs (e.g., Titile III,V, or XX)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceOtherGovernment' type='checkbox' value='7' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("7") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceOtherGovernment" class="radio">Other government (e.g.,CHAMPUS,VA,etc)</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourcePrivate' type='checkbox' value='8' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("8") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourcePrivate" class="radio">Private insurance</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourcePrivateHmo' type='checkbox' value='9' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("9") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourcePrivateHmo" class="radio">Private HMO/ managed care</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceSelf' type='checkbox' value='10' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("10") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceSelf" class="radio">Self-pay</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceUnknown' type='checkbox' value='11' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("11") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceUnknown" class="radio">Unknown</label></td>
                </tr>
                <tr>
                    <td colspan='4'><%= string.Format("<input id='Edit_Patient_PaymentSource' type='checkbox' value='12' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("12") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSource" class="radio more">Other (specify)</label><%= Html.TextBox("OtherPaymentSource", Model.OtherPaymentSource, new { @id = "Edit_Patient_OtherPaymentSource", @class = "text" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="marginBreak">
            <div class="column">
                <div class="row"><label for="Edit_Patient_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Patient_AddressLine1", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Patient_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Patient_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label><div class="float-right"><%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Patient_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @size = "5", @maxlength = "9" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label class="float-left">Home Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=3 ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Patient_HomePhone1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=6  ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Patient_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10  ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Patient_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
                <div class="row"><label class="float-left">Mobile Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 3 ? Model.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Patient_MobilePhone1", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 6 ? Model.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Patient_MobilePhone2", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 10 ? Model.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Patient_MobilePhone3", @class = "autotext digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
                <div class="row"><label for="Edit_Patient_Email" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = "Edit_Patient_Email", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_ServiceLocation" class="float-left">Default Service Location (Q Codes):</label><div class="float-right"><%= Html.ServiceLocations("ServiceLocation", Model.ServiceLocation, new { @id = "Edit_Patient_ServiceLocation", @class = "requireddropdown valid" })%></div></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Evacuation Address</legend>
        <div class="marginBreak">
            <div class="column">
                <div class="row"><label for="Edit_Evacuation_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("EvacuationAddressLine1", Model.EvacuationAddressLine1, new { @id = "Edit_Evacuation_AddressLine1", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Evacuation_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("EvacuationAddressLine2", Model.EvacuationAddressLine2, new { @id = "Edit_Evacuation_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Evacuation_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("EvacuationAddressCity", Model.EvacuationAddressCity, new { @id = "Edit_Evacuation_AddressCity", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Evacuation_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label><div class="float-right"><%= Html.States("EvacuationAddressStateCode", Model.EvacuationAddressStateCode, new { @id = "Edit_Evacuation_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("EvacuationAddressZipCode", Model.EvacuationAddressZipCode, new { @id = "Edit_Evacuation_AddressZipCode", @class = "text digits isValidUSZip zip", @size = "5", @maxlength = "9" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label class="float-left">Home Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneHomeArray", Model.EvacuationPhoneHome.IsNotNullOrEmpty() && Model.EvacuationPhoneHome.Length >= 3 ? Model.EvacuationPhoneHome.Substring(0, 3) : "", new { @id = "Edit_Evacuation_HomePhone1", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneHomeArray", Model.EvacuationPhoneHome.IsNotNullOrEmpty() && Model.EvacuationPhoneHome.Length >= 6 ? Model.EvacuationPhoneHome.Substring(3, 3) : "", new { @id = "Edit_Evacuation_HomePhone2", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneHomeArray", Model.EvacuationPhoneHome.IsNotNullOrEmpty() && Model.EvacuationPhoneHome.Length >= 10 ? Model.EvacuationPhoneHome.Substring(6, 4) : "", new { @id = "Edit_Evacuation_HomePhone3", @class = "autotext digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
                <div class="row"><label class="float-left">Mobile Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneMobileArray", Model.EvacuationPhoneMobile.IsNotNullOrEmpty() && Model.EvacuationPhoneMobile.Length >= 3 ? Model.EvacuationPhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Evacuation_MobilePhone1", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneMobileArray", Model.EvacuationPhoneMobile.IsNotNullOrEmpty() && Model.EvacuationPhoneMobile.Length >= 6 ? Model.EvacuationPhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Evacuation_MobilePhone2", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("EvacuationPhoneMobileArray", Model.EvacuationPhoneMobile.IsNotNullOrEmpty() && Model.EvacuationPhoneMobile.Length >= 10 ? Model.EvacuationPhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Evacuation_MobilePhone3", @class = "autotext digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
            </div>
        </div>
    </fieldset>
    
    <fieldset >
        <legend>Pharmacy Information</legend>
        <div class="column">
            <div class="row">
                <%  var htmlPharmacyPrimary = new Dictionary<string, string>(); %>
                <%  htmlPharmacyPrimary.Add("id", "Edit_Patient_PrimaryPharmacy"); %>
                <%  htmlPharmacyPrimary.Add("class", "Pharmacies longselect"); %>
                <label for="Edit_Patient_PrimaryPharmacy" class="float-left">Primary:</label>
                <div class="float-right" ><%= Html.Pharmacies("PrimaryPharmacyId", Model.PrimaryPharmacyId,  true, htmlPharmacyPrimary)%></div>
            </div>
         </div>
         <div class="column">
            <div class="row">
                <%  var htmlPharmacySecondary = new Dictionary<string, string>(); %>
                <%  htmlPharmacySecondary.Add("id", "Edit_Patient_SecondaryPharmacy"); %>
                <%  htmlPharmacySecondary.Add("class", "Pharmacies longselect"); %>
                <label for="Edit_Patient_SecondaryPharmacy" class="float-left">Secondary:</label>
                <div class="float-right" ><%= Html.Pharmacies("SecondaryPharmacyId", Model.SecondaryPharmacyId, true, htmlPharmacySecondary)%></div>
            </div>
        </div>
    </fieldset>
    
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <%  var htmlAttributesPrimary = new Dictionary<string, string>(); %>
                <%  htmlAttributesPrimary.Add("id", "Edit_Patient_PrimaryInsurance"); %>
                <%  htmlAttributesPrimary.Add("class", "Insurances requireddropdown"); %>
                <label for="Edit_Patient_PrimaryInsurance" class="float-left">Primary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance, Model.AgencyLocationId, true, htmlAttributesPrimary) %></div>
                <div id="Edit_Patient_PrimaryInsuranceContent">
                <%  if (Model.PrimaryInsurance.IsNotNullOrEmpty() && Model.PrimaryInsurance.IsInteger() && Model.PrimaryInsurance.ToInteger() >= 1000) { %>
                    <div class="margin float-right" >
                        <div class="row">
                            <label for="Edit_Patient_PrimaryHealthPlanId" class="float-left">Member Id:</label>
                            <div class="float-right"><%= Html.TextBox("PrimaryHealthPlanId", Model.PrimaryHealthPlanId, new { @id = "Edit_Patient_PrimaryHealthPlanId", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_PrimaryGroupName" class="float-left">Group Name:</label>
                            <div class="float-right"><%= Html.TextBox("PrimaryGroupName", Model.PrimaryGroupName, new { @id = "Edit_Patient_PrimaryGroupName", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_PrimaryGroupId" class="float-left">Group Id:</label>
                            <div class="float-right"><%= Html.TextBox("PrimaryGroupId", Model.PrimaryGroupId, new { @id = "Edit_Patient_PrimaryGroupId", @class = "" })%></div>
                        </div>
                        <div class="row" >
                            <label for="Edit_Patient_PrimaryRelationship" class="float-left">Relationship to Patient:</label>
                            <div class="float-right"><%= Html.InsuranceRelationships("PrimaryRelationship", Model.PrimaryRelationship, new { @id = "Edit_Patient_PrimaryRelationship", @class = "" })%></div>
                        </div>
                    </div>
                <%  } %>
                </div>
            </div>
            <div class="row">
                <%  var htmlAttributesSecondary = new Dictionary<string, string>(); %>
                <%  htmlAttributesSecondary.Add("id", "Edit_Patient_SecondaryInsurance"); %>
                <%  htmlAttributesSecondary.Add("class", "Insurances"); %>
                <label for="Edit_Patient_SecondaryInsurance" class="float-left">Secondary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance, Model.AgencyLocationId, true, htmlAttributesSecondary)%></div>
                <div class="row" id="Edit_Patient_SecondaryInsuranceContent">
                <%  if (Model.SecondaryInsurance.IsNotNullOrEmpty() && Model.SecondaryInsurance.IsInteger() && Model.SecondaryInsurance.ToInteger() >= 1000) { %>
                    <div class="margin float-right" >
                        <div class="row">
                            <label for="Edit_Patient_SecondaryHealthPlanId" class="float-left">Member Id:</label>
                            <div class="float-right"><%= Html.TextBox("SecondaryHealthPlanId", Model.SecondaryHealthPlanId, new { @id = "Edit_Patient_SecondaryHealthPlanId", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_SecondaryGroupName" class="float-left">Group Name:</label>
                            <div class="float-right"><%= Html.TextBox("SecondaryGroupName", Model.SecondaryGroupName, new { @id = "Edit_Patient_SecondaryGroupName", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_SecondaryGroupId" class="float-left">Group Id:</label>
                            <div class="float-right"><%= Html.TextBox("SecondaryGroupId", Model.SecondaryGroupId, new { @id = "Edit_Patient_SecondaryGroupId", @class = "" })%></div>
                        </div>
                        <div class="row" >
                            <label for="Edit_Patient_SecondaryRelationship" class="float-left">Relationship to Patient:</label>
                            <div class="float-right"><%= Html.InsuranceRelationships("SecondaryRelationship", Model.SecondaryRelationship, new { @id = "Edit_Patient_SecondaryRelationship", @class = "" })%></div>
                        </div>
                    </div>
                <%  } %>
                </div>
            </div>
            <div class="row">
                <%  var htmlAttributesTertiary = new Dictionary<string, string>(); %>
                <%  htmlAttributesTertiary.Add("id", "Edit_Patient_TertiaryInsurance"); %>
                <%  htmlAttributesTertiary.Add("class", "Insurances"); %>
                <label for="Edit_Patient_TertiaryInsurance" class="float-left">Tertiary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("TertiaryInsurance", Model.TertiaryInsurance,Model.AgencyLocationId, true, htmlAttributesTertiary)%></div>
                <div class="row" id="Edit_Patient_TertiaryInsuranceContent">
                <%  if (Model.TertiaryInsurance.IsNotNullOrEmpty() && Model.TertiaryInsurance.IsInteger() && Model.TertiaryInsurance.ToInteger() >= 1000) { %>
                    <div class="margin float-right" >
                        <div class="row">
                            <label for="Edit_Patient_TertiaryHealthPlanId" class="float-left">Member Id:</label>
                            <div class="float-right"><%= Html.TextBox("TertiaryHealthPlanId", Model.TertiaryHealthPlanId, new { @id = "Edit_Patient_TertiaryHealthPlanId", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_TertiaryGroupName" class="float-left">Group Name:</label>
                            <div class="float-right"><%= Html.TextBox("TertiaryGroupName", Model.TertiaryGroupName, new { @id = "Edit_Patient_TertiaryGroupName", @class = "" })%></div>
                        </div>
                        <div class="row">
                            <label for="Edit_Patient_TertiaryGroupId" class="float-left">Group Id:</label>
                            <div class="float-right"><%= Html.TextBox("TertiaryGroupId", Model.TertiaryGroupId, new { @id = "Edit_Patient_TertiaryGroupId", @class = "" })%></div>
                        </div>
                        <div class="row" >
                           
                            <label for="Edit_Patient_TertiaryRelationship" class="float-left">Relationship to Patient:</label>
                            <div class="float-right"><%= Html.InsuranceRelationships("TertiaryRelationship", Model.TertiaryRelationship, new { @id = "Edit_Patient_TertiaryRelationship", @class = "" })%></div>
                        </div>
                    </div>
                <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Emergency Triage <span class="light">(Select one)</span><span class="required-red">*</span></legend>
        <div class="column form">
            <div class="row">
                <%= Html.RadioButton("Triage", "1", Model.Triage == 1 ? true : false, new { @id = "Edit_Patient_Triage1", @class = "required radio Triage float-left" })%>
                <div class="float-left strong">&#160;1.&#160;</div>
                <label class="normal margin" for="Edit_Patient_Triage1"><strong>Life threatening</strong> (or potential) and requires ongoing medical treatment. When necessary,appropriate arrangements for evacuation to an acute care facility will be made.</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "2", Model.Triage == 2 ? true : false, new { @id = "Edit_Patient_Triage2", @class = "required radio Triage float-left" })%>
                <div class="float-left strong">&#160;2.&#160;</div>
                <label class="normal margin" for="Edit_Patient_Triage2"><strong>Not life threatening but would suffer severe adverse effects</strong> from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "3", Model.Triage == 3 ? true : false, new { @id = "Edit_Patient_Triage3", @class = "required radio Triage float-left" })%>
                <div class="float-left strong">&#160;3.&#160;</div>
                <label class="normal margin" for="Edit_Patient_Triage3"><strong>Visits could be postponed 24-48 hours</strong> without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "4", Model.Triage == 4 ? true : false, new { @id = "Edit_Patient_Triage4", @class = "required radio Triage float-left" })%>
                <div class="float-left strong">&#160;4.&#160;</div>
                <label class="normal margin" for="Edit_Patient_Triage4"><strong>Visits could be postponed 72-96 hours</strong> without adverse effects (i.e., post op withno open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</label></div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Evacuation Zone</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Evacuation Zone</label>
                <div class="float-right">
                    <%  var evacuationZone = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Zone A", Value = "A" },
                            new SelectListItem { Text = "Zone B", Value = "B" },
                            new SelectListItem { Text = "Zone C", Value = "C"},
                            new SelectListItem { Text = "Zone D", Value = "D" },
                            new SelectListItem { Text = "Zone E", Value = "E" },
                            new SelectListItem { Text = "Zone NE", Value = "NE"},
                            new SelectListItem { Text = "Zone SN", Value = "SN" }
                        }, "Value", "Text", Model.EvacuationZone); %>
                    <%= Html.DropDownList("EvacuationZone", evacuationZone, new { @id = "Edit_Patient_EvacuationZone", @class = "input_wrapper" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <table class="form">
            <tbody><%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <tr>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%><label for="ServicesRequiredCollection0" class="radio">SN</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection1" class="radio">HHA</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection2" class="radio">PT</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection3" class="radio">OT</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection4" class="radio">ST</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection5" class="radio">MSW</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <table class="form"><%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="DMECollection" />
            <tbody>
                <tr class="firstrow">
                    <td><%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%><label for="DMECollection0" class="radio">Bedside Commode</label></td>
                    <td><%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%><label for="DMECollection1" class="radio">Cane</label></td>
                    <td><%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%><label for="DMECollection2" class="radio">Elevated Toilet Seat</label></td>
                    <td><%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%><label for="DMECollection3" class="radio">Grab Bars</label></td>
                    <td><%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%><label for="DMECollection4" class="radio">Hospital Bed</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%><label for="DMECollection5" class="radio">Nebulizer</label></td>
                    <td><%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%><label for="DMECollection6" class="radio">Oxygen</label></td>
                    <td><%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%><label for="DMECollection7" class="radio">Tub/Shower Bench</label></td>
                    <td><%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%><label for="DMECollection8" class="radio">Walker</label></td>
                    <td><%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%><label for="DMECollection9" class="radio">Wheelchair</label></td>
                </tr>
                <tr>
                    <td colspan="5"><%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%><label for="DMECollection10" class="radio">Other</label><%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Patient_OtherDME", @class = "text", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Patient_ReferralPhysician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "Edit_Patient_ReferrerPhysician", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row"><label for="Edit_Patient_AdmissionSource" class="float-left">Admission Source:</label><div class="float-right"><%= Html.AdmissionSources("AdmissionSource", Model.AdmissionSource, new { @id = "Edit_Patient_AdmissionSource", @class = "AdmissionSource requireddropdown required" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Patient_OtherReferralSource" class="float-left">Other Referral Source:</label><div class="float-right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row">
            <label for="Edit_Patient_PatientReferralDate" class="float-left"><span class="green">(M0104)</span> Referral Date:</label><div class="float-right"><input type="text" class="date-picker" name="ReferralDate" value="<%= !Model.ReferralDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.ReferralDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_PatientReferralDate" /></div></div>
            <div class="row"><label for="Edit_Patient_InternalReferral" class="float-left">Internal Referral Source:</label><div class="float-right"><%= Html.Users("InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Patient_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset>
    <fieldset class="medication">
        <legend>Emergency Contact</legend>
        <div class="wide-column"><div class="row"><div class="buttons float-left"><ul><li><a href="javascript:void(0);" onclick="Patient.LoadNewEmergencyContact('<%=Model.Id %>');">Add New Emergency Contact</a></li></ul></div></div></div>
        <div class="clear"></div>
        <%= Html.Telerik().Grid<PatientEmergencyContact>().Name("Edit_patient_EmergencyContact_Grid").Columns(columns => {
                columns.Bound(c => c.FirstName);
                columns.Bound(c => c.LastName);
                columns.Bound(c => c.PrimaryPhoneFormatted).Title("Primary Phone");
                columns.Bound(c => c.AlternatePhoneFormatted).Title("Alternate Phone");
                columns.Bound(c => c.Relationship);
                columns.Bound(c => c.EmailAddress); 
                columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\" Patient.LoadEditEmergencyContact('" + Model.Id + "','<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteEmergencyContact('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a>").Title("Action").Width(100);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetEmergencyContacts", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
    </fieldset>
    <fieldset class="medication">
        <legend>Physicians</legend>
        <div class="wide-column">
            <div class="row">
                <div class="float-left"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "EditPatient_PhysicianSelector", @class = "Physicians" })%></div>
                <div class="buttons float-left">
                    <ul>
                        <li>
                            <a id="EditPatient_NewPhysician" href="javascript:void(0);">Add Selected Physician</a>
                        </li>
                    </ul>
                </div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditPatient_PhysicianGrid").Columns(columns => {
                columns.Bound(c => c.FirstName);
                columns.Bound(c => c.LastName);
                columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                columns.Bound(c => c.FaxNumberFormatted);
                columns.Bound(c => c.EmailAddress);
                columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.DeletePhysician('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');\" class=<#= !Primary ? \"\" : \"hidden\" #>><#=Primary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
    </fieldset>
      <%if(Model.Status==(int)PatientStatus.Discharged){ %>
             <fieldset><legend>Discharged Reason</legend>
                <div class="wide-column">
                    <div class="row"><%= Html.DischargeReasons("DischargeReasonId", Model.DischargeReasonId, new { @id = "Edit_Patient_DischargeReasonId", @class = "" })%></div>
                    <div class="row"><textarea id="Edit_Patient_DischargeReason" name="DischargeReason" cols="5" rows="6"><%= Model.DischargeReason%></textarea></div>
                </div>
            </fieldset>
            <%} %>
     <%if(Model.Status==(int)PatientStatus.NonAdmission){ %>
          <fieldset>
            <legend>Non-Admission Information</legend>
            <div class="column">
                <div class="row">
                    <label for="Edit_Patient_NonAdmitDate" class="float-left">Non-Admit Date:</label>
                    <div class="float-right"><input type="text" class="date-picker required" name="NonAdmitDate" value="<%= !Model.NonAdmitDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.NonAdmitDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_NonAdmitDate" />
                    </div>
                </div>
                <div class="row"><%= Html.NonAdmissionReasons("NonAdmitReason", Model.NonAdmitReason, new { @id = "Edit_Patient_NonAdmitReason", @class = "" })%></div>
                <div class="row"><textarea id="NonAdmitComments" name="NonAdmitComments" cols="5" rows="6"><%= Model.NonAdmitComments%></textarea></div>
            </div>
          </fieldset>
            <%} %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <%= Html.ToggleTemplates("Edit_Patient_CommentsTemplates", "", "#Edit_Patient_Comments")%>
            <div class="row">
                <p class="charsRemaining"></p>
                <textarea style="height:150px;" id="Edit_Patient_Comments" name="Comments" cols="5" rows="6" maxcharacters="2000"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>State Surveyor/Auditor Access</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">State Surveyor/Auditor:</label>
                <div class="float-right"><%= Html.Auditors("AuditorId", Model.AuditorId.ToString(), new { @id = "Edit_Patient_AuditorId", @class = "Employees input_wrapper" })%>
                </div>
            </div>
            <div class="row"><i>Hint: A user account must be created for the surveyor/auditor before you can grant them access to patient records.</i></div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editpatient');">Exit</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript">
    $("#window_editpatient select.Insurances").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('newinsurance');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") selectList.selectedIndex = 0;
    });
    $("#Edit_Patient_LocationId").change(function() {
        Agency.LoadAgencyInsurances($(this).val(), "Edit_Patient_PrimaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "Edit_Patient_SecondaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "Edit_Patient_TertiaryInsurance");
    });
    $("#Edit_Patient_PrimaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#Edit_Patient_PrimaryInsuranceContent", '<%=Model.Id %>', $(this).val(), 'Edit_Patient', 'Primary');
        else $("#Edit_Patient_PrimaryInsuranceContent").empty();
    });
    $("#Edit_Patient_SecondaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#Edit_Patient_SecondaryInsuranceContent", '<%=Model.Id %>', $(this).val(), 'Edit_Patient', 'Secondary');
        else $("#Edit_Patient_SecondaryInsuranceContent").empty()
    });
    $("#Edit_Patient_TertiaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#Edit_Patient_TertiaryInsuranceContent", '<%=Model.Id %>', $(this).val(), 'Edit_Patient', 'Tertiary')
        else $("#Edit_Patient_TertiaryInsuranceContent").empty()
    });

    $("#window_editpatient select.Pharmacies").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('newpharmacy');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") selectList.selectedIndex = 0;
    });

    $("#nonAdmissionContainer").show();
    
</script>