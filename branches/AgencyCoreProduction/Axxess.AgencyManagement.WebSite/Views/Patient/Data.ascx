﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<%  if (Model.Patient != null) { %>
    <div class="top" id="patientTop">
        <%  Html.RenderPartial("/Views/Patient/Info.ascx", Model.Patient); %>
    </div>
    <div class="bottom">
        <%  Html.RenderPartial("/Views/Patient/Activities.ascx", Model.Patient.Id); %>
    </div>
    <%  Html.RenderPartial("/Views/Patient/Popup.ascx", Model.Patient); %>
    <%= Html.Hidden("PatientCenter_PatientId", Model.Patient.Id) %>
    <script type="text/javascript">
	    Patient.Charts.Activities.Init();
     </script>
<%  } else { %>
    <div class="abs center">No Patient Data Found</div>
<%  } %>
