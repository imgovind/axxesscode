﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<FaceToFaceEncounter>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%=  Current.AgencyName + " | " %>Face to Face Encounter<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/physface2face.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.LocationProfile; %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= ((Current.AgencyName + "<br />") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : "") + (location.AddressLine2.IsNotNullOrEmpty() ? ", "+location.AddressLine2 + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") : "") + "<br />Phone:" + location.PhoneWorkFormatted+" | Fax:"+location.FaxNumberFormatted).Clean()%>",
            "patientname": "<%= Model != null && Model.PatientProfile != null ?
                (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : string.Empty).Clean() +
                (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : string.Empty).Clean() +
                (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() + "<br/>" : "<br/>").Clean() : string.Empty %>",
            "patient": "<%=(Model != null && Model.PatientProfile != null ? (Model.PatientProfile.Address.Line1.IsNotNullOrEmpty() ? Model.PatientProfile.Address.Line1.Clean() + "<br/>" : "") + (Model.PatientProfile.Address.Line2.IsNotNullOrEmpty() ? Model.PatientProfile.Address.Line2.Clean() + "<br/>" : "") + (Model.PatientProfile.Address.City.IsNotNullOrEmpty() ? Model.PatientProfile.Address.City.Clean() + ", " : "") + (Model.PatientProfile.Address.StateCode.IsNotNullOrEmpty() ? Model.PatientProfile.Address.StateCode.ToTitleCase() + "  " : "") + (Model.PatientProfile.Address.ZipCode.IsNotNullOrEmpty() ? Model.PatientProfile.Address.ZipCode + "<br/>" : "") + (Model.PatientProfile.Phone.IsNotNullOrEmpty() ? Model.PatientProfile.Phone + "<br/>" : "") + (Model.PatientProfile.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + Model.PatientProfile.MedicareNumber : "") : "")%>",
            "physicianname": "<%= Model != null && Model.Data.Physician != null && Model.Data.Physician.DisplayName.IsNotNullOrEmpty() ? Model.Data.Physician.DisplayName : string.Empty%>",
            "physician": "<%= (Model != null && Model.Data.Physician != null ? (Model.Data.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Data.Physician.AddressLine1.Clean() : "") + (Model.Data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.Data.Physician.AddressLine2.Clean() + "<br/>" : "<br/>") + (Model.Data.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Data.Physician.AddressCity.Clean() + ", " : "") + (Model.Data.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Data.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (Model.Data.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Data.Physician.AddressZipCode + "<br/>" : "") + (Model.Data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + Model.Data.Physician.PhoneWork.ToPhone() : "") + (Model.Data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + Model.Data.Physician.FaxNumber.ToPhone() + "<br/>" : "<br/>") + (Model.Data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + Model.Data.Physician.NPI : "") : "")%>",
            "mr": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientProfile.PatientIdNumber : string.Empty%>",
            "order": "<%= Model != null && Model.Data != null ? Model.Data.OrderNumber.ToString() : string.Empty %>",
            "dob": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.DOB.IsValid() ? Model.PatientProfile.DOB.ToZeroFilled() : string.Empty%>",
            "socdate": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.StartofCareDate.IsValid() ? Model.PatientProfile.StartofCareDate.ToZeroFilled() : string.Empty%>",
            "episode": "<%= Model != null ? (Model.Data.EpisodeStartDate.IsNotNullOrEmpty() && Model.Data.EpisodeStartDate.IsValidDate() ? (Model.Data.EpisodeEndDate.IsNotNullOrEmpty() && Model.Data.EpisodeEndDate.IsValidDate() ? string.Format("{0}-{1}", Model.Data.EpisodeStartDate, Model.Data.EpisodeEndDate) : string.Empty) : string.Empty) : string.Empty%>",
            "physsign": "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>",
            "physsigndate": "<%= Model != null && Model.Data.SignatureDate.IsValid() ? Model.Data.SignatureDate.ToShortDateString() : string.Empty %>"
        };
        PdfPrint.BuildSections(<%= Model.Data.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>