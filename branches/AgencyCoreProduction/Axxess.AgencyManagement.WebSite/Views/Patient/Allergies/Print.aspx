﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<AllergyProfileViewData>>" %><%
																									   var allergies = Model != null && Model.Data.AllergyProfile != null && Model.Data.AllergyProfile.Allergies.IsNotNullOrEmpty() ? Model.Data.AllergyProfile.Allergies.ToObject<List<Allergy>>().ToList() : new List<Allergy>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.LocationProfile != null && Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + " | " : "" %>Allergy Profile<%= Model != null && Model.PatientProfile != null ? (" | " + (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.Clean().ToTitleCase() : "") + ", " + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.Clean().ToTitleCase() : "") + " " + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.Clean().ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/allergyprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.LocationProfile != null ? (Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : "") + (Model.LocationProfile != null ? (Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : "") + (Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ?  ", "+Model.LocationProfile.AddressLine2.Clean() + "<br />" : "<br />") + (Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : "") + (Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.ToString().ToUpper() + "  " : "") + (Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : "") : "") : "").Clean() %>",
            "patientname": "<%= (Model != null && Model.PatientProfile != null ? (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "mr": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientProfile.PatientIdNumber.Clean() : string.Empty %>"
        };
        PdfPrint.BuildSections([
<%
bool first = true;
if (allergies.Count > 0) foreach (var allergy in allergies) if (allergy != null && !allergy.IsDeprecated) {
    if (first) first = false;
    else { %> , <% } %>
            {
                Content: [
                    [
                        "<p><%= allergy.Name.IsNotNullOrEmpty() ? allergy.Name.Clean() : string.Empty %></p>",
                        "<p><%= allergy.Type.IsNotNullOrEmpty() ? allergy.Type.Clean() : string.Empty %></p>"
                    ]
                ]
            } <%
} %>
<% if (first) { %>{Content:[["",""]]}<% } %>
        ]);
    <% }).Render(); %>
</body>
</html>