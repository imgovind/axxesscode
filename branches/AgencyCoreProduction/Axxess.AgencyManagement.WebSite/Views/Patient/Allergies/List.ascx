﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfile>" %>
<div id="<%= Model.Type %>_active" class="allergy-profile-active standard-chart">
    <ul>
        <li class="align-center"><h3>Active Allergies</h3></li>
        <li>
            <span class="name">Name</span>
            <span class="type">Type</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0)
        {
            foreach (var allergy in allergies) { %>
            <li class="<%= i % 2 != 0 ? "odd" : "even"%>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                <span class="name"><%= allergy.Name %></span>
                <span class="type"><%= allergy.Type %></span>
                <span class="action"><% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><a href="javascript:void(0);" onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>', '<%= Model.Type %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Allergy.Delete('<%=Model.Id %>', '<%= allergy.Id %>','<%= Model.Type %>');">Delete</a><% } %></span>
            </li><%
                i++;
            }
        } else { %>
            <li class="align-center"><span class="darkred">No Active Allergies</span></li>
    <% } } %>
    </ol>
</div>
<div id="<%= Model.Type %>_inactive" class="allergy-profile-inactive standard-chart">
    <ul>
        <li class="align-center"><h3>Deleted Allergies</h3></li>
        <li>
            <span class="name">Name</span>
            <span class="type">Type</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int j = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == true).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0) {
            foreach (var allergy in allergies) { %>
            <li class="<%= j % 2 != 0 ? "odd" : "even"%>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                <span class="name"><%= allergy.Name %></span>
                <span class="type"><%= allergy.Type %></span>
                <span class="action"><% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><a href="javascript:void(0);" onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>', '<%= Model.Type %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Allergy.Restore('<%=Model.Id %>', '<%= allergy.Id %>', '<%= Model.Type %>');">Restore</a><% } %></span>
            </li><%
                j++;
            }
        } else { %>
            <li class="align-center"><span class="darkred">No Deleted Allergies</span></li>
    <% } } %>
    </ol>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>