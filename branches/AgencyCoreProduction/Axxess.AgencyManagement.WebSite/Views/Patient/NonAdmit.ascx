﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("AddNonAdmit", "Patient", FormMethod.Post, new { @id = "newNonAdmitPatientForm" }))   { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Patient_Id" })%>
<%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Patient_IsAdmit" })%>
<div class="wrapper main">
    
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div><span class="bigtext align-center"><%= Model.DisplayName %></span></div>
        <table class="form"><tbody>   
        <tr>
            <td><label for="NonAdmit_Patient_Date" class="bold">Non-Admit Date:</label></td><td><label for="NonAdmit_Patient_Reason" class="bold">Reason Not Admitted:</label></td>
        </tr>
        <tr>
            <td><input type="text" class="date-picker required" name="NonAdmitDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="NonAdmit_Patient_Date" /></td>
            <td><%= Html.NonAdmissionReasons("Reason", "", new { @id = "NonAdmit_Patient_Reason", @class = "requireddropdown" })%></td>
        </tr>
        <tr>
            <td colspan="2"><label for="Comment"><strong>Comments:</strong></label><div><%= Html.TextArea("NonAdmissionComments", "")%></div></td>
        </tr>
    </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>
<%} %>

