﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayBPSystolic = new List<double>(); %>
<%  var arrayBPDiastolic = new List<double>(); %>
<%  var arrayBPSystolicDate = new List<string>(); %>
<%  var arrayBPDiastolicDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.BPSystolic > 0) { %>
        <%  arrayBPSystolic.Add(v.BPSystolic); %>
        <%  arrayBPSystolicDate.Add(v.VisitDate); %>
    <%  } %>
    <%  if (v.BPDiastolic > 0) { %>
        <%  arrayBPDiastolic.Add(v.BPDiastolic); %>
        <%  arrayBPDiastolicDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var bpSystolic = arrayBPSystolic.ToJavascrptArray(); %>
<%  var bpDiastolic = arrayBPDiastolic.ToJavascrptArray(); %>
<%  var dateBPJson = arrayBPSystolicDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Blood Pressure
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_BloodPressure" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Blood Pressure</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-single-value"><%= item.BPSystolic %> / <%= item.BPDiastolic %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_BloodPressure").Graph({
        Type: "line",
        Title: "Blood Pressure",
        YAxisTitle: "Blood Pressure",
        XAxisData: <%= dateBPJson %>,
        YAxisData: [{ name: "Systolic", data: <%= bpSystolic %> }, { name: "Diastolic", data: <%= bpDiastolic %> }]
    });
</script>