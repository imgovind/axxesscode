﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayPain = new List<double>(); %>
<%  var arrayPainDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.PainLevelGraph > 0) { %>
        <%  arrayPain.Add(v.PainLevelGraph); %>
        <%  arrayPainDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var painJson = arrayPain.ToJavascrptArray(); %>
<%  var datePainJson = arrayPainDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Pain Level
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Pain" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Pain</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-single-value"><%= item.PainLevel %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Pain").Graph({
        Type: "line",
        Title: "Pain Level",
        YAxisTitle: "Pain Level",
        XAxisData: <%= datePainJson %>,
        YAxisData: [{ name: "Pain", data: <%= painJson %> }]
    });
</script>