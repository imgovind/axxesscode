﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).ToList() : new List<VitalSign>(); %>
<h3 class="collapsable">
    Report
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="vitals-report-date">Date</span>
                <span class="vitals-report-employee">Employee Name</span>
                <span class="vitals-report-task">Task</span>
                <span class="vitals-report-value">BP Lying</span>
                <span class="vitals-report-value">BP Sit</span>
                <span class="vitals-report-value">BP Stand</span>
                <span class="vitals-report-value-sm">Temp</span>
                <span class="vitals-report-value-sm">Resp</span>
                <span class="vitals-report-value">Apical Pulse</span>
                <span class="vitals-report-value">Radial Pulse</span>
                <span class="vitals-report-value-sm">BS</span>
                <span class="vitals-report-value-sm">Weight</span>
                <span class="vitals-report-value-sm">Pain</span>
                <span class="vitals-report-value-sm">O<sub>2</sub> Sat</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-report-date"><%= item.VisitDate %></span>
                <span class="vitals-report-employee"><%= item.UserDisplayName %></span>
                <span class="vitals-report-task"><%= item.DisciplineTask%></span>
                <span class="vitals-report-value"><%= item.BPLying %></span>
                <span class="vitals-report-value"><%= item.BPSitting %></span>
                <span class="vitals-report-value"><%= item.BPStanding %></span>
                <span class="vitals-report-value-sm"><%= item.Temp %></span>
                <span class="vitals-report-value-sm"><%= item.Resp %></span>
                <span class="vitals-report-value"><%= item.ApicalPulse %></span>
                <span class="vitals-report-value"><%= item.RadialPulse %></span>
                <span class="vitals-report-value-sm"><%= item.BSMax %></span>
                <span class="vitals-report-value-sm"><%= item.Weight %></span>
                <span class="vitals-report-value-sm"><%= item.PainLevel %></span>
                <span class="vitals-report-value-sm"><%= item.OxygenSaturation %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
