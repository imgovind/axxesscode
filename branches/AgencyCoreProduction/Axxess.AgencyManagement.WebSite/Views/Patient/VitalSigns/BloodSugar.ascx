﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayBSMax = new List<double>(); %>
<%  var arrayBSMin = new List<double>(); %>
<%  var arrayBSDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.BSMaxGraph > 0) { %>
        <%  arrayBSMax.Add(v.BSMaxGraph); %>
        <%  arrayBSDate.Add(v.VisitDate); %>
    <%  } %>
    <%  if (v.BSMinGraph > 0) { %>
        <%  arrayBSMin.Add(v.BSMinGraph); %>
        <%  arrayBSDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var bsMaxJson = arrayBSMax.ToJavascrptArray(); %>
<%  var bsMinJson = arrayBSMin.ToJavascrptArray(); %>
<%  var dateBSJson = arrayBSDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Blood Sugar
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_BloodSugar" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-multi-value">Min</span>
                <span class="vitals-multi-value">Max</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-multi-value"><%= item.BSMin %></span>
                <span class="vitals-multi-value"><%= item.BSMax %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_BloodSugar").Graph({
        Type: "line",
        Title: "Blood Sugar Level",
        YAxisTitle: "Blood Sugar",
        XAxisData: <%= dateBSJson %>,
        YAxisData: [{ name: "Min", data: <% =bsMinJson %> }, { name: "Max", data: <%= bsMaxJson %> }]
    });
</script>