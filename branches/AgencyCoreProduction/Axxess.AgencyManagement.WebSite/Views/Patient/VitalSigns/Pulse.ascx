﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayAPulse = new List<double>(); %>
<%  var arrayRPulse = new List<double>(); %>
<%  var arrayPulseDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.ApicalPulseGraph > 0) { %>
        <%  arrayAPulse.Add(v.ApicalPulseGraph); %>
        <%  arrayPulseDate.Add(v.VisitDate); %>
    <%  } %>
    <%  if (v.RadialPulseGraph > 0) { %>
        <%  arrayRPulse.Add(v.RadialPulseGraph); %>
        <%  arrayPulseDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var pulseRJson = arrayRPulse.ToJavascrptArray(); %>
<%  var pulseAJson = arrayAPulse.ToJavascrptArray(); %>
<%  var datePulseJson = arrayPulseDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Pulse
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Pulse" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-multi-value">Apical</span>
                <span class="vitals-multi-value">Radial</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-multi-value"><%= item.ApicalPulse %></span>
                <span class="vitals-multi-value"><%= item.RadialPulse %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Pulse").Graph({
        Type: "line",
        Title: "Pulse",
        YAxisTitle: "Pulse",
        XAxisData: <%= datePulseJson %>,
        YAxisData: [{ name: "Apical", data: <%= pulseAJson %> }, { name: "Radial", data: <%= pulseRJson %> }]
    });
</script>