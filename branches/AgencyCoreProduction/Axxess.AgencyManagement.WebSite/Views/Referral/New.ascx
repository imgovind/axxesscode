﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Referral", FormMethod.Post, new { @id = "newReferralForm" })) { %>
<span class="wintitle">New Referral | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_Physician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician", "", new { @id = "New_Referral_Physician", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_Referral_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.AdmissionSources("AdmissionSource", "", new { @id = "New_Referral_AdmissionSource", @class = "AdmissionSource requireddropdown required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Referral_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", "", new { @id = "New_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Referral_Date" class="float-left">Referral Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="ReferralDate" id="New_Referral_Date" /></div>
            </div>
            <div class="row">
                <label for="New_Referral_InternalReferral" class="float-left">Internal Referral Source:</label>
                <div class="float-right"><%= Html.Users("InternalReferral", "", new { @id = "New_Referral_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", "", new { @id = "New_Referral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_MiddleInitial" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial", "", new { @id = "New_Referral_MiddleInitial", @class = "text input_wrapper mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="New_Referral_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", "", new { @id = "New_Referral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", new { @id = "New_Referral_Gender_F", @class = "radio required" }) %>
                    <label for="New_Referral_Gender_F" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", new { @id = "New_Referral_Gender_M", @class = "radio required" })%>
                    <label for="New_Referral_Gender_M" class="inline-radio">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="New_Referral_DateOfBirth" class="float-left">Date of Birth:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="DOB" id="New_Referral_DateOfBirth" /></div>
            </div>
            <div class="row">
                <label for="New_Referral_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right">
                        <%= Html.MartialStatus("MaritalStatus", "0", true, "** Select **", "0", new { @id = "New_Referral_MaritalStatus", @class = "input_wrapper valid" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Height</label>
                <div class="float-right">
                    <%= Html.TextBox("Height", "", new { @id = "New_Referral_Height", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", new { @id = "New_Referral_HeightMetric0", @class = "radio" })%>
                    <label for="New_Referral_HeightMetric0" style="display:inline-block;width:20px!important">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", new { @id = "New_Referral_HeightMetric1", @class = "radio" })%>
                    <label for="New_Referral_HeightMetric1" style="display:inline-block;width:20px!important">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Weight</label>
                <div class="float-right">
                    <%= Html.TextBox("Weight", "", new { @id = "New_Referral_Weight", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", new { @id = "New_Referral_WeightMetric0", @class = "radio" })%>
                    <label for="New_Referral_WeightMetric0" style="display:inline-block;width:20px!important">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", new { @id = "New_Referral_WeightMetric1", @class = "radio" })%>
                    <label for="New_Referral_WeightMetric1" style="display:inline-block;width:20px!important">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="New_Referral_Assign" class="float-left">Assign to Clinician:</label>
                <div class="float-right"><%= Html.Clinicians("UserId", "", new { @id = "New_Referral_Assign", @class = "Users required valid" })%></div>
            </div>
            <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowPatientEligibility($('#New_Referral_MedicareNo').val(), $('#New_Referral_LastName').val(),$('#New_Referral_FirstName').val(),$('#New_Referral_DateOfBirth').val(),$('input[name=Gender]:checked').val());">Verify Medicare Eligibility</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Referral_MedicareNo" class="float-left">Medicare No:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", " ", new { @id = "New_Referral_MedicareNo", @maxlength = "20", @class = "text MedicareNo" })%></div>
            </div>
            <div class="row">
                <label for="New_Referral_MedicaidNo" class="float-left">Medicaid No:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", " ", new { @id = "New_Referral_MedicaidNo", @maxlength = "20", @class = "text MedicaidNo" })%></div>
            </div>
            <div class="row">
                <label for="New_Referral_SSN" class="float-left">SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", "", new { @id = "New_Referral_SSN", @maxlength = "9", @class = "numeric ssn" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Referral_AddressLine1", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Referral_AddressLine2", @maxlength = "50", @class = "text input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Referral_AddressCity", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right">
                    <%= Html.States("AddressStateCode", "", new { @id = "New_Referral_AddressStateCode", @class = "AddressStateCode requireddropdown valid" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "New_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_Referral_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right">
                    <input type="text" class="autotext numeric required phone_short" name="PhoneHomeArray" id="New_Referral_HomePhone1" maxlength="3" />
                    -
                    <input type="text" class="autotext numeric required phone_short" name="PhoneHomeArray" id="New_Referral_HomePhone2" maxlength="3" />
                    -
                    <input type="text" class="autotext numeric required phone_long" name="PhoneHomeArray" id="New_Referral_HomePhone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="New_Referral_Email" class="float-left">Email Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Referral_Email", @class = "text email input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label class="float-left">DNR:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsDNR", "true", false, new { @id = "New_Referral_IsDNR1", @class = "radio" })%>
                    <label for="New_Referral_IsDNR1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsDNR", "false", true, new { @id = "New_Referral_IsDNR2", @class = "radio" })%>
                    <label for="New_Referral_IsDNR2" class="inline-radio">No</label>
                </div>
            </div>
        </div>
    </fieldset>
    
    <fieldset>
        <legend>Services Required</legend>
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" value="0" class="radio float-left" id="ServicesRequiredCollection0" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection0" class="radio">SN</label>
                    </td><td>
                        <input type="checkbox" value="1" class="radio float-left" id="ServicesRequiredCollection1" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection1" class="radio">HHA</label>
                    </td><td>
                        <input type="checkbox" value="2" class="radio float-left" id="ServicesRequiredCollection2" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection2" class="radio">PT</label>
                    </td><td>
                        <input type="checkbox" value="3" class="radio float-left" id="ServicesRequiredCollection3" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection3" class="radio">OT</label>
                    </td><td>
                        <input type="checkbox" value="4" class="radio float-left" id="ServicesRequiredCollection4" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection4" class="radio">SP</label>
                    </td><td>
                        <input type="checkbox" value="5" class="radio float-left" id="ServicesRequiredCollection5" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection5" class="radio">MSW</label>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <%  var htmlAttributesPrimary = new Dictionary<string, string>(); %>
                <%  htmlAttributesPrimary.Add("id", "New_Patient_PrimaryInsurance"); %>
                <%  htmlAttributesPrimary.Add("class", "Insurances "); %>
                <label for="New_Patient_PrimaryInsurance" class="float-left">Primary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("PrimaryInsurance", "",Guid.Empty, true, htmlAttributesPrimary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_PrimaryInsuranceContent"></div>
            </div>
            <div class="row">
                <%  var htmlAttributesSecondary = new Dictionary<string, string>(); %>
                <%  htmlAttributesSecondary.Add("id", "New_Patient_SecondaryInsurance"); %>
                <%  htmlAttributesSecondary.Add("class", "Insurances"); %>
                <label for="New_Patient_SecondaryInsurance" class="float-left">Secondary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("SecondaryInsurance", "", Guid.Empty, true, htmlAttributesSecondary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_SecondaryInsuranceContent"></div>
            </div>
            <div class="row">
                <%  var htmlAttributesTertiary = new Dictionary<string, string>(); %>
                <%  htmlAttributesTertiary.Add("id", "New_Patient_TertiaryInsurance"); %>
                <%  htmlAttributesTertiary.Add("class", "Insurances"); %>
                <label for="New_Patient_TertiaryInsurance" class="float-left">Tertiary:</label>
                <div class="float-right"><%= Html.InsurancesByBranch("TertiaryInsurance", "", Guid.Empty, true, htmlAttributesTertiary)%></div>
                <div class="clear"></div>
                <div id="New_Patient_TertiaryInsuranceContent"></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>DME Needed</legend>
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" value="0" class="radio float-left" id="DMECollection0" name="DMECollection" />
                        <label for="DMECollection0" class="radio">Bedside Commode</label>
                    </td><td>
                        <input type="checkbox" value="1" class="radio float-left" id="DMECollection1" name="DMECollection" />
                        <label for="DMECollection1" class="radio">Cane</label>
                    </td><td>
                        <input type="checkbox" value="2" class="radio float-left" id="DMECollection2" name="DMECollection" />
                        <label for="DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </td>
                    </tr><tr>
                    <td>
                        <input type="checkbox" value="3" class="radio float-left" id="DMECollection3" name="DMECollection" />
                        <label for="DMECollection3" class="radio">Grab Bars</label>
                    </td><td>
                        <input type="checkbox" value="4" class="radio float-left" id="DMECollection4" name="DMECollection" />
                        <label for="DMECollection4" class="radio">Hospital Bed</label>
                    </td><td>
                        <input type="checkbox" value="5" class="radio float-left" id="DMECollection5" name="DMECollection" />
                        <label for="DMECollection5" class="radio">Nebulizer</label>
                    </td>
                    </tr><tr>
                    <td>
                        <input type="checkbox" value="6" class="radio float-left" id="DMECollection6" name="DMECollection" />
                        <label for="DMECollection6" class="radio">Oxygen</label>
                    </td><td>
                        <input type="checkbox" value="7" class="radio float-left" id="DMECollection7" name="DMECollection" />
                        <label for="DMECollection7" class="radio">Tub/Shower Bench</label>
                    </td><td>
                        <input type="checkbox" value="8" class="radio float-left" id="DMECollection8" name="DMECollection" />
                        <label for="DMECollection8" class="radio">Walker</label>
                    </td>
                    </tr><tr>
                    <td>
                        <input type="checkbox" value="9" class="radio float-left" id="DMECollection9" name="DMECollection" />
                        <label for="DMECollection9" class="radio">Wheelchair</label>
                    </td><td>
                        <input type="checkbox" value="10" id="New_Referral_DMEOther" class="radio float-left" name="DMECollection" />
                        <label for="New_Referral_DMEOther" class="radio ">Other</label>
                    </td><td>
                        <%= Html.TextBox("OtherDME", "", new { @id = "New_Referral_OtherDME", @class = "text", @style="display:none;", @maxlength="50"}) %>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <div class="clear" />
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_PhysicianDropDown1" class="float-left">Primary Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown1", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.AddPhysRow(this)">add new</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown2" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown2", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown3" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown3", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown4" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown4", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown5" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown5", @class = "Physicians" })%><a class="abs addrem" href="javascript:void(0);" onclick="Patient.RemPhysRow(this)">remove</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="">
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_EmergencyContactFirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.FirstName", "", new { @id = "New_Referral_EmergencyContactFirstName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_EmergencyContactLastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.LastName", "", new { @id = "New_Referral_EmergencyContactLastName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="New_Referral_EmergencyContactRelationship" class="float-left">Relationship:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.Relationship", "", new { @id = "New_Referral_EmergencyContactRelationship", @class = "text input_wrapper" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Referral_EmergencyContactPhonePrimary1" class="float-left">Primary Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Referral_EmergencyContactPhonePrimary1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Referral_EmergencyContactPhonePrimary2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhonePrimaryArray" id="New_Referral_EmergencyContactPhonePrimary3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Referral_EmergencyContactPhoneAlternate1" class="float-left">Alternate Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="New_Referral_EmergencyContactPhoneAlternate1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhoneAlternateArray" id="New_Referral_EmergencyContactPhoneAlternate2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhoneAlternateArray" id="New_Referral_EmergencyContactPhoneAlternate3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Referral_EmergencyContactEmail" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "New_Referral_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <%= Html.ToggleTemplates("New_Referral_CommentsTemplates", "", "#New_Referral_Comments")%>
            <div class="row">
                <p class="charsRemaining"></p>
                <textarea style="height:150px;" id="New_Referral_Comments" name="Comments" cols="5" rows="6" maxcharacters="2000"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Add Referral</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $("select.Physicians").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("select.ReferrerPhysician").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("select.Insurances").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('newinsurance');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("#New_Patient_LocationId").change(function() { Agency.LoadAgencyInsurances($(this).val(), "New_Patient_PrimaryInsurance"); Agency.LoadAgencyInsurances($(this).val(), "New_Patient_SecondaryInsurance"); Agency.LoadAgencyInsurances($(this).val(), "New_Patient_TertiaryInsurance"); });
    $("#New_Patient_PrimaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_PrimaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Primary'); } else { $("#New_Patient_PrimaryInsuranceContent").empty(); } });
    $("#New_Patient_SecondaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_SecondaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Secondary'); } else { $("#New_Patient_SecondaryInsuranceContent").empty(); } });
    $("#New_Patient_TertiaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { Patient.LoadInsuranceContent("#New_Patient_TertiaryInsuranceContent", '00000000-0000-0000-0000-000000000000', $(this).val(), 'New_Patient', 'Tertiary'); } else { $("#New_Patient_TertiaryInsuranceContent").empty(); } });
</script>
<% } %>
