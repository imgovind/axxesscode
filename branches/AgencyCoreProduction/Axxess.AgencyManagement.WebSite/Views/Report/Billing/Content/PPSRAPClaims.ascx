﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Claim>>" %>
<% string pagename = "PPSRAPClaims"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.LastName).Title("Last Name");
               columns.Bound(p => p.FirstName).Title("First Name");
               columns.Bound(p => p.MiddleInitial).Title("Middle Initial").Width(100);
               columns.Bound(p => p.DateRange).Title("Episode");
               columns.Bound(p => p.HippsCode).Title("HIPPS");
               columns.Bound(p => p.ProspectivePay).Format("${0:0.00}").Title("Amount");
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty}))
                                       .Sortable(sorting =>
                                                         sorting.SortMode(GridSortMode.SingleColumn)
                                                             .OrderBy(order =>
                                                             {
                                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                 if (sortName == "PatientIdNumber")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.PatientIdNumber).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.PatientIdNumber).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "LastName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.LastName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.LastName).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "FirstName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.FirstName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.FirstName).Descending();
                                                                     }
                                                                 }
                                                             })
                                                     )
                                   .Scrollable()
                                           .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PPSRAPClaimsContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
