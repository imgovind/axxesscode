﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ManagedClaimsByStatus"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient").Width(150);
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode").Width(160);
               columns.Bound(p => p.ClaimAmount).Sortable(false).Format("${0:0.00}").Title("Claim Amount").Width(100);
               columns.Bound(p => p.ClaimDate).Template(p => string.Format("{0}", p.ClaimDate > DateTime.MinValue ? p.ClaimDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Claim Date").Width(80);
               columns.Bound(p => p.PaymentAmount).Sortable(false).Format("${0:0.00}").Title("Payment Amount").Width(110);
               columns.Bound(p => p.PaymentDate).Template(p => string.Format("{0}", p.PaymentDate > DateTime.MinValue ? p.PaymentDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Payment Date").Width(80);
               columns.Bound(p => p.PrimaryInsuranceName).Title("Insurance").Width(100);
               
           })
                                 .Sortable(sorting =>
                                                      sorting.SortMode(GridSortMode.SingleColumn)
                                                          .OrderBy(order =>
                                                          {
                                                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                              if (sortName == "PatientIdNumber")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.PatientIdNumber).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.PatientIdNumber).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "DisplayName")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.DisplayName).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.DisplayName).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "PaymentDate")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.PaymentDate).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.PaymentDate).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "ClaimDate")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.ClaimDate).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.ClaimDate).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "PrimaryInsuranceName")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.PrimaryInsuranceName).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.PrimaryInsuranceName).Descending();
                                                                  }
                                                              }
                                                              

                                                          })
                                                  )
                           .Scrollable()
                                   .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var insurance = [], checkbox = $("input[name=PrimaryInsurance]:checked");
        checkbox.each(function() { insurance.push($(this).val()); });
        if (insurance == "") insurance.push(0);
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ManagedClaimsByStatusContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", Status : \"" + $('#<%= pagename %>_Status').val() + "\", InsuranceId : \"" + insurance.join() + "\",StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
