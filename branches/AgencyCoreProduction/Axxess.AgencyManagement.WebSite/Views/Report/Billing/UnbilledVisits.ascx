﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Unbilled Visits for Managed Claims Report</legend>
        <div class="column">
            <div class="row"><label for="UnbilledVisitsForManagedClaims_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "UnbilledVisitsForManagedClaims_BranchId" })%></div></div>
            <div class="row"><label for="UnbilledVisitsForManagedClaims_InsuranceId" class="float-left">Insurance:</label><div class="float-right"><%= Html.InsurancesNoneMedicare("InsuranceId", "", true, "-- Select an Insurance --", new { @id = "UnbilledVisitsForManagedClaims_InsuranceId" })%></div></div>
            <div class="row"><label for="UnbilledVisitsForManagedClaims_Status" class="float-left">Visit Status:</label><div class="float-right"><%= Html.BillableVisitStatus("Status", "", true, "All", "0", new { @id = "UnbilledVisitsForManagedClaims_Status" })%></div></div>
            <div class="row">
                <label for="UnbilledVisitsForManagedClaims_DateRange" class="float-left">Date Range:</label>
                <div class="float-right">
                    <input id="UnbilledVisitsForManagedClaims_StartDate" class="date-picker oe" value="<%= DateTime.Today.AddDays(-59).ToZeroFilled() %>"/>
                    <input id="UnbilledVisitsForManagedClaims_EndDate" class="date-picker oe" value="<%= DateTime.Today.ToZeroFilled() %>"/>
                </div>
            </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReportWithRangeInsuranceStatus('/Request/UnbilledVisitsForManagedClaimsReport', '#UnbilledVisitsForManagedClaims_BranchId', '#UnbilledVisitsForManagedClaims_InsuranceId', '#UnbilledVisitsForManagedClaims_Status', '#UnbilledVisitsForManagedClaims_StartDate', '#UnbilledVisitsForManagedClaims_EndDate');">Request Report</a></li></ul></div>
    </fieldset>
</div>
