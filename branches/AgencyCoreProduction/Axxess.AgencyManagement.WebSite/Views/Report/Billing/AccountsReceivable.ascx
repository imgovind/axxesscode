﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "AccountsReceivable"; %>
<div class="wrapper">
    <fieldset>
        <legend>Accounts Receivable</legend>
        <div class="column">
              <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
              <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.InsurancesMedicare("InsuranceId", "0", true, "All", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div><div class="row"><label  class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "All", Value = "All" }, new SelectListItem { Text = "RAP", Value = "RAP" }, new SelectListItem { Text = "Final", Value = "Final" }}, "Value", "Text", "All");%><%= Html.DropDownList("Type", billType, new { @id = pagename + "_Type", @class = "oe" })%></div> </div>
              <div class="row"><label class="float-left">Date Range:</label><div class="float-right">
              <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
           <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"if($('#{0}_StartDate').val() && $('#{0}_EndDate').val())Report.RebindReportGridContent('{0}','AccountsReceivableContent',{{ BranchCode: $('#{0}_BranchCode').val(),InsuranceId: $('#{0}_InsuranceId').val(),Type: $('#{0}_Type').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');else U.Growl('StartDate and EndDate is required','error')\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportAccountsReceivable", new { BranchCode = Guid.Empty, InsuranceId = 0, Type = "All", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
  <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/AccountsReceivable", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadMedicareWithHMOInsuarnceDropDown('<%= pagename %>', 'All', true, function() { }); }); 
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
</script>
