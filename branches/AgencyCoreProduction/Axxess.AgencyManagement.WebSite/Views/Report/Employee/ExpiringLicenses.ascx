﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<% string pagename = "EmployeeExpiringLicense"; %>
<div class="wrapper">
    <fieldset>
    <legend> Employee Expiring Licenses</legend>
        <div class="column">
          <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", Guid.Empty.ToString(),false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
          <div class="row"><label  class="float-left">License:</label><div class="float-right"><%= Html.LicenseTypes("LicenseTypes", Guid.Empty.ToString(), new { @id = pagename + "_LicenseTypes", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(60).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ExpiringLicenseContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), LicenseType:$('#{0}_LicenseTypes').val(), StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.ExportExpiredLicense('Report/ExportEmployeeExpiringLicense',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), LicenseType:$('#{0}_LicenseTypes').val(), StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }});\">Export To Excel</a>", pagename)%></li></ul></div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Employee/Content/ExpiringLicenses", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
