﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<% string pagename = "NonUserExpiringLicense"; %>
        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
             columns.Bound(l => l.DisplayName).Title("Employee Name");
             columns.Bound(l => l.LicenseType).Title("License Name").Sortable(false);
             columns.Bound(l => l.IssueDateFormatted).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Issue Date");
             columns.Bound(l => l.ExpireDateFormatted).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Expiration Date");
         })
                 //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty , Status=1}))
                     .Sortable(sorting =>
                                         sorting.SortMode(GridSortMode.SingleColumn)
                                             .OrderBy(order =>
                                             {
                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                 if (sortName == "DisplayName")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.DisplayName).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.DisplayName).Descending();
                                                     }
                                                 }
                                                 else if (sortName == "IssueDateFormatted")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.IssueDateFormatted).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.IssueDateFormatted).Descending();
                                                     }
                                                 }
                                                 else if (sortName == "ExpirationDateFormatted")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.ExpireDateFormatted).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.ExpireDateFormatted).Descending();
                                                     }
                                                 }

                                             })
                                     )
         .Scrollable()
         .Footer(false)%>
 
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','NonUserExpiringLicenseContent',{ LicenseType: \"" + $('#<%= pagename %>_LicenseTypes').val() + "\", StartDate: \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate: \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
