﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<% string pagename = "EmployeeExpiringLicense"; %>
        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
             columns.Bound(l => l.CustomId).Title("Custom Id");
             columns.Bound(l => l.UserDisplayName).Title("Employee");
             columns.Bound(l => l.LicenseType).Title("License Name").Sortable(false);
             columns.Bound(l => l.LicenseNumber).Title("License Number");
             columns.Bound(l => l.InitiationDateFormatted).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Initiation Date");
             columns.Bound(l => l.ExpirationDateFormatted).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Expiration Date");
         })
                 //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty , Status=1}))
                     .Sortable(sorting =>
                                         sorting.SortMode(GridSortMode.SingleColumn)
                                             .OrderBy(order =>
                                             {
                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                 if (sortName == "UserDisplayName")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.UserDisplayName).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.UserDisplayName).Descending();
                                                     }
                                                 }
                                                 else if (sortName == "InitiationDateFormatted")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.InitiationDateFormatted).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.InitiationDateFormatted).Descending();
                                                     }
                                                 }
                                                 else if (sortName == "ExpirationDateFormatted")
                                                 {
                                                     if (sortDirection == "ASC")
                                                     {
                                                         order.Add(o => o.ExpirationDateFormatted).Ascending();
                                                     }
                                                     else if (sortDirection == "DESC")
                                                     {
                                                         order.Add(o => o.ExpirationDateFormatted).Descending();
                                                     }
                                                 }

                                             })
                                     )
         .Scrollable()
         .Footer(false)%>
 
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ExpiringLicenseContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", LicenseType: \""+ $('#<%= pagename %>_LicenseTypes').val()+"\", StartDate: \""+$('#<%= pagename %>_StartDate').val() +"\", EndDate: \"" +$('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
