﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Birthday>>" %>
<% string pagename = "EmployeeBirthdayList"; %>
        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.Name).Width(150);
               columns.Bound(p => p.Age).Width(50);
               columns.Bound(p => p.BirthDay).Width(130);
               columns.Bound(p => p.AddressFirstRow);
               columns.Bound(p => p.AddressSecondRow);
               columns.Bound(p => p.PhoneHomeFormatted).Title("Home Phone").Width(110);
           })
           //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, month = DateTime.Now.Month }))
           .Sortable()
                   .Scrollable()
                           .Footer(false)
        %>
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','BirthdayListContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", Month : \"" + $('#<%= pagename %>_Month').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>