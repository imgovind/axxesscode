﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<% string pagename = "NonUserExpiringLicense"; %>
<div class="wrapper">
    <fieldset>
    <legend> Non User Expiring Licenses</legend>
        <div class="column">
          <div class="row"><label  class="float-left">License:</label><div class="float-right"><%= Html.LicenseTypes("LicenseTypes", Guid.Empty.ToString(), new { @id = pagename + "_LicenseTypes", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(60).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','NonUserExpiringLicenseContent',{{  LicenseType:$('#{0}_LicenseTypes').val(), StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.ExportNonUserExpiredLicense('Report/ExportNonUserExpiringLicense',{{  LicenseType:$('#{0}_LicenseTypes').val(), StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }});\">Export To Excel</a>", pagename)%></li></ul></div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Employee/Content/NonUserExpiringLicenses", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>



