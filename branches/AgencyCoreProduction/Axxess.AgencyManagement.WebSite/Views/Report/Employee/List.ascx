﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div class="wrapper">
    <fieldset>
        <legend>Employee List</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList( "BranchId", Guid.Empty.ToString(),false,"", new { @id = "EmployeeList_BranchId", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="EmployeeList_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
        </div> 
       <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReportWithStatus('/Request/EmployeeList', '#EmployeeList_BranchId', '#EmployeeList_StatusId');">Request Report</a></li></ul></div>
        
    </fieldset>
    
</div>

