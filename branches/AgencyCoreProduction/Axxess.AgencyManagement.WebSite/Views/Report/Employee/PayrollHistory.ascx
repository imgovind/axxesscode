﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PayrollDetailSummaryItem>>" %>
<% string pagename = "PayrollDetailSummary"; %>
<div class="wrapper">
    <fieldset>
        <legend>Payroll History</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", Guid.Empty.ToString(),false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
          <div class="row"><label  class="float-left">Employees:</label> <div class="float-right"><%= Html.UserWithBranchAndStatus("UserId", Guid.Empty.ToString(), "-- Select User--", Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input valid" })%></div> </div>
            <div class="row">
                <label  class="float-left">Date:</label>
                <div class="float-right">
                    <input type="text" class="date-picker shortdate" name="PayDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_PayDate" /> 
                    
                </div>
            </div>
            
            
        </div>
        <div class="column">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons">
                <ul>
                    <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','EmployeePayrollDetailSummaryContent',{{ BranchCode: $('#{0}_BranchCode').val(), EmployeeId: $('#{0}_UserCode').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val(), PayrollStatus: $('#{0}_PayrollStatus').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li>
                </ul>
            </div>
            <div class="buttons">
                <ul>
                    <li>
                        <a href="javascript:void(0);" onclick="Report.ExportPayrollSummary();">Export To Excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <%--<% Html.RenderPartial("Employee/Content/PayrollHistory", Model); %>--%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $("#<%= pagename %>_UserId").multiSelect({
        selectAll: true,
        noneSelected: '',
        oneOrMoreSelected: '*'
    });
    
 </script>