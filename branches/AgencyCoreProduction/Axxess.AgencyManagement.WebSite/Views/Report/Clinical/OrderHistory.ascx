﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PhysicianOrder>>" %>
<% string pagename = "ClinicalPhysicianOrderHistory"; %>
<div class="wrapper">
    <fieldset>
        <legend> Order History </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><%var status = new SelectList(new[] { new SelectListItem { Value = "000", Text = "All" }, new SelectListItem { Value = "100", Text = "Not Yet Started" }, new SelectListItem { Value = "105", Text = "Not Yet Due" }, new SelectListItem { Value = "110", Text = "Saved" }, new SelectListItem { Value = "115", Text = "Submitted (Pending QA Review)" }, new SelectListItem { Value = "120", Text = "Returned For Review" }, new SelectListItem { Value = "125", Text = "To Be Sent To Physician" }, new SelectListItem { Value = "130", Text = "Sent To Physician (Manually)" }, new SelectListItem { Value = "135", Text = "Returned W/ Physician Signature" }, new SelectListItem { Value = "140", Text = "Reopened" }, new SelectListItem { Value = "145", Text = "Sent To Physician (Electronically)" }, new SelectListItem { Value = "150", Text = "Saved By Physician" } }, "Value", "Text", 000);%><%= Html.DropDownList("StatusId", status, new  { @id =pagename+ "_StatusId" })%></div></div> 
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"> <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
         <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','OrderHistoryContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(),  StartDate: $('#{0}_StartDate').val(),  EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalPhysicianOrderHistory", new { BranchCode= Guid.Empty, StatusId = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Clinical/Content/OrderHistory", Model); %>
    </div>
</div>
