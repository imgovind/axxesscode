﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<OpenOasis>>" %>
<% string pagename = "ClinicalOpenOasis"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(e => e.PatientIdNumber).Title("MRN").Width(90);
               columns.Bound(e => e.PatientName).Title("Patient");
               columns.Bound(e => e.AssessmentName).Sortable(false).Title("Assessment");
               columns.Bound(e => e.Date).Width(80);
               columns.Bound(e => e.Status).Sortable(false);
               columns.Bound(e => e.CurrentlyAssigned).Title("Employee");
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOpenOasis", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now}))
.Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "PatientIdNumber")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientIdNumber).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientIdNumber).Descending();
                                      }

                                  }
                                  else if (sortName == "PatientName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientName).Descending();
                                      }

                                  }
                                  else if (sortName == "Date")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.Date).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.Date).Descending();
                                      }

                                  }
                                  else if (sortName == "CurrentlyAssigned")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.CurrentlyAssigned).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.CurrentlyAssigned).Descending();
                                      }

                                  }
                              })
                      )
                   .Scrollable()
                           .Footer(false)
        %>
    
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','OpenOasisContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>