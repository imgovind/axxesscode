﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeTherapyException>>" %>
<% string pagename = "ClinicalThirteenAndNineteenVisitException"; %>
<div class="wrapper">
    <fieldset>
        <legend> 13th And 19th Visit Exception </legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Patient:</label><div class="float-right"><%= Html.Patients("Patients", "", 1, "All", new { @id = pagename + "_Patient", @class = " report_input valid" }) %></div></div>
            <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
           <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ThirteenAndNineteenVisitExceptionContent',{{ BranchCode: $('#{0}_BranchCode').val(), PatientId: $('#{0}_Patient').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalThirteenAndNineteenVisitException", new { BranchCode = Guid.Empty, PatientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Clinical/Content/ThirteenAndNineteenVisitException", Model); %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>

