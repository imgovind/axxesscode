﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<int, List<ReportDescription>>>" %>
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessPatientReports)) { %>
<% var patientReports = Model.ContainsKey((int)ReportCategory.Patient) ? Model[(int)ReportCategory.Patient] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Patient.GetDescription()%></h5>
        </div>
       <ul> 
        <% foreach( var report in patientReports)
           { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>"><span class="tooltip"style=" display:none" >
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments%></p><br/><%}%></span><%= report.Title %></a></li> 
          
        <% } %>
        </ul>   
    </div>  
     <% } %>   

   
<%if (Current.HasRight(Permissions.AccessClinicalReports)) { %>
<% var patientReports = Model.ContainsKey((int)ReportCategory.Clinical) ? Model[(int)ReportCategory.Clinical] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Clinical.GetDescription()%></h5>
        </div>
       <ul> 
        <% foreach( var report in patientReports) { %>
            <li> 

				<a class="<%= report.IsOnclick ? "onclick-link" : "report-link" %>" href="<%= report.IsOnclick ? "javascript:void(0);" : report.Link %>" description="<%= report.Description %>" <%= report.IsOnclick ? "onclick=\"" + report.Link + "\"" : "" %>> <span class="tooltip" style=" display:none">
					<p><strong>Description: </strong> <%= report.Description %></p><br/>
					<p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
					<p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
					<%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %>
                </a>
            </li> 
            <% } %>
        </ul>   
    </div>  
     <% } %> 
     
     
      <%  if (Current.HasRight(Permissions.AccessPayrollDetailSummaryReports)){ %>
      <% var patientReport = Model.ContainsKey((int)ReportCategory.PayrollDetailSummary) ? Model[(int)ReportCategory.PayrollDetailSummary] : new List<ReportDescription>(); %>
             <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.PayrollDetailSummary.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach( var report in patientReport) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>" ><span class="tooltip" id = "Span1" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
            <%}%>  
</div> 
     
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessBillingReports)) { %>
<% var patientReports = Model.ContainsKey((int)ReportCategory.BillingFinancial) ? Model[(int)ReportCategory.BillingFinancial] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.BillingFinancial.GetDescription()%></h5>
        </div>
       <ul> 
        <% foreach( var report in patientReports) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>"><span class="tooltip"style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
                <% } %>
        </ul>   
    </div>  
     <% } %>   

 
<% if (Current.HasRight(Permissions.AccessScheduleReports)) { %>  
<% var patientReports = Model.ContainsKey((int)ReportCategory.Schedule) ? Model[(int)ReportCategory.Schedule] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Schedule.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach( var report in patientReports) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>"><span class="tooltip" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
     <% } %>   
 </div> 
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessStatisticalReports)) { %>
<% var patientReports = Model.ContainsKey((int)ReportCategory.Statistical) ? Model[(int)ReportCategory.Statistical] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Statistical.GetDescription()%></h5>
        </div>
       <ul> 
        <% foreach( var report in patientReports) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>"><span class="tooltip" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
    <% var patientReport = Model.ContainsKey((int)ReportCategory.AnnualSurveyReportMissouri) ? Model[(int)ReportCategory.AnnualSurveyReportMissouri] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.AnnualSurveyReportMissouri.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach( var report in patientReport) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>"><span class="tooltip" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
    <% var patientsReport = Model.ContainsKey((int)ReportCategory.AnnualUtilizationReportCalifornia) ? Model[(int)ReportCategory.AnnualUtilizationReportCalifornia] : new List<ReportDescription>(); %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.AnnualUtilizationReportCalifornia.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach( var report in patientsReport) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>" description ="<%= report.Description %>" ><span class="tooltip" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/></span><%}%><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
<% } %>

<% if (Current.HasRight(Permissions.AccessEmployeeReports)) { %>
<% var patientReports = Model.ContainsKey((int)ReportCategory.Employee) ? Model[(int)ReportCategory.Employee] : new List<ReportDescription>(); %>
      <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Employee.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach( var report in patientReports) { %>
            <li> <a class="report-link"  href= "<%= report.Link %>"  description ="<%= report.Description %>"><span class="tooltip" id = "Span7" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description %></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
                <%if(report.Comments.IsNotNullOrEmpty()){%><p><strong>Comments: </strong> <%= report.Comments %></p><br/><%}%></span><%= report.Title %></a></li> 
            <% } %>
        </ul>   
    </div>  
  <% } %> 
  
  
 <%--test start--%>
  <% var patientsReport2 = Model.ContainsKey((int)ReportCategory.Testing) ? Model[(int)ReportCategory.Testing] : new List<ReportDescription>(); %>
    <% if (patientsReport2.Count>0)
       {%>
    <div class="reports">
    
        <div class="reports-head">
            <h5><%=ReportCategory.Testing.GetDescription()%></h5>
        </div>
        <ul> 
        <% foreach (var report in patientsReport2)
           { %>
            <li> <a class="report-link"  href= "<%= report.Link %>"  description ="<%= report.Description %>"><span class="tooltip" id = "Span2" style=" display:none">
                <p><strong>Description: </strong> <%= report.Description%></p><br/>
                <p><strong>Parameters: </strong> <%= report.Parameters%></p><br/>
                <p><strong>Report Includes: </strong> <%= report.InformationInclude%></p><br/>
                <%if (report.Comments.IsNotNullOrEmpty())
                  {%><p><strong>Comments: </strong> <%= report.Comments%></p><br/><%}%></span><%= report.Title%></a></li> 
            <% } %>
        </ul>   
    </div> 
    <%} %> 
<%--test end--%>
  
  
</div>

    
