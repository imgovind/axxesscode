﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "SchedulePastDueVisitsByDiscipline"; %>
<div class="wrapper main">
    <fieldset>
        <legend> Past Due Visits By Discipline</legend>
             <div class="column">
                <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList(pagename + "_BranchCode", "",false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
                <div class="row"><label class="float-left">Discipline:</label><div class="float-right"><%  var discipline = new SelectList(new[]{new SelectListItem { Text = "Nursing", Value = "Nursing" },new SelectListItem { Text = "Physical Therapy", Value = "PT" },new SelectListItem { Text = "Speech Therapy", Value = "ST" },new SelectListItem { Text = "Occupational Therapy", Value = "OT" },new SelectListItem { Text = "Social Worker", Value = "MSW" },new SelectListItem { Text = "Home Health Aide", Value = "HHA"},new SelectListItem { Text = "Orders", Value = "Orders" }}, "Value", "Text", "Nursing");%><%= Html.DropDownList(pagename + "_Discipline", discipline, new { @id = pagename + "_Discipline" })%></div></div>
                <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
            </div>
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PastDueVisitsByDisciplineContent',{{ BranchCode: $('#{0}_BranchCode').val(), Discipline: $('#{0}_Discipline').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePastDueVisitsByDiscipline", new { BranchCode = Guid.Empty, Discipline = "Nursing", StartDate = DateTime.Now.AddDays(-60), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </fieldset>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/PastDueVisitsByDiscipline", Model); %>
    </div>
</div>
