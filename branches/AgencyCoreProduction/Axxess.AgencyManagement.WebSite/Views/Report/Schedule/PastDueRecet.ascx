﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "SchedulePastDueRecerts"; %>
<div class="wrapper main">
    <fieldset>
    <legend> Past Due Recertifications.</legend>
         <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"]!=null ?ViewData["ManLocationId"].ToString(): Guid.Empty.ToString(),false,"" , new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("InsuranceId",ViewData.ContainsKey("Payor") && ViewData["Payor"]!=null ?ViewData["Payor"].ToString():"0", new { @id = pagename + "_InsuranceId", @class = "Insurances" })%></div></div>
            <div class="row"><label class="float-left">Due Date From:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> </div></div>
        </div>
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PastDueRecertsContent',{{ BranchCode: $('#{0}_BranchCode').val(), StartDate: $('#{0}_StartDate').val(), InsuranceId: $('#{0}_InsuranceId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindExportReport('{0}','{1}');\">Export to Excel</a>", pagename, sortParams)%></li></ul></div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/PastDueRecet", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>','All',true); });
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
    function RebindExportReport(pagename, sortParams) {
        U.GetAttachment('Report/ExportSchedulePastDueRecerts', { BranchCode: $("#" + pagename + "_BranchCode").val(), InsuranceId: $("#" + pagename + "_InsuranceId").val(), StartDate: $("#" + pagename + "_StartDate").val()}, sortParams);
    }
 </script>
