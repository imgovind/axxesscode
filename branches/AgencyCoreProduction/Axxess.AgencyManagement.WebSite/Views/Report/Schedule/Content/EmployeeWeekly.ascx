﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "ScheduleEmployeeWeekly"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Title("Task").Sortable(false);
         columns.Bound(p => p.StatusName).Title("Status").Sortable(false);
         columns.Bound(p => p.ScheduleDate).Title("Schedule Date").Sortable(true).Width(120);
         columns.Bound(p => p.VisitDate).Title("Visit Date").Sortable(false).Width(110);
         })
                // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, userId = Guid.Empty, StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(7) }))
                                     .Sortable(sorting =>
                                                          sorting.SortMode(GridSortMode.SingleColumn)
                                                              .OrderBy(order =>
                                                              {
                                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                  if (sortName == "PatientName")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Descending();
                                                                      }

                                                                  }
                                                                  if (sortName == "ScheduleDate")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.ScheduleDate).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.ScheduleDate).Descending();
                                                                      }

                                                                  }

                                                              })
                                                      )
                                         .Scrollable()
                                                 .Footer(false)%>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeeWeeklyContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", UserId : \"" + $('#<%= pagename %>_UserId').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
