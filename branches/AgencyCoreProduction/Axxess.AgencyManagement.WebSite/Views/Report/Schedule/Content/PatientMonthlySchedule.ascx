﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "PatientMonthlySchedule"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
             .Columns(columns =>
                     {
                     columns.Bound(s => s.DisciplineTaskName).Title("Task");
                     columns.Bound(p => p.StatusName).Title("Status");
                     columns.Bound(p => p.EventDate).Title("Schedule Date").Width(100);
                     columns.Bound(p => p.VisitDate).Title("Visit Date").Width(80);
                     columns.Bound(s => s.UserName).Title("Employee");
                     })
                             //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty, month = DateTime.Now.Month, year = DateTime.Now.Year }))
                                  .Sortable(sorting =>
                                              sorting.SortMode(GridSortMode.SingleColumn)
                                                  .OrderBy(order =>
                                                  {
                                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                      if (sortName == "UserName")
                                                      {
                                                          if (sortDirection == "ASC")
                                                          {
                                                              order.Add(o => o.UserName).Ascending();
                                                          }
                                                          else if (sortDirection == "DESC")
                                                          {
                                                              order.Add(o => o.UserName).Descending();
                                                          }

                                                      }

                                                  })
                                          )
                                       .Scrollable()
                                               .Footer(false)%>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PatientMonthlyScheduleContent',{  PatientId : \"" + $('#<%= pagename %>_PatientId').val() + "\", Month : \"" + $('#<%= pagename %>_Month').val() + "\", Year : \"" + $('#<%= pagename %>_Year').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
