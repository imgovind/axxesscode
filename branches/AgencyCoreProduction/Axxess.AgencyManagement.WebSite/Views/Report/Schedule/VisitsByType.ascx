﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleVisitsByType"; %>
<div class="wrapper main">
    <fieldset>
         <legend> Visits By Type</legend>
         <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", "",false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
            <div class="row"><label  class="float-left">Type:</label><div class="float-right"><%= Html.AllDisciplineTaskList("Type", "", "-- Select Type --", new { @id = pagename + "_Type", @class = "report_input valid" })%></div></div>
            <div class="row"><label  class="float-left">Patient:</label><div class="float-right"><%= Html.Patients("Patients", "0", 1, "All", new { @id = pagename + "_Patient", @class = " report_input valid" }) %></div></div>
            <div class="row"><label  class="float-left">Assigned To:</label><div class="float-right"><%= Html.Users("Clinicians", "", "All", new { @id = pagename + "_Clinician", @class = " report_input valid" })%></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ScheduleVisitsByTypeContent',{{ BranchCode: $('#{0}_BranchCode').val(), PatientId: $('#{0}_Patient').val(), ClinicianId: $('#{0}_Clinician').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val(), Type: $('#{0}_Type').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleVisitsByType", new { BranchCode = Guid.Empty, PatientId = Guid.Empty, ClinicianId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now, Type = 0 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/VisitsByType", Model); %>
    </div>
</div>
