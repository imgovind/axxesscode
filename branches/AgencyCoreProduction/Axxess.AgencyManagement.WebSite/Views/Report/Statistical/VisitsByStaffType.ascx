﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Type Of Staff Report</legend>
        <div class="column">
            <div class="row">
                <label for="VisitsByStaffType_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "VisitsByStaffType_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="VisitsByStaffType_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "VisitsByStaffType_Year" }) %></div>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/VisitsByStaffTypeReport', '#VisitsByStaffType_BranchId', '#VisitsByStaffType_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
