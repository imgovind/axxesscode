﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patients Visits By Principal Diagnosis</legend>
        <div class="column">
            <div class="row">
                <label for="PatientsVisitsByPrincipalDiagnosis_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "PatientsVisitsByPrincipalDiagnosis_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="PatientsVisitsByPrincipalDiagnosis_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "PatientsVisitsByPrincipalDiagnosis_Year" }) %></div>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/PatientsVisitsByPrincipalDiagnosisReport', '#PatientsVisitsByPrincipalDiagnosis_BranchId', '#PatientsVisitsByPrincipalDiagnosis_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
