﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalUnduplicatedServiceCensusReport"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientId).Title("MRN");
               columns.Bound(p => p.PatientDisplayName).Title("Patient");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
               columns.Bound(p => p.PatientDischargeDate).Title("Discharge Date").Width(120);
               columns.Bound(p => p.PatientStatusName).Title("Current Status").Width(120);
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now.Date }))
                                        .Sortable(sorting =>
                                                  sorting.SortMode(GridSortMode.SingleColumn)
                                                      .OrderBy(order =>
                                                      {
                                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                          if (sortName == "PatientFirstName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PatientFirstName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PatientFirstName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "PatientLastName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PatientLastName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PatientLastName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "PatientSoC")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PatientSoC).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PatientSoC).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "PatientDischargeDate")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PatientDischargeDate).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PatientDischargeDate).Descending();
                                                              }

                                                          }
                                                      })
                                              )
                           .Scrollable()
                           .Footer(false)%>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','StatisticalUnduplicatedServiceCensusReportContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
