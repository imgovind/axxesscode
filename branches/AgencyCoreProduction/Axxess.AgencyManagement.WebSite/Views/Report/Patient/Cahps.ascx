﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("CahpsReport", "Export", FormMethod.Post)) { %>
    <div class="wrapper">
        <fieldset>
            <legend>CAHPS Report</legend>
            <div class="column">
                <div class="row">
                    <label for="Cahps_BranchId" class="float-left">Branch:</label>
                    <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "Cahps_BranchId" })%></div>
                </div>
                <div class="row">
                    <label for="Cahps_SampleMonth" class="float-left">Sample Month:</label>
                    <div class="float-right">
                        <select id="Cahps_SampleMonth" name="sampleMonth">
                            <option value="1" selected="selected">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label for="Cahps_SampleYear" class="float-left">Sample Year:</label>
                    <div class="float-right"><%= Html.SampleYear("sampleYear", new { @id = "Cahps_SampleYear" }) %></div>
                </div>
                <div class="row">
                    <label for="Cahps_PaymentSource" class="float-left">Payment Source:</label>
                    <div class="float-right">
                        <select id="Cahps_PaymentSource" name="paymentSources" multiple="multiple">
                            <option value="0">None; no charge for current services</option>
                            <option value="1">Medicare (traditional fee-for-service)</option>
                            <option value="2">Medicare (HMO/managed care)</option>
                            <option value="3">Medicaid (traditional fee-for-service)</option>
                            <option value="4">Medicaid (HMO/managed care)</option>
                            <option value="5">Workers' compensation</option>
                            <option value="6">Title programs (e.g., Title III,V, or XX)</option>
                            <option value="7">Other government (e.g., CHAMPUS,VA,etc)</option>
                            <option value="8">Private insurance</option>
                            <option value="9">Private HMO/ managed care</option>
                            <option value="10">Self-pay</option>
                            <option value="11">Unknown</option>
                            <option value="12">Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="buttons">
                <ul>
                    <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate Report</a></li>
                </ul>
            </div>
        </fieldset>
    </div>
<%} %>

<script type="text/javascript">
    $("#Cahps_PaymentSource").multiSelect({
        selectAll: false,
        noneSelected: '- Select Payment Source(s) -',
        oneOrMoreSelected: '*'
    });
</script>