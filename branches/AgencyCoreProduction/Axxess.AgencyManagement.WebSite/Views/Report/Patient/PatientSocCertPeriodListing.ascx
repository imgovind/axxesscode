﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSocCertPeriod>>" %>
<% string pagename = "PatientSocCertPeriodListing"; %>
<div class="wrapper">
<fieldset>
<legend> Patient SOC Cert Period Listing</legend>
     <div class="column">
        <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", Guid.Empty.ToString(),false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
        <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected >Active</option><option value="2">Discharged</option></select></div></div>
        <div class="row"><label  class="float-left">SOC Cert. Start Date:</label><div class="float-right">From : <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
    </div>
     <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
    <div class="buttons"><ul><li> <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientSocCertPeriodListingContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
    <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSocCertPeriodListing", new { BranchCode = Guid.Empty, StatusId = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid">
   <% Html.RenderPartial("Patient/Content/PatientSocCertPeriodListingContent", Model); %>
</div>
</div>
