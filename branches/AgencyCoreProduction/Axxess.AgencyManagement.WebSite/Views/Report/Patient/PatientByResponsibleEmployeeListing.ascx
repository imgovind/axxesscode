﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleEmployeeListing"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient By Responsible Employee Listing</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", Guid.Empty.ToString(),false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected="selected">Active</option><option value="2">Inactive</option></select></div></div>
            <div class="row"><label  class="float-left">Employee:</label><div class="float-right"><%= Html.UserWithBranchAndStatus("UserId", Guid.Empty.ToString(), "-- Select User--", Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input valid" })%></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li> <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientByResponsibleEmployeeListingContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), UserId: $('#{0}_UserId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByResponsibleEmployeeListing", new { BranchCode = Guid.Empty, UserId = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
  <div id="<%= pagename %>GridContainer" class="report-grid">
   <% Html.RenderPartial("Patient/Content/PatientByResponsibleEmployeeListingContent", Model); %>
</div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
 </script>