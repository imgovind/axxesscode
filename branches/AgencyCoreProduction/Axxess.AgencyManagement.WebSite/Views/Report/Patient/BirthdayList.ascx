﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Birthday>>" %>
<% string pagename = "PatientBirthdayList"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Birthday Listing</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Branch:</label>
                <div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Month:</label>
                <div class="float-right">
                    <%  var months = new SelectList(new[] {
                            new SelectListItem { Text = "Select Month", Value = "0" },
                            new SelectListItem { Text = "January", Value = "1" },
                            new SelectListItem { Text = "February", Value = "2" },
                            new SelectListItem { Text = "March", Value = "3" },
                            new SelectListItem { Text = "April", Value = "4" },
                            new SelectListItem { Text = "May", Value = "5" },
                            new SelectListItem { Text = "June", Value = "6" },
                            new SelectListItem { Text = "July", Value = "7" },
                            new SelectListItem { Text = "August", Value = "8" },
                            new SelectListItem { Text = "September", Value = "9" },
                            new SelectListItem { Text = "October", Value = "10" },
                            new SelectListItem { Text = "November", Value = "11" },
                            new SelectListItem { Text = "December", Value = "12" }
                        }, "Value", "Text", DateTime.Now.Month); %>
                    <%= Html.DropDownList(pagename + "_Month", months, new { @id = pagename + "_Month", @class = "oe" })%>
                </div>
            </div>
        </div>
        <%  var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="column">
            <div class="buttons">
                <ul>
                    <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientBirthdayListContent',{{ BranchCode: $('#{0}_BranchCode').val(), Month: $('#{0}_Month').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li>
                </ul>
                <ul>
                    <li><%= Html.ActionLink("Export to Excel", "ExportPatientBirthdayList", new { BranchCode = Guid.Empty, Month = DateTime.Now.Month }, new { id = pagename + "_ExportLink" })%></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Patient/Content/BirthdayListContent", Model); %>
    </div>
</div>