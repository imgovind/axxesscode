﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ReferralInfo>>" %>
<% string pagename = "ReferralLog"; %>
<div class="wrapper">
    <fieldset>
        <legend>Referral Log Report</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><%= Html.ReferralStatusList("BranchCode", "", new { @id = pagename + "_Status", @class = "PatientStatusDropDown" })%></div></div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="column">
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ReferralLogContent',{{ Status: $('#{0}_Status').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Report/ExportReferralList',{{ 'Status': $('#{0}_Status').val(), 'StartDate': $('#{0}_StartDate').val(), 'EndDate': $('#{0}_EndDate').val() }});\">Export to Excel</a>", pagename)%></li></ul></div>
            
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer"  class="report-grid">
       <% Html.RenderPartial("Patient/Content/ReferralContent", Model); %>
    </div>
</div>
