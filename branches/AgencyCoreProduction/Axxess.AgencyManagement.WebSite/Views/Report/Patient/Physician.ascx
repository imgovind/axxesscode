﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientByPhysicians"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient By Physician Listing</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Guid.Empty.ToString(), new { @id = pagename + "_PhysicianId", @class = "Physicians" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Patient Status:</label>
                <div class="float-right"><select id="PatientStatus" class="PatientStatusDropDown"><option value="0">All</option><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div>
            </div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PhysicianContent',{{ PhysicianId:  $('#{0}_PhysicianId').next().val() != '' ?  $('#{0}_PhysicianId').next().val() : '00000000-0000-0000-0000-000000000000' ,Type:$('#PatientStatus option:selected').val()}},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByPhysicians", new { PhysicianId = Guid.Empty, Type="0" }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
  <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Patient/Content/Physician", Model); %>
    </div>
</div>
<script type="text/javascript">

    $("#<%= pagename %>_PhysicianId").PhysicianInput();
</script>