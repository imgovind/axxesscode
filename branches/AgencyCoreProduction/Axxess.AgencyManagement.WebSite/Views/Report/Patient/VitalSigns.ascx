﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientVitalSigns"; %>
<div class="wrapper">
    <fieldset>
        <legend> Vital Signs Report </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.UserBranchList("BranchCode", Guid.Empty.ToString(),false,"", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid", @tabindex = "3" })%></div> </div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Patient:</label> <div class="float-right"><%= Html.PatientsWithBranchAndStatus("PatientId", Guid.Empty.ToString(), "-- Select Patient--", Guid.Empty, 1, new { @id = pagename + "_PatientId", @class = "report_input valid required", @tabindex = "1" })%></div> </div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="column">
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','VitalSignsContent',{{ PatientId: $('#{0}_PatientId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="GetVitalSigns()">Export to Excel</a></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
    <% Html.RenderPartial("Patient/Content/VitalSignsContent", Model); %>
    </div>
     
</div>
<script type="text/javascript">
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    function GetVitalSigns() {
        pId = $('#PatientVitalSigns_PatientId').val();
        startDate = $('#PatientVitalSigns_StartDate').val();
        endDate = $('#PatientVitalSigns_EndDate').val();
        U.GetAttachment("Report/VitalSigns", { PatientId: pId, StartDate: startDate, EndDate: endDate });
    }
</script>
