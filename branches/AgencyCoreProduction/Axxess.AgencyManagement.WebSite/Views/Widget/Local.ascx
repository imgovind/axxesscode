﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="digital-clock"></div>
<div id="weather-item"></div>
<div class="weather-feed">
    <div id="weather-city" class="weather-city"></div>
    <div id="weather-temp" class="weather-temp"></div>
    <div id="weather-desc" class="weather-desc"></div>
    <div id="weather-wind" class="weather-wind"></div>
    <div id="weather-range" class="weather-range"></div>
    <div id="weather-linkc" class="weather-link"><a id="weather-link" target="_blank">Full forecast</a></div>
</div>
<script type="text/javascript">
    $("#local-widget .widget-head h5").text(U.DisplayDate(new XDate(), true));
    $.simpleWeather({
        zipcode: '<%= Html.ZipCode() %>',
        unit: 'f',
        success: function(weather) {
            $("#weather-city").html(weather.city + ", " + weather.region);
            $("#weather-temp").html(weather.temp + "&#176; " + weather.units.temp);
            $("#weather-desc").html(weather.currently);
            $("#weather-range").html("<strong>High</strong>: " + weather.high + "&#176; " + weather.units.temp + " <strong>Low</strong>: " + weather.low + "&#176; " + weather.units.temp);
            $("#weather-wind").html("<strong>Wind</strong>: " + weather.wind.direction + " " + weather.wind.speed + " " + weather.units.speed);
            $("#weather-link").attr("href", weather.link);
            $("#weather-item").css("background-image", "url(" + weather.image + ")");
            $("#weather-item").addClass("weather-item");
        },
        error: function(error) {
            $("#weather-item").html("<p>" + error + "</p>");
        }
    });
    $('#digital-clock').jdigiclock();
</script>