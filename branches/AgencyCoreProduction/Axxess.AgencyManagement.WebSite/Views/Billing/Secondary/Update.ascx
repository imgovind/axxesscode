﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateSecondaryClaimStatus", "Billing", FormMethod.Post, new { @id = "updateSecondaryClaimForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <fieldset>
        <legend>Update Claim Information</legend>
        <div class="column">
            <div class="row">
                <label for="Status" class="float-left">Claim Status:</label>
                <div class="float-right">
                    <% var status = new SelectList(new[] {
                            new SelectListItem { Value = "3000", Text = "Claim Created" },
                            new SelectListItem { Value = "3005", Text = "Claim Submitted" },
                            new SelectListItem { Value = "3010", Text = "Claim Rejected" },
                            new SelectListItem { Value = "3015", Text = "Payment Pending" },
                            new SelectListItem { Value = "3020", Text = "Claim Accepted/Processing" },
                            new SelectListItem { Value = "3025", Text = "Claim With Errors" },
                            new SelectListItem { Value = "3030", Text = "Paid Claim" },
                            new SelectListItem { Value = "3035", Text = "Cancelled Claim" }
                        }, "Value", "Text", Model.Status); %>
                    <%= Html.DropDownList("Status", status)%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Billed Date:</label>
                <div class="float-right"><input type="text" name="ClaimDate" class="date-picker" value="<%= Model.ClaimDate <= DateTime.MinValue ? "" : Model.ClaimDate.ToShortDateString() %>" id="ClaimDate" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Payment" class="float-left">Payment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("Payment", Model.Payment > 0 ? Model.Payment.ToString() : "", new { @class = "text input_wrapper", @maxlength = "" })%></div>
            </div>
            <div class="row">
                <label for="PaymentDate" class="float-left">Payment Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="PaymentDate" value="<%= Model.PaymentDate.Date <= DateTime.MinValue.Date ? "" : Model.PaymentDate.ToShortDateString() %>" /></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment" class="strong">Comment:</label>
                <div><%= Html.TextArea("Comment", Model.Comment)%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>

