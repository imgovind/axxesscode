﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
<div class="one-half float-left" style="height:268px"> 
    <table width="100%">
        <thead>
            <tr>
                <th colspan="2" class="bigtext"><strong><%=Model.Type %></strong></th>
            </tr>
        </thead>
        <tbody>
<%  if (Model.Visible) { %>
                            <tr>
                                <td class="align-right">Patient Name:</td>
                                <td class="align-left strong"><%= Model.PatientName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient MRN:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientIdNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Medicare Number:&#160;</td>
                                <td class="align-left strong"><%= Model.MedicareNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Insurance/Payer:&#160;</td>
                                <td class="align-left strong"><%= Model.PayorName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">HIPPS:&#160;</td>
                                <td class="align-left strong"><%= Model.HIPPS %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Key:&#160;</td>
                                <td class="align-left strong"><%= Model.ClaimKey %></td>
                            </tr>
                            <tr>
                                <td class="align-right">HHRG (Grouper):&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.Hhrg %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Labor Portion:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.LaborAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Non-Labor:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.NonLaborAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Supply Reimbursement:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.NonRoutineSuppliesAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Episode Prospective Pay:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.TotalProspectiveAmount %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Prospective Pay:&#160;</td>
                                <td class="align-left strong"><%= Model.ProspectivePayment.ClaimAmount %></td>
                            </tr>
<%  } else { %>
                            <tr>
                                <td class="align-right">Patient Name:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientName %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient MRN:&#160;</td>
                                <td class="align-left strong"><%= Model.PatientIdNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Medicare Number:&#160;</td>
                                <td class="align-left strong"><%= Model.MedicareNumber %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Insurance/Payer:&#160;</td>
                                <td class="align-left strong"><%= Model.PayorName %></td>
                            </tr>
    <%  if (Model.Visible) { %>
                            <tr>
                                <td class="align-right">HIPPS:&#160;</td>
                                <td class="align-left strong"><%= Model.HIPPS %></td>
                            </tr>
                            <tr>
                                <td class="align-right">Claim Key:&#160;</td>
                                <td class="align-left strong"><%= Model.ClaimKey %></td>
                            </tr>
    <%  } %>
<%  } %>
        </tbody>
    </table>
</div>