﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillingHistoryViewData>" %>
<% if (Model.ClaimInfo != null) { %>
<div id="Billing_CenterContentClaimInfo" class="top">
    <% if(!Current.IsAgencyFrozen) { %>
    <div class="window-menu">
        <ul>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('RAP');">New RAP</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('Final');">New Final</a></li>
        </ul>
    </div>
    <% } %>
    <div id="billingHistoryClaimData"><% Html.RenderPartial("ClaimInfo", Model.ClaimInfo); %></div>
</div>
<div class="bottom" style="top: 220px;"><% Html.RenderPartial("HistoryActivity", Model.ClaimInfo.PatientId); %></div>   
<% } else { %> <div class="abs center">No Patient Data Found</div><% } %>