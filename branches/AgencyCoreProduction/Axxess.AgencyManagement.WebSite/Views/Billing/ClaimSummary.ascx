﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle"><%= Model.ClaimType.ToString() %> Claim Summary | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateBilling" + (Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty) }))%><% { %>
<%if (Model!=null && Model.Claims != null && Model.Claims.Count > 0){%>
<%} else {%><div class="row"><b>Select the claim you want to generate.</b></div><%} %>
<% if (Model != null){ %>
<% if ( Model!=null && Model.Claims != null && Model.Claims.Count > 0){%>
<div class="row">
    <table class="claim" width="100%">
        <thead>
          <tr><th colspan="4"><%=string.Format("{0} | {1}",Model.BranchName,Model.InsuranceName) %></th></tr>
          <tr><th>Patient Name</th><th>Medicare No</th><th>Episode Date</th><th>Claim Amount</th></tr>
        </thead>
        <tbody>
            <%int j = 1;%>
            <% foreach (var claim in Model.Claims){%>
            <% if (j % 2 == 0){%>
            <tr class="even"><%}
               else{ %><tr class="odd"><%} %>
                    <td><%=string.Format("<input type='hidden' id='ClaimSelected{0}' name='ClaimSelected' value='{0}' />", claim.Id)%> <%=j %>.&#160;&#160;&#160;<%=claim.DisplayName + "(" + claim.PatientIdNumber + ")"%></td>
                    <td><%=claim.MedicareNumber%></td>
                    <td align="center"><% =claim.DateRange%></td>
                    <td> <%=claim.ProspectivePay%></td>
                </tr><% j++;} %>
        </tbody>
    </table>
</div><%} %>
<div class="clear"></div><% } %>
<%if (Model != null && Model.Claims != null && Model.Claims.Count > 0)
  {%>
<% = Html.Hidden("StatusType","Submit") %>
<% = Html.Hidden("BranchId", Model.BranchId)%>
<% = Html.Hidden("PrimaryInsurance", Model.Insurance)%>
<% = Html.Hidden("Type", Model.ClaimType.ToString())%>
<div class="buttons">
    <ul class="">
    <% if (Model.IsElectronicSubmssion){ %>
        <li class="align-left"> <a href="javascript:void(0);" onclick="Billing.SubmitClaimDirectly('#generateBilling<%=Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty %>','<%=Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty %>');">Submit Electronically</a></li>
    <%} else{ %>
        <li class="align-left"> <a href="javascript:void(0);" onclick="Billing.LoadGeneratedClaim('#generateBilling<%=Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty %>');">Download Claim(s)</a></li>
    <%}%>
    </ul>
</div>
 <% if (!Model.IsElectronicSubmssion){ %>
 <div class="buttons">
    <ul>
        <li><a href="javascript:void(0);" onclick="Billing.UpdateStatus('#generateBilling<%=Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty %>','<%=Model.ClaimType.ToString().IsNotNullOrEmpty() ? Model.ClaimType.ToString().ToLower() : string.Empty %>');">Mark Claim(s) As Submitted</a></li>
    </ul>
</div><%} %>
<%}%><%}%>
