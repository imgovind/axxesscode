﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle">Create Managed Care/Other Insurances Claims | <%= Current.AgencyName %></span>
<% var titleCaseType = Model.ClaimType.ToString().ToTitleCase(); %>
<% var insuranceId = ViewData.ContainsKey("Insurance") && ViewData["Insurance"] != null && ViewData["Insurance"].ToString().IsInteger() ? ViewData["Insurance"].ToString().ToInteger() : 0; %>
<% var branchId = ViewData.ContainsKey("Branch") && ViewData["Branch"] != null && ViewData["Branch"].ToString().IsGuid() ? ViewData["Branch"].ToString().ToGuid() : Guid.Empty; %>
<div id="Billing_ManagedClaimCenterContentMain" class="main wrapper">
    <div class="trical">
         <span class="strong">Branch:</span>
         <%= Html.UserBranchList("BranchId", branchId.ToString(), false,"", new { @id = "Billing_ManagedClaimCenterBranchCode", @class = "report_input valid" })%> 
         <%= Html.InsurancesNoneMedicare("PrimaryInsurance", insuranceId.ToString(), false,"", new { @id = "Billing_ManagedClaimCenterPrimaryInsurance", @class = "Insurances requireddropdown" })%>
         <div class="buttons editeps">
            <ul>
                <li><a href="javascript:void(0);" onclick="ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());">Refresh</a></li>
                <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': $('#Billing_ManagedClaimCenterBranchCode').val(), 'insuranceid':$('#Billing_ManagedClaimCenterPrimaryInsurance').val(), 'parentSortType': 'branch','columnSortType':'', 'claimType': '<%=titleCaseType%>' });">Print</a></li>
            </ul>
        </div>
    </div>
    <% var bill = Model;
       bill.Insurance = insuranceId; 
       bill.BranchId = branchId; %>
   <div id="Billing_ManagedClaimCenterContent" style="min-height:200px;"><% Html.RenderPartial("Managed/ManagedGrid", Model); %></div>
</div>
<script type="text/javascript">
    $("#Billing_ManagedClaimCenterBranchCode").change(function() {ManagedBilling.ReLoadUnProcessedManagedClaim($(this).val(), $("#Billing_ManagedClaimCenterPrimaryInsurance").val())});
    $("#Billing_ManagedClaimCenterPrimaryInsurance").change(function() { ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $(this).val());});
</script>