﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
 <% Html.Telerik().Grid<PatientSelection>().Name("ManagedBillingSelectionGrid").Columns(columns =>
    {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name/MI");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManageInsurancePatient", "Patient", new { branchId = Guid.Empty, statusId = 1, name = string.Empty, insuranceId = 0 }))
    .Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events.OnDataBound("ManagedBilling.PatientListDataBound").OnDataBinding("ManagedBilling.PatientListDataBinding").OnRowSelect("ManagedBilling.OnPatientRowSelected")).Render(); %>
