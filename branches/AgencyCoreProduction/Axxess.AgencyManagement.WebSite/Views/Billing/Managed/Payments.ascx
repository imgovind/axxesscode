﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
 <% Html.Telerik().Grid<ManagedClaimPayment>().DataKeys(keys => { keys.Add(c => c.Id).RouteKey("Id"); })
        .Name("ManagedBillingPaymentsGrid").Columns(columns =>
    {
        columns.Bound(p => p.PayorName).Title("Payor");
        columns.Bound(p => p.Payment).Format("{0:C}").Width(200);
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(200);
        columns.Bound(p => p.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>");
        columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalUpdatePaymentManagedClaim('<#=Id#>');\">Update</a> | <a href=\"javascript:void(0);\" onclick=\"ManagedBilling.DeletePayment('<#=PatientId#>','<#=ClaimId#>','<#=Id#>');\">Delete</a>").Title("Actions").Width(200).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax()
        .Select("ManagedClaimPaymentsGrid", "Billing", new { claimId = Model })).ClientEvents(evnts => evnts.OnRowDataBound("ManagedBilling.OnRowWithCommentsDataBound"))
    .Sortable().Selectable().Scrollable().Footer(false).Render(); %>
<script type="text/javascript"> $("#ManagedBillingPaymentsGrid .t-grid-content").css({ 'height': 'auto' }); </script>

