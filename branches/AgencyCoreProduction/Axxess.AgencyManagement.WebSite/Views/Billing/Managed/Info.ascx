﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var patientStatus = new SelectList(new[] {
        new SelectListItem { Text = "Active", Value = "1" },
        new SelectListItem { Text = "Discharged", Value = "2" }
    }, "Value", "Text", Model.PatientStatus); %>
<%  var diagnosis = XElement.Parse(Model.DiagnosisCode); %>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  using (Html.BeginForm("ManagedInfoVerify", "Billing", FormMethod.Post, new { @id = "managedBillingInfo" })) { %>
<fieldset style="margin-top:20px;">
    <div class="column">
        <%= Html.Hidden("Id", Model.Id) %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "ManageInfo_PatientId" })%>
        <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO)%>
        <div class="row">
            <label for="Type" class="float-left">Bill Type:</label>
             <%
                 var items = new SelectList(
                 new [] {
                        new SelectListItem { Text="-- Select Bill Type --", Value="0"},
                        new SelectListItem { Value="321",Text="321--Home Health - Admit thru Discharge"},
                        new SelectListItem { Value="322",Text="322-Home Health - PPS RAP"},
                        new SelectListItem { Value="323",Text="323-Home Health - Interim-Continuing Claims "},
                        new SelectListItem { Value="327",Text="327-Home Health - Adjustment For Insurance"},
                        new SelectListItem { Value="329",Text="329-Home Health - PPS Final"},
                        new SelectListItem { Value="331",Text="331-Home Health - Admit thru Discharge(Not for PPS bills)"},
                        new SelectListItem { Value="332",Text="332-Home Health - 1st Claim(Not for PPS bills)"},
                        new SelectListItem { Value="333",Text="333-Home Health - Continuing Claim(Not for PPS bills)"},
                        new SelectListItem { Value="334",Text="334-Home Health - Last Claim(Not for PPS bills)"},
                        new SelectListItem { Value="341",Text="341-OPT - Admit thru Discharge"},
                        new SelectListItem { Value="342",Text="342 OPT - 1st Claim"},
                        new SelectListItem { Value="343",Text="343-OPT - Continuing Claim"},
                        new SelectListItem { Value="344",Text="344-OPT - Last Claim"},
                        new SelectListItem { Value="811",Text="811-Hospice - Admit thru Discharge"},
                        new SelectListItem { Value="812",Text="812-Hospice - 1st Claim"},
                        new SelectListItem { Value="813",Text="813-Hospice - Continuing Claim"},
                        new SelectListItem { Value="814",Text="814-Hospice - Last Claim"}
                 },"Value", "Text", Model.Type);
              %>
            <div class="float-right"><%=  Html.DropDownList("Type",items, new { @class = "text input_wrapper requireddropdown" }) %></div>
<%--            <div class="float-right"><%=  Html.DropDownList("Type", items.AsEnumerable(), new { @class = "text input_wrapper requireddropdown" }) Html.BillType("Type", Model.Type, new { @class = "text input_wrapper requireddropdown" })%></div>--%>
        </div>
        <div class="row">
            <label for="FirstName" class="float-left">Patient First Name:</label>
            <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "text input_wrapper required", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="LastName" class="float-left">Patient Last Name:</label>
            <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @class = "text input_wrapper required", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="MedicareNumber" class="float-left">Patient Insurance Id #:</label>
            <div class="float-right"><%= Html.TextBox("IsuranceIdNumber", Model.HealthPlanId, new { @class = "text input_wrapper required", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="PrimaryInsuranceId" class="float-left">Insurance:</label>
            <div class="float-right"><%= Html.InsurancesNoneMedicare("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), true, "-- Select Insurnace --", new { @id = "ManageInfo_PrimaryInsuranceId", @class = "requireddropdown" })%></div>
        </div>
        <div id="ManageInfo_InsuranceContent"><% Html.RenderPartial("~/Views/Billing/InsuranceInfoContent.ascx", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Relationship = Model.Relationship, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "ManageInfo" }); %></div>
        <div class="row">
            <label for="PatientIdNumber" class="float-left">Patient Record #:</label>
            <div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text input_wrapper required", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="Gender" class="float-left">Gender:</label>
            <div class="float-right">
                <%= Html.RadioButton("Gender", "Female",  Model.Gender == "Female" ? true : false, new { @id = "GenderF", @class = "radio" }) %>
                <label for="GenderF" class="inlineradio">Female</label>
                <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "GenderM", @class = "radio" }) %>
                <label for="GenderM" class="inlineradio">Male</label>
            </div>
        </div>
        <div class="row">
            <label for="DOB" class="float-left">Date of Birth:</label>
            <div class="float-right"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" /></div>
        </div>
        <div class="row">
            <label for="EpisodeStartDate" class="float-left">Start Date:</label>
            <div class="float-right"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="ManageInfo_EpisodeStartDate" /></div>
        </div>
        <div class="row">
            <label for="EpisodeEndDate" class="float-left">End Date:</label>
            <div class="float-right"><input type="text" class="date-picker required" name="EpisodeEndDate" value="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="ManageInfo_EpisodeEndDate" /></div>
        </div>
        <div class="row">
            <label for="StartOfCareDate" class="float-left">Admission Date:</label>
            <div class="float-right"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="StartOfCareDate" /></div>
        </div>
        <div class="row">
            <label for="AdmissionSource" class="float-left">Admission Source:</label>
            <div class="float-right"><%=Html.AdmissionSources("AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : string.Empty, new { @class = "AdmissionSource requireddropdown" })%></div>
        </div>
        <div class="row">
            <label for="PatientStatus" class="float-left">Patient Status:</label>
            <div class="float-right"><%= Html.UB4PatientStatus("UB4PatientStatus", Model.UB4PatientStatus, new { @id = "ManagedClaim_PatientStatus", @class = "requireddropdown" })%></div>
        </div>
        <div class="row" id="ManagedClaimPatientStatusRow" style="<%= Model.PatientStatus == 2 ? string.Empty : "display:none;" %>">
            <label for="DischargeDate" class="float-left">Discharge Date:</label>
            <div class="float-right"><input type="text" class="date-picker" name="DischargeDate" value="<%= Model.DischargeDate.ToShortDateString() %>" id="ManagedClaim_DischargeDate" /></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <%= Html.Hidden("AssessmentType", Model.AssessmentType, new { @id="ManagedClaim_AssessmentType" })%>
            <label for="HippsCode" class="float-left">HIPPS Code:</label>
            <div class="float-right"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @id="ManagedClaim_HippsCode", @class = "text", @maxlength = "5" }) %></div>
        </div>
        <div class="row">
            <label for="ClaimKey" class="float-left">OASIS Matching Key:</label>
            <div class="float-right"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @id="ManagedClaim_ClaimKey", @class = "text", @maxlength = "18" }) %></div>
        </div>
        <div class="row">
            <label for="FirstBillableVisitDate" class="float-left">First Billable Visit Date:</label>
            <div class="float-right">
    <%  if ( Model.FirstBillableVisitDate.Date > DateTime.MinValue.Date) { %>
                <input type="text" class="date-picker required" name="FirstBillableVisitDate" value="<%= Model.FirstBillableVisitDate.ToShortDateString() %>" id="FirstBillableVisitDate" />
    <%  } else { %>
                <input type="text" class="date-picker required" name="FirstBillableVisitDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="FirstBillableVisitDate" />
    <%  } %>
            </div>
        </div>
        <div class="row">
            <label for="PhysicianLastName" class="float-left">Primary Physician Last Name:</label>
            <div class="float-right"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20" }) %></div>
        </div>
        <div class="row">
            <label for="PhysicianFirstName" class="float-left">Primary Physician First Name:</label>
            <div class="float-right"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "text required", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="PhysicianNPI" class="float-left">Primary Physician NPI #:</label>
            <div class="float-right"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "10" }) %></div>
        </div>
        <div class="row">
            <label for="ProspectivePay" class="float-left">HIPPS Code Payment:</label>
            <div class="float-right"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @id="ManagedClaim_ProspectivePay", @class = "text", @maxlength = "20" })%></div>
        </div>
        
        <div class="row">
            <div class="strong">Diagnosis Codes:</div>
            <div class="margin">
                <label for="Primary" class="float-left">Primary</label>
                <div class="float-right"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : "", new { @id = "ManagedClaim_DiagnosisCode1" })%></div>
                <div class="clear"></div>
                <label for="Second" class="float-left">Second</label>
                <div class="float-right"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : "", new { @id = "ManagedClaim_DiagnosisCode2" })%></div>
                <div class="clear"></div>
                <label for="Third" class="float-left">Third</label>
                <div class="float-right"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : "", new { @id = "ManagedClaim_DiagnosisCode3" })%></div>
                <div class="clear"></div>
                <label for="Fourth" class="float-left">Fourth</label>
                <div class="float-right"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : "", new { @id = "ManagedClaim_DiagnosisCode4" })%></div>
                <div class="clear"></div>
                <label for="Fifth" class="float-left">Fifth</label>
                <div class="float-right"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : "", new { @id = "ManagedClaim_DiagnosisCode5" })%></div>
                <div class="clear"></div>
                <label for="Sixth" class="float-left">Sixth</label>
                <div class="float-right"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : "", new { @id = "ManagedClaim_DiagnosisCode6" })%></div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row">
            <label for="AddressLine1" class="float-left">Address Line 1:</label>
            <div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @class = "text required" }) %></div>
        </div>
        <div class="row">
            <label for="AddressLine2" class="float-left">Address Line 2:</label>
            <div class="float-right"><%= Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text" }) %></div>
        </div>
        <div class="row">
            <label for="AddressCity" class="float-left">City:</label>
            <div class="float-right"><%= Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required" }) %></div>
        </div>
        <div class="row">
            <label for="AddressStateCode" class="float-left">State, Zip Code:</label>
            <div class="float-right">
                <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @class = "input_wrapper AddressStateCode" }) %>
                <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip zip", @maxlength = "9" }) %>
            </div>
        </div>
        <div class="margin">
            <div class="row">
                <%= Html.CheckBox("IsHomeHealthServiceIncluded", Model.IsHomeHealthServiceIncluded, new { @id = "ManagedClaim_IsHomeHealthServiceIncluded", @class = "radio float-left" })%>
                Uncheck if the home health service line is not included.
            </div>
        </div>
        
    </div>
    <div class="clear"></div>
    <div class="wide-column">
        <label class="float-left">Condition Codes:</label>
        <table>
            <tbody>
                <tr>
                    <td>
                        <label class="float-left">18.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">19.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">20.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">21.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">22.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">23.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">24.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">25.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">26.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">27.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                    <td>
                        <label class="float-left">28.</label>
                        <div class="float-left"><%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="wide-column">
        <label for="RapRemark" class="float-left">Remark:</label>
        <%= Html.TextArea("Remark", Model.Remark, new { })%>
    </div>
</fieldset>
<div class="buttons">
    <ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
    </ul>
</div>
<%  } %>
<script type="text/javascript">
    U.BasicTabSetup($("#managedBillingInfo"));
    if ($("#ManagedClaim_PatientStatus").val() != '30' && $("#ManagedClaim_PatientStatus").val() != '0') $("#ManagedClaim_DischargeDate").addClass("required");
    ManagedBilling.Navigate(1, '#managedBillingInfo', $("#ManageInfo_PatientId").val());
    $("#ManagedClaim_PatientStatus").change(function() {
        if ($(this).val() != '30' && $(this).val() != '0') {
            $("#ManagedClaimPatientStatusRow").show();
            $("#ManagedClaim_DischargeDate").addClass("required");
        } else {
            $("#ManagedClaimPatientStatusRow").hide();
            $("#ManagedClaim_DischargeDate").removeClass("required");
        }
    }); 
    $("#ManageInfo_PrimaryInsuranceId").change(function() {
        Billing.LoadInsuranceContent("#ManageInfo_InsuranceContent", $("#ManageInfo_PatientId").val(), $(this).val(), $("#ManageInfo_EpisodeStartDate").val(), $("#ManageInfo_EpisodeEndDate").val(), "ManageInfo")
    });
<%  if (Model.HasMultipleEpisodes) { %>
    Acore.Modal({
        Name: "Found Multiple Episodes in your date range",
        Url: "Billing/ManagedClaimEpisodes",
        Input: { managedClaimId: "<%= Model.Id %>", patientId: "<%= Model.PatientId %>" },
        Width: "500px",
        Height: "200px",
        WindowFrame: false
    });
<%  } %>
</script>