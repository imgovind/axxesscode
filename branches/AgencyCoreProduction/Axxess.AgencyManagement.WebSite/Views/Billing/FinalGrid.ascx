﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% var contentId = string.Format("Billing_CenterContent{2}Final_{0}{1}", Model.BranchId, Model.Insurance, ViewData.ContainsKey("BillUIIdentifier") ? ViewData["BillUIIdentifier"].ToString() : string.Empty); %>
<div id="<%=contentId %>">
<div class="billing">
<input type="hidden" name="FinalSort" value="final-name" />
 <% var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
     <input type="hidden" name="BranchId" value="<%=Model.BranchId %>" />
     <input type="hidden" name="PrimaryInsurance" value="<%=Model.Insurance %>" />
     <input type="hidden" name="Type" value="Final" />
     <input type="hidden" name="FinalSort" value="final-name" />
    <input type="hidden" name="IsSortInvert" value="true" />
<ul>
    <li class="align-center">
            <%=string.Format("{0} | {1}", Model.BranchName, Model.InsuranceName)%>
             <% if (isDataExist)
                { %> [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsXls', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>','parentSortType': 'branch', 'columnSortType': $('input[name=FinalSort]').val(), 'claimType': 'Final', });" >Export to Excel</a> ]
            [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>','parentSortType': 'branch', 'columnSortType': $('input[name=FinalSort]').val(), 'claimType': 'Final', });" >Print</a> ]<%} %>
    </li>
    <li>
        <% if(!Current.IsAgencyFrozen) { %>
        <span class="final-checkbox"></span>
        <% } %>
        <span class="final-name pointer" onclick="Billing.Sort('<%=contentId %>','Final', 'final-name');">Patient Name</span>
        <span class="final-id pointer" onclick="Billing.Sort('<%=contentId %>','Final', 'final-id');">Patient ID/MR Number</span>
        <span class="final-episode pointer" onclick="Billing.Sort('<%=contentId %>','Final', 'final-episode');">Episode Period</span>
        <span class="finalicon">RAP</span>
        <span class="finalicon">Visit</span>
        <span class="finalicon">Order</span>
        <span class="finalicon">Verified</span>
    </li>
</ul>
<%if (isDataExist) {%>
<ol>
  <%  var finals = Model.Claims.Where(c =>c.EpisodeEndDate < DateTime.Now);
    int i = 1;
    foreach (var claim in finals)
    {
        var final = (FinalBill)claim;  %>
    <li class="<%= (i % 2 != 0 ? "odd " : "even ") + (final.IsRapGenerated ? "ready" : "notready") %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
        <% if(!Current.IsAgencyFrozen) { %>
        <span class="final-checkbox"><%= i %>. <%= (final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified && final.AreOrdersComplete && final.IsRapGenerated) ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + final.Id + "' id='FinalSelected" + final.Id + "' />" : string.Empty%></span>
        <% } %>
        <%= string.Format("<a class='float-right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/FinalPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\"><span class='img icon print'></span></a>", final.EpisodeId, final.PatientId) %>
        <% if(!Current.IsAgencyFrozen) { %>
        <a href='javascript:void(0);' onclick="<%= (final.IsRapGenerated) ? "UserInterface.ShowFinal('" + final.EpisodeId + "','" + final.PatientId + "');\" title=\"Final Ready" : "U.Growl('Error: RAP Not Generated', 'error');\" title=\"Not Complete" %>">
        <% } %>
            <span class="final-name"><%= final.LastName %>, <%= final.FirstName %></span>
            </a>
            <span class="final-id"><%= final.PatientIdNumber%></span>
            <span class="final-episode"><span class="very-hidden"><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= final.EpisodeEndDate != null ? "&#8211;" + final.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %></span>
            <span class="finalicon"><span class='img icon <%= final.IsRapGenerated ? "success-small" : "error-small" %>'></span></span>
            <span class="finalicon"><span class='img icon <%= final.AreVisitsComplete? "success-small" : "error-small" %>'></span></span>
            <span class="finalicon"><a href="javascript:void(0);" onclick="UserInterface.ShowEpisodeOrders('<%=final.EpisodeId %>','<%=final.PatientId %>');"><span class='img icon <%= final.AreOrdersComplete ? "success-small" : "error-small" %>'></span></a></span>
            <span class="finalicon"><span class='img icon <%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "success-small" : "error-small" %>'></span></span>
    </li>
    <%i++;}%>
    </ol>

<%} %>
</div>
<%  if (isDataExist && Model.Insurance > 0 && Current.HasRight(Permissions.GenerateClaimFiles) && !Current.IsAgencyFrozen)
    { %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#<%=contentId %>','claimSummary<%= Model.ClaimType.ToString().ToLower()%>');">Generate Selected</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.GenerateAllCompleted('#<%=contentId %>','claimSummary<%= Model.ClaimType.ToString().ToLower()%>');">Generate All Completed</a></li>
        </ul>
    </div>
<%  } %>
<script type="text/javascript">
    $("#<%=contentId %> ol li:first").addClass("first");
    $("#<%=contentId %> ol li:last").addClass("last");
</script>
</div>