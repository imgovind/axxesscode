﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Claim Submission History | <%= Current.AgencyName %></span>
<div class="main wrapper">
<div class="buttons float-right">
    <ul>
         <li><%= Html.ActionLink("Export to Excel", "SubmittedBatchClaims", "Export", new { ClaimType = "ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "SubmittedClaims_ExportLink", @class = "excel" })%></li>
    </ul>
</div>
    <fieldset class="orders-filter">
        <div class="buttons float-right">
            <ul><li><a href="javascript:void(0);" onclick="Billing.RebindSubmittedBatchClaims();">Generate</a></li></ul>
        </div>
        <label class="strong">
            Claim Type:
            <%= Html.ClaimTypes("ClaimType", new { @id = "SubmittedClaims_ClaimType" })%>
        </label>
        <div style="display:inline-block">
            <label class="strong">
                Date From:
                <input type="text" class="date-picker shortdate" style="width:80px" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="SubmittedClaims_StartDate" />
            </label>
            <label class="strong">
                To:
                <input type="text" class="date-picker shortdate" style="width:80px" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="SubmittedClaims_EndDate" />
            </label>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<ClaimDataLean>().Name("List_SubmittedClaims").Columns(columns => {
            columns.Bound(o => o.Id).Title("Batch Id").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.Created).Format("{0:MM/dd/yyyy}").Title("Submission Date").Width(120).Sortable(true).ReadOnly();
            columns.Bound(o => o.Count).Title("# of claims").Sortable(false).ReadOnly();
            columns.Bound(o => o.RAPCount).Title("# of RAPs").Sortable(false).ReadOnly();
            columns.Bound(o => o.FinalCount).Title("# of Finals").Sortable(false).ReadOnly();
            columns.Bound(o => o.ActionUrl).ClientTemplate("<#= ActionUrl #>").Title("Action").Width(155);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimSubmittedList", "Billing", new { ClaimType="ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
</div>
<script type="text/javascript">
    $("#List_SubmittedClaims .t-grid-content").css({ 'height': 'auto' });
    if (Acore.Mobile) $("#List_SubmittedClaims").css({ 'top': '95px' });
    else $("#List_SubmittedClaims").css({ 'top': '65px' });
    $("#window_submittedclaims_content").css({
        "background-color": "#d6e5f3"
    });
</script>