﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<span class="wintitle">Raw EDI Files | <%= Current.AgencyName %></span>
<iframe style="position:absolute;top:0;left:0;height:100%;width:100%" src="<%= string.Format(AppSettings.EdiFilesUrl+"?u={0}&a={1}", HttpUtility.UrlEncode(SessionStore.SessionId), Current.AgencyId) %>"></iframe>
 
