﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<span class="wintitle">Remittance Detail | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <% if(!Current.IsAgencyFrozen) { %>
            <li>
                <a href="javascript:void(0)" onclick="Billing.PostRemittance('<%=Model.Id %>', $('table.remittance .claim table.episodes'));">Post Selected</a>
            </li>
            <% } %>
            <li>
                <a href="javascript:void(0)" onclick="U.GetAttachment('Billing/RemittancePdf', { 'Id': '<%= Model.Id %>' })">Print</a>
            </li>
        </ul>
    </div>
<%  if (Model != null) { %>
    <%  var data = Model.RemittanceData ?? new RemittanceData(); %>
    <table class="remittance">
     <tr class="reminfo">
            <td style="width:20%;"><label class="float-left">Check Number:</label><label class="float-right"><%= data.CheckNo %></label></td>
            <td style="width:20%;"><label class="float-left">Payment Total:</label><label class="float-right"><%= String.Format("${0:#,0.00}", Model.PaymentAmount) %></label></td>
            <td style="width:20%;"><label class="float-left">Total Claims:</label><label class="float-right"><%= Model.TotalClaims %></label></td>
            <td style="width:20%;"><label class="float-left">Remittance Date:</label><label class="float-right"><%= Model.RemittanceDate > DateTime.MinValue ? Model.RemittanceDate.ToShortDateString() : string.Empty %></label></td>
            <td style="width:20%;"><label class="float-left">Payment Date:</label><label class="float-right"><%= Model.PaymentDate > DateTime.MinValue ? Model.PaymentDate.ToShortDateString() : string.Empty%></label></td>
        </tr>
        <tr class="payinfo">
            <td colspan="5" class="half-odd-table-hiddentd">
                <div class="float-left half-odd-table-header align-center">Payer</div>
                <div class="float-right half-odd-table-header align-center">Payee</div>
            </td>
        </tr>
        <tr class="payinfo" style="height: 100%">
            <td colspan="5" class="half-odd-table-hiddentd" style="height: 100%">
                <div class="float-left half-odd-table-content" style="height: 100%">
                    <table>
                        <tr><td><label class="float-right strong">Name:</label></td><td><%= data.Payer != null ? data.Payer.Name : string.Empty %></td></tr>
                            <%  if (data.Payer != null && data.Payer.RefType.IsNotNullOrEmpty()) { %>
                        <tr><td><label class="float-right strong"><%= data.Payer.RefType %>:</label></td><td><%= data.Payer.RefNum %></td></tr>
                            <%  } %>
                        <tr><td><label class="float-right strong">Address 1:</label></td><td><%= data.Payer != null ? data.Payer.Add1 : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">Address 2:</label></td><td><%= data.Payer != null ? data.Payer.Add2 : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">City:</label></td><td><%= data.Payer != null ? data.Payer.City : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">State:</label></td><td><%= data.Payer != null ? data.Payer.State : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">Zip:</label></td><td><%= data.Payer != null ? data.Payer.Zip : string.Empty %></td></tr>
                    </table>
                </div>
                <div class="float-right half-odd-table-content" style="height: 100%">
                    <table>
                        <tr><td><label class="float-right strong">Name:</label></td><td><%= data.Payee != null ? data.Payee.Name : string.Empty %></td></tr>
                        <%  if (data.Payee != null && data.Payee.RefType.IsNotNullOrEmpty()) { %>
                        <tr><td><label class="float-right strong"><%= data.Payee.RefType %>:</label></td><td><%= data.Payee.RefNum %></td></tr>
                        <%  } %>
                        <%  if (data.Payee != null && data.Payee.IdType.IsNotNullOrEmpty()) { %>
                        <tr><td><label class="float-right strong"><%= data.Payee.IdType %>:</label></td><td><%= data.Payee.Id %></td></tr>
                        <%  } %>
                        <tr><td><label class="float-right strong">Address 1:</label></td><td><%= data.Payee != null ? data.Payee.Add1 : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">Address 2:</label></td><td><%= data.Payee != null ? data.Payee.Add2 : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">City:</label></td><td><%= data.Payee != null ? data.Payee.City : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">State:</label></td><td><%= data.Payee != null ? data.Payee.State : string.Empty %></td></tr>
                        <tr><td><label class="float-right strong">Zip:</label></td><td><%= data.Payee != null ? data.Payee.Zip : string.Empty %></td></tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
   <div id="RemittanceDetailContainer"> <% Html.RenderPartial("~/Views/Billing/RemittanceDetailContent.ascx", Model); %> </div>
<%  } %>
</div>
