﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<span class="wintitle">Remittance Advices | <%= Current.AgencyName %></span>
<%  var pagename = "BillingRemittance";%>
<div class="wrapper grid-bg">
    <table style="width:100%">
        <tr>
            <td colspan="2">
                <div class="float-left">
                    <label class="float-left">Remittance Date From </label>
                    <div class="float-right">
                        <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                        <span class="bold">To</span>
                        <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
                    </div>
                </div>
                <div class="buttons float-left">
                    <ul>
                        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"U.RebindDataGridContent('{0}','Billing/RemittanceContent',{{ StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate</a>", pagename, null) %></li>
                        <li><a onclick="Acore.Open('EdiFiles');return false">View EdiFiles</a></li>
                       
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="float-left buttons">
                    <ul>
                        <li><%= Html.ActionLink("Print", "RemittancesPdf", "Billing", new { StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename+"_ExportLink", @target = "_blank" })%></li>
                    </ul>
                </div>
            </td>
            <td>
                <% if(!Current.IsAgencyFrozen) { %>
                    <div class="float-right">
                    <%  using (Html.BeginForm("RemittanceUpload", "Billing", FormMethod.Post, new { @id = "externalRemittanceUploadForm", @class = "align-right" })) { %>
                        <label class="strong">Select a Remittance File:</label>
                        <input id="Billing_ExternalRemittanceUpload" type="file" name="Upload" />
                        <div class="buttons float-right">
                            <ul>
                                <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit()">Upload</a></li>
                            </ul>
                        </div>
                    <%  } %>
                    </div>
                <%  } %>
            </td>
        </tr>
    </table>
    <div id="<%= pagename %>GridContainer" class="billing remlist"><%Html.RenderPartial("RemittanceContent", Model); %></div>
</div>
<script type="text/javascript">
    $("#BillingRemittance_Content ol li:first").addClass("first");
    $("#BillingRemittance_Content ol li:last").addClass("last");
</script>