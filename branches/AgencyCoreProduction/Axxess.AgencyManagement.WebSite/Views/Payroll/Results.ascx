﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollSummaryViewData>" %>
<%  if (Model != null && Model.VisitSummary != null && Model.VisitSummary.Count > 0) {  %>
<ul>
    <li class="align-center">
        Summary
        [ <a href="javascript:void(0);" onclick="Payroll.LoadDetails();">View Details</a> ]
        [ <%= Html.ActionLink("Export to Excel", "PayrollSummary", "Export", new { StartDate = Model.StartDate, EndDate = Model.EndDate, payrollStatus = Model.PayrollStatus }, new { id = "ExportPayrollSummary_ExportLink" }) %> ]
        [ <a href="javascript:void(0);" onclick="U.GetAttachment('Payroll/SummaryPdf', { 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });">Print PDF</a> ]
    </li>
    <li>
        <span class="payrolllastname strong" onclick="Payroll.Sort('payrollSearchResult', 'payrolllastname', event);">Last Name</span>
        <span class="payrollfirstname strong" onclick="Payroll.Sort('payrollSearchResult', 'payrollfirstname', event);">First Name</span>
        <span class="payrollcredential strong" onclick="Payroll.Sort('payrollSearchResult','payrollcredential',event);">Credential</span>
        <span class="payrollcount strong" onclick="Payroll.Sort('payrollSearchResult', 'payrollcount', event);">Count</span>
        <span class="payrollcount strong">Total Visit Time</span>
        <span class="payrollcount strong">Total Travel Time</span>
        <span class="payrollaction strong">Action</span>
    </li>
</ul>
<ol>
    <%  int i = 1; %>
    <%  foreach (var summary in Model.VisitSummary) { %>
	<li class="<%= i % 2 != 0 ? "odd " : "even " %>">
        <span class="payrolllastname"><%= summary.LastName.ToUpperCase()%></span>
        <span class="payrollfirstname"><% =summary.FirstName.ToUpperCase() %></span>
        <span class="payrollcredential"><%= summary.Credential.ToUpperCase() %></span>
        <span class="payrollcount"><%= summary.VisitCount.ToString() %></span>
        <span class="payrollcount"><%=string.Format(" {0} min =  {1:#0.00} hour(s)", summary.TotalVisitTime.ToString(), (double)summary.TotalVisitTime / 60)%></span>
        <span class="payrollcount"><%=string.Format(" {0} min =  {1:#0.00} hour(s)", summary.TotalTravelTime.ToString(), (double)summary.TotalTravelTime / 60)%></span>
        <span class="payrollaction"><%= summary.VisitCount > 0 ? "<a href='javascript:void(0);' onclick=\"Payroll.LoadDetail('" + summary.UserId.ToString() + "');\">Detail</a>" : ""%></span>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<script type="text/javascript">
    $(".payroll ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last");
    })
</script>
<%  } else { %>
<div class="strong align-center">
    <span class="img warning"></span>
    Your search yielded 0 results.
</div>
<%  } %>
<%= Html.Hidden("PayrollSearchType", "", new { @id="payrollSearchType" }) %>