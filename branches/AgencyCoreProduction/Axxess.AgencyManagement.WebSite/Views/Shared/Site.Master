﻿<%@ Master Language="C#" Inherits="System.Web.Mvc.ViewMasterPage" %>
<%@ Import Namespace="Axxess.Core.Infrastructure" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:ContentPlaceHolder ID="TitleContent" runat="server" />
    </title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("desktop.css")
        .Add("forms.css")
        .Add("Site.css")
        .Add("report.css")
        .Add("Message.css")
        .Add("grids.css")
        .Add("jquery-ui-1.7.1.custom.css")
        .Add("telerik.common.css")
        .Add("telerik.office2007.css")
        .Add("jquery.autocomplete.css")
        .Add("token-input-facebook.css")
        .Add("jquery.multiselect.css")
        .Add("tabs.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" /> 
    <link href="/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!--[if IE 9]><link rel="stylesheet" href="/Content/ie9.css" /><![endif]-->
    <!--[if IE 8]><link rel="stylesheet" href="/Content/ie8.css" /><![endif]-->
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <asp:ContentPlaceHolder ID="MainContent" runat="server"></asp:ContentPlaceHolder>
    
    <script type="text/javascript">function onEdit(e) { $(e.form).find('#MedicationType').data('tDropDownList').select(function(dataItem) { return dataItem.Text == e.dataItem['MedicationType']; }); } </script>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
                
            // jQuery
            .Add("jquery-1.6.2.min.js")

            // jQuery UI Plugins
            .Add("Plugins/jQueryUI/jquery.ui.core.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.widget.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.mouse.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.position.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.autocomplete.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.datepicker.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.dialog.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.draggable.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.droppable.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.resizable.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.selectable.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.sortable.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.tabs.min.js")

            // Other Plugins
            .Add("Plugins/Other/alphanumeric.min.js")
            .Add("Plugins/Other/autotab.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/dateformat.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/highcharts.js")
            .Add("Plugins/Other/hoverIntent.js")
            .Add("Plugins/Other/idletimer.min.js")
            .Add("Plugins/Other/idletimeout.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Plugins/Other/jdigiclock.min.js")
            .Add("Plugins/Other/jqplugin.min.js")
            .Add("Plugins/Other/layout.min.js")
            .Add("Plugins/Other/maskedinput.min.js")
            .Add("Plugins/Other/multiselect.js")
            .Add("Plugins/Other/maxlength.min.js")
            .Add("Plugins/Other/simpleweather.min.js")
            .Add("Plugins/Other/styleselect.min.js")
            .Add("Plugins/Other/superfish.js")
            .Add("Plugins/Other/tokeninput.min.js")
            .Add("Plugins/Other/tooltip.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/xdate.min.js")

            // Telerik Plugins
            .Add("Plugins/Telerik/telerik.common.min.js")
            .Add("Plugins/Telerik/telerik.calendar.min.js")
            .Add("Plugins/Telerik/telerik.combobox.min.js")
            .Add("Plugins/Telerik/telerik.datepicker.min.js")
            .Add("Plugins/Telerik/telerik.draganddrop.min.js")
            .Add("Plugins/Telerik/telerik.grid.js")
            .Add("Plugins/Telerik/telerik.grid.editing.min.js")
            .Add("Plugins/Telerik/telerik.grid.filtering.min.js")
            .Add("Plugins/Telerik/telerik.grid.grouping.min.js")
            .Add("Plugins/Telerik/telerik.list.min.js")
            .Add("Plugins/Telerik/telerik.menu.min.js")
            .Add("Plugins/Telerik/telerik.panelbar.min.js")
            .Add("Plugins/Telerik/telerik.tabstrip.min.js")
            .Add("Plugins/Telerik/telerik.textbox.min.js")
            
            // Custom Plugins
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Acore.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "AutoComplete.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "CurrentDate.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "DatePicker.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "DiagnosesList.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "FormElements.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Graph.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Menu.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "MultiGrid.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "RequestTracker.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "ReturnComments.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "TimePicker.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Validation.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Widgets.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "WoundChart.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Zebra.js")

            // Modules            
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "AdjustmentCode.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "UploadType.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Allergy.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Agency.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "CareTeam.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Billing.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Contact.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "GridProcessor.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Home.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Hospital.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "HospitalizationLog.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Pharmacy.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "IncidentInfectionReport.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Insurance.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Location.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Log.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Lookup.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Supply.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "ManagedBilling.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Medication.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Oasis.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "OasisValidation.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Patient.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "PatientCharts.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Payroll.js")
            .Add("modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Physician.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "PlanofCare.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Referral.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Report.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Schedule.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "ScheduleCenter.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "SchedulePrivateDuty.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "SecondaryBilling.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Template.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "NonVisitTask.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "NonVisitTaskManager.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "User.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "UserInterface.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Utility.js")
			
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "VisitNote.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "VitalSigns.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "WoundCare.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "LicenseManager.js")
            
            //Visit Note Modules
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Shared.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "HHA.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "MSW.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Nursing.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "PAS.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Therapy.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "UAP.js")
            .Add("Visits/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Verification.js")

            //Message Modules
            .Add("Message/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Main.js")
            .Add("Message/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Compose.js")
            .Add("Message/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Folder.js")
            .Add("Message/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Inbox.js")
                  
			//System Modules
			.Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "GridUtility.js")
			
			
			      
            .Compress(false)
            .Combined(false)
            .CacheDurationInDays(1)
            .Version(Current.AssemblyVersion)).OnDocumentReady(() => { %> window.CKEDITOR_BASEPATH = '/Scripts/Plugins/ckeditor/'; <%  }).Render(); %>
    <% if (Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) || Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer)) { %>
        <% Html.RenderPartial("LimitedMenu"); %>
    <% } else if (Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
        <% Html.RenderPartial("ReadOnlyMenu"); %>
    <% } else { %>
        <% Html.RenderPartial("Menu"); %>
    <% } %>
    
</body>
</html>
