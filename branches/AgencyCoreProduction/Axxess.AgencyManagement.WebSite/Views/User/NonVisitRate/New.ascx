﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("NonVisitRate/Add", "User", FormMethod.Post, new { @id = "NewUserNonVisitPayRate_Form" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "NewUserNonVisitPayRate_UserId" })%>
    <fieldset>
        <legend>New Task Pay Rate</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewUserNonVisitPayRate_Task" class="fl strong">Task</label>
                <div class="fr"><%= Html.AllNonVisitTaskList("Id", Guid.Empty.ToString(), Current.AgencyId, "--Select Task--", new { @id = "NonVisitTaskId", @class = "required valid" })%></div>
            </div>
            <div class="row">
                <label for="NewUserNonVisitPayRate_RateType" class="fl strong">Rate Type</label>
                <div class="fr"><%= Html.UserRateTypes("RateType", "0", new { @id = "NewUserPayRate_RateType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserNonVisitPayRate_VisitRate" class="fl strong">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", string.Empty, new { @id = "NewUserPayRate_VisitRate", @class = "floatnum required", @maxlength = "8" })%></div>
            </div>            
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>