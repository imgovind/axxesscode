<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("License/Add", "User", FormMethod.Post, new { @id = "NewUserLicense_Form" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "NewUserLicense_UserId" })%>
    <fieldset>
        <legend>New Licenses</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewUserLicense_Type" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LicenseTypes("LicenseType", "", new { @id = "NewUserLicense_Type", @class = "required notzero" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUserLicense_TypeMore">
                    <label for="NewUserLicense_TypeOther"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", "", new { @id = "NewUserLicense_TypeOther" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUserLicense_LicenseNumber" class="fl strong">License Number</label>
                <div class="fr"><input type="text" name="LicenseNumber" id="NewUserLicense_LicenseNumber" /></div>
            </div>
            <div class="row">
                <label for="NewUserLicense_InitDate" class="fl strong">Issue Date</label>
                <div class="fr"><input type="text" class="date-picker" name="InitiationDate" id="NewUserLicense_InitDate" /></div>
            </div>
            <div class="row">
                <label for="NewUserLicense_ExpDate" class="fl strong">Expiration Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpirationDate" id="NewUserLicense_ExpDate" /></div>
            </div>
            <div class="row">
                <label for="NewUserLicense_Attachment" class="fl strong">Attachment</label>
                <div class="fr"><input type="file" name="Attachment" id="NewUserLicense_Attachment" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
    <%  } %>
</div>