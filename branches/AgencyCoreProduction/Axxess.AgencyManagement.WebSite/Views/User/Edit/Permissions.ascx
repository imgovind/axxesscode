﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "EditUserPermissions_Form" })) { %>
    <%= Html.Hidden("UserId", Model.Id, new { @id = "EditUserPermissions_UserId" })%>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide-column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="EditUserPermissions_AllPermissions" type="checkbox" class="strong" value="" />
                        <label for="EditUserPermissions_AllPermissions">Select all Permissions</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Clerical</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Reporting</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>QA</legend>
        <div class="wide-column">
            <%= Html.PermissionList("QA", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl" style="clear:left;">
        <legend>Clinical</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Billing</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>OASIS</legend>
        <div class="wide-column">
            <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Administration</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Schedule Management</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>State Survey/Audit</legend>
        <div class="wide-column">
            <%= Html.PermissionList("State Survey/Audit", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>