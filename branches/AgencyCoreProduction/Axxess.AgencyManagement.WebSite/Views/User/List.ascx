﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyLocationLean>>" %>
<span class="wintitle">List Users | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg">
	<% if (Current.HasRight(Permissions.ExportListToExcel) || (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen)) { %>
	<div id="UserList_MainButtons" class="buttons fr">
		<ul class="float-right">
			<% if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen) { %>
			<li><a class="new-user">New User</a> </li>
			<% } %>
			<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
			<li><a class="export">Export to Excel</a> </li>
			<% } %>
		</ul>
	</div>
	<% } %>
	<fieldset class="orders-filter">
		<div class="buttons float-right">
			<ul>
				<li><a href="javascript:void(0);" onclick="User.RebindUsers();">Refresh</a></li>
			</ul>
		</div>
		<label class="strong">Branch:</label>
		<%= Html.GenericSelectList("BranchId", Model.First().Id.ToString(), "", Model, new { @id = "UserList_BranchCode" })%>
		<label class="strong">Status:</label>
		<select id="UserList_Status" name="Status" class="PatientStatusDropDown">
			<option value="1" selected>Active</option>
			<option value="2">Inactive</option>
		</select>
		<br />
		<div id="UserList_Search">
		</div>
	</fieldset>
	<%= Html.Telerik().Grid<UserJson>().Name("List_User").HtmlAttributes(new { @style = "top:75px;bottom:38px" })
			.Columns(columns => {
				columns.Bound(u => u.Id).ClientTemplate("<input name='UserId' type='checkbox' value='<#= Id #>'/>").Title("").Width(4).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
				columns.Bound(u => u.Name).Title("Name").Sortable(true).Width(20);
				columns.Bound(u => u.Title).Title("Title").Sortable(true).Width(15);
				columns.Bound(u => u.Email).ClientTemplate("<a href='mailto:<#=Email#>'><#=Email#></a>").Title("Email").Width(20).Sortable(true);
				columns.Bound(u => u.Phone).Title("Phone").ClientTemplate("<#=U.FormatPhone(Phone)#>").Sortable(false).Width(8);
				columns.Bound(u => u.Mobile).Title("Mobile").ClientTemplate("<#=U.FormatPhone(Mobile)#>").Sortable(false).Width(8);
				columns.Bound(u => u.EmploymentType).ClientTemplate("<#= EmploymentType == 1 ? 'Employee' : 'Contractor'#>").Sortable(true).Width(10);
				//columns.Bound(u => u.Status).Title("Status").ClientTemplate("<#= Status == 1 ? 'Active' : 'Inactive'#>").Sortable(false).Width(8);
				columns.Bound(u => u.Gender).Title("Gender").Width(6).Sortable(true);
				columns.Bound(u => u.Created).Format("{0:MM/dd/yyyy}").Title("Created").Width(8).Sortable(true);
				columns.Bound(u => u.Comments).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=Comments#></a>").Title("").Sortable(false).Width(3);
				columns.Bound(u => u.Id).Width(15).HtmlAttributes(new { @class = "action"}).Sortable(false).Visible(Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen).Title("Action");
			}).ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound").OnLoad("U.OnTGridClientLoad"))
                     .DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "User", new { BranchId = Model.First().Id, Status = (int)UserStatus.Active }))
			.Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(user => user.Add("Name")))
			.Footer(false).Scrollable() %>
	<div id="List_User_BottomPanel" style="position:absolute;right:0;bottom:0;left:0">
		<div class="buttons">
			<ul>
				<% if(!Current.IsAgencyFrozen) { %>
				<li>
					<a href='javascript:void(0);' class="toggle-status">Deactivate</a>
				</li>
				<li>
					<a href="javascript:void(0);" onclick='User.MultiDelete();'>Delete</a>
				</li>
				<% } %>
			</ul>
		</div>
	</div>
</div>