﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div id="NewUser_Content">
    <fieldset>
        <legend>Roles</legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="NewUser_Role_1" type="checkbox" class="radio required" name="AgencyRoleList" value="1" <%= Model.Roles.IsAgencyAdmin().ToChecked() %>/>
                        <label for="NewUser_Role_1">Administrator</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_2" type="checkbox" class="radio required" name="AgencyRoleList" value="2" <%= Model.Roles.IsDirectorOfNursing().ToChecked() %>/>
                        <label for="NewUser_Role_2">Director of Nursing</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_3" type="checkbox" class="radio required" name="AgencyRoleList" value="3" <%= Model.Roles.IsCaseManager().ToChecked() %>/>
                        <label for="NewUser_Role_3">Case Manager</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_4" type="checkbox" class="radio required" name="AgencyRoleList" value="4" <%= Model.Roles.IsNurse().ToChecked() %> />
                        <label for="NewUser_Role_4">Nursing</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_5" type="checkbox" class="radio required" name="AgencyRoleList" value="5" <%= Model.Roles.IsClerk().ToChecked() %>/>
                        <label for="NewUser_Role_5">Clerk (non-clinical)</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_6" type="checkbox" class="radio required" name="AgencyRoleList" value="6" <%= Model.Roles.IsPT().ToChecked() %>/>
                        <label for="NewUser_Role_6">Physical Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_7" type="checkbox" class="radio required" name="AgencyRoleList" value="7" <%= Model.Roles.IsOT().ToChecked() %>/>
                        <label for="NewUser_Role_7">Occupational Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_8" type="checkbox" class="radio required" name="AgencyRoleList" value="8" <%= Model.Roles.IsRole(AgencyRoles.SpeechTherapist).ToChecked() %>/>
                        <label for="NewUser_Role_8">Speech Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_9" type="checkbox" class="radio required" name="AgencyRoleList" value="9" <%= Model.Roles.IsRole(AgencyRoles.MedicalSocialWorker).ToChecked() %>/>
                        <label for="NewUser_Role_9">Medical Social Worker</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_10" type="checkbox" class="radio required" name="AgencyRoleList" value="10" <%= Model.Roles.IsHHA().ToChecked() %>/>
                        <label for="NewUser_Role_10">Home Health Aide</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_11" type="checkbox" class="radio required" name="AgencyRoleList" value="11" <%= Model.Roles.IsScheduler().ToChecked() %>/>
                        <label for="NewUser_Role_11">Scheduler</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_12" type="checkbox" class="radio required" name="AgencyRoleList" value="12" <%= Model.Roles.IsBiller().ToChecked() %>/>
                        <label for="NewUser_Role_12">Biller</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_13" type="checkbox" class="radio required" name="AgencyRoleList" value="13" <%= Model.Roles.IsQA().ToChecked() %>/>
                        <label for="NewUser_Role_13">Quality Assurance</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_14" type="checkbox" class="radio required" name="AgencyRoleList" value="14" <%= Model.Roles.IsRole(AgencyRoles.Physician).ToChecked() %>/>
                        <label for="NewUser_Role_14">Physician</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_15" type="checkbox" class="radio required" name="AgencyRoleList" value="15" <%= Model.Roles.IsOfficeManager().ToChecked() %>/>
                        <label for="NewUser_Role_15">Office Manager</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_16" type="checkbox" class="radio required" name="AgencyRoleList" value="16" <%= Model.Roles.IsCommunityLiason().ToChecked() %>/>
                        <label for="NewUser_Role_16">Community Liason Officer/Marketer</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_17" type="checkbox" class="radio required" name="AgencyRoleList" value="17" <%= Model.Roles.IsRole(AgencyRoles.ExternalReferralSource).ToChecked() %>/>
                        <label for="NewUser_Role_17">External Referral Source</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_18" type="checkbox" class="radio required" name="AgencyRoleList" value="18" <%= Model.Roles.IsRole(AgencyRoles.DriverAndTransportation).ToChecked() %>/>
                        <label for="NewUser_Role_18">Driver/Transportation</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_19" type="checkbox" class="radio required" name="AgencyRoleList" value="19" <%= Model.Roles.IsRole(AgencyRoles.OfficeStaff).ToChecked() %>/>
                        <label for="NewUser_Role_19">Office Staff</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_20" type="checkbox" class="radio required" name="AgencyRoleList" value="20" <%= Model.Roles.IsRole(AgencyRoles.Auditor).ToChecked() %>/>
                        <label for="NewUser_Role_20">State Surveyor/Auditor</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset <% if (Current.IsTestingAgency) { %>class="half float-left"<% } %>>
        <legend>Access &#38; Restrictions</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("AllowWeekendAccess", Model.AllowWeekendAccess, new { @id = "NewUser_AllowWeekendAccess" })%>
                        <label for="NewUser_AllowWeekendAccess" class="strong">Allow Weekend Access</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_EarliestLoginTime" class="float-left">Earliest Login Time:</label>
                <div class="float-right"><input type="text" size="10" id="NewUser_EarliestLoginTime" name="EarliestLoginTime" class="time-picker" value="<%= Model.EarliestLoginTime %>"/></div>
            </div>
            <div class="row">
                <label for="NewUser_AutomaticLogoutTime" class="float-left">Automatic Logout Time:</label>
                <div class="float-right"><input type="text" size="10" id="NewUser_AutomaticLogoutTime" name="AutomaticLogoutTime" class="time-picker"  value="<%= Model.AutomaticLogoutTime %>"/></div>
            </div>
        </div>
    </fieldset>
    <% if (Current.IsTestingAgency) { %>
    <fieldset class="half float-right">
        <legend>Trial Account</legend>
        <div class="column">
            <div class="row">
                <label for="NewUser_AccountExpireDate" class="float-left">Trial Expiration Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="AccountExpireDate"  id="NewUser_AccountExpireDate" /></div>
            </div>
        </div>
    </fieldset>
    <% } %>
    <div class="clear"></div>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide-column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="NewUser_AllPermissions" type="checkbox" class="strong" value="" />
                        <label for="NewUser_AllPermissions">Select all Permissions</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Clerical Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Reporting Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>QA Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("QA", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Clinical Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Billing Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>OASIS Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Administration Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Schedule Management Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>State Survey/Audit Permissions</legend>
        <div class="wide-column">
            <%= Html.PermissionList("State Survey/Audit", Model.PermissionsArray)%>
        </div>
    </fieldset>
    <div class="clear"></div>
</div>