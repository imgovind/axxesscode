﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<span class="wintitle">Edit Profile | <%= Model.DisplayName %></span>
<%  using (Html.BeginForm("Edit", "Profile", FormMethod.Post, new { @id = "editProfileForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Profile_Id" }) %>
<div class="form-wrapper">
    <fieldset class="half float-left">
        <legend>Login Password</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_CurrentPassword">Current Password:</label><div class="float-right"> <%= Html.Password("PasswordChanger.CurrentPassword", "", new { @id = "Edit_Profile_CurrentPassword", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewPassword">New Password:</label><div class="float-right"><%= Html.Password("PasswordChanger.NewPassword", "", new { @id = "Edit_Profile_NewPassword", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewPasswordConfirm">Confirm New Password:</label><div class="float-right"> <%= Html.Password("PasswordChanger.NewPasswordConfirm", "", new { @id = "Edit_Profile_NewPasswordConfirm", @class = "input_wrapper", @maxlength = "15" })%></div></div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_CurrentSignature">Current Signature:</label><div class="float-right"> <%= Html.Password("SignatureChanger.CurrentSignature", "", new { @id = "Edit_Profile_CurrentSignature", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewSignature">New Signature:</label><div class="float-right"><%= Html.Password("SignatureChanger.NewSignature", "", new { @id = "Edit_Profile_NewSignature", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewSignatureConfirm">Confirm New Signature:</label><div class="float-right"> <%= Html.Password("SignatureChanger.NewSignatureConfirm", "", new { @id = "Edit_Profile_NewSignatureConfirm", @class = "input_wrapper", @maxlength = "15" })%></div></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "Edit_Profile_AddressLine1", @maxlength = "75", @class = "text input_wrapper" }) %></div></div>
            <div class="row"><label for="Edit_Profile_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "Edit_Profile_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Profile_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_Profile_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Profile_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.States("Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_Profile_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_Profile_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "9" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Profile_PhonePrimary1">Home Phone:</label><div class="float-right"><%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 3 ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Profile_PhonePrimary1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 6 ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Profile_PhonePrimary2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 9 ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Profile_PhonePrimary3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Profile_PhoneAlternate1">Mobile Phone:</label><div class="float-right"><%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 3 ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Profile_PhoneAlternate1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 6 ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Profile_PhoneAlternate2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length > 9 ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Profile_PhoneAlternate3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editprofile');">Close</a></li>
    </ul></div>
</div>
<% } %>