﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<span class="wintitle">Deleted Users | <%= Current.AgencyName %></span>
<div  id ="DeletedUsers-GridContainer" class="wrapper">
<% var restoreAction = string.Empty;
   var users = new List<Guid>();
   var visible = false;
   if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen)
   {
       visible = true;
       restoreAction = "<a href=\"javascript:void(0);\" onclick=\"User.Restore('<#=Id#>')\">Restore</a>";
   }    
 %>
 <%= Html.Telerik().Grid(Model).Name("List_DeletedUser")
    .HtmlAttributes(new {@style ="top:0px;bottom:35px"})
    .ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.Id).Template(u => string.Format("<input name='UserId' type='checkbox' value='{0}'/>", u.Id)).Title("").Width(4).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20).Sortable(true); ;
    columns.Bound(u => u.DisplayTitle).Title("Title").Sortable(true).Width(15).Sortable(true); ;
    columns.Bound(u => u.EmailAddress).Template(u => string.Format("<a href='mailto:{0}'>{0}</a>", u.EmailAddress)).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(25).Sortable(true);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(8);
    columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(8);
    columns.Bound(u => u.EmploymentType).Sortable(true).Width(10);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(8);
    columns.Bound(u => u.Profile.Gender).Title("Gender").Width(6).Sortable(true);
    columns.Bound(u => u.Created).Format("{0:MM/dd/yyyy}").Title("Created").Width(8).Sortable(true);
    columns.Bound(u => u.Comments).Template(u => string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\">{0}</a>", u.Comments)).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=Comments#></a>").Title("").Sortable(false).Width(3);
    columns.Bound(u => u.Id).Width(10).Sortable(false).Template(u => visible ? string.Format("<a href=\"javascript:void(0);\" onclick=\"User.Restore('{0}')\">Restore</a>", u.Id) : string.Empty).ClientTemplate(restoreAction).Visible(visible).Title("Action");
    }).ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound"))
      .DataBinding(dataBinding => dataBinding.Ajax().Select("DeletedUserGrid", "User"))
                          .Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(user =>
                           {
                               var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                               var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                               if (sortName == "DisplayName")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.DisplayName).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.DisplayName).Descending();
                               }
                               else if (sortName == "DisplayTitle")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.DisplayTitle).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.DisplayTitle).Descending();
                               }
                               else if (sortName == "EmailAddress")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.EmailAddress).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.EmailAddress).Descending();
                               }
                               else if (sortName == "EmploymentType")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.EmploymentType).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.EmploymentType).Descending();
                               }
                               else if (sortName == "Gender")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.Profile.Gender).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.Profile.Gender).Descending();
                               }
                               else if (sortName == "Created")
                               {
                                   if (sortDirection == "ASC") user.Add(u => u.Created).Ascending();
                                   else if (sortDirection == "DESC") user.Add(u => u.Created).Descending();
                               }
                           }))
 .Sortable().Footer(false)
 .Scrollable(scrolling => scrolling.Enabled(true))%>

<div id="DeleteMultiple_BottomPanel" style="position:absolute;right:0;bottom:0;left:0;background-color:#C3D8F1;">
<div class="buttons">
    <% if(!Current.IsAgencyFrozen) { %>
    <ul>
        <li><a href="javascript:void(0);" onclick='User.MultiRestore();'>Restore</a></li>
        <li><a href="javascript:void(0);" class="close">Exit</a></li>
    </ul>
    <% } %>
    </div> 
 </div> 
 </div> 
<script type="text/javascript">
    $("#window_DeletedUser_content").css({"background-color": "#C3D8F1"});
    $("#List_DeletedUser .t-grid-toolbar").html("");
    $("#List_DeletedUser .t-grid-content").css({ 'height': 'auto','top': '60px','background-color': '#FFFFFF'});    

   deleteExport = "";

<% var deleteExport = string.Empty; %>

<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    deleteExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22U.GetAttachment('Export/DeleteUsers', {});%22%3EExcel Export (Deleted)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_DeletedUser .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EDeleted Users%3C/div%3E%3Cdiv id=%22DeletedUser%22%3E%3C/div%3E" + deleteExport + "%3C/div%3E"));
 $("#DeletedUser_Search").append($("<div/>").GridSearchById("#List_DeletedUser"));
   $(".grid-search").css("margin-left","-370px");
 $("#List_DeletedUser div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "User.RebindDeletedUser('" + U.ParameterByName(link, 'List_DeletedUser-orderBy') + "');");
   });
    Agency.ToolTip("#List_DeletedUser");
</script>
