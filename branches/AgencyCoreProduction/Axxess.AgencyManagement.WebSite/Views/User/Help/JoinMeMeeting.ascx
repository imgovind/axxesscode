﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<div class="wrapper main">
    <fieldset class="half center">
        <legend>Join.Me</legend>
        <div class="column">
            <div class="row">
                <label for="MeetingId" class="float-left">Meeting ID</label>
                <div class="fr"><input type="text" name="MeetingId" id="Joinme_MeetingId" class="shorter" maxlength="20" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="JoinMeMeeting_Button" target="_blank">Join</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(".numeric").numeric();
    $("#Joinme_MeetingId").blur(function() {
        var href = "https://www.join.me/" + $("#Joinme_MeetingId").val();
        $("#JoinMeMeeting_Button").attr("href", href);
    })
</script>