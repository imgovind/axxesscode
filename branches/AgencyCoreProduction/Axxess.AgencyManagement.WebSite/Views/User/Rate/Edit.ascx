﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserRate>" %>
<div class="wrapper main">
<% using (Html.BeginForm("Rate/Update", "User", FormMethod.Post, new { @id = "EditUserPayRate_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUserPayRate_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserPayRate_UserId" })%>
    <fieldset>
        <legend>Edit Pay Rate</legend>
        <div class="wide_column">
        <% if (Model.Insurance.IsNullOrEmpty() && Model.RateType == 0) { %>
            <div class="row">
                <label for="EditUserPayRate_Insurance" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", Model.Insurance, false, new { @id = "EditUserPayRate_Insurance", @class = "Insurances required notzero" })%></div>
            </div>
        <% } else { %>
            <div class="row">
                <label class="fl strong">Insurance</label>
                <div class="fr"><%= Model.InsuranceName %></div>
                <%= Html.Hidden("Insurance", Model.Insurance, new { @id = "EditUserPayRate_Insurance" })%>
            </div>
        <% } %>
            <div class="row">
                <label class="fl strong">Task</label>
                <div class="fr"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_RateType" class="fl strong">Rate Type</label>
                <div class="fr"><%=Html.UserRateTypes("RateType", "0", new { @id = "EditUserPayRate_RateType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_VisitRate" class="fl strong">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", Model.Rate, new { @id = "EditUserPayRate_VisitRate", @class = "required floatnum", @maxlength = "8" })%></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_MileageRate" class="fl strong">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", Model.MileageRate, new { @id = "EditUserPayRate_MileageRate", @class = "floatnum", @maxlength = "8" })%></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>