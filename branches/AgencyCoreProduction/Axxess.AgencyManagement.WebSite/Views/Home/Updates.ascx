﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Recent Software Updates</span>
<div id="softwareUpdates" class="standard-chart">
    <ul>
        <li>
            <span class="recentchangesdate">Date</span>
            <span class="recentchangescomments">Comments</span>
        </li>
    </ul>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">12/12/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong> Dashboard->Upcoming Recertification Tile->More: Added ability to specify date range. Selection of ALL displays the primary insurance in the results.
                <br /><strong>Enhancement: </strong> Visit Note Signature Date: Alert message displays when signature date is either prior to  visit date or beyond the episode date range.
                <br /><strong>Enhancement: </strong> Admin->Payroll Summary: Added Employee Credentials to the payroll Summary list. 
                <br /><strong>Enhancement: </strong> Admin-> Task Manager: screen redesign with the ability to add, edit and delete individual tasks.
                <br /><strong>Enhancement: </strong> Billing->Medicare/Medicare HMO->Create Final Claims->Step 3 of 4 Verify Supplies: Add ability view “Deleted Supplies” and restore as needed. 
                <br /><strong>Enhancement: </strong> Create->New Insurance/View->List Insurances: Invoice type: HCFA1500: Added the functionality to modify field locator 31.  Providing the option to choose in the drop down the default “signature on file” or select “Other” to be able to add different data and include a  Date to that locator if required by a specific insurance.
                <br /><strong>Feature: </strong> Admin->New->Users/ Admin->Lists-> Users-> Edit:  Ability to enter Date of Hire and Date of Termination to user profile.
                <br /><strong>Feature: </strong> Patient->Patient Chart: Patient Profile: add patient mobile phone to the profile as a secondary contact number.
                <br /><strong>Feature: </strong> SN pediatric Assessment: Ability to view primary and secondary diagnosis generated from patient Assessment. 
                <br /><strong>Feature: </strong> Added Oasis Recertification (ST)
                <br /><strong>Feature: </strong> Added secondary pharmacy and Fax number in the patient profile to generate to the medication profile and the patient profile
                <br /><strong>Feature: </strong> Create->New Insurance/View->List Insurances: UB04 Print Option: Allows ability to print claim information on a pre-print UB-04 form. 
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">11/22/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Autosave:  Option presents at the bottom of the document frame once it has been open for a minute.  The default is (Autosave On).   Users may select to turn the autosave off/on by clicking the Autosave On/Off.
                <br /><strong>Enhancement: </strong>Pending Patients:  Ability to view pending patients via Patients->Patient Charts->View: Pending.  Allows users to add orders, communication notes, authorizations, as well as, upload documents for a patient prior to selecting to admit. Users do not have the ability to schedule to pending patients.
                <br /><strong>Enhancement: </strong>Patient->Patient Charts->Re-Admit now mimics the Patient->Pending Admissions->Admit Functionality.  Re-Admit Creates:  episode, schedules SOC OASIS and F2F.
                <br /><strong>Feature: </strong>Create/Edit Insurance:  HCFA 1500: Locator 31. Default value is Signature on File. If an insurance company requires another value in place of Signature on File users may update the field at the insurance level.  For values other than Signature of File the date field in locator 31 will populate with the date the user verified the claim. 
                <br /><strong>Feature: </strong>SN Psych Assessment:  Ability to send the assessment to the physician for signature. Physician signature line exists at the bottom of the note.
                <br /><strong>Feature: </strong>Report: Payroll Detail Summary: Visits not associated to a payor will present as No Valid Insurance.
                <br /><strong>Feature: </strong>Report: Visits by Payor: Report now includes visit calculations for all payors.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">11/08/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Payable Flag: Ability to individually mark a task as payable or not.
                <br /><strong>Feature: </strong>Report: Visit by Status: Added status ‘Exported’ to the available drop down selection.
                <br /><strong>Feature: </strong>Report: Cost Report ability to run by date range.
                <br /><strong>Feature: </strong>Task: 10 Day Summary added to accommodate a state requirement.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">10/21/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Time Overlap Warning:  Warning message presents when: 1. Completing a task which has overlapping time for the patient. 2. Updating task time details that will create a time overlap for the patient.
                <br /><strong>Enhancement: </strong>Pharmacy information may now be added to AgencyCore under Create->New->Pharmacy.  Update pharmacy information through the View->List->Pharmacy path.  Pharmacy information in Create->New Patient and Edit Patient is now a drop down selection list. 
                <br /><strong>Enhancement: </strong>Ability to enter Non-Visit Pay for employees.  To use the non-visit pay option: 1. Create new non-visit pay type under Admin->New->Non-Visit.  2. Enter pay rates for the non-visit pay type in the edit user->pay rates tab.  3. Add non-visit pay for an employee through Admin->Non-Visit Task Manager.Non-Visit Pay information entered will pull to Admin->Payroll Summary for processing. 
                <br /><strong>Enhancement: </strong>Schedule Center->Visit Log: New Feature allowing the ability to update multiple task time in-time outs from one screen.
                <br /><strong>Feature: </strong>Create/Edit Insurance:  Ability to indicate if the payor requires Q-Codes on the final claim.  Checking the ‘service location required’ checkbox will pull the Q-Code to final claims for the insurance. 
                <br /><strong>Feature: </strong>Non-Admit Patients: Non-admit patient selection now available in Patient Chart, Scheduling, QA and Billing History Menus.  Provides agencies the opportunity to view and complete outstanding processes on prior admissions for patients with a current non-admit status.
                <br /><strong>Feature: </strong>PT/OT/ST Eval: Added ‘send as order’ functionality back into the task.  Selecting the send as order checkbox will send the entire eval to the physician.  A POC is not created as a separate document when ‘send as an order’ is selected.
                <br /><strong>Feature: </strong>PT/OT/ST Plan of Care: Refresh action button added to Therapy POC.  Option has same functionality as 485 refresh. Changes made to the order section of the eval will pull to the POC when utilizing the refresh.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">09/27/2013</span>
            <span class="recentchangescomments">
                <strong>Feature: </strong>Reports->Report Center->Financial Reports: Excel Reports now provide column totals for columns containing dollar amounts. 
                <br /><strong>Feature: </strong>Reports->Report Center->Financial Reports->Managed Care Claims History by Status: Added multi-select option in the insurance drop down parameter list; and insurance column added to the report return values.
                <br /><strong>Feature: </strong>Reports->Report Center->Financial Reports->Manage Care Claims Report:  Added Insurance column to the report return values to assist in reviewing financials.
                <br /><strong>Feature: </strong>Reports->Report Center->Statistical Reports->Visits by Payor :  New report to accommodate several state reporting requirements.
                <br /><strong>Feature: </strong>Reports->Report Center->Survey Census by Date Range: New report to accommodate surveyor requests for patient census by date range.
                <br /><strong>Feature: </strong>Reports->Completed Reports: Updated report view to only show the reports for the user.
                <br /><strong>Feature: </strong>Billing->Claim Submission History->View Claim Response: Added ability to print the 999 file.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">09/13/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>PT/ OT/ ST Evaluations:  Upon clinician completion of the evaluation the form now separates into two documents, Evaluation and Plan of Care (POC).  The POC moves into the clinicians My Scheduled Tasks in a saved status. Once the POC is approved through QA, it automatically flows into Orders Management to send to the physician.
                <br /><strong>Enhancement: </strong>SN Psychiatric Nurse Assessment: Added the ability to send the assessment to the MD if needed.
                <br /><strong>Enhancement: </strong>LPN/LVN Note:  New clinical note type added to increase billing/payroll/reporting functionality. Note: remember to add billing/pay rates as needed if using the new note type.
                <br /><strong>Enhancement: </strong>SN Wound Note: New clinical note type added to allow additional tracking of wound documentation compliance.
                <br /><strong>Feature: </strong>Removal of Patient Request in HHA Care Plan:  Updated to comply with regulatory updates.
                <br /><strong>Feature: </strong>HCFA 1500 Claim Verification: Added functionality to indicate locator 10 information in step 2 of the claim verification.
                <br /><strong>Feature: </strong>UB04 Claim Verification: Added functionality to indicate locator 35 and 36 in step 2 of the claim verification.
                <br /><strong>Feature: </strong>User Credentials: Two options added to the credential drop down selection list, PCW and HMK.
                <br /><strong>Feature: </strong>Reports->Report Center->Patient By Responsible Case Manager Report: Added patient phone number column to the existing report.
                <br /><strong>Feature: </strong>Reports->Report Center->Patient List: Added assigned clinician column to the existing report.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">08/16/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Report Center->Payroll Detail Summary:  Columns added to the report include Primary Payor, Pay Rate, Mileage Rate and Total Pay.  Report separates tasks by primary payor to assist with payroll processing.
                <br /><strong>Enhancement: </strong>Admin ->Lists ->Users ->Click “Edit” (Action)->Permissions: Added Report permission for Payroll Detail Summary.
                <br /><strong>Enhancement: </strong>Report Center->Patient Admissions by Internal Referral Source: Admission Status added to the parameter list.
                <br /><strong>Enhancement: </strong>MSW Eval/Assessment: New version contains additional options under living situation and Psychosocial Assessment.
                <br /><strong>Enhancement: </strong>OT Discharge: New version removes treatment plan and goal information.
                <br /><strong>Feature: </strong>Report Center->Reports->Access Payroll Reports: Added additional permission for payroll reports. 
                <br /><strong>Feature: </strong>Billing->Medicare/Medicare HMO->Create Rap Claims:  Text warning “This is not the first scheduled visit” presents when the first completed visit does not match the first scheduled visit in the episode.  Warning appears by the first billable visit field in the episode section during the validation process.
                <br /><strong>Feature: </strong>Admin->Payroll Summary->Generate->View Details->Export All to Excel: Report now includes subtotaling by visit type (number and total pay) for each clinician. 
                <br /><strong>Feature: </strong>Billing->Medicare/Medicare HMO->Billing Claim History: Ability to create a secondary claim on finals in a Processed as Primary status.
                <br /><strong>Enhancement: </strong>Report Center->Patient Reports->Patient List Report: Provides comprehensive patient profile.
                <br /><strong>Enhancement: </strong>Report Center->Employee Reports->Employee List Report: Provides comprehensive list of employee information.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">08/02/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Report Center: Hovering over a report name presents a description of the report, parameter selections and return values.   Once selected a question mark is available next to the report name. Clicking the question mark will provide the report description.
                <br /><strong>Enhancement: </strong>Remit Files: Billing->Medicare/Medicare HMO->Remittance: Ability to review the raw remittance files including: 999, 277, 835.
                <br /><strong>New Feature: </strong>Edit Users: Admin->Lists->Users:  Multi-Select option available to Inactivate/Activate/Delete/Restore users.
                <br /><strong>New Feature: </strong>Payroll: Admin->Payroll Summary->Generate->View Details->Export All to Excel/Print PDF: Report now includes subtotaling by visit type (number and total pay) for each clinician and Summary for all clinicians. 
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">07/12/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Login:  Redesign of login page.  “Authorized to use” message displays in ‘sign in’ box once login is successful.
                <br /><strong>Enhancement: </strong>Infection and Incident forms:  Added ability to select a template for text sections. Create->New->Incident (or Infection).  SN notes->Add Infection/Add Incident
                <br /><strong>Feature: </strong>Pending Claims:  Ability to export Pending Claims to Excel.  Report generates two tabs in Excel (RAP/Final).  Billing=>Medicare=>Pending Claims->Export File
                <br /><strong>Enhancement: </strong>Outstanding Claims Report:  Added ability to sort by payor.Reports->Report Center->Financial Reports->Outstanding Claims Report
                <br /><strong>Enhancement: </strong>Patient Roster Report:  Added ability to view SOC and Discharge dates without exporting to Excel.  Select “+” sign to expand the patient information.Reports->Report Center->Patient Reports->Patient Roster Report
                <br /><strong>Enhancement: </strong>Payroll Summary Report:  Visit status added to employee exported excel report.Admin->Payroll Summary->Generate ->Select Detail for an Employee->Export to Excel
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">06/28/2013</span>
            <span class="recentchangescomments">
                <strong>New Features:</strong>Q Code Service Location:
                <br />Home Health episodes beginning on or after July 1, 2013, must report where home health services were provided.
                <br />Q5001 (Hospice or home health care provided in patient’s home/residence)
                <br />Q5002 (Hospice or home health care provided in assisted living facility) 
                <br />Q5009 (Hospice or home health care provided in place not otherwise specified (NO) 
                <br />Service location designation field has been added to Patient Demographic address information.  Service designation is available in: Create->New->Patient; Patient->Existing Referral->Admit action; Patient->Patient Chart->Edit feature as well as in the individual task details.  Q code flows into the final claim and is viewable in the final claim summary screen and the UB04 print version. 
                <br /><strong>New Features:</strong>PT Evaluation(version 4):Form reorganization to align physical therapy orders/treatment plan to the end of the evaluation for ease of sending to the physician for signature. Additional fields include: PT Short term and long term goals, Patient/CG desired outcomes; skilled care provided; safety issues;  
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">06/21/2013</span>
            <span class="recentchangescomments">
                <strong>New Features:</strong>New Report:  Referral Log:  Report Center->Patient Reports->Referral Log
                <br /><strong>Enhancement: </strong>Employee Weekly Schedule Report:  Sort function added to the scheduled date column.
                <br /><strong>New Features:</strong>Speech Therapy Visit Notes:  Added OASIS C Discharge ST note and ST Reassessment note.
                <br /><strong>Enhancement: </strong>Vital Sign Chart/Log: Includes PT/PTA/HHA recorded apical/radial pulse information.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">06/09/2013</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Transfer Summary: additional ACHC requirement fields added: Date of Transfer; Emergency Contact; Physician Phone; Date/Name of Person Receiving Report; Transfer Orders and instructions; Template drop down list added to the Significant Health History and Description of Services Provided sections.
                <br /><strong>Enhancement: </strong>PT Discharge Summary: additional ACHC requirement fields added: Physician Phone; Added PT related diagnosis field; Template drop down list added to the Instructions Given to the Patient/Responsible Party field.
                <br /><strong>Enhancement: </strong>ST Discharge: added functionality to send and track an ST Discharge to MD through Orders Management. Upper right hand corner contains “send to MD” checkbox statement.  Checking the statement sends an approve ST Discharge to Orders Management.
                <br /><strong>Enhancement: </strong>Patient Evacuation Address:  Patient->Patient Charts: added patient evacuation address information section to patient demographics.
                <br /><strong>Enhancement: </strong>Patient Emergency Preparedness Plan Report: Report->Report Center->Patient Reports: report contains patient name/triage/contact/relationship/contact phone/contact email/evacuation address/evacuation zone/evacuation home phone and evacuation mobile phone.
                <br /><strong>Enhancement: </strong>PAS Note/PAS Travel Note (travel time covered by payer):  View=>List=>Insurance->Edit.  Edit option contains statement ‘travel time is covered by this payer’ statement.  If selected and completed the travel time will pull to the claim and invoice.  If the biller marks either the PAS travel or PAS Note as nonbillable the associated record also checks.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">05/12/2013</span>
            <span class="recentchangescomments">
                <strong>New Features:</strong>Create->New->Referral:  Add ability to include patient Insurance information in a patient referral including:  Primary, Secondary and Tertiary insurances.  Patient Insurance Information pulls to the New Patient screen when selecting to ‘Admit’ the patient.
                <br /><strong>New Features:</strong>Print Icon added to increase efficiency in the order tracking processes. View->Orders Management->Orders History / Orders Pending Signature: Printer Icon for each order line item.
                <br /><strong>New Features:</strong>Report->Report Center->Visit by Status: Ability to multi-select status types and sort results by status.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">02/16/2013</span>
            <span class="recentchangescomments">
		        <strong>Major Enhancement:  </strong>Vital Sign Charts - Graphical representation of Patients Vital Signs. Go to Patient Charts -> Select Patient -> Click 'Vital Sign Charts' under Quick Reports.
		        <br /><strong>Major Enhancement:  </strong>Messaging System Overhaul - Added a new User-Interface and improved the Message loading speed.
		        <br /><strong>Major Enhancement:  </strong>Real-time Medicare Eligibility Verification direct from DDE instead of the HIPAA Eligibility Transaction System (HETS). 
		        <br /><strong>Enhancement:  </strong>User List page Redesign - Go to Admin -> List -> Users. Added a status filter and made the page more user-friendly.
		        <br /><strong>Enhancement:  </strong>Added new 'ST Discharge Summary' document. Available for use in the Schedule Center.
		        <br /><strong>Enhancement:  </strong>Added a new report called <i>Payroll Detail Summary</i> under the Employee Reports.
		        <br /><strong>Enhancement:  </strong>Added a new report called <i>Billed/Unbilled Revenue</i> under the Billing Reports.
		        <br /><strong>Enhancement:  </strong>Added a new report called <i>Earned/Unearned Revenue</i> under the Billing Reports.
		        <br /><strong>Enhancement:  </strong>Added a new 'Remittance Advice' permission to restrict access to Remittance Advice information.
		        <br /><strong>Enhancement:  </strong>Column sorting added for the Payroll Summary Report.
		        <br /><strong>Enhancement:  </strong>Added the ability for PT/OT ReAssessment documents to go through the Orders Management process.
		        <br /><strong>Enhancement:  </strong>Ensure the Remarks field is displayed on the UB-04 claim form.
		        <br /><strong>Enhancement:  </strong>Ability to bill Wound Supplies without including the regular Service Supplies for Medicare Claims.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">12/06/2012</span>
            <span class="recentchangescomments">
		        <strong>Major Enhancement:  </strong>Secondary Payor Claim Billing made easier. Use Medicare Remittance information to bill non-Medicare payors.
		        <br /><strong>Major Enhancement:  </strong>Added Patient Document Upload feature to the Patient Charts.
		        <br /><strong>Enhancement:  </strong>Flowing the Pulse Oximetry field in Skilled Nurse Visit note to the O2 Saturation column in the vital signs log.
		        <br /><strong>Enhancement:  </strong>User-Interface improvements to the Medicare claims history page.
		        <br /><strong>Enhancement:  </strong>Added the Quick Search box to the User List. Go to Admin -> Lists -> Users, type User name in the Quick Search box to filter by name.
		        <br /><strong>Enhancement:  </strong>Added the Status Column to the Excel Export for the Past Due Visit Report.
		        <br /><strong>Enhancement:  </strong>User-Interface improvements to the Login and Forgot Password pages.
		        <br /><strong>Minor Enhancement:  </strong>Maximize vertical spacing on the Non-Admitted Patients List.
		        <br /><strong>Minor Enhancement:  </strong>Non-software user licenses can be deleted properly in the License manager. Go to Admin -> License Manager.
		        <br /><strong>Minor Enhancement:  </strong>Message widget dropdown delete functionality restored.
		        <br /><strong>Minor Enhancement:  </strong>The <em>Not Yet Started</em> Status in the "Visit by Status" Report pulls past due visit documents.
		        <br /><strong>Minor Enhancement:  </strong>Sixty-Day Summary documents account for temperatures with the "F" appended to the end.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">10/08/2012</span>
            <span class="recentchangescomments">
		        <strong>New Feature:  </strong>Added a new report called <i>Patient Admissions by Internal Referrer</i> under the Statistical Reports.
		        <br /><strong>Enhancement:  </strong>Revamp of the Return Reason process (Red Sticky Notes) in the QA Center.
		        <br /><strong>Enhancement:  </strong>Wound care attachments in OASIS Assessments and Skilled Nursing notes can be deleted from the details page.
		        <br /><strong>Enhancement:  </strong>Added Load Previous Note feature to the Skilled Nursing Psych Note and Skilled Nursing Psych Assessment.
		        <br /><strong>Enhancement:  </strong>Completed Missed Visit notes are displayed in red font in both the Patient Charts and the Schedule Center.   
	            <br /><strong>Enhancement:  </strong>OT Evaluation Note layout issue resolved for smaller screen resolutions.  
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">10/03/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement:  </strong>Emergency Contact Information added to the New/Edit Referral Page.
	            <br /><strong>Enhancement:  </strong>In the New/Edit Contact pages, an Extension Number field added to the Contact Office Phone.
	            <br /><strong>Enhancement:  </strong>Added the ability to load pay rate information from one user to another user.  
	            <br /><strong>Enhancement:  </strong>In Managed Care Claims, added the ability to post multiple payments and multiple adjustments to claims.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">09/29/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement:  </strong>Missed Visit documents can be reviewed in the QA Center once it is completed by a clinician.
	            <br /><strong>Enhancement:  </strong>On the dashboard, patient names are hyperlinked to open the Patient Charts.
	            <br /><strong>Enhancement:  </strong>On the dashboard, Task names are hyperlinked to open the documents for edit/update.
                <br /><strong>Enhancement:  </strong>OASIS assessments and Visit Notes optimized for faster load times.
	            <br /><strong>Enhancement:  </strong>On the Contact List, contacts with comments will show a yellow sticky note. Go to View -> List -> Contacts. 
	            <br /><strong>Enhancement:  </strong>Fixed the issue with exporting the Infection log to Microsoft Excel. Go to View -> Lists -> Infection Logs, click on Excel Export button.
	            <br /><strong>Enhancement:  </strong>Added the ability to send PT Discharge documents to the Order Management center. Check 'Send as Order' checkbox if you want to send to the Physician.
	            <br /><strong>Enhancement:  </strong>The Physician Credentials appear after the Physician's name in the physician list. Go to View -> Lists -> Physicians.
	            <br /><strong>Enhancement:  </strong>Updates to the COTA Visit Note to include a Narrative section and expanded the Assessment box.
	            <br /><strong>Enhancement:  </strong>Updates to the ST Visit Notes and ST Discharge Notes to make it more consistent with the other Therapy Notes.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">09/10/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature:  </strong>Ability to print Messages from your inbox in the message center.
                <br /><strong>New Feature:  </strong>Added a new report called <i>Visits by Type</i>, which displays any/all documents based on patients and clinicians.
                <br /><strong>New Feature:  </strong>New Skilled Nursing Pediatric Assessment Note.
                <br /><strong>Enhancement:  </strong>Significantly improved the column spacing on all reports.
                <br /><strong>Enhancement:  </strong>Added Insurance, Policy Number, Address, and Gender columns to Non-Admission Patients. Also added the ability to export the Non-Admission list to Excel.
                <br /><strong>Enhancement:  </strong>Added templates to all Comment sections in the PT and OT Visit Notes, Evaluation Notes and Reassessment Notes. 
                <br /><strong>Enhancement:  </strong>Added templates to the Comments section in the New and Edit Referral pages.
                <br /><strong>Enhancement:  </strong>Added Physician's PECOS status to the physician list in View -> List -> Physicians.
                <br /><strong>Enhancement:  </strong>Added Evacuation Zone column to the Patient Survey Census Report
                <br /><strong>Enhancement:  </strong>Automatically Refresh the Patient Charts when the Primary Physician is changed in the Patient Profile page.
                <br /><strong>Enhancement:  </strong>Added Patient's Middle Initial to the patient list in the Communication Note.
                <br /><strong>Enhancement:  </strong>Added a Date Filter to the Reassign Task page to limit the reassignment of tasks to a selected date period.
                <br /><strong>Enhancement:  </strong>Added blank forms for the SN Pediatric Visit Note and the SN Pediatric Assessment Note. Go to View -> Blank Forms.
                <br /><strong>Enhancement:  </strong>Added Payment Date column to Remittance Advices. Go to Billing -> Medicare/Medicare HMO -> Remittance Advice.
                <br /><strong>Enhancement:  </strong>Automatically complete the City and State fields whenever a Zip Code is entered throughout the system.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">08/02/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature:  </strong>New version of the Skilled Nursing Pediatric Note introduced.
                <br /><strong>New Feature:  </strong>Added a new schedule report called <i>Visits By Status</i>, which displays scheduled visits based on status.
                <br /><strong>New Feature:  </strong>Added a new billing report called <i>Unbilled Managed Care Claims</i>
                <br /><strong>New Feature:  </strong>The Red Sticky Note can be edited in the Details page.
                <br /><strong>New Feature:  </strong>Skilled Nursing Insulin Noon/HS Visit added to the list of items that can be scheduled. 
                <br /><strong>Enhancement:  </strong>In the QA Center, added a date range filter for the items to be reviewed.
                <br /><strong>Enhancement:  </strong>Added the ability to edit the gender field in the Add/Edit User page.
                <br /><strong>Enhancement:  </strong>In the PT/OT Evaluations, ReEvaluations, and ReAssessments notes, the "Send as an Order" checkbox is checked by default.
                <br /><strong>Enhancement:  </strong>Added "Verify Insurance" tab to the managed care claim page. This allows billers to update pertinent insurance information such as claim modifiers within that particular claim.
                <br /><strong>Enhancement:  </strong>The Print Queue utilizes the printer icon instead of hyperlinking the name of the document to be printed.
                <br /><strong>Enhancement:  </strong>Added the PT/OT Reassessments to the blank forms page.
                <br /><strong>Enhancement:  </strong>Added the ability to check Medicare Eligibility for referals in the new and edit referal pages.
                <br /><strong>Enhancement:  </strong>In the Print Queue page, added a date range filter.
                <br /><strong>Enhancement:  </strong>In the Schedule Center, the list of discipline tasks has been sorted alphabetically.
                <br /><strong>Enhancement:  </strong>Added more editable fields to the Manage Company Information page to allow agencies more control over their information.
                <br /><strong>Enhancement:  </strong>DNR (Do Not Resuscitate) field added to the New/Edit Patient page and the New/Edit Referral page. This information is displayed in the patient profile as well.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">07/05/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature:  </strong>Added a new billing report called <i>Managed Care Claims History By Status</i>, which displays Managed Care Claims based on status.
                <br /><strong>Enhancement:  </strong>In the managed care claims history page, the claim amount reflects the total charges of the claim (supply/visit totals) instead of the HIPPS Code payment amount determined by the corresponding OASIS assessment.
                <br /><strong>Enhancement:  </strong>In the message inbox, added the ability to page through messages.
                <br /><strong>Enhancement:  </strong>Added Prior Level of Function section to the PT Evaluation/ReEvaluation/ReAssessment.
                <br /><strong>Enhancement:  </strong>Added the ability to check Medicare Eligibility for patients in the edit patient profile page with one click.
                <br /><strong>Enhancement:  </strong>From the list of Insurances/Payors, agencies can enter/edit addresses for Medicare Intermediaries. This will flow directly into the UB-04 Form for RAP/Final Claims.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">06/26/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature:  </strong>Under the Billing Menu, there is a new feature called 'All Insurances/Payors'. This page allows you to create Medicare and all non-Medicare claims for all your branches/locations in a central place.
                <br /><strong>Enhancement:  </strong>In the Start-of-Care, Recertification, and Resumption-of-Care OASIS assessments, the allergy section has been updated to be more consistent with the allergy profile in the patient charts. 
                <br /><strong>Enhancement:  </strong>Final Claims with wound care supplies using the revenue code 0623 will include an additional line item reporting the total charges of the wound care supplies. Per <i>Medicare Benefit Policy Manual</i> (CMS Pub 100-02, Ch. 7, § 50.4.1), agencies can report non-routine wound care supplies using the revenue code 0623.
                <br /><strong>Enhancement:  </strong>HCFA-1500 Form updated to include the insurance provider address at the top of the page. In addition, several fields have been prefilled with default values.
                <br /><strong>Enhancement:  </strong>In OASIS assessments and skilled nursing notes, pictures uploaded to the wound sheet are now available under the task details. Click on the details link in the patient charts or schedule center to view the wound care pictures even if the OASIS assessment or skilled nursing note has been completed.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">06/04/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement:  </strong>Increased the font size on all reports.
                <br /><strong>Enhancement:  </strong>In the New User page, you can set permissions for the new user based on another user's permissions.
                <br /><strong>New Feature:  </strong>Added a new report called <i>Employee Permissions</i>, which displays a list of permissions set for all your users.
            </span>
        </li>
    </ol>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last")
    })
</script>