﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="10">Current Function Status</th>
        </tr>
        <tr>
            <th colspan="4">Bed Mobility</th>
            <th colspan="6">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev2.ascx", Model); %></td>
            <td colspan="6" rowspan="7"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Transfer</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Gait</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev3.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">W/C Mobility
            <%= string.Format("<input class='radio' id='{0}IsWCMobilityApply' name='{0}_IsWCMobilityApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsWCMobilityApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4">
            <div id="<%=Model.Type %>WCMobilityContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %>
            </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" rowspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
            <th colspan="6">Skilled Care Provided This Visit:</th>
        </tr>
        <tr>
            <td colspan="6">
                <%= Html.ToggleTemplates(Model.Type + "_GenericSkilledCareProvidedTemplates", "", "#" + Model.Type + "_GenericSkilledCareProvided")%>
                <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.AnswerOrEmptyString("GenericSkilledCareProvided"), 8, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%>
            </td>
            
        </tr>
        <tr>
            <th colspan="5">Reason for discharge</th>
            <th colspan="5">Condition of patient at time of discharge</th>
        </tr>
        <tr>
            <td colspan="5">
                <%  string[] genericReasonForDischarge = data.AnswerArray("GenericReasonForDischarge"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericReasonForDischarge" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", genericReasonForDischarge.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", genericReasonForDischarge.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge2">No Longer Homebound</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", genericReasonForDischarge.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per Patient/Family Request</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", genericReasonForDischarge.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge4">Prolonged On-Hold Status</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", genericReasonForDischarge.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge5">Goals Met</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", genericReasonForDischarge.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge6">Hospitalized</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", genericReasonForDischarge.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Expired</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge8' class='radio' name='{1}_GenericReasonForDischarge' value='8' type='checkbox' {0} />", genericReasonForDischarge.Contains("8").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Moved Out Of Service Area</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericReasonForDischargeOther" class="float-left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.AnswerOrEmptyString("GenericReasonForDischargeOther"), new { @class = "", @id = Model.Type+"_GenericReasonForDischargeOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DischargeCondition/FormRev1.ascx", Model); %></td>
        </tr>
        
        <tr>
            <th colspan="5">Summary of care provided</th>
            <th colspan="5">Summary of progress made</th>
        </tr>
        <tr>
            <td colspan="5">
                <%= Html.ToggleTemplates(Model.Type + "_GenericSummaryOfCareTemplates", "", "#" + Model.Type + "_GenericSummaryOfCare")%>
                <%= Html.TextArea(Model.Type + "_GenericSummaryOfCare", data.AnswerOrEmptyString("GenericSummaryOfCare"), 8, 20, new { @id = Model.Type + "_GenericSummaryOfCare", @class = "fill" })%>
            </td>
            <td colspan="5">
                <%= Html.ToggleTemplates(Model.Type + "_GenericSummaryOfProgressTemplates", "", "#" + Model.Type + "_GenericSummaryOfProgress")%>
                <%= Html.TextArea(Model.Type + "_GenericSummaryOfProgress", data.AnswerOrEmptyString("GenericSummaryOfProgress"), 8, 20, new { @id = Model.Type + "_GenericSummaryOfProgress", @class = "fill" })%>
            </td>
         </tr>
    </tbody>
</table>

<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>IsWCMobilityApply"), $("#<%= Model.Type %>WCMobilityContainer"));
</script>