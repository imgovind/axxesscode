﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericHomeBoundStatus" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus7' class='radio' name='{1}_GenericHomeBoundStatus' value='7' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus7">
                                        Requires considerable and taxing effort.</label>
                                </div>
                            </td>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus8' class='radio' name='{1}_GenericHomeBoundStatus' value='8' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus8">
                                        Medical restriction.</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus1' class='radio' name='{1}_GenericHomeBoundStatus' value='1' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus1">
                                        Needs assist with transfer.</label>
                                </div>
                            </td>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus2' class='radio' name='{1}_GenericHomeBoundStatus' value='2' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus2">
                                        Needs assist with ambulation.</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus3' class='radio' name='{1}_GenericHomeBoundStatus' value='3' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus3">
                                        Needs assist leaving the home.</label>
                                </div>
                            </td>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus4' class='radio' name='{1}_GenericHomeBoundStatus' value='4' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus4">
                                        Unable to be up for long period.</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus5' class='radio' name='{1}_GenericHomeBoundStatus' value='5' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus5">
                                        Severe SOB upon exertion.</label>
                                </div>
                            </td>
                            <td class="align-left">
                                <div>
                                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus6' class='radio' name='{1}_GenericHomeBoundStatus' value='6' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericHomeBoundStatus6">
                                        Unsafe to go out of home alone.</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

