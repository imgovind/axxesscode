﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
<% if(data.ContainsKey("IsHomeboundApply")&&data.AnswerOrEmptyString("IsHomeboundApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Homebound Reason");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Requires considerable and taxing effort.",<%= genericHomeBoundStatus.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Medical restriction.",<%= genericHomeBoundStatus.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with transfer.",<%= genericHomeBoundStatus.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with ambulation.",<%= genericHomeBoundStatus.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Needs assist leaving the home.",<%= genericHomeBoundStatus.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Unable to be up for long period.",<%= genericHomeBoundStatus.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Severe SOB upon exertion.",<%= genericHomeBoundStatus.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Unsafe to go out of home alone.",<%= genericHomeBoundStatus.Contains("6").ToString().ToLower() %>)),
        "Homebound Reason");
</script>  
<%} %>             