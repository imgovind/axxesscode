﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentCodes = data.AnswerArray("GenericTreatmentCodes"); %>
<input type="hidden" name="<%= Model.Type %>_GenericTreatmentCodes" value="" />
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes1' class='radio' name='{1}_GenericTreatmentCodes' value='1' type='checkbox' {0} />", genericTreatmentCodes.Contains("1").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes1">B1 Evaluation</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes2' class='radio' name='{1}_GenericTreatmentCodes' value='2' type='checkbox' {0} />", genericTreatmentCodes.Contains("2").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes2">B2 Thera Ex</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes3' class='radio' name='{1}_GenericTreatmentCodes' value='3' type='checkbox' {0} />", genericTreatmentCodes.Contains("3").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes3">B3 Transfer Training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes4' class='radio' name='{1}_GenericTreatmentCodes' value='4' type='checkbox' {0} />", genericTreatmentCodes.Contains("4").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes4">B4 Home Program</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes5' class='radio' name='{1}_GenericTreatmentCodes' value='5' type='checkbox' {0} />", genericTreatmentCodes.Contains("5").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes5">B5 Gait Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes6' class='radio' name='{1}_GenericTreatmentCodes' value='6' type='checkbox' {0} />", genericTreatmentCodes.Contains("6").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes6">B6 Chest PT</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes7' class='radio' name='{1}_GenericTreatmentCodes' value='7' type='checkbox' {0} />", genericTreatmentCodes.Contains("7").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes7">B7 Ultrasound</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes8' class='radio' name='{1}_GenericTreatmentCodes' value='8' type='checkbox' {0} />", genericTreatmentCodes.Contains("8").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes8">B8 Electrother</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes9' class='radio' name='{1}_GenericTreatmentCodes' value='9' type='checkbox' {0} />", genericTreatmentCodes.Contains("9").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes9">B9 Prosthetic Training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes10' class='radio' name='{1}_GenericTreatmentCodes' value='10' type='checkbox' {0} />", genericTreatmentCodes.Contains("10").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes10">B10 Muscle Re-ed</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentCodes11' class='radio' name='{1}_GenericTreatmentCodes' value='11' type='checkbox' {0} />", genericTreatmentCodes.Contains("11").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericTreatmentCodes11">B11 Muscle Re-ed</label>
            </td>
            <td>
                <label class="float-left">Other</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTreatmentCodesOther", data.AnswerOrEmptyString("GenericTreatmentCodesOther"), new { @id = Model.Type + "_GenericTreatmentCodesOther" }) %></div>
            </td>
        </tr>
    </tbody>
</table>