﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
            <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
            <%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates", "", "#" + Model.Type + "_GenericAssessmentMore")%>
            <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"),4, 20, new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" })%>

