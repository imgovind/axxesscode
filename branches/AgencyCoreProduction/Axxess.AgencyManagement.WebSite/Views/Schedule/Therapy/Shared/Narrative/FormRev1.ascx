﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates", "", "#" + Model.Type + "_GenericNarrativeComment")%>
<%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 8, 20, new { @id = Model.Type + "_GenericNarrativeComment", @class = "fill" })%>