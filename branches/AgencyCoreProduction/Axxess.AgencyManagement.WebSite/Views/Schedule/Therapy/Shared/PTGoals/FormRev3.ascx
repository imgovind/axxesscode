﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericPTGoals = data.AnswerArray("POCGenericPTGoals"); %>
<% var noteDiscipline = data.AnswerOrEmptyString("DisciplineTask"); %>
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals1' class='radio' name='{1}_POCGenericPTGoals' value='1' type='checkbox' {0} />", genericPTGoals.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals1">Patient will demonstrate ability to perform home exercise program within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals1Weeks", data.AnswerOrEmptyString("POCGenericPTGoals1Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals1Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals1Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals2' class='radio' name='{1}_POCGenericPTGoals' value='2' type='checkbox' {0} />", genericPTGoals.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals2">Demonstrate effective pain management utilizing </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals2Utilize", data.AnswerOrEmptyString("POCGenericPTGoals2Utilize"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals2Utilize", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals2Utilize">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals2Weeks", data.AnswerOrEmptyString("POCGenericPTGoals2Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals2Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals2Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals3' class='radio' name='{1}_POCGenericPTGoals' value='3' type='checkbox' {0} />", genericPTGoals.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals3">Patient will be able to perform sit to supine/supine to sit with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals3Assist", data.AnswerOrEmptyString("POCGenericPTGoals3Assist"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals3Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals3Assist">assist with safe and effective technique within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals3Weeks", data.AnswerOrEmptyString("POCGenericPTGoals3Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals3Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals3Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals4' class='radio' name='{1}_POCGenericPTGoals' value='4' type='checkbox' {0} />", genericPTGoals.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals4">Improve bed mobility to independent within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals4Weeks", data.AnswerOrEmptyString("POCGenericPTGoals4Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals4Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals4Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals5' class='radio' name='{1}_POCGenericPTGoals' value='5' type='checkbox' {0} />", genericPTGoals.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals5">Improve transfers to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals5Assist", data.AnswerOrEmptyString("POCGenericPTGoals5Assist"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals5Assist", @maxlength = "30" })%> 
                     <label for="<%= Model.Type %>_POCGenericPTGoals5Assist">assist using </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals5Within", data.AnswerOrEmptyString("POCGenericPTGoals5Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals5Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals5Within">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals5Weeks", data.AnswerOrEmptyString("POCGenericPTGoals5Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals5Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals5Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals6' class='radio' name='{1}_POCGenericPTGoals' value='6' type='checkbox' {0} />", genericPTGoals.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals6">Independent with safe transfer skills within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals6Weeks", data.AnswerOrEmptyString("POCGenericPTGoals6Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals6Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals6Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals7' class='radio' name='{1}_POCGenericPTGoals' value='7' type='checkbox' {0} />", genericPTGoals.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals7">Patient will improve</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals7Skill", data.AnswerOrEmptyString("POCGenericPTGoals7Skill"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals7Skill", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals7Skill">transfer skill to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals7Assist", data.AnswerOrEmptyString("POCGenericPTGoals7Assist"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals7Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals7Assist">assist using </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals7Within", data.AnswerOrEmptyString("POCGenericPTGoals7Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals7Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals7Within">device within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals7Weeks", data.AnswerOrEmptyString("POCGenericPTGoals7Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals7Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals7Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals8' class='radio' name='{1}_POCGenericPTGoals' value='8' type='checkbox' {0} />", genericPTGoals.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals8">Patient to be independent with safety issues in</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals8Weeks", data.AnswerOrEmptyString("POCGenericPTGoals8Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals8Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals8Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals9' class='radio' name='{1}_POCGenericPTGoals' value='9' type='checkbox' {0} />", genericPTGoals.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals9">Patient will be able to negotiate stairs with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals9Assist", data.AnswerOrEmptyString("POCGenericPTGoals9Assist"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals9Assist", @maxlength = "30" })%> 
                     <label for="<%= Model.Type %>_POCGenericPTGoals9Assist">device with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals9Within", data.AnswerOrEmptyString("POCGenericPTGoals9Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals9Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals9Within">assist within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals9Weeks", data.AnswerOrEmptyString("POCGenericPTGoals9Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals9Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals9Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals10' class='radio' name='{1}_POCGenericPTGoals' value='10' type='checkbox' {0} />", genericPTGoals.Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals10">Patient will be able to ambulate using</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals10Device", data.AnswerOrEmptyString("POCGenericPTGoals10Device"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals10Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals10Device">device at least</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals10Feet", data.AnswerOrEmptyString("POCGenericPTGoals10Feet"), new { @class = "sn", @id = Model.Type + "_POCGenericPTGoals10Feet", @maxlength = "10" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals10Feet">feet with</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals10Within", data.AnswerOrEmptyString("POCGenericPTGoals10Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals10Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals10Within">assist with safe and effective gait pattern on even/uneven surface within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals10Weeks", data.AnswerOrEmptyString("POCGenericPTGoals10Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals10Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals10Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals11' class='radio' name='{1}_POCGenericPTGoals' value='11' type='checkbox' {0} />", genericPTGoals.Contains("11").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals11">Independent with ambulation without device indoor/outdoor at least </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals11Feet", data.AnswerOrEmptyString("POCGenericPTGoals11Feet"), new { @class = "sn", @id = Model.Type + "_POCGenericPTGoals11Feet", @maxlength = "10" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals11Feet">feet to allow community access within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals11Weeks", data.AnswerOrEmptyString("POCGenericPTGoals11Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals11Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals11Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals12' class='radio' name='{1}_POCGenericPTGoals' value='12' type='checkbox' {0} />", genericPTGoals.Contains("12").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals12">Patient will be able to ambulate using</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals12Device", data.AnswerOrEmptyString("POCGenericPTGoals12Device"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals12Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals12Device">device at least</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals12Feet", data.AnswerOrEmptyString("POCGenericPTGoals12Feet"), new { @class = "sn", @id = Model.Type + "_POCGenericPTGoals12Feet", @maxlength = "10" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals12Feet">feet on</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals12Within", data.AnswerOrEmptyString("POCGenericPTGoals12Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals12Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals12Within">surface to be able to perform ADL within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals12Weeks", data.AnswerOrEmptyString("POCGenericPTGoals12Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals12Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals12Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals13' class='radio' name='{1}_POCGenericPTGoals' value='13' type='checkbox' {0} />", genericPTGoals.Contains("13").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals13">Improve strength of</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals13Of", data.AnswerOrEmptyString("POCGenericPTGoals13Of"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals13Of", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals13Of">to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals13To", data.AnswerOrEmptyString("POCGenericPTGoals13To"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals13To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals13To">grade to improve</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals13Within", data.AnswerOrEmptyString("POCGenericPTGoals13Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals13Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals13Within">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals13Weeks", data.AnswerOrEmptyString("POCGenericPTGoals13Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals13Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals13Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals14' class='radio' name='{1}_POCGenericPTGoals' value='14' type='checkbox' {0} />", genericPTGoals.Contains("14").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals14">Increase muscle strength of </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals14Of", data.AnswerOrEmptyString("POCGenericPTGoals14Of"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals14Of", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals14Of">to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals14To", data.AnswerOrEmptyString("POCGenericPTGoals14To"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals14To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals14To">grade to improve gait pattern/stability and decrease fall risk within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals14Weeks", data.AnswerOrEmptyString("POCGenericPTGoals14Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals14Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals14Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals15' class='radio' name='{1}_POCGenericPTGoals' value='15' type='checkbox' {0} />", genericPTGoals.Contains("15").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals15">Increase trunk muscle strength to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals15To", data.AnswerOrEmptyString("POCGenericPTGoals15To"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals15To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals15Feet">to improve postural control and balance within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals15Weeks", data.AnswerOrEmptyString("POCGenericPTGoals15Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals15Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals15Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals16' class='radio' name='{1}_POCGenericPTGoals' value='16' type='checkbox' {0} />", genericPTGoals.Contains("16").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals16">Increase trunk muscle strength to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals16To", data.AnswerOrEmptyString("POCGenericPTGoals16To"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals16To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals16Feet">to improve postural control during bed mobility and transfer within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals16Weeks", data.AnswerOrEmptyString("POCGenericPTGoals16Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals16Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals16Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals17' class='radio' name='{1}_POCGenericPTGoals' value='17' type='checkbox' {0} />", genericPTGoals.Contains("17").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals17">Patient will increase ROM of </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals17Joint", data.AnswerOrEmptyString("POCGenericPTGoals17Joint"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals17Joint", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals17Joint">joint to </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals17Degree", data.AnswerOrEmptyString("POCGenericPTGoals17Degree"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals17Degree", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals17Degree">degree of </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals17In", data.AnswerOrEmptyString("POCGenericPTGoals17In"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals17In", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals17In">in</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals17Weeks", data.AnswerOrEmptyString("POCGenericPTGoals17Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals17Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals17Weeks">weeks to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals17IncreaseROMTo", data.AnswerOrEmptyString("POCGenericPTGoals17IncreaseROMTo"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals17IncreaseROMTo", @maxlength = "30" })%> 
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals18' class='radio' name='{1}_POCGenericPTGoals' value='18' type='checkbox' {0} />", genericPTGoals.Contains("18").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals18">Demonstrate safe and effective use of prosthesis/brace/splint within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals18Weeks", data.AnswerOrEmptyString("POCGenericPTGoals18Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals18Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals18Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals19' class='radio' name='{1}_POCGenericPTGoals' value='19' type='checkbox' {0} />", genericPTGoals.Contains("19").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals19">Demonstrate safe and effective use of</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals19Within", data.AnswerOrEmptyString("POCGenericPTGoals19Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals19Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals19Within">DME within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals19Weeks", data.AnswerOrEmptyString("POCGenericPTGoals19Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals19Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals19Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals20' class='radio' name='{1}_POCGenericPTGoals' value='20' type='checkbox' {0} />", genericPTGoals.Contains("20").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals20">Patient will have increase in Tinetti Performance Oriented Mobility Assessment score to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals20Within", data.AnswerOrEmptyString("POCGenericPTGoals20Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals20Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals20Within">over 28 to reduce fall risk within </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals20Weeks", data.AnswerOrEmptyString("POCGenericPTGoals20Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals20Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals20Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals21' class='radio' name='{1}_POCGenericPTGoals' value='21' type='checkbox' {0} />", genericPTGoals.Contains("21").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals21">Patient will have improved </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals21Improve", data.AnswerOrEmptyString("POCGenericPTGoals21Improve"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals21Improve", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals21Improve">standardized test score to improve </label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals21Within", data.AnswerOrEmptyString("POCGenericPTGoals21Within"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals21Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals21Within">within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals21Weeks", data.AnswerOrEmptyString("POCGenericPTGoals21Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals21Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals21Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_POCGenericPTGoals22' class='radio' name='{1}_POCGenericPTGoals' value='22' type='checkbox' {0} />", genericPTGoals.Contains("22").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCGenericPTGoals22">Patient will have increase in Timed Up and Go score to</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals22Seconds", data.AnswerOrEmptyString("POCGenericPTGoals22Seconds"), new { @class = "", @id = Model.Type + "_POCGenericPTGoals22Seconds", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals22Seconds">seconds to reduce fall risk and improve mobility within</label>
                    <%= Html.TextBox(Model.Type + "_POCGenericPTGoals22Weeks", data.AnswerOrEmptyString("POCGenericPTGoals22Weeks"), new { @class = "numeric sn", @id = Model.Type + "_POCGenericPTGoals22Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_POCGenericPTGoals22Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        
        <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericPTGoalsComments">Additional goals</label><br />
                <%= Html.ToggleTemplates(Model.Type + "_POCGenericPTGoalsTemplates", "", "#" + Model.Type + "_POCGenericPTGoalsComments")%>
                <%= Html.TextArea(Model.Type + "_POCGenericPTGoalsComments", data.AnswerOrEmptyString("POCGenericPTGoalsComments"), new { @id = Model.Type + "_POCGenericPTGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericPTGoalsComments">PT Short Term Goals</label><br />
                <%= Html.ToggleTemplates(Model.Type + "_POCGenericPTShortTermGoalsTemplates", "", "#" + Model.Type + "_POCGenericPTShortTermGoalsComments")%>
                <%= Html.TextArea(Model.Type + "_POCGenericPTShortTermGoalsComments", data.AnswerOrEmptyString("POCGenericPTShortTermGoalsComments"), new { @id = Model.Type + "_POCGenericPTShortTermGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericPTLongTermGoalsComments">PT Long Term Goals</label><br />
                <%= Html.ToggleTemplates(Model.Type + "_POCGenericPTLongTermGoalsTemplates", "", "#" + Model.Type + "_POCGenericPTLongTermGoalsComments")%>
                <%= Html.TextArea(Model.Type + "_POCGenericPTLongTermGoalsComments", data.AnswerOrEmptyString("POCGenericPTLongTermGoalsComments"), new { @id = Model.Type + "_POCGenericPTLongTermGoalsComments", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input class='radio' id='{0}POCPTGoalsDesired0' name='{0}_POCPTGoalsDesired' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCPTGoalsDesired").Contains("0").ToChecked())%>Patient
                <%= string.Format("<input class='radio' id='{0}POCPTGoalsDesired1' name='{0}_POCPTGoalsDesired' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCPTGoalsDesired").Contains("1").ToChecked())%>Caregiver
                <span> desired outcomes:</span>
                <%= Html.TextBox(Model.Type + "_POCPTGoalsDesiredOutcomes", data.AnswerOrEmptyString("POCPTGoalsDesiredOutcomes"), new { @class = "", @id = Model.Type + "_POCPTGoalsDesiredOutcomes" })%> 
            </td>
        </tr>
        <%if (noteDiscipline == "47")
          { %>
         <tr>
            <td>
                <label for="<%= Model.Type %>_POCGenericRehabPotential">
                        Rehab Potential</label><br />
                <%= Html.TextArea(Model.Type + "_POCGenericRehabPotential", data.AnswerOrEmptyString("POCGenericRehabPotential"), new { @class = "fill", @id = Model.Type + "_POCGenericRehabPotential" }) %>
            </td>
        </tr> 
        <%} %>
    </tbody>
</table>