﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if(data.ContainsKey("IsBedApply")&&data.AnswerOrEmptyString("IsBedApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Bed Mobility");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Rolling to Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Rolling to Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityComment").Clean() %>",0,1),
        "Bed Mobility");
</script>
<%} %>