﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistive Device",true) +
            printview.span("% Assist",true) +
            printview.span("Rolling",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssist").Clean() %>%",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssist").Clean() %>%",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssist").Clean() %>%",0,1)) +
        "%3Ch3%3EGait%3C/h3%3E" +
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet").Clean() %> Feet",0,1) +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet").Clean() %> Feet",0,1) +
            printview.span("Step/ Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet").Clean() %> Feet",0,1)+
            printview.span("Comments",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment").Clean() %>",0,2)),
        "Bed Mobility",2);
</script>