﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <thead>
        <tr>
            <th colspan="2"></th>
            <th class="strong">Assistive Device</th>
            <th class="strong">% Assist</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilityRollingAssistiveDevice" class="strong">Rolling</label></td>
            <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilityRollingAssistiveDevice" })%></td>
            <td><%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "sn", @id = Model.Type+"_GenericBedMobilityRollingAssist" })%> %</td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistiveDevice" class="strong">Sit Stand Sit</label></td>
            <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice" })%></td>
            <td><%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssist"), new { @class = "sn", @id = Model.Type+"_GenericBedMobilitySitStandSitAssist" })%> %</td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistiveDevice" class="strong">Sup to Sit</label></td>
            <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice" })%></td>
            <td><%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssist"), new { @class = "sn", @id = Model.Type+"_GenericBedMobilitySupToSitAssist" })%> %</td>
        </tr>
    </tbody>
</table>