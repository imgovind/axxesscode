﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSensoryAcuity = data.AnswerArray("GenericSensoryAcuity"); %>
<%  string[] genericSensoryTracking = data.AnswerArray("GenericSensoryTracking"); %>
<%  string[] genericSensoryVisual = data.AnswerArray("GenericSensoryVisual"); %>

<script type="text/javascript">
    printview.addsection(
        printview.col(7,
            printview.span("")+
            printview.span("Sharp/Dull",true)+
            printview.span("")+
            printview.span("Light/Firm Touch",true)+
            printview.span("")+
            printview.span("Proprioception",true)+
            printview.span("")+
            printview.span("Area",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
                printview.span("Right",true)+
                printview.span("Left",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea1").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft1").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea2").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft2").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea3").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft3").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensoryArea4").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensorySharpLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryLightLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionRight4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft4").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1)),
            "Sensory/Perceptual Skills");
            
    printview.addsection(
        printview.span("Visual Skills:Acuity",true)+
        printview.col(4,
            printview.checkbox("Intact",<%= genericSensoryAcuity.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Impaired",<%= genericSensoryAcuity.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Double",<%= genericSensoryAcuity.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Blurred",<%= genericSensoryAcuity.Contains("4").ToString().ToLower() %>)) +
        printview.span("Tracking:",true)+
        printview.col(5,
            printview.checkbox("Unilaterally",<%= genericSensoryTracking.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bilagterally",<%= genericSensoryTracking.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Smooth",<%= genericSensoryTracking.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Jumpy",<%= genericSensoryTracking.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Not Tracking",<%= genericSensoryTracking.Contains("5").ToString().ToLower() %>)) +
        printview.span("Visual Field Cut or Neglect Suspected:",true)+
        printview.col(2,
            printview.checkbox("Right",<%= genericSensoryVisual.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Left",<%= genericSensoryVisual.Contains("2").ToString().ToLower() %>)) +
        printview.span("Impacting Function?",true)+
        printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No(Specify):<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunctionSpecify").Clean() %>",<%= data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("0").ToString().ToLower() %>)) +
        printview.span("Referral Needed?",true)+
        printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No(Who contacted?):<%= data.AnswerOrEmptyString("GenericSensoryReferralNeededContact").Clean() %>",<%= data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("0").ToString().ToLower() %>)),        
        "");
</script>