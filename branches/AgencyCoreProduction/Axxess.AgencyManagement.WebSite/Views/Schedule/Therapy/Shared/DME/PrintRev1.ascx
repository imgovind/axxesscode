﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (data.AnswerOrEmptyString("POCIsDMEApply").Equals("1"))
   { %>

<script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "DME");
</script>

<%}
   else
   { %>

<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>

<%} %>