﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericOTGoals = data.AnswerArray("GenericOTGoals"); %>
<% var noteDiscipline = data.AnswerOrEmptyString("DisciplineTask"); %>
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals1' class='radio' name='{1}_GenericOTGoals' value='1' type='checkbox' {0} />", genericOTGoals.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals1">Patient will be able to perform upper body dressing with </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals1With", data.AnswerOrEmptyString("GenericOTGoals1With"), new { @class = "", @id = Model.Type + "_GenericOTGoals1With", @maxlength = "30" })%>
                    assist  utilizing
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals1Utilize", data.AnswerOrEmptyString("GenericOTGoals1Utilize"), new { @class = "", @id = Model.Type + "_GenericOTGoals1Utilize", @maxlength = "30" })%>
                    device within  
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals1Weeks", data.AnswerOrEmptyString("GenericOTGoals1Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals1Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals1Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals2' class='radio' name='{1}_GenericOTGoals' value='2' type='checkbox' {0} />", genericOTGoals.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals2">Patient will be able to perform lower body dressing with </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals2With", data.AnswerOrEmptyString("GenericOTGoals2With"), new { @class = "", @id = Model.Type + "_GenericOTGoals2With", @maxlength = "30" })%> 
                    assist utilizing 
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals2Utilize", data.AnswerOrEmptyString("GenericOTGoals2Utilize"), new { @class = "", @id = Model.Type + "_GenericOTGoals2Utilize", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals2Utilize">device within </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals2Weeks", data.AnswerOrEmptyString("GenericOTGoals2Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals2Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals2Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals3' class='radio' name='{1}_GenericOTGoals' value='3' type='checkbox' {0} />", genericOTGoals.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals3">Patient will be able to perform toilet hygiene with </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals3Assist", data.AnswerOrEmptyString("GenericOTGoals3Assist"), new { @class = "", @id = Model.Type + "_GenericOTGoals3Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals3Assist">assist within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals3Weeks", data.AnswerOrEmptyString("GenericOTGoals3Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals3Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals3Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals4' class='radio' name='{1}_GenericOTGoals' value='4' type='checkbox' {0} />", genericOTGoals.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals4">Patient will be able to perform grooming with</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals4Assist", data.AnswerOrEmptyString("GenericOTGoals4Assist"), new { @class = "", @id = Model.Type + "_GenericOTGoals4Assist", @maxlength = "30" })%> 
                    assist within 
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals4Weeks", data.AnswerOrEmptyString("GenericOTGoals4Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals4Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals4Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals5' class='radio' name='{1}_GenericOTGoals' value='5' type='checkbox' {0} />", genericOTGoals.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals5">Patient will be able to perform </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals5With", data.AnswerOrEmptyString("GenericOTGoals5With"), new { @class = "", @id = Model.Type + "_GenericOTGoals5With", @maxlength = "30" })%> 
                    (task)  with 
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals5Assist", data.AnswerOrEmptyString("GenericOTGoals5Assist"), new { @class = "", @id = Model.Type + "_GenericOTGoals5Assist", @maxlength = "30" })%> 
                     <label for="<%= Model.Type %>_GenericOTGoals5Assist">assist using </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals5Within", data.AnswerOrEmptyString("GenericOTGoals5Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals5Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals5Within">device within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals5Weeks", data.AnswerOrEmptyString("GenericOTGoals5Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals5Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals5Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals6' class='radio' name='{1}_GenericOTGoals' value='6' type='checkbox' {0} />", genericOTGoals.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals6">Improve strength of</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals6Of", data.AnswerOrEmptyString("GenericOTGoals6Of"), new { @class = "", @id = Model.Type + "_GenericOTGoals6Of", @maxlength = "30" })%> 
                    to
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals6To", data.AnswerOrEmptyString("GenericOTGoals6To"), new { @class = "", @id = Model.Type + "_GenericOTGoals6To", @maxlength = "30" })%> 
                    grade to improve
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals6Within", data.AnswerOrEmptyString("GenericOTGoals6Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals6Within", @maxlength = "30" })%> 
                    within
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals6Weeks", data.AnswerOrEmptyString("GenericOTGoals6Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals6Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals6Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals7' class='radio' name='{1}_GenericOTGoals' value='7' type='checkbox' {0} />", genericOTGoals.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals7">Increase muscle strength of </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals7Skill", data.AnswerOrEmptyString("GenericOTGoals7Skill"), new { @class = "", @id = Model.Type + "_GenericOTGoals7Skill", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals7Skill"> to </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals7Assist", data.AnswerOrEmptyString("GenericOTGoals7Assist"), new { @class = "", @id = Model.Type + "_GenericOTGoals7Assist", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals7Assist">grade to improve </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals7Within", data.AnswerOrEmptyString("GenericOTGoals7Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals7Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals7Within">(task) within </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals7Weeks", data.AnswerOrEmptyString("GenericOTGoals7Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals7Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals7Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals8' class='radio' name='{1}_GenericOTGoals' value='8' type='checkbox' {0} />", genericOTGoals.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals8">Increase trunk muscle strength to </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals8To", data.AnswerOrEmptyString("GenericOTGoals8To"), new { @class = "", @id = Model.Type + "_GenericOTGoals8To", @maxlength = "30" })%> 
                    to improve postural control and balance necessary to perform
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals8Within", data.AnswerOrEmptyString("GenericOTGoals8Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals8Within", @maxlength = "30" })%> 
                    (task) within
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals8Weeks", data.AnswerOrEmptyString("GenericOTGoals8Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals8Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals8Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals9' class='radio' name='{1}_GenericOTGoals' value='9' type='checkbox' {0} />", genericOTGoals.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals9">Increase trunk muscle strength to</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals9Within", data.AnswerOrEmptyString("GenericOTGoals9Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals9Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals9Within">to improve postural control during bed mobility and transfer within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals9Weeks", data.AnswerOrEmptyString("GenericOTGoals9Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals9Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals9Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals10' class='radio' name='{1}_GenericOTGoals' value='10' type='checkbox' {0} />", genericOTGoals.Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals10">Patient will increase ROM of </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals10Device", data.AnswerOrEmptyString("GenericOTGoals10Device"), new { @class = "", @id = Model.Type + "_GenericOTGoals10Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals10Device">joint to</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals10Feet", data.AnswerOrEmptyString("GenericOTGoals10Feet"), new { @class = "sn", @id = Model.Type + "_GenericOTGoals10Feet", @maxlength = "10" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals10Feet">degree of</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals10Within", data.AnswerOrEmptyString("GenericOTGoals10Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals10Within", @maxlength = "30" })%> 
                    within
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals10Weeks", data.AnswerOrEmptyString("GenericOTGoals10Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals10Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals10Weeks">weeks to improve </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals10DegreeOf", data.AnswerOrEmptyString("GenericOTGoals10DegreeOf"), new { @class = "", @id = Model.Type + "_GenericOTGoals10DegreeOf", @maxlength = "30" })%> 
                    (task).
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals11' class='radio' name='{1}_GenericOTGoals' value='11' type='checkbox' {0} />", genericOTGoals.Contains("11").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals11">Patient/caregiver will be able to perform HEP safely and effectively within  </label>                    
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals11Weeks", data.AnswerOrEmptyString("GenericOTGoals11Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals11Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals11Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals12' class='radio' name='{1}_GenericOTGoals' value='12' type='checkbox' {0} />", genericOTGoals.Contains("12").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals12">Patient will learn</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals12Device", data.AnswerOrEmptyString("GenericOTGoals12Device"), new { @class = "", @id = Model.Type + "_GenericOTGoals12Device", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals12Device">techniques to</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals12Within", data.AnswerOrEmptyString("GenericOTGoals12Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals12Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals12Within">within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals12Weeks", data.AnswerOrEmptyString("GenericOTGoals12Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals12Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals12Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals13' class='radio' name='{1}_GenericOTGoals' value='13' type='checkbox' {0} />", genericOTGoals.Contains("13").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals13">Patient will improve</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals13Of", data.AnswerOrEmptyString("GenericOTGoals13Of"), new { @class = "", @id = Model.Type + "_GenericOTGoals13Of", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals13Of">standardized test score to</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals13To", data.AnswerOrEmptyString("GenericOTGoals13To"), new { @class = "", @id = Model.Type + "_GenericOTGoals13To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals13To">to improve</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals13Within", data.AnswerOrEmptyString("GenericOTGoals13Within"), new { @class = "", @id = Model.Type + "_GenericOTGoals13Within", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals13Within">within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals13Weeks", data.AnswerOrEmptyString("GenericOTGoals13Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals13Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals13Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals14' class='radio' name='{1}_GenericOTGoals' value='14' type='checkbox' {0} />", genericOTGoals.Contains("14").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals14">Patient/caregiver will demonstrate proper use of brace/splint within </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals14Weeks", data.AnswerOrEmptyString("GenericOTGoals14Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals14Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals14Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals15' class='radio' name='{1}_GenericOTGoals' value='15' type='checkbox' {0} />", genericOTGoals.Contains("15").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals15">Patient/caregiver will demonstrate proper use of wheelchair with</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals15To", data.AnswerOrEmptyString("GenericOTGoals15To"), new { @class = "", @id = Model.Type + "_GenericOTGoals15To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals15Feet">assist within</label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals15Weeks", data.AnswerOrEmptyString("GenericOTGoals15Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals15Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals15Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals16' class='radio' name='{1}_GenericOTGoals' value='16' type='checkbox' {0} />", genericOTGoals.Contains("16").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals16">Patient will be able to perform upper body bathing with  </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals16With", data.AnswerOrEmptyString("GenericOTGoals16With"), new { @class = "", @id = Model.Type + "_GenericOTGoals16With", @maxlength = "30" })%> 
                    assist utilizing
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals16To", data.AnswerOrEmptyString("GenericOTGoals16To"), new { @class = "", @id = Model.Type + "_GenericOTGoals16To", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals16Feet">device within </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals16Weeks", data.AnswerOrEmptyString("GenericOTGoals16Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals16Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals16Weeks">weeks.</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <%= string.Format("<input id='{1}_GenericOTGoals17' class='radio' name='{1}_GenericOTGoals' value='17' type='checkbox' {0} />", genericOTGoals.Contains("17").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericOTGoals17">Patient will be able to perform lower body bathing with </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals17Joint", data.AnswerOrEmptyString("GenericOTGoals17Joint"), new { @class = "", @id = Model.Type + "_GenericOTGoals17Joint", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals17Joint">assist utilizing  </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals17Degree", data.AnswerOrEmptyString("GenericOTGoals17Degree"), new { @class = "", @id = Model.Type + "_GenericOTGoals17Degree", @maxlength = "30" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals17Degree">device within </label>
                    <%= Html.TextBox(Model.Type + "_GenericOTGoals17Weeks", data.AnswerOrEmptyString("GenericOTGoals17Weeks"), new { @class = "numeric sn", @id = Model.Type + "_GenericOTGoals17Weeks", @maxlength="3" })%> 
                    <label for="<%= Model.Type %>_GenericOTGoals17Weeks">weeks</label>
               </div>
            </td>
        </tr>
        <tr>
            <td>Frequency:<%= Html.TextBox(Model.Type + "_GenericOTGoalsFrequency", data.AnswerOrEmptyString("GenericOTGoalsFrequency"), new { @class = "", @id = Model.Type + "_GenericOTGoalsFrequency", @maxlength = "50" })%>
            Duration:<%= Html.TextBox(Model.Type + "_GenericOTGoalsDuration", data.AnswerOrEmptyString("GenericOTGoalsDuration"), new { @class = "", @id = Model.Type + "_GenericOTGoalsDuration", @maxlength = "50" })%> </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GenericOTGoalsComments">Additional goals</label><br />
                <%= Html.TextArea(Model.Type + "_GenericOTGoalsComments", data.AnswerOrEmptyString("GenericOTGoalsComments"), new { @id = Model.Type + "_GenericOTGoalsComments", @class = "fill" })%>
            </td>
        </tr>
         <tr>
            <td>
                <label for="<%= Model.Type %>_GenericRehabPotential">
                        Rehab Potential</label><br />
                <%= Html.TextArea(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @class = "fill", @id = Model.Type + "_GenericRehabPotential" }) %>
            </td>
        </tr> 
    </tbody>
</table>