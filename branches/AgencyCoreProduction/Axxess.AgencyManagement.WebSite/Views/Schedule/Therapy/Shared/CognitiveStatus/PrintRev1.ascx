﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Memory:Short term",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionShortTerm").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Sequencing",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSequencing").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Memory:Long term",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionLongTerm").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Problem Solving",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionProblemSolving").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Attention/Concentration",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionConcentration").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Coping Skills",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionCopingSkills").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Auditory Comprehension",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionAuditory").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Able to Express Needs",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionExpressNeeds").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Visual Comprehension",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionVisual").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Safety/Judgment",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSafety").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Self-Control",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionSelfControl").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("Initiation of Activity",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionInitiation").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1))+
            printview.span("Comments",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericComprehensionComment").Clean() %>",0,1),
            "Cognitive Status/Comprehension");
            
</script>