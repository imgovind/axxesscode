﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <tbody>
        <tr>
            <th>Deficit Area</th>
            <th></th>
            <th>Deficit Area</th>
            <th></th>
        </tr>
        <tr>
            <td class="align-left">MEMORY:Short term</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionShortTerm", data.AnswerOrEmptyString("GenericComprehensionShortTerm"), new { @id = Model.Type + "_GenericComprehensionShortTerm", @class = "fill" })%></td>
            <td class="align-left">Sequencing</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionSequencing", data.AnswerOrEmptyString("GenericComprehensionSequencing"), new { @id = Model.Type + "_GenericComprehensionSequencing", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left">MEMORY:Long term</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionLongTerm", data.AnswerOrEmptyString("GenericComprehensionLongTerm"), new { @id = Model.Type + "_GenericComprehensionLongTerm", @class = "fill" })%></td>
            <td class="align-left">Problem Solving</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionProblemSolving", data.AnswerOrEmptyString("GenericComprehensionProblemSolving"), new { @id = Model.Type + "_GenericComprehensionProblemSolving", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left">Attention/Concentration</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionConcentration", data.AnswerOrEmptyString("GenericComprehensionConcentration"), new { @id = Model.Type + "_GenericComprehensionConcentration", @class = "fill" })%></td>
            <td class="align-left">Coping Skills</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionCopingSkills", data.AnswerOrEmptyString("GenericComprehensionCopingSkills"), new { @id = Model.Type + "_GenericComprehensionCopingSkills", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left">Auditory Comprehension</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionAuditory", data.AnswerOrEmptyString("GenericComprehensionAuditory"), new { @id = Model.Type + "_GenericComprehensionAuditory", @class = "fill" })%></td>
            <td class="align-left">Able to Express Needs</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionExpressNeeds", data.AnswerOrEmptyString("GenericComprehensionExpressNeeds"), new { @id = Model.Type + "_GenericComprehensionExpressNeeds", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left">Visual Comprehension</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionVisual", data.AnswerOrEmptyString("GenericComprehensionVisual"), new { @id = Model.Type + "_GenericComprehensionVisual", @class = "fill" })%></td>
            <td class="align-left">Safety/Judgment</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionSafety", data.AnswerOrEmptyString("GenericComprehensionSafety"), new { @id = Model.Type + "_GenericComprehensionSafety", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left">Self-Control</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionSelfControl", data.AnswerOrEmptyString("GenericComprehensionSelfControl"), new { @id = Model.Type + "_GenericComprehensionSelfControl", @class = "fill" })%></td>
            <td class="align-left">Initiation of Activity</td>
            <td><%= Html.Sensory(Model.Type + "_GenericComprehensionInitiation", data.AnswerOrEmptyString("GenericComprehensionInitiation"), new { @id = Model.Type + "_GenericComprehensionInitiation", @class = "fill" })%></td>
        </tr>
        <tr>
            <td colspan="4">Comments
            <%= Html.ToggleTemplates(Model.Type + "_GenericComprehensionTemplates", "", "#" + Model.Type + "_GenericComprehensionComment")%>
            </td>
        </tr>
        <tr>
            <td colspan="4"><%= Html.TextArea(Model.Type + "_GenericComprehensionComment", data.AnswerOrEmptyString("GenericComprehensionComment"),4,20, new { @id = Model.Type + "_GenericComprehensionComment", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>