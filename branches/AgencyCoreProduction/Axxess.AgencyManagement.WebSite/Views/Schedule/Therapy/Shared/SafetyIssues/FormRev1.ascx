﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>SafetyIssuesInstructionContainer">
    <%= Html.ToggleTemplates(Model.Type + "_POCGenericSafetyIssueTemplates", "", "#" + Model.Type + "_POCGenericSafetyIssue")%>
    <%= Html.TextArea(Model.Type + "_POCGenericSafetyIssue", data.ContainsKey("POCGenericSafetyIssue") ? data["POCGenericSafetyIssue"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericSafetyIssue", @class = "fill" })%>
</div>
