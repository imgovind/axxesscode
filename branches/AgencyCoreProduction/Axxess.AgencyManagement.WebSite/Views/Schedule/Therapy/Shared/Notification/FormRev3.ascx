﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="float-left">
    <%= string.Format("<input class='radio' id='{0}POCNotificationUnderstands0' name='{0}_POCNotificationUnderstands' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("0").ToChecked())%>Patient
    <%= string.Format("<input class='radio' id='{0}POCNotificationUnderstands1' name='{0}_POCNotificationUnderstands' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("1").ToChecked())%>Caregiver

understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)  
</div>
<%= Html.RadioButton(Model.Type + "_POCNotificationPeopleAnswer", "1", data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Equals("1"), new { @id = Model.Type + "_POCNotificationPeopleAnswer1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_POCNotificationPeopleAnswer1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_POCNotificationPeopleAnswer", "0", data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Equals("0"), new { @id = Model.Type + "_POCNotificationPeopleAnswer0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_POCNotificationPeopleAnswer0" class="inline-radio">No</label>
                    
<div class="clear" />
<div class="float-left">
    <%= string.Format("<input class='radio' id='{0}POCNotificationPhysician0' name='{0}_POCNotificationPhysician' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCNotificationPhysician").Contains("0").ToChecked())%>
<label>Physician notified and agrees with POC, frequency and duration.  Comments (if any): </label>
<%= Html.TextBox(Model.Type + "_POCNotificationPhysicianComment", data.AnswerOrEmptyString("POCNotificationPhysicianComment"), new { @class = "", @id = Model.Type + "_POCNotificationPhysicianComment" })%>
</div>