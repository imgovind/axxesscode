﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="halfOfTd">
    <%= string.Format("<input class='radio' id='{0}Notification0' name='{0}_Notification' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Notification").Contains("0").ToChecked())%>
<label>Patient agreed to poc</label>
</div>
<div class="halfOfTd">
    <%= string.Format("<input class='radio' id='{0}Notification1' name='{0}_Notification' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Notification").Contains("1").ToChecked())%>
<label>Verbal order received from physician</label>
</div>