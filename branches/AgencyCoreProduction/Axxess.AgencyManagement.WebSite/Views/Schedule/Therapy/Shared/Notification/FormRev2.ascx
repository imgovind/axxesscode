﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="float-left">
    <%= string.Format("<input class='radio' id='{0}NotificationUnderstands0' name='{0}_NotificationUnderstands' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NotificationUnderstands").Contains("0").ToChecked())%>Patient
    <%= string.Format("<input class='radio' id='{0}NotificationUnderstands1' name='{0}_NotificationUnderstands' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NotificationUnderstands").Contains("1").ToChecked())%>Caregiver

understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)  
</div>
<%= Html.RadioButton(Model.Type + "_NotificationPeopleAnswer", "1", data.AnswerOrEmptyString("NotificationPeopleAnswer").Equals("1"), new { @id = Model.Type + "_NotificationPeopleAnswer1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_NotificationPeopleAnswer1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_NotificationPeopleAnswer", "0", data.AnswerOrEmptyString("NotificationPeopleAnswer").Equals("0"), new { @id = Model.Type + "_NotificationPeopleAnswer0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_NotificationPeopleAnswer0" class="inline-radio">No</label>
                    
<div class="clear" />
<div class="float-left">
    <%= string.Format("<input class='radio' id='{0}NotificationPhysician0' name='{0}_NotificationPhysician' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NotificationPhysician").Contains("0").ToChecked())%>
<label>Physician notified and agrees with POC, frequency and duration.  Comments (if any): </label>
<%= Html.TextBox(Model.Type + "_NotificationPhysicianComment", data.AnswerOrEmptyString("NotificationPhysicianComment"), new { @class = "", @id = Model.Type + "_NotificationPhysicianComment" })%>
</div>