﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <thead>
        <tr>
            <th colspan="2"></th>
            <th class="strong">Time Frame</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal1", data.AnswerOrEmptyString("GenericShortTermGoal1"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal1" }) %></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal1TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal1TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal1TimeFrame" })%></td>
        </tr>
        <tr>
            <td>2.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal2", data.AnswerOrEmptyString("GenericShortTermGoal2"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal2" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal2TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal2TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal2TimeFrame" })%></td>
        </tr>
        <tr>
            <td>3.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal3", data.AnswerOrEmptyString("GenericShortTermGoal3"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal3" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal3TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal3TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal3TimeFrame" })%></td>
        </tr>
        <tr>
            <td>4.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal4", data.AnswerOrEmptyString("GenericShortTermGoal4"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal4" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal4TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal4TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal4TimeFrame" })%></td>
        </tr>
        <tr>
            <td>5.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal5", data.AnswerOrEmptyString("GenericShortTermGoal5"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal5" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShortTermGoal5TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal5TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal5TimeFrame" })%></td>
        </tr>
    </tbody>
</table>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericShortTermFrequency" class="float-left">Frequency:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericShortTermFrequency", data.AnswerOrEmptyString("GenericShortTermFrequency"), new { @id = Model.Type + "_GenericShortTermFrequency" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericShortTermDuration" class="float-left">Duration:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericShortTermDuration", data.AnswerOrEmptyString("GenericShortTermDuration"), new { @class = "", @id = Model.Type + "_GenericShortTermDuration" })%></div>
</div>