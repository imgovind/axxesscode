﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
<% if(data.ContainsKey("IsTransferApply") && data.AnswerOrEmptyString("IsTransferApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Transfer Training");
<%}else{ %>
    printview.addsection(
        printview.col(3,
        printview.span("Transfer Training",true)+
        printview.span("x <%= data.AnswerOrEmptyString("GenericTransferTraining").Clean() %> reps", 0, 1) +
        printview.span("&#160;") +
        printview.span("Assistive Device",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericTransferTrainingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean()%>",0,1) +    
        printview.span("&#160;") + 
        printview.span("Bed -- Chair",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericBedChairAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +    
        printview.span("&#160;") + 
        printview.span("Chair -- Toilet",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericChairToiletAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +    
        printview.span("&#160;") + 
        printview.span("Chair -- Car",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericChairCarAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +    
        printview.span("&#160;") + 
        printview.span("Sitting Balance Activities",true)+
        printview.span("Static:<%= data.AnswerOrEmptyString("GenericSittingStaticAssist").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +    
        printview.span("Dynamic:<%= data.AnswerOrEmptyString("GenericSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean()%>",0,1) +    
        printview.span("Standing Balance Activities",true)+
        printview.span("Static:<%= data.AnswerOrEmptyString("GenericStandingStaticAssist").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +    
        printview.span("Dynamic:<%= data.AnswerOrEmptyString("GenericStandingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean()%>",0,1))+
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericTransferTrainingComment").Clean() %>",0,1),
        "Transfer Training");
<%} %>
</script>   
        