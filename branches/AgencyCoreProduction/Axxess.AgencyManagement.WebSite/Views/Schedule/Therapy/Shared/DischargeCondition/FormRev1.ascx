﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericConditionForDischarge = data.AnswerArray("GenericConditionForDischarge"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericConditionForDischarge" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericConditionForDischarge1' class='radio' name='{1}_GenericConditionForDischarge' value='1' type='checkbox' {0} />", genericConditionForDischarge.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericConditionForDischarge1">Totally dependent for care</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericConditionForDischarge2' class='radio' name='{1}_GenericConditionForDischarge' value='2' type='checkbox' {0} />", genericConditionForDischarge.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericConditionForDischarge2">Returned to optimum level of independence</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericConditionForDischarge3' class='radio' name='{1}_GenericConditionForDischarge' value='3' type='checkbox' {0} />", genericConditionForDischarge.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericConditionForDischarge3">Inappropriate for home care</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericConditionForDischarge4' class='radio' name='{1}_GenericConditionForDischarge' value='4' type='checkbox' {0} />", genericConditionForDischarge.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericConditionForDischarge4">Rehabilitated to maximum potential</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericConditionForDischarge5' class='radio' name='{1}_GenericConditionForDischarge' value='5' type='checkbox' {0} />", genericConditionForDischarge.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericConditionForDischarge5">Returned to self/family care</label>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericConditionForDischargeOther" class="float-left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericConditionForDischargeOther", data.AnswerOrEmptyString("GenericConditionForDischargeOther"), new { @class = "", @id = Model.Type+"_GenericConditionForDischargeOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>

