﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Total dependent for care",<%= data.AnswerOrEmptyString("GenericConditionForDischarge").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Returned to optimum level of independence",<%= data.AnswerOrEmptyString("GenericConditionForDischarge").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Inappropriate for home care",<%= data.AnswerOrEmptyString("GenericConditionForDischarge").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Rehabilitated to maximum potential",<%= data.AnswerOrEmptyString("GenericConditionForDischarge").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Returned to self/family care",<%= data.AnswerOrEmptyString("GenericConditionForDischarge").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E <%= data.AnswerOrEmptyString("GenericConditionForDischargeOther").Clean() %>",0,1)),
        "Condition for Discharge");
</script>

