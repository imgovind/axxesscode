﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("I = Independent", true) +
            printview.span("S = Supervision", true) +
            printview.span("VC = Verbal Cue", true) +
            printview.span("CGA = Contact Guard Assist", true) +
            printview.span("Min A = 25% Assist", true) +
            printview.span("Mod A = 50% Assist", true) +
            printview.span("Max A = 75% Assist", true) +
            printview.span("Total = 100% Assist", true)),
            "Functional Mobility Key");
</script>   