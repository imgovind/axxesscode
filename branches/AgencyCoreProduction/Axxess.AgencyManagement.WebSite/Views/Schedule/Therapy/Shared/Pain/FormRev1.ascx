﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div>
    <label for="<%= Model.Type %>_GenericPainLocation" class="float-left">Pain Location</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.AnswerOrEmptyString("GenericPainAssessmentLocation"), new { @id = Model.Type + "_GenericPainAssessmentLocation" }) %></div>
</div>
<div class="clear"></div>
<div>
    <label for="<%= Model.Type %>_GenericPainLevel" class="float-left">Pain Level</label>
    <div class="float-right">
        <%  var genericPainLevel = new SelectList(new[] {
                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                new SelectListItem { Text = "1", Value = "1" },
                new SelectListItem { Text = "2", Value = "2" },
                new SelectListItem { Text = "3", Value = "3" },
                new SelectListItem { Text = "4", Value = "4" },
                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                new SelectListItem { Text = "6", Value = "6" },
                new SelectListItem { Text = "7", Value = "7" },
                new SelectListItem { Text = "8", Value = "8" },
                new SelectListItem { Text = "9", Value = "9" },
                new SelectListItem { Text = "10", Value = "10" }
            }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", "0"));%>
        <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <div class="row">
        <img src="/Images/painscale.png" alt="Pain Scale Image" width="90%" /><br />
        <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
    </div>
    <div class="halfOfTd">
    <label for="<%= Model.Type %>_GenericPainAssessmentIncreasedBy" class="float-left">Increased by:</label>
    <%= Html.TextBox(Model.Type + "_GenericPainAssessmentIncreasedBy", data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy"), new { @id = Model.Type + "_GenericPainAssessmentIncreasedBy" })%>
    </div>
    <div class="halfOfTd">
    <label for="<%= Model.Type %>_GenericPainAssessmentRelievedBy" class="float-left">Relieved by:</label>
    <%= Html.TextBox(Model.Type + "_GenericPainAssessmentRelievedBy", data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy"), new { @id = Model.Type + "_GenericPainAssessmentRelievedBy" })%>
    </div>
</div>