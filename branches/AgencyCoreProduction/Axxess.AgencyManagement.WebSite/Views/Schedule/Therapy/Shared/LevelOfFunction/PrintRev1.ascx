﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if(data.ContainsKey("IsLOFApply")&&data.AnswerOrEmptyString("IsLOFApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Prior And Current Level Of Funtion");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Prior Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorFunctionalStatus").Clean() %>",0,1) +
        
        printview.span("Current Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericCurrentFunctionalStatus").Clean() %>",0,1),
       
        "Prior And Current Level Of Funtion");
</script>

<%} %>