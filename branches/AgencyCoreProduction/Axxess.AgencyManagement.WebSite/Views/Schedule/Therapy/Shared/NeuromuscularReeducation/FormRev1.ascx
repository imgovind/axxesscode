﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>

                <table>
                    <tbody>
                        <tr>
                            <th></th>
                            <th class="strong">Static</th>
                            <th class="strong">Dynamic</th>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="float-left">
                        Sitting Balance</label></td>
                            <td><%= Html.StaticBalance(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", @class = "fill" })%></td>
                            <td><%= Html.DynamicBalance(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <td><label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="float-left">
                        Standing Balance</label></td>
                            <td><%= Html.StaticBalance(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericStandingBalanceActivitiesStaticAssist"), new { @id = Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", @class = "fill" })%></td>
                            <td><%= Html.DynamicBalance(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericStandingBalanceActivitiesDynamicAssist"), new { @id = Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
                
               
                
                <div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
                    <%= string.Format("<input id='{1}_GenericUEWeightBearing1' class='radio' name='{1}_GenericUEWeightBearing' value='1' type='checkbox' {0} />", genericUEWeightBearing.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericUEWeightBearing1">
                        UE Weight-Bearing Activities</label>
                </div>