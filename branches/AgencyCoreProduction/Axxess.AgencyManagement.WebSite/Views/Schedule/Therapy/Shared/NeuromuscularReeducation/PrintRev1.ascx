﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("")+
            printview.span("Static",true)+
            printview.span("Dynamic",true)+
            printview.span("Sitting Balance",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Standing Balance",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesStaticAssist").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1))+
            printview.checkbox("UE Weight-Bearing Activities",<%= data.AnswerOrEmptyString("GenericUEWeightBearing").Contains("1").ToString().ToLower() %>),
            "Neuromuscular Reeducation");
    
</script>