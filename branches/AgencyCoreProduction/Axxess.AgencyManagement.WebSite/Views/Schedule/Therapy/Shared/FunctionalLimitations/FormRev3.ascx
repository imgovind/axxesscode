﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
<table>
    <tbody>
        <tr>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations1' class='radio' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations1">
                    Upper body Rom/strength deficit.</label>
            </td>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations2' class='radio' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations2">
                    Pain affecting function.</label>
            </td>
        </tr>
        <tr>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations3' class='radio' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations3">
                    Impaired safety.</label>
            </td>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations4' class='radio' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations4">
                    Difficulty with dressing/grooming/bathing/hygiene/toileting.</label>
            </td>
        </tr>
        <tr>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations5' class='radio' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations5">
                    Difficulty with homemaking skills/money management/meal prep/laundry.</label>
            </td>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations6' class='radio' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations6">
                    Impaired problem solving skills/attention/concentration/sequencing.</label>
            </td>
        </tr>
        <tr>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations7' class='radio' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations7">
                    Impaired coordination.</label>
            </td>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations8' class='radio' name='{1}_GenericFunctionalLimitations' value='8' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations8">
                    Visual deficit/disturbance/limitation.</label>
            </td>
        </tr>
        <tr>
            <td class="align-left">
                <%= string.Format("<input id='{1}_GenericFunctionalLimitations9' class='radio' name='{1}_GenericFunctionalLimitations' value='9' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations9">
                    Cognition (memory, safety awareness, judgement).</label>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
