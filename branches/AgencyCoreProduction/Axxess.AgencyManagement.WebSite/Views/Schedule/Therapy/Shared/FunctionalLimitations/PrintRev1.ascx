﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("Transfer",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Gait",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Strength",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Safety Techniques",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("ADL&#8217;s",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("ROM",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.checkbox("W/C Mobility",<%= data.AnswerOrEmptyString("GenericFunctionalLimitations").Split(',').Contains("8").ToString().ToLower() %>) +
            printview.span("Other") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFunctionalLimitationsOther").Clean()%>",0,1)),
        "Functional Limitations");
</script>