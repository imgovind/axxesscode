﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>

<% if (data.AnswerOrEmptyString("POCIsCareApply").Equals("1"))
   {%>

<script type="text/javascript">
        printview.addsubsection(
        printview.checkbox("N/A", true),
        "Care Coordination", 2);
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericCareCoordination").Clean() %>",0,3),"Care Coordination");
</script>

<%} %>