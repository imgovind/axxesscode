﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>CareContainer">
    <%= Html.ToggleTemplates(Model.Type + "_POCGenericCareCoordinationTemplates", "", "#" + Model.Type + "_POCGenericCareCoordination")%>
    <%= Html.TextArea(Model.Type + "_POCGenericCareCoordination", data.ContainsKey("POCGenericCareCoordination") ? data["POCGenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericCareCoordination", @class = "fill" })%>
</div>
