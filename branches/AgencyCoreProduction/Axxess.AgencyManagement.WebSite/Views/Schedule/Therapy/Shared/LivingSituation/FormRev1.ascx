﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
    <label class="float-left">Dwelling Level</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel1" class="inline-radio">One</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel0" class="inline-radio">Multiple</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Stairs</label>
                <div class="float-right">
                    <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left"># Steps</label>
                    <%= Html.TextBox(Model.Type + "_GenericHomeSafetyEvaluationStairsNumber", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairsNumber"), new { @class = "vitals", @id = Model.Type + "_GenericHomeSafetyEvaluationStairsNumber" })%>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs0" class="inline-radio">No</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Lives with</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "2", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives2", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives2" class="inline-radio">Alone</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives1" class="inline-radio">Family</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives0" class="inline-radio">Friends</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "3", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("3"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives3", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives3" class="inline-radio">Significant Other</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Support:</label>
                <table class="fixed align-left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericLivingSituationSupport1' class='radio' name='{1}_GenericLivingSituationSupport' value='1' type='checkbox' {0} />", genericLivingSituationSupport.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLivingSituationSupport1">Willing caregiver available</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericLivingSituationSupport2' class='radio' name='{1}_GenericLivingSituationSupport' value='2' type='checkbox' {0} />", genericLivingSituationSupport.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLivingSituationSupport2">Limited caregiver support</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericLivingSituationSupport3' class='radio' name='{1}_GenericLivingSituationSupport' value='3' type='checkbox' {0} />", genericLivingSituationSupport.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLivingSituationSupport3">No caregiver available</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="<%= Model.Type %>_GenericHomeSafetyBarriers" class="float-left">Home Safety Barriers:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericHomeSafetyBarriers", data.AnswerOrEmptyString("GenericHomeSafetyBarriers"), new { @class = "", @id = Model.Type + "_GenericHomeSafetyBarriers" })%></div>
                <div class="clear"></div>