﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <thead>
        <tr>
            <th class="strong">I.FUNCTIONAL MOBILITY</th>
            <th class="strong" colspan="2">Assistance</th>
            <th class="strong" colspan="2">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLBedMobility" class="float-left">Bed mobility</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLBedMobilityAssistance", data.AnswerOrEmptyString("GenericADLBedMobilityAssistance"), new { @id = Model.Type + "_GenericADLBedMobilityAssistance", @class = "fill" })%></td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLBedMobilityAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice"), new { @id = Model.Type + "_GenericADLBedMobilityAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLBedWCTransfers" class="float-left">Bed/WC transfers</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLBedWCTransfersAssistance", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance"), new { @id = Model.Type + "_GenericADLBedWCTransfersAssistance", @class = "fill" })%></td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLBedWCTransfersAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice"), new { @id = Model.Type + "_GenericADLBedWCTransfersAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLToiletTransfer" class="float-left">Toilet transfer</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletTransferAssistance", data.AnswerOrEmptyString("GenericADLToiletTransferAssistance"), new { @id = Model.Type + "_GenericADLToiletTransferAssistance", @class = "fill" })%></td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice"), new { @id = Model.Type + "_GenericADLToiletTransferAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLTubShowerTransfer" class="float-left">Tub/shower transfer</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance"), new { @id = Model.Type + "_GenericADLTubShowerTransferAssistance", @class = "fill" })%></td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice"), new { @id = Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Comment</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="5">
                <%= Html.TextArea(Model.Type + "_ADLFunctionalComment", data.AnswerOrEmptyString("ADLFunctionalComment"), new { @class = "fill", @id = Model.Type + "_ADLFunctionalComment" })%>
            </td>
        </tr>
    </tbody>
</table>

<table class="fixed">
    <thead>
        <tr>
            <th class="strong">II.SELFCARE/ADL SKILLS</th>
            <th class="strong" colspan="2">Assistance x Reps</th>
            <th class="strong" colspan="2">Assistive Device x Reps</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLSelfFeeding" class="float-left">Self Feeding</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLSelfFeedingAssistance", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance"), new { @id = Model.Type + "_GenericADLSelfFeedingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLSelfFeedingAssistanceReps", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLSelfFeedingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLSelfFeedingAssistiveDevice", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice"), new { @id = Model.Type + "_GenericADLSelfFeedingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLSelfFeedingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLSelfFeedingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLOralHygiene" class="float-left">Oral Hygiene</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLOralHygieneAssistance", data.AnswerOrEmptyString("GenericADLOralHygieneAssistance"), new { @id = Model.Type + "_GenericADLOralHygieneAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLOralHygieneAssistanceReps", data.AnswerOrEmptyString("GenericADLOralHygieneAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLOralHygieneAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLOralHygieneAssistiveDevice", data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDevice"), new { @id = Model.Type + "_GenericADLOralHygieneAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLOralHygieneAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLOralHygieneAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLGroomingAssistance", data.AnswerOrEmptyString("GenericADLGroomingAssistance"), new { @id = Model.Type + "_GenericADLGroomingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLGroomingAssistanceReps", data.AnswerOrEmptyString("GenericADLGroomingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLGroomingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLGroomingAssistiveDevice", data.AnswerOrEmptyString("GenericADLGroomingAssistiveDevice"), new { @id = Model.Type + "_GenericADLGroomingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLGroomingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLGroomingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLGroomingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletingAssistance", data.AnswerOrEmptyString("GenericADLToiletingAssistance"), new { @id = Model.Type + "_GenericADLToiletingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLToiletingAssistanceReps", data.AnswerOrEmptyString("GenericADLToiletingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLToiletingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLToiletingAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletingAssistiveDevice"), new { @id = Model.Type + "_GenericADLToiletingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLToiletingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLToiletingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLToiletingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLUBBathing" class="float-left">UB bathing</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLUBBathingAssistance", data.AnswerOrEmptyString("GenericADLUBBathingAssistance"), new { @id = Model.Type + "_GenericADLUBBathingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLUBBathingAssistanceReps", data.AnswerOrEmptyString("GenericADLUBBathingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLUBBathingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice"), new { @id = Model.Type + "_GenericADLUBBathingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLUBBathingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLUBBathingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLBBathing" class="float-left">LB bathing</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLLBBathingAssistance", data.AnswerOrEmptyString("GenericADLLBBathingAssistance"), new { @id = Model.Type + "_GenericADLLBBathingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLBBathingAssistanceReps", data.AnswerOrEmptyString("GenericADLLBBathingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLBBathingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice"), new { @id = Model.Type + "_GenericADLLBBathingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLBBathingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLBBathingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLUBdressing" class="float-left">UB dressing</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLUBdressingAssistance", data.AnswerOrEmptyString("GenericADLUBdressingAssistance"), new { @id = Model.Type + "_GenericADLUBdressingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLUBdressingAssistanceReps", data.AnswerOrEmptyString("GenericADLUBdressingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLUBdressingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice"), new { @id = Model.Type + "_GenericADLUBdressingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLUBdressingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLUBdressingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLBdressing" class="float-left">LB dressing</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLLBdressingAssistance", data.AnswerOrEmptyString("GenericADLLBdressingAssistance"), new { @id = Model.Type + "_GenericADLLBdressingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLBdressingAssistanceReps", data.AnswerOrEmptyString("GenericADLLBdressingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLBdressingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice"), new { @id = Model.Type + "_GenericADLLBdressingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLBdressingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLBdressingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left strong">Comment</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="5">
                <%= Html.TextBox(Model.Type + "_ADLSelfcareComment", data.AnswerOrEmptyString("ADLSelfcareComment"), new { @class = "fill", @id = Model.Type + "_ADLSelfcareComment" })%>
            </td>
        </tr>
    </tbody>
</table>

<table class="fixed">
    <thead>
        <tr>
            <th class="strong">III.INSTRUMENTAL ADL</th>
            <th class="strong" colspan="2">Assistance</th>
            <th class="strong" colspan="2">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLHousekeeping" class="float-left">Housekeeping</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLHousekeepingAssistance", data.AnswerOrEmptyString("GenericADLHousekeepingAssistance"), new { @id = Model.Type + "_GenericADLHousekeepingAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLHousekeepingAssistanceReps", data.AnswerOrEmptyString("GenericADLHousekeepingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLHousekeepingAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLHousekeepingAssistiveDevice", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice"), new { @id = Model.Type + "_GenericADLHousekeepingAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLHousekeepingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLHousekeepingAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal prep</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLMealPrepAssistance", data.AnswerOrEmptyString("GenericADLMealPrepAssistance"), new { @id = Model.Type + "_GenericADLMealPrepAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMealPrepAssistanceReps", data.AnswerOrEmptyString("GenericADLMealPrepAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMealPrepAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMealPrepAssistiveDevice", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice"), new { @id = Model.Type + "_GenericADLMealPrepAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMealPrepAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMealPrepAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLaundry" class="float-left">Laundry</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLLaundryAssistance", data.AnswerOrEmptyString("GenericADLLaundryAssistance"), new { @id = Model.Type + "_GenericADLLaundryAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLaundryAssistanceReps", data.AnswerOrEmptyString("GenericADLLaundryAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLaundryAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLaundryAssistiveDevice", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice"), new { @id = Model.Type + "_GenericADLLaundryAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLLaundryAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLLaundryAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLTelephoneUse" class="float-left">Telephone use</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLTelephoneUseAssistance", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLTelephoneUseAssistanceReps", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLTelephoneUseAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTelephoneUseAssistiveDevice", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLTelephoneUseAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLTelephoneUseAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMoneyManagement" class="float-left">Money management</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLMoneyManagementAssistance", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMoneyManagementAssistanceReps", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMoneyManagementAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMoneyManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMoneyManagementAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMoneyManagementAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMedicationManagement" class="float-left">Medication management</label></td>
            <td colspan="2"><%= Html.TherapyAssistance(Model.Type + "_GenericADLMedicationManagementAssistance", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistance", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMedicationManagementAssistanceReps", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMedicationManagementAssistanceReps" })%>
            </td>
            <td colspan="2"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMedicationManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistiveDevice", @class = "" })%>
                x<%= Html.TextBox(Model.Type + "_GenericADLMedicationManagementAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDeviceReps"), new { @class = "sn", @id = Model.Type + "_GenericADLMedicationManagementAssistiveDeviceReps" })%>
            </td>
        </tr>
        <tr>
            <td class="align-left strong">Comment</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="5">
                <%= Html.TextBox(Model.Type + "_ADLInstrumentalComment", data.AnswerOrEmptyString("ADLInstrumentalComment"), new { @class = "fill", @id = Model.Type + "_ADLInstrumentalComment" })%>
            </td>
        </tr>
    </tbody>
</table>

<table class="fixed">
         <tr>
            <th></th>
            <th class="strong" colspan="2">Static</th>
            <th class="strong" colspan="2">Dynamic</th>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLSittingBalanceStatic" class="strong">Sitting Balance</label></td>
            <td colspan="2"><%= Html.StaticBalance(Model.Type + "_GenericADLSittingBalanceStatic", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic", @class = "fill" })%>
                
            </td>
            <td colspan="2"><%= Html.DynamicBalance(Model.Type + "_GenericADLSittingDynamicAssist", data.AnswerOrEmptyString("GenericADLSittingDynamicAssist"), new { @id = Model.Type + "_GenericADLSittingDynamicAssist", @class = "fill" })%>
                
            </td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLStandBalanceStatic" class="strong">Stand Balance</label></td>
            <td colspan="2"><%= Html.StaticBalance(Model.Type + "_GenericADLStandBalanceStatic", data.AnswerOrEmptyString("GenericADLStandBalanceStatic"), new { @id = Model.Type + "_GenericADLStandBalanceStatic", @class = "fill" })%>
                
            </td>
            <td colspan="2"><%= Html.DynamicBalance(Model.Type + "_GenericADLStandBalanceDynamic", data.AnswerOrEmptyString("GenericADLStandBalanceDynamic"), new { @id = Model.Type + "_GenericADLStandBalanceDynamic", @class = "fill" })%>
                
            </td>
        </tr>
</table>