﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("Bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBathing").Clean() %>",0,1) +
            printview.span("UE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUEDressing").Clean() %>",0,1) +
            printview.span("LE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLEDressing").Clean() %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGrooming").Clean() %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToileting").Clean() %>",0,1) +
            printview.span("Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLFeeding").Clean() %>",0,1) +
            printview.span("Meal Prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrep").Clean() %>",0,1) +
            printview.span("House Cleaning",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHouseCleaning").Clean() %>",0,1) +
            printview.span("Adapt. Equip.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLAdaptiveEquipment").Clean() %>",0,1)) +
        printview.col(2,
            printview.col(2,
                printview.span("Bed Mobility",true) +
                printview.col(2,
                    printview.span("Rolling:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityRolling").Clean() %>",0,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Supine&#8594;Sit:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilitySupineToSit").Clean() %>",0,1)) +
                printview.col(2,
                    printview.span("Sit&#8594;Stand:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilitySitToStand").Clean() %>",0,1)))) +
        printview.col(5,
            printview.span("Transfers",true) +
            printview.span("Tub/Shower/Toilet:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTransfersTubShowerToilet").Clean() %>",0,1) +
            printview.span("Bed&#8594;Chair:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTransfersBedToChairWheelchair").Clean() %>",0,1)) +
        printview.col(2,
            printview.span("W/C Mobility:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLWCMobility").Clean() %>",0,1) +
            printview.col(2,
                printview.span("Sitting Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("Standing Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("0").ToString().ToLower() %>))),
        "ADLs / Functional Mobility Level / Level of Assist");
</script>