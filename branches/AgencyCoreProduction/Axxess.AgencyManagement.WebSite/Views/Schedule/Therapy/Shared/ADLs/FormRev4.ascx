﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <thead>
        <tr>
            <th class="strong">I.FUNCTIONAL MOBILITY</th>
            <th class="strong">Assistance</th>
            <th class="strong">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLBedMobility" class="float-left">Bed mobility</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLBedMobilityAssistance", data.AnswerOrEmptyString("GenericADLBedMobilityAssistance"), new { @id = Model.Type + "_GenericADLBedMobilityAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLBedMobilityAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice"), new { @id = Model.Type + "_GenericADLBedMobilityAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLBedWCTransfers" class="float-left">Bed/WC transfers</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLBedWCTransfersAssistance", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance"), new { @id = Model.Type + "_GenericADLBedWCTransfersAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLBedWCTransfersAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice"), new { @id = Model.Type + "_GenericADLBedWCTransfersAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLToiletTransfer" class="float-left">Toilet transfer</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletTransferAssistance", data.AnswerOrEmptyString("GenericADLToiletTransferAssistance"), new { @id = Model.Type + "_GenericADLToiletTransferAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice"), new { @id = Model.Type + "_GenericADLToiletTransferAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLTubShowerTransfer" class="float-left">Tub/shower transfer</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance"), new { @id = Model.Type + "_GenericADLTubShowerTransferAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice"), new { @id = Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left strong" >Comment</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
            <%= Html.ToggleTemplates(Model.Type + "_ADLFunctionalTemplates", "", "#" + Model.Type + "_ADLFunctionalComment")%>
            <%= Html.TextArea(Model.Type + "_ADLFunctionalComment", data.ContainsKey("ADLFunctionalComment") ? data["ADLFunctionalComment"].Answer : string.Empty,4,20, new { @id = Model.Type + "_ADLFunctionalComment", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>

<table class="fixed">
    <thead>
        <tr>
            <th class="strong">II.SELFCARE/ADL SKILLS</th>
            <th class="strong">Assistance</th>
            <th class="strong">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLSelfFeeding" class="float-left">Self Feeding</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLSelfFeedingAssistance", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance"), new { @id = Model.Type + "_GenericADLSelfFeedingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLSelfFeedingAssistiveDevice", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice"), new { @id = Model.Type + "_GenericADLSelfFeedingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLOralHygiene" class="float-left">Oral Hygiene</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLOralHygieneAssistance", data.AnswerOrEmptyString("GenericADLOralHygieneAssistance"), new { @id = Model.Type + "_GenericADLOralHygieneAssistance", @class = "fill" })%></td>
            <td></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLGroomingAssistance", data.AnswerOrEmptyString("GenericADLGroomingAssistance"), new { @id = Model.Type + "_GenericADLGroomingAssistance", @class = "fill" })%></td>
            <td></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletingAssistance", data.AnswerOrEmptyString("GenericADLToiletingAssistance"), new { @id = Model.Type + "_GenericADLToiletingAssistance", @class = "fill" })%></td>
            <td></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLUBBathing" class="float-left">UB bathing</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLUBBathingAssistance", data.AnswerOrEmptyString("GenericADLUBBathingAssistance"), new { @id = Model.Type + "_GenericADLUBBathingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLUBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice"), new { @id = Model.Type + "_GenericADLUBBathingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLBBathing" class="float-left">LB bathing</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLLBBathingAssistance", data.AnswerOrEmptyString("GenericADLLBBathingAssistance"), new { @id = Model.Type + "_GenericADLLBBathingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLLBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice"), new { @id = Model.Type + "_GenericADLLBBathingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLUBdressing" class="float-left">UB dressing</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLUBdressingAssistance", data.AnswerOrEmptyString("GenericADLUBdressingAssistance"), new { @id = Model.Type + "_GenericADLUBdressingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLUBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice"), new { @id = Model.Type + "_GenericADLUBdressingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLBdressing" class="float-left">LB dressing</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLLBdressingAssistance", data.AnswerOrEmptyString("GenericADLLBdressingAssistance"), new { @id = Model.Type + "_GenericADLLBdressingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLLBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice"), new { @id = Model.Type + "_GenericADLLBdressingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Comment</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
            <%= Html.ToggleTemplates(Model.Type + "_ADLSelfcareTemplates", "", "#" + Model.Type + "_ADLSelfcareComment")%>
            <%= Html.TextArea(Model.Type + "_ADLSelfcareComment", data.ContainsKey("ADLSelfcareComment") ? data["ADLSelfcareComment"].Answer : string.Empty,4,20, new { @id = Model.Type + "_ADLSelfcareComment", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>

<table class="fixed">
    <thead>
        <tr>
            <th class="strong">III.INSTRUMENTAL ADL</th>
            <th class="strong">Assistance</th>
            <th class="strong">Assistive Device</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLHousekeeping" class="float-left">Housekeeping</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLHousekeepingAssistance", data.AnswerOrEmptyString("GenericADLHousekeepingAssistance"), new { @id = Model.Type + "_GenericADLHousekeepingAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLHousekeepingAssistiveDevice", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice"), new { @id = Model.Type + "_GenericADLHousekeepingAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal prep</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLMealPrepAssistance", data.AnswerOrEmptyString("GenericADLMealPrepAssistance"), new { @id = Model.Type + "_GenericADLMealPrepAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLMealPrepAssistiveDevice", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice"), new { @id = Model.Type + "_GenericADLMealPrepAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLLaundry" class="float-left">Laundry</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLLaundryAssistance", data.AnswerOrEmptyString("GenericADLLaundryAssistance"), new { @id = Model.Type + "_GenericADLLaundryAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLLaundryAssistiveDevice", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice"), new { @id = Model.Type + "_GenericADLLaundryAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLTelephoneUse" class="float-left">Telephone use</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLTelephoneUseAssistance", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLTelephoneUseAssistiveDevice", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMoneyManagement" class="float-left">Money management</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLMoneyManagementAssistance", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLMoneyManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLMedicationManagement" class="float-left">Medication management</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericADLMedicationManagementAssistance", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistance", @class = "fill" })%></td>
            <td><%= Html.OralTherapyAssistiveDevice(Model.Type + "_GenericADLMedicationManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Comment</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
            <%= Html.ToggleTemplates(Model.Type + "_ADLInstrumentalTemplates", "", "#" + Model.Type + "_ADLInstrumentalComment")%>
            <%= Html.TextArea(Model.Type + "_ADLInstrumentalComment", data.ContainsKey("ADLInstrumentalComment") ? data["ADLInstrumentalComment"].Answer : string.Empty,4,20, new { @id = Model.Type + "_ADLInstrumentalComment", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>

<table class="fixed">
         <tr>
            <th></th>
            <th class="strong">Static</th>
            <th class="strong">Dynamic</th>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLSittingBalanceStatic" class="strong">Sitting Balance</label></td>
            <td><%= Html.StaticBalance(Model.Type + "_GenericADLSittingBalanceStatic", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic", @class = "fill" })%></td>
            <td><%= Html.DynamicBalance(Model.Type + "_GenericADLSittingDynamicAssist", data.AnswerOrEmptyString("GenericADLSittingDynamicAssist"), new { @id = Model.Type + "_GenericADLSittingDynamicAssist", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericADLStandBalanceStatic" class="strong">Stand Balance</label></td>
            <td><%= Html.StaticBalance(Model.Type + "_GenericADLStandBalanceStatic", data.AnswerOrEmptyString("GenericADLStandBalanceStatic"), new { @id = Model.Type + "_GenericADLStandBalanceStatic", @class = "fill" })%></td>
            <td><%= Html.DynamicBalance(Model.Type + "_GenericADLStandBalanceDynamic", data.AnswerOrEmptyString("GenericADLStandBalanceDynamic"), new { @id = Model.Type + "_GenericADLStandBalanceDynamic", @class = "fill" })%></td>
        </tr>
</table>