﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("I.FUNCTIONAL MOBILITY") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed mobility",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Bed/WC transfers",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/shower transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLFunctionalComment").Clean()%>") +
        printview.col(3,
            printview.span("II.SELFCARE/ADL SKILLS") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Self Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Oral Hygiene",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGroomingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("&#160;") +
            printview.span("UB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("LB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("UB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("LB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLSelfcareComment").Clean()%>") +
        printview.col(3,
            printview.span("III.INSTRUMENTAL ADL") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Housekeeping",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Meal prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Laundry",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Telephone use",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Money management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Medication management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice").StringIntToEnumDescriptionFactory("OralTherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLInstrumentalComment").Clean()%>") +
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),            
            "ADLs / Functional Mobility Level / Level of Assist");
</script>