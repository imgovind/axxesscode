﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("I.FUNCTIONAL MOBILITY") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed mobility",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Bed/WC transfers",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/shower transfer",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLFunctionalComment").Clean()%>") +
        printview.col(3,
            printview.span("II.SELFCARE/ADL SKILLS") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Self Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Oral Hygiene",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGroomingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLGroomingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGroomingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLGroomingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLToiletingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToiletingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLToiletingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("UB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("LB bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("UB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("LB dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDeviceReps").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLSelfcareComment").Clean()%>") +
        printview.col(3,
            printview.span("III.INSTRUMENTAL ADL") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Housekeeping",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Meal prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Laundry",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLLaundryAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLLaundryAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Telephone use",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Money management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDeviceReps").Clean() %>",0,1) +
            printview.span("Medication management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>x<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistanceReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>x<%= data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDeviceReps").Clean() %>",0,1)) +
            printview.span("Comment",true) +
            printview.span("<%= data.AnswerOrEmptyString("ADLInstrumentalComment").Clean()%>") +
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1)),            
            "ADL Training");
</script>