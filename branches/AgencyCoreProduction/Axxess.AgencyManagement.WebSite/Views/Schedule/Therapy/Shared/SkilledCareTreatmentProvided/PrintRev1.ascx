﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (data.AnswerOrEmptyString("POCIsSkilledCareApply").Equals("1"))
   { %>

<script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "Skilled Care Provided");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsection(
        printview.span("Training Topics:<%= data.AnswerOrEmptyString("POCSkilledCareTrainingTopics").Clean() %>",0,3)+
        printview.span("Trained:",true)+
        printview.col(2,
        printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToString().ToLower() %>) +
        printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToString().ToLower() %>)) +
        printview.span("Treatment Performed: <%= data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed").Clean() %>",0,3)+
        printview.span("Patient Response: <%= data.AnswerOrEmptyString("POCSkilledCarePatientResponse").Clean() %>",0,3),
    "Skilled Care Provided");
</script>

<%} %>
