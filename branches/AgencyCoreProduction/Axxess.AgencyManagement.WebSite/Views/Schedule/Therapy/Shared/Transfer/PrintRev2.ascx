﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  if(data.ContainsKey("IsTransferApply") && data.AnswerOrEmptyString("IsTransferApply").Equals("1")){%>

<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Transfer");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance",true) +
            printview.span("Assistive Device",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceStatic").StringIntToEnumDescriptionFactory("StaticBalance").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic").StringIntToEnumDescriptionFactory("DynamicBalance").Clean() %>",0,1))+
            printview.span("Comment:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericTransferComment").Clean() %>",2),
        "Transfer");
</script>
<%} %>