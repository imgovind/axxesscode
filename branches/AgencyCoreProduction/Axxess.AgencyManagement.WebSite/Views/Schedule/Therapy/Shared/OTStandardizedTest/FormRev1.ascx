﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table >
    <tbody>
        <tr >
            <td class="border-right">
                <div class="float-middle strong">Prior</div>
                <div><span class="float-left">Katz Index:</span><%= Html.TextBox(Model.Type + "_GenericKatzIndex", data.AnswerOrEmptyString("GenericKatzIndex"), new { @class = "float-right", @id = Model.Type + "_GenericKatzIndex" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">9 Hole Peg Test:</span><%= Html.TextBox(Model.Type + "_Generic9HolePeg", data.AnswerOrEmptyString("Generic9HolePeg"), new { @class = "float-right", @id = Model.Type + "_Generic9HolePeg" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Lawton & Brody IADL Scale:</span><%= Html.TextBox(Model.Type + "_GenericLawtonBrody", data.AnswerOrEmptyString("GenericLawtonBrody"), new { @class = "float-right", @id = Model.Type + "_GenericLawtonBrody" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Mini-Mental State Exam:</span><%= Html.TextBox(Model.Type + "_GenericMiniMentalState", data.AnswerOrEmptyString("GenericMiniMentalState"), new { @class = "float-right", @id = Model.Type + "_GenericMiniMentalState" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:<%= Html.TextBox(Model.Type + "_GenericOtherTest", data.AnswerOrEmptyString("GenericOtherTest"), new { @class = "", @id = Model.Type + "_GenericOtherTest" })%></span><%= Html.TextBox(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @class = "float-right", @id = Model.Type + "_GenericStandardizedTestOther" })%></div>
            </td>
            <td>
                <div class="float-middle strong">Current</div>
                <div><span class="float-left">Katz Index:</span><%= Html.TextBox(Model.Type + "_GenericKatzIndex1", data.AnswerOrEmptyString("GenericKatzIndex1"), new { @class = "float-right", @id = Model.Type + "_GenericKatzIndex1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">9 Hole Peg Test:</span><%= Html.TextBox(Model.Type + "_Generic9HolePeg1", data.AnswerOrEmptyString("Generic9HolePeg1"), new { @class = "float-right", @id = Model.Type + "_Generic9HolePeg1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Lawton & Brody IADL Scale:</span><%= Html.TextBox(Model.Type + "_GenericLawtonBrody1", data.AnswerOrEmptyString("GenericLawtonBrody1"), new { @class = "float-right", @id = Model.Type + "_GenericLawtonBrody1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Mini-Mental State Exam:</span><%= Html.TextBox(Model.Type + "_GenericMiniMentalState1", data.AnswerOrEmptyString("GenericMiniMentalState1"), new { @class = "float-right", @id = Model.Type + "_GenericMiniMentalState1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:<%= Html.TextBox(Model.Type + "_GenericOtherTest1", data.AnswerOrEmptyString("GenericOtherTest1"), new { @class = "", @id = Model.Type + "_GenericOtherTest1" })%></span><%= Html.TextBox(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @class = "float-right", @id = Model.Type + "_GenericStandardizedTestOther1" })%></div>
            </td>
        </tr>
    </tbody>
</table>