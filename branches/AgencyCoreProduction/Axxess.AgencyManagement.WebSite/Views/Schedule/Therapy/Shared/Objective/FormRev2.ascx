﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  if (Model.Type.Substring(0,1).Equals("O")) { %>
    <%  string[] objectiveLevelOfAssist = data.AnswerArray("GenericObjectiveLevelOfAssist"); %>
<table class="fixed">
    <tbody>
        <tr>
            <td colspan="8">
                <label class="float-left">Objective: Skilled Intervention consisted of the following: Level of Assist:</label>
                <input type="hidden" name="<%= Model.Type %>_GenericObjectiveLevelOfAssist" value="" /> 
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist1' name='{1}_GenericObjectiveLevelOfAssist' value='1' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("1").ToChecked(),Model.Type) %>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist1" class="radio">IND</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist2' name='{1}_GenericObjectiveLevelOfAssist' value='2' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("2").ToChecked(),Model.Type) %>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist2" class="radio">SUP</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist3' name='{1}_GenericObjectiveLevelOfAssist' value='3' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("3").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist3" class="radio">SBA</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist4' name='{1}_GenericObjectiveLevelOfAssist' value='4' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("4").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist4" class="radio">CGA</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist5' name='{1}_GenericObjectiveLevelOfAssist' value='5' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("5").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist5" class="radio">MIN</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist6' name='{1}_GenericObjectiveLevelOfAssist' value='6' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("6").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist6" class="radio">MOD</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist7' name='{1}_GenericObjectiveLevelOfAssist' value='7' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("7").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist7" class="radio">MAX</label>
            </td>
            <td>
                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist8' name='{1}_GenericObjectiveLevelOfAssist' value='8' type='checkbox' {0} />", objectiveLevelOfAssist.Contains("8").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist8" class="radio">DEP</label>
            </td>
        </tr>
    </tbody>
</table>
<%  } else if (Model.Type.Substring(0,1).Equals("P")) { %>
<div>
    <%  string[] genericTherapeuticExercises = data.AnswerArray("GenericTherapeuticExercises"); %>
<%= string.Format("<input class='radio' id='{1}_GenericTherapeuticExercises1' name='{1}_GenericTherapeuticExercises' value='1' type='checkbox' {0} />", genericTherapeuticExercises.Contains("1").ToChecked(), Model.Type)%>

    <label class="strong">Therapeutic Exercises</label>
    <div class="float-right">
        <label class="loc">Body Parts</label>
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericROMTo" class="float-left">ROM To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericROMTo", data.AnswerOrEmptyString("GenericROMTo"), new { @id = Model.Type + "_GenericROMTo", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericROMToReps", data.AnswerOrEmptyString("GenericROMToReps"), new { @id = Model.Type + "_GenericROMToReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericActiveTo" class="float-left">Active To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericActiveTo", data.AnswerOrEmptyString("GenericActiveTo"), new { @id = Model.Type + "_GenericActiveTo", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericActiveToReps", data.AnswerOrEmptyString("GenericActiveToReps"), new { @id = Model.Type + "_GenericActiveToReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericAssistive" class="float-left">Active/Assistive To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericAssistive", data.AnswerOrEmptyString("GenericAssistive"), new { @id = Model.Type + "_GenericAssistive", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericAssistiveReps", data.AnswerOrEmptyString("GenericAssistiveReps"), new { @id = Model.Type + "_GenericAssistiveReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericManual" class="float-left">Resistive, Manual, To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericManual", data.AnswerOrEmptyString("GenericManual"), new { @id = Model.Type + "_GenericManual", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericManualReps", data.AnswerOrEmptyString("GenericManualReps"), new { @id = Model.Type + "_GenericManualReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericResistiveWWeights" class="float-left">Resistive, w/Weights, To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericResistiveWWeights", data.AnswerOrEmptyString("GenericResistiveWWeights"), new { @id = Model.Type + "_GenericResistiveWWeights", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericResistiveWWeightsReps", data.AnswerOrEmptyString("GenericResistiveWWeightsReps"), new { @id = Model.Type + "_GenericResistiveWWeightsReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericStretchingTo" class="float-left">Stretching To:</label>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_GenericStretchingTo", data.AnswerOrEmptyString("GenericStretchingTo"), new { @id = Model.Type + "_GenericStretchingTo", @class = "loc" })%>
        x
        <%= Html.TextBox(Model.Type + "_GenericStretchingToReps", data.AnswerOrEmptyString("GenericStretchingToReps"), new { @id = Model.Type + "_GenericStretchingToReps", @class = "loc" })%>
        reps
    </div>
    <div class="clear"></div>
</div>
<div>
    <label for="<%= Model.Type %>_GenericObjectiveComment" class="float-left">Comment:</label>
    <%= Html.ToggleTemplates(Model.Type + "_ObjectiveTemplates", "", "#" + Model.Type + "_GenericObjectiveComment")%>
</div>
<div>
    <%= Html.TextArea(Model.Type + "_GenericObjectiveComment", data.AnswerOrEmptyString("GenericObjectiveComment"), new { @id = Model.Type + "_GenericObjectiveComment", @class = "fill" })%>
</div>
<%  } %>