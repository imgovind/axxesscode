﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
<%  if (Model.Type.Substring(0,1).Equals("O")|| Model.Type.Equals("COTAVisit")) { %>
    printview.addsection(
        printview.span("Skilled Intervention consisted of the following: Level of Assist",true) +
        printview.col(4,
            printview.checkbox("IND",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("SUP",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("SBA",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("CGA",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("MIN",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("MOD",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("MAX",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.checkbox("DEP",<%= data.AnswerOrEmptyString("GenericObjectiveLevelOfAssist").Split(',').Contains("8").ToString().ToLower() %>)),
        "Objective");
<%  } else if (Model.Type.Substring(0,1).Equals("P")) { %>
    <%  string[] genericTherapeuticExercises = data.AnswerArray("GenericTherapeuticExercises"); %>
    printview.addsection(
        printview.checkbox("Therapeutic Exercises", <%= genericTherapeuticExercises.Contains("1").ToString().ToLower() %>, true) +
        printview.col(4,
            printview.span("ROM To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericROMTo").Clean() %> x <%= data.AnswerOrEmptyString("GenericROMToReps").Clean() %>reps", 0, 1) +
            printview.span("Active To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericActiveTo").Clean()%> x <%= data.AnswerOrEmptyString("GenericActiveToReps").Clean() %>reps", 0, 1) +
            printview.span("Active/Assistive To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAssistive").Clean()%> x <%= data.AnswerOrEmptyString("GenericAssistiveReps").Clean() %>reps", 0, 1) +
            printview.span("Resistive, Manual, To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericManual").Clean()%> x <%= data.AnswerOrEmptyString("GenericManualReps").Clean() %>reps", 0, 1) +
            printview.span("Resistive, w/Weights, To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericResistiveWWeights").Clean()%> x <%= data.AnswerOrEmptyString("GenericResistiveWWeightsReps").Clean() %>reps", 0, 1) +
            printview.span("Stretching To:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericStretchingTo").Clean()%> x <%= data.AnswerOrEmptyString("GenericStretchingToReps").Clean() %>reps", 0, 1)),
        "Objective");
<%  } %>
</script>