﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan1' class='radio' name='{1}_GenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan1">Thera Ex</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan2' class='radio' name='{1}_GenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan2">Bed Mobility Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan3' class='radio' name='{1}_GenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan3">Transfer Training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan4' class='radio' name='{1}_GenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan4">Balance Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan5' class='radio' name='{1}_GenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan.Contains("5").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan5">Gait Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan6' class='radio' name='{1}_GenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan.Contains("6").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan6">HEP</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan7' class='radio' name='{1}_GenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan.Contains("7").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan7">Electrotherapy</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan8' class='radio' name='{1}_GenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan.Contains("8").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan8">Ultrasound</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan9' class='radio' name='{1}_GenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan.Contains("9").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan9">Prosthetic Training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan10' class='radio' name='{1}_GenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan.Contains("10").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan10">Manual Therapy</label>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericTreatmentPlanOther" class="float-left">Other</label>
                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"), new { @id = Model.Type + "_GenericTreatmentPlanOther" })%>
            </td>
        </tr>
    </tbody>
</table>