﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var noteDiscipline = Model.Type; %>
<%  string[] genericTreatmentPlan = data.AnswerArray("POCGenericTreatmentPlan"); %>
<% if(data.ContainsKey("IsTreatmentApply") && data.AnswerOrEmptyString("IsTreatmentApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Treatment Plan");
</script>
<%}else{ %>
<script type="text/javascript">
            printview.addsection(
            printview.span("OT Frequency & Duration:<%=data.AnswerOrEmptyString("POCGenericFrequencyAndDuration").Clean() %>")+
            printview.col(2,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Therapeutic activities (reaching, bending, etc)",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Sensory integrative techniques",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Wheelchair management training",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Teach work simplification",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Community/work integration",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Self care management training",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Cognitive skills development/training",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach task segmentation",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Manual therapy techniques",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.col(2,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan19Duration").Clean() %>",0,1))+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.col(3,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Dosage: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20Dosage").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20Duration").Clean() %>",0,1)))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("POCGenericTreatmentPlanOther").Clean() %>",0,1),
            "Treatment Plan");
</script>
<%} %>