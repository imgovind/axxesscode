﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
<table class="fixed align-left">
    <tbody>
        <tr>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="strong">PT Frequency & Duration</label>
                    
                    <%= Html.TextBox(Model.Type + "_GenericFrequencyAndDuration", data.AnswerOrEmptyString("GenericFrequencyAndDuration"), new { @id = Model.Type + "_GenericFrequencyAndDuration", @class = "" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan1' class='radio' name='{1}_GenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan1">Therapeutic exercise</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan2' class='radio' name='{1}_GenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan2">Bed Mobility Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan3' class='radio' name='{1}_GenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan3">Transfer Training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan4' class='radio' name='{1}_GenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan4">Balance Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan5' class='radio' name='{1}_GenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan.Contains("5").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan5">Gait Training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan6' class='radio' name='{1}_GenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan.Contains("6").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan6">Neuromuscular re-education</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan7' class='radio' name='{1}_GenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan.Contains("7").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan7">Functional mobility training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan8' class='radio' name='{1}_GenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan.Contains("8").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan8">Teach safe and effective use of adaptive/assist device</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan9' class='radio' name='{1}_GenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan.Contains("9").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan9">Teach safe stair climbing skills</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan10' class='radio' name='{1}_GenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan.Contains("10").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan10">Teach fall prevention/safety</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan11' class='radio' name='{1}_GenericTreatmentPlan' value='11' type='checkbox' {0} />", genericTreatmentPlan.Contains("11").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan11">Establish/upgrade home exercise program</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan12' class='radio' name='{1}_GenericTreatmentPlan' value='12' type='checkbox' {0} />", genericTreatmentPlan.Contains("12").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan12">Pt/caregiver education/training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan13' class='radio' name='{1}_GenericTreatmentPlan' value='13' type='checkbox' {0} />", genericTreatmentPlan.Contains("13").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan13">Proprioceptive training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan14' class='radio' name='{1}_GenericTreatmentPlan' value='14' type='checkbox' {0} />", genericTreatmentPlan.Contains("14").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan14">Postural control training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan15' class='radio' name='{1}_GenericTreatmentPlan' value='15' type='checkbox' {0} />", genericTreatmentPlan.Contains("15").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan15">Teach energy conservation techniques</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan16' class='radio' name='{1}_GenericTreatmentPlan' value='16' type='checkbox' {0} />", genericTreatmentPlan.Contains("16").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan16">Relaxation technique</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan17' class='radio' name='{1}_GenericTreatmentPlan' value='17' type='checkbox' {0} />", genericTreatmentPlan.Contains("17").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan17">Teach safe and effective breathing technique</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan18' class='radio' name='{1}_GenericTreatmentPlan' value='18' type='checkbox' {0} />", genericTreatmentPlan.Contains("18").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan18">Teach hip precaution</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_GenericTreatmentPlan19' class='radio' name='{1}_GenericTreatmentPlan' value='19' type='checkbox' {0} />", genericTreatmentPlan.Contains("19").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericTreatmentPlan19">Electrical stimulation</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan19BodyParts" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19Duration", data.AnswerOrEmptyString("GenericTreatmentPlan19Duration"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan19Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_GenericTreatmentPlan20' class='radio' name='{1}_GenericTreatmentPlan' value='20' type='checkbox' {0} />", genericTreatmentPlan.Contains("20").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericTreatmentPlan20">Ultrasound</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan20BodyParts" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Duration", data.AnswerOrEmptyString("GenericTreatmentPlan20Duration"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan20Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_GenericTreatmentPlan21' class='radio' name='{1}_GenericTreatmentPlan' value='21' type='checkbox' {0} />", genericTreatmentPlan.Contains("21").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericTreatmentPlan21">TENS</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan21BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan21BodyParts"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan21BodyParts" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan21Duration", data.AnswerOrEmptyString("GenericTreatmentPlan21Duration"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan21Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan22' class='radio' name='{1}_GenericTreatmentPlan' value='22' type='checkbox' {0} />", genericTreatmentPlan.Contains("22").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan22">Prosthetic training</label>
            </td>
            <td colspan="2">
                <%= string.Format("<input id='{1}_GenericTreatmentPlan23' class='radio' name='{1}_GenericTreatmentPlan' value='23' type='checkbox' {0} />", genericTreatmentPlan.Contains("23").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan23">Pulse oximetry PRN</label>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="align-center">
                <label for="<%= Model.Type %>_GenericTreatmentPlanOther">Other</label>
                <%= Html.ToggleTemplates(Model.Type + "_TreatmentPlanOtherTemplates", "", "#" + Model.Type + "_GenericTreatmentPlanOther")%>
                <%= Html.TextArea(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"), new { @id = Model.Type + "_GenericTreatmentPlanOther", @class="fill" })%>
            </td>
        </tr>
        <%if (Model.DisciplineTask == 47)
          { %>
        <tr>
            <td colspan="3" class="align-center">
                <label for="<%= Model.Type %>_GenericTreatmentPlanFrequencyDuration">Frequency & Duration</label><br />
                <%= Html.ToggleTemplates(Model.Type + "_TreatmentPlanTemplates", "", "#" + Model.Type + "_GenericTreatmentPlanFrequencyDuration")%>
                <%= Html.TextArea(Model.Type + "_GenericTreatmentPlanFrequencyDuration", data.AnswerOrEmptyString("GenericTreatmentPlanFrequencyDuration"),4,20, new { @id = Model.Type + "_GenericTreatmentPlanFrequencyDuration", @class = "fill" })%>
            </td>
        </tr>
        <%} %>
    </tbody>
</table>