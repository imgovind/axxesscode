﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("Thera Ex",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility Training",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfer Training",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Balance Training",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Gait Training",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("HEP",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Electrotherapy",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Ultrasound",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Prosthetic Training",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Manual Therapy",<%= data.AnswerOrEmptyString("GenericTreatmentPlan").Split(',').Contains("10").ToString().ToLower() %>) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E <%= data.AnswerOrEmptyString("GenericTreatmentPlanOther").Clean() %>",0,1)),
        "Treatment Plan");
</script>