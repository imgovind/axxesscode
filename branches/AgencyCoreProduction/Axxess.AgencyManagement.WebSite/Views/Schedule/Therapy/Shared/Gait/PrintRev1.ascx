﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitLevelAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitLevelFeet").Clean() %> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitUnLevelAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitUnLevelFeet").Clean() %> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitStepStairAssist").Clean() %> x <%= data.AnswerOrEmptyString("GenericGaitStepStairFeet").Clean() %> feet") +
            printview.span("Comments",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitComment").Clean() %>",0,2)),
        "Gait");
</script>