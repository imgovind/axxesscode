﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <tbody>
        <tr>
            <td class="align-left strong">
                Medical Diagnosis:
            </td>
            <td>
                <%= Html.TextBox(Model.Type + "_POCGenericMedicalDiagnosis", data.AnswerOrEmptyString("POCGenericMedicalDiagnosis"), new { @id = Model.Type + "_POCGenericMedicalDiagnosis" })%>
            </td>
            <td>
                <label for="<%= Model.Type %>_POCGenericMedicalDiagnosisOnset1" class="float-left radio">
                    Onset</label>
                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_POCMedicalDiagnosisDate"
                    value="<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate") %>" id="<%= Model.Type %>_POCMedicalDiagnosisDate" />
            </td>
        </tr>
        <tr>
            <td class="align-left strong">
            <%if (Model.DisciplineTask == 44)
              { %>
                PT Diagnosis:
                <%}
              else if (Model.DisciplineTask == 51)
              { %>
                ST Diagnosis:
                <%}
              else if (Model.DisciplineTask == 47)
              { %>
                OT Diagnosis:
                <%} %>
            </td>
            <td>
                <%= Html.TextBox(Model.Type + "_POCGenericTherapyDiagnosis", data.AnswerOrEmptyString("POCGenericTherapyDiagnosis"), new { @id = Model.Type + "_POCGenericTherapyDiagnosis" })%><br />
            </td>
            <td>
                <label for="<%= Model.Type %>_POCGenericTherapyDiagnosisOnset1" class="float-left radio">
                    Onset</label>
                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_POCTherapyDiagnosisDate"
                    value="<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate") %>" id="<%= Model.Type %>_POCTherapyDiagnosisDate" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_POCGenericMedicalDiagnosisComment" class="strong">
                        Comment</label>
                        <%= Html.ToggleTemplates(Model.Type + "_POCMedicalDiagnosisTemplates", "", "#" + Model.Type + "_POCGenericMedicalDiagnosisComment")%>
                    <%= Html.TextArea(Model.Type + "_POCGenericMedicalDiagnosisComment", data.ContainsKey("POCGenericMedicalDiagnosisComment") ? data["POCGenericMedicalDiagnosisComment"].Answer : string.Empty, new { @id = Model.Type + "_POCGenericMedicalDiagnosisComment", @class = "fill" })%>
                </div>
            </td>
        </tr>
    </tbody>
</table>

