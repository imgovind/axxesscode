﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>ModalitiesContainer">
    <%= Html.ToggleTemplates(Model.Type + "_POCGenericModalitiesTemplates", "", "#" + Model.Type + "_POCGenericModalitiesComment")%>
    <%= Html.TextArea(Model.Type + "_POCGenericModalitiesComment", data.ContainsKey("POCGenericModalitiesComment") ? data["POCGenericModalitiesComment"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCGenericModalitiesComment", @class = "fill" })%>
</div>
