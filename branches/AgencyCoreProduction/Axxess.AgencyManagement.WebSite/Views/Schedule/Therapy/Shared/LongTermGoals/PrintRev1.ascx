﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal1").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal1TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal2").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal2TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal3").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal3TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal4").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal4TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal5").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal5TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Freq.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericLongTermFrequency").Clean() %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericLongTermDuration").Clean() %>",0,1)),
        "Long Term Goals");
</script>