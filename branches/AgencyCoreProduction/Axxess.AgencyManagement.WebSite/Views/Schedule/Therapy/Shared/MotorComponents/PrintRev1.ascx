﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericMotorComponents = data.AnswerArray("GenericMotorComponents"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("Fine Motor Coordination",true)+
            printview.span("Right <%= data.AnswerOrEmptyString("GenericFineMotorR").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFineMotorSensoryR").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("")+
            printview.span("Left <%= data.AnswerOrEmptyString("GenericFineMotorL").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFineMotorSensoryL").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            
            printview.span("Gross Motor Coordination",true)+
            printview.span("Right <%= data.AnswerOrEmptyString("GenericGrossMotorR").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGrossMotorSensoryR").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1) +
            printview.span("")+
            printview.span("Left <%= data.AnswerOrEmptyString("GenericGrossMotorL").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGrossMotorSensoryL").StringIntToEnumDescriptionFactory("Sensory").Clean() %>",0,1)) +
          printview.col(4,
            printview.checkbox("Right handed",<%= genericMotorComponents.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Left handed",<%= genericMotorComponents.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Orthosis",<%= genericMotorComponents.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Used",<%= genericMotorComponents.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Needed(specify)",<%= genericMotorComponents.Contains("5").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMotorComponentsSpecify").Clean() %>",0,1) +
            printview.span("")+
            printview.span(""))+
            printview.span("Comments",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMotorComments").Clean() %>",0,1),
            "Motor Components(Enter Appropriate Response)");
</Script>