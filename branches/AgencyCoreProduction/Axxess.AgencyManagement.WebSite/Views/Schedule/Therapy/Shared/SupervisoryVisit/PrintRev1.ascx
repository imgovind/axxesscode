﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] superVisory = data.AnswerArray("SuperVisory"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("OT Assistant",<%= superVisory.Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Aide",<%= superVisory.Contains("2").ToString().ToLower() %>)+
            printview.checkbox("Present",<%= superVisory.Contains("3").ToString().ToLower() %>)+
            printview.checkbox("Not Present",<%= superVisory.Contains("4").ToString().ToLower() %>))+
        printview.span("Observation of:<%= data.AnswerOrEmptyString("GenericSupervisoryObservation").Clean() %>",0,1) +
        printview.span("Teaching/Training of:<%= data.AnswerOrEmptyString("GenericSupervisoryTeaching").Clean() %>",0,1) +
        printview.span("Care plan reviewed/revised with assistant/aide during this visit:",true)+
        printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerArray("GenericSupervisoryCarePLanReviewed").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("No",<%= data.AnswerArray("GenericSupervisoryCarePLanReviewed").Contains("0").ToString().ToLower() %>))+
        <%if(data.AnswerArray("GenericSupervisoryCarePLanReviewed").Contains("1")){ %>
        printview.span("If yes(specify)<%= data.AnswerOrEmptyString("GenericSupervisorySpecify").Clean() %>",0,1) +
        <%} %>
        printview.span("If OT assistant/aide not present, specify date he/she was contacted regarding updated care plan:<%= data.AnswerOrEmptyString("GenericSupervisoryDate").Clean() %>",0,1),
        "Supervisory Visit(Complete if applicable)");
</script>