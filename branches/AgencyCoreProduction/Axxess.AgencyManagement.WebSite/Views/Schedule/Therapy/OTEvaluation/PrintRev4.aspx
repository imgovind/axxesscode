﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
    <script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E" +
        "<%= Model.Data.TypeName %>" + 
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("EpsPeriod") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn")%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Data.PhysicianDisplayName.Clean() %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.Clean().IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EOccupational Therapy <%= Model.Data.Type == "OTDischarge" ? "Discharge" : (Model.Data.Type == "OTReEvaluation" ? "Re-" : "") + "Evaluation" %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
    </script>

    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev2.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/PrintRev2.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/PrintRev2.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Assessment/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Narrative/PrintRev1.ascx", Model.Data); %>

    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTStandardizedTest/PrintRev1.ascx", Model.Data); %>

    <script type="text/javascript">
        printview.addsection(
        printview.span(""), "MD Orders");
    </script>

    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DME/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/PrintRev3.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/PrintRev7.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Modalities/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTGoals/PrintRev2.ascx", Model.Data); %>
    
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OtherDisciplineRecommendation/PrintRev1.ascx", Model.Data); %>
    
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Rehab/PrintRev1.ascx", Model.Data); %>
    
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DischargePlan/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SkilledCareTreatmentProvided/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CareCoordination/PrintRev1.ascx", Model.Data); %>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SafetyIssues/PrintRev1.ascx", Model.Data); %>
    
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/PrintRev3.ascx", Model.Data); %>

    <script type="text/javascript">
        printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.Data.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.Data.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.Data.PhysicianSignatureDate.IsValid() ? Model.Data.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
    </script>

</body>
</html>
