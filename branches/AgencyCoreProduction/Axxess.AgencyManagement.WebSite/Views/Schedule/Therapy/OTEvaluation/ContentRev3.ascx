﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <!--
Vital Signs
-->
    <tr>
        <th colspan="4">
            Vital Signs
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
        </td>
    </tr>
    <!--
PLOF, Living Situation


-->
    <tr>
        <th colspan="2">
            PLOF and Medical History
        </th>
        <th colspan="2">
            Living Situation
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Homebound Reason
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Functional Mobility Key
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            ADLs/Functional Mobility Level/Level of Assist
        </th>
        <th colspan="2">
            Physical Assessment
        </th>
    </tr>
    <tr>
        <td colspan="2" rowspan="3">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev2.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/FormRev2.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Pain Assessment
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Sensory/Perceptual Skills
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Cognitive Status/Comprehension
        </th>
        <th colspan="2">
            Motor Components (Enter Appropriate Response)
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/FormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Assessment
        </th>
        <th colspan="2">
            Narrative
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Assessment/FormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Narrative/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Standardized Test
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTStandardizedTest/FormRev1.ascx", Model); %>
        </td>
    </tr>
    
    <!-- 
MD ORDERS - THE NEW FORM
-->
    <tr>
        <th colspan="4">
            MD Orders
        </th>
    </tr>
    <tr>
        <th colspan="2">
            DME
            <%= string.Format("<input class='radio' id='{0}POCIsDMEApply' name='{0}_POCIsDMEApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsDMEApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
        <th colspan="2">
            Diagnosis
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DME/FormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev3.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Treatment Plan
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRevOT1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Modalities<%= string.Format("<input class='radio' id='{0}POCIsModalitiesApply' name='{0}_POCIsModalitiesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsModalitiesApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
    </tr>
    <tr>
        <td colspan="4">
             <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Modalities/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            OT Goals
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTGoals/FormRev2.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Other Discipline Recommendation
            <%= string.Format("<input class='radio' id='{0}POCIsRecommendationApply' name='{0}_POCIsRecommendationApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsRecommendationApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OtherDisciplineRecommendation/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Rehab
            <%= string.Format("<input class='radio' id='{0}POCIsRehabApply' name='{0}_POCIsRehabApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsRehabApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
        <th colspan="2">
            Discharge Plan<%= string.Format("<input class='radio' id='{0}POCIsDCPlanApply' name='{0}_POCIsDCPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsDCPlanApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <div id="<%=Model.Type %>RehabContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Rehab/FormRev1.ascx", Model); %>
            </div>
        </td>
        <td colspan="2">
            
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/DischargePlan/FormRev1.ascx", Model); %>
            
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Skilled Care Provided
            <%= string.Format("<input class='radio' id='{0}POCIsSkilledCareApply' name='{0}_POCIsSkilledCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsSkilledCareApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
    </tr>
    <tr>
        <td colspan="2" valign="top">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SkilledCareTreatmentProvided/PartOneFormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SkilledCareTreatmentProvided/PartTwoFormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            Care Coordination
            <%= string.Format("<input class='radio' id='{0}POCIsCareApply' name='{0}_POCIsCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsCareApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
        <th colspan="2">
            Safety Issues/Instruction/Education
            <%= string.Format("<input class='radio' id='{0}POCIsSafetyIssueApply' name='{0}_POCIsSafetyIssueApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCIsSafetyIssueApply").Contains("1").ToChecked())%>
            <label>
                N/A</label>
        </th>
    </tr>
    <tr>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CareCoordination/FormRev1.ascx", Model); %>
        </td>
        <td colspan="2">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SafetyIssues/FormRev1.ascx", Model); %>
        </td>
    </tr>
    <tr>
        <th colspan="4">
            Notification
        </th>
    </tr>
    <tr>
        <td colspan="4">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev3.ascx", Model); %>
        </td>
    </tr>
</table>

<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>POCIsDMEApply"), $("#<%= Model.Type %>DMEContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsModalitiesApply"), $("#<%= Model.Type %>ModalitiesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsRecommendationApply"), $("#<%= Model.Type %>RecommendationContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsRehabApply"), $("#<%= Model.Type %>RehabContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsDCPlanApply"), $("#<%= Model.Type %>DCPlanContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsCareApply"), $("#<%= Model.Type %>CareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>POCIsSafetyIssueApply"), $("#<%= Model.Type %>SafetyIssuesInstructionContainer"));
</script>