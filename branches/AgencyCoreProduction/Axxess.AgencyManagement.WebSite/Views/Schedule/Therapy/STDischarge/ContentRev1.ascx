﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericHomeboundReason = data.AnswerArray("GenericHomeboundReason"); %>
<%  string[] liquids = data.AnswerArray("GenericLiquids"); %>
<%  string[] genericReferralFor = data.AnswerArray("GenericReferralFor"); %>
<%  string[] genericPlanOfCare = data.AnswerArray("GenericPlanOfCare"); %>
<%  string[] genericDischargeDiscussedWith = data.AnswerArray("GenericDischargeDiscussedWith"); %>
<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="3">Diagnosis</th>
        </tr>
        <tr>
            <td colspan="3">
                <input type="hidden" name="<%= Model.Type %>_GenericHomeboundReason" value="" />
                <table class="fixed align-left">
                    <tbody>
                        <tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericEvaluationType" class="float-left">Evaluation Type</label></td>
                            <td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "2", data.AnswerOrEmptyString("GenericEvaluationType").Equals("2"), new { @id = Model.Type + "_GenericEvaluationType2", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType2" class="inline-radio">Initial</label>
                            </td>
                            <td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "1", data.AnswerOrEmptyString("GenericEvaluationType").Equals("1"), new { @id = Model.Type + "_GenericEvaluationType1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType1" class="inline-radio">Interim</label>
                            </td>
                            <td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "0", data.AnswerOrEmptyString("GenericEvaluationType").Equals("0"), new { @id = Model.Type + "_GenericEvaluationType0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType0" class="inline-radio">Final</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="float-left">Homebound Reason</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason1' class='radio' name='{1}_GenericHomeboundReason' value='1' type='checkbox' {0} />", genericHomeboundReason.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason1">Needs assistance for all activities</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason2' class='radio' name='{1}_GenericHomeboundReason' value='2' type='checkbox' {0} />", genericHomeboundReason.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason2">Residual weakness</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason3' class='radio' name='{1}_GenericHomeboundReason' value='3' type='checkbox' {0} />", genericHomeboundReason.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason3">Requires assistance to ambulate</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason4' class='radio' name='{1}_GenericHomeboundReason' value='4' type='checkbox' {0} />", genericHomeboundReason.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason4">Confusion, unable to go out of home alone</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason5' class='radio' name='{1}_GenericHomeboundReason' value='5' type='checkbox' {0} />", genericHomeboundReason.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason5">Unable to safely leave home unassisted</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason6' class='radio' name='{1}_GenericHomeboundReason' value='6' type='checkbox' {0} />", genericHomeboundReason.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason6">Severe SOB, SOB upon exertion</label>
                            </td>
                        </tr>
                        <tr class="border-bottom">
                            <td></td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason7' class='radio' name='{1}_GenericHomeboundReason' value='7' type='checkbox' {0} />", genericHomeboundReason.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason7">Unable to safely leave home unassisted</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason8' class='radio' name='{1}_GenericHomeboundReason' value='8' type='checkbox' {0} />", genericHomeboundReason.Contains("8").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason8">Medical Restrictions</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason9' class='radio' name='{1}_GenericHomeboundReason' value='9' type='checkbox' {0} />", genericHomeboundReason.Contains("9").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason9">Other:</label>
                                <%= Html.TextBox(Model.Type + "_GenericHomeboundReasonOther", data.AnswerOrEmptyString("GenericHomeboundReasonOther"), new { @class = "float-right", @id = Model.Type + "_GenericHomeboundReasonOther" })%>
                            </td>
                        </tr>
                        <tr class="border-bottom">
                            <td><label class="float-left">Orders for Evaluation Only?</label></td>
                            <td class="align-right">
                                <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "1", data.AnswerOrEmptyString("GenericOrdersForEvaluationOnly").Equals("1"), new { @id = Model.Type + "_GenericOrdersForEvaluationOnly1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly1" class="inline-radio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "0", data.AnswerOrEmptyString("GenericOrdersForEvaluationOnly").Equals("0"), new { @id = Model.Type + "_GenericOrdersForEvaluationOnly0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly0" class="inline-radio">No</label>
                                <span id="<%= Model.Type %>_GenericIfNoOrdersAreSpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericIfNoOrdersAre">If No, orders are</label>
                                    <%= Html.TextBox(Model.Type + "_GenericIfNoOrdersAre", data.AnswerOrEmptyString("GenericIfNoOrdersAre"), new { @id = Model.Type + "_GenericIfNoOrdersAre" })%>
                                </span>
                            </td>
                            <td><label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float-left">Medical Diagnosis/Treatment Diagnosis:</label></td>
                            <td class="align-right">
                                <%= Html.TextArea(Model.Type + "_GenericMedicalDiagnosis", data.AnswerOrEmptyString("GenericMedicalDiagnosis"), new { @id = Model.Type + "_GenericMedicalDiagnosis", @class = "fill" })%><br />
                                <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate">Onset</label>
                                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate" value="<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisOnsetDate") %>" id="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate" />
                            </td>
                        </tr>
                        <tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericMedicalPrecautions" class="float-left">Medical Precautions:</label></td>
                            <td class="align-right"><%= Html.TextArea(Model.Type + "_GenericMedicalPrecautions", data.AnswerOrEmptyString("GenericMedicalPrecautions"), new { @id = Model.Type + "_GenericMedicalPrecautions", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPriorLevelOfFunctioning" class="float-left">Prior Level of Functioning:</label></td>
                            <td class="align-right"><%= Html.TextArea(Model.Type + "_GenericPriorLevelOfFunctioning", data.AnswerOrEmptyString("GenericPriorLevelOfFunctioning"), new { @id = Model.Type + "_GenericPriorLevelOfFunctioning", @class = "fill" })%></td>
                        </tr>
                        <tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericLivingSituation" class="float-left">Living Situation/Support System</label></td>
                            <td class="align-right"><%= Html.TextArea(Model.Type + "_GenericLivingSituation", data.AnswerOrEmptyString("GenericLivingSituation"), new { @id = Model.Type + "_GenericLivingSituation", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPreviousMedicalHistory" class="float-left">Describe pertinent medical/social history and/or previous therapy provided:</label></td>
                            <td class="align-right"><%= Html.TextArea(Model.Type + "_GenericPreviousMedicalHistory", data.AnswerOrEmptyString("GenericPreviousMedicalHistory"), new { @id = Model.Type + "_GenericPreviousMedicalHistory", @class = "fill" })%></td>
                        </tr>
                        <tr class="border-bottom">
                            <td><label class="float-left">Safe Swallowing Evaluation?</label></td>
                            <td class="align-right">
                                <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "1", data.AnswerOrEmptyString("GenericIsSSE").Equals("1"), new { @id = Model.Type + "_GenericIsSSE1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsSSE1" class="inline-radio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "0", data.AnswerOrEmptyString("GenericIsSSE").Equals("0"), new { @id = Model.Type + "_GenericIsSSE0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsSSE0" class="inline-radio">No</label>
                                <span id="<%= Model.Type %>_GenericGenericSSESpecifySpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericGenericSSESpecify">Specify date, facility and physician</label>
                                    <%= Html.TextArea(Model.Type + "_GenericGenericSSESpecify", data.AnswerOrEmptyString("GenericGenericSSESpecify"), new { @id = Model.Type + "_GenericGenericSSESpecify", @class = "fill" })%>
                                </span>
                            </td>
                            <td><label class="float-left">Video Fluoroscopy?</label></td>
                            <td class="align-right">
                                <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "1", data.AnswerOrEmptyString("GenericIsVideoFluoroscopy").Equals("1"), new { @id = Model.Type + "_GenericIsVideoFluoroscopy1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy1" class="inline-radio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "0", data.AnswerOrEmptyString("GenericIsVideoFluoroscopy").Equals("0"), new { @id = Model.Type + "_GenericIsVideoFluoroscopy0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy0" class="inline-radio">No</label>
                                <span id="<%= Model.Type %>_GenericVideoFluoroscopySpecifySpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericVideoFluoroscopySpecify">Specify date, facility and physician</label>
                                    <%= Html.TextArea(Model.Type + "_GenericVideoFluoroscopySpecify", data.AnswerOrEmptyString("GenericVideoFluoroscopySpecify"), new { @id = Model.Type + "_GenericVideoFluoroscopySpecify", @class = " fill" })%>
                                </span>
                            </td>
                        </tr>
                        <tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericCurrentDietTexture" class="float-left">Current Diet Texture</label></td>
                            <td class="align-right"><%= Html.TextArea(Model.Type + "_GenericCurrentDietTexture", data.AnswerOrEmptyString("GenericCurrentDietTexture"), new { @id = Model.Type + "_GenericCurrentDietTexture", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPainDescription" class="float-left">Pain (describe):</label></td>
                            <td class="align-right">
                                <%= Html.TextArea(Model.Type + "_GenericPainDescription", data.AnswerOrEmptyString("GenericPainDescription"), new { @id = Model.Type + "_GenericPainDescription", @class = "fill" })%><br />
                                <label>Impact on Therapy Care Plan</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "1", data.AnswerOrEmptyString("GenericIsPainImpactCarePlan").Equals("1"), new { @id = Model.Type + "_GenericIsPainImpactCarePlan1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan1" class="inline-radio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "0", data.AnswerOrEmptyString("GenericIsPainImpactCarePlan").Equals("0"), new { @id = Model.Type + "_GenericIsPainImpactCarePlan0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan0" class="inline-radio">No</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="float-left">Liquids</label></td>
                            <td>
                                <input type="hidden" name="<%= Model.Type %>_GenericLiquids" value="" />
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericLiquids1' name='{1}_GenericLiquids' value='1' type='checkbox' {0} />", liquids.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids1" class="float-left radio">Thin</label>
                            </td>
                            <td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericLiquids2' name='{1}_GenericLiquids' value='2' type='checkbox' {0} />", liquids.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids2" class="float-left radio">Thickened (specify)</label>
                                <%= Html.TextBox(Model.Type + "_GenericLiquidsThick", data.AnswerOrEmptyString("GenericLiquidsThick"), new { @class = "float-right", @id = Model.Type + "_GenericLiquidsThick" })%>
                            </td>
                            <td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericLiquids3' name='{1}_GenericLiquids' value='3' type='checkbox' {0} />", liquids.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids3" class="float-left radio">Other (specify)</label>
                                <%= Html.TextBox(Model.Type + "_GenericLiquidsOther", data.AnswerOrEmptyString("GenericLiquidsOther"), new { @class = "float-right", @id = Model.Type + "_GenericLiquidsOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Speech/Language Evaluation<br />
                <label style="font-size:xx-small;font-style:italic;">4 &#8211; WFL (Within Functional Limits) &#160; 3 &#8211; Mild Impairment &#160; 2 &#8211; Moderate Impairment &#160; 1 &#8211; Severe Impairment &#160; 0 &#8211; Unable to Assess/Did Not Test</label>
            </th>
            <td rowspan="5">
                <div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericReferralFor" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td><label class="float-left">Referral for:</label></td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor1' class='float-left radio' name='{1}_GenericReferralFor' value='1' type='checkbox' {0} />", genericReferralFor.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor1" class="float-left radio">Vision</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor2' class='float-left radio' name='{1}_GenericReferralFor' value='2' type='checkbox' {0} />", genericReferralFor.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor2" class="float-left radio">Hearing</label>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor3' class='float-left radio' name='{1}_GenericReferralFor' value='3' type='checkbox' {0} />", genericReferralFor.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor3" class="float-left radio">Swallowing</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor4' class='float-left radio' name='{1}_GenericReferralFor' value='4' type='checkbox' {0} />", genericReferralFor.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor4" class="float-left radio">Other:</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericReferralForOther", data.AnswerOrEmptyString("GenericReferralForOther"), new { @class = "", @id = Model.Type + "_GenericReferralForOther" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericDiagnosis" class="float-left">Diagnosis:</label>
                    <div class="float-right align-right">
                        <%= Html.TextBox(Model.Type + "_GenericDiagnosis", data.AnswerOrEmptyString("GenericDiagnosis"), new { @class = "", @id = Model.Type + "_GenericDiagnosis" })%><br />
                        <label for="<%= Model.Type %>_GenericDiagnosisOnsetDate" class="float-left radio">Onset</label>
                        <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_GenericDiagnosisOnsetDate" value="<%= data.AnswerOrEmptyString("GenericDiagnosisOnsetDate") %>" id="<%= Model.Type %>_GenericDiagnosisOnsetDate" />
                    </div>
                </div>
                <div class="padnoterow">
                    <div class="align-left strong">Analysis of Evaluation/Test Scores</div>
                    <div class="align-left margin">
                        <label for="<%= Model.Type %>" class="strong">Patient Desired Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericPatientDesiredOutcomes", data.AnswerOrEmptyString("GenericPatientDesiredOutcomes"), new { @id = Model.Type + "_GenericPatientDesiredOutcomes", @class = "fill" })%><br />
                        <label for="<%= Model.Type %>" class="strong">Short Term Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericShortTermOutcomes", data.AnswerOrEmptyString("GenericShortTermOutcomes"), new { @id = Model.Type + "_GenericShortTermOutcomes", @class = "fill" })%><br />
                        <label for="<%= Model.Type %>" class="strong">Long Term Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericLongTermOutcomes", data.AnswerOrEmptyString("GenericLongTermOutcomes"), new { @id = Model.Type + "_GenericLongTermOutcomes", @class = "fill" })%>
                    </div>
                </div>
                <div class="padnoterow">
                    <div class="strong">Plan of Care (check all that apply)</div>
                    <input type="hidden" name="<%= Model.Type %>_GenericPlanOfCare" value="" />
                    <table class="align-left" style="vertical-align:top;">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare1' class='radio' name='{1}_GenericPlanOfCare' value='1' type='checkbox' {0} />", genericPlanOfCare.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare1">Evaluation (C1)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare15' class='radio' name='{1}_GenericPlanOfCare' value='15' type='checkbox' {0} />", genericPlanOfCare.Contains("15").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare15">Language Processing</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare2' class='radio' name='{1}_GenericPlanOfCare' value='2' type='checkbox' {0} />", genericPlanOfCare.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare2">Establish Rehab Program</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare16' class='radio' name='{1}_GenericPlanOfCare' value='16' type='checkbox' {0} />", genericPlanOfCare.Contains("16").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare16">Food Texture Recommendations</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare3' class='radio' name='{1}_GenericPlanOfCare' value='3' type='checkbox' {0} />", genericPlanOfCare.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare3">Given to Patient</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare17' class='radio' name='{1}_GenericPlanOfCare' value='17' type='checkbox' {0} />", genericPlanOfCare.Contains("17").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare17">Safe Swallowing Evaluation</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare4' class='radio' name='{1}_GenericPlanOfCare' value='4' type='checkbox' {0} />", genericPlanOfCare.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare4">Attached to Chart</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare18' class='radio' name='{1}_GenericPlanOfCare' value='18' type='checkbox' {0} />", genericPlanOfCare.Contains("18").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare18">Therapy to Increase Articulation, Proficiency, Verbal Expression</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare5' class='radio' name='{1}_GenericPlanOfCare' value='5' type='checkbox' {0} />", genericPlanOfCare.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare5">Patient/Family Education</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare19' class='radio' name='{1}_GenericPlanOfCare' value='19' type='checkbox' {0} />", genericPlanOfCare.Contains("19").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare19">Lip, Tongue, Facial Exercises to Improve Swallowing/Vocal Skills</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare6' class='radio' name='{1}_GenericPlanOfCare' value='6' type='checkbox' {0} />", genericPlanOfCare.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare6">Voice Disorders</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare20' class='radio' name='{1}_GenericPlanOfCare' value='20' type='checkbox' {0} />", genericPlanOfCare.Contains("20").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare20">Pain Management</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare7' class='radio' name='{1}_GenericPlanOfCare' value='7' type='checkbox' {0} />", genericPlanOfCare.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare7">Speech Articulation Disorders</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare21' class='radio' name='{1}_GenericPlanOfCare' value='21' type='checkbox' {0} />", genericPlanOfCare.Contains("21").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare21">Speech Dysphagia Instruction Program</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare8' class='radio' name='{1}_GenericPlanOfCare' value='8' type='checkbox' {0} />", genericPlanOfCare.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare8">Dysphagia Treatments</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare22' class='radio' name='{1}_GenericPlanOfCare' value='22' type='checkbox' {0} />", genericPlanOfCare.Contains("22").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare22">Care of Voice Prosthesis &#8212; Removal, Cleaning, Site Maint</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare9' class='radio' name='{1}_GenericPlanOfCare' value='9' type='checkbox' {0} />", genericPlanOfCare.Contains("9").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare9">Language Disorders</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare23' class='radio' name='{1}_GenericPlanOfCare' value='23' type='checkbox' {0} />", genericPlanOfCare.Contains("23").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare23">Teach/Develop Comm. System</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare10' class='radio' name='{1}_GenericPlanOfCare' value='10' type='checkbox' {0} />", genericPlanOfCare.Contains("10").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare10">Aural Rehabilitation (C6)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare24' class='radio' name='{1}_GenericPlanOfCare' value='24' type='checkbox' {0} />", genericPlanOfCare.Contains("24").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare24">Trach Inst. and Care</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare11' class='radio' name='{1}_GenericPlanOfCare' value='11' type='checkbox' {0} />", genericPlanOfCare.Contains("11").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare11">Non-Oral Communication (C8)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare25' class='radio' name='{1}_GenericPlanOfCare' value='25' type='checkbox' {0} />", genericPlanOfCare.Contains("25").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare25">Other</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare12' class='radio' name='{1}_GenericPlanOfCare' value='12' type='checkbox' {0} />", genericPlanOfCare.Contains("12").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare12">Alaryngeal Speech Skills</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="float-left">Frequency and Duration</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericFrequencyAndDuration", data.AnswerOrEmptyString("GenericFrequencyAndDuration"), new { @class = "", @id = Model.Type + "_GenericFrequencyAndDuration" })%></div>
                </div>
                <div class="padnoterow">
                    <label class="float-left">Rehab Potential</label>
                    <div class="float-right">
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "2", data.AnswerOrEmptyString("GenericRehabPotential").Equals("2"), new { @id = Model.Type + "_GenericRehabPotential2", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential2" class="inline-radio">Good</label>
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "1", data.AnswerOrEmptyString("GenericRehabPotential").Equals("1"), new { @id = Model.Type + "_GenericRehabPotential1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential1" class="inline-radio">Fair</label>
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "0", data.AnswerOrEmptyString("GenericRehabPotential").Equals("0"), new { @id = Model.Type + "_GenericRehabPotential0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential0" class="inline-radio">Poor</label>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericEquipmentRecommendations" class="float-left">Equipment Recommendations</label>
                    <%= Html.TextArea(Model.Type + "_GenericEquipmentRecommendations", data.AnswerOrEmptyString("GenericEquipmentRecommendations"), new { @id = Model.Type + "_GenericEquipmentRecommendations", @class = "fill" })%>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSafetyIssues" class="float-left">Safety Issues/ Instruction/ Education</label>
                    <%= Html.TextArea(Model.Type + "_GenericSafetyIssues", data.AnswerOrEmptyString("GenericSafetyIssues"), new { @id = Model.Type + "_GenericSafetyIssues", @class = "fill" })%>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>" class="float-left">Comments/ Additional Information</label>
                    <%= Html.TextArea(Model.Type + "_GenericAdditionalInformation", data.AnswerOrEmptyString("GenericAdditionalInformation"), new { @id = Model.Type + "_GenericAdditionalInformation", @class = "fill" })%>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>" class="float-left">Patient/ Caregiver Response to Plan of Care</label>
                    <%= Html.TextArea(Model.Type + "_GenericResponseToPOC", data.AnswerOrEmptyString("GenericResponseToPOC"), new { @id = Model.Type + "_GenericResponseToPOC", @class = "fill" })%>
                </div>
                <div class="padnoterow">
                    <label class="float-left">Discharge discussed with:</label>
                    <input type="hidden" name="<%= Model.Type %>_GenericDischargeDiscussedWith" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith1' class='float-left radio' name='{1}_GenericDischargeDiscussedWith' value='1' type='checkbox' {0} />", genericDischargeDiscussedWith.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith1" class="float-left radio">Patient Family</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith2' class='float-left radio' name='{1}_GenericDischargeDiscussedWith' value='2' type='checkbox' {0} />", genericDischargeDiscussedWith.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith2" class="float-left radio">Case Manager</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith3' class='float-left radio' name='{1}_GenericDischargeDiscussedWith' value='3' type='checkbox' {0} />", genericDischargeDiscussedWith.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith3" class="float-left radio">Physician</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith4' class='float-left radio' name='{1}_GenericDischargeDiscussedWith' value='4' type='checkbox' {0} />", genericDischargeDiscussedWith.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith4" class="float-left radio">Other</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDischargeDiscussedWithOther", data.AnswerOrEmptyString("GenericDischargeDiscussedWithOther"), new { @class = "", @id = Model.Type + "_GenericDischargeDiscussedWithOther" })%></div>
                </div>
                <div class="padnoterow">
                    <label class="float-left">Care Coordination:</label>
                    <input type="hidden" name="<%= Model.Type %>_GenericCareCoordination" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination1' class='float-left radio' name='{1}_GenericCareCoordination' value='1' type='checkbox' {0} />", genericCareCoordination.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination1" class="float-left radio">Physician</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination2' class='float-left radio' name='{1}_GenericCareCoordination' value='2' type='checkbox' {0} />", genericCareCoordination.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination2" class="float-left radio">PT</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination3' class='float-left radio' name='{1}_GenericCareCoordination' value='3' type='checkbox' {0} />", genericCareCoordination.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination3" class="float-left radio">OT</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination4' class='float-left radio' name='{1}_GenericCareCoordination' value='4' type='checkbox' {0} />", genericCareCoordination.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination4" class="float-left radio">ST</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination5' class='float-left radio' name='{1}_GenericCareCoordination' value='5' type='checkbox' {0} />", genericCareCoordination.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination5" class="float-left radio">MSW</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination6' class='float-left radio' name='{1}_GenericCareCoordination' value='6' type='checkbox' {0} />", genericCareCoordination.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination6" class="float-left radio">SN</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination7' class='float-left radio' name='{1}_GenericCareCoordination' value='7' type='checkbox' {0} />", genericCareCoordination.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination7" class="float-left radio">Other</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericCareCoordinationOther", data.AnswerOrEmptyString("GenericCareCoordinationOther"), new { @class = "", @id = Model.Type + "_GenericCareCoordinationOther" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericPlanForNextVisit" class="float-left">Plan for Next Visit</label>
                    <%= Html.TextArea(Model.Type + "_GenericPlanForNextVisit", data.AnswerOrEmptyString("GenericPlanForNextVisit"), new { @id = Model.Type + "_GenericPlanForNextVisit", @class = "fill" })%>
                </div>                        
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Cognition Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrientationScore" class="float-left">Orientation (Person/Place/Time)</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOrientationScore", data.AnswerOrEmptyString("GenericOrientationScore"), new { @class = "vitals", @id = Model.Type + "_GenericOrientationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAttentionSpanScore" class="float-left">Attention Span</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAttentionSpanScore", data.AnswerOrEmptyString("GenericAttentionSpanScore"), new { @class = "vitals", @id = Model.Type + "_GenericAttentionSpanScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericShortTermMemoryScore" class="float-left">Short Term Memory</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericShortTermMemoryScore", data.AnswerOrEmptyString("GenericShortTermMemoryScore"), new { @class = "vitals", @id = Model.Type + "_GenericShortTermMemoryScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericLongTermMemoryScore" class="float-left">Long Term Memory</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLongTermMemoryScore", data.AnswerOrEmptyString("GenericLongTermMemoryScore"), new { @class = "vitals", @id = Model.Type + "_GenericLongTermMemoryScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericJudgmentScore" class="float-left">Judgment</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericJudgmentScore", data.AnswerOrEmptyString("GenericJudgmentScore"), new { @class = "vitals", @id = Model.Type + "_GenericJudgmentScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProblemSolvingScore" class="float-left">Problem Solving</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericProblemSolvingScore", data.AnswerOrEmptyString("GenericProblemSolvingScore"), new { @class = "vitals", @id = Model.Type + "_GenericProblemSolvingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrganizationScore" class="float-left">Organization</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOrganizationScore", data.AnswerOrEmptyString("GenericOrganizationScore"), new { @class = "vitals", @id = Model.Type + "_GenericOrganizationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedOtherScore" class="float-left">Other:</label><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOther"), new { @class = "", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOther" })%>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericCognitionFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Speech/Voice Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralFacialExamScore" class="float-left">Oral/Facial Exam</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOralFacialExamScore", data.AnswerOrEmptyString("GenericOralFacialExamScore"), new { @class = "vitals", @id = Model.Type + "_GenericOralFacialExamScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericArticulationScore" class="float-left">Articulation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericArticulationScore", data.AnswerOrEmptyString("GenericArticulationScore"), new { @class = "vitals", @id = Model.Type + "_GenericArticulationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProsodyScore" class="float-left">Prosody</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericProsodyScore", data.AnswerOrEmptyString("GenericProsodyScore"), new { @class = "vitals", @id = Model.Type + "_GenericProsodyScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVoiceRespirationScore" class="float-left">Voice/Respiration</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVoiceRespirationScore", data.AnswerOrEmptyString("GenericVoiceRespirationScore"), new { @class = "vitals", @id = Model.Type + "_GenericVoiceRespirationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechIntelligibilityScore" class="float-left">Speech Intelligibility</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechIntelligibilityScore", data.AnswerOrEmptyString("GenericSpeechIntelligibilityScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechIntelligibilityScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedOtherScore" class="float-left">Other: <%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOther"), new { @class = "", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOther" })%></label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSpeechFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Auditory Comprehension Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWordDiscriminationScore" class="float-left">Word Discrimination</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWordDiscriminationScore", data.AnswerOrEmptyString("GenericWordDiscriminationScore"), new { @class = "vitals", @id = Model.Type + "_GenericWordDiscriminationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOneStepDirectionsScore" class="float-left">One Step Directions</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOneStepDirectionsScore", data.AnswerOrEmptyString("GenericOneStepDirectionsScore"), new { @class = "vitals", @id = Model.Type + "_GenericOneStepDirectionsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericTwoStepDirectionsScore" class="float-left">Two Step Directions</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTwoStepDirectionsScore", data.AnswerOrEmptyString("GenericTwoStepDirectionsScore"), new { @class = "vitals", @id = Model.Type + "_GenericTwoStepDirectionsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericComplexSentencesScore", data.AnswerOrEmptyString("GenericComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericConversationScore" class="float-left">Conversation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericConversationScore", data.AnswerOrEmptyString("GenericConversationScore"), new { @class = "vitals", @id = Model.Type + "_GenericConversationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechReadingScore" class="float-left">Speech Reading</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechReadingScore", data.AnswerOrEmptyString("GenericSpeechReadingScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechReadingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericACFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericACFEComment", data.AnswerOrEmptyString("GenericACFEComment"), new { @id = Model.Type + "_GenericACFEComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Swallowing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericChewingAbilityScore" class="float-left">Chewing Ability</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChewingAbilityScore", data.AnswerOrEmptyString("GenericChewingAbilityScore"), new { @class = "vitals", @id = Model.Type + "_GenericChewingAbilityScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralStageManagementScore" class="float-left">Oral Stage Management</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOralStageManagementScore", data.AnswerOrEmptyString("GenericOralStageManagementScore"), new { @class = "vitals", @id = Model.Type + "_GenericOralStageManagementScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericPharyngealStageManagementScore" class="float-left">Pharyngeal Stage Management</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPharyngealStageManagementScore", data.AnswerOrEmptyString("GenericPharyngealStageManagementScore"), new { @class = "vitals", @id = Model.Type + "_GenericPharyngealStageManagementScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericReflexTimeScore" class="float-left">Reflex Time</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericReflexTimeScore", data.AnswerOrEmptyString("GenericReflexTimeScore"), new { @class = "vitals", @id = Model.Type + "_GenericReflexTimeScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedOtherScore" class="float-left">Other:</label><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOther"), new { @class = "", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOther" })%>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSwallowingFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Verbal Expression Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAugmentativeMethodsScore" class="float-left">Augmentative Methods</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAugmentativeMethodsScore", data.AnswerOrEmptyString("GenericAugmentativeMethodsScore"), new { @class = "vitals", @id = Model.Type + "_GenericAugmentativeMethodsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericNamingScore" class="float-left">Naming</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericNamingScore", data.AnswerOrEmptyString("GenericNamingScore"), new { @class = "vitals", @id = Model.Type + "_GenericNamingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAppropriateScore" class="float-left">Appropriate</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAppropriateScore", data.AnswerOrEmptyString("GenericAppropriateScore"), new { @class = "vitals", @id = Model.Type + "_GenericAppropriateScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVEFEComplexSentencesScore", data.AnswerOrEmptyString("GenericVEFEComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericVEFEComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEConversationScore" class="float-left">Conversation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVEFEConversationScore", data.AnswerOrEmptyString("GenericVEFEConversationScore"), new { @class = "vitals", @id = Model.Type + "_GenericVEFEConversationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericVEFEComment", data.AnswerOrEmptyString("GenericVEFEComment"), new { @id = Model.Type + "_GenericVEFEComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Reading Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFELettersNumbersScore" class="float-left">Letters/Numbers</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFELettersNumbersScore", data.AnswerOrEmptyString("GenericRFELettersNumbersScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFELettersNumbersScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEWordsScore" class="float-left">Words</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFEWordsScore", data.AnswerOrEmptyString("GenericRFEWordsScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFEWordsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFESimpleSentencesScore" class="float-left">Simple Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFESimpleSentencesScore", data.AnswerOrEmptyString("GenericRFESimpleSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFESimpleSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFEComplexSentencesScore", data.AnswerOrEmptyString("GenericRFEComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFEComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericParagraphScore" class="float-left">Paragraph</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericParagraphScore", data.AnswerOrEmptyString("GenericParagraphScore"), new { @class = "vitals", @id = Model.Type + "_GenericParagraphScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericRFEComment", data.AnswerOrEmptyString("GenericRFEComment"), new { @id = Model.Type + "_GenericRFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="align-center strong">Writing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFELettersNumbersScore" class="float-left">Letters/Numbers</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFELettersNumbersScore", data.AnswerOrEmptyString("GenericWFELettersNumbersScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFELettersNumbersScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEWordsScore" class="float-left">Words</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFEWordsScore", data.AnswerOrEmptyString("GenericWFEWordsScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFEWordsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESentencesScore" class="float-left">Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFESentencesScore", data.AnswerOrEmptyString("GenericWFESentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFESentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESpellingScore" class="float-left">Spelling</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFESpellingScore", data.AnswerOrEmptyString("GenericWFESpellingScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFESpellingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericFormulationScore" class="float-left">Formulation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericFormulationScore", data.AnswerOrEmptyString("GenericFormulationScore"), new { @class = "vitals", @id = Model.Type + "_GenericFormulationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSimpleAdditionSubtractionScore" class="float-left">Simple Addition/Subtraction</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSimpleAdditionSubtractionScore", data.AnswerOrEmptyString("GenericSimpleAdditionSubtractionScore"), new { @class = "vitals", @id = Model.Type + "_GenericSimpleAdditionSubtractionScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericWFEComment", data.AnswerOrEmptyString("GenericWFEComment"), new { @id = Model.Type + "_GenericWFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>   
    </tbody>
</table>