﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<%var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Speech Therapist Visit<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body></body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3ESpeech Therapy Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : ""%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.Data.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.Data.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.LocationProfile.Name.Clean().IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3ESpeech Therapist Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        printview.addsection(
            printview.span("Treatment Diagnosis/Problem") +
            printview.span("<%= data != null && data.ContainsKey("GenericTreatmentDiagnosis") && data["GenericTreatmentDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentDiagnosis"].Answer.Clean() : string.Empty %>",0,10));
        printview.addsection(
            printview.span("Short Term Goals") +
            printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoals") && data["GenericShortTermGoals"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoals"].Answer.Clean() : string.Empty %>",0,10));
        printview.addsection(
            printview.col(3,
                printview.checkbox("Speech (C2)",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Lip, tongue, facial, exercises to improve swallowing/vocal skills",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Voice",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Dysphagia Treatments (C4)",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Fluency",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Language Disorder (C5)",<%= data != null && data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer.Split(',').Contains("6") ? "true" : "false"%>)));
        printview.addsection(
            printview.span("Results Of Therapy Session") +
            printview.span("<%= data != null && data.ContainsKey("GenericResultsOfTherapySession") && data["GenericResultsOfTherapySession"].Answer.IsNotNullOrEmpty() ? data["GenericResultsOfTherapySession"].Answer.Clean() : string.Empty %>",0,10)); <%
    }).Render(); %>
</html>
