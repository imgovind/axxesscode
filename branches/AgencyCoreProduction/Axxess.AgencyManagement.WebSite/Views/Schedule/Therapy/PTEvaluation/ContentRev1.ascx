﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="5">Vital Signs</th>
            <th colspan="5">Diagnosis</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev1.ascx", Model); %></td>
            <td colspan="5" rowspan="3"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="5">Status and History</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Mental Assessment</th>
            <th colspan="6">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericMentalAssessmentOrientation" class="float-left">Orientation:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericMentalAssessmentLOC" class="float-left">LOC:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentLOC" })%></div>
            </td>
            <td colspan="6" rowspan="9"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Pain Assessment</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">DME</th>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">Available:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @class = "", @id = Model.Type + "_GenericDMEAvailable" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">Needs:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @class = "", @id = Model.Type + "_GenericDMENeeds" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">Suggestion:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @class = "", @id = Model.Type + "_GenericDMESuggestion" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Home Safety Evaluation</th>
        </tr>
        <tr>
            <td colspan="4">
                <label class="float-left">Level</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel1" class="inline-radio">One</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel0" class="inline-radio">Multiple</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Stairs</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs0" class="inline-radio">No</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Lives</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "2", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives2", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives2" class="inline-radio">Alone</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives1" class="inline-radio">Family</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives0" class="inline-radio">Friends</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Support</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationSupport", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationSupport").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationSupport1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationSupport1" class="inline-radio">Retirement</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationSupport", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationSupport").Equals("0"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationSupport0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationSupport0" class="inline-radio">Assisted Living</label>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Physical Assessment</th>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSpeech" class="float-left">Speech:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSpeech", data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSpeech" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentVision" class="float-left">Vision:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentVision", data.AnswerOrEmptyString("GenericPhysicalAssessmentVision"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentVision" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentHearing" class="float-left">Hearing:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentHearing", data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentHearing" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSkin" class="float-left">Skin:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSkin", data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSkin" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEdema" class="float-left">Edema:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEdema", data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEdema" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentMuscleTone" class="float-left">Muscle Tone:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentMuscleTone", data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentMuscleTone" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentCoordination" class="float-left">Coordination:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentCoordination", data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentCoordination" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSensation" class="float-left">Sensation:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSensation", data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSensation" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEndurance" class="float-left">Endurance:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEndurance", data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEndurance" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Bed Mobility</th>
            <th colspan="5">Gait</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev1.ascx", Model); %></td>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="5">Transfer</th>
            <th colspan="5">WBS</th>
        </tr>
        <tr>
            <td colspan="5" rowspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev1.ascx", Model); %></td>
            <td colspan="5">
                <table>
                    <thead>
                        <tr>
                            <th class="strong">Assistive Device</th>
                            <th class="strong">Description</th>
                            <th class="strong">Posture</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><%= Html.TextBox(Model.Type + "_GenericWBSAssistiveDevice", data.AnswerOrEmptyString("GenericWBSAssistiveDevice"), new { @class = "sn", @id = Model.Type + "_GenericWBSAssistiveDevice" })%> %</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericWBSDescription", data.AnswerOrEmptyString("GenericWBSDescription"), new { @class = "", @style = "width:90%", @id = Model.Type + "_GenericWBSDescription" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericWBSPosture", data.AnswerOrEmptyString("GenericWBSPosture"), new { @class = "", @style = "width:90%", @id = Model.Type + "_GenericWBSPosture" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="5">W/C Mobility</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="5">Assessment</th>
        </tr>
        <tr>
            <td colspan="5"><%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.ContainsKey("GenericAssessmentComment") ? data["GenericAssessmentComment"].Answer : string.Empty, 3,20,new { @id = Model.Type + "_GenericAssessmentComment", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="5">Treatment Codes</th>
            <th colspan="5">Treatment Plan</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentCodes/FormRev1.ascx", Model); %></td>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="5">Short Term Goals</th>
            <th colspan="5">Long Term Goals</th>
        </tr>
        <tr>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ShortTermGoals/FormRev1.ascx", Model); %></td>
            <td colspan="5"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LongTermGoals/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="10">Other Discipline Recommendation</th>
        </tr>
        <tr>
            <td colspan="10">
                <%  string[] genericDisciplineRecommendation = data.AnswerArray("GenericDisciplineRecommendation"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation1' class='radio' name='{1}_GenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation1">OT</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation2' class='radio' name='{1}_GenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation2">MSW</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation3' class='radio' name='{1}_GenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation3">ST</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation4' class='radio' name='{1}_GenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation4">Podiatrist</label>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendationOther" class="float-left">Other</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.AnswerOrEmptyString("GenericDisciplineRecommendationOther"), new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%></div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="float-left">Reason</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationReason", data.AnswerOrEmptyString("GenericDisciplineRecommendationReason"), new { @class = "", @id = Model.Type + "_GenericDisciplineRecommendationReason" })%></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericFrequency" class="float-left">Frequency:</label>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @class = "sn", @id = Model.Type + "_GenericFrequency" })%>
                                    X wk for
                                    <%= Html.TextBox(Model.Type + "_GenericFrequencyFor", data.AnswerOrEmptyString("GenericFrequencyFor"), new { @class = "sn", @id = Model.Type + "_GenericFrequencyFor" })%>
                                </div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericRehabPotential" class="float-left">Rehab Potential:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @class = "", @id = Model.Type + "_GenericRehabPotential" })%></div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPrognosis" class="float-left">Prognosis:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPrognosis", data.AnswerOrEmptyString("GenericPrognosis"), new { @class = "", @id = Model.Type + "_GenericPrognosis" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="5">Care Coordination</th>
            <th colspan="5">Skilled Care Provided This Visit</th>
        </tr>
        <tr>
            <td colspan="5"><%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%></td>
            <td colspan="5"><%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.ContainsKey("GenericSkilledCareProvided") ? data["GenericSkilledCareProvided"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%></td>
        </tr>       
    </tbody>
</table>