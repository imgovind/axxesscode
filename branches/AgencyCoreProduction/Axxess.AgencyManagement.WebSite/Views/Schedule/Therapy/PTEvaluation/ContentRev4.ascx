﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="10">Vital Signs
            <%= string.Format("<input class='radio' id='{0}IsVitalSignsApply' name='{0}_IsVitalSignsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsVitalSignsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10">
            <div id="<%= Model.Type %>VitalSignsContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Mental Assessment
            <%= string.Format("<input class='radio' id='{0}IsMentalApply' name='{0}_IsMentalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMentalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="6">Physical Assessment
            <%= string.Format("<input class='radio' id='{0}IsPhysicalAssessmentApply' name='{0}_IsPhysicalAssessmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPhysicalAssessmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%= Model.Type %>MentalContainer">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Orientation:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></td>
                        </tr>
                        <tr>
                            <td class="align-left strong">Level of Consciousness:</td>
                            <td><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @class = "", @id = Model.Type + "_GenericMentalAssessmentLOC" })%></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericMentalAssessmentComment" class="strong">Comment</label>
                                    <%= Html.ToggleTemplates(Model.Type + "_MentalAssessmentTemplates", "", "#" + Model.Type + "_GenericMentalAssessmentComment")%>
                                    <%= Html.TextArea(Model.Type + "_GenericMentalAssessmentComment", data.ContainsKey("GenericMentalAssessmentComment") ? data["GenericMentalAssessmentComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericMentalAssessmentComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table></div>
            </td>
            <td colspan="6" rowspan="9"><div id="<%=Model.Type %>PhysicalAssessmentContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="4">Pain Assessment
            <%= string.Format("<input class='radio' id='{0}IsPainApply' name='{0}_IsPainApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPainApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>PainContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></div></td>
            
        </tr>
        
        <tr>
            <th colspan="4">Living Situation
            <%= string.Format("<input class='radio' id='{0}IsLivingApply' name='{0}_IsLivingApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsLivingApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>LivingContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Physical Assessment
            <%= string.Format("<input class='radio' id='{0}IsPhysicalApply' name='{0}_IsPhysicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPhysicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>PhysicalContainer">
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSpeech" class="float-left">Speech:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSpeech", data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSpeech" }) %></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentVision" class="float-left">Vision:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentVision", data.AnswerOrEmptyString("GenericPhysicalAssessmentVision"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentVision" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentHearing" class="float-left">Hearing:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentHearing", data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentHearing" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSkin" class="float-left">Skin:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSkin", data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSkin" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEdema" class="float-left">Edema:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEdema", data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEdema" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentMuscleTone" class="float-left">Muscle Tone:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentMuscleTone", data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentMuscleTone" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentCoordination" class="float-left">Coordination:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentCoordination", data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentCoordination" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSensation" class="float-left">Sensation:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSensation", data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSensation" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEndurance" class="float-left">Endurance:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEndurance", data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEndurance" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSafetyAwareness" class="float-left">Safety Awareness:</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSafetyAwareness", data.AnswerOrEmptyString("GenericPhysicalAssessmentSafetyAwareness"), new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSafetyAwareness" })%></div>
                <div class="clear"></div>   
                </div>         
            </td>
        </tr>
        <tr>
            <th colspan="4">Prior And Current Level Of Function
            <%= string.Format("<input class='radio' id='{0}IsLOFApply' name='{0}_IsLOFApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsLOFApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>LOFContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LevelOfFunction/FormRev1.ascx", Model); %>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Homebound Reason
            <%= string.Format("<input class='radio' id='{0}IsHomeboundApply' name='{0}_IsHomeboundApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsHomeboundApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="6">Prior Therapy Received
            <%= string.Format("<input class='radio' id='{0}IsPTRApply' name='{0}_IsPTRApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPTRApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="4"><div id="<%=Model.Type %>HomeboundContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></div>
            </td>
            <td colspan="6"><div id="<%=Model.Type %>PTRContainer">
                <%= Html.ToggleTemplates(Model.Type + "_GenericPriorTherapyTemplates", "", "#" + Model.Type + "_GenericPriorTherapyReceived")%>
                <%= Html.TextArea(Model.Type + "_GenericPriorTherapyReceived", data.AnswerOrEmptyString("GenericPriorTherapyReceived"),6,20, new { @id = Model.Type + "_GenericPriorTherapyReceived", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Functional Mobility Key</th>
        </tr>
        <tr>
            <td colspan="10">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>I = Independent</td>
                            <td>S = Supervision</td>
                            <td>VC = Verbal Cue</td>
                            <td>CGA = Contact Guard Assist</td>
                        </tr>
                        <tr>
                            <td>Min A = 25% Assist</td>
                            <td>Mod A = 50% Assist</td>
                            <td>Max A = 75% Assist</td>
                            <td>Total = 100% Assist</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="5">Bed Mobility
            <%= string.Format("<input class='radio' id='{0}IsBedApply' name='{0}_IsBedApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsBedApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Gait Analysis
            <%= string.Format("<input class='radio' id='{0}IsGaitApply' name='{0}_IsGaitApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGaitApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>BedContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev2.ascx", Model); %></div></td>
            <td colspan="5"><div id="<%=Model.Type %>GaitContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="5">Transfer
            <%= string.Format("<input class='radio' id='{0}IsTransferApply' name='{0}_IsTransferApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTransferApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">WB
            <%= string.Format("<input class='radio' id='{0}IsWBApply' name='{0}_IsWBApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsWBApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5" rowspan="5"><div id="<%=Model.Type %>TransferContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev2.ascx", Model); %></div></td>
            <td colspan="5"><div id="<%=Model.Type %>WBContainer">
                <table>
                    <tbody>
                        <tr>
                            <td class="align-left strong">Status:</td>
                            <td><%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus", data.AnswerOrEmptyString("GenericWBStatus"), new { @id = Model.Type + "_GenericWBStatus", @class = "" })%></td>
                            <td><span>Other: </span><%= Html.TextBox(Model.Type + "_GenericWBStatusOther", data.AnswerOrEmptyString("GenericWBStatusOther"), new { @class = "", @id = Model.Type + "_GenericWBStatusOther" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div>
                                    <label for="<%= Model.Type %>_GenericWBSComment" class="strong">Comment</label>
                                    <%= Html.ToggleTemplates(Model.Type + "_WBSTemplates", "", "#" + Model.Type + "_GenericWBSComment")%>
                                    <%= Html.TextArea(Model.Type + "_GenericWBSComment", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment", @class = "fill" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">
                W/C Mobility &#8212;
                <%  string[] isWCMobilityApplied = data.AnswerArray("GenericIsWCMobilityApplied"); %>
                <%= Html.Hidden(Model.Type + "isWCMobilityApplied", string.Empty, new { @id = Model.Type + "_GenericIsWCMobilityAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsWCMobilityApplied1' name='{0}_GenericIsWCMobilityApplied' value='1' type='checkbox' {1} />", Model.Type, isWCMobilityApplied.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericIsWCMobilityApplied1">N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%= Model.Type %>_WCMobilityContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="5">Assessment
            <%= string.Format("<input class='radio' id='{0}IsAssessmentApply' name='{0}_IsAssessmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsAssessmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>AssessmentContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates", "", "#" + Model.Type + "_GenericAssessmentComment")%>
            <%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.ContainsKey("GenericAssessmentComment") ? data["GenericAssessmentComment"].Answer : string.Empty, 3,20,new { @id = Model.Type + "_GenericAssessmentComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="10">Standardized test
            <%= string.Format("<input class='radio' id='{0}IsTestApply' name='{0}_IsTestApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTestApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>TestContainer1">
                <div class="float-middle strong">Prior</div>
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA", data.AnswerOrEmptyString("GenericTinettiPOMA"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp", data.AnswerOrEmptyString("GenericTimedGetUp"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach", data.AnswerOrEmptyString("GenericFunctionalReach"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span>
                <%= Html.ToggleTemplates(Model.Type + "_StandardizedTestTemplates1", "", "#" + Model.Type + "_GenericStandardizedTestOther")%>
                <%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther" })%></div>
            </div>
            </td>
            <td colspan="5"><div id="<%=Model.Type %>TestContainer2">
                <div class="float-middle strong">Current</div>
                <div><span class="float-left">Tinetti POMA:</span><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA1", data.AnswerOrEmptyString("GenericTinettiPOMA1"), new { @class = "float-right", @id = Model.Type + "_GenericTinettiPOMA1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Timed Get Up & Go Test:</span><%= Html.TextBox(Model.Type + "_GenericTimedGetUp1", data.AnswerOrEmptyString("GenericTimedGetUp1"), new { @class = "float-right", @id = Model.Type + "_GenericTimedGetUp1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Functional Reach:</span><%= Html.TextBox(Model.Type + "_GenericFunctionalReach1", data.AnswerOrEmptyString("GenericFunctionalReach1"), new { @class = "float-right", @id = Model.Type + "_GenericFunctionalReach1" })%></div>
                <div class="clear"></div>
                <div><span class="float-left">Other:</span>
                <%= Html.ToggleTemplates(Model.Type + "_StandardizedTestTemplates2", "", "#" + Model.Type + "_GenericStandardizedTestOther1")%>
                <%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @class = "fill", @id = Model.Type + "_GenericStandardizedTestOther1" })%></div>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="10">MD Orders</th>
        </tr>
        <tr>
            <th colspan="5">DME
            <%= string.Format("<input class='radio' id='{0}IsDMEApply' name='{0}_IsDMEApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDMEApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Medical Diagnosis
            <%= string.Format("<input class='radio' id='{0}IsMedicalApply' name='{0}_IsMedicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMedicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>DMEContainer">
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">Available:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @class = "", @id = Model.Type + "_GenericDMEAvailable" }) %>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">Needs:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @class = "", @id = Model.Type + "_GenericDMENeeds" })%>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">Suggestion:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @class = "", @id = Model.Type + "_GenericDMESuggestion" })%>
                </div>
                </div>
            </td>
            <td colspan="5"><div id="<%=Model.Type %>MedicalContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Treatment Plan
            <%= string.Format("<input class='radio' id='{0}IsTreatmentApply' name='{0}_IsTreatmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTreatmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>TreatmentContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev4.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">Modalities<%= string.Format("<input class='radio' id='{0}IsModalitiesApply' name='{0}_IsModalitiesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsModalitiesApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>ModalitiesContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericModalitiesTemplates", "", "#" + Model.Type + "_GenericModalitiesComment")%>
            <%= Html.TextArea(Model.Type + "_GenericModalitiesComment", data.ContainsKey("GenericModalitiesComment") ? data["GenericModalitiesComment"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericModalitiesComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="10">PT Goals
            <%= string.Format("<input class='radio' id='{0}IsGoalsApply' name='{0}_IsGoalsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGoalsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>GoalsContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PTGoals/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">Other Discipline Recommendation
            <%= string.Format("<input class='radio' id='{0}IsRecommendationApply' name='{0}_IsRecommendationApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRecommendationApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>RecommendationContainer">
                <%  string[] genericDisciplineRecommendation = data.AnswerArray("GenericDisciplineRecommendation"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <div><label for="<%= Model.Type %>_GenericDisciplineRecommendation1" class="float-left">Disciplines</label></div>
                                <div class="margin">
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation1' class='radio' name='{1}_GenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation1">OT</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation2' class='radio' name='{1}_GenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation2">MSW</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation3' class='radio' name='{1}_GenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation3">ST</label>
                                    <%= string.Format("<input id='{1}_GenericDisciplineRecommendation4' class='radio' name='{1}_GenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation4">Podiatrist</label>
                                    <span>Other</span>
                                    <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.AnswerOrEmptyString("GenericDisciplineRecommendationOther"), new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="strong">Reason</label>
                                    <%= Html.ToggleTemplates(Model.Type + "_DisciplineRecommendationTemplates", "", "#" + Model.Type + "_GenericDisciplineRecommendationReason")%>
                                    <%= Html.TextArea(Model.Type + "_GenericDisciplineRecommendationReason", data.AnswerOrEmptyString("GenericDisciplineRecommendationReason"), new { @class = "fill", @id = Model.Type + "_GenericDisciplineRecommendationReason" })%>
                                </div>
                            </td>
                        </tr>
                        
                        
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Rehab <%= string.Format("<input class='radio' id='{0}IsRehabApply' name='{0}_IsRehabApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRehabApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Discharge Plan<%= string.Format("<input class='radio' id='{0}IsDCPlanApply' name='{0}_IsDCPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDCPlanApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>RehabContainer">
                <div class="float-left">
					<label for="<%= Model.Type %>_GenericDisciplineRehabDiagnosis" class="strong">Rehab Diagnosis:</label>
					<%= Html.TextBox(Model.Type + "_GenericDisciplineRehabDiagnosis", data.AnswerOrEmptyString("GenericDisciplineRehabDiagnosis"), new { @class = "", @id = Model.Type + "_GenericDisciplineRehabDiagnosis" })%>
				</div>
                <div class="clear" />        
                <div class="float-left">
                    <label for="<%= Model.Type %>_GenericRehabPotential" class="strong">Rehab Potential</label>
                    <%= Html.RadioButton(Model.Type + "_RehabPotential", "0", data.AnswerOrEmptyString("RehabPotential").Equals("0"), new { @id = Model.Type + "_RehabPotential0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential0" class="inline-radio">Good</label>
                    <%= Html.RadioButton(Model.Type + "_RehabPotential", "1", data.AnswerOrEmptyString("RehabPotential").Equals("1"), new { @id = Model.Type + "_RehabPotential1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential1" class="inline-radio">Fair</label>
                    <%= Html.RadioButton(Model.Type + "_RehabPotential", "2", data.AnswerOrEmptyString("RehabPotential").Equals("2"), new { @id = Model.Type + "_RehabPotential2", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential2" class="inline-radio">Poor</label>
                </div>
                <div class="clear" />
                <%= Html.ToggleTemplates(Model.Type + "_OtherRehabPotentialTemplate", "", "#" + Model.Type + "_OtherRehabPotential")%>
                <%= Html.TextArea(Model.Type + "_OtherRehabPotential", data.ContainsKey("OtherRehabPotential") ? data["OtherRehabPotential"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_OtherRehabPotential", @class = "fill" })%>
               </div> 
            </td>
            <td colspan="5"><div id="<%=Model.Type %>DCPlanContainer">
                <label class="strong float-left">Patient to be discharged to the care of:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_DCPlanCareOf1' class='radio' name='{1}_DCPlanCareOf' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("DCPlanCareOf").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Physician</label>
                    <%= string.Format("<input id='{1}_DCPlanCareOf2' class='radio' name='{1}_DCPlanCareOf' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("DCPlanCareOf").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Caregiver</label>
                    <%= string.Format("<input id='{1}_DCPlanCareOf3' class='radio' name='{1}_DCPlanCareOf' value='3' type='checkbox' {0} />", data.AnswerOrEmptyString("DCPlanCareOf").Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Self care</label>
                </div>
                <div class="clear" />
                <label class="strong float-left">Discharge Plans:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_DCPlanPlans1' class='radio' name='{1}_DCPlanPlans' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("DCPlanPlans").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanPlans1">Discharge when caregiver willing and able to manage all aspects of patient’s care</label>
                    <%= string.Format("<input id='{1}_DCPlanPlans1' class='radio' name='{1}_DCPlanPlans' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("DCPlanPlans").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanPlans1">Discharge when goals met.</label>
                </div>
            <div class="clear" />
                <%= Html.ToggleTemplates(Model.Type + "_DCPlanAdditionalTemplate", "", "#" + Model.Type + "_DCPlanAdditional")%>
                <%= Html.TextArea(Model.Type + "_DCPlanAdditional", data.ContainsKey("DCPlanAdditional") ? data["DCPlanAdditional"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_DCPlanAdditional", @class = "fill" })%>
            
            </div>
            </td>
            
        </tr>
        
        <tr>
            <th colspan="10">Skilled Care Provided
            <%= string.Format("<input class='radio' id='{0}IsSkilledCareApply' name='{0}_IsSkilledCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSkilledCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5" valign="top">
                <div id="<%=Model.Type %>SkilledCareContainer1">
                        <span class="float-left">Training Topics:</span>
                        <%= Html.ToggleTemplates(Model.Type + "_SkilledCareTrainingTopicsTemplate", "", "#" + Model.Type + "_SkilledCareTrainingTopics")%>
                        <%= Html.TextArea(Model.Type + "_SkilledCareTrainingTopics", data.AnswerOrEmptyString("SkilledCareTrainingTopics"), 4,20, new { @class = "fill", @id = Model.Type + "_SkilledCareTrainingTopics" })%>
                        <div class="clear" />
                        <span class="float-left">Trained:</span>
                        <%= string.Format("<input class='radio' id='{0}SkilledCareTrained0' name='{0}_SkilledCareTrained' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkilledCareTrained").Contains("0").ToChecked())%>Patient
                        <%= string.Format("<input class='radio' id='{0}SkilledCareTrained1' name='{0}_SkilledCareTrained' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkilledCareTrained").Contains("1").ToChecked())%>Caregiver
                    
                    </div>
                    </td>
                    <td colspan="5">
                    <div id="<%=Model.Type %>SkilledCareContainer2">
                        <span class="float-left">Treatment Performed:</span>
                        <%= Html.ToggleTemplates(Model.Type + "_SkilledCareTreatmentPerformedTemplate", "", "#" + Model.Type + "_SkilledCareTreatmentPerformed")%>
                        <%= Html.TextArea(Model.Type + "_SkilledCareTreatmentPerformed", data.AnswerOrEmptyString("SkilledCareTreatmentPerformed"),4,20, new { @class = "fill", @id = Model.Type + "_SkilledCareTreatmentPerformed" })%>
                        <div class="clear" />
                        <span class="float-left">Patient Response:</span>
                        <%= Html.ToggleTemplates(Model.Type + "_SkilledCarePatientResponseTemplate", "", "#" + Model.Type + "_SkilledCarePatientResponse")%>
                        <%= Html.TextArea(Model.Type + "_SkilledCarePatientResponse", data.AnswerOrEmptyString("SkilledCarePatientResponse"),4,20, new { @class = "fill", @id = Model.Type + "_SkilledCarePatientResponse" })%>
                    </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Care Coordination
            <%= string.Format("<input class='radio' id='{0}IsCareApply' name='{0}_IsCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Safety Issues/Instruction/Education 
            <%= string.Format("<input class='radio' id='{0}IsSafetyIssueApply' name='{0}_IsSafetyIssueApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSafetyIssueApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>CareContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates", "", "#" + Model.Type + "_GenericCareCoordination")%>
            <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%>
            </div></td>
            <td colspan="5"><div id="<%=Model.Type %>SafetyIssuesInstructionContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericSafetyIssueTemplates", "", "#" + Model.Type + "_GenericSafetyIssue")%>
            <%= Html.TextArea(Model.Type + "_GenericSafetyIssue", data.ContainsKey("GenericSafetyIssue") ? data["GenericSafetyIssue"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericSafetyIssue", @class = "fill" })%>
            </div></td>
        </tr>  
        <tr>
            <th colspan="10">Notification</th>
        </tr>
        <tr>
            <td colspan="10">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev2.ascx", Model); %>
            </td>
        </tr>     
    </tbody>
</table>

<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>IsVitalSignsApply"), $("#<%= Model.Type %>VitalSignsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMentalApply"), $("#<%= Model.Type %>MentalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMedicalApply"), $("#<%= Model.Type %>MedicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPainApply"), $("#<%= Model.Type %>PainContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsDMEApply"), $("#<%= Model.Type %>DMEContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLivingApply"), $("#<%= Model.Type %>LivingContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalApply"), $("#<%= Model.Type %>PhysicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLOFApply"), $("#<%= Model.Type %>LOFContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalAssessmentApply"), $("#<%= Model.Type %>PhysicalAssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsHomeboundApply"), $("#<%= Model.Type %>HomeboundContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPTRApply"), $("#<%= Model.Type %>PTRContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsBedApply"), $("#<%= Model.Type %>BedContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGaitApply"), $("#<%= Model.Type %>GaitContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTransferApply"), $("#<%= Model.Type %>TransferContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsWBApply"), $("#<%= Model.Type %>WBContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsAssessmentApply"), $("#<%= Model.Type %>AssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTreatmentApply"), $("#<%= Model.Type %>TreatmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGoalsApply"), $("#<%= Model.Type %>GoalsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRecommendationApply"), $("#<%= Model.Type %>RecommendationContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsCareApply"), $("#<%= Model.Type %>CareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsDCPlanApply"), $("#<%= Model.Type %>DCPlanContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsModalitiesApply"), $("#<%= Model.Type %>ModalitiesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSafetyIssueApply"), $("#<%= Model.Type %>SafetyIssuesInstructionContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRehabApply"), $("#<%= Model.Type %>RehabContainer"));
    U.HideIfChecked($("#<%= Model.Type %>_GenericIsWCMobilityApplied1"), $("#<%= Model.Type %>_WCMobilityContainer"));
</script>