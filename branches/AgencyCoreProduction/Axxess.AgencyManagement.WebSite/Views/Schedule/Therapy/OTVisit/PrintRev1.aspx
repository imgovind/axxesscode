﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Data.Type == "COTAVisit" ? "COTA" : "Occupational Therapist"%> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3E" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : ""%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Data.VisitDate!=null ? Model.Data.VisitDate.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()) : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.Data.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.Data.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Data.Type == "COTAVisit" ? "COT" : "Occupational Therapist"%> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model.Data); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/PrintRev1.ascx", Model.Data); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model.Data); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/PrintRev1.ascx", Model.Data); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.checkbox("Patient/Family",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Correct Use of Adaptive Equipment",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Safety Technique",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("ADLs",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("HEP",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Correct Use of Assistive Device",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.span("Other",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTeachingOther").Clean() %>",0,1)),
        "Teaching");
    printview.addsection(
        printview.col(6,
            printview.span("Bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBathing").Clean() %>",0,1) +
            printview.span("UE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUEDressing").Clean() %>",0,1) +
            printview.span("LE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLEDressing").Clean() %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGrooming").Clean() %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToileting").Clean() %>",0,1) +
            printview.span("Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLFeeding").Clean() %>",0,1) +
            printview.span("Meal Prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrep").Clean() %>",0,1) +
            printview.span("House Cleaning",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHouseCleaning").Clean() %>",0,1) +
            printview.span("House Safety",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHouseSafety").Clean() %>",0,1)) +
        printview.col(2,
            printview.span("Adapt. Equipment",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLAdaptiveEquipment").Clean() %>",0,1)),
        "ADL Training");
    printview.addsection(
        printview.col(5,
            printview.span("Sitting Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist").Clean() %>% Assist",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist").Clean() %>% Assist",0,1) +
            printview.span("Standing Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesStaticAssist").Clean() %>% Assist",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesDynamicAssist").Clean() %>% Assist",0,1)) +
        printview.checkbox("UE Weight-Bearing Activities",<%= data.AnswerOrEmptyString("GenericUEWeightBearing").Equals("1").ToString().ToLower() %>),
        "Neuromuscular Reeducation");
    printview.addsection(
        printview.col(4,
            printview.span("ROM",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseROM").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseROMSet").Clean() + "set(s)"%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseROMReps").Clean() + "reps"%>",0,1) +
            printview.span("AROM/AAROM",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROM").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMSet").Clean() + "set(s)"%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMReps").Clean() + "reps"%>",0,1) +
            printview.span("Resistive (Type)",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseResistive").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveSet").Clean() + "set(s)"%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveReps").Clean() + "reps"%>",0,1) +
            printview.span("Stretching",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseStretching").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingSet").Clean() + "set(s)"%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingReps").Clean() + "reps"%>",0,1) +
            printview.span("Other",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticExerciseOther").Clean()%>",0,1)),
        "Therapeutic Exercise");
    printview.addsection(
        printview.col(4,
            printview.span("Bed Mobility",true) +
            printview.span("Rolling") +
            printview.span("x<%= data.AnswerOrEmptyString("GenericBedMobilityRollingReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssist").Clean() %>% Assist",0,1) +
            printview.span("") +
            printview.span("Supine to Sit") +
            printview.span("x<%= data.AnswerOrEmptyString("GenericBedMobilitySupineToSitReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilitySupineToSitAssist").Clean() %>% Assist",0,1) +
            printview.span("") +
            printview.span("Dynamic Reaching") +
            printview.span("x<%= data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingAssist").Clean() %>% Assist",0,1) +
            printview.span("") +
            printview.span("Gross/Fine Motor Coord") +
            printview.span("x<%= data.AnswerOrEmptyString("GenericBedMobilityCoordReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityCoordAssist").Clean() %>% Assist",0,1) +
            printview.span("Transfers",true) +
            printview.span("Type: <%= data.AnswerOrEmptyString("GenericTransfersType").Clean() %>",0,1) +
            printview.span("x<%= data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordReps").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordAssist").Clean() %>% Assist",0,1) +
            printview.span("") +
            printview.span("Correct Unfolding") +
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("0").ToString().ToLower() %>) +
            printview.span("") +
            printview.span("Correct Foot Placement") +
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("0").ToString().ToLower() %>) +
            printview.span("") +
            printview.span("Assistive Device") +
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0").ToString().ToLower() %>)),
        "Therapeutic/Dynamic Activities");
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericAssessment").Clean() %>",0,10),"Assessment");
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericNarrative").Clean() %>",0,10),"Narrative");
    printview.addsection(
        printview.span("Propulsion with",true) +
        printview.col(6,
            printview.checkbox("RUE",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("LUe",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("BUE",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("RLE",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("LLE",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("BLE",<%= data.AnswerOrEmptyString("GenericPropulsionWith").Split(',').Contains("6").ToString().ToLower() %>)) +
        printview.col(4,
            printview.span("Distance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDistanceFT").Clean() %>ft x<%= data.AnswerOrEmptyString("GenericDistanceFTReps").Clean() %>",0,1) +
            printview.span("Management",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericManagement").Clean() %><%= data.AnswerOrEmptyString("GenericManagementAssist").Clean() %>% Assist",0,1)),
        "W/C Training");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Continue Prescribed Plan",<%= data.AnswerOrEmptyString("GenericPlan").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.span("Change Prescribed Plan") +
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanChangePrescribed").Clean()%>",0,1) +
            printview.checkbox("Plan Discharge",<%= data.AnswerOrEmptyString("GenericPlan").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("In Progress",<%= data.AnswerOrEmptyString("GenericPlan").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("As of Today",<%= data.AnswerOrEmptyString("GenericPlan").Split(',').Contains("4").ToString().ToLower() %>)) +
        printview.checkbox("Patient/Family Notified 5 Days Prior to Discharge",<%= data.AnswerOrEmptyString("GenericPlan").Split(',').Contains("5").ToString().ToLower() %>) +
        printview.col(2,
            printview.span("Agency Notification 5 Days Prior?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("0").ToString().ToLower() %>))),
        "Plan");
</script>
</body>
</html>
