﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var docName = Model.Data.TypeName; %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%=Model.Data.TypeName %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociate Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3Esurcharge#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.Data.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.Data.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%=Model.Data.TypeName %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3C/span%3E%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model.Data); %>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/PrintRev2.ascx", Model.Data); %>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev2.ascx", Model.Data); %>
<% if (data.AnswerOrEmptyString("IsSupervisoryApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Supervisory Visit");
</script>
<%}else{ %>
<script type="text/javascript">
<%  string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
    printview.addsection(
        printview.col(3,
            printview.checkbox("Supervisory Visit",<%= genericSupervisoryVisit.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("LPTA Present",<%= genericSupervisoryVisit.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Aide Present",<%= genericSupervisoryVisit.Contains("3").ToString().ToLower() %>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericSupervisoryVisitComment").Clean() %>",0,2),
        "Supervisory Visit");
</script>
<%} %>
<% if (data.AnswerOrEmptyString("IsSubjectiveApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A",true),
        "Subjective");
</script>
<%}
   else
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericSubjective").Clean() %>",0,2),
        "Subjective");
</script>
<%} %>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/PrintRev2.ascx", Model.Data); %>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/PrintRev1.ascx", Model.Data); %>
<script type="text/javascript">
<% if(data.AnswerOrEmptyString("IsBedApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Bed Mobility Training");
<%}else{ %>
    printview.addsection(
        printview.col(4,
            printview.span("Rolling",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericRolling").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericRollingReps").Clean() %>reps", 0, 1) +
            printview.span("Sup-Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSupSit").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericSupSitReps").Clean() %>reps", 0, 1) +
            printview.span("Scooting Toward",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericScootingToward").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericScootingTowardReps").Clean() %>reps", 0, 1) +
            printview.span("Sit to Stand",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSitToStand").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericSitToStandReps").Clean() %>reps", 0, 1))+
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityTrainingComment").Clean()%>"),
        "Bed Mobility Training");
<%} %>
</script>
<%  Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TransferTraining/PrintRev1.ascx", Model.Data); %> 
<script type="text/javascript">
<%  string[] genericWalkDirection = data.AnswerArray("GenericWalkDirection"); %>
<%  string[] genericTherapyTraning = data.AnswerArray("GenericTherapyTraning"); %>
<%  string[] genericRails = data.AnswerArray("GenericRails"); %>
<% if(data.AnswerOrEmptyString("IsGaitApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Gait Training");
<%}else{ %>
  printview.addsection(
        printview.col(4,
            printview.span("Ambulation", true) +
            printview.span("Distance <%= data.AnswerOrEmptyString("GenericAmbulationDistance").Clean() %>ft. x <%= data.AnswerOrEmptyString("GenericAmbulationReps").Clean() %>",0,1) +
            printview.span("Assistive Device", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAmbulationAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Assistance", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAmbulationAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("Gait Quality/Deviation", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitQualityComment").Clean() %>",0,1) +
            printview.span("Stairs", true) +
            printview.span("# of Steps <%= data.AnswerOrEmptyString("GenericNumberofSteps").Clean() %>",0,1) +
            printview.checkbox("Rail 1",<%= genericRails.Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Rail 2",<%= genericRails.Contains("2").ToString().ToLower() %>) +
            printview.span("Assistive Device", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericRailAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Assistance", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericStairsAssist").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean() %>",0,1) +
            printview.span("Quality/Deviation", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericStairsQualityComment").Clean() %>",0,1)),
        "Gait Training");
<%} %>
<%  string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
<% if(data.AnswerOrEmptyString("IsTeachingApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Teaching");
<%}else{ %>
    printview.addsection(
        printview.col(5,
            printview.checkbox("Patient",<%= genericTeaching.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= genericTeaching.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("HEP",<%= genericTeaching.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Safe Transfer",<%= genericTeaching.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Safe Gait",<%= genericTeaching.Contains("5").ToString().ToLower() %>)) +
        printview.span("Other: <%= data.AnswerOrEmptyString("GenericTeachingOther").Clean() %>",0,1)+
        printview.span("<%= data.AnswerOrEmptyString("GenericTeachingComment").Clean() %>"),
        "Teaching");
 <%} %>
 <% if(data.AnswerOrEmptyString("IsPainApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Pain");
 <%}else{ %>
    printview.addsection(
        printview.col(2,
            printview.span("%3Cstrong%3EPain level prior to therapy:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPriorIntensityOfPain").Clean() %>",0,1) +
            printview.span("%3Cstrong%3EPain level after therapy:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPostIntensityOfPain").Clean() %>",0,1) +
            printview.span("%3Cstrong%3ELocation:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPainLocation").Clean() %>",0,1) +
            printview.span("%3Cstrong%3ERelieved by:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPainRelievedBy").Clean() %>",0,1))+
        printview.span("Other Comment:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPainProfileComment").Clean() %>",0,1),
        "Pain");
 <%} %>
 <% if(data.AnswerOrEmptyString("IsAssessmentApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Assessment");
 <%}else{ %>
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericAssessment").Clean() %>",0,2),
        "Assessment");
 <%} %>
 <% if(data.AnswerOrEmptyString("IsPlanApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Plan");
 <%}else{ %>
    printview.addsection(
        printview.col(4,
            printview.span("Continue Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericContinuePrescribedPlan").Clean() %>",0,1) +
            printview.span("Change Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericChangePrescribedPlan").Clean() %>",0,1) +
            printview.span("Plan Discharge:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanDischarge").Clean() %>",0,1))+
        printview.span("Comment:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericPlanComment").Clean() %>",0,1),
        "Plan");
 <%} %>
 <% if(data.AnswerOrEmptyString("IsNarrativeApply").Equals("1")){ %>
    printview.addsection(
        printview.checkbox("N/A",true),
        "Narrative");
 <%}else{ %>
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericNarrativeComment").Clean() %>",0,2),
        "Narrative");
 <%} %>    
 <% if(data.AnswerOrEmptyString("IsProgressApply").Equals("1")){ %>   
    printview.addsection(
        printview.checkbox("N/A",true),
        "Progress made towards goals");
 <%}else{ %>
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericProgress").Clean() %>")+
        printview.checkbox("Goals met",<%= data.AnswerArray("GoalsMet").Contains("1").ToString().ToLower() %>),
        "Progress made towards goals");
 <%} %>  
 <% if(data.AnswerOrEmptyString("IsTreatmentApply").Equals("1")){ %>  
    printview.addsection(
        printview.checkbox("N/A",true),
        "Skilled treatment provided this visit");
 <%}else{ %>   
    printview.addsection(
        printview.col(3,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility Training",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfer Training",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Balance Training",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Gait Training",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Functional mobility training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Teach safe stair climbing skills",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Proprioceptive training",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Relaxation technique",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Teach hip precaution",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19Duration").Clean() %>",0,1)+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20Duration").Clean() %>",0,1)+
            printview.checkbox("TENS",<%= genericTreatmentPlan.Contains("21").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan21BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan21Duration").Clean() %>",0,1)+
            printview.checkbox("Prosthetic training",<%= genericTreatmentPlan.Contains("22").ToString().ToLower() %>) +
            printview.checkbox("Pulse oximetry PRN",<%= genericTreatmentPlan.Contains("23").ToString().ToLower() %>) +
            printview.span("&#160;"))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTreatmentPlanOther").Clean() %>",0,1),
            "Skilled treatment provided this visit");
 <%} %>
</script>
</body>
</html>
