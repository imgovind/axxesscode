﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="3">Homebound Reason
            <%= string.Format("<input class='radio' id='{0}IsHomeboundApply' name='{0}_IsHomeboundApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsHomeboundApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Functional Limitations
            <%= string.Format("<input class='radio' id='{0}IsLimitationsApply' name='{0}_IsLimitationsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsLimitationsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><div id="<%= Model.Type %>HomeboundContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></div>
            </td>
            <td colspan="3"><div id="<%=Model.Type %>LimitationsContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/FormRev2.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="6">Vital Signs
            <%= string.Format("<input class='radio' id='{0}IsVitalSignsApply' name='{0}_IsVitalSignsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsVitalSignsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="6"><div id="<%=Model.Type %>VitalSignsContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="3">Supervisory Visit
            <%= string.Format("<input class='radio' id='{0}IsSupervisoryApply' name='{0}_IsSupervisoryApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSupervisoryApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Subjective
            <%= string.Format("<input class='radio' id='{0}IsSubjectiveApply' name='{0}_IsSubjectiveApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSubjectiveApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="3"><div id="<%=Model.Type %>SupervisoryContainer">
                <% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericSupervisoryVisit" value=" " />
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit1' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='1' type='checkbox' {0} />", genericSupervisoryVisit.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit1" class="fixed radio">Supervisory Visit</label>
                </div>
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit2' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='2' type='checkbox' {0} />", genericSupervisoryVisit.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit2" class="fixed radio">LPTA Present</label>
                </div>
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit3' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='3' type='checkbox' {0} />", genericSupervisoryVisit.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit3" class="fixed radio">Aide Present</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericSupervisoryVisitComment" class="strong float-left">Comment:</label>
                <%= Html.ToggleTemplates(Model.Type + "_SupervisoryVisitTemplates", "", "#" + Model.Type + "_GenericSupervisoryVisitComment")%>
                <div><%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment", @class = "fill" })%></div>
            </div>
            </td>
            <td colspan="3"><div id="<%=Model.Type %>SubjectiveContainer">
            <%= Html.ToggleTemplates(Model.Type + "_SubjectiveTemplates", "", "#" + Model.Type + "_GenericSubjective")%>
            <%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"),4,20, new { @id = Model.Type + "_GenericSubjective", @class = "fill" })%>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="6">Functional Mobility Key</th>
        </tr>
        <tr>
            <td colspan="6">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="3">Objective
            <%= string.Format("<input class='radio' id='{0}IsObjectiveApply' name='{0}_IsObjectiveApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsObjectiveApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Bed Mobility Training
            <%= string.Format("<input class='radio' id='{0}IsBedApply' name='{0}_IsBedApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsBedApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><div id="<%=Model.Type %>ObjectiveContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/FormRev2.ascx", Model); %></div></td>
            <td colspan="3">
                
                <div id="<%=Model.Type %>BedContainer">
                    <div>
                        <label for="<%= Model.Type %>_GenericRolling" class="float-left">Rolling</label>
                        <div class="float-right">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericRolling", data.AnswerOrEmptyString("GenericRolling"), new { @id = Model.Type + "_GenericRolling", @class = "" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericRollingReps", data.AnswerOrEmptyString("GenericRollingReps"), new { @id = Model.Type + "_GenericRollingReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSupSit" class="float-left">Sup-Sit</label>
                        <div class="float-right">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericSupSit", data.AnswerOrEmptyString("GenericSupSit"), new { @id = Model.Type + "_GenericSupSit", @class = "" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericSupSitReps", data.AnswerOrEmptyString("GenericSupSitReps"), new { @id = Model.Type + "_GenericSupSitReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericScootingToward" class="float-left">Scooting Toward</label>
                        <div class="float-right">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericScootingToward", data.AnswerOrEmptyString("GenericScootingToward"), new { @id = Model.Type + "_GenericScootingToward", @class = "" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericScootingTowardReps", data.AnswerOrEmptyString("GenericScootingTowardReps"), new { @id = Model.Type + "_GenericScootingTowardReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSitToStand" class="float-left">Sit to Stand</label>
                        <div class="float-right">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericSitToStand", data.AnswerOrEmptyString("GenericSitToStand"), new { @id = Model.Type + "_GenericSitToStand", @class = "" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericSitToStandReps", data.AnswerOrEmptyString("GenericSitToStandReps"), new { @id = Model.Type + "_GenericSitToStandReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericBedMobilityTrainingComment" class="float-left">Comment</label>
                    </div>
                    <div>
                        <%= Html.ToggleTemplates(Model.Type + "_BedMobilityTrainingTemplates", "", "#" + Model.Type + "_GenericBedMobilityTrainingComment")%>
                        <%= Html.TextArea(Model.Type + "_GenericBedMobilityTrainingComment", data.AnswerOrEmptyString("GenericBedMobilityTrainingComment"),4,20, new { @id = Model.Type + "_GenericBedMobilityTrainingComment", @class = "fill" })%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Transfer Training
            <%= string.Format("<input class='radio' id='{0}IsTransferApply' name='{0}_IsTransferApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTransferApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Gait Training
            <%= string.Format("<input class='radio' id='{0}IsGaitApply' name='{0}_IsGaitApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGaitApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><div id="<%=Model.Type %>TransferContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TransferTraining/FormRev1.ascx", Model); %></div>
            </td>
            <td colspan="3">
                <div id="<%=Model.Type %>GaitContainer">
                <div>
                    <label for="<%= Model.Type %>_GenericAmbulationDistance" class="float-left">Ambulation</label>
                    <div class="float-right">
                        <span>Distance</span>
                        <%= Html.TextBox(Model.Type + "_GenericAmbulationDistance", data.AnswerOrEmptyString("GenericAmbulationDistance"), new { @id = Model.Type + "_GenericAmbulationDistance", @class = "sn" })%>
                        ft x
                        <%= Html.TextBox(Model.Type + "_GenericAmbulationReps", data.AnswerOrEmptyString("GenericAmbulationReps"), new { @id = Model.Type + "_GenericAmbulationReps", @class = "sn" })%> reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="float-right"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericAmbulationAssistiveDevice", data.AnswerOrEmptyString("GenericAmbulationAssistiveDevice"), new { @id = Model.Type + "_GenericAmbulationAssistiveDevice", @class = "" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericAmbulationAssist" class="float-left">Assistance</label>
                    <div class="float-right">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericAmbulationAssist", data.AnswerOrEmptyString("GenericAmbulationAssist"), new { @id = Model.Type + "_GenericAmbulationAssist", @class = "" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericGaitQualityComment" class="strong">Gait Quality/Deviation</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericGaitQualityComment", data.AnswerOrEmptyString("GenericGaitQualityComment"), new { @id = Model.Type + "_GenericGaitQualityComment", @class = "fill" })%></div>
                </div>
                <div style="margin-top: 20px;">
                    <label for="<%= Model.Type %>_GenericNumberofSteps" class="float-left">Stairs</label>
                    <div class="float-right">
                        <span># of Steps</span>
                        <%= Html.TextBox(Model.Type + "_GenericNumberofSteps", data.AnswerOrEmptyString("GenericNumberofSteps"), new { @id = Model.Type + "_GenericNumberofSteps", @class = "sn" })%>
                        <% string[] genericRails = data.AnswerArray("GenericRails"); %>
                        <input type="hidden" name="<%= Model.Type %>_GenericRails" value="" />
                        <%= string.Format("<input id='{1}_GenericRails1' class='radio' name='{1}_GenericRails' value='1' type='checkbox' {0} />", genericRails.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericRails1" class="fixed radio">Rail 1</label>
                        <%= string.Format("<input id='{1}_GenericRails2' class='radio' name='{1}_GenericRails' value='2' type='checkbox' {0} />", genericRails.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericRails2" class="fixed radio">Rail 2</label>
                    </div>
                    <div class="clear"></div>
                </div>
                 <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="float-right"><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericRailAssistiveDevice", data.AnswerOrEmptyString("GenericRailAssistiveDevice"), new { @id = Model.Type + "_GenericRailAssistiveDevice", @class = "" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericStairsAssist" class="float-left">Assistance</label>
                    <div class="float-right">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericStairsAssist", data.AnswerOrEmptyString("GenericStairsAssist"), new { @id = Model.Type + "_GenericStairsAssist", @class = "" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericStairsQualityComment" class="strong">Quality/Deviation</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericStairsQualityComment", data.AnswerOrEmptyString("GenericStairsQualityComment"), new { @id = Model.Type + "_GenericStairsQualityComment", @class = "fill" })%></div>
                </div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Teaching
            <%= string.Format("<input class='radio' id='{0}IsTeachingApply' name='{0}_IsTeachingApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTeachingApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Pain
            <%= string.Format("<input class='radio' id='{0}IsPainApply' name='{0}_IsPainApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPainApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><div id="<%=Model.Type %>TeachingContainer">
                <% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching1' class='radio' name='{1}_GenericTeaching' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching1">Patient</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching2' class='radio' name='{1}_GenericTeaching' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching2">Caregiver</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching3' class='radio' name='{1}_GenericTeaching' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching3">HEP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching4' class='radio' name='{1}_GenericTeaching' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching4">Safe Transfer</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching5' class='radio' name='{1}_GenericTeaching' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching5">Safe Gait</label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Other</td>
                            <td colspan="2"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther", @class = "" })%></td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td><%= Html.ToggleTemplates(Model.Type + "_GenericTeachingCommentTemplates", "", "#" + Model.Type + "_GenericTeachingComment")%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <%= Html.TextArea(Model.Type + "_GenericTeachingComment", data.AnswerOrEmptyString("GenericTeachingComment"), 10, 20, new { @id = Model.Type + "_GenericTeachingComment", @class = "fill" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </td>
            <td colspan="3">
            <div id="<%=Model.Type %>PainContainer">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPriorIntensityOfPain" class="float-left">Pain level prior to therapy:</label>
                                <div class="float-right">
                                    <%  var genericPriorIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPriorIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPriorIntensityOfPain", genericPriorIntensityOfPain, new { @id = Model.Type + "_GenericPriorIntensityOfPain", @class = "oe" })%>
                                </div>                                
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPostIntensityOfPain" class="float-left">Pain level after therapy:</label>
                                <div class="float-right">
                                    <%  var genericPostIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPostIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPostIntensityOfPain", genericPostIntensityOfPain, new { @id = Model.Type + "_GenericPostIntensityOfPain", @class = "oe" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="row">
                                    <img src="/Images/painscale.png" alt="Pain Scale Image" width="90%" /><br />
                                    <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainLocation" class="float-left">Location:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPainLocation", data.AnswerOrEmptyString("GenericPainLocation"), new { @id = Model.Type + "_GenericPainLocation" })%></div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainRelievedBy" class="float-left">Relieved by:</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPainRelievedBy", data.AnswerOrEmptyString("GenericPainRelievedBy"), new { @id = Model.Type + "_GenericPainRelievedBy" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Other Comment:</label>
                <%= Html.ToggleTemplates(Model.Type + "_PainProfileTemplates", "", "#" + Model.Type + "_GenericPainProfileComment")%>
                <div><%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment", @class = "fill" })%></div>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Plan
            <%= string.Format("<input class='radio' id='{0}IsPlanApply' name='{0}_IsPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPlanApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Assessment
            <%= string.Format("<input class='radio' id='{0}IsAssessmentApply' name='{0}_IsAssessmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsAssessmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
             <td colspan="3">
             <div id="<%=Model.Type %>PlanContainer">
                <div>
                    <label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="float-left">Continue Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @id = Model.Type + "_GenericContinuePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="float-left">Change Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @id = Model.Type + "_GenericChangePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericPlanDischarge" class="float-left">Plan Discharge:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @id = Model.Type + "_GenericPlanDischarge", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Comment:</label>
                <%= Html.ToggleTemplates(Model.Type + "_PlanTemplates", "", "#" + Model.Type + "_GenericPlanComment")%>
                <div><%= Html.TextArea(Model.Type + "_GenericPlanComment", data.AnswerOrEmptyString("GenericPlanComment"), new { @id = Model.Type + "_GenericPlanComment", @class = "fill" })%></div>
            </div>
            </td>
            <td colspan="3"><div id="<%=Model.Type %>AssessmentContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates", "", "#" + Model.Type + "_GenericAssessment")%>
            <%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"),4,20, new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Narrative
            <%= string.Format("<input class='radio' id='{0}IsNarrativeApply' name='{0}_IsNarrativeApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsNarrativeApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="3">Progress made towards goals
            <%= string.Format("<input class='radio' id='{0}IsProgressApply' name='{0}_IsProgressApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsProgressApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="3">
                <div id="<%=Model.Type %>NarrativeContainer">
                    <%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates", "", "#" + Model.Type + "_GenericNarrativeComment")%>
                    <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 8, 20, new { @id = Model.Type + "_GenericNarrativeComment", @class = "fill" })%>
                </div>
            </td>
            <td colspan="3"><div id="<%=Model.Type %>ProgressContainer">
            <%= Html.ToggleTemplates(Model.Type + "_GenericProgressTemplates", "", "#" + Model.Type + "_GenericProgress")%>
            <%= Html.TextArea(Model.Type + "_GenericProgress", data.AnswerOrEmptyString("GenericProgress"),5,20, new { @id = Model.Type + "_GenericProgress", @class = "fill" })%>
            <div class="align-left">
                <%= string.Format("<input id='{1}_GoalsMet' class='radio float-left' name='{1}_GoalsMet' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("GoalsMet").Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GoalsMet">Goals Met</label>
            </div>
            </div>
            </td>
         </tr>
        <tr>
            <th colspan="6">Skilled treatment provided this visit
            <%= string.Format("<input class='radio' id='{0}IsTreatmentApply' name='{0}_IsTreatmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTreatmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="6"><div id="<%=Model.Type %>TreatmentContainer">
            <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev2.ascx", Model); %></div></td>
        </tr>
    </tbody>
</table>


<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>IsHomeboundApply"), $("#<%= Model.Type %>HomeboundContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLimitationsApply"), $("#<%=Model.Type %>LimitationsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsVitalSignsApply"), $("#<%=Model.Type %>VitalSignsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSupervisoryApply"), $("#<%=Model.Type %>SupervisoryContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSubjectiveApply"), $("#<%=Model.Type %>SubjectiveContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsObjectiveApply"), $("#<%=Model.Type %>ObjectiveContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsBedApply"), $("#<%= Model.Type %>BedContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTransferApply"), $("#<%=Model.Type %>TransferContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGaitApply"), $("#<%=Model.Type %>GaitContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTeachingApply"), $("#<%=Model.Type %>TeachingContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPainApply"), $("#<%=Model.Type %>PainContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPlanApply"), $("#<%=Model.Type %>PlanContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsAssessmentApply"), $("#<%=Model.Type %>AssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsNarrativeApply"), $("#<%=Model.Type %>NarrativeContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsProgressApply"), $("#<%=Model.Type %>ProgressContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTreatmentApply"), $("#<%=Model.Type %>TreatmentContainer"));
</script>