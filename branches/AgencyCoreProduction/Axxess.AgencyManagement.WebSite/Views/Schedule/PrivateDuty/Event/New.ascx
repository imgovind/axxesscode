﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PrivateDuty/Event/Create", "Schedule", FormMethod.Post, new { @id = "NewPrivateDutyEvent_Form" })) { %>
<%= Html.Hidden("PatientId", Model.Id, new { @id = "NewPrivateDutyEvent_PatientId" })%>
    <fieldset>
        <legend>Employee Scheduler</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left strong">Patient</label>
                <div class="float-right"><%= Model.DisplayNameWithMi %></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyEvent_DisciplineTaskName">Task</label>
                <div class="float-right"><%= Html.MultipleDisciplineTasks("DisciplineTaskName", "", new Dictionary<string, string>())%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyEvent_UserId">User/Employee</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", "", new { @id = "NewPrivateDutyEvent_UserId" })%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyEvent_StartDate">Start Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="StartDate" id="NewPrivateDutyEvent_StartDate" />
                    <input type="text" class="time-picker required" name="StartTime" id="NewPrivateDutyEvent_StartTime" />
                    <br />
                    <input type="checkbox" class="radio" name="AllDay" id="NewPrivateDutyEvent_AllDay" />
                    <label for="NewPrivateDutyEvent_AllDay">All Day Event</label>
                </div>
            </div>
            <div id="NewPrivateDutyEvent_End" class="row">
                <label class="float-left strong" for="NewPrivateDutyEvent_EndDate">End Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="EndDate" id="NewPrivateDutyEvent_EndDate" />
                    <input type="text" class="time-picker required" name="EndTime" id="NewPrivateDutyEvent_EndTime" />
                </div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewPrivateDutyEvent_Comments">Comments</label>
                <div class="fr"><%= Html.TextArea("Comments", new { @id = "NewPrivateDutyEvent_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("AddAnother", "0", new { @id = "NewPrivateDutyEvent_AddAnother" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save and Close</a></li>
            <li><a class="save clear">Save and Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>