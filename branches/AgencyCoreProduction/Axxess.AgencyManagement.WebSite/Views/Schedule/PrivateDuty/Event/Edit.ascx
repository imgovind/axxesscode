﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PrivateDutyScheduleEvent>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PrivateDuty/Event/Update", "Schedule", FormMethod.Post, new { @id = "EditPrivateDutyEvent_Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditPrivateDutyEvent_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditPrivateDutyEvent_PatientId" })%>
    <fieldset>
        <legend>Edit Task</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left strong">Patient</label>
                <div class="float-right"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="float-left strong">Task</label>
                <div class="float-right"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyEvent_UserId">User/Employee</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserName, new { @id = "EditPrivateDutyEvent_UserId" })%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyEvent_StartDate">Start Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="StartDate" id="EditPrivateDutyEvent_StartDate" value="<%= Model.StartDate %>" />
                    <input type="text" class="time-picker required" name="StartTime" id="EditPrivateDutyEvent_StartTime" value="<%= Model.StartTime %>" />
                </div>
                <div class="clr"></div>
                <div class="float-right">
                    <%= string.Format("<input type=\"checkbox\" class=\"radio\" name=\"AllDay\" id=\"EditPrivateDutyEvent_AllDay\" {0} />", Model.AllDay.ToChecked()) %>
                    <label for="EditPrivateDutyEvent_AllDay">All Day Event</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyEvent_EndDate">End Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="EndDate" id="EditPrivateDutyEvent_EndDate" value="<%= Model.EndDate %>" />
                    <input type="text" class="time-picker required" name="EndTime" id="EditPrivateDutyEvent_EndTime" value="<%= Model.EndTime %>" />
                </div>
            </div>
            <div class="row">
                <label class="fl strong" for="EditPrivateDutyEvent_Comments">Comments</label>
                <div class="fr"><%= Html.TextArea("Comments", Model.Comments, new { @id = "EditPrivateDutyEvent_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>