﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>

<table class="fixed nursing">
    <thead>
        <tr>
            <th colspan="2">Tasks</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <table class="fixed">
                    <tr>
                        <th colspan="3" class="align-left">Assignment</th>
                        <th colspan="3"></th>
                    </tr>
                    <tr>
                        <th colspan="3" class="align-left">Light Housekeeping</th>
                        <th colspan="3"></th>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Vacuuming</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingVacuuming", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Dust/Damp mop</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingDustMop","HomeMakerNote", data) %></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Kitchen</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingKitchen", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Dishes</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingDishes", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Bedroom</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingBedroom", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Bathroom</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingBathroom", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Make/Change Bed</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingMakeChangeBed", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Empty Commode</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingEmptyCommode", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Empty Trash</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingEmptyTrash", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Wash Clothes</td>
                        <td><%= Html.ProperCheckBox("LightHousekeepingWashClothes", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                         <th colspan="3" class="align-left">Nutrition</th>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Meal Preparation</td>
                        <td><%= Html.ProperCheckBox("NutritionMealPreparation", "HomeMakerNote", data)%></td>
                    </tr>
                     <tr>
                        <td colspan="3" class="align-left">Assist with Feeding</td>
                        <td><%= Html.ProperCheckBox("NutritionAssistWithFeeding", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Limit/Encourage Fluids</td>
                        <td><%= Html.ProperCheckBox("NutritionLimitEncourageFluids", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                         <th colspan="3" class="align-left">Errands</th>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Shopping</td>
                        <td><%= Html.ProperCheckBox("ErrandsShopping", "HomeMakerNote", data)%></td>
                    </tr>
                     <tr>
                        <td colspan="3" class="align-left">Prescription pickup</td>
                        <td><%= Html.ProperCheckBox("ErrandsPrescriptionPickup", "HomeMakerNote", data)%></td>
                    </tr>
                     <tr>
                        <td colspan="3" class="align-left">Appointment Accompaniment</td>
                        <td><%= Html.ProperCheckBox("ErrandsAppointmentAccompaniment", "HomeMakerNote", data)%></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="fixed">
                    <tr>
                        <th colspan="3" class="align-left">Assignment</th>
                        <th colspan="3"></th>
                    </tr>
                    <tr>
                        <th colspan="3" class="align-left">Personal Care</th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Tub Bathe</td>
                        <td><%= Html.ProperCheckBox("PersonalCareTubBathe", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Sponge Bathe</td>
                        <td><%= Html.ProperCheckBox("PersonalCareSpongeBathe", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Bed Bathe</td>
                        <td><%= Html.ProperCheckBox("PersonalCareBedBathe", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Shower</td>
                         <td><%= Html.ProperCheckBox("PersonalCareShower", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Hair Care</td>
                        <td><%= Html.ProperCheckBox("PersonalCareHairCare", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Nail Care</td>
                        <td><%= Html.ProperCheckBox("PersonalCareNailCare", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Shampoo</td>
                        <td><%= Html.ProperCheckBox("PersonalCareShampoo", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Shave</td>
                        <td><%= Html.ProperCheckBox("PersonalCareShave", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Mouth Care</td>
                        <td><%= Html.ProperCheckBox("PersonalCareMouthCare", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Dressing</td>
                        <td><%= Html.ProperCheckBox("PersonalCareDressing", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Oral Care - Brush/Dentures</td>
                        <td><%= Html.ProperCheckBox("PersonalCareOralCare", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Elimination Assist</td>
                        <td><%= Html.ProperCheckBox("PersonalCareEliminationAssist", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                         <th colspan="3" class="align-left">Activity</th>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Ambulation Assist/WC/Walker/Cane</td>
                        <td><%= Html.ProperCheckBox("ActivityAmbulationAssistWCWalkerCane", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Mobility Assist</td>
                        <td><%= Html.ProperCheckBox("ActivityMobilityAssist", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Exercise</td>
                        <td><%= Html.ProperCheckBox("ActivityExercise", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">Assist with Medication</td>
                        <td><%= Html.ProperCheckBox("ActivityAssistWithMedication", "HomeMakerNote", data)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-left">ROM</td>
                        <td><%= Html.ProperCheckBox("ActivityROM", "HomeMakerNote", data)%></td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Changes in</th>
            <th>Universal Precautions</th>
        </tr>
        <tr>
            <td>
                <h6 class="align-left bold">Skin Care</h6>
                <ul class="checkgroup inline">
                    <li>
                       <%= Html.RadioButton("HomeMakerNote_ChangesInSkinCare", "2", data.ContainsKey("ChangesInSkinCare") && data["ChangesInSkinCare"].Answer == "2", new { @id = "", @class = "radio" })%> 
                       <label>Broken</label>
                    </li>
                    <li>
                       <%= Html.RadioButton("HomeMakerNote_ChangesInSkinCare", "1", data.ContainsKey("ChangesInSkinCare") && data["ChangesInSkinCare"].Answer == "1", new { @id = "", @class = "radio" })%> 
                       <label>Itchy</label>
                    </li>
                    <li>
                       <%= Html.RadioButton("HomeMakerNote_ChangesInSkinCare", "0", data.ContainsKey("ChangesInSkinCare") && data["ChangesInSkinCare"].Answer == "0", new { @id = "", @class = "radio" })%> 
                       <label>N/A</label>
                    </li>
                </ul>
                <ul class="checkgroup">
                    <li>
                        <label class="float-left bold">Swelling in</label>
                    </li>
                    <li>
                        <div class="option">
                            <%= Html.ProperCheckBox("SwellingInHandsFeet", "HomeMakerNote", data)%>
                            <label>Hands/Feet</label>
                        </div>
                    <li>
                    <li>
                        <div class="option">
                            <%= Html.ProperCheckBox("SwellingInAbdomen", "HomeMakerNote", data)%>
                            <label>Abdomen</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= Html.ProperCheckBox("SwellingInRightLeg", "HomeMakerNote", data)%>
                            <label>Right Leg</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= Html.ProperCheckBox("SwellingInLeftLeg", "HomeMakerNote", data)%>
                            <label>Left Leg</label>
                        </div>
                    </li>
                </ul>
            </td>
            <td>
                <%= Html.TextArea("HomeMakerNote_UniversalPrecautions", data.ContainsKey("UniversalPrecautions") ? data["UniversalPrecautions"].Answer : string.Empty, new { @id = "HomeMakerNoteUniversalPrecautions", @class = "fill", @rows = "12" })%>
            </td>
        </tr>
    </tbody>
</table>

<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Comments</th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.ToggleTemplates("HomeMakerNote_CommentTemplates", "", "#HomeMakerNote_Comment")%>
                <%= Html.TextArea("HomeMakerNote_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "HomeMakerNote_Comment", @class = "fill" })%>
            </td>
        </tr>
    </tbody>
</table>

