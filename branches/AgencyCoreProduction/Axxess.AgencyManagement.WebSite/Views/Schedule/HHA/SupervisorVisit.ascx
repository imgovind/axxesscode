﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<span class="wintitle">HHA Supervisory Visit | <%= Model.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHASVisitForm" })) { %>
    <%= Html.Hidden("HHASVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("HHASVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("HHASVisit_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "55")%>
    <%= Html.Hidden("Type", "HHASVisit")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                    HHA Supervisory Visit
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.DisplayName %> (<%= Model.MRN %>)</td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="HHASVisit_HealthAide" class="float-left">Health Aide:</label>
                        <div class="float-right"><%= Html.HHAides("HealthAide", data.ContainsKey("HealthAide") ? data["HealthAide"].Answer : "", new { @id = "HHASVisit_HealthAide" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left">Aide Present:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("HHASVisit_AidePresent", "1", data.ContainsKey("AidePresent") && data["AidePresent"].Answer == "1" ? true : false, new { @id="HHASVisit_AidePresentY", @class="radio" })%>
                            <label class="inline-radio" for="HHASVisit_AidePresentY">Yes</label>
                            <%= Html.RadioButton("HHASVisit_AidePresent", "0", data.ContainsKey("AidePresent") && data["AidePresent"].Answer == "0" ? true : false, new { @id="HHASVisit_AidePresentN", @class="radio" })%>
                            <label class="inline-radio" for="HHASVisit_AidePresentN">No</label>
                        </div>
                    </div>
                </td>
                <td colspan="2">
                    <div>
                        <label for="HHASVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="HHASVisit_VisitDate" value="<%= Model.VisitDate.ToZeroFilled() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="HHASVisit_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHASVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("HHASVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digitd", @maxlength=6, @id = "HHASVisit_AssociatedMileage" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Evaluation</th>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">1.</span>
                        Arrives for assigned visits as scheduled:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "HHASVisit_ArriveOnTimeY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_ArriveOnTimeY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "HHASVisit_ArriveOnTimeN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_ArriveOnTimeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">2.</span>
                        Follows client&#8217;s plan of care:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "HHASVisit_FollowPOCY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_FollowPOCY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "HHASVisit_FollowPOCN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_FollowPOCN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">3.</span>
                        Demonstrates positive and helpful attitude towards the client and others:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "HHASVisit_HasPositiveAttitudeY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_HasPositiveAttitudeY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "HHASVisit_HasPositiveAttitudeN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_HasPositiveAttitudeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">4.</span>
                        Informs Nurse Supervisor of client needs and changes in condition as appropriate:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "HHASVisit_InformChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_InformChangesY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "HHASVisit_InformChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_InformChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">5.</span>
                        Aide Implements Universal Precautions per agency policy:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "HHASVisit_IsUniversalPrecautionsY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_IsUniversalPrecautionsY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "HHASVisit_IsUniversalPrecautionsN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_IsUniversalPrecautionsN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">6.</span>
                        Any changes made to client plan of care at this time:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "HHASVisit_POCChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_POCChangesY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "HHASVisit_POCChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_POCChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">7.</span>
                        Patient/CG satisfied with care and services provided by aide:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("HHASVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "HHASVisit_IsServicesSatisfactoryY", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_IsServicesSatisfactoryY">Yes</label>
                        <%= Html.RadioButton("HHASVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "HHASVisit_IsServicesSatisfactoryN", @class = "radio" })%>
                        <label class="inline-radio" for="HHASVisit_IsServicesSatisfactoryN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="float-left">
                        <span class="alphali">8.</span>
                        Additional Comments/Findings:
                    </div>
                    <div class="clear"></div>
                    <div class="float-left" style="width:99%"><%= Html.TextArea("HHASVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @style="height:80px;width:100%" })%></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="HHASVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password("HHASVisit_Clinician", "", new { @class = "complete-required", @id = "HHASVisit_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="HHASVisit_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="HHASVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="HHASVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HHASVisit_Button" />
	<% Html.RenderPartial("NoteBottomButtons", Model); %>
<%  } %>
</div>
<script type="text/javascript">
    $("#window_hhasVisit form label.error").css({ 'position': 'relative', 'float': 'left' });
</script>