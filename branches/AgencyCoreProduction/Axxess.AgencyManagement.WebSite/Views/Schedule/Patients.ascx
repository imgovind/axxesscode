﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>
<%  Html.Telerik().Grid<PatientSelection>().Name("ScheduleCenter_PatientSelectionGrid").Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Schedule", new { branchId = Guid.Empty, statusId = Model, paymentSourceId = 0 })).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events =>
        events.OnDataBound("Schedule.Center.PatientSelector.OnDataBound").OnRowSelect("Schedule.Center.PatientSelector.OnRowSelect")
    ).Render(); %>