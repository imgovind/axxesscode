﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<style type="text/css">
    .missedvisitcontainer {
        background: none;
        border: none;
        box-shadow: none;
        max-width: 600px;
        margin: 0 auto;
        width: auto;
    }
    .missedvisitcontainer legend {
        font-size: 2em;
        font-weight: bold;
    }
    .missedvisitcontainer .topcontent { margin-bottom: 20px; }
    .missedvisitcontainer .row { max-width: none; }
    .missedvisitcontainer .signature img {
         max-width: 100%;
         max-height: 180px;
         border: 1px solid #eee;
         display: block; 
         width: 100%;
    }
</style>

<div class="wrapper main">
    <fieldset class="missedvisitcontainer">
        <legend>Missed Visit Details</legend>
        <div class="topcontent">
            <div class="row"><label for="Missed_Visit_Info_Name" class="float-left">Patient:</label><div class="float-right"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div></div>
            <div class="row"><label for="Missed_Visit_Info_TypeofVisit" class="float-left">Type of Visit:</label><div class="float-right"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div></div>
            <div class="row"><label for="Missed_Visit_Info_UserName" class="float-left">Assigned To:</label><div class="float-right"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div></div>
            <div class="row"><label for="Missed_Visit_Info_DateofVisit" class="float-left">Signature Date:</label><div class="float-right"><%= Model != null ? Model.SignatureDate.ToShortDateString() : string.Empty%></div></div>
            <div class="row"><label for="Missed_Visit_Info_OrderGenerated" class="float-left">Order Generated:</label><div class="float-right"><%= (Model != null && Model.IsOrderGenerated) ? "Yes" : "No"%></div></div>
            <div class="row"><label for="Missed_Visit_Info_PhysicianOfficeNotified" class="float-left">Physician Office Notified:</label><div class="float-right"><%= (Model != null && Model.IsPhysicianOfficeNotified) ? "Yes" : "No"%></div></div>
            <div class="row"><label for="Missed_Visit_Info_Reason" class="float-left">Reason:</label><div class="float-right"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div></div>
            <div class="row"><label for="Missed_Visit_Info_Comments" class="float-left">Comments:</label><div class="float-right"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div></div>
        </div>
        <% if (Model.UserSignatureAssetId.IsNotEmpty()) { %>
        <div class="clear"></div>
        <div class="signature">
            <b>User Signature:</b>
            <img alt="Staff Signature" src="/Asset/<%= Model.UserSignatureAssetId %>" />
        </div>
        <% } %>
    </fieldset>
</div>
