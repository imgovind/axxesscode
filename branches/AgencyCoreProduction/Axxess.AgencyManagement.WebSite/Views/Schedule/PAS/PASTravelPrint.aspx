﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<%var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>PAS Travel Note<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body></body><%
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => {  %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPAS Travel Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E" +
        "%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()) : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber.Clean() : string.Empty %>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETravel Start Time:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TravelTimeIn") ? data["TravelTimeIn"].Answer.Clean():( data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty )%>" +

        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +

        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETravel End Time:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TravelTimeOut") ? data["TravelTimeOut"].Answer.Clean() :(data.ContainsKey("TimeOut")?data["TimeOut"].Answer.Clean(): string.Empty) %>" +
        
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.LocationProfile.Name.Clean().IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPersonal Assistance Services%3Cbr /%3ETravel Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("Comment") && data["Comment"].Answer.IsNotNullOrEmpty() ? data["Comment"].Answer.Clean() : "" %>",0,10),
        "Comments");
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>