﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<%  var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Medical Social Worker <%= Model != null && Model.Data.Type.IsNotNullOrEmpty() && Model.Data.Type != "MSWEvaluationAssessment" ? Model.Data.Type.Substring(3) : "Evaluation"%><%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
</head>
<body></body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker <%= Model != null && Model.Data.Type.IsNotNullOrEmpty() && Model.Data.Type != "MSWEvaluationAssessment" ? Model.Data.Type.Substring(3) : "Evaluation"%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22octocol%22%3E%3Cspan%3E%3Cstrong%3EMR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan style=%27width:20%%27%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("Surcharge") ? data["Surcharge"].Answer.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Data.PhysicianDisplayName.IsNotNullOrEmpty()?Model.Data.PhysicianDisplayName: string.Empty%>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.LocationProfile.Name.Clean().IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker <%= Model != null && Model.Data.Type.IsNotNullOrEmpty() && Model.Data.Type != "MSWEvaluationAssessment" ? Model.Data.Type.Substring(3) : "Evaluation"%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E"+
            <%--"<%=Model!=null && Model.Data.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.Data.PhysicianSignatureText.Clean():"" %>"+
        "%3C/span%3E%3Cspan%3E"+
            "<%=Model!=null && Model.Data.PhysicianSignatureDate.IsValid()? Model.Data.PhysicianSignatureDate.ToShortDateString():"" %>"+
        "%3C/span%3E%3C/span%3E"+--%>
        <%--"%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +--%>
            "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        printview.addsection(
            printview.span("Patient Lives",true) +
            printview.col(4,
                printview.checkbox("Alone",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("With Friend/Family",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("With Dependent",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("With Spouse/Partner",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("With Religious Community",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Assisted Living",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("With Partner",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Has Paid Caregiver",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Has Live-In, Paid Caregiver",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericLivingSituation") && data["GenericLivingSituation"].Answer.Split(',').Contains("10") && data.ContainsKey("GenericLivingSituationOther") && data["GenericLivingSituationOther"].Answer.IsNotNullOrEmpty() ? data["GenericLivingSituationOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.col(2,
                printview.span("Primary Caregiver",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPrimaryCaregiver") && data["GenericPrimaryCaregiver"].Answer.IsNotNullOrEmpty() ? data["GenericPrimaryCaregiver"].Answer.Clean() : string.Empty %>",0,1) +
                printview.span("The quality of care that patient receives at home",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericQualityOfCare") && data["GenericQualityOfCare"].Answer.IsNotNullOrEmpty() ? data["GenericQualityOfCare"].Answer.Clean() : string.Empty %>",0,1) +
                printview.span("Environmental Conditions",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericEnvironmentalConditions") && data["GenericEnvironmentalConditions"].Answer.IsNotNullOrEmpty() ? data["GenericEnvironmentalConditions"].Answer.Clean() : string.Empty %>",0,1)+
                printview.span("Identification of the responsible party",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericIdentificationOfResponsibleParty") && data["GenericIdentificationOfResponsibleParty"].Answer.IsNotNullOrEmpty() ? data["GenericIdentificationOfResponsibleParty"].Answer.Clean() : string.Empty%>",0,1)+
                printview.span("Emergency Contact",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericEmergencyContact") && data["GenericEmergencyContact"].Answer.IsNotNullOrEmpty() ? data["GenericEmergencyContact"].Answer.Clean() : string.Empty%>",0,1)),
            "Living Situation");
        printview.addsection(
            printview.col(2,
                printview.checkbox("Assessment for Psychosocial Coping",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Lives Alone, No Identified Caregiver",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Counseling re Disease Process or Management",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Solo Caregiver for Minor Children and/or Other Dependents",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Family/Caregiver Coping Support",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Reported Noncompliance to Medical Plan of Care",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Hospice Eligibility",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Suspected Negligence or Abuse",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Financial/Practical Resources",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Assistance with Advanced Directive / DPOA/DNR",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericReasonForReferral") && data["GenericReasonForReferral"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericReasonForReferralOther") && data["GenericReasonForReferralOther"].Answer.IsNotNullOrEmpty() ? data["GenericReasonForReferralOther"].Answer.Clean() : string.Empty %>",0,1)),
            "Reason(s) for Referral");
        printview.addsection(
            printview.span("Mental Status",true) +
            printview.col(4,
                printview.checkbox("Alert",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Forgetful",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Confused",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Disoriented",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Oriented",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Lethargic",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Poor Short-Term Memory",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Unconscious",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Cannot Determine",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Reasoning",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Judgement",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("11") ? "true" : "false"%>)) +
                
            printview.span("Emotional Status",true) +
            printview.col(4,
                printview.checkbox("Stable",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Tearful",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Stressed",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Angry",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Sad",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Withdrawn",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Fearful",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Anxious",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Flat Affect",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("10") && data.ContainsKey("GenericEmotionalStatusOther") && data["GenericEmotionalStatusOther"].Answer.IsNotNullOrEmpty() ? data["GenericEmotionalStatusOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.span("<%= data != null && data.ContainsKey("GenericPsychosocialAssessment") && data["GenericPsychosocialAssessment"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialAssessment"].Answer.Clean() : string.Empty %>",0,10)+
            printview.span("The patient’s involvement with social and community activities",true)+
            printview.span("<%= data != null && data.ContainsKey("PatientInvolvement") && data["PatientInvolvement"].Answer.IsNotNullOrEmpty() ? data["PatientInvolvement"].Answer.Clean() : string.Empty%>",0,10),
            "Psychosocial Assessment");
        
        printview.addsection(
            printview.col(2,
                printview.span("Income Sources",true) +
                printview.col(4,
                    printview.span("N/A",true) +
                    printview.span("No",true) +
                    printview.span("Yes",true) +
                    printview.span("Amount",true)) +
                printview.span("Employment",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericEmployment") && data["GenericEmployment"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericEmployment") && data["GenericEmployment"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericEmployment") && data["GenericEmployment"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericEmploymentAmount") && data["GenericEmploymentAmount"].Answer.IsNotNullOrEmpty() ? data["GenericEmploymentAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Pt Social Security",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSocialSecurity") && data["GenericPtSocialSecurity"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSocialSecurity") && data["GenericPtSocialSecurity"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSocialSecurity") && data["GenericPtSocialSecurity"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericPtSocialSecurityAmount") && data["GenericPtSocialSecurityAmount"].Answer.IsNotNullOrEmpty() ? data["GenericPtSocialSecurityAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Spouse Social Security",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSocialSecurity") && data["GenericSpouseSocialSecurity"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSocialSecurity") && data["GenericSpouseSocialSecurity"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSocialSecurity") && data["GenericSpouseSocialSecurity"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericSpouseSocialSecurityAmount") && data["GenericSpouseSocialSecurityAmount"].Answer.IsNotNullOrEmpty() ? data["GenericSpouseSocialSecurityAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Pt SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSSI") && data["GenericPtSSI"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSSI") && data["GenericPtSSI"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPtSSI") && data["GenericPtSSI"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericPtSSIAmount") && data["GenericPtSSIAmount"].Answer.IsNotNullOrEmpty() ? data["GenericPtSSIAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Spouse SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSSI") && data["GenericSpouseSSI"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSSI") && data["GenericSpouseSSI"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseSSI") && data["GenericSpouseSSI"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericSpouseSSIAmount") && data["GenericSpouseSSIAmount"].Answer.IsNotNullOrEmpty() ? data["GenericSpouseSSIAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Pensions",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPensions") && data["GenericPensions"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPensions") && data["GenericPensions"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericPensions") && data["GenericPensions"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericPensionsAmount") && data["GenericPensionsAmount"].Answer.IsNotNullOrEmpty() ? data["GenericPensionsAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Other Income",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherIncome") && data["GenericOtherIncome"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherIncome") && data["GenericOtherIncome"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherIncome") && data["GenericOtherIncome"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericOtherIncomeAmount") && data["GenericOtherIncomeAmount"].Answer.IsNotNullOrEmpty() ? data["GenericOtherIncomeAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Food Stamps",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericFoodStamps") && data["GenericFoodStamps"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericFoodStamps") && data["GenericFoodStamps"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericFoodStamps") && data["GenericFoodStamps"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericFoodStampsAmount") && data["GenericFoodStampsAmount"].Answer.IsNotNullOrEmpty() ? data["GenericFoodStampsAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Total Income",true) +
                printview.col(4,
                    printview.span("") +
                    printview.span("") +
                    printview.span("") +
                    printview.span("<%= data != null && data.ContainsKey("GenericTotalIncomeAmount") && data["GenericTotalIncomeAmount"].Answer.IsNotNullOrEmpty() ? data["GenericTotalIncomeAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Assets",true) +
                printview.col(4,
                    printview.span("N/A",true) +
                    printview.span("No",true) +
                    printview.span("Yes",true) +
                    printview.span("Amount",true)) +
                printview.span("Savings Account",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSavingsAccount") && data["GenericSavingsAccount"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSavingsAccount") && data["GenericSavingsAccount"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSavingsAccount") && data["GenericSavingsAccount"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericSavingsAccountAmount") && data["GenericSavingsAccountAmount"].Answer.IsNotNullOrEmpty() ? data["GenericSavingsAccountAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Owns Home (value)",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsHome") && data["GenericOwnsHome"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsHome") && data["GenericOwnsHome"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsHome") && data["GenericOwnsHome"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericOwnsHomeAmount") && data["GenericOwnsHomeAmount"].Answer.IsNotNullOrEmpty() ? data["GenericOwnsHomeAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Owns Other Property (value)",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsOtherProperty") && data["GenericOwnsOtherProperty"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsOtherProperty") && data["GenericOwnsOtherProperty"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOwnsOtherProperty") && data["GenericOwnsOtherProperty"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericOwnsOtherPropertyAmount") && data["GenericOwnsOtherPropertyAmount"].Answer.IsNotNullOrEmpty() ? data["GenericOwnsOtherPropertyAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("VA Aid &#38; Assistance",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericVAAid") && data["GenericVAAid"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericVAAid") && data["GenericVAAid"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericVAAid") && data["GenericVAAid"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericVAAidAmount") && data["GenericVAAidAmount"].Answer.IsNotNullOrEmpty() ? data["GenericVAAidAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Spouse SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseAssetSSI") && data["GenericSpouseAssetSSI"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseAssetSSI") && data["GenericSpouseAssetSSI"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericSpouseAssetSSI") && data["GenericSpouseAssetSSI"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericSpouseAssetSSIAmount") && data["GenericSpouseAssetSSIAmount"].Answer.IsNotNullOrEmpty() ? data["GenericSpouseSSIAmount"].Answer.Clean() : string.Empty%>",0,1)) +
                printview.span("Other Assets",true) +
                printview.col(4,
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherAssets") && data["GenericOtherAssets"].Answer == "2" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherAssets") && data["GenericOtherAssets"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("",<%= data != null && data.ContainsKey("GenericOtherAssets") && data["GenericOtherAssets"].Answer == "1" ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericOtherAssetsAmount") && data["GenericOtherAssetsAmount"].Answer.IsNotNullOrEmpty() ? data["GenericOtherAssetsAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Total Assets",true) +
                printview.col(4,
                    printview.span("") +
                    printview.span("") +
                    printview.span("") +
                    printview.span("<%= data != null && data.ContainsKey("GenericTotalAssetsAmount") && data["GenericTotalAssetsAmount"].Answer.IsNotNullOrEmpty() ? data["GenericTotalAssetsAmount"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Transportation for medical care provided by",true) +
                
               printview.span("<%= data != null && data.ContainsKey("GenericTransportationProvidedBy") && data["GenericTransportationProvidedBy"].Answer.IsNotNullOrEmpty() ? data["GenericTransportationProvidedBy"].Answer.Clean() : string.Empty %>",0,1)),
            "Financial Assessment");
            
            printview.addsection(
             
                printview.span("Identified Problems",true) +
                printview.col(2,
                    printview.checkbox("Patient needs a meal prepared or delivered daily",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Patient/family reported noncompliant to plan of care",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs assistance with housekeeping/shopping",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs assist. with advanced directive/DPOA/DNR",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs daily contact to check on him/her",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs assistance with medical/insurance forms",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs assistance with alert device (ERS, PRS)",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs assistance with entitlement forms",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs transportation assistance to medical care",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs alternative living arrangements",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                    printview.checkbox("Patient needs alternative living arrangements",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                    printview.checkbox("Psychosocial counseling indicated",<%= data != null && data.ContainsKey("GenericIdentifiedProblems") && data["GenericIdentifiedProblems"].Answer.Split(',').Contains("12") ? "true" : "false"%>)) +
                printview.span("Provide further information",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericFurtherInformation") && data["GenericFurtherInformation"].Answer.IsNotNullOrEmpty() ? data["GenericFurtherInformation"].Answer.Clean() : string.Empty %>",0,4) +
                printview.span("Identified Strengths and Supports",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericIdentifiedStrengths") && data["GenericIdentifiedStrengths"].Answer.IsNotNullOrEmpty() ? data["GenericIdentifiedStrengths"].Answer.Clean() : string.Empty %>",0,4),
           "Identified Problems/Strengths");
           
            printview.addsection(
              
                printview.span("Planned Interventions",true) +
                printview.col(2,
                    printview.checkbox("Psychosocial Assessment",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Develop Appropriate Support System",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                    printview.checkbox("Counseling re Disease Process &#38; Management",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                    printview.checkbox("Community Resource Planning &#38; Outreach",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                    printview.checkbox("Counseling re Family Coping",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                    printview.checkbox("Stabilize Current Placement",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                    printview.checkbox("Crisis Intervention",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                    printview.checkbox("Determine/Locate Alternative Placement",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                    printview.checkbox("Long-range Planning &#38; Decision Making",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                    printview.checkbox("Financial Counseling and/or Referrals",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                    printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                    printview.span("<%= data != null && data.ContainsKey("GenericPlannedInterventionsOther") && data["GenericPlannedInterventionsOther"].Answer.IsNotNullOrEmpty() ? data["GenericPlannedInterventionsOther"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.span("Intervention Details",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericInterventionDetails") && data["GenericInterventionDetails"].Answer.IsNotNullOrEmpty() ? data["GenericInterventionDetails"].Answer.Clean() : string.Empty %>",0,4) +
                printview.span("Plan of Care",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer.IsNotNullOrEmpty() ? data["GenericPlanOfCare"].Answer.Clean() : string.Empty %>",0,4),
            "Interventions"); 
            
            printview.addsection(
            printview.col(2,
                printview.checkbox("Adequate Support System",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Improved Client/Family Coping",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Normal Grieving Process",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Appropriate Goals for Care Set by Client/Family",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Appropriate Community Resource Referrals",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Stable Placement Setting",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Mobilization of Financial Resources",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data != null && data.ContainsKey("GenericGoalsOther") && data["GenericGoalsOther"].Answer.IsNotNullOrEmpty() ? data["GenericGoalsOther"].Answer.Clean() : "%3Cspan class=%22blank%22 style=%22width:22.5em !important;%22%3E%3C/span%3E"%>",<%= data != null && data.ContainsKey("GenericGoals") && data["GenericGoals"].Answer.Split(',').Contains("8") ? "true" : "false"%>)+
                printview.span("Frequency:<%=data.AnswerOrEmptyString("GenericFrequency").Clean()%>") +
                printview.span("Duration:<%=data.AnswerOrEmptyString("GenericDuration").Clean()%>"))+
            printview.span("Visit Goal Details",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericVisitGoalDetails") && data["GenericVisitGoalDetails"].Answer.IsNotNullOrEmpty() ? data["GenericVisitGoalDetails"].Answer.Clean() : string.Empty %>",0,10),
            "Goals");
            
            printview.addsection(
            printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.Data.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.Data.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.Data.PhysicianSignatureDate.IsValid() ? Model.Data.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
            
            <%
    }).Render(); %>
</html>
