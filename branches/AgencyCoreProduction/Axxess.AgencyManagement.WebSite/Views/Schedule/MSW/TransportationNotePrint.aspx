﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%= Model.LocationProfile != null && Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Driver/Transportation Note<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/MSW/transportation.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (
                Model != null && Model.LocationProfile != null ?
                    (Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : string.Empty) +
                    (Model.LocationProfile != null ?
                        (Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : string.Empty) +
                        (Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", "+Model.LocationProfile.AddressLine2.Clean() + "<br />" : "<br />") +
                        (Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : string.Empty) +
                        (Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.ToUpper() + "  " : string.Empty) +
                        (Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : string.Empty) +
                        (Model.LocationProfile.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + Model.LocationProfile.PhoneWorkFormatted : string.Empty) +
                        (Model.LocationProfile.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.LocationProfile.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                : string.Empty).Clean() %>",
            "patientname": "<%= (
                Model != null && Model.PatientProfile != null ?
                    (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : string.Empty) +
                    (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : string.Empty) +
                    (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() + "<br/>" : "<br/>")
                : string.Empty).Clean() %>",
            "mr": "<%= Model != null && Model.PatientProfile != null && Model.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientProfile.PatientIdNumber : string.Empty %>",
            "visitdate": "<%= Model != null && Model.Data.VisitDate.IsNotNullOrEmpty() && Model.Data.VisitDate.IsValidDate() ? Model.Data.VisitDate.Clean() : string.Empty %>",
            "timein": "<%= Model != null && Model.Data.Questions != null && Model.Data.Questions.ContainsKey("TimeIn") && Model.Data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? Model.Data.Questions["TimeIn"].Answer.Clean() : string.Empty %>",
            "timeout": "<%= Model != null && Model.Data.Questions != null && Model.Data.Questions.ContainsKey("TimeOut") && Model.Data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? Model.Data.Questions["TimeOut"].Answer.Clean() : string.Empty %>",
            "mileage": "<%= Model != null && Model.Data.Questions.ContainsKey("AssociatedMileage") ? Model.Data.Questions["AssociatedMileage"].Answer.Clean() : ""%>",
            "episode": "<%= Model != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid() ? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()).Clean() : ""%>",
            "surcharge": "<%= Model != null && Model.Data.Questions.ContainsKey("Surcharge") ? Model.Data.Questions["Surcharge"].Answer.Clean() : ""%>",
            "sign": "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>",
            "signdate": "<%= Model != null && Model.Data.SignatureDate.IsDate() ? Model.Data.SignatureDate.Clean() : string.Empty %>"
            
        };
        PdfPrint.BuildSections(<%= Model.Data.PrintViewJson  %>); <%
    }).Render(); %>
</body>
</html>