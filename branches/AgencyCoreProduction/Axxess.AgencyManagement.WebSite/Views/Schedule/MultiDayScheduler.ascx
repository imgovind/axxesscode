﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeViewData>" %>
<%if(Model!=null){ %>
<% using (Html.BeginForm("AddMultiDaySchedule", "Schedule", FormMethod.Post, new { @id = "multiDayScheduleForm" })) { %>
<%= Html.Hidden("EpisodeId", Model.Id, new { @id = "multiDayScheduleEpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "multiDaySchedulePatientId" })%>
<%= Html.Hidden("visitDates", "", new { @id = "multiDayScheduleVisitDates" })%>
    <div class="bigtext">Quick Employee Scheduler</div>
    <div class="wrapper main">
        <fieldset>
            <legend>
	            <%--<span class="strong"><%= ""%></span> | <%= Model.StartDate.ToShortDateString()%> &#8211; <%= Model.EndDate.ToShortDateString()%>--%>
            </legend>
            <div class="column">
                <div class="row"><label class="float-left" for="multiDayScheduleUserID">User/Employee: </label><div class="float-right"><select class="Users requireddropdown" name="UserId" id="multiDayScheduleUserID" ></select></div></div>
                <div class="row"><label class="float-left" for="multiDayScheduleDisciplineTask">Task: </label><div class="float-right"><select class="requireddropdown" name="DisciplineTask" id="multiDayScheduleDisciplineTask" ></select></div></div>
            </div>
        </fieldset>
        <div class="strong">To schedule visits for the selected user, click on the desired dates in the calendar below:</div>
        <div class="trical">
	        <div class="calendar-container"></div>
		</div>
        <div class="clear"></div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" class="save close">Save</a></li><li><a href="javascript:void(0);" class="close">Cancel</a></li></ul></div>
    </div>
<% } %>
<%} %>
