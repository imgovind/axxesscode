﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions!=null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Nutritional Assessment<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>%3Cbr /%3E<%=Model.LocationProfile.PhoneWorkFormatted.IsNotNullOrEmpty()?"Phone:"+Model.LocationProfile.PhoneWorkFormatted:"" %><%=Model.LocationProfile.FaxNumberFormatted.IsNotNullOrEmpty()?"|Fax:"+Model.LocationProfile.FaxNumberFormatted:"" %>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Nutritional Assessment" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.PatientProfile.LastName.Clean()%>, <%= Model.PatientProfile.FirstName.Clean()%> <%= Model.PatientProfile.MiddleInitial.Clean()%>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.PatientProfile.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.Data.StartDate.ToShortDateString().ToZeroFilled(), Model.Data.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
       
       printview.addsection(
        printview.col(2,
            printview.span("Height:",true)+
            printview.span("<%=data.AnswerOrEmptyString("Height1").Clean() %>'"+"<%=data.AnswerOrEmptyString("Height2").Clean() %>")+
            printview.span("Weight:",true)+
            printview.span("<%=data.AnswerOrEmptyString("Weight").Clean() %> lbs")+
            printview.span("BMI:",true)+
            printview.span("<%=data.AnswerOrEmptyString("BMI").Clean() %>")+
            printview.span("Weight Status:<%=data.AnswerOrEmptyString("WeightStatus").Clean() %>")+
            printview.span("Past Medical History:<%=data.AnswerOrEmptyString("MedicalHistory").Clean() %>")+
            printview.span("Surgical History:<%=data.AnswerOrEmptyString("SurgicalHistory").Clean() %>")+
            printview.span("Current Diagnosis:<%=data.AnswerOrEmptyString("CurrentDiagnosis").Clean() %>")+
            printview.span("Food Allergies:<%=data.AnswerOrEmptyString("FoodAllergies").Clean() %>")+
            printview.span("Food Intolerances:<%=data.AnswerOrEmptyString("FoodIntolerances").Clean() %>")+
            printview.span("Dentition:<%=data.AnswerOrEmptyString("Dentition").Clean() %>")+
            printview.span("Chewing Ability:<%=data.AnswerOrEmptyString("ChewingAbility").Clean() %>")+
            printview.span("Swallowing Ability:<%=data.AnswerOrEmptyString("SwallowingAbility").Clean() %>")+
            printview.span("Current Diet/Texture:<%=data.AnswerOrEmptyString("CurrentDiet").Clean() %>")+
            printview.span("Liquid Consistency:<%=data.AnswerOrEmptyString("LiquidConsistency").Clean() %>")+
            printview.span("Previous Diets:<%=data.AnswerOrEmptyString("PreviousDiets").Clean() %>")+
            printview.span("Has client been counseled on diet?<%=data.AnswerOrEmptyString("HasClient").Clean() %>")+
            printview.span("Is Client taking any vitamins/minerals?<%=data.AnswerOrEmptyString("IsClient").Clean() %> <%=data.AnswerOrEmptyString("FeedingType").Clean() %>")+
            printview.span("Oral nutritional supplements?<%=data.AnswerOrEmptyString("OralNutritionalSupplements").Clean() %>")+
            printview.span("Is client receiving a tube feeding?<%=data.AnswerOrEmptyString("TubeFeeding").Clean() %>")+
            
            printview.span("Breakfast:<%=data.AnswerOrEmptyString("Breakfast").Clean() %>")+
            printview.span("Lunch:<%=data.AnswerOrEmptyString("Lunch").Clean() %>")+
            printview.span("Dinner:<%=data.AnswerOrEmptyString("Dinner").Clean() %>")+
            printview.span("Snacks:<%=data.AnswerOrEmptyString("Snacks").Clean() %>")+
            printview.span("Skin condition:<%=data.AnswerOrEmptyString("SkinCondition").Clean() %>")+
            printview.span("Stasis ulcer/location:<%=data.AnswerOrEmptyString("Stasis").Clean() %>")+
            printview.span("Diabetic ulcer/location:<%=data.AnswerOrEmptyString("Diabetic").Clean() %>")+
            printview.span("Skin Turgor:<%=data.AnswerOrEmptyString("SkinTurgor").Clean() %>")+
            printview.span("Mucous Membranes:<%=data.AnswerOrEmptyString("MucousMembranes").Clean() %>")+
            printview.span("Patient Lab Data:<%=data.AnswerOrEmptyString("PatientLabData").Clean() %>")+
            printview.span("Pertinent Medications:<%=data.AnswerOrEmptyString("PertinentMedications").Clean() %>")+
            printview.span("Estimated Caloric Needs:<%=data.AnswerOrEmptyString("EstimatedCaloricNeeds").Clean() %>")+
            printview.span("Estimated Protein Needs:<%=data.AnswerOrEmptyString("EstimatedProteinNeeds").Clean() %>")+
            printview.span("Estimated Fluid Needs:<%=data.AnswerOrEmptyString("EstimatedFluidNeeds").Clean() %>")+
            printview.span("Is current diet/TF adequate to meet caloric/protein/fluid needs?<%=data.AnswerOrEmptyString("IfAdequateToMeet").Equals("1")?"Yes":data.AnswerOrEmptyString("IfAdequateToMeet").Equals("0")?"No":"" %>")+
            printview.span("<%=data.AnswerOrEmptyString("FormulaType").Clean() %> provided by formula:<%=data.AnswerOrEmptyString("ProvidedByFormula").Clean() %>")),
                       
            ""); 
        printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("Assessment").Clean() %>",3),
            "Assessment");
        printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("Plan").Clean() %>",3),
            "Plan");
        printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>