﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleListViewData>" %>
<span class="wintitle">Visit Log | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("EditVisitLog", "Schedule", FormMethod.Post, new { @id = "newVisitLogForm" })) { %>
<%= Html.Hidden("StartDate", Model.StartDate, new { disabled = "disabled" }) %>
<%= Html.Hidden("EndDate", Model.EndDate, new { disabled = "disabled" }) %>
<%= Html.Hidden("PatientId", Model.PatientId, new { disabled = "disabled" }) %>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { disabled = "disabled" })%>
<div class="wrapper main">
    <fieldset>
    <div class="column">
        <div class="row">
            <span class="strong">Patient: <%=  Model.DisplayName %></span>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <span class="strong">Episode: <%= Model.StartDate.ToZeroFilled()%> -  <%= Model.EndDate.ToZeroFilled()%></span>
        </div>
    </div>
    <table class="form">
         <tr>
            <td><label class="fill"><strong>Task:</strong></label></td>
            <td><label class="fill"><strong>Task Date:</strong></label></td>
            <td><label class="fill"><strong>Assigned To</strong></label></td>
            <td><label class="fill"><strong>Status</strong></label></td>
            <td><label class="fill"><strong>Visit Date</strong></label></td>
            <td><label class="fill"><strong>Time In:</strong></label></td>
            <td><label class="fill"><strong>Time Out:</strong></label></td>
        </tr>
    
    <% var i = 0;
         foreach (var data in Model.List)
          { %>
            <%= Html.Hidden( "[" + i + "]."+"PatientId", Model.PatientId,  new { @id = "VisitLog_PatientId" , @class="patientid" })%>
            <%= Html.Hidden( "[" + i + "]."+"EpisodeId", Model.EpisodeId, new { @id = "VisitLog_EpisodeId" })%>
            <%= Html.Hidden("[" + i + "]." + "Id", data.Id, new { @id = "VisitLog_EventId" })%>
        <tr>
            <td><span id="VisitLog_TaskName_<%= i %>" name="[<%= i%>].TaskName"><%= data.DisciplineTaskName%></span></td>
            <td><span id="VisitLog_TaskDate_<%= i %>"  class="width100" name="[<%= i%>].TaskDate"><%= data.EventDate %></span></td>
            <td><span id="VisitLog_UserName_<%= i %>" name="[<%= i%>].UserName"><%= data.UserName %></span></td>
            <td><%= Html.Status("[" + i + "]." + "Status", data.Status.ToString(), data.DisciplineTask,data.EventDate, new { @id = "VisitLog_Status_" + i, @style = "width:130px" ,@class="status"})%></td>
            <td><input type="text"  style="width:100px" id="VisitLog_VisitDate_<%= i %>" class="date-picker required visitdate" name="[<%= i%>].VisitDate" value="<%= data.VisitDate %>" /></td>
            <td><input type="text" style="width:120px" size="10" id="VisitLog_TimeIn_<%= i %>" name="[<%= i%>].TimeIn" class="time-picker fill tasktimein" value="<%= data.TimeIn %>" /></td>
            <td><input type="text" style="width:120px" size="10" id="VisitLog_TimeOut_<%= i %>" name="[<%= i %>].TimeOut" class="time-picker fill tasktimeout" value="<%=data.TimeOut %>" /></td>
        </tr>
        <% i++;
          }
         %>
    </table>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" class="save close" id="savebutton">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div> 
<%  } %>
<script type="text/javascript">
    $("#savebutton").click(function() {
        Schedule.loadCalendarAndActivities("<%=Model.PatientId %>","<%=Model.EpisodeId %>");
    });
</script>


