﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<% DateTime startdate=Model.StartDate;%>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task:</label></td>
            <td><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, Model.PatientId, new { @id = "Schedule_Detail_DisciplineTask", @class = "requireddropdown" })%></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_Billable" class="bold">Billable:</label></td>
            <td style="min-height:24px"><%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "Schedule_Detail_Billable", @class = "radio" })%></td>
            <td><label for="Schedule_Detail_Payable" class="bold">Payable:</label></td>
            <td><%= Model.IsPayable == true ? string.Format("<input type='checkbox' id='Schedule_Detail_IsPayable' class='radio' value='true' checked='checked' name='IsPayable'>") : string.Format("<input type='checkbox' id='Schedule_Detail_IsPayable' class='radio' value='true' name='IsPayable'>")%></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Travel Time In:</label></td>
            <td><input type="text" size="10" id="Schedule_Detail_TravelTimeIn" name="TravelTimeIn" class="time-picker" value="<%= Model.TravelTimeIn %>" /></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Travel Time Out:</label></td>
            <td><input type="text" size="10" id="Schedule_Detail_TravelTimeOut" name="TravelTimeOut" class="time-picker" value="<%= Model.TravelTimeOut %>" /></td>
        </tr>
        <tr>
        <% if(Model.IsSkilledNurseNote()){ %>
            <tr>
                <td><label class="float-left">Supply:</label></td>
                <td>
                    <div class="buttons">
                        <ul><li>
                            <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" class=\"float-left\" >Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.Id)%>
                        </li></ul>
                    </div>
                </td>
            </tr>
        <% } %>
        <tr>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date:</label></td>
            <td><input type="text" class="date-picker required" name="EventDate" value="<%= Model.EventDate %>" id="Schedule_Detail_EventDate" /></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_Status" class="bold">Actual Visit Date:</label></td>
            <td><input type="text" class="date-picker required" name="VisitDate" value="<%= Model.VisitDate %>" id="Schedule_Detail_VisitDate" /></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Visit Time In:</label></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeIn" name="TimeIn" class="time-picker" value="<%= Model.TimeIn %>" /></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Visit Time Out:</label></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeOut" name="TimeOut" class="time-picker" value="<%= Model.TimeOut %>" /></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_Status" class="bold">Status:</label></td>
            <td><%= Html.Status("Status", Model.Status.ToString(), Model.DisciplineTask, Model.EventDate, new { @id = "Schedule_Detail_Status" }) %></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Assigned To:</label></td>
            <td style="min-height:24px"><% if ((!Model.IsComplete || Model.DisciplineTask == (int) DisciplineTasks.FaceToFaceEncounter) && !Model.IsUserDeleted){ %>
                    <%= Html.Users("UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users requireddropdown" })%>
                <% } else { %>
                    <%= Html.Hidden("UserId", Model.UserId)%>
                    <span style="font-size:16px"><%= Model.UserName %></span>
                <% } %></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_Surcharge" class="bold">Surcharge:</label></td>
            <td><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "Schedule_Detail_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></td>
        </tr>
        <tr>
            <td><label for="Schedule_Detail_AssociatedMileage" class="bold">Associated Mileage:</label></td>
            <td><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "Schedule_Detail_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></td>
        </tr>
        
        <% if (Model.Discipline == (int)Disciplines.Orders) { %>
        
            <tr>
                <td><label for="Schedule_Detail_PhysicianId" class="bold">Physician:</label></td>
                <td>
                    <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Schedule_Detail_PhysicianId", @class = "Physicians" })%>
                    <script type="text/javascript"> $("#Schedule_Detail_PhysicianId").PhysicianInput(); </script>
                </td>
            </tr>
        
        <% } else { %>
        
            <tr>
                <td colspan="4"><label for="Schedule_Detail_ServiceLocation" class="bold">Service Location (Q Codes):</label></td>
                <td>
                    <% if (Model.IsServiceLocationRequired) { %>
                        <%= Html.ServiceLocations("ServiceLocation", Model.ServiceLocation, new { @id = "Schedule_Detail_ServiceLocation", @class = "requireddropdown valid" })%>
                    <% } else { %>
                        <%= Html.ServiceLocations("ServiceLocation", Model.ServiceLocation, new { @id = "Schedule_Detail_ServiceLocation" })%>
                    <% } %>
                </td>
            </tr>
        
        <% } %>
    
    </tbody></table>               
</fieldset>
    
<%--<script type="text/javascript">
    var ActivateDate = new Date("05/01/2013");
    var StartDate = new Date("<%=startdate %>");
    if (StartDate >= ActivateDate) {
        alert("ok");
        $("#Schedule_Detail_ServiceLocation").addClass("requireddropdown valid");
    } else {
    alert("not ok");
    }
</script>--%>