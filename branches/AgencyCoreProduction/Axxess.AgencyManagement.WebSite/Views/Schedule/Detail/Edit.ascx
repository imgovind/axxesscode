﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<div class="wrapper main">
<span class="wintitle">Task Details | <%= Model.DisciplineTaskName %> | <%= Model.PatientName %></span>
<%  using (Html.BeginForm("UpdateDetails", "Schedule", FormMethod.Post, new { @id = "Schedule_DetailForm" }))
    { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("Discipline", Model.Discipline)%>
    <div class="align-center bigtext"><%= Model.PatientName + (Model.PatientIdNumber.IsNotNullOrEmpty() ? " (" + Model.PatientIdNumber + ") | " : "") %><%= Model.StartDate.ToShortDateString().ToZeroFilled() %> &#8211; <%= Model.EndDate.ToShortDateString().ToZeroFilled() %></div>
    <%  Html.RenderPartial("Detail/Note", Model); %>
    <fieldset>
        <legend>Episode Selection</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Schedule_Detail_IsEpisodeReassiged">Check here to move this visit/document to another Episode</label>
                <%= Html.CheckBox("IsEpisodeReassiged", false, new { @id = "Schedule_Detail_IsEpisodeReassiged", @class = "radio float-left" })%>
            </div>
            <div class="row" id="Schedule_Detail_EpisodeReassigedContainer">
                <label for="Schedule_Detail_NewEpisodeId">Episode List:</label>
                <%= Html.PatientEpisodesExceptCurrent("NewEpisodeId", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "Schedule_Detail_NewEpisodeId" })%>
            </div>
        </div>
    </fieldset>
    <% if (Current.UserId == Model.UserId) { %>
    <fieldset>
        <legend>Personal Notes<em> (From Mobile Device)</em></legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="Schedule_Detail_PersonalComments" name="PersonalComments" cols="5" rows="6"><%= Model.PersonalComments %></textarea>
            </div>
        </div>
    </fieldset>
    <% } %>
    <fieldset>
        <legend>Comments <span class="img icon note"></span><em>(Yellow Sticky Note)</em></legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="Schedule_Detail_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
    <fieldset>
        <legend style="height: 18px;">Return Reason <span class="tooltip red-note" style="display:inline-block; margin-right: 5px; margin-left: 5px;"></span><em>(Red Sticky Note)</em></legend>
        <div class="wide-column">
            <div id="Schedule_Detail_ReturnComments" class="row"></div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Attachments</legend>
        <div class="wide-column">
            <div class="row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <span>There are <span id="scheduleEvent_Assest_Count"><%= Model.Assets.Count.ToString() %></span> attachment(s).</span>
                <div class="clear"></div>
    <%  if(Model.Assets.Count>0){
    foreach (Guid assetId in Model.Assets) { %>
                <span><%= Html.Asset(assetId) + " | " + string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.DeleteScheduleEventAsset($(this),'{0}','{1}','{2}','{3}');\">Delete</a>&nbsp;", Model.PatientId, Model.EpisodeId, Model.Id, assetId) %></span>
                <div class="clear"></div>
    <%  }} %>
                <br />
                <br />
                <span>Use the upload fields below to upload files associated with this scheduled task.</span>
                <br />
                <table class="form">
                    <tbody>
                        <tr>
                            <td><input id="Schedule_Detail_File1" type="file" name="Attachment1" /></td>
                        </tr>
                        <tr>
                            <td><input id="Schedule_Detail_File2" type="file" name="Attachment2" /></td>
                        </tr>
                        <tr>
                            <td><input id="Schedule_Detail_File3" type="file" name="Attachment3" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.LoadLog('{0}','{1}',{2});\" >Activity Logs</a>", Model.PatientId, Model.Id, Model.DisciplineTask)%></div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" class="save">Save</a></li>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    U.ShowIfChecked($("#Schedule_Detail_IsEpisodeReassiged"), $("#Schedule_Detail_EpisodeReassigedContainer"));
    $("#Schedule_Detail_ReturnComments").ReturnComments({ Reasons: "<%= Model.ReturnReason %>", EventId: "<%= Model.Id %>", EpisodeId: "<%= Model.EpisodeId %>", PatientId: "<%= Model.PatientId %>" });
</script>