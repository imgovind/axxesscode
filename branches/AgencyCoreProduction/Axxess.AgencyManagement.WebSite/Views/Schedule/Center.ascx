﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<span class="wintitle">Schedule Center | <%= Current.AgencyName %></span>
<%  string[] stabs = new string[] { "Nursing", "HHA", "MSW", "Therapy", "Dietician" }; %>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="buttons heading">
                <ul>
<%  if (Current.HasRight(Permissions.ManagePatients) && !Current.IsAgencyFrozen) { %>
                    <li><a onclick="Acore.Open('newpatient')" title="Add New Patient">Add New Patient</a></li>
<%  } %>
                </ul>
            </div>
            <div class="row">
                <label for="ScheduleCenter_BranchCode">Branch</label>
                <div><%=Html.UserBranchList("BranchCode", "", true, "All", new { @id = "ScheduleCenter_BranchCode"}) %></div>
            </div>
            <div class="row">
                <label for="ScheduleCenter_PatientStatus">View</label>
                <div>
                    <%  var statusList = new List<SelectListItem>() {
                            new SelectListItem(){ Text = "Active Patients", Value = "1" },
                            new SelectListItem(){ Text = "Discharged Patients", Value = "2"},
                            new SelectListItem(){ Text = "Pending Patients", Value = "3"},
                            new SelectListItem(){ Text = "Non-Admitted Patients", Value = "4"}
                        }; %>
                    <%= Html.DropDownList("PatientStatus", statusList, new { @id = "ScheduleCenter_PatientStatus" })%>
                </div>
            </div>
            <div class="row">
                <label for="ScheduleCenter_PaymentSource">Filter</label>
                <div>
                    <select name="PaymentSource" id="ScheduleCenter_PaymentSource">
                        <option value="0">All</option>
                        <option value="1">Medicare (Traditional)</option>
                        <option value="2">Medicare (HMO/Managed Care)</option>
                        <option value="3">Medicaid (Traditional)</option>
                        <option value="4">Medicaid (HMO/Managed Care)</option>
                        <option value="5">Workers' Compensation</option>
                        <option value="6">Title Programs</option>
                        <option value="7">Other Government</option>
                        <option value="8">Private</option>
                        <option value="9">Private HMO/Managed Care</option>
                        <option value="10">Self Pay</option>
                        <option value="11">Unknown</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="ScheduleCenter_TextFilter">Find</label>
                <div><input id="ScheduleCenter_TextFilter" name="TextFilter" value="" type="text" /></div>
            </div>
        </div>
        <div class="bottom"><% Html.RenderPartial("Patients", Model.PatientStatus); %></div>
    </div>
    <div id="ScheduleCenter_MainResult" class="ui-layout-center loading-visibility">
        <div class="top">
            <div id="ScheduleCenter_Top">
                <div class="trical">
                    <div class="window-menu">
                        <ul>
							<% if (!Model.IsDischarged && !Model.IsNotAdmitted && !Model.IsPending && Current.HasRight(Permissions.EditEpisode) && !Current.IsAgencyFrozen) { %>
								<li><a id="ScheduleCenter_NewEpisode"  title="Add New Episode">New Episode</a></li>
							<% } %>
							<% if (!Model.IsNotAdmitted && !Model.IsPending && Current.HasRight(Permissions.ScheduleVisits) && !Current.IsAgencyFrozen) { %>
								<li><a id="ScheduleCenter_ScheduleEmployee" title="Multiple Employee">Schedule Employee</a></li>
							<% } %>
                            <li>
                                <a class="menu-trigger">Episode Manager</a>
                                <ul class="menu">
									<%  if (Current.HasRight(Permissions.EditEpisode)) { %>
										<li><a id="ScheduleCenter_InactiveEpisodes">Inactive Episodes</a></li>
									<% } %>
                                    <li><a id="ScheduleCenter_Frequencies">Episode Frequencies</a></li>
									<%  if (Current.HasRight(Permissions.DeleteTasks)&& !Current.IsAgencyFrozen) { %>
										<li><a id="ScheduleCenter_VisitLog" title="Visit Log">Visit Log</a></li>
									<% } %>
                                </ul>
                            </li>
                            <li><a id="ScheduleCenter_MasterCalendar">Master Calendar</a></li>
							<% if (Current.HasRight(Permissions.ScheduleVisits)&& !Current.IsAgencyFrozen) { %>
								<li>
									<a id="ScheduleCenter_ReassignSchedules" title="Reassign Schedules">Reassign Schedules</a>
									<div id="reassign_Episode_Container" class="reassignschedulepatientmodal hidden"></div>
								</li>
							<% } %>
							<% if (Current.HasRight(Permissions.DeleteTasks)&& !Current.IsAgencyFrozen) { %>
								<li><a id="ScheduleCenter_DeleteSchedules" title="Delete Schedules">Delete Multiple Tasks</a></li>
							<% } %>

                        </ul>
                    </div>
                    <span class="abs-left">
                        <a id="ScheduleCenter_PreviousEpisode" identifier="previousEpisodeId">
                            <span class="largefont">&#171;</span>
                            Previous Episode
                        </a>
                    </span>
                    <span class="abs-right">
                        <a id="ScheduleCenter_NextEpisode" identifier="nextEpisodeId">
                            Next Episode
                            <span class="largefont">&#187;</span>
                        </a>
                    </span>
                    <div class="clear"></div>
                    <span id="ScheduleCenter_PatientName" class="strong"></span>
                    <select id="ScheduleCenter_EpisodeDropDown"></select>
                    <div class="buttons editeps">
                        <ul>
	                        <% if (Current.HasRight(Permissions.EditEpisode) && !Current.IsAgencyFrozen) { %>
								<li><a onclick="Schedule.Center.EditEpisode();">Manage Episode</a></li>
							<% } %>
							<li><a onclick="Schedule.Center.Load();">Refresh</a></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                    <div class="calendar-container">

                    </div>
                </div>
                <div class="clear"></div>
                <div class="buttons float-left">
                    <ul>
						<li>
							<a id="ScheduleCenter_ViewPatientCharts" href="javascript:void(0);">View Patient Chart</a>
						</li>
                    </ul>
                </div>
                <fieldset class="calendar-legend" style="margin-left:25%">
                    <ul>
                        <li>
                            <div class="scheduled">&#160;</div>
                            Scheduled
                        </li>
                        <li>
                            <div class="completed">&#160;</div>
                            Completed
                        </li>
                        <li>
                            <div class="missed">&#160;</div>
                            Missed
                        </li>
                        <li>
                            <div class="multi">&#160;</div>
                            Multiple
                        </li>
                    </ul>
                </fieldset>
            </div>
            <div id="schedule-collapsed"><a class="show-scheduler">Show Scheduler</a></div>
            <div id="ScheduleCenter_TabStrip" class="scheduler horizontal-tabs">
				<ul class="horizontal-tab-list">
					<li><a href="#ScheduleCenter_NursingTab" discipline="Nursing" class="no-border">Nursing</a></li>
					<li><a href="#ScheduleCenter_HHATab" discipline="HHA" class="no-border">HHA</a></li>
					<li><a href="#ScheduleCenter_MSWTab" discipline="MSW" class="no-border">MSW/Other</a></li>
					<li><a href="#ScheduleCenter_TherapyTab" discipline="Therapy" class="no-border">Therapy</a></li>
					<li><a href="#ScheduleCenter_DieticianTab" discipline="Dietician" class="no-border">Dietician</a></li>
					<li><a href="#ScheduleCenter_OrdersTab" discipline="Orders" class="no-border">Orders/Care Plans</a></li>
					<li><a href="#ScheduleCenter_OutlierTab" discipline="Multiple" class="no-border">Daily/Outlier</a></li>
				</ul>
				<%  for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
					<%  string stitle = stabs[sindex]; %>
					<%  string tabname = stitle == "MSW" ? "MSW / Other" : stitle; %>
				<div id="ScheduleCenter_<%= stitle %>Tab" class="scheduler tab-content">
					<div class="buttons fr align-center">
						<ul>
							<li><a class="schedule-save">Save</a></li><br />
							<li><a class="schedule-cancel">Cancel</a></li>
						</ul>
					</div>
					<div class="acore-grid">
						<ul>
							<li>
								<span class="grid-third">Task</span>
								<span class="grid-third">User</span>
								<span>Date</span>
							</li>
						</ul>
						<ol></ol>
					</div>
				</div>
				<%  } %>
				<div id="ScheduleCenter_OrdersTab" class="scheduler tab-content">
					<div class="buttons fr align-center">
						<ul>
							<li><a class="schedule-save">Save</a></li><br />
							<li><a class="schedule-cancel">Cancel</a></li>
						</ul>
					</div>
					<div class="acore-grid">
						<ul>
							<li>
								<span class="grid-third">Task</span>
								<span class="grid-third">User</span>
								<span>Date</span>
							</li>
						</ul>
						<ol></ol>
					</div>
				</div>
				<div id="ScheduleCenter_OutlierTab" class="scheduler tab-content">
					<div class="buttons fr align-center">
						<ul>
							<li><a class="schedule-save multiple">Save</a></li><br />
							<li><a class="schedule-cancel">Cancel</a></li>
						</ul>
					</div>
					<div class="acore-grid">
						<ul>
							<li>
								<span class="grid-third">Task</span>
								<span class="grid-third">User</span>
								<span>Date Range</span>
							</li>
						</ul>
						<ol>
							<li class="permanent no-hover">
								<span class="grid-third"><select name="DisciplineTask"></select></span>
								<span class="grid-third"><select name="UserId"></select></span>
								<span class="grid-third">
									<input type="text" name="StartDate" class="schedule-date-range" />
									&#8211;
									<input type="text" name="EndDate" class="schedule-date-range" />
								</span>
							</li>
						</ol>
					</div>
				</div>
            </div>
        </div>
        <div id="ScheduleCenter_BottomPanel" class="bottom">
            <%  Html.Telerik().Grid<ScheduleEventJson>().Name("ScheduleActivityGrid").Columns(columns => {
					//columns.Bound(s => s.EventId).Visible(false).Width(0);
					columns.Bound(s => s.Task).ClientTemplate("<# if(IsComplete || !OnClick) { #><#= Task #><# } else { #><a class=\"link\" onclick=\"<#= OnClick #>\"><#= Task #></a><# } #>").Title("Task").Width(180);
					columns.Bound(s => s.Date).ClientTemplate("<#= U.FormatDate(Date) #>").Title("Scheduled Date").Width(120);
					columns.Bound(s => s.User).Title("Assigned To").Width(130);
					columns.Bound(s => s.Status).Title("Status").Width(100);
					columns.Bound(s => s.IsVisitVerified).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" });
					columns.Bound(s => s.OasisProfileUrl).Sortable(false).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" });
					columns.Bound(s => s.StatusComment).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"red-note tooltip\" href=\"javascript:void(0);\"><#=StatusComment#></a>");
					columns.Bound(s => s.Comments).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=Comments#></a>");
					columns.Bound(s => s.EpisodeNotes).Sortable(false).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\"><#=EpisodeNotes#></a>");
					columns.Bound(s => s.PrintUrl).Title(" ").Sortable(false).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=PrintUrl#>").Width(22);
					columns.Bound(s => s.HasAttachments).Title(" ").Sortable(false).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Width(22);
					columns.Bound(s => s.AP).ClientTemplate("<#=AP#>").Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
					columns.Bound(s => s.IsComplete).Visible(false);
				}).ClientEvents(c => c.OnRowDataBound("Schedule.Center.Activities.OnRowDataBound").OnDataBinding("Schedule.Center.Activities.OnDataBinding").OnDataBound("Schedule.Center.Activities.OnDataBound").OnLoad("U.OnTGridClientLoad"))
				.DataBinding(dataBinding => dataBinding.Ajax())
				.Sortable(s => s.OrderBy(o => o.Add("Date").Descending())).Scrollable().Footer(false).Render(); %>
        </div>
    </div>
</div>