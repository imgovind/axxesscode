﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EpisodeLean>>" %>
 <ul><li class="align-center"><h3>Inactive Episode(s)</h3></li><li><span class="range align-center">Episode Range</span><span class="action align-center">Action</span></li></ul>
 <ol>
    <%if (Model != null && Model.Count > 0)
      {
          int i = 1;
          foreach (var episode in Model)
          { %>
		<li class="<%= i % 2 != 0 ? "odd " : "even " %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
            <span class="range align-center"><%=episode.Range%></span>
            <span class="action align-center"><% if(!Current.IsAgencyFrozen) { %><a href="javascript:void(0);" onclick="Schedule.ActivateEpisode('<%=episode.Id %>', '<%= episode.PatientId %>');" >Activate</a> | <a href="javascript:void(0);" onclick="UserInterface.ShowEditEpisodeModal('<%=episode.Id %>','<%= episode.PatientId %>');">Edit</a><% } %></span>
        </li>
    <% i++;
          }
      }
      else
      { %><li class="align-center"><span ><strong>There is no inactive episode for this patient.</strong></span> </li> <%} %>
  </ol>
