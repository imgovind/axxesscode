﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<% using (Html.BeginForm("UpdateEpisode", "Schedule", FormMethod.Post, new { @id = "editEpisodeForm" })){ %>
   <%= Html.Hidden("Id", Model.Id) %>
   <%= Html.Hidden("Count", "0") %>
   <%= Html.Hidden("Edit_Episode_PreviousEndDate", Model.PreviousEpisode != null ? Model.PreviousEpisode.EndDate.ToShortDateString() : "")%>
   <%= Html.Hidden("PatientId", Model.PatientId, new { @Id = "Edit_Episode_PatientId" })%>
<div class="wrapper main">
    <span class="bigtext align-center">Edit Episode: <%=Model.DisplayName %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column"><div class="row"><span class="bigtext"><%= Model.DisplayName %></span></div></div>
        <div class="column"><div class="row"><label for="Edit_Episode_StartOfCareDate" class="float-left">Start Of Care Date:</label><div class="float-right"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "Edit_Episode_AdmissionId" })%></div></div></div>
    </fieldset>
    <% if (Model.PreviousEpisode != null) { %><label class="bold">Tip:</label><em> Previous episode end date is: <%= Model.PreviousEpisode.EndDate.ToShortDateString() %></em><% } %>
    <% if (Model.NextEpisode != null) { %><label class="bold">Tip:</label><em>Next episode start date is: <%= Model.NextEpisode.StartDate.ToShortDateString() %></em><% } %>
    <fieldset>
        <legend>Details</legend>
        <table class="form"><tbody>
            <tr>
                <td><label for="Edit_Episode_TargetDate" class="float-left">Episode Start Date:</label><br /><input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" onchange="Schedule.editEpisodeStartDateOnChange()" id="Edit_Episode_StartDate" style="width:205px" /></td>
                <td><label for="Edit_Episode_VisitDate" class="float-left">Episode End Date:</label><br /><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDateFormatted %>" id="Edit_Episode_EndDate" /></td>
                <td><label for="Edit_Episode_IsActive" class="float-left">Inactivate Episode:</label><br /><%= Html.CheckBox("IsActive",!Model.IsActive, new { @id = "Edit_Episode_IsActive", @class = "radio" })%></td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_CaseManager" class="float-left">Case Manager:</label><br /><%= Html.Users("Detail.CaseManager", Model.Detail.CaseManager, new { @id = "Edit_Episode_CaseManager", @class = "Users required valid" })%></td>
                <td><label for="Edit_Episode_PrimaryInsurance" class="float-left">Primary Insurance:</label><br /><%= Html.InsurancesByBranch("Detail.PrimaryInsurance", Model.Detail.PrimaryInsurance, Model.AgencyLocationId, false, new { @id = "Edit_Episode_PrimaryInsurance", @class = "Insurances requireddropdown valid" })%></td>
                <td><label for="Edit_Episode_SecondaryInsurance" class="float-left">Secondary Insurance:</label><br /><%= Html.InsurancesByBranch("Detail.SecondaryInsurance", Model.Detail.SecondaryInsurance, Model.AgencyLocationId, false, new { @id = "Edit_Episode_SecondaryInsurance", @class = "Insurances" })%></td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_PrimaryPhysician" class="float-left">Primary Physician:</label><br /><%= Html.TextBox("Detail.PrimaryPhysician", Model.Detail.PrimaryPhysician, new { @id = "Edit_Episode_PrimaryPhysician", @class = "Physicians" })%></td>
                <td></td>
                <td></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments <span class="img icon note-blue"></span><span style="font-weight: normal;">(Blue Sticky Note)</span></legend>
        <div class="wide-column"><div class="row"><textarea id="Edit_Episode_Comments" name="Detail.Comments" cols="" rows="10"><%= Model.Detail.Comments %></textarea></div></div>
        <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"$('#window_ModalWindow').Close();Log.LoadEpisodeLog('{0}','{1}');\" >Activity Logs</a>", Model.Id, Model.PatientId)%></div>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li></ul></div>
</div>
<script type="text/javascript">
    $("#Edit_Episode_PrimaryPhysician").PhysicianInput();
    Schedule.WarnEpisodeGap("Edit_Episode");
</script>
<% } %>
