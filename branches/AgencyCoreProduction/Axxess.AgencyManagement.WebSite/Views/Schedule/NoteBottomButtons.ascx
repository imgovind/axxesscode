﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<div class="buttons">
    <ul>
        <li><a href="javascript:void(0);" class="save autosave">Save</a></li>
        <li><a href="javascript:void(0);" class="complete">Complete</a></li>
    <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
        <li><a href="javascript:void(0);" class="complete">Approve</a></li>
        <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
        <li><a href="javascript:void(0);" class="save close">Return</a></li>
        <% } %>
    <% } %>
        <li><a href="javascript:void(0);" class="close">Exit</a></li>
    </ul>
</div>