﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Poor Insight", "Poor Judgment", "Unrealistic Regarding Degree of Illness", "Doesn't Know Why He Is Here", "Unmotivated for Treatment", "Unrealistic Regarding Goals" }; %>
<div id="<%= Model.Type %>_InsightAndJudgmentContainer">
    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericInsightAndJudgment<%= count %>"><%= question %>:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericInsightAndJudgment" + count, data.AnswerOrEmptyString("GenericInsightAndJudgment" + count), new { @id = Model.Type + "_GenericInsightAndJudgment" + count, @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <% count++; %>
    <% } %>
    <label for="<%= Model.Type %>_GenericInsightAndJudgmentComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericInsightAndJudgmentComment", data.AnswerOrEmptyString("GenericInsightAndJudgmentComment"), new { @id = Model.Type + "_GenericInsightAndJudgmentComment", @class = "fill" })%></div>
</div>