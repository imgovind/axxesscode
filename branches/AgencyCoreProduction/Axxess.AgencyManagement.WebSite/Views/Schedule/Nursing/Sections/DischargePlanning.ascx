﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Discharge Planning Discussed:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericDischargePlanningDiscussed", "1", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("1"), new { @id = Model.Type + "_GenericDischargePlanningDiscussed1" }) %>
                <label for="<%= Model.Type %>_GenericDischargePlanningDiscussed1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericDischargePlanningDiscussed", "0", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("0"), new { @id = Model.Type + "_GenericDischargePlanningDiscussed0" }) %>
                <label for="<%= Model.Type %>_GenericDischargePlanningDiscussed0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericDischargePlanningDiscussedWith" class="float-left">Discharge Planning Discussed with:</label>
<div class="float-right">
    <%  var dischargePlanningDiscussed = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Patient", Value = "Patient" },
        new SelectListItem { Text = "Caregiver", Value = "Caregiver" },
        new SelectListItem { Text = "Physician", Value = "Physician" },
        new SelectListItem { Text = "Pt/CG", Value = "Pt/CG" },
        new SelectListItem { Text = "Pt/CG/Physician", Value = "Pt/CG/Physician" },
        new SelectListItem { Text = "N/A", Value = "N/A" }
    }, "Value", "Text", data.AnswerOrDefault("GenericDischargePlanningDiscussedWith", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericDischargePlanningDiscussedWith", dischargePlanningDiscussed, new { @id = Model.Type + "_GenericDischargePlanningDiscussedWith", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label class="float-left">Reason for Discharge:</label>
<div class="float-right">
    <%  var reasonForDischarge = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Goals Met", Value = "Goals Met" },
        new SelectListItem { Text = "Transfer", Value = "Transfer" },
        new SelectListItem { Text = "Deceased", Value = "Deceased" },
        new SelectListItem { Text = "Other", Value = "Other" }
    }, "Value", "Text", data.AnswerOrDefault("GenericDischargeReasonForDischarge", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericDischargeReasonForDischarge", reasonForDischarge, new { @id = Model.Type + "_GenericDischargeReasonForDischarge", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label class="float-left">Physician Notified of Discharge:</label>
<div class="float-right">
    <ul class="checkgroup">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "1", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("1"), new { @id = Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge1" }) %>
                <label for="<%= Model.Type %>_GenericDischargePhysicianNotifiedOfDischarge1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "0", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("0"), new { @id = Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge0" }) %>
                <label for="<%= Model.Type %>_GenericDischargePhysicianNotifiedOfDischarge0">No</label>
            </div>
        </li>
    </ul>

</div>
<div class="clear"></div>
<%  string[] pateintReceivedDischarge = data.AnswerArray("GenericPateintReceivedDischarge"); %>
<%= Html.Hidden(Model.Type + "_GenericPateintReceivedDischarge", string.Empty, new { @id = Model.Type + "_GenericPateintReceivedDischargeHidden" })%>
<ul class="checkgroup">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericPateintReceivedDischarge1' name='{0}_GenericPateintReceivedDischarge' value='1' type='checkbox' {1} />", Model.Type, pateintReceivedDischarge.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericPateintReceivedDischarge1">Patient received discharge notice per agency policy.</label>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericDischargeComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericDischargeComment", data.AnswerOrEmptyString("GenericDischargeComment"), new { @class = "fill", @id = Model.Type + "_GenericDischargeComment" })%></div>