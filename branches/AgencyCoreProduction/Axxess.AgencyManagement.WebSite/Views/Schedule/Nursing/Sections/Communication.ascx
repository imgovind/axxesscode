﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Overall Communication Status:</label>
<div class="float-right">
    <%  var communication = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericCommunication1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericCommunication1", communication, new { @id = Model.Type + "_GenericCommunication1", @class = "oe" })%>
</div>
<div class="clear"></div>
<div>
    <label class="float-left" for="<%= Model.Type %>_GenericCommunicationSocialization">Socialization:</label>
    <%= Html.TextBox(Model.Type + "_GenericCommunicationSocialization", data.AnswerOrEmptyString("GenericCommunicationSocialization"), new { @id = Model.Type + "_GenericCommunicationSocialization", @class = "float-right" })%>
</div>
<div class="clear"></div>
<div>
    <label class="float-left" for="<%= Model.Type %>_GenericCommunicationSomatization">Somatization:</label>
    <%= Html.TextBox(Model.Type + "_GenericCommunicationSomatization", data.AnswerOrEmptyString("GenericCommunicationSomatization"), new { @id = Model.Type + "_GenericCommunicationSomatization", @class = "float-right" })%>
</div>
<div class="clear"></div>
<label class="float-left">Ventilates Feelings:</label>
<div class="float-right">
    <%  var communication2 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Good", Value = "Good" },
            new SelectListItem { Text = "Fair", Value = "Fair" },
            new SelectListItem { Text = "Poor", Value = "Poor" },
        }, "Value", "Text", data.AnswerOrDefault("GenericCommunicationVentilatesFeelings", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericCommunicationVentilatesFeelings", communication2, new { @id = Model.Type + "_GenericCommunicationVentilatesFeelings", @class = "oe" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericCommunicationComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericCommunicationComment", data.AnswerOrEmptyString("GenericCommunicationComment"), new { @id = Model.Type + "_GenericCommunicationComment", @class = "fill" })%></div>
