﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_MotorActivityContainer">
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity1">Increased Amount:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity1", data.AnswerOrEmptyString("GenericMotorActivity1"), new { @id = Model.Type + "_GenericMotorActivity1", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity2">Decreased Amount:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity2", data.AnswerOrEmptyString("GenericMotorActivity2"), new { @id = Model.Type + "_GenericMotorActivity2", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity3">Agitation:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity3", data.AnswerOrEmptyString("GenericMotorActivity3"), new { @id = Model.Type + "_GenericMotorActivity3", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity4">Tics:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity4", data.AnswerOrEmptyString("GenericMotorActivity4"), new { @id = Model.Type + "_GenericMotorActivity4", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity5">Tremor:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity5", data.AnswerOrEmptyString("GenericMotorActivity5"), new { @id = Model.Type + "_GenericMotorActivity5", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity6">Peculiar Posturing:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity6", data.AnswerOrEmptyString("GenericMotorActivity6"), new { @id = Model.Type + "_GenericMotorActivity6", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity7">Unusual Gait:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity7", data.AnswerOrEmptyString("GenericMotorActivity7"), new { @id = Model.Type + "_GenericMotorActivity7", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMotorActivity8">Repetitive Acts:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity8", data.AnswerOrEmptyString("GenericMotorActivity8"), new { @id = Model.Type + "_GenericMotorActivity8", @class = "float-right sensory" })%>
    </div>
    <label for="<%= Model.Type %>_GenericMoodAffectAssessmentComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericMotorActivityComment", data.AnswerOrEmptyString("GenericMotorActivityComment"), new { @id = Model.Type + "_GenericMotorActivityComment", @class = "fill" })%></div>
</div>