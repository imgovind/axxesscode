﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Status:</label>
<div class="float-right">
    <%  var nutritionStatus = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Regulated", Value = "Regulated" },
            new SelectListItem { Text = "Irregular", Value = "Irregular" },
        }, "Value", "Text", data.AnswerOrDefault("GenericGIBowelFunctions1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericGIBowelFunctions1", nutritionStatus, new { @id = Model.Type + "_GenericGIBowelFunctions1", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Cathartic Required:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='1' name='{0}_GenericGIBowelFunctionsCatharticRequired' id='{0}_GenericGIBowelFunctionsCatharticRequired1' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericGIBowelFunctionsCatharticRequired1">Yes</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='0' name='{0}_GenericGIBowelFunctionsCatharticRequired' id='{0}_GenericGIBowelFunctionsCatharticRequired0' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("0").ToChecked())%>
                <label for="<%= Model.Type %>_GenericGIBowelFunctionsCatharticRequired0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericGIBowelFunctionsComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericGIBowelFunctionsComment", data.AnswerOrEmptyString("GenericGIBowelFunctionsComment"), new { @id = Model.Type + "_GenericGIBowelFunctionsComment", @class = "fill" })%></div>
