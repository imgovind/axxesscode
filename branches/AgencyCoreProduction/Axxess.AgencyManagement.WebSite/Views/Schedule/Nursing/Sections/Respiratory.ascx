﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label for="<%= Model.Type %>_GenericRespiratoryBreathSounds" class="float-left">Breath Sounds:</label>
<div class="float-right align-right">
    <%  var breathSounds = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Clear/WNL (Within Normal Limits)", Value = "Clear/WNL" },
            new SelectListItem { Text = "Rales/Crackles", Value = "Rales/Crackles" },
            new SelectListItem { Text = "Rhonchi/Wheezing", Value = "Rhonchi/Wheezing" },
            new SelectListItem { Text = "Diminished/Absent", Value = "Diminished/Absent" },
            new SelectListItem { Text = "Stridor", Value = "Stridor" }
        }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryBreathSounds", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRespiratoryBreathSounds", breathSounds, new { @id = Model.Type + "_GenericRespiratoryBreathSounds", @class = "oe" }) %>
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition0' type='checkbox' value='0' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("0").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition0">Bilateral</label>
            </div>
        </li><li>
            <div class="option">
                <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition1' type='checkbox' value='1' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition1">Left</label>
            </div>
        </li><li>
            <div class="option">
                <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition2' type='checkbox' value='2' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition2">Right</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericRespiratoryDyspnea" class="float-left">Dyspnea:</label>
<div class="float-right">
    <%  var dyspnea = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "N/A", Value = "N/A" },
            new SelectListItem { Text = "On Exertion", Value = "On Exertion" },
            new SelectListItem { Text = "Orthopnea", Value = "Orthopnea" },
            new SelectListItem { Text = "At Rest", Value = "At Rest" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryDyspnea", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRespiratoryDyspnea", dyspnea, new { @id = Model.Type + "_GenericRespiratoryDyspnea", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericCoughList" class="float-left">Cough:</label>
<div class="float-right">
    <%  var cough = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "N/A", Value = "N/A" },
            new SelectListItem { Text = "Non-Productive", Value = "Non-Productive" },
            new SelectListItem { Text = "Productive", Value = "Productive" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericCoughList", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericCoughList", cough, new { @id = Model.Type + "_GenericCoughList", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_Generic02AtText" class="float-left">O<sub>2</sub> at:</label>
<div class="float-right"><%= Html.TextBox(Model.Type + "_Generic02AtText", data.AnswerOrEmptyString("Generic02AtText"), new { @class = "vitals", @id = Model.Type + "_Generic02AtText" })%></div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericLPMVia" class="float-left">LPM via:</label>
<div class="float-right">
    <% var via = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Nasal Cannula", Value = "Nasal Cannula" },
        new SelectListItem { Text = "Mask", Value = "Mask" },
        new SelectListItem { Text = "Trach", Value = "Trach" },
        new SelectListItem { Text = "Other", Value = "Other" }
    }, "Value", "Text", data.AnswerOrDefault("GenericLPMVia", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericLPMVia", via, new { @id = Model.Type + "_GenericLPMVia", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericRespiratoryFreq" class="float-left">Freq:</label>
<div class="float-right">
    <%  var respiratoryFreq = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Intermittent", Value = "Intermittent" },
            new SelectListItem { Text = "Continuous", Value = "Continuous" },
            new SelectListItem { Text = "PRN", Value = "PRN" }
        }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryFreq", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRespiratoryFreq", respiratoryFreq, new { @id = Model.Type + "_GenericRespiratoryFreq", @class = "oe" }) %>
</div>
<div class="clear"></div>
<ul class="checkgroup">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_RespiratoryO2Precautions' type='checkbox' value='false' name='{0}_RespiratoryO2Precautions' {1} />", Model.Type, data.AnswerOrEmptyString("RespiratoryO2Precautions").Equals("false").ToChecked()) %>
            <label for="<%= Model.Type %>_RespiratoryO2Precautions">O<sub>2</sub> Precautions</label>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericRespiratoryComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericRespiratoryComment", data.AnswerOrEmptyString("GenericRespiratoryComment"), new { @id = Model.Type + "_GenericRespiratoryComment", @class = "fill" })%></div>
<script type="text/javascript">
    U.ChangeToRadio("<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition");
</script>