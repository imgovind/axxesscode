﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions1 = new List<string>() { "Angry Outbursts", "Irritable", "Impulsive", "Hostile", "Silly", "Sensitive", "Apathetic", "Withdrawn", "Evasive", "Passive" }; %>
<% var questions2 = new List<string>() { "Aggressive", "Naive", "Overly Dramatic", "Manipulative", "Dependent", "Uncooperative", "Demanding", "Negativistic", "Callous", "Mood Swings" }; %>

<div id="<%= Model.Type %>_InterviewBehaviorContainer">
    <% int count = 1; %>
    <div class="float-left half">
        <% foreach(string question in questions1) { %>
        <div>
            <label class="float-left" for="<%= Model.Type %>_GenericInterviewBehavior<%= count %>"><%= question %>:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericInterviewBehavior" + count, data.AnswerOrEmptyString("GenericInterviewBehavior" + count), new { @id = Model.Type + "_GenericInterviewBehavior" + count, @class = "float-right sensory" })%>
            <% count++; %>
        </div>
        <div class="clear"></div>
        <% } %>
    </div>
    <div class="float-right half">
        <% foreach(string question in questions2) { %>
        <div>
            <label class="float-left" for="<%= Model.Type %>_GenericInterviewBehavior<%= count %>"><%= question %>:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericInterviewBehavior" + count, data.AnswerOrEmptyString("GenericInterviewBehavior" + count), new { @id = Model.Type + "_GenericInterviewBehavior" + count, @class = "float-right sensory" })%>
            <% count++; %>
        </div>
        <div class="clear"></div>
        <% } %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericInterviewBehaviorComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericInterviewBehaviorComment", data.AnswerOrEmptyString("GenericInterviewBehaviorComment"), new { @id = Model.Type + "_GenericInterviewBehaviorComment", @class = "fill" })%></div>
</div>