﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_AppearanceContainer">
    <label class="bold">Facial Expressions</label>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceFacial1">Sad:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial1", data.AnswerOrEmptyString("GenericAppearanceFacial1"), new { @id = Model.Type + "_GenericAppearanceFacial1", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceFacial2">Expressionless:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial2", data.AnswerOrEmptyString("GenericAppearanceFacial2"), new { @id = Model.Type + "_GenericAppearanceFacial2", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceFacial3">Hostile:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial3", data.AnswerOrEmptyString("GenericAppearanceFacial3"), new { @id = Model.Type + "_GenericAppearanceFacial3", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceFacial4">Worried:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial4", data.AnswerOrEmptyString("GenericAppearanceFacial4"), new { @id = Model.Type + "_GenericAppearanceFacial4", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceFacial5">Avoids Gaze:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial5", data.AnswerOrEmptyString("GenericAppearanceFacial5"), new { @id = Model.Type + "_GenericAppearanceFacial5", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <label class="bold">Dress</label>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceDress1">Meticulous:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress1", data.AnswerOrEmptyString("GenericAppearanceDress1"), new { @id = Model.Type + "_GenericAppearanceDress1", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceDress2">Clothing, Hygiene Poor:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress2", data.AnswerOrEmptyString("GenericAppearanceDress2"), new { @id = Model.Type + "_GenericAppearanceDress2", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceDress3">Eccentric:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress3", data.AnswerOrEmptyString("GenericAppearanceDress3"), new { @id = Model.Type + "_GenericAppearanceDress3", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceDress4">Seductive:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress4", data.AnswerOrEmptyString("GenericAppearanceDress4"), new { @id = Model.Type + "_GenericAppearanceDress4", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericAppearanceDress5">Exposed:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress5", data.AnswerOrEmptyString("GenericAppearanceDress5"), new { @id = Model.Type + "_GenericAppearanceDress5", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericAppearanceComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericAppearanceComment", data.AnswerOrEmptyString("GenericAppearanceComment"), new { @id = Model.Type + "_GenericAppearanceComment", @class = "fill" })%></div>
</div>