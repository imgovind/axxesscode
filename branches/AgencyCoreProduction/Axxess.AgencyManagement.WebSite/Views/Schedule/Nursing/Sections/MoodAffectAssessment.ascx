﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Anxious", "Inappropriate Affect", "Flat Affect", "Elevated Mood", "Depressed Mood", "Labile Mood" }; %>
<div id="<%= Model.Type %>_MoodAffectAssessmentContainer">
    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericMoodAffectAssessment<%= count %>"><%= question %>:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericMoodAffectAssessment" + count, data.AnswerOrEmptyString("GenericMoodAffectAssessment" + count), new { @id = Model.Type + "_GenericMoodAffectAssessment" + count, @class = "float-right sensory" })%>
        <% count++; %>
    </div>
    <div class="clear"></div>
    <% } %>
    <label for="<%= Model.Type %>_GenericMoodAffectAssessmentComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericMoodAffectAssessmentComment", data.AnswerOrEmptyString("GenericMoodAffectAssessmentComment"), new { @id = Model.Type + "_GenericMoodAffectAssessmentComment", @class = "fill" })%></div>
</div>