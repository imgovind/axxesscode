﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] teachings = data.AnswerArray("GenericPatientFamilyTeachings"); %>
<%  string[] teachings2 = data.AnswerArray("GenericPatientFamilyTeachingsNutrition"); %>
<%  string[] teachings3 = data.AnswerArray("GenericPatientFamilyTeachingsTherapyProvided"); %>
<%= Html.Hidden(Model.Type + "_GenericPatientFamilyTeachings", string.Empty, new { @id = Model.Type + "_GenericPatientFamilyTeachingsHidden" })%>
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings1' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='1' type='checkbox' {1} />", Model.Type, teachings.Contains("1").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings1" class="radio">Medication Regime</label>
</div>
<div class="clear"></div>   
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings2' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='2' type='checkbox' {1} />", Model.Type, teachings.Contains("2").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings2" class="float-left inline-radio light">Action/Side Effects</label>
    <%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings2Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings2Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings2Text", @class = "float-right" })%>
</div>
<div class="clear"></div>   
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings3' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='3' type='checkbox' {1} />", Model.Type, teachings.Contains("3").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings3" class="float-left inline-radio light">S/S Disease Process</label>
    <%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings3Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings3Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings3Text", @class = "float-right" })%>
</div>
<div class="clear"></div>   
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings4' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='4' type='checkbox' {1} />", Model.Type, teachings.Contains("4").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings4" class="float-left inline-radio light">S/S of Complications</label>
    <%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings4Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings4Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings4Text", @class = "float-right" })%>
</div>
<div class="clear"></div>   
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings5' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='5' type='checkbox' {1} />", Model.Type, teachings.Contains("5").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings5" class="radio">Extrapyramidal Symptoms</label>
</div>
<div class="clear"></div>  
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings6' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='6' type='checkbox' {1} />", Model.Type, teachings.Contains("6").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings6" class="radio">Safety Measures</label>
</div>
<div class="clear"></div>  
<div class="align-left">
    <%= string.Format("<input id='{0}_GenericPatientFamilyTeachings7' class='float-left radio' name='{0}_GenericPatientFamilyTeachings' value='7' type='checkbox' {1} />", Model.Type, teachings.Contains("7").ToChecked())%>
    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings7" class="radio">Relaxation Techniques</label>
</div>
<div class="clear"></div>
<div>
    <label class="bold">Nutrition</label>
    <div>
        <%= string.Format("<input id='{0}_GenericPatientFamilyTeachingsNutrition1' class='float-left radio' name='{0}_GenericPatientFamilyTeachingsNutrition' value='1' type='checkbox' {1} />", Model.Type, teachings2.Contains("1").ToChecked())%>
        <label class="float-left normal" for="<%= Model.Type %>_GenericPatientFamilyTeachingsNutrition1">Diet:</label>
        <%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachingsNutritionDiet", data.AnswerOrEmptyString("GenericPatientFamilyTeachingsNutritionDiet"), new { @id = Model.Type + "_GenericPatientFamilyTeachingsNutritionDiet", @class = "float-right" })%>
    </div>
    <div class="clear"></div>
    <div class="align-left">
        <%= string.Format("<input id='{0}_GenericPatientFamilyTeachingsNutrition2' class='float-left radio' name='{0}_GenericPatientFamilyTeachingsNutrition' value='2' type='checkbox' {1} />", Model.Type, teachings2.Contains("2").ToChecked())%>
        <label for="<%= Model.Type %>_GenericPatientFamilyTeachingsNutrition2" class="radio">Proper Fluid Intake</label>
    </div>
</div>
<div class="clear"></div>
<div>
    <label class="bold">Therapy Provided</label>
    <div class="align-left">
        <%= string.Format("<input id='{0}_GenericPatientFamilyTeachingsTherapyProvided1' class='float-left radio' name='{0}_GenericPatientFamilyTeachingsTherapyProvided' value='1' type='checkbox' {1} />", Model.Type, teachings3.Contains("1").ToChecked())%>
        <label for="<%= Model.Type %>_GenericPatientFamilyTeachings1" class="radio">Supportive</label>
    </div>
    <div class="clear"></div>
    <div class="align-left">
        <%= string.Format("<input id='{0}_GenericPatientFamilyTeachingsTherapyProvided2' class='float-left radio' name='{0}_GenericPatientFamilyTeachingsTherapyProvided' value='2' type='checkbox' {1} />", Model.Type, teachings3.Contains("2").ToChecked())%>
        <label for="<%= Model.Type %>_GenericPatientFamilyTeachingsTherapyProvided2" class="radio">Reality</label>
    </div>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericPatientFamilyTeachingsComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericPatientFamilyTeachingsComment", data.AnswerOrEmptyString("GenericPatientFamilyTeachingsComment"), new { @id = Model.Type + "_GenericPatientFamilyTeachingsComment", @class = "fill" })%></div>

<script type="text/javascript">
    U.ShowIfChecked(
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings2"),
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings2Text"));
    U.ShowIfChecked(
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings3"),
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings3Text"));
    U.ShowIfChecked(
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings4"),
        $("#<%= Model.Type %>_GenericPatientFamilyTeachings4Text"));
    U.ShowIfChecked(
        $("#<%= Model.Type %>_GenericPatientFamilyTeachingsNutrition1"),
        $("#<%= Model.Type %>_GenericPatientFamilyTeachingsNutritionDiet"));
</script>