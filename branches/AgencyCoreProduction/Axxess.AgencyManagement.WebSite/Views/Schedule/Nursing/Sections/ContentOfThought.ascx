﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Suicidal Thoughts", "Suicidal Plans", "Assaultive Ideas", "Homicidal Thoughts",
       "Homicidal Plans", "Antisocial Attitudes", "Suspiciousness", "Poverty of Content", "Phobias", "Obsessions",
       "Compulsions", "Feelings of Unreality", "Feels Persecuted", "Thoughts of Running Away", "Somatic Complaints",
       "Ideas of Guilt", "Ideas of Hopelessness" }; %>
<% var questions2 = new List<string>() { "Ideas of Worthlessness", "Excessive Religiosity", "Sexual Preoccupation", "Blames Others" }; %>
<div id="<%= Model.Type %>_ContentOfThoughtContainer">
    <% int count = 1; %>
    <div class="float-left half">
        <% foreach(string question in questions) { %>
        <div>
            <label class="float-left" for="<%= Model.Type %>_GenericContentOfThought<%= count %>"><%= question %>:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThought" + count, data.AnswerOrEmptyString("GenericContentOfThought" + count), new { @id = Model.Type + "_GenericContentOfThought" + count, @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <% count++; %>
        <% } %>
    </div>
    <div class="float-left half">
        <% foreach(string question in questions2) { %>
        <div>
            <label class="float-left" for="<%= Model.Type %>_GenericContentOfThought<%= count %>"><%= question %>:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThought" + count, data.AnswerOrEmptyString("GenericContentOfThought" + count), new { @id = Model.Type + "_GenericContentOfThought" + count, @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <% count++; %>
        <% } %>
        <label class="bold">Illusions</label>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtIllusions1">Present:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtIllusions1", data.AnswerOrEmptyString("GenericContentOfThoughtIllusions1"), new { @id = Model.Type + "_GenericContentOfThoughtIllusions1", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <label class="bold">Hallucinations</label>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations1">Auditory:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations1", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations1"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations1", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations2">Visual:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations2", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations2"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations2", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations3">Other:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations3", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations3"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations3", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <label class="bold">Delusions</label>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions1">Of Persecution:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions1", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions1"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions1", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions2">Of Grandeur:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions2", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions2"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions2", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions3">Of Reference:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions3", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions3"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions3", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions4">Of Influence:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions4", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions4"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions4", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions5">Somatic:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions5", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions5"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions5", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions6">Are Systematized:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions6", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions6"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions6", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
        <div>
            <label class="float-left normal" for="<%= Model.Type %>_GenericContentOfThoughtDelusions7">Other:</label>
            <%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions7", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions7"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions7", @class = "float-right sensory" })%>
        </div>
        <div class="clear"></div>
    </div>
     <label for="<%= Model.Type %>_GenericContentOfThoughtComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericContentOfThoughtComment", data.AnswerOrEmptyString("GenericContentOfThoughtComment"), new { @id = Model.Type + "_GenericContentOfThoughtComment", @class = "fill" })%></div>
</div>