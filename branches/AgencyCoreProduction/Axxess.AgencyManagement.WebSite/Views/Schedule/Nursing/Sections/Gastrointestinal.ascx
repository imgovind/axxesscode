﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
<%= Html.Hidden(Model.Type + "_GenericDigestive", string.Empty, new { @id = Model.Type + "_GenericDigestiveHidden" })%>
<ul class="checkgroup bold">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive1' name='{0}_GenericDigestive' value='1' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive1">WNL (Within Normal Limits)</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive2' name='{0}_GenericDigestive' value='2' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive2">Bowel Sounds:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestive2More">
            <%  var bowelSounds = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "Present/WNL (Within Normal Limits) x4 quadrants", Value = "1" },
                new SelectListItem { Text = "Hyperactive", Value = "2" },
                new SelectListItem { Text = "Hypoactive", Value = "3" },
                new SelectListItem { Text = "Absent", Value = "4" }
            }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveBowelSoundsType", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = Model.Type + "_GenericDigestiveBowelSoundsType", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive3' name='{0}_GenericDigestive' value='3' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive3">Abdominal Palpation:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestive3More">
            <%  var abdominalPalpation = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "Soft/WNL (Within Normal Limits)", Value = "1" },
                new SelectListItem { Text = "Firm", Value = "2" },
                new SelectListItem { Text = "Tender", Value = "3" },
                new SelectListItem { Text = "Other", Value = "4" }
            }, "Value", "Text", data.AnswerOrDefault("GenericAbdominalPalpation", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericAbdominalPalpation", abdominalPalpation, new { @id = Model.Type + "_GenericAbdominalPalpation", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive4' name='{0}_GenericDigestive' value='4' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive4">Bowel Incontinence</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive5' name='{0}_GenericDigestive' value='5' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive5">Nausea</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive6' name='{0}_GenericDigestive' value='6' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive6">Vomiting</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive7' name='{0}_GenericDigestive' value='7' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive7">GERD</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive8' name='{0}_GenericDigestive' value='8' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("8").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive8">Abd Girth:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestive8More">
            <%= Html.TextBox(Model.Type + "_GenericDigestiveAbdGirthLength", data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength"), new { @id = Model.Type + "_GenericDigestiveAbdGirthLength", @class = "oe", @maxlength = "5" }) %>
        </div>
    </li>
</ul>
<div class="clear"></div>
<div class="strong">Elimination:</div>
<ul class="checkgroup">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive11' name='{0}_GenericDigestive' value='11' class='check-omit' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("11").ToChecked())%>
            <label for="<%= Model.Type %>_GenericDigestive11">Last BM:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestive11More">
            <label for="<%= Model.Type %>_GenericDigestiveLastBMDate">Date:</label>
            <%= Html.TextBox(Model.Type + "_GenericDigestiveLastBMDate", data.AnswerOrEmptyString("GenericDigestiveLastBMDate"), new { @id = Model.Type + "_GenericDigestiveLastBMDate", @class = "date-picker", @maxlength = "10" })%>
            <%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
            <%= Html.Hidden(Model.Type + "_GenericDigestiveLastBM", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMHidden" })%>
            <ul class="checkgroup">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBM1' name='{0}_GenericDigestiveLastBM' value='1' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBM1">WNL (Within Normal Limits)</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBM2' name='{0}_GenericDigestiveLastBM' value='2' class='check-omit' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBM2">Abnormal Stool:</label>
                    </div>
                    <div class="extra" id="<%= Model.Type %>_GenericDigestiveLastBM2More">
                        <%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
                        <%= Html.Hidden(Model.Type + "_GenericDigestiveLastBMAbnormalStool", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMAbnormalStoolHidden" })%>
                        <ul class="checkgroup">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool1' name='{0}_GenericDigestiveLastBMAbnormalStool' value='1' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("1").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool1">Gray</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool2' name='{0}_GenericDigestiveLastBMAbnormalStool' value='2' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("2").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool2">Tarry</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool4' name='{0}_GenericDigestiveLastBMAbnormalStool' value='4' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("4").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool4">Black</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool3' name='{0}_GenericDigestiveLastBMAbnormalStool' value='3' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("3").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool3">Fresh Blood</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBM3' name='{0}_GenericDigestiveLastBM' value='3' class='check-omit' type='checkbox' {1} />", Model.Type, genericDigestiveLastBM.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBM3">Constipation:</label>
                    </div>
                    <div class="extra" id="<%= Model.Type %>_GenericDigestiveLastBM3More">
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeChronic' name='{0}_GenericDigestiveLastBMConstipationType' value='Chronic' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeChronic">Chronic</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeAcute' name='{0}_GenericDigestiveLastBMConstipationType' value='Acute' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeAcute">Acute</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeOccasional' name='{0}_GenericDigestiveLastBMConstipationType' value='Occasional' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeOccasional">Occasional</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBM4' name='{0}_GenericDigestiveLastBM' value='4' class='check-omit' type='checkbox' {1} />", Model.Type, genericDigestiveLastBM.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBM4">Diarrhea:</label>
                    </div>
                    <div class="extra" id="<%= Model.Type %>_GenericDigestiveLastBM4More">
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeChronic' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Chronic' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeChronic">Chronic</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeAcute' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Acute' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeAcute">Acute</label>
                                </div>
                            </li><li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeOccasional' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Occasional' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional").ToChecked()) %>
                                    <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeOccasional">Occasional</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>
<div class="clear"></div>
<div class="strong">Ostomy:</div>
<%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
<%= Html.Hidden(Model.Type + "_GenericDigestiveOstomy", string.Empty, new { @id = Model.Type + "_GenericDigestiveOstomyHidden" })%>
<ul class="checkgroup">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy1' name='{0}_GenericDigestiveOstomy' value='1' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy1">Ostomy Type:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestiveOstomy1More">
            <%  var ostomy = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "0" },
                new SelectListItem { Text = "N/A", Value = "1" },
                new SelectListItem { Text = "Ileostomy ", Value = "2" },
                new SelectListItem { Text = "Colostomy", Value = "3" },
                new SelectListItem { Text = "Other", Value = "4" }
            }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveOstomyType", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericDigestiveOstomyType", ostomy, new { @id = Model.Type + "_GenericDigestiveOstomyType", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy2' name='{0}_GenericDigestiveOstomy' value='2' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy2">Stoma Appearance:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestiveOstomy2More">
            <%= Html.TextBox(Model.Type + "_GenericDigestiveStomaAppearance", data.AnswerOrEmptyString("GenericDigestiveStomaAppearance"), new { @id = Model.Type + "_GenericDigestiveStomaAppearance", @class = "oe" }) %>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy3' name='{0}_GenericDigestiveOstomy' value='3' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy3">Surrounding Skin:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericDigestiveOstomy3More">
            <%= Html.TextBox(Model.Type + "_GenericDigestiveSurSkinType", data.AnswerOrEmptyString("GenericDigestiveSurSkinType"), new { @id = Model.Type + "_GenericDigestiveSurSkinType", @class = "oe" }) %>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericGastrointestinalComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericGastrointestinalComment", data.AnswerOrEmptyString("GenericGastrointestinalComment"), new { @class = "fill", @id = Model.Type + "_GenericGastrointestinalComment" })%></div>
<script type="text/javascript">
    U.ChangeToRadio("<%= Model.Type %>_GenericDigestiveLastBMConstipationType");
    U.ChangeToRadio("<%= Model.Type %>_GenericDigestiveLastBMDiarrheaType");

    $('.check-omit').each(function() {
        $(this).click(function() {
            if (!$(this).is(':checked')) {
                $(this).closest('li').find('.extra').find(':checkbox').attr('checked', false);
            }
        });
    });
</script>
