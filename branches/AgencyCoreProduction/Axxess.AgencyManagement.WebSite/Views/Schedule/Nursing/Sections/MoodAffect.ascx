﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Overall Mood Status:</label>
<div class="float-right">
    <%  var moodAffect = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericMoodAffect1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericMoodAffect1", moodAffect, new { @id = Model.Type + "_GenericMoodAffect1", @class = "oe" })%>
</div>
<div class="clear"></div>
<%  string[] genericMoodAffect2 = data.AnswerArray("GenericMoodAffect2"); %>
<table class="fixed">
    <tbody>
        <tr>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='1' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectFlat' {1}>", Model.Type, genericMoodAffect2.Contains("1").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectFlat">Flat</label>
                </div>
            </td>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='2' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectDepressed' {1}>", Model.Type, genericMoodAffect2.Contains("2").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectDepressed">Depressed</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='3' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectCombative' {1}>", Model.Type, genericMoodAffect2.Contains("3").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectCombative">Combative</label>
                </div>
            </td>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='4' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectAgitated' {1}>", Model.Type, genericMoodAffect2.Contains("4").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectAgitated">Agitated</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='5' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectAnxious' {1}>", Model.Type, genericMoodAffect2.Contains("5").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectAnxious">Anxious</label>
                </div>
            </td>
            <td>
                <div class="align-left option">
                    <%= string.Format("<input type='checkbox' class='radio align-left' value='6' name='{0}_GenericMoodAffect2' id='{0}_GenericMoodAffectNegative' {1}>", Model.Type, genericMoodAffect2.Contains("6").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericMoodAffectNegative">Negative</label>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericMoodAffectComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericMoodAffectComment", data.AnswerOrEmptyString("GenericMoodAffectComment"), new { @id = Model.Type + "_GenericMoodAffectComment", @class = "fill" })%></div>
