﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Excessive Amount", "Reduced Amount", "Speech", "Slowed", "Loud", "Soft", "Mute", "Slurred", "Stuttering" }; %>
<div id="<%= Model.Type %>_SpeechContainer">
    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericSpeech<%= count %>"><%= question %>:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSpeech" + count, data.AnswerOrEmptyString("GenericSpeech" + count), new { @id = Model.Type + "_GenericSpeech" + count, @class = "float-right sensory" })%>
        <% count++; %>
    </div>
    <div class="clear"></div>
    <% } %>
    <label for="<%= Model.Type %>_GenericSpeechComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericSpeechComment", data.AnswerOrEmptyString("GenericSpeechComment"), new { @id = Model.Type + "_GenericSpeechComment", @class = "fill" })%></div>
</div>