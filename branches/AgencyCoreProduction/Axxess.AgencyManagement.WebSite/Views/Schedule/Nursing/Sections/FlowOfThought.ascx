﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var questions = new List<string>() { "Blocking", "Circumstantial", "Tangential", "Perseveration", "Flight of Ideas", "Loose Association", "Indecisive" }; %>
<div id="<%= Model.Type %>_FlowOfThoughtContainer">
    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    <div>
        <label class="float-left" for="<%= Model.Type %>_GenericFlowOfThought<%= count %>"><%= question %>:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericFlowOfThought" + count, data.AnswerOrEmptyString("GenericFlowOfThought" + count), new { @id = Model.Type + "_GenericFlowOfThought" + count, @class = "float-right sensory" })%>
        <% count++; %>
    </div>
    <div class="clear"></div>
    <% } %>
    <label for="<%= Model.Type %>_GenericFlowOfThoughtComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericFlowOfThoughtComment", data.AnswerOrEmptyString("GenericFlowOfThoughtComment"), new { @id = Model.Type + "_GenericFlowOfThoughtComment", @class = "fill" })%></div>
</div>