﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  if (Model.Type == "SkilledNurseVisit" || Model.Type=="SNFoleyChange")
    { %>
<%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
<div class="buttons">
    <ul>
        <li><%= Model.CarePlanOrEvalUrl %></li>
    </ul>
</div><%
} %>
<div class="half">
    <div class="half align-left bold">Primary Diagnosis:</div>
    <%  var primaryDiagnosis = data.AnswerOrEmptyString("PrimaryDiagnosis"); %>
    <div class="half align-left"><%= primaryDiagnosis.Trim() %><%  var primaryICD9Code = data.AnswerOrEmptyString("ICD9M"); %>
    <%= primaryICD9Code.IsNotNullOrEmpty() ? string.Format(" <a class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>", primaryICD9Code) : string.Empty%></div>
</div>
<div class="clear"></div>
<div class="half">
    <div class="half align-left bold">Secondary Diagnosis:</div>
    <%  var secondaryDiagnosis = data.AnswerOrEmptyString("PrimaryDiagnosis1"); %>
    <div class="half align-left"><%= secondaryDiagnosis %><%  var secondaryICD9Code = data.AnswerOrEmptyString("ICD9M1"); %>
    <%= secondaryICD9Code.IsNotNullOrEmpty() ? string.Format(" <a class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>", secondaryICD9Code) : string.Empty%></div>
</div>
<div class="clear"></div>
<%  } %>
<div class="align-left">
    <label class="strong">Narrative</label>
    <div class="template-text align-center">
     <%-- <%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates", "", "#" + Model.Type + "_GenericNarrativeComment") %>--%>
     <%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates", string.Empty, "#" + Model.Type + "_GenericNarrativeComment")%>
     <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"),8,20, new { @class = "fill", @id = Model.Type + "_GenericNarrativeComment" })%></div>
    
</div>
<%  if (Model.Type == "SNDiabeticDailyVisit") { %>
<div class="third">
    <label class="float-left">Tolerated Cares:</label>
    <div class="float-right">
        <ul class="checkgroup inline">
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_ToleratedCares", "1", data.AnswerOrEmptyString("ToleratedCares").Equals("1"), new { @id = Model.Type + "_ToleratedCares1" })%>
                    <label for="<%= Model.Type %>_ToleratedCares1">Yes</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_ToleratedCares", "0", data.AnswerOrEmptyString("ToleratedCares").Equals("0"), new { @id = Model.Type + "_ToleratedCares0" })%>
                    <label for="<%= Model.Type %>_ToleratedCares0">No</label>
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="float-right align-left">
        <label for="<%= Model.Type %>_ToleratedCaresDescribe">Describe:</label><br />
        <%= Html.TextArea(Model.Type + "_ToleratedCaresDescribe", data.AnswerOrEmptyString("ToleratedCaresDescribe"), 3, 30, new { @id = Model.Type + "_ToleratedCaresDescribe" })%>
    </div>
</div>
<div class="third">
    <label class="float-left">Patient Goals Met this Visit:</label>
    <div class="float-right">
        <ul class="checkgroup inline">
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_GoalsMet", "1", data.AnswerOrEmptyString("GoalsMet").Equals("1"), new { @id = Model.Type + "_GoalsMet1" })%>
                    <label for="<%= Model.Type %>_GoalsMet1">Yes</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_GoalsMet", "0", data.AnswerOrEmptyString("GoalsMet").Equals("0"), new { @id = Model.Type + "_GoalsMet0" })%>
                    <label for="<%= Model.Type %>_GoalsMet0">No</label>
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="float-right align-left">
        <label for="<%= Model.Type %>_GoalsMetSpecify">Specify:</label><br />
        <%= Html.TextArea(Model.Type + "_GoalsMetSpecify", data.AnswerOrEmptyString("GoalsMetSpecify") , 3, 30, new { @id = Model.Type + "_GoalsMetSpecify" })%>
    </div>
</div>
<div class="third">
    <label class="float-left">Care Plan Revised:</label>
    <div class="float-right">
        <ul class="checkgroup inline">
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "1", data.AnswerOrEmptyString("RevisedCarePlan").Equals("1"), new { @id = Model.Type + "_RevisedCarePlan1" })%>
                    <label for="<%= Model.Type %>_RevisedCarePlan1">Yes</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "0", data.AnswerOrEmptyString("RevisedCarePlan").Equals("0"), new { @id = Model.Type + "_RevisedCarePlan0" })%>
                    <label for="<%= Model.Type %>_RevisedCarePlan0">No</label>
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="float-right align-left">
        <label for="<%= Model.Type %>_RevisedCarePlanSpecify">Specify:</label><br />
        <%= Html.TextArea(Model.Type + "_RevisedCarePlanSpecify", data.AnswerOrEmptyString("RevisedCarePlanSpecify"), 3, 30, new { @id = Model.Type + "_RevisedCarePlanSpecify" })%>
    </div>
</div>
<div class="clear"></div>
<div class="align-left">
    <label class="strong">Patient Response:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericPatientResponse", data.AnswerOrEmptyString("GenericPatientResponse"), 3, 20, new { @class = "fill", @id = Model.Type + "_GenericPatientResponse" })%></div>
</div>
<%  } %>