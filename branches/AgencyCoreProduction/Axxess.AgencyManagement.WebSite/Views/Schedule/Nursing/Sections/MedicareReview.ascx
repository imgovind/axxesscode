﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>

<label class="float-left">Was poc sent with patient?</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsPOCSentWithPatient", "1", data.AnswerOrEmptyString("GenericIsPOCSentWithPatient").Equals("1"), new { @id = Model.Type + "_GenericIsPOCSentWithPatient1" })%>
                <label for="<%= Model.Type %>_GenericIsPOCSentWithPatient1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsPOCSentWithPatient", "0", data.AnswerOrEmptyString("GenericIsPOCSentWithPatient").Equals("0"), new { @id = Model.Type + "_GenericIsPOCSentWithPatient0" })%>
                <label for="<%= Model.Type %>_GenericIsPOCSentWithPatient0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label class="float-left">Was medication sent with patient?</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsMedicationSentWithPatient", "1", data.AnswerOrEmptyString("GenericIsMedicationSentWithPatient").Equals("1"), new { @id = Model.Type + "_GenericIsMedicationSentWithPatient1" })%>
                <label for="<%= Model.Type %>_GenericIsMedicationSentWithPatient1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsMedicationSentWithPatient", "0", data.AnswerOrEmptyString("GenericIsMedicationSentWithPatient").Equals("0"), new { @id = Model.Type + "_GenericIsMedicationSentWithPatient0" })%>
                <label for="<%= Model.Type %>_GenericIsMedicationSentWithPatient0">No</label>
            </div>
        </li>
    </ul>
</div>
