﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_GenericPhlebotomyContainer">
    <label for="<%= Model.Type %>_GenericPhlebotomyLabs" class="float-left">Labs:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPhlebotomyLabs", data.AnswerOrEmptyString("GenericPhlebotomyLabs"), new { @id = Model.Type + "_GenericPhlebotomyLabs" })%></div>
    <div class="clear"></div>
    <label class="float-left">Venipuncture:</label>
    <div class="margin">
        <%= Html.TextBox(Model.Type + "_GenericPhlebotomyVenipuncture", data.AnswerOrEmptyString("GenericPhlebotomyVenipuncture"), new { @class = "vitals", @id = Model.Type + "_GenericPhlebotomyVenipuncture" })%>
        <label>(# of attempts) to</label>
        <%= Html.TextBox(Model.Type + "_GenericPhlebotomySite", data.AnswerOrEmptyString("GenericPhlebotomySite"), new { @class = "oe", @id = Model.Type + "_GenericPhlebotomySite" })%>
        <label>(site) with</label>
        <%= Html.TextBox(Model.Type + "_GenericPhlebotomyGA", data.AnswerOrEmptyString("GenericPhlebotomyGA"), new { @class = "vitals", @id = Model.Type + "_GenericPhlebotomyGA" })%>
        <label>ga. needle/vacutainer using aseptic technique.  Applied pressure for</label>
        <%= Html.TextBox(Model.Type + "_GenericPhlebotomyMinutes", data.AnswerOrEmptyString("GenericPhlebotomyMinutes"), new { @class = "vitals", @id = Model.Type + "_GenericPhlebotomyMinutes" })%>
        <label>minutes.</label>
    </div>
    <div class="clear"></div>
    <%  string[] phlebotomyActivity = data.AnswerArray("GenericPhlebotomyActivity"); %>
    <%= Html.Hidden(Model.Type + "_GenericPhlebotomyActivity", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyActivityHidden" })%>
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericPhlebotomyActivity1' name='{0}_GenericPhlebotomyActivity' value='1' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_phlebotomyActivity1">Pt tolerated procedure</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericPhlebotomyActivity1' name='{0}_GenericPhlebotomyActivity' value='2' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericPhlebotomyActivity2">No bruising</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericPhlebotomyActivity3' name='{0}_GenericPhlebotomyActivity' value='3' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericPhlebotomyActivity3">No bleeding</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericPhlebotomyActivity4' name='{0}_GenericPhlebotomyActivity' value='4' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericPhlebotomyActivity4">Sharps Disposal</label>
            </div>
        </li>
    </ul>
    <div class="clear"></div>
    <%  string[] phlebotomyDeliveredTo = data.AnswerArray("GenericPhlebotomyDeliveredTo"); %>
    <%= Html.Hidden(Model.Type + "_GenericPhlebotomyDeliveredTo", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyDeliveredToHidden" })%>
    <label class="float-left">Delivered to:</label>
    <div class="float-right">
        <ul class="checkgroup inline">
            <li>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo1' name='{0}_GenericPhlebotomyDeliveredTo' value='1' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo1">Lab</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo2' name='{0}_GenericPhlebotomyDeliveredTo' value='2' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo2">Hospital</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo3' name='{0}_GenericPhlebotomyDeliveredTo' value='3' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo3">MD</label>
                </div>
            </li>
            <li>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo4' name='{0}_GenericPhlebotomyDeliveredTo' value='4' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo4">Other</label>
                </div>
                <div class="extra" id="<%= Model.Type %>_GenericPhlebotomyDeliveredTo4More">
                    <%= Html.TextBox(Model.Type + "_GenericPhlebotomyDeliveredToOther", data.AnswerOrEmptyString("GenericPhlebotomyDeliveredToOther"), new { @class = "oe", @id = Model.Type + "_GenericPhlebotomyDeliveredToOther" }) %>
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericPhlebotomyComment" class="strong">Comments:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericPhlebotomyComment", data.AnswerOrEmptyString("GenericPhlebotomyComment"), new { @class = "fill", @id = Model.Type + "_GenericPhlebotomyComment" })%></div>
 </div>
<script type="text/javascript">
    Schedule.PhlebotomyLab("#<%= Model.Type %>_GenericPhlebotomyLabs");
</script>