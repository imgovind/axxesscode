﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isCareCoordinationAndCommunicationApplied = data.AnswerArray("GenericIsCareCoordinationAndCommunicationApplied"); %>
<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
printview.span("Care Coordination with:",true) +
printview.col(3,
    printview.checkbox("SN",<%= genericCareCoordination.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("PT",<%= genericCareCoordination.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("ST",<%= genericCareCoordination.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("MSW",<%= genericCareCoordination.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("HHA",<%= genericCareCoordination.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("MD",<%= genericCareCoordination.Contains("6").ToString().ToLower() %>)) +
printview.checkbox("Other <%=genericCareCoordination.Contains("7")?" "+ data.AnswerOrEmptyString("GenericCareCoordinationOther").Clean():string.Empty %>",<%= genericCareCoordination.Contains("7").ToString().ToLower() %>) +
printview.span("Regarding:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericCareCoordinationRegarding").Clean() %>"),
"Care Coordination"