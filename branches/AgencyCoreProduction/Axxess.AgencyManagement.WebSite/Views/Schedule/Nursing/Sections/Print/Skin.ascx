﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] skinColor = data.AnswerArray("GenericSkinColor"); %>
<%  string[] skinCondition = data.AnswerArray("GenericSkinCondition"); %>
printview.span("Color:",true) +
printview.col(2,
    printview.checkbox("Pink/WNL",<%= skinColor.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Pallor",<%= skinColor.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Jaundice",<%= skinColor.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Cyanotic",<%= skinColor.Contains("4").ToString().ToLower() %>)) +
printview.checkbox("Other <%= data.AnswerOrEmptyString("GenericSkinColorOther").Clean() %>",<%= skinColor.Contains("5").ToString().ToLower() %>) +
printview.col(2,
    printview.span("Temp:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSkinTemp").Clean() %>") +
    printview.span("Turgor:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSkinTurgor").Clean() %>")) +
printview.span("Condition:",true) +
printview.col(3,
    printview.checkbox("Dry",<%= skinCondition.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Diaphoret.",<%= skinCondition.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Wound",<%= skinCondition.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Ulcer",<%= skinCondition.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Incision",<%= skinCondition.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Rash",<%= skinCondition.Contains("6").ToString().ToLower() %>)) +
printview.checkbox("Other <%= data.AnswerOrEmptyString("GenericSkinConditionOther").Clean() %>",<%= skinCondition.Contains("7").ToString().ToLower() %>) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericSkinComment").Clean() %>"),
"Skin"