﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Excessive Amount", "Reduced Amount", "Speech", "Slowed", "Loud", "Soft", "Mute", "Slurred", "Stuttering" }; %>
<% if (data.AnswerOrEmptyString("GenericIsSpeechApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsSpeechApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Speech"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSpeech" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSpeechComment").Clean()%>"),
    "Speech"
<% } %>
