﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] responseToTeachingInterventions = data.AnswerArray("GenericResponseToTeachingInterventions"); %>
printview.checkbox("Verbalize <%= data.AnswerOrDefault("GenericResponseToTeachingInterventionsUVI", "__________").Clean()%> understanding of verbal instructions/teaching given.",<%= responseToTeachingInterventions.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Able to return <%= data.AnswerOrDefault("GenericResponseToTeachingInterventionsCDP", "__________").Clean()%> correct demonstration of procedure/technique instructed on.",<%= responseToTeachingInterventions.Contains("2").ToString().ToLower() %>) +
printview.checkbox("Treatment/Procedure well tolerated by patient.",<%= responseToTeachingInterventions.Contains("3").ToString().ToLower() %>) +
printview.checkbox("Infusion well tolerated by patient.",<%= responseToTeachingInterventions.Contains("4").ToString().ToLower() %>) +
printview.checkbox("Other <%= data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsOther").Clean() %>",<%= responseToTeachingInterventions.Contains("5").ToString().ToLower() %>),
"Response to Teaching/Interventions"