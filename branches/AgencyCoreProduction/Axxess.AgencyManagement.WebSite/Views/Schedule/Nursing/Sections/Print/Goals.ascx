﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] goals = data.AnswerArray("GenericGoals"); %>
<%  string[] isGoalsApplied = data.AnswerArray("GenericIsGoalsApplied"); %>
<% if(data.AnswerOrEmptyString("GenericIsGoalsApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsGoalsApplied").ToString()=="1"){ %>
printview.checkbox("N/A",true),"Goals"
<%}else{ %>
printview.col(2,
    <% if(goals.Contains("1")) { %>
        printview.checkbox("Make daily social contacts as evidenced by: <%= data.AnswerOrDefault("GenericGoals1Weeks", "__________").Clean()%> wks.",<%= goals.Contains("1").ToString().ToLower() %>) +
    <% } %>
    <% if(goals.Contains("2")) { %>
        printview.checkbox("Exhibit stable weight, nutrition, hydration status w/weight gain of <%= data.AnswerOrDefault("GenericGoals2Weight", "__________").Clean()%> lbs by <%= data.AnswerOrDefault("GenericGoals2Weeks", "__________").Clean()%> wks.",<%= goals.Contains("2").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("3")) { %>
        printview.checkbox("Improve interpersonal relationships by <%= data.AnswerOrDefault("GenericGoals3Weeks", "__________").Clean()%> wks.",<%= goals.Contains("3").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("4")) { %>
        printview.checkbox("Demonstrate coping strategies by <%= data.AnswerOrDefault("GenericGoals4Weeks", "__________").Clean()%> wks.",<%= goals.Contains("4").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("5")) { %>
        printview.checkbox("Decrease neurotic behavior by <%= data.AnswerOrDefault("GenericGoals5Weeks", "__________").Clean()%> wks.",<%= goals.Contains("5").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("6")) { %>
        printview.checkbox("Verbalize a decrease in depression by <%= data.AnswerOrDefault("GenericGoals6Weeks", "__________").Clean()%> wks.",<%= goals.Contains("6").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("7")) { %>
        printview.checkbox("Verbalize absense of suicidal ideation, intent and plan by <%= data.AnswerOrDefault("GenericGoals7Weeks", "__________").Clean()%> wks.",<%= goals.Contains("7").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("8")) { %>
        printview.checkbox("Will not harm self as evidenced by: <%= data.AnswerOrDefault("GenericGoals8Weeks", "__________").Clean()%> wks.",<%= goals.Contains("8").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("9")) { %>
        printview.checkbox("Verbalize absense of violent ideation by <%= data.AnswerOrDefault("GenericGoals9Weeks", "__________").Clean()%> wks.",<%= goals.Contains("9").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("10")) { %>
        printview.checkbox("Verbalize s/sx suicidality, crisis intervention, when to call physician/911 by  <%= data.AnswerOrDefault("GenericGoals10Weeks", "__________").Clean()%> wks.",<%= goals.Contains("10").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("11")) { %>
        printview.checkbox("Exhibit elevated mood as evidenced by:  <%= data.AnswerOrDefault("GenericGoals11Weeks", "__________").Clean()%> wks.",<%= goals.Contains("11").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("12")) { %>
        printview.checkbox("Demonstrate 2 coping skills as evidenced by: <%= data.AnswerOrDefault("GenericGoals12Weeks", "__________").Clean()%> wks.",<%= goals.Contains("12").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("13")) { %>
        printview.checkbox("Make daily social contacts as evidenced by: <%= data.AnswerOrDefault("GenericGoals13Weeks", "__________").Clean()%> wks.",<%= goals.Contains("13").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("14")) { %>
        printview.checkbox("Exhibit goal directed thoughts as evidence by: <%= data.AnswerOrDefault("GenericGoals14Weeks", "__________").Clean()%> wks.",<%= goals.Contains("14").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("15")) { %>
        printview.checkbox("Maintain stable weight, nutrition, hydration status w/weight gain of <%= data.AnswerOrDefault("GenericGoals15Weight", "__________").Clean()%> lbs by <%= data.AnswerOrDefault("GenericGoals15Weeks", "__________").Clean()%> wks.",<%= goals.Contains("15").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("16")) { %>
        printview.checkbox("Achieve sx control of CV & CP status w/meds & relaxation skills AEB by <%= data.AnswerOrDefault("GenericGoals16Weeks", "__________").Clean()%> wks.",<%= goals.Contains("16").ToString().ToLower() %>) +
    <% } %>
    <% if(goals.Contains("17")) { %>
        printview.checkbox("Achieve GI/GU managements as evidence by: <%= data.AnswerOrDefault("GenericGoals17Weeks", "__________").Clean()%> wks.",<%= goals.Contains("17").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("18")) { %>
        printview.checkbox("Verbalize bowel management as evidence by: <%= data.AnswerOrDefault("GenericGoals18Weeks", "__________").Clean()%> wks.",<%= goals.Contains("18").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("19")) { %>
        printview.checkbox("Verbalize and achieve symptom control of sleep disturbance of <%= data.AnswerOrDefault("GenericGoals19Noc", "__________").Clean()%> h/noc by <%= data.AnswerOrDefault("GenericGoals19Weeks", "__________").Clean()%> wks.",<%= goals.Contains("19").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("20")) { %>
        printview.checkbox("Exhibit control of anxiety w/med & relaxation skills AEB by <%= data.AnswerOrDefault("GenericGoals20Weeks", "__________").Clean()%> wks.",<%= goals.Contains("20").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("21")) { %>
        printview.checkbox("Exhibit control of thought disorder as evidenced by <%= data.AnswerOrDefault("GenericGoals21Weeks", "__________").Clean()%> wks.",<%= goals.Contains("21").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("22")) { %>
        printview.checkbox("Achieve mobility/safety management as evidenced by <%= data.AnswerOrDefault("GenericGoals22Weeks", "__________").Clean()%> wks.",<%= goals.Contains("22").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("23")) { %>
        printview.checkbox("Achieve maximum level of self-care as evidenced by <%= data.AnswerOrDefault("GenericGoals23Weeks", "__________").Clean()%> wks.",<%= goals.Contains("23").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("24")) { %>
        printview.checkbox("Verbalized medications, use schedule & side effecs and patient will take as ordered by <%= data.AnswerOrDefault("GenericGoals24Weeks", "__________").Clean()%> wks.",<%= goals.Contains("24").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("25")) { %>
        printview.checkbox("Verbalize adequate knowledge of disease process & know when to notify physician by <%= data.AnswerOrDefault("GenericGoals25Weeks", "__________").Clean()%> wks.",<%= goals.Contains("25").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("26")) { %>
        printview.checkbox("97 Verbalize goal-directed thoughts, really based orientation & congruent thinking by <%= data.AnswerOrDefault("GenericGoals26Weeks", "__________").Clean()%> wks.",<%= goals.Contains("26").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("27")) { %>
        printview.checkbox("Exhibit decreased hyperactivity & safe behaviors as evidence by <%= data.AnswerOrDefault("GenericGoals27Weeks", "__________").Clean()%> wks.",<%= goals.Contains("27").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("28")) { %>
        printview.checkbox("Refrain from boastful/delusional behaviors & from interrupting conversations by <%= data.AnswerOrDefault("GenericGoals28Weeks", "__________").Clean()%> wks.",<%= goals.Contains("28").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("29")) { %>
        printview.checkbox("Demonstrate positive coping mechanisms & verbalize 2 realistic goals by <%= data.AnswerOrDefault("GenericGoals29Weeks", "__________").Clean()%> wks.",<%= goals.Contains("29").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("30")) { %>
        printview.checkbox("Experience no untoward ECT complications by <%= data.AnswerOrDefault("GenericGoals30Weeks", "__________").Clean()%> wks.",<%= goals.Contains("30").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("31")) { %>
        printview.checkbox("Verbalize 2 management techniques of disease by <%= data.AnswerOrDefault("GenericGoals31Weeks", "__________").Clean()%> wks.",<%= goals.Contains("31").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("32")) { %>
        printview.checkbox("Verbalize 2 s/sx of illness by <%= data.AnswerOrDefault("GenericGoals32Weeks", "__________").Clean()%> wks.",<%= goals.Contains("32").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("33")) { %>
        printview.checkbox("Other: <%= data.AnswerOrDefault("GenericGoalsOther", "__________").Clean()%>",<%= goals.Contains("33").ToString().ToLower() %>)) +
    <% } else { %> "")+ <% } %>
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGoalsComment").Clean() %>"),
"Goals"
<%} %>