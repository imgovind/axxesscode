﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericMoodAffect2 = data.AnswerArray("GenericMoodAffect2"); %>
printview.col(2,
    printview.span("Overall Mood Status:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericMoodAffect1","____________").ToString()%>")) +
printview.col(2,
    printview.checkbox("Flat",<%= genericMoodAffect2.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("Depressed",<%= genericMoodAffect2.Contains("2").ToString().ToLower()%>) +
    printview.checkbox("Combative",<%= genericMoodAffect2.Contains("3").ToString().ToLower()%>) +
    printview.checkbox("Agitated",<%= genericMoodAffect2.Contains("4").ToString().ToLower()%>) +
    printview.checkbox("Anxious",<%= genericMoodAffect2.Contains("5").ToString().ToLower()%>) +
    printview.checkbox("Negative",<%= genericMoodAffect2.Contains("6").ToString().ToLower()%>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericMoodAffectComment").Clean()%>"),
   "Mood/Affect"
