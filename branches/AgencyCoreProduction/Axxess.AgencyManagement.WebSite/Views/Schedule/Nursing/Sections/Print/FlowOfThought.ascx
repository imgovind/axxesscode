﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Blocking", "Circumstantial", "Tangential", "Perseveration", "Flight of Ideas", "Loose Association", "Indecisive" }; %>
<% if (data.AnswerOrEmptyString("GenericIsFlowOfThoughtApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsFlowOfThoughtApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Flow of Thought"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericFlowOfThought" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericFlowOfThoughtComment").Clean()%>"),
"Flow of Thought"
<% } %>
