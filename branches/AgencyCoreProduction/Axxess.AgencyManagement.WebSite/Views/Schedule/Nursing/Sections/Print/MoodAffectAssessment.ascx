﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Anxious", "Inappropriate Affect", "Flat Affect", "Elevated Mood", "Depressed Mood", "Labile Mood" }; %>
<% if (data.AnswerOrEmptyString("GenericIsMoodAffectAssessmentApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsMoodAffectAssessmentApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Mood and Affect"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericMoodAffectAssessment" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericMoodAffectAssessmentComment").Clean()%>"),
"Mood and Affect"
<% } %>
