﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
<%  string[] genericNutritionDiet = data.AnswerArray("GenericNutritionDiet"); %>
<%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
<%  string[] genericNutritionResidualProblem = data.AnswerArray("GenericNutritionResidualProblem"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericNutrition.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Dysphagia",<%= genericNutrition.Contains("2").ToString().ToLower() %>) +
printview.checkbox("Decreased Appetite",<%= genericNutrition.Contains("3").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Weight",<%= genericNutrition.Contains("4").ToString().ToLower() %>) +
    <%if(genericNutrition.Contains("4")){ %>
    printview.col(2,
        printview.checkbox("Loss",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToString().ToLower() %>) +
        printview.checkbox("Gain",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToString().ToLower() %>))) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Diet",<%= genericNutrition.Contains("5").ToString().ToLower() %>) +
<%if(genericNutrition.Contains("5")){ %>
printview.col(2,
    printview.checkbox("Adequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToString().ToLower() %>) +
    printview.checkbox("Inadequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToString().ToLower()%>)) +
<%} %>
printview.col(2,
    printview.checkbox("Diet Type",<%= genericNutrition.Contains("9").ToString().ToLower()%>) +
    printview.span("<%= genericNutrition.Contains("9") ? data.AnswerOrEmptyString("GenericNutritionDietType").Clean() : string.Empty%>")) +
printview.checkbox("Enteral Feeding:",<%= genericNutrition.Contains("6").ToString().ToLower() %>) +
<%if(genericNutrition.Contains("6")){ %>
printview.col(3,
    printview.checkbox("NG",<%= genericNutritionEnteralFeeding.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("PEG",<%= genericNutritionEnteralFeeding.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Dobhof",<%= genericNutritionEnteralFeeding.Contains("3").ToString().ToLower() %>)) +
<%} %>
printview.checkbox("Tube Placement Checked",<%= genericNutrition.Contains("7").ToString().ToLower() %>) +
printview.checkbox("Residual Checked:",<%= genericNutrition.Contains("8").ToString().ToLower() %>) +
printview.span(" <%=genericNutrition.Contains("8")?"Amount:"+ data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount").Clean():string.Empty %>ml") +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericNutritionComment").Clean() %>"),
"Nutrition"