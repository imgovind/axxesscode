﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Status:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericGIBowelFunctions1", "____________").ToString()%>")) +
printview.span("Cathartic Required:",true) + 
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("1").ToString().ToLower()%>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("0").ToString().ToLower()%>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGIBowelFunctionsComment").Clean()%>"),
   "G.I. Bowel Functions"
