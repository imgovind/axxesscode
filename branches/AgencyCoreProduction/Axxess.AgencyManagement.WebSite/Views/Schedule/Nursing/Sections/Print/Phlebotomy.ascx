﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var phlebotomyActivity = data.AnswerArray("GenericPhlebotomyActivity"); %>
<%  var phlebotomyDeliveredTo = data.AnswerArray("GenericPhlebotomyDeliveredTo"); %>
<% if (data.AnswerOrEmptyString("GenericIsPhlebotomyApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsPhlebotomyApplied").ToString() == "1")
   { %>
printview.checkbox("N/A",true),"Phlebotomy"
<%}  else { %>
printview.col(2,
    printview.span("Labs:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericPhlebotomyLabs").Clean()%>")) +
printview.span("Venipuncture:",true) +
printview.span("<%= data.AnswerOrDefault("GenericPhlebotomyVenipuncture", "___").Clean()%> to <%= data.AnswerOrDefault("GenericPhlebotomySite", "__________").Clean()%> with <%= data.AnswerOrDefault("GenericPhlebotomyGA", "___").Clean()%> ga. needle/vacutainer using aseptic technique.  Applied pressure for <%= data.AnswerOrDefault("GenericPhlebotomyMinutes", "___").Clean()%> minutes") +
printview.col(2,
    printview.checkbox("Pt tolerated procedure", <%= phlebotomyActivity.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("No bruising", <%= phlebotomyActivity.Contains("2").ToString().ToLower()%>) +
    printview.checkbox("No bleeding", <%= phlebotomyActivity.Contains("3").ToString().ToLower()%>) +
    printview.checkbox("Sharps disposal", <%= phlebotomyActivity.Contains("4").ToString().ToLower()%>)) +
printview.span("Delivered to:",true) +
printview.col(4,
    printview.checkbox("Lab", <%= phlebotomyDeliveredTo.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("Hospital", <%= phlebotomyDeliveredTo.Contains("2").ToString().ToLower()%>) +
    printview.checkbox("MD", <%= phlebotomyDeliveredTo.Contains("3").ToString().ToLower()%>) +
    printview.checkbox("Other <%= data.AnswerOrEmptyString("GenericPhlebotomyDeliveredToOther").Clean()%>", <%= phlebotomyDeliveredTo.Contains("4").ToString().ToLower()%>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPhlebotomyComment").Clean()%>"),
"Phlebotomy"
<% } %>