﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>

printview.col(3,
    printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("0").ToString().ToLower()%>) + 
    printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("1").ToString().ToLower()%>) + 
    printview.checkbox("Excellent",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("2").ToString().ToLower()%>)),
"Rehab Potential"
