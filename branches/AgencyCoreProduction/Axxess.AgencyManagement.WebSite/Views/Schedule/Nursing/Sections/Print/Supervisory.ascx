﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
<%  string[] isGoalsApplied = data.AnswerArray("GenericIsGoalsApplied"); %>
<% if(data.AnswerOrEmptyString("GenericIsSupervisoryApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsSupervisoryApplied").ToString()=="1"){ %>
printview.checkbox("N/A",true),"Supervisory Visit"
<%}else{ %>
printview.checkbox("LVN present",<%= genericSupervisoryVisit.Contains("1").ToString().ToLower()%>) +
printview.checkbox("HHA present",<%= genericSupervisoryVisit.Contains("2").ToString().ToLower()%>) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericSupervisoryVisitComment").Clean()%>"),
"Supervisory Visit"
<% } %>
