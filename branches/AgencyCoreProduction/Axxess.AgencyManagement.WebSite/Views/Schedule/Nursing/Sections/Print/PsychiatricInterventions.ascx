﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] interventions = data.AnswerArray("GenericPsychiatricInterventions"); %>
printview.col(2,
    <% if(interventions.Contains("1")) { %>
        printview.checkbox("Suicidal and safety precautions",<%= interventions.Contains("1").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("2")) { %>
        printview.checkbox("Relaxation, imagery and deep breathing exercises",<%= interventions.Contains("2").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("3")) { %>
        printview.checkbox("Problem solving, positive coping, decision making and stress management technique",<%= interventions.Contains("3").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("4")) { %>
        printview.checkbox("Recognition of s/sx complications of crisis and when to call MD",<%= interventions.Contains("4").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("5")) { %>
        printview.checkbox("Reality/congruent thinking techniques",<%= interventions.Contains("5").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("6")) { %>
        printview.checkbox("Anger management",<%= interventions.Contains("6").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("7")) { %>
        printview.checkbox("Emergency and crisis intervention",<%= interventions.Contains("7").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("8")) { %>
        printview.checkbox("Recognition of thoughts and verbally express painful ones",<%= interventions.Contains("8").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("9")) { %>
        printview.checkbox("Recognition of cardiovascular and neurological side effects of medication",<%= interventions.Contains("9").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("10")) { %>
        printview.checkbox("Ability to focus thoughts on feelings and verbally express painful ones",<%= interventions.Contains("10").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("11")) { %>
        printview.checkbox("Positive feedback to reality and realistic feelings",<%= interventions.Contains("11").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("12")) { %>
        printview.checkbox("Importance of supportive thearpy, reality testing, and positive feedback, validation and confrontation",<%= interventions.Contains("12").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("13")) { %>
        printview.checkbox("Relaxation and stress management techniques",<%= interventions.Contains("13").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("14")) { %>
        printview.checkbox("Relationship between feelings and behavior, impulse control behaviors",<%= interventions.Contains("14").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("15")) { %>
        printview.checkbox("Entry back into community and importance of interacting with others in the environment",<%= interventions.Contains("15").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("16")) { %>
        printview.checkbox("Grieving process and bereavement counseling",<%= interventions.Contains("16").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("17")) { %>
        printview.checkbox("Calming techniques for agitation",<%= interventions.Contains("17").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("18")) { %>
        printview.checkbox("Instructs time planning skills to prevent being overwhelmed",<%= interventions.Contains("18").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("19")) { %>
        printview.checkbox("Instruct recognition of exacerbation of illness, hallucinations, and delusions, inappropriate thought patterns and disorganization",<%= interventions.Contains("19").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("20")) { %>
        printview.checkbox("Instruct exploration of painful or anxious feelings and/or identifying ambivalent feelings",<%= interventions.Contains("20").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("21")) { %>
        printview.checkbox("Instruct importance of providing positive reinforcement for positive actions",<%= interventions.Contains("21").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("22")) { %>
        printview.checkbox("Instruct need for concrete realites and focus on thoughts",<%= interventions.Contains("22").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("23")) { %>
        printview.checkbox("Instruct need for supportive psychotherapist",<%= interventions.Contains("23").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("24")) { %>
        printview.checkbox("Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence",<%= interventions.Contains("24").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("25")) { %>
        printview.checkbox("Instruct in s/sx of lithium toxicity",<%= interventions.Contains("26").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("26")) { %>
        printview.checkbox("Instruct positive coping skills to deal with disease and symptoms",<%= interventions.Contains("26").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("27")) { %>
        printview.checkbox("Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence",<%= interventions.Contains("27").ToString().ToLower() %>)) +
    <% } else { %> "")+ <% } %>

printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPsychiatricInterventionsComment").Clean()%>"),
"Interventions"