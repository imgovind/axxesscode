﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Poor Insight", "Poor Judgment", "Unrealistic Regarding Degree of Illness", "Doesn't Know Why He Is Here", "Unmotivated for Treatment", "Unrealistic Regarding Goals" }; %>
<% if (data.AnswerOrEmptyString("GenericIsInsightAndJudgmentApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsInsightAndJudgmentApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Insight and Judgment"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInsightAndJudgment" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInsightAndJudgmentComment").Clean()%>"),
"Insight and Judgment"
<% } %>
