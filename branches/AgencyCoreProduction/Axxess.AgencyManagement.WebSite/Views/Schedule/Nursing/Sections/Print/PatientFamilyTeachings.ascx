﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] teachings = data.AnswerArray("GenericPatientFamilyTeachings"); %>
<%  string[] teachings2 = data.AnswerArray("GenericPatientFamilyTeachingsNutrition"); %>
<%  string[] teachings3 = data.AnswerArray("GenericPatientFamilyTeachingsTherapyProvided"); %>
printview.col(1,
    printview.checkbox("Medication Regime",<%= teachings.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("Action/Side Effects: <%= teachings.Contains("2") ? data.AnswerOrEmptyString("GenericPatientFamilyTeachings2Text") : "" %>",<%= teachings.Contains("2").ToString().ToLower()%>) +
    printview.checkbox("S/S Disease Process: <%= teachings.Contains("3") ? data.AnswerOrEmptyString("GenericPatientFamilyTeachings3Text") : "" %>",<%= teachings.Contains("3").ToString().ToLower()%>) +
    printview.checkbox("S/S of Complications: <%= teachings.Contains("4") ? data.AnswerOrEmptyString("GenericPatientFamilyTeachings4Text") : ""%>",<%= teachings.Contains("4").ToString().ToLower()%>) +
    printview.checkbox("Extrapyramidal Symptoms",<%= teachings.Contains("5").ToString().ToLower()%>) +
    printview.checkbox("Safety Measures",<%= teachings.Contains("6").ToString().ToLower()%>) +
    printview.checkbox("Relaxation Techniques",<%= teachings.Contains("7").ToString().ToLower()%>)) +
printview.span("Nutrition", true) + 
    printview.checkbox("Diet: <%= teachings2.Contains("1") ? data.AnswerOrEmptyString("GenericPatientFamilyTeachingsNutritionDiet") : string.Empty %>",<%= teachings2.Contains("1").ToString().ToLower()%>) +
    printview.col(1,
        printview.checkbox("Proper Fluid Intake",<%= teachings2.Contains("2").ToString().ToLower()%>)) +
printview.span("Therapy Provided", true) + 
printview.col(1,
    printview.checkbox("Supportive",<%= teachings3.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("Reality",<%= teachings3.Contains("2").ToString().ToLower()%>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPatientFamilyTeachingsComment").Clean()%>"),
"Patient/Family Teachings"