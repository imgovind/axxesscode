﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericNeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
<%  string[] genericNeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
<%  string[] genericBehaviorStatus = data.AnswerArray("GenericBehaviorStatus"); %>
<%  string[] genericNeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
printview.checkbox("LOC",<%= genericNeurologicalStatus.Contains("1").ToString().ToLower() %>) +
<%if(genericNeurologicalStatus.Contains("1")){ %>
printview.span("<%= data.AnswerOrEmptyString("GenericNeurologicalLOC").Clean() %>")+
printview.span("Orientation:",true)+
printview.col(3,
    printview.checkbox("Person",<%= genericNeurologicalOriented.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Place",<%= genericNeurologicalOriented.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Time",<%= genericNeurologicalOriented.Contains("3").ToString().ToLower() %>)) +
<%}else{ %>
printview.checkbox("LOC",false)+
<%} %>

printview.checkbox("Behavior Status",<%= genericNeurologicalStatus.Contains("12").ToString().ToLower() %>) +
<%if(genericNeurologicalStatus.Contains("12")){ %>
printview.col(2,
    printview.checkbox("WNL",<%= genericBehaviorStatus.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Diff. Coping",<%= genericBehaviorStatus.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Withdrawn",<%= genericBehaviorStatus.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Combative",<%= genericBehaviorStatus.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Exp. Depression",<%= genericBehaviorStatus.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Irritability",<%= genericBehaviorStatus.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Imp. Decision",<%= genericBehaviorStatus.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Other",<%= genericBehaviorStatus.Contains("8").ToString().ToLower() %>)) +
<%} %>
printview.checkbox("Pupils",<%= genericNeurologicalStatus.Contains("2").ToString().ToLower() %>) +
<%if(genericNeurologicalStatus.Contains("2")){ %>
printview.span("<%= (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("1") ? "PERRLA/WNL (Within Normal Limits)" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("2") ? "Sluggish" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("3") ? "Non-Reactive" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("4") ? "Other" : string.Empty) %>") +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("2").ToString().ToLower() %>)) +
<%} %>
printview.checkbox("Vision",<%= genericNeurologicalStatus.Contains("3").ToString().ToLower() %>) +
<%if(genericNeurologicalStatus.Contains("3")){ %>
printview.col(2,
    printview.checkbox("WNL",<%= genericNeurologicalVisionStatus.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Blurred Vision",<%= genericNeurologicalVisionStatus.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Cataracts",<%= genericNeurologicalVisionStatus.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Wears Corr. Lenses",<%= genericNeurologicalVisionStatus.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Glaucoma",<%= genericNeurologicalVisionStatus.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Legally Blind",<%= genericNeurologicalVisionStatus.Contains("6").ToString().ToLower() %>)) +
<%} %>
printview.col(2,
    printview.checkbox("Speech",<%= genericNeurologicalStatus.Contains("4").ToString().ToLower() %>) +
    <%if(genericNeurologicalStatus.Contains("4")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("1") ? "Clear" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("2") ? "Slurred" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("3") ? "Aphasic" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("4") ? "Other" : string.Empty) %>") +
    <%}else{ %>
    printview.span("")+
    <%} %>
    printview.checkbox("Paralysis",<%= genericNeurologicalStatus.Contains("5").ToString().ToLower() %>) +
    printview.span("<%= genericNeurologicalStatus.Contains("5")?data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation").Clean():string.Empty %>") +
    printview.checkbox("Quadriplegia",<%= genericNeurologicalStatus.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Paraplegia",<%= genericNeurologicalStatus.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Seizures",<%= genericNeurologicalStatus.Contains("8").ToString().ToLower() %>) +
    printview.span("")+
    printview.checkbox("Tremors",<%= genericNeurologicalStatus.Contains("9").ToString().ToLower() %>) +
    printview.span("<%= genericNeurologicalStatus.Contains("9")?data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation").Clean():string.Empty %>") +
    printview.checkbox("Dizziness",<%= genericNeurologicalStatus.Contains("10").ToString().ToLower() %>) +
    printview.checkbox("Headache",<%= genericNeurologicalStatus.Contains("11").ToString().ToLower() %>)) +

printview.span("HOH:",true) +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("2").ToString().ToLower() %>)) +
printview.span("Comments",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericNeurologicalComment").Clean() %>"),
"Neurological"