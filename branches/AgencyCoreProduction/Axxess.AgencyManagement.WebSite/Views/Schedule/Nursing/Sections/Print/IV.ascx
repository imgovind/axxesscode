﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isIVApplied = data.AnswerArray("GenericIsIVApplied"); %>
<% if(data.AnswerOrEmptyString("GenericIsIVApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsIVApplied").ToString()=="1"){ %>
printview.checkbox("N/A",true),"IV"
<%}else{ %>
printview.col(2,
    printview.span("IV Access",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIVAccess").Clean() %>") +
    printview.span("IV Location",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIVLocation").Clean() %>") +
    printview.span("Cond. of IV Site",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIVConditionOfIVSite").Clean() %>") +
    printview.span("Cond. of Dress.",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIVConditionOfDressing").Clean() %>") +
    printview.span("Dress. changed this visit:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIVSiteDressing").Clean() %>")) +
printview.span("Flush:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericFlush").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericFlush").Equals("0").ToString().ToLower()%>)) +
printview.span("Flushed with <%= data.AnswerOrDefault("GenericIVAccessFlushed", "____").Clean() %>/ml of <%= data.AnswerOrDefault("GenericIVSiteDressingUnit", "_________").Clean()%>") +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericIVComment").Clean() %>"),
"IV"
<%} %>