﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Appetite:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericGIBowelFunctions1", "____________").ToString()%>")) +
printview.col(2,
    printview.span("Fluid Intake:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericNutritionStatus2", "____________").ToString()%>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericNutritionStatusComment").Clean()%>"),
   "Nutrition Status"
