﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
<%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericCardioVascular.Contains("1").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Heart Rhythm",<%= genericCardioVascular.Contains("2").ToString().ToLower() %>) +
    <%if(genericCardioVascular.Contains("2")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("1") ? "Regular/WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("2") ? "Tachycardia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("3") ? "Bradycardia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("4") ? "Arrhythmia/ Dysrhythmia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("5") ? "Other" : string.Empty) %>") +
    <%}else{ %>
    printview.span("")+
    <%} %>
    printview.checkbox("Cap. Refill",<%= genericCardioVascular.Contains("3").ToString().ToLower() %>) +
    <%if(genericCardioVascular.Contains("3")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("1") ? "&#60;3 sec" : string.Empty) + (data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("0") ? "&#62;3 sec" : string.Empty) %>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.checkbox("Pulses:",<%= genericCardioVascular.Contains("4").ToString().ToLower() %>) +
<%if(genericCardioVascular.Contains("4")){ %>
printview.col(2,
    printview.span("Radial",true) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("1") ? "1+ Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("2") ? "2+ Normal" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("3") ? "3+ Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("4") ? "4+ Bounding" : string.Empty) %>")) +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("2").ToString().ToLower() %>)) +
printview.col(2,
    printview.span("Pedal",true) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("1") ? "1+ Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("2") ? "2+ Normal" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("3") ? "3+ Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("4") ? "4+ Bounding" : string.Empty)%>")) +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("2").ToString().ToLower() %>)) +
<%} %>
printview.checkbox("Edema:",<%= genericCardioVascular.Contains("5").ToString().ToLower() %>,true) +
<%if(genericCardioVascular.Contains("5")){ %>
printview.col(2,
    printview.span("Location",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericEdemaLocation").Clean() %>")) +
printview.col(2,
    printview.checkbox("Non-Pitting",<%= genericPittingEdemaType.Contains("1").ToString().ToLower() %>) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("2") ? "Mild" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("3") ? "Moderate" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("4") ? "Severe" : string.Empty) %>")) +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2").ToString().ToLower()%>)) +
printview.col(2,
    printview.checkbox("Pitting",<%= genericPittingEdemaType.Contains("2").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericEdemaPitting") != string.Empty && data.AnswerOrEmptyString("GenericEdemaPitting") != "0" ? data.AnswerOrEmptyString("GenericEdemaPitting").Clean() + "+" : string.Empty%>")) +
<%} %>
printview.col(2,    
    printview.checkbox("Heart Sound",<%= genericCardioVascular.Contains("6").ToString().ToLower() %>) +
    <%if(genericCardioVascular.Contains("6")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericHeartSound").Equals("1") ? "Distant" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSound").Equals("2") ? "Faint" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSound").Equals("3") ? "Reg" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSound").Equals("4") ? "Murmur" : string.Empty)%>") +
    <%}else {%>
    printview.span("")+
    <%} %>
    printview.checkbox("Neck Veins",<%= genericCardioVascular.Contains("7").ToString().ToLower() %>) +
    <%if(genericCardioVascular.Contains("7")){ %>
    printview.span("<%= (data.AnswerOrEmptyString("GenericNeckVeins").Equals("1") ? "Flat" : string.Empty) + (data.AnswerOrEmptyString("GenericNeckVeins").Equals("2") ? "Distended" : string.Empty) + (data.AnswerOrEmptyString("GenericNeckVeins").Equals("3") ? "Cyanosis" : string.Empty) + (data.AnswerOrEmptyString("GenericNeckVeins").Equals("4") ? "Other" : string.Empty)%>")) +
    <%}else{ %>
    printview.span(""))+
    <%} %>
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericCardiovascularComment").Clean() %>"),
"Cardiovascular"