﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Breath Sounds:", true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryBreathSounds").Clean() %>")) +
printview.col(3,
    printview.checkbox("Bilateral",<%= data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("0").ToString().ToLower() %>) +
    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("2").ToString().ToLower() %>)) +
printview.col(2,
    printview.span("Dyspnea:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryDyspnea").Clean() %>") +
    printview.span("Cough:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericCoughList").Clean() %>") +
    printview.span("O<sub>2</sub> at:",true) +
    printview.span("<%= data.AnswerOrEmptyString("Generic02AtText").Clean() %>") +
    printview.span("LPM via:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericLPMVia").Clean() %>") +
    printview.span("Freq:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryFreq").Clean() %>")) +
printview.checkbox("O<sub>2</sub> Precautions",<%= data.AnswerOrEmptyString("RespiratoryO2Precautions").Equals("false").ToString().ToLower()%>) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryComment").Clean() %>"),
"Respiratory"