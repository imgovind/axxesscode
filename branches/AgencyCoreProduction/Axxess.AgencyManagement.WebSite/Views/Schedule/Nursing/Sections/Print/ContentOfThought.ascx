﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Suicidal Thoughts", "Suicidal Plans", "Assaultive Ideas", "Homicidal Thoughts",
       "Homicidal Plans", "Antisocial Attitudes", "Suspiciousness", "Poverty of Content", "Phobias", "Obsessions",
       "Compulsions", "Feelings of Unreality", "Feels Persecuted", "Thoughts of Running Away", "Somatic Complaints",
       "Ideas of Guilt", "Ideas of Hopelessness" }; %>
<% var questions2 = new List<string>() { "Ideas of Worthlessness", "Excessive Religiosity", "Sexual Preoccupation", "Blames Others" }; %>
<% var questions3 = new List<string>() { "Auditory", "Visual", "Other" }; %>
<% var questions4 = new List<string>() { "Of Persection", "Of Grandeur", "Of Reference", "Of Influcence", "Somatic", "Are Systematized", "Other" }; %>
<% if (data.AnswerOrEmptyString("GenericIsContentOfThoughtApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsContentOfThoughtApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Content of Thought"
<%}else{ %>

    <% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThought" + count, possibleAnswers)%>")) +
    <% count++; %>
    <% if(count == questions.Count/2) { %> printview.split() +  <% } %>
    <% } %> 
    printview.split() + 
    <% foreach (string question in questions2)
       { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThought" + count, possibleAnswers)%>")) +
    <% count++; %>
    <% } %> 
        
    printview.span("Illusions", true) +
    printview.col(2,
        printview.span("Present:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtIllusions", possibleAnswers)%>")) +
     printview.span("Hallucinations", true) +
    <% int hallucinationsCount = 1; %>
    <% foreach(string question in questions3) { %>
    printview.col(2,
        printview.span("<%= question %>:", false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtHallucinations" + hallucinationsCount, possibleAnswers)%>")) +
    <% hallucinationsCount++; %>
    <% } %> 
    printview.split() + 
    printview.span("Delusions", true) +
    <% int delusionsCount = 1; %>
    <% foreach(string question in questions4) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtDelusions" + delusionsCount, possibleAnswers)%>")) +
    <% delusionsCount++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericContentOfThoughtComment").Clean()%>"), 
"Content of Thought"
<% } %>
