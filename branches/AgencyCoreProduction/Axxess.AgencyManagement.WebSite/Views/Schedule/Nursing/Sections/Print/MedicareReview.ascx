﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>

        printview.col(2,
            printview.span("Was poc sent with patient?",true)+
            printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericIsPOCSentWithPatient").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericIsPOCSentWithPatient").Equals("0").ToString().ToLower() %>)) +
            printview.span("Was medication sent with patient?",true)+
            printview.col(2,
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericIsMedicationSentWithPatient").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericIsMedicationSentWithPatient").Equals("0").ToString().ToLower() %>))),
            "Medicare Review"

