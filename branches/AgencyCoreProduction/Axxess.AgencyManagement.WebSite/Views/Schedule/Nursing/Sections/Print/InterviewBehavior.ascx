﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions1 = new List<string>() { "Angry Outbursts", "Irritable", "Impulsive", "Hostile", "Silly", "Sensitive", "Apathetic", "Withdrawn", "Evasive", "Passive" }; %>
<% var questions2 = new List<string>() { "Aggressive", "Naive", "Overly Dramatic", "Manipulative", "Dependent", "Uncooperative", "Demanding", "Negativistic", "Callous", "Mood Swings" }; %>
<% if (data.AnswerOrEmptyString("GenericIsInterviewBehaviorApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsInterviewBehaviorApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true), "Interview Behavior"
<%}else{ %>
   <%-- printview.col(2,--%>
    <% int count = 1; %>
    <% foreach(string question in questions1) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInterviewBehavior" + count, possibleAnswers)%>")) +
    <% count++; %>
    <% } %> 
    
    <% int memoryCount = 1; %>
    <% foreach (string question in questions2)
       { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInterviewBehavior" + count, possibleAnswers)%>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInterviewBehaviorComment").Clean()%>"),
"Interview Behavior"
<% } %>
