﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNotePrintViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericMentalStatusOriented = data.AnswerArray("GenericMentalStatusOriented"); %>
printview.col(2,
    printview.span("Overall Mental Status:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericMentalStatus1", "__________").ToString() %>")) +
printview.col(2,
    printview.span("Level of Consiousness:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericMentalStatus2", "__________").ToString()%>")) +
     
printview.span("Hallucinations/Delusions:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("1").ToString().ToLower()%>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("0").ToString().ToLower()%>)) +
printview.span("Suicidal Tendencies:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("1").ToString().ToLower()%>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("0").ToString().ToLower()%>)) +
printview.span("Extrapyramidal SX:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("1").ToString().ToLower()%>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("0").ToString().ToLower()%>)) +
printview.span("Oriented:",true) +
printview.col(3,
    printview.checkbox("Time",<%= genericMentalStatusOriented.Contains("0").ToString().ToLower()%>) +
    printview.checkbox("Place",<%= genericMentalStatusOriented.Contains("1").ToString().ToLower()%>) +
    printview.checkbox("Person",<%= genericMentalStatusOriented.Contains("2").ToString().ToLower()%>)) +
printview.col(2,
    printview.span("Insight PT/Family:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericMentalStatus3").ToString()%>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericMentalStatusComment").Clean()%>"),
   "Mental Status"