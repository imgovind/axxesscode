﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Patient with Family:</label>
<div class="float-right">
    <%  var rapport1 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericRapportPatientFamily", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRapportPatientFamily", rapport1, new { @id = Model.Type + "_GenericRapportPatientFamily", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Familty with Patient:</label>
<div class="float-right">
    <%  var rapport2 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericRapportFamilyPatient", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRapportFamilyPatient", rapport2, new { @id = Model.Type + "_GenericRapportFamilyPatient", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Patient with RN:</label>
<div class="float-right">
    <%  var rapport3 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericRapportPatientRN", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRapportPatientRN", rapport3, new { @id = Model.Type + "_GenericRapportPatientRN", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Family with RN:</label>
<div class="float-right">
    <%  var rapport4 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericRapportFamilyRN", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRapportFamilyRN", rapport4, new { @id = Model.Type + "_GenericRapportFamilyRN", @class = "oe" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericRapportComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericRapportComment", data.AnswerOrEmptyString("GenericRapportComment"), new { @id = Model.Type + "_GenericRapportComment", @class = "fill" })%></div>
