﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
<%= Html.Hidden(Model.Type + "_GenericSupervisoryVisit", string.Empty, new { @id = Model.Type + "_GenericSupervisoryVisitHidden" })%>
<div id="<%= Model.Type %>_SupervisoryContainer">
<ul class="checkgroup inline">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSupervisoryVisit1' name='{0}_GenericSupervisoryVisit' value='1' type='checkbox' {1} />", Model.Type, genericSupervisoryVisit.Contains("1").ToChecked())%>
            <label for="<%= Model.Type %>_GenericSupervisoryVisit1">LVN present</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSupervisoryVisit2' name='{0}_GenericSupervisoryVisit' value='2' type='checkbox' {1} />", Model.Type, genericSupervisoryVisit.Contains("2").ToChecked())%>
            <label for="<%= Model.Type %>_GenericSupervisoryVisit2">HHA present</label>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericSupervisoryVisitComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment", @class = "fill" })%></div>
</div>