﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Overall Mental Status:</label>
<div class="float-right">
    <%  var mentalStatus = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericMentalStatus1", mentalStatus, new { @id = Model.Type + "_GenericMentalStatus1", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Level of Consiousness:</label>
<div class="float-right">
    <%  var mentalStatus2 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Alert", Value = "Alert" },
            new SelectListItem { Text = "Confused", Value = "Confused" },
            new SelectListItem { Text = "Disoriented", Value = "Disoriented" },
        }, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus2", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericMentalStatus2", mentalStatus2, new { @id = Model.Type + "_GenericMentalStatus2", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Hallucinations/Delusions:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='1' name='{0}_GenericMentalStatusHallucinations' id='{0}_GenericMentalStatusHallucinations1' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusHallucinations1">Yes</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='0' name='{0}_GenericMentalStatusHallucinations' id='{0}_GenericMentalStatusHallucinations0' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("0").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusHallucinations0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label class="float-left">Suicidal Tendencies:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='1' name='{0}_GenericMentalStatusSuicidal' id='{0}_GenericMentalStatusSuicidal1' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusSuicidal1">Yes</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='0' name='{0}_GenericMentalStatusSuicidal' id='{0}_GenericMentalStatusSuicidal0' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("0").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusSuicidal0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label class="float-left">Extrapyramidal SX:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='1' name='{0}_GenericMentalStatusExtrapyramidal' id='{0}_GenericMentalStatusExtrapyramidal1' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusExtrapyramidal1">Yes</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='radio' value='0' name='{0}_GenericMentalStatusExtrapyramidal' id='{0}_GenericMentalStatusExtrapyramidal0' {1}>", Model.Type, data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("0").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusExtrapyramidal0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label class="float-left">Oriented:</label>
<div class="float-right">
    <%  string[] genericMentalStatusOriented = data.AnswerArray("GenericMentalStatusOriented"); %>
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= string.Format("<input type='checkbox' value='0' name='{0}_GenericMentalStatusOriented' id='{0}_GenericMentalStatusOrientedTime' {1}>", Model.Type, genericMentalStatusOriented.Contains("0").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusOrientedTime">Time</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='checkbox' value='1' name='{0}_GenericMentalStatusOriented' id='{0}_GenericMentalStatusOrientedPlace' {1}>", Model.Type, genericMentalStatusOriented.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusOrientedPlace">Place</label>
            </div>
        </li>
        <li>
            <div class="option">
                <%= string.Format("<input type='checkbox' value='2' name='{0}_GenericMentalStatusOriented' id='{0}_GenericMentalStatusOrientedPerson' {1}>", Model.Type, genericMentalStatusOriented.Contains("2").ToChecked())%>
                <label for="<%= Model.Type %>_GenericMentalStatusOrientedPerson">Person</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label class="float-left">Insight PT/Family:</label>
<div class="float-right">
    <%  var mentalStatus3 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Good", Value = "Good" },
            new SelectListItem { Text = "Fair", Value = "Fair" },
            new SelectListItem { Text = "Poor", Value = "Poor" },
        }, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus3", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericMentalStatus3", mentalStatus3, new { @id = Model.Type + "_GenericMentalStatus3", @class = "oe" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericMentalStatusComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericMentalStatusComment", data.AnswerOrEmptyString("GenericMentalStatusComment"), new { @id = Model.Type + "_GenericMentalStatusComment", @class = "fill" })%></div>
