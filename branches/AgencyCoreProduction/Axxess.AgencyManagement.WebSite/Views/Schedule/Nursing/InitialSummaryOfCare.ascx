﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<% var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle">Initial Summary of Care | <%= Model.DisplayName %></span>
<div class="wrapper main">
<% using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "InitialSummaryForm" })) { %>
    <%= Html.Hidden("InitialSummaryOfCare_PatientId", Model.PatientId)%>
    <%= Html.Hidden("InitialSummaryOfCare_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("InitialSummaryOfCare_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "125")%>
    <%= Html.Hidden("Type", "InitialSummaryOfCare")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                    Initial Summary of Care
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" class="bigtext"><%= Model.DisplayName %> (<%= Model.MRN %>)</td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label for="InitialSummary_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type+"_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = Model.Type+"_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="InitialSummary_DateCompleted" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%=Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%=Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeIn", data.AnswerOrEmptyString("TimeIn"), new { @id = Model.Type + "_TimeIn", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeOut", data.AnswerOrEmptyString("TimeOut"), new { @id = Model.Type + "_TimeOut", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
                <td>
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <div>
                            <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                            <div class="float-right"><%= Html.TogglePreviousNotes("PreviousNotes", Model.Type + "_PreviousNotes")%></div>
                        </div>
                        <div class="clear"></div>
                    <%  } %>
                    <div>
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
            <tr>
                <th colspan="2">
                    Initial Summary of Care
                </th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="center">
                        <label for="<%= Model.Type %>_CareSummaryTemplates">Templates:</label>
                        <%= Html.ToggleTemplates(Model.Type+"_CareSummaryTemplates", "", "#"+Model.Type+"_CareSummary")%>
                    </div>
                    <%= Html.TextArea(Model.Type+"_CareSummary",data!=null && data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : string.Empty, new { @class = "fill", @id = Model.Type+"_CareSummary", @style="height:150px" })%>
                
                </td>
            </tr>
            <tr>
                <th colspan="2">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician", @class = "complete-required" }) %></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data.AnswerOrEmptyString("SignatureDate"):DateTime.Now.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="InitialSummaryOfCare_Button" />
	<% Html.RenderPartial("NoteBottomButtons", Model); %>
</div>
<% } %>