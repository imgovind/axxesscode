﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<% using (Html.BeginForm("WoundCareSave", "Schedule", FormMethod.Post, new { @id = "WoundCareForm" })) { %>
<% var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Hidden("WoundCare_PatientId", Model.PatientId)%>
<%= Html.Hidden("WoundCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("WoundCare_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "WoundCare")%>
<div class="visitContainer">
<table cellspacing="0" cellpadding="0" style="padding: 0pt;" class="woundCare">
    <tbody>
        <tr><th colspan="6" class="align-center bold"> Wound Graph</th></tr>
        <tr><td class="align-center" colspan="6"><img alt="body outline" src="/Images/body_outline.gif"></td></tr>
        <tr><th width="10%" class="textCenter bold">&#160;</th> <th class="textCenter bold">Wound One</th><th class="textCenter bold">Wound Two</th> <th class="textCenter bold">Wound Three</th><th class="textCenter bold"> Wound Four</th> <th class="textCenter bold"> Wound Five</th> </tr>
        <tr>
            <td>
             <label class="strong">Upload File: </label> 
            </td>
            <td>
             <% if (data.AnswerOrDefault("GenericUploadFile1", Guid.Empty.ToString()) != Guid.Empty.ToString())
              { %><%= Html.Hidden("WoundCare_GenericUploadFile1", data["GenericUploadFile1"].Answer.ToGuid())%> <%= Html.Asset(data["GenericUploadFile1"].Answer.ToGuid()) %>| <% =string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.WoundCareDeleteAsset($(this),'GenericUploadFile1','{0}');\">Delete</a>&#160;", data["GenericUploadFile1"].Answer)%> <%}
              else
              { %>         
                <input type="file" name="WoundCare_GenericUploadFile1" value="Upload" size="13.75" class = "float-left uploadWidth" />
                <%} %>
            </td>
            <td>
              <%if (data.AnswerOrDefault("GenericUploadFile2", Guid.Empty.ToString()) != Guid.Empty.ToString())
                { %> <%= Html.Hidden("WoundCare_GenericUploadFile2", data["GenericUploadFile2"].Answer.ToGuid())%> <%= Html.Asset(data["GenericUploadFile2"].Answer.ToGuid())%> | <% =string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.WoundCareDeleteAsset($(this),'GenericUploadFile2','{0}');\">Delete</a>&#160;", data["GenericUploadFile2"].Answer)%> <%}
                else
                { %>    
                <input type="file" name="WoundCare_GenericUploadFile2" value="Upload" size="13.75" class = "float-left uploadWidth"/>
                <%} %>
            </td>
            <td>
              <%if (data.AnswerOrDefault("GenericUploadFile3", Guid.Empty.ToString()) != Guid.Empty.ToString())
                { %> <%= Html.Hidden("WoundCare_GenericUploadFile3", data["GenericUploadFile3"].Answer.ToGuid())%>  <%= Html.Asset(data["GenericUploadFile3"].Answer.ToGuid())%> | <% =string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.WoundCareDeleteAsset($(this),'GenericUploadFile3','{0}');\">Delete</a>&#160;", data["GenericUploadFile3"].Answer)%>  <%}
                else
                { %>    
               <input type="file" name="WoundCare_GenericUploadFile3" value="Upload" size="13.75" class = "float-left uploadWidth"/>
               <%} %>
            </td>
            <td>
              <%if (data.AnswerOrDefault("GenericUploadFile4", Guid.Empty.ToString()) != Guid.Empty.ToString())
                { %> <%= Html.Hidden("WoundCare_GenericUploadFile4", data["GenericUploadFile4"].Answer.ToGuid())%> <%= Html.Asset(data["GenericUploadFile4"].Answer.ToGuid())%> | <% =string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.WoundCareDeleteAsset($(this),'GenericUploadFile4','{0}');\">Delete</a>&#160;", data["GenericUploadFile4"].Answer)%><%}
                else
                { %>    
                <input type="file" name="WoundCare_GenericUploadFile4" value="Upload" size="13.75" class = "float-left uploadWidth"/>
                <%} %>
            </td>
            <td>
              <%if (data.AnswerOrDefault("GenericUploadFile5", Guid.Empty.ToString()) != Guid.Empty.ToString())
                { %><%= Html.Hidden("WoundCare_GenericUploadFile5", data["GenericUploadFile5"].Answer.ToGuid())%>   <%= Html.Asset(data["GenericUploadFile5"].Answer.ToGuid())%> | <% =string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.WoundCareDeleteAsset($(this),'GenericUploadFile5','{0}');\">Delete</a>&#160;", data["GenericUploadFile5"].Answer)%> <%}
                else
                { %>    
               <input type="file" name="WoundCare_GenericUploadFile5" value="Upload" size="13.75" class = "float-left uploadWidth"/>
               <%} %>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Location:</label> 
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericLocation1", data.ContainsKey("GenericLocation1") ? data["GenericLocation1"].Answer : "", new { @id = "WoundCare_GenericLocation1", @class = "float-left width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericLocation2", data.ContainsKey("GenericLocation2") ? data["GenericLocation2"].Answer: "", new { @id = "WoundCare_GenericLocation2", @class = "float-left width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericLocation3", data.ContainsKey("GenericLocation3") ? data["GenericLocation3"].Answer : "", new { @id = "WoundCare_GenericLocation3", @class = "float-left width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericLocation4", data.ContainsKey("GenericLocation4") ? data["GenericLocation4"].Answer : "", new { @id = "WoundCare_GenericLocation4", @class = "float-left width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericLocation5", data.ContainsKey("GenericLocation5") ? data["GenericLocation5"].Answer : "", new { @id = "WoundCare_GenericLocation5", @class = "float-left width" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Onset Date:</label> 
            </td>
            <td>
               <input type="text" class="date-picker width" name="WoundCare_GenericOnsetDate1" value="<%= data.ContainsKey("GenericOnsetDate1") ? data["GenericOnsetDate1"].Answer : "" %>" id="WoundCare_GenericOnsetDate1" />
            </td>
            <td>
               <input type="text" class="date-picker width" name="WoundCare_GenericOnsetDate2" value="<%= data.ContainsKey("GenericOnsetDate2") ? data["GenericOnsetDate2"].Answer : "" %>" id="WoundCare_GenericOnsetDate2" />
            </td>
            <td>
               <input type="text" class="date-picker width" name="WoundCare_GenericOnsetDate3" value="<%= data.ContainsKey("GenericOnsetDate3") ? data["GenericOnsetDate3"].Answer : "" %>" id="WoundCare_GenericOnsetDate3" />
            </td>
            <td>
               <input type="text" class="date-picker width" name="WoundCare_GenericOnsetDate4" value="<%= data.ContainsKey("GenericOnsetDate4") ? data["GenericOnsetDate4"].Answer : "" %>" id="WoundCare_GenericOnsetDate4" />
            </td>
            <td>
               <input type="text" class="date-picker width" name="WoundCare_GenericOnsetDate5" value="<%= data.ContainsKey("GenericOnsetDate5") ? data["GenericOnsetDate5"].Answer : "" %>" id="WoundCare_GenericOnsetDate5" />
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Wound Type:</label> 
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericWoundType1", data.ContainsKey("GenericWoundType1") ? data["GenericWoundType1"].Answer : "", new { @id = "WoundCare_GenericWoundType1", @class = "float-left WoundType width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericWoundType2", data.ContainsKey("GenericWoundType2") ? data["GenericWoundType2"].Answer : "", new { @id = "WoundCare_GenericWoundType2", @class = "float-left WoundType width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericWoundType3", data.ContainsKey("GenericWoundType3") ? data["GenericWoundType3"].Answer : "", new { @id = "WoundCare_GenericWoundType3", @class = "float-left WoundType width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericWoundType4", data.ContainsKey("GenericWoundType4") ? data["GenericWoundType4"].Answer : "", new { @id = "WoundCare_GenericWoundType4", @class = "float-left WoundType width" })%>
            </td>
            <td>
               <%= Html.TextBox("WoundCare_GenericWoundType5", data.ContainsKey("GenericWoundType5") ? data["GenericWoundType5"].Answer : "", new { @id = "WoundCare_GenericWoundType5", @class = "float-left WoundType width" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Pressure Ulcer Stage:</label> 
            </td>
            <td>
                 <% var pressureUlcerStage1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "I", Value = "I" }, new SelectListItem { Text = "II", Value = "II" }, new SelectListItem { Text = "III", Value = "III" }, new SelectListItem { Text = "IV", Value = "IV" }, new SelectListItem { Text = "Unstageable", Value = "Unstageable" } }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage1") ? data["GenericPressureUlcerStage1"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericPressureUlcerStage1", pressureUlcerStage1, new { @id = "WoundCare_GenericPressureUlcerStage1", @class = "width" })%>
            </td>
            <td>
                 <% var pressureUlcerStage2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "I", Value = "I" }, new SelectListItem { Text = "II", Value = "II" }, new SelectListItem { Text = "III", Value = "III" }, new SelectListItem { Text = "IV", Value = "IV" }, new SelectListItem { Text = "Unstageable", Value = "Unstageable" } }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage2") ? data["GenericPressureUlcerStage2"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericPressureUlcerStage2", pressureUlcerStage2, new { @id = "WoundCare_GenericPressureUlcerStage2", @class = "width" })%>
            </td>
            <td>
                 <% var pressureUlcerStage3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "I", Value = "I" }, new SelectListItem { Text = "II", Value = "II" }, new SelectListItem { Text = "III", Value = "III" }, new SelectListItem { Text = "IV", Value = "IV" }, new SelectListItem { Text = "Unstageable", Value = "Unstageable" } }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage3") ? data["GenericPressureUlcerStage3"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericPressureUlcerStage3", pressureUlcerStage3, new { @id = "WoundCare_GenericPressureUlcerStage3", @class = "width" })%>
            </td>
            <td>
                 <% var pressureUlcerStage4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "I", Value = "I" }, new SelectListItem { Text = "II", Value = "II" }, new SelectListItem { Text = "III", Value = "III" }, new SelectListItem { Text = "IV", Value = "IV" }, new SelectListItem { Text = "Unstageable", Value = "Unstageable" } }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage4") ? data["GenericPressureUlcerStage4"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericPressureUlcerStage4", pressureUlcerStage4, new { @id = "WoundCare_GenericPressureUlcerStage4", @class = "width" })%>
            </td>
            <td>
                 <% var pressureUlcerStage5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "I", Value = "I" }, new SelectListItem { Text = "II", Value = "II" }, new SelectListItem { Text = "III", Value = "III" }, new SelectListItem { Text = "IV", Value = "IV" }, new SelectListItem { Text = "Unstageable", Value = "Unstageable" } }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage5") ? data["GenericPressureUlcerStage5"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericPressureUlcerStage5", pressureUlcerStage5, new { @id = "WoundCare_GenericPressureUlcerStage5", @class = "width" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Measurements:</label> 
                <div class="clear"></div><label class="float-right woundPad">Length:</label>
                <div class="clear"></div><label class="float-right woundPad">Width:</label>
                <div class="clear"></div><label class="float-right woundPad">Depth:</label>
            </td>
            <td>
             <br /><%= Html.TextBox("WoundCare_GenericMeasurementLength1", data.ContainsKey("GenericMeasurementLength1") ? data["GenericMeasurementLength1"].Answer : "", new { @id = "WoundCare_GenericMeasurementLength1", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementWidth1", data.ContainsKey("GenericMeasurementWidth1") ? data["GenericMeasurementWidth1"].Answer : "", new { @id = "WoundCare_GenericMeasurementWidth1", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementDepth1", data.ContainsKey("GenericMeasurementDepth1") ? data["GenericMeasurementDepth1"].Answer : "", new { @id = "WoundCare_GenericMeasurementDepth1", @class = "float-left st woundPad" })%> cm
            </td>
            <td>
             <br /><%= Html.TextBox("WoundCare_GenericMeasurementLength2", data.ContainsKey("GenericMeasurementLength2") ? data["GenericMeasurementLength2"].Answer : "", new { @id = "WoundCare_GenericMeasurementLength2", @class = "float-left st woundPad" })%> cm
             <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementWidth2", data.ContainsKey("GenericMeasurementWidth2") ? data["GenericMeasurementWidth2"].Answer : "", new { @id = "WoundCare_GenericMeasurementWidth2", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementDepth2", data.ContainsKey("GenericMeasurementDepth2") ? data["GenericMeasurementDepth2"].Answer : "", new { @id = "WoundCare_GenericMeasurementDepth2", @class = "float-left st woundPad" })%> cm
            </td>
            <td>
             <br /><%= Html.TextBox("WoundCare_GenericMeasurementLength3", data.ContainsKey("GenericMeasurementLength3") ? data["GenericMeasurementLength3"].Answer : "", new { @id = "WoundCare_GenericMeasurementLength3", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementWidth3", data.ContainsKey("GenericMeasurementWidth3") ? data["GenericMeasurementWidth3"].Answer : "", new { @id = "WoundCare_GenericMeasurementWidth3", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementDepth3", data.ContainsKey("GenericMeasurementDepth3") ? data["GenericMeasurementDepth3"].Answer : "", new { @id = "WoundCare_GenericMeasurementDepth3", @class = "float-left st woundPad" })%> cm
            </td>
            <td>
              <br /><%= Html.TextBox("WoundCare_GenericMeasurementLength4", data.ContainsKey("GenericMeasurementLength4") ? data["GenericMeasurementLength4"].Answer : "", new { @id = "WoundCare_GenericMeasurementLength4", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementWidth4", data.ContainsKey("GenericMeasurementWidth4") ? data["GenericMeasurementWidth4"].Answer : "", new { @id = "WoundCare_GenericMeasurementWidth4", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementDepth4", data.ContainsKey("GenericMeasurementDepth4") ? data["GenericMeasurementDepth4"].Answer : "", new { @id = "WoundCare_GenericMeasurementDepth4", @class = "float-left st woundPad" })%> cm
            </td>
            <td>
              <br /><%= Html.TextBox("WoundCare_GenericMeasurementLength5", data.ContainsKey("GenericMeasurementLength5") ? data["GenericMeasurementLength5"].Answer : "", new { @id = "WoundCare_GenericMeasurementLength5", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementWidth5", data.ContainsKey("GenericMeasurementWidth5") ? data["GenericMeasurementWidth5"].Answer : "", new { @id = "WoundCare_GenericMeasurementWidth5", @class = "float-left st woundPad" })%> cm
              <div class="clear"></div><%= Html.TextBox("WoundCare_GenericMeasurementDepth5", data.ContainsKey("GenericMeasurementDepth5") ? data["GenericMeasurementDepth5"].Answer : "", new { @id = "WoundCare_GenericMeasurementDepth5", @class = "float-left st woundPad" })%> cm
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Wound Bed:</label> 
                <div class="clear"></div><label class="float-right woundPad">Granulation %:</label>
                <div class="clear"></div><label class="float-right woundPad">Slough %:</label>
                <div class="clear"></div><label class="float-right woundPad">Eschar %:</label>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericWoundBedGranulation1", data.ContainsKey("GenericWoundBedGranulation1") ? data["GenericWoundBedGranulation1"].Answer : "", new { @id = "WoundCare_GenericWoundBedGranulation1", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedSlough1", data.ContainsKey("GenericWoundBedSlough1") ? data["GenericWoundBedSlough1"].Answer : "", new { @id = "WoundCare_GenericWoundBedSlough1", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedEschar1", data.ContainsKey("GenericWoundBedEschar1") ? data["GenericWoundBedEschar1"].Answer : "", new { @id = "WoundCare_GenericWoundBedEschar1", @class = "float-left st woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericWoundBedGranulation2", data.ContainsKey("GenericWoundBedGranulation2") ? data["GenericWoundBedGranulation2"].Answer : "", new { @id = "WoundCare_GenericWoundBedGranulation2", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedSlough2", data.ContainsKey("GenericWoundBedSlough2") ? data["GenericWoundBedSlough2"].Answer : "", new { @id = "WoundCare_GenericWoundBedSlough2", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedEschar2", data.ContainsKey("GenericWoundBedEschar2") ? data["GenericWoundBedEschar2"].Answer : "", new { @id = "WoundCare_GenericWoundBedEschar2", @class = "float-left st woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericWoundBedGranulation3", data.ContainsKey("GenericWoundBedGranulation3") ? data["GenericWoundBedGranulation3"].Answer : "", new { @id = "WoundCare_GenericWoundBedGranulation3", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedSlough3", data.ContainsKey("GenericWoundBedSlough3") ? data["GenericWoundBedSlough3"].Answer : "", new { @id = "WoundCare_GenericWoundBedSlough3", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedEschar3", data.ContainsKey("GenericWoundBedEschar3") ? data["GenericWoundBedEschar3"].Answer : "", new { @id = "WoundCare_GenericWoundBedEschar3", @class = "float-left st woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericWoundBedGranulation4", data.ContainsKey("GenericWoundBedGranulation4") ? data["GenericWoundBedGranulation4"].Answer : "", new { @id = "WoundCare_GenericWoundBedGranulation4", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedSlough4", data.ContainsKey("GenericWoundBedSlough4") ? data["GenericWoundBedSlough4"].Answer : "", new { @id = "WoundCare_GenericWoundBedSlough4", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedEschar4", data.ContainsKey("GenericWoundBedEschar4") ? data["GenericWoundBedEschar4"].Answer : "", new { @id = "WoundCare_GenericWoundBedEschar4", @class = "float-left st woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericWoundBedGranulation5", data.ContainsKey("GenericWoundBedGranulation5") ? data["GenericWoundBedGranulation5"].Answer : "", new { @id = "WoundCare_GenericWoundBedGranulation5", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedSlough5", data.ContainsKey("GenericWoundBedSlough5") ? data["GenericWoundBedSlough5"].Answer : "", new { @id = "WoundCare_GenericWoundBedSlough5", @class = "float-left st woundPad" })%>
                 <div class="clear"></div><%= Html.TextBox("WoundCare_GenericWoundBedEschar5", data.ContainsKey("GenericWoundBedEschar5") ? data["GenericWoundBedEschar5"].Answer : "", new { @id = "WoundCare_GenericWoundBedEschar5", @class = "float-left st woundPad" })%>
            </td>
        </tr>
        <tr>
            <td>
               <label class="strong">Surrounding Tissue:</label> 
            </td>
            <td>
                <% var surroundingTissue1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Pink", Value = "Pink" }, new SelectListItem { Text = "Dry", Value = "Dry" }, new SelectListItem { Text = "Pale", Value = "Pale" }, new SelectListItem { Text = "Moist", Value = "Moist" }, new SelectListItem { Text = "Excoriated", Value = "Excoriated" }, new SelectListItem { Text = "Calloused", Value = "Calloused" }, new SelectListItem { Text = "Normal", Value = "Normal" } }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue1") ? data["GenericSurroundingTissue1"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericSurroundingTissue1", surroundingTissue1, new { @id = "WoundCare_GenericSurroundingTissue1", @class = "width" })%>
            </td>
            <td>
                <% var surroundingTissue2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Pink", Value = "Pink" }, new SelectListItem { Text = "Dry", Value = "Dry" }, new SelectListItem { Text = "Pale", Value = "Pale" }, new SelectListItem { Text = "Moist", Value = "Moist" }, new SelectListItem { Text = "Excoriated", Value = "Excoriated" }, new SelectListItem { Text = "Calloused", Value = "Calloused" }, new SelectListItem { Text = "Normal", Value = "Normal" } }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue2") ? data["GenericSurroundingTissue2"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericSurroundingTissue2", surroundingTissue2, new { @id = "WoundCare_GenericSurroundingTissue2", @class = "width" })%>
            </td>
            <td>
                <% var surroundingTissue3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Pink", Value = "Pink" }, new SelectListItem { Text = "Dry", Value = "Dry" }, new SelectListItem { Text = "Pale", Value = "Pale" }, new SelectListItem { Text = "Moist", Value = "Moist" }, new SelectListItem { Text = "Excoriated", Value = "Excoriated" }, new SelectListItem { Text = "Calloused", Value = "Calloused" }, new SelectListItem { Text = "Normal", Value = "Normal" } }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue3") ? data["GenericSurroundingTissue3"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericSurroundingTissue3", surroundingTissue3, new { @id = "WoundCare_GenericSurroundingTissue3", @class = "width" })%>
            </td>
            <td>
                <% var surroundingTissue4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Pink", Value = "Pink" }, new SelectListItem { Text = "Dry", Value = "Dry" }, new SelectListItem { Text = "Pale", Value = "Pale" }, new SelectListItem { Text = "Moist", Value = "Moist" }, new SelectListItem { Text = "Excoriated", Value = "Excoriated" }, new SelectListItem { Text = "Calloused", Value = "Calloused" }, new SelectListItem { Text = "Normal", Value = "Normal" } }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue4") ? data["GenericSurroundingTissue4"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericSurroundingTissue4", surroundingTissue4, new { @id = "WoundCare_GenericSurroundingTissue4", @class = "width" })%>
            </td>
            <td>
                <% var surroundingTissue5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Pink", Value = "Pink" }, new SelectListItem { Text = "Dry", Value = "Dry" }, new SelectListItem { Text = "Pale", Value = "Pale" }, new SelectListItem { Text = "Moist", Value = "Moist" }, new SelectListItem { Text = "Excoriated", Value = "Excoriated" }, new SelectListItem { Text = "Calloused", Value = "Calloused" }, new SelectListItem { Text = "Normal", Value = "Normal" } }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue5") ? data["GenericSurroundingTissue5"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericSurroundingTissue5", surroundingTissue5, new { @id = "WoundCare_GenericSurroundingTissue5", @class = "width" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Drainage:</label> 
            </td>
            <td>
               <% var drainage1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Serous", Value = "Serous" }, new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" }, new SelectListItem { Text = "Purulent", Value = "Purulent" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainage1") ? data["GenericDrainage1"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainage1", drainage1, new { @id = "WoundCare_GenericDrainage1", @class = "width" })%>
            </td>
            <td>
               <% var drainage2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Serous", Value = "Serous" }, new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" }, new SelectListItem { Text = "Purulent", Value = "Purulent" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainage2") ? data["GenericDrainage2"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainage2", drainage2, new { @id = "WoundCare_GenericDrainage2", @class = "width" })%>
            </td>
            <td>
               <% var drainage3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Serous", Value = "Serous" }, new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" }, new SelectListItem { Text = "Purulent", Value = "Purulent" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainage3") ? data["GenericDrainage3"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainage3", drainage3, new { @id = "WoundCare_GenericDrainage3", @class = "width" })%>
            </td>
            <td>
               <% var drainage4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Serous", Value = "Serous" }, new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" }, new SelectListItem { Text = "Purulent", Value = "Purulent" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainage4") ? data["GenericDrainage4"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainage4", drainage4, new { @id = "WoundCare_GenericDrainage4", @class = "width" })%>
            </td>
            <td>
               <% var drainage5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Serous", Value = "Serous" }, new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" }, new SelectListItem { Text = "Purulent", Value = "Purulent" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainage5") ? data["GenericDrainage5"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainage5", drainage5, new { @id = "WoundCare_GenericDrainage5", @class = "width" })%>
            </td>
        </tr>
        <tr>
            <td>
              <label class="strong">Drainage Amount:</label> 
            </td>
            <td>
               <% var drainageAmount1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Minimal", Value = "Minimal" }, new SelectListItem { Text = "Moderate", Value = "Moderate" }, new SelectListItem { Text = "Heavy", Value = "Heavy" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainageAmount1") ? data["GenericDrainageAmount1"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainageAmount1", drainageAmount1, new { @id = "WoundCare_GenericDrainageAmount1", @class = "width" })%>
            </td>
            <td>
               <% var drainageAmount2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Minimal", Value = "Minimal" }, new SelectListItem { Text = "Moderate", Value = "Moderate" }, new SelectListItem { Text = "Heavy", Value = "Heavy" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainageAmount2") ? data["GenericDrainageAmount2"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainageAmount2", drainageAmount2, new { @id = "WoundCare_GenericDrainageAmount2", @class = "width" })%>
            </td>
            <td>
               <% var drainageAmount3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Minimal", Value = "Minimal" }, new SelectListItem { Text = "Moderate", Value = "Moderate" }, new SelectListItem { Text = "Heavy", Value = "Heavy" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainageAmount3") ? data["GenericDrainageAmount3"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainageAmount3", drainageAmount3, new { @id = "WoundCare_GenericDrainageAmount3", @class = "width" })%>
            </td>
            <td>
               <% var drainageAmount4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Minimal", Value = "Minimal" }, new SelectListItem { Text = "Moderate", Value = "Moderate" }, new SelectListItem { Text = "Heavy", Value = "Heavy" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainageAmount4") ? data["GenericDrainageAmount4"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainageAmount4", drainageAmount4, new { @id = "WoundCare_GenericDrainageAmount4", @class = "width" })%>
            </td>
            <td>
               <% var drainageAmount5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Minimal", Value = "Minimal" }, new SelectListItem { Text = "Moderate", Value = "Moderate" }, new SelectListItem { Text = "Heavy", Value = "Heavy" }, new SelectListItem { Text = "None", Value = "None" } }, "Value", "Text", data.ContainsKey("GenericDrainageAmount5") ? data["GenericDrainageAmount5"].Answer : "0");%><%= Html.DropDownList("WoundCare_GenericDrainageAmount5", drainageAmount5, new { @id = "WoundCare_GenericDrainageAmount5", @class = "width" })%>
            </td>
        </tr>
        <tr>
            <td>
              <label class="strong">Odor:</label> 
            </td>
            <td>
                <%= Html.TextBox("WoundCare_GenericOdor1", data.ContainsKey("GenericOdor1") ? data["GenericOdor1"].Answer : "", new { @id = "WoundCare_GenericOdor1", @class = "float-left width" })%>
            </td>
            <td>
                <%= Html.TextBox("WoundCare_GenericOdor2", data.ContainsKey("GenericOdor2") ? data["GenericOdor2"].Answer : "", new { @id = "WoundCare_GenericOdor2", @class = "float-left width" })%>
            </td>
            <td>
                <%= Html.TextBox("WoundCare_GenericOdor3", data.ContainsKey("GenericOdor3") ? data["GenericOdor3"].Answer : "", new { @id = "WoundCare_GenericOdor3", @class = "float-left width" })%>
            </td>
            <td>
                <%= Html.TextBox("WoundCare_GenericOdor4", data.ContainsKey("GenericOdor4") ? data["GenericOdor4"].Answer : "", new { @id = "WoundCare_GenericOdor4", @class = "float-left width" })%>
            </td>
            <td>
                <%= Html.TextBox("WoundCare_GenericOdor5", data.ContainsKey("GenericOdor5") ? data["GenericOdor5"].Answer : "", new { @id = "WoundCare_GenericOdor5", @class = "float-left width" })%>
            </td>
        </tr>
        <tr>
            <td>
              <label class="strong">Tunneling:</label> 
                <div class="clear"></div><label class="float-right woundPad">Length:</label>
                <div class="clear"></div><label class="float-right woundPad">Time:</label>
            </td>
            <td>
              <br /> <%= Html.TextBox("WoundCare_GenericTunnelingLength1", data.ContainsKey("GenericTunnelingLength1") ? data["GenericTunnelingLength1"].Answer : "", new { @id = "WoundCare_GenericTunnelingLength1", @class = "float-left st woundPad" })%> cm <div class="clear"></div><% var tunnelingTime1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericTunnelingTime1") ? data["GenericTunnelingTime1"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericTunnelingTime1", tunnelingTime1, new { @id = "WoundCare_GenericTunnelingTime1", @class = "st" })%> o’clock
            </td>
            <td>
              <br /> <%= Html.TextBox("WoundCare_GenericTunnelingLength2", data.ContainsKey("GenericTunnelingLength2") ? data["GenericTunnelingLength2"].Answer : "", new { @id = "WoundCare_GenericTunnelingLength2", @class = "float-left st woundPad" })%> cm <div class="clear"></div> <% var tunnelingTime2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericTunnelingTime2") ? data["GenericTunnelingTime2"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericTunnelingTime2", tunnelingTime2, new { @id = "WoundCare_GenericTunnelingTime2", @class = "st" })%> o’clock
            </td>
            <td>
             <br />  <%= Html.TextBox("WoundCare_GenericTunnelingLength3", data.ContainsKey("GenericTunnelingLength3") ? data["GenericTunnelingLength3"].Answer : "", new { @id = "WoundCare_GenericTunnelingLength3", @class = "float-left st woundPad" })%> cm <div class="clear"></div> <% var tunnelingTime3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericTunnelingTime3") ? data["GenericTunnelingTime3"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericTunnelingTime3", tunnelingTime3, new { @id = "WoundCare_GenericTunnelingTime3", @class = "st" })%> o’clock
            </td>
            <td>
             <br />  <%= Html.TextBox("WoundCare_GenericTunnelingLength4", data.ContainsKey("GenericTunnelingLength4") ? data["GenericTunnelingLength4"].Answer : "", new { @id = "WoundCare_GenericTunnelingLength4", @class = "float-left st woundPad" })%> cm <div class="clear"></div> <% var tunnelingTime4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericTunnelingTime4") ? data["GenericTunnelingTime4"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericTunnelingTime4", tunnelingTime4, new { @id = "WoundCare_GenericTunnelingTime4", @class = "st" })%> o’clock
            </td>
            <td>
             <br />  <%= Html.TextBox("WoundCare_GenericTunnelingLength5", data.ContainsKey("GenericTunnelingLength5") ? data["GenericTunnelingLength5"].Answer : "", new { @id = "WoundCare_GenericTunnelingLength5", @class = "float-left st woundPad" })%> cm <div class="clear"></div> <% var tunnelingTime5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericTunnelingTime5") ? data["GenericTunnelingTime5"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericTunnelingTime5", tunnelingTime5, new { @id = "WoundCare_GenericTunnelingTime5", @class = "st" })%> o’clock
            </td>
        </tr>
        <tr>
            <td>
               <label class="strong"> Undermining:</label> 
                <div class="clear"></div><label class="float-right woundPad">Length:</label>
                <div class="clear"></div><label class="float-right woundPad">Time:</label>
            </td>
            <td>
              <br /> <%= Html.TextBox("WoundCare_GenericUnderminingLength1", data.ContainsKey("GenericUnderminingLength1") ? data["GenericUnderminingLength1"].Answer : "", new { @id = "WoundCare_GenericUnderminingLength1", @class = "float-left st woundPad" })%> cm  <div class="clear"></div><% var underminingTimeFrom1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom1") ? data["GenericUnderminingTimeFrom1"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeFrom1", underminingTimeFrom1, new { @id = "WoundCare_GenericUnderminingTimeFrom1", @class = "st" })%> To <% var underminingTimeTo1 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo1") ? data["GenericUnderminingTimeTo1"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeTo1", underminingTimeTo1, new { @id = "WoundCare_GenericUnderminingTimeTo1", @class = "st" })%> o’clock
            </td>
            <td>
               <br /> <%= Html.TextBox("WoundCare_GenericUnderminingLength2", data.ContainsKey("GenericUnderminingLength2") ? data["GenericUnderminingLength2"].Answer : "", new { @id = "WoundCare_GenericUnderminingLength2", @class = "float-left st woundPad" })%> cm  <div class="clear"></div><% var underminingTimeFrom2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom2") ? data["GenericUnderminingTimeFrom2"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeFrom2", underminingTimeFrom2, new { @id = "WoundCare_GenericUnderminingTimeFrom2", @class = "st" })%> To <% var underminingTimeTo2 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo2") ? data["GenericUnderminingTimeTo2"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeTo2", underminingTimeTo2, new { @id = "WoundCare_GenericUnderminingTimeTo2", @class = "st" })%> o’clock
            </td>
            <td>
               <br /> <%= Html.TextBox("WoundCare_GenericUnderminingLength3", data.ContainsKey("GenericUnderminingLength3") ? data["GenericUnderminingLength3"].Answer : "", new { @id = "WoundCare_GenericUnderminingLength3", @class = "float-left st woundPad" })%> cm  <div class="clear"></div><% var underminingTimeFrom3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom3") ? data["GenericUnderminingTimeFrom3"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeFrom3", underminingTimeFrom3, new { @id = "WoundCare_GenericUnderminingTimeFrom3", @class = "st" })%> To <% var underminingTimeTo3 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo3") ? data["GenericUnderminingTimeTo3"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeTo3", underminingTimeTo3, new { @id = "WoundCare_GenericUnderminingTimeTo3", @class = "st" })%> o’clock
            </td>
            <td>
               <br /> <%= Html.TextBox("WoundCare_GenericUnderminingLength4", data.ContainsKey("GenericUnderminingLength4") ? data["GenericUnderminingLength4"].Answer : "", new { @id = "WoundCare_GenericUnderminingLength4", @class = "float-left st woundPad" })%> cm  <div class="clear"></div><% var underminingTimeFrom4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom4") ? data["GenericUnderminingTimeFrom4"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeFrom4", underminingTimeFrom4, new { @id = "WoundCare_GenericUnderminingTimeFrom4", @class = "st" })%> To <% var underminingTimeTo4 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo4") ? data["GenericUnderminingTimeTo4"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeTo4", underminingTimeTo4, new { @id = "WoundCare_GenericUnderminingTimeTo4", @class = "st" })%> o’clock
            </td>
            <td>
                <br /><%= Html.TextBox("WoundCare_GenericUnderminingLength5", data.ContainsKey("GenericUnderminingLength5") ? data["GenericUnderminingLength5"].Answer : "", new { @id = "WoundCare_GenericUnderminingLength5", @class = "float-left st woundPad" })%> cm  <div class="clear"></div><% var underminingTimeFrom5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom5") ? data["GenericUnderminingTimeFrom5"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeFrom5", underminingTimeFrom5, new { @id = "WoundCare_GenericUnderminingTimeFrom6", @class = "st" })%> To <% var underminingTimeTo5 = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "1", Value = "1" }, new SelectListItem { Text = "2", Value = "2" }, new SelectListItem { Text = "3", Value = "3" }, new SelectListItem { Text = "4", Value = "4" }, new SelectListItem { Text = "5", Value = "5" }, new SelectListItem { Text = "6", Value = "6" }, new SelectListItem { Text = "7", Value = "7" }, new SelectListItem { Text = "8", Value = "8" }, new SelectListItem { Text = "9", Value = "9" }, new SelectListItem { Text = "10", Value = "10" }, new SelectListItem { Text = "11", Value = "11" }, new SelectListItem { Text = "12", Value = "12" } }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo5") ? data["GenericUnderminingTimeTo5"].Answer : "");%><%= Html.DropDownList("WoundCare_GenericUnderminingTimeTo5", underminingTimeTo5, new { @id = "WoundCare_GenericUnderminingTimeTo5", @class = "st" })%> o’clock
            </td>
        </tr>
        <tr>
            <td>
               <label class="strong"> Device:</label> 
                <div class="clear"></div><label class="float-right woundPad">Type:</label>
                <div class="clear"></div><label class="float-right woundPad">Setting:</label>
            </td>
            <td>
                <br /><%= Html.TextBox("WoundCare_GenericDeviceType1", data.ContainsKey("GenericDeviceType1") ? data["GenericDeviceType1"].Answer : "", new { @id = "WoundCare_GenericDeviceType1", @class = "float-left width woundPad DeviceType" })%>
                <div class="clear"></div><%= Html.TextBox("WoundCare_GenericDeviceSetting1", data.ContainsKey("GenericDeviceSetting1") ? data["GenericDeviceSetting1"].Answer : "", new { @id = "WoundCare_GenericDeviceSetting1", @class = "float-left width woundPad" })%>
            </td>
            <td>
                <br /><%= Html.TextBox("WoundCare_GenericDeviceType2", data.ContainsKey("GenericDeviceType2") ? data["GenericDeviceType2"].Answer : "", new { @id = "WoundCare_GenericDeviceType2", @class = "float-left width woundPad DeviceType" })%>
                <div class="clear"></div><%= Html.TextBox("WoundCare_GenericDeviceSetting2", data.ContainsKey("GenericDeviceSetting2") ? data["GenericDeviceSetting2"].Answer : "", new { @id = "WoundCare_GenericDeviceSetting2", @class = "float-left width woundPad" })%>
            </td>
            <td>
               <br /><%= Html.TextBox("WoundCare_GenericDeviceType3", data.ContainsKey("GenericDeviceType3") ? data["GenericDeviceType3"].Answer : "", new { @id = "WoundCare_GenericDeviceType3", @class = "float-left width woundPad DeviceType" })%>
                <div class="clear"></div><%= Html.TextBox("WoundCare_GenericDeviceSetting3", data.ContainsKey("GenericDeviceSetting3") ? data["GenericDeviceSetting3"].Answer : "", new { @id = "WoundCare_GenericDeviceSetting3", @class = "float-left width woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericDeviceType4", data.ContainsKey("GenericDeviceType4") ? data["GenericDeviceType4"].Answer : "", new { @id = "WoundCare_GenericDeviceType4", @class = "float-left width woundPad DeviceType" })%>
                <div class="clear"></div><%= Html.TextBox("WoundCare_GenericDeviceSetting4", data.ContainsKey("GenericDeviceSetting4") ? data["GenericDeviceSetting4"].Answer : "", new { @id = "WoundCare_GenericDeviceSetting4", @class = "float-left width woundPad" })%>
            </td>
            <td>
                 <br /><%= Html.TextBox("WoundCare_GenericDeviceType5", data.ContainsKey("GenericDeviceType5") ? data["GenericDeviceType5"].Answer : "", new { @id = "WoundCare_GenericDeviceType5", @class = "float-left width woundPad DeviceType" })%>
                <div class="clear"></div><%= Html.TextBox("WoundCare_GenericDeviceSetting5", data.ContainsKey("GenericDeviceSetting5") ? data["GenericDeviceSetting5"].Answer : "", new { @id = "WoundCare_GenericDeviceSetting5", @class = "float-left width woundPad" })%>
            </td>
        </tr>
        <tr>
            <td colspan="6">
               <label class="strong"> Treatment Performed:</label> 
               <div class="margin"><div><%= Html.ToggleTemplates("WoundCare_GenericTreatmentPerformedTemplate", "", "#WoundCare_GenericTreatmentPerformed")%>
               <%= Html.TextArea("WoundCare_GenericTreatmentPerformed", data.ContainsKey("GenericTreatmentPerformed") ? data["GenericTreatmentPerformed"].Answer : "", 8, 20, new { @class = "", @id = "WoundCare_GenericTreatmentPerformed", @style = "width:95%;" })%></div></div> 
            </td>
            </tr>
            <tr>
            <td colspan="6">
              <label class="strong"> Narrative:</label>
                <div class="margin"><div><%= Html.ToggleTemplates("WoundCare_GenericNarrativeTemplate", "", "#WoundCare_GenericNarrative")%>
                <%= Html.TextArea("WoundCare_GenericNarrative", data.ContainsKey("GenericNarrative") ? data["GenericNarrative"].Answer : "", 8, 20, new { @class = "", @id = "WoundCare_GenericNarrative", @style = "width:95%;" })%></div></div> 
            </td>
        </tr>
    </tbody>
</table>
</div>
<div class="clear"></div>
<div class="buttons">
    <ul>
        <li>
            <a href="javascript:void(0);" class="save autosave">Save</a>
        </li><li>
            <a href="javascript:void(0);" class="close">Exit</a>
        </li>
    </ul>
</div>
<%} %> 