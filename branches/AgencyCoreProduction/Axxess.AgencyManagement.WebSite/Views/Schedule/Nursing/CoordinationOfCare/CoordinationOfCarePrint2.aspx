﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<%var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.TransferSummary.ToString(), "Transfer Summary" },
    { DisciplineTasks.CoordinationOfCare.ToString(), "Coordination of Care" }
     }; %>
<%var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %> <%= dictonary.ContainsKey(Model.Data.Type) ? dictonary[Model.Data.Type] : string.Empty %><%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", "+Model.LocationProfile.AddressLine2.Clean() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= dictonary.ContainsKey(Model.Data.Type) ? dictonary[Model.Data.Type] : ""%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        '<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : ""%>' +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3ECompletion Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("CompletedDate") && data["CompletedDate"].Answer.IsNotNullOrEmpty() ? data["CompletedDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()) : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null ? Model.Data.PhysicianDisplayName.Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer.Clean() : string.Empty %>" +
        
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.LocationProfile.Name.Clean().IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.LocationProfile.AddressLine2.Clean() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3ECoordination of Care/%3Cbr /%3ETransfer Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.PatientProfile != null ? (Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.col(3,
            printview.checkbox("Amputation",<%= functionLimitations != null && functionLimitations.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Legally Blind",<%= functionLimitations != null && functionLimitations.Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Endurance",<%= functionLimitations != null && functionLimitations.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Contracture",<%= functionLimitations != null && functionLimitations.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Hearing",<%= functionLimitations != null && functionLimitations.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Paralysis",<%= functionLimitations != null && functionLimitations.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations != null && functionLimitations.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Dyspnea with Minimal Exertion",<%= functionLimitations != null && functionLimitations.Contains("A") ? "true" : "false"%>) +
            printview.checkbox("Ambulation",<%= functionLimitations != null && functionLimitations.Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Speech",<%= functionLimitations != null && functionLimitations.Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Other",<%= functionLimitations != null && functionLimitations.Contains("B") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer.Clean() : string.Empty %>",0,1) ),
        "Functional Limitations");
    printview.addsection(
        printview.col(5,
            printview.checkbox("Stable",<%= patientCondition != null && patientCondition.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Improved",<%= patientCondition != null && patientCondition.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Unchanged",<%= patientCondition != null && patientCondition.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Unstable",<%= patientCondition != null && patientCondition.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Declined",<%= patientCondition != null && patientCondition.Contains("5") ? "true" : "false"%>) ),
        "Patient Condition");
    printview.addsection(
        printview.col(3,
            printview.checkbox("SN",<%= serviceProvided != null && serviceProvided.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("PT",<%= serviceProvided != null && serviceProvided.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("OT",<%= serviceProvided != null && serviceProvided.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("ST",<%= serviceProvided != null && serviceProvided.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("MSW",<%= serviceProvided != null && serviceProvided.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("HHA",<%= serviceProvided != null && serviceProvided.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Other",<%= serviceProvided != null && serviceProvided.Contains("7") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer.Clean() : string.Empty %>",0,1) ),
        "Service(s) Provided");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ESBP%3C/th%3E%3Cth%3EDBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3Cth%3EBG%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3ELowest%3C/th%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPDiaMin") ? data["VitalSignBPDiaMin"].Answer.Clean() : string.Empty%>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer.Clean() : string.Empty %>" + 
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3EHighest%3C/th%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPDiaMax") ? data["VitalSignBPDiaMax"].Answer.Clean() : string.Empty%>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
        "Vital Sign Ranges");
        printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model.Data); %>);
    
    printview.addsection(
        printview.col(6,
            printview.span("Facility:",true) +
            printview.span("<%= data != null && data.ContainsKey("Facility") ? data["Facility"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Phone:",true) +
            printview.span("<%= data != null && data.ContainsKey("Phone1") && data.ContainsKey("Phone2") && data.ContainsKey("Phone3") ? data["Phone1"].Answer.Clean() + '-' + data["Phone2"].Answer.Clean() + '-' + data["Phone3"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Contact:",true) +
            printview.span("<%= data != null && data.ContainsKey("Contact") ? data["Contact"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.span("Services Providing:",true) +
        printview.span("<%= data != null && data.ContainsKey("ServicesProviding") && data["ServicesProviding"].Answer.IsNotNullOrEmpty() ? data["ServicesProviding"].Answer.Clean() : string.Empty %>",0,10),
        "Transfer Facility Information");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer.Clean() : string.Empty %>",0,10),"Summary of Care Provided by HHA");
        printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MedicareReview.ascx", Model.Data); %>);
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>
