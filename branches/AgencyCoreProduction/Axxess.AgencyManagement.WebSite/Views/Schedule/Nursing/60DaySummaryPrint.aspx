﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>();
string[] homeboundStatus = data.ContainsKey("HomeboundStatus") && data["HomeboundStatus"].Answer != "" ? data["HomeboundStatus"].Answer.Split(',') : null;
string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.LocationProfile != null && Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %><%=Model.Data.TypeName %><%= Model != null && Model.PatientProfile != null ? (" | " + (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToTitleCase() + ", " : "") + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToTitleCase() + " " : "") + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/Nursing/sixtyday.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>


<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.LocationProfile != null ? (Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : "") + (Model.LocationProfile != null ? (Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : "") + (Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", "+Model.LocationProfile.AddressLine2.Clean() + "<br />" : "<br />") + (Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity + ", " : "") + (Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.ToUpper() + "  " : "") + (Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : "") + (Model.LocationProfile.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + Model.LocationProfile.PhoneWorkFormatted : "") + (Model.LocationProfile.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.LocationProfile.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "subject": "<%=Model.Data.TypeName %>",
            "patientname": "<%= (Model != null && Model.PatientProfile != null ? (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() : "") : "").Clean() %>",
            "mr": "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber.Clean() : "" %>",
            "dob": "<%= Model.PatientProfile != null ? Model.PatientProfile.DOB.ToZeroFilled() : "" %>",
            "episode": "<%= data != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid() ? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()).Clean() : "" %>",
            "physician": "<%= Model.Data.PhysicianDisplayName.Clean().ToTitleCase() %>",
            <%string notificationDate = ""; if (data.ContainsKey("DNR") && data["DNR"].Answer == "1" && data.ContainsKey("NotificationDate")) { if (data["NotificationDate"].Answer == "1") { notificationDate = "5 days"; } else if (data["NotificationDate"].Answer == "2") { notificationDate = "2 days"; } else if (data["NotificationDate"].Answer == "3") { notificationDate = data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "Other"; } }   %>
        
            "dnr": "<%= data != null ? (data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? "Yes" + notificationDate : "No") : ""%>",
            "sign": "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>",
            "signdate": "<%= Model != null && Model.Data.SignatureDate != null && Model.Data.SignatureDate.ToDateTime().IsValid() ? Model.Data.SignatureDate : string.Empty %>"
        };
        PdfPrint.BuildSections(<%= Model.Data.PrintViewJson  %>); <%
    }).Render(); %>
</body>
</html>