﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions!=null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Skilled Nurse Pediatric Visit<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Skilled Nurse Pediatric Visit Note(Version 2)" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.PatientProfile.LastName.Clean()%>, <%= Model.PatientProfile.FirstName.Clean()%> <%= Model.PatientProfile.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.PatientProfile.DOB.ToZeroFilled().Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.PatientProfile.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.Data.StartDate.ToShortDateString().ToZeroFilled(), Model.Data.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model.Data); %>,3);
    printview.addsubsection(
        printview.col(3,
            printview.span("Turgor:",true)+
            printview.checkbox("Elastic",<%= data.AnswerArray("IntegTurgor").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Inelastic",<%= data.AnswerArray("IntegTurgor").Contains("1").ToString().ToLower()%>) +
            printview.span("Skin Temp:",true)+
            printview.checkbox("Warm",<%= data.AnswerArray("IntegSkinTemp").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Cool",<%= data.AnswerArray("IntegSkinTemp").Contains("1").ToString().ToLower()%>) +
            printview.span("Skin Condition:",true)+
            printview.checkbox("Diaphoretic",<%= data.AnswerArray("IntegSkinCondition").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Dry",<%= data.AnswerArray("IntegSkinCondition").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Normal",<%= data.AnswerArray("IntegSkinCondition").Contains("2").ToString().ToLower()%>) +
            printview.checkbox("Rash",<%= data.AnswerArray("IntegSkinCondition").Contains("3").ToString().ToLower()%>) +
            printview.checkbox("Excoriation",<%= data.AnswerArray("IntegSkinCondition").Contains("4").ToString().ToLower()%>) +
            printview.span("Edema:",true)+
            printview.checkbox("None",<%= data.AnswerArray("IntegEdema").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Present <%= data.AnswerArray("IntegEdema").Contains("1")? ":"+data.AnswerOrEmptyString("IntegEdemaPresent").Clean():string.Empty%>",<%= data.AnswerArray("IntegEdema").Contains("1").ToString().ToLower()%>)) +
            printview.span("Incisions/Dressings: ",true)+
            printview.span("<%=data.AnswerOrEmptyString("IntegIncisions").Clean() %>",2)+
            printview.span("Comments:",true)+
            printview.span("<%=data.AnswerOrEmptyString("IntegComment").Clean()%>",2),
            "Integ");
    printview.addsubsection(
        printview.col(3,
            printview.span("Rhythm:",true)+
            printview.checkbox("Regular",<%= data.AnswerArray("Cardiorhythm").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Irregular",<%= data.AnswerArray("Cardiorhythm").Contains("1").ToString().ToLower()%>) +
            printview.span("Rate:",true)+
            printview.checkbox("Strong",<%= data.AnswerArray("CardioRate").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Weak",<%= data.AnswerArray("CardioRate").Contains("1").ToString().ToLower()%>)) +
        printview.col(4,
            printview.span("Capillary Refill:",true)+
            printview.checkbox("< 1 sec",<%= data.AnswerArray("CardioCapillaryRefill").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("< 3 sec",<%= data.AnswerArray("CardioCapillaryRefill").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("< 6 sec",<%= data.AnswerArray("CardioCapillaryRefill").Contains("2").ToString().ToLower()%>)) +
        printview.span("Pulse",true)+
        printview.col(3,
            printview.span("Femoral",true)+
            printview.checkbox("L<%=data.AnswerArray("CardioFemoral").Contains("0")?":"+data.AnswerOrEmptyString("CardioFemoralL").Clean():string.Empty%>",<%= data.AnswerArray("CardioFemoral").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("R<%=data.AnswerArray("CardioFemoral").Contains("1")?":"+data.AnswerOrEmptyString("CardioFemoralR").Clean():string.Empty%>",<%= data.AnswerArray("CardioFemoral").Contains("1").ToString().ToLower()%>) +
            printview.span("Radial:",true)+
            printview.checkbox("L<%=data.AnswerArray("CardioRadial").Contains("0") ? ":" + data.AnswerOrEmptyString("CardioRadialL").Clean() : string.Empty%>",<%= data.AnswerArray("CardioRadial").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("R<%=data.AnswerArray("CardioRadial").Contains("1") ? ":" + data.AnswerOrEmptyString("CardioRadialR").Clean() : string.Empty%>",<%= data.AnswerArray("CardioRadial").Contains("1").ToString().ToLower()%>) +
            printview.span("Pedal:",true)+
            printview.checkbox("L<%=data.AnswerArray("CardioPedal").Contains("0") ? ":" + data.AnswerOrEmptyString("CardioPedalL").Clean() : string.Empty%>",<%= data.AnswerArray("CardioPedal").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("R<%=data.AnswerArray("CardioPedal").Contains("1") ? ":" + data.AnswerOrEmptyString("CardioPedalR").Clean() : string.Empty%>",<%= data.AnswerArray("CardioPedal").Contains("1").ToString().ToLower()%>)) +
        printview.col(2,
            printview.checkbox("Cyanosis(general)",<%= data.AnswerArray("CardioPulse").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Circumoral cyanosis",<%= data.AnswerArray("CardioPulse").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Edema<%=data.AnswerArray("CardioPulse").Contains("2")?":"+data.AnswerOrEmptyString("CardioPulseEdema").Clean():string.Empty%>",<%= data.AnswerArray("CardioPulse").Contains("2").ToString().ToLower()%>) +
            printview.span(""))+
            printview.span("Comments:",true)+
            printview.span("<%=data.AnswerOrEmptyString("CardioComments").Clean() %>",2),
            "Cardio");
    printview.addsubsection(
        printview.span("Muscle Tone:",true)+
        printview.col(3,
            printview.checkbox("Good",<%= data.AnswerArray("MUSCTone").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Fair",<%= data.AnswerArray("MUSCTone").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Poor",<%= data.AnswerArray("MUSCTone").Contains("2").ToString().ToLower()%>) +
            printview.checkbox("Relaxed",<%= data.AnswerArray("MUSCTone").Contains("3").ToString().ToLower()%>) +
            printview.checkbox("Flaccid",<%= data.AnswerArray("MUSCTone").Contains("4").ToString().ToLower()%>) +
            printview.checkbox("Spastic",<%= data.AnswerArray("MUSCTone").Contains("5").ToString().ToLower()%>) +
            printview.span("Extremity Movement:",true)+
            printview.checkbox("Normal",<%= data.AnswerArray("MUSCMovement").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Abnormal<%=data.AnswerArray("MUSCMovement").Contains("1")?":"+data.AnswerOrEmptyString("MUSCMovementAbnormal").Clean():string.Empty%>",<%= data.AnswerArray("MUSCMovement").Contains("1").ToString().ToLower()%>)) +
        printview.span("Comments/Problems:",true)+
        printview.span("<%=data.AnswerOrEmptyString("MUSCComments").Clean() %>",2),
        "Musc",3);
    printview.addsubsection(
        printview.span("Abdomen",true)+
        printview.col(3,
            printview.checkbox("Soft",<%= data.AnswerArray("GIAbdomen").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Flat",<%= data.AnswerArray("GIAbdomen").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Firm",<%= data.AnswerArray("GIAbdomen").Contains("2").ToString().ToLower()%>) +
            printview.checkbox("Tender",<%= data.AnswerArray("GIAbdomen").Contains("3").ToString().ToLower()%>) +
            printview.checkbox("Rigid",<%= data.AnswerArray("GIAbdomen").Contains("4").ToString().ToLower()%>) +
            printview.checkbox("Distended",<%= data.AnswerArray("GIAbdomen").Contains("5").ToString().ToLower()%>) +
            printview.checkbox("Cramping",<%= data.AnswerArray("GIAbdomen").Contains("6").ToString().ToLower()%>) +
            printview.span("")+
            printview.span(""))+
         printview.col(2,
            printview.span("Bowel sounds/patterns:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GIBowel").Clean() %>"))+
         printview.span("Comments",true)+
         printview.span("<%=data.AnswerOrEmptyString("GIComments").Clean() %>",2),
         "GI");
    printview.addsubsection(
        printview.span("Catheter:",true)+
        printview.col(2,
            printview.checkbox("Foley",<%= data.AnswerArray("GUCatheter").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Other<%=data.AnswerArray("GUCatheter").Contains("1")?":"+data.AnswerOrEmptyString("GUCatheterOther").Clean():string.Empty%>",<%= data.AnswerArray("GUCatheter").Contains("1").ToString().ToLower()%>) +
            printview.span("Type/Size:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GUTypeSize").Clean() %>")+
            printview.span("Last changed:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GULastChanged").Clean()%>"))+
        printview.span("Ostomy",true)+
        printview.col(2,
            printview.span("Type/Appliance:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GUTypeAppliance").Clean() %>"))+
        printview.col(3,
            printview.checkbox("Diapers",<%= data.AnswerArray("GUOstomy").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Training pants",<%= data.AnswerArray("GUOstomy").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Toilet trained",<%= data.AnswerArray("GUOstomy").Contains("2").ToString().ToLower()%>)) +
        printview.span("Comments:",true)+
        printview.span("<%=data.AnswerOrEmptyString("GUComments").Clean() %>",2),
        "GU");
    printview.addsubsection(
        printview.span("RESP:",true)+
        printview.col(3,
            printview.checkbox("Diminished<%=data.AnswerArray("PulmResp").Contains("0")?":"+data.AnswerOrEmptyString("PulmRespDiminished").Clean():string.Empty%>",<%= data.AnswerArray("PulmResp").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Wheezes<%=data.AnswerArray("PulmResp").Contains("1")?":"+data.AnswerOrEmptyString("PulmRespWheezes").Clean():string.Empty%>",<%= data.AnswerArray("PulmResp").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Crackles<%=data.AnswerArray("PulmResp").Contains("2")?":"+data.AnswerOrEmptyString("PulmRespCrackles").Clean():string.Empty%>",<%= data.AnswerArray("PulmResp").Contains("2").ToString().ToLower()%>)) +
        printview.span("cough",true)+
        printview.col(3,
            printview.checkbox("Non-productive",<%= data.AnswerArray("PulmCough").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Productive<%=data.AnswerArray("PulmCough").Contains("1")?":"+data.AnswerOrEmptyString("PulmCoughProductive").Clean():string.Empty%>",<%= data.AnswerArray("PulmCough").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Retraction",<%= data.AnswerArray("PulmCough").Contains("2").ToString().ToLower()%>) +
            printview.checkbox("Nasal flare",<%= data.AnswerArray("PulmCough").Contains("3").ToString().ToLower()%>) +
            printview.checkbox("Grunting",<%= data.AnswerArray("PulmCough").Contains("4").ToString().ToLower()%>) +
            printview.checkbox("Cyanosis",<%= data.AnswerArray("PulmCough").Contains("5").ToString().ToLower()%>) +
            printview.checkbox("Paradoxical excursions",<%= data.AnswerArray("PulmCough").Contains("6").ToString().ToLower()%>) +
            printview.span("",true)+
            printview.span("",true))+
        printview.col(2,
            printview.span("Apnea monitor:Settings:",true)+
            printview.span("<%=data.AnswerOrEmptyString("PulmCoughApnea").Clean() %>"))+
        printview.span("Oxygen Use:",true)+
        printview.col(3,
            printview.checkbox("Yes",<%= data.AnswerArray("PulmOxygenUse").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("No",<%= data.AnswerArray("PulmOxygenUse").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Liter/min<%=data.AnswerArray("PulmOxygenUse").Contains("2")?":"+data.AnswerOrEmptyString("PulmOxygenUseLiter").Clean():string.Empty%>",<%= data.AnswerArray("PulmOxygenUse").Contains("2").ToString().ToLower()%>)) +
        printview.span("Via:",true)+
        printview.col(3,
            printview.checkbox("Nasal cannula",<%= data.AnswerArray("PulmOxygenUseVia").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Tracheostomy",<%= data.AnswerArray("PulmOxygenUseVia").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Mask",<%= data.AnswerArray("PulmOxygenUseVia").Contains("2").ToString().ToLower()%>)) +
        printview.span("Pulmonary treatments:",true)+
        printview.col(3,
            printview.checkbox("Nebulizers",<%= data.AnswerArray("PulmPulmonary").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Nebulizers",<%= data.AnswerArray("PulmPulmonary").Contains("1").ToString().ToLower()%>) +
            printview.checkbox("Nebulizers",<%= data.AnswerArray("PulmPulmonary").Contains("2").ToString().ToLower()%>)),
        "Pulm",3);
    printview.addsubsection(
        printview.col(3,
            printview.span("Facial Expression:",true)+
            printview.checkbox("Relaxed",<%= data.AnswerArray("PAINFacialExpression").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Grimace",<%= data.AnswerArray("PAINFacialExpression").Contains("1").ToString().ToLower()%>)) +
        printview.col(4, 
            printview.span("Cry:",true)+
            printview.checkbox("No cry",<%= data.AnswerArray("PAINCry").Contains("0").ToString().ToLower()%>) +  
            printview.checkbox("Whimper",<%= data.AnswerArray("PAINCry").Contains("1").ToString().ToLower()%>) +  
            printview.checkbox("Vigorous",<%= data.AnswerArray("PAINCry").Contains("2").ToString().ToLower()%>)) + 
        printview.col(3,
            printview.span("Breathing patterns:",true)+
            printview.checkbox("Relaxed",<%= data.AnswerArray("PAINBreathing").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Changed",<%= data.AnswerArray("PAINBreathing").Contains("1").ToString().ToLower()%>) +
            printview.span("Arms:",true)+
            printview.checkbox("Relaxed",<%= data.AnswerArray("PAINArms").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Flexed/extended",<%= data.AnswerArray("PAINArms").Contains("1").ToString().ToLower()%>) +
            printview.span("Legs:",true)+
            printview.checkbox("Relaxed",<%= data.AnswerArray("PAINLegs").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Flexed/extended",<%= data.AnswerArray("PAINLegs").Contains("1").ToString().ToLower()%>) +
            printview.span("State of Arousal:",true)+
            printview.checkbox("Sleeping/awake",<%= data.AnswerArray("PAINState").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Fussy",<%= data.AnswerArray("PAINState").Contains("1").ToString().ToLower()%>)),
        "Pain: Under 3 years of age");
    printview.addsubsection(
        printview.col(2,
            printview.span("Pain Level:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericPainLevel").Clean()%>") +
            printview.span("Frequency:",true)+
            printview.span("<%= data.AnswerOrEmptyString("PAINFrequency").Clean()%>") +
            printview.span("Location:",true)+
            printview.span("<%= data.AnswerOrEmptyString("PAINLocation").Clean()%>") +
            printview.span("Pain relief measures:",true)+
            printview.span("<%= data.AnswerOrEmptyString("PAINReliefMeasures").Clean()%>")) +
        printview.col(3,
            printview.span("Effective:",true)+
            printview.checkbox("No",<%= data.AnswerArray("PAINEffective").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Yes",<%= data.AnswerArray("PAINEffective").Contains("1").ToString().ToLower()%>)) +
        printview.span("Comments",true)+
        printview.span("<%=data.AnswerOrEmptyString("PAINComments").Clean() %>",2),
        "Pain: Over 3 years of age");
    printview.addsubsection(
        printview.span("Significant Clinical Findings Relative to Diagnosis and Care Plan:",true)+
        printview.span("<%=data.AnswerOrEmptyString("GenericCarePlan").Clean() %>",3),
        "",3)
    printview.addsubsection(
        printview.span("Skilled Intervention or training provided:",true)+
        printview.col(2,
            printview.checkbox("No",<%= data.AnswerArray("SkilledInterventionProvided").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Yes",<%= data.AnswerArray("SkilledInterventionProvided").Contains("1").ToString().ToLower()%>)) +
        printview.span("Explain:",true)+
        printview.span("<%=data.AnswerOrEmptyString("SkilledInterventionExplain").Clean() %>",2),
        "")
    printview.addsubsection(
        printview.col(2,
            printview.span("Drug:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericDrug").Clean() %>")+
            printview.span("Dose:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericDose").Clean()%>")+
            printview.span("Route:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericRoute").Clean()%>")+
            printview.span("Time:",true)+
            printview.span("<%=data.AnswerOrEmptyString("GenericDrugTime").Clean()%>"))+
        printview.span("Case supervisory visit made:",true)+
        printview.col(2,
            printview.checkbox("No",<%= data.AnswerArray("SupervisoryVisit").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Yes",<%= data.AnswerArray("SupervisoryVisit").Contains("1").ToString().ToLower()%>)),
        "");        
    printview.addsection(
        printview.span("Narrative:",true)+
        printview.span("<%=data.AnswerOrEmptyString("Narrative").Clean() %>",3),
        "");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>