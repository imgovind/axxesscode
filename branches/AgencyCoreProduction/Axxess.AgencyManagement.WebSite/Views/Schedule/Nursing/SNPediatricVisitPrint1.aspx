﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions!=null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Skilled Nurse Pediatric Visit<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.LocationProfile.Name.Clean() %>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Skilled Nurse Pediatric Visit Note" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.PatientProfile.LastName.Clean()%>, <%= Model.PatientProfile.FirstName.Clean()%> <%= Model.PatientProfile.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.PatientProfile.DOB.ToZeroFilled().Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.PatientProfile.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.Data.StartDate.ToShortDateString().ToZeroFilled(), Model.Data.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PainProfile.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Skin.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Respiratory.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Cardiovascular.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Neurological.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Musculoskeletal.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Gastrointestinal.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Nutrition.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Genitourinary.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/DiabeticCare.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/IV.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/InfectionControl.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/CareCoordination.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/CarePlan.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/DischargePlanning.ascx", Model.Data); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Interventions.ascx", Model.Data); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Narrative.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Response.ascx", Model.Data); %>,2);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model.Data); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Phlebotomy.ascx", Model.Data); %>);
    <%  if (Model.Data.IsWoundCareExist) { %>
        <%  for (int i = 1; i < 6; i++) { %>
            <%  if (Model.Data.WoundCare.ContainsKey("GenericLocation" + i) && Model.Data.WoundCare["GenericLocation" + i].Answer.IsNotNullOrEmpty()) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Location:",true) +
                printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericLocation" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Onset Date:",true) +
                printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericOnsetDate" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Wound Type:",true) +
                printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericWoundType" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Pressure Ulcer Stage:",true) +
                printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericPressureUlcerStage" + i).Clean()%>",false,1)) +
            printview.col(2,
                printview.span("Measurements:",true) +
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericMeasurementLength" + i).Clean()%> cm",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Width:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericMeasurementWidth" + i).Clean()%> cm",false,1)) +
                printview.col(2,
                    printview.span("Depth:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericMeasurementDepth" + i).Clean()%> cm",false,1))) +
            printview.col(2,
                printview.span("Wound Bed:",true) +
                printview.col(2,
                    printview.span("Granulation %:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericWoundBedGranulation" + i).Clean()%>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Slough %:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericWoundBedSlough" + i).Clean()%>",false,1)) +
                printview.col(2,
                    printview.span("Eschar %:") +
                    printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericWoundBedEschar" + i).Clean()%>",false,1))) +
            printview.col(2,
                printview.span("<strong>Surrounding Tissue</strong>: <%= Model.Data.WoundCare.AnswerOrDefault("GenericSurroundingTissue" + i, "<span class='blank' style='width:4.9em !important;'></span>").Clean()%>") +
                printview.span("<strong>Drainage</strong>: <%= Model.Data.WoundCare.AnswerOrDefault("GenericDrainage" + i, "<span class='blank' style='width:9.9em !important;'></span>").Clean()%>")) +
            printview.col(2,
                printview.span("<strong>Drainage Amount</strong>: <%= Model.Data.WoundCare.AnswerOrDefault("GenericDrainageAmount" + i, "<span class='blank' style='width:5.8em !important;'></span>").Clean() %>") +
                printview.span("<strong>Odor</strong>: <%= Model.Data.WoundCare.AnswerOrDefault("GenericOdor" + i, "<span class='blank' style='width:11.8em !important;'></span>").Clean()%>"))) +
        printview.col(3,
            printview.col(3,
                printview.span("Tunneling:",true) +
                printview.span("Length: <%= Model.Data.WoundCare.AnswerOrDefault("GenericTunnelingLength" + i, "<span class='blank' style='width:3.2em !important;'></span>").Clean()%> cm") +
                printview.span("Time: <%= Model.Data.WoundCare.AnswerOrDefault("GenericTunnelingTime" + i, "<span class='blank' style='width:4.4em !important;'></span>").Clean()%>")) +
            printview.col(3,
                printview.span("Undermining:",true) +
                printview.span("Length: <%= Model.Data.WoundCare.AnswerOrDefault("GenericUnderminingLength" + i, "<span class='blank' style='width:3.2em !important;'></span>").Clean()%>") +
                printview.span("Time: <%= Model.Data.WoundCare.AnswerOrDefault("GenericUnderminingTimeFrom" + i, "<span class='blank' style='width:2em !important;'></span>").Clean()%> to <%= Model.Data.WoundCare.AnswerOrDefault("GenericUnderminingTimeTo" + i, "<span class='blank' style='width:2em !important;'></span>").Clean()%> o&#8217;clock")) +
            printview.col(3,
                printview.span("Device:",true) +
                printview.span("Type: <%= Model.Data.WoundCare.AnswerOrDefault("GenericDeviceType" + i, "<span class='blank' style='width:4em !important;'></span>").Clean()%>") +
                printview.span("Setting: <%= Model.Data.WoundCare.AnswerOrDefault("GenericDeviceSetting" + i, "<span class='blank' style='width:3em !important;'></span>").Clean() %>"))),
        "Wound <%= i %>",1);
            <%  } %>
        <%  } %>
    printview.addsection(
        printview.span("Treatment Performed:",true) +
        printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericTreatmentPerformed").Clean() %>",false,2) +
        printview.span("Narrative:",true) +
        printview.span("<%= Model.Data.WoundCare.AnswerOrEmptyString("GenericNarrative").Clean() %>",false,2));
    <%  } %>
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>