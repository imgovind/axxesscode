﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions!=null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>Lab Note<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>


<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.LocationProfile.Name.Clean() %>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Lab Note" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model.Data); %>,1);
        printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>