﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<% var noteName = Model.DisciplineTask.ToEnum<DisciplineTasks>(DisciplineTasks.SNVPsychNurse).GetDescription(); %>
<span class="wintitle"><%= noteName %> Note | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId, new { @id = Model.Type + "_EpisodeId" })%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId, new { @id = Model.Type + "_Id" })%>
    <%= Html.Hidden("Type", Model.Type)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    <%= noteName %> Note
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2">
                    <span class="bigtext"><%= Model.DisplayName %> (<%= Model.MRN %>)</span>
                </td>
                <td>
                    <%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, Model.PatientId, new { @id = Model.Type + "_DisciplineTask", @class = "requireddropdown" })%>
                </td>
                <td>
                <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
                <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate.IsValidDate() ? Model.VisitDate : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeIn", data.AnswerOrEmptyString("TimeIn"), new { @id = Model.Type + "_TimeIn", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_TimeOut", data.AnswerOrEmptyString("TimeOut"), new { @id = Model.Type + "_TimeOut", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
                <td colspan="2">
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <div>
                            <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                            <div class="float-right"><%= Html.TogglePreviousNotes("PreviousNotes", Model.Type + "_PreviousNotes")%></div>
                        </div>
                        <div class="clear"></div>
                    <%  } %>
                     <div>
                        <label for="<%= Model.Type %>_LastVisitDate" class="float-left">Last Physician Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_LastVisitDate" value="<%= data.AnswerOrEmptyString("LastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("LastVisitDate") : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>"  id="<%= Model.Type %>_LastVisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                             <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("~/Views/Schedule/Nursing/SNPsychVisit/ContentRev1.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician", @class = "complete-required" }) %></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="buttons">
                     <ul>
    <%  if (Model.IsSupplyExist) { %>
                        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Edit Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
    <%  } else { %>
                        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Add Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
    <%  } %>
                        <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
                        <li style="float: right;"><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDayScheduleModal('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');" title="Schedule Supervisory Visit">Schedule Supervisory Visit</a></li>
                        <%  } %>
                        </ul>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="SNPsychVisit_Button" />
	<% Html.RenderPartial("NoteBottomButtons", Model); %>
</div>
<%  } %>
