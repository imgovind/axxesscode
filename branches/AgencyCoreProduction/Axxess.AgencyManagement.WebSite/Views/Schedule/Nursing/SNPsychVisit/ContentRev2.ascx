﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Vital Signs</th>
            <th>Pain Profile</th>
            <th>Mental Status</th>
            <th>Patient/Family Teachings</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/PainProfile.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/MentalStatus.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/PatientFamilyTeachings.ascx", Model); %></td>
        </tr>
        <tr><%--G.I. Bowel Functions--%>
            <th>Mood/Affect</th>
            <th>Homebound Status</th>
            <th>Communication</th>
            <th>ADL Level</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/MoodAffect.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/HomeBoundStatus.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Communication.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/ADL.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Care Coordination</th>
            <th>Care Plan</th>
            <th>Discharge Planning</th>
            <th>Rapport</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CareCoordination.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CarePlan.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DischargePlanning.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Rapport.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Nutrition Status</th>
            <th colspan="2">G.I. Bowel Functions</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/NutritionStatus.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/GIBowelFunctions.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Supervisory Visit &#8212;
                <%  string[] isSupervisoryApplied = data.AnswerArray("GenericIsSupervisoryApplied"); %>
                <%= Html.Hidden(Model.Type + "isSupervisoryApplied", string.Empty, new { @id = Model.Type + "_GenericIsSupervisoryAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsSupervisoryApplied1' name='{0}_GenericIsSupervisoryApplied' value='1' type='checkbox' {1} />", Model.Type, isSupervisoryApplied.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>isSupervisoryApplied1">N/A</label></th>
            <th colspan="2">Response to Teaching/Interventions</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Supervisory.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Response.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Interventions</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/PsychiatricInterventions.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Goals &#8212;
                <%  string[] isGoalsApplied = data.AnswerArray("GenericIsGoalsApplied"); %>
                <%= Html.Hidden(Model.Type + "isGoalsApplied", string.Empty, new { @id = Model.Type + "_GenericIsGoalsAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsGoalsApplied1' name='{0}_GenericIsGoalsApplied' value='1' type='checkbox' {1} />", Model.Type, isGoalsApplied.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>isGoalsApplied1">N/A</label></th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Goals.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Rehab Potential</th>
        </tr>
        <tr>
            <td colspan="4">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input type='radio' class='radio' value='0' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialFair' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericRehabPotentialFair">Fair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='radio' class='radio' value='1' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialGood' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericRehabPotentialGood">Good</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='radio' class='radio' value='2' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialExcellent' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("2").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericRehabPotentialExcellent">Excellent</label>
                    </div>
                </div>
            </td>
            
        </tr>
        <tr>
            <th colspan="4">Narrative & Teaching</th>
        </tr>
        <tr>
            <td colspan="4">
                <div class="align-center">
                    <%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates", "", "#" + Model.Type + "_GenericNarrativeComment")%>
                    <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 4, 20, new { @class = "fill", @id = Model.Type + "_GenericNarrativeComment" })%>
                </div>
            </td>
        </tr>
         <tr>
            <th colspan="4">Physician Communication</th>
        </tr>
        <tr>
            <td colspan="4">
                <div class="align-center">
                    <%= Html.ToggleTemplates(Model.Type + "_GenericPhysicianCommunicationTemplates", "", "#" + Model.Type + "_GenericPhysicianCommunication")%>
                    <%= Html.TextArea(Model.Type + "_GenericPhysicianCommunication", data.AnswerOrEmptyString("GenericPhysicianCommunication"), 4, 20, new { @class = "fill", @id = Model.Type + "_GenericPhysicianCommunication" })%>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    U.HideOptions();
    U.HideIfChecked(
        $("#SNVPsychNurse_GenericIsGoalsApplied1"),
        $("#SNVPsychNurse_GoalsContainer"));
    U.HideIfChecked(
        $("#SNVPsychNurse_GenericIsSupervisoryApplied1"),
        $("#SNVPsychNurse_SupervisoryContainer"));
</script>