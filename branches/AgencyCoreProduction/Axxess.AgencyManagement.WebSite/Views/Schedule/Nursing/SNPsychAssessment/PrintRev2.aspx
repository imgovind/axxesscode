﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %><%
var data = Model != null && Model.Data.Questions!=null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + " | " : "" %>SN Psychiatric Assessment<%= Model.PatientProfile != null ? (" | " + Model.PatientProfile.LastName + ", " + Model.PatientProfile.FirstName + " " + Model.PatientProfile.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name.Clean() + "%3Cbr /%3E" : ""%><%= Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean().ToTitleCase() : ""%><%= Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.Clean().ToUpper() + "&#160; " : ""%><%= Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "SN Psychiatric Assessment" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.PatientProfile.LastName.Clean()%>, <%= Model.PatientProfile.FirstName.Clean()%> <%= Model.PatientProfile.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.PatientProfile.DOB.ToZeroFilled().Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.PatientProfile.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.Data.StartDate.ToShortDateString().ToZeroFilled(), Model.Data.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
         "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician: %3C/strong%3E" +
        "<%= data != null && Model.Data.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.Data.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Appearance.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MotorActivity.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Speech.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/FlowOfThought.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MoodAffectAssessment.ascx", Model.Data); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Sensorium.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Intellect.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/InsightAndJudgment.ascx", Model.Data); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/InterviewBehavior.ascx", Model.Data); %>, 2);
    printview.addspliltablesubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/ContentOfThought.ascx", Model.Data); %>);
    
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/PsychiatricInterventions.ascx", Model.Data); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Goals.ascx", Model.Data); %>);
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/RehabPotential.ascx", Model.Data); %>);
    
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericComments").Clean() %>", false), "Comments/Narrative");
    printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model.Data); %>);
              
    printview.addsection(
    printview.col(2,
        printview.span("Physician Signature", 1) +
        printview.span("Date", 1) +
        printview.span("<%= Model.Data.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.Data.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
        printview.span("<%= Model.Data.PhysicianSignatureDate.IsValid() ? Model.Data.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));

    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.Data.SignatureDate.IsNotNullOrEmpty() && Model.Data.SignatureDate != "1/1/0001" ? Model.Data.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>