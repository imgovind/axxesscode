﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PrintViewData<VisitNotePrintViewData>>" %>
<%  var data = Model != null && Model.Data.Questions != null ? Model.Data.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/UAP/woundcare.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>


<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.LocationProfile != null ? (Model.LocationProfile.Name.IsNotNullOrEmpty() ? Model.LocationProfile.Name + "<br />" : "") + (Model.LocationProfile != null ? (Model.LocationProfile.AddressLine1.IsNotNullOrEmpty() ? Model.LocationProfile.AddressLine1.Clean() : "") + (Model.LocationProfile.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.LocationProfile.AddressLine2.Clean() + "<br />" : "<br />") + (Model.LocationProfile.AddressCity.IsNotNullOrEmpty() ? Model.LocationProfile.AddressCity.Clean() + ", " : "") + (Model.LocationProfile.AddressStateCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressStateCode.ToUpper() + "  " : "") + (Model.LocationProfile.AddressZipCode.IsNotNullOrEmpty() ? Model.LocationProfile.AddressZipCode.Clean() : "") + (Model.LocationProfile.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + Model.LocationProfile.PhoneWorkFormatted : "") + (Model.LocationProfile.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.LocationProfile.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= (Model != null && Model.PatientProfile != null ? (Model.PatientProfile.LastName.IsNotNullOrEmpty() ? Model.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.PatientProfile.FirstName.IsNotNullOrEmpty() ? Model.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? Model.PatientProfile.MiddleInitial.ToUpper() : "") : "").Clean() %>",
            "mr": "<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber.Clean() : "" %>",
            "episode": "<%= data != null && Model.Data.StartDate.IsValid() && Model.Data.EndDate.IsValid() ? string.Format(" {0} &#8211; {1}", Model.Data.StartDate.ToShortDateString(), Model.Data.EndDate.ToShortDateString()).Clean() : "" %>",
            "visitdate": "<%= data != null && data.ContainsKey("VisitDate") && data["VisitDate"].Answer.IsNotNullOrEmpty() ? data["VisitDate"].Answer : string.Empty %>",
            "timein": "<%= data != null && data.ContainsKey("TimeIn") && data["TimeIn"].Answer.IsNotNullOrEmpty() ? data["TimeIn"].Answer : string.Empty %>",
            "timeout": "<%= data != null && data.ContainsKey("TimeOut") && data["TimeOut"].Answer.IsNotNullOrEmpty() ? data["TimeOut"].Answer : string.Empty %>",
            "sign": "<%= Model != null && Model.Data.SignatureText.IsNotNullOrEmpty() ? Model.Data.SignatureText : string.Empty %>",
            "signdate": "<%= Model != null && Model.Data.SignatureDate != null && Model.Data.SignatureDate.ToDateTime().IsValid() ? Model.Data.SignatureDate : string.Empty %>",
            "mileage": "<%= data != null && data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer.Clean() : ""%>",
            "surcharge": "<%= data != null && data.ContainsKey("Surcharge") ? data["Surcharge"].Answer.Clean() : ""%>"
        };
        PdfPrint.BuildSections(<%= Model.Data.PrintViewJson  %>);
<%  }).Render(); %>
</body>
</html>