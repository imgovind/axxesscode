﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteEditViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] cleansedWith = data.AnswerArray("CleansedWith"); %>
<%  string[] rinsedWith = data.AnswerArray("RinsedWith"); %>
<%  string[] patDryWith = data.AnswerArray("PatDryWith"); %>
<%  string[] medicationApplied = data.AnswerArray("MedicationApplied"); %>
<%  string[] dressingApplied = data.AnswerArray("DressingApplied"); %>
<%  string[] dressingSecured = data.AnswerArray("DressingSecured"); %>
<%  string[] patientUnable = data.AnswerArray("PatientUnable"); %>
<%  string[] supplies = data.AnswerArray("Supplies"); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%  string[] isVitalSigns = data.AnswerArray("IsVitalSigns"); %>
<input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
<input name="<%= Model.Type %>_IsVitalSigns" value=" " type="hidden" />
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>
                Vital Sign Parameters &#8212;
                <%= string.Format("<input class='radio' id='{0}_IsVitalSignParameter' name='{0}_IsVitalSignParameter' value='1' type='checkbox' {1} />", Model.Type, isVitalSignParameter.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
            </th>
            <th>
                Vital Signs &#8212;
                <%= string.Format("<input class='radio' id='{0}_IsVitalSigns' name='{0}_IsVitalSigns' value='1' type='checkbox' {1} />", Model.Type, isVitalSigns.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_IsVitalSigns">N/A</label>
            </th>
        </tr>
        <tr>
            <td>
                <table id="<%= Model.Type %>_IsVitalSignParameterMore" class="fixed vitalsigns">
                    <tbody>
                        <tr>
                            <th></th>
                            <th>SBP</th>
                            <th>DBP</th>
                            <th>HR</th>
                            <th>Resp</th>
                            <th>Temp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <th>greater than (&#62;)</th>
                            <td><%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.AnswerOrEmptyString("SystolicBPGreaterThan"), new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.AnswerOrEmptyString("DiastolicBPGreaterThan"), new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.AnswerOrEmptyString("PulseGreaterThan"), new { @id = Model.Type + "_PulseGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.AnswerOrEmptyString("RespirationGreaterThan"), new { @id = Model.Type + "_RespirationGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_TempGreaterThan", data.AnswerOrEmptyString("TempGreaterThan"), new { @id = Model.Type + "_TempGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.AnswerOrEmptyString("WeightGreaterThan"), new { @id = Model.Type + "_WeightGreaterThan", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <th>less than (&#60;)</th>
                            <td><%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.AnswerOrEmptyString("SystolicBPLessThan"), new { @id = Model.Type + "_SystolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.AnswerOrEmptyString("DiastolicBPLessThan"), new { @id = Model.Type + "_DiastolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_PulseLessThan", data.AnswerOrEmptyString("PulseLessThan"), new { @id = Model.Type + "_PulseLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_RespirationLessThan", data.AnswerOrEmptyString("RespirationLessThan"), new { @id = Model.Type + "_RespirationLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_TempLessThan", data.AnswerOrEmptyString("TempLessThan"), new { @id = Model.Type + "_TempLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_WeightLessThan", data.AnswerOrEmptyString("WeightLessThan"), new { @id = Model.Type + "_WeightLessThan", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table id="<%= Model.Type %>_IsVitalSignsMore" class="fixed vitalsignparameter">
                    <tbody>
                        <tr>
                            <th>SBP</th>
                            <th>DBP</th>
                            <th>HR</th>
                            <th>Resp</th>
                            <th>Temp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignSBPVal", data.AnswerOrEmptyString("VitalSignSBPVal"), new { @id = Model.Type + "_VitalSignSBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignDBPVal", data.AnswerOrEmptyString("VitalSignDBPVal"), new { @id = Model.Type + "_VitalSignDBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignHRVal", data.AnswerOrEmptyString("VitalSignHRVal"), new { @id = Model.Type + "_VitalSignHRVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignRespVal", data.AnswerOrEmptyString("VitalSignRespVal"), new { @id = Model.Type + "_VitalSignRespVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignTempVal", data.AnswerOrEmptyString("VitalSignTempVal"), new { @id = Model.Type + "_VitalSignTempVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignWeightVal", data.AnswerOrEmptyString("VitalSignWeightVal"), new { @id = Model.Type + "_VitalSignWeightVal", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Wound Care (Aseptic Technique and Sterile Supplies)</th>
        </tr>
        <tr>
            <td>
                <div class="align-left strong">Cleansed With:</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_CleansedWith1' class='float-left radio' name='{0}_CleansedWith' value='1' type='checkbox' {1} />", Model.Type, cleansedWith.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_CleansedWith1" class="radio">Sterile NS</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_CleansedWith2' class='float-left radio' name='{0}_CleansedWith' value='2' type='checkbox' {1} />", Model.Type, cleansedWith.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_CleansedWith2" class="radio">Shur Clens</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_CleansedWith3' class='float-left radio' name='{0}_CleansedWith' value='3' type='checkbox' {1} />", Model.Type, cleansedWith.Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_CleansedWith3" class="radio">Wound Cleanser</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="strong">Rinsed With:</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_RinsedWith1' class='float-left radio' name='{0}_RinsedWith' value='1' type='checkbox' {1} />", Model.Type, rinsedWith.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_RinsedWith1" class="radio">Sterile NS</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_RinsedWith2' class='float-left radio' name='{0}_RinsedWith' value='2' type='checkbox' {1} />", Model.Type, rinsedWith.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_RinsedWith2" class="radio">Sterile H20</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="strong">Pat Dry With:</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_PatDryWith1' class='float-left radio' name='{0}_PatDryWith' value='1' type='checkbox' {1} />", Model.Type, patDryWith.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PatDryWith1" class="radio">Sterile 4x4</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_PatDryWith2' class='float-left radio' name='{0}_PatDryWith' value='2' type='checkbox' {1} />", Model.Type, patDryWith.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PatDryWith2" class="radio">Sterile 2x2</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-left strong">Medication Applied:</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied1' class='float-left radio' name='{0}_MedicationApplied' value='1' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied1" class="radio">No Medication Applied</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied2' class='float-left radio' name='{0}_MedicationApplied' value='2' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied2" class="radio">Triple Antibiotic Ointment</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied3' class='float-left radio' name='{0}_MedicationApplied' value='3' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied3" class="radio">Bactroban</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied4' class='float-left radio' name='{0}_MedicationApplied' value='4' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied4" class="radio">Bacitracin</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied5' class='float-left radio' name='{0}_MedicationApplied' value='5' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied5" class="radio">Neosporin</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_MedicationApplied6' class='float-left radio' name='{0}_MedicationApplied' value='6' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("6").ToChecked())%>
                                <label for="<%= Model.Type %>_MedicationApplied6" class="radio">Wound Gel</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="float-left">
                                    <%= string.Format("<input id='{0}_MedicationApplied7' class='float-left radio' name='{0}_MedicationApplied' value='7' type='checkbox' {1} />", Model.Type, medicationApplied.Contains("7").ToChecked())%>
                                    <label for="<%= Model.Type %>_MedicationApplied7" class="radio">Other</label>
                                </div>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_MedicationAppliedOther", data.AnswerOrEmptyString("MedicationAppliedOther"), new { @id = Model.Type + "_MedicationAppliedOther" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-left strong">Dressing Applied</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied1' class='float-left radio' name='{0}_DressingApplied' value='1' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied1" class="radio">Telfa</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied2' class='float-left radio' name='{0}_DressingApplied' value='2' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied2" class="radio">Dry 4x4 Gauze</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied3' class='float-left radio' name='{0}_DressingApplied' value='3' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied3" class="radio">Dry 2x2 Gauze</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied4' class='float-left radio' name='{0}_DressingApplied' value='4' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied4" class="radio">Wet to Dry with NS</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied5' class='float-left radio' name='{0}_DressingApplied' value='5' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied5" class="radio">Fluffs</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied6' class='float-left radio' name='{0}_DressingApplied' value='6' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("6").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied6" class="radio">Kerlix</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied7' class='float-left radio' name='{0}_DressingApplied' value='7' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("7").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied7" class="radio">Kling</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied8' class='float-left radio' name='{0}_DressingApplied' value='8' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("8").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied8" class="radio">Duoderm</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingApplied9' class='float-left radio' name='{0}_DressingApplied' value='9' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("9").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingApplied9" class="radio">Ace Wrap</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="float-left">
                                    <%= string.Format("<input id='{0}_DressingApplied10' class='float-left radio' name='{0}_DressingApplied' value='10' type='checkbox' {1} />", Model.Type, dressingApplied.Contains("10").ToChecked())%>
                                    <label for="<%= Model.Type %>_DressingApplied10" class="radio">Other</label>
                                </div>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_DressingAppliedOther", data.AnswerOrEmptyString("DressingAppliedOther"), new { @id = Model.Type + "_DressingAppliedOther" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <div class="align-left strong">Dressing Secured With:</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_DressingSecured1' class='float-left radio' name='{0}_DressingSecured' value='1' type='checkbox' {1} />", Model.Type, dressingSecured.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingSecured1" class="radio">Micropore tape</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_DressingSecured2' class='float-left radio' name='{0}_DressingSecured' value='2' type='checkbox' {1} />", Model.Type, dressingSecured.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_DressingSecured2" class="radio">Medifix</label>
                            </td>
                            <td>
                                <div class="float-left">
                                    <%= string.Format("<input id='{0}_DressingSecured3' class='float-left radio' name='{0}_DressingSecured' value='3' type='checkbox' {1} />", Model.Type, dressingSecured.Contains("3").ToChecked())%>
                                    <label for="<%= Model.Type %>_DressingSecured3" class="radio">Other</label>
                                </div>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_DressingSecuredOther", data.AnswerOrEmptyString("DressingSecuredOther"), new { @id = Model.Type + "_DressingSecuredOther" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-left strong">Patient Unable to Perform Treatment due to</div>
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_PatientUnable1' class='float-left radio' name='{0}_PatientUnable' value='1' type='checkbox' {1} />", Model.Type, patientUnable.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_PatientUnable1" class="radio">Unable to Visualize Site</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_PatientUnable2' class='float-left radio' name='{0}_PatientUnable' value='2' type='checkbox' {1} />", Model.Type, patientUnable.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_PatientUnable2" class="radio">Poor Manual Dexterity</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_PatientUnable3' class='float-left radio' name='{0}_PatientUnable' value='3' type='checkbox' {1} />", Model.Type, patientUnable.Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_PatientUnable3" class="radio">Unable to Reach Site Due to Limited Functional Mobility</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_PatientUnable4' class='float-left radio' name='{0}_PatientUnable' value='4' type='checkbox' {1} />", Model.Type, patientUnable.Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_PatientUnable4" class="radio">No Caregiver willing/available to assist</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_PatientUnable5' class='float-left radio' name='{0}_PatientUnable' value='5' type='checkbox' {1} />", Model.Type, patientUnable.Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_PatientUnable5" class="radio">Standard Universal Precautions Maintained.</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>RN Notification</th>
            <th>MD Notification</th>
        </tr>
        <tr>
            <td>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_ClinicianNotified' name='{0}_ClinicianNotified' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ClinicianNotified").IsEqual("Yes").ToChecked())%>
                                <label for="<%= Model.Type %>_ClinicianNotified" class="radio">RN Notified</label>
                            </td>
                            <td>
                                <div class="float-right"><%= Html.Clinicians(Model.Type + "_ClinicianId", data.AnswerOrEmptyString("ClinicianId"), new { @id = Model.Type + "_ClinicianId", @class = "fill" })%></div>
                            </td>
                            <td>
                                <div class="float-right"><input type="text" class="date-picker shortdate" name="<%= Model.Type %>_ClinicianNotifiedDate" value="<%= data.AnswerOrEmptyString("ClinicianNotifiedDate") %>" id="<%= Model.Type %>_ClinicianNotifiedDate" /></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <label for="<%= Model.Type %>_InstructionsGiven" class="strong">Instructions Given:</label>
                                <div class="align-center"><%= Html.TextArea(Model.Type + "_InstructionsGiven", data.AnswerOrEmptyString("InstructionsGiven"), new { @class = "fill", @id = Model.Type + "_InstructionsGiven" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_DoctorNotified' name='{0}_DoctorNotified' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("DoctorNotified").IsEqual("Yes").ToChecked())%>
                                <label for="<%= Model.Type %>_DoctorNotified" class="radio">MD Notified</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_NewOrders' name='{0}_NewOrders' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NewOrders").IsEqual("Yes").ToChecked())%>
                                <label for="<%= Model.Type %>_NewOrders" class="radio">New Order(s)</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Supplies</th>
        </tr>
        <tr>
            <td colspan="2">
                <table class="align-left fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies1' class='float-left radio' name='{0}_Supplies' value='1' type='checkbox' {1} />", Model.Type, supplies.Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies1" class="radio">Gloves</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies2' class='float-left radio' name='{0}_Supplies' value='2' type='checkbox' {1} />", Model.Type, supplies.Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies2" class="radio">N/S</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies3' class='float-left radio' name='{0}_Supplies' value='3' type='checkbox' {1} />", Model.Type, supplies.Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies3" class="radio">Wound Cleanser</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies4' class='float-left radio' name='{0}_Supplies' value='4' type='checkbox' {1} />", Model.Type, supplies.Contains("4").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies4" class="radio">4x4s</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies5' class='float-left radio' name='{0}_Supplies' value='5' type='checkbox' {1} />", Model.Type, supplies.Contains("5").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies5" class="radio">2x2s</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies6' class='float-left radio' name='{0}_Supplies' value='6' type='checkbox' {1} />", Model.Type, supplies.Contains("6").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies6" class="radio">CTA</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies7' class='float-left radio' name='{0}_Supplies' value='7' type='checkbox' {1} />", Model.Type, supplies.Contains("7").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies7" class="radio">Kerlix</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies8' class='float-left radio' name='{0}_Supplies' value='8' type='checkbox' {1} />", Model.Type, supplies.Contains("8").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies8" class="radio">Tape</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies9' class='float-left radio' name='{0}_Supplies' value='9' type='checkbox' {1} />", Model.Type, supplies.Contains("9").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies9" class="radio">Probe Covers</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{0}_Supplies10' class='float-left radio' name='{0}_Supplies' value='10' type='checkbox' {1} />", Model.Type, supplies.Contains("10").ToChecked())%>
                                <label for="<%= Model.Type %>_Supplies10" class="radio">Alcohol pads</label>
                            </td>
                            <td colspan="2">
                                <div class="float-left">
                                    <%= string.Format("<input id='{0}_Supplies11' class='float-left radio' name='{0}_Supplies' value='11' type='checkbox' {1} />", Model.Type, supplies.Contains("11").ToChecked())%>
                                    <label for="<%= Model.Type %>_Supplies11" class="radio">Other</label>
                                </div>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_SuppliesOther", data.AnswerOrEmptyString("SuppliesOther"), new { @id = Model.Type + "_SuppliesOther" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Comments</th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.ToggleTemplates(Model.Type + "_CommentTemplates", "", "#" + Model.Type + "_Comment")%>
                <%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "fill" })%>
            </td>
        </tr>
    </tbody>
</table>