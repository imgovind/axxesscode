﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeLean>" %>
<div class="wrapper">
    <div class="grid-bg">
        <fieldset style="width:720px; margin:0.5em auto">
            <div class="column">
                <div class="row">
                    <span class="strong">Patient: <%= Model.DisplayName%></span>
                </div>
                <div class="row">
                    <div class="float-left">
                        <input id="DeleteMultiple_SelectAll" type="checkbox" class="radio" />&#160;<label for="DeleteMultiple_SelectAll">Check/Uncheck All</label>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <span class="strong">Episode: <%= Model.StartDate.ToZeroFilled() %> -  <%= Model.EndDate.ToZeroFilled() %></span>
                </div>
            </div>
        </fieldset>
		<% Html.Telerik().Grid<ScheduleEvent>().Name("DeleteMultipleActivityGrid").HtmlAttributes(new { @style = "top:100px; bottom:40px;" }).Columns(columns =>
		   {
			   columns.Bound(s => s.Id).ClientTemplate("<input name='EventId' type='checkbox' value='<#= Id #>' />").Title("").Sortable(false).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Width(20);
			   columns.Bound(s => s.DisciplineTaskName).Title("Task").Width(140);
			   columns.Bound(s => s.EventDateSortable).ClientTemplate("<#=EventDateSortable#>").Title("Scheduled Date").Width(80);
			   columns.Bound(s => s.UserName).Title("Assigned To").Width(110);
			   columns.Bound(s => s.StatusName).Title("Status").Width(100);
			   columns.Bound(s => s.IsComplete).Visible(false);
		   }).ClientEvents(c => c.OnRowSelect("Schedule.DeleteActivityRowSelected")).DataBinding(dataBinding => dataBinding.Ajax().Select("DeleteList", "Schedule", new { episodeId = Model.Id, patientId = Model.PatientId }))
		   .Sortable().Selectable().Scrollable().Footer(false).Render();
		%>
		<div id="DeleteMultiple_BottomPanel" style="position:absolute;right:0;bottom:0;left:0">
			<div class="buttons">
				<ul>
					<% if(!Current.IsAgencyFrozen) { %>
					<li>
						<a href='javascript:void(0);' onclick='Schedule.DeleteAction();'>Delete</a>
					</li>
					<% } %>
					<li>
						<a href="javascript:void(0);" class="close">Exit</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $("#window_scheduledelete_content").css({
        "background-color": "#d6e5f3"
    });
    $('#DeleteMultipleActivityGrid .t-grid-content').css({ "height": "auto", "position": "absolute", "top": "25px" });
    $("#DeleteMultiple_SelectAll").change(function(e) {
            var isChecked = $(this).prop("checked");
            $("#DeleteMultipleActivityGrid .t-grid-content tr input").each(function() {
                $(this).prop("checked", isChecked);
            });
        });
</script>