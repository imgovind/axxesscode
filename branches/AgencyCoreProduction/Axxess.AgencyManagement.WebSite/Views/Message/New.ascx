﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Message | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout messaging-container">
<%  using (Html.BeginForm("Create", "Message", FormMethod.Post, new { @id = "NewMessage_Form" })) { %>
    <div class="ui-layout-west"><%= Html.Recipients() %></div>
    <div class="compose message-content ui-layout-center">
        <div class="compose-header">
            <div class="buttons fr">
                <ul>
                    <li><a class="close">Cancel</a></li>
                </ul>
            </div>
            <span>New Message</span>
        </div>
        <div class="compose message-header-container">
            <div class="buttons abs">
                <ul>
                    <li class="send">
                        <a class="save">
                            <span class="img icon send"></span>
                            <br />Send
                        </a>
                        <li class="sending">
                            <img src="images/indicator.gif"/>
                            <br />
                            <em>Processing</em>
                        </li>
                    </li>
                </ul>
            </div>
            <div class="message-header-row">
                <label for="NewMessage_Recipents" class="strong">To</label>
                <div class="compose-input to">
                    <input type="text" id="NewMessage_Recipents" name="newMessageRecipents" />
                </div>
            </div>
            <div class="message-header-row">
                <label for="NewMessage_Subject" class="strong">Subject</label>
                <div class="compose-input">
                    <input type="text" id="NewMessage_Subject" name="Subject" maxlength="75" class="fill required" />
                </div>
            </div>
    <%  if (!Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer)) { %>
            <div class="message-header-row">
                <label for="NewMessage_PatientId" class="strong">Regarding</label>
                <div class="compose-input">
                    <%= Html.PatientsWithBranchAndStatus("PatientId", "", new { @id = "NewMessage_PatientId", @class = "fill" })%>
                </div>
            </div>
    <%  } %>
            <div class="message-header-row">
                <label for="NewMessage_Attachment" class="strong">Attachment</label>
                <div class="compose-input">
                    <input type="file" id="NewMessage_Attachment" name="Attachment1" class="fill" />
                </div>
            </div>
        </div>
        <div class="new-message-row" id="NewMessage_BodyWrapper">
            <textarea id="NewMessage_Body" name="Body"></textarea>
        </div>
    </div>
<%  } %>
</div>