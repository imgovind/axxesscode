﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main blue ac">
    <ul class="dropdown-button">
        <li>
            <a class="menu-trigger new-message">
                <span class="img icon email"></span>
                New Message
                <%-- <span class="ui-triangle-s"></span> --%>
            </a>
            <%-- <ul class="menu">
                <li class="hover-toggle new-message">Message</li>
                <li class="hover-toggle new-folder">Folder</li>
            </ul> --%>
        </li>
    </ul>
    <div class="folder-selection al">
        <span class="ui-triangle-s fr"></span>
        <select class="folder" name="message-folders"></select>
    </div>
    <div class="message-list al">
        <%  Html.Telerik().Grid<MessageItem>().Name("list-messages").Columns(columns => {
            columns.Bound(m => m.FromName).ClientTemplate(
                "<div id='<#=Id#>' type='<#=Type#>' uid='<#=UserMessageId#>' class='message read-<#=MarkAsRead#>'>" +
                    "<span class='fr'><#=Date#></span>" +
                    "<span><#=FromName#></span>" +
                    "<br />" +
                    "<span class='normal'><#=Subject#></span>" +
                "</div>").Title("Inbox");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Message", new { inboxType = "inbox" })).ClientEvents(events => events.OnDataBound("Message.OnMessageListReady").OnRowSelected("Message.LoadMessage")).Pageable(settings => {
            settings.Enabled(true);
            settings.PageSize(75);
            settings.Position(GridPagerPosition.Bottom);
        }).Sortable().Selectable().Footer(true).Scrollable(scrolling => scrolling.Enabled(true)).Render(); %>
    </div>
</div>