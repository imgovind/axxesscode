﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Message>" %>
<%  if (Model != null) { %>
<div class="message-content">
    <div class="window-menu">
        <ul>
    <%  if (Model.Type == MessageType.User || Model.Type == MessageType.Sent) { %>
            <li><a class="reply">Reply</a></li>
            <li><a class="reply-all">Reply to All</a></li>
            <li><a class="forward">Forward</a></li>
    <%  } %>
    <%  if (!Model.IsDeprecated && Model.Type != MessageType.Sent) { %><li><a class="delete">Delete</a></li><% } %>
            <li><a class="print">Print</a></li>
        </ul>
    </div>
    <div class="message-content-panel">
        <input type="hidden" id="MessageView_MessageId" value="<%= Model.Id %>" />
        <input type="hidden" id="MessageView_MessageType" value="<%= (int)Model.Type %>" />
        <input type="hidden" id="MessageView_FolderId" value="<%= Model.FolderId %>" />
        <div class="message-header-container">
            <div class="message-header-row">
                <label for="messageSender" class="strong">From</label>
                <span id="messageSender"><%= Model.FromName %></span>
            </div>
            <div class="message-header-row">
                <label for="messageRecipients" class="strong">To</label>
                <span id="messageRecipients"><%= Model.RecipientNames %></span>
            </div>
            <div class="message-header-row">
                <label for="messageDate" class="strong">Sent</label>
                <span id="messageDate"><%= Model.MessageDate %></span>
            </div>
            <div class="message-header-row">
                <label for="messageSubject" class="strong">Subject</label>
                <span id="messageSubject"><%= Model.Subject %></span>
            </div>
    <%  if ((Model.Type == MessageType.User) || (Model.Type == MessageType.Sent)) { %>
            <div class="message-header-row">
                <label for="messageRegarding" class="strong">Regarding</label>
                <span id="messageRegarding"><%= Model.PatientName %></span>
            </div>
    <%  } %>
    <%  if (Model.HasAttachment) { %>
            <div class="message-header-row">
                <label for="messageAttachment" class="strong">Attachment</label>
                <span id="messageAttachment"><%= Html.Asset(Model.AttachmentId) %></span>
            </div>
    <%  } %>
        </div>
        <div id="message-body-container" class="message-body-container">
            <div id="message-body">
                <%= Model.Body %>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#message-content .message-header-row").each(function() {
        if ($(this).find("span").prop("scrollHeight") > $(this).find("span").prop("offsetHeight")) $(this).append(unescape("%3Ca onclick=%22return false%22 class=%22more%22 tooltip=%22" + $(this).find("span").html() + "%22%3EMore&#187;%3C/a%3E"));
        $(this).find("span").css("overflow", "hidden")
    });
</script>
<% } else { %> 
<script type="text/javascript">
    $("#messageInfoResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Messages found." })).append(
            $("<div/>", { "class": "heading" }).Buttons([{
                Text: "Add New Message",
                Click: function() {
                    Message.New()
                }
            }])
        )
    );
</script>
<% } %>