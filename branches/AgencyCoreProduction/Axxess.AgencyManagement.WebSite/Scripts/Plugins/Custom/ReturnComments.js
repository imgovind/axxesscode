(function($) {
    $.extend($.fn, {
        //  Arguments:
        //      EventId         Id of the note that the return reason will belong to
        //      PatientId       Related patient's id
        //      EpisodeId       Related episode's id
        //      Comment         Comment to be inserted
        //      Reasons         Reasons that have already been pulled from the database

        //      ReturnFunction  The function to be called when returning a document
        ReturnComments: function(Arguments) {
            return this.each(function() {
                if (!$(this).hasClass("return-comments-container")) {
                    var Container = $(this), ButtonArray = new Array();
                    if (typeof Arguments.ReturnFunction == "function") ButtonArray.push({
                        Text: "Return",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("textarea", Container).val().length) {
                                var patientId = Container.attr("PatientId");
                                U.PostUrl("Schedule/AddReturnReason", { eventId: Container.attr("EventId"), episodeId: Container.attr("EpisodeId"), patientId: patientId, comment: $("textarea", Container).val() }, function(Result) {
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) {
                                        Arguments.ReturnFunction();
                                        Agency.RebindCaseManagement();
                                        Patient.Charts.Activities.Refresh(patientId);
                                        Schedule.Center.RefreshSchedule(patientId);
                                        User.RebindScheduleList();
                                        Container.closest(".window").Close();
                                    }
                                })
                            } else U.Growl("Please give a reason for returning this document.", "error");
                        }
                    });
                    else ButtonArray.push({
                        Text: "Add Comment",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("textarea", Container).val().length) {
                                var patientId = Container.attr("PatientId");
                                U.PostUrl("Schedule/AddReturnReason", { eventId: Container.attr("EventId"), episodeId: Container.attr("EpisodeId"), patientId: patientId, comment: $("textarea", Container).val() }, function(Result) {
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) {
                                        Container.ReturnComments("Refresh");
                                        $("textarea", Container).val("");
                                        Agency.RebindCaseManagement();
                                        Patient.Charts.Activities.Refresh(patientId);
                                        Schedule.Center.RefreshSchedule(patientId);
                                        User.RebindScheduleList();
                                    }
                                })
                            } else U.Growl("Please enter your comments that you would like to add.", "error");
                        }
                    });
                    if (Container.hasClass("main")) ButtonArray.push({
                        Text: "Close",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("#print-controls").length) $("ul", "#print-controls").show();
                            Container.closest(".window").Close();
                        }
                    });
                    Container.addClass("return-comments-container").attr({ EventId: Arguments.EventId, EpisodeId: Arguments.EpisodeId, PatientId: Arguments.PatientId }).append(
                        $("<div/>", { "class": "return-comments" })).append(
                        $("<textarea/>", { "class": "tall" }).val(Arguments.Comment)).append(
                        $("<div/>").Buttons(ButtonArray));
                    if (Arguments.Reasons == undefined) Container.ReturnComments("Refresh");
                    else {
                        $(".return-comments", Container).html(Arguments.Reasons);
                        Container.ReturnComments("BuildComments");
                    }
                } else if (Arguments == "Refresh") {
                    var Container = $(this);
                    U.PostUrl("Schedule/ReturnReason", { eventId: Container.attr("EventId"), episodeId: Container.attr("EpisodeId"), patientId: Container.attr("PatientId") }, function(Result) {
                        $(".return-comments", Container).html(Result.errorMessage).scrollTop($(".return-comments", Container).prop("scrollHeight"));
                        Container.ReturnComments("BuildComments");
                    })
                } else if (Arguments == "BuildComments") {
                    var Container = $(this);
                    $(".return-comments .edit-controls", Container).each(function() {
                        var CommentId = $(this).text(), Element = $(this).closest(".main");
                        $(this).html(
                            $("<span/>").addClass("img icon edit-comment").click(function() {
                                UserInterface.EditReturnComments(CommentId, $(this).parent().next().next().next().text(), Element)
                            })
                        ).append(
                            $("<span/>").DeleteIcon({ Size: '2em' }).addClass("delete-comment").click(function() {
                                if (confirm("Are you sure you want to delete this comment?")) U.PostUrl("Schedule/DeleteReturnReason", { id: CommentId }, function(Result) {
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) Container.ReturnComments("Refresh");
                                })
                            })
                        )
                    })
                }
            })
        }
    })
})(jQuery);