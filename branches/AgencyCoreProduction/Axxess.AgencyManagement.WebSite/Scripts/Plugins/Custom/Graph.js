// Style Options
var fontFamily = "Trebuchet MS, Verdana, sans-serif";
Highcharts.setOptions({
    colors: ["#058dc7", "#50b432", "#ed561b", "#dddf00", "#24cbe5", "#64e572", "#ff9655", "#fff263", "#6af9c4"],
    chart: {
        backgroundColor: {
            linearGradient: [0, 0, 0, "100%"],
            stops: [[0, "#dde8f5"], [1, "#c3d8f1"]]
        },
        borderWidth: 1,
        borderColor: "#888",
        plotBackgroundColor: "rgba(255, 255, 255, .25)",
        plotBorderWidth: 1
    },
    title: { style: { color: "#333", font: "bold 16px " + fontFamily} },
    subtitle: { style: { color: "#333", font: "bold 12px " + fontFamily} },
    xAxis: {
        gridLineWidth: 1,
        lineColor: "#333",
        tickColor: "#333",
        labels: { style: { color: "#333", font: "11px " + fontFamily} },
        title: { style: { color: "#333", fontWeight: "bold", fontSize: "12px", fontFamily: fontFamily } }
    },
    yAxis: {
        alternateGridColor: "rgba(255, 255, 255, .75)",
        lineColor: "#333",
        lineWidth: 1,
        tickWidth: 1,
        tickColor: "#333",
        labels: { style: { color: "#333", font: "11px" + fontFamily } },
        title: { style: { color: "#333", fontWeight: "bold", fontSize: "12px", fontFamily: fontFamily } }
    },
    legend: {
        itemStyle: { font: "9pt" + fontFamily, color: "#333" },
        itemHoverStyle: { color: "#039" },
        itemHiddenStyle: { color: "gray" }
    },
    labels: { style: { color: "#333" } }
});
// Define Plugin
(function($) {
    var graphMethods = {
        init: function(options) {
            this.each(function() {
                var e = $(this);
                // If already initialized, destroy existing chart
                if (e.hasClass("acore-graph")) e.data("Graph").destroy();
                // If does not have ID, create one
                if (e.attr("id") == undefined) e.attr("id", "AcoreGraph_" + new Date().getTime());
                e.addClass("acore-graph").data("Graph", new Highcharts.Chart({
                        chart: { renderTo: e.attr("id"), type: options.Type, marginRight: 130, marginBottom: 25 },
                        credits: { enabled: false },
                        title: { text: options.Title, x: -20 },
                        subtitle: { text: options.Subtitle, x: -20 },
                        xAxis: { categories: options.XAxisData },
                        yAxis: {
                            title: { text: options.YAxisTitle },
                            plotLines: [{ value: 0, width: 1, color: "#808080"}]
                        },
                        tooltip: {
                            formatter: function() {
                                return "<b>" + this.series.name + "</b><br/>" + this.x + ": " + this.y + (options.YAxisSuffix != undefined ? options.YAxisSuffix : "");
                            }
                        },
                        legend: { layout: "vertical", align: "right", verticalAlign: "top", x: -10, y: 100, borderWidth: 0 },
                        series: options.YAxisData,
                        exporting: { enabled: false }
                    })
                );
                return e;
            })
        },
        resize: function() {
            this.each(function() {
                if ($(this).is(":visible")) $(this).data("Graph").setSize($(this).width(), $(this).height());
                return this;
            })
        }
    };
    $.fn.Graph = function(method) {
        if (graphMethods[method]) return graphMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === 'object' || !method) return graphMethods.init.apply(this, arguments);
        else $.error('Method ' + method + ' does not exist for the Graph plugin');
    };
})(jQuery);