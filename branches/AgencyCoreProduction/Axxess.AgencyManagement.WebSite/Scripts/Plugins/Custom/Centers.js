(function($) {
    $.extend($.fn, {
        PatientSelector: function(Options, Arguments) {
            var Options = $.extend({}, {
                Suffix: "_PatientSelector"
            }, Options);
            if (typeof Options == "string") {
                switch (Options) {
                    case "GetPrefix": return $(this).prop("id").substr(0, -16);
                    case "BuildPatientList": return this.each(function() {
                        var Selector = $(this),
                            Prefix = Selector.PatientSelector("GetPrefix");
                        U.PostUrl("Patient/List", {}, function(data) {
                            $(".GridJsonObject", Selector).text(data);
                            $(".bottom tbody", Selector).empty();
                            $.each(data, function() {
                                $(".bottom tbody", Selector).append(
                                    $("<tr/>").prop("id", Prefix + "_" + data.Id).append(
                                        $("<td/>").text(data.LastName)).append(
                                        $("<td/>").text(data.FirstName)
                                    )
                                )
                            })
                        })
                    })
                }
            } else {
                return this.each(function() {
                    $(this).prop("id", Options.Type + "_PatientSelector").addClass("patient-selector").append(
                        $("<div/>").addClass("top").append(
                            $("<div/>").addClass("heading").Buttons([{ Text: "Add New Patient", Click: function() { Acore.Open("newpatient") } }])).append(
                            $("<div/>", { "class": "row", "text": "Select Patient" })).append(
                            $("<div/>").Row({ Prefix: Options.Type, Name: "StatusDropDown", Label: "View", Type: "select",
                                Options: [
                                    { Value: "1", Text: "Active Patients" },
                                    { Value: "2", Text: "Discharged Patients" }
                                ]
                            })).append(
                            $("<div/>").Row({ Prefix: Options.Type, Name: "PaymentDropDown", Label: "Filter", Type: "select",
                                Options: [
                                    { Value: "0", Text: "All" },
                                    { Value: "1", Text: "Medicare (traditional)" },
                                    { Value: "2", Text: "Medicare (HMO/managed care)" },
                                    { Value: "3", Text: "Medicaid (traditional)" },
                                    { Value: "4", Text: "Medicaid (HMO/managed care)" },
                                    { Value: "5", Text: "Workers' compensation" },
                                    { Value: "6", Text: "Title programs" },
                                    { Value: "7", Text: "Other government" },
                                    { Value: "8", Text: "Private" },
                                    { Value: "9", Text: "Private HMO/managed care" },
                                    { Value: "10", Text: "Self Pay" },
                                    { Value: "11", Text: "Unknown" }
                                ]
                            })).append(
                            $("<div/>").Row({ Prefix: Options.Type, Name: "TextSearch", Label: "Find", Type: "text" }))).append(
                        $("<div/>").addClass("bottom").append(
                            $("<div/>").addClass("GridJsonObject")).append(
                            $("<table/>").append(
                                $("<thead/>").append(
                                    $("<tr/>").append(
                                        $("<th/>", { "text": "Last Name" })).append(
                                        $("<th/>", { "text": "First Name" })))).append(
                                $("<tbody/>"))));
                    $(this).PatientSelector("BuildPatientList");
                })
            }
        },
        WindowMenuBar: function(Options) {
            return this.each(function() {
                var $menu = $(this);
                $menu.addClass("window-menu").append(
                    $("<ul/>")
                );
                $.each(Options, function() {
                    $("ul", $menu).append(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0)", "text": this.Text }).bind("click", this.Click)
                        )
                    )
                })
            })
        },
        ScheduleCenter: function() {
            return this.each(function() {
                $(this).addClass("layout").append(
                    $("<div/>").addClass("ui-layout-west").PatientSelector()).append(
                    $("<div/>").addClass("ui-layout-center").append(
                        $("<div/>").addClass("top").append(
                            $("<div/>").WindowMenuBar([
                                { Text: "New Episode", Click: function() { } },
                                { Text: "Schedule Employee", Click: function() { } },
                                { Text: "Reassign Schedule", Click: function() { } },
                                { Text: "Master Calendar", Click: function() { } },
                                { Text: "Inactive Episodes", Click: function() { } }
                            ])
                        ).append(

                        )
                    )
                )
            })
        }
    })
})(jQuery);