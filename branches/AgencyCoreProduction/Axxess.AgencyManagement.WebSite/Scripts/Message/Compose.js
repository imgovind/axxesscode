﻿if (typeof Message == "undefined") var Message = new Object();
$.extend(Message, {
    CKELoaded: false,
    TokenList: null,
    AddRemoveRecipient: function(input) {
        if ($("#" + input).prop("checked")) $.data($("#" + input).get(0), "tokenbox", { "token": Message.TokenList.insertToken($("#" + input).attr("value"), $("#" + input).attr("title")) });
        else Message.TokenList.removeToken($.data($("#" + input).get(0), "tokenbox").token);
        $(".recipient-list-token-input").removeClass("error");
        Message.PositionBottom();
    },
    Compose: function() {
        Message.New();
    },
    InitCKE: function() {
        var e = "NewMessage_Body", args = {
            resize_enabled: false,
            removePlugins: 'elementspath',
            uiColor: '#85a4d5',
            toolbar: [
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
                { name: 'color', items: ['TextColor', 'BGColor'] },
                { name: 'styles', items: ['Font', 'FontSize'] },
                { name: 'list', items: ['NumberedList', 'BulletedList', 'Outdent', 'Indent'] },
                { name: 'links', items: ['Link', 'Unlink'] },
                { name: 'spell', items: ['SpellChecker', 'Scayt'] }
            ]
        };
        if (!Message.CKELoaded) $.getScript("Scripts/Plugins/ckeditor/ckeditor.js", function() {
            Message.CKELoaded = true;
            CKEDITOR.replace(e, args);
            Message.AfterLoad();
        });
        else {
            CKEDITOR.replace(e, args);
            Message.AfterLoad();
        }
    },
    AfterLoadCalled: 0,
    AfterLoad: function() {
        var e = $('.cke_wysiwyg_frame').contents().find('html, body');
        if (e.length === 0) {
            if (++Message.AfterLoadCalled < 10) {
                window.setTimeout(Message.AfterLoad, 500);
            }
        } else {
            // Fix Tablet Iframe Issue
            if (Acore.Mobile) {
                e.css({
                    'height': '100%',
                    'box-sizing': 'border-box',
                    'margin': '0',
                    'font-size': '20px'
                }).filter('body').css('padding', '1em');
            }
        }
    },
    InitExistingMessage: function(message, e) {
        var type = "NewMessage", prefix = "#" + type + "_";
        Message.Forward = Message.Reply = Message.ReplyAll = false;
        Message.DefaultMessage = "";
        $(prefix + "Subject").val(message.Subject);
        $(prefix + "PatientId").val(message.PatientId);
        $(prefix + "Body").val(message.ReplyForwardBody);
        Message.InitCKE();
    },
    InitForward: function(message, e) {
        $(".compose-header span", e).text("Forward Message");
        if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
        Message.InitExistingMessage(message, e);
    },
    InitCompose: function(r, t, x, e) {
        var type = "NewMessage", prefix = "#" + type + "_";
        $(".layout", e).layout({
            west: {
                paneSelector: ".ui-layout-west",
                size: 300,
                minSize: 160,
                maxSize: 400,
                livePaneResizing: true,
                spacing_open: 3
            }
        });
        $("input[name=Recipients]", e).change(function() { Message.AddRemoveRecipient($(this).attr("id")) });
        U.SelectAll($(prefix + "SelectAllRecipients"), $("[name=Recipients]", e));
        Message.TokenList = $.fn.tokenInput(prefix + "Recipents", "Message/Recipients", {
            classes: { tokenList: "recipient-list-input", token: "recipient-list-token", selectedToken: "recipient-list-token-selected", dropdown: "recipient-list-select", inputToken: "recipient-list-token-input" }
        });
        if (Message.Forward || Message.Reply || Message.ReplyAll) U.PostUrl("Message/Get", { id: Message.DefaultMessage,userMessageId: Message.UserMessageId, messageType: $("#MessageView_MessageType").val() }, function(data) {
            if (Message.Forward) Message.InitForward(data, e);
            if (Message.Reply) Message.InitReply(data, e);
            if (Message.ReplyAll) Message.InitReplyAll(data, e);
        });
        else Message.InitCKE();
        $("form", e).Validate({
            PreprocessForm: function(form) {
                if (!$(".recipient-list-token", form).length) {
                    $(".recipient-list-token-input > input", form).addClass("error");
                    U.Growl("Please Select a Recipient", "error");
                    return false;
                }
                if (CKEDITOR.instances["NewMessage_Body"].getData().length) {
                    $("[name=Body]", form).val(CKEDITOR.instances["NewMessage_Body"].getData());
                    $("li.send", form).hide().parent().find("li.sending").show();
                }
                else {
                    U.Growl("Please Type in a Message", "error");
                    return false;
                }
            },
            Success: function() {
                e.Close();
                Message.RefreshInbox();
            },
            Fail: function(result, form) {
                $("li.send", form).show().parent().find("li.sending").hide();
            }
        });
    },
    InitReply: function(message, e) {
        $(".compose-header span", e).text("Reply Message");
        if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
        if ($("[name=Recipients][value=" + message.FromId + "]", e).length) $("[name=Recipients][value=" + message.FromId + "]", e).prop("checked", true).change();
        Message.InitExistingMessage(message, e);
    },
    InitReplyAll: function(message, e) {
        $.each(message.RecipientNames.split(/;\s?/), function(index, data) {
            $(".recipient:not(.self)").each(function() {
                if ($("label", this).text() == data) $("input", this).prop("checked", true).change();
            })
        });
        Message.InitReply(message, e);
    },
    PositionBottom: function() {
        $("#NewMessage_BodyWrapper").css("top", $(".compose.message-header-container").height() + 50)
    }
});