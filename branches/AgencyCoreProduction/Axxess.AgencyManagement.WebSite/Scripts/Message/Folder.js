﻿if (typeof Message == "undefined") var Message = new Object();
$.extend(Message, {
    Folder: {
        InitNew: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { Message.Folder.Refresh() } })
        },
        New: function() {
            Acore.Modal({
                Name: "New Message Folder",
                Url: "Message/FolderNew",
                OnLoad: Message.Folder.InitNew
            })
        },
        LoadMoveMenu: function() {
            U.PostUrl("Message/FolderList", {}, Message.Folder.PopulateMoveToMenu)
        },
        LoadSelector: function() {
            // U.PostUrl("Message/FolderList", {}, Message.Folder.PopulateSelector)
            Message.Folder.PopulateSelector(new Array());
        },
        PopulateMoveToMenu: function(folders) {
            var e = $("#window_" + Message.InboxAcoreId + "_content"),
                currentFolder = $("#folderId", e).val(),
                currentMessage = $("#messageId", e).val(),
                currentMessageType = $("#messageType", e).val();
            // Destroy Current Superfish
            $(".window-menu ul", e).Menu("destroy");
            // If not on Inbox, Show Move To Inbox
            if (currentFolder != U.GuidEmpty) $(".move .menu", e).empty().append(
                $("<li/>").append(
                    $("<a/>").text("Inbox").data("guid", U.GuidEmpty).click(function() {
                        Message.Move(currentMessage, currentMessageType, U.GuidEmpty, "Inbox")
                    })
                )
            );
            // Set variable for current menu
            var currentMenu = $(".move .menu", e);
            // Iterate through available folders
            $.each(folders, function(key, val) {
                // If it is not the current folder, add to list
                if (currentFolder != val.Id) {
                    // If list already has nine items in it, add a submenu
                    if ($("li", currentMenu).length > 8) {
                        currentMenu.append(
                            $("<li/>").append(
                                $("<a/>").html("More<span class='img icon more'></span>").addClass("menu-trigger")).append(
                                $("<ul/>").addClass("menu")
                            )
                        );
                        // Set new submenu as current menu
                        currentMenu = $(".menu", currentMenu);
                    }
                    // Add folder to current menu
                    currentMenu.append(
                        $("<li/>").append(
                            $("<a/>").text(val.Name).click(function() {
                                Message.Move(currentMessage, currentMessageType, val.Id, val.Name)
                            })
                        )
                    )
                }
            });
            // Reinitialize superfish
            $(".window-menu ul", e).Menu();
        },
        PopulateSelector: function(folders) {
            var e = $("#window_" + Message.InboxAcoreId + "_content");
            // Remove style select plugin
            $(".selectMenu", e).remove();
            // Empty selector and add default options
            $(".folder", e).empty().append(
                $("<option/>", { value: "inbox", text: "Inbox" })).append(
                $("<option/>", { value: "sent", text: "Sent Messages" })).append(
                $("<option/>", { value: "bin", text: "Deleted Items" })
            );
            // Iterate through folders adding custom folders
            $.each(folders, function(key, val) {
                $(".folder", e).append(
                    $("<option/>", { value: val.Id, text: val.Name })
                )
            });
            // Reinitialize style select plugin
            $(".folder", e).styleSelect();
        },
        Refresh: function() {
            U.PostUrl("Message/FolderList", {}, function(folders) {
                Message.Folder.PopulateSelector(folders);
                Message.Folder.PopulateMoveToMenu(folders);
            })
        }
    }
});