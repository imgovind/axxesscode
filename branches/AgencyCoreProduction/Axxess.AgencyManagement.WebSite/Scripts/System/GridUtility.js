﻿if (typeof U == "undefined") var U = new Object();
$.extend(U, {
    //For Telerik Grids that are bound client side to sort the data without making additional requests
    OnTGridClientLoad: function() {
        var grid = $(this).data("tGrid");
        var defaultSort = grid.orderBy ? grid.orderBy.split("-") : [],
            defaultMember = defaultSort.length ? defaultSort[0] : "",
            defaultOrder = defaultSort.length ? defaultSort[1] : "";
        //Overrides the teleriks sorting
        $(this).find(".t-grid-header .t-header a").each(function() {
            $(this).click(function(clickEvnt) {
                //If the grid is currently not sorting and has data it will be sorted
                if (!grid.isSorting && grid.data && grid.data.length) {
                    grid.isSorting = true;
                    var index = grid.$columns().index($(this).parent()),
                        column = grid.columns[index];
                    //Toggles the ordering of the telerik grid
                    grid.toggleOrder(index);
                    var order = column.order ? column.order : defaultOrder,
                        member = column.order ? column.member : defaultMember;
                    if (column.member == defaultMember && defaultOrder == "desc" && column.order == null) order = U.ToggleOrder(order);
                    //Sorts the data
                    grid.data.sort(function(a, b) {
                        return U.SortByParameter(a, b, member, order);
                    });
                    grid.orderBy = member + "-" + order;
                    //Binds the sorted data
                    grid.dataBind(grid.data);
                    grid.isSorting = false;
                }
                clickEvnt.preventDefault();
                return false;
            });
            $(this).attr("href", "");
        });
        return grid;
    },
    ToggleOrder: function(orderBy) {
        if (orderBy.indexOf("asc") > -1) return orderBy.replace("asc", "desc");
        else return orderBy.replace("desc", "asc");
    },
    SortByParameter: function(a, b, parameterName, sortOrder) {
        //Default value is used to make null values at the bottom of the sort.
        var defaultValue = sortOrder == "asc" ? "|||" : "!!!",
            aName = a[parameterName] || defaultValue,
            bName = b[parameterName] || defaultValue;
        //Checks if the values are numbers. If they are they will be sorted as such.
        if (U.isNumber(aName) && U.isNumber(bName)) {
            if (sortOrder == "asc") return (+aName) - (+bName);
            else return (+bName) - (+aName);
        }
        //Casts the values as XDates to check if they are a valid date. If they are they will be sorted as dates.
        var aDate = XDate(aName),
            bDate = XDate(bName);
        if (aDate.valid() && bDate.valid()) {
            if (sortOrder == "asc") return aDate.diffMilliseconds(bDate);
            else return bDate.diffMilliseconds(aDate);
        }
        //If the values are strings they will be set to all uppercase to remove case sensitivity.
        if (typeof aName == "string" && typeof bName == "string") {
            aName = aName.toUpperCase().trim() || defaultValue;
            bName = bName.toUpperCase().trim() || defaultValue;
        }
        if (sortOrder == "asc") return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        else return ((aName > bName) ? -1 : ((aName < bName) ? 1 : 0));
    },
    AddTGridEmptyRow: function(message, $grid) {
        var colspan = $("th", $grid).filter(":visible").length;
        $(".t-grid-content tbody", $grid).html(
            $("<tr/>").addClass("t-no-data").append(
                $("<td/>").attr("colspan", colspan).append(
                    $("<div/>").html(message))));

    }
});