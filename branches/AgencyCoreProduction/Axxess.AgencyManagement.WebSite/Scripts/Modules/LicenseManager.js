﻿var LicenseManager = {
    AcoreId: "LicenseManager",
    New: function() {
        Acore.Modal({
            Name: "New Non-Software User License",
            Url: "Agency/License/New",
            OnLoad: LicenseManager.InitNew,
            Width: 450,
            Height: 330,
            WindowFrame: false
        })
    },
    Delete: function(id, userId) {
        if (confirm("Are you sure you want to delete this license?")) {
            U.PostUrl("Agency/License/Delete", { Id: id, UserId: userId }, function(result) {
                U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                if (result.isSuccessful) LicenseManager.Refresh();
            })
        }
    },
    Edit: function(id, userId) {
        Acore.Modal({
            Name: "Edit Non-Software User License",
            Url: "Agency/License/Edit",
            Input: { Id: id, UserId: userId },
            OnLoad: LicenseManager.InitEdit,
            Width: 450,
            Height: 300,
            WindowFrame: false
        })
    },
    Init: function(r, t, x, e) {
        $(".new-license", e).click(function() {
            LicenseManager.New()
        });
        $(".grid-refresh", e).click(function() {
            LicenseManager.Refresh()
        });
        $(".acore-grid ol", e).Zebra();
    },
    InitList: function(r, t, x, e) {
        $("ol", e).Zebra()
    },
    InitNew: function(r, t, x, e) {
        LicenseManager.InitShared(r, t, x, e)
    },
    InitEdit: function(r, t, x, e) {
        LicenseManager.InitShared(r, t, x, e)
    },
    InitShared: function(r, t, x, e) {
        U.ShowIfSelectEquals($("[name=LicenseType]", e), "Other", $(".license-type-other", e));
        $("form", e).Validate({ Success: LicenseManager.Refresh });
    },
    Refresh: function() {
        $(".acore-grid", "#window_" + LicenseManager.AcoreId).Load("Agency/License/List", {}, LicenseManager.InitList);
    }
}