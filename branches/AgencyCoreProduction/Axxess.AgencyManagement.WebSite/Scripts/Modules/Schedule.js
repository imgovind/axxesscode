﻿var Schedule = {
    MissedVisitPopup: function(e, missedVisitId) {
        Acore.Modal({
            Name: "Missed Visit",
            Url: "Schedule/MissedVisitInfo",
            Input: { id: missedVisitId },
            Width: 600,
            Height: 470
        });
    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        $(form).ajaxSubmit({
            dataType: "json",
            beforeSubmit: function() { },
            success: function(result) {
                if (result.isSuccessful) {
                    Patient.Charts.Activities.Refresh(patientId);
                    Schedule.Center.RefreshSchedule(patientId, episodeId);
                    if ($("#PatientActivityGrid").length) $("#PatientActivityGrid").data("tGrid").rebind();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            },
            error: function() { U.Growl("Unable to reassign task to this user", "error"); }
        });
        return false;
    },
    SubmitReassign: function(control, patientId, episodeId) {
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    },
    Delete: function(patientId, episodeId, eventId) {
        if (confirm("Are you sure you want to delete this task?"))
            U.PostUrl("Schedule/Delete", { patientId: patientId, eventId: eventId, episodeId: episodeId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.Charts.Activities.Refresh(patientId);
                    Schedule.Center.RefreshSchedule(patientId);
                    Agency.RebindMissedVisitList();
                } else U.Growl(result.errorMessage, "error");
            });
    },
    Restore: function(episodeId, patientId, eventId) {
        if (confirm("Are you sure you want to restore this task?"))
            U.PostUrl("Schedule/Restore", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
                if (result.isSuccessful) {
                    Patient.Charts.Activities.Refresh(patientId);
                    Schedule.Center.RefreshSchedule(patientId, episodeId);
                    Agency.RebindMissedVisitList();
                    var deletedGrid = $("#List_Patient_DeletedTasks").data("tGrid");
                    if (deletedGrid != null) deletedGrid.rebind({ patientId: patientId });
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
    },
    ReAssign: function(episodeId, patientId, id) {
        Acore.Modal({
            Name: "Reassign",
            Url: "Schedule/ReassignModal",
            Input: { episodeId: episodeId, patientId: patientId, eventId: id },
            OnLoad: function(r, t, x, e) {
                $("form", e).Validate({
                    Success: function() {
                        User.RebindScheduleList();
                        Schedule.Center.RefreshSchedule(patientId, episodeId);
                        Patient.Charts.Activities.Rebind(patientId);
                    }
                });
            },
            Width: "500px",
            Height: "240px",
            WindowFrame: false
        });
    },
    ReOpen: function(episodeId, patientId, eventId) {
        U.PostUrl("Schedule/Reopen", { patientId: patientId, eventId: eventId, episodeId: episodeId }, function(result) {
            if (result.isSuccessful) {
                Schedule.Center.RefreshSchedule(patientId, episodeId);
                Patient.Charts.Activities.Rebind(patientId);
                Agency.RebindMissedVisitList();
                if (Patient.Charts._patientId == patientId) Patient.Charts.Activities.Rebind();
            } else U.Growl(result.errorMessage, "error");
        });
    },
    CancelReassign: function(control) {
        control.parent().parent().find("a.reassign").show();
        control.parent().remove();
    },
    loadMasterCalendar: function(patientId, episodeId) {
        Acore.Open("masterCalendarMain", "Schedule/MasterCalendarMain", function() {
            $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                $(this).css({ position: "relative", left: -100, top: 0 });
            });
        }, { patientId: patientId, episodeId: episodeId });
    },
    loadMasterCalendarNavigation: function(episodeId, patientId) {
        $("#window_masterCalendarMain_content").load("Schedule/MasterCalendar", { patientId: patientId, episodeId: episodeId }, function(r, t) {
            if (t == "error") $("#masterCalendarResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E"));
        });
    },
    InitEpisode: function(action) {
        $("#Edit_Episode_StartDate").blur(function() { setTimeout(Schedule.editEpisodeStartDateOnChange, 200); });
        $("#editEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function() { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $(form).find("[name=PatientId]").val();
                            Patient.Charts.Activities.Refresh(patientId);
                            Schedule.Center.Reload(patientId);
                            Schedule.ReLoadInactiveEpisodes($("#editEpisodeForm").find("#Edit_Episode_PatientId").val());
                            if (typeof action == "function") action();
                            UserInterface.CloseModal();
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    gatherMultiSchedulerDates: function(e) {
        var visitDates = "";
        $(".cal td.selectdate a", e).each(function() { visitDates += $(this).attr("date") + ","; });
        $("#multiDayScheduleVisitDates", e).val(visitDates);
    },
    InitMultiDayScheduler: function(r, t, x, e) {
        Schedule.Center.PopulateDropDowns(e);
        $("legend", e).append(
            $("<span/>").text(Schedule.Center.PatientName + " | " + U.DisplayDate(Schedule.Center.StartDate) + " - " + U.DisplayDate(Schedule.Center.EndDate)));
        var cals = $("#ScheduleCenter_MainResult .cal").clone(true);
        cals.find("td").unbind("click").not(".inactive").bind("click", function() {
            $(this).toggleClass('selectdate');
        });
        $(".calendar-container", e).append(cals);
        $("#multiDayScheduleForm").Validate({
            PreprocessForm: function() { Schedule.gatherMultiSchedulerDates(e); },
            BeforeSubmit: function() {
                if (!$(".cal td.selectdate", e).length) {
                    U.Growl("Select at least one date from the calendar.", "error");
                    return false;
                }
                return true;
            },
            Success: function() {
                var patientId = $("[name=PatientId]", e).val(),
                    episodeId = $("[name=EpisodeId]").val();
                Schedule.Center.RefreshSchedule(patientId, episodeId);
                Patient.Charts.Activities.Refresh(patientId);
            }
        });
    },
    InitNewEpisode: function() {
        $("#New_Episode_StartDate").blur(function() { setTimeout(Schedule.newEpisodeStartDateOnChange, 200); });
        $("#New_Episode_PatientId").change(function() {
            $("#New_Episode_PrimaryPhysician").PhysicianInput();
            var patientId = $(this).val();
            U.PostUrl("Patient/GetPatientForEpisode", { patientId: patientId }, function(data) {
                if (data != null) {
                    $("#newEpisodeTargetDateDiv").show();
                    $("#newEpisodeTargetDate").html(data.StartOfCareDateFormatted);
                    $("#newEpisodeTip").html("<label class=\"bold\">Tip:</label><em> Last Episode end date is: " + data.EndDateFormatted + "</em>");
                } else {
                    $("#newEpisodeTip").html("");
                    $("#newEpisodeTargetDate").html("");
                    $("#newEpisodeTargetDateDiv").hide();
                }
            });
        });
        $("#newEpisodeForm").validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    beforeSubmit: function() { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $(form).find("[name=PatientId]").val();
                            UserInterface.CloseModal();
                            Patient.Charts.Activities.Refresh(patientId);
                            Schedule.Center.Reload(patientId);
                            UserInterface.CloseWindow("newepisode");
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function() { }
                });
                return false;
            }
        });
    },
    InitTopMenuNewEpisode: function() {
        $("#TopMenuNew_Episode_PatientId").change(function() {
            var patientId = $(this).val();
            $("#topMenuNewEpisodeContent").Load("Schedule/NewPatientEpisodeContent", { patientId: patientId }, function(r, t) {
                if (t == "error") $("#topMenuNewEpisodeContent").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E"));
            });
        });
        $("#topMenuNewEpisodeForm").validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    beforeSubmit: function() { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $(form).find("[name=PatientId]").val();
                            UserInterface.CloseModal();
                            UserInterface.CloseWindow("newepisode");
                            Patient.Charts.Activities.Refresh(patientId);
                            Schedule.Center.Reload(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function() { }
                });
                return false;
            }
        });
    },
    GetEpisode: function(episodeId, patientId) {
        Acore.Open("editepisode", "Schedule/EditEpisode", Schedule.InitEpisode, { episodeId: episodeId, patientId: patientId });
    },
    InitTaskDetails: function() {
        $("#Schedule_DetailForm").Validate({
            Success: function(result, form) {
                var patientId = $(form).find("[name=PatientId]").val();
                Patient.Charts.Activities.Refresh(patientId);
                Schedule.Center.Reload(patientId);
                UserInterface.CloseWindow("scheduledetails");
            },
            PreprocessForm: function() {
                return !Schedule.CheckTimeInOut("Schedule_Detail", false);
            }
        });
    },
    GetTaskDetails: function(episodeId, patientId, eventId) {
        Acore.Open("scheduledetails", "Schedule/EditDetails", Schedule.InitTaskDetails, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    GetTaskAttachments: function(episodeId, patientId, eventId) {
        Acore.Modal({
            Url: "Schedule/Attachments",
            Input: { episodeId: episodeId, patientId: patientId, eventId: eventId },
            Width: 500,
            Height: 225,
            WindowFrame: false
        });
    },
    EventMouseOver: function(control) {
        var currentControl = $(".events", $(control));
        currentControl.show();
    },
    EventMouseOut: function(control) {
        $(".events", $(control)).hide();
    },
    loadNoteSupplyWorkSheet: function(episodeId, patientId, eventId) {
        Acore.Open("notessupplyworksheet", "Schedule/SupplyWorksheet", function() { }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ProcessNote: function(button, episodeId, patientId, eventId) {
        U.PostUrl("Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                UserInterface.CloseModal();
                Agency.RebindCaseManagement();
                Patient.Charts.Activities.Refresh(patientId);
                Schedule.Center.RefreshSchedule(patientId);
                User.RebindScheduleList();
            }
        });
    },
    ToolTip: function(e) {
        $("a.tooltip", e.row).each(function() {
            var c = "";
            if ($(this).hasClass("blue-note")) c = "blue-note";
            if ($(this).hasClass("red-note")) c = "red-note";
            if ($(this).attr("tooltip"))
                $(this).click(function() {
                    UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : ""));
                }).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() { return $(this).attr("tooltip"); }
                });
            else $(this).hide();
        });
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?"))
            U.PostUrl("Schedule/DeleteWoundCareAsset", { episodeId: $("#WoundCare_EpisodeId").val(), patientId: $("#WoundCare_PatientId").val(), eventId: $("#WoundCare_EventId").val(), name: name, assetId: assetId }, function(result) {
                if (result.isSuccessful) $(control).closest("td").empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"float-left uploadWidth\" />");
            });
    },
    DeleteScheduleEventAsset: function(control, patientId, episodeId, eventId, assetId) {
        if (confirm("Are you sure you want to delete this asset?"))
            U.PostUrl("Schedule/DeleteScheduleEventAsset", { patientId: patientId, episodeId: episodeId, eventId: eventId, assetId: assetId }, function(result) {
                if (result.isSuccessful) {
                    $(control).closest("span").remove();
                    $("#scheduleEvent_Assest_Count").html($("#scheduleEvent_Assest_Count").html() - 1);
                    Patient.Chart.Activities.Refresh(patientId);
                    Schedule.Center.RefreshSchedule(patientId, episodeId);
                }
            });
    },
    Rebind: function() {
        Schedule._isRebind = true;
        Schedule.Center.Discipline = "all";
        Schedule.Center.DisciplineIndex = 0;
        U.RebindTGrid($("#ScheduleCenter_PatientSelectionGrid"));
        U.RebindTGrid($("#caseManagementGrid"));
    },
    AddSupply: function(control, episodeId, patientId, eventId, type, supplyId) {
        var quantity = $(control).closest("tr").find(".quantity").val();
        var date = $(control).closest("tr").find(".date").val();
        U.PostUrl("Schedule/AddNoteSupply", { episodeId: episodeId, patientId: patientId, eventId: eventId, supplyId: supplyId, quantity: quantity, date: date }, function() {
            var gridfilter = $("#" + type + "_SupplyFilterGrid").data("tGrid");
            if (gridfilter != null) {
                $("#" + type + "_GenericSupplyDescription").val("");
                $("#" + type + "_GenericSupplyCode").val("");
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + type + "_SupplyGrid").data("tGrid");
            if (grid != null) grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId });
        });
    },
    newEpisodeStartDateOnChange: function() {
        Schedule.EpisodeStartDateChange("New");
    },
    editEpisodeStartDateOnChange: function() {
        var count = $("#Count").val();
        if (count == "0") $("#Count").val("1");
        else Schedule.EpisodeStartDateChange("Edit");
    },
    EpisodeStartDateChange: function(prefix) {
        var startDate = $("#" + prefix + "_Episode_StartDate"), endDate = $("#" + prefix + "_Episode_EndDate");
        if (startDate.val()) {
            var newStartDate = new Date(startDate.val());
            var newEndDate = new Date(startDate.val());
            newEndDate.setDate(newStartDate.getDate() + 59);
            var month = newEndDate.getMonth() + 1, day = newEndDate.getDate(), year = newEndDate.getFullYear();
            endDate.val(month + "/" + day + "/" + year);
        }
    },
    PhlebotomyLab: function(selector) {
        $(selector).Autocomplete({ source: ["CBC", "Chem 7", "BMP", "PT/INR", "PT", "Chem 8", "K+", "Urinalysis", "TSH", "CBC W/ diff", "Hemoglobin A1c", "Lipid Panel", "Comp Metabolic Panel  (CMP 14)", "TSH", "ALT/SGOT", "ALT/SGBOT", "Iron (Fe)"] });
    },
    visitLogInit: function(r, t, x, e) {
        $("form", e).Validate({
            PreprocessForm: function(form) {
                var startDate = new Date($("[name=StartDate]", form).val());
                var endDate = new Date($("[name=EndDate]", form).val());
                var result = true;
                $(".visitdate", form).each(function() {
                    if (new Date($(this).val()) < startDate || new Date($(this).val()) > endDate) {
                        $(this).addClass("error");
                        result = false;
                    } else $(this).removeClass("error");
                });
                if (!result) U.Growl("Visit Date is not in the episode range.", "error");
                return result;
            },
            Success: function(result, form) {
                Schedule.Center.RefreshSchedule($("[name=PatientId]", form).val(), $("[name=EpisodeId]", form).val());
            }
        });
    },
    reassignScheduleInit: function(identifier) {
        $("#reassign" + identifier + "Form").validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    beforeSubmit: function() { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $(form).find("[name=PatientId]").val();
                            Patient.Charts.Rebind(patientId);
                            Schedule.Center.RefreshSchedule(patientId);
                            User.RebindScheduleList();
                            if (identifier == "All") UserInterface.CloseWindow("schedulereassign");
                            else UserInterface.CloseModal();
                        } else U.Growl(result.errorMessage, "error");
                    }
                });
                return false;
            }
        });
    },
    loadEpisodeDropDown: function(dropDown, control) {
        var patientId = $(control).val(),
            updateSelect = function(data) {
                var s = $("select#" + dropDown);
                s.children("option").remove();
                s.get(0).options[0] = new Option("-- Select Episode --", "00000000-0000-0000-0000-000000000000", false, false);
                $.each(data, function(index, itemData) {
                    s.get(0).options[s.get(0).options.length] = new Option(itemData.Range, itemData.Id, false, false);
                });
            };
        //If there is a patientid then query for their episodes
        if (patientId != U.GuidEmpty) {
            U.PostUrl("Schedule/EpisodeRangeList", { patientId: patientId }, function(data) {
                updateSelect(data);
            });
            //Otherwise remove the data from the dropdown
        } else updateSelect([]);
    },
    BulkUpdate: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.PostUrl("Schedule/BulkUpdate", fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Charts.Activities.Rebind();
                Schedule.Center.Load();
                Agency.LoadCaseManagement($("#CaseManagement_GroupName").val());
                Agency.RebindPrintQueue();
            } else U.Growl(data.errorMessage, "error");
        });
    },
    LoadLog: function(patientId, eventId, task) {
        Acore.Open("schdeuleeventlogs", "Schedule/ScheduleLogs", function() { }, { patientId: patientId, eventId: eventId, task: task });
    },
    GetPlanofCareUrl: function(episodeId, patientId, eventId) {
        U.PostUrl("Oasis/GetPlanofCareUrl", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { episodeId: result.episodeId, patientId: result.patientId, eventId: result.eventId });
            else U.Growl(result.errorMessage, "error");
        });
    },
    GetHHACarePlanUrl: function(episodeId, patientId, eventId) {
        U.GetAttachment("Schedule/HHACarePlanPdf", { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    GetHHACarePlanUrlFromHHAVisit: function(episodeId, patientId, eventId) {
        U.PostUrl("Schedule/HHACarePlanPdfFromHHAVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { episodeId: result.episodeId, patientId: result.patientId, eventId: result.eventId });
            else U.Growl(result.errorMessage, "error");
        });
    },
    loadInactiveEpisodes: function(patientId) {
        Acore.Open("inactiveepisode", "Schedule/Inactive", function() { }, { patientId: patientId });
    },
    ActivateEpisode: function(episodeId, patientId) {
        U.PostUrl("Schedule/ActivateEpisode", { episodeId: episodeId, patientId: patientId }, function(result) {
            if (result.isSuccessful) {
                U.Growl(result.errorMessage, "success");
                Patient.Charts.Activities.Refresh(patientId);
                Schedule.Center.Reload(patientId);
                Agency.RebindCaseManagement();
                User.RebindScheduleList();
                Schedule.ReLoadInactiveEpisodes(patientId);
            } else U.Growl(result.errorMessage, "error");
        });
    },
    ReLoadInactiveEpisodes: function(patientId) {
        $("#InactiveEpisodesContent ol").addClass("loading");
        $("#InactiveEpisodesContent").load("Schedule/InactiveGrid", { patientId: patientId }, function(r, t) {
            $("#InactiveEpisodesContent ol").removeClass("loading");
            if (t == "error") $("#InactiveEpisodesContent").html(U.AjaxError);
        });
    },
    LoadEpisodeLog: function(episodeId, patientId) {
        Acore.Open("episodelogs", "Schedule/EpisodeLogs", function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ConvertToTime: function(time) {
        if (time != undefined) {
            var hours = +time.substring(0, 2);
            var minutes = +time.substring(3, 5);
            var pm = time.indexOf("P") != -1;
            if (pm && hours !== 12) hours += 12;
            var date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);
            return date;
        } else return new Date();
    },
    CheckTimeInOut: function(prefix, showMessage) {
        var tempTimeIn = $("#" + prefix + "_TimeIn").val();
        var tempTimeOut = $("#" + prefix + "_TimeOut").val();
        var timeIn = Schedule.ConvertToTime(tempTimeIn);
        var timeOut = Schedule.ConvertToTime(tempTimeOut);
        var timeOutNextDay = Schedule.ConvertToTime(tempTimeOut);
        timeOutNextDay.setDate(timeOutNextDay.getDate() + 1);
        if (timeIn && timeOut) {
            var timeDiff = (timeOut - timeIn) / 60 / 60 / 1000;
            var timeDiffNextDay = (timeOutNextDay - timeIn) / 60 / 60 / 1000;
            var over3Hours = timeDiff > 3 || timeDiff < 0;
            var over3HoursNextDay = timeDiffNextDay > 3 || timeDiffNextDay < 0;
            if (over3Hours && !over3HoursNextDay) over3Hours = false;
            if (over3Hours == true) {
                if (showMessage == true) {
                    var growl = $(".jGrowl-notification").text();
                    if (growl == "") U.Growl("WARNING: The Time In and Out are over 3 hours apart.", "warning");
                    return false;
                } else {
                    var answer = confirm("The time out is 3 hours greater than the time in. Are you sure you want to continue?");
                    return !answer;
                }
            } else return false;
        }
    },
    WarnTimeInOut: function(prefix, type) {
        Schedule.CheckTimeInOut(prefix, true);
        if (type === "Oasis") {
            var finishBtn = $("div#print-controls a:contains('Finish')");
            var finishBtnClick = finishBtn.attr("onClick");
            finishBtn.attr("onClick", "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + finishBtnClick + "}else{return false}");
        } else {
            var saveBtn = $("form#" + prefix + "Form a:contains('Save')");
            var completeBtn = $("form#" + prefix + "Form a:contains('Complete')");
            var approveBtn = $("form#" + prefix + "Form a:contains('Approve')");
            var returnBtn = $("form#" + prefix + "Form a:contains('Return')");
            var saveBtnClick = saveBtn.attr("onClick");
            var completeBtnClick = completeBtn.attr("onClick");
            var approveBtnClick = approveBtn.attr("onClick");
            var returnBtnClick = returnBtn.attr("onClick");
            saveBtn.attr("onClick", "if(Schedule.CheckTimeInOut('" + prefix + "', true) === false){" + saveBtnClick + "}");
            completeBtn.attr("onClick", "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + completeBtnClick + "}");
            approveBtn.attr("onClick", "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + approveBtnClick + "}");
            returnBtn.attr("onClick", "if(Schedule.CheckTimeInOut('" + prefix + "', false) === false){" + returnBtnClick + "}");
        }
    },
    loadDiagnosisData: function(patientId, episodeId) {
        U.PostUrl("Schedule/GetDiagnosisData", { patientId: patientId, episodeId: episodeId }, function(data) {
            $("#PrimaryDiagnosis").text(data.diag1);
            $("#SecondaryDiagnosis").text(data.diag2);
            if (data.code1 === "") $("#ICD9M").remove();
            else $("#PrimaryDiagnosis").append("<a id='ICD9M' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code1 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
            if (data.code2 === "") $("#ICD9M1").remove();
            else $("#SecondaryDiagnosis").append("<a id='ICD9M1' class='teachingguide' href='http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code2 + "&informationRecipient.languageCode.c=en' target='_blank'>Teaching Guide</a>");
        });
    },
    WarnEpisodeGap: function(prefix) {
        $("#" + prefix + "_StartDate").bind("ValueSet", function() { Schedule.CheckEpisodeStartDate(prefix, true); });
        var formPrefix = "";
        if (prefix == "Edit_Episode") formPrefix = "editEpisode";
        else if (prefix == "New_Episode") formPrefix = "newEpisode";
        else if (prefix == "TopMenuNew_Episode") formPrefix = "topMenuNewEpisode";
        $("#" + formPrefix + "Form a:contains('Save')").unbind("click");
        $("#" + formPrefix + "Form a:contains('Save')").click(function() {
            if (Schedule.CheckEpisodeStartDate(prefix, false)) {
                var answer = confirm("There is a gap between the start date and the last episode's end date. Are you sure you want to save?");
                if (answer) $(this).closest("form").submit();
            } else $(this).closest("form").submit();
        });
    },
    CheckEpisodeStartDate: function(prefix, showGrowl) {
        var d1 = $("#" + prefix + "_StartDate").val();
        var d2 = $("#" + prefix + "_PreviousEndDate").val();
        if (d1 != "" && d2 != "") {
            var startDate = $.datepicker.parseDate("mm/dd/yy", d1);
            var previousEndDate = $.datepicker.parseDate("mm/dd/yy", d2);
            previousEndDate.setDate(previousEndDate.getDate() + 1);
            if (startDate.getTime() != previousEndDate.getTime()) {
                if (showGrowl) U.Growl("WARNING: There is a gap between the start date and the last episode's end date.", "warning");
                return true;
            }
        }
        return false;
    },
    RestoreMissedVisit: function(episodeId, patientId, eventId) {
        if (confirm("Are you sure you want to restore this missed visit?")) {
            U.PostUrl("Schedule/MissedVisitRestore", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(data) {
                if (data.isSuccessful) {
                    Agency.RebindMissedVisitList();
                    Agency.RebindOrders();
                    Patient.Charts.Activities.Refresh(patientId);
                    Schedule.Center.RefreshSchedule(patientId);
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    DeleteAction: function() {
        var data = $("#DeleteMultipleActivityGrid .t-grid-content tr input:checked").serializeArray();
        if (!data.length) U.Growl("You must select at least one task to delete.", "error");
        else if (confirm("Are you sure you want to delete these event(s)?")) {
            data.push({ name: "PatientId", value: Schedule.Center.PatientId });
            data.push({ name: "EpisodeId", value: Schedule.Center.EpisodeId });
            U.PostUrl("Schedule/DeleteMultiple", data, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Agency.RebindMissedVisitList();
                    Agency.RebindOrders();
                    Patient.Charts.Activities.Refresh(Schedule.Center.PatientId);
                    Schedule.Center.RefreshSchedule(Schedule.Center.PatientId, Schedule.Center.EpisodeId);
                    UserInterface.CloseWindow("scheduledelete");
                } else U.Growl(result.errorMessage, "error");
            }, null);
        }
    },
    DeleteActivityRowSelected: function(e) {
        var checkbox = $(e.row).find("input");
        checkbox.prop("checked", !checkbox.prop("checked"));
    },
    Download: function(episodeId, patientId) {
        U.PostUrl("Request/EpisodeDownload", { episodeId: episodeId, patientId: patientId }, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
        });
    }
}