﻿var Template = {
    Delete: function(id) { U.DeleteTemplate("Template", id); },
    InitEdit: function() {
        U.InitEditTemplate("Template");
    },
    InitNew: function() {
        U.InitNewTemplate("Template");
    },
    RebindList: function() { U.RebindTGrid($('#List_Template')); },
    OnChangeInit: function() {
        $("select.Templates").change(function() {
            var selectList = this;
            var textarea = $(this).attr("template");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/Template/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    },
    DropDownList: function(currentItem, form, callback) {
        U.PostUrl("/Agency/TemplateSelect", null, function(data) {
            var options = [];
            $.each(data, function(index, itemData) {
                options[index] = new Option(itemData.Text, itemData.Value, false, false);
            });
            $(currentItem).trigger("ProcessingComplete");
            if (typeof callback == "function") callback(currentItem, form, options);
        }, function() {
            U.Error("Unable to load templates, please try again later");
            //$("select.Templates").removeClass("processing").hide();
            $(".load-template", form).show();
            $("select.Templates").trigger("ProcessingComplete");
        })
    },
    LoadFormTemplates: function(currentClickControl, contaningForm) {
        var listElement = $(currentClickControl).closest(".buttons").next("select");
        $(".load-template", contaningForm).hide();
        $("select.Templates", contaningForm).trigger("ProcessingStart").show();
        var callBack = function(currentSelect, form, options) {
            currentSelect.empty();
            currentSelect.append(options);
            var clonedOptions = $("option", currentSelect).clone();
            $("select.Templates", form).not(currentSelect).each(function(index, element) {
                var s = $(element);
                s.empty();
                s.append(clonedOptions);
                s.show();
                clonedOptions = $("option", s).clone();
                //s.closest(".template-text").find("span").hide();
            });
            $("select.Templates").trigger("ProcessingComplete");
        };
        Template.DropDownList(listElement, contaningForm, callBack);
    }
}