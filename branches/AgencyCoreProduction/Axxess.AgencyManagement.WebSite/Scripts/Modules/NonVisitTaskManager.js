﻿var NonVisitTaskManager = {
    Delete: function(id) {
        U.DeleteTemplate("NonVisitTaskManager", id);
    },
    InitEdit: function() {
        U.InitEditTemplate("NonVisitTaskManager");
    },
    InitNew: function() {
        this.UserTaskIndex = 1;
        U.InitNewTemplate("NonVisitTaskManager");
    },
    RebindList: function() {
        U.RebindTGrid($('#List_NonVisitTaskManager'));
    },
    Modal: {
        InitEdit: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { NonVisitTaskManager.RebindList() } });
        },
        Edit: function(id, userId, insurance) {
            Acore.Modal({
                Name: "Edit User-Non-VisitTask",
                Url: "NonVisitTaskManager/Edit",
                Input: { Id: id },
                OnLoad: NonVisitTaskManager.Modal.InitEdit,
                Width: 420,
                Height: 400,
                WindowFrame: false
            })
        },
        Refresh: function() {
            $("#List_NonVisitTaskManager").Load("User/Rate/List", { UserId: $("#List_NonVisitTaskManager").attr("guid") })
        }
    },
    OnChangeInit: function() {
        $("select.NonVisitTaskManagers").change(function() {
            var selectList = this;
            var textarea = $(this).attr("nonvisittaskmanager");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/NonVisitTaskManager/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    },

    UserTaskIndex: 1,

    AddUserTask: function(location) {
        var index = ++this.UserTaskIndex;
        var template = $(".nonvisittask").first(),
            element = template.clone(),
            keys = ['UserId', 'TaskId', 'TaskDate', 'TimeIn', 'TimeOut', 'Comments'],
            nameIndex = index - 1;
        for (var key in keys) {
            var input = element.find('#NonVisitTask_' + keys[key] + '_1'),
                id = 'NonVisitTask_' + keys[key] + '_' + index,
                namer = 'NonVisitTasks[' + nameIndex + '].' + keys[key]
            input.attr({
                'id': id,
                'name': namer
            }).removeClass("hasDatepicker").next(".img,.t-icon").remove();
            input.closest('.row').find('label').attr('for', id)
        }
        $("input,textarea", element).val('').removeAttr('style');
        $("textarea[maxcharacters]", element).limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings(".characters-remaining").text("You have " + remaining + " characters remaining");
                if (remaining > 0) $(this).css("background-color", "#fff")
            },
            onLimit: function() {
                $(this).css("background-color", "#ecbab3")
            }
        })
        $('select', element).val('00000000-0000-0000-0000-000000000000');
        $('.usertask-delete', element).show('hidden');
        $(".date-picker", element).DatePicker();
        $(".time-picker", element).TimePicker();
        element.find('input.indexvalue').val(nameIndex);
        location.before(element)
    },

    RemoveUserTask: function(el) {
        $(el).closest('fieldset').remove()
        //this.UserTaskIndex--;
    }
}