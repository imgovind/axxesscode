﻿var Medication = {
    assessmentType: "",
    Activate: function(medProfileId, medicationId, assessmentType) {
        if (confirm("Are you sure you want to activate this medication?")) {
            U.PostUrl('Patient/UpdateMedicationStatus', { medProfileId: medProfileId, medicationId: medicationId, medicationCategory: "Active", dischargeDate: "01/01/0001" }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Medication.Refresh(medProfileId, assessmentType);
                } else U.Growl(result.errorMessage, "error");
            })
        }
    },
    Add: function(medProfileId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Medication",
            "Url": "Patient/NewMedication",
            "Input": { medProfileId: medProfileId },
            "OnLoad": function() { Medication.InitNew(assessmentType) },
            "Width": "670px",
            "Height": "330px",
            "WindowFrame": false
        })
    },
    Delete: function(medProfileId, medicationId, assessmentType) {
        if (confirm("Are you sure you want to delete this medication?")) {
            U.PostUrl("Patient/DeletePatientMedication", { medProfileId: medProfileId, medicationId: medicationId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Medication.Refresh(medProfileId);
                } else U.Growl(result.errorMessage, "error");
            })
        }
    },
    Discharge: function(medProfileId, medicationId, control) {
        control.closest("form").validate();
        if (control.closest("form").valid()) {
            U.PostUrl("Patient/UpdateMedicationStatus", { medProfileId: medProfileId, medicationId: medicationId, medicationCategory: "DC", dischargeDate: $("#Discharge_Medication_Date").val() }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Medication.Refresh(medProfileId);
                    $("#window_ModalWindow").Close();
                } else U.Growl(result.errorMessage, "error");
            });
        } else U.ValidationError(control);
    },
    Discontinue: function(medProfileId, medicationId, assessmentType) {
        Acore.Modal({
            "Name": "Discharge Medication",
            "Url": "Patient/DischargeMedication",
            "Input": { medProfileId: medProfileId, medicationId: medicationId },
            "OnLoad": function() { Medication.InitDischarge(assessmentType) },
            "Width": "650px",
            "Height": "165px",
            "WindowFrame": false
        })
    },
    Edit: function(medProfileId, medicationId, assessmentType) {
        Acore.Modal({
            "Name": "Edit Medication",
            "Url": "Patient/EditMedication",
            "Input": { medProfileId: medProfileId, medicationId: medicationId },
            "OnLoad": function() { Medication.InitEdit(assessmentType) },
            "Width": "670px",
            "Height": "330px",
            "WindowFrame": false
        })
    },
    InitAutocomplete: function(e) {
        var drugAutoCompleteInput = $("input[name=MedicationDosage]", e).AjaxAutocomplete({
            minLength: 130,
            SourceUrl: "LookUp/Drugs",
            close: function() {
                drugAutoCompleteInput.autocomplete("option", { minLength: 130 })
            },
            Response: function(text) {
                if (!text.length) U.Growl("Could not find any medication using the specified term.", "error");
            },
            Format: function(json) {
                return json.Name
            },
            Select: function(json, input) {
                input.val(json.Name).next().val(json.Name).closest("form");
                $('input[name=LexiDrugId]', e).val(json.LexiDrugId);
                U.PostUrl("LookUp/DrugClassifications", { genericDrugId: json.LexiDrugId }, function(result) {
                    if (result != undefined) {
                        if (result.length == 1) {
                            $("input[name=Classification]", e).val(result[0]);
                        } else if (result.length > 1) {
                            $("input[name=Classification]", e).Autocomplete({
                                minLength: 0,
                                source: result
                            }).autocomplete("search", "");
                        }
                    }
                });
            }
        });
        $("input[name=Route]", e).AjaxAutocomplete({
            minLength: 1,
            SourceUrl: "LookUp/MedicationRoute",
            Format: function(json) {
                return json.Id + " &#8211; " + json.ShortName + " " + json.LongName
            },
            Select: function(json, input) {
                input.val(json.LongName + " (" + json.ShortName + ")").next().val(json.LongName + " (" + json.ShortName + ")")
            }
        });
        $("input[name=Frequency]", e).Autocomplete({
            source: [
                "Before Meals",
                "After meals",
                "Daily",
                "Every Day",
                "Twice/day",
                "Three times/day",
                "Four times /day",
                "Every other day",
                "PRN",
                "Bedtime"
            ]
        });
    },
    Search: function(actionType) {
        $('#' + actionType + '_Medication_MedicationDosage').autocomplete("option", { minLength: 3 }).autocomplete("search");
    },
    InitDischarge: function(assessmentType) {
        if (assessmentType != undefined) {
            this.assessmentType = assessmentType;
            $("#Discharge_Medication_AssessmentType").val(assessmentType);
        }
    },
    InitEdit: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        var $form = $("#editMedicationForm");
        this.InitAutocomplete($form);
        U.HideIfChecked($("#Edit_Medication_IsLongStanding", $form), $("#Edit_Medication_StartDateRow", $form));
        $form.validate({
            submitHandler: function(form) {
                form = $(form);
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var medProfileId = $("#Edit_Medication_ProfileId", form).val();
                            Medication.Refresh(medProfileId);
                            U.Growl(result.errorMessage, "success");
                            $("#window_ModalWindow").Close();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                form.ajaxSubmit(options);
                return false;
            }
        })
    },
    InitNew: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        var $form = $("#newMedicationForm");
        this.InitAutocomplete($form);
        U.HideIfChecked($("#New_Medication_IsLongStanding", $form), $("#New_Medication_StartDateRow", $form));
        $form.validate({
            submitHandler: function(form) {
                form = $(form);
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var medProfileId = $("#New_Medication_ProfileId", form).val();
                            Medication.Refresh(medProfileId);
                            U.Growl(result.errorMessage, "success");
                            if ($("#New_Medication_AddAnother", form).val() == "AddAnother") {
                                $("#New_Medication_StartDateRow", form).show();
                                form.clearForm().find("#New_Medication_ProfileId").val(medProfileId);
                            }
                            else $("#window_ModalWindow").Close();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                form.ajaxSubmit(options);
                return false;
            }
        })
    },
    Refresh: function(medicationProfileId, prefix) {
        if (prefix == undefined && this.assessmentType.length) prefix = this.assessmentType
        else if (prefix == undefined) prefix = "MedProfile";
        $("#" + prefix + "_activeMeds ol").addClass("loading");
        $("#" + prefix + "_dischargeMeds ol").addClass("loading");
        $("#" + prefix + "_medications").load(
            (prefix == "MedProfile" ? "Patient/Medications" : "Patient/MedicationsForOasis"),
            (prefix == "MedProfile" ? { "medicationProfileId": medicationProfileId} : { "medicationProfileId": medicationProfileId, "assessmentType": prefix }),
            function(responseText, textStatus, XMLHttpRequest) {
                $('#' + prefix + '_activeMeds ol').removeClass("loading");
                $('#' + prefix + '_dischargeMeds ol').removeClass("loading");
                if (textStatus == 'error') $('#' + prefix + '_medications').html(U.AjaxError);
            }
        );
    },
    DrugDrugInteractions: function(medProfileId) {
        Acore.Modal({
            "Name": "Drug-Drug Interactions",
            "Url": "Patient/DrugDrugInteractions",
            "Input": { medicationProfileId: medProfileId },
            "OnLoad": function() { },
            "Width": "750px",
            "Height": "350px",
            "WindowFrame": false
        })
    }
}