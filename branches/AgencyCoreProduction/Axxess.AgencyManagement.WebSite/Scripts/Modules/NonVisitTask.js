﻿var NonVisitTask = {
    Delete: function(id) {
        U.DeleteTemplate("NonVisitTask", id); 
    },
    InitEdit: function() {
        U.InitEditTemplate("NonVisitTask");
    },
    InitNew: function() {
        U.InitNewTemplate("NonVisitTask");
    },
    RebindList: function() {
        U.RebindTGrid($('#List_NonVisitTask')); 
    },
    OnChangeInit: function() {
        $("select.NonVisitTasks").change(function() {
            var selectList = this;
            var textarea = $(this).attr("nonvisittask");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/NonVisitTask/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    }
}