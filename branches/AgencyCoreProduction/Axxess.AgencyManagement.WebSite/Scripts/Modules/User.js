﻿var User = {
    Edit: function(id) {
        Acore.Open("EditUser", { Id: id });
    },
    InitNewRate: function() { U.InitTemplate($("#newUserRateForm"), function() { User.RebindRates(); UserInterface.CloseModal(); }, "Rate successfully added"); },
    InitEditRate: function() { U.InitTemplate($("#editUserRateForm"), function() { User.RebindRates(); UserInterface.CloseModal(); }, "Rate successfully updated"); },
    InitNew: function(r, t, x, e) {
        var type = "NewUser", prefix = "#" + type + "_";
        U.SelectAll($(prefix + "AllPermissions"), $("input[name=PermissionsArray]", e));
        $(".select-all-group", e).each(function() { U.SelectAll($(this), $("." + $(this).attr("group"), e)) });
        $(".option", e).each(function() {
            if ($(this).attr("tooltip") != undefined) $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: "calday",
                bodyHandler: function() { return $(this).attr("tooltip") }
            })
        });
        $("#NewUser_LocationId", e).multiSelect({
            selectAll: false,
            oneOrMoreSelected: '*'
        });
        $("form", e).Validate({
            PreprocessForm: function() {

            },
            Success: function() {

            }
        });
        U.ShowIfSelectEquals($(prefix + "TitleType"), "Other", $(prefix + "TitleTypeMore"));
        U.ShowIfSelectEquals($(prefix + "Credentials"), "Other", $(prefix + "CredentialsMore"));
        $(prefix + "Previous").change(function() {
            $(prefix + "Content").Load("User/NewContent", { userId: $(prefix + "Previous").val() })
        })
    },
    InitEdit: function(r, t, x, e) {
        var type = "EditUser", prefix = "#" + type + "_";
        $(prefix + "Tabs").tabs().bind("tabsselect", {
            Id: $(prefix + "Id").val()
        }, function(event, ui) {
            $($(ui.tab).attr("href")).Load("User/EditSection", { id: event.data.Id, category: $(ui.tab).attr("href") }, User.InitSection);
        });
        User.InitSection(r, t, x, e.find(".general:eq(0)"));
    },
    InitSection: function(r, t, x, e) {
        if (e && e.attr("id")) var Category = e.attr("id").split("_")[1];
        if (typeof User.Sections[Category] == "function") User.Sections[Category](r, t, x, e);
    },
    Sections: {
        Information: function(r, t, x, e) {
            var type = "EditUser", prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "TitleType"), "Other", $(prefix + "TitleTypeMore"));
            U.ShowIfSelectEquals($(prefix + "Credentials"), "Other", $(prefix + "CredentialsMore"));
            //            $(".activity-log", e).click(function() {
            //                //User.Logs(e.closest(".window-content").find("[name=Id]").val());
            //                return false;
            //            });
            $("form", e).Validate();
        },
        Licenses: function(r, t, x, e) {
            $(".new-license", e).click(function() {
                User.License.New($(this).attr("guid"))
            });
            $(".grid-refresh", e).click(function() {
                User.License.Refresh()
            });
            User.License.InitList(r, t, e, $(".acore-grid", e))
        },
//        PatientAccess: function(r, t, x, e) {
//            $(".patient-access", e).MultiGrid({
//                DataUri: "User/PatientAccessData",
//                MoveUri: "User/PatientAccessMove",
//                LeftGridTitle: "User has Access to These Patients",
//                RightGridTitle: "User does not have Access to These Patients"
//            })
//        },
        PayRates: function(r, t, x, e) {
            $(".new-rate", e).click(function() {
                User.Rate.New($(this).attr("guid"))
            });
            $(".new-nonvisit-rate", e).click(function() {
                User.NonVisitRate.New($(this).attr("guid"))
            });
            $(".grid-refresh", e).click(function() {
                User.Rate.Refresh();
                User.NonVisitRate.Refresh();
            });
            User.Rate.InitList(r, t, e, $(".acore-grid", e));
            User.NonVisitRate.InitList(r, t, e, $(".acore-grid", e))
        },
        Permissions: function(r, t, x, e) {
            var type = "EditUserPermissions", prefix = "#" + type + "_";
            U.SelectAll($(prefix + "AllPermissions"), $("input[name=PermissionsArray]", e));
            $(prefix + "AllPermissions").change(function() {
                if (!$(this).prop("checked")) $(".select-all-group:checked").click();
            });
            $(".select-all-group", e).each(function() { U.SelectAll($(this), $("." + $(this).attr("group"), e)) });
            $(".option", e).each(function() {
                if ($(this).attr("tooltip") != undefined) $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 10,
                    left: 10,
                    extraClass: "calday",
                    bodyHandler: function() { return $(this).attr("tooltip") }
                })
            });
            $("form", e).Validate();
        }
    },
    License: {
        Delete: function(id, userId) {
            if (confirm("Are you sure you want to delete this license?")) {
                U.PostUrl("User/License/Delete", { Id: id, UserId: userId }, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) User.License.Refresh();
                })
            }
        },
        InitEdit: function(r, t, x, e) {
            var type = "EditUserLicense", prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "Type"), "Other", $(prefix + "TypeMore"));
            $("form", e).Validate({ Success: function() { User.License.Refresh() } });
        },
        InitList: function(r, t, x, e) {
            $("ol", e).Zebra();
            $(".edit-license", e).click(function() {
                User.License.Edit($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"))
            });
            $(".delete-license", e).click(function() {
                User.License.Delete($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"))
            });
        },
        InitNew: function(r, t, x, e) {
            var type = "NewUserLicense", prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "Type"), "Other", $(prefix + "TypeMore"));
            $("form", e).Validate({ Success: function() { User.License.Refresh() } });
        },
        New: function(userId) {
            Acore.Modal({
                Name: "New License",
                Url: "User/License/New",
                Input: { UserId: userId },
                OnLoad: User.License.InitNew,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Edit: function(id, userId) {
            Acore.Modal({
                Name: "Edit License",
                Url: "User/License/Edit",
                Input: { Id: id, UserId: userId },
                OnLoad: User.License.InitEdit,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Refresh: function() {
            $("#EditUser_LicenseGrid").Load("User/License/List", { UserId: $("#EditUser_LicenseGrid").attr("guid") }, User.License.InitList)
        }
    },
    Rate: {
        Delete: function(id, userId, insurance) {
            if (confirm("Are you sure you want to delete this pay rate?")) {
                U.PostUrl("User/Rate/Delete", { Id: id, UserId: userId, Insurance: insurance }, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) User.Rate.Refresh();
                })
            }
        },
        InitEdit: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { User.Rate.Refresh() } });
        },
        InitList: function(r, t, x, e) {
            $("ol", e).Zebra();
            $(".edit-rate", e).click(function() {
                User.Rate.Edit($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"), $(this).attr("insurance"))
            });
            $(".delete-rate", e).click(function() {
                User.Rate.Delete($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"), $(this).attr("insurance"))
            });
        },
        InitNew: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { User.Rate.Refresh() } });
        },
        New: function(userId) {
            Acore.Modal({
                Name: "New Pay Rate",
                Url: "User/Rate/New",
                Input: { UserId: userId },
                OnLoad: User.Rate.InitNew,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Edit: function(id, userId, insurance) {
            Acore.Modal({
                Name: "Edit License",
                Url: "User/Rate/Edit",
                Input: { Id: id, UserId: userId, Insurance: insurance },
                OnLoad: User.Rate.InitEdit,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Refresh: function() {
            $("#EditUser_PayRateGrid").Load("User/Rate/List", { UserId: $("#EditUser_PayRateGrid").attr("guid") }, User.Rate.InitList)
        }
    },
    NonVisitRate: {
        Delete: function(id, userId) {
            if (confirm("Are you sure you want to delete this pay rate?")) {
                U.PostUrl("User/NonVisitRate/Delete", { Id: id, UserId: userId }, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) User.NonVisitRate.Refresh();
                })
            }
        },
        InitEdit: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { User.NonVisitRate.Refresh() } });
        },
        InitList: function(r, t, x, e) {
            $("ol", e).Zebra();
            $(".edit-nonvisit-rate", e).click(function() {
                User.NonVisitRate.Edit($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"))
            });
            $(".delete-nonvisit-rate", e).click(function() {
                User.NonVisitRate.Delete($(this).attr("guid"), $(this).closest(".acore-grid").attr("guid"))
            });
        },
        InitNew: function(r, t, x, e) {
            $("form", e).Validate({ Success: function() { User.NonVisitRate.Refresh() } });
        },
        New: function(userId) {
            Acore.Modal({
                Name: "New Non-Visit Pay Rate",
                Url: "User/NonVisitRate/New",
                Input: { UserId: userId },
                OnLoad: User.NonVisitRate.InitNew,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Edit: function(id, userId, insurance) {
            Acore.Modal({
                Name: "Edit Non-Visit Pay Rate",
                Url: "User/NonVisitRate/Edit",
                Input: { Id: id, UserId: userId },
                OnLoad: User.NonVisitRate.InitEdit,
                Width: 500,
                Height: 300,
                WindowFrame: false
            })
        },
        Refresh: function() {
            $("#EditUser_NonVisitPayRateGrid").Load("User/NonVisitRate/List", { UserId: $("#EditUser_NonVisitPayRateGrid").attr("guid") }, User.NonVisitRate.InitList)
        }
    },
    InitProfile: function() {
        U.PhoneAutoTab("Edit_Profile_PhonePrimary");
        U.PhoneAutoTab("Edit_Profile_PhoneAlternate");
        $("#editProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Profile updated successfully.", "success");
                            UserInterface.CloseWindow('editprofile');
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").click(function() {
            U.PostUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Your request to reset your signature was successful. Please check your e-mail.", "success");
                    UserInterface.CloseWindow('forgotsignature');
                }
                else {
                    $("#resetSignatureMessage").addClass("error-message").html(result.errorMessage);
                }
            });
        });
    },
    InitMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Missed Visit successfully created.", "success");
                            UserInterface.CloseWindow('newmissedvisit');
                            User.RebindScheduleList();
                            UserInterface.CloseModal();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitEditMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Missed Visit successfully created.", "success");
                            var patientId = $(form).find("[name=PatientId]").val();
                            UserInterface.CloseAndRefresh('editmissedvisit', patientId);
                            UserInterface.CloseModal();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    ListUserRowDataBound: function(e) {
        $("a.tooltip", e.row).each(function() {
            if ($(this).html().length)
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), "") }).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            else $(this).remove();
        });
        var actionCell = $("td.action", e.row).last();
        if (actionCell.length) {
            var statusText = e.dataItem.Status == 1 ? "Deactivate" : "Activate";
            actionCell.html($("<a/>").addClass("link").html("Edit").click(function() {
                User.Edit(e.dataItem.Id);
            })).append($("<a/>").addClass("link").html(statusText).click(function() {
                User[statusText](e.dataItem.Id);
            })).append($("<a/>").addClass("link").html("Delete").click(function() {
                User.Delete(e.dataItem.Id);
            }));
        }
    },
    SelectPermissions: function(control, className) {
        if (control.attr("checked"))
            $(className).each(function() {
                if (!$(this).attr("checked")) $(this).attr("checked", true);
            });
        else
            $(className).each(function() {
                if ($(this).attr("checked")) $(this).attr("checked", false);
            });
    },
    InitList: function(r, t, x, e) {
        $(".t-grid-content", e).css({ 'height': 'auto' });
        $("#UserList_Search", e).append($("<div/>").GridSearchById("#List_User"));
        $('.grid-search', e).css({ 'position': 'relative', 'left': '0', 'margin-left': '5px' });
        $(e).css({
            "background-color": "#d6e5f3"
        });
        $("a.export", e).click(function() {
            U.GetAttachment("Export/Users", U.JsonSerialize($("select", e)));
        });
        $("a.new-user", e).click(function() {
            UserInterface.ShowNewUser();
        });
        var links = $("#UserList_MainButtons").find("li");
        if (links.length) {
            if (links.length > 1) links.eq(0).after($("<div/>").addClass("clear"));
            links.css({ display: "inline-block", "float": "none" });
        }
        var toggleStatusButton = $(".toggle-status", e);
        $("select", e).change(function() {
            U.RebindTGrid($("#List_User"), U.JsonSerialize($("select", e)));
            if ($(this).hasClass("PatientStatusDropDown")) {
                var status = $(this).val();
                toggleStatusButton.text(status == 1 ? "Deactivate" : "Activate");
            }
        });
        toggleStatusButton.click(function() {
            var status = $("#UserList_Status").val();
            if (status == 1) User.MultiDeactivate();
            else User.MultiActivate();
        });
    },
    RebindList: function() {
        User.RebindUsers();
        User.RebindDeletedUser({ SortParams: "DisplayName" });
    },
    RebindUsers: function() {
        U.RebindTGrid($("#List_User"));
    },
    RebindDeletedUser: function(SortParams) {
        $("#DeletedUsers-GridContainer").Load('User/DeletedUser', { SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error')
            { U.Growl('Deleted Users List could not be loaded. Please close this window and try again.', "error"); }
        });
    },
    RebindScheduleList: function() { User.LoadUserSchedule('VisitDate', 'PatientName-ASC'); },
    LoadUserSchedule: function(groupName, SortParams) {
        $("#myScheduledTasksContentId").empty().addClass("loading").load('User/ScheduleGrouped', { groupName: groupName, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl('Schedule List could not be grouped. Please close this window and try again.', "error"); }
            else if (textStatus == "success") { $("#myScheduledTasksContentId").removeClass("loading"); }
        });

    },
    Delete: function(userId) {
        if (confirm("Are you sure you want to delete this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Delete", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Restore: function(userId) {
        if (confirm("Are you sure you want to restore this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Restore", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    MultiRestore: function() {
        var data = $("#List_DeletedUser .t-grid-content tr input:checked").serializeArray();
        if (!data.length) {
            U.Growl("You must select at least one user to restore.", 'error');
        }
        else {
            if (confirm("Are you sure you want to restore these user(s)?")) {
                U.PostUrl("/User/MultiRestore", data, function(result) {
                    if (result.isSuccessful) {
                        User.RebindList();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        }
    },
    MultiDelete: function() {
        var data = $("#List_User .t-grid-content tr input:checked").serializeArray();
        if (!data.length) {
            U.Growl("You must select at least one user to delete.", 'error');
        }
        else {
            if (confirm("Are you sure you want to delete these user(s)?")) {
                U.PostUrl("/User/MultiDelete", data, function(result) {
                    if (result.isSuccessful) {
                        User.RebindList();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        }
    },
    Activate: function(userId) {
        if (confirm("Are you sure you want to activate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Activate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    MultiActivate: function() {
        var data = $("#List_User .t-grid-content tr input:checked").serializeArray();
        if (!data.length) {
            U.Growl("You must select at least one user to activate.", 'error');
        }
        else {
            if (confirm("Are you sure you want to activate these user(s)?")) {
                U.PostUrl("/User/MultiActivate", data, function(result) {
                    if (result.isSuccessful) {
                        User.RebindList();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        }
    },
    Deactivate: function(userId) {
        if (confirm("Are you sure you want to deactivate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Deactivate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    MultiDeactivate: function() {
        var data = $("#List_User .t-grid-content tr input:checked").serializeArray();
        if (!data.length) {
            U.Growl("You must select at least one user to deactivate.", 'error');
        }
        else {
            if (confirm("Are you sure you want to deactivate these user(s)?")) {
                U.PostUrl("/User/MultiDeactivate", data, function(result) {
                    if (result.isSuccessful) {
                        User.RebindList();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        }
    },

    NavigateMonthlyCalendar: function(month, year) {
        $("#window_userschedulemonthlycalendar_content").Load("User/UserCalendarNavigate", { month: month, year: year }, function(r, t, x, e) {
            if (t == "error") $("#window_userschedulemonthlycalendar_content").html(U.AjaxError);
        })
    },
    LoadUserCalendar: function(month, year, SortParams) {
        $("#UserCalendar_GridContainer").empty().addClass("loading").load('User/UserVisits', { month: month, year: year, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl('User calendar could not be grouped. Please close this window and try again.', "error"); }
            else if (textStatus == "success") { $("#UserCalendar_GridContainer").removeClass("loading"); }
        });
    },
    LoadRate: function(fromId, toId) {
        if (confirm("Are you sure you want to apply the same rate as the selected user?")) {
            U.PostUrl("User/LoadUserRate", { fromId: fromId, toId: toId }, function(result) {
                U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                if (result.isSuccessful) {
                    User.Rate.Refresh();
                    User.NonVisitRate.Refresh()
                }
            })
        }
    },
    NewRate: function(userId) {
        Acore.Modal({
            Name: "New User Rate",
            Url: "User/NewUserRate",
            Input: { userId: userId },
            OnLoad: User.InitNewRate,
            Width: "500px",
            Height: "265px",
            WindowFrame: false
        });
    },
    EditRate: function(userId, id, insurance) {
        Acore.Modal({
            Name: "Edit User Rate",
            Url: "User/EditUserRate",
            Input: { userId: userId, id: id, insurance: insurance },
            OnLoad: User.InitEditRate,
            Width: "500px",
            Height: "265px",
            WindowFrame: false
        });
    },
    DeleteRate: function(userId, id, insurance) {
        if (confirm("Are you sure you want to delete this rate?")) {
            U.PostUrl("/User/DeleteUserRate", { userId: userId, id: id, insurance: insurance }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    User.RebindRates();
                }
                else {
                    $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    RebindRates: function() {
        var grid = $('#List_User_Rates').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}