﻿var ManagedBilling = {
    _patientId: "",
    _ClaimId: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { ManagedBilling.UnBlockClickAndEnable(); var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(ManagedBilling._patientId); } else U.Growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    managedClaimTabStripOnSelect: function(e) {
        var element = $(e.item);
        if (!element.hasClass("t-state-disabled")) {
            ManagedBilling.LoadContent(element.find('.t-link').data('ContentUrl'), e.contentElement)
        }
        return false;
    },
    InitCenter: function(r, t, x, e) {
        var patientFilterTimer,
            currentFilterValue,
            selectionGrid = $("#ManagedBillingSelectionGrid .t-grid-content", e),
            mainResult = $("#managedBillingMainResult", e);
        $("#txtSearch_managedBilling_Selection", e).keyup(function() {
            var filterValue = $(this).val();
            patientFilterTimer = setTimeout(function() {
                if (currentFilterValue != filterValue) {
                    currentFilterValue = filterValue;
                    var removeError = function() {
                        if (mainResult.find(".ajaxerror")) mainResult.find("div").show().filter(".ajaxerror").remove();
                    };
                    if (filterValue) {
                        ManagedBilling.PatientListFilter(filterValue);
                        if ($("tr.match:even", selectionGrid).length) {
                            removeError();
                            $("tr.match:first", selectionGrid).click();
                        }
                        else if (!$(".ajaxerror", mainResult).length) mainResult.append($("<div/>").addClass("ajaxerror").append($("<h1/>").html("No patients found matching your search criteria"))).children("div").not(".ajaxerror").hide();
                    } else {
                        removeError();
                        $("tr", selectionGrid).removeClass("match t-alt").show();
                        $("tr:even", selectionGrid).addClass("t-alt");
                        $("tr:first", selectionGrid).click();
                    }
                }
            }, 350);
        }).keydown(function() {
            clearTimeout(patientFilterTimer);
        });
        $('#window_managedclaims .layout').layout({
            west: {
                paneSelector: ".ui-layout-west",
                size: 200,
                minSize: 160,
                maxSize: 400,
                livePaneResizing: true,
                spacing_open: 3
            }
        });
        $('#window_managedclaims .t-grid-content').css({ height: 'auto', top: '26px' });
        $("#ManagedBillingHistory_PrimaryInsuranceId").change(function() {
            if ($("tr", selectionGrid).length) {
                ManagedBilling.FilterActivity();
            }
        });

        var rebind = function() {
            $('#ManagedBillingActivityGrid', e).find('.t-grid-content tbody').empty();
            ManagedBilling._patientId = "";
            ManagedBilling.RebindPatientList();
        };
        $("select.managedBillingBranchCode").change(function() {
            Insurance.loadInsuarnceDropDown('ManagedBillingHistory', 'All', true, function() {
                $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty();
                ManagedBilling._patientId = "";
                ManagedBilling.loadInsuranceForActivity('ManagedBillingHistory', 'All', true, function() { ManagedBilling.RebindPatientList(); });
            });
        });
        $(".top select", e).not(".managedBillingBranchCode").change(function() {
            rebind();
        });
    },
    ReLoadUnProcessedManagedClaim: function(branchId, insuranceId) {
        $("#Billing_ManagedClaimCenterContent ol").addClass('loading');
        $("#Billing_ManagedClaimCenterContent").load('Billing/ManagedGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#Billing_ManagedClaimCenterContent ol").removeClass("loading");
            if (textStatus == 'error') $('#Billing_ManagedClaimCenterContent').html(U.AjaxError)
        })
    },
    SubmitClaimDirectly: function(control) {
        U.PostUrl('Billing/SubmitManagedClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    LoadGeneratedManagedClaim: function(control) {
        U.PostUrl("/Billing/CreateManagedANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
            else U.Growl(result.errorMessage, "error")
        }, function(result) { })
    },
    LoadGeneratedSingleManagedClaim: function(id) {
        U.PostUrl("/Billing/CreateSingleManagedANSI", { Id: id }, function(result) {
            if (result != null) {
                if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                else U.Growl(result.errorMessage, "error")
            }
        }, function(result) { })
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/SubmitManagedClaims', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error")
        }, null)
    },
    RebindPatientList: function() {
        var patientGrid = $('#ManagedBillingSelectionGrid').data('tGrid');
        if (patientGrid != null) patientGrid.rebind({
            branchId: $("select.managedBillingBranchCode").val(),
            statusId: $("select.managedBillingStatusDropDown").val(),
            insurnace: $("select.managedBillingInsuranceDropDown").val(),
            name: $("#txtSearch_managedBilling_Selection").val()
        })
    },
    HideOrShowNewClaimMenu: function() {
        var gridTr = $('#ManagedBillingSelectionGrid').find('.t-grid-content tbody tr');
        if (gridTr != undefined && gridTr != null) {
            if (gridTr.length > 0) $("#managedBillingTopMenu").show();
            else $("#managedBillingTopMenu").hide();
        }
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl)
            $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function() {
                $(contentElement).removeClass("loading");

            });
    },
    Navigate: function(index, id, patientId) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) { if (result.isSuccessful) { ManagedBilling.UnBlockClickAndEnable(); var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(patientId); ManagedBilling.ReLoadUnProcessedManagedClaim($("#Billing_ManagedClaimCenterBranchCode").val(), $("#Billing_ManagedClaimCenterPrimaryInsurance").val()); } else U.Growl(result.errorMessage, 'error'); },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    NavigateBack: function(index) {
        var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    loadGenerate: function(control) {
        var a = $(":input", $(control)).serializeArray();
        Acore.Open("managedclaimsummary", 'Billing/ManagedClaimSummary', function() { }, $(":input", $(control)).serializeArray())

    },
    GenerateAllCompleted: function(control) {
        $("input[name=ManagedClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true)
        });
        ManagedBilling.loadGenerate(control)
    },
    Download: function(control) {
        if ($("input:checkbox:checked", $(control)).length) {
            U.PostUrl("Request/ClaimFormDownload", $("input:checkbox:checked,input[name=BranchId],input[name=PrimaryInsurance]", $(control)).serializeArray(), function(result) {
                U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            });
        } else { U.Growl("Select at least one managed claims to generate.", "error"); }
    },
    NoPatientBind: function(id) { ManagedBilling.RebindActivity(id) },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            if (patientId != ManagedBilling._patientId) {
                $("#managedBillingMainResult").addClass("loading-visibility");
                ManagedBilling._patientId = patientId;
                ManagedBilling._ClaimId = "";
                ManagedBilling._ClaimType = "";
                ManagedBilling.AbortClaimInfo();
                $("#ManagedBillingActivityGrid").data("tGrid").isFiltered = false;
                $("#managedBillingClaimData").empty();
                $("#ManagedBillingHistory_PrimaryInsuranceId").val("0");
                ManagedBilling.RebindActivity(patientId);
            }
        }
    },
    PatientListDataBound: function() {
        ManagedBilling.PatientListFilter($("#txtSearch_managedBilling_Selection").val());
        if ($(".ajaxerror", "#managedBillingMainResult")) $("#managedBillingMainResult div").show().filter(".ajaxerror").remove();
        if ($("#ManagedBillingSelectionGrid .t-grid-content tr").length) {
            $("#managedBillingTopMenu").show();
            if (ManagedBilling._patientId) $("td:contains(" + ManagedBilling._patientId + ")", $("#ManagedBillingSelectionGrid")).closest("tr").click();
            else $("#ManagedBillingSelectionGrid .t-grid-content tr" + ($("#txtSearch_managedBilling_Selection").val().length ? ".match" : "")).eq(0).click();
        } else {
            $("#managedBillingMainResult").append($("<div/>").addClass("ajaxerror").append($("<h1/>").html("No patients found matching your search criteria."))).children("div").not(".ajaxerror").hide();
            $("#managedBillingTopMenu").hide();
        }
        if ($("#ManagedBillingSelectionGrid .t-state-selected").length) $("#ManagedBillingSelectionGrid .t-grid-content").scrollTop($("#ManagedBillingSelectionGrid .t-state-selected").position().top - 50);
    },
    PatientListFilter: function(text) {
        var search = text.split(" ");
        $("tr", "#ManagedBillingSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++)
            $("td", "#ManagedBillingSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        $("tr.match", "#ManagedBillingSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#ManagedBillingSelectionGrid .t-grid-content").addClass("t-alt");
    },
    FilterActivity: function() {
        var activityGrid = $('#ManagedBillingActivityGrid').data('tGrid'),
            insuranceFilter = $("#ManagedBillingHistory_PrimaryInsuranceId").val();
        //If the claims have not been filtered yet save the unfiltered data into the grid for later usage
        if (!activityGrid.isFiltered) {
            activityGrid.unFilteredData = activityGrid.data;
        }
        activityGrid.isFiltered = insuranceFilter != 0;
        var result = [];
        //If the filter was changed to something other than All then filter the unFilteredData by insuranceFilter
        if (activityGrid.isFiltered) {
            result = $.grep(activityGrid.unFilteredData, function(item) {
                return item.PrimaryInsuranceId == insuranceFilter;
            });
            //Otherwise set result to unFilteredData
        } else {
            result = activityGrid.unFilteredData;
        }
        //Binds the telerik grids data to the new result of data
        activityGrid.dataBind(result);
    },
    RebindActivity: function(id) {
        var activityGrid = $('#ManagedBillingActivityGrid').data('tGrid');
        if (activityGrid != null) {
            activityGrid.rebind({
                patientId: id,
                insuranceId: 0
            });
            if ($('#ManagedBillingActivityGrid').find('.t-grid-content tbody').is(':empty')) $("#managedBillingClaimData").empty();
        }
    },

    OnClaimRowSelected: function(e) {
        if (e.row.cells[12] != undefined && ManagedBilling._patientId != undefined && ManagedBilling._patientId != "") {
            var claimId = e.row.cells[12].innerHTML;
            ManagedBilling._ClaimId = claimId;
            ManagedBilling.loadClaimInfo(ManagedBilling._patientId, claimId);
        }
    },
    OnClaimDataBinding: function(e) {
        if (!ManagedBilling._patientId || ManagedBilling._patientId == U.GuidEmpty) e.preventDefault();
        var grid = $(e.target).data("tGrid"),
            selectUrl = grid.ajax.selectUrl;
        if (selectUrl.indexOf(U.GuidEmpty) > -1) {
            grid.ajax.selectUrl = selectUrl.replace(U.GuidEmpty, ManagedBilling._patientId);
        }
    },
    OnCliamDataBound: function() {
        var mainResult = $("#managedBillingMainResult").removeClass("loading-visibility"),
            hasError = $(".ajaxerror", mainResult).length > 0,
            $grid = $("#ManagedBillingActivityGrid"),
            grid = $grid.data("tGrid"),
            rows = $(".t-grid-content tr", $grid);
        if (rows.length) {
            if (hasError) $("div", mainResult).show().filter(".ajaxerror").remove();
            if (grid.isSorting) {
                //Gets the Managed Claim with a specific id
                var claimRow = $("td:contains(" + ManagedBilling._ClaimId + ")", rows);
                if (claimRow.length) {
                    claimRow.parent().addClass("t-state-selected");
                    return;
                }
            }
            rows.eq(0).click();
            //Handles the error message to be displayed due to no claims being found
        } else {
            $("#managedBillingClaimData").empty();
            if (grid.isFiltered) {
                U.AddTGridEmptyRow("No claims could be found using the insurance you have selected.  <br/>Either change the insurance filter above or create a new claim for this insurance.", $grid);
            } else {
                var button = $("#managedBillingTopMenu a:contains('New Claim')"),
                    buttonsToAdd = button.length ? $("<div/>").Buttons([{ Text: "New Claim", Click: function() { button.click(); } }]) : $("<div/>");
                if (!hasError)
                    mainResult.append(
                        $("<div/>").addClass("ajaxerror").append(
                            $("<h1/>").html("No claims could be found for the patient you have selected.")).append(buttonsToAdd
                        )).children("div").not(".ajaxerror").hide();
            }
        }
    },
    AbortClaimInfo: function() {
        RequestTracker.Abort('Billing/ManagedSnapShotClaimInfo');
    },
    loadClaimInfo: function(patientId, claimId) {
        $("#managedBillingClaimData").LoadTracked('Billing/ManagedSnapShotClaimInfo', { patientId: patientId, claimId: claimId });
    },
    ManagedComplete: function(id, patientId) {
        U.PostUrl('Billing/ManagedComplete', { id: id, patientId: patientId, total: $("#Claim_Total").val() }, function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimedit');
                U.Growl(result.errorMessage, "success");
                ManagedBilling.Rebind();
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    InitClaim: function() {
        U.InitTemplate($("#updateManagedClaimForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.RebindActivity(ManagedBilling._patientId);
        }, "Claim update is successfully.")
    },
    InitNewPayment: function() {
        U.InitTemplate($("#newManagedClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was created successfully.")
    },
    InitEditPayment: function() {
        U.InitTemplate($("#updateManagedClaimPaymentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was updated successfully.")
    },
    InitNewAdjustment: function() {
        U.InitTemplate($("#newManagedClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was created successfully.")
    },
    InitEditAdjustment: function() {
        U.InitTemplate($("#updateManagedClaimAdjustmentForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.Rebind();
        }, "Payment was updated successfully.")
    },
    InitNewClaim: function() {
        U.InitTemplate($("#createManagedClaimForm"), function() {
            UserInterface.CloseModal();
            ManagedBilling.RebindActivity(ManagedBilling._patientId);
            ManagedBilling.Rebind();
        }, "Claim is created successfully.")
    },
    DeleteClaim: function(patientId, id) {
        if (confirm("Are you sure you want to delete this claim?")) U.PostUrl("/Billing/DeleteManagedClaim", { patientId: patientId, id: id }, function(result) {
            if (result.isSuccessful) {
                ManagedBilling.RebindActivity(patientId);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        })
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
                U.PostUrl("Billing/ChangeSupplyBillStatus", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    }
                })
            }
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else {
            if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
                if (inputs != null) {
                    inputs[inputs.length] = { name: "Id", value: Id };
                    inputs[inputs.length] = { name: "PatientId", value: PatientId };
                    U.PostUrl("Billing/ManagedCareSuppliesDelete", inputs, function(result) {
                        if (result != null && result.isSuccessful) {
                            U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                            U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        }
                    })
                }
            }
        }
    },
    loadInsuranceForActivity: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/InsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() }, function(data) {
            var s = $("select#" + reportName + "_PrimaryInsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[s.get(0).options.length] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false) })
            s.get(0).options[s.get(0).options.length] = new Option("No Insurance assigned", "-1", false, false);
            if (action != null && action != undefined && typeof (action) == "function") action();
        })
    },
    InitBillData: function() { U.InitTemplate($("#newInsuranceBillData"), function() { ManagedBilling.RebindManagedBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully saved"); },
    InitEditBillData: function() { U.InitTemplate($("#editInsuranceBillData"), function() { ManagedBilling.RebindManagedBillDataList(); UserInterface.CloseModal(); }, "Visit rate successfully updated"); },
    RebindManagedBillDataList: function() { var grid = $("#ManagedInsurance_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ ClaimId: $("#ManagedInsurance_ClaimId").val() }); } },
    DeleteBillData: function(claimId, Id) {
        if (confirm("Are you sure you want to delete this visit rate?")) {
            U.PostUrl("/Billing/ManagedClaimDeleteBillData", { ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.RebindManagedBillDataList();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    ReloadInsuranceData: function(claimId) {
        if (confirm("Any edits you have made to the visit rates or locators will be lost. Are you sure you want to reload the insurance?")) {
            U.PostUrl("/Billing/ManagedClaimReloadInsurance", { Id: claimId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.LoadContent($("#ManagedClaimTabStrip ul li.t-state-active .t-link").data('ContentUrl'), $("#ManagedClaimTabStrip div.t-content.t-state-active"));
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    UnBlockClickAndEnable: function() {
        var element = $("#ManagedClaimTabStrip ul li.t-state-active").next();
        element.removeClass("t-state-disabled");
        element.find(".t-link").unbind('click', U.BlockClick);
    },
    DeletePayment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this payment?")) {
            U.PostUrl("/Billing/DeleteManagedClaimPayment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    Rebind: function() {
        U.RebindTGrid($('#ManagedBillingPaymentsGrid'));
        U.RebindTGrid($('#ManagedBillingAdjustmentsGrid'));
        ManagedBilling.RebindActivity(ManagedBilling._patientId);
    },
    ToolTip: function(Id) {
        $("a.tooltip", $(Id)).each(function() {
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), "") });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    DeleteAdjustment: function(patientId, claimId, Id) {
        if (confirm("Are you sure you want to delete this adjustment?")) {
            U.PostUrl("/Billing/DeleteManagedClaimAdjustment", { PatientId: patientId, ClaimId: claimId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    ManagedBilling.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    OnRowWithCommentsDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    OnAdjustmentsDataBound: function(e) {
        U.ToolTip($("#ManagedBillingAdjustmentsGrid .t-grid-content tr"), "");
    },
    InitVisits: function() {
        $("#managedBillingVisitForm .day-per-line").each(function(index, Element) {
            var checkbox = $(Element);

            checkbox.closest(".main-line-item").find("ol input").each(function(index2, actualVisitCheckbox) {
                var jActualVisitCheckbox = $(actualVisitCheckbox);
                jActualVisitCheckbox.change(function() {
                    if (jActualVisitCheckbox.prop("checked") == false) {
                        checkbox.prop('checked', false);
                    }
                    var secondarySection = jActualVisitCheckbox.closest(".main-line-item ol");
                    var totalCount = secondarySection.find("input").length;
                    var checkedCount = secondarySection.find("input:checked").length;
                    if (totalCount == checkedCount) {
                        checkbox.prop('checked', true);
                    }
                });
            });

            checkbox.change(function() {
                if (checkbox.prop("checked") == true) {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', true);
                    });
                } else {
                    checkbox.parent().parent().next().find("input").each(function(index2, secondary) {
                        $(secondary).prop('checked', false);
                    });
                }
            });

        });
    }
}