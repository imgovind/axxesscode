﻿if (typeof Schedule == "undefined") var Schedule = new Object();
$.extend(Schedule, {
    Center: {
        AcoreId: "ScheduleCenter",          // The Id of the window in Acore
        DieticianData: [],                  // List of Dietician Scheduled Events
        Discipline: "all",                  // Current Selected Discipline
        //DisciplineIndex: 0,                 // Current Selected Discipline Index
        EndDate: new Date(),                // Episode End Date
        EpisodeData: [],                    // All Schedule Events
        EpisodeId: "",                      // Current Selected Episode ID
        EpisodeList: [],                    // List of Current Patient's Episodes
        HHAData: [],                        // List of HHA Scheduled Events
        IsRebind: false,
        IsDisciplineChange: false,
        MSWData: [],                        // List of MSW Scheduled Events
        NursingData: [],                    // List of Nursing Scheduled Events
        OrdersData: [],                     // List of Orders and Care Plans Scheduled Events
        PatientToSelect: "",                // ID of Patient to be Selected
        PatientId: "",                      // Current Selected Patient ID
        PatientName: "",                    // Current Patient Name
        PatientStatus: "",                  // Current Patient Status
        StartDate: new Date(),              // Episode Start Date
        //TableId: "NursingScheduleTable",    // Current Scheduler Tab
        TaskDropDown: [],                   // All Schedulable Tasks
        FilteredTaskDropDown: [],           // Schedulable Tasks filtered by the current discipline
        TherapyData: [],                    // List of Therapy Schedule Events
        UserDropDown: [],                   // All Schedulable Users
        SchedulerMaxLines: 5,               // Number of entries that can be added into a scheduler tab at one time
        // Build Calendar
        BuildCalendar: function(events) {
            var startDates = [new XDate(Schedule.Center.StartDate), new XDate(Schedule.Center.StartDate), new XDate(Schedule.Center.StartDate)];
            startDates[0].setDate(1);
            startDates[1].setDate(1).addMonths(1);
            startDates[2].setDate(1).addMonths(2);
            var endDates = [new XDate(startDates[1]), new XDate(startDates[2]), new XDate(startDates[2])];
            endDates[0].addDays(-1);
            endDates[1].addDays(-1);
            endDates[2].addMonths(1).addDays(-1);
            var currentDate = [new XDate(startDates[0]), new XDate(startDates[1]), new XDate(startDates[2])];
            currentDate[0].setDate(startDates[0].getDate() - startDates[0].getDay());
            currentDate[1].setDate(startDates[1].getDate() - startDates[1].getDay());
            currentDate[2].setDate(startDates[2].getDate() - startDates[2].getDay());
            $(".cal", "#ScheduleCenter_Top").remove();
            var shallowCopy = $.merge([], events);
            for (var c = 0; c < 3; c++) {
                if (startDates[c] <= Schedule.Center.EndDate) {
                    $(".calendar-container", "#ScheduleCenter_Top").append(
                        $("<div/>", { "class": "cal" }).append(
                            $("<table/>").append(
                                $("<thead/>").append(
                                    $("<tr/>").append(
                                        $("<th/>", { colspan: 7, "class": "caltitle", text: startDates[c].toString("MMM yyyy") }))).append(
                                    $("<tr/>").append(
                                        $("<th/>", { text: "Su" })).append(
                                        $("<th/>", { text: "Mo" })).append(
                                        $("<th/>", { text: "Tu" })).append(
                                        $("<th/>", { text: "We" })).append(
                                        $("<th/>", { text: "Th" })).append(
                                        $("<th/>", { text: "Fr" })).append(
                                        $("<th/>", { text: "Sa" })))).append(
                                $("<tbody/>"))));
                    var maxWeek = Math.ceil((startDates[c].getDay() + endDates[c].getDate()) / 7) - 1;
                    for (var i = 0; i <= maxWeek; i++) {
                        var addedDate = i * 7;
                        var startOfWeek = new XDate(currentDate[c]);
                        startOfWeek.addDays(addedDate);
                        var endOfWeek = new XDate(startOfWeek);
                        endOfWeek.addDays(7);
                        if (endOfWeek >= Schedule.Center.StartDate && startOfWeek <= endDates[c] && startOfWeek <= Schedule.Center.EndDate) {
                            $("tbody", "#ScheduleCenter_Top .cal:last").append(
                                $("<tr/>"));
                            var tooltip = "";
                            for (var j = 0; j <= 6; j++) {
                                $("tbody tr:last", "#ScheduleCenter_Top .cal:last").append(
                                    $("<td/>"));
                                var specificDate = new XDate(currentDate[c]);
                                specificDate.addDays(j + addedDate);
                                if (specificDate < Schedule.Center.StartDate || specificDate < startDates[c] || specificDate > endDates[c] || specificDate > Schedule.Center.EndDate)
                                    $("td:last", "#ScheduleCenter_Top .cal:last").addClass("inactive");
                                else {
                                    var numberOfEvents = 0,
                                        missed = "",
                                        status = "",
                                        allEvents = "";
                                    if (shallowCopy.length) {
                                        shallowCopy = $.grep(shallowCopy, function(item) {
                                            //If there is no difference between the days then add to allEvents
                                            if (!specificDate.diffDays(item.Date)) {
                                                numberOfEvents++;
                                                allEvents += (allEvents.length ? "<br/>" : "") + item.Task + " - <em>" + item.User + "</em>";
                                                if (item.IsMissed) missed = "missed";
                                                else if (item.IsComplete) status = "completed";
                                                return false;
                                            } else return true;
                                        });
                                    }
                                    tooltip = "<strong>" + U.DisplayDate(specificDate, true) + "</strong><hr/>" + allEvents;
                                    if (numberOfEvents) $("td:last", "#ScheduleCenter_Top .cal:last").addClass("status " + status + " scheduled " + missed + (numberOfEvents > 1 ? " multi" : "")).append(
                                        $("<div/>").addClass("datelabel").attr("tooltip", tooltip).tooltip({
                                            track: true,
                                            showURL: false,
                                            top: 15,
                                            left: -15,
                                            extraClass: "calday",
                                            bodyHandler: function() { return $(this).attr("tooltip"); }
                                        }).append(
                                            $("<a/>").text(specificDate.getDate()).attr("date", U.DisplayDate(specificDate))));
                                    else $("td:last", "#ScheduleCenter_Top .cal:last").append(
                                        $("<div/>").addClass("datelabel").append(
                                            $("<a/>").text(specificDate.getDate()).attr("date", U.DisplayDate(specificDate))));
                                }
                            }
                        }
                    }
                }
            }
            Schedule.Center.PositionBottom();
        },
        //Rebinds the data to the calendar without deleting the calendar. This is used for refreshing the schedule center without the patient or episode changing.
        RebindCalendar: function(events) {
            var calendarDays = $(".cal td", "#ScheduleCenter_Top");
            calendarDays.removeClass("completed scheduled multi missed").find("div").attr("tooltip", "").tooltip("destroy");
            if (events.length) {
                var shallowCopy = $.merge([], events);
                calendarDays.not(".inactive").each(function() {
                    var el = $(this),
                        cache = el.data('cache') || {},
                        specificDay = el.children().first();
                    if (!cache[Schedule.Center.Discipline]) {
                        //No more events to add to the calendar, stop looping
                        if (!shallowCopy.length) return false;
                        var specificDate = new XDate($("a", specificDay).attr("date")),
                            numberOfEvents = 0,
                            missed = "",
                            status = "",
                            allEvents = "";
                        shallowCopy = $.grep(shallowCopy, function(item) {
                            //If there is no difference between the days then add the date to allEvents
                            if (!specificDate.diffDays(item.Date)) {
                                numberOfEvents++;
                                allEvents += (allEvents.length ? "<br/>" : "") + item.Task + " - <em>" + item.User + "</em>";
                                if (item.IsMissed) missed = "missed";
                                else if (item.IsComplete) status = "completed";
                                return false;
                            } else return true;
                        });
                        cache[Schedule.Center.Discipline] = {
                            'isEmpty': !!allEvents.length,
                            'tooltip': "<strong>" + U.DisplayDate(specificDate, true) + "</strong><hr/>" + allEvents,
                            'class': status + " scheduled " + missed + (numberOfEvents > 1 ? " multi" : "")
                        };
                    }
                    if (cache[Schedule.Center.Discipline]['isEmpty']) {
                        this.className = cache[Schedule.Center.Discipline]['class'];
                        specificDay.attr("tooltip", cache[Schedule.Center.Discipline]['tooltip']).tooltip({
                            track: true,
                            showURL: false,
                            top: 15,
                            left: -15,
                            extraClass: "calday",
                            bodyHandler: function() { return $(this).attr("tooltip"); }
                        });
                    }
                    el.data('cache', cache);
                });
            }
        },
        Init: function(r, t, x, e) {
            $(".layout", e).layout({
                west: {
                    paneSelector: ".ui-layout-west",
                    size: 200,
                    minSize: 160,
                    maxSize: 400,
                    livePaneResizing: true,
                    spacing_open: 3
                }
            });
            $(".t-grid-content", e).css("height", "auto");
            $(".window-menu ul", "#ScheduleCenter_Top").Menu();
            Schedule.Center.PositionBottom();
            var patientFilterTimer,
                currentFilterValue,
                selectionGrid = $("#ScheduleCenter_PatientSelectionGrid .t-grid-content", e);
            $(".top input", e).keyup(function() {
                var filterValue = $(this).val();
                patientFilterTimer = setTimeout(function() {
                    if (currentFilterValue != filterValue) {
                        currentFilterValue = filterValue;
                        if (filterValue) {
                            Schedule.Center.PatientSelector.Filter(filterValue);
                            if ($("tr.match:even", selectionGrid).length) $("tr.match:first", selectionGrid).click();
                            else Schedule.Center.SetPatientError();
                        } else {
                            $("tr", selectionGrid).removeClass("match t-alt").show();
                            $("tr:even", selectionGrid).addClass("t-alt");
                            $("tr:first", selectionGrid).click();
                        }
                    }
                }, 350);
            }).keydown(function() {
                clearTimeout(patientFilterTimer);
            });
            $("#ScheduleCenter_NewEpisode", e).click(function() {
                UserInterface.ShowNewEpisodeModal(Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_ScheduleEmployee", e).click(function() {
                UserInterface.ShowMultipleDayScheduleModal(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_InactiveEpisodes", e).click(function() {
                Schedule.loadInactiveEpisodes(Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_Frequencies", e).click(function() {
                Schedule.ShowFrequencies(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_VisitLog", e).click(function() {
                UserInterface.VisitLogEdit(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_MasterCalendar", e).click(function() {
                Schedule.loadMasterCalendar(Schedule.Center.PatientId, Schedule.Center.EpisodeId);
            });
            $("#ScheduleCenter_ReassignSchedules", e).click(function() {
                UserInterface.ShowMultipleReassignModal(Schedule.Center.EpisodeId, Schedule.Center.PatientId, 'Episode');
            });
            $("#ScheduleCenter_DeleteSchedules", e).click(function() {
                UserInterface.ShowMultipleDelete(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
            });
            $("#ScheduleCenter_EpisodeDropDown", e).change(function() {
                // Schedule.Center.ChangeEpisode($(this).val());
                Schedule.Center.ReloadByEpisode($(this).val());
            });
            $("#ScheduleCenter_ViewPatientCharts").click(function() {
                UserInterface.ShowPatientChart(Schedule.Center.PatientId, Schedule.Center.PatientStatus);
            });
            if ($("#" + Schedule.Center.AcoreId + "_TabStrip").length) Schedule.Center.Scheduler.Init(r, t, x, e);
            Schedule.Center.PatientId = "";
            $(".top select", ".ui-layout-west").change(function() {
                Schedule.Center.PatientId = "";
                Schedule.Center.PatientToSelect = "";
                $('#ScheduleCenter_MainResult').addClass("loading-visibility");
                U.RebindTGrid($('#ScheduleCenter_PatientSelectionGrid'), { branchId: $("#ScheduleCenter_BranchCode").val(), statusId: $("#ScheduleCenter_PatientStatus").val(), paymentSourceId: $("#ScheduleCenter_PaymentSource").val() });
                //U.FilterResults("Schedule");
            });
        },
        //Initially loads the user and discipline task data when the schedule center is loaded.
        LoadDropDowns: function(e) {
            if (!Schedule.Center.UserDropDown.length) {
                U.PostUrl("User/All", {}, function(result) {
                    Schedule.Center.UserDropDown = [];
                    Schedule.Center.UserDropDown.push({ Value: "", Text: "-- Select User --" });
                    $.each(result, function() {
                        Schedule.Center.UserDropDown.push({ Value: $(this).attr("Id"), Text: $(this).attr("DisplayName") });
                    });
                    Schedule.Center.PopulateDropDowns(e);
                });
                U.PostUrl("LookUp/DisciplineTasks", {}, function(result) {
                    Schedule.Center.TaskDropDown = [];
                    $.each(result, function() {
                        Schedule.Center.TaskDropDown.push({ value: $(this).attr("Id"), text: $(this).attr("Task"), Discipline: $(this).attr("DisciplineId"), IsBillable: $(this).attr("IsBillable"), IsMultiple: $(this).attr("IsMultiple") });
                    });
                    Schedule.Center.PopulateDropDowns(e);
                });
            } else {
                Schedule.Center.PopulateDropDowns(e);
            }
        },
        Load: function() {
            var mainResultContent = $("#ScheduleCenter_MainResult").addClass("loading-visibility"),
                mainDivs = mainResultContent.children("div");
            if (mainDivs.length > 2) {
                mainDivs.not(".ajaxerror").show();
                mainDivs.filter(".ajaxerror").remove();
            }
            if (U.IsGuid(Schedule.Center.PatientToSelect)) {
                Schedule.Center.PatientId = Schedule.Center.PatientToSelect;
                Schedule.Center.PatientToSelect = "";
            }
            Schedule.Center.IsRebind = true;
            U.PostTrackedUrl("Schedule/Activity", { patientId: Schedule.Center.PatientId, episodeId: Schedule.Center.EpisodeId }, function(result) {
                Schedule.Center.Scheduler.Reset();
                if (result.id != U.GuidEmpty) {
                    Schedule.Center.PatientStatus = $("#ScheduleCenter_PatientStatus").val();
                    Schedule.Center.StartDate = new Date(result.startDate);
                    Schedule.Center.EndDate = new Date(result.endDate);
                    Schedule.Center.ResetEpisodeData(result.scheduleEvents);
                    Schedule.Center.EpisodeList = result.episodes;
                    Schedule.Center.PatientName = result.patientName;
                    Schedule.Center.EpisodeId = result.id;
                    $("#ScheduleCenter_PreviousEpisode, #ScheduleCenter_NextEpisode").each(function() {
                        $(this).unbind("click");
                        var identifier = $(this).attr("identifier"),
                            episodeId = result[identifier],
                            hasEpisode = episodeId != U.GuidEmpty;
                        $(this).parent().toggle(hasEpisode);
                        if (hasEpisode)
                            $(this).click(function() {
                                Schedule.Center.ChangeEpisode(episodeId);
                            });
                    });
                    Schedule.Center.PopulateDropDowns(mainResultContent);
                    Schedule.Center.Render();
                    var scheduler = $("#" + Schedule.Center.AcoreId + "_TabStrip");
                    if (scheduler.length) {
                        // Click to add
                        $(".datelabel", mainResultContent).parent().bind("click", function() {
                            Schedule.Center.Scheduler.RowAdd($("a", this).attr("date"));
                        });
                        // Hide items if scheduler is open
                        if (scheduler.is(":visible")) $(".scheduler-toggle", mainResultContent).hide();
                    }
                    //Uses the DOM attribute scrollTop to reset the grid back to the top
                    $("#ScheduleActivityGrid .t-grid-content")[0].scrollTop = 0;
                } else {
                    $.proxy(Schedule.Center.SetError, mainResultContent, "No Episodes found for this patient.", [
                        { Text: "Add New Episode", Click: function() { UserInterface.ShowNewEpisodeModal(Schedule.Center.PatientId); } },
                        { Text: "Inactive Episodes", Click: function() { Schedule.loadInactiveEpisodes(Schedule.Center.PatientId); } }
                    ])();
                }
                mainResultContent.removeClass("loading-visibility");
                Schedule.Center.IsRebind = false;
            }, function(result) {
                if (result.statusText != "abort") {
                    $.proxy(Schedule.Center.SetError, mainResultContent, "A problem occurred while procuring the episode's schedule.", [
                        { Text: "Refresh", Click: function() { Schedule.Center.Load(); } }
                    ], true)();
                }
            });
        },
        ReloadByEpisode: function(episodeId) {
            U.PostTrackedUrl("Schedule/Activity", { patientId: Schedule.Center.PatientId, episodeId: episodeId }, function(result) {
                Schedule.Center.StartDate = new Date(result.startDate);
                Schedule.Center.EndDate = new Date(result.endDate);
                Schedule.Center.ResetEpisodeData(result.scheduleEvents);
                Schedule.Center.EpisodeId = result.id;
                Schedule.Center.Rerender(true);
                var episodeDropDown = $("#ScheduleCenter_EpisodeDropDown");
                if (episodeDropDown) {
                    var selectedOption = $("option[value='" + episodeId + "']", episodeDropDown);
                    if (selectedOption) {
                        selectedOption.prop("selected");
                        var previousEpisode = selectedOption.next($("option", episodeDropDown));
                        if (previousEpisode && previousEpisode.val() != U.GuidEmpty) {

                        }
                        var nextEpisode = selectedOption.prev($("option", episodeDropDown));
                        if (nextEpisode && nextEpisode.val() != U.GuidEmpty) {

                        }
                    }
                }
            });
        },
        ChangeEpisode: function(episodeId) {
            Schedule.Center.EpisodeId = episodeId;
            Schedule.Center.Load();
        },
        //Will only reload the schedule center if the patientId matches the currently selected patient in the schedule center
        Reload: function(patientId) {
            if (patientId == Schedule.Center.PatientId) Schedule.Center.Load();
        },
        RefreshSchedule: function(patientId, episodeId) {
            if (patientId == Schedule.Center.PatientId && (!episodeId || episodeId == Schedule.Center.EpisodeId)) {
                U.PostUrl("Schedule/Activities", { patientId: Schedule.Center.PatientId, episodeId: Schedule.Center.EpisodeId }, function(result) {
                    Schedule.Center.ResetEpisodeData(result.scheduleEvents);
                    Schedule.Center.Scheduler.Reset();
                });
            }
        },
        ResetEpisodeData: function(scheduleEvents) {
            for (var index = 0; index < scheduleEvents.length; index++) {
                scheduleEvents[index] = $.extend({
                    PatientId: Schedule.Center.PatientId,
                    OasisProfileUrl: "",
                    StatusComment: "",
                    EpisodeNotes: "",
                    PrintUrl: "",
                    OnClick: "",
                    Comments: "",
                    IsVisitVerified: 0,
                    HasAttachments: 0,
                    IsMissed: 0,
                    Orphaned: 0,
                    IsComplete: 0,
                    AP: "",
                    Task: ""
                }, scheduleEvents[index]);
            }
            Schedule.Center.EpisodeData = scheduleEvents;
            Schedule.Center.NursingData = [];
            Schedule.Center.HHAData = [];
            Schedule.Center.MSWData = [];
            Schedule.Center.TherapyData = [];
            Schedule.Center.Dietician = [];
            Schedule.Center.OrdersData = [];
            for (var i = 0; i < Schedule.Center.EpisodeData.length; i++) {
                var d = Schedule.Center.EpisodeData[i].Discipline;
                //Nursing
                if (d == 1 || d == 8) Schedule.Center.NursingData.push(Schedule.Center.EpisodeData[i]);
                //HHA
                else if (d == 5) Schedule.Center.HHAData.push(Schedule.Center.EpisodeData[i]);
                //MSW
                else if (d == 6) Schedule.Center.MSWData.push(Schedule.Center.EpisodeData[i]);
                //Therapy
                else if (d == 2 || d == 3 || d == 4) Schedule.Center.TherapyData.push(Schedule.Center.EpisodeData[i]);
                //Dietician
                else if (d == 7) Schedule.Center.DieticianData.push(Schedule.Center.EpisodeData[i]);
                //Orders
                else if (d == 9) Schedule.Center.OrdersData.push(Schedule.Center.EpisodeData[i]);
            }
        },
        EditEpisode: function() {
            UserInterface.ShowEditEpisodeModal(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
        },
        // Change the Selected Discipline Filter and Refresh Calendar and Activites
        ChangeDiscipline: function(discipline) {
            if (Schedule.Center.Discipline != discipline) {
                Schedule.Center.IsDisciplineChange = true;
                Schedule.Center.FilteredTaskDropDown = discipline == "all" ? Schedule.Center.TaskDropDown : $.grep(Schedule.Center.TaskDropDown, function(item) {
                    var disciplineId = Schedule.Center.GetDisciplineId(discipline);
                    return disciplineId == item.Discipline || (disciplineId == -1 && (item.Discipline == 2 || item.Discipline == 3 || item.Discipline == 4));
                });
                Schedule.Center.Discipline = discipline;
                Schedule.Center.Render();
                Schedule.Center.IsDisciplineChange = false;
            }
        },
        SetPatientError: function() {
            $.proxy(Schedule.Center.SetError, $("#ScheduleCenter_MainResult"), "No Patients Found to Meet Your Search Requirements.", [
                { Text: "Add New Patient", Click: function() { Acore.Open('newpatient'); } }
            ], true)();
        },
        SetError: function(message, buttons, removeRefresh) {
            if (removeRefresh) $(this).removeClass("loading-visibility");
            var error = $("<div/>").addClass("ajaxerror").append(
                $("<h1/>").text(message)).append(
                $("<div/>").Buttons(buttons));
            if ($(".ajaxerror", this).length) $(".ajaxerror", this).html(error);
            else $(this).append(error).children("div").not(".ajaxerror").hide();
        },
        GetDisciplineId: function(disicipline) {
            var id = 0;
            if (disicipline == "Nursing") id = 1;
            else if (disicipline == "MSW") id = 6;
            else if (disicipline == "HHA") id = 5;
            else if (disicipline == "Dietician") id = 7;
            else if (disicipline == "Orders") id = 9;
            else if (disicipline == "Therapy") id = -1;
            return id;
        },
        PatientSelector: {
            Select: function(patientId, status) {
                var selectionFilter = $("#ScheduleCenter_TextFilter"),
                    selectionGrid = $("#ScheduleCenter_PatientSelectionGrid"),
                    selectionGridContent = $(".t-grid-content", selectionGrid);
                if (selectionFilter.val() != "") {
                    $("#ScheduleCenter_TextFilter").val("");
                    $("tr", selectionGridContent).removeClass("match t-alt").show();
                    $("tr:even", selectionGridContent).addClass("t-alt");
                }
                if (patientId == Schedule.Center.PatientId) {
                    Schedule.Center.RefreshSchedule(patientId, Schedule.Center.EpisodeId);
                    return;
                }
                var statusNumber = !isNaN(status) ? status : (status.toLowerCase() == "false" ? "1" : "2"),
                    statusDropDown = $("#ScheduleCenter_PatientStatus");
                Schedule.Center.PatientToSelect = patientId;
                Schedule.Center.EpisodeId = "";
                Schedule.Center.PatientStatus = status;
                if (statusNumber != statusDropDown.val()) {
                    statusDropDown.val(statusNumber);
                    U.RebindTGrid(selectionGrid, { branchId: $("#ScheduleCenter_BranchCode").val(), statusId: statusNumber, paymentSourceId: $("#ScheduleCenter_PaymentSource").val() });
                } else {
                    var row = $("tr td.t-last:contains('" + patientId + "')", selectionGridContent).parent();
                    $("tr.t-state-selected", selectionGridContent).removeClass("t-state-selected");
                    row.addClass("t-state-selected");
                    var scroll = $(row).position().top + selectionGridContent.scrollTop() - 24;
                    selectionGridContent.animate({ scrollTop: scroll }, "slow");
                    Schedule.Center.Load();
                }
            },
            OnDataBound: function() {
                Schedule.Center.PatientSelector.Filter($("#ScheduleCenter_TextFilter").val());
                if ($("#ScheduleCenter_PatientSelectionGrid .t-grid-content tr").length) {
                    var patientId = Schedule.Center.PatientToSelect || Schedule.Center.PatientId;
                    if (patientId.length) $("td:contains(" + patientId + ")", $("#ScheduleCenter_PatientSelectionGrid")).closest("tr").click();
                    else $("#ScheduleCenter_PatientSelectionGrid .t-grid-content tr" + ($("#ScheduleCenter_TextFilter").val().length ? ".match" : "")).eq(0).click();
                } else Schedule.Center.SetPatientError();
                if ($("#ScheduleCenter_PatientSelectionGrid .t-state-selected").length) $("#ScheduleCenter_PatientSelectionGrid .t-grid-content").scrollTop($("#ScheduleCenter_PatientSelectionGrid .t-state-selected").position().top - 50);
            },
            Filter: function(text) {
                var search = text.split(" ");
                $("tr", "#ScheduleCenter_PatientSelectionGrid .t-grid-content").removeClass("match").hide();
                for (var i = 0; i < search.length; i++)
                    $("td", "#ScheduleCenter_PatientSelectionGrid .t-grid-content").each(function() {
                        if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                $("tr.match", "#ScheduleCenter_PatientSelectionGrid .t-grid-content").removeClass("t-alt").show();
                $("tr.match:even", "#ScheduleCenter_PatientSelectionGrid .t-grid-content").addClass("t-alt");
            },
            OnRowSelect: function(e) {
                if (e.row.cells[2] != undefined) {
                    var patientId = e.row.cells[2].innerHTML;
                    if (patientId != Schedule.Center.PatientId || patientId == Schedule.Center.PatientToSelect) {
                        //if (!Schedule.Center.IsRebind) 
                        Schedule.Center.EpisodeId = "";
                        Schedule.Center.Discipline = "all";
                        var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
                        $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, "slow");
                        Schedule.Center.PatientId = patientId;
                        Schedule.Center.PatientToSelect = "";
                        Schedule.Center.Load();
                    }
                }
            }
        },
        Activities: {
            FixPosition: function(e, callBack) {
                var top = $(e).closest(".ui-layout-center").find(".top").height();
                if ($(".above", e).is(":visible")) top += $(".above", e).height();
                $(e).animate({ top: top }, function() {
                    if (typeof callBack == "function") callBack();
                });
            },
            Populate: function(events) {
                var grid = $("#ScheduleActivityGrid"),
                    gridData = grid.data("tGrid");
                if (Schedule.Center.IsDisciplineChange) {
                    $(".t-grid-content tr", grid).each(function() {
                        var discipline = $(this).data("discipline"),
                            disciplineId = Schedule.Center.GetDisciplineId(Schedule.Center.Discipline);
                        $(this).toggle(Schedule.Center.Discipline == "all" || discipline == disciplineId || (disciplineId == -1 && (discipline == 2 || discipline == 3 || discipline == 4)));
                    });
                } else {
                    gridData.data = [];
                    gridData.bindTo(events);
                }
                //if (!events.length) $(".t-grid-content tbody", grid).empty().append($("<tr/>").addClass("t-no-data").append($("<td/>").attr("colspan", gridData.columns.length).append($("<div/>").html("No Tasks could be found."))));
            },
            OnDataBound: function(e) {
                var grid = $(e.target).data("tGrid");
                if (!grid.hasCompletedInitialLoading) {
                    grid.hasCompletedInitialLoading = true;
                }
                if (Schedule.Center.Discipline != "all")
                    $(".t-grid-content tr", e.target).each(function() {
                        var discipline = $(this).data("discipline"),
                            disciplineId = Schedule.Center.GetDisciplineId(Schedule.Center.Discipline);
                        $(this).toggle(discipline == disciplineId || (disciplineId == -1 && (discipline == 2 || discipline == 3 || discipline == 4)));
                    });
                else $(".t-grid-content tr", e.target).show();
            },
            OnRowDataBound: function(e) { GridProcessor.ProcessActivityRow(e.row, e.dataItem); },
            OnDataBinding: function(e) {
                var grid = $(e.target).data("tGrid");
                if (!grid.hasFirstClientBindingOccured) {
                    grid.hasFirstClientBindingOccured = true;
                    e.preventDefault();
                }
            }
        },
        Scheduler: {
            // Function for Adding to Schedule
            Add: function(button) {
                button = $(button);
                var e = button.closest(".scheduler"),
                multiple = $(button).hasClass("multiple"),
                uri = multiple ? "Schedule/AddMultiple" : "Schedule/Add",
                input = {},
                isValid = true;
                $("input, select", e).each(function() {
                    var value = $(this).val();
                    if (!value || value == U.GuidEmpty) {
                        $(this).addClass("red");
                        isValid = false;
                    } else $(this).removeClass("red");
                });
                if (!isValid) {
                    U.Growl("Not all of the options have been filled.", "error");
                    return;
                }
                if (!multiple) {
                    input = [];
                    input.push({ name: "PatientId", value: Schedule.Center.PatientId });
                    input.push({ name: "EpisodeId", value: Schedule.Center.EpisodeId });
                    var i = 0;
                    $("ol li", e).each(function() {
                        var disciplineTask = $("[name=DisciplineTask] option", this).filter(":selected");
                        if (disciplineTask && $("[name=UserId]", this).val() && $("[name=EventDate]", this).val()) {
                            input.push({ name: "Tasks[" + i + "].DisciplineTask", value: disciplineTask.val() });
                            input.push({ name: "Tasks[" + i + "].UserId", value: $("[name=UserId]", this).val() });
                            input.push({ name: "Tasks[" + i + "].EventDate", value: $("[name=EventDate]", this).val() });
                            input.push({ name: "Tasks[" + i + "].Discipline", value: disciplineTask.attr("discipline") });
                            input.push({ name: "Tasks[" + i + "].IsBillable", value: disciplineTask.attr("isbillable") });
                            i++;
                        }
                    });
                } else {
                    var disciplineTask = $("[name=DisciplineTask] option", e).filter(":selected");
                    input = {
                        EpisodeId: Schedule.Center.EpisodeId,
                        PatientId: Schedule.Center.PatientId,
                        DisciplineTask: disciplineTask.val(),
                        Discipline: disciplineTask.attr("discipline"),
                        UserId: $("[name=UserId]", e).val(),
                        IsBillable: disciplineTask.attr("isbillable"),
                        StartDate: $("[name=StartDate]", e).val(),
                        EndDate: $("[name=EndDate]", e).val()
                    };
                }
                button.trigger("ProcessingStart");
                U.PostUrl(uri, input, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) { Schedule.Center.RefreshSchedule(Schedule.Center.PatientId); }
                    button.trigger("ProcessingComplete");
                });
            },
            // For date-picking on the current episode
            CalendarModal: function(input) {
                var e = $("#window_" + Schedule.Center.AcoreId + "_content"),
                    cals = $(".cal", e).clone(true);
                Acore.Modal({
                    Name: "Date Picker",
                    Content: $("<div/>").addClass("wrapper main ac").append(
                    $("<p class='strong'>Select a day on the calendar to change the date.</p>")).append($("<div/>").addClass("calendar-container").append(
                        cals.css("width", (84 / cals.length) + "%"))).append(
                        $("<div/>").addClass("clear")).append(
                        $("<div/>").Buttons([{ Text: "Cancel", Click: function() { $(this).closest(".window").Close(); } }])),
                    Width: "75%",
                    Height: "45%"
                });
                $("#window_" + Acore.DialogId).find(".datelabel").parent().bind("click", function() {
                    $(input).val($("a", this).attr("date"));
                    $(this).closest(".window").Close();
                });
            },
            // Initalize Scheduler
            Init: function(r, t, x, e) {
                var id = "#" + Schedule.Center.AcoreId + "_TabStrip";
                // Clicking on scheduler tab
                $(".show-scheduler", e).bind("click", function() {
                    Schedule.Center.IsDisciplineChange = true;
                    Schedule.Center.Scheduler.ShowHide();
                });
                // Initialize tabs
                $(id).tabs({
                    show: function(event, ui) {
                        if ($(id).is(":visible")) {
                            Schedule.Center.ChangeDiscipline($(ui.tab).attr("discipline") == "Multiple" ? "all" : $(ui.tab).attr("discipline"));
                            if (!$("ol li", ui.panel).length) Schedule.Center.Scheduler.RowAdd();
                            else {
                                var permRow = $(".permanent", ui.panel);
                                if (permRow.length && $("select:empty", permRow).length) Schedule.Center.PopulateDropDowns(permRow);
                            }
                        }
                    }
                });
                // Setup Multiple Tab
                $("ol li [type=text]", "#" + Schedule.Center.AcoreId + "_OutlierTab").bind("click", function() {
                    Schedule.Center.Scheduler.CalendarModal(this);
                });
                // Initialize buttons
                $(".schedule-save", e).bind("click", function() {
                    Schedule.Center.Scheduler.Add(this);
                });
                $(".schedule-cancel", e).bind("click", function() {
                    Schedule.Center.Scheduler.ShowHide(e);
                });
            },
            // Reset Scheduler
            Reset: function(e) {
                $(".scheduler ol li", e).not(".permanent").remove();
                $(".scheduler .permanent", e).find("input,select").val("");
                if ($(".scheduler", e).is(":visible")) Schedule.Center.Scheduler.ShowHide();
                Schedule.Center.Discipline = "all";
                Schedule.Center.Render();
                Schedule.Center.IsDisciplineChange = false;
            },
            // Function for Adding Row from Scheduler
            RowAdd: function(date) {
                var e = $("#" + Schedule.Center.AcoreId + "_TabStrip"),
                    tabContent = $($(".ui-tabs-selected > a", e).attr("href"), e),
                    discipline = $(".ui-tabs-selected > a", e).attr("discipline");
                // If scheduler is hidden, display it
                if (e.is(":hidden")) Schedule.Center.Scheduler.ShowHide(date);
                // If empty slot found, set date
                else if ($("input:blank", tabContent).length) $("input:blank:first", tabContent).val(date);
                // Add new row, if space permits, set date on new row
                else if ($("ol li", tabContent).length < Schedule.Center.SchedulerMaxLines) {
                    if (discipline != "Multiple") {
                        var scheduleContent = $("ol", tabContent);
                        var firstLi = $("li.schedule-row", scheduleContent).first().clone(true);
                        if (firstLi.length > 0) {
                            $("[name=EventDate]", firstLi).val(date);
                            $(scheduleContent).append(firstLi);
                        }
                        else {
                            $(scheduleContent).append(
                            $("<li/>", { "class": "schedule-row" }).append(
                                $("<span/>").DeleteIcon({ Size: '1.75em' }).bind("click", function() {
                                    Schedule.Center.Scheduler.RowRemove(this);
                                })).append(
                                $("<span/>").addClass("grid-third").append(
                                    $("<select/>", { name: "DisciplineTask" }).append(
                                        $("<option/>").val("0").text("-- Select Task --")))).append(
                                $("<span/>").addClass("grid-third").append(
                                    $("<select/>", { name: "UserId" }).append(
                                        $("<option/>").val("0").text("-- Select User --")))).append(
                                $("<span/>").append(
                                    $("<input/>", { type: "text", name: "EventDate", value: date }).bind("click", function() {
                                        Schedule.Center.Scheduler.CalendarModal(this);
                                    })))).Zebra();
                            var scheduleRow = $(".schedule-row", scheduleContent);
                            Schedule.Center.LoadDropDowns(scheduleRow);
                        }
                    }
                } else U.Growl("Can only add " + Schedule.Center.SchedulerMaxLines + " items at a time in the scheduler.", "warning");
            },
            // Function for Removing Row from Scheduler
            RowRemove: function(e) {
                var list = $(e).closest("ol");
                $(e).closest("li").remove();
                if ($("li", list).length == 0) Schedule.Center.Scheduler.ShowHide();
                else list.Zebra();
            },
            // Function to Toggle the Scheduler
            ShowHide: function(date) {
                var e = $("#window_" + Schedule.Center.AcoreId + "_content"),
                    scheduler = $("#" + Schedule.Center.AcoreId + "_TabStrip");
                // If Scheduler is Open, Close It
                if (scheduler.is(":visible")) {
                    // Toggle Show/Hide Text on Tab
                    $(".show-scheduler", e).text("Show Scheduler");
                    // Hide Scheduler
                    scheduler.hide();
                    // Adjust Activites Grid Position for New Top Panel Height
                    Schedule.Center.Activities.FixPosition($(".ui-layout-center .bottom", e), Schedule.Center.Scheduler.Reset);
                    // If Scheduler is Closed, Open It
                } else {
                    // Toggle Show/Hide Text on Tab
                    Schedule.Center.ChangeDiscipline("Nursing");
                    scheduler.tabs("select", 0);
                    $(".show-scheduler", e).text("Hide Scheduler");
                    // Show Scheduler
                    scheduler.show();
                    // If No Rows on Current Tab, Add One
                    if ($($(".ui-tabs-selected > a", e).attr("href")).find("ol > li").length == 0) Schedule.Center.Scheduler.RowAdd(date);
                    // Hide All the Elements Hidden when Scheduler is Open
                    // Scroll Activites to Show Scheduler
                    Schedule.Center.Activities.FixPosition($(".ui-layout-center .bottom", e));
                }
            }
        },
        PopulateDropDowns: function(e) {
            //if e has the class ui-layout-center than it must be the main result and needs to populate the episode list
            if (e.hasClass("ui-layout-center")) {
                var episodeDropDown = $("#ScheduleCenter_EpisodeDropDown", e).empty();
                $.each(Schedule.Center.EpisodeList, function() {
                    episodeDropDown.append($("<option/>", { value: this.id, text: this.dateRange }).prop("selected", Schedule.Center.EpisodeId == this.id));
                });
                //Otherwise e contains a user and discipline task drop down that needs to be populated.
            } else {
                //Readds the data to the drop downs
                if (Schedule.Center.UserDropDown.length) {
                    $("[name=UserId]", e).empty().Select({ Options: Schedule.Center.UserDropDown, BlankEntry: false });
                }
                if (Schedule.Center.TaskDropDown.length) {
                    $("[name=DisciplineTask]", e).empty().prepend($("<option/>", { value: "", text: "-- Select Task --" }));
                    var disciplineTasksDropDown = $("[name=DisciplineTask]", e),
                        tasks = Schedule.Center.FilteredTaskDropDown.length ? Schedule.Center.FilteredTaskDropDown : Schedule.Center.TaskDropDown;
                    $.each(tasks, function() {
                        disciplineTasksDropDown.append($("<option/>", { value: $(this).attr("value"), text: $(this).attr("text"), Discipline: $(this).attr("Discipline"), IsBillable: $(this).attr("IsBillable") }));
                    });
                }
            }
        },
        PositionBottom: function() {
            $("#window_ScheduleCenter .ui-layout-center .bottom").css("top", $("#window_ScheduleCenter .ui-layout-center .top").height());
        },
        Render: function() {
            var events;
            if (Schedule.Center.Discipline == "Nursing") events = Schedule.Center.NursingData;
            else if (Schedule.Center.Discipline == "HHA") events = Schedule.Center.HHAData;
            else if (Schedule.Center.Discipline == "MSW") events = Schedule.Center.MSWData;
            else if (Schedule.Center.Discipline == "Therapy") events = Schedule.Center.TherapyData;
            else if (Schedule.Center.Discipline == "Dietician") events = Schedule.Center.DieticianData;
            else if (Schedule.Center.Discipline == "Orders") events = Schedule.Center.OrdersData;
            else events = Schedule.Center.EpisodeData;
            $("#ScheduleCenter_PatientName").text(Schedule.Center.PatientName);
            if (Schedule.Center.IsRebind) Schedule.Center.BuildCalendar(events);
            else Schedule.Center.RebindCalendar(events);
            Schedule.Center.Activities.Populate(events);
        }
        ,
        Rerender: function(isBuilCalender) {
            var events;
            if (Schedule.Center.Discipline == "Nursing") events = Schedule.Center.NursingData;
            else if (Schedule.Center.Discipline == "HHA") events = Schedule.Center.HHAData;
            else if (Schedule.Center.Discipline == "MSW") events = Schedule.Center.MSWData;
            else if (Schedule.Center.Discipline == "Therapy") events = Schedule.Center.TherapyData;
            else if (Schedule.Center.Discipline == "Dietician") events = Schedule.Center.DieticianData;
            else if (Schedule.Center.Discipline == "Orders") events = Schedule.Center.OrdersData;
            else events = Schedule.Center.EpisodeData;
            if (isBuilCalender) Schedule.Center.BuildCalendar(events);
            else Schedule.Center.RebindCalendar(events);
            Schedule.Center.Activities.Populate(events);
        }
    }
});