﻿var IncidentReport = {
    Delete: function(patientId, episodeId, id) {
        U.DeleteTemplateWithInput("IncidentReport", { PatientId: patientId, EpisodeId: episodeId, Id: id },
        function() {
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
        }, "Incident Log");
    },
    InitEdit: function(r, t, x, e) {
        U.InitEditTemplate("IncidentReport", function() {
            var patientId = $("input[name=PatientId]", e).val(),
                status = $("input[name=Status]", e).val();
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
            IncidentReport.RebindList();
            if (status == 520) Agency.RebindCaseManagement();
        }, "Incident Log");
        $("#window_editincidentreport .Physicians").PhysicianInput();
    },
    InitNew: function(r, t, x, e) {
        U.InitNewTemplate("IncidentReport", function() {
            var patientId = $("select[name=PatientId]", e).val(),
                status = $("input[name=Status]", e).val();
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
            IncidentReport.RebindList();
            if (status == 520) Agency.RebindCaseManagement();
        }, "incident log");
        U.ShowIfOtherChecked($("#New_Incident_IndividualInvolved4"), $("#New_Incident_IndividualInvolvedOther"));
        $("#window_newincidentreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.RebindTGrid($('#List_IncidentReport')); },
    ProcessIncident: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Schedule.Center.RefreshSchedule(patientId);
                        Patient.Charts.Activities.Refresh(patientId);
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Schedule.Center.RefreshSchedule(patientId);
                    Patient.Charts.Activities.Refresh(patientId);
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}

var InfectionReport = {
    Delete: function(patientId, episodeId, id) {
        U.DeleteTemplateWithInput("InfectionReport", { PatientId: patientId, EpisodeId: episodeId, Id: id },
        function() {
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
            InfectionReport.RebindList();
        });
    },
    InitEdit: function(r, t, x, e) {
        U.InitEditTemplate("InfectionReport", function() {
            var patientId = $("input[name=PatientId]", e).val(),
                status = $("input[name=Status]", e).val();
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
            InfectionReport.RebindList();
            if (status == 520) Agency.RebindCaseManagement();
        });
        $("#window_editinfectionreport .Physicians").PhysicianInput();
    },
    InitNew: function(r, t, x, e) {
        U.InitNewTemplate("InfectionReport", function() {
            var patientId = $("select[name=PatientId]", e).val(),
                status = $("input[name=Status]", e).val();
            Schedule.Center.RefreshSchedule(patientId);
            Patient.Charts.Activities.Refresh(patientId);
            InfectionReport.RebindList();
            if (status == 520) Agency.RebindCaseManagement();
        }, "infection report");
        U.ShowIfOtherChecked($("#New_Infection_InfectionType6"), $("#New_Infection_InfectionTypeOther"));
        $("#window_newinfectionreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.RebindTGrid($('#List_InfectionReport')); },
    ProcessInfection: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Schedule.Center.RefreshSchedule(patientId);
                        Patient.Charts.Activities.Refresh(patientId);
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Schedule.Center.RefreshSchedule(patientId);
                    Patient.Charts.Activities.Refresh(patientId);
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}