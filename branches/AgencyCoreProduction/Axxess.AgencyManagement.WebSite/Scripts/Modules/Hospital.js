﻿var Hospital = {
    Delete: function(Id) { U.DeleteTemplate("Hospital", Id); },
    InitEdit: function() {
        U.InitEditTemplate("Hospital");
        U.PhoneAutoTab("Edit_Hospital_PhoneArray");
        U.PhoneAutoTab("Edit_Hospital_FaxNumberArray");
    },
    InitNew: function() {
        U.InitNewTemplate("Hospital");
        U.PhoneAutoTab("New_Hospital_PhoneArray");
        U.PhoneAutoTab("New_Hospital_FaxNumberArray");
    },
    RebindList: function() { U.RebindTGrid($('#List_AgencyHospital')); }
}